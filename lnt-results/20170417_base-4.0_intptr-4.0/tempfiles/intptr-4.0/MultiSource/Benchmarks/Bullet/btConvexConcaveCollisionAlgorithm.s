	.text
	.file	"btConvexConcaveCollisionAlgorithm.bc"
	.globl	_ZN33btConvexConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b,@function
_ZN33btConvexConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b: # @_ZN33btConvexConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movq	%rcx, %rbp
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV33btConvexConcaveCollisionAlgorithm+16, (%rbx)
	movb	%r15b, 16(%rbx)
	leaq	24(%rbx), %r14
	movq	(%r13), %rdi
	movq	$_ZTV24btConvexTriangleCallback+16, 24(%rbx)
	movq	%rdi, 88(%rbx)
	movq	$0, 96(%rbx)
	testb	%r15b, %r15b
	movq	%r12, %rsi
	cmovneq	%rbp, %rsi
	movq	%rsi, 32(%rbx)
	cmovneq	%r12, %rbp
	movq	%rbp, 40(%rbx)
	movq	(%rdi), %rax
.Ltmp0:
	movq	%rbp, %rdx
	callq	*24(%rax)
.Ltmp1:
# BB#1:
	movq	%rax, 112(%rbx)
	movq	88(%rbx), %rdi
	movq	(%rdi), %rcx
.Ltmp2:
	movq	%rax, %rsi
	callq	*40(%rcx)
.Ltmp3:
# BB#2:                                 # %_ZN24btConvexTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_3:
.Ltmp4:
	movq	%rax, %r15
.Ltmp5:
	movq	%r14, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp6:
# BB#4:                                 # %.body
.Ltmp8:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp9:
# BB#5:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_6:
.Ltmp10:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_7:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN33btConvexConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b, .Lfunc_end0-_ZN33btConvexConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end0-.Ltmp9      #   Call between .Ltmp9 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN33btConvexConcaveCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithmD2Ev,@function
_ZN33btConvexConcaveCollisionAlgorithmD2Ev: # @_ZN33btConvexConcaveCollisionAlgorithmD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	$_ZTV33btConvexConcaveCollisionAlgorithm+16, (%rbx)
	leaq	24(%rbx), %r14
	movq	$_ZTV24btConvexTriangleCallback+16, 24(%rbx)
	movq	88(%rbx), %rdi
	movq	112(%rbx), %rsi
	movq	(%rdi), %rax
.Ltmp11:
	callq	*40(%rax)
.Ltmp12:
# BB#1:                                 # %_ZN24btConvexTriangleCallback10clearCacheEv.exit.i
	movq	88(%rbx), %rdi
	movq	112(%rbx), %rsi
	movq	(%rdi), %rax
.Ltmp13:
	callq	*32(%rax)
.Ltmp14:
# BB#2:
.Ltmp19:
	movq	%r14, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp20:
# BB#3:                                 # %_ZN24btConvexTriangleCallbackD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN30btActivatingCollisionAlgorithmD2Ev # TAILCALL
.LBB2_6:
.Ltmp21:
	movq	%rax, %r15
	jmp	.LBB2_7
.LBB2_4:
.Ltmp15:
	movq	%rax, %r15
.Ltmp16:
	movq	%r14, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp17:
.LBB2_7:                                # %.body
.Ltmp22:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp23:
# BB#8:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB2_5:
.Ltmp18:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_9:
.Ltmp24:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN33btConvexConcaveCollisionAlgorithmD2Ev, .Lfunc_end2-_ZN33btConvexConcaveCollisionAlgorithmD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp14-.Ltmp11         #   Call between .Ltmp11 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp20         #   Call between .Ltmp20 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Lfunc_end2-.Ltmp23     #   Call between .Ltmp23 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN33btConvexConcaveCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithmD0Ev,@function
_ZN33btConvexConcaveCollisionAlgorithmD0Ev: # @_ZN33btConvexConcaveCollisionAlgorithmD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp25:
	callq	_ZN33btConvexConcaveCollisionAlgorithmD2Ev
.Ltmp26:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp27:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN33btConvexConcaveCollisionAlgorithmD0Ev, .Lfunc_end3-_ZN33btConvexConcaveCollisionAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp25-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin2   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp26     #   Call between .Ltmp26 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN33btConvexConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN33btConvexConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN33btConvexConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 48
.Lcfi29:
	.cfi_offset %rbx, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	112(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB4_17
# BB#1:
	movl	4(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB4_16
# BB#2:
	leal	(%rax,%rax), %edx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%edx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB4_16
# BB#3:
	testl	%ebp, %ebp
	je	.LBB4_4
# BB#5:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB4_7
	jmp	.LBB4_11
.LBB4_4:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB4_11
.LBB4_7:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB4_9
	.p2align	4, 0x90
.LBB4_8:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB4_8
.LBB4_9:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB4_11
	.p2align	4, 0x90
.LBB4_10:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB4_10
.LBB4_11:                               # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_15
# BB#12:
	cmpb	$0, 24(%rbx)
	je	.LBB4_14
# BB#13:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%rbx), %eax
.LBB4_14:
	movq	$0, 16(%rbx)
.LBB4_15:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
	movb	$1, 24(%rbx)
	movq	%r15, 16(%rbx)
	movl	%ebp, 8(%rbx)
	movq	112(%r14), %rcx
.LBB4_16:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
	movq	16(%rbx), %rdx
	cltq
	movq	%rcx, (%rdx,%rax,8)
	incl	%eax
	movl	%eax, 4(%rbx)
.LBB4_17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN33btConvexConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end4-_ZN33btConvexConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.globl	_ZN24btConvexTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b
	.p2align	4, 0x90
	.type	_ZN24btConvexTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b,@function
_ZN24btConvexTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b: # @_ZN24btConvexTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 32
.Lcfi36:
	.cfi_offset %rbx, -24
.Lcfi37:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV24btConvexTriangleCallback+16, (%rbx)
	movq	%rsi, 64(%rbx)
	movq	$0, 72(%rbx)
	testb	%r8b, %r8b
	movq	%rdx, %rax
	cmovneq	%rcx, %rax
	movq	%rax, 8(%rbx)
	cmovneq	%rdx, %rcx
	movq	%rcx, 16(%rbx)
	movq	(%rsi), %r8
.Ltmp28:
	movq	%rsi, %rdi
	movq	%rax, %rsi
	movq	%rcx, %rdx
	callq	*24(%r8)
.Ltmp29:
# BB#1:
	movq	%rax, 88(%rbx)
	movq	64(%rbx), %rdi
	movq	(%rdi), %rcx
.Ltmp30:
	movq	%rax, %rsi
	callq	*40(%rcx)
.Ltmp31:
# BB#2:                                 # %_ZN24btConvexTriangleCallback10clearCacheEv.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_3:
.Ltmp32:
	movq	%rax, %r14
.Ltmp33:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp34:
# BB#4:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_5:
.Ltmp35:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN24btConvexTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b, .Lfunc_end5-_ZN24btConvexTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp28-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp31-.Ltmp28         #   Call between .Ltmp28 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin3   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin3   #     jumps to .Ltmp35
	.byte	1                       #   On action: 1
	.long	.Ltmp34-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end5-.Ltmp34     #   Call between .Ltmp34 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN24btConvexTriangleCallback10clearCacheEv
	.p2align	4, 0x90
	.type	_ZN24btConvexTriangleCallback10clearCacheEv,@function
_ZN24btConvexTriangleCallback10clearCacheEv: # @_ZN24btConvexTriangleCallback10clearCacheEv
	.cfi_startproc
# BB#0:
	movq	64(%rdi), %rax
	movq	88(%rdi), %rsi
	movq	(%rax), %rcx
	movq	40(%rcx), %rcx
	movq	%rax, %rdi
	jmpq	*%rcx                   # TAILCALL
.Lfunc_end6:
	.size	_ZN24btConvexTriangleCallback10clearCacheEv, .Lfunc_end6-_ZN24btConvexTriangleCallback10clearCacheEv
	.cfi_endproc

	.globl	_ZN24btConvexTriangleCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN24btConvexTriangleCallbackD2Ev,@function
_ZN24btConvexTriangleCallbackD2Ev:      # @_ZN24btConvexTriangleCallbackD2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -24
.Lcfi42:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV24btConvexTriangleCallback+16, (%rbx)
	movq	64(%rbx), %rdi
	movq	88(%rbx), %rsi
	movq	(%rdi), %rax
.Ltmp36:
	callq	*40(%rax)
.Ltmp37:
# BB#1:                                 # %_ZN24btConvexTriangleCallback10clearCacheEv.exit
	movq	64(%rbx), %rdi
	movq	88(%rbx), %rsi
	movq	(%rdi), %rax
.Ltmp38:
	callq	*32(%rax)
.Ltmp39:
# BB#2:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN18btTriangleCallbackD2Ev # TAILCALL
.LBB7_3:
.Ltmp40:
	movq	%rax, %r14
.Ltmp41:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp42:
# BB#4:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_5:
.Ltmp43:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN24btConvexTriangleCallbackD2Ev, .Lfunc_end7-_ZN24btConvexTriangleCallbackD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp36-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp39-.Ltmp36         #   Call between .Ltmp36 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin4   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp41-.Ltmp39         #   Call between .Ltmp39 and .Ltmp41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin4   #     jumps to .Ltmp43
	.byte	1                       #   On action: 1
	.long	.Ltmp42-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp42     #   Call between .Ltmp42 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN24btConvexTriangleCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN24btConvexTriangleCallbackD0Ev,@function
_ZN24btConvexTriangleCallbackD0Ev:      # @_ZN24btConvexTriangleCallbackD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -24
.Lcfi47:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV24btConvexTriangleCallback+16, (%rbx)
	movq	64(%rbx), %rdi
	movq	88(%rbx), %rsi
	movq	(%rdi), %rax
.Ltmp44:
	callq	*40(%rax)
.Ltmp45:
# BB#1:                                 # %_ZN24btConvexTriangleCallback10clearCacheEv.exit.i
	movq	64(%rbx), %rdi
	movq	88(%rbx), %rsi
	movq	(%rdi), %rax
.Ltmp46:
	callq	*32(%rax)
.Ltmp47:
# BB#2:
.Ltmp52:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp53:
# BB#3:                                 # %_ZN24btConvexTriangleCallbackD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_6:
.Ltmp54:
	movq	%rax, %r14
	jmp	.LBB8_7
.LBB8_4:
.Ltmp48:
	movq	%rax, %r14
.Ltmp49:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp50:
.LBB8_7:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_5:
.Ltmp51:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN24btConvexTriangleCallbackD0Ev, .Lfunc_end8-_ZN24btConvexTriangleCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp44-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp47-.Ltmp44         #   Call between .Ltmp44 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin5   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin5   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin5   #     jumps to .Ltmp51
	.byte	1                       #   On action: 1
	.long	.Ltmp50-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp50     #   Call between .Ltmp50 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN24btConvexTriangleCallback15processTriangleEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN24btConvexTriangleCallback15processTriangleEP9btVector3ii,@function
_ZN24btConvexTriangleCallback15processTriangleEP9btVector3ii: # @_ZN24btConvexTriangleCallback15processTriangleEP9btVector3ii
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 208
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%edx, %r14d
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	16(%rbx), %r15
	movq	64(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.LBB9_4
# BB#1:
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB9_4
# BB#2:
	movq	(%rdi), %rax
	callq	*96(%rax)
	testb	$1, %al
	je	.LBB9_4
# BB#3:
	movabsq	$4863606123715821568, %rax # imm = 0x437F0000437F0000
	movq	%rax, 40(%rsp)
	movq	$0, 48(%rsp)
	movq	72(%rbx), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movss	(%r13), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%r15), %xmm8         # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	28(%r15), %xmm9         # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm2    # xmm2 = xmm2[0],xmm9[0],xmm2[1],xmm9[1]
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm3, %xmm1
	movss	32(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	addps	%xmm1, %xmm4
	movsd	56(%r15), %xmm10        # xmm10 = mem[0],zero
	addps	%xmm10, %xmm4
	movss	40(%r15), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm7
	movss	44(%r15), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm7, %xmm0
	movss	48(%r15), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm5
	addss	%xmm0, %xmm5
	movss	64(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm5
	xorps	%xmm1, %xmm1
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	movss	16(%r13), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	20(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1]
	movaps	%xmm7, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	unpcklps	%xmm9, %xmm6    # xmm6 = xmm6[0],xmm9[0],xmm6[1],xmm9[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm6, %xmm3
	addps	%xmm5, %xmm3
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm2, %xmm5
	addps	%xmm3, %xmm5
	addps	%xmm10, %xmm5
	mulss	%xmm11, %xmm7
	mulss	%xmm12, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm13, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	leaq	40(%rsp), %rbp
	movq	%rbp, %rcx
	callq	*%rax
	movq	72(%rbx), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movss	16(%r13), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	20(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%r15), %xmm8         # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	28(%r15), %xmm9         # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm2    # xmm2 = xmm2[0],xmm9[0],xmm2[1],xmm9[1]
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm3, %xmm1
	movss	32(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	addps	%xmm1, %xmm4
	movsd	56(%r15), %xmm10        # xmm10 = mem[0],zero
	addps	%xmm10, %xmm4
	movss	40(%r15), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm7
	movss	44(%r15), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm7, %xmm0
	movss	48(%r15), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm5
	addss	%xmm0, %xmm5
	movss	64(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm5
	xorps	%xmm1, %xmm1
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	movss	32(%r13), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	36(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	40(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1]
	movaps	%xmm7, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	unpcklps	%xmm9, %xmm6    # xmm6 = xmm6[0],xmm9[0],xmm6[1],xmm9[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm6, %xmm3
	addps	%xmm5, %xmm3
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm2, %xmm5
	addps	%xmm3, %xmm5
	addps	%xmm10, %xmm5
	mulss	%xmm11, %xmm7
	mulss	%xmm12, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm13, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%rbp, %rcx
	callq	*%rax
	movq	72(%rbx), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movss	32(%r13), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	36(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	40(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%r15), %xmm8         # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	28(%r15), %xmm9         # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm2    # xmm2 = xmm2[0],xmm9[0],xmm2[1],xmm9[1]
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm3, %xmm1
	movss	32(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	addps	%xmm1, %xmm4
	movsd	56(%r15), %xmm10        # xmm10 = mem[0],zero
	addps	%xmm10, %xmm4
	movss	40(%r15), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm7
	movss	44(%r15), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm7, %xmm0
	movss	48(%r15), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm5
	addss	%xmm0, %xmm5
	movss	64(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm5
	xorps	%xmm1, %xmm1
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	movss	(%r13), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1]
	movaps	%xmm7, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	unpcklps	%xmm9, %xmm6    # xmm6 = xmm6[0],xmm9[0],xmm6[1],xmm9[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm6, %xmm3
	addps	%xmm5, %xmm3
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm2, %xmm5
	addps	%xmm3, %xmm5
	addps	%xmm10, %xmm5
	mulss	%xmm11, %xmm7
	mulss	%xmm12, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm13, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%rbp, %rcx
	callq	*%rax
.LBB9_4:
	movq	8(%rbx), %rax
	movq	200(%rax), %rax
	cmpl	$19, 8(%rax)
	jg	.LBB9_11
# BB#5:
	leaq	40(%rsp), %rbp
	movq	%rbp, %rdi
	callq	_ZN23btPolyhedralConvexShapeC2Ev
	movq	$_ZTV15btTriangleShape+16, 40(%rsp)
	movl	$1, 48(%rsp)
	movups	(%r13), %xmm0
	movups	%xmm0, 104(%rsp)
	movups	16(%r13), %xmm0
	movups	%xmm0, 120(%rsp)
	movups	32(%r13), %xmm0
	movups	%xmm0, 136(%rsp)
	movl	80(%rbx), %eax
	movl	%eax, 96(%rsp)
	movq	200(%r15), %r13
	movq	%rbp, 200(%r15)
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	88(%rbx), %rcx
.Ltmp55:
	callq	*16(%rax)
	movq	%rax, %rbp
.Ltmp56:
# BB#6:
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp58:
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	*24(%rax)
.Ltmp59:
# BB#7:
	movq	(%rbp), %rax
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	72(%rbx), %rcx
	movq	56(%rbx), %r8
.Ltmp60:
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp61:
# BB#8:
	movq	(%rbp), %rax
.Ltmp62:
	movq	%rbp, %rdi
	callq	*(%rax)
.Ltmp63:
# BB#9:
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp64:
	movq	%rbp, %rsi
	callq	*104(%rax)
.Ltmp65:
# BB#10:
	movq	%r13, 200(%r15)
	leaq	40(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.LBB9_11:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_12:
.Ltmp57:
	jmp	.LBB9_14
.LBB9_13:
.Ltmp66:
.LBB9_14:
	movq	%rax, %rbx
.Ltmp67:
	leaq	40(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp68:
# BB#15:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_16:
.Ltmp69:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN24btConvexTriangleCallback15processTriangleEP9btVector3ii, .Lfunc_end9-_ZN24btConvexTriangleCallback15processTriangleEP9btVector3ii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp55-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin6   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp65-.Ltmp58         #   Call between .Ltmp58 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin6   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Ltmp67-.Ltmp65         #   Call between .Ltmp65 and .Ltmp67
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin6   # >> Call Site 5 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin6   #     jumps to .Ltmp69
	.byte	1                       #   On action: 1
	.long	.Ltmp68-.Lfunc_begin6   # >> Call Site 6 <<
	.long	.Lfunc_end9-.Ltmp68     #   Call between .Ltmp68 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end10-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN24btConvexTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN24btConvexTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult,@function
_ZN24btConvexTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult: # @_ZN24btConvexTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 16
	subq	$208, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 224
.Lcfi63:
	.cfi_offset %rbx, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	movq	%rsi, 72(%rbx)
	movss	%xmm0, 80(%rbx)
	movq	%rdx, 56(%rbx)
	movq	8(%rbx), %rax
	movq	16(%rbx), %rcx
	movsd	8(%rcx), %xmm8          # xmm8 = mem[0],zero
	movsd	24(%rcx), %xmm6         # xmm6 = mem[0],zero
	movsd	40(%rcx), %xmm3         # xmm3 = mem[0],zero
	movss	16(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	%xmm7, 4(%rsp)          # 4-byte Spill
	movss	32(%rcx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	48(%rcx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	%xmm10, 8(%rsp)         # 4-byte Spill
	movd	56(%rcx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movdqa	.LCPI11_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm13
	movd	60(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movdqa	%xmm1, %xmm2
	pxor	%xmm0, %xmm2
	movd	64(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	pxor	%xmm4, %xmm0
	pshufd	$224, %xmm13, %xmm5     # xmm5 = xmm13[0,0,2,3]
	mulps	%xmm8, %xmm5
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm6, %xmm2
	addps	%xmm5, %xmm2
	pshufd	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm2, %xmm0
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	mulss	%xmm7, %xmm13
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm13
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm13
	movss	8(%rax), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	movaps	%xmm2, %xmm0
	mulss	%xmm12, %xmm0
	movss	24(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	40(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, (%rsp)           # 4-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm0
	movaps	%xmm2, 80(%rsp)         # 16-byte Spill
	mulss	%xmm14, %xmm0
	movss	28(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm4
	movaps	%xmm6, %xmm10
	movaps	%xmm10, 96(%rsp)        # 16-byte Spill
	mulss	%xmm5, %xmm4
	addss	%xmm0, %xmm4
	movss	44(%rax), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm15, %xmm0
	addss	%xmm4, %xmm0
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movss	16(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	mulss	%xmm11, %xmm6
	movss	32(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm8
	mulss	%xmm4, %xmm8
	addss	%xmm6, %xmm8
	movss	48(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	mulss	%xmm6, %xmm0
	addss	%xmm8, %xmm0
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	pshufd	$229, %xmm2, %xmm3      # xmm3 = xmm2[1,1,2,3]
	movdqa	%xmm3, %xmm8
	mulss	%xmm12, %xmm8
	pshufd	$229, %xmm10, %xmm2     # xmm2 = xmm10[1,1,2,3]
	movdqa	%xmm2, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm8, %xmm0
	pshufd	$229, %xmm1, %xmm10     # xmm10 = xmm1[1,1,2,3]
	movdqa	%xmm10, %xmm1
	mulss	(%rsp), %xmm1           # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	movdqa	%xmm3, %xmm0
	mulss	%xmm14, %xmm0
	movdqa	%xmm2, %xmm1
	mulss	%xmm5, %xmm1
	addss	%xmm0, %xmm1
	movdqa	%xmm10, %xmm8
	mulss	%xmm15, %xmm8
	addss	%xmm1, %xmm8
	mulss	%xmm11, %xmm3
	mulss	%xmm4, %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm6, %xmm10
	addss	%xmm2, %xmm10
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm12
	mulss	%xmm9, %xmm7
	addss	%xmm12, %xmm7
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm7, %xmm2
	movaps	%xmm2, %xmm7
	mulss	%xmm0, %xmm14
	mulss	%xmm9, %xmm5
	addss	%xmm14, %xmm5
	mulss	%xmm1, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm0, %xmm11
	movaps	%xmm0, %xmm2
	mulss	%xmm9, %xmm4
	addss	%xmm11, %xmm4
	mulss	%xmm1, %xmm6
	movaps	%xmm1, %xmm3
	addss	%xmm4, %xmm6
	movss	56(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	80(%rsp), %xmm0         # 16-byte Folded Reload
	movss	60(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm9
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	96(%rsp), %xmm1         # 16-byte Folded Reload
	addps	%xmm0, %xmm1
	movss	64(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	112(%rsp), %xmm0        # 16-byte Folded Reload
	addps	%xmm1, %xmm0
	addps	192(%rsp), %xmm0        # 16-byte Folded Reload
	addss	%xmm2, %xmm9
	addss	%xmm3, %xmm9
	addss	%xmm13, %xmm9
	xorps	%xmm1, %xmm1
	movss	%xmm9, %xmm1            # xmm1 = xmm9[0],xmm1[1,2,3]
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 16(%rsp)
	movaps	160(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 20(%rsp)
	movaps	144(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 24(%rsp)
	movl	$0, 28(%rsp)
	movaps	128(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 32(%rsp)
	movss	%xmm8, 36(%rsp)
	movss	%xmm10, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	%xmm7, 48(%rsp)
	movss	%xmm15, 52(%rsp)
	movss	%xmm6, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm0, 64(%rsp)
	movlps	%xmm1, 72(%rsp)
	movq	8(%rbx), %rax
	movq	200(%rax), %rdi
	movq	(%rdi), %rax
	leaq	24(%rbx), %rdx
	leaq	40(%rbx), %rcx
	leaq	16(%rsp), %rsi
	callq	*16(%rax)
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, 40(%rbx)
	movss	44(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, 44(%rbx)
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, 48(%rbx)
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	%xmm0, 24(%rbx)
	movss	28(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	%xmm0, 28(%rbx)
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	%xmm0, 32(%rbx)
	addq	$208, %rsp
	popq	%rbx
	retq
.Lfunc_end11:
	.size	_ZN24btConvexTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult, .Lfunc_end11-_ZN24btConvexTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.globl	_ZN33btConvexConcaveCollisionAlgorithm10clearCacheEv
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithm10clearCacheEv,@function
_ZN33btConvexConcaveCollisionAlgorithm10clearCacheEv: # @_ZN33btConvexConcaveCollisionAlgorithm10clearCacheEv
	.cfi_startproc
# BB#0:
	movq	88(%rdi), %rax
	movq	112(%rdi), %rsi
	movq	(%rax), %rcx
	movq	40(%rcx), %rcx
	movq	%rax, %rdi
	jmpq	*%rcx                   # TAILCALL
.Lfunc_end12:
	.size	_ZN33btConvexConcaveCollisionAlgorithm10clearCacheEv, .Lfunc_end12-_ZN33btConvexConcaveCollisionAlgorithm10clearCacheEv
	.cfi_endproc

	.globl	_ZN33btConvexConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN33btConvexConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN33btConvexConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 64
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r13
	movq	%rdx, %rbx
	movq	%rdi, %r15
	cmpb	$0, 16(%r15)
	movq	%rsi, %rbp
	cmovneq	%rbx, %rbp
	cmovneq	%rsi, %rbx
	movq	200(%rbx), %r12
	movl	8(%r12), %eax
	addl	$-21, %eax
	cmpl	$8, %eax
	ja	.LBB13_7
# BB#1:
	movq	200(%rbp), %rax
	cmpl	$19, 8(%rax)
	jg	.LBB13_7
# BB#2:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*88(%rax)
	leaq	24(%r15), %rdi
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	112(%r15), %rax
	movq	%rax, 8(%r14)
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	_ZN24btConvexTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult
	movq	112(%r15), %rax
	movq	%rbp, 712(%rax)
	movq	%rbx, 720(%rax)
	movq	(%r12), %rax
	leaq	48(%r15), %rdx
	addq	$64, %r15
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r15, %rcx
	callq	*96(%rax)
	movq	8(%r14), %rdi
	cmpl	$0, 728(%rdi)
	je	.LBB13_7
# BB#3:
	movq	712(%rdi), %rax
	cmpq	144(%r14), %rax
	je	.LBB13_6
# BB#4:
	leaq	80(%r14), %rsi
	addq	$16, %r14
	jmp	.LBB13_5
.LBB13_7:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_6:
	leaq	16(%r14), %rsi
	addq	$80, %r14
.LBB13_5:
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_ # TAILCALL
.Lfunc_end13:
	.size	_ZN33btConvexConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end13-_ZN33btConvexConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI14_2:
	.zero	16
	.text
	.globl	_ZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
	subq	$808, %rsp              # imm = 0x328
.Lcfi79:
	.cfi_def_cfa_offset 832
.Lcfi80:
	.cfi_offset %rbx, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	cmpb	$0, 16(%rdi)
	movq	%rsi, %rbx
	cmovneq	%rdx, %rbx
	cmovneq	%rsi, %rdx
	movss	120(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	124(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	56(%rbx), %xmm0
	subss	60(%rbx), %xmm1
	movss	128(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	64(%rbx), %xmm2
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	268(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movss	.LCPI14_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	ja	.LBB14_20
# BB#1:
	movq	200(%rdx), %rdi
	movl	8(%rdi), %eax
	addl	$-21, %eax
	cmpl	$8, %eax
	ja	.LBB14_20
# BB#2:
	movss	24(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	40(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	12(%rdx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	28(%rdx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	44(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	16(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	32(%rdx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	48(%rdx), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movss	56(%rdx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	xorps	.LCPI14_1(%rip), %xmm12
	movss	60(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm14
	mulss	%xmm12, %xmm14
	movaps	%xmm4, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm14
	movaps	%xmm7, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm14
	movaps	%xmm11, %xmm10
	mulss	%xmm12, %xmm10
	movaps	%xmm13, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm10
	movaps	%xmm6, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm10
	mulss	%xmm5, %xmm12
	mulss	%xmm8, %xmm0
	subss	%xmm0, %xmm12
	mulss	%xmm15, %xmm1
	subss	%xmm1, %xmm12
	movaps	%xmm9, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	120(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	mulps	%xmm0, %xmm2
	movss	124(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	60(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	movaps	%xmm4, 752(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	addps	%xmm2, %xmm3
	movss	128(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	movaps	%xmm7, 384(%rsp)        # 16-byte Spill
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	addps	%xmm3, %xmm4
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	addps	%xmm4, %xmm14
	movaps	%xmm11, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm0, %xmm3
	movaps	%xmm13, 352(%rsp)       # 16-byte Spill
	movaps	%xmm13, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm1, %xmm4
	addps	%xmm3, %xmm4
	movaps	%xmm6, 400(%rsp)        # 16-byte Spill
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	addps	%xmm4, %xmm3
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	addps	%xmm3, %xmm10
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	movaps	%xmm5, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm0, %xmm3
	movaps	%xmm8, 368(%rsp)        # 16-byte Spill
	movaps	%xmm8, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	%xmm14, %xmm4
	unpcklps	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1]
	addps	%xmm3, %xmm0
	movaps	%xmm15, 416(%rsp)       # 16-byte Spill
	movaps	%xmm15, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm0, %xmm1
	shufps	$224, %xmm12, %xmm12    # xmm12 = xmm12[0,0,2,3]
	addps	%xmm1, %xmm12
	xorps	%xmm1, %xmm1
	movaps	%xmm12, %xmm0
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movaps	%xmm14, %xmm15
	shufps	$229, %xmm15, %xmm15    # xmm15 = xmm15[1,1,2,3]
	movaps	%xmm10, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm12, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	movaps	%xmm4, 768(%rsp)        # 16-byte Spill
	movaps	%xmm0, 736(%rsp)        # 16-byte Spill
	unpcklpd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0]
	movss	8(%rbx), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movss	28(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	44(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 124(%rsp)        # 4-byte Spill
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movss	72(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	88(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	104(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	movss	76(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	92(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	108(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movss	96(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 120(%rsp)        # 4-byte Spill
	movss	112(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 128(%rsp)        # 4-byte Spill
	movapd	%xmm4, 688(%rsp)        # 16-byte Spill
	movupd	%xmm4, 104(%rsp)
	ucomiss	%xmm15, %xmm14
	movaps	%xmm14, 336(%rsp)       # 16-byte Spill
	movss	%xmm10, 60(%rsp)        # 4-byte Spill
	movss	%xmm14, 56(%rsp)        # 4-byte Spill
	jbe	.LBB14_4
# BB#3:
	movss	%xmm15, 104(%rsp)
	movaps	%xmm15, %xmm14
.LBB14_4:                               # %_Z8btSetMinIfEvRT_RKS0_.exit.i
	movss	%xmm14, 140(%rsp)       # 4-byte Spill
	movss	60(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm0
	jbe	.LBB14_6
# BB#5:
	movss	%xmm6, 108(%rsp)
	movaps	%xmm6, %xmm0
.LBB14_6:                               # %_Z8btSetMinIfEvRT_RKS0_.exit7.i
	movaps	%xmm6, 704(%rsp)        # 16-byte Spill
	movss	%xmm0, 136(%rsp)        # 4-byte Spill
	ucomiss	%xmm7, %xmm12
	movaps	%xmm12, %xmm0
	jbe	.LBB14_8
# BB#7:
	movss	%xmm7, 112(%rsp)
	movaps	%xmm7, %xmm0
.LBB14_8:                               # %_ZN9btVector36setMinERKS_.exit
	movss	%xmm0, 132(%rsp)        # 4-byte Spill
	movaps	%xmm7, 720(%rsp)        # 16-byte Spill
	movaps	%xmm10, 784(%rsp)       # 16-byte Spill
	movaps	%xmm9, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm0, 256(%rsp)        # 16-byte Spill
	movaps	752(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm4
	movss	4(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm4
	movaps	%xmm4, 640(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm6
	mulss	%xmm1, %xmm6
	movaps	%xmm6, 320(%rsp)        # 16-byte Spill
	movaps	%xmm0, %xmm4
	mulss	%xmm13, %xmm4
	movaps	%xmm4, 656(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm6
	mulss	%xmm5, %xmm6
	movaps	%xmm6, 304(%rsp)        # 16-byte Spill
	movaps	%xmm13, %xmm14
	movaps	%xmm0, %xmm4
	movss	124(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	movaps	%xmm4, 672(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm6
	mulss	%xmm8, %xmm6
	movaps	%xmm6, 288(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm6
	mulss	%xmm1, %xmm6
	movaps	%xmm6, 272(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm6
	mulss	%xmm5, %xmm6
	movaps	%xmm6, 208(%rsp)        # 16-byte Spill
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm4, %xmm8
	movss	%xmm8, 40(%rsp)         # 4-byte Spill
	mulss	%xmm4, %xmm1
	movss	%xmm1, 44(%rsp)         # 4-byte Spill
	mulss	%xmm4, %xmm5
	movss	%xmm5, 36(%rsp)         # 4-byte Spill
	movaps	%xmm9, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm1, 192(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	movss	(%rsp), %xmm5           # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm9
	movaps	%xmm9, 224(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm1, 160(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	mulss	%xmm5, %xmm11
	movaps	%xmm11, 240(%rsp)       # 16-byte Spill
	movaps	%xmm10, %xmm11
	movaps	%xmm0, %xmm10
	mulss	%xmm4, %xmm2
	movss	%xmm2, 52(%rsp)         # 4-byte Spill
	mulss	%xmm4, %xmm3
	movss	%xmm3, 48(%rsp)         # 4-byte Spill
	mulss	%xmm5, %xmm4
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	movaps	352(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm3, %xmm0
	mulss	%xmm7, %xmm0
	movaps	%xmm3, %xmm5
	mulss	%xmm14, %xmm5
	movaps	%xmm3, %xmm4
	mulss	%xmm11, %xmm4
	movaps	368(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm2, %xmm7
	movss	%xmm7, 4(%rsp)          # 4-byte Spill
	mulss	%xmm2, %xmm14
	movss	%xmm14, (%rsp)          # 4-byte Spill
	mulss	%xmm2, %xmm11
	movaps	%xmm10, %xmm9
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm9
	movaps	%xmm2, %xmm6
	movaps	%xmm10, %xmm14
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm14
	movss	120(%rsp), %xmm13       # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm10
	movaps	%xmm3, %xmm2
	mulss	%xmm1, %xmm2
	movaps	%xmm3, %xmm8
	mulss	%xmm7, %xmm8
	mulss	%xmm13, %xmm3
	movaps	%xmm3, 352(%rsp)        # 16-byte Spill
	mulss	%xmm6, %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	mulss	%xmm6, %xmm7
	movss	%xmm7, 12(%rsp)         # 4-byte Spill
	mulss	%xmm13, %xmm6
	movaps	%xmm6, 368(%rsp)        # 16-byte Spill
	movaps	688(%rsp), %xmm1        # 16-byte Reload
	movups	%xmm1, 88(%rsp)
	ucomiss	336(%rsp), %xmm15       # 16-byte Folded Reload
	jbe	.LBB14_10
# BB#9:
	movss	%xmm15, 88(%rsp)
	movss	%xmm15, 56(%rsp)        # 4-byte Spill
.LBB14_10:                              # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	movaps	256(%rsp), %xmm1        # 16-byte Reload
	addss	640(%rsp), %xmm1        # 16-byte Folded Reload
	movaps	%xmm1, 256(%rsp)        # 16-byte Spill
	movaps	320(%rsp), %xmm3        # 16-byte Reload
	addss	656(%rsp), %xmm3        # 16-byte Folded Reload
	movaps	%xmm3, 320(%rsp)        # 16-byte Spill
	movaps	304(%rsp), %xmm3        # 16-byte Reload
	addss	672(%rsp), %xmm3        # 16-byte Folded Reload
	movaps	%xmm3, 304(%rsp)        # 16-byte Spill
	movaps	288(%rsp), %xmm3        # 16-byte Reload
	addss	%xmm0, %xmm3
	movaps	%xmm3, 288(%rsp)        # 16-byte Spill
	movaps	272(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm5, %xmm0
	movaps	%xmm0, 272(%rsp)        # 16-byte Spill
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm4, %xmm0
	movaps	%xmm0, 208(%rsp)        # 16-byte Spill
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	4(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	movss	44(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	(%rsp), %xmm0           # 4-byte Folded Reload
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movss	36(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	%xmm11, %xmm0
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
	movaps	384(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm3, %xmm11
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm11
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm9, %xmm0
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	movaps	%xmm3, %xmm9
	movss	20(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm9
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm14, %xmm0
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movaps	%xmm3, %xmm13
	movss	24(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm13
	movaps	224(%rsp), %xmm1        # 16-byte Reload
	addss	%xmm10, %xmm1
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	movaps	400(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm15
	mulss	%xmm6, %xmm15
	movaps	160(%rsp), %xmm1        # 16-byte Reload
	addss	%xmm2, %xmm1
	movaps	%xmm1, 160(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm5
	mulss	%xmm4, %xmm5
	movaps	144(%rsp), %xmm1        # 16-byte Reload
	addss	%xmm8, %xmm1
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm1
	mulss	%xmm0, %xmm1
	movaps	416(%rsp), %xmm14       # 16-byte Reload
	mulss	%xmm14, %xmm6
	movss	%xmm6, 16(%rsp)         # 4-byte Spill
	mulss	%xmm14, %xmm4
	movss	%xmm4, 20(%rsp)         # 4-byte Spill
	mulss	%xmm14, %xmm0
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	addss	352(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	movaps	%xmm3, %xmm2
	movss	28(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	movss	52(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	8(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, 52(%rsp)         # 4-byte Spill
	movaps	%xmm3, %xmm0
	movss	32(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	128(%rsp), %xmm8        # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	movaps	%xmm3, 384(%rsp)        # 16-byte Spill
	movss	48(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	addss	12(%rsp), %xmm3         # 4-byte Folded Reload
	movss	%xmm3, 48(%rsp)         # 4-byte Spill
	movaps	%xmm10, %xmm3
	mulss	%xmm7, %xmm3
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	addss	368(%rsp), %xmm4        # 16-byte Folded Reload
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	movaps	%xmm10, %xmm4
	mulss	%xmm6, %xmm4
	mulss	%xmm8, %xmm10
	movaps	%xmm10, 400(%rsp)       # 16-byte Spill
	mulss	%xmm14, %xmm7
	movss	%xmm7, 28(%rsp)         # 4-byte Spill
	mulss	%xmm14, %xmm6
	movss	%xmm6, 32(%rsp)         # 4-byte Spill
	mulss	%xmm8, %xmm14
	movaps	%xmm14, 416(%rsp)       # 16-byte Spill
	movaps	784(%rsp), %xmm10       # 16-byte Reload
	shufps	$17, 336(%rsp), %xmm10  # 16-byte Folded Reload
                                        # xmm10 = xmm10[1,0],mem[1,0]
	movss	60(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	704(%rsp), %xmm7        # 16-byte Reload
	ucomiss	%xmm6, %xmm7
	jbe	.LBB14_12
# BB#11:
	movss	%xmm7, 92(%rsp)
	movaps	%xmm7, %xmm6
.LBB14_12:                              # %_Z8btSetMaxIfEvRT_RKS0_.exit7.i
	movaps	256(%rsp), %xmm8        # 16-byte Reload
	addss	%xmm11, %xmm8
	movaps	320(%rsp), %xmm11       # 16-byte Reload
	addss	%xmm9, %xmm11
	movaps	304(%rsp), %xmm7        # 16-byte Reload
	addss	%xmm13, %xmm7
	movaps	%xmm7, %xmm13
	movaps	288(%rsp), %xmm14       # 16-byte Reload
	addss	%xmm15, %xmm14
	movaps	272(%rsp), %xmm15       # 16-byte Reload
	addss	%xmm5, %xmm15
	movaps	208(%rsp), %xmm5        # 16-byte Reload
	addss	%xmm1, %xmm5
	movaps	%xmm5, 208(%rsp)        # 16-byte Spill
	movss	40(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	addss	16(%rsp), %xmm9         # 4-byte Folded Reload
	movss	44(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	addss	20(%rsp), %xmm5         # 4-byte Folded Reload
	movss	36(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	addss	24(%rsp), %xmm7         # 4-byte Folded Reload
	movaps	192(%rsp), %xmm1        # 16-byte Reload
	addss	%xmm2, %xmm1
	movaps	%xmm1, 192(%rsp)        # 16-byte Spill
	movaps	176(%rsp), %xmm1        # 16-byte Reload
	addss	%xmm0, %xmm1
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	addss	384(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm3, %xmm0
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm4, %xmm0
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	addss	400(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	movss	52(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	addss	28(%rsp), %xmm3         # 4-byte Folded Reload
	movss	48(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	addss	32(%rsp), %xmm4         # 4-byte Folded Reload
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	addss	416(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	shufps	$226, 336(%rsp), %xmm10 # 16-byte Folded Reload
                                        # xmm10 = xmm10[2,0],mem[2,3]
	movaps	720(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm1, %xmm0
	unpcklps	.LCPI14_2, %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	ucomiss	%xmm12, %xmm1
	jbe	.LBB14_14
# BB#13:
	movss	%xmm1, 96(%rsp)
	movaps	%xmm1, %xmm12
.LBB14_14:                              # %_ZN9btVector36setMaxERKS_.exit
	movss	264(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	140(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 104(%rsp)
	movss	136(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 108(%rsp)
	movss	132(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 112(%rsp)
	movss	56(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	movss	%xmm2, 88(%rsp)
	addss	%xmm1, %xmm6
	movss	%xmm6, 92(%rsp)
	addss	%xmm1, %xmm12
	movss	%xmm12, 96(%rsp)
	movq	$_ZTVZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback+16, 432(%rsp)
	movss	%xmm8, 440(%rsp)
	movss	%xmm11, 444(%rsp)
	movss	%xmm13, 448(%rsp)
	movl	$0, 452(%rsp)
	movss	%xmm14, 456(%rsp)
	movss	%xmm15, 460(%rsp)
	movaps	208(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 464(%rsp)
	movl	$0, 468(%rsp)
	movss	%xmm9, 472(%rsp)
	movss	%xmm5, 476(%rsp)
	movss	%xmm7, 480(%rsp)
	movl	$0, 484(%rsp)
	movaps	768(%rsp), %xmm2        # 16-byte Reload
	movlps	%xmm2, 488(%rsp)
	movaps	736(%rsp), %xmm2        # 16-byte Reload
	movlps	%xmm2, 496(%rsp)
	movaps	192(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 504(%rsp)
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 508(%rsp)
	movaps	224(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 512(%rsp)
	movl	$0, 516(%rsp)
	movaps	160(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 520(%rsp)
	movaps	144(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 524(%rsp)
	movaps	240(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 528(%rsp)
	movl	$0, 532(%rsp)
	movss	%xmm3, 536(%rsp)
	movss	%xmm4, 540(%rsp)
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movss	%xmm2, 544(%rsp)
	movl	$0, 548(%rsp)
	movlps	%xmm10, 552(%rsp)
	movlps	%xmm0, 560(%rsp)
	movss	%xmm1, 632(%rsp)
	movl	260(%rbx), %eax
	movl	%eax, 636(%rsp)
	testq	%rdi, %rdi
	movb	$1, %bpl
	je	.LBB14_15
# BB#16:
	movq	(%rdi), %rax
.Ltmp70:
	leaq	432(%rsp), %rsi
	leaq	104(%rsp), %rdx
	leaq	88(%rsp), %rcx
	callq	*96(%rax)
.Ltmp71:
# BB#17:
	movss	636(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movd	260(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movss	%xmm1, 64(%rsp)         # 4-byte Spill
	jbe	.LBB14_19
# BB#18:
	movss	%xmm1, 260(%rbx)
	xorl	%ebp, %ebp
	jmp	.LBB14_19
.LBB14_15:
	movd	%eax, %xmm0
	movd	%xmm0, 64(%rsp)         # 4-byte Folded Spill
.LBB14_19:                              # %.thread
	leaq	432(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
	testb	%bpl, %bpl
	movss	.LCPI14_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	64(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	je	.LBB14_21
.LBB14_20:
	movaps	%xmm1, %xmm0
.LBB14_21:
	addq	$808, %rsp              # imm = 0x328
	popq	%rbx
	popq	%rbp
	retq
.LBB14_22:
.Ltmp72:
	movq	%rax, %rbx
.Ltmp73:
	leaq	432(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp74:
# BB#23:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB14_24:
.Ltmp75:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end14-_ZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp70-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin7   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp73-.Ltmp71         #   Call between .Ltmp71 and .Ltmp73
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin7   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp74-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Lfunc_end14-.Ltmp74    #   Call between .Ltmp74 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN15btTriangleShapeD0Ev,"axG",@progbits,_ZN15btTriangleShapeD0Ev,comdat
	.weak	_ZN15btTriangleShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN15btTriangleShapeD0Ev,@function
_ZN15btTriangleShapeD0Ev:               # @_ZN15btTriangleShapeD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 32
.Lcfi85:
	.cfi_offset %rbx, -24
.Lcfi86:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp76:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp77:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB15_2:
.Ltmp78:
	movq	%rax, %r14
.Ltmp79:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp80:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_4:
.Ltmp81:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN15btTriangleShapeD0Ev, .Lfunc_end15-_ZN15btTriangleShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp76-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin8   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Ltmp79-.Ltmp77         #   Call between .Ltmp77 and .Ltmp79
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin8   # >> Call Site 3 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin8   #     jumps to .Ltmp81
	.byte	1                       #   On action: 1
	.long	.Ltmp80-.Lfunc_begin8   # >> Call Site 4 <<
	.long	.Lfunc_end15-.Ltmp80    #   Call between .Ltmp80 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end16:
	.size	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end16-_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end17:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end17-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,"axG",@progbits,_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,comdat
	.weak	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3: # @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end18:
	.size	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end18-_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape7getNameEv,"axG",@progbits,_ZNK15btTriangleShape7getNameEv,comdat
	.weak	_ZNK15btTriangleShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getNameEv,@function
_ZNK15btTriangleShape7getNameEv:        # @_ZNK15btTriangleShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end19:
	.size	_ZNK15btTriangleShape7getNameEv, .Lfunc_end19-_ZNK15btTriangleShape7getNameEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end20:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end20-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,"axG",@progbits,_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,comdat
	.weak	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	80(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	84(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	88(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	mulss	96(%rdi), %xmm2
	mulss	100(%rdi), %xmm1
	addss	%xmm2, %xmm1
	mulss	104(%rdi), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm5, -16(%rsp)
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, -12(%rsp)
	movss	%xmm0, -8(%rsp)
	movl	$0, -4(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm5, %xmm1
	seta	%al
	ucomiss	-16(%rsp,%rax,4), %xmm0
	movl	$2, %ecx
	cmovbeq	%rax, %rcx
	shlq	$4, %rcx
	movsd	64(%rdi,%rcx), %xmm0    # xmm0 = mem[0],zero
	movsd	72(%rdi,%rcx), %xmm1    # xmm1 = mem[0],zero
	retq
.Lfunc_end21:
	.size	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end21-_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,"axG",@progbits,_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,comdat
	.weak	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	jle	.LBB22_3
# BB#1:                                 # %.lr.ph
	movl	%ecx, %eax
	addq	$8, %rsi
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB22_2:                               # =>This Inner Loop Header: Depth=1
	movss	-8(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	80(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	84(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	88(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	mulss	96(%rdi), %xmm2
	mulss	100(%rdi), %xmm1
	addss	%xmm2, %xmm1
	mulss	104(%rdi), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm5, -16(%rsp)
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, -12(%rsp)
	movss	%xmm0, -8(%rsp)
	movl	$0, -4(%rsp)
	xorl	%ecx, %ecx
	ucomiss	%xmm5, %xmm1
	seta	%cl
	ucomiss	-16(%rsp,%rcx,4), %xmm0
	cmovaq	%r8, %rcx
	shlq	$4, %rcx
	movups	64(%rdi,%rcx), %xmm0
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	addq	$16, %rsi
	decq	%rax
	jne	.LBB22_2
.LBB22_3:                               # %._crit_edge
	retq
.Lfunc_end22:
	.size	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end22-_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	movl	$2, %eax
	retq
.Lfunc_end23:
	.size	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end23-_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI24_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI24_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 32
.Lcfi90:
	.cfi_offset %rbx, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movss	64(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	80(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movsd	84(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	68(%rdi), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm1
	movss	100(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	96(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	104(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm6
	movaps	%xmm1, %xmm3
	mulps	%xmm6, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm0
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm1, %xmm6
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	subss	%xmm6, %xmm0
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, (%rbx)
	movlps	%xmm2, 8(%rbx)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB24_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB24_2:                               # %.split
	movss	.LCPI24_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	%xmm2, 4(%rbx)
	mulss	8(%rbx), %xmm0
	movss	%xmm0, 8(%rbx)
	testl	%ebp, %ebp
	je	.LBB24_4
# BB#3:
	movaps	.LCPI24_1(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm3, %xmm1
	movss	%xmm1, (%rbx)
	xorps	%xmm3, %xmm2
	movss	%xmm2, 4(%rbx)
	xorps	%xmm3, %xmm0
	movss	%xmm0, 8(%rbx)
.LBB24_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end24:
	.size	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end24-_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape14getNumVerticesEv,"axG",@progbits,_ZNK15btTriangleShape14getNumVerticesEv,comdat
	.weak	_ZNK15btTriangleShape14getNumVerticesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape14getNumVerticesEv,@function
_ZNK15btTriangleShape14getNumVerticesEv: # @_ZNK15btTriangleShape14getNumVerticesEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end25:
	.size	_ZNK15btTriangleShape14getNumVerticesEv, .Lfunc_end25-_ZNK15btTriangleShape14getNumVerticesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape11getNumEdgesEv,"axG",@progbits,_ZNK15btTriangleShape11getNumEdgesEv,comdat
	.weak	_ZNK15btTriangleShape11getNumEdgesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape11getNumEdgesEv,@function
_ZNK15btTriangleShape11getNumEdgesEv:   # @_ZNK15btTriangleShape11getNumEdgesEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end26:
	.size	_ZNK15btTriangleShape11getNumEdgesEv, .Lfunc_end26-_ZNK15btTriangleShape11getNumEdgesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,"axG",@progbits,_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,comdat
	.weak	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,@function
_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_: # @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 32
.Lcfi95:
	.cfi_offset %rbx, -32
.Lcfi96:
	.cfi_offset %r14, -24
.Lcfi97:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*168(%rax)
	movq	(%rbx), %rax
	movq	168(%rax), %rax
	leal	1(%r15), %ecx
	movslq	%ecx, %rcx
	imulq	$1431655766, %rcx, %rcx # imm = 0x55555556
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,2), %ecx
	negl	%ecx
	leal	1(%r15,%rcx), %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.Lfunc_end27:
	.size	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_, .Lfunc_end27-_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape9getVertexEiR9btVector3,"axG",@progbits,_ZNK15btTriangleShape9getVertexEiR9btVector3,comdat
	.weak	_ZNK15btTriangleShape9getVertexEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape9getVertexEiR9btVector3,@function
_ZNK15btTriangleShape9getVertexEiR9btVector3: # @_ZNK15btTriangleShape9getVertexEiR9btVector3
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	shlq	$4, %rax
	movups	64(%rdi,%rax), %xmm0
	movups	%xmm0, (%rdx)
	retq
.Lfunc_end28:
	.size	_ZNK15btTriangleShape9getVertexEiR9btVector3, .Lfunc_end28-_ZNK15btTriangleShape9getVertexEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape12getNumPlanesEv,"axG",@progbits,_ZNK15btTriangleShape12getNumPlanesEv,comdat
	.weak	_ZNK15btTriangleShape12getNumPlanesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape12getNumPlanesEv,@function
_ZNK15btTriangleShape12getNumPlanesEv:  # @_ZNK15btTriangleShape12getNumPlanesEv
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	retq
.Lfunc_end29:
	.size	_ZNK15btTriangleShape12getNumPlanesEv, .Lfunc_end29-_ZNK15btTriangleShape12getNumPlanesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape8getPlaneER9btVector3S1_i,"axG",@progbits,_ZNK15btTriangleShape8getPlaneER9btVector3S1_i,comdat
	.weak	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i,@function
_ZNK15btTriangleShape8getPlaneER9btVector3S1_i: # @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movq	%rsi, %rdx
	movq	(%rdi), %rsi
	movq	200(%rsi), %rax
	movl	%ecx, %esi
	movq	%r8, %rcx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end30:
	.size	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i, .Lfunc_end30-_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI31_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI31_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK15btTriangleShape8isInsideERK9btVector3f,"axG",@progbits,_ZNK15btTriangleShape8isInsideERK9btVector3f,comdat
	.weak	_ZNK15btTriangleShape8isInsideERK9btVector3f
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape8isInsideERK9btVector3f,@function
_ZNK15btTriangleShape8isInsideERK9btVector3f: # @_ZNK15btTriangleShape8isInsideERK9btVector3f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi103:
	.cfi_def_cfa_offset 192
.Lcfi104:
	.cfi_offset %rbx, -48
.Lcfi105:
	.cfi_offset %r12, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movaps	%xmm0, %xmm8
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	84(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movss	88(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm6
	movss	96(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm5
	movss	100(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm7
	movss	104(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movaps	%xmm6, %xmm2
	mulss	%xmm5, %xmm6
	mulss	%xmm3, %xmm5
	mulss	%xmm1, %xmm3
	mulss	%xmm7, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm0, %xmm7
	subss	%xmm5, %xmm7
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm7, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB31_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	movss	%xmm7, 16(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
.LBB31_2:                               # %.split
	movss	.LCPI31_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	divss	%xmm0, %xmm5
	mulss	%xmm5, %xmm3
	mulss	%xmm5, %xmm6
	mulss	%xmm7, %xmm5
	movss	64(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	movss	4(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	%xmm4, 48(%rsp)         # 16-byte Spill
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm6, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm3, %xmm1
	addps	%xmm0, %xmm1
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	72(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	%xmm1, %xmm0
	movaps	.LCPI31_1(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm1
	xorl	%eax, %eax
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	ucomiss	%xmm1, %xmm0
	jb	.LBB31_11
# BB#3:                                 # %.split
	ucomiss	%xmm0, %xmm8
	jb	.LBB31_11
# BB#4:                                 # %.preheader
	xorl	%ebp, %ebp
	movq	%rsp, %r15
	leaq	80(%rsp), %r12
	.p2align	4, 0x90
.LBB31_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	*160(%rax)
	movss	80(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	84(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	(%rsp), %xmm5
	subss	4(%rsp), %xmm0
	movss	88(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	8(%rsp), %xmm6
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm7
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm1
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm7
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm3, %xmm6
	movaps	%xmm4, %xmm1
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm5
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB31_7
# BB#6:                                 # %call.sqrt86
                                        #   in Loop: Header=BB31_5 Depth=1
	movaps	%xmm5, 128(%rsp)        # 16-byte Spill
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	movaps	112(%rsp), %xmm6        # 16-byte Reload
	movaps	128(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB31_7:                               # %.split85
                                        #   in Loop: Header=BB31_5 Depth=1
	movss	.LCPI31_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm7
	mulss	%xmm0, %xmm6
	mulss	%xmm0, %xmm5
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm2, %xmm7
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm3, %xmm6
	addps	%xmm7, %xmm6
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm5, %xmm1
	addps	%xmm6, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	ucomiss	%xmm1, %xmm0
	ja	.LBB31_10
# BB#8:                                 #   in Loop: Header=BB31_5 Depth=1
	incl	%ebp
	cmpl	$2, %ebp
	jle	.LBB31_5
# BB#9:
	movb	$1, %al
	jmp	.LBB31_11
.LBB31_10:
	xorl	%eax, %eax
.LBB31_11:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	_ZNK15btTriangleShape8isInsideERK9btVector3f, .Lfunc_end31-_ZNK15btTriangleShape8isInsideERK9btVector3f
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI32_0:
	.long	1065353216              # float 1
	.section	.text._ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,"axG",@progbits,_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,comdat
	.weak	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,@function
_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_: # @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -32
.Lcfi113:
	.cfi_offset %r14, -24
.Lcfi114:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movss	64(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movsd	84(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	68(%rbx), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm1
	movss	100(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	96(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	104(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm6
	movaps	%xmm1, %xmm3
	mulps	%xmm6, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm0
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm1, %xmm6
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	subss	%xmm6, %xmm0
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, (%r15)
	movlps	%xmm2, 8(%r15)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB32_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB32_2:                               # %.split
	movss	.LCPI32_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%r15)
	mulss	8(%r15), %xmm0
	movss	%xmm0, 8(%r15)
	movups	64(%rbx), %xmm0
	movups	%xmm0, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end32:
	.size	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_, .Lfunc_end32-_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev,@function
_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev: # @_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi117:
	.cfi_def_cfa_offset 32
.Lcfi118:
	.cfi_offset %rbx, -24
.Lcfi119:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp82:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp83:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB33_2:
.Ltmp84:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end33:
	.size	_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev, .Lfunc_end33-_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp82-.Lfunc_begin9   # >> Call Site 1 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin9   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin9   # >> Call Site 2 <<
	.long	.Lfunc_end33-.Ltmp83    #   Call between .Ltmp83 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii,@function
_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii: # @_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 40
	subq	$824, %rsp              # imm = 0x338
.Lcfi124:
	.cfi_def_cfa_offset 864
.Lcfi125:
	.cfi_offset %rbx, -40
.Lcfi126:
	.cfi_offset %r14, -32
.Lcfi127:
	.cfi_offset %r15, -24
.Lcfi128:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	$1065353216, 32(%rsp)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 36(%rsp)
	movl	$1065353216, 52(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 56(%rsp)
	movl	$1065353216, 72(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 76(%rsp)
	movl	$0, 92(%rsp)
	movq	$_ZTVN12btConvexCast10CastResultE+16, 272(%rsp)
	movq	$0, 448(%rsp)
	movl	$0, 456(%rsp)
	movl	204(%r15), %eax
	movl	%eax, 440(%rsp)
	movl	200(%r15), %ebp
.Ltmp85:
	leaq	96(%rsp), %r14
	movq	%r14, %rdi
	callq	_ZN21btConvexInternalShapeC2Ev
.Ltmp86:
# BB#1:
	movq	$_ZTV13btSphereShape+16, 96(%rsp)
	movl	$8, 104(%rsp)
	movl	%ebp, 136(%rsp)
	movl	%ebp, 152(%rsp)
.Ltmp88:
	leaq	160(%rsp), %rdi
	callq	_ZN23btPolyhedralConvexShapeC2Ev
.Ltmp89:
# BB#2:
	movq	$_ZTV15btTriangleShape+16, 160(%rsp)
	movl	$1, 168(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 224(%rsp)
	movups	16(%rbx), %xmm0
	movups	%xmm0, 240(%rsp)
	movups	32(%rbx), %xmm0
	movups	%xmm0, 256(%rsp)
	movb	$0, 792(%rsp)
.Ltmp91:
	movq	%rsp, %rdi
	leaq	96(%rsp), %r14
	leaq	160(%rsp), %rdx
	leaq	464(%rsp), %rcx
	movq	%r14, %rsi
	callq	_ZN22btSubsimplexConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
.Ltmp92:
# BB#3:
	leaq	8(%r15), %rsi
	leaq	72(%r15), %rdx
.Ltmp94:
	movq	%rsp, %rdi
	leaq	32(%rsp), %rcx
	leaq	272(%rsp), %r9
	movq	%rcx, %r8
	callq	_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
.Ltmp95:
# BB#4:
	testb	%al, %al
	je	.LBB34_7
# BB#5:
	movss	204(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	440(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB34_7
# BB#6:
	movss	%xmm0, 204(%r15)
.LBB34_7:
.Ltmp99:
	movq	%rsp, %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp100:
# BB#8:
.Ltmp104:
	leaq	160(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp105:
# BB#9:
.Ltmp110:
	leaq	96(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp111:
# BB#10:
	addq	$824, %rsp              # imm = 0x338
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB34_18:
.Ltmp112:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB34_14:
.Ltmp106:
	jmp	.LBB34_20
.LBB34_13:
.Ltmp101:
	jmp	.LBB34_16
.LBB34_12:
.Ltmp96:
	movq	%rax, %rbx
.Ltmp97:
	movq	%rsp, %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp98:
	jmp	.LBB34_17
.LBB34_15:
.Ltmp93:
.LBB34_16:
	movq	%rax, %rbx
.LBB34_17:
.Ltmp102:
	leaq	160(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp103:
	jmp	.LBB34_21
.LBB34_19:
.Ltmp90:
.LBB34_20:
	movq	%rax, %rbx
.LBB34_21:
.Ltmp107:
	movq	%r14, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp108:
# BB#22:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB34_23:
.Ltmp109:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB34_11:
.Ltmp87:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end34:
	.size	_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii, .Lfunc_end34-_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp85-.Lfunc_begin10  # >> Call Site 1 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin10  #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin10  # >> Call Site 2 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin10  #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin10  # >> Call Site 3 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin10  #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin10  # >> Call Site 4 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin10  #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin10  # >> Call Site 5 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin10 #     jumps to .Ltmp101
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin10 #     jumps to .Ltmp106
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin10 #     jumps to .Ltmp112
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin10 # >> Call Site 8 <<
	.long	.Ltmp97-.Ltmp111        #   Call between .Ltmp111 and .Ltmp97
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin10  # >> Call Site 9 <<
	.long	.Ltmp108-.Ltmp97        #   Call between .Ltmp97 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin10 #     jumps to .Ltmp109
	.byte	1                       #   On action: 1
	.long	.Ltmp108-.Lfunc_begin10 # >> Call Site 10 <<
	.long	.Lfunc_end34-.Ltmp108   #   Call between .Ltmp108 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12btConvexCast10CastResultD2Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD2Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD2Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD2Ev,@function
_ZN12btConvexCast10CastResultD2Ev:      # @_ZN12btConvexCast10CastResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end35:
	.size	_ZN12btConvexCast10CastResultD2Ev, .Lfunc_end35-_ZN12btConvexCast10CastResultD2Ev
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult9DebugDrawEf,"axG",@progbits,_ZN12btConvexCast10CastResult9DebugDrawEf,comdat
	.weak	_ZN12btConvexCast10CastResult9DebugDrawEf
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult9DebugDrawEf,@function
_ZN12btConvexCast10CastResult9DebugDrawEf: # @_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end36:
	.size	_ZN12btConvexCast10CastResult9DebugDrawEf, .Lfunc_end36-_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,"axG",@progbits,_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,comdat
	.weak	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,@function
_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform: # @_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end37:
	.size	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform, .Lfunc_end37-_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResultD0Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD0Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD0Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD0Ev,@function
_ZN12btConvexCast10CastResultD0Ev:      # @_ZN12btConvexCast10CastResultD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end38:
	.size	_ZN12btConvexCast10CastResultD0Ev, .Lfunc_end38-_ZN12btConvexCast10CastResultD0Ev
	.cfi_endproc

	.type	_ZTV33btConvexConcaveCollisionAlgorithm,@object # @_ZTV33btConvexConcaveCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV33btConvexConcaveCollisionAlgorithm
	.p2align	3
_ZTV33btConvexConcaveCollisionAlgorithm:
	.quad	0
	.quad	_ZTI33btConvexConcaveCollisionAlgorithm
	.quad	_ZN33btConvexConcaveCollisionAlgorithmD2Ev
	.quad	_ZN33btConvexConcaveCollisionAlgorithmD0Ev
	.quad	_ZN33btConvexConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN33btConvexConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV33btConvexConcaveCollisionAlgorithm, 56

	.type	_ZTV24btConvexTriangleCallback,@object # @_ZTV24btConvexTriangleCallback
	.globl	_ZTV24btConvexTriangleCallback
	.p2align	3
_ZTV24btConvexTriangleCallback:
	.quad	0
	.quad	_ZTI24btConvexTriangleCallback
	.quad	_ZN24btConvexTriangleCallbackD2Ev
	.quad	_ZN24btConvexTriangleCallbackD0Ev
	.quad	_ZN24btConvexTriangleCallback15processTriangleEP9btVector3ii
	.size	_ZTV24btConvexTriangleCallback, 40

	.type	_ZTS33btConvexConcaveCollisionAlgorithm,@object # @_ZTS33btConvexConcaveCollisionAlgorithm
	.globl	_ZTS33btConvexConcaveCollisionAlgorithm
	.p2align	4
_ZTS33btConvexConcaveCollisionAlgorithm:
	.asciz	"33btConvexConcaveCollisionAlgorithm"
	.size	_ZTS33btConvexConcaveCollisionAlgorithm, 36

	.type	_ZTI33btConvexConcaveCollisionAlgorithm,@object # @_ZTI33btConvexConcaveCollisionAlgorithm
	.globl	_ZTI33btConvexConcaveCollisionAlgorithm
	.p2align	4
_ZTI33btConvexConcaveCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS33btConvexConcaveCollisionAlgorithm
	.quad	_ZTI30btActivatingCollisionAlgorithm
	.size	_ZTI33btConvexConcaveCollisionAlgorithm, 24

	.type	_ZTS24btConvexTriangleCallback,@object # @_ZTS24btConvexTriangleCallback
	.globl	_ZTS24btConvexTriangleCallback
	.p2align	4
_ZTS24btConvexTriangleCallback:
	.asciz	"24btConvexTriangleCallback"
	.size	_ZTS24btConvexTriangleCallback, 27

	.type	_ZTI24btConvexTriangleCallback,@object # @_ZTI24btConvexTriangleCallback
	.globl	_ZTI24btConvexTriangleCallback
	.p2align	4
_ZTI24btConvexTriangleCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS24btConvexTriangleCallback
	.quad	_ZTI18btTriangleCallback
	.size	_ZTI24btConvexTriangleCallback, 24

	.type	_ZTV15btTriangleShape,@object # @_ZTV15btTriangleShape
	.section	.rodata._ZTV15btTriangleShape,"aG",@progbits,_ZTV15btTriangleShape,comdat
	.weak	_ZTV15btTriangleShape
	.p2align	3
_ZTV15btTriangleShape:
	.quad	0
	.quad	_ZTI15btTriangleShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN15btTriangleShapeD0Ev
	.quad	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK15btTriangleShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK15btTriangleShape14getNumVerticesEv
	.quad	_ZNK15btTriangleShape11getNumEdgesEv
	.quad	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.quad	_ZNK15btTriangleShape9getVertexEiR9btVector3
	.quad	_ZNK15btTriangleShape12getNumPlanesEv
	.quad	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.quad	_ZNK15btTriangleShape8isInsideERK9btVector3f
	.quad	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.size	_ZTV15btTriangleShape, 224

	.type	_ZTS15btTriangleShape,@object # @_ZTS15btTriangleShape
	.section	.rodata._ZTS15btTriangleShape,"aG",@progbits,_ZTS15btTriangleShape,comdat
	.weak	_ZTS15btTriangleShape
	.p2align	4
_ZTS15btTriangleShape:
	.asciz	"15btTriangleShape"
	.size	_ZTS15btTriangleShape, 18

	.type	_ZTI15btTriangleShape,@object # @_ZTI15btTriangleShape
	.section	.rodata._ZTI15btTriangleShape,"aG",@progbits,_ZTI15btTriangleShape,comdat
	.weak	_ZTI15btTriangleShape
	.p2align	4
_ZTI15btTriangleShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btTriangleShape
	.quad	_ZTI23btPolyhedralConvexShape
	.size	_ZTI15btTriangleShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Triangle"
	.size	.L.str, 9

	.type	_ZTVZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback,@object # @_ZTVZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback
	.section	.rodata,"a",@progbits
	.p2align	3
_ZTVZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback:
	.quad	0
	.quad	_ZTIZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev
	.quad	_ZZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii
	.size	_ZTVZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback, 40

	.type	_ZTSZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback,@object # @_ZTSZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback
	.p2align	4
_ZTSZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback:
	.asciz	"ZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback"
	.size	_ZTSZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback, 158

	.type	_ZTIZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback,@object # @_ZTIZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback
	.p2align	4
_ZTIZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback
	.quad	_ZTI18btTriangleCallback
	.size	_ZTIZN33btConvexConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback, 24

	.type	_ZTVN12btConvexCast10CastResultE,@object # @_ZTVN12btConvexCast10CastResultE
	.section	.rodata._ZTVN12btConvexCast10CastResultE,"aG",@progbits,_ZTVN12btConvexCast10CastResultE,comdat
	.weak	_ZTVN12btConvexCast10CastResultE
	.p2align	3
_ZTVN12btConvexCast10CastResultE:
	.quad	0
	.quad	_ZTIN12btConvexCast10CastResultE
	.quad	_ZN12btConvexCast10CastResult9DebugDrawEf
	.quad	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.quad	_ZN12btConvexCast10CastResultD2Ev
	.quad	_ZN12btConvexCast10CastResultD0Ev
	.size	_ZTVN12btConvexCast10CastResultE, 48

	.type	_ZTSN12btConvexCast10CastResultE,@object # @_ZTSN12btConvexCast10CastResultE
	.section	.rodata._ZTSN12btConvexCast10CastResultE,"aG",@progbits,_ZTSN12btConvexCast10CastResultE,comdat
	.weak	_ZTSN12btConvexCast10CastResultE
	.p2align	4
_ZTSN12btConvexCast10CastResultE:
	.asciz	"N12btConvexCast10CastResultE"
	.size	_ZTSN12btConvexCast10CastResultE, 29

	.type	_ZTIN12btConvexCast10CastResultE,@object # @_ZTIN12btConvexCast10CastResultE
	.section	.rodata._ZTIN12btConvexCast10CastResultE,"aG",@progbits,_ZTIN12btConvexCast10CastResultE,comdat
	.weak	_ZTIN12btConvexCast10CastResultE
	.p2align	3
_ZTIN12btConvexCast10CastResultE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN12btConvexCast10CastResultE
	.size	_ZTIN12btConvexCast10CastResultE, 16


	.globl	_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.type	_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b,@function
_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b = _ZN33btConvexConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.globl	_ZN33btConvexConcaveCollisionAlgorithmD1Ev
	.type	_ZN33btConvexConcaveCollisionAlgorithmD1Ev,@function
_ZN33btConvexConcaveCollisionAlgorithmD1Ev = _ZN33btConvexConcaveCollisionAlgorithmD2Ev
	.globl	_ZN24btConvexTriangleCallbackC1EP12btDispatcherP17btCollisionObjectS3_b
	.type	_ZN24btConvexTriangleCallbackC1EP12btDispatcherP17btCollisionObjectS3_b,@function
_ZN24btConvexTriangleCallbackC1EP12btDispatcherP17btCollisionObjectS3_b = _ZN24btConvexTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b
	.globl	_ZN24btConvexTriangleCallbackD1Ev
	.type	_ZN24btConvexTriangleCallbackD1Ev,@function
_ZN24btConvexTriangleCallbackD1Ev = _ZN24btConvexTriangleCallbackD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
