	.text
	.file	"getvlc.bc"
	.globl	Get_macroblock_type
	.p2align	4, 0x90
	.type	Get_macroblock_type,@function
Get_macroblock_type:                    # @Get_macroblock_type
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	ld(%rip), %rax
	cmpl	$3, 3148(%rax)
	jne	.LBB0_6
# BB#1:
	movl	$3, %edi
	callq	Show_Bits
	testl	%eax, %eax
	je	.LBB0_2
# BB#5:
	movslq	%eax, %rbx
	movsbl	SNRMBtab+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	SNRMBtab(%rbx,%rbx), %ebx
	jmp	.LBB0_43
.LBB0_6:
	movl	picture_coding_type(%rip), %ecx
	decl	%ecx
	cmpl	$3, %ecx
	ja	.LBB0_41
# BB#7:
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_8:
	cmpl	$0, 3160(%rax)
	je	.LBB0_11
# BB#9:
	movl	$4, %edi
	callq	Show_Bits
	testl	%eax, %eax
	je	.LBB0_2
# BB#10:
	movslq	%eax, %rbx
	movsbl	spIMBtab+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	spIMBtab(%rbx,%rbx), %ebx
	jmp	.LBB0_43
.LBB0_41:
	movl	$.Lstr, %edi
	callq	puts
	jmp	.LBB0_42
.LBB0_16:
	cmpl	$0, 3160(%rax)
	je	.LBB0_21
# BB#17:
	movl	$7, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	cmpl	$1, %ebx
	jg	.LBB0_18
.LBB0_2:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB0_4
# BB#3:
	movl	$.Lstr.8, %edi
	callq	puts
.LBB0_4:
	movl	$1, Fault_Flag(%rip)
.LBB0_42:                               # %Get_SNR_macroblock_type.exit
	xorl	%ebx, %ebx
.LBB0_43:                               # %Get_SNR_macroblock_type.exit
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB0_25:
	cmpl	$0, 3160(%rax)
	je	.LBB0_33
# BB#26:
	movl	$9, %edi
	callq	Show_Bits
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$64, %eax
	jl	.LBB0_28
# BB#27:
	shrl	$5, %eax
	leaq	spBMBtab0-4(%rax,%rax), %rbx
	jmp	.LBB0_32
.LBB0_37:
	callq	Get_Bits1
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB0_43
# BB#38:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB0_40
# BB#39:
	movl	$.Lstr.8, %edi
	callq	puts
.LBB0_40:
	movl	$1, Fault_Flag(%rip)
	jmp	.LBB0_43
.LBB0_11:
	callq	Get_Bits1
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB0_43
# BB#12:
	callq	Get_Bits1
	movl	$17, %ebx
	testl	%eax, %eax
	jne	.LBB0_43
# BB#13:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB0_15
# BB#14:
	movl	$.Lstr.8, %edi
	callq	puts
.LBB0_15:
	movl	$1, Fault_Flag(%rip)
	jmp	.LBB0_43
.LBB0_21:
	movl	$6, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	cmpl	$8, %ebx
	jl	.LBB0_23
# BB#22:
	shrl	$3, %ebx
	movsbl	PMBtab0+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	PMBtab0(%rbx,%rbx), %ebx
	jmp	.LBB0_43
.LBB0_33:
	movl	$6, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	cmpl	$8, %ebx
	jl	.LBB0_35
# BB#34:
	shrl	$2, %ebx
	movsbl	BMBtab0+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	BMBtab0(%rbx,%rbx), %ebx
	jmp	.LBB0_43
.LBB0_18:
	cmpl	$16, %ebx
	jl	.LBB0_20
# BB#19:
	shrl	$3, %ebx
	movsbl	spPMBtab0+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	spPMBtab0(%rbx,%rbx), %ebx
	jmp	.LBB0_43
.LBB0_28:
	cmpl	$16, %eax
	jl	.LBB0_30
# BB#29:
	shrl	$2, %eax
	leaq	spBMBtab1-8(%rax,%rax), %rbx
	jmp	.LBB0_32
.LBB0_23:
	testl	%ebx, %ebx
	je	.LBB0_2
# BB#24:
	movslq	%ebx, %rbx
	movsbl	PMBtab1+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	PMBtab1(%rbx,%rbx), %ebx
	jmp	.LBB0_43
.LBB0_35:
	testl	%ebx, %ebx
	je	.LBB0_2
# BB#36:
	movslq	%ebx, %rbx
	movsbl	BMBtab1+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	BMBtab1(%rbx,%rbx), %ebx
	jmp	.LBB0_43
.LBB0_20:
	movslq	%ebx, %rbx
	movsbl	spPMBtab1+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	spPMBtab1(%rbx,%rbx), %ebx
	jmp	.LBB0_43
.LBB0_30:
	cmpl	$8, %eax
	jl	.LBB0_2
# BB#31:
	cltq
	leaq	spBMBtab2-16(%rax,%rax), %rbx
.LBB0_32:
	movsbl	1(%rbx), %edi
	callq	Flush_Buffer
	movsbl	(%rbx), %ebx
	jmp	.LBB0_43
.Lfunc_end0:
	.size	Get_macroblock_type, .Lfunc_end0-Get_macroblock_type
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_8
	.quad	.LBB0_16
	.quad	.LBB0_25
	.quad	.LBB0_37

	.text
	.globl	Get_motion_code
	.p2align	4, 0x90
	.type	Get_motion_code,@function
Get_motion_code:                        # @Get_motion_code
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	callq	Get_Bits1
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jne	.LBB1_11
# BB#1:
	movl	$9, %edi
	callq	Show_Bits
	movl	%eax, %r14d
	cmpl	$64, %r14d
	jl	.LBB1_3
# BB#2:
	shrl	$6, %r14d
	movsbl	MVtab0+1(%r14,%r14), %edi
	callq	Flush_Buffer
	callq	Get_Bits1
	movsbl	MVtab0(%r14,%r14), %ecx
	jmp	.LBB1_10
.LBB1_3:
	cmpl	$24, %r14d
	jl	.LBB1_5
# BB#4:
	shrl	$3, %r14d
	movsbl	MVtab1+1(%r14,%r14), %edi
	callq	Flush_Buffer
	callq	Get_Bits1
	movsbl	MVtab1(%r14,%r14), %ecx
	jmp	.LBB1_10
.LBB1_5:
	cmpl	$11, %r14d
	jg	.LBB1_9
# BB#6:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB1_8
# BB#7:
	movl	global_MBA(%rip), %esi
	movl	global_pic(%rip), %edx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_8:
	movl	$1, Fault_Flag(%rip)
	jmp	.LBB1_11
.LBB1_9:
	movslq	%r14d, %rbx
	movsbl	MVtab2-23(%rbx,%rbx), %edi
	callq	Flush_Buffer
	callq	Get_Bits1
	movsbl	MVtab2-24(%rbx,%rbx), %ecx
.LBB1_10:
	movl	%ecx, %ebx
	negl	%ebx
	testl	%eax, %eax
	cmovel	%ecx, %ebx
.LBB1_11:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	Get_motion_code, .Lfunc_end1-Get_motion_code
	.cfi_endproc

	.globl	Get_dmvector
	.p2align	4, 0x90
	.type	Get_dmvector,@function
Get_dmvector:                           # @Get_dmvector
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	movl	$1, %edi
	callq	Get_Bits
	testl	%eax, %eax
	je	.LBB2_1
# BB#2:
	movl	$1, %edi
	callq	Get_Bits
	cmpl	$1, %eax
	sbbl	%eax, %eax
	notl	%eax
	orl	$1, %eax
	popq	%rcx
	retq
.LBB2_1:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	Get_dmvector, .Lfunc_end2-Get_dmvector
	.cfi_endproc

	.globl	Get_coded_block_pattern
	.p2align	4, 0x90
	.type	Get_coded_block_pattern,@function
Get_coded_block_pattern:                # @Get_coded_block_pattern
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movl	$9, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	cmpl	$128, %ebx
	jl	.LBB3_2
# BB#1:
	shrl	$4, %ebx
	movsbl	CBPtab0+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	CBPtab0(%rbx,%rbx), %eax
	popq	%rbx
	retq
.LBB3_2:
	cmpl	$8, %ebx
	jl	.LBB3_4
# BB#3:
	andl	$-2, %ebx
	movsbl	CBPtab1+1(%rbx), %edi
	callq	Flush_Buffer
	movsbl	CBPtab1(%rbx), %eax
	popq	%rbx
	retq
.LBB3_4:
	testl	%ebx, %ebx
	jle	.LBB3_5
# BB#8:
	movslq	%ebx, %rbx
	movsbl	CBPtab2+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	CBPtab2(%rbx,%rbx), %eax
	popq	%rbx
	retq
.LBB3_5:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB3_7
# BB#6:
	movl	$.Lstr.9, %edi
	callq	puts
.LBB3_7:
	movl	$1, Fault_Flag(%rip)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	Get_coded_block_pattern, .Lfunc_end3-Get_coded_block_pattern
	.cfi_endproc

	.globl	Get_macroblock_address_increment
	.p2align	4, 0x90
	.type	Get_macroblock_address_increment,@function
Get_macroblock_address_increment:       # @Get_macroblock_address_increment
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	$11, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	cmpl	$23, %ebx
	jg	.LBB4_6
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$15, %ebx
	je	.LBB4_5
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpl	$8, %ebx
	jne	.LBB4_8
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	addl	$33, %ebp
.LBB4_5:                                #   in Loop: Header=BB4_2 Depth=1
	movl	$11, %edi
	callq	Flush_Buffer
	movl	$11, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	cmpl	$24, %ebx
	jl	.LBB4_2
.LBB4_6:                                # %._crit_edge
	cmpl	$1024, %ebx             # imm = 0x400
	jl	.LBB4_11
# BB#7:
	movl	$1, %edi
	callq	Flush_Buffer
	incl	%ebp
	jmp	.LBB4_15
.LBB4_11:
	cmpl	$128, %ebx
	jl	.LBB4_13
# BB#12:
	shrl	$6, %ebx
	movsbl	MBAtab1+1(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	MBAtab1(%rbx,%rbx), %eax
	jmp	.LBB4_14
.LBB4_8:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB4_10
# BB#9:
	movl	$.Lstr.10, %edi
	callq	puts
.LBB4_10:
	movl	$1, Fault_Flag(%rip)
	movl	$1, %ebp
	jmp	.LBB4_15
.LBB4_13:
	movslq	%ebx, %rbx
	movsbl	MBAtab2-47(%rbx,%rbx), %edi
	callq	Flush_Buffer
	movsbl	MBAtab2-48(%rbx,%rbx), %eax
.LBB4_14:
	addl	%eax, %ebp
.LBB4_15:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	Get_macroblock_address_increment, .Lfunc_end4-Get_macroblock_address_increment
	.cfi_endproc

	.globl	Get_Luma_DC_dct_diff
	.p2align	4, 0x90
	.type	Get_Luma_DC_dct_diff,@function
Get_Luma_DC_dct_diff:                   # @Get_Luma_DC_dct_diff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movl	$5, %edi
	callq	Show_Bits
	cmpl	$30, %eax
	jg	.LBB5_2
# BB#1:
	cltq
	movb	DClumtab0(%rax,%rax), %bl
	movsbl	DClumtab0+1(%rax,%rax), %edi
	jmp	.LBB5_3
.LBB5_2:
	movl	$9, %edi
	callq	Show_Bits
	addl	$-496, %eax             # imm = 0xFE10
	cltq
	movb	DClumtab1(%rax,%rax), %bl
	movsbl	DClumtab1+1(%rax,%rax), %edi
.LBB5_3:
	callq	Flush_Buffer
	testb	%bl, %bl
	je	.LBB5_4
# BB#5:
	movsbl	%bl, %ebp
	movl	%ebp, %edi
	callq	Get_Bits
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	decl	%ebp
	btl	%ebp, %eax
	jb	.LBB5_7
# BB#6:
	movl	$-1, %edx
	movl	%ebx, %ecx
	shll	%cl, %edx
	leal	1(%rdx,%rax), %eax
	jmp	.LBB5_7
.LBB5_4:
	xorl	%eax, %eax
.LBB5_7:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	Get_Luma_DC_dct_diff, .Lfunc_end5-Get_Luma_DC_dct_diff
	.cfi_endproc

	.globl	Get_Chroma_DC_dct_diff
	.p2align	4, 0x90
	.type	Get_Chroma_DC_dct_diff,@function
Get_Chroma_DC_dct_diff:                 # @Get_Chroma_DC_dct_diff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	$5, %edi
	callq	Show_Bits
	cmpl	$30, %eax
	jg	.LBB6_2
# BB#1:
	cltq
	movb	DCchromtab0(%rax,%rax), %bl
	movsbl	DCchromtab0+1(%rax,%rax), %edi
	jmp	.LBB6_3
.LBB6_2:
	movl	$10, %edi
	callq	Show_Bits
	addl	$-992, %eax             # imm = 0xFC20
	cltq
	movb	DCchromtab1(%rax,%rax), %bl
	movsbl	DCchromtab1+1(%rax,%rax), %edi
.LBB6_3:
	callq	Flush_Buffer
	testb	%bl, %bl
	je	.LBB6_4
# BB#5:
	movsbl	%bl, %ebp
	movl	%ebp, %edi
	callq	Get_Bits
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	decl	%ebp
	btl	%ebp, %eax
	jb	.LBB6_7
# BB#6:
	movl	$-1, %edx
	movl	%ebx, %ecx
	shll	%cl, %edx
	leal	1(%rdx,%rax), %eax
	jmp	.LBB6_7
.LBB6_4:
	xorl	%eax, %eax
.LBB6_7:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	Get_Chroma_DC_dct_diff, .Lfunc_end6-Get_Chroma_DC_dct_diff
	.cfi_endproc

	.type	DCTtabfirst,@object     # @DCTtabfirst
	.data
	.globl	DCTtabfirst
	.p2align	4
DCTtabfirst:
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	4                       # 0x4
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.size	DCTtabfirst, 36

	.type	DCTtabnext,@object      # @DCTtabnext
	.globl	DCTtabnext
	.p2align	4
DCTtabnext:
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	4                       # 0x4
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.size	DCTtabnext, 36

	.type	DCTtab0,@object         # @DCTtab0
	.globl	DCTtab0
	.p2align	4
DCTtab0:
	.byte	65                      # 0x41
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	65                      # 0x41
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	65                      # 0x41
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	65                      # 0x41
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	7                       # 0x7
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	7                       # 0x7
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	13                      # 0xd
	.byte	1                       # 0x1
	.byte	8                       # 0x8
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	1                       # 0x1
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	1                       # 0x1
	.byte	8                       # 0x8
	.byte	3                       # 0x3
	.byte	2                       # 0x2
	.byte	8                       # 0x8
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	8                       # 0x8
	.byte	0                       # 0x0
	.byte	5                       # 0x5
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	1                       # 0x1
	.byte	8                       # 0x8
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.size	DCTtab0, 180

	.type	DCTtab0a,@object        # @DCTtab0a
	.globl	DCTtab0a
	.p2align	4
DCTtab0a:
	.byte	65                      # 0x41
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	65                      # 0x41
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	65                      # 0x41
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	65                      # 0x41
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	7                       # 0x7
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	7                       # 0x7
	.byte	0                       # 0x0
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	0                       # 0x0
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	0                       # 0x0
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	0                       # 0x0
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	1                       # 0x1
	.byte	8                       # 0x8
	.byte	0                       # 0x0
	.byte	11                      # 0xb
	.byte	8                       # 0x8
	.byte	0                       # 0x0
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	13                      # 0xd
	.byte	1                       # 0x1
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	1                       # 0x1
	.byte	8                       # 0x8
	.byte	3                       # 0x3
	.byte	2                       # 0x2
	.byte	8                       # 0x8
	.byte	1                       # 0x1
	.byte	4                       # 0x4
	.byte	8                       # 0x8
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	64                      # 0x40
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	9                       # 0x9
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	7                       # 0x7
	.byte	1                       # 0x1
	.byte	3                       # 0x3
	.byte	7                       # 0x7
	.byte	10                      # 0xa
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	10                      # 0xa
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	0                       # 0x0
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	0                       # 0x0
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	0                       # 0x0
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	0                       # 0x0
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	0                       # 0x0
	.byte	12                      # 0xc
	.byte	8                       # 0x8
	.byte	0                       # 0x0
	.byte	13                      # 0xd
	.byte	8                       # 0x8
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	8                       # 0x8
	.byte	4                       # 0x4
	.byte	2                       # 0x2
	.byte	8                       # 0x8
	.byte	0                       # 0x0
	.byte	14                      # 0xe
	.byte	8                       # 0x8
	.byte	0                       # 0x0
	.byte	15                      # 0xf
	.byte	8                       # 0x8
	.size	DCTtab0a, 756

	.type	DCTtab1,@object         # @DCTtab1
	.globl	DCTtab1
	.p2align	4
DCTtab1:
	.byte	16                      # 0x10
	.byte	1                       # 0x1
	.byte	10                      # 0xa
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	10                      # 0xa
	.byte	0                       # 0x0
	.byte	7                       # 0x7
	.byte	10                      # 0xa
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	10                      # 0xa
	.byte	1                       # 0x1
	.byte	4                       # 0x4
	.byte	10                      # 0xa
	.byte	15                      # 0xf
	.byte	1                       # 0x1
	.byte	10                      # 0xa
	.byte	14                      # 0xe
	.byte	1                       # 0x1
	.byte	10                      # 0xa
	.byte	4                       # 0x4
	.byte	2                       # 0x2
	.byte	10                      # 0xa
	.size	DCTtab1, 24

	.type	DCTtab1a,@object        # @DCTtab1a
	.globl	DCTtab1a
	.p2align	4
DCTtab1a:
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	9                       # 0x9
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	9                       # 0x9
	.byte	14                      # 0xe
	.byte	1                       # 0x1
	.byte	9                       # 0x9
	.byte	14                      # 0xe
	.byte	1                       # 0x1
	.byte	9                       # 0x9
	.byte	2                       # 0x2
	.byte	4                       # 0x4
	.byte	10                      # 0xa
	.byte	16                      # 0x10
	.byte	1                       # 0x1
	.byte	10                      # 0xa
	.byte	15                      # 0xf
	.byte	1                       # 0x1
	.byte	9                       # 0x9
	.byte	15                      # 0xf
	.byte	1                       # 0x1
	.byte	9                       # 0x9
	.size	DCTtab1a, 24

	.type	DCTtab2,@object         # @DCTtab2
	.globl	DCTtab2
	.p2align	4
DCTtab2:
	.byte	0                       # 0x0
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	8                       # 0x8
	.byte	2                       # 0x2
	.byte	12                      # 0xc
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	12                      # 0xc
	.byte	0                       # 0x0
	.byte	10                      # 0xa
	.byte	12                      # 0xc
	.byte	2                       # 0x2
	.byte	4                       # 0x4
	.byte	12                      # 0xc
	.byte	7                       # 0x7
	.byte	2                       # 0x2
	.byte	12                      # 0xc
	.byte	21                      # 0x15
	.byte	1                       # 0x1
	.byte	12                      # 0xc
	.byte	20                      # 0x14
	.byte	1                       # 0x1
	.byte	12                      # 0xc
	.byte	0                       # 0x0
	.byte	9                       # 0x9
	.byte	12                      # 0xc
	.byte	19                      # 0x13
	.byte	1                       # 0x1
	.byte	12                      # 0xc
	.byte	18                      # 0x12
	.byte	1                       # 0x1
	.byte	12                      # 0xc
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	12                      # 0xc
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	12                      # 0xc
	.byte	0                       # 0x0
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	6                       # 0x6
	.byte	2                       # 0x2
	.byte	12                      # 0xc
	.byte	17                      # 0x11
	.byte	1                       # 0x1
	.byte	12                      # 0xc
	.size	DCTtab2, 48

	.type	DCTtab3,@object         # @DCTtab3
	.globl	DCTtab3
	.p2align	4
DCTtab3:
	.byte	10                      # 0xa
	.byte	2                       # 0x2
	.byte	13                      # 0xd
	.byte	9                       # 0x9
	.byte	2                       # 0x2
	.byte	13                      # 0xd
	.byte	5                       # 0x5
	.byte	3                       # 0x3
	.byte	13                      # 0xd
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	13                      # 0xd
	.byte	2                       # 0x2
	.byte	5                       # 0x5
	.byte	13                      # 0xd
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	13                      # 0xd
	.byte	1                       # 0x1
	.byte	6                       # 0x6
	.byte	13                      # 0xd
	.byte	0                       # 0x0
	.byte	15                      # 0xf
	.byte	13                      # 0xd
	.byte	0                       # 0x0
	.byte	14                      # 0xe
	.byte	13                      # 0xd
	.byte	0                       # 0x0
	.byte	13                      # 0xd
	.byte	13                      # 0xd
	.byte	0                       # 0x0
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	26                      # 0x1a
	.byte	1                       # 0x1
	.byte	13                      # 0xd
	.byte	25                      # 0x19
	.byte	1                       # 0x1
	.byte	13                      # 0xd
	.byte	24                      # 0x18
	.byte	1                       # 0x1
	.byte	13                      # 0xd
	.byte	23                      # 0x17
	.byte	1                       # 0x1
	.byte	13                      # 0xd
	.byte	22                      # 0x16
	.byte	1                       # 0x1
	.byte	13                      # 0xd
	.size	DCTtab3, 48

	.type	DCTtab4,@object         # @DCTtab4
	.globl	DCTtab4
	.p2align	4
DCTtab4:
	.byte	0                       # 0x0
	.byte	31                      # 0x1f
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	30                      # 0x1e
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	29                      # 0x1d
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	28                      # 0x1c
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	27                      # 0x1b
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	26                      # 0x1a
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	25                      # 0x19
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	24                      # 0x18
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	23                      # 0x17
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	22                      # 0x16
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	21                      # 0x15
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	20                      # 0x14
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	19                      # 0x13
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	18                      # 0x12
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	17                      # 0x11
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	16                      # 0x10
	.byte	14                      # 0xe
	.size	DCTtab4, 48

	.type	DCTtab5,@object         # @DCTtab5
	.globl	DCTtab5
	.p2align	4
DCTtab5:
	.byte	0                       # 0x0
	.byte	40                      # 0x28
	.byte	15                      # 0xf
	.byte	0                       # 0x0
	.byte	39                      # 0x27
	.byte	15                      # 0xf
	.byte	0                       # 0x0
	.byte	38                      # 0x26
	.byte	15                      # 0xf
	.byte	0                       # 0x0
	.byte	37                      # 0x25
	.byte	15                      # 0xf
	.byte	0                       # 0x0
	.byte	36                      # 0x24
	.byte	15                      # 0xf
	.byte	0                       # 0x0
	.byte	35                      # 0x23
	.byte	15                      # 0xf
	.byte	0                       # 0x0
	.byte	34                      # 0x22
	.byte	15                      # 0xf
	.byte	0                       # 0x0
	.byte	33                      # 0x21
	.byte	15                      # 0xf
	.byte	0                       # 0x0
	.byte	32                      # 0x20
	.byte	15                      # 0xf
	.byte	1                       # 0x1
	.byte	14                      # 0xe
	.byte	15                      # 0xf
	.byte	1                       # 0x1
	.byte	13                      # 0xd
	.byte	15                      # 0xf
	.byte	1                       # 0x1
	.byte	12                      # 0xc
	.byte	15                      # 0xf
	.byte	1                       # 0x1
	.byte	11                      # 0xb
	.byte	15                      # 0xf
	.byte	1                       # 0x1
	.byte	10                      # 0xa
	.byte	15                      # 0xf
	.byte	1                       # 0x1
	.byte	9                       # 0x9
	.byte	15                      # 0xf
	.byte	1                       # 0x1
	.byte	8                       # 0x8
	.byte	15                      # 0xf
	.size	DCTtab5, 48

	.type	DCTtab6,@object         # @DCTtab6
	.globl	DCTtab6
	.p2align	4
DCTtab6:
	.byte	1                       # 0x1
	.byte	18                      # 0x12
	.byte	16                      # 0x10
	.byte	1                       # 0x1
	.byte	17                      # 0x11
	.byte	16                      # 0x10
	.byte	1                       # 0x1
	.byte	16                      # 0x10
	.byte	16                      # 0x10
	.byte	1                       # 0x1
	.byte	15                      # 0xf
	.byte	16                      # 0x10
	.byte	6                       # 0x6
	.byte	3                       # 0x3
	.byte	16                      # 0x10
	.byte	16                      # 0x10
	.byte	2                       # 0x2
	.byte	16                      # 0x10
	.byte	15                      # 0xf
	.byte	2                       # 0x2
	.byte	16                      # 0x10
	.byte	14                      # 0xe
	.byte	2                       # 0x2
	.byte	16                      # 0x10
	.byte	13                      # 0xd
	.byte	2                       # 0x2
	.byte	16                      # 0x10
	.byte	12                      # 0xc
	.byte	2                       # 0x2
	.byte	16                      # 0x10
	.byte	11                      # 0xb
	.byte	2                       # 0x2
	.byte	16                      # 0x10
	.byte	31                      # 0x1f
	.byte	1                       # 0x1
	.byte	16                      # 0x10
	.byte	30                      # 0x1e
	.byte	1                       # 0x1
	.byte	16                      # 0x10
	.byte	29                      # 0x1d
	.byte	1                       # 0x1
	.byte	16                      # 0x10
	.byte	28                      # 0x1c
	.byte	1                       # 0x1
	.byte	16                      # 0x10
	.byte	27                      # 0x1b
	.byte	1                       # 0x1
	.byte	16                      # 0x10
	.size	DCTtab6, 48

	.type	MVtab0,@object          # @MVtab0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
MVtab0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.size	MVtab0, 16

	.type	MVtab1,@object          # @MVtab1
	.p2align	4
MVtab1:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.size	MVtab1, 16

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"Invalid motion_vector code (MBA %d, pic %d)\n"
	.size	.L.str.1, 45

	.type	MVtab2,@object          # @MVtab2
	.section	.rodata,"a",@progbits
	.p2align	4
MVtab2:
	.byte	16                      # 0x10
	.byte	9                       # 0x9
	.byte	15                      # 0xf
	.byte	9                       # 0x9
	.byte	14                      # 0xe
	.byte	9                       # 0x9
	.byte	13                      # 0xd
	.byte	9                       # 0x9
	.byte	12                      # 0xc
	.byte	9                       # 0x9
	.byte	11                      # 0xb
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.size	MVtab2, 24

	.type	CBPtab0,@object         # @CBPtab0
	.p2align	4
CBPtab0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	62                      # 0x3e
	.byte	5                       # 0x5
	.byte	2                       # 0x2
	.byte	5                       # 0x5
	.byte	61                      # 0x3d
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	56                      # 0x38
	.byte	5                       # 0x5
	.byte	52                      # 0x34
	.byte	5                       # 0x5
	.byte	44                      # 0x2c
	.byte	5                       # 0x5
	.byte	28                      # 0x1c
	.byte	5                       # 0x5
	.byte	40                      # 0x28
	.byte	5                       # 0x5
	.byte	20                      # 0x14
	.byte	5                       # 0x5
	.byte	48                      # 0x30
	.byte	5                       # 0x5
	.byte	12                      # 0xc
	.byte	5                       # 0x5
	.byte	32                      # 0x20
	.byte	4                       # 0x4
	.byte	32                      # 0x20
	.byte	4                       # 0x4
	.byte	16                      # 0x10
	.byte	4                       # 0x4
	.byte	16                      # 0x10
	.byte	4                       # 0x4
	.byte	8                       # 0x8
	.byte	4                       # 0x4
	.byte	8                       # 0x8
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	60                      # 0x3c
	.byte	3                       # 0x3
	.byte	60                      # 0x3c
	.byte	3                       # 0x3
	.byte	60                      # 0x3c
	.byte	3                       # 0x3
	.byte	60                      # 0x3c
	.byte	3                       # 0x3
	.size	CBPtab0, 64

	.type	CBPtab1,@object         # @CBPtab1
	.p2align	4
CBPtab1:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	58                      # 0x3a
	.byte	8                       # 0x8
	.byte	54                      # 0x36
	.byte	8                       # 0x8
	.byte	46                      # 0x2e
	.byte	8                       # 0x8
	.byte	30                      # 0x1e
	.byte	8                       # 0x8
	.byte	57                      # 0x39
	.byte	8                       # 0x8
	.byte	53                      # 0x35
	.byte	8                       # 0x8
	.byte	45                      # 0x2d
	.byte	8                       # 0x8
	.byte	29                      # 0x1d
	.byte	8                       # 0x8
	.byte	38                      # 0x26
	.byte	8                       # 0x8
	.byte	26                      # 0x1a
	.byte	8                       # 0x8
	.byte	37                      # 0x25
	.byte	8                       # 0x8
	.byte	25                      # 0x19
	.byte	8                       # 0x8
	.byte	43                      # 0x2b
	.byte	8                       # 0x8
	.byte	23                      # 0x17
	.byte	8                       # 0x8
	.byte	51                      # 0x33
	.byte	8                       # 0x8
	.byte	15                      # 0xf
	.byte	8                       # 0x8
	.byte	42                      # 0x2a
	.byte	8                       # 0x8
	.byte	22                      # 0x16
	.byte	8                       # 0x8
	.byte	50                      # 0x32
	.byte	8                       # 0x8
	.byte	14                      # 0xe
	.byte	8                       # 0x8
	.byte	41                      # 0x29
	.byte	8                       # 0x8
	.byte	21                      # 0x15
	.byte	8                       # 0x8
	.byte	49                      # 0x31
	.byte	8                       # 0x8
	.byte	13                      # 0xd
	.byte	8                       # 0x8
	.byte	35                      # 0x23
	.byte	8                       # 0x8
	.byte	19                      # 0x13
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	34                      # 0x22
	.byte	7                       # 0x7
	.byte	34                      # 0x22
	.byte	7                       # 0x7
	.byte	18                      # 0x12
	.byte	7                       # 0x7
	.byte	18                      # 0x12
	.byte	7                       # 0x7
	.byte	10                      # 0xa
	.byte	7                       # 0x7
	.byte	10                      # 0xa
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	33                      # 0x21
	.byte	7                       # 0x7
	.byte	33                      # 0x21
	.byte	7                       # 0x7
	.byte	17                      # 0x11
	.byte	7                       # 0x7
	.byte	17                      # 0x11
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	5                       # 0x5
	.byte	7                       # 0x7
	.byte	5                       # 0x5
	.byte	7                       # 0x7
	.byte	63                      # 0x3f
	.byte	6                       # 0x6
	.byte	63                      # 0x3f
	.byte	6                       # 0x6
	.byte	63                      # 0x3f
	.byte	6                       # 0x6
	.byte	63                      # 0x3f
	.byte	6                       # 0x6
	.byte	3                       # 0x3
	.byte	6                       # 0x6
	.byte	3                       # 0x3
	.byte	6                       # 0x6
	.byte	3                       # 0x3
	.byte	6                       # 0x6
	.byte	3                       # 0x3
	.byte	6                       # 0x6
	.byte	36                      # 0x24
	.byte	6                       # 0x6
	.byte	36                      # 0x24
	.byte	6                       # 0x6
	.byte	36                      # 0x24
	.byte	6                       # 0x6
	.byte	36                      # 0x24
	.byte	6                       # 0x6
	.byte	24                      # 0x18
	.byte	6                       # 0x6
	.byte	24                      # 0x18
	.byte	6                       # 0x6
	.byte	24                      # 0x18
	.byte	6                       # 0x6
	.byte	24                      # 0x18
	.byte	6                       # 0x6
	.size	CBPtab1, 128

	.type	CBPtab2,@object         # @CBPtab2
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
CBPtab2:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	9                       # 0x9
	.byte	39                      # 0x27
	.byte	9                       # 0x9
	.byte	27                      # 0x1b
	.byte	9                       # 0x9
	.byte	59                      # 0x3b
	.byte	9                       # 0x9
	.byte	55                      # 0x37
	.byte	9                       # 0x9
	.byte	47                      # 0x2f
	.byte	9                       # 0x9
	.byte	31                      # 0x1f
	.byte	9                       # 0x9
	.size	CBPtab2, 16

	.type	MBAtab1,@object         # @MBAtab1
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
MBAtab1:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	7                       # 0x7
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.size	MBAtab1, 32

	.type	MBAtab2,@object         # @MBAtab2
	.section	.rodata,"a",@progbits
	.p2align	4
MBAtab2:
	.byte	33                      # 0x21
	.byte	11                      # 0xb
	.byte	32                      # 0x20
	.byte	11                      # 0xb
	.byte	31                      # 0x1f
	.byte	11                      # 0xb
	.byte	30                      # 0x1e
	.byte	11                      # 0xb
	.byte	29                      # 0x1d
	.byte	11                      # 0xb
	.byte	28                      # 0x1c
	.byte	11                      # 0xb
	.byte	27                      # 0x1b
	.byte	11                      # 0xb
	.byte	26                      # 0x1a
	.byte	11                      # 0xb
	.byte	25                      # 0x19
	.byte	11                      # 0xb
	.byte	24                      # 0x18
	.byte	11                      # 0xb
	.byte	23                      # 0x17
	.byte	11                      # 0xb
	.byte	22                      # 0x16
	.byte	11                      # 0xb
	.byte	21                      # 0x15
	.byte	10                      # 0xa
	.byte	21                      # 0x15
	.byte	10                      # 0xa
	.byte	20                      # 0x14
	.byte	10                      # 0xa
	.byte	20                      # 0x14
	.byte	10                      # 0xa
	.byte	19                      # 0x13
	.byte	10                      # 0xa
	.byte	19                      # 0x13
	.byte	10                      # 0xa
	.byte	18                      # 0x12
	.byte	10                      # 0xa
	.byte	18                      # 0x12
	.byte	10                      # 0xa
	.byte	17                      # 0x11
	.byte	10                      # 0xa
	.byte	17                      # 0x11
	.byte	10                      # 0xa
	.byte	16                      # 0x10
	.byte	10                      # 0xa
	.byte	16                      # 0x10
	.byte	10                      # 0xa
	.byte	15                      # 0xf
	.byte	8                       # 0x8
	.byte	15                      # 0xf
	.byte	8                       # 0x8
	.byte	15                      # 0xf
	.byte	8                       # 0x8
	.byte	15                      # 0xf
	.byte	8                       # 0x8
	.byte	15                      # 0xf
	.byte	8                       # 0x8
	.byte	15                      # 0xf
	.byte	8                       # 0x8
	.byte	15                      # 0xf
	.byte	8                       # 0x8
	.byte	15                      # 0xf
	.byte	8                       # 0x8
	.byte	14                      # 0xe
	.byte	8                       # 0x8
	.byte	14                      # 0xe
	.byte	8                       # 0x8
	.byte	14                      # 0xe
	.byte	8                       # 0x8
	.byte	14                      # 0xe
	.byte	8                       # 0x8
	.byte	14                      # 0xe
	.byte	8                       # 0x8
	.byte	14                      # 0xe
	.byte	8                       # 0x8
	.byte	14                      # 0xe
	.byte	8                       # 0x8
	.byte	14                      # 0xe
	.byte	8                       # 0x8
	.byte	13                      # 0xd
	.byte	8                       # 0x8
	.byte	13                      # 0xd
	.byte	8                       # 0x8
	.byte	13                      # 0xd
	.byte	8                       # 0x8
	.byte	13                      # 0xd
	.byte	8                       # 0x8
	.byte	13                      # 0xd
	.byte	8                       # 0x8
	.byte	13                      # 0xd
	.byte	8                       # 0x8
	.byte	13                      # 0xd
	.byte	8                       # 0x8
	.byte	13                      # 0xd
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	8                       # 0x8
	.byte	12                      # 0xc
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	8                       # 0x8
	.byte	11                      # 0xb
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.size	MBAtab2, 208

	.type	DClumtab0,@object       # @DClumtab0
	.p2align	4
DClumtab0:
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	6                       # 0x6
	.byte	5                       # 0x5
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.size	DClumtab0, 64

	.type	DClumtab1,@object       # @DClumtab1
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
DClumtab1:
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	6                       # 0x6
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	7                       # 0x7
	.byte	9                       # 0x9
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	8                       # 0x8
	.byte	10                      # 0xa
	.byte	9                       # 0x9
	.byte	11                      # 0xb
	.byte	9                       # 0x9
	.size	DClumtab1, 32

	.type	DCchromtab0,@object     # @DCchromtab0
	.section	.rodata,"a",@progbits
	.p2align	4
DCchromtab0:
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.size	DCchromtab0, 64

	.type	DCchromtab1,@object     # @DCchromtab1
	.p2align	4
DCchromtab1:
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	9                       # 0x9
	.byte	9                       # 0x9
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	10                      # 0xa
	.size	DCchromtab1, 64

	.type	PMBtab0,@object         # @PMBtab0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
PMBtab0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	8                       # 0x8
	.byte	3                       # 0x3
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	10                      # 0xa
	.byte	1                       # 0x1
	.byte	10                      # 0xa
	.byte	1                       # 0x1
	.byte	10                      # 0xa
	.byte	1                       # 0x1
	.byte	10                      # 0xa
	.byte	1                       # 0x1
	.size	PMBtab0, 16

	.type	PMBtab1,@object         # @PMBtab1
	.p2align	4
PMBtab1:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	17                      # 0x11
	.byte	6                       # 0x6
	.byte	18                      # 0x12
	.byte	5                       # 0x5
	.byte	18                      # 0x12
	.byte	5                       # 0x5
	.byte	26                      # 0x1a
	.byte	5                       # 0x5
	.byte	26                      # 0x1a
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.size	PMBtab1, 16

	.type	BMBtab0,@object         # @BMBtab0
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
BMBtab0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	8                       # 0x8
	.byte	4                       # 0x4
	.byte	10                      # 0xa
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	6                       # 0x6
	.byte	3                       # 0x3
	.byte	6                       # 0x6
	.byte	3                       # 0x3
	.byte	12                      # 0xc
	.byte	2                       # 0x2
	.byte	12                      # 0xc
	.byte	2                       # 0x2
	.byte	12                      # 0xc
	.byte	2                       # 0x2
	.byte	12                      # 0xc
	.byte	2                       # 0x2
	.byte	14                      # 0xe
	.byte	2                       # 0x2
	.byte	14                      # 0xe
	.byte	2                       # 0x2
	.byte	14                      # 0xe
	.byte	2                       # 0x2
	.byte	14                      # 0xe
	.byte	2                       # 0x2
	.size	BMBtab0, 32

	.type	BMBtab1,@object         # @BMBtab1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
BMBtab1:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	17                      # 0x11
	.byte	6                       # 0x6
	.byte	22                      # 0x16
	.byte	6                       # 0x6
	.byte	26                      # 0x1a
	.byte	6                       # 0x6
	.byte	30                      # 0x1e
	.byte	5                       # 0x5
	.byte	30                      # 0x1e
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.byte	1                       # 0x1
	.byte	5                       # 0x5
	.size	BMBtab1, 16

	.type	spIMBtab,@object        # @spIMBtab
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
spIMBtab:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	64                      # 0x40
	.byte	4                       # 0x4
	.byte	17                      # 0x11
	.byte	4                       # 0x4
	.byte	1                       # 0x1
	.byte	4                       # 0x4
	.byte	82                      # 0x52
	.byte	2                       # 0x2
	.byte	82                      # 0x52
	.byte	2                       # 0x2
	.byte	82                      # 0x52
	.byte	2                       # 0x2
	.byte	82                      # 0x52
	.byte	2                       # 0x2
	.byte	66                      # 0x42
	.byte	1                       # 0x1
	.byte	66                      # 0x42
	.byte	1                       # 0x1
	.byte	66                      # 0x42
	.byte	1                       # 0x1
	.byte	66                      # 0x42
	.byte	1                       # 0x1
	.byte	66                      # 0x42
	.byte	1                       # 0x1
	.byte	66                      # 0x42
	.byte	1                       # 0x1
	.byte	66                      # 0x42
	.byte	1                       # 0x1
	.byte	66                      # 0x42
	.byte	1                       # 0x1
	.size	spIMBtab, 32

	.type	spPMBtab0,@object       # @spPMBtab0
	.p2align	4
spPMBtab0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	8                       # 0x8
	.byte	4                       # 0x4
	.byte	40                      # 0x28
	.byte	4                       # 0x4
	.byte	26                      # 0x1a
	.byte	3                       # 0x3
	.byte	26                      # 0x1a
	.byte	3                       # 0x3
	.byte	42                      # 0x2a
	.byte	3                       # 0x3
	.byte	42                      # 0x2a
	.byte	3                       # 0x3
	.byte	10                      # 0xa
	.byte	2                       # 0x2
	.byte	10                      # 0xa
	.byte	2                       # 0x2
	.byte	10                      # 0xa
	.byte	2                       # 0x2
	.byte	10                      # 0xa
	.byte	2                       # 0x2
	.byte	58                      # 0x3a
	.byte	2                       # 0x2
	.byte	58                      # 0x3a
	.byte	2                       # 0x2
	.byte	58                      # 0x3a
	.byte	2                       # 0x2
	.byte	58                      # 0x3a
	.byte	2                       # 0x2
	.size	spPMBtab0, 32

	.type	spPMBtab1,@object       # @spPMBtab1
	.p2align	4
spPMBtab1:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	82                      # 0x52
	.byte	7                       # 0x7
	.byte	64                      # 0x40
	.byte	7                       # 0x7
	.byte	2                       # 0x2
	.byte	7                       # 0x7
	.byte	66                      # 0x42
	.byte	7                       # 0x7
	.byte	17                      # 0x11
	.byte	7                       # 0x7
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	18                      # 0x12
	.byte	6                       # 0x6
	.byte	18                      # 0x12
	.byte	6                       # 0x6
	.byte	50                      # 0x32
	.byte	6                       # 0x6
	.byte	50                      # 0x32
	.byte	6                       # 0x6
	.byte	32                      # 0x20
	.byte	6                       # 0x6
	.byte	32                      # 0x20
	.byte	6                       # 0x6
	.byte	34                      # 0x22
	.byte	6                       # 0x6
	.byte	34                      # 0x22
	.byte	6                       # 0x6
	.size	spPMBtab1, 32

	.type	spBMBtab0,@object       # @spBMBtab0
	.section	.rodata,"a",@progbits
	.p2align	4
spBMBtab0:
	.byte	8                       # 0x8
	.byte	4                       # 0x4
	.byte	10                      # 0xa
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	6                       # 0x6
	.byte	3                       # 0x3
	.byte	6                       # 0x6
	.byte	3                       # 0x3
	.byte	12                      # 0xc
	.byte	2                       # 0x2
	.byte	12                      # 0xc
	.byte	2                       # 0x2
	.byte	12                      # 0xc
	.byte	2                       # 0x2
	.byte	12                      # 0xc
	.byte	2                       # 0x2
	.byte	14                      # 0xe
	.byte	2                       # 0x2
	.byte	14                      # 0xe
	.byte	2                       # 0x2
	.byte	14                      # 0xe
	.byte	2                       # 0x2
	.byte	14                      # 0xe
	.byte	2                       # 0x2
	.size	spBMBtab0, 28

	.type	spBMBtab1,@object       # @spBMBtab1
	.p2align	4
spBMBtab1:
	.byte	26                      # 0x1a
	.byte	7                       # 0x7
	.byte	22                      # 0x16
	.byte	7                       # 0x7
	.byte	1                       # 0x1
	.byte	7                       # 0x7
	.byte	30                      # 0x1e
	.byte	7                       # 0x7
	.byte	40                      # 0x28
	.byte	6                       # 0x6
	.byte	40                      # 0x28
	.byte	6                       # 0x6
	.byte	42                      # 0x2a
	.byte	6                       # 0x6
	.byte	42                      # 0x2a
	.byte	6                       # 0x6
	.byte	36                      # 0x24
	.byte	6                       # 0x6
	.byte	36                      # 0x24
	.byte	6                       # 0x6
	.byte	38                      # 0x26
	.byte	6                       # 0x6
	.byte	38                      # 0x26
	.byte	6                       # 0x6
	.size	spBMBtab1, 24

	.type	spBMBtab2,@object       # @spBMBtab2
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
spBMBtab2:
	.byte	17                      # 0x11
	.byte	8                       # 0x8
	.byte	17                      # 0x11
	.byte	8                       # 0x8
	.byte	58                      # 0x3a
	.byte	8                       # 0x8
	.byte	58                      # 0x3a
	.byte	8                       # 0x8
	.byte	54                      # 0x36
	.byte	9                       # 0x9
	.byte	82                      # 0x52
	.byte	9                       # 0x9
	.byte	64                      # 0x40
	.byte	9                       # 0x9
	.byte	66                      # 0x42
	.byte	9                       # 0x9
	.size	spBMBtab2, 16

	.type	SNRMBtab,@object        # @SNRMBtab
	.p2align	4
SNRMBtab:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	18                      # 0x12
	.byte	2                       # 0x2
	.byte	18                      # 0x12
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.size	SNRMBtab, 16

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Get_macroblock_type(): unrecognized picture coding type"
	.size	.Lstr, 56

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	"Invalid macroblock_type code"
	.size	.Lstr.8, 29

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	"Invalid coded_block_pattern code"
	.size	.Lstr.9, 33

	.type	.Lstr.10,@object        # @str.10
	.p2align	4
.Lstr.10:
	.asciz	"Invalid macroblock_address_increment code"
	.size	.Lstr.10, 42


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
