	.text
	.file	"FileFind.bc"
	.globl	_Z21my_windows_split_pathRK11CStringBaseIcERS0_S3_
	.p2align	4, 0x90
	.type	_Z21my_windows_split_pathRK11CStringBaseIcERS0_S3_,@function
_Z21my_windows_split_pathRK11CStringBaseIcERS0_S3_: # @_Z21my_windows_split_pathRK11CStringBaseIcERS0_S3_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movslq	8(%r12), %rax
	testq	%rax, %rax
	je	.LBB0_9
# BB#1:
	movq	(%r12), %rdi
	leaq	-1(%rdi,%rax), %rbx
	cmpb	$47, -1(%rdi,%rax)
	je	.LBB0_4
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdi, %rbx
	je	.LBB0_9
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rsi
	callq	_Z9CharPrevAPKcS0_
	movq	%rax, %rbx
	movq	(%r12), %rdi
	cmpb	$47, (%rbx)
	jne	.LBB0_2
.LBB0_4:                                # %_ZNK11CStringBaseIcE11ReverseFindEc.exit
	subq	%rdi, %rbx
	cmpl	$-1, %ebx
	je	.LBB0_9
# BB#5:
	leal	1(%rbx), %edx
	movl	8(%r12), %ecx
	subl	%edx, %ecx
	jle	.LBB0_24
# BB#6:
	movq	%rsp, %r13
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	_ZNK11CStringBaseIcE3MidEii
	cmpq	%r15, %r13
	je	.LBB0_113
# BB#7:
	movl	$0, 8(%r15)
	movq	(%r15), %rax
	movb	$0, (%rax)
	movslq	8(%rsp), %rax
	leaq	1(%rax), %rcx
	movl	12(%r15), %r13d
	cmpl	%r13d, %ecx
	jne	.LBB0_67
# BB#8:                                 # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i74
	movq	(%r15), %rbp
	movq	%rsp, %r13
	jmp	.LBB0_110
.LBB0_9:                                # %_ZNK11CStringBaseIcE11ReverseFindEc.exit.thread
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movb	$0, (%rax)
	movl	12(%r14), %ebp
	cmpl	$2, %ebp
	jne	.LBB0_11
# BB#10:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	(%r14), %r13
	jmp	.LBB0_35
.LBB0_11:
	movl	$2, %edi
	callq	_Znam
	movq	%rax, %r13
	testl	%ebp, %ebp
	jle	.LBB0_34
# BB#12:                                # %.preheader.i.i
	movslq	8(%r14), %rax
	testq	%rax, %rax
	movq	(%r14), %rdi
	jle	.LBB0_32
# BB#13:                                # %.lr.ph.preheader.i.i
	cmpl	$31, %eax
	jbe	.LBB0_17
# BB#14:                                # %min.iters.checked299
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_17
# BB#15:                                # %vector.memcheck310
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r13
	jae	.LBB0_104
# BB#16:                                # %vector.memcheck310
	leaq	(%r13,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_104
.LBB0_17:
	xorl	%ecx, %ecx
.LBB0_18:                               # %.lr.ph.i.i.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_21
# BB#19:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r13,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_20
.LBB0_21:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB0_33
# BB#22:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r13,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_23
	jmp	.LBB0_33
.LBB0_24:                               # %.preheader
	movb	(%rdi), %al
	testb	%al, %al
	je	.LBB0_30
# BB#25:                                # %.lr.ph.preheader
	movl	$-1, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$47, %al
	cmovnel	%edx, %ecx
	movzbl	1(%rdi,%rdx), %eax
	incq	%rdx
	testb	%al, %al
	jne	.LBB0_26
# BB#27:                                # %._crit_edge
	cmpl	$-1, %ecx
	je	.LBB0_30
# BB#28:
	incl	%ecx
	movq	%rsp, %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZNK11CStringBaseIcE3MidEii
.Ltmp0:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_Z21my_windows_split_pathRK11CStringBaseIcERS0_S3_
.Ltmp1:
	jmp	.LBB0_180
.LBB0_30:                               # %_Z11MyStringLenIcEiPKT_.exit.i130
	movl	$0, 8(%r15)
	movq	(%r15), %rax
	movb	$0, (%rax)
	movl	12(%r15), %ebp
	cmpl	$2, %ebp
	jne	.LBB0_81
# BB#31:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i132
	movq	(%r15), %r12
	jmp	.LBB0_155
.LBB0_32:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB0_34
.LBB0_33:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB0_34:                               # %._crit_edge17.i.i
	movq	%r13, (%r14)
	movslq	8(%r14), %rax
	movb	$0, (%r13,%rax)
	movl	$2, 12(%r14)
.LBB0_35:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.preheader
	movb	$46, (%r13)
	movb	$0, 1(%r13)
	movl	$1, 8(%r14)
	cmpl	$0, 8(%r12)
	je	.LBB0_39
# BB#36:
	cmpq	%r15, %r12
	je	.LBB0_182
# BB#37:
	movl	$0, 8(%r15)
	movq	(%r15), %rax
	movb	$0, (%rax)
	movslq	8(%r12), %rax
	leaq	1(%rax), %r14
	movl	12(%r15), %ebp
	cmpl	%ebp, %r14d
	jne	.LBB0_41
# BB#38:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	(%r15), %rbx
	jmp	.LBB0_97
.LBB0_39:                               # %_Z11MyStringLenIcEiPKT_.exit.i44
	movl	$0, 8(%r15)
	movq	(%r15), %rax
	movb	$0, (%rax)
	movl	12(%r15), %ebp
	cmpl	$2, %ebp
	jne	.LBB0_54
# BB#40:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i46
	movq	(%r15), %r14
	jmp	.LBB0_103
.LBB0_41:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r14, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB0_96
# BB#42:                                # %.preheader.i.i61
	movslq	8(%r15), %r8
	testq	%r8, %r8
	movq	(%r15), %rdi
	jle	.LBB0_94
# BB#43:                                # %.lr.ph.preheader.i.i62
	cmpl	$31, %r8d
	jbe	.LBB0_47
# BB#44:                                # %min.iters.checked326
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB0_47
# BB#45:                                # %vector.memcheck337
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB0_183
# BB#46:                                # %vector.memcheck337
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_183
.LBB0_47:
	xorl	%ecx, %ecx
.LBB0_48:                               # %.lr.ph.i.i67.preheader
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_51
# BB#49:                                # %.lr.ph.i.i67.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_50:                               # %.lr.ph.i.i67.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_50
.LBB0_51:                               # %.lr.ph.i.i67.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB0_95
# BB#52:                                # %.lr.ph.i.i67.preheader.new
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_53:                               # %.lr.ph.i.i67
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB0_53
	jmp	.LBB0_95
.LBB0_54:
	movl	$2, %edi
	callq	_Znam
	movq	%rax, %r14
	testl	%ebp, %ebp
	jle	.LBB0_102
# BB#55:                                # %.preheader.i.i47
	movslq	8(%r15), %rax
	testq	%rax, %rax
	movq	(%r15), %rdi
	jle	.LBB0_100
# BB#56:                                # %.lr.ph.preheader.i.i48
	cmpl	$31, %eax
	jbe	.LBB0_60
# BB#57:                                # %min.iters.checked353
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_60
# BB#58:                                # %vector.memcheck364
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r14
	jae	.LBB0_186
# BB#59:                                # %vector.memcheck364
	leaq	(%r14,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_186
.LBB0_60:
	xorl	%ecx, %ecx
.LBB0_61:                               # %.lr.ph.i.i53.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_64
# BB#62:                                # %.lr.ph.i.i53.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_63:                               # %.lr.ph.i.i53.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r14,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_63
.LBB0_64:                               # %.lr.ph.i.i53.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB0_101
# BB#65:                                # %.lr.ph.i.i53.preheader.new
	subq	%rcx, %rax
	leaq	7(%r14,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_66:                               # %.lr.ph.i.i53
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_66
	jmp	.LBB0_101
.LBB0_67:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	cmovlq	%rax, %rdi
.Ltmp3:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp4:
# BB#68:                                # %.noexc
	testl	%r13d, %r13d
	movq	%rsp, %r13
	jle	.LBB0_109
# BB#69:                                # %.preheader.i.i75
	movslq	8(%r15), %r9
	testq	%r9, %r9
	movq	(%r15), %rdi
	jle	.LBB0_107
# BB#70:                                # %.lr.ph.preheader.i.i76
	cmpl	$31, %r9d
	jbe	.LBB0_74
# BB#71:                                # %min.iters.checked218
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB0_74
# BB#72:                                # %vector.memcheck229
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB0_204
# BB#73:                                # %vector.memcheck229
	leaq	(%rbp,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_204
.LBB0_74:
	xorl	%ecx, %ecx
.LBB0_75:                               # %.lr.ph.i.i81.preheader
	movl	%r9d, %esi
	subl	%ecx, %esi
	leaq	-1(%r9), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB0_78
# BB#76:                                # %.lr.ph.i.i81.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_77:                               # %.lr.ph.i.i81.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_77
.LBB0_78:                               # %.lr.ph.i.i81.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB0_108
# BB#79:                                # %.lr.ph.i.i81.preheader.new
	subq	%rcx, %r9
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_80:                               # %.lr.ph.i.i81
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB0_80
	jmp	.LBB0_108
.LBB0_81:
	movl	$2, %edi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebp, %ebp
	jle	.LBB0_154
# BB#82:                                # %.preheader.i.i133
	movslq	8(%r15), %rax
	testq	%rax, %rax
	movq	(%r15), %rdi
	jle	.LBB0_152
# BB#83:                                # %.lr.ph.preheader.i.i134
	cmpl	$31, %eax
	jbe	.LBB0_87
# BB#84:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_87
# BB#85:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB0_207
# BB#86:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_207
.LBB0_87:
	xorl	%ecx, %ecx
.LBB0_88:                               # %.lr.ph.i.i139.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_91
# BB#89:                                # %.lr.ph.i.i139.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_90:                               # %.lr.ph.i.i139.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r12,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_90
.LBB0_91:                               # %.lr.ph.i.i139.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB0_153
# BB#92:                                # %.lr.ph.i.i139.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_93:                               # %.lr.ph.i.i139
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_93
	jmp	.LBB0_153
.LBB0_94:                               # %._crit_edge.i.i63
	testq	%rdi, %rdi
	je	.LBB0_96
.LBB0_95:                               # %._crit_edge.thread.i.i68
	callq	_ZdaPv
.LBB0_96:                               # %._crit_edge17.i.i69
	movq	%rbx, (%r15)
	movslq	8(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r14d, 12(%r15)
.LBB0_97:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i70
	movq	(%r12), %rax
	.p2align	4, 0x90
.LBB0_98:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB0_98
# BB#99:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
	movl	8(%r12), %eax
	movl	%eax, 8(%r15)
	jmp	.LBB0_182
.LBB0_100:                              # %._crit_edge.i.i49
	testq	%rdi, %rdi
	je	.LBB0_102
.LBB0_101:                              # %._crit_edge.thread.i.i54
	callq	_ZdaPv
.LBB0_102:                              # %._crit_edge17.i.i55
	movq	%r14, (%r15)
	movslq	8(%r15), %rax
	movb	$0, (%r14,%rax)
	movl	$2, 12(%r15)
.LBB0_103:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i58.preheader
	movb	$46, (%r14)
	movb	$0, 1(%r14)
	movl	$1, 8(%r15)
	jmp	.LBB0_182
.LBB0_104:                              # %vector.body295.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_189
# BB#105:                               # %vector.body295.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_106:                              # %vector.body295.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r13,%rbp)
	movups	%xmm1, 16(%r13,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_106
	jmp	.LBB0_190
.LBB0_107:                              # %._crit_edge.i.i77
	testq	%rdi, %rdi
	je	.LBB0_109
.LBB0_108:                              # %._crit_edge.thread.i.i82
	callq	_ZdaPv
.LBB0_109:                              # %._crit_edge17.i.i83
	movq	%rbp, (%r15)
	movslq	8(%r15), %rax
	movb	$0, (%rbp,%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 12(%r15)
.LBB0_110:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i84
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB0_111:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB0_111
# BB#112:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i87
	movl	8(%rsp), %eax
	movl	%eax, 8(%r15)
.LBB0_113:                              # %_ZN11CStringBaseIcEaSERKS0_.exit88
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_115
# BB#114:
	callq	_ZdaPv
.LBB0_115:                              # %_ZN11CStringBaseIcED2Ev.exit
	movq	(%r12), %rax
	movslq	%ebx, %rcx
	.p2align	4, 0x90
.LBB0_116:                              # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	jle	.LBB0_119
# BB#117:                               #   in Loop: Header=BB0_116 Depth=1
	leaq	-1(%rcx), %rdx
	cmpb	$47, -1(%rax,%rcx)
	movq	%rdx, %rcx
	je	.LBB0_116
# BB#118:                               # %.critedge.thread.loopexit
	incl	%edx
	movl	%edx, %ecx
	jmp	.LBB0_120
.LBB0_119:                              # %.critedge
	testl	%ecx, %ecx
	je	.LBB0_137
.LBB0_120:                              # %.critedge.thread
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIcE3MidEii
	cmpq	%r14, %r13
	je	.LBB0_180
# BB#121:
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movb	$0, (%rax)
	movslq	8(%rsp), %rax
	leaq	1(%rax), %r15
	movl	12(%r14), %ebp
	cmpl	%ebp, %r15d
	jne	.LBB0_123
# BB#122:                               # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i110
	movq	(%r14), %rbx
	jmp	.LBB0_177
.LBB0_123:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r15, %rdi
	cmovlq	%rax, %rdi
.Ltmp6:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp7:
# BB#124:                               # %.noexc124
	testl	%ebp, %ebp
	jle	.LBB0_176
# BB#125:                               # %.preheader.i.i111
	movslq	8(%r14), %r8
	testq	%r8, %r8
	movq	(%r14), %rdi
	jle	.LBB0_174
# BB#126:                               # %.lr.ph.preheader.i.i112
	cmpl	$31, %r8d
	jbe	.LBB0_130
# BB#127:                               # %min.iters.checked272
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB0_130
# BB#128:                               # %vector.memcheck283
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB0_213
# BB#129:                               # %vector.memcheck283
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_213
.LBB0_130:
	xorl	%ecx, %ecx
.LBB0_131:                              # %.lr.ph.i.i117.preheader
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_134
# BB#132:                               # %.lr.ph.i.i117.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_133:                              # %.lr.ph.i.i117.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_133
.LBB0_134:                              # %.lr.ph.i.i117.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB0_175
# BB#135:                               # %.lr.ph.i.i117.preheader.new
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_136:                              # %.lr.ph.i.i117
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB0_136
	jmp	.LBB0_175
.LBB0_137:                              # %_Z11MyStringLenIcEiPKT_.exit.i92
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movb	$0, (%rax)
	movl	12(%r14), %ebp
	cmpl	$2, %ebp
	jne	.LBB0_139
# BB#138:                               # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i94
	movq	(%r14), %r15
	jmp	.LBB0_173
.LBB0_139:
	movl	$2, %edi
	callq	_Znam
	movq	%rax, %r15
	testl	%ebp, %ebp
	jle	.LBB0_172
# BB#140:                               # %.preheader.i.i95
	movslq	8(%r14), %rax
	testq	%rax, %rax
	movq	(%r14), %rdi
	jle	.LBB0_170
# BB#141:                               # %.lr.ph.preheader.i.i96
	cmpl	$31, %eax
	jbe	.LBB0_145
# BB#142:                               # %min.iters.checked245
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_145
# BB#143:                               # %vector.memcheck256
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r15
	jae	.LBB0_231
# BB#144:                               # %vector.memcheck256
	leaq	(%r15,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_231
.LBB0_145:
	xorl	%ecx, %ecx
.LBB0_146:                              # %.lr.ph.i.i101.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_149
# BB#147:                               # %.lr.ph.i.i101.prol.preheader
	negq	%rsi
.LBB0_148:                              # %.lr.ph.i.i101.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r15,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_148
.LBB0_149:                              # %.lr.ph.i.i101.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB0_171
# BB#150:                               # %.lr.ph.i.i101.preheader.new
	subq	%rcx, %rax
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB0_151:                              # %.lr.ph.i.i101
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_151
	jmp	.LBB0_171
.LBB0_152:                              # %._crit_edge.i.i135
	testq	%rdi, %rdi
	je	.LBB0_154
.LBB0_153:                              # %._crit_edge.thread.i.i140
	callq	_ZdaPv
.LBB0_154:                              # %._crit_edge17.i.i141
	movq	%r12, (%r15)
	movslq	8(%r15), %rax
	movb	$0, (%r12,%rax)
	movl	$2, 12(%r15)
.LBB0_155:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i144.preheader
	movb	$47, (%r12)
	movb	$0, 1(%r12)
	movl	$1, 8(%r15)
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movb	$0, (%rax)
	movl	12(%r14), %ebp
	cmpl	$2, %ebp
	jne	.LBB0_157
# BB#156:                               # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i151
	movq	(%r14), %r15
	jmp	.LBB0_173
.LBB0_157:
	movl	$2, %edi
	callq	_Znam
	movq	%rax, %r15
	testl	%ebp, %ebp
	jle	.LBB0_172
# BB#158:                               # %.preheader.i.i152
	movslq	8(%r14), %rax
	testq	%rax, %rax
	movq	(%r14), %rdi
	jle	.LBB0_170
# BB#159:                               # %.lr.ph.preheader.i.i153
	cmpl	$31, %eax
	jbe	.LBB0_163
# BB#160:                               # %min.iters.checked191
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_163
# BB#161:                               # %vector.memcheck202
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r15
	jae	.LBB0_210
# BB#162:                               # %vector.memcheck202
	leaq	(%r15,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_210
.LBB0_163:
	xorl	%ecx, %ecx
.LBB0_164:                              # %.lr.ph.i.i158.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_167
# BB#165:                               # %.lr.ph.i.i158.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_166:                              # %.lr.ph.i.i158.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r15,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_166
.LBB0_167:                              # %.lr.ph.i.i158.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB0_171
# BB#168:                               # %.lr.ph.i.i158.preheader.new
	subq	%rcx, %rax
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_169:                              # %.lr.ph.i.i158
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_169
	jmp	.LBB0_171
.LBB0_170:                              # %._crit_edge.i.i97
	testq	%rdi, %rdi
	je	.LBB0_172
.LBB0_171:                              # %._crit_edge.thread.i.i102
	callq	_ZdaPv
.LBB0_172:                              # %._crit_edge17.i.i103
	movq	%r15, (%r14)
	movslq	8(%r14), %rax
	movb	$0, (%r15,%rax)
	movl	$2, 12(%r14)
.LBB0_173:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i106.preheader
	movb	$47, (%r15)
	movb	$0, 1(%r15)
	movl	$1, 8(%r14)
	jmp	.LBB0_182
.LBB0_174:                              # %._crit_edge.i.i113
	testq	%rdi, %rdi
	je	.LBB0_176
.LBB0_175:                              # %._crit_edge.thread.i.i118
	callq	_ZdaPv
.LBB0_176:                              # %._crit_edge17.i.i119
	movq	%rbx, (%r14)
	movslq	8(%r14), %rax
	movb	$0, (%rbx,%rax)
	movl	%r15d, 12(%r14)
.LBB0_177:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i120
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB0_178:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB0_178
# BB#179:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i123
	movl	8(%rsp), %eax
	movl	%eax, 8(%r14)
.LBB0_180:                              # %_ZN11CStringBaseIcEaSERKS0_.exit125
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_182
# BB#181:
	callq	_ZdaPv
.LBB0_182:                              # %_ZN11CStringBaseIcEaSERKS0_.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_183:                              # %vector.body322.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_194
# BB#184:                               # %vector.body322.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB0_185:                              # %vector.body322.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%rbx,%rbp)
	movups	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_185
	jmp	.LBB0_195
.LBB0_186:                              # %vector.body349.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_199
# BB#187:                               # %vector.body349.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB0_188:                              # %vector.body349.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r14,%rbp)
	movups	%xmm1, 16(%r14,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_188
	jmp	.LBB0_200
.LBB0_189:
	xorl	%ebp, %ebp
.LBB0_190:                              # %vector.body295.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB0_193
# BB#191:                               # %vector.body295.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r13,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
	.p2align	4, 0x90
.LBB0_192:                              # %vector.body295
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_192
.LBB0_193:                              # %middle.block296
	cmpq	%rcx, %rax
	jne	.LBB0_18
	jmp	.LBB0_33
.LBB0_194:
	xorl	%ebp, %ebp
.LBB0_195:                              # %vector.body322.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB0_198
# BB#196:                               # %vector.body322.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB0_197:                              # %vector.body322
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_197
.LBB0_198:                              # %middle.block323
	cmpq	%rcx, %r8
	jne	.LBB0_48
	jmp	.LBB0_95
.LBB0_199:
	xorl	%ebp, %ebp
.LBB0_200:                              # %vector.body349.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB0_203
# BB#201:                               # %vector.body349.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r14,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB0_202:                              # %vector.body349
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_202
.LBB0_203:                              # %middle.block350
	cmpq	%rcx, %rax
	jne	.LBB0_61
	jmp	.LBB0_101
.LBB0_204:                              # %vector.body214.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_216
# BB#205:                               # %vector.body214.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
.LBB0_206:                              # %vector.body214.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbp,%rdx)
	movups	%xmm1, 16(%rbp,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB0_206
	jmp	.LBB0_217
.LBB0_207:                              # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_221
# BB#208:                               # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB0_209:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r12,%rbp)
	movups	%xmm1, 16(%r12,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_209
	jmp	.LBB0_222
.LBB0_210:                              # %vector.body187.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_226
# BB#211:                               # %vector.body187.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB0_212:                              # %vector.body187.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r15,%rbp)
	movups	%xmm1, 16(%r15,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_212
	jmp	.LBB0_227
.LBB0_213:                              # %vector.body268.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_234
# BB#214:                               # %vector.body268.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB0_215:                              # %vector.body268.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%rbx,%rbp)
	movups	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_215
	jmp	.LBB0_235
.LBB0_216:
	xorl	%edx, %edx
.LBB0_217:                              # %vector.body214.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB0_220
# BB#218:                               # %vector.body214.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbp,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB0_219:                              # %vector.body214
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB0_219
.LBB0_220:                              # %middle.block215
	cmpq	%rcx, %r9
	jne	.LBB0_75
	jmp	.LBB0_108
.LBB0_221:
	xorl	%ebp, %ebp
.LBB0_222:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB0_225
# BB#223:                               # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r12,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB0_224:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_224
.LBB0_225:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB0_88
	jmp	.LBB0_153
.LBB0_226:
	xorl	%ebp, %ebp
.LBB0_227:                              # %vector.body187.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB0_230
# BB#228:                               # %vector.body187.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r15,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB0_229:                              # %vector.body187
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_229
.LBB0_230:                              # %middle.block188
	cmpq	%rcx, %rax
	jne	.LBB0_164
	jmp	.LBB0_171
.LBB0_231:                              # %vector.body241.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_239
# BB#232:                               # %vector.body241.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB0_233:                              # %vector.body241.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r15,%rbp)
	movups	%xmm1, 16(%r15,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_233
	jmp	.LBB0_240
.LBB0_234:
	xorl	%ebp, %ebp
.LBB0_235:                              # %vector.body268.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB0_238
# BB#236:                               # %vector.body268.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB0_237:                              # %vector.body268
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_237
.LBB0_238:                              # %middle.block269
	cmpq	%rcx, %r8
	jne	.LBB0_131
	jmp	.LBB0_175
.LBB0_239:
	xorl	%ebp, %ebp
.LBB0_240:                              # %vector.body241.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB0_243
# BB#241:                               # %vector.body241.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r15,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB0_242:                              # %vector.body241
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_242
.LBB0_243:                              # %middle.block242
	cmpq	%rcx, %rax
	jne	.LBB0_146
	jmp	.LBB0_171
.LBB0_244:
.Ltmp8:
	jmp	.LBB0_247
.LBB0_245:
.Ltmp5:
	jmp	.LBB0_247
.LBB0_246:
.Ltmp2:
.LBB0_247:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_249
# BB#248:
	callq	_ZdaPv
.LBB0_249:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z21my_windows_split_pathRK11CStringBaseIcERS0_S3_, .Lfunc_end0-_Z21my_windows_split_pathRK11CStringBaseIcERS0_S3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp6-.Ltmp4           #   Call between .Ltmp4 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcED2Ev,"axG",@progbits,_ZN11CStringBaseIcED2Ev,comdat
	.weak	_ZN11CStringBaseIcED2Ev
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcED2Ev,@function
_ZN11CStringBaseIcED2Ev:                # @_ZN11CStringBaseIcED2Ev
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB1_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB1_1:
	retq
.Lfunc_end1:
	.size	_ZN11CStringBaseIcED2Ev, .Lfunc_end1-_ZN11CStringBaseIcED2Ev
	.cfi_endproc

	.text
	.globl	_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv
	.p2align	4, 0x90
	.type	_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv,@function
_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv: # @_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv
	.cfi_startproc
# BB#0:
	testb	$16, 32(%rdi)
	je	.LBB2_1
# BB#3:
	movl	48(%rdi), %ecx
	testl	%ecx, %ecx
	je	.LBB2_1
# BB#4:
	movq	40(%rdi), %rdx
	cmpb	$46, (%rdx)
	jne	.LBB2_1
# BB#5:
	movb	$1, %al
	cmpl	$1, %ecx
	je	.LBB2_2
# BB#6:
	cmpb	$46, 1(%rdx)
	sete	%dl
	cmpl	$2, %ecx
	sete	%al
	andb	%dl, %al
	retq
.LBB2_1:
	xorl	%eax, %eax
.LBB2_2:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end2:
	.size	_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv, .Lfunc_end2-_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv
	.cfi_endproc

	.globl	_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv
	.p2align	4, 0x90
	.type	_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv,@function
_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv: # @_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv
	.cfi_startproc
# BB#0:
	testb	$16, 32(%rdi)
	je	.LBB3_1
# BB#3:
	movl	48(%rdi), %ecx
	testl	%ecx, %ecx
	je	.LBB3_1
# BB#4:
	movq	40(%rdi), %rdx
	cmpl	$46, (%rdx)
	jne	.LBB3_1
# BB#5:
	movb	$1, %al
	cmpl	$1, %ecx
	je	.LBB3_2
# BB#6:
	cmpl	$46, 4(%rdx)
	sete	%dl
	cmpl	$2, %ecx
	sete	%al
	andb	%dl, %al
	retq
.LBB3_1:
	xorl	%eax, %eax
.LBB3_2:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end3:
	.size	_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv, .Lfunc_end3-_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv
	.cfi_endproc

	.globl	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv,@function
_ZN8NWindows5NFile5NFind9CFindFile5CloseEv: # @_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movb	$1, %bpl
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#1:
	callq	closedir
	testl	%eax, %eax
	je	.LBB4_3
# BB#2:
	xorl	%ebp, %ebp
	jmp	.LBB4_4
.LBB4_3:
	movq	$0, (%rbx)
.LBB4_4:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv, .Lfunc_end4-_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
	.cfi_endproc

	.globl	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKcRNS1_9CFileInfoE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKcRNS1_9CFileInfoE,@function
_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKcRNS1_9CFileInfoE: # @_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKcRNS1_9CFileInfoE
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 96
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB5_3
# BB#1:
	callq	closedir
	testl	%eax, %eax
	jne	.LBB5_60
# BB#2:
	movq	$0, (%r13)
.LBB5_3:
	testq	%rbx, %rbx
	je	.LBB5_27
# BB#4:
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB5_27
# BB#5:
	cmpb	$99, %al
	jne	.LBB5_7
# BB#6:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB5_7:                                # %_ZL16nameWindowToUnixPKc.exit
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB5_8:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB5_8
# BB#9:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	1(%rbp), %eax
	movslq	%eax, %r15
	cmpl	$-1, %ebp
	movq	$-1, %rdi
	cmovgeq	%r15, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	%r15d, 28(%rsp)
	.p2align	4, 0x90
.LBB5_10:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB5_10
# BB#11:                                # %_ZN11CStringBaseIcEC2EPKc.exit
	movl	%ebp, 24(%rsp)
	leaq	24(%r13), %r15
	leaq	8(%r13), %r12
.Ltmp9:
	leaq	16(%rsp), %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	_Z21my_windows_split_pathRK11CStringBaseIcERS0_S3_
.Ltmp10:
# BB#12:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_14
# BB#13:
	callq	_ZdaPv
.LBB5_14:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	24(%r13), %rdi
	callq	opendir
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB5_53
# BB#15:                                # %_ZN11CStringBaseIcED2Ev.exit
	movl	global_use_utf16_conversion(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB5_53
# BB#16:
	leaq	16(%rsp), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp12:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp13:
# BB#17:
	movq	%rbp, (%rsp)
	movl	$4, 12(%rsp)
	movl	$0, 8(%rsp)
	movb	$0, (%rbp)
.Ltmp15:
	movl	$1, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp16:
# BB#18:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i29.preheader
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	%rbx, (%rsp)
	movl	$1, 12(%rsp)
	movb	$0, (%rbx)
	movl	$0, 8(%rsp)
	movq	16(%rsp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.LBB5_24
# BB#19:                                # %.lr.ph.i.preheader
	movl	$4, %ebp
	movq	%rsp, %rbx
	.p2align	4, 0x90
.LBB5_20:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$255, %eax
	jg	.LBB5_48
# BB#21:                                #   in Loop: Header=BB5_20 Depth=1
.Ltmp18:
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp19:
# BB#22:                                # %.noexc23
                                        #   in Loop: Header=BB5_20 Depth=1
	movq	16(%rsp), %rax
	movl	(%rax,%rbp), %eax
	addq	$4, %rbp
	testl	%eax, %eax
	jne	.LBB5_20
# BB#23:                                # %.loopexit49.loopexit
	movq	(%rsp), %rbx
.LBB5_24:                               # %.loopexit49
	movq	%rbx, %rdi
	callq	opendir
	movq	%rax, (%r13)
	movq	%rsp, %rax
	cmpq	%r15, %rax
	je	.LBB5_48
# BB#25:
	movl	$0, 32(%r13)
	movq	24(%r13), %rax
	movb	$0, (%rax)
	movslq	8(%rsp), %rax
	leaq	1(%rax), %rcx
	movl	36(%r13), %ebp
	cmpl	%ebp, %ecx
	jne	.LBB5_28
# BB#26:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	(%r15), %rbx
	jmp	.LBB5_45
.LBB5_27:
	callq	__errno_location
	movl	$2, (%rax)
	jmp	.LBB5_60
.LBB5_28:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	cmovlq	%rax, %rdi
.Ltmp21:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp22:
# BB#29:                                # %.noexc44
	testl	%ebp, %ebp
	jle	.LBB5_44
# BB#30:                                # %.preheader.i.i32
	movslq	32(%r13), %r8
	testq	%r8, %r8
	movq	24(%r13), %rdi
	jle	.LBB5_42
# BB#31:                                # %.lr.ph.preheader.i.i33
	cmpl	$31, %r8d
	jbe	.LBB5_35
# BB#32:                                # %min.iters.checked
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB5_35
# BB#33:                                # %vector.memcheck
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB5_63
# BB#34:                                # %vector.memcheck
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB5_63
.LBB5_35:
	xorl	%ecx, %ecx
.LBB5_36:                               # %.lr.ph.i.i38.preheader
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB5_39
# BB#37:                                # %.lr.ph.i.i38.prol.preheader
	negq	%rsi
.LBB5_38:                               # %.lr.ph.i.i38.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB5_38
.LBB5_39:                               # %.lr.ph.i.i38.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB5_43
# BB#40:                                # %.lr.ph.i.i38.preheader.new
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB5_41:                               # %.lr.ph.i.i38
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB5_41
	jmp	.LBB5_43
.LBB5_42:                               # %._crit_edge.i.i34
	testq	%rdi, %rdi
	je	.LBB5_44
.LBB5_43:                               # %._crit_edge.thread.i.i39
	callq	_ZdaPv
.LBB5_44:                               # %._crit_edge17.i.i40
	movq	%rbx, 24(%r13)
	movslq	32(%r13), %rax
	movb	$0, (%rbx,%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, 36(%r13)
.LBB5_45:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i41
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB5_46:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB5_46
# BB#47:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
	movl	8(%rsp), %eax
	movl	%eax, 32(%r13)
.LBB5_48:                               # %_ZN8NWindows5NFile5NFindL16originalFilenameERK11CStringBaseIwERS2_IcE.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_50
# BB#49:
	callq	_ZdaPv
.LBB5_50:                               # %_ZN11CStringBaseIcED2Ev.exit46
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_52
# BB#51:
	callq	_ZdaPv
.LBB5_52:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	(%r13), %rax
.LBB5_53:
	testq	%rax, %rax
	je	.LBB5_60
# BB#54:                                # %.preheader
	movq	%rax, %rdi
	jmp	.LBB5_56
	.p2align	4, 0x90
.LBB5_55:                               # %._crit_edge
                                        #   in Loop: Header=BB5_56 Depth=1
	movq	(%r13), %rdi
.LBB5_56:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	callq	readdir64
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_59
# BB#57:                                # %.lr.ph
                                        #   in Loop: Header=BB5_56 Depth=1
	addq	$19, %rbx
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	callq	_ZL14filter_patternPKcS0_i
	cmpl	$1, %eax
	jne	.LBB5_55
# BB#58:
	movq	(%r15), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKcS5_
	testl	%eax, %eax
	je	.LBB5_62
.LBB5_59:                               # %._crit_edge52
	movq	(%r13), %rdi
	callq	closedir
	movq	$0, (%r13)
	callq	__errno_location
	movl	$1048867, (%rax)        # imm = 0x100123
.LBB5_60:                               # %_ZN8NWindows5NFile5NFind9CFindFile5CloseEv.exit
	xorl	%eax, %eax
.LBB5_61:                               # %_ZN8NWindows5NFile5NFind9CFindFile5CloseEv.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_62:
	movb	$1, %al
	jmp	.LBB5_61
.LBB5_63:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_66
# BB#64:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB5_65:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%rbx,%rbp)
	movups	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB5_65
	jmp	.LBB5_67
.LBB5_66:
	xorl	%ebp, %ebp
.LBB5_67:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB5_70
# BB#68:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB5_69:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB5_69
.LBB5_70:                               # %middle.block
	cmpq	%rcx, %r8
	jne	.LBB5_36
	jmp	.LBB5_43
.LBB5_71:
.Ltmp23:
	jmp	.LBB5_77
.LBB5_72:                               # %.loopexit.split-lp
.Ltmp17:
	jmp	.LBB5_77
.LBB5_73:
.Ltmp14:
	jmp	.LBB5_75
.LBB5_74:
.Ltmp11:
.LBB5_75:
	movq	%rax, %rbx
	jmp	.LBB5_79
.LBB5_76:                               # %.loopexit
.Ltmp20:
.LBB5_77:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_79
# BB#78:
	callq	_ZdaPv
.LBB5_79:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_81
# BB#80:
	callq	_ZdaPv
.LBB5_81:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKcRNS1_9CFileInfoE, .Lfunc_end5-_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKcRNS1_9CFileInfoE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp9-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp10         #   Call between .Ltmp10 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin1   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Lfunc_end5-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL14filter_patternPKcS0_i,@function
_ZL14filter_patternPKcS0_i:             # @_ZL14filter_patternPKcS0_i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB6_11
# BB#1:                                 # %.lr.ph.preheader
	movb	(%r14), %bpl
	testb	%bpl, %bpl
	je	.LBB6_11
.LBB6_2:                                # %.lr.ph.split.split.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	incq	%r14
	movq	%rsi, %rbx
	jmp	.LBB6_4
	.p2align	4, 0x90
.LBB6_3:                                # %tailrecurse
                                        #   in Loop: Header=BB6_4 Depth=2
	incq	%rbx
.LBB6_4:                                # %.lr.ph.split.split
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %ecx
	movsbl	%cl, %edx
	cmpl	$42, %edx
	jne	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZL14filter_patternPKcS0_i
	testl	%eax, %eax
	je	.LBB6_3
	jmp	.LBB6_15
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph.split.split
                                        #   in Loop: Header=BB6_2 Depth=1
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.LBB6_16
# BB#7:                                 # %.lr.ph.split.split
                                        #   in Loop: Header=BB6_2 Depth=1
	cmpl	$63, %edx
	je	.LBB6_9
# BB#8:                                 # %.us-lcssa
                                        #   in Loop: Header=BB6_2 Depth=1
	cmpb	%bpl, %cl
	jne	.LBB6_16
.LBB6_9:                                # %tailrecurse.outer.backedge
                                        #   in Loop: Header=BB6_2 Depth=1
	leaq	1(%rbx), %rsi
	movb	(%r14), %bpl
	testb	%bpl, %bpl
	jne	.LBB6_2
# BB#10:                                # %tailrecurse.outer._crit_edge.loopexit
	incq	%rbx
	movq	%rbx, %rsi
.LBB6_11:                               # %tailrecurse.outer._crit_edge
	testq	%rsi, %rsi
	je	.LBB6_15
	.p2align	4, 0x90
.LBB6_12:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %ecx
	incq	%rsi
	cmpb	$42, %cl
	je	.LBB6_12
# BB#13:
	xorl	%eax, %eax
	testb	%cl, %cl
	sete	%al
	jmp	.LBB6_16
.LBB6_15:
	movl	$1, %eax
.LBB6_16:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZL14filter_patternPKcS0_i, .Lfunc_end6-_ZL14filter_patternPKcS0_i
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.byte	115                     # 0x73
	.byte	116                     # 0x74
	.byte	97                      # 0x61
	.byte	116                     # 0x74
	.byte	32                      # 0x20
	.byte	101                     # 0x65
	.byte	114                     # 0x72
	.byte	114                     # 0x72
	.byte	111                     # 0x6f
	.byte	114                     # 0x72
	.byte	32                      # 0x20
	.byte	102                     # 0x66
	.byte	111                     # 0x6f
	.byte	114                     # 0x72
	.byte	32                      # 0x20
	.byte	0                       # 0x0
	.text
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKcS5_,@function
_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKcS5_: # @_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKcS5_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$1048, %rsp             # imm = 0x418
.Lcfi43:
	.cfi_def_cfa_offset 1104
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r12
	leaq	2(%rbx,%r12), %rax
	cmpq	$1024, %rax             # imm = 0x400
	jae	.LBB7_52
# BB#1:
	leaq	16(%rsp), %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	xorl	%r13d, %r13d
	testq	%rbx, %rbx
	je	.LBB7_2
# BB#3:
	leaq	-1(%rbx), %rax
	cmpb	$47, 15(%rsp,%rbx)
	cmovneq	%rbx, %rax
	jmp	.LBB7_4
.LBB7_2:
	xorl	%eax, %eax
.LBB7_4:
	movb	$47, 16(%rsp,%rax)
	leaq	17(%rsp,%rax), %rdi
	incq	%r12
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movl	$0, 48(%r14)
	movq	40(%r14), %rax
	movb	$0, (%rax)
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB7_5:                                # =>This Inner Loop Header: Depth=1
	incl	%r13d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB7_5
# BB#6:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	-1(%r13), %r12d
	movl	52(%r14), %ebp
	cmpl	%r13d, %ebp
	jne	.LBB7_8
# BB#7:                                 # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	40(%r14), %rbx
	jmp	.LBB7_32
.LBB7_8:
	movslq	%r13d, %rax
	cmpl	$-1, %r12d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB7_31
# BB#9:                                 # %.preheader.i.i
	movslq	48(%r14), %r8
	testq	%r8, %r8
	movq	40(%r14), %rdi
	jle	.LBB7_29
# BB#10:                                # %.lr.ph.preheader.i.i
	cmpl	$31, %r8d
	jbe	.LBB7_11
# BB#18:                                # %min.iters.checked
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB7_11
# BB#19:                                # %vector.memcheck
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB7_21
# BB#20:                                # %vector.memcheck
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB7_21
.LBB7_11:
	xorl	%ecx, %ecx
.LBB7_12:                               # %.lr.ph.i.i.preheader
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB7_15
# BB#13:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB7_14:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB7_14
.LBB7_15:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB7_30
# BB#16:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB7_17:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB7_17
	jmp	.LBB7_30
.LBB7_29:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB7_31
.LBB7_30:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB7_31:                               # %._crit_edge17.i.i
	movq	%rbx, 40(%r14)
	movslq	48(%r14), %rax
	movb	$0, (%rbx,%rax)
	movl	%r13d, 52(%r14)
	.p2align	4, 0x90
.LBB7_32:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15), %eax
	incq	%r15
	movb	%al, (%rbx)
	incq	%rbx
	testb	%al, %al
	jne	.LBB7_32
# BB#33:                                # %_ZN11CStringBaseIcEaSEPKc.exit
	movl	%r12d, 48(%r14)
	leaq	16(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
	testl	%eax, %eax
	je	.LBB7_50
# BB#34:                                # %_Z11MyStringLenIcEiPKT_.exit.i35
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%rsp)
	movl	$16, 12(%rsp)
	movaps	.LCPI7_0(%rip), %xmm0   # xmm0 = [115,116,97,116,32,101,114,114,111,114,32,102,111,114,32,0]
	movups	%xmm0, (%rax)
	movl	$15, 8(%rsp)
.Ltmp24:
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp25:
# BB#35:
.Ltmp26:
	movq	%rsp, %rdi
	movl	$.L.str.5, %esi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp27:
# BB#36:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
.Ltmp28:
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp29:
# BB#37:
.Ltmp30:
	movq	%rsp, %rdi
	movl	$.L.str.6, %esi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp31:
# BB#38:
	movl	$16, %edi
	callq	__cxa_allocate_exception
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%rsp), %rax
	leaq	1(%rax), %rbp
	testl	%ebp, %ebp
	je	.LBB7_39
# BB#40:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbp, %rdi
	cmovlq	%rax, %rdi
.Ltmp32:
	callq	_Znam
.Ltmp33:
# BB#41:                                # %.noexc
	movq	%rax, (%rbx)
	movb	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB7_42
.LBB7_50:
	xorl	%eax, %eax
	addq	$1048, %rsp             # imm = 0x418
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_39:
	xorl	%eax, %eax
.LBB7_42:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i39
	movq	(%rsp), %rcx
	.p2align	4, 0x90
.LBB7_43:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB7_43
# BB#44:
	movl	8(%rsp), %eax
	movl	%eax, 8(%rbx)
.Ltmp35:
	movl	$_ZTI11CStringBaseIcE, %esi
	movl	$_ZN11CStringBaseIcED2Ev, %edx
	movq	%rbx, %rdi
	callq	__cxa_throw
.Ltmp36:
# BB#51:
.LBB7_21:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB7_22
# BB#23:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_24:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%rbx,%rbp)
	movups	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB7_24
	jmp	.LBB7_25
.LBB7_22:
	xorl	%ebp, %ebp
.LBB7_25:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB7_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
	.p2align	4, 0x90
.LBB7_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB7_27
.LBB7_28:                               # %middle.block
	cmpq	%rcx, %r8
	jne	.LBB7_12
	jmp	.LBB7_30
.LBB7_52:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.3, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB7_46:
.Ltmp34:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	__cxa_free_exception
	jmp	.LBB7_47
.LBB7_45:
.Ltmp37:
	movq	%rax, %r14
.LBB7_47:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_49
# BB#48:
	callq	_ZdaPv
.LBB7_49:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKcS5_, .Lfunc_end7-_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKcS5_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp24-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp24         #   Call between .Ltmp24 and .Ltmp31
	.long	.Ltmp37-.Lfunc_begin2   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin2   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin2   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Lfunc_end7-.Ltmp36     #   Call between .Ltmp36 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKwRNS1_10CFileInfoWE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKwRNS1_10CFileInfoWE,@function
_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKwRNS1_10CFileInfoWE: # @_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKwRNS1_10CFileInfoWE
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 144
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB8_5
# BB#1:
	callq	closedir
	testl	%eax, %eax
	je	.LBB8_4
# BB#2:
	xorl	%ebp, %ebp
	jmp	.LBB8_3
.LBB8_4:
	movq	$0, (%r12)
.LBB8_5:
	leaq	72(%rsp), %r14
	movq	$0, 80(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 72(%rsp)
	movb	$0, (%rax)
	movl	$4, 84(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_6:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB8_6
# BB#7:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbp), %r13d
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp38:
	callq	_Znam
.Ltmp39:
# BB#8:                                 # %.noexc
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	%r13d, 12(%rsp)
	.p2align	4, 0x90
.LBB8_9:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_9
# BB#10:
	movl	%ebp, 8(%rsp)
.Ltmp41:
	leaq	16(%rsp), %rdi
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp42:
# BB#11:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_13
# BB#12:
	callq	_ZdaPv
.LBB8_13:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	16(%rsp), %rsi
.Ltmp44:
	leaq	32(%rsp), %rdx
	movq	%r12, %rdi
	callq	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKcRNS1_9CFileInfoE
	movb	%al, %bpl
.Ltmp45:
# BB#14:
	testb	%bpl, %bpl
	je	.LBB8_29
# BB#15:
	movl	64(%rsp), %eax
	movl	%eax, 32(%r15)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 16(%r15)
	movaps	32(%rsp), %xmm0
	movups	%xmm0, (%r15)
	movb	68(%rsp), %al
	movb	%al, 36(%r15)
.Ltmp47:
	movq	%rsp, %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp48:
# BB#16:                                # %_Z16GetUnicodeStringRK11CStringBaseIcEj.exit
	leaq	40(%r15), %rax
	cmpq	%rax, %rbx
	je	.LBB8_17
# BB#18:
	movl	$0, 48(%r15)
	movq	40(%r15), %rbx
	movl	$0, (%rbx)
	movslq	8(%rsp), %r14
	incq	%r14
	movl	52(%r15), %r13d
	cmpl	%r13d, %r14d
	je	.LBB8_24
# BB#19:
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp50:
	callq	_Znam
	movq	%rax, %r12
.Ltmp51:
# BB#20:                                # %.noexc22
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB8_23
# BB#21:                                # %.noexc22
	testl	%r13d, %r13d
	jle	.LBB8_23
# BB#22:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%r15), %rax
.LBB8_23:                               # %._crit_edge16.i.i
	movq	%r12, 40(%r15)
	movl	$0, (%r12,%rax,4)
	movl	%r14d, 52(%r15)
	movq	%r12, %rbx
.LBB8_24:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i19
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_25:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_25
# BB#26:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%rsp), %eax
	movl	%eax, 48(%r15)
	testq	%rdi, %rdi
	jne	.LBB8_28
	jmp	.LBB8_29
.LBB8_17:                               # %_Z16GetUnicodeStringRK11CStringBaseIcEj.exit._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_29
.LBB8_28:
	callq	_ZdaPv
.LBB8_29:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_31
# BB#30:
	callq	_ZdaPv
.LBB8_31:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_3
# BB#32:
	callq	_ZdaPv
.LBB8_3:                                # %_ZN8NWindows5NFile5NFind9CFindFile5CloseEv.exit
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_36:
.Ltmp52:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_40
# BB#37:
	callq	_ZdaPv
	jmp	.LBB8_40
.LBB8_35:
.Ltmp49:
	jmp	.LBB8_39
.LBB8_38:
.Ltmp46:
.LBB8_39:
	movq	%rax, %rbx
.LBB8_40:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB8_42
	jmp	.LBB8_43
.LBB8_34:
.Ltmp43:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_43
.LBB8_42:
	callq	_ZdaPv
	jmp	.LBB8_43
.LBB8_33:
.Ltmp40:
	movq	%rax, %rbx
.LBB8_43:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_45
# BB#44:
	callq	_ZdaPv
.LBB8_45:                               # %_ZN8NWindows5NFile5NFind9CFileInfoD2Ev.exit27
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKwRNS1_10CFileInfoWE, .Lfunc_end8-_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKwRNS1_10CFileInfoWE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp38-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin3   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin3   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin3   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin3   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin3   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Lfunc_end8-.Ltmp51     #   Call between .Ltmp51 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_9CFileInfoE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_9CFileInfoE,@function
_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_9CFileInfoE: # @_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_9CFileInfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 32
.Lcfi66:
	.cfi_offset %rbx, -32
.Lcfi67:
	.cfi_offset %r14, -24
.Lcfi68:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	jne	.LBB9_1
# BB#7:
	callq	__errno_location
	movl	$9, (%rax)
	jmp	.LBB9_5
	.p2align	4, 0x90
.LBB9_3:                                # %._crit_edge
                                        #   in Loop: Header=BB9_1 Depth=1
	movq	(%r15), %rdi
.LBB9_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	callq	readdir64
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB9_4
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB9_1 Depth=1
	addq	$19, %rbx
	movq	8(%r15), %rsi
	movq	%rbx, %rdi
	callq	_ZL14filter_patternPKcS0_i
	cmpl	$1, %eax
	jne	.LBB9_3
# BB#8:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKcS5_
	testl	%eax, %eax
	sete	%al
	jmp	.LBB9_6
.LBB9_4:                                # %._crit_edge8
	callq	__errno_location
	movl	$1048867, (%rax)        # imm = 0x100123
.LBB9_5:
	xorl	%eax, %eax
.LBB9_6:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_9CFileInfoE, .Lfunc_end9-_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_9CFileInfoE
	.cfi_endproc

	.globl	_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_10CFileInfoWE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_10CFileInfoWE,@function
_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_10CFileInfoWE: # @_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_10CFileInfoWE
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 48
	subq	$80, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 128
.Lcfi75:
	.cfi_offset %rbx, -48
.Lcfi76:
	.cfi_offset %r12, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	$0, 64(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 56(%rsp)
	movb	$0, (%rax)
	movl	$4, 68(%rsp)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_33
# BB#1:
	leaq	56(%rsp), %r14
	jmp	.LBB10_2
	.p2align	4, 0x90
.LBB10_5:                               # %._crit_edge.i
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	(%r12), %rdi
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
.Ltmp53:
	callq	readdir64
	movq	%rax, %rbx
.Ltmp54:
# BB#3:                                 # %.noexc
                                        #   in Loop: Header=BB10_2 Depth=1
	testq	%rbx, %rbx
	je	.LBB10_6
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	addq	$19, %rbx
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	callq	_ZL14filter_patternPKcS0_i
	cmpl	$1, %eax
	jne	.LBB10_5
# BB#7:
	movq	24(%r12), %rsi
.Ltmp56:
	leaq	16(%rsp), %rdi
	movq	%rbx, %rdx
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKcS5_
.Ltmp57:
# BB#8:                                 # %_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_9CFileInfoE.exit
	testl	%eax, %eax
	jne	.LBB10_34
# BB#9:
	movl	48(%rsp), %eax
	movl	%eax, 32(%r15)
	movaps	32(%rsp), %xmm0
	movups	%xmm0, 16(%r15)
	movaps	16(%rsp), %xmm0
	movups	%xmm0, (%r15)
	movb	52(%rsp), %al
	movb	%al, 36(%r15)
.Ltmp59:
	movq	%rsp, %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp60:
# BB#10:                                # %_Z16GetUnicodeStringRK11CStringBaseIcEj.exit
	leaq	40(%r15), %rax
	cmpq	%rax, %rbx
	je	.LBB10_11
# BB#12:
	movl	$0, 48(%r15)
	movq	40(%r15), %rbx
	movl	$0, (%rbx)
	movslq	8(%rsp), %r14
	incq	%r14
	movl	52(%r15), %ebp
	cmpl	%ebp, %r14d
	je	.LBB10_18
# BB#13:
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp62:
	callq	_Znam
	movq	%rax, %r12
.Ltmp63:
# BB#14:                                # %.noexc16
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB10_17
# BB#15:                                # %.noexc16
	testl	%ebp, %ebp
	jle	.LBB10_17
# BB#16:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%r15), %rax
.LBB10_17:                              # %._crit_edge16.i.i
	movq	%r12, 40(%r15)
	movl	$0, (%r12,%rax,4)
	movl	%r14d, 52(%r15)
	movq	%r12, %rbx
.LBB10_18:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB10_19:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB10_19
# BB#20:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%rsp), %eax
	movl	%eax, 48(%r15)
	testq	%rdi, %rdi
	jne	.LBB10_22
	jmp	.LBB10_23
.LBB10_33:
	callq	__errno_location
	movl	$9, (%rax)
	jmp	.LBB10_34
.LBB10_6:
	callq	__errno_location
	movl	$1048867, (%rax)        # imm = 0x100123
.LBB10_34:                              # %_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_9CFileInfoE.exit.thread
	xorl	%ebx, %ebx
.LBB10_35:                              # %_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_9CFileInfoE.exit.thread
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_37
# BB#36:
	callq	_ZdaPv
.LBB10_37:                              # %_ZN8NWindows5NFile5NFind9CFileInfoD2Ev.exit
	movl	%ebx, %eax
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_11:                              # %_Z16GetUnicodeStringRK11CStringBaseIcEj.exit._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_23
.LBB10_22:
	callq	_ZdaPv
.LBB10_23:                              # %_ZN11CStringBaseIwED2Ev.exit
	movb	$1, %bl
	jmp	.LBB10_35
.LBB10_31:
.Ltmp64:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_27
# BB#32:
	callq	_ZdaPv
	jmp	.LBB10_27
.LBB10_30:
.Ltmp61:
	jmp	.LBB10_26
.LBB10_25:                              # %.loopexit.split-lp
.Ltmp58:
	jmp	.LBB10_26
.LBB10_24:                              # %.loopexit
.Ltmp55:
.LBB10_26:
	movq	%rax, %rbx
.LBB10_27:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_29
# BB#28:
	callq	_ZdaPv
.LBB10_29:                              # %_ZN8NWindows5NFile5NFind9CFileInfoD2Ev.exit18
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_10CFileInfoWE, .Lfunc_end10-_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_10CFileInfoWE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp53-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin4   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin4   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin4   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin4   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Lfunc_end10-.Ltmp63    #   Call between .Ltmp63 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.zero	16
	.text
	.globl	_ZN8NWindows5NFile5NFind9CFileInfo4FindEPKc
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind9CFileInfo4FindEPKc,@function
_ZN8NWindows5NFile5NFind9CFileInfo4FindEPKc: # @_ZN8NWindows5NFile5NFind9CFileInfo4FindEPKc
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 80
.Lcfi84:
	.cfi_offset %rbx, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, 8(%rsp)
	movb	$0, (%rbx)
	movl	$4, 20(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rsp)
.Ltmp65:
	movl	$4, %edi
	callq	_Znam
.Ltmp66:
# BB#1:                                 # %_ZN8NWindows5NFile5NFind9CFindFileC2Ev.exit
	movq	%rax, 24(%rsp)
	movb	$0, (%rax)
	movl	$4, 36(%rsp)
.Ltmp68:
	movq	%rsp, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKcRNS1_9CFileInfoE
	movl	%eax, %ebx
.Ltmp69:
# BB#2:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_5
# BB#3:
	callq	closedir
	testl	%eax, %eax
	jne	.LBB11_5
# BB#4:
	movq	$0, (%rsp)
.LBB11_5:                               # %_ZN8NWindows5NFile5NFind9CFindFile5CloseEv.exit.i
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_7
# BB#6:
	callq	_ZdaPv
.LBB11_7:                               # %_ZN11CStringBaseIcED2Ev.exit.i3
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_9
# BB#8:
	callq	_ZdaPv
.LBB11_9:                               # %_ZN8NWindows5NFile5NFind9CFindFileD2Ev.exit
	movl	%ebx, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB11_10:
.Ltmp70:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_13
# BB#11:
	callq	closedir
	testl	%eax, %eax
	jne	.LBB11_13
# BB#12:
	movq	$0, (%rsp)
.LBB11_13:                              # %_ZN8NWindows5NFile5NFind9CFindFile5CloseEv.exit.i4
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_15
# BB#14:
	callq	_ZdaPv
.LBB11_15:                              # %_ZN11CStringBaseIcED2Ev.exit.i5
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB11_16
	jmp	.LBB11_17
.LBB11_18:                              # %_ZN11CStringBaseIcED2Ev.exit.i
.Ltmp67:
	movq	%rax, %r14
	movq	%rbx, %rdi
.LBB11_16:
	callq	_ZdaPv
.LBB11_17:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN8NWindows5NFile5NFind9CFileInfo4FindEPKc, .Lfunc_end11-_ZN8NWindows5NFile5NFind9CFileInfo4FindEPKc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp65-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin5   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin5   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Lfunc_end11-.Ltmp69    #   Call between .Ltmp69 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.zero	16
	.text
	.globl	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw,@function
_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw: # @_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 80
.Lcfi91:
	.cfi_offset %rbx, -32
.Lcfi92:
	.cfi_offset %r14, -24
.Lcfi93:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, 8(%rsp)
	movb	$0, (%rbx)
	movl	$4, 20(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rsp)
.Ltmp71:
	movl	$4, %edi
	callq	_Znam
.Ltmp72:
# BB#1:                                 # %_ZN8NWindows5NFile5NFind9CFindFileC2Ev.exit
	movq	%rax, 24(%rsp)
	movb	$0, (%rax)
	movl	$4, 36(%rsp)
.Ltmp74:
	movq	%rsp, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKwRNS1_10CFileInfoWE
	movl	%eax, %ebx
.Ltmp75:
# BB#2:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_5
# BB#3:
	callq	closedir
	testl	%eax, %eax
	jne	.LBB12_5
# BB#4:
	movq	$0, (%rsp)
.LBB12_5:                               # %_ZN8NWindows5NFile5NFind9CFindFile5CloseEv.exit.i
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_7
# BB#6:
	callq	_ZdaPv
.LBB12_7:                               # %_ZN11CStringBaseIcED2Ev.exit.i3
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_9
# BB#8:
	callq	_ZdaPv
.LBB12_9:                               # %_ZN8NWindows5NFile5NFind9CFindFileD2Ev.exit
	movl	%ebx, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB12_10:
.Ltmp76:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_13
# BB#11:
	callq	closedir
	testl	%eax, %eax
	jne	.LBB12_13
# BB#12:
	movq	$0, (%rsp)
.LBB12_13:                              # %_ZN8NWindows5NFile5NFind9CFindFile5CloseEv.exit.i4
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_15
# BB#14:
	callq	_ZdaPv
.LBB12_15:                              # %_ZN11CStringBaseIcED2Ev.exit.i5
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB12_16
	jmp	.LBB12_17
.LBB12_18:                              # %_ZN11CStringBaseIcED2Ev.exit.i
.Ltmp73:
	movq	%rax, %r14
	movq	%rbx, %rdi
.LBB12_16:
	callq	_ZdaPv
.LBB12_17:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw, .Lfunc_end12-_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp71-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp71
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin6   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin6   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp75    #   Call between .Ltmp75 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.zero	16
	.text
	.globl	_ZN8NWindows5NFile5NFind8FindFileEPKcRNS1_9CFileInfoE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind8FindFileEPKcRNS1_9CFileInfoE,@function
_ZN8NWindows5NFile5NFind8FindFileEPKcRNS1_9CFileInfoE: # @_ZN8NWindows5NFile5NFind8FindFileEPKcRNS1_9CFileInfoE
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi97:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi99:
	.cfi_def_cfa_offset 96
.Lcfi100:
	.cfi_offset %rbx, -48
.Lcfi101:
	.cfi_offset %r12, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movq	$0, 40(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, 32(%rsp)
	movb	$0, (%rbx)
	movl	$4, 44(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp77:
	movl	$4, %edi
	callq	_Znam
.Ltmp78:
# BB#1:
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	$4, 12(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rbp,%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB13_2
# BB#3:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leaq	-1(%rbx), %r14
	movslq	%ebx, %rax
	cmpl	$-1, %r14d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp80:
	callq	_Znam
.Ltmp81:
# BB#4:                                 # %.noexc
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	%ebx, 28(%rsp)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_5:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp,%rcx), %edx
	movb	%dl, (%rax,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB13_5
# BB#6:
	movl	%r14d, 24(%rsp)
.Ltmp83:
	leaq	16(%rsp), %rdi
	leaq	32(%rsp), %rsi
	movq	%rsp, %rbx
	movq	%rbx, %rdx
	callq	_Z21my_windows_split_pathRK11CStringBaseIcERS0_S3_
.Ltmp84:
# BB#7:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_9
# BB#8:
	callq	_ZdaPv
.LBB13_9:                               # %_ZN11CStringBaseIcED2Ev.exit
	cmpb	$99, (%rbp)
	jne	.LBB13_11
# BB#10:
	leaq	2(%rbp), %rax
	cmpb	$58, 1(%rbp)
	cmoveq	%rax, %rbp
.LBB13_11:                              # %_ZL16nameWindowToUnixPKc.exit
.Ltmp86:
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
	movl	%eax, %r14d
.Ltmp87:
# BB#12:
	leaq	40(%r15), %rax
	cmpq	%rax, %rbx
	je	.LBB13_43
# BB#13:
	movl	$0, 48(%r15)
	movq	40(%r15), %rcx
	movb	$0, (%rcx)
	movslq	8(%rsp), %rcx
	leaq	1(%rcx), %r12
	movl	52(%r15), %ebx
	cmpl	%ebx, %r12d
	jne	.LBB13_15
# BB#14:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	(%rax), %rbp
	jmp	.LBB13_40
.LBB13_15:
	cmpl	$-1, %ecx
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
.Ltmp89:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp90:
# BB#16:                                # %.noexc17
	testl	%ebx, %ebx
	jle	.LBB13_39
# BB#17:                                # %.preheader.i.i
	movslq	48(%r15), %rax
	testq	%rax, %rax
	movq	40(%r15), %rdi
	jle	.LBB13_37
# BB#18:                                # %.lr.ph.preheader.i.i
	cmpl	$31, %eax
	jbe	.LBB13_19
# BB#26:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB13_19
# BB#27:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB13_29
# BB#28:                                # %vector.memcheck
	leaq	(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB13_29
.LBB13_19:
	xorl	%ecx, %ecx
.LBB13_20:                              # %.lr.ph.i.i.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB13_23
# BB#21:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB13_22:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB13_22
.LBB13_23:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB13_38
# BB#24:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB13_25:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB13_25
	jmp	.LBB13_38
.LBB13_37:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB13_39
.LBB13_38:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB13_39:                              # %._crit_edge17.i.i
	movq	%rbp, 40(%r15)
	movslq	48(%r15), %rax
	movb	$0, (%rbp,%rax)
	movl	%r12d, 52(%r15)
.LBB13_40:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i14
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB13_41:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB13_41
# BB#42:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
	movl	8(%rsp), %eax
	movl	%eax, 48(%r15)
.LBB13_43:                              # %_ZN11CStringBaseIcEaSERKS0_.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_45
# BB#44:
	callq	_ZdaPv
.LBB13_45:                              # %_ZN11CStringBaseIcED2Ev.exit18
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_47
# BB#46:
	callq	_ZdaPv
.LBB13_47:                              # %_ZN11CStringBaseIcED2Ev.exit19
	testl	%r14d, %r14d
	sete	%al
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_29:                              # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB13_30
# BB#31:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_32:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%rbp,%rbx)
	movups	%xmm1, 16(%rbp,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB13_32
	jmp	.LBB13_33
.LBB13_30:
	xorl	%ebx, %ebx
.LBB13_33:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB13_36
# BB#34:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%rbp,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB13_35:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB13_35
.LBB13_36:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB13_20
	jmp	.LBB13_38
.LBB13_53:
.Ltmp91:
	jmp	.LBB13_54
.LBB13_52:
.Ltmp88:
	jmp	.LBB13_54
.LBB13_50:
.Ltmp85:
	movq	%rax, %rbp
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_55
# BB#51:
	callq	_ZdaPv
	jmp	.LBB13_55
.LBB13_49:
.Ltmp82:
.LBB13_54:
	movq	%rax, %rbp
.LBB13_55:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_57
# BB#56:
	callq	_ZdaPv
.LBB13_57:
	movq	32(%rsp), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_58
	jmp	.LBB13_59
.LBB13_48:                              # %.thread
.Ltmp79:
	movq	%rax, %rbp
.LBB13_58:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB13_59:                              # %_ZN11CStringBaseIcED2Ev.exit11
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN8NWindows5NFile5NFind8FindFileEPKcRNS1_9CFileInfoE, .Lfunc_end13-_ZN8NWindows5NFile5NFind8FindFileEPKcRNS1_9CFileInfoE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp77-.Lfunc_begin7   #   Call between .Lfunc_begin7 and .Ltmp77
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin7   #     jumps to .Ltmp79
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin7   #     jumps to .Ltmp82
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin7   #     jumps to .Ltmp85
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin7   # >> Call Site 5 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin7   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin7   # >> Call Site 6 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin7   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin7   # >> Call Site 7 <<
	.long	.Lfunc_end13-.Ltmp90    #   Call between .Ltmp90 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc,@function
_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc: # @_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 16
	subq	$144, %rsp
.Lcfi106:
	.cfi_def_cfa_offset 160
.Lcfi107:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, global_use_lstat(%rip)
	je	.LBB14_2
# BB#1:
	movq	%rsp, %rdx
	movl	$1, %edi
	callq	__lxstat64
	jmp	.LBB14_3
.LBB14_2:
	movq	%rsp, %rdx
	movl	$1, %edi
	callq	__xstat64
.LBB14_3:
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	testl	%eax, %eax
	jne	.LBB14_5
# BB#4:
	movl	24(%rsp), %eax
	movl	%eax, %ecx
	andl	$61440, %ecx            # imm = 0xF000
	cmpl	$16384, %ecx            # imm = 0x4000
	movl	$16, %ecx
	movl	$32, %edx
	cmovel	%ecx, %edx
	movl	%eax, %ecx
	shrl	$7, %ecx
	andl	$1, %ecx
	shll	$16, %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	xorl	$32769, %eax            # imm = 0x8001
	movl	%eax, 32(%rbx)
	movl	104(%rsp), %edi
	leaq	8(%rbx), %rsi
	callq	_Z29RtlSecondsSince1970ToFileTimejP9_FILETIME
	movl	88(%rsp), %edi
	leaq	24(%rbx), %rsi
	callq	_Z29RtlSecondsSince1970ToFileTimejP9_FILETIME
	movl	72(%rsp), %edi
	leaq	16(%rbx), %rsi
	callq	_Z29RtlSecondsSince1970ToFileTimejP9_FILETIME
	movb	$0, 36(%rbx)
	movl	$61440, %ecx            # imm = 0xF000
	andl	24(%rsp), %ecx
	xorl	%eax, %eax
	cmpl	$16384, %ecx            # imm = 0x4000
	movq	48(%rsp), %rcx
	cmoveq	%rax, %rcx
	movq	%rcx, (%rbx)
.LBB14_5:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$144, %rsp
	popq	%rbx
	retq
.Lfunc_end14:
	.size	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc, .Lfunc_end14-_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.zero	16
	.text
	.globl	_ZN8NWindows5NFile5NFind8FindFileEPKwRNS1_10CFileInfoWE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind8FindFileEPKwRNS1_10CFileInfoWE,@function
_ZN8NWindows5NFile5NFind8FindFileEPKwRNS1_10CFileInfoWE: # @_ZN8NWindows5NFile5NFind8FindFileEPKwRNS1_10CFileInfoWE
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi114:
	.cfi_def_cfa_offset 176
.Lcfi115:
	.cfi_offset %rbx, -56
.Lcfi116:
	.cfi_offset %r12, -48
.Lcfi117:
	.cfi_offset %r13, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB15_1:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB15_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbp), %eax
	movslq	%eax, %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 48(%rsp)
	movl	$0, (%rax)
	movl	%r15d, 60(%rsp)
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB15_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB15_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%ebp, 56(%rsp)
.Ltmp92:
	leaq	104(%rsp), %rdi
	leaq	48(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp93:
# BB#5:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_7
# BB#6:
	callq	_ZdaPv
.LBB15_7:                               # %_ZN11CStringBaseIwED2Ev.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rsp)
.Ltmp95:
	movl	$4, %edi
	callq	_Znam
.Ltmp96:
# BB#8:
	movq	%rax, 88(%rsp)
	movb	$0, (%rax)
	movl	$4, 100(%rsp)
	movq	104(%rsp), %rsi
	cmpb	$99, (%rsi)
	jne	.LBB15_10
# BB#9:
	leaq	2(%rsi), %rax
	cmpb	$58, 1(%rsi)
	cmoveq	%rax, %rsi
.LBB15_10:                              # %_ZL16nameWindowToUnixPKc.exit
.Ltmp98:
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
	movl	%eax, %r15d
.Ltmp99:
# BB#11:
	testl	%r15d, %r15d
	je	.LBB15_32
# BB#12:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp101:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp102:
# BB#13:
	movq	%r12, (%rsp)
	movb	$0, (%r12)
	movl	$4, 12(%rsp)
	xorl	%eax, %eax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB15_14:                              # =>This Inner Loop Header: Depth=1
	addq	%rcx, %rax
	cmpl	$0, (%rdx)
	leaq	4(%rdx), %rdx
	jne	.LBB15_14
# BB#15:                                # %_Z11MyStringLenIwEiPKT_.exit.i30
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp103:
	callq	_Znam
	movq	%rax, %r14
.Ltmp104:
# BB#16:                                # %.noexc
	movq	%r13, %rbp
	movl	$0, (%r14)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB15_17:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i33
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rax), %ecx
	movl	%ecx, (%r14,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB15_17
# BB#18:
	movl	$0, 8(%rsp)
	movb	$0, (%r12)
.Ltmp106:
	movl	$1, %edi
	callq	_Znam
	movq	%rax, %r13
.Ltmp107:
# BB#19:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.preheader
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	%r13, (%rsp)
	movl	$1, 12(%rsp)
	movb	$0, (%r13)
	movl	$0, 8(%rsp)
	movl	(%r14), %eax
	testl	%eax, %eax
	movq	%rbp, %r13
	je	.LBB15_24
# BB#20:                                # %.lr.ph.i.preheader
	movq	%r14, %rbp
	addq	$4, %rbp
	movq	%rsp, %r12
	.p2align	4, 0x90
.LBB15_21:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$255, %eax
	jg	.LBB15_27
# BB#22:                                #   in Loop: Header=BB15_21 Depth=1
.Ltmp109:
	movsbl	%al, %esi
	movq	%r12, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp110:
# BB#23:                                # %.noexc36
                                        #   in Loop: Header=BB15_21 Depth=1
	movl	(%rbp), %eax
	addq	$4, %rbp
	testl	%eax, %eax
	jne	.LBB15_21
.LBB15_24:                              # %.loopexit
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	(%rsp), %rsi
	cmpb	$99, (%rsi)
	jne	.LBB15_26
# BB#25:
	leaq	2(%rsi), %rax
	cmpb	$58, 1(%rsi)
	cmoveq	%rax, %rsi
.LBB15_26:                              # %_ZL16nameWindowToUnixPKc.exit44
.Ltmp112:
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
	movl	%eax, %r15d
.Ltmp113:
	jmp	.LBB15_28
.LBB15_27:                              # %_ZN11CStringBaseIwED2Ev.exit37
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB15_28:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_30
# BB#29:
	callq	_ZdaPv
.LBB15_30:
	testl	%r15d, %r15d
	je	.LBB15_32
# BB#31:
	xorl	%ebx, %ebx
	jmp	.LBB15_57
.LBB15_32:                              # %.thread
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
.Ltmp115:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp116:
# BB#33:
	movq	%rbp, 32(%rsp)
	movl	$0, (%rbp)
	movl	$4, 44(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
.Ltmp118:
	movl	$16, %edi
	callq	_Znam
.Ltmp119:
# BB#34:
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	$4, 28(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB15_35:                              # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB15_35
# BB#36:                                # %_Z11MyStringLenIwEiPKT_.exit.i56
	leal	1(%rbp), %r14d
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp121:
	callq	_Znam
.Ltmp122:
# BB#37:                                # %.noexc60
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	%r14d, 12(%rsp)
	.p2align	4, 0x90
.LBB15_38:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i59
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB15_38
# BB#39:
	movl	%ebp, 8(%rsp)
.Ltmp124:
	movq	%rsp, %rdi
	leaq	32(%rsp), %rsi
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdx
	callq	_ZL21my_windows_split_pathRK11CStringBaseIwERS0_S3_
.Ltmp125:
# BB#40:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_42
# BB#41:
	callq	_ZdaPv
.LBB15_42:                              # %_ZN11CStringBaseIwED2Ev.exit62
	movl	80(%rsp), %eax
	movl	%eax, 32(%r13)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, 16(%r13)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, (%r13)
	leaq	40(%r13), %rax
	cmpq	%rax, %rbx
	je	.LBB15_52
# BB#43:
	movl	$0, 48(%r13)
	movq	40(%r13), %rbx
	movl	$0, (%rbx)
	movslq	24(%rsp), %rbp
	incq	%rbp
	movl	52(%r13), %r14d
	cmpl	%r14d, %ebp
	je	.LBB15_49
# BB#44:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp127:
	callq	_Znam
	movq	%rax, %r15
.Ltmp128:
# BB#45:                                # %.noexc68
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB15_48
# BB#46:                                # %.noexc68
	testl	%r14d, %r14d
	jle	.LBB15_48
# BB#47:                                # %._crit_edge.thread.i.i64
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%r13), %rax
.LBB15_48:                              # %._crit_edge16.i.i
	movq	%r15, 40(%r13)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 52(%r13)
	movq	%r15, %rbx
.LBB15_49:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i65
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB15_50:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB15_50
# BB#51:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	24(%rsp), %eax
	movl	%eax, 48(%r13)
	testq	%rdi, %rdi
	jne	.LBB15_53
	jmp	.LBB15_54
.LBB15_52:                              # %_ZN11CStringBaseIwED2Ev.exit62._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_54
.LBB15_53:
	callq	_ZdaPv
.LBB15_54:                              # %_ZN11CStringBaseIwED2Ev.exit69
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_56
# BB#55:
	callq	_ZdaPv
.LBB15_56:                              # %_ZN11CStringBaseIwED2Ev.exit70
	movb	$1, %bl
.LBB15_57:
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_59
# BB#58:
	callq	_ZdaPv
.LBB15_59:                              # %_ZN8NWindows5NFile5NFind9CFileInfoD2Ev.exit
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_61
# BB#60:
	callq	_ZdaPv
.LBB15_61:                              # %_ZN11CStringBaseIcED2Ev.exit74
	movl	%ebx, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_62:
.Ltmp129:
	jmp	.LBB15_67
.LBB15_63:
.Ltmp114:
	jmp	.LBB15_75
.LBB15_64:
.Ltmp126:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_68
# BB#65:
	callq	_ZdaPv
	jmp	.LBB15_68
.LBB15_66:
.Ltmp123:
.LBB15_67:
	movq	%rax, %rbx
.LBB15_68:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_70
# BB#69:
	callq	_ZdaPv
.LBB15_70:
	movq	32(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB15_72
	jmp	.LBB15_85
.LBB15_71:                              # %.thread92
.Ltmp120:
	movq	%rax, %rbx
.LBB15_72:
	movq	%rbp, %rdi
	jmp	.LBB15_84
.LBB15_73:                              # %_ZN11CStringBaseIwED2Ev.exit46.loopexit.split-lp
.Ltmp108:
	jmp	.LBB15_82
.LBB15_74:
.Ltmp105:
.LBB15_75:
	movq	%rax, %rbx
	jmp	.LBB15_83
.LBB15_76:
.Ltmp100:
	jmp	.LBB15_80
.LBB15_77:
.Ltmp97:
	movq	%rax, %rbx
	jmp	.LBB15_87
.LBB15_78:
.Ltmp94:
	movq	%rax, %rbx
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB15_88
	jmp	.LBB15_89
.LBB15_79:
.Ltmp117:
.LBB15_80:
	movq	%rax, %rbx
	jmp	.LBB15_85
.LBB15_81:                              # %_ZN11CStringBaseIwED2Ev.exit46.loopexit
.Ltmp111:
.LBB15_82:                              # %_ZN11CStringBaseIwED2Ev.exit46
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB15_83:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_85
.LBB15_84:
	callq	_ZdaPv
.LBB15_85:
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_87
# BB#86:
	callq	_ZdaPv
.LBB15_87:
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_89
.LBB15_88:
	callq	_ZdaPv
.LBB15_89:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZN8NWindows5NFile5NFind8FindFileEPKwRNS1_10CFileInfoWE, .Lfunc_end15-_ZN8NWindows5NFile5NFind8FindFileEPKwRNS1_10CFileInfoWE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\306\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp92-.Lfunc_begin8   #   Call between .Lfunc_begin8 and .Ltmp92
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin8   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin8   # >> Call Site 3 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin8   #     jumps to .Ltmp97
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin8   # >> Call Site 4 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin8  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp117-.Lfunc_begin8  #     jumps to .Ltmp117
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin8  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp106-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin8  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin8  #     jumps to .Ltmp111
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin8  # >> Call Site 9 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin8  #     jumps to .Ltmp114
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin8  # >> Call Site 10 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin8  #     jumps to .Ltmp117
	.byte	0                       #   On action: cleanup
	.long	.Ltmp118-.Lfunc_begin8  # >> Call Site 11 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin8  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin8  # >> Call Site 12 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin8  #     jumps to .Ltmp123
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin8  # >> Call Site 13 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin8  #     jumps to .Ltmp126
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin8  # >> Call Site 14 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin8  #     jumps to .Ltmp129
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin8  # >> Call Site 15 <<
	.long	.Lfunc_end15-.Ltmp128   #   Call between .Ltmp128 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL21my_windows_split_pathRK11CStringBaseIwERS0_S3_,@function
_ZL21my_windows_split_pathRK11CStringBaseIwERS0_S3_: # @_ZL21my_windows_split_pathRK11CStringBaseIwERS0_S3_
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi124:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi127:
	.cfi_def_cfa_offset 80
.Lcfi128:
	.cfi_offset %rbx, -56
.Lcfi129:
	.cfi_offset %r12, -48
.Lcfi130:
	.cfi_offset %r13, -40
.Lcfi131:
	.cfi_offset %r14, -32
.Lcfi132:
	.cfi_offset %r15, -24
.Lcfi133:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r13
	movslq	8(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB16_16
# BB#1:
	movq	(%r13), %rax
	leaq	(,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	cmpl	$47, -4(%rax,%rdx)
	je	.LBB16_4
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	addq	$-4, %rdx
	jne	.LBB16_2
	jmp	.LBB16_16
.LBB16_4:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	leaq	-4(%rax,%rdx), %r12
	subq	%rax, %r12
	shrq	$2, %r12
	cmpl	$-1, %r12d
	je	.LBB16_16
# BB#5:
	leal	1(%r12), %edx
	cmpl	%edx, %ecx
	jle	.LBB16_37
# BB#6:
	subl	%edx, %ecx
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	movq	%r13, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%r15, %rbx
	je	.LBB16_50
# BB#7:
	movl	$0, 8(%r15)
	movq	(%r15), %rbx
	movl	$0, (%rbx)
	movslq	8(%rsp), %rbp
	incq	%rbp
	movl	12(%r15), %eax
	cmpl	%eax, %ebp
	je	.LBB16_13
# BB#8:
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp133:
	callq	_Znam
.Ltmp134:
# BB#9:                                 # %.noexc
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB16_12
# BB#10:                                # %.noexc
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jle	.LBB16_12
# BB#11:                                # %._crit_edge.thread.i.i65
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	8(%r15), %rcx
.LBB16_12:                              # %._crit_edge16.i.i66
	movq	%rax, (%r15)
	movl	$0, (%rax,%rcx,4)
	movl	%ebp, 12(%r15)
	movq	%rax, %rbx
.LBB16_13:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i67
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_14:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB16_14
# BB#15:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i70
	movl	8(%rsp), %eax
	movl	%eax, 8(%r15)
	movq	%rsp, %rbx
	testq	%rdi, %rdi
	jne	.LBB16_51
	jmp	.LBB16_52
.LBB16_16:                              # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.thread
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movl	12(%r14), %ebp
	cmpl	$2, %ebp
	je	.LBB16_21
# BB#17:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB16_20
# BB#18:
	testl	%ebp, %ebp
	jle	.LBB16_20
# BB#19:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB16_20:                              # %._crit_edge16.i.i
	movq	%r12, (%r14)
	movl	$0, (%r12,%rax,4)
	movl	$2, 12(%r14)
	movq	%r12, %rbx
.LBB16_21:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
	movl	$46, (%rbx)
	movl	$0, 4(%rbx)
	movl	$1, 8(%r14)
	cmpl	$0, 8(%r13)
	je	.LBB16_31
# BB#22:
	cmpq	%r15, %r13
	je	.LBB16_75
# BB#23:
	movl	$0, 8(%r15)
	movq	(%r15), %rbx
	movl	$0, (%rbx)
	movslq	8(%r13), %rbp
	incq	%rbp
	movl	12(%r15), %r12d
	cmpl	%r12d, %ebp
	je	.LBB16_28
# BB#24:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB16_27
# BB#25:
	testl	%r12d, %r12d
	jle	.LBB16_27
# BB#26:                                # %._crit_edge.thread.i.i57
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r15), %rax
.LBB16_27:                              # %._crit_edge16.i.i58
	movq	%r14, (%r15)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 12(%r15)
	movq	%r14, %rbx
.LBB16_28:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i59
	movq	(%r13), %rax
	.p2align	4, 0x90
.LBB16_29:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB16_29
# BB#30:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%r13), %eax
	movl	%eax, 8(%r15)
	jmp	.LBB16_75
.LBB16_31:                              # %_Z11MyStringLenIwEiPKT_.exit.i44
	movl	$0, 8(%r15)
	movq	(%r15), %rbx
	movl	$0, (%rbx)
	movl	12(%r15), %ebp
	cmpl	$2, %ebp
	je	.LBB16_36
# BB#32:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB16_35
# BB#33:
	testl	%ebp, %ebp
	jle	.LBB16_35
# BB#34:                                # %._crit_edge.thread.i.i48
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r15), %rax
.LBB16_35:                              # %._crit_edge16.i.i49
	movq	%r14, (%r15)
	movl	$0, (%r14,%rax,4)
	movl	$2, 12(%r15)
	movq	%r14, %rbx
.LBB16_36:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i52.preheader
	movl	$46, (%rbx)
	movl	$0, 4(%rbx)
	movl	$1, 8(%r15)
	jmp	.LBB16_75
.LBB16_37:                              # %.preheader
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.LBB16_44
# BB#38:                                # %.lr.ph.preheader
	movl	$-1, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB16_39:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$47, %edx
	cmovnel	%esi, %ecx
	movl	4(%rax,%rsi,4), %edx
	incq	%rsi
	testl	%edx, %edx
	jne	.LBB16_39
# BB#40:                                # %._crit_edge
	cmpl	$-1, %ecx
	je	.LBB16_44
# BB#41:
	incl	%ecx
	movq	%rsp, %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp130:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZL21my_windows_split_pathRK11CStringBaseIwERS0_S3_
.Ltmp131:
# BB#42:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB16_74
	jmp	.LBB16_75
.LBB16_44:                              # %_Z11MyStringLenIwEiPKT_.exit.i101
	movl	$0, 8(%r15)
	movq	(%r15), %rbx
	movl	$0, (%rbx)
	movl	12(%r15), %ebp
	cmpl	$2, %ebp
	je	.LBB16_49
# BB#45:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB16_48
# BB#46:
	testl	%ebp, %ebp
	jle	.LBB16_48
# BB#47:                                # %._crit_edge.thread.i.i105
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r15), %rax
.LBB16_48:                              # %._crit_edge16.i.i106
	movq	%r12, (%r15)
	movl	$0, (%r12,%rax,4)
	movl	$2, 12(%r15)
	movq	%r12, %rbx
.LBB16_49:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i109.preheader
	movl	$47, (%rbx)
	movl	$0, 4(%rbx)
	movl	$1, 8(%r15)
	jmp	.LBB16_67
.LBB16_50:                              # %._ZN11CStringBaseIwEaSERKS0_.exit71_crit_edge
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB16_52
.LBB16_51:
	callq	_ZdaPv
.LBB16_52:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	(%r13), %rax
	movslq	%r12d, %rcx
	.p2align	4, 0x90
.LBB16_53:                              # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	jle	.LBB16_56
# BB#54:                                #   in Loop: Header=BB16_53 Depth=1
	leaq	-1(%rcx), %rdx
	cmpl	$47, -4(%rax,%rcx,4)
	movq	%rdx, %rcx
	je	.LBB16_53
# BB#55:                                # %.critedge.thread.loopexit
	incl	%edx
	movl	%edx, %ecx
	jmp	.LBB16_57
.LBB16_56:                              # %.critedge
	testl	%ecx, %ecx
	je	.LBB16_67
.LBB16_57:                              # %.critedge.thread
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%r14, %rbx
	je	.LBB16_73
# BB#58:
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movslq	8(%rsp), %rbp
	incq	%rbp
	movl	12(%r14), %r12d
	cmpl	%r12d, %ebp
	je	.LBB16_64
# BB#59:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp136:
	callq	_Znam
	movq	%rax, %r15
.Ltmp137:
# BB#60:                                # %.noexc95
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB16_63
# BB#61:                                # %.noexc95
	testl	%r12d, %r12d
	jle	.LBB16_63
# BB#62:                                # %._crit_edge.thread.i.i89
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB16_63:                              # %._crit_edge16.i.i90
	movq	%r15, (%r14)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 12(%r14)
	movq	%r15, %rbx
.LBB16_64:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i91
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_65:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB16_65
# BB#66:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i94
	movl	8(%rsp), %eax
	movl	%eax, 8(%r14)
	testq	%rdi, %rdi
	jne	.LBB16_74
	jmp	.LBB16_75
.LBB16_67:                              # %_Z11MyStringLenIwEiPKT_.exit.i75
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movl	12(%r14), %ebp
	cmpl	$2, %ebp
	je	.LBB16_72
# BB#68:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB16_71
# BB#69:
	testl	%ebp, %ebp
	jle	.LBB16_71
# BB#70:                                # %._crit_edge.thread.i.i79
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB16_71:                              # %._crit_edge16.i.i80
	movq	%r15, (%r14)
	movl	$0, (%r15,%rax,4)
	movl	$2, 12(%r14)
	movq	%r15, %rbx
.LBB16_72:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i83.preheader
	movl	$47, (%rbx)
	movl	$0, 4(%rbx)
	movl	$1, 8(%r14)
	jmp	.LBB16_75
.LBB16_73:                              # %.critedge.thread._ZN11CStringBaseIwEaSERKS0_.exit96_crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB16_75
.LBB16_74:
	callq	_ZdaPv
.LBB16_75:                              # %_ZN11CStringBaseIwEaSERKS0_.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_76:
.Ltmp138:
	jmp	.LBB16_79
.LBB16_77:
.Ltmp135:
	jmp	.LBB16_79
.LBB16_78:
.Ltmp132:
.LBB16_79:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_81
# BB#80:
	callq	_ZdaPv
.LBB16_81:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZL21my_windows_split_pathRK11CStringBaseIwERS0_S3_, .Lfunc_end16-_ZL21my_windows_split_pathRK11CStringBaseIwERS0_S3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin9-.Lfunc_begin9 # >> Call Site 1 <<
	.long	.Ltmp133-.Lfunc_begin9  #   Call between .Lfunc_begin9 and .Ltmp133
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin9  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp130-.Ltmp134       #   Call between .Ltmp134 and .Ltmp130
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin9  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Ltmp136-.Ltmp131       #   Call between .Ltmp131 and .Ltmp136
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin9  # >> Call Site 6 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin9  #     jumps to .Ltmp138
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin9  # >> Call Site 7 <<
	.long	.Lfunc_end16-.Ltmp137   #   Call between .Ltmp137 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile5NFind13DoesFileExistEPKc
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind13DoesFileExistEPKc,@function
_ZN8NWindows5NFile5NFind13DoesFileExistEPKc: # @_ZN8NWindows5NFile5NFind13DoesFileExistEPKc
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi135:
	.cfi_def_cfa_offset 80
.Lcfi136:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$0, 56(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 48(%rsp)
	movb	$0, (%rax)
	movl	$4, 60(%rsp)
	cmpb	$99, (%rbx)
	jne	.LBB17_2
# BB#1:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB17_2:                               # %_ZL16nameWindowToUnixPKc.exit
.Ltmp139:
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
.Ltmp140:
# BB#3:
	testl	%eax, %eax
	je	.LBB17_5
# BB#4:
	xorl	%ebx, %ebx
	jmp	.LBB17_6
.LBB17_5:
	testb	$16, 40(%rsp)
	sete	%bl
.LBB17_6:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_8
# BB#7:
	callq	_ZdaPv
.LBB17_8:                               # %_ZN8NWindows5NFile5NFind9CFileInfoD2Ev.exit7
	movl	%ebx, %eax
	addq	$64, %rsp
	popq	%rbx
	retq
.LBB17_9:
.Ltmp141:
	movq	%rax, %rbx
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_11
# BB#10:
	callq	_ZdaPv
.LBB17_11:                              # %_ZN8NWindows5NFile5NFind9CFileInfoD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end17:
	.size	_ZN8NWindows5NFile5NFind13DoesFileExistEPKc, .Lfunc_end17-_ZN8NWindows5NFile5NFind13DoesFileExistEPKc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin10-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp139-.Lfunc_begin10 #   Call between .Lfunc_begin10 and .Ltmp139
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp140-.Ltmp139       #   Call between .Ltmp139 and .Ltmp140
	.long	.Ltmp141-.Lfunc_begin10 #     jumps to .Ltmp141
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Lfunc_end17-.Ltmp140   #   Call between .Ltmp140 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile5NFind12DoesDirExistEPKc
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind12DoesDirExistEPKc,@function
_ZN8NWindows5NFile5NFind12DoesDirExistEPKc: # @_ZN8NWindows5NFile5NFind12DoesDirExistEPKc
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi138:
	.cfi_def_cfa_offset 80
.Lcfi139:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$0, 56(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 48(%rsp)
	movb	$0, (%rax)
	movl	$4, 60(%rsp)
	cmpb	$99, (%rbx)
	jne	.LBB18_2
# BB#1:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB18_2:                               # %_ZL16nameWindowToUnixPKc.exit
.Ltmp142:
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
.Ltmp143:
# BB#3:
	testl	%eax, %eax
	je	.LBB18_5
# BB#4:
	xorl	%ebx, %ebx
	jmp	.LBB18_6
.LBB18_5:
	movb	40(%rsp), %bl
	andb	$16, %bl
	shrb	$4, %bl
.LBB18_6:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_8
# BB#7:
	callq	_ZdaPv
.LBB18_8:                               # %_ZN8NWindows5NFile5NFind9CFileInfoD2Ev.exit7
	movl	%ebx, %eax
	addq	$64, %rsp
	popq	%rbx
	retq
.LBB18_9:
.Ltmp144:
	movq	%rax, %rbx
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_11
# BB#10:
	callq	_ZdaPv
.LBB18_11:                              # %_ZN8NWindows5NFile5NFind9CFileInfoD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZN8NWindows5NFile5NFind12DoesDirExistEPKc, .Lfunc_end18-_ZN8NWindows5NFile5NFind12DoesDirExistEPKc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp142-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp142
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp142-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp143-.Ltmp142       #   Call between .Ltmp142 and .Ltmp143
	.long	.Ltmp144-.Lfunc_begin11 #     jumps to .Ltmp144
	.byte	0                       #   On action: cleanup
	.long	.Ltmp143-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Lfunc_end18-.Ltmp143   #   Call between .Ltmp143 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKc
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKc,@function
_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKc: # @_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKc
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi141:
	.cfi_def_cfa_offset 80
.Lcfi142:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$0, 56(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 48(%rsp)
	movb	$0, (%rax)
	movl	$4, 60(%rsp)
	cmpb	$99, (%rbx)
	jne	.LBB19_2
# BB#1:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB19_2:                               # %_ZL16nameWindowToUnixPKc.exit
.Ltmp145:
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
	movl	%eax, %ebx
.Ltmp146:
# BB#3:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB19_5
# BB#4:
	callq	_ZdaPv
.LBB19_5:                               # %_ZN8NWindows5NFile5NFind9CFileInfoD2Ev.exit
	testl	%ebx, %ebx
	sete	%al
	addq	$64, %rsp
	popq	%rbx
	retq
.LBB19_6:
.Ltmp147:
	movq	%rax, %rbx
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB19_8
# BB#7:
	callq	_ZdaPv
.LBB19_8:                               # %_ZN8NWindows5NFile5NFind9CFileInfoD2Ev.exit4
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKc, .Lfunc_end19-_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin12-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp145-.Lfunc_begin12 #   Call between .Lfunc_begin12 and .Ltmp145
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp146-.Ltmp145       #   Call between .Ltmp145 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin12 #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Lfunc_end19-.Ltmp146   #   Call between .Ltmp146 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile5NFind13DoesFileExistEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind13DoesFileExistEPKw,@function
_ZN8NWindows5NFile5NFind13DoesFileExistEPKw: # @_ZN8NWindows5NFile5NFind13DoesFileExistEPKw
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%rbp
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi145:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi146:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi148:
	.cfi_def_cfa_offset 192
.Lcfi149:
	.cfi_offset %rbx, -48
.Lcfi150:
	.cfi_offset %r12, -40
.Lcfi151:
	.cfi_offset %r14, -32
.Lcfi152:
	.cfi_offset %r15, -24
.Lcfi153:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %ebp
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB20_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbp), %eax
	movslq	%eax, %rbx
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	%ebx, 28(%rsp)
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB20_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB20_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%ebp, 24(%rsp)
.Ltmp148:
	leaq	72(%rsp), %rdi
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp149:
# BB#5:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_7
# BB#6:
	callq	_ZdaPv
.LBB20_7:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	72(%rsp), %rbx
	movq	$0, 136(%rsp)
.Ltmp151:
	movl	$4, %edi
	callq	_Znam
.Ltmp152:
# BB#8:                                 # %.noexc
	movq	%rax, 128(%rsp)
	movb	$0, (%rax)
	movl	$4, 140(%rsp)
	cmpb	$99, (%rbx)
	jne	.LBB20_10
# BB#9:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB20_10:                              # %_ZL16nameWindowToUnixPKc.exit.i
.Ltmp154:
	leaq	88(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
.Ltmp155:
# BB#11:
	testl	%eax, %eax
	je	.LBB20_13
# BB#12:
	xorl	%r12d, %r12d
	jmp	.LBB20_14
.LBB20_13:
	testb	$16, 120(%rsp)
	sete	%r12b
.LBB20_14:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_16
# BB#15:
	callq	_ZdaPv
.LBB20_16:
	movb	$1, %bl
	testb	%r12b, %r12b
	jne	.LBB20_41
# BB#17:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp157:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp158:
# BB#18:
	movq	%r15, (%rsp)
	movb	$0, (%r15)
	movl	$4, 12(%rsp)
	xorl	%eax, %eax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB20_19:                              # =>This Inner Loop Header: Depth=1
	addq	%rcx, %rax
	cmpl	$0, (%rdx)
	leaq	4(%rdx), %rdx
	jne	.LBB20_19
# BB#20:                                # %_Z11MyStringLenIwEiPKT_.exit.i19
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp160:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp161:
# BB#21:                                # %.noexc23
	movl	$0, (%rbp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB20_22:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i22
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB20_22
# BB#23:
	movl	$0, 8(%rsp)
	movb	$0, (%r15)
.Ltmp163:
	movl	$1, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp164:
# BB#24:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.preheader
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r14, (%rsp)
	movl	$1, 12(%rsp)
	movb	$0, (%r14)
	movl	$0, 8(%rsp)
	movl	(%rbp), %eax
	testl	%eax, %eax
	je	.LBB20_29
# BB#25:                                # %.lr.ph.i.preheader
	movq	%rbp, %rbx
	addq	$4, %rbx
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB20_26:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$255, %eax
	jg	.LBB20_35
# BB#27:                                #   in Loop: Header=BB20_26 Depth=1
.Ltmp166:
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp167:
# BB#28:                                # %.noexc26
                                        #   in Loop: Header=BB20_26 Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB20_26
.LBB20_29:                              # %.loopexit
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	(%rsp), %rbx
	movq	$0, 64(%rsp)
.Ltmp169:
	movl	$4, %edi
	callq	_Znam
.Ltmp170:
# BB#30:                                # %.noexc36
	movq	%rax, 56(%rsp)
	movb	$0, (%rax)
	movl	$4, 68(%rsp)
	cmpb	$99, (%rbx)
	jne	.LBB20_32
# BB#31:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB20_32:                              # %_ZL16nameWindowToUnixPKc.exit.i34
.Ltmp172:
	leaq	16(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
.Ltmp173:
# BB#33:
	testl	%eax, %eax
	je	.LBB20_36
# BB#34:
	xorl	%r12d, %r12d
	jmp	.LBB20_37
.LBB20_35:                              # %_ZN11CStringBaseIwED2Ev.exit27
	movq	%rbp, %rdi
	jmp	.LBB20_38
.LBB20_36:
	testb	$16, 48(%rsp)
	sete	%r12b
.LBB20_37:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_39
.LBB20_38:
	callq	_ZdaPv
.LBB20_39:
	testb	%r12b, %r12b
	setne	%bl
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_41
# BB#40:
	callq	_ZdaPv
.LBB20_41:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_43
# BB#42:
	callq	_ZdaPv
.LBB20_43:                              # %_ZN11CStringBaseIcED2Ev.exit43
	movl	%ebx, %eax
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_44:
.Ltmp174:
	movq	%rax, %r14
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB20_58
	jmp	.LBB20_59
.LBB20_46:
.Ltmp171:
	jmp	.LBB20_49
.LBB20_47:                              # %_ZN11CStringBaseIwED2Ev.exit40.loopexit.split-lp
.Ltmp165:
	jmp	.LBB20_57
.LBB20_48:
.Ltmp162:
.LBB20_49:
	movq	%rax, %r14
	jmp	.LBB20_59
.LBB20_50:
.Ltmp159:
	jmp	.LBB20_54
.LBB20_51:
.Ltmp156:
	movq	%rax, %r14
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB20_60
	jmp	.LBB20_61
.LBB20_53:
.Ltmp153:
.LBB20_54:
	movq	%rax, %r14
	jmp	.LBB20_61
.LBB20_55:
.Ltmp150:
	movq	%rax, %r14
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB20_62
	jmp	.LBB20_63
.LBB20_56:                              # %_ZN11CStringBaseIwED2Ev.exit40.loopexit
.Ltmp168:
.LBB20_57:                              # %_ZN11CStringBaseIwED2Ev.exit40
	movq	%rax, %r14
	movq	%rbp, %rdi
.LBB20_58:
	callq	_ZdaPv
.LBB20_59:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_61
.LBB20_60:
	callq	_ZdaPv
.LBB20_61:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_63
.LBB20_62:
	callq	_ZdaPv
.LBB20_63:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end20:
	.size	_ZN8NWindows5NFile5NFind13DoesFileExistEPKw, .Lfunc_end20-_ZN8NWindows5NFile5NFind13DoesFileExistEPKw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp148-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp148
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin13 #     jumps to .Ltmp150
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin13 #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin13 #     jumps to .Ltmp156
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin13 #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin13 # >> Call Site 6 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin13 #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin13 # >> Call Site 7 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin13 #     jumps to .Ltmp165
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin13 # >> Call Site 8 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin13 #     jumps to .Ltmp168
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin13 # >> Call Site 9 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin13 #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin13 # >> Call Site 10 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin13 #     jumps to .Ltmp174
	.byte	0                       #   On action: cleanup
	.long	.Ltmp173-.Lfunc_begin13 # >> Call Site 11 <<
	.long	.Lfunc_end20-.Ltmp173   #   Call between .Ltmp173 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile5NFind12DoesDirExistEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind12DoesDirExistEPKw,@function
_ZN8NWindows5NFile5NFind12DoesDirExistEPKw: # @_ZN8NWindows5NFile5NFind12DoesDirExistEPKw
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%rbp
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi157:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi158:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi159:
	.cfi_def_cfa_offset 192
.Lcfi160:
	.cfi_offset %rbx, -48
.Lcfi161:
	.cfi_offset %r12, -40
.Lcfi162:
	.cfi_offset %r14, -32
.Lcfi163:
	.cfi_offset %r15, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %ebp
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB21_1:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB21_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbp), %eax
	movslq	%eax, %rbx
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	%ebx, 28(%rsp)
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB21_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB21_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%ebp, 24(%rsp)
.Ltmp175:
	leaq	72(%rsp), %rdi
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp176:
# BB#5:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_7
# BB#6:
	callq	_ZdaPv
.LBB21_7:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	72(%rsp), %rbx
	movq	$0, 136(%rsp)
.Ltmp178:
	movl	$4, %edi
	callq	_Znam
.Ltmp179:
# BB#8:                                 # %.noexc
	movq	%rax, 128(%rsp)
	movb	$0, (%rax)
	movl	$4, 140(%rsp)
	cmpb	$99, (%rbx)
	jne	.LBB21_10
# BB#9:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB21_10:                              # %_ZL16nameWindowToUnixPKc.exit.i
.Ltmp181:
	leaq	88(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
.Ltmp182:
# BB#11:
	testl	%eax, %eax
	je	.LBB21_13
# BB#12:
	xorl	%r12d, %r12d
	jmp	.LBB21_14
.LBB21_13:
	movb	120(%rsp), %r12b
	andb	$16, %r12b
	shrb	$4, %r12b
.LBB21_14:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_16
# BB#15:
	callq	_ZdaPv
.LBB21_16:
	movb	$1, %bl
	testb	%r12b, %r12b
	jne	.LBB21_41
# BB#17:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp184:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp185:
# BB#18:
	movq	%r15, (%rsp)
	movb	$0, (%r15)
	movl	$4, 12(%rsp)
	xorl	%eax, %eax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB21_19:                              # =>This Inner Loop Header: Depth=1
	addq	%rcx, %rax
	cmpl	$0, (%rdx)
	leaq	4(%rdx), %rdx
	jne	.LBB21_19
# BB#20:                                # %_Z11MyStringLenIwEiPKT_.exit.i19
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp187:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp188:
# BB#21:                                # %.noexc23
	movl	$0, (%rbp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB21_22:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i22
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB21_22
# BB#23:
	movl	$0, 8(%rsp)
	movb	$0, (%r15)
.Ltmp190:
	movl	$1, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp191:
# BB#24:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.preheader
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r14, (%rsp)
	movl	$1, 12(%rsp)
	movb	$0, (%r14)
	movl	$0, 8(%rsp)
	movl	(%rbp), %eax
	testl	%eax, %eax
	je	.LBB21_29
# BB#25:                                # %.lr.ph.i.preheader
	movq	%rbp, %rbx
	addq	$4, %rbx
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB21_26:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$255, %eax
	jg	.LBB21_35
# BB#27:                                #   in Loop: Header=BB21_26 Depth=1
.Ltmp193:
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp194:
# BB#28:                                # %.noexc26
                                        #   in Loop: Header=BB21_26 Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB21_26
.LBB21_29:                              # %.loopexit
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	(%rsp), %rbx
	movq	$0, 64(%rsp)
.Ltmp196:
	movl	$4, %edi
	callq	_Znam
.Ltmp197:
# BB#30:                                # %.noexc36
	movq	%rax, 56(%rsp)
	movb	$0, (%rax)
	movl	$4, 68(%rsp)
	cmpb	$99, (%rbx)
	jne	.LBB21_32
# BB#31:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB21_32:                              # %_ZL16nameWindowToUnixPKc.exit.i34
.Ltmp199:
	leaq	16(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
.Ltmp200:
# BB#33:
	testl	%eax, %eax
	je	.LBB21_36
# BB#34:
	xorl	%r12d, %r12d
	jmp	.LBB21_37
.LBB21_35:                              # %_ZN11CStringBaseIwED2Ev.exit27
	movq	%rbp, %rdi
	jmp	.LBB21_38
.LBB21_36:
	movb	48(%rsp), %r12b
	andb	$16, %r12b
	shrb	$4, %r12b
.LBB21_37:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_39
.LBB21_38:
	callq	_ZdaPv
.LBB21_39:
	testb	%r12b, %r12b
	setne	%bl
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_41
# BB#40:
	callq	_ZdaPv
.LBB21_41:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_43
# BB#42:
	callq	_ZdaPv
.LBB21_43:                              # %_ZN11CStringBaseIcED2Ev.exit43
	movl	%ebx, %eax
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_44:
.Ltmp201:
	movq	%rax, %r14
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB21_58
	jmp	.LBB21_59
.LBB21_46:
.Ltmp198:
	jmp	.LBB21_49
.LBB21_47:                              # %_ZN11CStringBaseIwED2Ev.exit40.loopexit.split-lp
.Ltmp192:
	jmp	.LBB21_57
.LBB21_48:
.Ltmp189:
.LBB21_49:
	movq	%rax, %r14
	jmp	.LBB21_59
.LBB21_50:
.Ltmp186:
	jmp	.LBB21_54
.LBB21_51:
.Ltmp183:
	movq	%rax, %r14
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB21_60
	jmp	.LBB21_61
.LBB21_53:
.Ltmp180:
.LBB21_54:
	movq	%rax, %r14
	jmp	.LBB21_61
.LBB21_55:
.Ltmp177:
	movq	%rax, %r14
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB21_62
	jmp	.LBB21_63
.LBB21_56:                              # %_ZN11CStringBaseIwED2Ev.exit40.loopexit
.Ltmp195:
.LBB21_57:                              # %_ZN11CStringBaseIwED2Ev.exit40
	movq	%rax, %r14
	movq	%rbp, %rdi
.LBB21_58:
	callq	_ZdaPv
.LBB21_59:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_61
.LBB21_60:
	callq	_ZdaPv
.LBB21_61:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_63
.LBB21_62:
	callq	_ZdaPv
.LBB21_63:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZN8NWindows5NFile5NFind12DoesDirExistEPKw, .Lfunc_end21-_ZN8NWindows5NFile5NFind12DoesDirExistEPKw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin14-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp175-.Lfunc_begin14 #   Call between .Lfunc_begin14 and .Ltmp175
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp175-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin14 #     jumps to .Ltmp177
	.byte	0                       #   On action: cleanup
	.long	.Ltmp178-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin14 #     jumps to .Ltmp180
	.byte	0                       #   On action: cleanup
	.long	.Ltmp181-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Ltmp182-.Ltmp181       #   Call between .Ltmp181 and .Ltmp182
	.long	.Ltmp183-.Lfunc_begin14 #     jumps to .Ltmp183
	.byte	0                       #   On action: cleanup
	.long	.Ltmp184-.Lfunc_begin14 # >> Call Site 5 <<
	.long	.Ltmp185-.Ltmp184       #   Call between .Ltmp184 and .Ltmp185
	.long	.Ltmp186-.Lfunc_begin14 #     jumps to .Ltmp186
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin14 # >> Call Site 6 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin14 #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp190-.Lfunc_begin14 # >> Call Site 7 <<
	.long	.Ltmp191-.Ltmp190       #   Call between .Ltmp190 and .Ltmp191
	.long	.Ltmp192-.Lfunc_begin14 #     jumps to .Ltmp192
	.byte	0                       #   On action: cleanup
	.long	.Ltmp193-.Lfunc_begin14 # >> Call Site 8 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin14 #     jumps to .Ltmp195
	.byte	0                       #   On action: cleanup
	.long	.Ltmp196-.Lfunc_begin14 # >> Call Site 9 <<
	.long	.Ltmp197-.Ltmp196       #   Call between .Ltmp196 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin14 #     jumps to .Ltmp198
	.byte	0                       #   On action: cleanup
	.long	.Ltmp199-.Lfunc_begin14 # >> Call Site 10 <<
	.long	.Ltmp200-.Ltmp199       #   Call between .Ltmp199 and .Ltmp200
	.long	.Ltmp201-.Lfunc_begin14 #     jumps to .Ltmp201
	.byte	0                       #   On action: cleanup
	.long	.Ltmp200-.Lfunc_begin14 # >> Call Site 11 <<
	.long	.Lfunc_end21-.Ltmp200   #   Call between .Ltmp200 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKw,@function
_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKw: # @_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKw
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%rbp
.Lcfi165:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi166:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi167:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi168:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi170:
	.cfi_def_cfa_offset 192
.Lcfi171:
	.cfi_offset %rbx, -48
.Lcfi172:
	.cfi_offset %r12, -40
.Lcfi173:
	.cfi_offset %r14, -32
.Lcfi174:
	.cfi_offset %r15, -24
.Lcfi175:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movl	$-1, %ebp
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB22_1:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB22_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbp), %eax
	movslq	%eax, %rbx
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 32(%rsp)
	movl	$0, (%rax)
	movl	%ebx, 44(%rsp)
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB22_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB22_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%ebp, 40(%rsp)
.Ltmp202:
	leaq	16(%rsp), %rdi
	leaq	32(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp203:
# BB#5:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB22_7
# BB#6:
	callq	_ZdaPv
.LBB22_7:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	16(%rsp), %rbx
	movq	$0, 136(%rsp)
.Ltmp205:
	movl	$4, %edi
	callq	_Znam
.Ltmp206:
# BB#8:                                 # %.noexc
	movq	%rax, 128(%rsp)
	movb	$0, (%rax)
	movl	$4, 140(%rsp)
	cmpb	$99, (%rbx)
	jne	.LBB22_10
# BB#9:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB22_10:                              # %_ZL16nameWindowToUnixPKc.exit.i
.Ltmp208:
	leaq	88(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
	movl	%eax, %ebx
.Ltmp209:
# BB#11:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB22_13
# BB#12:
	callq	_ZdaPv
.LBB22_13:
	testl	%ebx, %ebx
	sete	%r12b
	je	.LBB22_33
# BB#14:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp211:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp212:
# BB#15:
	movq	%r15, (%rsp)
	movb	$0, (%r15)
	movl	$4, 12(%rsp)
	xorl	%eax, %eax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB22_16:                              # =>This Inner Loop Header: Depth=1
	addq	%rcx, %rax
	cmpl	$0, (%rdx)
	leaq	4(%rdx), %rdx
	jne	.LBB22_16
# BB#17:                                # %_Z11MyStringLenIwEiPKT_.exit.i19
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp214:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp215:
# BB#18:                                # %.noexc23
	movl	$0, (%rbp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_19:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i22
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB22_19
# BB#20:
	movl	$0, 8(%rsp)
	movb	$0, (%r15)
.Ltmp217:
	movl	$1, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp218:
# BB#21:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.preheader
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r14, (%rsp)
	movl	$1, 12(%rsp)
	movb	$0, (%r14)
	movl	$0, 8(%rsp)
	movl	(%rbp), %eax
	testl	%eax, %eax
	je	.LBB22_26
# BB#22:                                # %.lr.ph.i.preheader
	movq	%rbp, %rbx
	addq	$4, %rbx
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB22_23:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$255, %eax
	jg	.LBB22_34
# BB#24:                                #   in Loop: Header=BB22_23 Depth=1
.Ltmp220:
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp221:
# BB#25:                                # %.noexc26
                                        #   in Loop: Header=BB22_23 Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB22_23
.LBB22_26:                              # %.loopexit
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	(%rsp), %rbx
	movq	$0, 80(%rsp)
.Ltmp223:
	movl	$4, %edi
	callq	_Znam
.Ltmp224:
# BB#27:                                # %.noexc36
	movq	%rax, 72(%rsp)
	movb	$0, (%rax)
	movl	$4, 84(%rsp)
	cmpb	$99, (%rbx)
	jne	.LBB22_29
# BB#28:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB22_29:                              # %_ZL16nameWindowToUnixPKc.exit.i34
.Ltmp226:
	leaq	32(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKc
	movl	%eax, %ebx
.Ltmp227:
# BB#30:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB22_32
# BB#31:
	callq	_ZdaPv
.LBB22_32:
	testl	%ebx, %ebx
	sete	%r12b
	jmp	.LBB22_35
.LBB22_33:
	movb	$1, %bl
	jmp	.LBB22_37
.LBB22_34:                              # %_ZN11CStringBaseIwED2Ev.exit27
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB22_35:
	testb	%r12b, %r12b
	setne	%bl
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB22_37
# BB#36:
	callq	_ZdaPv
.LBB22_37:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB22_39
# BB#38:
	callq	_ZdaPv
.LBB22_39:                              # %_ZN11CStringBaseIcED2Ev.exit43
	movl	%ebx, %eax
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_40:
.Ltmp228:
	movq	%rax, %r14
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB22_54
	jmp	.LBB22_55
.LBB22_42:
.Ltmp225:
	jmp	.LBB22_45
.LBB22_43:                              # %_ZN11CStringBaseIwED2Ev.exit40.loopexit.split-lp
.Ltmp219:
	jmp	.LBB22_53
.LBB22_44:
.Ltmp216:
.LBB22_45:
	movq	%rax, %r14
	jmp	.LBB22_55
.LBB22_46:
.Ltmp213:
	jmp	.LBB22_50
.LBB22_47:
.Ltmp210:
	movq	%rax, %r14
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB22_56
	jmp	.LBB22_57
.LBB22_49:
.Ltmp207:
.LBB22_50:
	movq	%rax, %r14
	jmp	.LBB22_57
.LBB22_51:
.Ltmp204:
	movq	%rax, %r14
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB22_58
	jmp	.LBB22_59
.LBB22_52:                              # %_ZN11CStringBaseIwED2Ev.exit40.loopexit
.Ltmp222:
.LBB22_53:                              # %_ZN11CStringBaseIwED2Ev.exit40
	movq	%rax, %r14
	movq	%rbp, %rdi
.LBB22_54:
	callq	_ZdaPv
.LBB22_55:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB22_57
.LBB22_56:
	callq	_ZdaPv
.LBB22_57:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB22_59
.LBB22_58:
	callq	_ZdaPv
.LBB22_59:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKw, .Lfunc_end22-_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin15-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp202-.Lfunc_begin15 #   Call between .Lfunc_begin15 and .Ltmp202
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp202-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp203-.Ltmp202       #   Call between .Ltmp202 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin15 #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin15 #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin15 #     jumps to .Ltmp210
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin15 # >> Call Site 5 <<
	.long	.Ltmp212-.Ltmp211       #   Call between .Ltmp211 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin15 #     jumps to .Ltmp213
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin15 # >> Call Site 6 <<
	.long	.Ltmp215-.Ltmp214       #   Call between .Ltmp214 and .Ltmp215
	.long	.Ltmp216-.Lfunc_begin15 #     jumps to .Ltmp216
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin15 # >> Call Site 7 <<
	.long	.Ltmp218-.Ltmp217       #   Call between .Ltmp217 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin15 #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin15 # >> Call Site 8 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin15 #     jumps to .Ltmp222
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin15 # >> Call Site 9 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin15 #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp226-.Lfunc_begin15 # >> Call Site 10 <<
	.long	.Ltmp227-.Ltmp226       #   Call between .Ltmp226 and .Ltmp227
	.long	.Ltmp228-.Lfunc_begin15 #     jumps to .Ltmp228
	.byte	0                       #   On action: cleanup
	.long	.Ltmp227-.Lfunc_begin15 # >> Call Site 11 <<
	.long	.Lfunc_end22-.Ltmp227   #   Call between .Ltmp227 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile5NFind11CEnumerator7NextAnyERNS1_9CFileInfoE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind11CEnumerator7NextAnyERNS1_9CFileInfoE,@function
_ZN8NWindows5NFile5NFind11CEnumerator7NextAnyERNS1_9CFileInfoE: # @_ZN8NWindows5NFile5NFind11CEnumerator7NextAnyERNS1_9CFileInfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi176:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi177:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 32
.Lcfi179:
	.cfi_offset %rbx, -32
.Lcfi180:
	.cfi_offset %r14, -24
.Lcfi181:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	jne	.LBB23_1
# BB#5:
	movq	40(%r15), %rsi
	movq	%r15, %rdi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKcRNS1_9CFileInfoE # TAILCALL
	.p2align	4, 0x90
.LBB23_3:                               # %._crit_edge.i
                                        #   in Loop: Header=BB23_1 Depth=1
	movq	(%r15), %rdi
.LBB23_1:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	callq	readdir64
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB23_4
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB23_1 Depth=1
	addq	$19, %rbx
	movq	8(%r15), %rsi
	movq	%rbx, %rdi
	callq	_ZL14filter_patternPKcS0_i
	cmpl	$1, %eax
	jne	.LBB23_3
# BB#6:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	_ZN8NWindows5NFile5NFindL16fillin_CFileInfoERNS1_9CFileInfoEPKcS5_
	testl	%eax, %eax
	sete	%al
	jmp	.LBB23_7
.LBB23_4:                               # %._crit_edge
	callq	__errno_location
	movl	$1048867, (%rax)        # imm = 0x100123
	xorl	%eax, %eax
.LBB23_7:                               # %_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_9CFileInfoE.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end23:
	.size	_ZN8NWindows5NFile5NFind11CEnumerator7NextAnyERNS1_9CFileInfoE, .Lfunc_end23-_ZN8NWindows5NFile5NFind11CEnumerator7NextAnyERNS1_9CFileInfoE
	.cfi_endproc

	.globl	_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoE,@function
_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoE: # @_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi182:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi183:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi184:
	.cfi_def_cfa_offset 32
.Lcfi185:
	.cfi_offset %rbx, -24
.Lcfi186:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
.LBB24_1:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFind11CEnumerator7NextAnyERNS1_9CFileInfoE
	testb	%al, %al
	je	.LBB24_8
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB24_1 Depth=1
	movb	$1, %al
	testb	$16, 32(%rbx)
	je	.LBB24_9
# BB#3:                                 #   in Loop: Header=BB24_1 Depth=1
	movl	48(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB24_9
# BB#4:                                 #   in Loop: Header=BB24_1 Depth=1
	movq	40(%rbx), %rdx
	cmpb	$46, (%rdx)
	jne	.LBB24_9
# BB#5:                                 #   in Loop: Header=BB24_1 Depth=1
	cmpl	$1, %ecx
	je	.LBB24_1
# BB#6:                                 # %_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv.exit
                                        #   in Loop: Header=BB24_1 Depth=1
	cmpl	$2, %ecx
	jne	.LBB24_9
# BB#7:                                 # %_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv.exit
                                        #   in Loop: Header=BB24_1 Depth=1
	cmpb	$46, 1(%rdx)
	je	.LBB24_1
	jmp	.LBB24_9
.LBB24_8:
	xorl	%eax, %eax
.LBB24_9:                               # %_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv.exit.thread
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end24:
	.size	_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoE, .Lfunc_end24-_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoE
	.cfi_endproc

	.globl	_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoERb
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoERb,@function
_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoERb: # @_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoERb
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi187:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi188:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi189:
	.cfi_def_cfa_offset 32
.Lcfi190:
	.cfi_offset %rbx, -32
.Lcfi191:
	.cfi_offset %r14, -24
.Lcfi192:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
.LBB25_1:                               # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFind11CEnumerator7NextAnyERNS1_9CFileInfoE
	testb	%al, %al
	je	.LBB25_9
# BB#2:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB25_1 Depth=1
	testb	$16, 32(%rbx)
	je	.LBB25_8
# BB#3:                                 #   in Loop: Header=BB25_1 Depth=1
	movl	48(%rbx), %eax
	testl	%eax, %eax
	je	.LBB25_8
# BB#4:                                 #   in Loop: Header=BB25_1 Depth=1
	movq	40(%rbx), %rcx
	cmpb	$46, (%rcx)
	jne	.LBB25_8
# BB#5:                                 #   in Loop: Header=BB25_1 Depth=1
	cmpl	$1, %eax
	je	.LBB25_1
# BB#6:                                 # %_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv.exit.i
                                        #   in Loop: Header=BB25_1 Depth=1
	cmpl	$2, %eax
	jne	.LBB25_8
# BB#7:                                 # %_ZNK8NWindows5NFile5NFind9CFileInfo6IsDotsEv.exit.i
                                        #   in Loop: Header=BB25_1 Depth=1
	cmpb	$46, 1(%rcx)
	je	.LBB25_1
.LBB25_8:
	movb	$1, (%r14)
	movb	$1, %al
	jmp	.LBB25_10
.LBB25_9:                               # %_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoE.exit
	movb	$0, (%r14)
	callq	__errno_location
	cmpl	$1048867, (%rax)        # imm = 0x100123
	sete	%al
.LBB25_10:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end25:
	.size	_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoERb, .Lfunc_end25-_ZN8NWindows5NFile5NFind11CEnumerator4NextERNS1_9CFileInfoERb
	.cfi_endproc

	.globl	_ZN8NWindows5NFile5NFind12CEnumeratorW7NextAnyERNS1_10CFileInfoWE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind12CEnumeratorW7NextAnyERNS1_10CFileInfoWE,@function
_ZN8NWindows5NFile5NFind12CEnumeratorW7NextAnyERNS1_10CFileInfoWE: # @_ZN8NWindows5NFile5NFind12CEnumeratorW7NextAnyERNS1_10CFileInfoWE
	.cfi_startproc
# BB#0:
	movq	%rsi, %rax
	cmpq	$0, (%rdi)
	je	.LBB26_2
# BB#1:
	movq	%rax, %rsi
	jmp	_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_10CFileInfoWE # TAILCALL
.LBB26_2:
	movq	40(%rdi), %rsi
	movq	%rax, %rdx
	jmp	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKwRNS1_10CFileInfoWE # TAILCALL
.Lfunc_end26:
	.size	_ZN8NWindows5NFile5NFind12CEnumeratorW7NextAnyERNS1_10CFileInfoWE, .Lfunc_end26-_ZN8NWindows5NFile5NFind12CEnumeratorW7NextAnyERNS1_10CFileInfoWE
	.cfi_endproc

	.globl	_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWE,@function
_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWE: # @_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi195:
	.cfi_def_cfa_offset 32
.Lcfi196:
	.cfi_offset %rbx, -24
.Lcfi197:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	.p2align	4, 0x90
.LBB27_1:                               # %_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv.exit.thread5
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%r14)
	je	.LBB27_3
# BB#2:                                 #   in Loop: Header=BB27_1 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_10CFileInfoWE
	testb	%al, %al
	jne	.LBB27_4
	jmp	.LBB27_11
	.p2align	4, 0x90
.LBB27_3:                               # %_ZN8NWindows5NFile5NFind12CEnumeratorW7NextAnyERNS1_10CFileInfoWE.exit
                                        #   in Loop: Header=BB27_1 Depth=1
	movq	40(%r14), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKwRNS1_10CFileInfoWE
	testb	%al, %al
	je	.LBB27_11
.LBB27_4:                               #   in Loop: Header=BB27_1 Depth=1
	movb	$1, %al
	testb	$16, 32(%rbx)
	je	.LBB27_12
# BB#5:                                 #   in Loop: Header=BB27_1 Depth=1
	movl	48(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB27_12
# BB#6:                                 #   in Loop: Header=BB27_1 Depth=1
	movq	40(%rbx), %rdx
	cmpl	$46, (%rdx)
	jne	.LBB27_12
# BB#7:                                 #   in Loop: Header=BB27_1 Depth=1
	cmpl	$1, %ecx
	je	.LBB27_1
# BB#8:                                 # %_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv.exit
                                        #   in Loop: Header=BB27_1 Depth=1
	cmpl	$2, %ecx
	jne	.LBB27_12
# BB#9:                                 # %_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv.exit
                                        #   in Loop: Header=BB27_1 Depth=1
	cmpl	$46, 4(%rdx)
	je	.LBB27_1
	jmp	.LBB27_12
.LBB27_11:
	xorl	%eax, %eax
.LBB27_12:                              # %_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv.exit.thread
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end27:
	.size	_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWE, .Lfunc_end27-_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWE
	.cfi_endproc

	.globl	_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWERb
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWERb,@function
_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWERb: # @_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWERb
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi198:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi199:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi200:
	.cfi_def_cfa_offset 32
.Lcfi201:
	.cfi_offset %rbx, -32
.Lcfi202:
	.cfi_offset %r14, -24
.Lcfi203:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	.p2align	4, 0x90
.LBB28_1:                               # %_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv.exit.thread5.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%r15)
	je	.LBB28_3
# BB#2:                                 #   in Loop: Header=BB28_1 Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile5NFind9CFindFile8FindNextERNS1_10CFileInfoWE
	testb	%al, %al
	jne	.LBB28_5
	jmp	.LBB28_12
	.p2align	4, 0x90
.LBB28_3:                               # %_ZN8NWindows5NFile5NFind12CEnumeratorW7NextAnyERNS1_10CFileInfoWE.exit.i
                                        #   in Loop: Header=BB28_1 Depth=1
	movq	40(%r15), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	_ZN8NWindows5NFile5NFind9CFindFile9FindFirstEPKwRNS1_10CFileInfoWE
	testb	%al, %al
	je	.LBB28_12
.LBB28_5:                               #   in Loop: Header=BB28_1 Depth=1
	testb	$16, 32(%rbx)
	je	.LBB28_11
# BB#6:                                 #   in Loop: Header=BB28_1 Depth=1
	movl	48(%rbx), %eax
	testl	%eax, %eax
	je	.LBB28_11
# BB#7:                                 #   in Loop: Header=BB28_1 Depth=1
	movq	40(%rbx), %rcx
	cmpl	$46, (%rcx)
	jne	.LBB28_11
# BB#8:                                 #   in Loop: Header=BB28_1 Depth=1
	cmpl	$1, %eax
	je	.LBB28_1
# BB#9:                                 # %_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv.exit.i
                                        #   in Loop: Header=BB28_1 Depth=1
	cmpl	$2, %eax
	jne	.LBB28_11
# BB#10:                                # %_ZNK8NWindows5NFile5NFind10CFileInfoW6IsDotsEv.exit.i
                                        #   in Loop: Header=BB28_1 Depth=1
	cmpl	$46, 4(%rcx)
	je	.LBB28_1
.LBB28_11:
	movb	$1, (%r14)
	movb	$1, %al
	jmp	.LBB28_13
.LBB28_12:                              # %_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWE.exit
	movb	$0, (%r14)
	callq	__errno_location
	cmpl	$1048867, (%rax)        # imm = 0x100123
	sete	%al
.LBB28_13:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end28:
	.size	_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWERb, .Lfunc_end28-_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWERb
	.cfi_endproc

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi204:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi205:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi206:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi207:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi208:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi209:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi210:
	.cfi_def_cfa_offset 64
.Lcfi211:
	.cfi_offset %rbx, -56
.Lcfi212:
	.cfi_offset %r12, -48
.Lcfi213:
	.cfi_offset %r13, -40
.Lcfi214:
	.cfi_offset %r14, -32
.Lcfi215:
	.cfi_offset %r15, -24
.Lcfi216:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB29_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB29_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB29_3:                               # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB29_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB29_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB29_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB29_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB29_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB29_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB29_17
.LBB29_7:
	xorl	%ecx, %ecx
.LBB29_8:                               # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB29_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB29_10:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB29_10
.LBB29_11:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB29_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB29_13:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB29_13
	jmp	.LBB29_26
.LBB29_25:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB29_27
.LBB29_26:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB29_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB29_28:                              # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_17:                              # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB29_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB29_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB29_20
	jmp	.LBB29_21
.LBB29_18:
	xorl	%ebx, %ebx
.LBB29_21:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB29_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB29_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB29_23
.LBB29_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB29_8
	jmp	.LBB29_26
.Lfunc_end29:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end29-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.section	.text._ZN11CStringBaseIcEpLEPKc,"axG",@progbits,_ZN11CStringBaseIcEpLEPKc,comdat
	.weak	_ZN11CStringBaseIcEpLEPKc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEPKc,@function
_ZN11CStringBaseIcEpLEPKc:              # @_ZN11CStringBaseIcEpLEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi217:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi218:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi219:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi220:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi221:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi222:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi223:
	.cfi_def_cfa_offset 64
.Lcfi224:
	.cfi_offset %rbx, -56
.Lcfi225:
	.cfi_offset %r12, -48
.Lcfi226:
	.cfi_offset %r13, -40
.Lcfi227:
	.cfi_offset %r14, -32
.Lcfi228:
	.cfi_offset %r15, -24
.Lcfi229:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$-1, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB30_1:                               # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB30_1
# BB#2:                                 # %_Z11MyStringLenIcEiPKT_.exit
	movl	8(%r14), %ebp
	movl	12(%r14), %r13d
	movl	%r13d, %eax
	subl	%ebp, %eax
	cmpl	%r12d, %eax
	jg	.LBB30_30
# BB#3:
	decl	%eax
	cmpl	$8, %r13d
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %r13d
	jl	.LBB30_5
# BB#4:                                 # %select.true.sink
	movl	%r13d, %ecx
	shrl	$31, %ecx
	addl	%r13d, %ecx
	sarl	%ecx
.LBB30_5:                               # %select.end
	addl	%ecx, %eax
	movl	%ebp, %edx
	subl	%r13d, %edx
	cmpl	%r12d, %eax
	leal	1(%rdx,%r12), %eax
	cmovgel	%ecx, %eax
	leal	1(%rax,%r13), %ecx
	cmpl	%r13d, %ecx
	je	.LBB30_30
# BB#6:
	addl	%r13d, %eax
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movslq	%ecx, %rcx
	cmpl	$-1, %eax
	movq	$-1, %rdi
	cmovgeq	%rcx, %rdi
	callq	_Znam
	movq	%rax, %r15
	testl	%r13d, %r13d
	jle	.LBB30_29
# BB#7:                                 # %.preheader.i.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB30_27
# BB#8:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %r9
	cmpl	$31, %ebp
	jbe	.LBB30_9
# BB#16:                                # %min.iters.checked
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB30_9
# BB#17:                                # %vector.memcheck
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %r15
	jae	.LBB30_19
# BB#18:                                # %vector.memcheck
	leaq	(%r15,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB30_19
.LBB30_9:
	xorl	%ecx, %ecx
.LBB30_10:                              # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%r9), %rsi
	subq	%rcx, %rsi
	andq	$7, %rbp
	je	.LBB30_13
# BB#11:                                # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB30_12:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r15,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB30_12
.LBB30_13:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB30_28
# BB#14:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %r9
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB30_15:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB30_15
	jmp	.LBB30_28
.LBB30_27:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB30_29
.LBB30_28:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB30_29:
	movq	%r15, (%r14)
	movslq	%ebp, %rax
	movb	$0, (%r15,%rax)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 12(%r14)
.LBB30_30:                              # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movslq	%ebp, %rax
	addq	(%r14), %rax
	.p2align	4, 0x90
.LBB30_31:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB30_31
# BB#32:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit
	addl	%r12d, 8(%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB30_19:                              # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB30_20
# BB#21:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB30_22:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%r15,%rdx)
	movups	%xmm1, 16(%r15,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB30_22
	jmp	.LBB30_23
.LBB30_20:
	xorl	%edx, %edx
.LBB30_23:                              # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB30_26
# BB#24:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%r15,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB30_25:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB30_25
.LBB30_26:                              # %middle.block
	cmpq	%rcx, %r9
	jne	.LBB30_10
	jmp	.LBB30_28
.Lfunc_end30:
	.size	_ZN11CStringBaseIcEpLEPKc, .Lfunc_end30-_ZN11CStringBaseIcEpLEPKc
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%rbp
.Lcfi230:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi231:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi232:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi233:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi234:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi236:
	.cfi_def_cfa_offset 64
.Lcfi237:
	.cfi_offset %rbx, -56
.Lcfi238:
	.cfi_offset %r12, -48
.Lcfi239:
	.cfi_offset %r13, -40
.Lcfi240:
	.cfi_offset %r14, -32
.Lcfi241:
	.cfi_offset %r15, -24
.Lcfi242:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB31_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB31_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB31_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB31_5
.LBB31_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB31_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp229:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp230:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB31_35
# BB#12:
	movq	%rbx, %r13
.LBB31_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB31_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB31_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB31_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB31_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB31_15
.LBB31_14:
	xorl	%esi, %esi
.LBB31_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB31_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB31_16
.LBB31_29:
	movq	%r13, %rbx
.LBB31_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp231:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp232:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB31_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB31_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB31_8
.LBB31_3:
	xorl	%eax, %eax
.LBB31_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB31_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB31_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB31_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB31_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB31_30
.LBB31_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB31_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB31_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB31_24
	jmp	.LBB31_25
.LBB31_22:
	xorl	%ecx, %ecx
.LBB31_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB31_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB31_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB31_27
.LBB31_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB31_15
	jmp	.LBB31_29
.LBB31_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp233:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end31:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end31-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin16-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp229-.Lfunc_begin16 #   Call between .Lfunc_begin16 and .Ltmp229
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp229-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp232-.Ltmp229       #   Call between .Ltmp229 and .Ltmp232
	.long	.Ltmp233-.Lfunc_begin16 #     jumps to .Ltmp233
	.byte	0                       #   On action: cleanup
	.long	.Ltmp232-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Lfunc_end31-.Ltmp232   #   Call between .Ltmp232 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIcE3MidEii,"axG",@progbits,_ZNK11CStringBaseIcE3MidEii,comdat
	.weak	_ZNK11CStringBaseIcE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIcE3MidEii,@function
_ZNK11CStringBaseIcE3MidEii:            # @_ZNK11CStringBaseIcE3MidEii
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%rbp
.Lcfi243:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi244:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi245:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi246:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi247:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi248:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi249:
	.cfi_def_cfa_offset 64
.Lcfi250:
	.cfi_offset %rbx, -56
.Lcfi251:
	.cfi_offset %r12, -48
.Lcfi252:
	.cfi_offset %r13, -40
.Lcfi253:
	.cfi_offset %r14, -32
.Lcfi254:
	.cfi_offset %r15, -24
.Lcfi255:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %rbp
	leal	(%rcx,%r12), %edx
	movl	8(%r15), %eax
	movl	%eax, %r14d
	subl	%r12d, %r14d
	cmpl	%eax, %edx
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB32_9
# BB#1:
	leal	(%r14,%r12), %ecx
	cmpl	%eax, %ecx
	jne	.LBB32_9
# BB#2:
	movslq	%eax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB32_3
# BB#4:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbx, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rbp)
	movb	$0, (%rax)
	movl	%ebx, 12(%rbp)
	jmp	.LBB32_5
.LBB32_9:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbx
	movb	$0, (%rbx)
	leal	1(%r14), %r13d
	cmpl	$4, %r13d
	je	.LBB32_13
# BB#10:
	movslq	%r13d, %rax
	cmpl	$-1, %r14d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp234:
	callq	_Znam
	movl	%r13d, %ecx
	movq	%rax, %r13
.Ltmp235:
# BB#11:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%rbx, %rdi
	callq	_ZdaPv
	movb	$0, (%r13)
	testl	%r14d, %r14d
	jle	.LBB32_33
# BB#12:
	movq	%r13, %rbx
	movl	4(%rsp), %r13d          # 4-byte Reload
.LBB32_13:                              # %.lr.ph
	movslq	%r12d, %r9
	movslq	%r14d, %rax
	movq	(%r15), %rcx
	testq	%rax, %rax
	movl	$1, %r10d
	cmovgq	%rax, %r10
	cmpq	$31, %r10
	jbe	.LBB32_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775776, %rsi # imm = 0x7FFFFFFFFFFFFFE0
	andq	%r10, %rsi
	je	.LBB32_14
# BB#18:                                # %vector.memcheck
	testq	%rax, %rax
	movl	$1, %edx
	cmovgq	%rax, %rdx
	leaq	(%rdx,%r9), %rdi
	addq	%rcx, %rdi
	cmpq	%rdi, %rbx
	jae	.LBB32_20
# BB#19:                                # %vector.memcheck
	addq	%rbx, %rdx
	leaq	(%rcx,%r9), %rdi
	cmpq	%rdx, %rdi
	jae	.LBB32_20
.LBB32_14:
	xorl	%esi, %esi
.LBB32_15:                              # %scalar.ph.preheader
	addq	%r9, %rcx
	.p2align	4, 0x90
.LBB32_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx,%rsi), %edx
	movb	%dl, (%rbx,%rsi)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB32_16
	jmp	.LBB32_28
.LBB32_3:
	xorl	%eax, %eax
.LBB32_5:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB32_6:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB32_6
# BB#7:
	movl	8(%r15), %eax
	movl	%eax, 8(%rbp)
	jmp	.LBB32_8
.LBB32_33:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.._crit_edge_crit_edge
	movslq	%r14d, %rax
	movq	%r13, %rbx
	movl	4(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB32_28
.LBB32_20:                              # %vector.body.preheader
	movq	%rbp, %r11
	leaq	-32(%rsi), %r8
	movl	%r8d, %edx
	shrl	$5, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB32_21
# BB#22:                                # %vector.body.prol.preheader
	leaq	16(%rcx,%r9), %rdi
	negq	%rdx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB32_23:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi,%rbp), %xmm0
	movups	(%rdi,%rbp), %xmm1
	movups	%xmm0, (%rbx,%rbp)
	movups	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rdx
	jne	.LBB32_23
	jmp	.LBB32_24
.LBB32_21:
	xorl	%ebp, %ebp
.LBB32_24:                              # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB32_27
# BB#25:                                # %vector.body.preheader.new
	movq	%rsi, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rdi
	addq	%r9, %rbp
	leaq	112(%rcx,%rbp), %rbp
	.p2align	4, 0x90
.LBB32_26:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB32_26
.LBB32_27:                              # %middle.block
	cmpq	%rsi, %r10
	movq	%r11, %rbp
	jne	.LBB32_15
.LBB32_28:                              # %._crit_edge
	movb	$0, (%rbx,%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movslq	%r13d, %rax
	cmpl	$-1, %r14d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp236:
	callq	_Znam
.Ltmp237:
# BB#29:                                # %.noexc23
	movq	%rax, (%rbp)
	movb	$0, (%rax)
	movl	%r13d, 12(%rbp)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB32_30:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i20
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx,%rcx), %edx
	movb	%dl, (%rax,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB32_30
# BB#31:                                # %_ZN11CStringBaseIcED2Ev.exit25
	movl	%r14d, 8(%rbp)
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB32_8:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB32_32:                              # %_ZN11CStringBaseIcED2Ev.exit
.Ltmp238:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZNK11CStringBaseIcE3MidEii, .Lfunc_end32-_ZNK11CStringBaseIcE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin17-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp234-.Lfunc_begin17 #   Call between .Lfunc_begin17 and .Ltmp234
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp237-.Ltmp234       #   Call between .Ltmp234 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin17 #     jumps to .Ltmp238
	.byte	0                       #   On action: cleanup
	.long	.Ltmp237-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Lfunc_end32-.Ltmp237   #   Call between .Ltmp237 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	global_use_lstat,@object # @global_use_lstat
	.data
	.globl	global_use_lstat
	.p2align	2
global_use_lstat:
	.long	1                       # 0x1
	.size	global_use_lstat, 4

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"fillin_CFileInfo - internal error - MAX_PATHNAME_LEN"
	.size	.L.str.3, 53

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" ("
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	")"
	.size	.L.str.6, 2

	.type	_ZTS11CStringBaseIcE,@object # @_ZTS11CStringBaseIcE
	.section	.rodata._ZTS11CStringBaseIcE,"aG",@progbits,_ZTS11CStringBaseIcE,comdat
	.weak	_ZTS11CStringBaseIcE
	.p2align	4
_ZTS11CStringBaseIcE:
	.asciz	"11CStringBaseIcE"
	.size	_ZTS11CStringBaseIcE, 17

	.type	_ZTI11CStringBaseIcE,@object # @_ZTI11CStringBaseIcE
	.section	.rodata._ZTI11CStringBaseIcE,"aG",@progbits,_ZTI11CStringBaseIcE,comdat
	.weak	_ZTI11CStringBaseIcE
	.p2align	3
_ZTI11CStringBaseIcE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS11CStringBaseIcE
	.size	_ZTI11CStringBaseIcE, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
