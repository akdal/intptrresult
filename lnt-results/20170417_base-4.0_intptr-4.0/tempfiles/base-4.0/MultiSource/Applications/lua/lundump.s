	.text
	.file	"lundump.bc"
	.hidden	luaU_undump
	.globl	luaU_undump
	.p2align	4, 0x90
	.type	luaU_undump,@function
luaU_undump:                            # @luaU_undump
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 80
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rcx, %rbx
	movq	%rdi, %r14
	movb	(%rbx), %al
	cmpb	$64, %al
	je	.LBB0_2
# BB#1:
	cmpb	$61, %al
	jne	.LBB0_3
.LBB0_2:
	incq	%rbx
	jmp	.LBB0_4
.LBB0_3:
	cmpb	$27, %al
	movl	$.L.str.1, %eax
	cmoveq	%rax, %rbx
.LBB0_4:
	movq	%rbx, 24(%rsp)
	movq	%r14, (%rsp)
	movq	%rsi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movl	$1635077147, 32(%rsp)   # imm = 0x61754C1B
	movabsq	$2256232287109201, %rax # imm = 0x8040804010051
	movq	%rax, 36(%rsp)
	leaq	44(%rsp), %rax
	movl	$12, %edx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB0_6
# BB#5:
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	luaO_pushfstring
	movq	(%rsp), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB0_6:                                # %LoadBlock.exit.i
	leaq	32(%rsp), %rdi
	leaq	44(%rsp), %rsi
	movl	$12, %edx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_8
# BB#7:
	movq	(%rsp), %rdi
	movq	24(%rsp), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.3, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%rsp), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB0_8:                                # %LoadHeader.exit
	movl	$.L.str.2, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	luaS_newlstr
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	LoadFunction
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	luaU_undump, .Lfunc_end0-luaU_undump
	.cfi_endproc

	.p2align	4, 0x90
	.type	LoadFunction,@function
LoadFunction:                           # @LoadFunction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r13
	movq	(%r13), %rdi
	movzwl	96(%rdi), %eax
	incl	%eax
	movw	%ax, 96(%rdi)
	movzwl	%ax, %eax
	cmpl	$201, %eax
	jb	.LBB1_2
# BB#1:
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.6, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	(%r13), %rdi
.LBB1_2:
	callq	luaF_newproto
	movq	%rax, %r14
	movq	(%r13), %rax
	movq	16(%rax), %rax
	movq	%r14, (%rax)
	movl	$9, 8(%rax)
	movq	(%r13), %rdi
	movq	16(%rdi), %rax
	movq	56(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$16, %rcx
	jg	.LBB1_4
# BB#3:
	movl	$1, %esi
	callq	luaD_growstack
	movq	(%r13), %rdi
	movq	16(%rdi), %rax
.LBB1_4:
	addq	$16, %rax
	movq	%rax, 16(%rdi)
	movq	8(%r13), %rdi
	movq	%rsp, %rsi
	movl	$8, %edx
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_6
# BB#5:
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_6:                                # %LoadBlock.exit
	movq	(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB1_7
# BB#8:
	movq	(%r13), %rdi
	movq	16(%r13), %rsi
	callq	luaZ_openspace
	movq	%rax, %rbp
	movq	(%rsp), %rdx
	movq	8(%r13), %rdi
	movq	%rbp, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_10
# BB#9:
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_10:                               # %LoadBlock.exit49
	movq	(%r13), %rdi
	movq	(%rsp), %rdx
	decq	%rdx
	movq	%rbp, %rsi
	callq	luaS_newlstr
	jmp	.LBB1_11
.LBB1_7:
	xorl	%eax, %eax
.LBB1_11:                               # %LoadString.exit
	testq	%rax, %rax
	cmovneq	%rax, %r15
	movq	%r15, 64(%r14)
	movq	%r13, %rdi
	callq	LoadInt
	movl	%eax, 96(%r14)
	movq	%r13, %rdi
	callq	LoadInt
	movl	%eax, 100(%r14)
	movq	8(%r13), %rdi
	movq	%rsp, %rsi
	movl	$1, %edx
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_13
# BB#12:
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_13:                               # %LoadChar.exit
	movb	(%rsp), %al
	movb	%al, 112(%r14)
	movq	8(%r13), %rdi
	movq	%rsp, %rsi
	movl	$1, %edx
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_15
# BB#14:
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_15:                               # %LoadChar.exit42
	movb	(%rsp), %al
	movb	%al, 113(%r14)
	movq	8(%r13), %rdi
	movq	%rsp, %rsi
	movl	$1, %edx
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_17
# BB#16:
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_17:                               # %LoadChar.exit43
	movb	(%rsp), %al
	movb	%al, 114(%r14)
	movq	8(%r13), %rdi
	movq	%rsp, %rsi
	movl	$1, %edx
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_19
# BB#18:
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_19:                               # %LoadChar.exit44
	movb	(%rsp), %al
	movb	%al, 115(%r14)
	movq	%r13, %rdi
	callq	LoadInt
	movl	%eax, %r15d
	movq	(%r13), %rdi
	cmpl	$-1, %r15d
	jl	.LBB1_21
# BB#20:
	movslq	%r15d, %rbp
	shlq	$2, %rbp
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbp, %rcx
	callq	luaM_realloc_
	jmp	.LBB1_22
.LBB1_21:
	callq	luaM_toobig
	movslq	%r15d, %rbp
	shlq	$2, %rbp
.LBB1_22:
	movq	%rax, 24(%r14)
	movl	%r15d, 80(%r14)
	movq	8(%r13), %rdi
	movq	%rax, %rsi
	movq	%rbp, %rdx
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_24
# BB#23:
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_24:                               # %LoadCode.exit
	movq	%r13, %rdi
	callq	LoadInt
	movl	%eax, %r12d
	cmpl	$-2, %r12d
	movq	(%r13), %rdi
	jle	.LBB1_25
# BB#28:
	movslq	%r12d, %rcx
	shlq	$4, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	luaM_realloc_
	movq	%rax, %r15
	movq	%r15, 16(%r14)
	movl	%r12d, 76(%r14)
	testl	%r12d, %r12d
	jle	.LBB1_26
# BB#29:                                # %.lr.ph62.preheader
	movl	%r12d, %ebp
	leaq	-1(%rbp), %rcx
	movq	%rbp, %rdx
	andq	$7, %rdx
	je	.LBB1_30
# BB#31:                                # %.lr.ph62.prol.preheader
	leaq	8(%r15), %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_32:                               # %.lr.ph62.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rsi)
	incq	%rax
	addq	$16, %rsi
	cmpq	%rax, %rdx
	jne	.LBB1_32
	jmp	.LBB1_33
.LBB1_25:                               # %.thread
	callq	luaM_toobig
	movq	%rax, 16(%r14)
	movl	%r12d, 76(%r14)
.LBB1_26:                               # %._crit_edge
	movq	%r13, %rdi
	callq	LoadInt
	movl	%eax, %ebp
	cmpl	$-2, %ebp
	movq	(%r13), %rdi
	jle	.LBB1_27
# BB#60:
	movslq	%ebp, %rcx
	shlq	$3, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	luaM_realloc_
	movq	%rax, 32(%r14)
	movl	%ebp, 88(%r14)
	testl	%ebp, %ebp
	jle	.LBB1_71
# BB#61:                                # %.lr.ph57.preheader
	movl	%ebp, %ebx
	movq	$0, (%rax)
	cmpl	$1, %ebp
	je	.LBB1_68
# BB#62:                                # %.lr.ph57..lr.ph57_crit_edge.preheader
	leal	7(%rbx), %edx
	leaq	-2(%rbx), %rcx
	andq	$7, %rdx
	je	.LBB1_63
# BB#64:                                # %.lr.ph57..lr.ph57_crit_edge.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_65:                               # %.lr.ph57..lr.ph57_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rsi
	movq	$0, 8(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB1_65
# BB#66:                                # %.lr.ph57..lr.ph57_crit_edge.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$7, %rcx
	jae	.LBB1_116
	jmp	.LBB1_68
.LBB1_27:                               # %.thread80
	callq	luaM_toobig
	movq	%rax, 32(%r14)
	movl	%ebp, 88(%r14)
	jmp	.LBB1_71
.LBB1_30:
	xorl	%eax, %eax
.LBB1_33:                               # %.lr.ph62.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB1_36
# BB#34:                                # %.lr.ph62.preheader.new
	movq	%rbp, %rcx
	subq	%rax, %rcx
	shlq	$4, %rax
	leaq	120(%r15,%rax), %rax
	.p2align	4, 0x90
.LBB1_35:                               # %.lr.ph62
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, -112(%rax)
	movl	$0, -96(%rax)
	movl	$0, -80(%rax)
	movl	$0, -64(%rax)
	movl	$0, -48(%rax)
	movl	$0, -32(%rax)
	movl	$0, -16(%rax)
	movl	$0, (%rax)
	subq	$-128, %rax
	addq	$-8, %rcx
	jne	.LBB1_35
.LBB1_36:                               # %.preheader52
	testl	%r12d, %r12d
	jle	.LBB1_26
# BB#37:                                # %.lr.ph59
	decq	%rbp
	movl	$8, %r12d
	movq	%rsp, %rbx
	jmp	.LBB1_38
	.p2align	4, 0x90
.LBB1_59:                               # %._crit_edge77
                                        #   in Loop: Header=BB1_38 Depth=1
	movq	16(%r14), %r15
	addq	$16, %r12
	decq	%rbp
.LBB1_38:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_40
# BB#39:                                #   in Loop: Header=BB1_38 Depth=1
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_40:                               # %LoadChar.exit.i
                                        #   in Loop: Header=BB1_38 Depth=1
	movsbl	(%rsp), %eax
	cmpl	$4, %eax
	ja	.LBB1_57
# BB#41:                                # %LoadChar.exit.i
                                        #   in Loop: Header=BB1_38 Depth=1
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_42:                               #   in Loop: Header=BB1_38 Depth=1
	movl	$0, (%r15,%r12)
	testq	%rbp, %rbp
	jne	.LBB1_59
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_43:                               #   in Loop: Header=BB1_38 Depth=1
	movq	8(%r13), %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_45
# BB#44:                                #   in Loop: Header=BB1_38 Depth=1
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_45:                               # %LoadChar.exit64.i
                                        #   in Loop: Header=BB1_38 Depth=1
	xorl	%eax, %eax
	cmpb	$0, (%rsp)
	setne	%al
	movl	%eax, -8(%r15,%r12)
	movl	$1, (%r15,%r12)
	testq	%rbp, %rbp
	jne	.LBB1_59
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_57:                               #   in Loop: Header=BB1_38 Depth=1
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.9, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
	testq	%rbp, %rbp
	jne	.LBB1_59
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_46:                               #   in Loop: Header=BB1_38 Depth=1
	movq	8(%r13), %rdi
	movl	$8, %edx
	movq	%rbx, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_48
# BB#47:                                #   in Loop: Header=BB1_38 Depth=1
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_48:                               # %LoadNumber.exit.i
                                        #   in Loop: Header=BB1_38 Depth=1
	movq	(%rsp), %rax
	movq	%rax, -8(%r15,%r12)
	movl	$3, (%r15,%r12)
	testq	%rbp, %rbp
	jne	.LBB1_59
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_49:                               #   in Loop: Header=BB1_38 Depth=1
	movq	8(%r13), %rdi
	movl	$8, %edx
	movq	%rbx, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_51
# BB#50:                                #   in Loop: Header=BB1_38 Depth=1
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_51:                               # %LoadBlock.exit50
                                        #   in Loop: Header=BB1_38 Depth=1
	movq	(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB1_52
# BB#53:                                #   in Loop: Header=BB1_38 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rsi
	callq	luaZ_openspace
	movq	%rax, %rbx
	movq	(%rsp), %rdx
	movq	8(%r13), %rdi
	movq	%rbx, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_55
# BB#54:                                #   in Loop: Header=BB1_38 Depth=1
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_55:                               # %LoadBlock.exit51
                                        #   in Loop: Header=BB1_38 Depth=1
	movq	(%r13), %rdi
	movq	(%rsp), %rdx
	decq	%rdx
	movq	%rbx, %rsi
	callq	luaS_newlstr
	movq	%rsp, %rbx
	jmp	.LBB1_56
.LBB1_52:                               #   in Loop: Header=BB1_38 Depth=1
	xorl	%eax, %eax
.LBB1_56:                               # %LoadString.exit.i48
                                        #   in Loop: Header=BB1_38 Depth=1
	movq	%rax, -8(%r15,%r12)
	movl	$4, (%r15,%r12)
	testq	%rbp, %rbp
	jne	.LBB1_59
	jmp	.LBB1_26
.LBB1_63:
	movl	$1, %eax
	cmpq	$7, %rcx
	jb	.LBB1_68
	.p2align	4, 0x90
.LBB1_116:                              # %.lr.ph57..lr.ph57_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	32(%r14), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	32(%r14), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	32(%r14), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	32(%r14), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	32(%r14), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	32(%r14), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	32(%r14), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %rbx
	jne	.LBB1_116
.LBB1_68:                               # %.preheader
	testl	%ebp, %ebp
	jle	.LBB1_71
# BB#69:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_70:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	64(%r14), %rsi
	movq	%r13, %rdi
	callq	LoadFunction
	movq	32(%r14), %rcx
	movq	%rax, (%rcx,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_70
.LBB1_71:                               # %LoadConstants.exit
	movq	%r13, %rdi
	callq	LoadInt
	movl	%eax, %r15d
	movq	(%r13), %rdi
	cmpl	$-1, %r15d
	jl	.LBB1_73
# BB#72:
	movslq	%r15d, %rbp
	shlq	$2, %rbp
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbp, %rcx
	callq	luaM_realloc_
	jmp	.LBB1_74
.LBB1_73:
	callq	luaM_toobig
	movslq	%r15d, %rbp
	shlq	$2, %rbp
.LBB1_74:
	movq	%rax, 40(%r14)
	movl	%r15d, 84(%r14)
	movq	8(%r13), %rdi
	movq	%rax, %rsi
	movq	%rbp, %rdx
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_76
# BB#75:
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_76:                               # %LoadBlock.exit.i
	movq	%r13, %rdi
	callq	LoadInt
	movl	%eax, %ebp
	cmpl	$-2, %ebp
	movq	(%r13), %rdi
	jle	.LBB1_77
# BB#78:
	movslq	%ebp, %rcx
	shlq	$4, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	luaM_realloc_
	movq	%rax, 48(%r14)
	movl	%ebp, 92(%r14)
	testl	%ebp, %ebp
	jle	.LBB1_94
# BB#79:                                # %.lr.ph69.preheader.i
	movl	%ebp, %r12d
	leaq	-1(%r12), %rdx
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	andq	$7, %rsi
	je	.LBB1_82
# BB#80:                                # %.lr.ph69.i.prol.preheader
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB1_81:                               # %.lr.ph69.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, (%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB1_81
.LBB1_82:                               # %.lr.ph69.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB1_85
# BB#83:                                # %.lr.ph69.preheader.i.new
	movq	%r12, %rdx
	subq	%rcx, %rdx
	shlq	$4, %rcx
	leaq	112(%rax,%rcx), %rax
	.p2align	4, 0x90
.LBB1_84:                               # %.lr.ph69.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, -112(%rax)
	movq	$0, -96(%rax)
	movq	$0, -80(%rax)
	movq	$0, -64(%rax)
	movq	$0, -48(%rax)
	movq	$0, -32(%rax)
	movq	$0, -16(%rax)
	movq	$0, (%rax)
	subq	$-128, %rax
	addq	$-8, %rdx
	jne	.LBB1_84
.LBB1_85:                               # %.lr.ph65.i
	movq	%rsp, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_86:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rdi
	movl	$8, %edx
	movq	%r15, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_88
# BB#87:                                #   in Loop: Header=BB1_86 Depth=1
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_88:                               # %LoadBlock.exit55.i
                                        #   in Loop: Header=BB1_86 Depth=1
	movq	(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB1_89
# BB#90:                                #   in Loop: Header=BB1_86 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rsi
	callq	luaZ_openspace
	movq	%rax, %rbp
	movq	(%rsp), %rdx
	movq	8(%r13), %rdi
	movq	%rbp, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_92
# BB#91:                                #   in Loop: Header=BB1_86 Depth=1
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_92:                               # %LoadBlock.exit56.i
                                        #   in Loop: Header=BB1_86 Depth=1
	movq	(%r13), %rdi
	movq	(%rsp), %rdx
	decq	%rdx
	movq	%rbp, %rsi
	callq	luaS_newlstr
	jmp	.LBB1_93
	.p2align	4, 0x90
.LBB1_89:                               #   in Loop: Header=BB1_86 Depth=1
	xorl	%eax, %eax
.LBB1_93:                               # %LoadString.exit.i
                                        #   in Loop: Header=BB1_86 Depth=1
	movq	48(%r14), %rcx
	movq	%rax, (%rcx,%rbx)
	movq	%r13, %rdi
	callq	LoadInt
	movq	48(%r14), %rcx
	movl	%eax, 8(%rcx,%rbx)
	movq	%r13, %rdi
	callq	LoadInt
	movq	48(%r14), %rcx
	movl	%eax, 12(%rcx,%rbx)
	addq	$16, %rbx
	decq	%r12
	jne	.LBB1_86
	jmp	.LBB1_94
.LBB1_77:                               # %.thread.i
	callq	luaM_toobig
	movq	%rax, 48(%r14)
	movl	%ebp, 92(%r14)
.LBB1_94:                               # %._crit_edge66.i
	movq	%r13, %rdi
	callq	LoadInt
	movl	%eax, %ebp
	cmpl	$-2, %ebp
	movq	(%r13), %rdi
	jle	.LBB1_95
# BB#96:
	movslq	%ebp, %rcx
	shlq	$3, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	luaM_realloc_
	movq	%rax, 56(%r14)
	movl	%ebp, 72(%r14)
	testl	%ebp, %ebp
	jle	.LBB1_113
# BB#97:                                # %.lr.ph63.preheader.i
	movl	%ebp, %r12d
	movq	$0, (%rax)
	cmpl	$1, %ebp
	je	.LBB1_104
# BB#98:                                # %.lr.ph63..lr.ph63_crit_edge.i.preheader
	leal	7(%r12), %edx
	leaq	-2(%r12), %rcx
	andq	$7, %rdx
	je	.LBB1_99
# BB#100:                               # %.lr.ph63..lr.ph63_crit_edge.i.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_101:                              # %.lr.ph63..lr.ph63_crit_edge.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rsi
	movq	$0, 8(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB1_101
# BB#102:                               # %.lr.ph63..lr.ph63_crit_edge.i.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$7, %rcx
	jae	.LBB1_117
	jmp	.LBB1_104
.LBB1_95:                               # %.thread85.i
	callq	luaM_toobig
	movq	%rax, 56(%r14)
	movl	%ebp, 72(%r14)
	jmp	.LBB1_113
.LBB1_99:
	movl	$1, %eax
	cmpq	$7, %rcx
	jb	.LBB1_104
	.p2align	4, 0x90
.LBB1_117:                              # %.lr.ph63..lr.ph63_crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %r12
	jne	.LBB1_117
.LBB1_104:                              # %.lr.ph.i
	movq	%rsp, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_105:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rdi
	movl	$8, %edx
	movq	%r15, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_107
# BB#106:                               #   in Loop: Header=BB1_105 Depth=1
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_107:                              # %LoadBlock.exit57.i
                                        #   in Loop: Header=BB1_105 Depth=1
	movq	(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB1_108
# BB#109:                               #   in Loop: Header=BB1_105 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rsi
	callq	luaZ_openspace
	movq	%rax, %rbp
	movq	(%rsp), %rdx
	movq	8(%r13), %rdi
	movq	%rbp, %rsi
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB1_111
# BB#110:                               #   in Loop: Header=BB1_105 Depth=1
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_111:                              # %LoadBlock.exit58.i
                                        #   in Loop: Header=BB1_105 Depth=1
	movq	(%r13), %rdi
	movq	(%rsp), %rdx
	decq	%rdx
	movq	%rbp, %rsi
	callq	luaS_newlstr
	jmp	.LBB1_112
	.p2align	4, 0x90
.LBB1_108:                              #   in Loop: Header=BB1_105 Depth=1
	xorl	%eax, %eax
.LBB1_112:                              # %LoadString.exit54.i
                                        #   in Loop: Header=BB1_105 Depth=1
	movq	56(%r14), %rcx
	movq	%rax, (%rcx,%rbx,8)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB1_105
.LBB1_113:                              # %LoadDebug.exit
	movq	%r14, %rdi
	callq	luaG_checkcode
	testl	%eax, %eax
	jne	.LBB1_115
# BB#114:
	movq	(%r13), %rdi
	movq	24(%r13), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.7, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB1_115:
	movq	(%r13), %rax
	addq	$-16, 16(%rax)
	decw	96(%rax)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	LoadFunction, .Lfunc_end1-LoadFunction
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_42
	.quad	.LBB1_43
	.quad	.LBB1_57
	.quad	.LBB1_46
	.quad	.LBB1_49

	.text
	.hidden	luaU_header
	.globl	luaU_header
	.p2align	4, 0x90
	.type	luaU_header,@function
luaU_header:                            # @luaU_header
	.cfi_startproc
# BB#0:
	movl	$1635077147, (%rdi)     # imm = 0x61754C1B
	movabsq	$2256232287109201, %rax # imm = 0x8040804010051
	movq	%rax, 4(%rdi)
	retq
.Lfunc_end2:
	.size	luaU_header, .Lfunc_end2-luaU_header
	.cfi_endproc

	.p2align	4, 0x90
	.type	LoadInt,@function
LoadInt:                                # @LoadInt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	leaq	12(%rsp), %rsi
	movl	$4, %edx
	callq	luaZ_read
	testq	%rax, %rax
	je	.LBB3_2
# BB#1:
	movq	(%rbx), %rdi
	movq	24(%rbx), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%rbx), %rdi
	movl	$3, %esi
	callq	luaD_throw
.LBB3_2:                                # %LoadBlock.exit
	movl	12(%rsp), %eax
	testl	%eax, %eax
	jns	.LBB3_4
# BB#3:
	movq	(%rbx), %rdi
	movq	24(%rbx), %rdx
	movl	$.L.str.5, %esi
	movl	$.L.str.8, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	(%rbx), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movl	12(%rsp), %eax
.LBB3_4:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	LoadInt, .Lfunc_end3-LoadInt
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"binary string"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"=?"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"bad header"
	.size	.L.str.3, 11

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"unexpected end"
	.size	.L.str.4, 15

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s: %s in precompiled chunk"
	.size	.L.str.5, 28

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"code too deep"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"bad code"
	.size	.L.str.7, 9

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"bad integer"
	.size	.L.str.8, 12

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"bad constant"
	.size	.L.str.9, 13

	.hidden	luaS_newlstr
	.hidden	luaZ_read
	.hidden	luaO_pushfstring
	.hidden	luaD_throw
	.hidden	luaF_newproto
	.hidden	luaD_growstack
	.hidden	luaG_checkcode
	.hidden	luaZ_openspace
	.hidden	luaM_realloc_
	.hidden	luaM_toobig

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
