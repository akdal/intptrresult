	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$21288, %rsp            # imm = 0x5328
.Lcfi6:
	.cfi_def_cfa_offset 21344
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	leaq	8(%rsp), %rdi
	callq	lame_init
	cmpl	$1, %ebp
	jne	.LBB0_2
# BB#1:
	movq	(%rbx), %rsi
	leaq	8(%rsp), %rdi
	callq	lame_usage
.LBB0_2:
	leaq	8(%rsp), %rdi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	callq	lame_parse_args
	cmpl	$0, 28(%rsp)
                                        # implicit-def: %R14
	jne	.LBB0_7
# BB#3:
	movq	144(%rsp), %rdi
	cmpb	$45, (%rdi)
	jne	.LBB0_6
# BB#4:
	cmpb	$0, 1(%rdi)
	je	.LBB0_5
.LBB0_6:                                # %.thread
	movl	$.L.str.1, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_15
.LBB0_7:
	leaq	8(%rsp), %r12
	movq	%r12, %rdi
	callq	lame_init_infile
	movq	%r12, %rdi
	callq	lame_init_params
	movq	%r12, %rdi
	callq	lame_print_config
	leaq	288(%rsp), %r13
	leaq	4896(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	lame_readframe
	movl	%eax, %ebp
	movl	$16384, %r9d            # imm = 0x4000
	movq	%r12, %rdi
	movq	%r13, %rsi
	leaq	2592(%rsp), %rdx
	movl	%ebp, %ecx
	movq	%rbx, %r8
	callq	lame_encode_buffer
	cmpl	$-1, %eax
	je	.LBB0_9
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	movslq	%eax, %r15
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	fwrite
	cmpq	%r15, %rax
	jne	.LBB0_12
# BB#13:                                #   in Loop: Header=BB0_8 Depth=1
	testl	%ebp, %ebp
	jne	.LBB0_8
# BB#14:
	leaq	8(%rsp), %rbx
	leaq	4896(%rsp), %rbp
	movl	$16384, %edx            # imm = 0x4000
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	lame_encode_finish
	movslq	%eax, %rdx
	movl	$1, %esi
	movq	%rbp, %rdi
	movq	%r14, %rcx
	callq	fwrite
	movq	%r14, %rdi
	callq	fclose
	movq	%rbx, %rdi
	callq	lame_close_infile
	movq	%rbx, %rdi
	callq	lame_mp3_tags
	xorl	%eax, %eax
	addq	$21288, %rsp            # imm = 0x5328
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:
	movq	stdout(%rip), %r14
	jmp	.LBB0_7
.LBB0_9:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$33, %esi
	jmp	.LBB0_10
.LBB0_12:
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$24, %esi
.LBB0_10:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB0_15:
	movq	stderr(%rip), %rdi
	movq	144(%rsp), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"-"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"wb"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Could not create \"%s\".\n"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"mp3 buffer is not big enough... \n"
	.size	.L.str.3, 34

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Error writing mp3 output"
	.size	.L.str.4, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
