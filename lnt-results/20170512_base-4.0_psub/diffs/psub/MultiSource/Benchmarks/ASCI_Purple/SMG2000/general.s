	.text
	.file	"general.bc"
	.globl	hypre_Log2
	.p2align	4, 0x90
	.type	hypre_Log2,@function
hypre_Log2:                             # @hypre_Log2
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB0_1
# BB#2:                                 # %.preheader
	xorl	%eax, %eax
	cmpl	$1, %edi
	je	.LBB0_5
# BB#3:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	movl	%edi, %ecx
	shrl	%ecx
	cmpl	$3, %edi
	movl	%ecx, %edi
	ja	.LBB0_4
.LBB0_5:                                # %.loopexit
	retq
.LBB0_1:
	movl	$-1, %eax
	retq
.Lfunc_end0:
	.size	hypre_Log2, .Lfunc_end0-hypre_Log2
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
