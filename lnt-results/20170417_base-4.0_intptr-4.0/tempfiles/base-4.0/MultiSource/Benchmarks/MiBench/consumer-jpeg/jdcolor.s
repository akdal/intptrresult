	.text
	.file	"jdcolor.bc"
	.globl	jinit_color_deconverter
	.p2align	4, 0x90
	.type	jinit_color_deconverter,@function
jinit_color_deconverter:                # @jinit_color_deconverter
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$48, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 600(%r15)
	movq	$start_pass_dcolor, (%r14)
	movl	52(%r15), %eax
	leal	-2(%rax), %ecx
	cmpl	$2, %ecx
	jb	.LBB0_3
# BB#1:
	leal	-4(%rax), %ecx
	cmpl	$2, %ecx
	jae	.LBB0_4
# BB#2:
	cmpl	$4, 48(%r15)
	jne	.LBB0_7
	jmp	.LBB0_8
.LBB0_3:
	cmpl	$3, 48(%r15)
	jne	.LBB0_7
	jmp	.LBB0_8
.LBB0_4:
	cmpl	$1, %eax
	jne	.LBB0_6
# BB#5:
	cmpl	$1, 48(%r15)
	jne	.LBB0_7
	jmp	.LBB0_8
.LBB0_6:
	cmpl	$0, 48(%r15)
	jg	.LBB0_8
.LBB0_7:
	movq	(%r15), %rax
	movl	$8, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB0_8:
	movl	56(%r15), %eax
	cmpl	$4, %eax
	je	.LBB0_17
# BB#9:
	cmpl	$2, %eax
	je	.LBB0_21
# BB#10:
	cmpl	$1, %eax
	jne	.LBB0_25
# BB#11:
	movl	$1, 136(%r15)
	movl	52(%r15), %eax
	orl	$2, %eax
	cmpl	$3, %eax
	jne	.LBB0_27
# BB#12:
	movq	$grayscale_convert, 8(%r14)
	movslq	48(%r15), %rax
	cmpq	$2, %rax
	jl	.LBB0_28
# BB#13:                                # %.lr.ph
	movq	296(%r15), %rdx
	leal	7(%rax), %edi
	leaq	-2(%rax), %rsi
	andq	$7, %rdi
	je	.LBB0_33
# BB#14:                                # %.prol.preheader
	leaq	144(%rdx), %rbx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_15:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rbx)
	incq	%rcx
	addq	$96, %rbx
	cmpq	%rcx, %rdi
	jne	.LBB0_15
# BB#16:                                # %.prol.loopexit.unr-lcssa
	incq	%rcx
	cmpq	$7, %rsi
	jb	.LBB0_28
	jmp	.LBB0_34
.LBB0_17:
	movl	$4, 136(%r15)
	movl	52(%r15), %eax
	cmpl	$4, %eax
	je	.LBB0_32
# BB#18:
	cmpl	$5, %eax
	jne	.LBB0_27
# BB#19:
	movq	$ycck_cmyk_convert, 8(%r14)
	movq	600(%r15), %rbx
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, 16(%rbx)
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, 24(%rbx)
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$2048, %edx             # imm = 0x800
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, 32(%rbx)
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$2048, %edx             # imm = 0x800
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, 40(%rbx)
	movq	16(%rbx), %r8
	movq	24(%rbx), %r9
	movq	32(%rbx), %r10
	movq	$-14831872, %r11        # imm = 0xFF1DAF00
	movq	$-11728000, %rdi        # imm = 0xFF4D0B80
	xorl	%ecx, %ecx
	movl	$2919680, %edx          # imm = 0x2C8D00
	movl	$5990656, %esi          # imm = 0x5B6900
	.p2align	4, 0x90
.LBB0_20:                               # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rbx
	shrq	$16, %rbx
	movl	%ebx, (%r8,%rcx)
	movq	%r11, %rbx
	shrq	$16, %rbx
	movl	%ebx, (%r9,%rcx)
	movq	%rsi, (%r10,%rcx,2)
	movq	%rdx, (%rax,%rcx,2)
	addq	$4, %rcx
	addq	$-22554, %rdx           # imm = 0xA7E6
	addq	$-46802, %rsi           # imm = 0xFFFF492E
	addq	$116130, %r11           # imm = 0x1C5A2
	addq	$91881, %rdi            # imm = 0x166E9
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB0_20
	jmp	.LBB0_28
.LBB0_21:
	movl	$3, 136(%r15)
	movl	52(%r15), %eax
	cmpl	$2, %eax
	je	.LBB0_32
# BB#22:
	cmpl	$3, %eax
	jne	.LBB0_27
# BB#23:
	movq	$ycc_rgb_convert, 8(%r14)
	movq	600(%r15), %rbx
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, 16(%rbx)
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, 24(%rbx)
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$2048, %edx             # imm = 0x800
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, 32(%rbx)
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$2048, %edx             # imm = 0x800
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, 40(%rbx)
	movq	16(%rbx), %r8
	movq	24(%rbx), %r9
	movq	32(%rbx), %r10
	movq	$-14831872, %r11        # imm = 0xFF1DAF00
	movq	$-11728000, %rdi        # imm = 0xFF4D0B80
	xorl	%ecx, %ecx
	movl	$2919680, %edx          # imm = 0x2C8D00
	movl	$5990656, %esi          # imm = 0x5B6900
	.p2align	4, 0x90
.LBB0_24:                               # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rbx
	shrq	$16, %rbx
	movl	%ebx, (%r8,%rcx)
	movq	%r11, %rbx
	shrq	$16, %rbx
	movl	%ebx, (%r9,%rcx)
	movq	%rsi, (%r10,%rcx,2)
	movq	%rdx, (%rax,%rcx,2)
	addq	$4, %rcx
	addq	$-22554, %rdx           # imm = 0xA7E6
	addq	$-46802, %rsi           # imm = 0xFFFF492E
	addq	$116130, %r11           # imm = 0x1C5A2
	addq	$91881, %rdi            # imm = 0x166E9
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB0_24
	jmp	.LBB0_28
.LBB0_25:
	cmpl	52(%r15), %eax
	jne	.LBB0_27
# BB#26:
	movl	48(%r15), %eax
	movl	%eax, 136(%r15)
	movq	$null_convert, 8(%r14)
	jmp	.LBB0_28
.LBB0_27:
	movq	(%r15), %rax
	movl	$25, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	jmp	.LBB0_28
.LBB0_32:
	movq	$null_convert, 8(%r14)
.LBB0_28:                               # %build_ycc_rgb_table.exit
	movl	$1, %eax
	cmpl	$0, 100(%r15)
	jne	.LBB0_30
# BB#29:
	movl	136(%r15), %eax
.LBB0_30:
	movl	%eax, 140(%r15)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB0_33:
	movl	$1, %ecx
	cmpq	$7, %rsi
	jb	.LBB0_28
.LBB0_34:                               # %.lr.ph.new
	leaq	(%rcx,%rcx,2), %rsi
	shlq	$5, %rsi
	leaq	720(%rdx,%rsi), %rdx
	.p2align	4, 0x90
.LBB0_35:                               # =>This Inner Loop Header: Depth=1
	movl	$0, -672(%rdx)
	movl	$0, -576(%rdx)
	movl	$0, -480(%rdx)
	movl	$0, -384(%rdx)
	movl	$0, -288(%rdx)
	movl	$0, -192(%rdx)
	movl	$0, -96(%rdx)
	movl	$0, (%rdx)
	addq	$8, %rcx
	addq	$768, %rdx              # imm = 0x300
	cmpq	%rax, %rcx
	jl	.LBB0_35
	jmp	.LBB0_28
.Lfunc_end0:
	.size	jinit_color_deconverter, .Lfunc_end0-jinit_color_deconverter
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_dcolor,@function
start_pass_dcolor:                      # @start_pass_dcolor
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	start_pass_dcolor, .Lfunc_end1-start_pass_dcolor
	.cfi_endproc

	.p2align	4, 0x90
	.type	grayscale_convert,@function
grayscale_convert:                      # @grayscale_convert
	.cfi_startproc
# BB#0:
	movq	%rcx, %rax
	movq	(%rsi), %rsi
	movl	128(%rdi), %r9d
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	movl	%edx, %esi
	movq	%rax, %rdx
	jmp	jcopy_sample_rows       # TAILCALL
.Lfunc_end2:
	.size	grayscale_convert, .Lfunc_end2-grayscale_convert
	.cfi_endproc

	.p2align	4, 0x90
	.type	ycc_rgb_convert,@function
ycc_rgb_convert:                        # @ycc_rgb_convert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB3_6
# BB#1:                                 # %.lr.ph79
	movl	128(%rdi), %eax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	testl	%eax, %eax
	je	.LBB3_6
# BB#2:                                 # %.lr.ph79.split.us.preheader
	movq	600(%rdi), %rax
	movq	408(%rdi), %r13
	movq	16(%rax), %r8
	movq	24(%rax), %r11
	movq	32(%rax), %r15
	movq	40(%rax), %r12
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph79.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	movq	%rbp, -8(%rsp)          # 8-byte Spill
	leal	-1(%rbp), %eax
	movl	%eax, -40(%rsp)         # 4-byte Spill
	movl	%edx, %edi
	movq	-24(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rax
	movq	8(%rsi), %rbx
	movq	(%rax,%rdi,8), %rbp
	movq	(%rbx,%rdi,8), %rax
	movq	16(%rsi), %rbx
	movq	(%rbx,%rdi,8), %rbx
	incl	%edx
	movl	%edx, -36(%rsp)         # 4-byte Spill
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movq	(%rcx), %rdi
	movq	-32(%rsp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %r14d
	movzbl	(%rbx), %r10d
	movslq	(%r8,%r10,4), %rdx
	addq	%r14, %rdx
	movzbl	(%r13,%rdx), %edx
	movzbl	(%rax), %esi
	movb	%dl, (%rdi)
	movq	(%r15,%r10,8), %rdx
	addq	(%r12,%rsi,8), %rdx
	shrq	$16, %rdx
	movslq	%edx, %rdx
	addq	%r14, %rdx
	movzbl	(%r13,%rdx), %edx
	movb	%dl, 1(%rdi)
	movslq	(%r11,%rsi,4), %rdx
	addq	%r14, %rdx
	movzbl	(%r13,%rdx), %edx
	movb	%dl, 2(%rdi)
	incq	%rbp
	incq	%rax
	incq	%rbx
	addq	$3, %rdi
	decq	%r9
	jne	.LBB3_4
# BB#5:                                 # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	-16(%rsp), %rcx         # 8-byte Reload
	addq	$8, %rcx
	cmpl	$1, -8(%rsp)            # 4-byte Folded Reload
	movl	-40(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ebp
	movl	-36(%rsp), %edx         # 4-byte Reload
	jg	.LBB3_3
.LBB3_6:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	ycc_rgb_convert, .Lfunc_end3-ycc_rgb_convert
	.cfi_endproc

	.p2align	4, 0x90
	.type	null_convert,@function
null_convert:                           # @null_convert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%r8d, %r8d
	jle	.LBB4_13
# BB#1:                                 # %.preheader.lr.ph
	movslq	48(%rdi), %r13
	testl	%r13d, %r13d
	jle	.LBB4_13
# BB#2:                                 # %.preheader.us.preheader
	movl	128(%rdi), %r15d
	movl	%r13d, %r9d
	leal	-1(%r15), %r10d
	movl	%r15d, %r11d
	andl	$7, %r11d
	movl	%r11d, %eax
	negl	%eax
	movl	%eax, -4(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #       Child Loop BB4_8 Depth 3
                                        #       Child Loop BB4_10 Depth 3
	testl	%r15d, %r15d
	movl	%edx, %edx
	je	.LBB4_12
# BB#4:                                 # %.lr.ph.us51.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph.us51
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_8 Depth 3
                                        #       Child Loop BB4_10 Depth 3
	movq	(%rcx), %rbp
	addq	%r12, %rbp
	testl	%r11d, %r11d
	movq	(%rsi,%r12,8), %rax
	movq	(%rax,%rdx,8), %rdi
	je	.LBB4_6
# BB#7:                                 # %.prol.preheader
                                        #   in Loop: Header=BB4_5 Depth=2
	movl	-4(%rsp), %eax          # 4-byte Reload
	movl	%r15d, %ebx
	.p2align	4, 0x90
.LBB4_8:                                #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rdi), %r14d
	incq	%rdi
	movb	%r14b, (%rbp)
	addq	%r13, %rbp
	decl	%ebx
	incl	%eax
	jne	.LBB4_8
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_6:                                #   in Loop: Header=BB4_5 Depth=2
	movl	%r15d, %ebx
.LBB4_9:                                # %.prol.loopexit
                                        #   in Loop: Header=BB4_5 Depth=2
	cmpl	$7, %r10d
	jb	.LBB4_11
	.p2align	4, 0x90
.LBB4_10:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rdi), %eax
	movb	%al, (%rbp)
	movzbl	1(%rdi), %eax
	movb	%al, (%rbp,%r13)
	addq	%r13, %rbp
	movzbl	2(%rdi), %eax
	movb	%al, (%r13,%rbp)
	addq	%r13, %rbp
	movzbl	3(%rdi), %eax
	movb	%al, (%r13,%rbp)
	addq	%r13, %rbp
	movzbl	4(%rdi), %eax
	movb	%al, (%r13,%rbp)
	addq	%r13, %rbp
	movzbl	5(%rdi), %eax
	movb	%al, (%r13,%rbp)
	addq	%r13, %rbp
	movzbl	6(%rdi), %eax
	movb	%al, (%r13,%rbp)
	addq	%r13, %rbp
	movzbl	7(%rdi), %eax
	movb	%al, (%r13,%rbp)
	addq	%r13, %rbp
	addq	$8, %rdi
	addq	%r13, %rbp
	addl	$-8, %ebx
	jne	.LBB4_10
.LBB4_11:                               # %._crit_edge.us52
                                        #   in Loop: Header=BB4_5 Depth=2
	incq	%r12
	cmpq	%r9, %r12
	jne	.LBB4_5
.LBB4_12:                               # %._crit_edge41.us
                                        #   in Loop: Header=BB4_3 Depth=1
	incl	%edx
	addq	$8, %rcx
	cmpl	$1, %r8d
	leal	-1(%r8), %eax
	movl	%eax, %r8d
	jg	.LBB4_3
.LBB4_13:                               # %._crit_edge46
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	null_convert, .Lfunc_end4-null_convert
	.cfi_endproc

	.p2align	4, 0x90
	.type	ycck_cmyk_convert,@function
ycck_cmyk_convert:                      # @ycck_cmyk_convert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	testl	%r8d, %r8d
	jle	.LBB5_6
# BB#1:                                 # %.lr.ph92
	movl	128(%rdi), %eax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	testl	%eax, %eax
	je	.LBB5_6
# BB#2:                                 # %.lr.ph92.split.us.preheader
	movq	600(%rdi), %rax
	movq	408(%rdi), %r13
	movq	16(%rax), %r11
	movq	24(%rax), %r10
	movq	32(%rax), %r15
	movq	40(%rax), %r12
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph92.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
	movq	%r8, -16(%rsp)          # 8-byte Spill
	leal	-1(%r8), %eax
	movl	%eax, -40(%rsp)         # 4-byte Spill
	movl	%edx, %r9d
	movq	-24(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rax
	movq	8(%rsi), %rbx
	movq	(%rax,%r9,8), %r8
	movq	(%rbx,%r9,8), %rax
	movq	16(%rsi), %rbx
	movq	(%rbx,%r9,8), %rbx
	movq	24(%rsi), %rdi
	movq	(%rdi,%r9,8), %rdi
	incl	%edx
	movl	%edx, -36(%rsp)         # 4-byte Spill
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movq	(%rcx), %r9
	movq	-32(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_4:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r8), %esi
	movzbl	(%rbx), %edx
	xorl	$255, %esi
	movq	%r10, %rcx
	movl	%esi, %r10d
	subl	(%r11,%rdx,4), %r10d
	movslq	%r10d, %rbp
	movq	%rcx, %r10
	movzbl	(%r13,%rbp), %ecx
	movzbl	(%rax), %ebp
	movb	%cl, (%r9)
	movq	(%r15,%rdx,8), %rcx
	addq	(%r12,%rbp,8), %rcx
	shrq	$16, %rcx
	movl	%esi, %edx
	subl	%ecx, %edx
	movslq	%edx, %rcx
	movzbl	(%r13,%rcx), %ecx
	movb	%cl, 1(%r9)
	subl	(%r10,%rbp,4), %esi
	movslq	%esi, %rcx
	movzbl	(%r13,%rcx), %ecx
	movb	%cl, 2(%r9)
	movzbl	(%rdi), %ecx
	movb	%cl, 3(%r9)
	incq	%r8
	incq	%rax
	incq	%rbx
	incq	%rdi
	addq	$4, %r9
	decq	%r14
	jne	.LBB5_4
# BB#5:                                 # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	-8(%rsp), %rcx          # 8-byte Reload
	addq	$8, %rcx
	cmpl	$1, -16(%rsp)           # 4-byte Folded Reload
	movl	-40(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r8d
	movl	-36(%rsp), %edx         # 4-byte Reload
	jg	.LBB5_3
.LBB5_6:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	ycck_cmyk_convert, .Lfunc_end5-ycck_cmyk_convert
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
