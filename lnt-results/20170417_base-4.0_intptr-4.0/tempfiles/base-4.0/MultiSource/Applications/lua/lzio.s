	.text
	.file	"lzio.bc"
	.hidden	luaZ_fill
	.globl	luaZ_fill
	.p2align	4, 0x90
	.type	luaZ_fill,@function
luaZ_fill:                              # @luaZ_fill
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rdi
	leaq	8(%rsp), %rdx
	callq	*16(%rbx)
	testq	%rax, %rax
	movl	$-1, %ecx
	je	.LBB0_3
# BB#1:
	movq	8(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB0_3
# BB#2:
	decq	%rdx
	movq	%rdx, (%rbx)
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, 8(%rbx)
	movzbl	(%rax), %ecx
.LBB0_3:
	movl	%ecx, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	luaZ_fill, .Lfunc_end0-luaZ_fill
	.cfi_endproc

	.hidden	luaZ_lookahead
	.globl	luaZ_lookahead
	.p2align	4, 0x90
	.type	luaZ_lookahead,@function
luaZ_lookahead:                         # @luaZ_lookahead
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpq	$0, (%rbx)
	je	.LBB1_2
# BB#1:                                 # %._crit_edge
	movq	8(%rbx), %rax
	jmp	.LBB1_6
.LBB1_2:
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rdi
	leaq	8(%rsp), %rdx
	callq	*16(%rbx)
	testq	%rax, %rax
	je	.LBB1_4
# BB#3:
	movq	8(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB1_4
# BB#5:                                 # %luaZ_fill.exit
	movq	%rcx, (%rbx)
	movq	%rax, 8(%rbx)
.LBB1_6:
	movzbl	(%rax), %eax
	jmp	.LBB1_7
.LBB1_4:                                # %luaZ_fill.exit.thread
	movl	$-1, %eax
.LBB1_7:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	luaZ_lookahead, .Lfunc_end1-luaZ_lookahead
	.cfi_endproc

	.hidden	luaZ_init
	.globl	luaZ_init
	.p2align	4, 0x90
	.type	luaZ_init,@function
luaZ_init:                              # @luaZ_init
	.cfi_startproc
# BB#0:
	movq	%rdi, 32(%rsi)
	movq	%rdx, 16(%rsi)
	movq	%rcx, 24(%rsi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end2:
	.size	luaZ_init, .Lfunc_end2-luaZ_init
	.cfi_endproc

	.hidden	luaZ_read
	.globl	luaZ_read
	.p2align	4, 0x90
	.type	luaZ_read,@function
luaZ_read:                              # @luaZ_read
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -48
.Lcfi13:
	.cfi_offset %r12, -40
.Lcfi14:
	.cfi_offset %r13, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r13, %r13
	je	.LBB3_8
# BB#1:                                 # %.lr.ph
	movq	(%r15), %rbx
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	je	.LBB3_4
# BB#3:                                 # %.luaZ_lookahead.exit_crit_edge
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	8(%r15), %rax
	jmp	.LBB3_7
	.p2align	4, 0x90
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movq	24(%r15), %rsi
	movq	32(%r15), %rdi
	movq	%r12, %rdx
	callq	*16(%r15)
	testq	%rax, %rax
	je	.LBB3_9
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB3_9
# BB#6:                                 # %luaZ_fill.exit.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%rbx, (%r15)
	movq	%rax, 8(%r15)
.LBB3_7:                                # %luaZ_lookahead.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	%rbx, %r13
	cmovbeq	%r13, %rbx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	(%r15), %rax
	subq	%rbx, %rax
	movq	%rax, (%r15)
	addq	%rbx, 8(%r15)
	addq	%rbx, %r14
	subq	%rbx, %r13
	movq	%rax, %rbx
	jne	.LBB3_2
.LBB3_8:
	xorl	%r13d, %r13d
.LBB3_9:                                # %.thread
	movq	%r13, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	luaZ_read, .Lfunc_end3-luaZ_read
	.cfi_endproc

	.hidden	luaZ_openspace
	.globl	luaZ_openspace
	.p2align	4, 0x90
	.type	luaZ_openspace,@function
luaZ_openspace:                         # @luaZ_openspace
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %r14, -16
	movq	%rdx, %rax
	movq	%rsi, %rbx
	movq	16(%rbx), %rdx
	cmpq	%rax, %rdx
	jae	.LBB4_1
# BB#2:
	cmpq	$32, %rax
	movl	$32, %r14d
	cmovaq	%rax, %r14
	leaq	1(%r14), %rax
	cmpq	$-3, %rax
	ja	.LBB4_4
# BB#3:
	movq	(%rbx), %rsi
	movq	%r14, %rcx
	callq	luaM_realloc_
	jmp	.LBB4_5
.LBB4_1:                                # %._crit_edge
	movq	(%rbx), %rax
	jmp	.LBB4_6
.LBB4_4:
	callq	luaM_toobig
.LBB4_5:
	movq	%rax, (%rbx)
	movq	%r14, 16(%rbx)
.LBB4_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	luaZ_openspace, .Lfunc_end4-luaZ_openspace
	.cfi_endproc

	.hidden	luaM_realloc_
	.hidden	luaM_toobig

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
