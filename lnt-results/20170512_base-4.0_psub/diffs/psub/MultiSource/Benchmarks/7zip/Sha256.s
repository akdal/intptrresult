	.text
	.file	"Sha256.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1779033703              # 0x6a09e667
	.long	3144134277              # 0xbb67ae85
	.long	1013904242              # 0x3c6ef372
	.long	2773480762              # 0xa54ff53a
.LCPI0_1:
	.long	1359893119              # 0x510e527f
	.long	2600822924              # 0x9b05688c
	.long	528734635               # 0x1f83d9ab
	.long	1541459225              # 0x5be0cd19
	.text
	.globl	Sha256_Init
	.p2align	4, 0x90
	.type	Sha256_Init,@function
Sha256_Init:                            # @Sha256_Init
	.cfi_startproc
# BB#0:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1779033703,3144134277,1013904242,2773480762]
	movups	%xmm0, (%rdi)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [1359893119,2600822924,528734635,1541459225]
	movups	%xmm0, 16(%rdi)
	movq	$0, 32(%rdi)
	retq
.Lfunc_end0:
	.size	Sha256_Init, .Lfunc_end0-Sha256_Init
	.cfi_endproc

	.globl	Sha256_Update
	.p2align	4, 0x90
	.type	Sha256_Update,@function
Sha256_Update:                          # @Sha256_Update
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.LBB1_6
# BB#1:                                 # %.lr.ph.preheader
	movq	32(%rbx), %rcx
	movl	%ecx, %eax
	andl	$63, %eax
	movl	$1, %r15d
	subq	%rdx, %r15
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_5:                                # %.backedge..lr.ph_crit_edge
                                        #   in Loop: Header=BB1_2 Depth=1
	incq	%r14
	movq	32(%rbx), %rcx
	incq	%r15
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14), %edx
	movl	%eax, %esi
	incl	%eax
	movb	%dl, 40(%rbx,%rsi)
	incq	%rcx
	movq	%rcx, 32(%rbx)
	cmpl	$64, %eax
	jne	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	%rbx, %rdi
	callq	Sha256_WriteByteBlock
	xorl	%eax, %eax
.LBB1_4:                                # %.backedge
                                        #   in Loop: Header=BB1_2 Depth=1
	testq	%r15, %r15
	jne	.LBB1_5
.LBB1_6:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	Sha256_Update, .Lfunc_end1-Sha256_Update
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.text
	.p2align	4, 0x90
	.type	Sha256_WriteByteBlock,@function
Sha256_WriteByteBlock:                  # @Sha256_WriteByteBlock
	.cfi_startproc
# BB#0:                                 # %vector.body
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi9:
	.cfi_def_cfa_offset 64
.Lcfi10:
	.cfi_offset %rbx, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movdqu	40(%rdi), %xmm1
	movdqa	.LCPI2_0(%rip), %xmm0   # xmm0 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	movdqa	%xmm1, %xmm2
	pslld	$24, %xmm2
	movdqa	%xmm1, %xmm3
	psrlw	$8, %xmm3
	pand	%xmm0, %xmm3
	pslld	$16, %xmm3
	por	%xmm2, %xmm3
	movdqa	%xmm1, %xmm2
	psrld	$16, %xmm2
	pand	%xmm0, %xmm2
	pslld	$8, %xmm2
	psrld	$24, %xmm1
	por	%xmm2, %xmm1
	por	%xmm3, %xmm1
	movdqa	%xmm1, -32(%rsp)
	movdqu	56(%rdi), %xmm1
	movdqa	%xmm1, %xmm2
	pslld	$24, %xmm2
	movdqa	%xmm1, %xmm3
	psrlw	$8, %xmm3
	pand	%xmm0, %xmm3
	pslld	$16, %xmm3
	por	%xmm2, %xmm3
	movdqa	%xmm1, %xmm2
	psrld	$16, %xmm2
	pand	%xmm0, %xmm2
	pslld	$8, %xmm2
	psrld	$24, %xmm1
	por	%xmm2, %xmm1
	por	%xmm3, %xmm1
	movdqa	%xmm1, -16(%rsp)
	movdqu	72(%rdi), %xmm1
	movdqa	%xmm1, %xmm2
	pslld	$24, %xmm2
	movdqa	%xmm1, %xmm3
	psrlw	$8, %xmm3
	pand	%xmm0, %xmm3
	pslld	$16, %xmm3
	por	%xmm2, %xmm3
	movdqa	%xmm1, %xmm2
	psrld	$16, %xmm2
	pand	%xmm0, %xmm2
	pslld	$8, %xmm2
	psrld	$24, %xmm1
	por	%xmm2, %xmm1
	por	%xmm3, %xmm1
	movdqa	%xmm1, (%rsp)
	movdqu	88(%rdi), %xmm1
	movdqa	%xmm1, %xmm2
	pslld	$24, %xmm2
	movdqa	%xmm1, %xmm3
	psrlw	$8, %xmm3
	pand	%xmm0, %xmm3
	pslld	$16, %xmm3
	por	%xmm2, %xmm3
	movdqa	%xmm1, %xmm2
	psrld	$16, %xmm2
	pand	%xmm0, %xmm2
	pslld	$8, %xmm2
	psrld	$24, %xmm1
	por	%xmm2, %xmm1
	por	%xmm3, %xmm1
	movdqa	%xmm1, 16(%rsp)
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movaps	%xmm1, -112(%rsp)
	movaps	%xmm0, -128(%rsp)
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_1:                                # %.preheader60.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_2:                                #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$4, %eax
	subl	%ebx, %eax
	andl	$7, %eax
	movl	-128(%rsp,%rax,4), %eax
	movl	%eax, %esi
	movl	%eax, %edx
	movl	$6, %ecx
	subl	%ebx, %ecx
	andl	$7, %ecx
	movl	-128(%rsp,%rcx,4), %r9d
	movl	$5, %ecx
	subl	%ebx, %ecx
	andl	$7, %ecx
	movl	-128(%rsp,%rcx,4), %r11d
	xorl	%r9d, %r11d
	andl	%eax, %r11d
	roll	$26, %eax
	roll	$21, %esi
	xorl	%eax, %esi
	roll	$7, %edx
	xorl	%esi, %edx
	xorl	%r9d, %r11d
	leaq	(%rbx,%r8), %rax
	testq	%r8, %r8
	movl	K(,%rax,4), %r9d
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=2
	leal	14(%rbx), %eax
	andl	$15, %eax
	movl	-96(%rsp,%rax,4), %esi
	movl	%esi, %eax
	roll	$15, %eax
	movl	%esi, %ecx
	roll	$13, %ecx
	shrl	$10, %esi
	xorl	%ecx, %esi
	xorl	%eax, %esi
	leal	9(%rbx), %r14d
	andl	$15, %r14d
	leaq	1(%rbx), %r10
	movl	%r10d, %eax
	andl	$15, %eax
	movl	-96(%rsp,%rax,4), %ebp
	movl	%ebp, %eax
	roll	$25, %eax
	movl	%ebp, %ecx
	roll	$14, %ecx
	shrl	$3, %ebp
	xorl	%ecx, %ebp
	xorl	%eax, %ebp
	movl	-96(%rsp,%rbx,4), %eax
	addl	-96(%rsp,%r14,4), %eax
	addl	%esi, %eax
	addl	%ebp, %eax
	movl	%eax, -96(%rsp,%rbx,4)
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=2
	movl	-32(%rsp,%rbx,4), %eax
	movl	%eax, -96(%rsp,%rbx,4)
	leaq	1(%rbx), %r10
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=2
	movl	$7, %r14d
	subl	%ebx, %r14d
	andl	$7, %r14d
	addl	%r9d, %edx
	addl	%r11d, %edx
	addl	%eax, %edx
	addl	-128(%rsp,%r14,4), %edx
	movl	%edx, -128(%rsp,%r14,4)
	movl	$3, %eax
	subl	%ebx, %eax
	andl	$7, %eax
	addl	%edx, -128(%rsp,%rax,4)
	movl	$1, %eax
	subl	%ebx, %eax
	movl	$2, %ecx
	subl	%ebx, %ecx
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	negl	%ebx
	andl	$7, %ebx
	movl	-128(%rsp,%rbx,4), %edx
	movl	%edx, %ebp
	movl	%edx, %ebx
	andl	$7, %eax
	movl	-128(%rsp,%rax,4), %eax
	movl	%eax, %esi
	andl	%edx, %esi
	orl	%edx, %eax
	roll	$30, %edx
	roll	$19, %ebp
	xorl	%edx, %ebp
	roll	$10, %ebx
	xorl	%ebp, %ebx
	andl	$7, %ecx
	andl	-128(%rsp,%rcx,4), %eax
	orl	%esi, %eax
	addl	-128(%rsp,%r14,4), %eax
	addl	%ebx, %eax
	movl	%eax, -128(%rsp,%r14,4)
	cmpq	$16, %r10
	movq	%r10, %rbx
	jne	.LBB2_2
# BB#6:                                 #   in Loop: Header=BB2_1 Depth=1
	addq	$16, %r8
	cmpq	$64, %r8
	jb	.LBB2_1
# BB#7:                                 # %Sha256_Transform.exit
	movdqu	(%rdi), %xmm0
	paddd	-128(%rsp), %xmm0
	movdqu	%xmm0, (%rdi)
	movdqu	16(%rdi), %xmm0
	paddd	-112(%rsp), %xmm0
	movdqu	%xmm0, 16(%rdi)
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	Sha256_WriteByteBlock, .Lfunc_end2-Sha256_WriteByteBlock
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1779033703              # 0x6a09e667
	.long	3144134277              # 0xbb67ae85
	.long	1013904242              # 0x3c6ef372
	.long	2773480762              # 0xa54ff53a
.LCPI3_1:
	.long	1359893119              # 0x510e527f
	.long	2600822924              # 0x9b05688c
	.long	528734635               # 0x1f83d9ab
	.long	1541459225              # 0x5be0cd19
	.text
	.globl	Sha256_Final
	.p2align	4, 0x90
	.type	Sha256_Final,@function
Sha256_Final:                           # @Sha256_Final
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	32(%rbx), %r15
	movl	%r15d, %ecx
	andl	$63, %ecx
	leal	1(%rcx), %eax
	movb	$-128, 40(%rbx,%rcx)
	cmpl	$56, %eax
	jne	.LBB3_2
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_4:                                # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	%eax, %ecx
	incl	%eax
	movb	$0, 40(%rbx,%rcx)
	cmpl	$56, %eax
	jne	.LBB3_2
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_3:                                # %.backedge.thread
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%rbx, %rdi
	callq	Sha256_WriteByteBlock
	movb	$0, 40(%rbx)
	movl	$1, %eax
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	andl	$63, %eax
	jne	.LBB3_4
	jmp	.LBB3_3
.LBB3_5:                                # %.preheader.preheader
	movq	%r15, %rax
	shrq	$53, %rax
	movb	%al, 96(%rbx)
	movq	%r15, %rax
	shrq	$45, %rax
	movb	%al, 97(%rbx)
	movq	%r15, %rax
	shrq	$37, %rax
	movb	%al, 98(%rbx)
	movq	%r15, %rax
	shrq	$29, %rax
	movb	%al, 99(%rbx)
	movq	%r15, %rax
	shrq	$21, %rax
	movb	%al, 100(%rbx)
	movq	%r15, %rax
	shrq	$13, %rax
	movb	%al, 101(%rbx)
	movq	%r15, %rax
	shrq	$5, %rax
	movb	%al, 102(%rbx)
	shlb	$3, %r15b
	movb	%r15b, 103(%rbx)
	movq	%rbx, %rdi
	callq	Sha256_WriteByteBlock
	movb	3(%rbx), %al
	movb	%al, (%r14)
	movb	2(%rbx), %al
	movb	%al, 1(%r14)
	movb	1(%rbx), %al
	movb	%al, 2(%r14)
	movb	(%rbx), %al
	movb	%al, 3(%r14)
	movb	7(%rbx), %al
	movb	%al, 4(%r14)
	movb	6(%rbx), %al
	movb	%al, 5(%r14)
	movb	5(%rbx), %al
	movb	%al, 6(%r14)
	movb	4(%rbx), %al
	movb	%al, 7(%r14)
	movb	11(%rbx), %al
	movb	%al, 8(%r14)
	movb	10(%rbx), %al
	movb	%al, 9(%r14)
	movb	9(%rbx), %al
	movb	%al, 10(%r14)
	movb	8(%rbx), %al
	movb	%al, 11(%r14)
	movb	15(%rbx), %al
	movb	%al, 12(%r14)
	movb	14(%rbx), %al
	movb	%al, 13(%r14)
	movb	13(%rbx), %al
	movb	%al, 14(%r14)
	movb	12(%rbx), %al
	movb	%al, 15(%r14)
	movb	19(%rbx), %al
	movb	%al, 16(%r14)
	movb	18(%rbx), %al
	movb	%al, 17(%r14)
	movb	17(%rbx), %al
	movb	%al, 18(%r14)
	movb	16(%rbx), %al
	movb	%al, 19(%r14)
	movb	23(%rbx), %al
	movb	%al, 20(%r14)
	movb	22(%rbx), %al
	movb	%al, 21(%r14)
	movb	21(%rbx), %al
	movb	%al, 22(%r14)
	movb	20(%rbx), %al
	movb	%al, 23(%r14)
	movb	27(%rbx), %al
	movb	%al, 24(%r14)
	movb	26(%rbx), %al
	movb	%al, 25(%r14)
	movb	25(%rbx), %al
	movb	%al, 26(%r14)
	movb	24(%rbx), %al
	movb	%al, 27(%r14)
	movb	31(%rbx), %al
	movb	%al, 28(%r14)
	movb	30(%rbx), %al
	movb	%al, 29(%r14)
	movb	29(%rbx), %al
	movb	%al, 30(%r14)
	movb	28(%rbx), %al
	movb	%al, 31(%r14)
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [1779033703,3144134277,1013904242,2773480762]
	movups	%xmm0, (%rbx)
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [1359893119,2600822924,528734635,1541459225]
	movups	%xmm0, 16(%rbx)
	movq	$0, 32(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	Sha256_Final, .Lfunc_end3-Sha256_Final
	.cfi_endproc

	.type	K,@object               # @K
	.section	.rodata,"a",@progbits
	.p2align	4
K:
	.long	1116352408              # 0x428a2f98
	.long	1899447441              # 0x71374491
	.long	3049323471              # 0xb5c0fbcf
	.long	3921009573              # 0xe9b5dba5
	.long	961987163               # 0x3956c25b
	.long	1508970993              # 0x59f111f1
	.long	2453635748              # 0x923f82a4
	.long	2870763221              # 0xab1c5ed5
	.long	3624381080              # 0xd807aa98
	.long	310598401               # 0x12835b01
	.long	607225278               # 0x243185be
	.long	1426881987              # 0x550c7dc3
	.long	1925078388              # 0x72be5d74
	.long	2162078206              # 0x80deb1fe
	.long	2614888103              # 0x9bdc06a7
	.long	3248222580              # 0xc19bf174
	.long	3835390401              # 0xe49b69c1
	.long	4022224774              # 0xefbe4786
	.long	264347078               # 0xfc19dc6
	.long	604807628               # 0x240ca1cc
	.long	770255983               # 0x2de92c6f
	.long	1249150122              # 0x4a7484aa
	.long	1555081692              # 0x5cb0a9dc
	.long	1996064986              # 0x76f988da
	.long	2554220882              # 0x983e5152
	.long	2821834349              # 0xa831c66d
	.long	2952996808              # 0xb00327c8
	.long	3210313671              # 0xbf597fc7
	.long	3336571891              # 0xc6e00bf3
	.long	3584528711              # 0xd5a79147
	.long	113926993               # 0x6ca6351
	.long	338241895               # 0x14292967
	.long	666307205               # 0x27b70a85
	.long	773529912               # 0x2e1b2138
	.long	1294757372              # 0x4d2c6dfc
	.long	1396182291              # 0x53380d13
	.long	1695183700              # 0x650a7354
	.long	1986661051              # 0x766a0abb
	.long	2177026350              # 0x81c2c92e
	.long	2456956037              # 0x92722c85
	.long	2730485921              # 0xa2bfe8a1
	.long	2820302411              # 0xa81a664b
	.long	3259730800              # 0xc24b8b70
	.long	3345764771              # 0xc76c51a3
	.long	3516065817              # 0xd192e819
	.long	3600352804              # 0xd6990624
	.long	4094571909              # 0xf40e3585
	.long	275423344               # 0x106aa070
	.long	430227734               # 0x19a4c116
	.long	506948616               # 0x1e376c08
	.long	659060556               # 0x2748774c
	.long	883997877               # 0x34b0bcb5
	.long	958139571               # 0x391c0cb3
	.long	1322822218              # 0x4ed8aa4a
	.long	1537002063              # 0x5b9cca4f
	.long	1747873779              # 0x682e6ff3
	.long	1955562222              # 0x748f82ee
	.long	2024104815              # 0x78a5636f
	.long	2227730452              # 0x84c87814
	.long	2361852424              # 0x8cc70208
	.long	2428436474              # 0x90befffa
	.long	2756734187              # 0xa4506ceb
	.long	3204031479              # 0xbef9a3f7
	.long	3329325298              # 0xc67178f2
	.size	K, 256


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
