	.text
	.file	"explicit_gop.bc"
	.globl	create_hierarchy
	.p2align	4, 0x90
	.type	create_hierarchy,@function
create_hierarchy:                       # @create_hierarchy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	input(%rip), %rsi
	movslq	2096(%rsi), %r11
	cmpl	$1, 2968(%rsi)
	jne	.LBB0_10
# BB#1:                                 # %.preheader
	testl	%r11d, %r11d
	jle	.LBB0_9
# BB#2:                                 # %.lr.ph
	movl	%r11d, %ecx
	shrl	$31, %ecx
	addl	%r11d, %ecx
	sarl	%ecx
	movq	gop_structure(%rip), %rax
	movslq	%ecx, %rcx
	addq	$16, %rax
	leal	(%rcx,%rcx), %r9d
	negl	%r9d
	movl	$1, %edx
	xorl	%r8d, %r8d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movl	$1, -16(%rax)
	cmpq	%rcx, %rbp
	jge	.LBB0_7
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	%edx, -12(%rax)
	movl	$1, (%rax)
	movl	$2, -8(%rax)
	movl	2104(%rsi), %edi
	movl	$-1, %ebx
	cmpl	$0, 2972(%rsi)
	jne	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	2108(%rsi), %ebx
.LBB0_6:                                #   in Loop: Header=BB0_3 Depth=1
	addl	%edi, %ebx
	cmovsl	%r8d, %ebx
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_3 Depth=1
	leal	-1(%r9,%rdx), %edi
	movl	%edi, -12(%rax)
	movl	$0, (%rax)
	movl	$0, -8(%rax)
	movl	2104(%rsi), %ebx
.LBB0_8:                                #   in Loop: Header=BB0_3 Depth=1
	movl	%ebx, -4(%rax)
	incq	%rbp
	addl	$2, %edx
	addq	$24, %rax
	cmpq	%r11, %rbp
	jl	.LBB0_3
.LBB0_9:                                # %._crit_edge
	movq	img(%rip), %rax
	movl	$2, 15612(%rax)
	jmp	.LBB0_31
.LBB0_10:
	movl	%r11d, %r8d
	leal	1(%r8), %r15d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_11:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %r12d
	movl	%r15d, %edx
	movl	%r12d, %ecx
	sarl	%cl, %edx
	leal	1(%r12), %eax
	cmpl	$1, %edx
	jg	.LBB0_11
# BB#12:
	movq	img(%rip), %rax
	movl	%r12d, 15612(%rax)
	testl	%r8d, %r8d
	jle	.LBB0_19
# BB#13:                                # %.lr.ph102
	movq	gop_structure(%rip), %rdi
	movl	2104(%rsi), %ecx
	testb	$1, %r11b
	jne	.LBB0_15
# BB#14:
	xorl	%edx, %edx
	cmpl	$1, %r11d
	jne	.LBB0_17
	jmp	.LBB0_19
.LBB0_15:
	movl	$0, 4(%rdi)
	movl	$1, (%rdi)
	movl	$0, 16(%rdi)
	movl	$0, 8(%rdi)
	movl	%ecx, 12(%rdi)
	movl	$1, %edx
	cmpl	$1, %r11d
	je	.LBB0_19
.LBB0_17:                               # %.lr.ph102.new
	leaq	(%rdx,%rdx,2), %rax
	leaq	40(%rdi,%rax,8), %rsi
	.p2align	4, 0x90
.LBB0_18:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, -36(%rsi)
	movl	$1, -40(%rsi)
	movl	$0, -24(%rsi)
	movl	$0, -32(%rsi)
	movl	%ecx, -28(%rsi)
	leal	1(%rdx), %eax
	movl	%eax, -12(%rsi)
	movl	$1, -16(%rsi)
	movl	$0, (%rsi)
	movl	$0, -8(%rsi)
	movl	%ecx, -4(%rsi)
	addq	$2, %rdx
	addq	$48, %rsi
	cmpq	%r11, %rdx
	jl	.LBB0_18
.LBB0_19:                               # %.preheader86
	cmpl	$2, %r12d
	jl	.LBB0_25
# BB#20:                                # %.lr.ph99
	movq	gop_structure(%rip), %r9
	movq	input(%rip), %r14
	addq	$16, %r9
	movl	$1, %ecx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_21:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_24 Depth 2
                                        #     Child Loop BB0_33 Depth 2
	movl	$1, %eax
	shll	%cl, %eax
	movl	%r15d, %edi
	subl	%eax, %edi
	cmpl	%edi, %eax
	jg	.LBB0_34
# BB#22:                                # %.lr.ph94
                                        #   in Loop: Header=BB0_21 Depth=1
	leal	-1(%rax), %esi
	cmpl	$0, 2972(%r14)
	movl	2104(%r14), %edx
	movslq	%esi, %rbx
	movslq	%eax, %rsi
	movslq	%edi, %rdi
	je	.LBB0_32
# BB#23:                                # %.lr.ph94.split.preheader
                                        #   in Loop: Header=BB0_21 Depth=1
	subl	%ecx, %edx
	cmovsl	%r10d, %edx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r9,%rax,8), %rbp
	leaq	(,%rsi,8), %rax
	leaq	(%rax,%rax,2), %rax
	.p2align	4, 0x90
.LBB0_24:                               # %.lr.ph94.split
                                        #   Parent Loop BB0_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, (%rbp)
	movl	$2, -8(%rbp)
	movl	%edx, -4(%rbp)
	addq	%rsi, %rbx
	addq	%rax, %rbp
	cmpq	%rdi, %rbx
	jl	.LBB0_24
	jmp	.LBB0_34
.LBB0_32:                               # %.lr.ph94.split.us.preheader
                                        #   in Loop: Header=BB0_21 Depth=1
	addl	2108(%r14), %edx
	cmovsl	%r10d, %edx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r9,%rax,8), %rbp
	leaq	(,%rsi,8), %rax
	leaq	(%rax,%rax,2), %rax
	.p2align	4, 0x90
.LBB0_33:                               # %.lr.ph94.split.us
                                        #   Parent Loop BB0_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, (%rbp)
	movl	$2, -8(%rbp)
	movl	%edx, -4(%rbp)
	addq	%rsi, %rbx
	addq	%rax, %rbp
	cmpq	%rdi, %rbx
	jl	.LBB0_33
	.p2align	4, 0x90
.LBB0_34:                               # %._crit_edge95
                                        #   in Loop: Header=BB0_21 Depth=1
	incl	%ecx
	cmpl	%ecx, %r12d
	jne	.LBB0_21
.LBB0_25:                               # %.preheader85
	cmpl	$2, %r11d
	jl	.LBB0_31
# BB#26:                                # %.lr.ph89.preheader.preheader
	movl	$1, %eax
	movl	$2, %ecx
	movl	$24, %edx
	.p2align	4, 0x90
.LBB0_27:                               # %.lr.ph89.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_28 Depth 2
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph89
                                        #   Parent Loop BB0_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	gop_structure(%rip), %rbp
	movl	16(%rbp,%rsi), %ebx
	cmpl	-8(%rbp,%rsi), %ebx
	jle	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_28 Depth=2
	movq	-8(%rbp,%rsi), %rbx
	movq	%rbx, -16(%rsp)
	movups	-24(%rbp,%rsi), %xmm0
	movaps	%xmm0, -32(%rsp)
	movq	16(%rbp,%rsi), %rbx
	movq	%rbx, -8(%rbp,%rsi)
	movups	(%rbp,%rsi), %xmm0
	movups	%xmm0, -24(%rbp,%rsi)
	movq	gop_structure(%rip), %rbp
	movq	-16(%rsp), %rbx
	movq	%rbx, 16(%rbp,%rsi)
	movaps	-32(%rsp), %xmm0
	movups	%xmm0, (%rbp,%rsi)
	decq	%rdi
	addq	$-24, %rsi
	cmpq	$1, %rdi
	jg	.LBB0_28
.LBB0_30:                               # %.critedge
                                        #   in Loop: Header=BB0_27 Depth=1
	incq	%rax
	incq	%rcx
	addq	$24, %rdx
	cmpq	%r8, %rax
	jne	.LBB0_27
.LBB0_31:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	create_hierarchy, .Lfunc_end0-create_hierarchy
	.cfi_endproc

	.globl	init_gop_structure
	.p2align	4, 0x90
	.type	init_gop_structure,@function
init_gop_structure:                     # @init_gop_structure
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movq	input(%rip), %rax
	cmpl	$3, 2968(%rax)
	leaq	2096(%rax), %rcx
	leaq	20(%rax), %rax
	cmoveq	%rax, %rcx
	movl	(%rcx), %eax
	cmpl	$9, %eax
	movl	$10, %edi
	cmovgl	%eax, %edi
	movl	$24, %esi
	callq	calloc
	movq	%rax, gop_structure(%rip)
	testq	%rax, %rax
	je	.LBB1_2
# BB#1:
	popq	%rax
	retq
.LBB1_2:
	movl	$.L.str.2, %edi
	popq	%rax
	jmp	no_mem_exit             # TAILCALL
.Lfunc_end1:
	.size	init_gop_structure, .Lfunc_end1-init_gop_structure
	.cfi_endproc

	.globl	clear_gop_structure
	.p2align	4, 0x90
	.type	clear_gop_structure,@function
clear_gop_structure:                    # @clear_gop_structure
	.cfi_startproc
# BB#0:
	movq	gop_structure(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB2_1:
	retq
.Lfunc_end2:
	.size	clear_gop_structure, .Lfunc_end2-clear_gop_structure
	.cfi_endproc

	.globl	interpret_gop_structure
	.p2align	4, 0x90
	.type	interpret_gop_structure,@function
interpret_gop_structure:                # @interpret_gop_structure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 80
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	input(%rip), %rdi
	addq	$2976, %rdi             # imm = 0xBA0
	callq	strlen
	movq	%rax, %r14
	testl	%r14d, %r14d
	jle	.LBB3_57
# BB#1:                                 # %.lr.ph96
	leal	-2(%r14), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movabsq	$4294967297, %r12       # imm = 0x100000001
	movl	$0, %r13d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_22 Depth 2
	movl	%r13d, %r13d
	testl	%ebp, %ebp
	je	.LBB3_3
# BB#11:                                #   in Loop: Header=BB3_2 Depth=1
	testl	%ebx, %ebx
	je	.LBB3_16
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$1, %ebx
	jne	.LBB3_50
# BB#13:                                #   in Loop: Header=BB3_2 Depth=1
	movl	4(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB3_14
# BB#37:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$1, %ecx
	movl	8(%rsp), %eax           # 4-byte Reload
	jne	.LBB3_47
# BB#38:                                #   in Loop: Header=BB3_2 Depth=1
	testl	%eax, %eax
	jne	.LBB3_47
# BB#39:                                #   in Loop: Header=BB3_2 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rdx
	movq	input(%rip), %rax
	movslq	%r15d, %rcx
	movsbq	2976(%rax,%rcx), %rsi
	testb	$8, 1(%rdx,%rsi,2)
	jne	.LBB3_40
# BB#46:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.9, %edx
.LBB3_36:                               # %.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	jmp	.LBB3_34
	.p2align	4, 0x90
.LBB3_3:                                #   in Loop: Header=BB3_2 Depth=1
	movq	input(%rip), %rax
	movslq	%r15d, %rcx
	movsbl	2976(%rax,%rcx), %eax
	addl	$-66, %eax
	cmpl	$46, %eax
	ja	.LBB3_10
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	btq	%rax, %r12
	jb	.LBB3_8
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movabsq	$549755814016, %rcx     # imm = 0x8000000080
	btq	%rax, %rcx
	jb	.LBB3_9
# BB#6:                                 #   in Loop: Header=BB3_2 Depth=1
	movabsq	$70368744194048, %rcx   # imm = 0x400000004000
	btq	%rax, %rcx
	jae	.LBB3_10
# BB#7:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	gop_structure(%rip), %rax
	movslq	%r13d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	$0, (%rax,%rcx,8)
	movl	$1, %ebp
	jmp	.LBB3_50
	.p2align	4, 0x90
.LBB3_16:                               #   in Loop: Header=BB3_2 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rdx
	movq	input(%rip), %rax
	movslq	%r15d, %rcx
	movsbq	2976(%rax,%rcx), %rsi
	testb	$8, 1(%rdx,%rsi,2)
	jne	.LBB3_17
# BB#26:                                #   in Loop: Header=BB3_2 Depth=1
	xorl	%ebx, %ebx
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.7, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	jmp	.LBB3_50
.LBB3_8:                                #   in Loop: Header=BB3_2 Depth=1
	movq	gop_structure(%rip), %rax
	movslq	%r13d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	$1, (%rax,%rcx,8)
	movl	$1, %ebp
	jmp	.LBB3_50
.LBB3_9:                                #   in Loop: Header=BB3_2 Depth=1
	movq	gop_structure(%rip), %rax
	movslq	%r13d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	$2, (%rax,%rcx,8)
	movl	$1, %ebp
	jmp	.LBB3_50
.LBB3_17:                               #   in Loop: Header=BB3_2 Depth=1
	leaq	2976(%rax,%rcx), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	leaq	12(%rsp), %rdx
	callq	sscanf
	movl	12(%rsp), %eax
	movq	gop_structure(%rip), %rcx
	movslq	%r13d, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movl	%eax, 4(%rcx,%rdx,8)
	movq	input(%rip), %rcx
	movl	20(%rcx), %ecx
	testl	%eax, %eax
	js	.LBB3_19
# BB#18:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	%ecx, %eax
	jl	.LBB3_20
.LBB3_19:                               # %._crit_edge
                                        #   in Loop: Header=BB3_2 Depth=1
	decl	%ecx
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
.LBB3_20:                               # %.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	testl	%r13d, %r13d
	jle	.LBB3_49
# BB#21:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$4, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_22:                               # %.lr.ph
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	gop_structure(%rip), %rax
	movl	(%rax,%r12), %ecx
	cmpl	12(%rsp), %ecx
	jne	.LBB3_24
# BB#23:                                #   in Loop: Header=BB3_22 Depth=2
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.6, %edx
	xorl	%eax, %eax
	movl	%r13d, %r8d
	movl	%ebx, %r9d
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
.LBB3_24:                               #   in Loop: Header=BB3_22 Depth=2
	incq	%rbx
	addq	$24, %r12
	cmpq	%rbx, %r13
	jne	.LBB3_22
# BB#25:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %ebx
	movabsq	$4294967297, %r12       # imm = 0x100000001
	jmp	.LBB3_50
.LBB3_14:                               #   in Loop: Header=BB3_2 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movq	input(%rip), %rax
	movslq	%r15d, %rdx
	movsbq	2976(%rax,%rdx), %rax
	testb	$8, 1(%rcx,%rax,2)
	jne	.LBB3_15
# BB#27:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$100, %eax
	jg	.LBB3_31
# BB#28:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$69, %eax
	je	.LBB3_33
# BB#29:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$82, %eax
	je	.LBB3_30
	jmp	.LBB3_35
.LBB3_47:                               # %.thread
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpl	$1, %eax
	jne	.LBB3_49
# BB#48:                                # %.thread
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpl	$1, %ecx
	jne	.LBB3_49
# BB#51:                                #   in Loop: Header=BB3_2 Depth=1
	callq	__ctype_b_loc
	cmpl	16(%rsp), %r15d         # 4-byte Folded Reload
	jge	.LBB3_53
# BB#52:                                #   in Loop: Header=BB3_2 Depth=1
	movq	(%rax), %rcx
	movq	input(%rip), %rax
	movslq	%r15d, %rdx
	movsbq	2976(%rax,%rdx), %rdx
	movzwl	(%rcx,%rdx,2), %ecx
	andl	$2048, %ecx             # imm = 0x800
	testw	%cx, %cx
	jne	.LBB3_53
# BB#54:                                #   in Loop: Header=BB3_2 Depth=1
	decl	%r15d
	incl	%r13d
	xorl	%ebp, %ebp
	cmpl	20(%rax), %r13d
	jl	.LBB3_55
# BB#56:                                #   in Loop: Header=BB3_2 Depth=1
	xorl	%ebp, %ebp
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.10, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
.LBB3_55:                               #   in Loop: Header=BB3_2 Depth=1
	xorl	%ebx, %ebx
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	jmp	.LBB3_50
.LBB3_10:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	movl	$1, %ebp
	jmp	.LBB3_50
.LBB3_15:                               # %..loopexit_crit_edge116
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
.LBB3_49:                               # %.thread..loopexit_crit_edge
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %ebx
	jmp	.LBB3_50
.LBB3_40:                               #   in Loop: Header=BB3_2 Depth=1
	leaq	2976(%rax,%rcx), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	leaq	20(%rsp), %rdx
	callq	sscanf
	movq	gop_structure(%rip), %rax
	movslq	%r13d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	(%rax,%rcx,8), %esi
	cmpl	$2, %esi
	jne	.LBB3_42
# BB#41:                                #   in Loop: Header=BB3_2 Depth=1
	movq	input(%rip), %rdx
	addq	$12, %rdx
	jmp	.LBB3_45
.LBB3_31:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	$114, %eax
	je	.LBB3_30
# BB#32:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$101, %eax
	jne	.LBB3_35
.LBB3_33:                               #   in Loop: Header=BB3_2 Depth=1
	movq	gop_structure(%rip), %rax
	movslq	%r13d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	$0, 8(%rax,%rcx,8)
	movl	$0, 16(%rax,%rcx,8)
	jmp	.LBB3_34
.LBB3_42:                               #   in Loop: Header=BB3_2 Depth=1
	movq	input(%rip), %rdx
	testl	%esi, %esi
	je	.LBB3_43
# BB#44:                                #   in Loop: Header=BB3_2 Depth=1
	addq	$2104, %rdx             # imm = 0x838
	jmp	.LBB3_45
.LBB3_30:                               #   in Loop: Header=BB3_2 Depth=1
	movq	gop_structure(%rip), %rax
	movslq	%r13d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	$2, 8(%rax,%rcx,8)
	movl	$1, 16(%rax,%rcx,8)
	movq	img(%rip), %rax
	movl	$2, 15612(%rax)
.LBB3_34:                               # %.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %ebx
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB3_50
.LBB3_35:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.8, %edx
	jmp	.LBB3_36
.LBB3_43:                               #   in Loop: Header=BB3_2 Depth=1
	addq	$16, %rdx
.LBB3_45:                               #   in Loop: Header=BB3_2 Depth=1
	movq	img(%rip), %rsi
	xorl	%edi, %edi
	subl	15452(%rsi), %edi
	movl	20(%rsp), %esi
	addl	(%rdx), %esi
	cmpl	%edi, %esi
	cmovll	%edi, %esi
	cmpl	$52, %esi
	movl	$51, %edx
	cmovgel	%edx, %esi
	movl	%esi, 12(%rax,%rcx,8)
.LBB3_53:                               # %..loopexit_crit_edge
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %ebx
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB3_50:                               # %.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	incl	%r15d
	cmpl	%r14d, %r15d
	jl	.LBB3_2
	jmp	.LBB3_58
.LBB3_57:
	xorl	%r13d, %r13d
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.11, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
.LBB3_58:                               # %.loopexit88
	incl	%r13d
	movq	input(%rip), %rax
	movl	%r13d, 2096(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	interpret_gop_structure, .Lfunc_end3-interpret_gop_structure
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	encode_enhancement_layer
	.p2align	4, 0x90
	.type	encode_enhancement_layer,@function
encode_enhancement_layer:               # @encode_enhancement_layer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	input(%rip), %r9
	movl	2096(%r9), %eax
	testl	%eax, %eax
	movq	img(%rip), %rsi
	je	.LBB4_42
# BB#1:
	movl	(%rsi), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	jle	.LBB4_42
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$0, 2100(%r9)
	sete	%cl
	movl	%ecx, 20(%rsi)
	xorl	%ecx, %ecx
	cmpl	$0, 4736(%r9)
	setne	%cl
	movl	%ecx, 15248(%rsi)
	movl	2964(%r9), %ebp
	movl	2968(%r9), %ecx
	cmpl	$1, %ebp
	jne	.LBB4_3
# BB#6:
	movl	$0, 15360(%rsi)
	testl	%ecx, %ecx
	movl	$1, 14364(%rsi)
	jne	.LBB4_7
# BB#18:                                # %.preheader
	movl	$1, %ebp
	testl	%eax, %eax
	jg	.LBB4_20
	jmp	.LBB4_42
.LBB4_3:
	testl	%ecx, %ecx
	je	.LBB4_5
# BB#4:                                 # %.thread82
	movl	$0, 15360(%rsi)
	movl	$1, 14364(%rsi)
.LBB4_7:                                # %.preheader67
	testl	%eax, %eax
	jle	.LBB4_41
# BB#8:                                 # %.lr.ph69.preheader
	movl	$1, %r11d
	movq	gop_structure(%rip), %r8
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph69
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 15360(%rsi)
	leal	-1(%r11), %ecx
	movslq	%ecx, %rcx
	leaq	(%rcx,%rcx,2), %r10
	movl	(%r8,%r10,8), %ecx
	movl	%ecx, 20(%rsi)
	cmpl	$1, %r14d
	jne	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_9 Depth=1
	movl	15332(%rsi), %edx
	incl	%edx
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	decl	%edi
	andl	%edx, %edi
	movl	%edi, 15332(%rsi)
.LBB4_11:                               #   in Loop: Header=BB4_9 Depth=1
	xorl	%r14d, %r14d
	cmpl	$2, 8(%r8,%r10,8)
	jne	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_9 Depth=1
	movl	$1, 15360(%rsi)
	movl	$1, %r14d
.LBB4_13:                               #   in Loop: Header=BB4_9 Depth=1
	movl	20(%r9), %ecx
	incl	%ecx
	cvtsi2sdl	%ecx, %xmm1
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm0
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 14352(%rsi)
	cmpl	$3, 2968(%r9)
	movapd	%xmm2, %xmm0
	je	.LBB4_15
# BB#14:                                #   in Loop: Header=BB4_9 Depth=1
	movapd	%xmm1, %xmm0
.LBB4_15:                               #   in Loop: Header=BB4_9 Depth=1
	movsd	%xmm0, 14352(%rsi)
	movl	1560(%r9), %ebp
	testl	%ebp, %ebp
	je	.LBB4_28
# BB#16:                                #   in Loop: Header=BB4_9 Depth=1
	cmpl	$0, 1568(%r9)
	je	.LBB4_28
# BB#17:                                #   in Loop: Header=BB4_9 Depth=1
	movl	(%rsi), %edi
	movl	start_frame_no_in_this_IGOP(%rip), %ebx
	movl	%edi, %eax
	subl	%ebx, %eax
	cltd
	idivl	%ebp
	jmp	.LBB4_29
	.p2align	4, 0x90
.LBB4_28:                               #   in Loop: Header=BB4_9 Depth=1
	movl	(%rsi), %edi
	movl	start_frame_no_in_this_IGOP(%rip), %ebx
	movl	%edi, %edx
	subl	%ebx, %edx
.LBB4_29:                               #   in Loop: Header=BB4_9 Depth=1
	decl	%edx
	imull	%ecx, %edx
	movl	4(%r8,%r10,8), %eax
	incl	%eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	movl	%eax, 15316(%rsi)
	movl	start_tr_in_this_IGOP(%rip), %edx
	subl	%ebx, %edi
	cmpl	$1, %r11d
	jne	.LBB4_31
# BB#30:                                #   in Loop: Header=BB4_9 Depth=1
	imull	%edi, %ecx
	jmp	.LBB4_32
	.p2align	4, 0x90
.LBB4_31:                               #   in Loop: Header=BB4_9 Depth=1
	decl	%edi
	imull	%edi, %ecx
	addl	%edx, %ecx
	addsd	%xmm0, %xmm0
	addl	$-2, %r11d
	movslq	%r11d, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movl	4(%r8,%rdx,8), %edx
	incl	%edx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %edx
.LBB4_32:                               #   in Loop: Header=BB4_9 Depth=1
	addl	%edx, %ecx
	addl	%ecx, %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	movl	%edx, 15304(%rsi)
	cmpl	$0, 4704(%r9)
	jne	.LBB4_34
# BB#33:                                #   in Loop: Header=BB4_9 Depth=1
	cmpl	$0, 4708(%r9)
	movl	%eax, %ecx
	je	.LBB4_35
.LBB4_34:                               #   in Loop: Header=BB4_9 Depth=1
	movl	%eax, %ecx
	orl	$1, %ecx
.LBB4_35:                               #   in Loop: Header=BB4_9 Depth=1
	movl	%ecx, 15320(%rsi)
	cmpl	%ecx, %eax
	cmovlel	%eax, %ecx
	movl	%ecx, 15324(%rsi)
	movl	$0, 15308(%rsi)
	callq	encode_one_frame
	movq	input(%rip), %rax
	cmpl	$0, 5104(%rax)
	je	.LBB4_37
# BB#36:                                #   in Loop: Header=BB4_9 Depth=1
	callq	report_frame_statistic
.LBB4_37:                               #   in Loop: Header=BB4_9 Depth=1
	movq	gop_structure(%rip), %r8
	movq	img(%rip), %rsi
	movslq	14364(%rsi), %rdx
	leaq	(%rdx,%rdx,2), %rcx
	movq	input(%rip), %r9
	movl	2096(%r9), %eax
	cmpl	$2, -16(%r8,%rcx,8)
	jne	.LBB4_40
# BB#38:                                #   in Loop: Header=BB4_9 Depth=1
	cmpl	%eax, %edx
	jne	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_9 Depth=1
	movl	15332(%rsi), %eax
	incl	%eax
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	decl	%edi
	andl	%eax, %edi
	movl	%edi, 15332(%rsi)
	movl	%edx, %eax
.LBB4_40:                               # %._crit_edge73
                                        #   in Loop: Header=BB4_9 Depth=1
	leal	1(%rdx), %r11d
	movl	%r11d, 14364(%rsi)
	cmpl	%eax, %edx
	jl	.LBB4_9
.LBB4_41:                               # %._crit_edge
	movl	$0, 14364(%rsi)
	jmp	.LBB4_42
.LBB4_5:                                # %.thread
	movl	15332(%rsi), %edx
	incl	%edx
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	decl	%edi
	andl	%edx, %edi
	movl	%edi, 15332(%rsi)
	movl	$0, 15360(%rsi)
	movl	$1, 14364(%rsi)
	testl	%eax, %eax
	jle	.LBB4_42
.LBB4_20:                               # %.lr.ph.preheader
	movl	$1, %ebx
	movl	$-2, %r14d
	jmp	.LBB4_21
	.p2align	4, 0x90
.LBB4_53:                               # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB4_21 Depth=1
	movl	2964(%r9), %ebp
.LBB4_21:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 15360(%rsi)
	cmpl	$1, %ebp
	jne	.LBB4_23
# BB#22:                                #   in Loop: Header=BB4_21 Depth=1
	movl	$1, 15360(%rsi)
	movl	15332(%rsi), %edx
	incl	%edx
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	decl	%edi
	andl	%edx, %edi
	movl	%edi, 15332(%rsi)
.LBB4_23:                               #   in Loop: Header=BB4_21 Depth=1
	movl	20(%r9), %ecx
	incl	%ecx
	cvtsi2sdl	%ecx, %xmm1
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm0
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 14352(%rsi)
	cmpl	$3, 2968(%r9)
	movapd	%xmm2, %xmm0
	je	.LBB4_25
# BB#24:                                #   in Loop: Header=BB4_21 Depth=1
	movapd	%xmm1, %xmm0
.LBB4_25:                               #   in Loop: Header=BB4_21 Depth=1
	movsd	%xmm0, 14352(%rsi)
	movl	1560(%r9), %edi
	testl	%edi, %edi
	je	.LBB4_43
# BB#26:                                #   in Loop: Header=BB4_21 Depth=1
	cmpl	$0, 1568(%r9)
	je	.LBB4_43
# BB#27:                                #   in Loop: Header=BB4_21 Depth=1
	movl	(%rsi), %eax
	subl	start_frame_no_in_this_IGOP(%rip), %eax
	cltd
	idivl	%edi
	jmp	.LBB4_44
	.p2align	4, 0x90
.LBB4_43:                               #   in Loop: Header=BB4_21 Depth=1
	movl	(%rsi), %edx
	subl	start_frame_no_in_this_IGOP(%rip), %edx
.LBB4_44:                               #   in Loop: Header=BB4_21 Depth=1
	decl	%edx
	imull	%edx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	movl	%eax, 15316(%rsi)
	cmpl	$0, 4704(%r9)
	jne	.LBB4_46
# BB#45:                                #   in Loop: Header=BB4_21 Depth=1
	cmpl	$0, 4708(%r9)
	movl	%eax, %ecx
	je	.LBB4_47
.LBB4_46:                               #   in Loop: Header=BB4_21 Depth=1
	movl	%eax, %ecx
	orl	$1, %ecx
.LBB4_47:                               #   in Loop: Header=BB4_21 Depth=1
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	cmpl	$1, %ebp
	movl	%ecx, 15320(%rsi)
	movl	%eax, 15324(%rsi)
	leal	-2(%rbx,%rbx), %eax
	cmovel	%r14d, %eax
	movl	%eax, 15304(%rsi)
	movl	$0, 15308(%rsi)
	callq	encode_one_frame
	movq	input(%rip), %r9
	cmpl	$1, 2964(%r9)
	jne	.LBB4_50
# BB#48:                                #   in Loop: Header=BB4_21 Depth=1
	movq	img(%rip), %rax
	movl	14364(%rax), %ecx
	cmpl	2096(%r9), %ecx
	jne	.LBB4_50
# BB#49:                                #   in Loop: Header=BB4_21 Depth=1
	movl	15332(%rax), %edx
	incl	%edx
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	decl	%esi
	andl	%edx, %esi
	movl	%esi, 15332(%rax)
.LBB4_50:                               #   in Loop: Header=BB4_21 Depth=1
	cmpl	$0, 5104(%r9)
	je	.LBB4_52
# BB#51:                                #   in Loop: Header=BB4_21 Depth=1
	callq	report_frame_statistic
	movq	input(%rip), %r9
.LBB4_52:                               #   in Loop: Header=BB4_21 Depth=1
	movq	img(%rip), %rsi
	movl	14364(%rsi), %ecx
	leal	1(%rcx), %ebx
	movl	%ebx, 14364(%rsi)
	movl	2096(%r9), %eax
	cmpl	%eax, %ecx
	jl	.LBB4_53
.LBB4_42:                               # %.loopexit
	movl	$0, 14364(%rsi)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	encode_enhancement_layer, .Lfunc_end4-encode_enhancement_layer
	.cfi_endproc

	.globl	poc_based_ref_management
	.p2align	4, 0x90
	.type	poc_based_ref_management,@function
poc_based_ref_management:               # @poc_based_ref_management
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 48
.Lcfi35:
	.cfi_offset %rbx, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movq	img(%rip), %rax
	cmpq	$0, 15376(%rax)
	jne	.LBB5_15
# BB#1:
	movl	dpb+32(%rip), %eax
	addl	dpb+36(%rip), %eax
	je	.LBB5_15
# BB#2:                                 # %.preheader
	movl	dpb+28(%rip), %eax
	testq	%rax, %rax
	je	.LBB5_3
# BB#4:                                 # %.lr.ph
	movl	$2147483647, %esi       # imm = 0x7FFFFFFF
	xorl	%ecx, %ecx
	movq	dpb(%rip), %rdx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rcx,8), %rdi
	cmpl	$0, 4(%rdi)
	je	.LBB5_9
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	cmpl	$0, 8(%rdi)
	jne	.LBB5_9
# BB#7:                                 #   in Loop: Header=BB5_5 Depth=1
	cmpl	%esi, 36(%rdi)
	jge	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_5 Depth=1
	movq	40(%rdi), %rdi
	movl	4(%rdi), %esi
	movl	6364(%rdi), %r15d
	.p2align	4, 0x90
.LBB5_9:                                #   in Loop: Header=BB5_5 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB5_5
	jmp	.LBB5_10
.LBB5_3:
	xorl	%r15d, %r15d
.LBB5_10:                               # %._crit_edge
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB5_12
# BB#11:
	movl	$.L.str.12, %edi
	callq	no_mem_exit
.LBB5_12:
	movq	$0, 24(%rbx)
	movl	$0, (%rbx)
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB5_14
# BB#13:
	movl	$.L.str.13, %edi
	callq	no_mem_exit
.LBB5_14:
	movq	%rbx, 24(%rbp)
	movl	$1, (%rbp)
	decl	%r14d
	subl	%r15d, %r14d
	movl	%r14d, 4(%rbp)
	movq	img(%rip), %rax
	movq	%rbp, 15376(%rax)
.LBB5_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	poc_based_ref_management, .Lfunc_end5-poc_based_ref_management
	.cfi_endproc

	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"init_gop_structure: gop_structure"
	.size	.L.str.2, 34

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Slice Type invalid in ExplicitHierarchyFormat param. Please check configuration file."
	.size	.L.str.3, 86

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Invalid Frame Order value. Frame position needs to be in [0,%d] range."
	.size	.L.str.5, 71

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Frame Order value %d in frame %d already used for enhancement frame %d."
	.size	.L.str.6, 72

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Slice Type needs to be followed by Display Order. Please check configuration file."
	.size	.L.str.7, 83

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Reference_IDC invalid in ExplicitHierarchyFormat param. Please check configuration file."
	.size	.L.str.8, 89

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Reference_IDC needs to be followed by QP. Please check configuration file."
	.size	.L.str.9, 75

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Total number of frames in Enhancement GOP need to be fewer or equal to FrameSkip parameter."
	.size	.L.str.10, 92

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"ExplicitHierarchyFormat is empty. Please check configuration file."
	.size	.L.str.11, 67

	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"poc_based_ref_management: tmp_drpm"
	.size	.L.str.12, 35

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"poc_based_ref_management: tmp_drpm2"
	.size	.L.str.13, 36

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
