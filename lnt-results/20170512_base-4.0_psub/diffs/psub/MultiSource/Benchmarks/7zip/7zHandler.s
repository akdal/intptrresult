	.text
	.file	"7zHandler.bc"
	.globl	_ZN8NArchive3N7z8CHandlerC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandlerC2Ev,@function
_ZN8NArchive3N7z8CHandlerC2Ev:          # @_ZN8NArchive3N7z8CHandlerC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %r15
	leaq	16(%rbx), %r14
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbx)
	movq	$8, 40(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, 16(%rbx)
.Ltmp0:
	movq	%r15, %rdi
	callq	_ZN8NArchive11COutHandler4InitEv
.Ltmp1:
# BB#1:
	movl	$0, 128(%rbx)
	movq	$_ZTVN8NArchive3N7z8CHandlerE+16, (%rbx)
	movl	$_ZTVN8NArchive3N7z8CHandlerE+240, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3N7z8CHandlerE+176, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 112(%rbx)
	movq	$0, 136(%rbx)
	leaq	144(%rbx), %r12
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 152(%rbx)
	movq	$8, 168(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 144(%rbx)
	movdqu	%xmm0, 184(%rbx)
	movq	$1, 200(%rbx)
	movq	$_ZTV13CRecordVectorIbE+16, 176(%rbx)
	movdqu	%xmm0, 216(%rbx)
	movq	$4, 232(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 208(%rbx)
	movdqu	%xmm0, 248(%rbx)
	movq	$8, 264(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 240(%rbx)
	movdqu	%xmm0, 280(%rbx)
	movq	$4, 296(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 272(%rbx)
	movdqu	%xmm0, 312(%rbx)
	movq	$8, 328(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE+16, 304(%rbx)
	movdqu	%xmm0, 344(%rbx)
	movq	$8, 360(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 336(%rbx)
	movdqu	%xmm0, 376(%rbx)
	movq	$1, 392(%rbx)
	movq	$_ZTV13CRecordVectorIbE+16, 368(%rbx)
	movdqu	%xmm0, 408(%rbx)
	movq	$8, 424(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 400(%rbx)
	movdqu	%xmm0, 440(%rbx)
	movq	$1, 456(%rbx)
	movq	$_ZTV13CRecordVectorIbE+16, 432(%rbx)
	movdqu	%xmm0, 472(%rbx)
	movq	$8, 488(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 464(%rbx)
	movdqu	%xmm0, 504(%rbx)
	movq	$1, 520(%rbx)
	movq	$_ZTV13CRecordVectorIbE+16, 496(%rbx)
	movdqu	%xmm0, 536(%rbx)
	movq	$8, 552(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 528(%rbx)
	movdqu	%xmm0, 568(%rbx)
	movq	$1, 584(%rbx)
	movq	$_ZTV13CRecordVectorIbE+16, 560(%rbx)
	movdqu	%xmm0, 600(%rbx)
	movq	$1, 616(%rbx)
	movq	$_ZTV13CRecordVectorIbE+16, 592(%rbx)
	movdqu	%xmm0, 672(%rbx)
	movq	$8, 688(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 664(%rbx)
	movdqu	%xmm0, 704(%rbx)
	movq	$8, 720(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 696(%rbx)
	movdqu	%xmm0, 736(%rbx)
	movq	$4, 752(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 728(%rbx)
	movdqu	%xmm0, 768(%rbx)
	movq	$4, 784(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 760(%rbx)
	movdqu	%xmm0, 800(%rbx)
	movq	$4, 816(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 792(%rbx)
	leaq	848(%rbx), %r13
	movdqu	%xmm0, 856(%rbx)
	movq	$16, 872(%rbx)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE+16, 848(%rbx)
	leaq	880(%rbx), %rbp
	movdqu	%xmm0, 888(%rbx)
	movq	$8, 904(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 880(%rbx)
	movl	$4, 12(%rbx)
	movb	$0, 840(%rbx)
.Ltmp12:
	movq	%r15, %rdi
	callq	_ZN8NArchive11COutHandler4InitEv
.Ltmp13:
# BB#2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_10:
.Ltmp14:
	movq	%rax, %r15
.Ltmp15:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp16:
# BB#11:
.Ltmp17:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp18:
# BB#12:
.Ltmp19:
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z18CArchiveDatabaseExD2Ev
.Ltmp20:
# BB#13:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_15
# BB#14:
	movq	(%rdi), %rax
.Ltmp21:
	callq	*16(%rax)
.Ltmp22:
.LBB0_15:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, (%r14)
.Ltmp23:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp24:
# BB#16:                                # %_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev.exit.i3
.Ltmp29:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp30:
	jmp	.LBB0_5
.LBB0_17:
.Ltmp25:
	movq	%rax, %rbx
.Ltmp26:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp27:
	jmp	.LBB0_20
.LBB0_18:
.Ltmp28:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_19:
.Ltmp31:
	movq	%rax, %rbx
.LBB0_20:                               # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_3:
.Ltmp2:
	movq	%rax, %r15
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, (%r14)
.Ltmp3:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp4:
# BB#4:
.Ltmp9:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp10:
.LBB0_5:                                # %unwind_resume
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_8:
.Ltmp11:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_6:
.Ltmp5:
	movq	%rax, %rbx
.Ltmp6:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp7:
# BB#9:                                 # %.body.i
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_7:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN8NArchive3N7z8CHandlerC2Ev, .Lfunc_end0-_ZN8NArchive3N7z8CHandlerC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp15         #   Call between .Ltmp15 and .Ltmp22
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 8 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp6-.Ltmp10          #   Call between .Ltmp10 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 10 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN8NArchive3N7z18CArchiveDatabaseExD2Ev,"axG",@progbits,_ZN8NArchive3N7z18CArchiveDatabaseExD2Ev,comdat
	.weak	_ZN8NArchive3N7z18CArchiveDatabaseExD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z18CArchiveDatabaseExD2Ev,@function
_ZN8NArchive3N7z18CArchiveDatabaseExD2Ev: # @_ZN8NArchive3N7z18CArchiveDatabaseExD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	648(%rbx), %rdi
.Ltmp32:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp33:
# BB#1:
	leaq	616(%rbx), %rdi
.Ltmp37:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp38:
# BB#2:
	leaq	584(%rbx), %rdi
.Ltmp42:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp43:
# BB#3:
	leaq	552(%rbx), %rdi
.Ltmp47:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp48:
# BB#4:
	leaq	520(%rbx), %rdi
.Ltmp52:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp53:
# BB#5:                                 # %_ZN8NArchive3N7z14CInArchiveInfoD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN8NArchive3N7z16CArchiveDatabaseD2Ev # TAILCALL
.LBB2_11:
.Ltmp54:
	movq	%rax, %r14
	jmp	.LBB2_14
.LBB2_12:
.Ltmp49:
	movq	%rax, %r14
	jmp	.LBB2_13
.LBB2_9:
.Ltmp44:
	movq	%rax, %r14
	jmp	.LBB2_10
.LBB2_7:
.Ltmp39:
	movq	%rax, %r14
	jmp	.LBB2_8
.LBB2_6:
.Ltmp34:
	movq	%rax, %r14
	leaq	616(%rbx), %rdi
.Ltmp35:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp36:
.LBB2_8:
	leaq	584(%rbx), %rdi
.Ltmp40:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp41:
.LBB2_10:
	leaq	552(%rbx), %rdi
.Ltmp45:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp46:
.LBB2_13:
	leaq	520(%rbx), %rdi
.Ltmp50:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp51:
.LBB2_14:                               # %_ZN8NArchive3N7z14CInArchiveInfoD2Ev.exit6
.Ltmp55:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z16CArchiveDatabaseD2Ev
.Ltmp56:
# BB#15:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_16:
.Ltmp57:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN8NArchive3N7z18CArchiveDatabaseExD2Ev, .Lfunc_end2-_ZN8NArchive3N7z18CArchiveDatabaseExD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin1   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin1   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin1   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin1   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp35-.Ltmp53         #   Call between .Ltmp53 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp56-.Ltmp35         #   Call between .Ltmp35 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin1   #     jumps to .Ltmp57
	.byte	1                       #   On action: 1
	.long	.Ltmp56-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Lfunc_end2-.Ltmp56     #   Call between .Ltmp56 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z8CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive3N7z8CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive3N7z8CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	movl	316(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	_ZN8NArchive3N7z8CHandler16GetNumberOfItemsEPj, .Lfunc_end3-_ZN8NArchive3N7z8CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive3N7z8CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive3N7z8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 192
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r15
	movl	$0, 56(%rsp)
	addl	$-13, %esi
	cmpl	$32, %esi
	ja	.LBB4_137
# BB#1:
	jmpq	*.LJTI4_0(,%rsi,8)
.LBB4_124:
	movslq	284(%r15), %rax
	testq	%rax, %rax
	jle	.LBB4_125
# BB#128:                               # %.lr.ph.i
	movq	288(%r15), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_129:                              # =>This Inner Loop Header: Depth=1
	cmpl	$2, (%rcx,%rdx,4)
	jae	.LBB4_130
# BB#126:                               #   in Loop: Header=BB4_129 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB4_129
# BB#127:
	xorl	%eax, %eax
	jmp	.LBB4_131
.LBB4_135:
	movq	632(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB4_137
# BB#136:
.Ltmp58:
	leaq	56(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp59:
	jmp	.LBB4_137
.LBB4_133:
	movq	824(%r15), %rsi
.Ltmp62:
	leaq	56(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp63:
	jmp	.LBB4_137
.LBB4_2:
.Ltmp68:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
.Ltmp69:
# BB#3:
	movl	$0, (%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rsp)
	movq	$8, 104(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 80(%rsp)
	cmpl	$0, 252(%r15)
	jle	.LBB4_4
# BB#11:                                # %.lr.ph136
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
.LBB4_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
                                        #       Child Loop BB4_17 Depth 3
	movq	256(%r15), %rax
	movq	(%rax,%r13,8), %rbp
	movslq	12(%rbp), %r14
	testq	%r14, %r14
	jle	.LBB4_13
	.p2align	4, 0x90
.LBB4_14:                               #   Parent Loop BB4_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_17 Depth 3
	movq	16(%rbp), %rax
	movq	-8(%rax,%r14,8), %rax
	movl	92(%rsp), %ebx
	testl	%ebx, %ebx
	movq	(%rax), %r12
	je	.LBB4_15
# BB#16:                                # %.lr.ph.i49.preheader
                                        #   in Loop: Header=BB4_14 Depth=2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_17:                               # %.lr.ph.i49
                                        #   Parent Loop BB4_12 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rbx,%rax), %esi
	movl	%esi, %edx
	shrl	$31, %edx
	addl	%esi, %edx
	sarl	%edx
	movslq	%edx, %rsi
	movq	(%rcx,%rsi,8), %rsi
	cmpq	%rsi, %r12
	je	.LBB4_21
# BB#18:                                # %.thread.i
                                        #   in Loop: Header=BB4_17 Depth=3
	leal	1(%rdx), %edi
	cmpq	%rsi, %r12
	cmovbl	%edx, %ebx
	cmovbl	%eax, %edi
	cmpl	%ebx, %edi
	movl	%edi, %eax
	jne	.LBB4_17
	jmp	.LBB4_19
	.p2align	4, 0x90
.LBB4_15:                               #   in Loop: Header=BB4_14 Depth=2
	xorl	%ebx, %ebx
.LBB4_19:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_14 Depth=2
.Ltmp70:
	leaq	80(%rsp), %rdi
	movl	%ebx, %esi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
.Ltmp71:
# BB#20:                                # %.noexc
                                        #   in Loop: Header=BB4_14 Depth=2
	movq	96(%rsp), %rcx
	movslq	%ebx, %rax
	movq	%r12, (%rcx,%rax,8)
.LBB4_21:                               # %_ZN13CRecordVectorIyE17AddToUniqueSortedERKy.exit.backedge
                                        #   in Loop: Header=BB4_14 Depth=2
	cmpq	$1, %r14
	leaq	-1(%r14), %r14
	jg	.LBB4_14
.LBB4_13:                               # %_ZN13CRecordVectorIyE17AddToUniqueSortedERKy.exit._crit_edge
                                        #   in Loop: Header=BB4_12 Depth=1
	incq	%r13
	movslq	252(%r15), %rax
	cmpq	%rax, %r13
	jl	.LBB4_12
# BB#9:                                 # %.preheader
	cmpl	$0, 92(%rsp)
	jle	.LBB4_10
# BB#23:                                # %.lr.ph
	movl	$4, %r15d
	xorl	%r14d, %r14d
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%r13, 72(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	jmp	.LBB4_24
.LBB4_132:
	movl	252(%r15), %esi
.Ltmp64:
	leaq	56(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp65:
	jmp	.LBB4_137
.LBB4_134:
	movq	832(%r15), %rsi
.Ltmp60:
	leaq	56(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp61:
	jmp	.LBB4_137
.LBB4_125:
	xorl	%eax, %eax
	jmp	.LBB4_131
.LBB4_4:                                # %.preheader.thread
	leaq	80(%rsp), %rbp
	movq	%r13, %r14
	movq	%r13, %r15
.LBB4_5:                                # %._crit_edge
.Ltmp89:
	leaq	56(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp90:
# BB#6:
.Ltmp94:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp95:
# BB#7:
	testq	%r15, %r15
	je	.LBB4_137
# BB#8:
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB4_137
.LBB4_130:
	movb	$1, %al
.LBB4_131:                              # %_ZNK8NArchive3N7z16CArchiveDatabase7IsSolidEv.exit
.Ltmp66:
	movzbl	%al, %esi
	leaq	56(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp67:
.LBB4_137:                              # %_ZN11CStringBaseIwED2Ev.exit47
.Ltmp97:
	leaq	56(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp98:
# BB#138:
.Ltmp103:
	leaq	56(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp104:
# BB#139:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit46
	xorl	%eax, %eax
.LBB4_147:
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_114:                              # %_ZN11CStringBaseIwED2Ev.exit50._crit_edge
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	96(%rsp), %rcx
	movl	%ebx, %r15d
.LBB4_24:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_37 Depth 2
                                        #     Child Loop BB4_69 Depth 2
                                        #     Child Loop BB4_72 Depth 2
                                        #     Child Loop BB4_59 Depth 2
                                        #     Child Loop BB4_62 Depth 2
                                        #     Child Loop BB4_100 Depth 2
                                        #     Child Loop BB4_103 Depth 2
                                        #     Child Loop BB4_90 Depth 2
                                        #     Child Loop BB4_93 Depth 2
                                        #     Child Loop BB4_109 Depth 2
	movq	(%rcx,%r14,8), %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
.Ltmp73:
	movl	$16, %edi
	callq	_Znam
.Ltmp74:
# BB#25:                                #   in Loop: Header=BB4_24 Depth=1
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	$4, 28(%rsp)
.Ltmp76:
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_Z10FindMethodyR11CStringBaseIwE
.Ltmp77:
# BB#26:                                #   in Loop: Header=BB4_24 Depth=1
	cmpl	$0, 24(%rsp)
	movq	40(%rsp), %r13          # 8-byte Reload
	jne	.LBB4_40
# BB#27:                                #   in Loop: Header=BB4_24 Depth=1
.Ltmp78:
	leaq	120(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_Z23ConvertMethodIdToStringy
.Ltmp79:
# BB#28:                                #   in Loop: Header=BB4_24 Depth=1
	movl	%r12d, 52(%rsp)         # 4-byte Spill
	movq	%r14, %r12
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbp
	movl	$0, (%rbp)
	movslq	128(%rsp), %rbx
	incq	%rbx
	movl	28(%rsp), %r14d
	cmpl	%r14d, %ebx
	jne	.LBB4_30
# BB#29:                                #   in Loop: Header=BB4_24 Depth=1
	movq	%r12, %r14
	movl	52(%rsp), %r12d         # 4-byte Reload
	jmp	.LBB4_36
.LBB4_30:                               #   in Loop: Header=BB4_24 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp81:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp82:
# BB#31:                                # %.noexc61
                                        #   in Loop: Header=BB4_24 Depth=1
	testq	%rbp, %rbp
	je	.LBB4_32
# BB#33:                                # %.noexc61
                                        #   in Loop: Header=BB4_24 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	movq	%r12, %r14
	jle	.LBB4_35
# BB#34:                                # %._crit_edge.thread.i.i57
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	jmp	.LBB4_35
.LBB4_32:                               #   in Loop: Header=BB4_24 Depth=1
	xorl	%eax, %eax
	movq	%r12, %r14
.LBB4_35:                               # %._crit_edge16.i.i58
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	%r13, 16(%rsp)
	movl	$0, (%r13,%rax,4)
	movl	%ebx, 28(%rsp)
	movq	%r13, %rbp
	movl	52(%rsp), %r12d         # 4-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
.LBB4_36:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	120(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_37:                               #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB4_37
# BB#38:                                #   in Loop: Header=BB4_24 Depth=1
	movl	128(%rsp), %eax
	movl	%eax, 24(%rsp)
	testq	%rdi, %rdi
	je	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_24 Depth=1
	callq	_ZdaPv
.LBB4_40:                               #   in Loop: Header=BB4_24 Depth=1
	testl	%r12d, %r12d
	je	.LBB4_41
# BB#47:                                #   in Loop: Header=BB4_24 Depth=1
	movl	%r15d, %eax
	subl	%r12d, %eax
	cmpl	$1, %eax
	jg	.LBB4_48
# BB#49:                                #   in Loop: Header=BB4_24 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %r15d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r15d
	jl	.LBB4_51
# BB#50:                                # %select.true.sink
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%r15d, %edx
	shrl	$31, %edx
	addl	%r15d, %edx
	sarl	%edx
.LBB4_51:                               # %select.end
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r15,%rsi), %ebx
	cmpl	%r15d, %ebx
	jne	.LBB4_52
.LBB4_48:                               #   in Loop: Header=BB4_24 Depth=1
	movl	%r15d, %ebx
	movq	%r13, %rbp
	movq	(%rsp), %rcx            # 8-byte Reload
	jmp	.LBB4_77
.LBB4_41:                               #   in Loop: Header=BB4_24 Depth=1
	xorl	%r12d, %r12d
	jmp	.LBB4_78
.LBB4_52:                               #   in Loop: Header=BB4_24 Depth=1
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp84:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp85:
# BB#53:                                # %.noexc78
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%r12d, %r13d
	testl	%r15d, %r15d
	movq	(%rsp), %r12            # 8-byte Reload
	jle	.LBB4_76
# BB#54:                                # %.preheader.i.i68
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%r13d, %ecx
	testl	%ecx, %ecx
	jle	.LBB4_74
# BB#55:                                # %.lr.ph.i.i69
                                        #   in Loop: Header=BB4_24 Depth=1
	movslq	%ecx, %rax
	cmpl	$7, %ecx
	jbe	.LBB4_56
# BB#63:                                # %min.iters.checked195
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB4_56
# BB#64:                                # %vector.memcheck208
                                        #   in Loop: Header=BB4_24 Depth=1
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB4_66
# BB#65:                                # %vector.memcheck208
                                        #   in Loop: Header=BB4_24 Depth=1
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, 8(%rsp)           # 8-byte Folded Reload
	jae	.LBB4_66
.LBB4_56:                               #   in Loop: Header=BB4_24 Depth=1
	xorl	%ecx, %ecx
.LBB4_57:                               # %scalar.ph193.preheader
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%r13d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB4_60
# BB#58:                                # %scalar.ph193.prol.preheader
                                        #   in Loop: Header=BB4_24 Depth=1
	negq	%rsi
.LBB4_59:                               # %scalar.ph193.prol
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12,%rcx,4), %edi
	movl	%edi, (%rbp,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB4_59
.LBB4_60:                               # %scalar.ph193.prol.loopexit
                                        #   in Loop: Header=BB4_24 Depth=1
	cmpq	$7, %rdx
	jb	.LBB4_75
# BB#61:                                # %scalar.ph193.preheader.new
                                        #   in Loop: Header=BB4_24 Depth=1
	subq	%rcx, %rax
	leaq	28(%rbp,%rcx,4), %rdx
	leaq	28(%r12,%rcx,4), %rcx
.LBB4_62:                               # %scalar.ph193
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB4_62
	jmp	.LBB4_75
.LBB4_74:                               # %._crit_edge.i.i70
                                        #   in Loop: Header=BB4_24 Depth=1
	testq	%r12, %r12
	je	.LBB4_76
.LBB4_75:                               # %._crit_edge.thread.i.i75
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
.LBB4_76:                               # %._crit_edge16.i.i76
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%r13d, %r12d
	movslq	%r12d, %rax
	movl	$0, (%rbp,%rax,4)
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rcx
.LBB4_77:                               # %_ZN11CStringBaseIwEpLEw.exit
                                        #   in Loop: Header=BB4_24 Depth=1
	movslq	%r12d, %rax
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	$32, (%rcx,%rax,4)
	incl	%r12d
	movl	%ebx, %r15d
	movq	%rbp, %r13
.LBB4_78:                               #   in Loop: Header=BB4_24 Depth=1
	movl	24(%rsp), %eax
	movl	%r15d, %ecx
	subl	%r12d, %ecx
	cmpl	%eax, %ecx
	jg	.LBB4_79
# BB#80:                                #   in Loop: Header=BB4_24 Depth=1
	decl	%ecx
	cmpl	$8, %r15d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r15d
	jl	.LBB4_82
# BB#81:                                # %select.true.sink477
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%r15d, %edx
	shrl	$31, %edx
	addl	%r15d, %edx
	sarl	%edx
.LBB4_82:                               # %select.end476
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%r15,%rsi), %ebx
	cmpl	%r15d, %ebx
	jne	.LBB4_83
.LBB4_79:                               #   in Loop: Header=BB4_24 Depth=1
	movl	%r15d, %ebx
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB4_108
.LBB4_83:                               #   in Loop: Header=BB4_24 Depth=1
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp86:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp87:
# BB#84:                                # %.noexc53
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%r12d, %ebp
	testl	%r15d, %r15d
	movq	(%rsp), %r12            # 8-byte Reload
	jle	.LBB4_107
# BB#85:                                # %.preheader.i.i
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%ebp, %ecx
	testl	%ecx, %ecx
	jle	.LBB4_105
# BB#86:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB4_24 Depth=1
	movslq	%ecx, %rax
	cmpl	$7, %ecx
	jbe	.LBB4_87
# BB#94:                                # %min.iters.checked
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB4_87
# BB#95:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_24 Depth=1
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %r13
	jae	.LBB4_97
# BB#96:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_24 Depth=1
	leaq	(%r13,%rax,4), %rdx
	cmpq	%rdx, 8(%rsp)           # 8-byte Folded Reload
	jae	.LBB4_97
.LBB4_87:                               #   in Loop: Header=BB4_24 Depth=1
	xorl	%ecx, %ecx
.LBB4_88:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB4_91
# BB#89:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB4_24 Depth=1
	negq	%rsi
.LBB4_90:                               # %scalar.ph.prol
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12,%rcx,4), %edi
	movl	%edi, (%r13,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB4_90
.LBB4_91:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB4_24 Depth=1
	cmpq	$7, %rdx
	jb	.LBB4_106
# BB#92:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB4_24 Depth=1
	subq	%rcx, %rax
	leaq	28(%r13,%rcx,4), %rdx
	leaq	28(%r12,%rcx,4), %rcx
.LBB4_93:                               # %scalar.ph
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB4_93
	jmp	.LBB4_106
.LBB4_105:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB4_24 Depth=1
	testq	%r12, %r12
	je	.LBB4_107
.LBB4_106:                              # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
.LBB4_107:                              # %._crit_edge16.i.i
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%ebp, %r12d
	movslq	%r12d, %rax
	movl	$0, (%r13,%rax,4)
	movq	%r13, 72(%rsp)          # 8-byte Spill
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%r13, %r15
.LBB4_108:                              # %.noexc51
                                        #   in Loop: Header=BB4_24 Depth=1
	movslq	%r12d, %rax
	leaq	(%r15,%rax,4), %rax
	movq	16(%rsp), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_109:                              #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB4_109
# BB#110:                               #   in Loop: Header=BB4_24 Depth=1
	addl	24(%rsp), %r12d
	testq	%rdi, %rdi
	je	.LBB4_112
# BB#111:                               #   in Loop: Header=BB4_24 Depth=1
	callq	_ZdaPv
.LBB4_112:                              # %_ZN11CStringBaseIwED2Ev.exit50
                                        #   in Loop: Header=BB4_24 Depth=1
	incq	%r14
	movslq	92(%rsp), %rax
	cmpq	%rax, %r14
	jl	.LBB4_114
	jmp	.LBB4_113
.LBB4_97:                               # %vector.body.preheader
                                        #   in Loop: Header=BB4_24 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB4_98
# BB#99:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB4_24 Depth=1
	negq	%rsi
	xorl	%edi, %edi
.LBB4_100:                              # %vector.body.prol
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdi,4), %xmm0
	movups	16(%r12,%rdi,4), %xmm1
	movups	%xmm0, (%r13,%rdi,4)
	movups	%xmm1, 16(%r13,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB4_100
	jmp	.LBB4_101
.LBB4_66:                               # %vector.body191.preheader
                                        #   in Loop: Header=BB4_24 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB4_67
# BB#68:                                # %vector.body191.prol.preheader
                                        #   in Loop: Header=BB4_24 Depth=1
	negq	%rsi
	xorl	%edi, %edi
.LBB4_69:                               # %vector.body191.prol
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdi,4), %xmm0
	movups	16(%r12,%rdi,4), %xmm1
	movups	%xmm0, (%rbp,%rdi,4)
	movups	%xmm1, 16(%rbp,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB4_69
	jmp	.LBB4_70
.LBB4_98:                               #   in Loop: Header=BB4_24 Depth=1
	xorl	%edi, %edi
.LBB4_101:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB4_24 Depth=1
	cmpq	$24, %rdx
	jb	.LBB4_104
# BB#102:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r13,%rdi,4), %rsi
	leaq	112(%r12,%rdi,4), %rdi
.LBB4_103:                              # %vector.body
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB4_103
.LBB4_104:                              # %middle.block
                                        #   in Loop: Header=BB4_24 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB4_88
	jmp	.LBB4_106
.LBB4_67:                               #   in Loop: Header=BB4_24 Depth=1
	xorl	%edi, %edi
.LBB4_70:                               # %vector.body191.prol.loopexit
                                        #   in Loop: Header=BB4_24 Depth=1
	cmpq	$24, %rdx
	jb	.LBB4_73
# BB#71:                                # %vector.body191.preheader.new
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rbp,%rdi,4), %rsi
	leaq	112(%r12,%rdi,4), %rdi
.LBB4_72:                               # %vector.body191
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB4_72
.LBB4_73:                               # %middle.block192
                                        #   in Loop: Header=BB4_24 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB4_57
	jmp	.LBB4_75
.LBB4_10:
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r13, %r14
	movq	%r13, %r15
	movq	112(%rsp), %rbx         # 8-byte Reload
	leaq	80(%rsp), %rbp
	jmp	.LBB4_5
.LBB4_113:
	movq	112(%rsp), %rbx         # 8-byte Reload
	leaq	80(%rsp), %rbp
	movq	72(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_5
.LBB4_121:
.Ltmp96:
	movq	%rdx, %r14
	movq	%rax, %rbx
	testq	%r15, %r15
	jne	.LBB4_123
	jmp	.LBB4_142
.LBB4_119:
.Ltmp91:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB4_120
.LBB4_140:
.Ltmp105:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB4_143
.LBB4_141:
.Ltmp99:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB4_142
.LBB4_44:
.Ltmp83:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	movq	40(%rsp), %r13          # 8-byte Reload
	je	.LBB4_46
# BB#45:
	callq	_ZdaPv
.LBB4_46:                               # %_ZN11CStringBaseIwED2Ev.exit63
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB4_116
.LBB4_43:
.Ltmp80:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB4_116
.LBB4_42:
.Ltmp75:
	movq	%rdx, %r14
	movq	%rax, %rbx
	leaq	80(%rsp), %rbp
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB4_120
.LBB4_115:
.Ltmp88:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	(%rsp), %r15            # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
.LBB4_116:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_118
# BB#117:
	callq	_ZdaPv
.LBB4_118:                              # %_ZN11CStringBaseIwED2Ev.exit48
	leaq	80(%rsp), %rbp
	jmp	.LBB4_120
.LBB4_22:
.Ltmp72:
	movq	%rdx, %r14
	movq	%rax, %rbx
	leaq	80(%rsp), %rbp
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	%r15, %r13
.LBB4_120:
.Ltmp92:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp93:
# BB#122:
	testq	%r15, %r15
	je	.LBB4_142
.LBB4_123:
	movq	%r13, %rdi
	callq	_ZdaPv
.LBB4_142:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp100:
	leaq	56(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp101:
.LBB4_143:                              # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB4_144
# BB#146:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB4_147
.LBB4_144:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp106:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp107:
# BB#149:
.LBB4_145:
.Ltmp108:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_148:
.Ltmp102:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN8NArchive3N7z8CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end4-_ZN8NArchive3N7z8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_124
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_2
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_135
	.quad	.LBB4_137
	.quad	.LBB4_132
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_137
	.quad	.LBB4_134
	.quad	.LBB4_133
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\337\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp69-.Ltmp58         #   Call between .Ltmp58 and .Ltmp69
	.long	.Ltmp99-.Lfunc_begin2   #     jumps to .Ltmp99
	.byte	3                       #   On action: 2
	.long	.Ltmp70-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin2   #     jumps to .Ltmp72
	.byte	3                       #   On action: 2
	.long	.Ltmp64-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp61-.Ltmp64         #   Call between .Ltmp64 and .Ltmp61
	.long	.Ltmp99-.Lfunc_begin2   #     jumps to .Ltmp99
	.byte	3                       #   On action: 2
	.long	.Ltmp89-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin2   #     jumps to .Ltmp91
	.byte	3                       #   On action: 2
	.long	.Ltmp94-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin2   #     jumps to .Ltmp96
	.byte	3                       #   On action: 2
	.long	.Ltmp66-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp98-.Ltmp66         #   Call between .Ltmp66 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin2   #     jumps to .Ltmp99
	.byte	3                       #   On action: 2
	.long	.Ltmp103-.Lfunc_begin2  # >> Call Site 7 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin2  #     jumps to .Ltmp105
	.byte	3                       #   On action: 2
	.long	.Ltmp73-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin2   #     jumps to .Ltmp75
	.byte	3                       #   On action: 2
	.long	.Ltmp76-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp88-.Lfunc_begin2   #     jumps to .Ltmp88
	.byte	3                       #   On action: 2
	.long	.Ltmp78-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin2   #     jumps to .Ltmp80
	.byte	3                       #   On action: 2
	.long	.Ltmp81-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin2   #     jumps to .Ltmp83
	.byte	3                       #   On action: 2
	.long	.Ltmp84-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp87-.Ltmp84         #   Call between .Ltmp84 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin2   #     jumps to .Ltmp88
	.byte	3                       #   On action: 2
	.long	.Ltmp92-.Lfunc_begin2   # >> Call Site 13 <<
	.long	.Ltmp101-.Ltmp92        #   Call between .Ltmp92 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin2  #     jumps to .Ltmp102
	.byte	1                       #   On action: 1
	.long	.Ltmp101-.Lfunc_begin2  # >> Call Site 14 <<
	.long	.Ltmp106-.Ltmp101       #   Call between .Ltmp101 and .Ltmp106
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp106-.Lfunc_begin2  # >> Call Site 15 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin2  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin2  # >> Call Site 16 <<
	.long	.Lfunc_end4-.Ltmp107    #   Call between .Ltmp107 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z8CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive3N7z8CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive3N7z8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$6, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	_ZN8NArchive3N7z8CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end5-_ZN8NArchive3N7z8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive3N7z8CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive3N7z8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$5, %esi
	ja	.LBB6_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive3N7z9kArcPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive3N7z9kArcPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB6_2:
	retq
.Lfunc_end6:
	.size	_ZN8NArchive3N7z8CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end6-_ZN8NArchive3N7z8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZNK8NArchive3N7z8CHandler11IsEncryptedEj
	.p2align	4, 0x90
	.type	_ZNK8NArchive3N7z8CHandler11IsEncryptedEj,@function
_ZNK8NArchive3N7z8CHandler11IsEncryptedEj: # @_ZNK8NArchive3N7z8CHandler11IsEncryptedEj
	.cfi_startproc
# BB#0:
	movq	808(%rdi), %rax
	movslq	%esi, %rcx
	movslq	(%rax,%rcx,4), %rax
	cmpq	$-1, %rax
	je	.LBB7_1
# BB#2:
	movq	256(%rdi), %rcx
	movq	(%rcx,%rax,8), %rax
	movslq	12(%rax), %rcx
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	jle	.LBB7_4
# BB#5:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	16(%rax), %rdx
	movq	-8(%rdx,%rcx,8), %rdx
	decq	%rcx
	cmpq	$116459265, (%rdx)      # imm = 0x6F10701
	jne	.LBB7_3
# BB#6:
	movb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB7_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB7_4:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end7:
	.size	_ZNK8NArchive3N7z8CHandler11IsEncryptedEj, .Lfunc_end7-_ZNK8NArchive3N7z8CHandler11IsEncryptedEj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	58                      # 0x3a
	.long	109                     # 0x6d
	.long	101                     # 0x65
	.long	109                     # 0x6d
.LCPI8_1:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive3N7z8CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive3N7z8CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi37:
	.cfi_def_cfa_offset 592
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	$0, 224(%rsp)
	movslq	%esi, %rcx
	leal	-3(%rdx), %ebp
	cmpl	$26, %ebp
	ja	.LBB8_7
# BB#1:
	movq	320(%rdi), %rax
	movq	(%rax,%rcx,8), %rax
	jmpq	*.LJTI8_0(,%rbp,8)
.LBB8_2:
	cmpl	$0, 24(%rax)
	je	.LBB8_74
# BB#3:
	addq	$16, %rax
.Ltmp235:
	leaq	384(%rsp), %rdi
	movq	%rax, %rsi
	callq	_ZN8NArchive9NItemName9GetOSNameERK11CStringBaseIwE
.Ltmp236:
# BB#4:
	movq	384(%rsp), %rsi
.Ltmp237:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp238:
# BB#5:
	movq	384(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_74
# BB#6:
	callq	_ZdaPv
	jmp	.LBB8_74
.LBB8_7:
	addl	$-65536, %edx           # imm = 0xFFFF0000
	cmpl	$5, %edx
	jae	.LBB8_74
# BB#8:
	movq	808(%rdi), %rax
	movslq	(%rax,%rcx,4), %rax
	cmpq	$-1, %rax
	je	.LBB8_13
# BB#9:
	movq	776(%rdi), %rcx
	cmpl	%esi, (%rcx,%rax,4)
	jne	.LBB8_12
# BB#10:
	movq	256(%rdi), %rcx
	movq	(%rcx,%rax,8), %rcx
	cmpl	%edx, 76(%rcx)
	jle	.LBB8_12
# BB#11:
	movq	744(%rdi), %rcx
	addl	(%rcx,%rax,4), %edx
	movq	160(%rdi), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rsi
.Ltmp111:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp112:
	jmp	.LBB8_74
.LBB8_12:
.Ltmp109:
	leaq	224(%rsp), %rdi
	xorl	%esi, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp110:
	jmp	.LBB8_74
.LBB8_13:
.Ltmp114:
	leaq	224(%rsp), %rdi
	xorl	%esi, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp115:
	jmp	.LBB8_74
.LBB8_14:
	movq	808(%rdi), %rax
	movslq	(%rax,%rcx,4), %rax
	cmpq	$-1, %rax
	je	.LBB8_58
# BB#15:
	movq	256(%rdi), %rcx
	movq	(%rcx,%rax,8), %rax
	movslq	12(%rax), %rcx
	.p2align	4, 0x90
.LBB8_16:                               # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	jle	.LBB8_58
# BB#17:                                #   in Loop: Header=BB8_16 Depth=1
	movq	16(%rax), %rdx
	movq	-8(%rdx,%rcx,8), %rdx
	decq	%rcx
	cmpq	$116459265, (%rdx)      # imm = 0x6F10701
	jne	.LBB8_16
# BB#18:
	movb	$1, %al
	jmp	.LBB8_59
.LBB8_19:
	cmpl	%esi, 444(%rdi)
	jle	.LBB8_74
# BB#20:
	movq	448(%rdi), %rax
	cmpb	$0, (%rax,%rcx)
	je	.LBB8_74
# BB#21:
	movq	416(%rdi), %rax
	movq	(%rax,%rcx,8), %rax
	movl	%eax, 328(%rsp)
	shrq	$32, %rax
	movl	%eax, 332(%rsp)
.Ltmp219:
	leaq	224(%rsp), %rdi
	leaq	328(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp220:
	jmp	.LBB8_74
.LBB8_22:
	cmpb	$0, 34(%rax)
	je	.LBB8_74
# BB#23:
	movl	12(%rax), %esi
.Ltmp213:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp214:
	jmp	.LBB8_74
.LBB8_24:
	cmpl	%esi, 572(%rdi)
	jle	.LBB8_74
# BB#25:
	movq	576(%rdi), %rax
	cmpb	$0, (%rax,%rcx)
	je	.LBB8_74
# BB#26:
	movq	544(%rdi), %rax
	movq	(%rax,%rcx,8), %rsi
.Ltmp223:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp224:
	jmp	.LBB8_74
.LBB8_27:
	cmpb	$0, 35(%rax)
	je	.LBB8_74
# BB#28:
	movl	8(%rax), %esi
.Ltmp215:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp216:
	jmp	.LBB8_74
.LBB8_29:
	movq	808(%rdi), %rax
	movslq	(%rax,%rcx,4), %rax
	cmpq	$-1, %rax
	je	.LBB8_57
# BB#30:
	movq	776(%rdi), %rcx
	cmpl	%esi, (%rcx,%rax,4)
	jne	.LBB8_74
# BB#31:
	movq	256(%rdi), %rcx
	movq	(%rcx,%rax,8), %rcx
	movslq	76(%rcx), %rdx
	testq	%rdx, %rdx
	jle	.LBB8_72
# BB#32:                                # %.lr.ph.i
	movq	744(%rdi), %rcx
	movslq	(%rcx,%rax,4), %rbx
	movl	%ebx, %r8d
	movq	160(%rdi), %rcx
	cmpl	$4, %edx
	jb	.LBB8_64
# BB#33:                                # %min.iters.checked1688
	movq	%rdx, %rbp
	andq	$-4, %rbp
	je	.LBB8_64
# BB#34:                                # %vector.scevcheck
	leaq	-1(%rdx), %rax
	leal	(%rax,%rbx), %esi
	xorl	%edi, %edi
	cmpl	%r8d, %esi
	jl	.LBB8_65
# BB#35:                                # %vector.scevcheck
	shrq	$32, %rax
	movl	$0, %esi
	jne	.LBB8_66
# BB#36:                                # %vector.body1684.preheader
	leaq	-4(%rbp), %rsi
	movq	%rsi, %rax
	shrq	$2, %rax
	btl	$2, %esi
	jb	.LBB8_79
# BB#37:                                # %vector.body1684.prol
	movdqu	(%rcx,%rbx,8), %xmm0
	movdqu	16(%rcx,%rbx,8), %xmm1
	movl	$4, %esi
	testq	%rax, %rax
	jne	.LBB8_80
	jmp	.LBB8_82
.LBB8_38:
	movzbl	33(%rax), %esi
.Ltmp233:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp234:
	jmp	.LBB8_74
.LBB8_39:
	movq	(%rax), %rsi
.Ltmp231:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp232:
	jmp	.LBB8_74
.LBB8_40:
	cmpl	%esi, 508(%rdi)
	jle	.LBB8_74
# BB#41:
	movq	512(%rdi), %rax
	cmpb	$0, (%rax,%rcx)
	je	.LBB8_74
# BB#42:
	movq	480(%rdi), %rax
	movq	(%rax,%rcx,8), %rax
	movl	%eax, 336(%rsp)
	shrq	$32, %rax
	movl	%eax, 340(%rsp)
.Ltmp217:
	leaq	224(%rsp), %rdi
	leaq	336(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp218:
	jmp	.LBB8_74
.LBB8_43:
	movq	808(%rdi), %rax
	movl	(%rax,%rcx,4), %esi
	cmpl	$-1, %esi
	je	.LBB8_74
# BB#44:
.Ltmp117:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp118:
	jmp	.LBB8_74
.LBB8_45:
	cmpl	%esi, 380(%rdi)
	jle	.LBB8_74
# BB#46:
	movq	384(%rdi), %rax
	cmpb	$0, (%rax,%rcx)
	je	.LBB8_74
# BB#47:
	movq	352(%rdi), %rax
	movq	(%rax,%rcx,8), %rax
	movl	%eax, 320(%rsp)
	shrq	$32, %rax
	movl	%eax, 324(%rsp)
.Ltmp221:
	leaq	224(%rsp), %rdi
	leaq	320(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp222:
	jmp	.LBB8_74
.LBB8_48:
	movq	808(%rdi), %rax
	movslq	(%rax,%rcx,4), %rax
	cmpq	$-1, %rax
	je	.LBB8_74
# BB#49:
	movq	256(%rdi), %rcx
	movq	(%rcx,%rax,8), %r12
.Ltmp120:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp121:
# BB#50:
	movq	%r14, 360(%rsp)         # 8-byte Spill
	movl	$0, (%rbx)
	movslq	12(%r12), %rdi
	testq	%rdi, %rdi
	jle	.LBB8_60
# BB#51:                                # %.lr.ph894
	movl	$4, %r14d
	xorl	%r13d, %r13d
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	movq	%rbx, 208(%rsp)         # 8-byte Spill
	movq	%rbx, 200(%rsp)         # 8-byte Spill
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movq	%rbx, %r8
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	movq	%rbx, 184(%rsp)         # 8-byte Spill
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 216(%rsp)         # 8-byte Spill
	movq	%rbx, 160(%rsp)         # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r12, 248(%rsp)         # 8-byte Spill
	jmp	.LBB8_99
.LBB8_52:
	cmpl	%esi, 604(%rdi)
	jle	.LBB8_54
# BB#53:
	movq	608(%rdi), %rax
	cmpb	$0, (%rax,%rcx)
	setne	%al
	jmp	.LBB8_55
.LBB8_58:
	xorl	%eax, %eax
.LBB8_59:                               # %_ZNK8NArchive3N7z8CHandler11IsEncryptedEj.exit
.Ltmp211:
	movzbl	%al, %esi
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp212:
	jmp	.LBB8_74
.LBB8_54:
	xorl	%eax, %eax
.LBB8_55:                               # %_ZNK8NArchive3N7z16CArchiveDatabase10IsItemAntiEi.exit
.Ltmp209:
	movzbl	%al, %esi
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp210:
	jmp	.LBB8_74
.LBB8_57:
.Ltmp228:
	leaq	224(%rsp), %rdi
	xorl	%esi, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp229:
	jmp	.LBB8_74
.LBB8_60:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %r8
.LBB8_61:                               # %._crit_edge
.Ltmp206:
	leaq	224(%rsp), %rdi
	movq	%r8, %rsi
	movq	%r8, %rbp
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp207:
# BB#62:
	testq	%rbp, %rbp
	movq	360(%rsp), %r14         # 8-byte Reload
	je	.LBB8_74
# BB#63:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	jmp	.LBB8_74
.LBB8_64:
	xorl	%edi, %edi
.LBB8_65:                               # %scalar.ph1686.preheader
	xorl	%esi, %esi
.LBB8_66:                               # %scalar.ph1686.preheader
	movl	%edx, %eax
	subl	%edi, %eax
	leaq	-1(%rdx), %rbp
	subq	%rdi, %rbp
	andq	$3, %rax
	je	.LBB8_69
# BB#67:                                # %scalar.ph1686.prol.preheader
	leal	(%rdi,%r8), %ebx
	negq	%rax
.LBB8_68:                               # %scalar.ph1686.prol
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	addq	(%rcx,%rbx,8), %rsi
	incq	%rdi
	incl	%ebx
	incq	%rax
	jne	.LBB8_68
.LBB8_69:                               # %scalar.ph1686.prol.loopexit
	cmpq	$3, %rbp
	jb	.LBB8_73
# BB#70:                                # %scalar.ph1686.preheader.new
	subq	%rdi, %rdx
	leaq	3(%r8,%rdi), %rax
	xorl	%edi, %edi
.LBB8_71:                               # %scalar.ph1686
                                        # =>This Inner Loop Header: Depth=1
	leal	-3(%rax,%rdi), %ebp
	movslq	%ebp, %rbp
	addq	(%rcx,%rbp,8), %rsi
	leal	-2(%rax,%rdi), %ebp
	movslq	%ebp, %rbp
	addq	(%rcx,%rbp,8), %rsi
	leal	-1(%rax,%rdi), %ebp
	movslq	%ebp, %rbp
	addq	(%rcx,%rbp,8), %rsi
	leal	(%rax,%rdi), %ebp
	movslq	%ebp, %rbp
	addq	(%rcx,%rbp,8), %rsi
	addq	$4, %rdi
	cmpq	%rdi, %rdx
	jne	.LBB8_71
	jmp	.LBB8_73
.LBB8_72:
	xorl	%esi, %esi
.LBB8_73:                               # %_ZNK8NArchive3N7z18CArchiveDatabaseEx21GetFolderFullPackSizeEi.exit
.Ltmp226:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp227:
.LBB8_74:                               # %_ZN11CStringBaseIwED2Ev.exit160
.Ltmp240:
	leaq	224(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp241:
# BB#75:
.Ltmp246:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp247:
# BB#76:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit159
	xorl	%eax, %eax
.LBB8_77:
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_79:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	je	.LBB8_82
.LBB8_80:                               # %vector.body1684.preheader.new
	movl	%ebx, %eax
.LBB8_81:                               # %vector.body1684
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rax,%rsi), %rdi
	movslq	%edi, %rdi
	movdqu	(%rcx,%rdi,8), %xmm2
	movdqu	16(%rcx,%rdi,8), %xmm3
	paddq	%xmm0, %xmm2
	paddq	%xmm1, %xmm3
	addl	$4, %edi
	movslq	%edi, %rdi
	movdqu	(%rcx,%rdi,8), %xmm0
	movdqu	16(%rcx,%rdi,8), %xmm1
	paddq	%xmm2, %xmm0
	paddq	%xmm3, %xmm1
	addq	$8, %rsi
	cmpq	%rsi, %rbp
	jne	.LBB8_81
.LBB8_82:                               # %middle.block1685
	paddq	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %rsi
	cmpq	%rbp, %rdx
	movq	%rbp, %rdi
	jne	.LBB8_66
	jmp	.LBB8_73
.LBB8_83:                               # %vector.body1540.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB8_89
# BB#84:                                # %vector.body1540.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rdx
	xorl	%esi, %esi
.LBB8_85:                               # %vector.body1540.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rsi,4), %xmm0
	movdqu	16(%rdi,%rsi,4), %xmm1
	movdqu	%xmm0, (%r9,%rsi,4)
	movdqu	%xmm1, 16(%r9,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB8_85
	jmp	.LBB8_90
.LBB8_86:                               # %vector.body1511.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB8_94
# BB#87:                                # %vector.body1511.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rdx
	xorl	%esi, %esi
.LBB8_88:                               # %vector.body1511.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rsi,4), %xmm0
	movdqu	16(%rdi,%rsi,4), %xmm1
	movdqu	%xmm0, (%r9,%rsi,4)
	movdqu	%xmm1, 16(%r9,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB8_88
	jmp	.LBB8_95
.LBB8_89:                               #   in Loop: Header=BB8_99 Depth=1
	xorl	%esi, %esi
.LBB8_90:                               # %vector.body1540.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$24, %rcx
	jb	.LBB8_93
# BB#91:                                # %vector.body1540.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r9,%rsi,4), %rdx
	leaq	112(%rdi,%rsi,4), %rsi
.LBB8_92:                               # %vector.body1540
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB8_92
.LBB8_93:                               # %middle.block1541
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	%rax, 40(%rsp)          # 8-byte Folded Reload
	jne	.LBB8_497
	jmp	.LBB8_504
.LBB8_94:                               #   in Loop: Header=BB8_99 Depth=1
	xorl	%esi, %esi
.LBB8_95:                               # %vector.body1511.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$24, %rcx
	jb	.LBB8_98
# BB#96:                                # %vector.body1511.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r9,%rsi,4), %rdx
	leaq	112(%rdi,%rsi,4), %rsi
.LBB8_97:                               # %vector.body1511
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB8_97
.LBB8_98:                               # %middle.block1512
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	%rax, %rbp
	jne	.LBB8_520
	jmp	.LBB8_527
.LBB8_99:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_364 Depth 2
                                        #     Child Loop BB8_425 Depth 2
                                        #     Child Loop BB8_116 Depth 2
                                        #     Child Loop BB8_119 Depth 2
                                        #     Child Loop BB8_418 Depth 2
                                        #     Child Loop BB8_440 Depth 2
                                        #     Child Loop BB8_165 Depth 2
                                        #     Child Loop BB8_168 Depth 2
                                        #     Child Loop BB8_237 Depth 2
                                        #     Child Loop BB8_415 Depth 2
                                        #     Child Loop BB8_435 Depth 2
                                        #     Child Loop BB8_151 Depth 2
                                        #     Child Loop BB8_154 Depth 2
                                        #     Child Loop BB8_173 Depth 2
                                        #     Child Loop BB8_181 Depth 2
                                        #     Child Loop BB8_184 Depth 2
                                        #     Child Loop BB8_189 Depth 2
                                        #     Child Loop BB8_212 Depth 2
                                        #     Child Loop BB8_215 Depth 2
                                        #     Child Loop BB8_356 Depth 2
                                        #     Child Loop BB8_462 Depth 2
                                        #     Child Loop BB8_466 Depth 2
                                        #     Child Loop BB8_470 Depth 2
                                        #     Child Loop BB8_473 Depth 2
                                        #     Child Loop BB8_477 Depth 2
                                        #     Child Loop BB8_205 Depth 2
                                        #     Child Loop BB8_198 Depth 2
                                        #     Child Loop BB8_222 Depth 2
                                        #     Child Loop BB8_225 Depth 2
                                        #     Child Loop BB8_230 Depth 2
                                        #     Child Loop BB8_85 Depth 2
                                        #     Child Loop BB8_92 Depth 2
                                        #     Child Loop BB8_499 Depth 2
                                        #     Child Loop BB8_502 Depth 2
                                        #     Child Loop BB8_88 Depth 2
                                        #     Child Loop BB8_97 Depth 2
                                        #     Child Loop BB8_522 Depth 2
                                        #     Child Loop BB8_525 Depth 2
                                        #     Child Loop BB8_530 Depth 2
                                        #     Child Loop BB8_385 Depth 2
                                        #     Child Loop BB8_430 Depth 2
                                        #     Child Loop BB8_259 Depth 2
                                        #     Child Loop BB8_262 Depth 2
                                        #     Child Loop BB8_284 Depth 2
                                        #       Child Loop BB8_270 Depth 3
                                        #       Child Loop BB8_277 Depth 3
                                        #       Child Loop BB8_302 Depth 3
                                        #       Child Loop BB8_304 Depth 3
                                        #       Child Loop BB8_273 Depth 3
                                        #       Child Loop BB8_282 Depth 3
                                        #       Child Loop BB8_325 Depth 3
                                        #       Child Loop BB8_327 Depth 3
                                        #     Child Loop BB8_449 Depth 2
                                        #     Child Loop BB8_457 Depth 2
                                        #     Child Loop BB8_350 Depth 2
                                        #     Child Loop BB8_353 Depth 2
                                        #     Child Loop BB8_421 Depth 2
                                        #     Child Loop BB8_445 Depth 2
                                        #     Child Loop BB8_405 Depth 2
                                        #     Child Loop BB8_408 Depth 2
	movq	16(%r12), %rax
	movq	-8(%rax,%rdi,8), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	testl	%r13d, %r13d
	movq	%rdi, 376(%rsp)         # 8-byte Spill
	je	.LBB8_105
# BB#100:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%r14d, %eax
	subl	%r13d, %eax
	cmpl	$1, %eax
	jg	.LBB8_104
# BB#101:                               #   in Loop: Header=BB8_99 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %r14d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r14d
	jl	.LBB8_103
# BB#102:                               # %select.true.sink
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r14d, %edx
	shrl	$31, %edx
	addl	%r14d, %edx
	sarl	%edx
.LBB8_103:                              # %select.end
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r14,%rsi), %r12d
	cmpl	%r14d, %r12d
	jne	.LBB8_106
.LBB8_104:                              #   in Loop: Header=BB8_99 Depth=1
	movl	%r14d, %r12d
	movq	%rbx, %rbp
	jmp	.LBB8_123
.LBB8_105:                              #   in Loop: Header=BB8_99 Depth=1
	movq	%r8, 16(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	jmp	.LBB8_124
.LBB8_106:                              #   in Loop: Header=BB8_99 Depth=1
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movslq	%r12d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp123:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp124:
# BB#107:                               # %.noexc239
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r14d, %r14d
	movq	16(%rsp), %rbx          # 8-byte Reload
	jle	.LBB8_122
# BB#108:                               # %.preheader.i.i230
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%r13, %rcx
	testl	%ecx, %ecx
	jle	.LBB8_120
# BB#109:                               # %.lr.ph.i.i231
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%ecx, %rax
	cmpl	$7, %ecx
	jbe	.LBB8_113
# BB#110:                               # %min.iters.checked1659
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB8_113
# BB#111:                               # %vector.memcheck1672
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rbx,%rax,4), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB8_362
# BB#112:                               # %vector.memcheck1672
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, 128(%rsp)         # 8-byte Folded Reload
	jae	.LBB8_362
.LBB8_113:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%ecx, %ecx
.LBB8_114:                              # %scalar.ph1657.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r13d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB8_117
# BB#115:                               # %scalar.ph1657.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rsi
.LBB8_116:                              # %scalar.ph1657.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rcx,4), %edi
	movl	%edi, (%rbp,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB8_116
.LBB8_117:                              # %scalar.ph1657.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$7, %rdx
	jb	.LBB8_121
# BB#118:                               # %scalar.ph1657.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	subq	%rcx, %rax
	leaq	28(%rbp,%rcx,4), %rdx
	leaq	28(%rbx,%rcx,4), %rcx
.LBB8_119:                              # %scalar.ph1657
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB8_119
	jmp	.LBB8_121
.LBB8_120:                              # %._crit_edge.i.i232
                                        #   in Loop: Header=BB8_99 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_122
.LBB8_121:                              # %._crit_edge.thread.i.i237
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	192(%rsp), %rdi         # 8-byte Reload
	callq	_ZdaPv
.LBB8_122:                              # %._crit_edge16.i.i238
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	movl	$0, (%rbp,%rax,4)
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	movq	%rbp, 216(%rsp)         # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 152(%rsp)         # 8-byte Spill
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	%rbp, 176(%rsp)         # 8-byte Spill
	movq	%rbp, 192(%rsp)         # 8-byte Spill
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movq	%rbp, 208(%rsp)         # 8-byte Spill
	movq	%rbp, 200(%rsp)         # 8-byte Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movq	%rbp, %r8
.LBB8_123:                              # %_ZN11CStringBaseIwEpLEw.exit226
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	$32, (%r8,%rax,4)
	incl	%r13d
	movq	%r13, %r15
	movl	%r12d, %r14d
	movq	%rbp, %rbx
.LBB8_124:                              #   in Loop: Header=BB8_99 Depth=1
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 272(%rsp)
.Ltmp125:
	movl	$16, %edi
	callq	_Znam
.Ltmp126:
# BB#125:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, 272(%rsp)
	movl	$0, (%rax)
	movl	$4, 284(%rsp)
.Ltmp128:
	movl	$16, %edi
	callq	_Znam
.Ltmp129:
# BB#126:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$0, (%rax)
	movq	256(%rsp), %r12         # 8-byte Reload
	movq	(%r12), %rdi
.Ltmp131:
	leaq	272(%rsp), %rsi
	callq	_Z10FindMethodyR11CStringBaseIwE
.Ltmp132:
	movq	%r15, %r13
# BB#127:                               #   in Loop: Header=BB8_99 Depth=1
	testb	%al, %al
	je	.LBB8_130
# BB#128:                               #   in Loop: Header=BB8_99 Depth=1
	movl	280(%rsp), %eax
	movl	%r14d, %ecx
	subl	%r13d, %ecx
	cmpl	%eax, %ecx
	jle	.LBB8_133
# BB#129:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%r14d, %ebp
	movq	%rbx, %r9
	movq	16(%rsp), %r8           # 8-byte Reload
	jmp	.LBB8_172
.LBB8_130:                              #   in Loop: Header=BB8_99 Depth=1
	movq	(%r12), %rsi
.Ltmp134:
	leaq	400(%rsp), %rdi
	callq	_Z23ConvertMethodIdToStringy
.Ltmp135:
# BB#131:                               #   in Loop: Header=BB8_99 Depth=1
	movl	408(%rsp), %eax
	movl	%r14d, %ecx
	subl	%r13d, %ecx
	cmpl	%eax, %ecx
	jle	.LBB8_137
# BB#132:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%r14d, %ebp
	movq	%rbx, %r9
	movq	16(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB8_236
.LBB8_133:                              #   in Loop: Header=BB8_99 Depth=1
	decl	%ecx
	cmpl	$8, %r14d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r14d
	movq	16(%rsp), %r8           # 8-byte Reload
	jl	.LBB8_135
# BB#134:                               # %select.true.sink3147
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r14d, %edx
	shrl	$31, %edx
	addl	%r14d, %edx
	sarl	%edx
.LBB8_135:                              # %select.end3146
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%r14,%rsi), %ebp
	cmpl	%r14d, %ebp
	jne	.LBB8_141
# BB#136:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%r14d, %ebp
	movq	%rbx, %r9
	jmp	.LBB8_172
.LBB8_137:                              #   in Loop: Header=BB8_99 Depth=1
	decl	%ecx
	cmpl	$8, %r14d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r14d
	movq	16(%rsp), %rdi          # 8-byte Reload
	jl	.LBB8_139
# BB#138:                               # %select.true.sink3121
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r14d, %edx
	shrl	$31, %edx
	addl	%r14d, %edx
	sarl	%edx
.LBB8_139:                              # %select.end3120
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%r14,%rsi), %r15d
	cmpl	%r14d, %r15d
	jne	.LBB8_155
# BB#140:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%r14d, %ebp
	movq	%rbx, %r9
	jmp	.LBB8_236
.LBB8_141:                              #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp140:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r9
.Ltmp141:
# BB#142:                               # %.noexc280
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%r15, %r13
	jle	.LBB8_171
# BB#143:                               # %.preheader.i.i271
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r13d, %r13d
	jle	.LBB8_169
# BB#144:                               # %.lr.ph.i.i272
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	cmpl	$7, %r13d
	jbe	.LBB8_148
# BB#145:                               # %min.iters.checked1602
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB8_148
# BB#146:                               # %vector.memcheck1615
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, %r9
	jae	.LBB8_413
# BB#147:                               # %vector.memcheck1615
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%r9,%rax,4), %rdx
	cmpq	%rdx, 80(%rsp)          # 8-byte Folded Reload
	jae	.LBB8_413
.LBB8_148:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%ecx, %ecx
.LBB8_149:                              # %scalar.ph1600.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r13d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB8_152
# BB#150:                               # %scalar.ph1600.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rsi
.LBB8_151:                              # %scalar.ph1600.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rcx,4), %edi
	movl	%edi, (%r9,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB8_151
.LBB8_152:                              # %scalar.ph1600.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$7, %rdx
	jb	.LBB8_170
# BB#153:                               # %scalar.ph1600.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	subq	%rcx, %rax
	leaq	28(%r9,%rcx,4), %rdx
	leaq	28(%rbp,%rcx,4), %rcx
.LBB8_154:                              # %scalar.ph1600
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB8_154
	jmp	.LBB8_170
.LBB8_155:                              #   in Loop: Header=BB8_99 Depth=1
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp137:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r9
.Ltmp138:
# BB#156:                               # %.noexc260
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r14d, %r14d
	movq	16(%rsp), %rbp          # 8-byte Reload
	jle	.LBB8_235
# BB#157:                               # %.preheader.i.i251
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r13d, %r13d
	jle	.LBB8_233
# BB#158:                               # %.lr.ph.i.i252
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	cmpl	$7, %r13d
	jbe	.LBB8_162
# BB#159:                               # %min.iters.checked1631
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB8_162
# BB#160:                               # %vector.memcheck1643
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, %r9
	jae	.LBB8_416
# BB#161:                               # %vector.memcheck1643
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%r9,%rax,4), %rdx
	cmpq	%rdx, 80(%rsp)          # 8-byte Folded Reload
	jae	.LBB8_416
.LBB8_162:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%ecx, %ecx
.LBB8_163:                              # %scalar.ph1629.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r13d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB8_166
# BB#164:                               # %scalar.ph1629.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rsi
.LBB8_165:                              # %scalar.ph1629.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rcx,4), %edi
	movl	%edi, (%r9,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB8_165
.LBB8_166:                              # %scalar.ph1629.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$7, %rdx
	jb	.LBB8_234
# BB#167:                               # %scalar.ph1629.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	subq	%rcx, %rax
	leaq	28(%r9,%rcx,4), %rdx
	leaq	28(%rbp,%rcx,4), %rcx
.LBB8_168:                              # %scalar.ph1629
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB8_168
	jmp	.LBB8_234
.LBB8_169:                              # %._crit_edge.i.i273
                                        #   in Loop: Header=BB8_99 Depth=1
	testq	%rbp, %rbp
	je	.LBB8_171
.LBB8_170:                              # %._crit_edge.thread.i.i278
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	176(%rsp), %rdi         # 8-byte Reload
	movq	%r9, %rbx
	callq	_ZdaPv
	movq	%rbx, %r9
.LBB8_171:                              # %._crit_edge16.i.i279
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	movl	$0, (%r9,%rax,4)
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movq	%r9, 216(%rsp)          # 8-byte Spill
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r9, 184(%rsp)          # 8-byte Spill
	movq	%r9, 168(%rsp)          # 8-byte Spill
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movq	%r9, 144(%rsp)          # 8-byte Spill
	movq	%r9, 176(%rsp)          # 8-byte Spill
	movq	%r9, 192(%rsp)          # 8-byte Spill
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movq	%r9, 120(%rsp)          # 8-byte Spill
	movq	%r9, 208(%rsp)          # 8-byte Spill
	movq	%r9, 200(%rsp)          # 8-byte Spill
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%r9, %r8
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB8_172:                              # %.noexc266
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	leaq	(%r8,%rax,4), %rcx
	movq	272(%rsp), %rdx
	.p2align	4, 0x90
.LBB8_173:                              #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rcx)
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB8_173
# BB#174:                               #   in Loop: Header=BB8_99 Depth=1
	movslq	280(%rsp), %r13
	addq	%rax, %r13
	movq	(%r12), %rax
	cmpq	$196864, %rax           # imm = 0x30100
	jle	.LBB8_190
# BB#175:                               #   in Loop: Header=BB8_99 Depth=1
	cmpq	$196865, %rax           # imm = 0x30101
	je	.LBB8_199
# BB#176:                               #   in Loop: Header=BB8_99 Depth=1
	cmpq	$197633, %rax           # imm = 0x30401
	je	.LBB8_208
# BB#177:                               #   in Loop: Header=BB8_99 Depth=1
	cmpq	$116459265, %rax        # imm = 0x6F10701
	jne	.LBB8_232
# BB#178:                               #   in Loop: Header=BB8_99 Depth=1
	cmpq	$0, 16(%r12)
	je	.LBB8_232
# BB#179:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r12, %rax
	movq	%r9, %rbx
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	24(%rax), %rax
	movzbl	(%rax), %edi
	andl	$63, %edi
.Ltmp142:
	leaq	400(%rsp), %rsi
	callq	_Z21ConvertUInt64ToStringyPw
.Ltmp143:
# BB#180:                               # %.noexc402.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	$-1, %r13d
	leaq	400(%rsp), %rax
.LBB8_181:                              # %.noexc402
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r13d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB8_181
# BB#182:                               # %_Z11MyStringLenIwEiPKT_.exit.i.i398
                                        #   in Loop: Header=BB8_99 Depth=1
	leal	1(%r13), %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	%rax, %r14
	movq	$-1, %rax
	cmovoq	%rax, %r14
.Ltmp144:
	movq	%r14, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp145:
# BB#183:                               # %.noexc403
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	$0, (%rbp)
	xorl	%eax, %eax
.LBB8_184:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i401
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	400(%rsp,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_184
# BB#185:                               #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	$0, (%r15)
	cmpl	$3, %r13d
	je	.LBB8_188
# BB#186:                               #   in Loop: Header=BB8_99 Depth=1
.Ltmp147:
	movq	%r14, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp148:
# BB#187:                               # %._crit_edge16.i.i409
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
	movl	$0, (%r15)
	movq	%r15, 8(%rsp)           # 8-byte Spill
.LBB8_188:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i410
                                        #   in Loop: Header=BB8_99 Depth=1
	xorl	%eax, %eax
.LBB8_189:                              #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_189
	jmp	.LBB8_231
.LBB8_190:                              #   in Loop: Header=BB8_99 Depth=1
	cmpq	$3, %rax
	je	.LBB8_219
# BB#191:                               #   in Loop: Header=BB8_99 Depth=1
	cmpq	$33, %rax
	jne	.LBB8_232
# BB#192:                               #   in Loop: Header=BB8_99 Depth=1
	cmpq	$1, 16(%r12)
	jne	.LBB8_232
# BB#193:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%r9, %r14
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	24(%r12), %rax
	movb	(%rax), %cl
	movl	%ecx, %eax
	andb	$1, %al
	orb	$2, %al
	movzbl	%al, %esi
	shrb	%cl
	addb	$11, %cl
	shll	%cl, %esi
.Ltmp169:
	leaq	400(%rsp), %rdi
	callq	_ZN8NArchive3N7zL21GetStringForSizeValueEj
.Ltmp170:
# BB#194:                               #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	$0, (%r15)
	movslq	408(%rsp), %r13
	leaq	1(%r13), %rax
	cmpl	$4, %eax
	je	.LBB8_197
# BB#195:                               #   in Loop: Header=BB8_99 Depth=1
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp172:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp173:
# BB#196:                               # %._crit_edge16.i.i308
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
	movl	$0, (%r15)
	movq	%r15, 8(%rsp)           # 8-byte Spill
.LBB8_197:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i309
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	400(%rsp), %rdi
	xorl	%eax, %eax
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r14, %r9
.LBB8_198:                              #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_198
	jmp	.LBB8_206
.LBB8_199:                              #   in Loop: Header=BB8_99 Depth=1
	cmpq	$5, 16(%r12)
	jne	.LBB8_232
# BB#200:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%r9, %r14
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	24(%r12), %rax
	movl	1(%rax), %esi
.Ltmp175:
	leaq	400(%rsp), %rdi
	callq	_ZN8NArchive3N7zL21GetStringForSizeValueEj
.Ltmp176:
# BB#201:                               #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	$0, (%r15)
	movslq	408(%rsp), %r13
	leaq	1(%r13), %rax
	cmpl	$4, %eax
	je	.LBB8_204
# BB#202:                               #   in Loop: Header=BB8_99 Depth=1
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp178:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp179:
# BB#203:                               # %._crit_edge16.i.i296
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
	movl	$0, (%r15)
	movq	%r15, 8(%rsp)           # 8-byte Spill
.LBB8_204:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i297
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	400(%rsp), %rdi
	xorl	%eax, %eax
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r14, %r9
.LBB8_205:                              #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_205
.LBB8_206:                              #   in Loop: Header=BB8_99 Depth=1
	testq	%rdi, %rdi
	je	.LBB8_481
# BB#207:                               #   in Loop: Header=BB8_99 Depth=1
	callq	_ZdaPv
	movq	%r14, %r9
	movq	16(%rsp), %r8           # 8-byte Reload
	testl	%r13d, %r13d
	jne	.LBB8_482
	jmp	.LBB8_484
.LBB8_208:                              #   in Loop: Header=BB8_99 Depth=1
	cmpq	$5, 16(%r12)
	jne	.LBB8_232
# BB#209:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	24(%r12), %rax
	movzbl	(%rax), %ebp
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	$0, (%r14)
.Ltmp150:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp151:
# BB#210:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	$111, (%rbx)
.Ltmp152:
	movq	%rbp, %rdi
	leaq	400(%rsp), %rsi
	callq	_Z21ConvertUInt64ToStringyPw
.Ltmp153:
# BB#211:                               # %.noexc329.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	$5, %eax
	movl	$4, %ebx
	xorl	%ecx, %ecx
.LBB8_212:                              # %.noexc329
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %r12
	movl	%eax, %r15d
	leaq	1(%r12), %rcx
	leal	1(%r15), %eax
	incl	%ebx
	cmpl	$0, 400(%rsp,%r12,4)
	jne	.LBB8_212
# BB#213:                               # %_Z11MyStringLenIwEiPKT_.exit.i.i325
                                        #   in Loop: Header=BB8_99 Depth=1
	leal	1(%r12), %r13d
	movslq	%r13d, %r14
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp154:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp155:
# BB#214:                               # %.noexc330
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	$0, (%rbp)
	xorl	%eax, %eax
.LBB8_215:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i328
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	400(%rsp,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_215
# BB#216:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%r13d, 56(%rsp)         # 4-byte Spill
	testl	%r12d, %r12d
	jle	.LBB8_354
# BB#217:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%r14, 240(%rsp)         # 8-byte Spill
	cmpl	$3, %r12d
	movl	$4, %r13d
	cmovgl	%r12d, %r13d
	addl	$3, %r13d
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp157:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp158:
# BB#218:                               # %.lr.ph.i.i340
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	(%rdi), %eax
	movl	%eax, (%r14)
	callq	_ZdaPv
	movl	$0, 4(%r14)
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	240(%rsp), %r14         # 8-byte Reload
	jmp	.LBB8_355
.LBB8_219:                              #   in Loop: Header=BB8_99 Depth=1
	cmpq	$1, 16(%r12)
	jne	.LBB8_232
# BB#220:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r12, %rax
	movq	%r9, %rbx
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	24(%rax), %rax
	movzbl	(%rax), %edi
	incq	%rdi
.Ltmp181:
	leaq	400(%rsp), %rsi
	callq	_Z21ConvertUInt64ToStringyPw
.Ltmp182:
# BB#221:                               # %.noexc282.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	$-1, %r13d
	leaq	400(%rsp), %rax
.LBB8_222:                              # %.noexc282
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r13d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB8_222
# BB#223:                               # %_Z11MyStringLenIwEiPKT_.exit.i.i
                                        #   in Loop: Header=BB8_99 Depth=1
	leal	1(%r13), %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	%rax, %r14
	movq	$-1, %rax
	cmovoq	%rax, %r14
.Ltmp183:
	movq	%r14, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp184:
# BB#224:                               # %.noexc283
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	$0, (%rbp)
	xorl	%eax, %eax
.LBB8_225:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	400(%rsp,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_225
# BB#226:                               #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	$0, (%r15)
	cmpl	$3, %r13d
	je	.LBB8_229
# BB#227:                               #   in Loop: Header=BB8_99 Depth=1
.Ltmp186:
	movq	%r14, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp187:
# BB#228:                               # %._crit_edge16.i.i286
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
	movl	$0, (%r15)
	movq	%r15, 8(%rsp)           # 8-byte Spill
.LBB8_229:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB8_99 Depth=1
	xorl	%eax, %eax
.LBB8_230:                              #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_230
.LBB8_231:                              # %_ZN11CStringBaseIwED2Ev.exit290
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	movq	256(%rsp), %r12         # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	testl	%r13d, %r13d
	jne	.LBB8_482
	jmp	.LBB8_484
.LBB8_232:                              #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rdi, %r15
	jmp	.LBB8_241
.LBB8_233:                              # %._crit_edge.i.i253
                                        #   in Loop: Header=BB8_99 Depth=1
	testq	%rbp, %rbp
	je	.LBB8_235
.LBB8_234:                              # %._crit_edge.thread.i.i258
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	168(%rsp), %rdi         # 8-byte Reload
	movq	%r9, %rbx
	callq	_ZdaPv
	movq	%rbx, %r9
.LBB8_235:                              # %._crit_edge16.i.i259
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	movl	$0, (%r9,%rax,4)
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movq	%r9, 216(%rsp)          # 8-byte Spill
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r9, 184(%rsp)          # 8-byte Spill
	movq	%r9, 168(%rsp)          # 8-byte Spill
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movq	%r9, 144(%rsp)          # 8-byte Spill
	movq	%r9, 176(%rsp)          # 8-byte Spill
	movq	%r9, 192(%rsp)          # 8-byte Spill
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movq	%r9, 120(%rsp)          # 8-byte Spill
	movq	%r9, 208(%rsp)          # 8-byte Spill
	movq	%r9, 200(%rsp)          # 8-byte Spill
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%r9, %rdi
	movq	%r15, %rbp
.LBB8_236:                              # %.noexc247
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	movq	%rdi, %r14
	leaq	(%rdi,%rax,4), %rax
	movq	400(%rsp), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_237:                              #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB8_237
# BB#238:                               #   in Loop: Header=BB8_99 Depth=1
	addl	408(%rsp), %r13d
	testq	%rdi, %rdi
	je	.LBB8_240
# BB#239:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%rbp, %rbx
	movq	%r9, %rbp
	callq	_ZdaPv
	movq	%rbp, %r9
	movq	%rbx, %rbp
.LBB8_240:                              # %_ZN11CStringBaseIwED2Ev.exit262
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rdi, %r15
	movq	%r14, %r8
.LBB8_241:                              # %.thread
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	movq	%r15, 24(%rsp)          # 8-byte Spill
	je	.LBB8_244
# BB#242:                               # %.preheader629.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	%ebp, %eax
	subl	%r13d, %eax
	cmpl	$2, %eax
	jle	.LBB8_245
# BB#243:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%ebp, %edi
	jmp	.LBB8_266
.LBB8_244:                              #   in Loop: Header=BB8_99 Depth=1
	movl	%ebp, %r14d
	movq	%r9, %rbx
	movq	248(%rsp), %r12         # 8-byte Reload
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB8_533
	jmp	.LBB8_534
.LBB8_245:                              #   in Loop: Header=BB8_99 Depth=1
	leal	-1(%rax), %edx
	cmpl	$8, %ebp
	movl	$4, %esi
	movl	$16, %edi
	cmovgl	%edi, %esi
	cmpl	$65, %ebp
	jl	.LBB8_247
# BB#246:                               # %select.true.sink3260
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%ebp, %esi
	shrl	$31, %esi
	addl	%ebp, %esi
	sarl	%esi
.LBB8_247:                              # %select.end3259
                                        #   in Loop: Header=BB8_99 Depth=1
	addl	%esi, %edx
	movl	$3, %edi
	subl	%eax, %edi
	cmpl	$2, %edx
	cmovgel	%esi, %edi
	leal	1(%rbp,%rdi), %eax
	cmpl	%ebp, %eax
	jne	.LBB8_249
# BB#248:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%ebp, %edi
	jmp	.LBB8_266
.LBB8_249:                              #   in Loop: Header=BB8_99 Depth=1
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r9, %rbx
	movq	%rax, %r14
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp193:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r9
.Ltmp194:
# BB#250:                               # %.noexc473
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%ebp, %ebp
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r14, %rdi
	jle	.LBB8_265
# BB#251:                               # %.preheader.i.i463
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r13d, %r13d
	jle	.LBB8_263
# BB#252:                               # %.lr.ph.i.i464
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	cmpl	$7, %r13d
	jbe	.LBB8_256
# BB#253:                               # %min.iters.checked1486
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB8_256
# BB#254:                               # %vector.memcheck1499
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rbx,%rax,4), %rdx
	cmpq	%rdx, %r9
	jae	.LBB8_383
# BB#255:                               # %vector.memcheck1499
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%r9,%rax,4), %rdx
	cmpq	%rdx, 120(%rsp)         # 8-byte Folded Reload
	jae	.LBB8_383
.LBB8_256:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%ecx, %ecx
.LBB8_257:                              # %scalar.ph1484.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r13d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB8_260
# BB#258:                               # %scalar.ph1484.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rsi
.LBB8_259:                              # %scalar.ph1484.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rcx,4), %edi
	movl	%edi, (%r9,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB8_259
.LBB8_260:                              # %scalar.ph1484.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$7, %rdx
	jb	.LBB8_264
# BB#261:                               # %scalar.ph1484.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	subq	%rcx, %rax
	leaq	28(%r9,%rcx,4), %rdx
	leaq	28(%rbx,%rcx,4), %rcx
.LBB8_262:                              # %scalar.ph1484
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB8_262
	jmp	.LBB8_264
.LBB8_263:                              # %._crit_edge.i.i465
                                        #   in Loop: Header=BB8_99 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_265
.LBB8_264:                              # %._crit_edge.thread.i.i470
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	184(%rsp), %rdi         # 8-byte Reload
	movq	%r9, %rbp
	callq	_ZdaPv
	movq	%r14, %rdi
	movq	%rbp, %r9
.LBB8_265:                              # %._crit_edge16.i.i471
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	movl	$0, (%r9,%rax,4)
	movq	16(%r12), %rcx
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movq	%r9, 216(%rsp)          # 8-byte Spill
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r9, 184(%rsp)          # 8-byte Spill
	movq	%r9, 168(%rsp)          # 8-byte Spill
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movq	%r9, 144(%rsp)          # 8-byte Spill
	movq	%r9, 176(%rsp)          # 8-byte Spill
	movq	%r9, 192(%rsp)          # 8-byte Spill
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movq	%r9, 120(%rsp)          # 8-byte Spill
	movq	%r9, 208(%rsp)          # 8-byte Spill
	movq	%r9, 200(%rsp)          # 8-byte Spill
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%r9, %r8
.LBB8_266:                              # %.noexc458
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	movabsq	$390842023994, %rdx     # imm = 0x5B0000003A
	movq	%rdx, (%r8,%rax,4)
	movl	$0, 8(%r8,%rax,4)
	addl	$2, %r13d
	testq	%rcx, %rcx
	je	.LBB8_333
# BB#267:                               # %.lr.ph
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rsi
	leaq	1(%rsi), %rax
	leaq	(,%rsi,4), %rdx
	movq	%rdx, 352(%rsp)         # 8-byte Spill
	leaq	4(,%rsi,4), %rdx
	movq	%rdx, 344(%rsp)         # 8-byte Spill
	leaq	-1(%rsi), %rdx
	movq	%rdx, 368(%rsp)         # 8-byte Spill
	leaq	-8(%rsi), %r11
	leaq	-7(%rsi), %rbx
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	%rax, %r14
	movq	%rsi, %r15
	xorl	%r10d, %r10d
	movq	%rsi, 312(%rsp)         # 8-byte Spill
	jmp	.LBB8_284
.LBB8_268:                              # %vector.body1453.preheader
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	leaq	-8(%rcx,%rdx,2), %rcx
	movq	%rcx, %rdx
	shrq	$3, %rdx
	incq	%rdx
	testb	$3, %dl
	je	.LBB8_274
# BB#269:                               # %vector.body1453.prol.preheader
                                        #   in Loop: Header=BB8_284 Depth=2
	movl	%r11d, %edx
	shrl	$3, %edx
	incl	%edx
	andl	$3, %edx
	negq	%rdx
	xorl	%esi, %esi
.LBB8_270:                              # %vector.body1453.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        #     Parent Loop BB8_284 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%rdi,%rsi,4), %xmm0
	movdqu	16(%rdi,%rsi,4), %xmm1
	movdqu	%xmm0, (%rbp,%rsi,4)
	movdqu	%xmm1, 16(%rbp,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB8_270
	jmp	.LBB8_275
.LBB8_271:                              # %vector.body1424.preheader
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	296(%rsp), %rsi         # 8-byte Reload
	leaq	-8(%rsi,%rdi,2), %rsi
	movq	%rsi, %rdi
	shrq	$3, %rdi
	incq	%rdi
	testb	$3, %dil
	je	.LBB8_279
# BB#272:                               # %vector.body1424.prol.preheader
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	304(%rsp), %rdi         # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	shrl	$3, %edi
	incl	%edi
	andl	$3, %edi
	negq	%rdi
	xorl	%ebp, %ebp
.LBB8_273:                              # %vector.body1424.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        #     Parent Loop BB8_284 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%rbx,%rbp,4), %xmm0
	movdqu	16(%rbx,%rbp,4), %xmm1
	movdqu	%xmm0, (%r9,%rbp,4)
	movdqu	%xmm1, 16(%r9,%rbp,4)
	addq	$8, %rbp
	incq	%rdi
	jne	.LBB8_273
	jmp	.LBB8_280
.LBB8_274:                              #   in Loop: Header=BB8_284 Depth=2
	xorl	%esi, %esi
.LBB8_275:                              # %vector.body1453.prol.loopexit
                                        #   in Loop: Header=BB8_284 Depth=2
	cmpq	$24, %rcx
	jb	.LBB8_278
# BB#276:                               # %vector.body1453.preheader.new
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	%r15, %rcx
	andq	$-8, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbp,%rsi,4), %rdx
	leaq	112(%rdi,%rsi,4), %rsi
.LBB8_277:                              # %vector.body1453
                                        #   Parent Loop BB8_99 Depth=1
                                        #     Parent Loop BB8_284 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB8_277
.LBB8_278:                              # %middle.block1454
                                        #   in Loop: Header=BB8_284 Depth=2
	cmpq	%rax, %r8
	jne	.LBB8_300
	jmp	.LBB8_306
.LBB8_279:                              #   in Loop: Header=BB8_284 Depth=2
	xorl	%ebp, %ebp
.LBB8_280:                              # %vector.body1424.prol.loopexit
                                        #   in Loop: Header=BB8_284 Depth=2
	cmpq	$24, %rsi
	jb	.LBB8_283
# BB#281:                               # %vector.body1424.preheader.new
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	%r14, %rsi
	andq	$-8, %rsi
	subq	%rbp, %rsi
	leaq	112(%rax,%rbp,4), %rax
	leaq	112(%rbx,%rbp,4), %rdi
.LBB8_282:                              # %vector.body1424
                                        #   Parent Loop BB8_99 Depth=1
                                        #     Parent Loop BB8_284 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rax)
	movups	%xmm1, -96(%rax)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rax)
	movups	%xmm1, -64(%rax)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rax)
	movups	%xmm1, -32(%rax)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rax)
	movdqu	%xmm1, (%rax)
	subq	$-128, %rax
	subq	$-128, %rdi
	addq	$-32, %rsi
	jne	.LBB8_282
.LBB8_283:                              # %middle.block1425
                                        #   in Loop: Header=BB8_284 Depth=2
	cmpq	%rcx, %rdx
	jne	.LBB8_323
	jmp	.LBB8_329
	.p2align	4, 0x90
.LBB8_284:                              #   Parent Loop BB8_99 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_270 Depth 3
                                        #       Child Loop BB8_277 Depth 3
                                        #       Child Loop BB8_302 Depth 3
                                        #       Child Loop BB8_304 Depth 3
                                        #       Child Loop BB8_273 Depth 3
                                        #       Child Loop BB8_282 Depth 3
                                        #       Child Loop BB8_325 Depth 3
                                        #       Child Loop BB8_327 Depth 3
	movq	%r10, %rbp
	leaq	1(%rbp), %r10
	cmpq	$6, %rbp
	jb	.LBB8_286
# BB#285:                               #   in Loop: Header=BB8_284 Depth=2
	cmpq	%rcx, %r10
	jb	.LBB8_334
.LBB8_286:                              #   in Loop: Header=BB8_284 Depth=2
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	312(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,2), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	24(%r12), %rax
	movb	(%rax,%rbp), %dl
	cmpb	$-96, %dl
	movl	$55, %r14d
	movl	$48, %eax
	cmovbl	%eax, %r14d
	movl	%edi, %eax
	subl	%r15d, %eax
	cmpl	$1, %eax
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r10, 240(%rsp)         # 8-byte Spill
	movq	%rbp, 264(%rsp)         # 8-byte Spill
	jle	.LBB8_288
# BB#287:                               #   in Loop: Header=BB8_284 Depth=2
	movl	%edi, %r13d
	movq	%r9, %rbp
	jmp	.LBB8_309
.LBB8_288:                              #   in Loop: Header=BB8_284 Depth=2
	movb	%dl, 48(%rsp)           # 1-byte Spill
	leal	-1(%rax), %ecx
	cmpl	$8, %edi
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %edi
	jl	.LBB8_290
# BB#289:                               # %select.true.sink3307
                                        #   in Loop: Header=BB8_284 Depth=2
	movl	%edi, %edx
	shrl	$31, %edx
	addl	%edi, %edx
	sarl	%edx
.LBB8_290:                              # %select.end3306
                                        #   in Loop: Header=BB8_284 Depth=2
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rdi,%rsi), %eax
	cmpl	%edi, %eax
	jne	.LBB8_292
# BB#291:                               #   in Loop: Header=BB8_284 Depth=2
	movl	%edi, %r13d
	movq	%r9, %rbp
	jmp	.LBB8_308
.LBB8_292:                              #   in Loop: Header=BB8_284 Depth=2
	movq	%r11, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rax, %r13
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp195:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp196:
# BB#293:                               # %.noexc191
                                        #   in Loop: Header=BB8_284 Depth=2
	testl	%r12d, %r12d
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	jle	.LBB8_307
# BB#294:                               # %.preheader.i.i182
                                        #   in Loop: Header=BB8_284 Depth=2
	testq	%r15, %r15
	jle	.LBB8_305
# BB#295:                               # %.lr.ph.i.i183.preheader
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	288(%rsp), %r8          # 8-byte Reload
	cmpq	$7, %r8
	jbe	.LBB8_299
# BB#296:                               # %min.iters.checked1457
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	%r8, %rax
	andq	$-8, %rax
	je	.LBB8_299
# BB#297:                               # %vector.memcheck1470
                                        #   in Loop: Header=BB8_284 Depth=2
	leaq	(%rdi,%r8,4), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB8_268
# BB#298:                               # %vector.memcheck1470
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	352(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	addq	%rbp, %rcx
	cmpq	%rcx, 208(%rsp)         # 8-byte Folded Reload
	jae	.LBB8_268
.LBB8_299:                              #   in Loop: Header=BB8_284 Depth=2
	xorl	%eax, %eax
.LBB8_300:                              # %.lr.ph.i.i183.preheader1707
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	368(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	leaq	(%rcx,%rdx,2), %rcx
	movq	288(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%eax, %edx
	subq	%rax, %rcx
	testb	$7, %dl
	je	.LBB8_303
# BB#301:                               # %.lr.ph.i.i183.prol.preheader
                                        #   in Loop: Header=BB8_284 Depth=2
	movl	%r15d, %edx
	subl	%eax, %edx
	andl	$7, %edx
	negq	%rdx
	.p2align	4, 0x90
.LBB8_302:                              # %.lr.ph.i.i183.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        #     Parent Loop BB8_284 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%rbp,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB8_302
.LBB8_303:                              # %.lr.ph.i.i183.prol.loopexit
                                        #   in Loop: Header=BB8_284 Depth=2
	cmpq	$7, %rcx
	jb	.LBB8_306
	.p2align	4, 0x90
.LBB8_304:                              # %.lr.ph.i.i183
                                        #   Parent Loop BB8_99 Depth=1
                                        #     Parent Loop BB8_284 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rax,4), %ecx
	movl	%ecx, (%rbp,%rax,4)
	movl	4(%rdi,%rax,4), %ecx
	movl	%ecx, 4(%rbp,%rax,4)
	movl	8(%rdi,%rax,4), %ecx
	movl	%ecx, 8(%rbp,%rax,4)
	movl	12(%rdi,%rax,4), %ecx
	movl	%ecx, 12(%rbp,%rax,4)
	movl	16(%rdi,%rax,4), %ecx
	movl	%ecx, 16(%rbp,%rax,4)
	movl	20(%rdi,%rax,4), %ecx
	movl	%ecx, 20(%rbp,%rax,4)
	movl	24(%rdi,%rax,4), %ecx
	movl	%ecx, 24(%rbp,%rax,4)
	movl	28(%rdi,%rax,4), %ecx
	movl	%ecx, 28(%rbp,%rax,4)
	addq	$8, %rax
	cmpq	%rax, %r15
	jne	.LBB8_304
	jmp	.LBB8_306
.LBB8_305:                              # %._crit_edge.i.i184
                                        #   in Loop: Header=BB8_284 Depth=2
	testq	%rdi, %rdi
	je	.LBB8_307
.LBB8_306:                              # %._crit_edge.thread.i.i189
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movq	32(%rsp), %r11          # 8-byte Reload
.LBB8_307:                              # %._crit_edge16.i.i190
                                        #   in Loop: Header=BB8_284 Depth=2
	movl	$0, (%rbp,%r15,4)
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	movq	%rbp, 216(%rsp)         # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 152(%rsp)         # 8-byte Spill
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	%rbp, 176(%rsp)         # 8-byte Spill
	movq	%rbp, 192(%rsp)         # 8-byte Spill
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movq	%rbp, 208(%rsp)         # 8-byte Spill
	movq	%rbp, 200(%rsp)         # 8-byte Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movq	%rbp, %r8
.LBB8_308:                              # %.noexc177
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	240(%rsp), %r10         # 8-byte Reload
	movb	48(%rsp), %dl           # 1-byte Reload
.LBB8_309:                              # %.noexc177
                                        #   in Loop: Header=BB8_284 Depth=2
	movl	%edx, %eax
	shrb	$4, %al
	movzbl	%al, %eax
	addl	%eax, %r14d
	movl	%r14d, (%r8,%r15,4)
	leaq	1(%r15), %rcx
	movl	$0, 4(%r8,%r15,4)
	andb	$15, %dl
	cmpb	$10, %dl
	movl	$55, %r12d
	movl	$48, %eax
	cmovbl	%eax, %r12d
	movl	%r13d, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	jle	.LBB8_311
# BB#310:                               #   in Loop: Header=BB8_284 Depth=2
	movl	%r13d, %edi
	movq	%rbp, %r9
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	jmp	.LBB8_331
.LBB8_311:                              #   in Loop: Header=BB8_284 Depth=2
	movb	%dl, 48(%rsp)           # 1-byte Spill
	leal	-1(%rax), %ecx
	cmpl	$8, %r13d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB8_313
# BB#312:                               # %select.true.sink3405
                                        #   in Loop: Header=BB8_284 Depth=2
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB8_313:                              # %select.end3404
                                        #   in Loop: Header=BB8_284 Depth=2
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r13,%rsi), %eax
	cmpl	%r13d, %eax
	jne	.LBB8_315
# BB#314:                               #   in Loop: Header=BB8_284 Depth=2
	movl	%r13d, %edi
	movq	%rbp, %r9
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	240(%rsp), %r10         # 8-byte Reload
	movb	48(%rsp), %dl           # 1-byte Reload
	jmp	.LBB8_331
.LBB8_315:                              #   in Loop: Header=BB8_284 Depth=2
	movq	%rbx, 304(%rsp)         # 8-byte Spill
	movq	%r11, 32(%rsp)          # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp197:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp198:
# BB#316:                               # %.noexc206
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	%rax, %r9
	testl	%r13d, %r13d
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	movb	48(%rsp), %dl           # 1-byte Reload
	movq	288(%rsp), %r8          # 8-byte Reload
	jle	.LBB8_330
# BB#317:                               # %.preheader.i.i197
                                        #   in Loop: Header=BB8_284 Depth=2
	testq	%r15, %r15
	js	.LBB8_328
# BB#318:                               # %.lr.ph.i.i198.preheader
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %rdi         # 8-byte Reload
	leaq	(%rcx,%rdi,2), %rdx
	cmpq	$7, %rdx
	jbe	.LBB8_322
# BB#319:                               # %min.iters.checked1428
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	%rdx, %rcx
	andq	$-8, %rcx
	je	.LBB8_322
# BB#320:                               # %vector.memcheck1441
                                        #   in Loop: Header=BB8_284 Depth=2
	leaq	(%rbx,%rdx,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB8_271
# BB#321:                               # %vector.memcheck1441
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	344(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdi,8), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, 200(%rsp)         # 8-byte Folded Reload
	jae	.LBB8_271
.LBB8_322:                              #   in Loop: Header=BB8_284 Depth=2
	xorl	%ecx, %ecx
.LBB8_323:                              # %.lr.ph.i.i198.preheader1706
                                        #   in Loop: Header=BB8_284 Depth=2
	subl	%ecx, %edx
	subq	%rcx, %r8
	testb	$7, %dl
	je	.LBB8_326
# BB#324:                               # %.lr.ph.i.i198.prol.preheader
                                        #   in Loop: Header=BB8_284 Depth=2
	movl	%r14d, %eax
	subl	%ecx, %eax
	andl	$7, %eax
	negq	%rax
	.p2align	4, 0x90
.LBB8_325:                              # %.lr.ph.i.i198.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        #     Parent Loop BB8_284 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx,%rcx,4), %edx
	movl	%edx, (%r9,%rcx,4)
	incq	%rcx
	incq	%rax
	jne	.LBB8_325
.LBB8_326:                              # %.lr.ph.i.i198.prol.loopexit
                                        #   in Loop: Header=BB8_284 Depth=2
	cmpq	$7, %r8
	jb	.LBB8_329
	.p2align	4, 0x90
.LBB8_327:                              # %.lr.ph.i.i198
                                        #   Parent Loop BB8_99 Depth=1
                                        #     Parent Loop BB8_284 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx,%rcx,4), %eax
	movl	%eax, (%r9,%rcx,4)
	movl	4(%rbx,%rcx,4), %eax
	movl	%eax, 4(%r9,%rcx,4)
	movl	8(%rbx,%rcx,4), %eax
	movl	%eax, 8(%r9,%rcx,4)
	movl	12(%rbx,%rcx,4), %eax
	movl	%eax, 12(%r9,%rcx,4)
	movl	16(%rbx,%rcx,4), %eax
	movl	%eax, 16(%r9,%rcx,4)
	movl	20(%rbx,%rcx,4), %eax
	movl	%eax, 20(%r9,%rcx,4)
	movl	24(%rbx,%rcx,4), %eax
	movl	%eax, 24(%r9,%rcx,4)
	movl	28(%rbx,%rcx,4), %eax
	movl	%eax, 28(%r9,%rcx,4)
	addq	$8, %rcx
	cmpq	%rcx, %r14
	jne	.LBB8_327
	jmp	.LBB8_329
.LBB8_328:                              # %._crit_edge.i.i199
                                        #   in Loop: Header=BB8_284 Depth=2
	testq	%rbx, %rbx
	je	.LBB8_330
.LBB8_329:                              # %._crit_edge.thread.i.i204
                                        #   in Loop: Header=BB8_284 Depth=2
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	%r9, %rbx
	callq	_ZdaPv
	movb	48(%rsp), %dl           # 1-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %r9
.LBB8_330:                              # %._crit_edge16.i.i205
                                        #   in Loop: Header=BB8_284 Depth=2
	movl	$0, 4(%r9,%r15,4)
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movq	%r9, 216(%rsp)          # 8-byte Spill
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r9, 184(%rsp)          # 8-byte Spill
	movq	%r9, 168(%rsp)          # 8-byte Spill
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movq	%r9, 144(%rsp)          # 8-byte Spill
	movq	%r9, 176(%rsp)          # 8-byte Spill
	movq	%r9, 192(%rsp)          # 8-byte Spill
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movq	%r9, 120(%rsp)          # 8-byte Spill
	movq	%r9, 208(%rsp)          # 8-byte Spill
	movq	%r9, 200(%rsp)          # 8-byte Spill
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%r9, %r8
	movq	304(%rsp), %rbx         # 8-byte Reload
	movq	240(%rsp), %r10         # 8-byte Reload
.LBB8_331:                              #   in Loop: Header=BB8_284 Depth=2
	movzbl	%dl, %eax
	addl	%eax, %r12d
	movl	%r12d, 4(%r8,%r15,4)
	movl	$0, 8(%r8,%r15,4)
	addq	$2, %r15
	movq	256(%rsp), %r12         # 8-byte Reload
	movq	16(%r12), %rcx
	addq	$2, %r11
	addq	$2, %rbx
	addq	$2, %r14
	addl	$2, %r13d
	cmpq	%rcx, %r10
	jb	.LBB8_284
# BB#332:                               # %.loopexit.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r15d, %r13d
.LBB8_333:                              # %.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%r9, %rbx
	movq	248(%rsp), %r12         # 8-byte Reload
	jmp	.LBB8_390
.LBB8_334:                              # %.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%edi, %eax
	subl	%r15d, %eax
	cmpl	$2, %eax
	jle	.LBB8_336
# BB#335:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%edi, %edx
	movq	%r9, %rbx
	movq	248(%rsp), %r12         # 8-byte Reload
	jmp	.LBB8_389
.LBB8_336:                              #   in Loop: Header=BB8_99 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %edi
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %edi
	jl	.LBB8_338
# BB#337:                               # %select.true.sink3284
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%edi, %edx
	shrl	$31, %edx
	addl	%edi, %edx
	sarl	%edx
.LBB8_338:                              # %select.end3283
                                        #   in Loop: Header=BB8_99 Depth=1
	addl	%edx, %ecx
	movl	$3, %esi
	subl	%eax, %esi
	cmpl	$2, %ecx
	cmovgel	%edx, %esi
	leal	1(%rdi,%rsi), %eax
	cmpl	%edi, %eax
	movq	248(%rsp), %r12         # 8-byte Reload
	jne	.LBB8_340
# BB#339:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%edi, %edx
	movq	%r9, %rbx
	jmp	.LBB8_389
.LBB8_340:                              #   in Loop: Header=BB8_99 Depth=1
	movq	%rdi, %rbp
	movq	%r9, %r14
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	%eax, 48(%rsp)          # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp200:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp201:
# BB#341:                               # %.noexc223
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%ebp, %ebp
	movq	16(%rsp), %rbp          # 8-byte Reload
	jle	.LBB8_388
# BB#342:                               # %.preheader.i.i214
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r15d, %r15d
	jle	.LBB8_386
# BB#343:                               # %.lr.ph.i.i215
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r15d, %rax
	cmpq	$7, %rax
	jbe	.LBB8_347
# BB#344:                               # %min.iters.checked1399
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB8_347
# BB#345:                               # %vector.memcheck1412
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB8_447
# BB#346:                               # %vector.memcheck1412
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%r15, %rdx
	shlq	$32, %rdx
	sarq	$30, %rdx
	addq	%rbx, %rdx
	cmpq	%rdx, 104(%rsp)         # 8-byte Folded Reload
	jae	.LBB8_447
.LBB8_347:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%ecx, %ecx
.LBB8_348:                              # %scalar.ph1397.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%eax, %edx
	subl	%ecx, %edx
	decq	%rax
	subq	%rcx, %rax
	testb	$7, %dl
	je	.LBB8_351
# BB#349:                               # %scalar.ph1397.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r13d, %edx
	subl	%ecx, %edx
	andl	$7, %edx
	negq	%rdx
.LBB8_350:                              # %scalar.ph1397.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rcx,4), %esi
	movl	%esi, (%rbx,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB8_350
.LBB8_351:                              # %scalar.ph1397.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$7, %rax
	jb	.LBB8_387
# BB#352:                               # %scalar.ph1397.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	subq	%rcx, %rax
	leaq	28(%rbx,%rcx,4), %rdx
	leaq	28(%rbp,%rcx,4), %rcx
.LBB8_353:                              # %scalar.ph1397
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB8_353
	jmp	.LBB8_387
.LBB8_354:                              #   in Loop: Header=BB8_99 Depth=1
	movl	$2, %r13d
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB8_355:                              # %.noexc334
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	$4, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB8_356:                              #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbp,%rax), %ecx
	movl	%ecx, (%rdx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_356
# BB#357:                               # %_ZN11CStringBaseIwED2Ev.exit351
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
	movl	%r13d, %eax
	subl	56(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$4, %eax
	jg	.LBB8_361
# BB#358:                               #   in Loop: Header=BB8_99 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %r13d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB8_360
# BB#359:                               # %select.true.sink3184
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB8_360:                              # %select.end3183
                                        #   in Loop: Header=BB8_99 Depth=1
	addl	%edx, %ecx
	movl	$5, %esi
	subl	%eax, %esi
	cmpl	$4, %ecx
	cmovgel	%edx, %esi
	leal	1(%r13,%rsi), %ebp
	cmpl	%r13d, %ebp
	jne	.LBB8_365
.LBB8_361:                              #   in Loop: Header=BB8_99 Depth=1
	movl	%r13d, %ebp
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jmp	.LBB8_371
.LBB8_362:                              # %vector.body1655.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_422
# BB#363:                               # %vector.body1655.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rsi
	xorl	%edi, %edi
.LBB8_364:                              # %vector.body1655.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rbx,%rdi,4), %xmm0
	movdqu	16(%rbx,%rdi,4), %xmm1
	movdqu	%xmm0, (%rbp,%rdi,4)
	movdqu	%xmm1, 16(%rbp,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB8_364
	jmp	.LBB8_423
.LBB8_365:                              #   in Loop: Header=BB8_99 Depth=1
	movq	%r14, 240(%rsp)         # 8-byte Spill
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp160:
	movq	%rax, %rdi
	movq	8(%rsp), %r14           # 8-byte Reload
	callq	_Znam
	movq	%rax, 56(%rsp)          # 8-byte Spill
.Ltmp161:
# BB#366:                               # %.noexc372
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r13d, %r13d
	movq	240(%rsp), %r14         # 8-byte Reload
	jle	.LBB8_370
# BB#367:                               # %.preheader.i.i362
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r12d, %r12d
	js	.LBB8_369
# BB#368:                               # %.lr.ph.i.i363.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(,%r14,4), %rdx
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	memcpy
.LBB8_369:                              # %._crit_edge.thread.i.i369
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
.LBB8_370:                              # %._crit_edge16.i.i370
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r14,4)
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB8_371:                              # %.noexc357
                                        #   in Loop: Header=BB8_99 Depth=1
	movdqa	.LCPI8_0(%rip), %xmm0   # xmm0 = [58,109,101,109]
	movq	24(%rsp), %rax          # 8-byte Reload
	movdqu	%xmm0, (%rax,%r14,4)
	movl	$0, 16(%rax,%r14,4)
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %rax
	movl	1(%rax), %esi
.Ltmp163:
	leaq	400(%rsp), %rdi
	callq	_ZN8NArchive3N7zL21GetStringForSizeValueEj
.Ltmp164:
# BB#372:                               #   in Loop: Header=BB8_99 Depth=1
	leal	5(%r12), %r14d
	movl	408(%rsp), %r13d
	movl	%ebp, %eax
	subl	%r14d, %eax
	cmpl	%r13d, %eax
	jle	.LBB8_374
# BB#373:                               #   in Loop: Header=BB8_99 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	jmp	.LBB8_476
.LBB8_374:                              #   in Loop: Header=BB8_99 Depth=1
	decl	%eax
	cmpl	$8, %ebp
	movl	$4, %ecx
	movl	$16, %edx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebp
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	jl	.LBB8_376
# BB#375:                               # %select.true.sink3192
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%ebp, %ecx
	shrl	$31, %ecx
	addl	%ebp, %ecx
	sarl	%ecx
.LBB8_376:                              # %select.end3191
                                        #   in Loop: Header=BB8_99 Depth=1
	leal	(%rcx,%rax), %edx
	movl	%r13d, %esi
	subl	%eax, %esi
	cmpl	%r13d, %edx
	cmovgel	%ecx, %esi
	leal	1(%rbp,%rsi), %eax
	cmpl	%ebp, %eax
	jne	.LBB8_378
# BB#377:                               #   in Loop: Header=BB8_99 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %r15          # 8-byte Reload
	jmp	.LBB8_476
.LBB8_378:                              #   in Loop: Header=BB8_99 Depth=1
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp166:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp167:
# BB#379:                               # %.noexc391
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%ebp, %ebp
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	jle	.LBB8_475
# BB#380:                               # %.preheader.i.i381
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpl	$-4, %r12d
	movq	24(%rsp), %rbp          # 8-byte Reload
	jl	.LBB8_474
# BB#381:                               # %.lr.ph.i.i382
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r14d, %rax
	cmpl	$8, %r14d
	jae	.LBB8_450
# BB#382:                               #   in Loop: Header=BB8_99 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB8_468
.LBB8_383:                              # %vector.body1482.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_427
# BB#384:                               # %vector.body1482.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rsi
	xorl	%edi, %edi
.LBB8_385:                              # %vector.body1482.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rbx,%rdi,4), %xmm0
	movdqu	16(%rbx,%rdi,4), %xmm1
	movdqu	%xmm0, (%r9,%rdi,4)
	movdqu	%xmm1, 16(%r9,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB8_385
	jmp	.LBB8_428
.LBB8_386:                              # %._crit_edge.i.i216
                                        #   in Loop: Header=BB8_99 Depth=1
	testq	%rbp, %rbp
	je	.LBB8_388
.LBB8_387:                              # %._crit_edge.thread.i.i221
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	152(%rsp), %rdi         # 8-byte Reload
	callq	_ZdaPv
.LBB8_388:                              # %._crit_edge16.i.i222
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r15d, %rax
	movl	$0, (%rbx,%rax,4)
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 160(%rsp)         # 8-byte Spill
	movq	%rbx, 216(%rsp)         # 8-byte Spill
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	movq	%rbx, 184(%rsp)         # 8-byte Spill
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	movq	%rbx, 208(%rsp)         # 8-byte Spill
	movq	%rbx, 200(%rsp)         # 8-byte Spill
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movq	%rbx, %r8
	movl	48(%rsp), %edx          # 4-byte Reload
.LBB8_389:                              # %.noexc210
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r15d, %rax
	movabsq	$197568495662, %rcx     # imm = 0x2E0000002E
	movq	%rcx, (%r8,%rax,4)
	movl	$0, 8(%r8,%rax,4)
	leal	2(%r15), %r13d
	movl	%edx, %edi
.LBB8_390:                              # %.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%edi, %eax
	subl	%r13d, %eax
	cmpl	$1, %eax
	jg	.LBB8_394
# BB#391:                               #   in Loop: Header=BB8_99 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %edi
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %edi
	jl	.LBB8_393
# BB#392:                               # %select.true.sink3516
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%edi, %edx
	shrl	$31, %edx
	addl	%edi, %edx
	sarl	%edx
.LBB8_393:                              # %select.end3515
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rdi,%rsi), %r14d
	cmpl	%edi, %r14d
	jne	.LBB8_395
.LBB8_394:                              #   in Loop: Header=BB8_99 Depth=1
	movl	%edi, %r14d
	movq	%rbx, %rbp
	jmp	.LBB8_412
.LBB8_395:                              #   in Loop: Header=BB8_99 Depth=1
	movq	%rdi, %r15
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp203:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp204:
# BB#396:                               # %.noexc176
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r15d, %r15d
	movq	16(%rsp), %rbx          # 8-byte Reload
	jle	.LBB8_411
# BB#397:                               # %.preheader.i.i
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r13d, %r13d
	jle	.LBB8_409
# BB#398:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	cmpl	$7, %r13d
	jbe	.LBB8_402
# BB#399:                               # %min.iters.checked
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB8_402
# BB#400:                               # %vector.memcheck
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rbx,%rax,4), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB8_419
# BB#401:                               # %vector.memcheck
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, 112(%rsp)         # 8-byte Folded Reload
	jae	.LBB8_419
.LBB8_402:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%ecx, %ecx
.LBB8_403:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r13d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB8_406
# BB#404:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rsi
.LBB8_405:                              # %scalar.ph.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rcx,4), %edi
	movl	%edi, (%rbp,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB8_405
.LBB8_406:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$7, %rdx
	jb	.LBB8_410
# BB#407:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	subq	%rcx, %rax
	leaq	28(%rbp,%rcx,4), %rdx
	leaq	28(%rbx,%rcx,4), %rcx
.LBB8_408:                              # %scalar.ph
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB8_408
	jmp	.LBB8_410
.LBB8_409:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB8_99 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_411
.LBB8_410:                              # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	160(%rsp), %rdi         # 8-byte Reload
	callq	_ZdaPv
.LBB8_411:                              # %._crit_edge16.i.i
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	movl	$0, (%rbp,%rax,4)
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	movq	%rbp, 216(%rsp)         # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 152(%rsp)         # 8-byte Spill
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	%rbp, 176(%rsp)         # 8-byte Spill
	movq	%rbp, 192(%rsp)         # 8-byte Spill
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movq	%rbp, 208(%rsp)         # 8-byte Spill
	movq	%rbp, 200(%rsp)         # 8-byte Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movq	%rbp, %r8
	movq	248(%rsp), %r12         # 8-byte Reload
.LBB8_412:                              # %_ZN11CStringBaseIwEpLEw.exit
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rax
	movq	$93, (%r8,%rax,4)
	incl	%r13d
	movq	%rbp, %rbx
	jmp	.LBB8_532
.LBB8_413:                              # %vector.body1598.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_432
# BB#414:                               # %vector.body1598.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rsi
	xorl	%edi, %edi
.LBB8_415:                              # %vector.body1598.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rbp,%rdi,4), %xmm0
	movdqu	16(%rbp,%rdi,4), %xmm1
	movdqu	%xmm0, (%r9,%rdi,4)
	movdqu	%xmm1, 16(%r9,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB8_415
	jmp	.LBB8_433
.LBB8_416:                              # %vector.body1627.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_437
# BB#417:                               # %vector.body1627.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rsi
	xorl	%edi, %edi
.LBB8_418:                              # %vector.body1627.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rbp,%rdi,4), %xmm0
	movdqu	16(%rbp,%rdi,4), %xmm1
	movdqu	%xmm0, (%r9,%rdi,4)
	movdqu	%xmm1, 16(%r9,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB8_418
	jmp	.LBB8_438
.LBB8_419:                              # %vector.body.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_442
# BB#420:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rsi
	xorl	%edi, %edi
.LBB8_421:                              # %vector.body.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rbx,%rdi,4), %xmm0
	movdqu	16(%rbx,%rdi,4), %xmm1
	movdqu	%xmm0, (%rbp,%rdi,4)
	movdqu	%xmm1, 16(%rbp,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB8_421
	jmp	.LBB8_443
.LBB8_422:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%edi, %edi
.LBB8_423:                              # %vector.body1655.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$24, %rdx
	jb	.LBB8_426
# BB#424:                               # %vector.body1655.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rbp,%rdi,4), %rsi
	leaq	112(%rbx,%rdi,4), %rdi
.LBB8_425:                              # %vector.body1655
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB8_425
.LBB8_426:                              # %middle.block1656
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB8_114
	jmp	.LBB8_121
.LBB8_427:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%edi, %edi
.LBB8_428:                              # %vector.body1482.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$24, %rdx
	jb	.LBB8_431
# BB#429:                               # %vector.body1482.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r9,%rdi,4), %rsi
	leaq	112(%rbx,%rdi,4), %rdi
.LBB8_430:                              # %vector.body1482
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB8_430
.LBB8_431:                              # %middle.block1483
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB8_257
	jmp	.LBB8_264
.LBB8_432:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%edi, %edi
.LBB8_433:                              # %vector.body1598.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$24, %rdx
	jb	.LBB8_436
# BB#434:                               # %vector.body1598.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r9,%rdi,4), %rsi
	leaq	112(%rbp,%rdi,4), %rdi
.LBB8_435:                              # %vector.body1598
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB8_435
.LBB8_436:                              # %middle.block1599
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB8_149
	jmp	.LBB8_170
.LBB8_437:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%edi, %edi
.LBB8_438:                              # %vector.body1627.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$24, %rdx
	jb	.LBB8_441
# BB#439:                               # %vector.body1627.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r9,%rdi,4), %rsi
	leaq	112(%rbp,%rdi,4), %rdi
.LBB8_440:                              # %vector.body1627
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB8_440
.LBB8_441:                              # %middle.block1628
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB8_163
	jmp	.LBB8_234
.LBB8_442:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%edi, %edi
.LBB8_443:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$24, %rdx
	jb	.LBB8_446
# BB#444:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rbp,%rdi,4), %rsi
	leaq	112(%rbx,%rdi,4), %rdi
.LBB8_445:                              # %vector.body
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB8_445
.LBB8_446:                              # %middle.block
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB8_403
	jmp	.LBB8_410
.LBB8_447:                              # %vector.body1395.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	testb	$3, %sil
	je	.LBB8_454
# BB#448:                               # %vector.body1395.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r13d, %esi
	andl	$24, %esi
	addl	$-8, %esi
	shrl	$3, %esi
	incl	%esi
	andl	$3, %esi
	negq	%rsi
	xorl	%edi, %edi
.LBB8_449:                              # %vector.body1395.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rbp,%rdi,4), %xmm0
	movdqu	16(%rbp,%rdi,4), %xmm1
	movdqu	%xmm0, (%rbx,%rdi,4)
	movdqu	%xmm1, 16(%rbx,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB8_449
	jmp	.LBB8_455
.LBB8_450:                              # %min.iters.checked1573
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB8_459
# BB#451:                               # %vector.memcheck1586
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, 8(%rsp)           # 8-byte Folded Reload
	jae	.LBB8_460
# BB#452:                               # %vector.memcheck1586
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	cmpq	%rdx, 56(%rsp)          # 8-byte Folded Reload
	jae	.LBB8_460
# BB#453:                               #   in Loop: Header=BB8_99 Depth=1
	xorl	%ecx, %ecx
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB8_468
.LBB8_454:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%edi, %edi
.LBB8_455:                              # %vector.body1395.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$24, %rdx
	jb	.LBB8_458
# BB#456:                               # %vector.body1395.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r13d, %rdx
	andq	$-8, %rdx
	subq	%rdi, %rdx
	leaq	112(%rbx,%rdi,4), %rsi
	leaq	112(%rbp,%rdi,4), %rdi
.LBB8_457:                              # %vector.body1395
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB8_457
.LBB8_458:                              # %middle.block1396
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB8_348
	jmp	.LBB8_387
.LBB8_459:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB8_468
.LBB8_460:                              # %vector.body1569.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	-8(%rcx), %r8
	movl	%r8d, %esi
	shrl	$3, %esi
	incl	%esi
	testb	$3, %sil
	je	.LBB8_463
# BB#461:                               # %vector.body1569.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r15d, %esi
	andl	$24, %esi
	addl	$-8, %esi
	shrl	$3, %esi
	incl	%esi
	andl	$3, %esi
	negq	%rsi
	xorl	%edi, %edi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB8_462:                              # %vector.body1569.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdx,%rdi,4), %xmm0
	movdqu	16(%rdx,%rdi,4), %xmm1
	movdqu	%xmm0, (%rbp,%rdi,4)
	movdqu	%xmm1, 16(%rbp,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB8_462
	jmp	.LBB8_464
.LBB8_463:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%edi, %edi
.LBB8_464:                              # %vector.body1569.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$24, %r8
	jb	.LBB8_467
# BB#465:                               # %vector.body1569.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r15d, %rdx
	andq	$-8, %rdx
	subq	%rdi, %rdx
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	112(%rsi,%rdi,4), %rsi
	movq	24(%rsp), %rbp          # 8-byte Reload
	leaq	112(%rbp,%rdi,4), %rdi
.LBB8_466:                              # %vector.body1569
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB8_466
.LBB8_467:                              # %middle.block1570
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	%rcx, %rax
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB8_474
.LBB8_468:                              # %scalar.ph1571.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r14d, %edx
	subl	%ecx, %edx
	decq	%rax
	subq	%rcx, %rax
	testb	$7, %dl
	je	.LBB8_471
# BB#469:                               # %scalar.ph1571.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r15d, %edx
	subl	%ecx, %edx
	andl	$7, %edx
	negq	%rdx
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB8_470:                              # %scalar.ph1571.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rcx,4), %esi
	movl	%esi, (%rdi,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB8_470
.LBB8_471:                              # %scalar.ph1571.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$7, %rax
	jb	.LBB8_474
# BB#472:                               # %scalar.ph1571.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r15d, %rax
	subq	%rcx, %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	28(%rdx,%rcx,4), %rdx
	leaq	28(%rbp,%rcx,4), %rcx
.LBB8_473:                              # %scalar.ph1571
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB8_473
.LBB8_474:                              # %._crit_edge.thread.i.i388
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
.LBB8_475:                              # %._crit_edge16.i.i389
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	%r14d, %rax
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	$0, (%r15,%rax,4)
.LBB8_476:                              # %.noexc376
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	400(%rsp), %rdi
	movslq	%ebx, %rax
	leaq	(%r15,%rax,4), %rax
	xorl	%ecx, %ecx
	movq	256(%rsp), %r12         # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB8_477:                              #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB8_477
# BB#478:                               #   in Loop: Header=BB8_99 Depth=1
	testq	%rdi, %rdi
	je	.LBB8_480
# BB#479:                               #   in Loop: Header=BB8_99 Depth=1
	callq	_ZdaPv
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
.LBB8_480:                              # %_ZN11CStringBaseIwED2Ev.exit393
                                        #   in Loop: Header=BB8_99 Depth=1
	addl	%r14d, %r13d
.LBB8_481:                              #   in Loop: Header=BB8_99 Depth=1
	testl	%r13d, %r13d
	je	.LBB8_484
.LBB8_482:                              #   in Loop: Header=BB8_99 Depth=1
	movl	%ebp, %eax
	subl	40(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$1, %eax
	movq	%r15, 24(%rsp)          # 8-byte Spill
	jle	.LBB8_485
# BB#483:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%ebp, %r12d
	jmp	.LBB8_506
.LBB8_484:                              #   in Loop: Header=BB8_99 Depth=1
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB8_241
.LBB8_485:                              #   in Loop: Header=BB8_99 Depth=1
	cmpl	$8, %ebp
	movl	$4, %ecx
	movl	$16, %edx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebp
	jl	.LBB8_487
# BB#486:                               # %select.true.sink3217
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%ebp, %ecx
	shrl	$31, %ecx
	addl	%ebp, %ecx
	sarl	%ecx
.LBB8_487:                              # %select.end3216
                                        #   in Loop: Header=BB8_99 Depth=1
	leal	-1(%rcx,%rax), %edx
	movl	$2, %esi
	subl	%eax, %esi
	testl	%edx, %edx
	cmovgl	%ecx, %esi
	leal	1(%rbp,%rsi), %r12d
	cmpl	%ebp, %r12d
	jne	.LBB8_489
# BB#488:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%ebp, %r12d
	jmp	.LBB8_506
.LBB8_489:                              #   in Loop: Header=BB8_99 Depth=1
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r9, %rbx
	movslq	%r12d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp189:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r9
.Ltmp190:
# BB#490:                               # %.noexc432
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%ebp, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jle	.LBB8_505
# BB#491:                               # %.preheader.i.i422
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	testl	%edx, %edx
	jle	.LBB8_503
# BB#492:                               # %.lr.ph.i.i423
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpl	$7, %edx
	jbe	.LBB8_496
# BB#493:                               # %min.iters.checked1544
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rdx, %rax
	andq	$-8, %rax
	je	.LBB8_496
# BB#494:                               # %vector.memcheck1557
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rdi,%rdx,4), %rcx
	cmpq	%rcx, %r9
	jae	.LBB8_83
# BB#495:                               # %vector.memcheck1557
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%r9,%rdx,4), %rcx
	cmpq	%rcx, 96(%rsp)          # 8-byte Folded Reload
	jae	.LBB8_83
.LBB8_496:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%eax, %eax
.LBB8_497:                              # %scalar.ph1542.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edx
	subl	%eax, %edx
	decq	%rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB8_500
# BB#498:                               # %scalar.ph1542.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rdx
.LBB8_499:                              # %scalar.ph1542.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%r9,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB8_499
.LBB8_500:                              # %scalar.ph1542.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$7, %rcx
	jb	.LBB8_504
# BB#501:                               # %scalar.ph1542.preheader.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	leaq	28(%r9,%rax,4), %rdx
	leaq	28(%rdi,%rax,4), %rax
.LBB8_502:                              # %scalar.ph1542
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB8_502
	jmp	.LBB8_504
.LBB8_503:                              # %._crit_edge.i.i424
                                        #   in Loop: Header=BB8_99 Depth=1
	testq	%rdi, %rdi
	je	.LBB8_505
.LBB8_504:                              # %._crit_edge.thread.i.i429
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	%r9, %rbp
	callq	_ZdaPv
	movq	%rbp, %r9
.LBB8_505:                              # %._crit_edge16.i.i430
                                        #   in Loop: Header=BB8_99 Depth=1
	movslq	40(%rsp), %rax          # 4-byte Folded Reload
	movl	$0, (%r9,%rax,4)
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movq	%r9, 216(%rsp)          # 8-byte Spill
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r9, 184(%rsp)          # 8-byte Spill
	movq	%r9, 168(%rsp)          # 8-byte Spill
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movq	%r9, 144(%rsp)          # 8-byte Spill
	movq	%r9, 176(%rsp)          # 8-byte Spill
	movq	%r9, 192(%rsp)          # 8-byte Spill
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movq	%r9, 120(%rsp)          # 8-byte Spill
	movq	%r9, 208(%rsp)          # 8-byte Spill
	movq	%r9, 200(%rsp)          # 8-byte Spill
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%r9, %r8
.LBB8_506:                              #   in Loop: Header=BB8_99 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	%eax, %rbp
	movl	$58, (%r8,%rbp,4)
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leal	1(%rax), %ecx
	movl	$0, 4(%r8,%rbp,4)
	incq	%rbp
	movl	%r12d, %eax
	subl	%ecx, %eax
	cmpl	%r13d, %eax
	jle	.LBB8_508
# BB#507:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%r12d, %r14d
	jmp	.LBB8_529
.LBB8_508:                              #   in Loop: Header=BB8_99 Depth=1
	decl	%eax
	cmpl	$8, %r12d
	movl	$4, %ecx
	movl	$16, %edx
	cmovgl	%edx, %ecx
	cmpl	$65, %r12d
	jl	.LBB8_510
# BB#509:                               # %select.true.sink3240
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%r12d, %ecx
	shrl	$31, %ecx
	addl	%r12d, %ecx
	sarl	%ecx
.LBB8_510:                              # %select.end3239
                                        #   in Loop: Header=BB8_99 Depth=1
	leal	(%rcx,%rax), %edx
	movl	%r13d, %esi
	subl	%eax, %esi
	cmpl	%r13d, %edx
	cmovgel	%ecx, %esi
	leal	1(%r12,%rsi), %r14d
	cmpl	%r12d, %r14d
	jne	.LBB8_512
# BB#511:                               #   in Loop: Header=BB8_99 Depth=1
	movl	%r12d, %r14d
	jmp	.LBB8_529
.LBB8_512:                              #   in Loop: Header=BB8_99 Depth=1
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r9, %rbx
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp191:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r9
.Ltmp192:
# BB#513:                               # %.noexc451
                                        #   in Loop: Header=BB8_99 Depth=1
	testl	%r12d, %r12d
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jle	.LBB8_528
# BB#514:                               # %.preheader.i.i441
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	js	.LBB8_526
# BB#515:                               # %.lr.ph.i.i442.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpl	$7, %ebp
	jbe	.LBB8_519
# BB#516:                               # %min.iters.checked1515
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rbp, %rax
	andq	$-8, %rax
	je	.LBB8_519
# BB#517:                               # %vector.memcheck1528
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%rdi,%rbp,4), %rcx
	cmpq	%rcx, %r9
	jae	.LBB8_86
# BB#518:                               # %vector.memcheck1528
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%r9,%rbp,4), %rcx
	cmpq	%rcx, 88(%rsp)          # 8-byte Folded Reload
	jae	.LBB8_86
.LBB8_519:                              #   in Loop: Header=BB8_99 Depth=1
	xorl	%eax, %eax
.LBB8_520:                              # %.lr.ph.i.i442.preheader1708
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	%ebp, %edx
	subl	%eax, %edx
	leaq	-1(%rbp), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB8_523
# BB#521:                               # %.lr.ph.i.i442.prol.preheader
                                        #   in Loop: Header=BB8_99 Depth=1
	negq	%rdx
.LBB8_522:                              # %.lr.ph.i.i442.prol
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%r9,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB8_522
.LBB8_523:                              # %.lr.ph.i.i442.prol.loopexit
                                        #   in Loop: Header=BB8_99 Depth=1
	cmpq	$7, %rcx
	jb	.LBB8_527
# BB#524:                               # %.lr.ph.i.i442.preheader1708.new
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	%rbp, %rcx
	subq	%rax, %rcx
	leaq	28(%r9,%rax,4), %rdx
	leaq	28(%rdi,%rax,4), %rax
.LBB8_525:                              # %.lr.ph.i.i442
                                        #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB8_525
	jmp	.LBB8_527
.LBB8_526:                              # %._crit_edge.i.i443
                                        #   in Loop: Header=BB8_99 Depth=1
	testq	%rdi, %rdi
	je	.LBB8_528
.LBB8_527:                              # %._crit_edge.thread.i.i448
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	136(%rsp), %rdi         # 8-byte Reload
	movq	%r9, %rbx
	callq	_ZdaPv
	movq	%rbx, %r9
.LBB8_528:                              # %._crit_edge16.i.i449
                                        #   in Loop: Header=BB8_99 Depth=1
	movl	$0, (%r9,%rbp,4)
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movq	%r9, 216(%rsp)          # 8-byte Spill
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r9, 184(%rsp)          # 8-byte Spill
	movq	%r9, 168(%rsp)          # 8-byte Spill
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movq	%r9, 144(%rsp)          # 8-byte Spill
	movq	%r9, 176(%rsp)          # 8-byte Spill
	movq	%r9, 192(%rsp)          # 8-byte Spill
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movq	%r9, 120(%rsp)          # 8-byte Spill
	movq	%r9, 208(%rsp)          # 8-byte Spill
	movq	%r9, 200(%rsp)          # 8-byte Spill
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%r9, %r8
.LBB8_529:                              # %.noexc436
                                        #   in Loop: Header=BB8_99 Depth=1
	leaq	(%r8,%rbp,4), %rax
	xorl	%ecx, %ecx
	movq	248(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_530:                              #   Parent Loop BB8_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r15,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB8_530
# BB#531:                               # %_ZN11CStringBaseIwEpLERKS0_.exit437
                                        #   in Loop: Header=BB8_99 Depth=1
	addl	%ebp, %r13d
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	movq	%r9, %rbx
.LBB8_532:                              #   in Loop: Header=BB8_99 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB8_534
.LBB8_533:                              #   in Loop: Header=BB8_99 Depth=1
	movq	%r8, %rbp
	callq	_ZdaPv
	movq	%rbp, %r8
.LBB8_534:                              # %_ZN11CStringBaseIwED2Ev.exit171
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	272(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_536
# BB#535:                               #   in Loop: Header=BB8_99 Depth=1
	movq	%r8, %rbp
	callq	_ZdaPv
	movq	%rbp, %r8
.LBB8_536:                              # %_ZN11CStringBaseIwED2Ev.exit165
                                        #   in Loop: Header=BB8_99 Depth=1
	movq	376(%rsp), %rdi         # 8-byte Reload
	cmpq	$1, %rdi
	leaq	-1(%rdi), %rdi
	jg	.LBB8_99
	jmp	.LBB8_61
.LBB8_537:
.Ltmp208:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jmp	.LBB8_579
.LBB8_538:
.Ltmp225:
	jmp	.LBB8_547
.LBB8_539:
.Ltmp239:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	384(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB8_587
	jmp	.LBB8_588
.LBB8_540:
.Ltmp122:
	jmp	.LBB8_547
.LBB8_541:
.Ltmp119:
	jmp	.LBB8_547
.LBB8_542:
.Ltmp230:
	jmp	.LBB8_547
.LBB8_543:
.Ltmp116:
	jmp	.LBB8_547
.LBB8_544:
.Ltmp113:
	jmp	.LBB8_547
.LBB8_545:
.Ltmp248:
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB8_589
.LBB8_546:
.Ltmp242:
.LBB8_547:                              # %_ZN11CStringBaseIwED2Ev.exit161
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB8_588
.LBB8_548:
.Ltmp168:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	400(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_560
# BB#549:
	callq	_ZdaPv
	jmp	.LBB8_560
.LBB8_550:                              # %_ZN11CStringBaseIwED2Ev.exit291
.Ltmp188:
	jmp	.LBB8_557
.LBB8_551:
.Ltmp180:
	jmp	.LBB8_553
.LBB8_552:
.Ltmp174:
.LBB8_553:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	400(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_564
# BB#554:
	callq	_ZdaPv
	jmp	.LBB8_564
.LBB8_555:                              # %_ZN11CStringBaseIwED2Ev.exit394
.Ltmp159:
	movq	%rax, %r15
	movq	%rbp, %rdi
	movq	%rdx, %rbp
	callq	_ZdaPv
	jmp	.LBB8_567
.LBB8_556:                              # %_ZN11CStringBaseIwED2Ev.exit416
.Ltmp149:
.LBB8_557:                              # %.thread618
	movq	%rax, %r15
	movq	%rbp, %rdi
	movq	%rdx, %rbp
	callq	_ZdaPv
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB8_582
.LBB8_558:                              # %.loopexit.split-lp
.Ltmp202:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	%r14, %rbx
	jmp	.LBB8_581
.LBB8_559:
.Ltmp165:
	movq	%rdx, %rbp
	movq	%rax, %r15
.LBB8_560:                              # %_ZN11CStringBaseIwED2Ev.exit395
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB8_582
.LBB8_561:
.Ltmp177:
	jmp	.LBB8_563
.LBB8_562:
.Ltmp171:
.LBB8_563:                              # %_ZN11CStringBaseIwED2Ev.exit303
	movq	%rdx, %rbp
	movq	%rax, %r15
.LBB8_564:                              # %_ZN11CStringBaseIwED2Ev.exit303
	movq	%r14, %rbx
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB8_582
.LBB8_565:
.Ltmp162:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	%r14, %rdi
	jmp	.LBB8_582
.LBB8_566:
.Ltmp156:
	movq	%rdx, %rbp
	movq	%rax, %r15
.LBB8_567:                              # %.thread618
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB8_582
.LBB8_568:
.Ltmp185:
	jmp	.LBB8_576
.LBB8_569:
.Ltmp146:
	jmp	.LBB8_576
.LBB8_570:
.Ltmp139:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	400(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_572
# BB#571:
	callq	_ZdaPv
.LBB8_572:                              # %_ZN11CStringBaseIwED2Ev.exit263
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB8_582
.LBB8_573:
.Ltmp205:
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB8_581
.LBB8_574:
.Ltmp136:
	jmp	.LBB8_576
.LBB8_575:
.Ltmp133:
.LBB8_576:                              # %.thread618
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB8_582
.LBB8_577:
.Ltmp130:
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB8_583
.LBB8_578:
.Ltmp127:
.LBB8_579:
	movq	%rdx, %rbp
	movq	%rax, %r15
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB8_586
	jmp	.LBB8_588
.LBB8_580:                              # %.loopexit628
.Ltmp199:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	72(%rsp), %rbx          # 8-byte Reload
.LBB8_581:
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB8_583
.LBB8_582:                              # %.thread618
	callq	_ZdaPv
.LBB8_583:                              # %_ZN11CStringBaseIwED2Ev.exit164
	movq	272(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_585
# BB#584:
	callq	_ZdaPv
.LBB8_585:
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB8_588
.LBB8_586:
	movq	%rbx, %rdi
.LBB8_587:                              # %_ZN11CStringBaseIwED2Ev.exit161
	callq	_ZdaPv
.LBB8_588:                              # %_ZN11CStringBaseIwED2Ev.exit161
.Ltmp243:
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp244:
.LBB8_589:                              # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%r15, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %ebp
	je	.LBB8_591
# BB#590:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB8_77
.LBB8_591:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp249:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp250:
# BB#592:
.LBB8_593:
.Ltmp251:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_594:
.Ltmp245:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN8NArchive3N7z8CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end8-_ZN8NArchive3N7z8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_2
	.quad	.LBB8_74
	.quad	.LBB8_74
	.quad	.LBB8_38
	.quad	.LBB8_39
	.quad	.LBB8_29
	.quad	.LBB8_27
	.quad	.LBB8_45
	.quad	.LBB8_19
	.quad	.LBB8_40
	.quad	.LBB8_74
	.quad	.LBB8_74
	.quad	.LBB8_14
	.quad	.LBB8_74
	.quad	.LBB8_74
	.quad	.LBB8_74
	.quad	.LBB8_22
	.quad	.LBB8_74
	.quad	.LBB8_52
	.quad	.LBB8_48
	.quad	.LBB8_74
	.quad	.LBB8_74
	.quad	.LBB8_74
	.quad	.LBB8_74
	.quad	.LBB8_43
	.quad	.LBB8_74
	.quad	.LBB8_24
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\330\004"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\311\004"              # Call site table length
	.long	.Ltmp235-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp236-.Ltmp235       #   Call between .Ltmp235 and .Ltmp236
	.long	.Ltmp242-.Lfunc_begin3  #     jumps to .Ltmp242
	.byte	3                       #   On action: 2
	.long	.Ltmp237-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp238-.Ltmp237       #   Call between .Ltmp237 and .Ltmp238
	.long	.Ltmp239-.Lfunc_begin3  #     jumps to .Ltmp239
	.byte	3                       #   On action: 2
	.long	.Ltmp111-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp110-.Ltmp111       #   Call between .Ltmp111 and .Ltmp110
	.long	.Ltmp113-.Lfunc_begin3  #     jumps to .Ltmp113
	.byte	3                       #   On action: 2
	.long	.Ltmp114-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin3  #     jumps to .Ltmp116
	.byte	3                       #   On action: 2
	.long	.Ltmp219-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp214-.Ltmp219       #   Call between .Ltmp219 and .Ltmp214
	.long	.Ltmp242-.Lfunc_begin3  #     jumps to .Ltmp242
	.byte	3                       #   On action: 2
	.long	.Ltmp223-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin3  #     jumps to .Ltmp225
	.byte	3                       #   On action: 2
	.long	.Ltmp215-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp218-.Ltmp215       #   Call between .Ltmp215 and .Ltmp218
	.long	.Ltmp242-.Lfunc_begin3  #     jumps to .Ltmp242
	.byte	3                       #   On action: 2
	.long	.Ltmp117-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp118-.Ltmp117       #   Call between .Ltmp117 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin3  #     jumps to .Ltmp119
	.byte	3                       #   On action: 2
	.long	.Ltmp221-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp222-.Ltmp221       #   Call between .Ltmp221 and .Ltmp222
	.long	.Ltmp242-.Lfunc_begin3  #     jumps to .Ltmp242
	.byte	3                       #   On action: 2
	.long	.Ltmp120-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin3  #     jumps to .Ltmp122
	.byte	3                       #   On action: 2
	.long	.Ltmp211-.Lfunc_begin3  # >> Call Site 11 <<
	.long	.Ltmp210-.Ltmp211       #   Call between .Ltmp211 and .Ltmp210
	.long	.Ltmp242-.Lfunc_begin3  #     jumps to .Ltmp242
	.byte	3                       #   On action: 2
	.long	.Ltmp228-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin3  #     jumps to .Ltmp230
	.byte	3                       #   On action: 2
	.long	.Ltmp206-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp207-.Ltmp206       #   Call between .Ltmp206 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin3  #     jumps to .Ltmp208
	.byte	3                       #   On action: 2
	.long	.Ltmp226-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp227-.Ltmp226       #   Call between .Ltmp226 and .Ltmp227
	.long	.Ltmp230-.Lfunc_begin3  #     jumps to .Ltmp230
	.byte	3                       #   On action: 2
	.long	.Ltmp240-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp241-.Ltmp240       #   Call between .Ltmp240 and .Ltmp241
	.long	.Ltmp242-.Lfunc_begin3  #     jumps to .Ltmp242
	.byte	3                       #   On action: 2
	.long	.Ltmp246-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp247-.Ltmp246       #   Call between .Ltmp246 and .Ltmp247
	.long	.Ltmp248-.Lfunc_begin3  #     jumps to .Ltmp248
	.byte	3                       #   On action: 2
	.long	.Ltmp123-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp126-.Ltmp123       #   Call between .Ltmp123 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin3  #     jumps to .Ltmp127
	.byte	3                       #   On action: 2
	.long	.Ltmp128-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin3  #     jumps to .Ltmp130
	.byte	3                       #   On action: 2
	.long	.Ltmp131-.Lfunc_begin3  # >> Call Site 19 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin3  #     jumps to .Ltmp133
	.byte	3                       #   On action: 2
	.long	.Ltmp134-.Lfunc_begin3  # >> Call Site 20 <<
	.long	.Ltmp135-.Ltmp134       #   Call between .Ltmp134 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin3  #     jumps to .Ltmp136
	.byte	3                       #   On action: 2
	.long	.Ltmp140-.Lfunc_begin3  # >> Call Site 21 <<
	.long	.Ltmp141-.Ltmp140       #   Call between .Ltmp140 and .Ltmp141
	.long	.Ltmp205-.Lfunc_begin3  #     jumps to .Ltmp205
	.byte	3                       #   On action: 2
	.long	.Ltmp137-.Lfunc_begin3  # >> Call Site 22 <<
	.long	.Ltmp138-.Ltmp137       #   Call between .Ltmp137 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin3  #     jumps to .Ltmp139
	.byte	3                       #   On action: 2
	.long	.Ltmp142-.Lfunc_begin3  # >> Call Site 23 <<
	.long	.Ltmp145-.Ltmp142       #   Call between .Ltmp142 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin3  #     jumps to .Ltmp146
	.byte	3                       #   On action: 2
	.long	.Ltmp147-.Lfunc_begin3  # >> Call Site 24 <<
	.long	.Ltmp148-.Ltmp147       #   Call between .Ltmp147 and .Ltmp148
	.long	.Ltmp149-.Lfunc_begin3  #     jumps to .Ltmp149
	.byte	3                       #   On action: 2
	.long	.Ltmp169-.Lfunc_begin3  # >> Call Site 25 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin3  #     jumps to .Ltmp171
	.byte	3                       #   On action: 2
	.long	.Ltmp172-.Lfunc_begin3  # >> Call Site 26 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin3  #     jumps to .Ltmp174
	.byte	3                       #   On action: 2
	.long	.Ltmp175-.Lfunc_begin3  # >> Call Site 27 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin3  #     jumps to .Ltmp177
	.byte	3                       #   On action: 2
	.long	.Ltmp178-.Lfunc_begin3  # >> Call Site 28 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin3  #     jumps to .Ltmp180
	.byte	3                       #   On action: 2
	.long	.Ltmp150-.Lfunc_begin3  # >> Call Site 29 <<
	.long	.Ltmp151-.Ltmp150       #   Call between .Ltmp150 and .Ltmp151
	.long	.Ltmp162-.Lfunc_begin3  #     jumps to .Ltmp162
	.byte	3                       #   On action: 2
	.long	.Ltmp152-.Lfunc_begin3  # >> Call Site 30 <<
	.long	.Ltmp155-.Ltmp152       #   Call between .Ltmp152 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin3  #     jumps to .Ltmp156
	.byte	3                       #   On action: 2
	.long	.Ltmp157-.Lfunc_begin3  # >> Call Site 31 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin3  #     jumps to .Ltmp159
	.byte	3                       #   On action: 2
	.long	.Ltmp181-.Lfunc_begin3  # >> Call Site 32 <<
	.long	.Ltmp184-.Ltmp181       #   Call between .Ltmp181 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin3  #     jumps to .Ltmp185
	.byte	3                       #   On action: 2
	.long	.Ltmp186-.Lfunc_begin3  # >> Call Site 33 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin3  #     jumps to .Ltmp188
	.byte	3                       #   On action: 2
	.long	.Ltmp193-.Lfunc_begin3  # >> Call Site 34 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp205-.Lfunc_begin3  #     jumps to .Ltmp205
	.byte	3                       #   On action: 2
	.long	.Ltmp195-.Lfunc_begin3  # >> Call Site 35 <<
	.long	.Ltmp198-.Ltmp195       #   Call between .Ltmp195 and .Ltmp198
	.long	.Ltmp199-.Lfunc_begin3  #     jumps to .Ltmp199
	.byte	3                       #   On action: 2
	.long	.Ltmp200-.Lfunc_begin3  # >> Call Site 36 <<
	.long	.Ltmp201-.Ltmp200       #   Call between .Ltmp200 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin3  #     jumps to .Ltmp202
	.byte	3                       #   On action: 2
	.long	.Ltmp160-.Lfunc_begin3  # >> Call Site 37 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin3  #     jumps to .Ltmp162
	.byte	3                       #   On action: 2
	.long	.Ltmp161-.Lfunc_begin3  # >> Call Site 38 <<
	.long	.Ltmp163-.Ltmp161       #   Call between .Ltmp161 and .Ltmp163
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin3  # >> Call Site 39 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin3  #     jumps to .Ltmp165
	.byte	3                       #   On action: 2
	.long	.Ltmp166-.Lfunc_begin3  # >> Call Site 40 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin3  #     jumps to .Ltmp168
	.byte	3                       #   On action: 2
	.long	.Ltmp203-.Lfunc_begin3  # >> Call Site 41 <<
	.long	.Ltmp192-.Ltmp203       #   Call between .Ltmp203 and .Ltmp192
	.long	.Ltmp205-.Lfunc_begin3  #     jumps to .Ltmp205
	.byte	3                       #   On action: 2
	.long	.Ltmp243-.Lfunc_begin3  # >> Call Site 42 <<
	.long	.Ltmp244-.Ltmp243       #   Call between .Ltmp243 and .Ltmp244
	.long	.Ltmp245-.Lfunc_begin3  #     jumps to .Ltmp245
	.byte	1                       #   On action: 1
	.long	.Ltmp244-.Lfunc_begin3  # >> Call Site 43 <<
	.long	.Ltmp249-.Ltmp244       #   Call between .Ltmp244 and .Ltmp249
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp249-.Lfunc_begin3  # >> Call Site 44 <<
	.long	.Ltmp250-.Ltmp249       #   Call between .Ltmp249 and .Ltmp250
	.long	.Ltmp251-.Lfunc_begin3  #     jumps to .Ltmp251
	.byte	0                       #   On action: cleanup
	.long	.Ltmp250-.Lfunc_begin3  # >> Call Site 45 <<
	.long	.Lfunc_end8-.Ltmp250    #   Call between .Ltmp250 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7zL21GetStringForSizeValueEj,@function
_ZN8NArchive3N7zL21GetStringForSizeValueEj: # @_ZN8NArchive3N7zL21GetStringForSizeValueEj
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 208
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r15
	cmpl	$32767, %ebx            # imm = 0x7FFF
	jg	.LBB9_21
# BB#1:
	cmpl	$127, %ebx
	jg	.LBB9_6
# BB#2:
	leal	-1(%rbx), %eax
	cmpl	$63, %eax
	ja	.LBB9_3
# BB#5:
	jmpq	*.LJTI9_0(,%rax,8)
.LBB9_70:                               # %.critedge.fold.split283
	xorl	%edi, %edi
	jmp	.LBB9_71
.LBB9_21:
	cmpl	$8388607, %ebx          # imm = 0x7FFFFF
	jg	.LBB9_37
# BB#22:
	cmpl	$524287, %ebx           # imm = 0x7FFFF
	jg	.LBB9_30
# BB#23:
	cmpl	$131071, %ebx           # imm = 0x1FFFF
	jg	.LBB9_27
# BB#24:
	cmpl	$32768, %ebx            # imm = 0x8000
	je	.LBB9_59
# BB#25:
	cmpl	$65536, %ebx            # imm = 0x10000
	jne	.LBB9_77
# BB#26:                                # %.critedge.fold.split267
	movl	$16, %edi
	jmp	.LBB9_71
.LBB9_6:
	cmpl	$2047, %ebx             # imm = 0x7FF
	jg	.LBB9_14
# BB#7:
	cmpl	$511, %ebx              # imm = 0x1FF
	jg	.LBB9_11
# BB#8:
	cmpl	$128, %ebx
	je	.LBB9_63
# BB#9:
	cmpl	$256, %ebx              # imm = 0x100
	jne	.LBB9_77
# BB#10:                                # %.critedge.fold.split275
	movl	$8, %edi
	jmp	.LBB9_71
.LBB9_37:
	cmpl	$134217727, %ebx        # imm = 0x7FFFFFF
	jg	.LBB9_45
# BB#38:
	cmpl	$33554431, %ebx         # imm = 0x1FFFFFF
	jg	.LBB9_42
# BB#39:
	cmpl	$8388608, %ebx          # imm = 0x800000
	je	.LBB9_55
# BB#40:
	cmpl	$16777216, %ebx         # imm = 0x1000000
	jne	.LBB9_77
# BB#41:                                # %.critedge.fold.split259
	movl	$24, %edi
	jmp	.LBB9_71
.LBB9_30:
	cmpl	$2097151, %ebx          # imm = 0x1FFFFF
	jg	.LBB9_34
# BB#31:
	cmpl	$524288, %ebx           # imm = 0x80000
	je	.LBB9_57
# BB#32:
	cmpl	$1048576, %ebx          # imm = 0x100000
	jne	.LBB9_77
# BB#33:                                # %.critedge.fold.split263
	movl	$20, %edi
	jmp	.LBB9_71
.LBB9_14:
	cmpl	$8191, %ebx             # imm = 0x1FFF
	jg	.LBB9_18
# BB#15:
	cmpl	$2048, %ebx             # imm = 0x800
	je	.LBB9_61
# BB#16:
	cmpl	$4096, %ebx             # imm = 0x1000
	jne	.LBB9_77
# BB#17:                                # %.critedge.fold.split271
	movl	$12, %edi
	jmp	.LBB9_71
.LBB9_45:
	cmpl	$536870911, %ebx        # imm = 0x1FFFFFFF
	jg	.LBB9_49
# BB#46:
	cmpl	$134217728, %ebx        # imm = 0x8000000
	je	.LBB9_53
# BB#47:
	cmpl	$268435456, %ebx        # imm = 0x10000000
	jne	.LBB9_77
# BB#48:                                # %.critedge.fold.split255
	movl	$28, %edi
	jmp	.LBB9_71
.LBB9_27:
	cmpl	$131072, %ebx           # imm = 0x20000
	je	.LBB9_58
# BB#28:
	cmpl	$262144, %ebx           # imm = 0x40000
	jne	.LBB9_77
# BB#29:                                # %.critedge.fold.split265
	movl	$18, %edi
	jmp	.LBB9_71
.LBB9_11:
	cmpl	$512, %ebx              # imm = 0x200
	je	.LBB9_62
# BB#12:
	cmpl	$1024, %ebx             # imm = 0x400
	jne	.LBB9_77
# BB#13:                                # %.critedge.fold.split273
	movl	$10, %edi
	jmp	.LBB9_71
.LBB9_42:
	cmpl	$33554432, %ebx         # imm = 0x2000000
	je	.LBB9_54
# BB#43:
	cmpl	$67108864, %ebx         # imm = 0x4000000
	jne	.LBB9_77
# BB#44:                                # %.critedge.fold.split257
	movl	$26, %edi
	jmp	.LBB9_71
.LBB9_34:
	cmpl	$2097152, %ebx          # imm = 0x200000
	je	.LBB9_56
# BB#35:
	cmpl	$4194304, %ebx          # imm = 0x400000
	jne	.LBB9_77
# BB#36:                                # %.critedge.fold.split261
	movl	$22, %edi
	jmp	.LBB9_71
.LBB9_18:
	cmpl	$8192, %ebx             # imm = 0x2000
	je	.LBB9_60
# BB#19:
	cmpl	$16384, %ebx            # imm = 0x4000
	jne	.LBB9_77
# BB#20:                                # %.critedge.fold.split269
	movl	$14, %edi
	jmp	.LBB9_71
.LBB9_49:
	cmpl	$536870912, %ebx        # imm = 0x20000000
	je	.LBB9_52
# BB#50:
	cmpl	$1073741824, %ebx       # imm = 0x40000000
	jne	.LBB9_77
# BB#51:                                # %.critedge.fold.split
	movl	$30, %edi
	jmp	.LBB9_71
.LBB9_3:
	cmpl	$-2147483648, %ebx      # imm = 0x80000000
	jne	.LBB9_77
# BB#4:
	movl	$31, %edi
	jmp	.LBB9_71
.LBB9_69:                               # %.critedge.fold.split282
	movl	$1, %edi
	jmp	.LBB9_71
.LBB9_68:                               # %.critedge.fold.split281
	movl	$2, %edi
	jmp	.LBB9_71
.LBB9_67:                               # %.critedge.fold.split280
	movl	$3, %edi
	jmp	.LBB9_71
.LBB9_66:                               # %.critedge.fold.split279
	movl	$4, %edi
	jmp	.LBB9_71
.LBB9_65:                               # %.critedge.fold.split278
	movl	$5, %edi
	jmp	.LBB9_71
.LBB9_64:                               # %.critedge.fold.split277
	movl	$6, %edi
	jmp	.LBB9_71
.LBB9_77:
	movabsq	$4294967296, %r12       # imm = 0x100000000
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r14
	movl	$0, (%r14)
	testl	$1048575, %ebx          # imm = 0xFFFFF
	je	.LBB9_78
# BB#108:
	testw	$1023, %bx              # imm = 0x3FF
	je	.LBB9_109
# BB#138:
	movl	%ebx, %edi
.Ltmp252:
	leaq	16(%rsp), %rsi
	callq	_Z21ConvertUInt64ToStringyPw
.Ltmp253:
# BB#139:                               # %.noexc104.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_140:                              # %.noexc104
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	leaq	1(%rbx), %rax
	cmpl	$0, 16(%rsp,%rbx,4)
	jne	.LBB9_140
# BB#141:                               # %_Z11MyStringLenIwEiPKT_.exit.i.i100
	movq	%rbx, %rax
	shlq	$32, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	addq	%rax, %r12
	sarq	$32, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp254:
	callq	_Znam
	movq	%rax, %r13
.Ltmp255:
# BB#142:                               # %.noexc105
	movl	$0, (%r13)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_143:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i103
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%rsp,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_143
# BB#144:
	cmpl	$4, %ebx
	jge	.LBB9_146
# BB#145:
	movl	$4, %ebp
	jmp	.LBB9_150
.LBB9_59:                               # %.critedge.fold.split268
	movl	$15, %edi
	jmp	.LBB9_71
.LBB9_63:                               # %.critedge.fold.split276
	movl	$7, %edi
	jmp	.LBB9_71
.LBB9_55:                               # %.critedge.fold.split260
	movl	$23, %edi
	jmp	.LBB9_71
.LBB9_57:                               # %.critedge.fold.split264
	movl	$19, %edi
	jmp	.LBB9_71
.LBB9_61:                               # %.critedge.fold.split272
	movl	$11, %edi
	jmp	.LBB9_71
.LBB9_53:                               # %.critedge.fold.split256
	movl	$27, %edi
	jmp	.LBB9_71
.LBB9_58:                               # %.critedge.fold.split266
	movl	$17, %edi
	jmp	.LBB9_71
.LBB9_62:                               # %.critedge.fold.split274
	movl	$9, %edi
	jmp	.LBB9_71
.LBB9_54:                               # %.critedge.fold.split258
	movl	$25, %edi
	jmp	.LBB9_71
.LBB9_56:                               # %.critedge.fold.split262
	movl	$21, %edi
	jmp	.LBB9_71
.LBB9_60:                               # %.critedge.fold.split270
	movl	$13, %edi
	jmp	.LBB9_71
.LBB9_52:                               # %.critedge.fold.split254
	movl	$29, %edi
.LBB9_71:                               # %.critedge
	leaq	16(%rsp), %rbx
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt64ToStringyPw
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB9_72:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB9_72
# BB#73:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i
	leaq	16(%rsp), %rbx
	leal	1(%rbp), %eax
	movslq	%eax, %r14
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r15)
	movl	$0, (%rax)
	movl	%r14d, 12(%r15)
	.p2align	4, 0x90
.LBB9_74:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_74
# BB#75:                                # %_ZN8NArchive3N7zL21ConvertUInt32ToStringEj.exit
	movl	%ebp, 8(%r15)
.LBB9_76:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_78:
	shrl	$20, %ebx
.Ltmp272:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_Z21ConvertUInt64ToStringyPw
.Ltmp273:
# BB#79:                                # %.noexc.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_80:                               # %.noexc
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	leaq	1(%rbx), %rax
	cmpl	$0, 16(%rsp,%rbx,4)
	jne	.LBB9_80
# BB#81:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i20
	movq	%rbx, %rax
	shlq	$32, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	addq	%rax, %r12
	sarq	$32, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp274:
	callq	_Znam
	movq	%rax, %r13
.Ltmp275:
# BB#82:                                # %.noexc24
	movl	$0, (%r13)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_83:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i23
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%rsp,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_83
# BB#84:
	cmpl	$4, %ebx
	jge	.LBB9_86
# BB#85:
	movl	$4, %ebp
	jmp	.LBB9_90
.LBB9_109:
	shrl	$10, %ebx
.Ltmp262:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_Z21ConvertUInt64ToStringyPw
.Ltmp263:
# BB#110:                               # %.noexc54.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_111:                              # %.noexc54
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	leaq	1(%rbx), %rax
	cmpl	$0, 16(%rsp,%rbx,4)
	jne	.LBB9_111
# BB#112:                               # %_Z11MyStringLenIwEiPKT_.exit.i.i50
	movq	%rbx, %rax
	shlq	$32, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	addq	%rax, %r12
	sarq	$32, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp264:
	callq	_Znam
	movq	%rax, %r13
.Ltmp265:
# BB#113:                               # %.noexc55
	movl	$0, (%r13)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_114:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i53
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%rsp,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_114
# BB#115:
	cmpl	$4, %ebx
	jge	.LBB9_117
# BB#116:
	movl	$4, %ebp
	jmp	.LBB9_121
.LBB9_146:
	leal	2(%rbx), %eax
	cmpl	$7, %ebx
	movl	$9, %ebp
	cmovgl	%eax, %ebp
	cmpl	$4, %ebp
	jne	.LBB9_148
# BB#147:
	movl	$4, %ebp
	jmp	.LBB9_150
.LBB9_86:
	leal	2(%rbx), %eax
	cmpl	$7, %ebx
	movl	$9, %ebp
	cmovgl	%eax, %ebp
	cmpl	$4, %ebp
	jne	.LBB9_88
# BB#87:
	movl	$4, %ebp
	jmp	.LBB9_90
.LBB9_117:
	leal	2(%rbx), %eax
	cmpl	$7, %ebx
	movl	$9, %ebp
	cmovgl	%eax, %ebp
	cmpl	$4, %ebp
	jne	.LBB9_119
# BB#118:
	movl	$4, %ebp
	jmp	.LBB9_121
.LBB9_148:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp257:
	callq	_Znam
	movq	%rax, %r12
.Ltmp258:
# BB#149:                               # %._crit_edge16.i.i122
	movq	%r14, %rdi
	callq	_ZdaPv
	movl	$0, (%r12)
	movq	%r12, %r14
.LBB9_150:                              # %.noexc109
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_151:                              # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%r14,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_151
# BB#152:                               # %_ZN11CStringBaseIwED2Ev.exit125
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	%ebp, %eax
	subl	%ebx, %eax
	cmpl	$1, %eax
	jle	.LBB9_154
# BB#153:
	movq	%r14, %r12
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB9_164
.LBB9_154:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebp
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	movq	8(%rsp), %r13           # 8-byte Reload
	jl	.LBB9_156
# BB#155:                               # %select.true.sink338
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB9_156:                              # %select.end337
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rbp,%rsi), %eax
	cmpl	%ebp, %eax
	jne	.LBB9_158
# BB#157:
	movq	%r14, %r12
	jmp	.LBB9_164
.LBB9_158:
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp260:
	callq	_Znam
	movq	%rax, %r12
.Ltmp261:
# BB#159:                               # %.noexc145
	testl	%ebp, %ebp
	jle	.LBB9_163
# BB#160:                               # %.preheader.i.i136
	testl	%ebx, %ebx
	jle	.LBB9_162
# BB#161:                               # %.lr.ph.i.i137
	movq	%r13, %rdx
	sarq	$30, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	memcpy
.LBB9_162:                              # %._crit_edge.thread.i.i143
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB9_163:                              # %._crit_edge16.i.i144
	movq	%r13, %rax
	sarq	$30, %rax
	movl	$0, (%r12,%rax)
	movq	%r12, %r14
.LBB9_164:                              # %.noexc131
	movq	%r14, %rbp
	sarq	$30, %r13
	leaq	4(%rbp,%r13), %rax
	movl	$98, (%rbp,%r13)
	jmp	.LBB9_165
.LBB9_88:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp277:
	callq	_Znam
	movq	%rax, %r12
.Ltmp278:
# BB#89:                                # %._crit_edge16.i.i
	movq	%r14, %rdi
	callq	_ZdaPv
	movl	$0, (%r12)
	movq	%r12, %r14
.LBB9_90:                               # %.noexc26
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_91:                               # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%r14,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_91
# BB#92:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	%ebp, %eax
	subl	%ebx, %eax
	cmpl	$1, %eax
	jle	.LBB9_94
# BB#93:
	movq	%r14, %r12
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB9_104
.LBB9_94:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebp
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	movq	8(%rsp), %r13           # 8-byte Reload
	jl	.LBB9_96
# BB#95:                                # %select.true.sink
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB9_96:                               # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rbp,%rsi), %eax
	cmpl	%ebp, %eax
	jne	.LBB9_98
# BB#97:
	movq	%r14, %r12
	jmp	.LBB9_104
.LBB9_98:
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp280:
	callq	_Znam
	movq	%rax, %r12
.Ltmp281:
# BB#99:                                # %.noexc45
	testl	%ebp, %ebp
	jle	.LBB9_103
# BB#100:                               # %.preheader.i.i36
	testl	%ebx, %ebx
	jle	.LBB9_102
# BB#101:                               # %.lr.ph.i.i37
	movq	%r13, %rdx
	sarq	$30, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	memcpy
.LBB9_102:                              # %._crit_edge.thread.i.i43
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB9_103:                              # %._crit_edge16.i.i44
	movq	%r13, %rax
	sarq	$30, %rax
	movl	$0, (%r12,%rax)
	movq	%r12, %r14
.LBB9_104:                              # %.noexc32
	movq	%r14, %rbp
	sarq	$30, %r13
	leaq	4(%rbp,%r13), %rax
	movl	$109, (%rbp,%r13)
	jmp	.LBB9_165
.LBB9_119:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp267:
	callq	_Znam
	movq	%rax, %r12
.Ltmp268:
# BB#120:                               # %._crit_edge16.i.i72
	movq	%r14, %rdi
	callq	_ZdaPv
	movl	$0, (%r12)
	movq	%r12, %r14
.LBB9_121:                              # %.noexc59
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_122:                              # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%r14,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_122
# BB#123:                               # %_ZN11CStringBaseIwED2Ev.exit75
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	%ebp, %eax
	subl	%ebx, %eax
	cmpl	$1, %eax
	jle	.LBB9_125
# BB#124:
	movq	%r14, %r12
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB9_135
.LBB9_125:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebp
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	movq	8(%rsp), %r13           # 8-byte Reload
	jl	.LBB9_127
# BB#126:                               # %select.true.sink333
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB9_127:                              # %select.end332
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rbp,%rsi), %eax
	cmpl	%ebp, %eax
	jne	.LBB9_129
# BB#128:
	movq	%r14, %r12
	jmp	.LBB9_135
.LBB9_129:
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp270:
	callq	_Znam
	movq	%rax, %r12
.Ltmp271:
# BB#130:                               # %.noexc95
	testl	%ebp, %ebp
	jle	.LBB9_134
# BB#131:                               # %.preheader.i.i86
	testl	%ebx, %ebx
	jle	.LBB9_133
# BB#132:                               # %.lr.ph.i.i87
	movq	%r13, %rdx
	sarq	$30, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	memcpy
.LBB9_133:                              # %._crit_edge.thread.i.i93
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB9_134:                              # %._crit_edge16.i.i94
	movq	%r13, %rax
	sarq	$30, %rax
	movl	$0, (%r12,%rax)
	movq	%r12, %r14
.LBB9_135:                              # %.noexc81
	movq	%r14, %rbp
	sarq	$30, %r13
	leaq	4(%rbp,%r13), %rax
	movl	$107, (%rbp,%r13)
.LBB9_165:                              # %._crit_edge16.i.i148
	movq	%r12, %r14
	movl	$0, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	leal	2(%rbx), %r12d
	movslq	%r12d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp282:
	callq	_Znam
.Ltmp283:
# BB#166:                               # %.noexc151
	incl	%ebx
	movq	%rax, (%r15)
	movl	$0, (%rax)
	movl	%r12d, 12(%r15)
	.p2align	4, 0x90
.LBB9_167:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_167
# BB#168:                               # %_ZN11CStringBaseIwED2Ev.exit152
	movl	%ebx, 8(%r15)
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB9_76
.LBB9_137:                              # %_ZN11CStringBaseIwED2Ev.exit97
.Ltmp269:
	jmp	.LBB9_107
.LBB9_106:                              # %_ZN11CStringBaseIwED2Ev.exit47
.Ltmp279:
	jmp	.LBB9_107
.LBB9_170:                              # %_ZN11CStringBaseIwED2Ev.exit147
.Ltmp259:
.LBB9_107:                              # %_ZN11CStringBaseIwED2Ev.exit153
	movq	%rax, %r15
	movq	%r13, %rdi
	callq	_ZdaPv
	jmp	.LBB9_173
.LBB9_136:
.Ltmp266:
	jmp	.LBB9_172
.LBB9_105:
.Ltmp276:
	jmp	.LBB9_172
.LBB9_169:
.Ltmp256:
	jmp	.LBB9_172
.LBB9_171:                              # %.thread231
.Ltmp284:
.LBB9_172:                              # %_ZN11CStringBaseIwED2Ev.exit153
	movq	%rax, %r15
.LBB9_173:                              # %_ZN11CStringBaseIwED2Ev.exit153
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZN8NArchive3N7zL21GetStringForSizeValueEj, .Lfunc_end9-_ZN8NArchive3N7zL21GetStringForSizeValueEj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI9_0:
	.quad	.LBB9_70
	.quad	.LBB9_69
	.quad	.LBB9_77
	.quad	.LBB9_68
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_67
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_66
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_65
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_77
	.quad	.LBB9_64
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\323\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp252-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp252
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp252-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp255-.Ltmp252       #   Call between .Ltmp252 and .Ltmp255
	.long	.Ltmp256-.Lfunc_begin4  #     jumps to .Ltmp256
	.byte	0                       #   On action: cleanup
	.long	.Ltmp255-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp272-.Ltmp255       #   Call between .Ltmp255 and .Ltmp272
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp272-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp275-.Ltmp272       #   Call between .Ltmp272 and .Ltmp275
	.long	.Ltmp276-.Lfunc_begin4  #     jumps to .Ltmp276
	.byte	0                       #   On action: cleanup
	.long	.Ltmp262-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp265-.Ltmp262       #   Call between .Ltmp262 and .Ltmp265
	.long	.Ltmp266-.Lfunc_begin4  #     jumps to .Ltmp266
	.byte	0                       #   On action: cleanup
	.long	.Ltmp257-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp259-.Lfunc_begin4  #     jumps to .Ltmp259
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp284-.Lfunc_begin4  #     jumps to .Ltmp284
	.byte	0                       #   On action: cleanup
	.long	.Ltmp261-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp277-.Ltmp261       #   Call between .Ltmp261 and .Ltmp277
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp277-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp278-.Ltmp277       #   Call between .Ltmp277 and .Ltmp278
	.long	.Ltmp279-.Lfunc_begin4  #     jumps to .Ltmp279
	.byte	0                       #   On action: cleanup
	.long	.Ltmp280-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp281-.Ltmp280       #   Call between .Ltmp280 and .Ltmp281
	.long	.Ltmp284-.Lfunc_begin4  #     jumps to .Ltmp284
	.byte	0                       #   On action: cleanup
	.long	.Ltmp281-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp267-.Ltmp281       #   Call between .Ltmp281 and .Ltmp267
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp267-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Ltmp268-.Ltmp267       #   Call between .Ltmp267 and .Ltmp268
	.long	.Ltmp269-.Lfunc_begin4  #     jumps to .Ltmp269
	.byte	0                       #   On action: cleanup
	.long	.Ltmp270-.Lfunc_begin4  # >> Call Site 13 <<
	.long	.Ltmp271-.Ltmp270       #   Call between .Ltmp270 and .Ltmp271
	.long	.Ltmp284-.Lfunc_begin4  #     jumps to .Ltmp284
	.byte	0                       #   On action: cleanup
	.long	.Ltmp271-.Lfunc_begin4  # >> Call Site 14 <<
	.long	.Ltmp282-.Ltmp271       #   Call between .Ltmp271 and .Ltmp282
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp282-.Lfunc_begin4  # >> Call Site 15 <<
	.long	.Ltmp283-.Ltmp282       #   Call between .Ltmp282 and .Ltmp283
	.long	.Ltmp284-.Lfunc_begin4  #     jumps to .Ltmp284
	.byte	0                       #   On action: cleanup
	.long	.Ltmp283-.Lfunc_begin4  # >> Call Site 16 <<
	.long	.Lfunc_end9-.Ltmp283    #   Call between .Ltmp283 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive3N7z8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive3N7z8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 160
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	(%r14), %rax
.Ltmp285:
	callq	*48(%rax)
.Ltmp286:
# BB#1:
	leaq	880(%r14), %rdi
.Ltmp287:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp288:
# BB#2:
	testq	%r12, %r12
	je	.LBB10_3
# BB#4:
	movq	(%r12), %rax
.Ltmp289:
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp290:
# BB#5:
	movq	%rsp, %rdx
	movq	$0, (%rsp)
	movq	(%r12), %rax
.Ltmp292:
	movl	$IID_ICryptoGetTextPassword, %esi
	movq	%r12, %rdi
	callq	*(%rax)
.Ltmp293:
	jmp	.LBB10_6
.LBB10_3:                               # %_ZN9CMyComPtrI20IArchiveOpenCallbackEC2EPS0_.exit.thread
	movq	$0, (%rsp)
.LBB10_6:
	movq	$0, 8(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rsp)
	movq	$8, 40(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE+16, 16(%rsp)
.Ltmp295:
	leaq	8(%rsp), %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	_ZN8NArchive3N7z10CInArchive4OpenEP9IInStreamPKy
	movl	%eax, %ebp
.Ltmp296:
# BB#7:
	movl	$1, %r13d
	testl	%ebp, %ebp
	jne	.LBB10_18
# BB#8:
	leaq	840(%r14), %rcx
	movb	$0, 840(%r14)
	leaq	144(%r14), %rbx
	movq	(%rsp), %rdx
.Ltmp298:
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive3N7z10CInArchive12ReadDatabaseERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
	movl	%eax, %ebp
.Ltmp299:
# BB#9:
	testl	%ebp, %ebp
	jne	.LBB10_18
# BB#10:
.Ltmp301:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z18CArchiveDatabaseEx25FillFolderStartPackStreamEv
.Ltmp302:
# BB#11:                                # %.noexc45
.Ltmp303:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z18CArchiveDatabaseEx12FillStartPosEv
.Ltmp304:
# BB#12:                                # %.noexc46
.Ltmp305:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z18CArchiveDatabaseEx24FillFolderStartFileIndexEv
.Ltmp306:
# BB#13:                                # %_ZN8NArchive3N7z18CArchiveDatabaseEx4FillEv.exit
	testq	%r15, %r15
	je	.LBB10_15
# BB#14:
	movq	(%r15), %rax
.Ltmp307:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp308:
.LBB10_15:                              # %.noexc48
	movq	136(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB10_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp309:
	callq	*16(%rax)
.Ltmp310:
.LBB10_17:                              # %_ZN9CMyComPtrI9IInStreamEaSEPS0_.exit
	movq	%r15, 136(%r14)
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
.LBB10_18:                              # %_ZN11CStringBaseIwED2Ev.exit
	leaq	16(%rsp), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE+16, 16(%rsp)
.Ltmp326:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp327:
# BB#19:
.Ltmp332:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp333:
# BB#20:                                # %_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev.exit.i
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_22
# BB#21:
	movq	(%rdi), %rax
.Ltmp338:
	callq	*16(%rax)
.Ltmp339:
.LBB10_22:                              # %_ZN8NArchive3N7z10CInArchiveD2Ev.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_24
# BB#23:
	movq	(%rdi), %rax
.Ltmp343:
	callq	*16(%rax)
.Ltmp344:
.LBB10_24:                              # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit52
	testq	%r12, %r12
	je	.LBB10_26
# BB#25:
	movq	(%r12), %rax
.Ltmp348:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp349:
.LBB10_26:                              # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit54
	testl	%r13d, %r13d
	jne	.LBB10_66
# BB#27:
	xorl	%ebp, %ebp
.Ltmp359:
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z8CHandler10FillPopIDsEv
.Ltmp360:
.LBB10_66:
	movl	%ebp, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_30:
.Ltmp300:
	jmp	.LBB10_40
.LBB10_37:
.Ltmp311:
	jmp	.LBB10_40
.LBB10_57:
.Ltmp350:
	jmp	.LBB10_58
.LBB10_50:
.Ltmp345:
	movq	%rax, %r15
	testq	%r12, %r12
	jne	.LBB10_56
	jmp	.LBB10_59
.LBB10_38:
.Ltmp340:
	jmp	.LBB10_52
.LBB10_51:
.Ltmp294:
.LBB10_52:
	movq	%rax, %r15
	jmp	.LBB10_53
.LBB10_29:
.Ltmp291:
.LBB10_58:                              # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit
	movq	%rax, %r15
	jmp	.LBB10_59
.LBB10_33:
.Ltmp334:
	movq	%rax, %r15
	jmp	.LBB10_34
.LBB10_31:
.Ltmp328:
	movq	%rax, %r15
.Ltmp329:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp330:
.LBB10_34:                              # %.body.i
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_53
# BB#35:
	movq	(%rdi), %rax
.Ltmp335:
	callq	*16(%rax)
.Ltmp336:
	jmp	.LBB10_53
.LBB10_32:
.Ltmp331:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_36:
.Ltmp337:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_39:
.Ltmp297:
.LBB10_40:                              # %_ZN11CStringBaseIwED2Ev.exit55
	movq	%rax, %r15
	leaq	16(%rsp), %rbp
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE+16, 16(%rsp)
.Ltmp312:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp313:
# BB#41:
.Ltmp318:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp319:
# BB#42:                                # %_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev.exit.i56
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_53
# BB#43:
	movq	(%rdi), %rax
.Ltmp324:
	callq	*16(%rax)
.Ltmp325:
.LBB10_53:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_55
# BB#54:
	movq	(%rdi), %rax
.Ltmp341:
	callq	*16(%rax)
.Ltmp342:
.LBB10_55:                              # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit
	testq	%r12, %r12
	je	.LBB10_59
.LBB10_56:
	movq	(%r12), %rax
.Ltmp346:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp347:
.LBB10_59:                              # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit
	movq	%r15, %rdi
	callq	__cxa_begin_catch
	movq	(%r14), %rax
.Ltmp351:
	movq	%r14, %rdi
	callq	*48(%rax)
.Ltmp352:
# BB#60:
	movl	$1, %ebp
.Ltmp357:
	callq	__cxa_end_catch
.Ltmp358:
	jmp	.LBB10_66
.LBB10_46:
.Ltmp320:
	movq	%rax, %rbx
	jmp	.LBB10_47
.LBB10_44:
.Ltmp314:
	movq	%rax, %rbx
.Ltmp315:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp316:
.LBB10_47:                              # %.body.i58
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_68
# BB#48:
	movq	(%rdi), %rax
.Ltmp321:
	callq	*16(%rax)
.Ltmp322:
	jmp	.LBB10_68
.LBB10_49:
.Ltmp323:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_45:
.Ltmp317:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_61:
.Ltmp353:
	movq	%rdx, %rbx
	movq	%rax, %rbp
.Ltmp354:
	callq	__cxa_end_catch
.Ltmp355:
	jmp	.LBB10_62
.LBB10_67:
.Ltmp356:
	movq	%rax, %rbx
.LBB10_68:                              # %.body61
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB10_28:
.Ltmp361:
	movq	%rdx, %rbx
	movq	%rax, %rbp
.LBB10_62:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$2, %ebx
	je	.LBB10_63
# BB#65:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB10_66
.LBB10_63:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp362:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp363:
# BB#69:
.LBB10_64:
.Ltmp364:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN8NArchive3N7z8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end10-_ZN8NArchive3N7z8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\341\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\322\002"              # Call site table length
	.long	.Ltmp285-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp288-.Ltmp285       #   Call between .Ltmp285 and .Ltmp288
	.long	.Ltmp361-.Lfunc_begin5  #     jumps to .Ltmp361
	.byte	3                       #   On action: 2
	.long	.Ltmp289-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp290-.Ltmp289       #   Call between .Ltmp289 and .Ltmp290
	.long	.Ltmp291-.Lfunc_begin5  #     jumps to .Ltmp291
	.byte	1                       #   On action: 1
	.long	.Ltmp292-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp293-.Ltmp292       #   Call between .Ltmp292 and .Ltmp293
	.long	.Ltmp294-.Lfunc_begin5  #     jumps to .Ltmp294
	.byte	1                       #   On action: 1
	.long	.Ltmp295-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp296-.Ltmp295       #   Call between .Ltmp295 and .Ltmp296
	.long	.Ltmp297-.Lfunc_begin5  #     jumps to .Ltmp297
	.byte	1                       #   On action: 1
	.long	.Ltmp298-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp299-.Ltmp298       #   Call between .Ltmp298 and .Ltmp299
	.long	.Ltmp300-.Lfunc_begin5  #     jumps to .Ltmp300
	.byte	1                       #   On action: 1
	.long	.Ltmp301-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp310-.Ltmp301       #   Call between .Ltmp301 and .Ltmp310
	.long	.Ltmp311-.Lfunc_begin5  #     jumps to .Ltmp311
	.byte	1                       #   On action: 1
	.long	.Ltmp326-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp327-.Ltmp326       #   Call between .Ltmp326 and .Ltmp327
	.long	.Ltmp328-.Lfunc_begin5  #     jumps to .Ltmp328
	.byte	1                       #   On action: 1
	.long	.Ltmp332-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp333-.Ltmp332       #   Call between .Ltmp332 and .Ltmp333
	.long	.Ltmp334-.Lfunc_begin5  #     jumps to .Ltmp334
	.byte	1                       #   On action: 1
	.long	.Ltmp338-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin5  #     jumps to .Ltmp340
	.byte	1                       #   On action: 1
	.long	.Ltmp343-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp344-.Ltmp343       #   Call between .Ltmp343 and .Ltmp344
	.long	.Ltmp345-.Lfunc_begin5  #     jumps to .Ltmp345
	.byte	1                       #   On action: 1
	.long	.Ltmp348-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp349-.Ltmp348       #   Call between .Ltmp348 and .Ltmp349
	.long	.Ltmp350-.Lfunc_begin5  #     jumps to .Ltmp350
	.byte	1                       #   On action: 1
	.long	.Ltmp359-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin5  #     jumps to .Ltmp361
	.byte	3                       #   On action: 2
	.long	.Ltmp329-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp330-.Ltmp329       #   Call between .Ltmp329 and .Ltmp330
	.long	.Ltmp331-.Lfunc_begin5  #     jumps to .Ltmp331
	.byte	1                       #   On action: 1
	.long	.Ltmp335-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp336-.Ltmp335       #   Call between .Ltmp335 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin5  #     jumps to .Ltmp337
	.byte	1                       #   On action: 1
	.long	.Ltmp312-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp313-.Ltmp312       #   Call between .Ltmp312 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin5  #     jumps to .Ltmp314
	.byte	1                       #   On action: 1
	.long	.Ltmp318-.Lfunc_begin5  # >> Call Site 16 <<
	.long	.Ltmp319-.Ltmp318       #   Call between .Ltmp318 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin5  #     jumps to .Ltmp320
	.byte	1                       #   On action: 1
	.long	.Ltmp324-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Ltmp347-.Ltmp324       #   Call between .Ltmp324 and .Ltmp347
	.long	.Ltmp356-.Lfunc_begin5  #     jumps to .Ltmp356
	.byte	1                       #   On action: 1
	.long	.Ltmp347-.Lfunc_begin5  # >> Call Site 18 <<
	.long	.Ltmp351-.Ltmp347       #   Call between .Ltmp347 and .Ltmp351
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp351-.Lfunc_begin5  # >> Call Site 19 <<
	.long	.Ltmp352-.Ltmp351       #   Call between .Ltmp351 and .Ltmp352
	.long	.Ltmp353-.Lfunc_begin5  #     jumps to .Ltmp353
	.byte	3                       #   On action: 2
	.long	.Ltmp357-.Lfunc_begin5  # >> Call Site 20 <<
	.long	.Ltmp358-.Ltmp357       #   Call between .Ltmp357 and .Ltmp358
	.long	.Ltmp361-.Lfunc_begin5  #     jumps to .Ltmp361
	.byte	3                       #   On action: 2
	.long	.Ltmp315-.Lfunc_begin5  # >> Call Site 21 <<
	.long	.Ltmp316-.Ltmp315       #   Call between .Ltmp315 and .Ltmp316
	.long	.Ltmp317-.Lfunc_begin5  #     jumps to .Ltmp317
	.byte	1                       #   On action: 1
	.long	.Ltmp321-.Lfunc_begin5  # >> Call Site 22 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin5  #     jumps to .Ltmp323
	.byte	1                       #   On action: 1
	.long	.Ltmp354-.Lfunc_begin5  # >> Call Site 23 <<
	.long	.Ltmp355-.Ltmp354       #   Call between .Ltmp354 and .Ltmp355
	.long	.Ltmp356-.Lfunc_begin5  #     jumps to .Ltmp356
	.byte	1                       #   On action: 1
	.long	.Ltmp355-.Lfunc_begin5  # >> Call Site 24 <<
	.long	.Ltmp362-.Ltmp355       #   Call between .Ltmp355 and .Ltmp362
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp362-.Lfunc_begin5  # >> Call Site 25 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin5  #     jumps to .Ltmp364
	.byte	0                       #   On action: cleanup
	.long	.Ltmp363-.Lfunc_begin5  # >> Call Site 26 <<
	.long	.Lfunc_end10-.Ltmp363   #   Call between .Ltmp363 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z8CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler5CloseEv,@function
_ZN8NArchive3N7z8CHandler5CloseEv:      # @_ZN8NArchive3N7z8CHandler5CloseEv
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi72:
	.cfi_def_cfa_offset 32
.Lcfi73:
	.cfi_offset %rbx, -24
.Lcfi74:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_3
# BB#1:
	movq	(%rdi), %rax
.Ltmp365:
	callq	*16(%rax)
.Ltmp366:
# BB#2:                                 # %.noexc
	movq	$0, 136(%rbx)
.LBB11_3:                               # %_ZN9CMyComPtrI9IInStreamE7ReleaseEv.exit
	leaq	144(%rbx), %rdi
.Ltmp367:
	callq	_ZN8NArchive3N7z16CArchiveDatabase5ClearEv
.Ltmp368:
# BB#4:                                 # %.noexc8
	leaq	664(%rbx), %rdi
.Ltmp369:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp370:
# BB#5:                                 # %.noexc9
	leaq	696(%rbx), %rdi
.Ltmp371:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp372:
# BB#6:                                 # %.noexc10
	leaq	728(%rbx), %rdi
.Ltmp373:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp374:
# BB#7:                                 # %.noexc11
	leaq	760(%rbx), %rdi
.Ltmp375:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp376:
# BB#8:                                 # %.noexc12
	leaq	792(%rbx), %rdi
.Ltmp377:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp378:
# BB#9:                                 # %_ZN8NArchive3N7z18CArchiveDatabaseEx5ClearEv.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, 824(%rbx)
	xorl	%eax, %eax
.LBB11_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB11_10:
.Ltmp379:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB11_11
# BB#13:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB11_14
.LBB11_11:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp380:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp381:
# BB#15:
.LBB11_12:
.Ltmp382:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN8NArchive3N7z8CHandler5CloseEv, .Lfunc_end11-_ZN8NArchive3N7z8CHandler5CloseEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp365-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp378-.Ltmp365       #   Call between .Ltmp365 and .Ltmp378
	.long	.Ltmp379-.Lfunc_begin6  #     jumps to .Ltmp379
	.byte	3                       #   On action: 2
	.long	.Ltmp378-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp380-.Ltmp378       #   Call between .Ltmp378 and .Ltmp380
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp380-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp381-.Ltmp380       #   Call between .Ltmp380 and .Ltmp381
	.long	.Ltmp382-.Lfunc_begin6  #     jumps to .Ltmp382
	.byte	0                       #   On action: cleanup
	.long	.Ltmp381-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Lfunc_end11-.Ltmp381   #   Call between .Ltmp381 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB12_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB12_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB12_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB12_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB12_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB12_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB12_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB12_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB12_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB12_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB12_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB12_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB12_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB12_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB12_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB12_16
.LBB12_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB12_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInArchive+1(%rip), %al
	jne	.LBB12_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInArchive+2(%rip), %al
	jne	.LBB12_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInArchive+3(%rip), %al
	jne	.LBB12_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInArchive+4(%rip), %al
	jne	.LBB12_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInArchive+5(%rip), %al
	jne	.LBB12_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInArchive+6(%rip), %al
	jne	.LBB12_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInArchive+7(%rip), %al
	jne	.LBB12_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInArchive+8(%rip), %al
	jne	.LBB12_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInArchive+9(%rip), %al
	jne	.LBB12_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInArchive+10(%rip), %al
	jne	.LBB12_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInArchive+11(%rip), %al
	jne	.LBB12_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInArchive+12(%rip), %al
	jne	.LBB12_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInArchive+13(%rip), %al
	jne	.LBB12_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInArchive+14(%rip), %al
	jne	.LBB12_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %al
	cmpb	IID_IInArchive+15(%rip), %al
	jne	.LBB12_33
.LBB12_16:
	movq	%rdi, (%rdx)
	jmp	.LBB12_68
.LBB12_33:                              # %_ZeqRK4GUIDS1_.exit10.thread
	cmpb	IID_ISetProperties(%rip), %cl
	jne	.LBB12_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ISetProperties+1(%rip), %al
	jne	.LBB12_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ISetProperties+2(%rip), %al
	jne	.LBB12_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ISetProperties+3(%rip), %al
	jne	.LBB12_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ISetProperties+4(%rip), %al
	jne	.LBB12_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ISetProperties+5(%rip), %al
	jne	.LBB12_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ISetProperties+6(%rip), %al
	jne	.LBB12_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ISetProperties+7(%rip), %al
	jne	.LBB12_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ISetProperties+8(%rip), %al
	jne	.LBB12_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ISetProperties+9(%rip), %al
	jne	.LBB12_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ISetProperties+10(%rip), %al
	jne	.LBB12_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ISetProperties+11(%rip), %al
	jne	.LBB12_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ISetProperties+12(%rip), %al
	jne	.LBB12_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ISetProperties+13(%rip), %al
	jne	.LBB12_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ISetProperties+14(%rip), %al
	jne	.LBB12_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %al
	cmpb	IID_ISetProperties+15(%rip), %al
	jne	.LBB12_50
# BB#49:
	leaq	112(%rdi), %rax
	jmp	.LBB12_67
.LBB12_50:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IOutArchive(%rip), %cl
	jne	.LBB12_69
# BB#51:
	movb	1(%rsi), %cl
	cmpb	IID_IOutArchive+1(%rip), %cl
	jne	.LBB12_69
# BB#52:
	movb	2(%rsi), %cl
	cmpb	IID_IOutArchive+2(%rip), %cl
	jne	.LBB12_69
# BB#53:
	movb	3(%rsi), %cl
	cmpb	IID_IOutArchive+3(%rip), %cl
	jne	.LBB12_69
# BB#54:
	movb	4(%rsi), %cl
	cmpb	IID_IOutArchive+4(%rip), %cl
	jne	.LBB12_69
# BB#55:
	movb	5(%rsi), %cl
	cmpb	IID_IOutArchive+5(%rip), %cl
	jne	.LBB12_69
# BB#56:
	movb	6(%rsi), %cl
	cmpb	IID_IOutArchive+6(%rip), %cl
	jne	.LBB12_69
# BB#57:
	movb	7(%rsi), %cl
	cmpb	IID_IOutArchive+7(%rip), %cl
	jne	.LBB12_69
# BB#58:
	movb	8(%rsi), %cl
	cmpb	IID_IOutArchive+8(%rip), %cl
	jne	.LBB12_69
# BB#59:
	movb	9(%rsi), %cl
	cmpb	IID_IOutArchive+9(%rip), %cl
	jne	.LBB12_69
# BB#60:
	movb	10(%rsi), %cl
	cmpb	IID_IOutArchive+10(%rip), %cl
	jne	.LBB12_69
# BB#61:
	movb	11(%rsi), %cl
	cmpb	IID_IOutArchive+11(%rip), %cl
	jne	.LBB12_69
# BB#62:
	movb	12(%rsi), %cl
	cmpb	IID_IOutArchive+12(%rip), %cl
	jne	.LBB12_69
# BB#63:
	movb	13(%rsi), %cl
	cmpb	IID_IOutArchive+13(%rip), %cl
	jne	.LBB12_69
# BB#64:
	movb	14(%rsi), %cl
	cmpb	IID_IOutArchive+14(%rip), %cl
	jne	.LBB12_69
# BB#65:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %cl
	cmpb	IID_IOutArchive+15(%rip), %cl
	jne	.LBB12_69
# BB#66:
	leaq	120(%rdi), %rax
.LBB12_67:                              # %_ZeqRK4GUIDS1_.exit12.thread
	movq	%rax, (%rdx)
.LBB12_68:                              # %_ZeqRK4GUIDS1_.exit12.thread
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB12_69:                              # %_ZeqRK4GUIDS1_.exit12.thread
	popq	%rcx
	retq
.Lfunc_end12:
	.size	_ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end12-_ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z8CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive3N7z8CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive3N7z8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler6AddRefEv,@function
_ZN8NArchive3N7z8CHandler6AddRefEv:     # @_ZN8NArchive3N7z8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	128(%rdi), %eax
	incl	%eax
	movl	%eax, 128(%rdi)
	retq
.Lfunc_end13:
	.size	_ZN8NArchive3N7z8CHandler6AddRefEv, .Lfunc_end13-_ZN8NArchive3N7z8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z8CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive3N7z8CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive3N7z8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler7ReleaseEv,@function
_ZN8NArchive3N7z8CHandler7ReleaseEv:    # @_ZN8NArchive3N7z8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 16
	movl	128(%rdi), %eax
	decl	%eax
	movl	%eax, 128(%rdi)
	jne	.LBB14_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB14_2:
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZN8NArchive3N7z8CHandler7ReleaseEv, .Lfunc_end14-_ZN8NArchive3N7z8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z8CHandlerD2Ev,"axG",@progbits,_ZN8NArchive3N7z8CHandlerD2Ev,comdat
	.weak	_ZN8NArchive3N7z8CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandlerD2Ev,@function
_ZN8NArchive3N7z8CHandlerD2Ev:          # @_ZN8NArchive3N7z8CHandlerD2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -24
.Lcfi81:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive3N7z8CHandlerE+16, (%rbx)
	movl	$_ZTVN8NArchive3N7z8CHandlerE+240, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3N7z8CHandlerE+176, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 112(%rbx)
	leaq	880(%rbx), %rdi
.Ltmp383:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp384:
# BB#1:
	leaq	848(%rbx), %rdi
.Ltmp388:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp389:
# BB#2:
	leaq	144(%rbx), %rdi
.Ltmp393:
	callq	_ZN8NArchive3N7z18CArchiveDatabaseExD2Ev
.Ltmp394:
# BB#3:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp398:
	callq	*16(%rax)
.Ltmp399:
.LBB15_5:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, 16(%rbx)
	addq	$16, %rbx
.Ltmp410:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp411:
# BB#6:                                 # %_ZN8NArchive11COutHandlerD2Ev.exit10
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB15_13:
.Ltmp400:
	movq	%rax, %r14
	jmp	.LBB15_17
.LBB15_7:
.Ltmp412:
	movq	%rax, %r14
.Ltmp413:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp414:
	jmp	.LBB15_8
.LBB15_9:
.Ltmp415:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_14:
.Ltmp395:
	movq	%rax, %r14
	jmp	.LBB15_15
.LBB15_11:
.Ltmp390:
	movq	%rax, %r14
	jmp	.LBB15_12
.LBB15_10:
.Ltmp385:
	movq	%rax, %r14
	leaq	848(%rbx), %rdi
.Ltmp386:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp387:
.LBB15_12:
	leaq	144(%rbx), %rdi
.Ltmp391:
	callq	_ZN8NArchive3N7z18CArchiveDatabaseExD2Ev
.Ltmp392:
.LBB15_15:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp396:
	callq	*16(%rax)
.Ltmp397:
.LBB15_17:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit12
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, 16(%rbx)
	addq	$16, %rbx
.Ltmp401:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp402:
# BB#18:                                # %_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev.exit.i
.Ltmp407:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp408:
.LBB15_8:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_19:
.Ltmp403:
	movq	%rax, %r14
.Ltmp404:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp405:
	jmp	.LBB15_22
.LBB15_20:
.Ltmp406:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_21:
.Ltmp409:
	movq	%rax, %r14
.LBB15_22:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN8NArchive3N7z8CHandlerD2Ev, .Lfunc_end15-_ZN8NArchive3N7z8CHandlerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp383-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp384-.Ltmp383       #   Call between .Ltmp383 and .Ltmp384
	.long	.Ltmp385-.Lfunc_begin7  #     jumps to .Ltmp385
	.byte	0                       #   On action: cleanup
	.long	.Ltmp388-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp389-.Ltmp388       #   Call between .Ltmp388 and .Ltmp389
	.long	.Ltmp390-.Lfunc_begin7  #     jumps to .Ltmp390
	.byte	0                       #   On action: cleanup
	.long	.Ltmp393-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp394-.Ltmp393       #   Call between .Ltmp393 and .Ltmp394
	.long	.Ltmp395-.Lfunc_begin7  #     jumps to .Ltmp395
	.byte	0                       #   On action: cleanup
	.long	.Ltmp398-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp399-.Ltmp398       #   Call between .Ltmp398 and .Ltmp399
	.long	.Ltmp400-.Lfunc_begin7  #     jumps to .Ltmp400
	.byte	0                       #   On action: cleanup
	.long	.Ltmp410-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp411-.Ltmp410       #   Call between .Ltmp410 and .Ltmp411
	.long	.Ltmp412-.Lfunc_begin7  #     jumps to .Ltmp412
	.byte	0                       #   On action: cleanup
	.long	.Ltmp411-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp413-.Ltmp411       #   Call between .Ltmp411 and .Ltmp413
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp413-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp414-.Ltmp413       #   Call between .Ltmp413 and .Ltmp414
	.long	.Ltmp415-.Lfunc_begin7  #     jumps to .Ltmp415
	.byte	1                       #   On action: 1
	.long	.Ltmp386-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp397-.Ltmp386       #   Call between .Ltmp386 and .Ltmp397
	.long	.Ltmp409-.Lfunc_begin7  #     jumps to .Ltmp409
	.byte	1                       #   On action: 1
	.long	.Ltmp401-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp402-.Ltmp401       #   Call between .Ltmp401 and .Ltmp402
	.long	.Ltmp403-.Lfunc_begin7  #     jumps to .Ltmp403
	.byte	1                       #   On action: 1
	.long	.Ltmp407-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Ltmp408-.Ltmp407       #   Call between .Ltmp407 and .Ltmp408
	.long	.Ltmp409-.Lfunc_begin7  #     jumps to .Ltmp409
	.byte	1                       #   On action: 1
	.long	.Ltmp408-.Lfunc_begin7  # >> Call Site 11 <<
	.long	.Ltmp404-.Ltmp408       #   Call between .Ltmp408 and .Ltmp404
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp404-.Lfunc_begin7  # >> Call Site 12 <<
	.long	.Ltmp405-.Ltmp404       #   Call between .Ltmp404 and .Ltmp405
	.long	.Ltmp406-.Lfunc_begin7  #     jumps to .Ltmp406
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z8CHandlerD0Ev,"axG",@progbits,_ZN8NArchive3N7z8CHandlerD0Ev,comdat
	.weak	_ZN8NArchive3N7z8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandlerD0Ev,@function
_ZN8NArchive3N7z8CHandlerD0Ev:          # @_ZN8NArchive3N7z8CHandlerD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 32
.Lcfi85:
	.cfi_offset %rbx, -24
.Lcfi86:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp416:
	callq	_ZN8NArchive3N7z8CHandlerD2Ev
.Ltmp417:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB16_2:
.Ltmp418:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZN8NArchive3N7z8CHandlerD0Ev, .Lfunc_end16-_ZN8NArchive3N7z8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp416-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp417-.Ltmp416       #   Call between .Ltmp416 and .Ltmp417
	.long	.Ltmp418-.Lfunc_begin8  #     jumps to .Ltmp418
	.byte	0                       #   On action: cleanup
	.long	.Ltmp417-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end16-.Ltmp417   #   Call between .Ltmp417 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn112_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn112_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn112_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn112_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn112_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn112_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-112, %rdi
	jmp	_ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end17:
	.size	_ZThn112_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end17-_ZThn112_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn112_N8NArchive3N7z8CHandler6AddRefEv,"axG",@progbits,_ZThn112_N8NArchive3N7z8CHandler6AddRefEv,comdat
	.weak	_ZThn112_N8NArchive3N7z8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn112_N8NArchive3N7z8CHandler6AddRefEv,@function
_ZThn112_N8NArchive3N7z8CHandler6AddRefEv: # @_ZThn112_N8NArchive3N7z8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end18:
	.size	_ZThn112_N8NArchive3N7z8CHandler6AddRefEv, .Lfunc_end18-_ZThn112_N8NArchive3N7z8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn112_N8NArchive3N7z8CHandler7ReleaseEv,"axG",@progbits,_ZThn112_N8NArchive3N7z8CHandler7ReleaseEv,comdat
	.weak	_ZThn112_N8NArchive3N7z8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn112_N8NArchive3N7z8CHandler7ReleaseEv,@function
_ZThn112_N8NArchive3N7z8CHandler7ReleaseEv: # @_ZThn112_N8NArchive3N7z8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB19_2
# BB#1:
	addq	$-112, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB19_2:                               # %_ZN8NArchive3N7z8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZThn112_N8NArchive3N7z8CHandler7ReleaseEv, .Lfunc_end19-_ZThn112_N8NArchive3N7z8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn112_N8NArchive3N7z8CHandlerD1Ev,"axG",@progbits,_ZThn112_N8NArchive3N7z8CHandlerD1Ev,comdat
	.weak	_ZThn112_N8NArchive3N7z8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn112_N8NArchive3N7z8CHandlerD1Ev,@function
_ZThn112_N8NArchive3N7z8CHandlerD1Ev:   # @_ZThn112_N8NArchive3N7z8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-112, %rdi
	jmp	_ZN8NArchive3N7z8CHandlerD2Ev # TAILCALL
.Lfunc_end20:
	.size	_ZThn112_N8NArchive3N7z8CHandlerD1Ev, .Lfunc_end20-_ZThn112_N8NArchive3N7z8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn112_N8NArchive3N7z8CHandlerD0Ev,"axG",@progbits,_ZThn112_N8NArchive3N7z8CHandlerD0Ev,comdat
	.weak	_ZThn112_N8NArchive3N7z8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn112_N8NArchive3N7z8CHandlerD0Ev,@function
_ZThn112_N8NArchive3N7z8CHandlerD0Ev:   # @_ZThn112_N8NArchive3N7z8CHandlerD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 32
.Lcfi91:
	.cfi_offset %rbx, -24
.Lcfi92:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-112, %rbx
.Ltmp419:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z8CHandlerD2Ev
.Ltmp420:
# BB#1:                                 # %_ZN8NArchive3N7z8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB21_2:
.Ltmp421:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZThn112_N8NArchive3N7z8CHandlerD0Ev, .Lfunc_end21-_ZThn112_N8NArchive3N7z8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp419-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp420-.Ltmp419       #   Call between .Ltmp419 and .Ltmp420
	.long	.Ltmp421-.Lfunc_begin9  #     jumps to .Ltmp421
	.byte	0                       #   On action: cleanup
	.long	.Ltmp420-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end21-.Ltmp420   #   Call between .Ltmp420 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn120_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn120_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn120_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn120_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn120_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn120_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-120, %rdi
	jmp	_ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end22:
	.size	_ZThn120_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end22-_ZThn120_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn120_N8NArchive3N7z8CHandler6AddRefEv,"axG",@progbits,_ZThn120_N8NArchive3N7z8CHandler6AddRefEv,comdat
	.weak	_ZThn120_N8NArchive3N7z8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn120_N8NArchive3N7z8CHandler6AddRefEv,@function
_ZThn120_N8NArchive3N7z8CHandler6AddRefEv: # @_ZThn120_N8NArchive3N7z8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end23:
	.size	_ZThn120_N8NArchive3N7z8CHandler6AddRefEv, .Lfunc_end23-_ZThn120_N8NArchive3N7z8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn120_N8NArchive3N7z8CHandler7ReleaseEv,"axG",@progbits,_ZThn120_N8NArchive3N7z8CHandler7ReleaseEv,comdat
	.weak	_ZThn120_N8NArchive3N7z8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn120_N8NArchive3N7z8CHandler7ReleaseEv,@function
_ZThn120_N8NArchive3N7z8CHandler7ReleaseEv: # @_ZThn120_N8NArchive3N7z8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB24_2
# BB#1:
	addq	$-120, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB24_2:                               # %_ZN8NArchive3N7z8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZThn120_N8NArchive3N7z8CHandler7ReleaseEv, .Lfunc_end24-_ZThn120_N8NArchive3N7z8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn120_N8NArchive3N7z8CHandlerD1Ev,"axG",@progbits,_ZThn120_N8NArchive3N7z8CHandlerD1Ev,comdat
	.weak	_ZThn120_N8NArchive3N7z8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn120_N8NArchive3N7z8CHandlerD1Ev,@function
_ZThn120_N8NArchive3N7z8CHandlerD1Ev:   # @_ZThn120_N8NArchive3N7z8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-120, %rdi
	jmp	_ZN8NArchive3N7z8CHandlerD2Ev # TAILCALL
.Lfunc_end25:
	.size	_ZThn120_N8NArchive3N7z8CHandlerD1Ev, .Lfunc_end25-_ZThn120_N8NArchive3N7z8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn120_N8NArchive3N7z8CHandlerD0Ev,"axG",@progbits,_ZThn120_N8NArchive3N7z8CHandlerD0Ev,comdat
	.weak	_ZThn120_N8NArchive3N7z8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn120_N8NArchive3N7z8CHandlerD0Ev,@function
_ZThn120_N8NArchive3N7z8CHandlerD0Ev:   # @_ZThn120_N8NArchive3N7z8CHandlerD0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 32
.Lcfi97:
	.cfi_offset %rbx, -24
.Lcfi98:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-120, %rbx
.Ltmp422:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z8CHandlerD2Ev
.Ltmp423:
# BB#1:                                 # %_ZN8NArchive3N7z8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_2:
.Ltmp424:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZThn120_N8NArchive3N7z8CHandlerD0Ev, .Lfunc_end26-_ZThn120_N8NArchive3N7z8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp422-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp423-.Ltmp422       #   Call between .Ltmp422 and .Ltmp423
	.long	.Ltmp424-.Lfunc_begin10 #     jumps to .Ltmp424
	.byte	0                       #   On action: cleanup
	.long	.Ltmp423-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp423   #   Call between .Ltmp423 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, (%rbx)
.Ltmp425:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp426:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB27_2:
.Ltmp427:
	movq	%rax, %r14
.Ltmp428:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp429:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB27_4:
.Ltmp430:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev, .Lfunc_end27-_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp425-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp426-.Ltmp425       #   Call between .Ltmp425 and .Ltmp426
	.long	.Ltmp427-.Lfunc_begin11 #     jumps to .Ltmp427
	.byte	0                       #   On action: cleanup
	.long	.Ltmp426-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp428-.Ltmp426       #   Call between .Ltmp426 and .Ltmp428
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp428-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp429-.Ltmp428       #   Call between .Ltmp428 and .Ltmp429
	.long	.Ltmp430-.Lfunc_begin11 #     jumps to .Ltmp430
	.byte	1                       #   On action: 1
	.long	.Ltmp429-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end27-.Ltmp429   #   Call between .Ltmp429 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, (%rbx)
.Ltmp431:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp432:
# BB#1:
.Ltmp437:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp438:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_5:
.Ltmp439:
	movq	%rax, %r14
	jmp	.LBB28_6
.LBB28_3:
.Ltmp433:
	movq	%rax, %r14
.Ltmp434:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp435:
.LBB28_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB28_4:
.Ltmp436:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end28:
	.size	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev, .Lfunc_end28-_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp431-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp432-.Ltmp431       #   Call between .Ltmp431 and .Ltmp432
	.long	.Ltmp433-.Lfunc_begin12 #     jumps to .Ltmp433
	.byte	0                       #   On action: cleanup
	.long	.Ltmp437-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp438-.Ltmp437       #   Call between .Ltmp437 and .Ltmp438
	.long	.Ltmp439-.Lfunc_begin12 #     jumps to .Ltmp439
	.byte	0                       #   On action: cleanup
	.long	.Ltmp434-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp435-.Ltmp434       #   Call between .Ltmp434 and .Ltmp435
	.long	.Ltmp436-.Lfunc_begin12 #     jumps to .Ltmp436
	.byte	1                       #   On action: 1
	.long	.Ltmp435-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Lfunc_end28-.Ltmp435   #   Call between .Ltmp435 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi115:
	.cfi_def_cfa_offset 64
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB29_9
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB29_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB29_8
# BB#3:                                 #   in Loop: Header=BB29_2 Depth=1
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB29_5
# BB#4:                                 #   in Loop: Header=BB29_2 Depth=1
	callq	_ZdaPv
.LBB29_5:                               # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbp)
.Ltmp440:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp441:
# BB#6:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit.i
                                        #   in Loop: Header=BB29_2 Depth=1
.Ltmp446:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp447:
# BB#7:                                 # %_ZN8NArchive14COneMethodInfoD2Ev.exit
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB29_8:                               #   in Loop: Header=BB29_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB29_2
.LBB29_9:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB29_10:
.Ltmp442:
	movq	%rax, %rbx
.Ltmp443:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp444:
	jmp	.LBB29_13
.LBB29_11:
.Ltmp445:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB29_12:
.Ltmp448:
	movq	%rax, %rbx
.LBB29_13:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end29:
	.size	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii, .Lfunc_end29-_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp440-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp441-.Ltmp440       #   Call between .Ltmp440 and .Ltmp441
	.long	.Ltmp442-.Lfunc_begin13 #     jumps to .Ltmp442
	.byte	0                       #   On action: cleanup
	.long	.Ltmp446-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp447-.Ltmp446       #   Call between .Ltmp446 and .Ltmp447
	.long	.Ltmp448-.Lfunc_begin13 #     jumps to .Ltmp448
	.byte	0                       #   On action: cleanup
	.long	.Ltmp447-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp443-.Ltmp447       #   Call between .Ltmp447 and .Ltmp443
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp443-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp444-.Ltmp443       #   Call between .Ltmp443 and .Ltmp444
	.long	.Ltmp445-.Lfunc_begin13 #     jumps to .Ltmp445
	.byte	1                       #   On action: 1
	.long	.Ltmp444-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Lfunc_end29-.Ltmp444   #   Call between .Ltmp444 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED2Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED2Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED2Ev,@function
_ZN13CObjectVectorI5CPropED2Ev:         # @_ZN13CObjectVectorI5CPropED2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 32
.Lcfi125:
	.cfi_offset %rbx, -24
.Lcfi126:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp449:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp450:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB30_2:
.Ltmp451:
	movq	%rax, %r14
.Ltmp452:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp453:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB30_4:
.Ltmp454:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end30:
	.size	_ZN13CObjectVectorI5CPropED2Ev, .Lfunc_end30-_ZN13CObjectVectorI5CPropED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp449-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp450-.Ltmp449       #   Call between .Ltmp449 and .Ltmp450
	.long	.Ltmp451-.Lfunc_begin14 #     jumps to .Ltmp451
	.byte	0                       #   On action: cleanup
	.long	.Ltmp450-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp452-.Ltmp450       #   Call between .Ltmp450 and .Ltmp452
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp452-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp453-.Ltmp452       #   Call between .Ltmp452 and .Ltmp453
	.long	.Ltmp454-.Lfunc_begin14 #     jumps to .Ltmp454
	.byte	1                       #   On action: 1
	.long	.Ltmp453-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end30-.Ltmp453   #   Call between .Ltmp453 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED0Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED0Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED0Ev,@function
_ZN13CObjectVectorI5CPropED0Ev:         # @_ZN13CObjectVectorI5CPropED0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 32
.Lcfi130:
	.cfi_offset %rbx, -24
.Lcfi131:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp455:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp456:
# BB#1:
.Ltmp461:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp462:
# BB#2:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB31_5:
.Ltmp463:
	movq	%rax, %r14
	jmp	.LBB31_6
.LBB31_3:
.Ltmp457:
	movq	%rax, %r14
.Ltmp458:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp459:
.LBB31_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB31_4:
.Ltmp460:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end31:
	.size	_ZN13CObjectVectorI5CPropED0Ev, .Lfunc_end31-_ZN13CObjectVectorI5CPropED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp455-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp456-.Ltmp455       #   Call between .Ltmp455 and .Ltmp456
	.long	.Ltmp457-.Lfunc_begin15 #     jumps to .Ltmp457
	.byte	0                       #   On action: cleanup
	.long	.Ltmp461-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp462-.Ltmp461       #   Call between .Ltmp461 and .Ltmp462
	.long	.Ltmp463-.Lfunc_begin15 #     jumps to .Ltmp463
	.byte	0                       #   On action: cleanup
	.long	.Ltmp458-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp459-.Ltmp458       #   Call between .Ltmp458 and .Ltmp459
	.long	.Ltmp460-.Lfunc_begin15 #     jumps to .Ltmp460
	.byte	1                       #   On action: 1
	.long	.Ltmp459-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end31-.Ltmp459   #   Call between .Ltmp459 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI5CPropE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI5CPropE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropE6DeleteEii,@function
_ZN13CObjectVectorI5CPropE6DeleteEii:   # @_ZN13CObjectVectorI5CPropE6DeleteEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi138:
	.cfi_def_cfa_offset 64
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB32_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB32_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB32_5
# BB#3:                                 #   in Loop: Header=BB32_2 Depth=1
	leaq	8(%rbp), %rdi
.Ltmp464:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp465:
# BB#4:                                 # %_ZN5CPropD2Ev.exit
                                        #   in Loop: Header=BB32_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB32_5:                               #   in Loop: Header=BB32_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB32_2
.LBB32_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB32_7:
.Ltmp466:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZN13CObjectVectorI5CPropE6DeleteEii, .Lfunc_end32-_ZN13CObjectVectorI5CPropE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp464-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp465-.Ltmp464       #   Call between .Ltmp464 and .Ltmp465
	.long	.Ltmp466-.Lfunc_begin16 #     jumps to .Ltmp466
	.byte	0                       #   On action: cleanup
	.long	.Ltmp465-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Lfunc_end32-.Ltmp465   #   Call between .Ltmp465 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive3N7z16CArchiveDatabaseD2Ev,"axG",@progbits,_ZN8NArchive3N7z16CArchiveDatabaseD2Ev,comdat
	.weak	_ZN8NArchive3N7z16CArchiveDatabaseD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CArchiveDatabaseD2Ev,@function
_ZN8NArchive3N7z16CArchiveDatabaseD2Ev: # @_ZN8NArchive3N7z16CArchiveDatabaseD2Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r15
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 32
.Lcfi148:
	.cfi_offset %rbx, -32
.Lcfi149:
	.cfi_offset %r14, -24
.Lcfi150:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	448(%r15), %rdi
.Ltmp467:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp468:
# BB#1:
	leaq	384(%r15), %rbx
	leaq	416(%r15), %rdi
.Ltmp478:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp479:
# BB#2:
.Ltmp484:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp485:
# BB#3:                                 # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit
	leaq	320(%r15), %rbx
	leaq	352(%r15), %rdi
.Ltmp495:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp496:
# BB#4:
.Ltmp501:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp502:
# BB#5:                                 # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit13
	leaq	256(%r15), %rbx
	leaq	288(%r15), %rdi
.Ltmp512:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp513:
# BB#6:
.Ltmp518:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp519:
# BB#7:                                 # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit16
	leaq	192(%r15), %rbx
	leaq	224(%r15), %rdi
.Ltmp529:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp530:
# BB#8:
.Ltmp535:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp536:
# BB#9:                                 # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit19
	leaq	160(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE+16, 160(%r15)
.Ltmp546:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp547:
# BB#10:
.Ltmp552:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp553:
# BB#11:                                # %_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev.exit
	movq	%r15, %rdi
	subq	$-128, %rdi
.Ltmp557:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp558:
# BB#12:
	leaq	96(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 96(%r15)
.Ltmp568:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp569:
# BB#13:
.Ltmp574:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp575:
# BB#14:                                # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev.exit
	leaq	64(%r15), %rdi
.Ltmp579:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp580:
# BB#15:
	leaq	32(%r15), %rdi
.Ltmp584:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp585:
# BB#16:
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB33_62:
.Ltmp586:
	movq	%rax, %r14
	jmp	.LBB33_65
.LBB33_63:
.Ltmp581:
	movq	%rax, %r14
	jmp	.LBB33_64
.LBB33_55:
.Ltmp576:
	movq	%rax, %r14
	jmp	.LBB33_59
.LBB33_27:
.Ltmp570:
	movq	%rax, %r14
.Ltmp571:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp572:
	jmp	.LBB33_59
.LBB33_28:
.Ltmp573:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_56:
.Ltmp559:
	movq	%rax, %r14
	jmp	.LBB33_57
.LBB33_52:
.Ltmp554:
	movq	%rax, %r14
	jmp	.LBB33_39
.LBB33_25:
.Ltmp548:
	movq	%rax, %r14
.Ltmp549:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp550:
	jmp	.LBB33_39
.LBB33_26:
.Ltmp551:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_49:
.Ltmp537:
	movq	%rax, %r14
	jmp	.LBB33_37
.LBB33_23:
.Ltmp531:
	movq	%rax, %r14
.Ltmp532:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp533:
	jmp	.LBB33_37
.LBB33_24:
.Ltmp534:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_46:
.Ltmp520:
	movq	%rax, %r14
	jmp	.LBB33_35
.LBB33_21:
.Ltmp514:
	movq	%rax, %r14
.Ltmp515:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp516:
	jmp	.LBB33_35
.LBB33_22:
.Ltmp517:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_43:
.Ltmp503:
	movq	%rax, %r14
	jmp	.LBB33_33
.LBB33_19:
.Ltmp497:
	movq	%rax, %r14
.Ltmp498:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp499:
	jmp	.LBB33_33
.LBB33_20:
.Ltmp500:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_42:
.Ltmp486:
	movq	%rax, %r14
	jmp	.LBB33_31
.LBB33_17:
.Ltmp480:
	movq	%rax, %r14
.Ltmp481:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp482:
	jmp	.LBB33_31
.LBB33_18:
.Ltmp483:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_29:
.Ltmp469:
	movq	%rax, %r14
	leaq	384(%r15), %rbx
	leaq	416(%r15), %rdi
.Ltmp470:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp471:
# BB#30:
.Ltmp476:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp477:
.LBB33_31:                              # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit26
	leaq	320(%r15), %rbx
	leaq	352(%r15), %rdi
.Ltmp487:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp488:
# BB#32:
.Ltmp493:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp494:
.LBB33_33:                              # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit29
	leaq	256(%r15), %rbx
	leaq	288(%r15), %rdi
.Ltmp504:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp505:
# BB#34:
.Ltmp510:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp511:
.LBB33_35:                              # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit32
	leaq	192(%r15), %rbx
	leaq	224(%r15), %rdi
.Ltmp521:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp522:
# BB#36:
.Ltmp527:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp528:
.LBB33_37:                              # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit35
	leaq	160(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE+16, 160(%r15)
.Ltmp538:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp539:
# BB#38:
.Ltmp544:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp545:
.LBB33_39:                              # %_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev.exit38
	movq	%r15, %rdi
	subq	$-128, %rdi
.Ltmp555:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp556:
.LBB33_57:
	leaq	96(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 96(%r15)
.Ltmp560:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp561:
# BB#58:
.Ltmp566:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp567:
.LBB33_59:                              # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev.exit41
	leaq	64(%r15), %rdi
.Ltmp577:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp578:
.LBB33_64:
	leaq	32(%r15), %rdi
.Ltmp582:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp583:
.LBB33_65:
.Ltmp587:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp588:
# BB#66:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB33_40:
.Ltmp472:
	movq	%rax, %r14
.Ltmp473:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp474:
	jmp	.LBB33_68
.LBB33_41:
.Ltmp475:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_44:
.Ltmp489:
	movq	%rax, %r14
.Ltmp490:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp491:
	jmp	.LBB33_68
.LBB33_45:
.Ltmp492:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_47:
.Ltmp506:
	movq	%rax, %r14
.Ltmp507:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp508:
	jmp	.LBB33_68
.LBB33_48:
.Ltmp509:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_50:
.Ltmp523:
	movq	%rax, %r14
.Ltmp524:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp525:
	jmp	.LBB33_68
.LBB33_51:
.Ltmp526:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_53:
.Ltmp540:
	movq	%rax, %r14
.Ltmp541:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp542:
	jmp	.LBB33_68
.LBB33_54:
.Ltmp543:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_60:
.Ltmp562:
	movq	%rax, %r14
.Ltmp563:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp564:
	jmp	.LBB33_68
.LBB33_61:
.Ltmp565:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB33_67:
.Ltmp589:
	movq	%rax, %r14
.LBB33_68:                              # %.body24
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end33:
	.size	_ZN8NArchive3N7z16CArchiveDatabaseD2Ev, .Lfunc_end33-_ZN8NArchive3N7z16CArchiveDatabaseD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\253\204"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\242\004"              # Call site table length
	.long	.Ltmp467-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp468-.Ltmp467       #   Call between .Ltmp467 and .Ltmp468
	.long	.Ltmp469-.Lfunc_begin17 #     jumps to .Ltmp469
	.byte	0                       #   On action: cleanup
	.long	.Ltmp478-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp479-.Ltmp478       #   Call between .Ltmp478 and .Ltmp479
	.long	.Ltmp480-.Lfunc_begin17 #     jumps to .Ltmp480
	.byte	0                       #   On action: cleanup
	.long	.Ltmp484-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp485-.Ltmp484       #   Call between .Ltmp484 and .Ltmp485
	.long	.Ltmp486-.Lfunc_begin17 #     jumps to .Ltmp486
	.byte	0                       #   On action: cleanup
	.long	.Ltmp495-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Ltmp496-.Ltmp495       #   Call between .Ltmp495 and .Ltmp496
	.long	.Ltmp497-.Lfunc_begin17 #     jumps to .Ltmp497
	.byte	0                       #   On action: cleanup
	.long	.Ltmp501-.Lfunc_begin17 # >> Call Site 5 <<
	.long	.Ltmp502-.Ltmp501       #   Call between .Ltmp501 and .Ltmp502
	.long	.Ltmp503-.Lfunc_begin17 #     jumps to .Ltmp503
	.byte	0                       #   On action: cleanup
	.long	.Ltmp512-.Lfunc_begin17 # >> Call Site 6 <<
	.long	.Ltmp513-.Ltmp512       #   Call between .Ltmp512 and .Ltmp513
	.long	.Ltmp514-.Lfunc_begin17 #     jumps to .Ltmp514
	.byte	0                       #   On action: cleanup
	.long	.Ltmp518-.Lfunc_begin17 # >> Call Site 7 <<
	.long	.Ltmp519-.Ltmp518       #   Call between .Ltmp518 and .Ltmp519
	.long	.Ltmp520-.Lfunc_begin17 #     jumps to .Ltmp520
	.byte	0                       #   On action: cleanup
	.long	.Ltmp529-.Lfunc_begin17 # >> Call Site 8 <<
	.long	.Ltmp530-.Ltmp529       #   Call between .Ltmp529 and .Ltmp530
	.long	.Ltmp531-.Lfunc_begin17 #     jumps to .Ltmp531
	.byte	0                       #   On action: cleanup
	.long	.Ltmp535-.Lfunc_begin17 # >> Call Site 9 <<
	.long	.Ltmp536-.Ltmp535       #   Call between .Ltmp535 and .Ltmp536
	.long	.Ltmp537-.Lfunc_begin17 #     jumps to .Ltmp537
	.byte	0                       #   On action: cleanup
	.long	.Ltmp546-.Lfunc_begin17 # >> Call Site 10 <<
	.long	.Ltmp547-.Ltmp546       #   Call between .Ltmp546 and .Ltmp547
	.long	.Ltmp548-.Lfunc_begin17 #     jumps to .Ltmp548
	.byte	0                       #   On action: cleanup
	.long	.Ltmp552-.Lfunc_begin17 # >> Call Site 11 <<
	.long	.Ltmp553-.Ltmp552       #   Call between .Ltmp552 and .Ltmp553
	.long	.Ltmp554-.Lfunc_begin17 #     jumps to .Ltmp554
	.byte	0                       #   On action: cleanup
	.long	.Ltmp557-.Lfunc_begin17 # >> Call Site 12 <<
	.long	.Ltmp558-.Ltmp557       #   Call between .Ltmp557 and .Ltmp558
	.long	.Ltmp559-.Lfunc_begin17 #     jumps to .Ltmp559
	.byte	0                       #   On action: cleanup
	.long	.Ltmp568-.Lfunc_begin17 # >> Call Site 13 <<
	.long	.Ltmp569-.Ltmp568       #   Call between .Ltmp568 and .Ltmp569
	.long	.Ltmp570-.Lfunc_begin17 #     jumps to .Ltmp570
	.byte	0                       #   On action: cleanup
	.long	.Ltmp574-.Lfunc_begin17 # >> Call Site 14 <<
	.long	.Ltmp575-.Ltmp574       #   Call between .Ltmp574 and .Ltmp575
	.long	.Ltmp576-.Lfunc_begin17 #     jumps to .Ltmp576
	.byte	0                       #   On action: cleanup
	.long	.Ltmp579-.Lfunc_begin17 # >> Call Site 15 <<
	.long	.Ltmp580-.Ltmp579       #   Call between .Ltmp579 and .Ltmp580
	.long	.Ltmp581-.Lfunc_begin17 #     jumps to .Ltmp581
	.byte	0                       #   On action: cleanup
	.long	.Ltmp584-.Lfunc_begin17 # >> Call Site 16 <<
	.long	.Ltmp585-.Ltmp584       #   Call between .Ltmp584 and .Ltmp585
	.long	.Ltmp586-.Lfunc_begin17 #     jumps to .Ltmp586
	.byte	0                       #   On action: cleanup
	.long	.Ltmp585-.Lfunc_begin17 # >> Call Site 17 <<
	.long	.Ltmp571-.Ltmp585       #   Call between .Ltmp585 and .Ltmp571
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp571-.Lfunc_begin17 # >> Call Site 18 <<
	.long	.Ltmp572-.Ltmp571       #   Call between .Ltmp571 and .Ltmp572
	.long	.Ltmp573-.Lfunc_begin17 #     jumps to .Ltmp573
	.byte	1                       #   On action: 1
	.long	.Ltmp549-.Lfunc_begin17 # >> Call Site 19 <<
	.long	.Ltmp550-.Ltmp549       #   Call between .Ltmp549 and .Ltmp550
	.long	.Ltmp551-.Lfunc_begin17 #     jumps to .Ltmp551
	.byte	1                       #   On action: 1
	.long	.Ltmp532-.Lfunc_begin17 # >> Call Site 20 <<
	.long	.Ltmp533-.Ltmp532       #   Call between .Ltmp532 and .Ltmp533
	.long	.Ltmp534-.Lfunc_begin17 #     jumps to .Ltmp534
	.byte	1                       #   On action: 1
	.long	.Ltmp515-.Lfunc_begin17 # >> Call Site 21 <<
	.long	.Ltmp516-.Ltmp515       #   Call between .Ltmp515 and .Ltmp516
	.long	.Ltmp517-.Lfunc_begin17 #     jumps to .Ltmp517
	.byte	1                       #   On action: 1
	.long	.Ltmp498-.Lfunc_begin17 # >> Call Site 22 <<
	.long	.Ltmp499-.Ltmp498       #   Call between .Ltmp498 and .Ltmp499
	.long	.Ltmp500-.Lfunc_begin17 #     jumps to .Ltmp500
	.byte	1                       #   On action: 1
	.long	.Ltmp481-.Lfunc_begin17 # >> Call Site 23 <<
	.long	.Ltmp482-.Ltmp481       #   Call between .Ltmp481 and .Ltmp482
	.long	.Ltmp483-.Lfunc_begin17 #     jumps to .Ltmp483
	.byte	1                       #   On action: 1
	.long	.Ltmp470-.Lfunc_begin17 # >> Call Site 24 <<
	.long	.Ltmp471-.Ltmp470       #   Call between .Ltmp470 and .Ltmp471
	.long	.Ltmp472-.Lfunc_begin17 #     jumps to .Ltmp472
	.byte	1                       #   On action: 1
	.long	.Ltmp476-.Lfunc_begin17 # >> Call Site 25 <<
	.long	.Ltmp477-.Ltmp476       #   Call between .Ltmp476 and .Ltmp477
	.long	.Ltmp589-.Lfunc_begin17 #     jumps to .Ltmp589
	.byte	1                       #   On action: 1
	.long	.Ltmp487-.Lfunc_begin17 # >> Call Site 26 <<
	.long	.Ltmp488-.Ltmp487       #   Call between .Ltmp487 and .Ltmp488
	.long	.Ltmp489-.Lfunc_begin17 #     jumps to .Ltmp489
	.byte	1                       #   On action: 1
	.long	.Ltmp493-.Lfunc_begin17 # >> Call Site 27 <<
	.long	.Ltmp494-.Ltmp493       #   Call between .Ltmp493 and .Ltmp494
	.long	.Ltmp589-.Lfunc_begin17 #     jumps to .Ltmp589
	.byte	1                       #   On action: 1
	.long	.Ltmp504-.Lfunc_begin17 # >> Call Site 28 <<
	.long	.Ltmp505-.Ltmp504       #   Call between .Ltmp504 and .Ltmp505
	.long	.Ltmp506-.Lfunc_begin17 #     jumps to .Ltmp506
	.byte	1                       #   On action: 1
	.long	.Ltmp510-.Lfunc_begin17 # >> Call Site 29 <<
	.long	.Ltmp511-.Ltmp510       #   Call between .Ltmp510 and .Ltmp511
	.long	.Ltmp589-.Lfunc_begin17 #     jumps to .Ltmp589
	.byte	1                       #   On action: 1
	.long	.Ltmp521-.Lfunc_begin17 # >> Call Site 30 <<
	.long	.Ltmp522-.Ltmp521       #   Call between .Ltmp521 and .Ltmp522
	.long	.Ltmp523-.Lfunc_begin17 #     jumps to .Ltmp523
	.byte	1                       #   On action: 1
	.long	.Ltmp527-.Lfunc_begin17 # >> Call Site 31 <<
	.long	.Ltmp528-.Ltmp527       #   Call between .Ltmp527 and .Ltmp528
	.long	.Ltmp589-.Lfunc_begin17 #     jumps to .Ltmp589
	.byte	1                       #   On action: 1
	.long	.Ltmp538-.Lfunc_begin17 # >> Call Site 32 <<
	.long	.Ltmp539-.Ltmp538       #   Call between .Ltmp538 and .Ltmp539
	.long	.Ltmp540-.Lfunc_begin17 #     jumps to .Ltmp540
	.byte	1                       #   On action: 1
	.long	.Ltmp544-.Lfunc_begin17 # >> Call Site 33 <<
	.long	.Ltmp556-.Ltmp544       #   Call between .Ltmp544 and .Ltmp556
	.long	.Ltmp589-.Lfunc_begin17 #     jumps to .Ltmp589
	.byte	1                       #   On action: 1
	.long	.Ltmp560-.Lfunc_begin17 # >> Call Site 34 <<
	.long	.Ltmp561-.Ltmp560       #   Call between .Ltmp560 and .Ltmp561
	.long	.Ltmp562-.Lfunc_begin17 #     jumps to .Ltmp562
	.byte	1                       #   On action: 1
	.long	.Ltmp566-.Lfunc_begin17 # >> Call Site 35 <<
	.long	.Ltmp588-.Ltmp566       #   Call between .Ltmp566 and .Ltmp588
	.long	.Ltmp589-.Lfunc_begin17 #     jumps to .Ltmp589
	.byte	1                       #   On action: 1
	.long	.Ltmp588-.Lfunc_begin17 # >> Call Site 36 <<
	.long	.Ltmp473-.Ltmp588       #   Call between .Ltmp588 and .Ltmp473
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp473-.Lfunc_begin17 # >> Call Site 37 <<
	.long	.Ltmp474-.Ltmp473       #   Call between .Ltmp473 and .Ltmp474
	.long	.Ltmp475-.Lfunc_begin17 #     jumps to .Ltmp475
	.byte	1                       #   On action: 1
	.long	.Ltmp490-.Lfunc_begin17 # >> Call Site 38 <<
	.long	.Ltmp491-.Ltmp490       #   Call between .Ltmp490 and .Ltmp491
	.long	.Ltmp492-.Lfunc_begin17 #     jumps to .Ltmp492
	.byte	1                       #   On action: 1
	.long	.Ltmp507-.Lfunc_begin17 # >> Call Site 39 <<
	.long	.Ltmp508-.Ltmp507       #   Call between .Ltmp507 and .Ltmp508
	.long	.Ltmp509-.Lfunc_begin17 #     jumps to .Ltmp509
	.byte	1                       #   On action: 1
	.long	.Ltmp524-.Lfunc_begin17 # >> Call Site 40 <<
	.long	.Ltmp525-.Ltmp524       #   Call between .Ltmp524 and .Ltmp525
	.long	.Ltmp526-.Lfunc_begin17 #     jumps to .Ltmp526
	.byte	1                       #   On action: 1
	.long	.Ltmp541-.Lfunc_begin17 # >> Call Site 41 <<
	.long	.Ltmp542-.Ltmp541       #   Call between .Ltmp541 and .Ltmp542
	.long	.Ltmp543-.Lfunc_begin17 #     jumps to .Ltmp543
	.byte	1                       #   On action: 1
	.long	.Ltmp563-.Lfunc_begin17 # >> Call Site 42 <<
	.long	.Ltmp564-.Ltmp563       #   Call between .Ltmp563 and .Ltmp564
	.long	.Ltmp565-.Lfunc_begin17 #     jumps to .Ltmp565
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi153:
	.cfi_def_cfa_offset 32
.Lcfi154:
	.cfi_offset %rbx, -24
.Lcfi155:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE+16, (%rbx)
.Ltmp590:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp591:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB34_2:
.Ltmp592:
	movq	%rax, %r14
.Ltmp593:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp594:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB34_4:
.Ltmp595:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end34:
	.size	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev, .Lfunc_end34-_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp590-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp591-.Ltmp590       #   Call between .Ltmp590 and .Ltmp591
	.long	.Ltmp592-.Lfunc_begin18 #     jumps to .Ltmp592
	.byte	0                       #   On action: cleanup
	.long	.Ltmp591-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp593-.Ltmp591       #   Call between .Ltmp591 and .Ltmp593
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp593-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp594-.Ltmp593       #   Call between .Ltmp593 and .Ltmp594
	.long	.Ltmp595-.Lfunc_begin18 #     jumps to .Ltmp595
	.byte	1                       #   On action: 1
	.long	.Ltmp594-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end34-.Ltmp594   #   Call between .Ltmp594 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 32
.Lcfi159:
	.cfi_offset %rbx, -24
.Lcfi160:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, (%rbx)
.Ltmp596:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp597:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB35_2:
.Ltmp598:
	movq	%rax, %r14
.Ltmp599:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp600:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB35_4:
.Ltmp601:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev, .Lfunc_end35-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp596-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp597-.Ltmp596       #   Call between .Ltmp596 and .Ltmp597
	.long	.Ltmp598-.Lfunc_begin19 #     jumps to .Ltmp598
	.byte	0                       #   On action: cleanup
	.long	.Ltmp597-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Ltmp599-.Ltmp597       #   Call between .Ltmp597 and .Ltmp599
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp599-.Lfunc_begin19 # >> Call Site 3 <<
	.long	.Ltmp600-.Ltmp599       #   Call between .Ltmp599 and .Ltmp600
	.long	.Ltmp601-.Lfunc_begin19 #     jumps to .Ltmp601
	.byte	1                       #   On action: 1
	.long	.Ltmp600-.Lfunc_begin19 # >> Call Site 4 <<
	.long	.Lfunc_end35-.Ltmp600   #   Call between .Ltmp600 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIbED0Ev,"axG",@progbits,_ZN13CRecordVectorIbED0Ev,comdat
	.weak	_ZN13CRecordVectorIbED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIbED0Ev,@function
_ZN13CRecordVectorIbED0Ev:              # @_ZN13CRecordVectorIbED0Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 32
.Lcfi164:
	.cfi_offset %rbx, -24
.Lcfi165:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp602:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp603:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB36_2:
.Ltmp604:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end36:
	.size	_ZN13CRecordVectorIbED0Ev, .Lfunc_end36-_ZN13CRecordVectorIbED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp602-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp603-.Ltmp602       #   Call between .Ltmp602 and .Ltmp603
	.long	.Ltmp604-.Lfunc_begin20 #     jumps to .Ltmp604
	.byte	0                       #   On action: cleanup
	.long	.Ltmp603-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Lfunc_end36-.Ltmp603   #   Call between .Ltmp603 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi166:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi168:
	.cfi_def_cfa_offset 32
.Lcfi169:
	.cfi_offset %rbx, -24
.Lcfi170:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, (%rbx)
.Ltmp605:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp606:
# BB#1:
.Ltmp611:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp612:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB37_5:
.Ltmp613:
	movq	%rax, %r14
	jmp	.LBB37_6
.LBB37_3:
.Ltmp607:
	movq	%rax, %r14
.Ltmp608:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp609:
.LBB37_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB37_4:
.Ltmp610:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end37:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev, .Lfunc_end37-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp605-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp606-.Ltmp605       #   Call between .Ltmp605 and .Ltmp606
	.long	.Ltmp607-.Lfunc_begin21 #     jumps to .Ltmp607
	.byte	0                       #   On action: cleanup
	.long	.Ltmp611-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Ltmp612-.Ltmp611       #   Call between .Ltmp611 and .Ltmp612
	.long	.Ltmp613-.Lfunc_begin21 #     jumps to .Ltmp613
	.byte	0                       #   On action: cleanup
	.long	.Ltmp608-.Lfunc_begin21 # >> Call Site 3 <<
	.long	.Ltmp609-.Ltmp608       #   Call between .Ltmp608 and .Ltmp609
	.long	.Ltmp610-.Lfunc_begin21 #     jumps to .Ltmp610
	.byte	1                       #   On action: 1
	.long	.Ltmp609-.Lfunc_begin21 # >> Call Site 4 <<
	.long	.Lfunc_end37-.Ltmp609   #   Call between .Ltmp609 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi174:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi175:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi177:
	.cfi_def_cfa_offset 64
.Lcfi178:
	.cfi_offset %rbx, -56
.Lcfi179:
	.cfi_offset %r12, -48
.Lcfi180:
	.cfi_offset %r13, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB38_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB38_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB38_5
# BB#3:                                 #   in Loop: Header=BB38_2 Depth=1
.Ltmp614:
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp615:
# BB#4:                                 #   in Loop: Header=BB38_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB38_5:                               #   in Loop: Header=BB38_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB38_2
.LBB38_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB38_7:
.Ltmp616:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end38:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii, .Lfunc_end38-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp614-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp615-.Ltmp614       #   Call between .Ltmp614 and .Ltmp615
	.long	.Ltmp616-.Lfunc_begin22 #     jumps to .Ltmp616
	.byte	0                       #   On action: cleanup
	.long	.Ltmp615-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Lfunc_end38-.Ltmp615   #   Call between .Ltmp615 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive3N7z7CFolderD2Ev,"axG",@progbits,_ZN8NArchive3N7z7CFolderD2Ev,comdat
	.weak	_ZN8NArchive3N7z7CFolderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z7CFolderD2Ev,@function
_ZN8NArchive3N7z7CFolderD2Ev:           # @_ZN8NArchive3N7z7CFolderD2Ev
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi186:
	.cfi_def_cfa_offset 32
.Lcfi187:
	.cfi_offset %rbx, -24
.Lcfi188:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	96(%rbx), %rdi
.Ltmp617:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp618:
# BB#1:
	leaq	64(%rbx), %rdi
.Ltmp622:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp623:
# BB#2:
	leaq	32(%rbx), %rdi
.Ltmp627:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp628:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp639:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp640:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB39_5:
.Ltmp641:
	movq	%rax, %r14
.Ltmp642:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp643:
	jmp	.LBB39_6
.LBB39_7:
.Ltmp644:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB39_9:
.Ltmp629:
	movq	%rax, %r14
	jmp	.LBB39_12
.LBB39_10:
.Ltmp624:
	movq	%rax, %r14
	jmp	.LBB39_11
.LBB39_8:
.Ltmp619:
	movq	%rax, %r14
	leaq	64(%rbx), %rdi
.Ltmp620:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp621:
.LBB39_11:
	leaq	32(%rbx), %rdi
.Ltmp625:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp626:
.LBB39_12:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp630:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp631:
# BB#13:
.Ltmp636:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp637:
.LBB39_6:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB39_14:
.Ltmp632:
	movq	%rax, %r14
.Ltmp633:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp634:
	jmp	.LBB39_17
.LBB39_15:
.Ltmp635:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB39_16:
.Ltmp638:
	movq	%rax, %r14
.LBB39_17:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end39:
	.size	_ZN8NArchive3N7z7CFolderD2Ev, .Lfunc_end39-_ZN8NArchive3N7z7CFolderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp617-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp618-.Ltmp617       #   Call between .Ltmp617 and .Ltmp618
	.long	.Ltmp619-.Lfunc_begin23 #     jumps to .Ltmp619
	.byte	0                       #   On action: cleanup
	.long	.Ltmp622-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp623-.Ltmp622       #   Call between .Ltmp622 and .Ltmp623
	.long	.Ltmp624-.Lfunc_begin23 #     jumps to .Ltmp624
	.byte	0                       #   On action: cleanup
	.long	.Ltmp627-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp628-.Ltmp627       #   Call between .Ltmp627 and .Ltmp628
	.long	.Ltmp629-.Lfunc_begin23 #     jumps to .Ltmp629
	.byte	0                       #   On action: cleanup
	.long	.Ltmp639-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Ltmp640-.Ltmp639       #   Call between .Ltmp639 and .Ltmp640
	.long	.Ltmp641-.Lfunc_begin23 #     jumps to .Ltmp641
	.byte	0                       #   On action: cleanup
	.long	.Ltmp640-.Lfunc_begin23 # >> Call Site 5 <<
	.long	.Ltmp642-.Ltmp640       #   Call between .Ltmp640 and .Ltmp642
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp642-.Lfunc_begin23 # >> Call Site 6 <<
	.long	.Ltmp643-.Ltmp642       #   Call between .Ltmp642 and .Ltmp643
	.long	.Ltmp644-.Lfunc_begin23 #     jumps to .Ltmp644
	.byte	1                       #   On action: 1
	.long	.Ltmp620-.Lfunc_begin23 # >> Call Site 7 <<
	.long	.Ltmp626-.Ltmp620       #   Call between .Ltmp620 and .Ltmp626
	.long	.Ltmp638-.Lfunc_begin23 #     jumps to .Ltmp638
	.byte	1                       #   On action: 1
	.long	.Ltmp630-.Lfunc_begin23 # >> Call Site 8 <<
	.long	.Ltmp631-.Ltmp630       #   Call between .Ltmp630 and .Ltmp631
	.long	.Ltmp632-.Lfunc_begin23 #     jumps to .Ltmp632
	.byte	1                       #   On action: 1
	.long	.Ltmp636-.Lfunc_begin23 # >> Call Site 9 <<
	.long	.Ltmp637-.Ltmp636       #   Call between .Ltmp636 and .Ltmp637
	.long	.Ltmp638-.Lfunc_begin23 #     jumps to .Ltmp638
	.byte	1                       #   On action: 1
	.long	.Ltmp637-.Lfunc_begin23 # >> Call Site 10 <<
	.long	.Ltmp633-.Ltmp637       #   Call between .Ltmp637 and .Ltmp633
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp633-.Lfunc_begin23 # >> Call Site 11 <<
	.long	.Ltmp634-.Ltmp633       #   Call between .Ltmp633 and .Ltmp634
	.long	.Ltmp635-.Lfunc_begin23 #     jumps to .Ltmp635
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%r14
.Lcfi189:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi190:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi191:
	.cfi_def_cfa_offset 32
.Lcfi192:
	.cfi_offset %rbx, -24
.Lcfi193:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp645:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp646:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB40_2:
.Ltmp647:
	movq	%rax, %r14
.Ltmp648:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp649:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB40_4:
.Ltmp650:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev, .Lfunc_end40-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp645-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp646-.Ltmp645       #   Call between .Ltmp645 and .Ltmp646
	.long	.Ltmp647-.Lfunc_begin24 #     jumps to .Ltmp647
	.byte	0                       #   On action: cleanup
	.long	.Ltmp646-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Ltmp648-.Ltmp646       #   Call between .Ltmp646 and .Ltmp648
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp648-.Lfunc_begin24 # >> Call Site 3 <<
	.long	.Ltmp649-.Ltmp648       #   Call between .Ltmp648 and .Ltmp649
	.long	.Ltmp650-.Lfunc_begin24 #     jumps to .Ltmp650
	.byte	1                       #   On action: 1
	.long	.Ltmp649-.Lfunc_begin24 # >> Call Site 4 <<
	.long	.Lfunc_end40-.Ltmp649   #   Call between .Ltmp649 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r14
.Lcfi194:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi196:
	.cfi_def_cfa_offset 32
.Lcfi197:
	.cfi_offset %rbx, -24
.Lcfi198:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp651:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp652:
# BB#1:
.Ltmp657:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp658:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB41_5:
.Ltmp659:
	movq	%rax, %r14
	jmp	.LBB41_6
.LBB41_3:
.Ltmp653:
	movq	%rax, %r14
.Ltmp654:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp655:
.LBB41_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB41_4:
.Ltmp656:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end41:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev, .Lfunc_end41-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp651-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp652-.Ltmp651       #   Call between .Ltmp651 and .Ltmp652
	.long	.Ltmp653-.Lfunc_begin25 #     jumps to .Ltmp653
	.byte	0                       #   On action: cleanup
	.long	.Ltmp657-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Ltmp658-.Ltmp657       #   Call between .Ltmp657 and .Ltmp658
	.long	.Ltmp659-.Lfunc_begin25 #     jumps to .Ltmp659
	.byte	0                       #   On action: cleanup
	.long	.Ltmp654-.Lfunc_begin25 # >> Call Site 3 <<
	.long	.Ltmp655-.Ltmp654       #   Call between .Ltmp654 and .Ltmp655
	.long	.Ltmp656-.Lfunc_begin25 #     jumps to .Ltmp656
	.byte	1                       #   On action: 1
	.long	.Ltmp655-.Lfunc_begin25 # >> Call Site 4 <<
	.long	.Lfunc_end41-.Ltmp655   #   Call between .Ltmp655 and .Lfunc_end41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi199:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi200:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi201:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi202:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi203:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi204:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi205:
	.cfi_def_cfa_offset 64
.Lcfi206:
	.cfi_offset %rbx, -56
.Lcfi207:
	.cfi_offset %r12, -48
.Lcfi208:
	.cfi_offset %r13, -40
.Lcfi209:
	.cfi_offset %r14, -32
.Lcfi210:
	.cfi_offset %r15, -24
.Lcfi211:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB42_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB42_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB42_6
# BB#3:                                 #   in Loop: Header=BB42_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB42_5
# BB#4:                                 #   in Loop: Header=BB42_2 Depth=1
	callq	_ZdaPv
.LBB42_5:                               # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit
                                        #   in Loop: Header=BB42_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB42_6:                               #   in Loop: Header=BB42_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB42_2
.LBB42_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end42:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii, .Lfunc_end42-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB43_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB43_1:
	retq
.Lfunc_end43:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end43-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi212:
	.cfi_def_cfa_offset 16
.Lcfi213:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB44_2
# BB#1:
	callq	_ZdaPv
.LBB44_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end44:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end44-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:
	pushq	%r14
.Lcfi214:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi215:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi216:
	.cfi_def_cfa_offset 32
.Lcfi217:
	.cfi_offset %rbx, -24
.Lcfi218:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE+16, (%rbx)
.Ltmp660:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp661:
# BB#1:
.Ltmp666:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp667:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB45_5:
.Ltmp668:
	movq	%rax, %r14
	jmp	.LBB45_6
.LBB45_3:
.Ltmp662:
	movq	%rax, %r14
.Ltmp663:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp664:
.LBB45_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB45_4:
.Ltmp665:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end45:
	.size	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev, .Lfunc_end45-_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp660-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp661-.Ltmp660       #   Call between .Ltmp660 and .Ltmp661
	.long	.Ltmp662-.Lfunc_begin26 #     jumps to .Ltmp662
	.byte	0                       #   On action: cleanup
	.long	.Ltmp666-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Ltmp667-.Ltmp666       #   Call between .Ltmp666 and .Ltmp667
	.long	.Ltmp668-.Lfunc_begin26 #     jumps to .Ltmp668
	.byte	0                       #   On action: cleanup
	.long	.Ltmp663-.Lfunc_begin26 # >> Call Site 3 <<
	.long	.Ltmp664-.Ltmp663       #   Call between .Ltmp663 and .Ltmp664
	.long	.Ltmp665-.Lfunc_begin26 #     jumps to .Ltmp665
	.byte	1                       #   On action: 1
	.long	.Ltmp664-.Lfunc_begin26 # >> Call Site 4 <<
	.long	.Lfunc_end45-.Ltmp664   #   Call between .Ltmp664 and .Lfunc_end45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi219:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi220:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi221:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi222:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi223:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi224:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi225:
	.cfi_def_cfa_offset 64
.Lcfi226:
	.cfi_offset %rbx, -56
.Lcfi227:
	.cfi_offset %r12, -48
.Lcfi228:
	.cfi_offset %r13, -40
.Lcfi229:
	.cfi_offset %r14, -32
.Lcfi230:
	.cfi_offset %r15, -24
.Lcfi231:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB46_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB46_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB46_6
# BB#3:                                 #   in Loop: Header=BB46_2 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB46_5
# BB#4:                                 #   in Loop: Header=BB46_2 Depth=1
	callq	_ZdaPv
.LBB46_5:                               # %_ZN8NArchive3N7z9CFileItemD2Ev.exit
                                        #   in Loop: Header=BB46_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB46_6:                               #   in Loop: Header=BB46_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB46_2
.LBB46_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end46:
	.size	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii, .Lfunc_end46-_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:
	pushq	%r14
.Lcfi232:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi233:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi234:
	.cfi_def_cfa_offset 32
.Lcfi235:
	.cfi_offset %rbx, -24
.Lcfi236:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp669:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp670:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB47_2:
.Ltmp671:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end47:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end47-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table47:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp669-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp670-.Ltmp669       #   Call between .Ltmp669 and .Ltmp670
	.long	.Ltmp671-.Lfunc_begin27 #     jumps to .Ltmp671
	.byte	0                       #   On action: cleanup
	.long	.Ltmp670-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Lfunc_end47-.Ltmp670   #   Call between .Ltmp670 and .Lfunc_end47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:
	pushq	%r14
.Lcfi237:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi238:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi239:
	.cfi_def_cfa_offset 32
.Lcfi240:
	.cfi_offset %rbx, -24
.Lcfi241:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE+16, (%rbx)
.Ltmp672:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp673:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB48_2:
.Ltmp674:
	movq	%rax, %r14
.Ltmp675:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp676:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB48_4:
.Ltmp677:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end48:
	.size	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev, .Lfunc_end48-_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table48:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp672-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp673-.Ltmp672       #   Call between .Ltmp672 and .Ltmp673
	.long	.Ltmp674-.Lfunc_begin28 #     jumps to .Ltmp674
	.byte	0                       #   On action: cleanup
	.long	.Ltmp673-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Ltmp675-.Ltmp673       #   Call between .Ltmp673 and .Ltmp675
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp675-.Lfunc_begin28 # >> Call Site 3 <<
	.long	.Ltmp676-.Ltmp675       #   Call between .Ltmp675 and .Ltmp676
	.long	.Ltmp677-.Lfunc_begin28 #     jumps to .Ltmp677
	.byte	1                       #   On action: 1
	.long	.Ltmp676-.Lfunc_begin28 # >> Call Site 4 <<
	.long	.Lfunc_end48-.Ltmp676   #   Call between .Ltmp676 and .Lfunc_end48
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED0Ev
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:
	pushq	%r14
.Lcfi242:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi243:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi244:
	.cfi_def_cfa_offset 32
.Lcfi245:
	.cfi_offset %rbx, -24
.Lcfi246:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE+16, (%rbx)
.Ltmp678:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp679:
# BB#1:
.Ltmp684:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp685:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB49_5:
.Ltmp686:
	movq	%rax, %r14
	jmp	.LBB49_6
.LBB49_3:
.Ltmp680:
	movq	%rax, %r14
.Ltmp681:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp682:
.LBB49_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB49_4:
.Ltmp683:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end49:
	.size	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED0Ev, .Lfunc_end49-_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table49:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp678-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp679-.Ltmp678       #   Call between .Ltmp678 and .Ltmp679
	.long	.Ltmp680-.Lfunc_begin29 #     jumps to .Ltmp680
	.byte	0                       #   On action: cleanup
	.long	.Ltmp684-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Ltmp685-.Ltmp684       #   Call between .Ltmp684 and .Ltmp685
	.long	.Ltmp686-.Lfunc_begin29 #     jumps to .Ltmp686
	.byte	0                       #   On action: cleanup
	.long	.Ltmp681-.Lfunc_begin29 # >> Call Site 3 <<
	.long	.Ltmp682-.Ltmp681       #   Call between .Ltmp681 and .Ltmp682
	.long	.Ltmp683-.Lfunc_begin29 #     jumps to .Ltmp683
	.byte	1                       #   On action: 1
	.long	.Ltmp682-.Lfunc_begin29 # >> Call Site 4 <<
	.long	.Lfunc_end49-.Ltmp682   #   Call between .Ltmp682 and .Lfunc_end49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z8CInByte2EE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi247:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi248:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi249:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi250:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi251:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi252:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi253:
	.cfi_def_cfa_offset 64
.Lcfi254:
	.cfi_offset %rbx, -56
.Lcfi255:
	.cfi_offset %r12, -48
.Lcfi256:
	.cfi_offset %r13, -40
.Lcfi257:
	.cfi_offset %r14, -32
.Lcfi258:
	.cfi_offset %r15, -24
.Lcfi259:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r14d
	movq	%rdi, %r12
	leal	(%rdx,%r14), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	subl	%r14d, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB50_5
# BB#1:                                 # %.lr.ph
	movslq	%r14d, %rbp
	movslq	%r15d, %r13
	shlq	$3, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB50_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbp, %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB50_4
# BB#3:                                 #   in Loop: Header=BB50_2 Depth=1
	callq	_ZdlPv
.LBB50_4:                               #   in Loop: Header=BB50_2 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	.LBB50_2
.LBB50_5:                               # %._crit_edge
	movq	%r12, %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end50:
	.size	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EE6DeleteEii, .Lfunc_end50-_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EE6DeleteEii
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z16CArchiveDatabase5ClearEv,"axG",@progbits,_ZN8NArchive3N7z16CArchiveDatabase5ClearEv,comdat
	.weak	_ZN8NArchive3N7z16CArchiveDatabase5ClearEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CArchiveDatabase5ClearEv,@function
_ZN8NArchive3N7z16CArchiveDatabase5ClearEv: # @_ZN8NArchive3N7z16CArchiveDatabase5ClearEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi260:
	.cfi_def_cfa_offset 16
.Lcfi261:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	32(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	64(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	96(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%rbx, %rdi
	subq	$-128, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	160(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	192(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	224(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	256(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	288(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	320(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	352(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	384(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	416(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	addq	$448, %rbx              # imm = 0x1C0
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZN17CBaseRecordVector5ClearEv # TAILCALL
.Lfunc_end51:
	.size	_ZN8NArchive3N7z16CArchiveDatabase5ClearEv, .Lfunc_end51-_ZN8NArchive3N7z16CArchiveDatabase5ClearEv
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,@function
_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev: # @_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
.Lfunc_begin30:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception30
# BB#0:
	pushq	%r14
.Lcfi262:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi263:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi264:
	.cfi_def_cfa_offset 32
.Lcfi265:
	.cfi_offset %rbx, -24
.Lcfi266:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp687:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp688:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB52_2:
.Ltmp689:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end52:
	.size	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev, .Lfunc_end52-_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table52:
.Lexception30:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp687-.Lfunc_begin30 # >> Call Site 1 <<
	.long	.Ltmp688-.Ltmp687       #   Call between .Ltmp687 and .Ltmp688
	.long	.Ltmp689-.Lfunc_begin30 #     jumps to .Ltmp689
	.byte	0                       #   On action: cleanup
	.long	.Ltmp688-.Lfunc_begin30 # >> Call Site 2 <<
	.long	.Lfunc_end52-.Ltmp688   #   Call between .Ltmp688 and .Lfunc_end52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin31:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception31
# BB#0:
	pushq	%r14
.Lcfi267:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi268:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi269:
	.cfi_def_cfa_offset 32
.Lcfi270:
	.cfi_offset %rbx, -24
.Lcfi271:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp690:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp691:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB53_2:
.Ltmp692:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end53:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end53-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table53:
.Lexception31:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp690-.Lfunc_begin31 # >> Call Site 1 <<
	.long	.Ltmp691-.Ltmp690       #   Call between .Ltmp690 and .Ltmp691
	.long	.Ltmp692-.Lfunc_begin31 #     jumps to .Ltmp692
	.byte	0                       #   On action: cleanup
	.long	.Ltmp691-.Lfunc_begin31 # >> Call Site 2 <<
	.long	.Lfunc_end53-.Ltmp691   #   Call between .Ltmp691 and .Lfunc_end53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTVN8NArchive3N7z8CHandlerE,@object # @_ZTVN8NArchive3N7z8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive3N7z8CHandlerE
	.p2align	3
_ZTVN8NArchive3N7z8CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive3N7z8CHandlerE
	.quad	_ZN8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive3N7z8CHandler6AddRefEv
	.quad	_ZN8NArchive3N7z8CHandler7ReleaseEv
	.quad	_ZN8NArchive3N7z8CHandlerD2Ev
	.quad	_ZN8NArchive3N7z8CHandlerD0Ev
	.quad	_ZN8NArchive3N7z8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive3N7z8CHandler5CloseEv
	.quad	_ZN8NArchive3N7z8CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive3N7z8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive3N7z8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive3N7z8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive3N7z8CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive3N7z8CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive3N7z8CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive3N7z8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.quad	_ZN8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZN8NArchive3N7z8CHandler15GetFileTimeTypeEPj
	.quad	-112
	.quad	_ZTIN8NArchive3N7z8CHandlerE
	.quad	_ZThn112_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn112_N8NArchive3N7z8CHandler6AddRefEv
	.quad	_ZThn112_N8NArchive3N7z8CHandler7ReleaseEv
	.quad	_ZThn112_N8NArchive3N7z8CHandlerD1Ev
	.quad	_ZThn112_N8NArchive3N7z8CHandlerD0Ev
	.quad	_ZThn112_N8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.quad	-120
	.quad	_ZTIN8NArchive3N7z8CHandlerE
	.quad	_ZThn120_N8NArchive3N7z8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn120_N8NArchive3N7z8CHandler6AddRefEv
	.quad	_ZThn120_N8NArchive3N7z8CHandler7ReleaseEv
	.quad	_ZThn120_N8NArchive3N7z8CHandlerD1Ev
	.quad	_ZThn120_N8NArchive3N7z8CHandlerD0Ev
	.quad	_ZThn120_N8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZThn120_N8NArchive3N7z8CHandler15GetFileTimeTypeEPj
	.size	_ZTVN8NArchive3N7z8CHandlerE, 296

	.type	_ZN8NArchive3N7z9kArcPropsE,@object # @_ZN8NArchive3N7z9kArcPropsE
	.data
	.globl	_ZN8NArchive3N7z9kArcPropsE
	.p2align	4
_ZN8NArchive3N7z9kArcPropsE:
	.quad	0
	.long	22                      # 0x16
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	13                      # 0xd
	.short	11                      # 0xb
	.zero	2
	.quad	0
	.long	38                      # 0x26
	.short	19                      # 0x13
	.zero	2
	.quad	0
	.long	44                      # 0x2c
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	45                      # 0x2d
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	36                      # 0x24
	.short	21                      # 0x15
	.zero	2
	.size	_ZN8NArchive3N7z9kArcPropsE, 96

	.type	_ZTSN8NArchive3N7z8CHandlerE,@object # @_ZTSN8NArchive3N7z8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN8NArchive3N7z8CHandlerE
	.p2align	4
_ZTSN8NArchive3N7z8CHandlerE:
	.asciz	"N8NArchive3N7z8CHandlerE"
	.size	_ZTSN8NArchive3N7z8CHandlerE, 25

	.type	_ZTSN8NArchive11COutHandlerE,@object # @_ZTSN8NArchive11COutHandlerE
	.section	.rodata._ZTSN8NArchive11COutHandlerE,"aG",@progbits,_ZTSN8NArchive11COutHandlerE,comdat
	.weak	_ZTSN8NArchive11COutHandlerE
	.p2align	4
_ZTSN8NArchive11COutHandlerE:
	.asciz	"N8NArchive11COutHandlerE"
	.size	_ZTSN8NArchive11COutHandlerE, 25

	.type	_ZTIN8NArchive11COutHandlerE,@object # @_ZTIN8NArchive11COutHandlerE
	.section	.rodata._ZTIN8NArchive11COutHandlerE,"aG",@progbits,_ZTIN8NArchive11COutHandlerE,comdat
	.weak	_ZTIN8NArchive11COutHandlerE
	.p2align	3
_ZTIN8NArchive11COutHandlerE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NArchive11COutHandlerE
	.size	_ZTIN8NArchive11COutHandlerE, 16

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS14ISetProperties,@object # @_ZTS14ISetProperties
	.section	.rodata._ZTS14ISetProperties,"aG",@progbits,_ZTS14ISetProperties,comdat
	.weak	_ZTS14ISetProperties
	.p2align	4
_ZTS14ISetProperties:
	.asciz	"14ISetProperties"
	.size	_ZTS14ISetProperties, 17

	.type	_ZTI14ISetProperties,@object # @_ZTI14ISetProperties
	.section	.rodata._ZTI14ISetProperties,"aG",@progbits,_ZTI14ISetProperties,comdat
	.weak	_ZTI14ISetProperties
	.p2align	4
_ZTI14ISetProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ISetProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI14ISetProperties, 24

	.type	_ZTS11IOutArchive,@object # @_ZTS11IOutArchive
	.section	.rodata._ZTS11IOutArchive,"aG",@progbits,_ZTS11IOutArchive,comdat
	.weak	_ZTS11IOutArchive
_ZTS11IOutArchive:
	.asciz	"11IOutArchive"
	.size	_ZTS11IOutArchive, 14

	.type	_ZTI11IOutArchive,@object # @_ZTI11IOutArchive
	.section	.rodata._ZTI11IOutArchive,"aG",@progbits,_ZTI11IOutArchive,comdat
	.weak	_ZTI11IOutArchive
	.p2align	4
_ZTI11IOutArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11IOutArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI11IOutArchive, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive3N7z8CHandlerE,@object # @_ZTIN8NArchive3N7z8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive3N7z8CHandlerE
	.p2align	4
_ZTIN8NArchive3N7z8CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive3N7z8CHandlerE
	.long	1                       # 0x1
	.long	5                       # 0x5
	.quad	_ZTIN8NArchive11COutHandlerE
	.quad	2050                    # 0x802
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI14ISetProperties
	.quad	28674                   # 0x7002
	.quad	_ZTI11IOutArchive
	.quad	30722                   # 0x7802
	.quad	_ZTI13CMyUnknownImp
	.quad	32770                   # 0x8002
	.size	_ZTIN8NArchive3N7z8CHandlerE, 104

	.type	_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE:
	.asciz	"13CObjectVectorIN8NArchive14COneMethodInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE, 45

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE, 24

	.type	_ZTV13CObjectVectorI5CPropE,@object # @_ZTV13CObjectVectorI5CPropE
	.section	.rodata._ZTV13CObjectVectorI5CPropE,"aG",@progbits,_ZTV13CObjectVectorI5CPropE,comdat
	.weak	_ZTV13CObjectVectorI5CPropE
	.p2align	3
_ZTV13CObjectVectorI5CPropE:
	.quad	0
	.quad	_ZTI13CObjectVectorI5CPropE
	.quad	_ZN13CObjectVectorI5CPropED2Ev
	.quad	_ZN13CObjectVectorI5CPropED0Ev
	.quad	_ZN13CObjectVectorI5CPropE6DeleteEii
	.size	_ZTV13CObjectVectorI5CPropE, 40

	.type	_ZTS13CObjectVectorI5CPropE,@object # @_ZTS13CObjectVectorI5CPropE
	.section	.rodata._ZTS13CObjectVectorI5CPropE,"aG",@progbits,_ZTS13CObjectVectorI5CPropE,comdat
	.weak	_ZTS13CObjectVectorI5CPropE
	.p2align	4
_ZTS13CObjectVectorI5CPropE:
	.asciz	"13CObjectVectorI5CPropE"
	.size	_ZTS13CObjectVectorI5CPropE, 24

	.type	_ZTI13CObjectVectorI5CPropE,@object # @_ZTI13CObjectVectorI5CPropE
	.section	.rodata._ZTI13CObjectVectorI5CPropE,"aG",@progbits,_ZTI13CObjectVectorI5CPropE,comdat
	.weak	_ZTI13CObjectVectorI5CPropE
	.p2align	4
_ZTI13CObjectVectorI5CPropE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI5CPropE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI5CPropE, 24

	.type	_ZTV13CRecordVectorIbE,@object # @_ZTV13CRecordVectorIbE
	.section	.rodata._ZTV13CRecordVectorIbE,"aG",@progbits,_ZTV13CRecordVectorIbE,comdat
	.weak	_ZTV13CRecordVectorIbE
	.p2align	3
_ZTV13CRecordVectorIbE:
	.quad	0
	.quad	_ZTI13CRecordVectorIbE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIbED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIbE, 40

	.type	_ZTS13CRecordVectorIbE,@object # @_ZTS13CRecordVectorIbE
	.section	.rodata._ZTS13CRecordVectorIbE,"aG",@progbits,_ZTS13CRecordVectorIbE,comdat
	.weak	_ZTS13CRecordVectorIbE
	.p2align	4
_ZTS13CRecordVectorIbE:
	.asciz	"13CRecordVectorIbE"
	.size	_ZTS13CRecordVectorIbE, 19

	.type	_ZTI13CRecordVectorIbE,@object # @_ZTI13CRecordVectorIbE
	.section	.rodata._ZTI13CRecordVectorIbE,"aG",@progbits,_ZTI13CRecordVectorIbE,comdat
	.weak	_ZTI13CRecordVectorIbE
	.p2align	4
_ZTI13CRecordVectorIbE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIbE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIbE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z7CFolderEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE, 41

	.type	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z10CCoderInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 45

	.type	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 24

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z9CFileItemEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE, 43

	.type	_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE, 24

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z8CInByte2EE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z8CInByte2EE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z8CInByte2EE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z8CInByte2EE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z8CInByte2EE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z8CInByte2EE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z8CInByte2EE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z8CInByte2EE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z8CInByte2EE:
	.asciz	"13CObjectVectorIN8NArchive3N7z8CInByte2EE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z8CInByte2EE, 42

	.type	_ZTI13CObjectVectorIN8NArchive3N7z8CInByte2EE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z8CInByte2EE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z8CInByte2EE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z8CInByte2EE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z8CInByte2EE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z8CInByte2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z8CInByte2EE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z8CInByte2EE, 24

	.type	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE:
	.asciz	"13CRecordVectorIN8NArchive3N7z5CBindEE"
	.size	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE, 39

	.type	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24


	.globl	_ZN8NArchive3N7z8CHandlerC1Ev
	.type	_ZN8NArchive3N7z8CHandlerC1Ev,@function
_ZN8NArchive3N7z8CHandlerC1Ev = _ZN8NArchive3N7z8CHandlerC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
