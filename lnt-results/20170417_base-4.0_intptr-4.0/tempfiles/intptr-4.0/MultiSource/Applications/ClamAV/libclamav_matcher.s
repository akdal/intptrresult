	.text
	.file	"libclamav_matcher.bc"
	.globl	cli_scanbuff
	.p2align	4, 0x90
	.type	cli_scanbuff,@function
cli_scanbuff:                           # @cli_scanbuff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%esi, %r15d
	movq	%rdi, %r13
	testq	%rcx, %rcx
	je	.LBB0_1
# BB#2:
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	16(%rcx), %rax
	movq	(%rax), %r14
	leal	-502(%rbx), %edx
	cmpl	$27, %edx
	ja	.LBB0_15
# BB#3:
	movl	$1, %ecx
	jmpq	*.LJTI0_0(,%rdx,8)
.LBB0_8:                                # %.fold.split61
	movl	$6, %ecx
	jmp	.LBB0_9
.LBB0_1:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, %r12d
	jmp	.LBB0_20
.LBB0_4:                                # %.fold.split
	movl	$2, %ecx
	jmp	.LBB0_9
.LBB0_7:                                # %.fold.split60
	movl	$5, %ecx
	jmp	.LBB0_9
.LBB0_5:                                # %.fold.split58
	movl	$3, %ecx
	jmp	.LBB0_9
.LBB0_6:                                # %.fold.split59
	movl	$4, %ecx
.LBB0_9:
	movq	(%rax,%rcx,8), %rbp
	testq	%rbp, %rbp
	je	.LBB0_15
# BB#10:
	movl	64(%rbp), %esi
	leaq	8(%rsp), %rdi
	movl	$8, %edx
	callq	cli_ac_initdata
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB0_20
# BB#11:
	cmpb	$0, 2(%rbp)
	jne	.LBB0_14
# BB#12:
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	%r15d, %esi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rbp, %rcx
	movl	%ebx, %r9d
	pushq	$-1
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	cli_bm_scanbuff
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	cmpl	$1, %eax
	jne	.LBB0_14
# BB#13:                                # %.thread49
	leaq	8(%rsp), %rdi
	callq	cli_ac_freedata
	movl	$1, %r12d
	jmp	.LBB0_20
.LBB0_14:
	leaq	8(%rsp), %r12
	movl	$0, %r9d
	movq	%r13, %rdi
	movl	%r15d, %esi
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rbp, %rcx
	movq	%r12, %r8
	pushq	$0
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	$-1
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_scanbuff
	addq	$32, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %ebp
	movq	%r12, %rdi
	callq	cli_ac_freedata
	movl	$1, %r12d
	cmpl	$1, %ebp
	je	.LBB0_20
.LBB0_15:                               # %.thread
	movl	64(%r14), %esi
	leaq	8(%rsp), %rdi
	movl	$8, %edx
	callq	cli_ac_initdata
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB0_20
# BB#16:
	movq	%r14, %rbp
	cmpb	$0, 2(%rbp)
	movq	(%rsp), %r14            # 8-byte Reload
	jne	.LBB0_18
# BB#17:
	subq	$8, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	%r15d, %esi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	movl	%ebx, %r9d
	pushq	$-1
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	callq	cli_bm_scanbuff
	addq	$16, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset -16
	movl	$1, %r12d
	cmpl	$1, %eax
	je	.LBB0_19
.LBB0_18:
	leaq	8(%rsp), %r8
	movl	$0, %r9d
	movq	%r13, %rdi
	movl	%r15d, %esi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	pushq	$0
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	$-1
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_scanbuff
	addq	$32, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %r12d
.LBB0_19:
	leaq	8(%rsp), %rdi
	callq	cli_ac_freedata
.LBB0_20:
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_scanbuff, .Lfunc_end0-cli_scanbuff
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_9
	.quad	.LBB0_8
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_4
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_7
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_15
	.quad	.LBB0_5
	.quad	.LBB0_6

	.text
	.globl	cli_vermd5
	.p2align	4, 0x90
	.type	cli_vermd5,@function
cli_vermd5:                             # @cli_vermd5
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	24(%rsi), %rax
	movzbl	(%r14), %ecx
	movq	(%rax,%rcx,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_2
	jmp	.LBB1_4
	.p2align	4, 0x90
.LBB1_3:                                #   in Loop: Header=BB1_2 Depth=1
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB1_4
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	$16, %edx
	movq	%r14, %rsi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB1_3
	jmp	.LBB1_5
.LBB1_4:
	xorl	%ebx, %ebx
.LBB1_5:                                # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	cli_vermd5, .Lfunc_end1-cli_vermd5
	.cfi_endproc

	.globl	cli_caloff
	.p2align	4, 0x90
	.type	cli_caloff,@function
cli_caloff:                             # @cli_caloff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 224
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movl	%ecx, %ebp
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%r8, (%rsp)             # 8-byte Spill
	movl	$0, (%r8)
	movl	$.L.str.1, %esi
	movl	$2, %edx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_2
# BB#1:
	cmpb	$83, (%rbx)
	jne	.LBB2_10
.LBB2_2:
	movb	32(%r15), %al
	testb	%al, %al
	je	.LBB2_4
# BB#3:
	cmpb	$-1, %al
	je	.LBB2_24
	jmp	.LBB2_10
.LBB2_4:
	xorl	%eax, %eax
	cmpl	$503, %ebp              # imm = 0x1F7
	movl	$cli_elfheader, %ecx
	cmoveq	%rcx, %rax
	cmpl	$502, %ebp              # imm = 0x1F6
	movl	$cli_peheader, %r12d
	cmovneq	%rax, %r12
	testq	%r12, %r12
	je	.LBB2_10
# BB#5:
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r14d, %ebp
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB2_8
# BB#6:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	callq	lseek
	leaq	8(%r15), %rsi
	movl	%ebp, %edi
	callq	*%r12
	movl	%eax, %r12d
	xorl	%edx, %edx
	movl	%ebp, %edi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	lseek
	testl	%r12d, %r12d
	je	.LBB2_9
.LBB2_7:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$-1, (%rax)
	movb	$-1, 32(%r15)
	jmp	.LBB2_25
.LBB2_8:
	xorl	%ebp, %ebp
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$-1, (%rax)
	movb	$-1, 32(%r15)
	jmp	.LBB2_26
.LBB2_9:
	movb	$1, 32(%r15)
.LBB2_10:
	movl	$44, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB2_12
# BB#11:
	incq	%rax
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	movl	%eax, (%r13)
.LBB2_12:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	(%rbx), %rbp
	testb	$8, 1(%rax,%rbp,2)
	jne	.LBB2_22
# BB#13:
	cmpb	$1, 32(%r15)
	jne	.LBB2_23
# BB#14:
	movl	$.L.str.3, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_27
# BB#15:
	movl	$.L.str.4, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_27
# BB#16:
	cmpb	$83, %bpl
	jne	.LBB2_23
# BB#17:
	movl	$.L.str.5, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_34
.LBB2_18:
	leaq	24(%rsp), %rdx
	leaq	12(%rsp), %rcx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sscanf
	cmpl	$2, %eax
	jne	.LBB2_24
# BB#19:
	movl	24(%rsp), %eax
	movzwl	12(%r15), %ecx
	cmpl	%ecx, %eax
	jae	.LBB2_24
# BB#20:
	movq	24(%r15), %rcx
	leaq	(%rax,%rax,8), %rax
	cmpl	$0, 12(%rcx,%rax,4)
	je	.LBB2_24
# BB#21:
	leaq	8(%rcx,%rax,4), %rax
	jmp	.LBB2_37
.LBB2_22:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movslq	%eax, %rbp
	jmp	.LBB2_26
.LBB2_23:                               # %.thread
	movl	$.L.str.8, %esi
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_29
.LBB2_24:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$-1, (%rax)
.LBB2_25:
	xorl	%ebp, %ebp
.LBB2_26:
	movq	%rbp, %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_27:
	movb	2(%rbx), %r12b
	movl	8(%r15), %r14d
	addq	$3, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %rbp
	cmpb	$43, %r12b
	jne	.LBB2_33
# BB#28:
	addl	%r14d, %ebp
	jmp	.LBB2_26
.LBB2_29:
	movq	(%r15), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_32
# BB#30:
	leaq	24(%rsp), %rdx
	movl	$1, %edi
	movl	%r14d, %esi
	callq	__fxstat
	cmpl	$-1, %eax
	je	.LBB2_7
# BB#31:
	movq	72(%rsp), %rbp
	movq	%rbp, (%r15)
.LBB2_32:
	addq	$4, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	cltq
	subq	%rax, %rbp
	jmp	.LBB2_26
.LBB2_33:
	subl	%ebp, %r14d
	movq	%r14, %rbp
	jmp	.LBB2_26
.LBB2_34:
	movq	24(%r15), %rax
	movzwl	12(%r15), %ecx
	leaq	(%rcx,%rcx,8), %rcx
	cmpl	$0, -24(%rax,%rcx,4)
	je	.LBB2_18
# BB#35:
	leaq	12(%rsp), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB2_24
# BB#36:
	movq	24(%r15), %rax
	movzwl	12(%r15), %ecx
	leaq	(%rcx,%rcx,8), %rcx
	leaq	-28(%rax,%rcx,4), %rax
.LBB2_37:
	movl	(%rax), %ebp
	addl	12(%rsp), %ebp
	jmp	.LBB2_26
.Lfunc_end2:
	.size	cli_caloff, .Lfunc_end2-cli_caloff
	.cfi_endproc

	.globl	cli_validatesig
	.p2align	4, 0x90
	.type	cli_validatesig,@function
cli_validatesig:                        # @cli_validatesig
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 48
.Lcfi51:
	.cfi_offset %rbx, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rdx, %rbx
	movl	%edi, %eax
	movl	$0, 8(%rsp)
	movl	$1, %ebp
	testq	%rsi, %rsi
	je	.LBB3_10
# BB#1:
	cmpl	$-1, %r8d
	je	.LBB3_10
# BB#2:
	leaq	12(%rsp), %r10
	leaq	8(%rsp), %r9
	movq	%rsi, %rdi
	movq	%rcx, %rsi
	movl	%r8d, %edx
	movl	%eax, %ecx
	movq	%r10, %r8
	callq	cli_caloff
	movq	%rax, %rdx
	cmpl	$-1, 12(%rsp)
	je	.LBB3_3
# BB#4:
	movl	8(%rsp), %ecx
	testq	%rcx, %rcx
	je	.LBB3_8
# BB#5:
	addq	%rdx, %rcx
	cmpq	%rbx, %rdx
	jg	.LBB3_7
# BB#6:
	cmpq	%rbx, %rcx
	jge	.LBB3_10
.LBB3_7:                                # %._crit_edge
	xorl	%ebp, %ebp
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r14, %r8
	callq	cli_dbgmsg
	jmp	.LBB3_10
.LBB3_3:
	xorl	%ebp, %ebp
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB3_10
.LBB3_8:
	cmpq	%rbx, %rdx
	je	.LBB3_10
# BB#9:
	xorl	%ebp, %ebp
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r14, %rcx
	callq	cli_dbgmsg
.LBB3_10:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	cli_validatesig, .Lfunc_end3-cli_validatesig
	.cfi_endproc

	.globl	cli_scandesc
	.p2align	4, 0x90
	.type	cli_scandesc,@function
cli_scandesc:                           # @cli_scandesc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$472, %rsp              # imm = 0x1D8
.Lcfi60:
	.cfi_def_cfa_offset 528
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movl	%r8d, %r12d
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movq	%rsi, %r14
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.LBB4_1
# BB#2:
	testb	%r12b, %r12b
	je	.LBB4_4
# BB#3:
	xorl	%r15d, %r15d
	jmp	.LBB4_5
.LBB4_1:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, %eax
	jmp	.LBB4_83
.LBB4_4:
	movq	16(%rax), %rcx
	movq	(%rcx), %r15
.LBB4_5:
	xorl	%ebp, %ebp
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	-502(%rcx), %edx
	cmpl	$27, %edx
	ja	.LBB4_13
# BB#6:
	movl	$1, %ecx
	jmpq	*.LJTI4_0(,%rdx,8)
.LBB4_11:                               # %.fold.split225
	movl	$6, %ecx
	jmp	.LBB4_12
.LBB4_7:                                # %.fold.split
	movl	$2, %ecx
	jmp	.LBB4_12
.LBB4_10:                               # %.fold.split224
	movl	$5, %ecx
	jmp	.LBB4_12
.LBB4_8:                                # %.fold.split222
	movl	$3, %ecx
	jmp	.LBB4_12
.LBB4_9:                                # %.fold.split223
	movl	$4, %ecx
.LBB4_12:
	movq	16(%rax), %rax
	movq	(%rax,%rcx,8), %rbp
.LBB4_13:                               # %.loopexit
	testb	%r12b, %r12b
	je	.LBB4_16
# BB#14:
	testq	%rbp, %rbp
	movq	%rbp, %rax
	jne	.LBB4_18
# BB#15:
	xorl	%eax, %eax
	jmp	.LBB4_83
.LBB4_16:
	testq	%rbp, %rbp
	movq	%r15, %rax
	je	.LBB4_18
# BB#17:
	movzwl	(%rbp), %eax
	cmpw	(%r15), %ax
	movq	%r15, %rax
	cmovaq	%rbp, %rax
.LBB4_18:
	movzwl	(%rax), %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leal	131072(%rax), %ebx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB4_19
# BB#20:
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testb	%r12b, %r12b
	je	.LBB4_21
.LBB4_22:
	testq	%rbp, %rbp
	je	.LBB4_24
# BB#23:
	movl	64(%rbp), %esi
	leaq	136(%rsp), %rdi
	movl	$8, %edx
	callq	cli_ac_initdata
	testl	%eax, %eax
	jne	.LBB4_83
.LBB4_24:
	testb	%r12b, %r12b
	jne	.LBB4_27
# BB#25:
	movq	24(%r14), %rax
	cmpq	$0, 24(%rax)
	je	.LBB4_27
# BB#26:
	leaq	176(%rsp), %rdi
	callq	cli_md5_init
.LBB4_27:
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	(%rbx,%rdx), %rax
	leaq	131072(%rbx), %rsi
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	movl	%edx, %esi
	negl	%esi
	movl	%esi, 92(%rsp)          # 4-byte Spill
	xorl	%r13d, %r13d
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rax, %rsi
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r14, (%rsp)            # 8-byte Spill
	movl	%r12d, 28(%rsp)         # 4-byte Spill
	movq	%r15, 104(%rsp)         # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	jmp	.LBB4_28
.LBB4_19:
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movl	$-114, %eax
	jmp	.LBB4_83
.LBB4_21:
	movl	64(%r15), %esi
	leaq	120(%rsp), %rdi
	movl	$8, %edx
	callq	cli_ac_initdata
	testl	%eax, %eax
	jne	.LBB4_83
	jmp	.LBB4_22
.LBB4_29:                               # %.outer.split.preheader
                                        #   in Loop: Header=BB4_28 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_30:                               # %.outer.split
                                        #   Parent Loop BB4_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r12d, %ebp
	addq	72(%rsp), %rbp          # 8-byte Folded Reload
	movl	$131072, %edx           # imm = 0x20000
	subl	%r12d, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%rbp, %rsi
	callq	cli_readn
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB4_31
# BB#58:                                #   in Loop: Header=BB4_30 Depth=2
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.LBB4_60
# BB#59:                                #   in Loop: Header=BB4_30 Depth=2
	movl	%ebx, %ecx
	shrl	$12, %ecx
	addq	%rcx, (%rax)
.LBB4_60:                               #   in Loop: Header=BB4_30 Depth=2
	addl	%ebx, %r12d
	cmpb	$0, 28(%rsp)            # 1-byte Folded Reload
	je	.LBB4_65
# BB#61:                                #   in Loop: Header=BB4_30 Depth=2
	movl	%r13d, %r14d
	jmp	.LBB4_74
	.p2align	4, 0x90
.LBB4_65:                               #   in Loop: Header=BB4_30 Depth=2
	movl	%r13d, 20(%rsp)         # 4-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	leal	(%r12,%rax), %r15d
	movq	104(%rsp), %r13         # 8-byte Reload
	cmpb	$0, 2(%r13)
	jne	.LBB4_67
# BB#66:                                #   in Loop: Header=BB4_30 Depth=2
	movq	%rdx, %r14
	movq	(%r14), %rdx
	subq	$8, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movq	%r13, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	56(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	callq	cli_bm_scanbuff
	movq	%r14, %rdx
	addq	$16, %rsp
.Lcfi69:
	.cfi_adjust_cfa_offset -16
	cmpl	$1, %eax
	je	.LBB4_68
.LBB4_67:                               #   in Loop: Header=BB4_30 Depth=2
	movq	%rdx, %r14
	movq	(%r14), %rdx
	movzbl	24(%rsp), %r9d          # 1-byte Folded Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movq	%r13, %rcx
	leaq	120(%rsp), %r8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_scanbuff
	movq	%r14, %rdx
	addq	$32, %rsp
.Lcfi74:
	.cfi_adjust_cfa_offset -32
	cmpl	$1, %eax
	je	.LBB4_68
# BB#72:                                #   in Loop: Header=BB4_30 Depth=2
	cmpl	$499, %eax              # imm = 0x1F3
	movq	%rdx, %rcx
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	%edx, %r14d
	cmovgl	%eax, %r14d
	cmpb	$0, 24(%rsp)            # 1-byte Folded Reload
	cmovel	%edx, %r14d
	cmpl	%edx, %eax
	cmovlel	%edx, %r14d
	movq	24(%rcx), %rax
	cmpq	$0, 24(%rax)
	je	.LBB4_74
# BB#73:                                #   in Loop: Header=BB4_30 Depth=2
	movslq	%ebx, %rdx
	leaq	176(%rsp), %rdi
	movq	%rbp, %rsi
	callq	cli_md5_update
.LBB4_74:                               #   in Loop: Header=BB4_30 Depth=2
	cmpl	$131072, %r12d          # imm = 0x20000
	movl	%r14d, %r13d
	jne	.LBB4_30
.LBB4_75:                               # %.us-lcssa191.us
                                        #   in Loop: Header=BB4_28 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	callq	memmove
	movq	%rbp, %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmpq	72(%rsp), %rsi          # 8-byte Folded Reload
	cmoveq	%rbx, %rsi
	movl	$0, %eax
	cmovel	92(%rsp), %eax          # 4-byte Folded Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	leal	131072(%rdi,%rax), %edi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movl	%r14d, %r13d
	movq	64(%rsp), %rbp          # 8-byte Reload
.LBB4_28:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_30 Depth 2
                                        #     Child Loop BB4_43 Depth 2
	cmpq	%rbx, %rsi
	movl	$0, %eax
	cmovel	%edx, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	testq	%rbp, %rbp
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	je	.LBB4_29
# BB#42:                                # %.outer.split.us.preheader
                                        #   in Loop: Header=BB4_28 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_43:                               # %.outer.split.us
                                        #   Parent Loop BB4_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r13d, 20(%rsp)         # 4-byte Spill
	movl	%r12d, %r13d
	addq	72(%rsp), %r13          # 8-byte Folded Reload
	movl	$131072, %edx           # imm = 0x20000
	subl	%r12d, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r13, %rsi
	callq	cli_readn
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jle	.LBB4_44
# BB#45:                                #   in Loop: Header=BB4_43 Depth=2
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.LBB4_47
# BB#46:                                #   in Loop: Header=BB4_43 Depth=2
	movl	%r15d, %ecx
	shrl	$12, %ecx
	addq	%rcx, (%rax)
.LBB4_47:                               #   in Loop: Header=BB4_43 Depth=2
	addl	%r15d, %r12d
	movq	112(%rsp), %rax         # 8-byte Reload
	leal	(%r12,%rax), %ebx
	movq	64(%rsp), %r14          # 8-byte Reload
	cmpb	$0, 2(%r14)
	jne	.LBB4_49
# BB#48:                                #   in Loop: Header=BB4_43 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdx
	subq	$8, %rsp
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	movq	%r14, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	56(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	callq	cli_bm_scanbuff
	addq	$16, %rsp
.Lcfi77:
	.cfi_adjust_cfa_offset -16
	cmpl	$1, %eax
	je	.LBB4_62
.LBB4_49:                               #   in Loop: Header=BB4_43 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdx
	movzbl	24(%rsp), %ebp          # 1-byte Folded Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	movq	%r14, %rcx
	leaq	136(%rsp), %r8
	movl	%ebp, %r9d
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_scanbuff
	addq	$32, %rsp
.Lcfi82:
	.cfi_adjust_cfa_offset -32
	cmpl	$1, %eax
	je	.LBB4_62
# BB#50:                                #   in Loop: Header=BB4_43 Depth=2
	cmpb	$0, 28(%rsp)            # 1-byte Folded Reload
	je	.LBB4_52
# BB#51:                                #   in Loop: Header=BB4_43 Depth=2
	movl	20(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB4_57
	.p2align	4, 0x90
.LBB4_52:                               #   in Loop: Header=BB4_43 Depth=2
	movq	104(%rsp), %r14         # 8-byte Reload
	cmpb	$0, 2(%r14)
	jne	.LBB4_54
# BB#53:                                #   in Loop: Header=BB4_43 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdx
	subq	$8, %rsp
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	movq	%r14, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	56(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	callq	cli_bm_scanbuff
	movq	16(%rsp), %rdx          # 8-byte Reload
	addq	$16, %rsp
.Lcfi85:
	.cfi_adjust_cfa_offset -16
	cmpl	$1, %eax
	je	.LBB4_68
.LBB4_54:                               #   in Loop: Header=BB4_43 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdx
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	movq	%r14, %rcx
	leaq	120(%rsp), %r8
	movl	%ebp, %r9d
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_scanbuff
	movq	32(%rsp), %rdx          # 8-byte Reload
	addq	$32, %rsp
.Lcfi90:
	.cfi_adjust_cfa_offset -32
	cmpl	$1, %eax
	je	.LBB4_68
# BB#55:                                #   in Loop: Header=BB4_43 Depth=2
	cmpl	$499, %eax              # imm = 0x1F3
	movq	%rdx, %rcx
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	%edx, %r14d
	cmovgl	%eax, %r14d
	cmpb	$0, 24(%rsp)            # 1-byte Folded Reload
	cmovel	%edx, %r14d
	cmpl	%edx, %eax
	cmovlel	%edx, %r14d
	movq	24(%rcx), %rax
	cmpq	$0, 24(%rax)
	je	.LBB4_57
# BB#56:                                #   in Loop: Header=BB4_43 Depth=2
	movslq	%r15d, %rdx
	leaq	176(%rsp), %rdi
	movq	%r13, %rsi
	callq	cli_md5_update
.LBB4_57:                               #   in Loop: Header=BB4_43 Depth=2
	cmpl	$131072, %r12d          # imm = 0x20000
	movl	%r14d, %r13d
	jne	.LBB4_43
	jmp	.LBB4_75
.LBB4_62:                               # %.thread
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	cmpb	$0, 28(%rsp)            # 1-byte Folded Reload
	jne	.LBB4_64
# BB#63:
	leaq	120(%rsp), %rdi
	callq	cli_ac_freedata
.LBB4_64:
	leaq	136(%rsp), %rdi
	callq	cli_ac_freedata
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebx, %edi
	callq	lseek
	movq	(%rsp), %rax            # 8-byte Reload
	movq	24(%rax), %rsi
	jmp	.LBB4_71
.LBB4_44:
	movq	(%rsp), %r14            # 8-byte Reload
	movl	20(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB4_32
.LBB4_68:                               # %.thread185
	movq	%rdx, %rbp
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	leaq	120(%rsp), %rdi
	callq	cli_ac_freedata
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB4_70
# BB#69:
	leaq	136(%rsp), %rdi
	callq	cli_ac_freedata
.LBB4_70:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebx, %edi
	callq	lseek
	movq	24(%rbp), %rsi
.LBB4_71:
	movl	%ebx, %edi
	callq	cli_checkfp
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	sete	%al
	jmp	.LBB4_83
.LBB4_31:
	movq	(%rsp), %r14            # 8-byte Reload
.LBB4_32:                               # %.us-lcssa.us
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	28(%rsp), %ebx          # 4-byte Reload
	testb	%bl, %bl
	movq	64(%rsp), %rbp          # 8-byte Reload
	jne	.LBB4_34
# BB#33:
	leaq	120(%rsp), %rdi
	callq	cli_ac_freedata
.LBB4_34:
	testq	%rbp, %rbp
	je	.LBB4_36
# BB#35:
	leaq	136(%rsp), %rdi
	callq	cli_ac_freedata
.LBB4_36:
	testb	%bl, %bl
	je	.LBB4_37
.LBB4_82:                               # %cli_vermd5.exit.thread
	xorl	%eax, %eax
	cmpb	$0, 24(%rsp)            # 1-byte Folded Reload
	cmovnel	%r13d, %eax
.LBB4_83:
	addq	$472, %rsp              # imm = 0x1D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_37:
	movq	24(%r14), %rax
	cmpq	$0, 24(%rax)
	je	.LBB4_82
# BB#38:
	leaq	160(%rsp), %rdi
	leaq	176(%rsp), %rsi
	callq	cli_md5_final
	movq	24(%r14), %rax
	movq	24(%rax), %rax
	movzbl	160(%rsp), %ecx
	movq	(%rax,%rcx,8), %rbp
	testq	%rbp, %rbp
	je	.LBB4_82
# BB#39:                                # %.lr.ph.i.preheader
	leaq	160(%rsp), %rbx
.LBB4_40:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	$16, %edx
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB4_76
# BB#41:                                #   in Loop: Header=BB4_40 Depth=1
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_40
	jmp	.LBB4_82
.LBB4_76:                               # %cli_vermd5.exit
	cmpw	$0, 20(%rbp)
	jne	.LBB4_82
# BB#77:
	leaq	328(%rsp), %rdx
	movl	$1, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	__fxstat
	movl	%eax, %ecx
	movl	$-123, %eax
	testl	%ecx, %ecx
	jne	.LBB4_83
# BB#78:
	movl	376(%rsp), %eax
	cmpl	16(%rbp), %eax
	jne	.LBB4_81
# BB#79:
	movq	(%r14), %rcx
	movl	$1, %eax
	testq	%rcx, %rcx
	je	.LBB4_83
# BB#80:
	movq	(%rbp), %rdx
	movq	%rdx, (%rcx)
	jmp	.LBB4_83
.LBB4_81:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB4_82
.Lfunc_end4:
	.size	cli_scandesc, .Lfunc_end4-cli_scandesc
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_12
	.quad	.LBB4_11
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_7
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_10
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_8
	.quad	.LBB4_9

	.text
	.p2align	4, 0x90
	.type	cli_checkfp,@function
cli_checkfp:                            # @cli_checkfp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi95:
	.cfi_def_cfa_offset 192
.Lcfi96:
	.cfi_offset %rbx, -40
.Lcfi97:
	.cfi_offset %r14, -32
.Lcfi98:
	.cfi_offset %r15, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movl	%edi, %r15d
	xorl	%r14d, %r14d
	cmpq	$0, 24(%rbp)
	je	.LBB5_14
# BB#1:
	movl	%r15d, %edi
	callq	cli_md5digest
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_2
# BB#3:
	movq	24(%rbp), %rax
	movzbl	(%rbx), %ecx
	movq	(%rax,%rcx,8), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_5
	jmp	.LBB5_13
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_5 Depth=1
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB5_13
.LBB5_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	$16, %edx
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB5_6
# BB#7:                                 # %cli_vermd5.exit
	cmpw	$0, 20(%rbp)
	je	.LBB5_13
# BB#8:
	leaq	8(%rsp), %rdx
	movl	$1, %edi
	movl	%r15d, %esi
	callq	__fxstat
	movl	$-123, %ecx
	testl	%eax, %eax
	jne	.LBB5_11
# BB#9:
	movl	56(%rsp), %eax
	cmpl	16(%rbp), %eax
	jne	.LBB5_12
# BB#10:
	movq	(%rbp), %rsi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
	movl	$1, %ecx
.LBB5_11:                               # %.thread
	movl	%ecx, %r14d
	jmp	.LBB5_14
.LBB5_2:
	xorl	%r14d, %r14d
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB5_14
.LBB5_12:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB5_13:                               # %cli_vermd5.exit.thread
	movq	%rbx, %rdi
	callq	free
.LBB5_14:
	movl	%r14d, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	cli_checkfp, .Lfunc_end5-cli_checkfp
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cli_scanbuff: engine == NULL\n"
	.size	.L.str, 30

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"EP"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Invalid descriptor\n"
	.size	.L.str.2, 20

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"EP+"
	.size	.L.str.3, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"EP-"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SL"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"SL+%u"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"S%u+%u"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"EOF-"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cli_validatesig: Can't calculate offset for signature %s\n"
	.size	.L.str.9, 58

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Signature offset: %lu, expected: [%lu..%lu] (%s)\n"
	.size	.L.str.10, 50

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Signature offset: %lu, expected: %lu (%s)\n"
	.size	.L.str.11, 43

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"cli_scandesc: engine == NULL\n"
	.size	.L.str.12, 30

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"cli_scandesc(): unable to cli_calloc(%u)\n"
	.size	.L.str.13, 42

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Detected false positive MD5 match. Please report.\n"
	.size	.L.str.14, 51

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"cli_checkfp(): Can't generate MD5 checksum\n"
	.size	.L.str.15, 44

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Eliminated false positive match (fp sig: %s)\n"
	.size	.L.str.16, 46


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
