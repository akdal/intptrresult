	.text
	.file	"Pbkdf2HmacSha1.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_ZN7NCrypto5NSha110Pbkdf2HmacEPKhmS2_mjPhm
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha110Pbkdf2HmacEPKhmS2_mjPhm,@function
_ZN7NCrypto5NSha110Pbkdf2HmacEPKhmS2_mjPhm: # @_ZN7NCrypto5NSha110Pbkdf2HmacEPKhmS2_mjPhm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$472, %rsp              # imm = 0x1D8
.Lcfi6:
	.cfi_def_cfa_offset 528
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movl	%r8d, 24(%rsp)          # 4-byte Spill
	movq	%rcx, %r15
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, %rax
	movq	%rdi, %rcx
	movq	528(%rsp), %r14
	leaq	56(%rsp), %rdi
	movq	%rcx, %rsi
	movq	%rax, %rdx
	callq	_ZN7NCrypto5NSha15CHmac6SetKeyEPKhm
	movq	%r14, %rdi
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#1:                                 # %.lr.ph53
	cmpl	$1, 24(%rsp)            # 4-byte Folded Reload
	movq	%r15, 32(%rsp)          # 8-byte Spill
	jbe	.LBB0_2
# BB#10:                                # %.lr.ph53.split.us.preheader
	movl	$1, %eax
	leaq	264(%rsp), %r13
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph53.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_13 Depth 2
                                        #     Child Loop BB0_15 Depth 2
                                        #     Child Loop BB0_20 Depth 2
                                        #       Child Loop BB0_21 Depth 3
                                        #     Child Loop BB0_17 Depth 2
	movl	$208, %edx
	movq	%rdi, %rbp
	movq	%r13, %rdi
	leaq	56(%rsp), %rsi
	movl	%eax, %r12d
	callq	memcpy
	movq	%r13, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$0, 16(%rsp)
	movl	%r12d, %ecx
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, (%rsp)
	movl	%ecx, %eax
	shrl	$16, %eax
	movb	%al, 1(%rsp)
	movb	%ch, 2(%rsp)  # NOREX
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movb	%cl, 3(%rsp)
	cmpq	$20, %rbp
	movl	$20, %r12d
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	cmovbq	%rbp, %r12
	movl	$4, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movl	$20, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha15CHmac5FinalEPhm
	testq	%r12, %r12
	movl	24(%rsp), %ebp          # 4-byte Reload
	je	.LBB0_17
# BB#12:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB0_11 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	notq	%rax
	cmpq	$-21, %rax
	movq	$-21, %rcx
	cmovbeq	%rcx, %rax
	movq	$-2, %rsi
	subq	%rax, %rsi
	movq	%r12, %rdx
	xorl	%eax, %eax
	andq	$7, %rdx
	je	.LBB0_14
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph.us.prol
                                        #   Parent Loop BB0_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rsp,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB0_13
.LBB0_14:                               # %.lr.ph.us.prol.loopexit
                                        #   in Loop: Header=BB0_11 Depth=1
	cmpq	$7, %rsi
	movl	24(%rsp), %r15d         # 4-byte Reload
	jb	.LBB0_20
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph.us
                                        #   Parent Loop BB0_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rsp,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	movzbl	1(%rsp,%rax), %ecx
	movb	%cl, 1(%rbx,%rax)
	movzbl	2(%rsp,%rax), %ecx
	movb	%cl, 2(%rbx,%rax)
	movzbl	3(%rsp,%rax), %ecx
	movb	%cl, 3(%rbx,%rax)
	movzbl	4(%rsp,%rax), %ecx
	movb	%cl, 4(%rbx,%rax)
	movzbl	5(%rsp,%rax), %ecx
	movb	%cl, 5(%rbx,%rax)
	movzbl	6(%rsp,%rax), %ecx
	movb	%cl, 6(%rbx,%rax)
	movzbl	7(%rsp,%rax), %ecx
	movb	%cl, 7(%rbx,%rax)
	addq	$8, %rax
	cmpq	%r12, %rax
	jb	.LBB0_15
# BB#16:                                #   in Loop: Header=BB0_11 Depth=1
	movl	24(%rsp), %r15d         # 4-byte Reload
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph43.us.us
                                        #   Parent Loop BB0_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_21 Depth 3
	movl	$208, %edx
	movq	%r13, %rdi
	leaq	56(%rsp), %rsi
	callq	memcpy
	movl	$20, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movl	$20, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha15CHmac5FinalEPhm
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_21:                               #   Parent Loop BB0_11 Depth=1
                                        #     Parent Loop BB0_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rsp,%rax), %ecx
	xorb	%cl, (%rbx,%rax)
	incq	%rax
	cmpq	%r12, %rax
	jb	.LBB0_21
# BB#19:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB0_20 Depth=2
	decl	%r15d
	cmpl	$1, %r15d
	ja	.LBB0_20
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph45..lr.ph45.split_crit_edge.us
                                        #   Parent Loop BB0_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$208, %edx
	movq	%r13, %rdi
	leaq	56(%rsp), %rsi
	callq	memcpy
	movl	$20, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movl	$20, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha15CHmac5FinalEPhm
	decl	%ebp
	cmpl	$1, %ebp
	ja	.LBB0_17
.LBB0_18:                               # %._crit_edge46.us
                                        #   in Loop: Header=BB0_11 Depth=1
	addq	%r12, %rbx
	movl	28(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	subq	%r12, %rdi
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB0_11
	jmp	.LBB0_9
.LBB0_2:                                # %.lr.ph53.split.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph53.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
                                        #     Child Loop BB0_7 Depth 2
	movl	$208, %edx
	movq	%rdi, %r14
	leaq	264(%rsp), %r12
	movq	%r12, %rdi
	leaq	56(%rsp), %rsi
	callq	memcpy
	movq	%r12, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$0, 16(%rsp)
	movl	%ebp, %ecx
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, (%rsp)
	movl	%ecx, %eax
	shrl	$16, %eax
	movb	%al, 1(%rsp)
	movb	%ch, 2(%rsp)  # NOREX
	movb	%cl, 3(%rsp)
	cmpq	$20, %r14
	movl	$20, %r13d
	cmovbq	%r14, %r13
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rsp, %r15
	movq	%r15, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movl	$20, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_ZN7NCrypto5NSha15CHmac5FinalEPhm
	movq	%r14, %rdi
	testq	%r13, %r13
	je	.LBB0_8
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rdi, %rax
	notq	%rax
	cmpq	$-21, %rax
	movq	$-21, %rcx
	cmovbeq	%rcx, %rax
	movq	$-2, %rsi
	subq	%rax, %rsi
	movq	%r13, %rdx
	xorl	%eax, %eax
	andq	$7, %rdx
	je	.LBB0_6
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rsp,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB0_5
.LBB0_6:                                # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$7, %rsi
	jb	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rsp,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	movzbl	1(%rsp,%rax), %ecx
	movb	%cl, 1(%rbx,%rax)
	movzbl	2(%rsp,%rax), %ecx
	movb	%cl, 2(%rbx,%rax)
	movzbl	3(%rsp,%rax), %ecx
	movb	%cl, 3(%rbx,%rax)
	movzbl	4(%rsp,%rax), %ecx
	movb	%cl, 4(%rbx,%rax)
	movzbl	5(%rsp,%rax), %ecx
	movb	%cl, 5(%rbx,%rax)
	movzbl	6(%rsp,%rax), %ecx
	movb	%cl, 6(%rbx,%rax)
	movzbl	7(%rsp,%rax), %ecx
	movb	%cl, 7(%rbx,%rax)
	addq	$8, %rax
	cmpq	%r13, %rax
	jb	.LBB0_7
.LBB0_8:                                # %.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	addq	%r13, %rbx
	incl	%ebp
	subq	%r13, %rdi
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB0_3
.LBB0_9:                                # %._crit_edge54
	addq	$472, %rsp              # imm = 0x1D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN7NCrypto5NSha110Pbkdf2HmacEPKhmS2_mjPhm, .Lfunc_end0-_ZN7NCrypto5NSha110Pbkdf2HmacEPKhmS2_mjPhm
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16
	.text
	.globl	_ZN7NCrypto5NSha112Pbkdf2Hmac32EPKhmPKjmjPjm
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha112Pbkdf2Hmac32EPKhmPKjmjPjm,@function
_ZN7NCrypto5NSha112Pbkdf2Hmac32EPKhmPKjmjPjm: # @_ZN7NCrypto5NSha112Pbkdf2Hmac32EPKhmPKjmjPjm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$488, %rsp              # imm = 0x1E8
.Lcfi19:
	.cfi_def_cfa_offset 544
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, %rax
	movq	%rdi, %rcx
	movq	544(%rsp), %r15
	leaq	72(%rsp), %rdi
	movq	%rcx, %rsi
	movq	%rax, %rdx
	callq	_ZN7NCrypto5NSha17CHmac326SetKeyEPKhm
	testq	%r15, %r15
	je	.LBB1_5
# BB#1:                                 # %.lr.ph32
	decl	12(%rsp)                # 4-byte Folded Spill
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movl	$208, %edx
	leaq	280(%rsp), %rbx
	movq	%rbx, %rdi
	leaq	72(%rsp), %r14
	movq	%r14, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	callq	_ZN7NCrypto5NSha110CContext326UpdateEPKjm
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$0, 32(%rsp)
	movl	%r12d, 16(%rsp)
	cmpq	$5, %r15
	movl	$5, %r13d
	cmovbq	%r15, %r13
	movl	$1, %edx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rbp
	movq	%rbp, %rsi
	callq	_ZN7NCrypto5NSha110CContext326UpdateEPKjm
	movl	$5, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN7NCrypto5NSha17CHmac325FinalEPjm
	movl	$208, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbp, %rsi
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	_ZN7NCrypto5NSha17CHmac3216GetLoopXorDigestEPjj
	testq	%r13, %r13
	je	.LBB1_4
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	16(%rsp), %eax
	movl	%eax, (%rbx)
	cmpq	$1, %r13
	jbe	.LBB1_4
# BB#6:                                 # %.lr.ph.1
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	20(%rsp), %eax
	movl	%eax, 4(%rbx)
	cmpq	$2, %r13
	je	.LBB1_4
# BB#7:                                 # %.lr.ph.2
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	24(%rsp), %eax
	movl	%eax, 8(%rbx)
	cmpq	$4, %r13
	jb	.LBB1_4
# BB#8:                                 # %.lr.ph.3
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	28(%rsp), %eax
	movl	%eax, 12(%rbx)
	je	.LBB1_4
# BB#9:                                 # %.lr.ph.4
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	32(%rsp), %eax
	movl	%eax, 16(%rbx)
	.p2align	4, 0x90
.LBB1_4:                                # %._crit_edge
                                        #   in Loop: Header=BB1_2 Depth=1
	leaq	(%rbx,%r13,4), %rbx
	incl	%r12d
	subq	%r13, %r15
	jne	.LBB1_2
.LBB1_5:                                # %._crit_edge33
	addq	$488, %rsp              # imm = 0x1E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN7NCrypto5NSha112Pbkdf2Hmac32EPKhmPKjmjPjm, .Lfunc_end1-_ZN7NCrypto5NSha112Pbkdf2Hmac32EPKhmPKjmjPjm
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
