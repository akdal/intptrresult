	.text
	.file	"access.bc"
	.globl	_ZN11AccessFlags8toStringEPc
	.p2align	4, 0x90
	.type	_ZN11AccessFlags8toStringEPc,@function
_ZN11AccessFlags8toStringEPc:           # @_ZN11AccessFlags8toStringEPc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movb	$0, (%r14)
	movw	(%rdi), %bp
	testw	%bp, %bp
	je	.LBB0_5
# BB#1:                                 # %.lr.ph.preheader
	movl	$flag2str, %ebx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %bpl
	je	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	callq	strcat
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	andl	$65534, %ebp            # imm = 0xFFFE
	shrl	%ebp
	addq	$8, %rbx
	testw	%bp, %bp
	jne	.LBB0_2
.LBB0_5:                                # %._crit_edge
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN11AccessFlags8toStringEPc, .Lfunc_end0-_ZN11AccessFlags8toStringEPc
	.cfi_endproc

	.globl	_ZN11AccessFlags6strlenEv
	.p2align	4, 0x90
	.type	_ZN11AccessFlags6strlenEv,@function
_ZN11AccessFlags6strlenEv:              # @_ZN11AccessFlags6strlenEv
	.cfi_startproc
# BB#0:
	movw	(%rdi), %cx
	testw	%cx, %cx
	je	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	movl	$flag2strlen, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %cl
	je	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	addl	(%rdx), %eax
.LBB1_5:                                #   in Loop: Header=BB1_3 Depth=1
	andl	$65534, %ecx            # imm = 0xFFFE
	shrl	%ecx
	addq	$4, %rdx
	testw	%cx, %cx
	jne	.LBB1_3
# BB#6:                                 # %._crit_edge
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.LBB1_1:
	xorl	%eax, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.Lfunc_end1:
	.size	_ZN11AccessFlags6strlenEv, .Lfunc_end1-_ZN11AccessFlags6strlenEv
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"public "
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"private "
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"protected "
	.size	.L.str.2, 11

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"static "
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"final "
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"synchronized "
	.size	.L.str.5, 14

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"threadsafe "
	.size	.L.str.6, 12

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"transient "
	.size	.L.str.7, 11

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"native "
	.size	.L.str.8, 8

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"interface "
	.size	.L.str.9, 11

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"abstract "
	.size	.L.str.10, 10

	.type	flag2str,@object        # @flag2str
	.data
	.globl	flag2str
	.p2align	4
flag2str:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	.L.str.9
	.quad	.L.str.10
	.size	flag2str, 88

	.type	flag2strlen,@object     # @flag2strlen
	.globl	flag2strlen
	.p2align	4
flag2strlen:
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	10                      # 0xa
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	13                      # 0xd
	.long	11                      # 0xb
	.long	10                      # 0xa
	.long	7                       # 0x7
	.long	10                      # 0xa
	.long	9                       # 0x9
	.size	flag2strlen, 44


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
