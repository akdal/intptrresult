	.text
	.file	"BraIA64.bc"
	.globl	IA64_Convert
	.p2align	4, 0x90
	.type	IA64_Convert,@function
IA64_Convert:                           # @IA64_Convert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, %r13
	movq	%rdi, %rax
	cmpq	$16, %r13
	jae	.LBB0_2
# BB#1:
	xorl	%r10d, %r10d
	jmp	.LBB0_15
.LBB0_2:
	movabsq	$687194767360, %r11     # imm = 0xA000000000
	addq	$-16, %r13
	xorl	%r10d, %r10d
	testl	%ecx, %ecx
	je	.LBB0_9
	.p2align	4, 0x90
.LBB0_3:                                # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	movzbl	(%rax,%r10), %ecx
	andl	$31, %ecx
	movzbl	kBranchTable(%rcx), %r14d
	leal	(%r10,%rdx), %r8d
	movl	$5, %ecx
	movl	$5, %r9d
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-5(%rcx), %esi
	btl	%esi, %r14d
	jae	.LBB0_7
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=2
	movl	%r9d, %edi
	shrl	$3, %edi
	addq	%r10, %rdi
	movzbl	(%rax,%rdi), %ebx
	movzbl	1(%rax,%rdi), %esi
	shlq	$8, %rsi
	orq	%rbx, %rsi
	movzbl	2(%rax,%rdi), %ebx
	shlq	$16, %rbx
	orq	%rsi, %rbx
	movzbl	3(%rax,%rdi), %esi
	shlq	$24, %rsi
	orq	%rbx, %rsi
	movzbl	4(%rax,%rdi), %ebx
	shlq	$32, %rbx
	orq	%rsi, %rbx
	movzbl	5(%rax,%rdi), %r12d
	shlq	$40, %r12
	orq	%rbx, %r12
	movq	%r12, %rsi
	shrq	%cl, %rsi
	movq	%rsi, %rbx
	movabsq	$2061584305664, %rbp    # imm = 0x1E000000E00
	andq	%rbp, %rbx
	cmpq	%r11, %rbx
	jne	.LBB0_7
# BB#6:                                 # %.loopexit.loopexit108
                                        #   in Loop: Header=BB0_4 Depth=2
	movq	%rsi, %r15
	shrq	$13, %r15
	andl	$1048575, %r15d         # imm = 0xFFFFF
	movq	%rsi, %rbx
	shrq	$16, %rbx
	andl	$1048576, %ebx          # imm = 0x100000
	orl	%r15d, %ebx
	shll	$4, %ebx
	addl	%r8d, %ebx
	shrl	$4, %ebx
	movabsq	$-77309403137, %rbp     # imm = 0xFFFFFFEE00001FFF
	andq	%rbp, %rsi
	movl	%ebx, %ebp
	andl	$1048575, %ebp          # imm = 0xFFFFF
	shlq	$13, %rbp
	orq	%rsi, %rbp
	andl	$1048576, %ebx          # imm = 0x100000
	shlq	$16, %rbx
	orq	%rbp, %rbx
	movl	$1, %esi
	shll	%cl, %esi
	decl	%esi
	movslq	%esi, %rsi
	andq	%rsi, %r12
	shlq	%cl, %rbx
	orq	%r12, %rbx
	movb	%bl, (%rax,%rdi)
	movb	%bh, 1(%rax,%rdi)  # NOREX
	movq	%rbx, %rsi
	shrq	$16, %rsi
	movb	%sil, 2(%rax,%rdi)
	movq	%rbx, %rsi
	shrq	$24, %rsi
	movb	%sil, 3(%rax,%rdi)
	movq	%rbx, %rsi
	shrq	$32, %rsi
	movb	%sil, 4(%rax,%rdi)
	shrq	$40, %rbx
	movb	%bl, 5(%rax,%rdi)
.LBB0_7:                                # %.loopexit
                                        #   in Loop: Header=BB0_4 Depth=2
	addq	$41, %r9
	incq	%rcx
	cmpl	$128, %r9d
	jne	.LBB0_4
# BB#8:                                 #   in Loop: Header=BB0_3 Depth=1
	addq	$16, %r10
	cmpq	%r13, %r10
	jbe	.LBB0_3
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_9:                                # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_10 Depth 2
	movq	%r13, %rdi
	movzbl	(%rax,%r10), %ecx
	andl	$31, %ecx
	movzbl	kBranchTable(%rcx), %r15d
	movq	%rdx, %rsi
	leal	(%r10,%rdx), %r14d
	xorl	%r9d, %r9d
	movl	$5, %r12d
	.p2align	4, 0x90
.LBB0_10:                               #   Parent Loop BB0_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	btl	%r9d, %r15d
	jae	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_10 Depth=2
	movl	%r12d, %ebp
	shrl	$3, %ebp
	addq	%r10, %rbp
	movzbl	(%rax,%rbp), %ecx
	movzbl	1(%rax,%rbp), %edx
	shlq	$8, %rdx
	orq	%rcx, %rdx
	movzbl	2(%rax,%rbp), %ecx
	shlq	$16, %rcx
	orq	%rdx, %rcx
	movzbl	3(%rax,%rbp), %edx
	shlq	$24, %rdx
	orq	%rcx, %rdx
	movzbl	4(%rax,%rbp), %ecx
	shlq	$32, %rcx
	orq	%rdx, %rcx
	movzbl	5(%rax,%rbp), %r13d
	shlq	$40, %r13
	orq	%rcx, %r13
	leaq	5(%r9), %r8
	movq	%r13, %rdx
	movl	%r8d, %ecx
	shrq	%cl, %rdx
	movq	%rdx, %rcx
	movabsq	$2061584305664, %rbx    # imm = 0x1E000000E00
	andq	%rbx, %rcx
	cmpq	%r11, %rcx
	jne	.LBB0_13
# BB#12:                                # %.loopexit.us.loopexit101
                                        #   in Loop: Header=BB0_10 Depth=2
	movq	%rdx, %rcx
	shrq	$13, %rcx
	andl	$1048575, %ecx          # imm = 0xFFFFF
	movq	%rdx, %rbx
	shrq	$16, %rbx
	andl	$1048576, %ebx          # imm = 0x100000
	orl	%ecx, %ebx
	shll	$4, %ebx
	subl	%r14d, %ebx
	shrl	$4, %ebx
	movabsq	$-77309403137, %rcx     # imm = 0xFFFFFFEE00001FFF
	andq	%rcx, %rdx
	movl	%ebx, %ecx
	andl	$1048575, %ecx          # imm = 0xFFFFF
	shlq	$13, %rcx
	orq	%rdx, %rcx
	andl	$1048576, %ebx          # imm = 0x100000
	shlq	$16, %rbx
	orq	%rcx, %rbx
	leal	5(%r9), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	decl	%edx
	movslq	%edx, %rcx
	andq	%rcx, %r13
	movl	%r8d, %ecx
	shlq	%cl, %rbx
	orq	%r13, %rbx
	movb	%bl, (%rax,%rbp)
	movb	%bh, 1(%rax,%rbp)  # NOREX
	movq	%rbx, %rcx
	shrq	$16, %rcx
	movb	%cl, 2(%rax,%rbp)
	movq	%rbx, %rcx
	shrq	$24, %rcx
	movb	%cl, 3(%rax,%rbp)
	movq	%rbx, %rcx
	shrq	$32, %rcx
	movb	%cl, 4(%rax,%rbp)
	shrq	$40, %rbx
	movb	%bl, 5(%rax,%rbp)
.LBB0_13:                               # %.loopexit.us
                                        #   in Loop: Header=BB0_10 Depth=2
	addq	$41, %r12
	incq	%r9
	cmpl	$3, %r9d
	jne	.LBB0_10
# BB#14:                                #   in Loop: Header=BB0_9 Depth=1
	addq	$16, %r10
	movq	%rdi, %r13
	cmpq	%r13, %r10
	movq	%rsi, %rdx
	jbe	.LBB0_9
.LBB0_15:                               # %.loopexit90
	movq	%r10, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	IA64_Convert, .Lfunc_end0-IA64_Convert
	.cfi_endproc

	.type	kBranchTable,@object    # @kBranchTable
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
kBranchTable:
	.asciz	"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\004\004\006\006\000\000\007\007\004\004\000\000\004\004\000"
	.size	kBranchTable, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
