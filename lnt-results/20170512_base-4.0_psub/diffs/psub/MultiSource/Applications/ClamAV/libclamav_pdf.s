	.text
	.file	"libclamav_pdf.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_1:
	.long	85                      # 0x55
	.long	85                      # 0x55
	.long	85                      # 0x55
	.long	85                      # 0x55
.LCPI0_2:
	.long	1904149089              # 0x717f0261
	.long	1904149089              # 0x717f0261
	.long	1904149089              # 0x717f0261
	.long	1904149089              # 0x717f0261
.LCPI0_3:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
.LCPI0_4:
	.quad	24                      # 0x18
	.quad	16                      # 0x10
.LCPI0_5:
	.quad	255                     # 0xff
	.quad	255                     # 0xff
	.text
	.globl	cli_pdf
	.p2align	4, 0x90
	.type	cli_pdf,@function
cli_pdf:                                # @cli_pdf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$632, %rsp              # imm = 0x278
.Lcfi6:
	.cfi_def_cfa_offset 688
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebx
	movq	%rdi, %r13
	xorl	%ebp, %ebp
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_dbgmsg
	leaq	488(%rsp), %rdx
	movl	$1, %edi
	movl	%ebx, %esi
	callq	__fxstat
	testl	%eax, %eax
	js	.LBB0_13
# BB#1:
	movq	536(%rsp), %r15
	testq	%r15, %r15
	je	.LBB0_14
# BB#2:
	movl	$-124, %ebp
	cmpq	$8, %r15
	jl	.LBB0_14
# BB#3:
	xorl	%edi, %edi
	movl	$1, %edx
	movl	$2, %ecx
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	movl	%ebx, %r8d
	callq	mmap
	movq	%rax, %r12
	cmpq	$-1, %r12
	je	.LBB0_15
# BB#4:
	movq	%r13, 184(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	movq	%r12, %rbp
	je	.LBB0_6
# BB#5:
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	memcpy
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	munmap
	movq	%r13, %rbp
.LBB0_6:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.2, %esi
	movl	$7, %edx
	movq	%rbp, %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB0_11
# BB#7:
	movq	%r14, 120(%rsp)         # 8-byte Spill
	leaq	-6(%r15), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cmpq	$7, %rax
	jl	.LBB0_11
# BB#8:                                 # %.lr.ph732.preheader
	leaq	6(%rbp), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	-13(%r15), %rbx
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph732
                                        # =>This Inner Loop Header: Depth=1
	leaq	7(%rbp,%rbx), %r14
	movl	$.L.str.3, %esi
	movl	$5, %edx
	movq	%r14, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_18
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	leaq	-1(%rbx), %rax
	addq	$6, %rbx
	cmpq	$6, %rbx
	movq	%rax, %rbx
	jg	.LBB0_9
.LBB0_11:
	testq	%r13, %r13
	je	.LBB0_16
.LBB0_12:
	movq	%r13, %rdi
	callq	free
	movl	$-124, %ebp
	jmp	.LBB0_14
.LBB0_13:
	movl	$-115, %ebp
.LBB0_14:
	movl	%ebp, %eax
	addq	$632, %rsp              # imm = 0x278
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_15:
	movl	$-114, %ebp
	jmp	.LBB0_14
.LBB0_16:
	movq	%r12, %rdi
	movq	%r15, %rsi
.LBB0_17:
	callq	munmap
	movl	$-124, %ebp
	jmp	.LBB0_14
.LBB0_18:
	movq	%r13, 32(%rsp)          # 8-byte Spill
	cmpq	$7, %rbx
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	jl	.LBB0_21
.LBB0_19:                               # %.lr.ph722
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rbp,%rbx), %r13
	movl	$.L.str.4, %esi
	movl	$7, %edx
	movq	%r13, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_22
# BB#20:                                #   in Loop: Header=BB0_19 Depth=1
	decq	%rbx
	cmpq	$6, %rbx
	jg	.LBB0_19
.LBB0_21:
	movq	%rbp, %rax
	movq	%r12, %rbp
	leaq	(%rax,%rbx), %r13
	jmp	.LBB0_23
.LBB0_22:                               # %.lr.ph722.._crit_edge.loopexit_crit_edge
	movq	%r12, %rbp
.LBB0_23:                               # %._crit_edge
	movl	$.L.str.5, %eax
	cmpq	%rax, %r13
	je	.LBB0_284
# BB#24:
	subq	%r13, %r14
	cmpq	$7, %r14
	jae	.LBB0_31
.LBB0_25:                               # %cli_pmemstr.exit.thread.preheader
	movq	%rbp, %rdi
	cmpq	$7, %rbx
	jl	.LBB0_37
# BB#26:
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	120(%rsp), %rbp         # 8-byte Reload
.LBB0_27:                               # %.lr.ph716
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$1717924472, (%rax,%rbx) # imm = 0x66657278
	jne	.LBB0_30
# BB#28:                                #   in Loop: Header=BB0_27 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movzbl	-1(%rax,%rbx), %eax
	cmpb	$10, %al
	je	.LBB0_41
# BB#29:                                #   in Loop: Header=BB0_27 Depth=1
	cmpb	$13, %al
	je	.LBB0_41
.LBB0_30:                               # %cli_pmemstr.exit.thread
                                        #   in Loop: Header=BB0_27 Depth=1
	decq	%rbx
	cmpq	$6, %rbx
	jg	.LBB0_27
	jmp	.LBB0_38
.LBB0_31:
	movl	$.L.str.5, %esi
	movl	$7, %edx
	movq	%r13, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_283
# BB#32:                                # %.preheader.i
	movl	$69, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	memchr
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_25
# BB#33:                                # %.lr.ph.i.preheader
	movq	$-1, %r15
.LBB0_34:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rax
	subq	%r13, %rax
	subq	%rax, %r14
	cmpq	$6, %r14
	jbe	.LBB0_25
# BB#35:                                #   in Loop: Header=BB0_34 Depth=1
	movl	$.L.str.5, %esi
	movl	$7, %edx
	movq	%r12, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_284
# BB#36:                                # %.backedge.i
                                        #   in Loop: Header=BB0_34 Depth=1
	cmpq	%r12, %r13
	leaq	1(%r13), %r13
	cmovneq	%r12, %r13
	movl	$0, %eax
	cmoveq	%r15, %rax
	addq	%rax, %r14
	movl	$69, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	memchr
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB0_34
	jmp	.LBB0_25
.LBB0_37:
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	120(%rsp), %rbp         # 8-byte Reload
.LBB0_38:                               # %cli_pmemstr.exit.thread._crit_edge
	cmpq	$6, %rbx
	jne	.LBB0_41
# BB#39:
	testq	%r13, %r13
	jne	.LBB0_12
# BB#40:
	movq	56(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB0_17
.LBB0_41:                               # %.thread
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	callq	tableCreate
	movq	%rax, %r14
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	cmpq	$7, %rbx
	jl	.LBB0_293
# BB#42:                                # %.lr.ph
	addq	%rbx, 40(%rsp)          # 8-byte Folded Spill
	movq	32(%rbp), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	testq	%r13, %r13
	movq	176(%rsp), %rax         # 8-byte Reload
	cmovneq	%r13, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movd	%eax, %xmm0
	movdqa	%xmm0, 208(%rsp)        # 16-byte Spill
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqa	%xmm0, 192(%rsp)        # 16-byte Spill
	movl	$0, 136(%rsp)           # 4-byte Folded Spill
	movl	$0, 140(%rsp)           # 4-byte Folded Spill
	movl	$0, 92(%rsp)            # 4-byte Folded Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_281
.LBB0_43:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	cmpq	40(%rsp), %rbp          # 8-byte Folded Reload
	je	.LBB0_293
# BB#44:                                #   in Loop: Header=BB0_281 Depth=1
	cmpl	$1717924472, (%rbp)     # imm = 0x66657278
	je	.LBB0_293
# BB#45:                                #   in Loop: Header=BB0_281 Depth=1
	movq	%rbp, %rax
	subq	96(%rsp), %rax          # 8-byte Folded Reload
	subq	%rax, %r12
	movl	$.L.str.8, %esi
	movl	$6, %edx
	movq	%rbp, %rdi
	callq	memcmp
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	testl	%eax, %eax
	je	.LBB0_59
# BB#46:                                #   in Loop: Header=BB0_281 Depth=1
	callq	__ctype_b_loc
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	(%rax), %r14
	movsbq	(%rbp), %rax
	testb	$8, 1(%r14,%rax,2)
	je	.LBB0_291
# BB#47:                                #   in Loop: Header=BB0_281 Depth=1
	movq	%rbp, %rdi
	movq	%r12, %r15
	movq	%r12, %rsi
	callq	pdf_nextobject
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_289
# BB#48:                                #   in Loop: Header=BB0_281 Depth=1
	movsbq	(%rbx), %rax
	testb	$8, 1(%r14,%rax,2)
	je	.LBB0_289
# BB#49:                                #   in Loop: Header=BB0_281 Depth=1
	movq	%rbx, %rax
	subq	%rbp, %rax
	movq	%r15, %rsi
	subq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%rsi, %r12
	callq	pdf_nextobject
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_290
# BB#50:                                #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.11, %esi
	movl	$3, %edx
	movq	%rbp, %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB0_290
# BB#51:                                #   in Loop: Header=BB0_281 Depth=1
	movq	%rbp, %rax
	subq	%rbx, %rax
	addq	$-3, %r12
	subq	%rax, %r12
	leaq	3(%rbp), %rcx
	movl	$.L.str.8, %eax
	cmpq	%rax, %rcx
	je	.LBB0_60
# BB#52:                                #   in Loop: Header=BB0_281 Depth=1
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r12, %r15
	cmpq	$6, %r12
	movq	24(%rsp), %r13          # 8-byte Reload
	jb	.LBB0_288
# BB#53:                                #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.8, %esi
	movl	$6, %edx
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	movq	%rbx, %rcx
	je	.LBB0_62
# BB#54:                                # %.preheader.i384
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$101, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	memchr
	testq	%rax, %rax
	je	.LBB0_288
# BB#55:                                # %.lr.ph.i387.preheader
                                        #   in Loop: Header=BB0_281 Depth=1
	movq	%r15, %rbp
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_56:                               # %.lr.ph.i387
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rcx
	subq	%rbx, %rcx
	subq	%rcx, %rbp
	cmpq	$6, %rbp
	jb	.LBB0_288
# BB#57:                                #   in Loop: Header=BB0_56 Depth=2
	movl	$.L.str.8, %esi
	movl	$6, %edx
	movq	%rax, %rdi
	movq	%rax, %r14
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_61
# BB#58:                                # %.backedge.i390
                                        #   in Loop: Header=BB0_56 Depth=2
	cmpq	%r14, %rbx
	leaq	1(%rbx), %rbx
	cmovneq	%r14, %rbx
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbp
	movl	$101, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memchr
	testq	%rax, %rax
	jne	.LBB0_56
	jmp	.LBB0_288
.LBB0_59:                               #   in Loop: Header=BB0_281 Depth=1
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_278
.LBB0_60:                               # %.thread451
                                        #   in Loop: Header=BB0_281 Depth=1
	addq	$-6, %r12
	addq	$9, %rbp
	movq	%rbp, 96(%rsp)          # 8-byte Spill
.LBB0_72:                               #   in Loop: Header=BB0_281 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_278
.LBB0_61:                               #   in Loop: Header=BB0_281 Depth=1
	movq	%r14, %rcx
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_62:                               # %.loopexit505
                                        #   in Loop: Header=BB0_281 Depth=1
	movq	%rcx, %rbp
	subq	%rbx, %rbp
	movq	%r15, %r12
	addq	$-6, %r12
	subq	%rbp, %r12
	leaq	6(%rcx), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$.L.str.14, %eax
	cmpq	%rax, %rbx
	movl	$.L.str.14, %esi
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r14
	movq	32(%rsp), %r13          # 8-byte Reload
	je	.LBB0_74
# BB#63:                                #   in Loop: Header=BB0_281 Depth=1
	cmpq	$6, %rbp
	jae	.LBB0_65
# BB#64:                                #   in Loop: Header=BB0_281 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_278
.LBB0_65:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.14, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	memcmp
	movq	48(%rsp), %rcx          # 8-byte Reload
	testl	%eax, %eax
	movq	%rbx, %rsi
	je	.LBB0_74
# BB#66:                                # %.preheader.i393
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$115, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memchr
	testq	%rax, %rax
	je	.LBB0_71
.LBB0_67:                               # %.lr.ph.i396
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rcx
	subq	%rbx, %rcx
	subq	%rcx, %rbp
	cmpq	$6, %rbp
	jb	.LBB0_72
# BB#68:                                #   in Loop: Header=BB0_67 Depth=2
	movl	$.L.str.14, %esi
	movl	$6, %edx
	movq	%rax, %rdi
	movq	%rax, %r14
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_73
# BB#69:                                # %.backedge.i399
                                        #   in Loop: Header=BB0_67 Depth=2
	cmpq	%r14, %rbx
	leaq	1(%rbx), %rbx
	cmovneq	%r14, %rbx
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbp
	movl	$115, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memchr
	testq	%rax, %rax
	jne	.LBB0_67
# BB#70:                                #   in Loop: Header=BB0_281 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	jmp	.LBB0_72
.LBB0_71:                               #   in Loop: Header=BB0_281 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_278
.LBB0_73:                               #   in Loop: Header=BB0_281 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %rsi
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_74:                               # %cli_pmemstr.exit401.thread457
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$1, %eax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	$0, 76(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
	movl	$0, 132(%rsp)           # 4-byte Folded Spill
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rsi, 8(%rsp)           # 8-byte Spill
.LBB0_75:                               #   Parent Loop BB0_281 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_92 Depth 3
                                        #       Child Loop BB0_84 Depth 3
                                        #       Child Loop BB0_98 Depth 3
                                        #       Child Loop BB0_107 Depth 3
                                        #       Child Loop BB0_113 Depth 3
	cmpq	%rsi, %rbx
	jae	.LBB0_119
# BB#76:                                #   in Loop: Header=BB0_75 Depth=2
	cmpb	$47, (%rbx)
	jne	.LBB0_82
# BB#77:                                #   in Loop: Header=BB0_75 Depth=2
	leaq	1(%rbx), %r15
	movl	$.L.str.15, %esi
	movl	$7, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_83
# BB#78:                                #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.21, %esi
	movl	$8, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_90
# BB#79:                                #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.22, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_91
# BB#80:                                #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.23, %esi
	movl	$11, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_93
# BB#81:                                #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.24, %esi
	movl	$13, %edx
	movq	%r15, %rdi
	callq	strncmp
	addq	$14, %rbx
	testl	%eax, %eax
	movl	$1, %eax
	movq	160(%rsp), %rcx         # 8-byte Reload
	cmovel	%eax, %ecx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	cmoveq	%rbx, %r15
	jmp	.LBB0_94
.LBB0_82:                               #   in Loop: Header=BB0_75 Depth=2
	movq	%rbx, %r15
	jmp	.LBB0_118
.LBB0_83:                               #   in Loop: Header=BB0_75 Depth=2
	leaq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	shlq	$32, %r14
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	addq	$7, %rbx
	movq	%rbx, %r15
.LBB0_84:                               #   Parent Loop BB0_281 Depth=1
                                        #     Parent Loop BB0_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	1(%r15), %rcx
	incq	%r15
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB0_84
# BB#85:                                #   in Loop: Header=BB0_75 Depth=2
	sarq	$32, %r14
	cmpq	$12, %r12
	jl	.LBB0_117
# BB#86:                                #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.16, %esi
	movl	$4, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB0_117
# BB#87:                                #   in Loop: Header=BB0_75 Depth=2
	addq	$4, %r15
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movl	$14, %esi
	movl	$.L.str.18, %edx
	xorl	%eax, %eax
	leaq	106(%rsp), %r13
	movq	%r13, %rdi
	movq	%r14, %rcx
	callq	snprintf
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	144(%rsp), %rbx         # 8-byte Reload
	cmpq	%r13, %rbx
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB0_101
# BB#88:                                #   in Loop: Header=BB0_75 Depth=2
	cmpq	%rbp, 56(%rsp)          # 8-byte Folded Reload
	jae	.LBB0_95
.LBB0_89:                               # %cli_pmemstr.exit410.thread.thread
                                        #   in Loop: Header=BB0_75 Depth=2
	movb	$13, 106(%rsp)
	jmp	.LBB0_103
.LBB0_90:                               #   in Loop: Header=BB0_75 Depth=2
	movl	$1, 132(%rsp)           # 4-byte Folded Spill
	jmp	.LBB0_94
.LBB0_91:                               #   in Loop: Header=BB0_75 Depth=2
	leaq	11(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	addq	$9, %rbx
	movq	%rbx, %r15
.LBB0_92:                               #   Parent Loop BB0_281 Depth=1
                                        #     Parent Loop BB0_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	2(%r15), %rcx
	incq	%r15
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB0_92
	jmp	.LBB0_94
.LBB0_93:                               #   in Loop: Header=BB0_75 Depth=2
	addq	$12, %rbx
	movl	$1, 76(%rsp)            # 4-byte Folded Spill
	movq	%rbx, %r15
.LBB0_94:                               #   in Loop: Header=BB0_75 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB0_118
.LBB0_95:                               #   in Loop: Header=BB0_75 Depth=2
	movq	%rbx, %rdi
	leaq	106(%rsp), %rsi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_101
# BB#96:                                # %.preheader.i402
                                        #   in Loop: Header=BB0_75 Depth=2
	movsbl	106(%rsp), %r13d
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	56(%rsp), %rdx          # 8-byte Reload
	callq	memchr
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_89
# BB#97:                                # %.lr.ph.i405.preheader
                                        #   in Loop: Header=BB0_75 Depth=2
	movq	%rbx, %rbp
	movq	56(%rsp), %rbx          # 8-byte Reload
.LBB0_98:                               # %.lr.ph.i405
                                        #   Parent Loop BB0_281 Depth=1
                                        #     Parent Loop BB0_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r14, %rax
	subq	%rbp, %rax
	subq	%rax, %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	jb	.LBB0_102
# BB#99:                                #   in Loop: Header=BB0_98 Depth=3
	movq	%r14, %rdi
	leaq	106(%rsp), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_111
# BB#100:                               # %.backedge.i408
                                        #   in Loop: Header=BB0_98 Depth=3
	cmpq	%r14, %rbp
	leaq	1(%rbp), %rbp
	cmovneq	%r14, %rbp
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbx
	movq	%rbp, %rdi
	movl	%r13d, %esi
	movq	%rbx, %rdx
	callq	memchr
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB0_98
	jmp	.LBB0_102
.LBB0_101:                              # %cli_pmemstr.exit410
                                        #   in Loop: Header=BB0_75 Depth=2
	testq	%rbx, %rbx
	movq	%rbx, %r14
	jne	.LBB0_111
.LBB0_102:                              # %cli_pmemstr.exit410.thread
                                        #   in Loop: Header=BB0_75 Depth=2
	movq	144(%rsp), %r14         # 8-byte Reload
	leaq	106(%rsp), %rax
	cmpq	%rax, %r14
	movb	$13, 106(%rsp)
	je	.LBB0_110
.LBB0_103:                              #   in Loop: Header=BB0_75 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 56(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_115
# BB#104:                               #   in Loop: Header=BB0_75 Depth=2
	movq	144(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdi
	leaq	106(%rsp), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_110
# BB#105:                               # %.preheader.i411
                                        #   in Loop: Header=BB0_75 Depth=2
	movl	$13, %esi
	movq	%r14, %rdi
	movq	56(%rsp), %rdx          # 8-byte Reload
	callq	memchr
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_115
# BB#106:                               # %.lr.ph.i414.preheader
                                        #   in Loop: Header=BB0_75 Depth=2
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
.LBB0_107:                              # %.lr.ph.i414
                                        #   Parent Loop BB0_281 Depth=1
                                        #     Parent Loop BB0_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r14, %rax
	subq	%rbp, %rax
	subq	%rax, %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	jb	.LBB0_115
# BB#108:                               #   in Loop: Header=BB0_107 Depth=3
	movq	%r14, %rdi
	leaq	106(%rsp), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_111
# BB#109:                               # %.backedge.i417
                                        #   in Loop: Header=BB0_107 Depth=3
	cmpq	%r14, %rbp
	leaq	1(%rbp), %rbp
	cmovneq	%r14, %rbp
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbx
	movl	$13, %esi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memchr
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB0_107
	jmp	.LBB0_115
.LBB0_110:                              # %cli_pmemstr.exit419
                                        #   in Loop: Header=BB0_75 Depth=2
	testq	%r14, %r14
	je	.LBB0_115
.LBB0_111:                              # %cli_pmemstr.exit419.thread462
                                        #   in Loop: Header=BB0_75 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	-1(%r14,%rax), %rdi
	movq	%rax, %r14
	movq	%rdi, %rax
	subq	%r15, %rax
	movq	64(%rsp), %rsi          # 8-byte Reload
	subq	%rax, %rsi
	callq	pdf_nextobject
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_116
# BB#112:                               #   in Loop: Header=BB0_75 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r14
	shlq	$32, %r14
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
.LBB0_113:                              #   Parent Loop BB0_281 Depth=1
                                        #     Parent Loop BB0_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%rbx), %rcx
	incq	%rbx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB0_113
# BB#114:                               #   in Loop: Header=BB0_75 Depth=2
	sarq	$32, %r14
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	leaq	107(%rsp), %rsi
	movq	%r14, %rdx
	callq	cli_dbgmsg
	jmp	.LBB0_116
.LBB0_115:                              # %cli_pmemstr.exit419.thread
                                        #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	leaq	107(%rsp), %rsi
	callq	cli_warnmsg
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB0_116:                              #   in Loop: Header=BB0_75 Depth=2
	movq	64(%rsp), %r12          # 8-byte Reload
.LBB0_117:                              #   in Loop: Header=BB0_75 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	decq	%r15
.LBB0_118:                              #   in Loop: Header=BB0_75 Depth=2
	subq	%r15, %rsi
	movq	%r15, %rdi
	callq	pdf_nextobject
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movq	48(%rsp), %rcx          # 8-byte Reload
	jne	.LBB0_75
.LBB0_119:                              #   in Loop: Header=BB0_281 Depth=1
	cmpl	$0, 132(%rsp)           # 4-byte Folded Reload
	je	.LBB0_122
# BB#120:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	cmpl	$0, 140(%rsp)           # 4-byte Folded Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	jne	.LBB0_278
# BB#121:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, 140(%rsp)           # 4-byte Folded Spill
	jmp	.LBB0_278
.LBB0_122:                              #   in Loop: Header=BB0_281 Depth=1
	cmpl	$2, 152(%rsp)           # 4-byte Folded Reload
	jl	.LBB0_126
# BB#123:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	cmpl	$0, 136(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_125
# BB#124:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movq	152(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	$1, 136(%rsp)           # 4-byte Folded Spill
.LBB0_125:                              # %.thread476
                                        #   in Loop: Header=BB0_281 Depth=1
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_278
.LBB0_126:                              #   in Loop: Header=BB0_281 Depth=1
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r12, 64(%rsp)          # 8-byte Spill
	leaq	6(%rsi), %rax
	subq	%rax, %rcx
	movq	%rcx, %rdi
	movslq	%ecx, %rdx
	movb	6(%rsi), %cl
	movl	$1, %esi
	shll	%cl, %esi
	cmpb	$15, %cl
	ja	.LBB0_128
# BB#127:                               #   in Loop: Header=BB0_281 Depth=1
	andl	$9217, %esi             # imm = 0x2401
	testw	%si, %si
	movq	%rax, %r13
	jne	.LBB0_132
	jmp	.LBB0_129
.LBB0_128:                              #   in Loop: Header=BB0_281 Depth=1
	movq	%rax, %r13
.LBB0_129:                              # %.lr.ph30.i
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$1, %rdx
	je	.LBB0_295
# BB#130:                               #   in Loop: Header=BB0_129 Depth=2
	decq	%rdx
	movzbl	1(%r13), %ecx
	incq	%r13
	movl	$1, %esi
	shll	%cl, %esi
	cmpb	$15, %cl
	ja	.LBB0_129
# BB#131:                               #   in Loop: Header=BB0_129 Depth=2
	andl	$9217, %esi             # imm = 0x2401
	testw	%si, %si
	je	.LBB0_129
.LBB0_132:                              # %.preheader.i420
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$1, %esi
	shll	%cl, %esi
	cmpb	$15, %cl
	ja	.LBB0_138
# BB#133:                               # %.preheader.i420
                                        #   in Loop: Header=BB0_281 Depth=1
	andl	$9217, %esi             # imm = 0x2401
	testw	%si, %si
	je	.LBB0_138
# BB#134:                               # %.lr.ph.i421.preheader
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$1, %esi
	subq	%rdx, %rsi
.LBB0_135:                              # %.lr.ph.i421
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rsi, %rsi
	je	.LBB0_295
# BB#136:                               #   in Loop: Header=BB0_135 Depth=2
	movzbl	1(%r13), %ecx
	incq	%r13
	movl	$1, %edx
	shll	%cl, %edx
	cmpb	$15, %cl
	ja	.LBB0_138
# BB#137:                               #   in Loop: Header=BB0_135 Depth=2
	andl	$9217, %edx             # imm = 0x2401
	incq	%rsi
	testw	%dx, %dx
	jne	.LBB0_135
.LBB0_138:                              # %.loopexit502
                                        #   in Loop: Header=BB0_281 Depth=1
	xorl	%r14d, %r14d
	movl	$.L.str.27, %ecx
	cmpq	%rcx, %r13
	je	.LBB0_148
# BB#139:                               #   in Loop: Header=BB0_281 Depth=1
	movl	%r13d, %ecx
	subl	%eax, %ecx
	subl	%ecx, %edi
	movslq	%edi, %r12
	cmpq	$10, %r12
	jae	.LBB0_149
.LBB0_140:                              # %.loopexit500
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$1, %r14d
	movl	$.L.str.28, %eax
	cmpq	%rax, %r13
	je	.LBB0_155
# BB#141:                               #   in Loop: Header=BB0_281 Depth=1
	cmpq	$10, %r12
	jb	.LBB0_299
# BB#142:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.28, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_157
# BB#143:                               # %.preheader.i431
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$101, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	memchr
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_299
# BB#144:                               # %.lr.ph.i434.preheader
                                        #   in Loop: Header=BB0_281 Depth=1
	movq	%r13, %rbx
.LBB0_145:                              # %.lr.ph.i434
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rax
	subq	%rbx, %rax
	subq	%rax, %r12
	cmpq	$10, %r12
	jb	.LBB0_299
# BB#146:                               #   in Loop: Header=BB0_145 Depth=2
	movl	$.L.str.28, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_158
# BB#147:                               # %.backedge.i437
                                        #   in Loop: Header=BB0_145 Depth=2
	cmpq	%r15, %rbx
	leaq	1(%rbx), %rbx
	cmovneq	%r15, %rbx
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %r12
	movl	$101, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	callq	memchr
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB0_145
	jmp	.LBB0_299
.LBB0_148:                              #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.27, %r15d
	jmp	.LBB0_158
.LBB0_149:                              #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.27, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_157
# BB#150:                               # %.preheader.i422
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$101, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	memchr
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_140
# BB#151:                               # %.lr.ph.i425.preheader
                                        #   in Loop: Header=BB0_281 Depth=1
	movq	%r12, %rbp
	movq	%r13, %rbx
.LBB0_152:                              # %.lr.ph.i425
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rax
	subq	%rbx, %rax
	subq	%rax, %rbp
	cmpq	$10, %rbp
	jb	.LBB0_140
# BB#153:                               #   in Loop: Header=BB0_152 Depth=2
	movl	$.L.str.27, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_158
# BB#154:                               # %.backedge.i428
                                        #   in Loop: Header=BB0_152 Depth=2
	cmpq	%r15, %rbx
	leaq	1(%rbx), %rbx
	cmovneq	%r15, %rbx
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbp
	movl	$101, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memchr
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB0_152
	jmp	.LBB0_140
.LBB0_157:                              #   in Loop: Header=BB0_281 Depth=1
	movq	%r13, %r15
	jmp	.LBB0_158
.LBB0_155:                              #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.28, %r15d
.LBB0_158:                              # %cli_pmemstr.exit430.thread468
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$257, %esi              # imm = 0x101
	movl	$.L.str.30, %edx
	xorl	%eax, %eax
	leaq	224(%rsp), %rbx
	movq	%rbx, %rdi
	movq	184(%rsp), %rcx         # 8-byte Reload
	callq	snprintf
	movq	%rbx, %rdi
	callq	mkstemp
	movl	%eax, 48(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	js	.LBB0_302
# BB#159:                               #   in Loop: Header=BB0_281 Depth=1
	movb	-1(%r15), %al
	cmpb	$13, %al
	je	.LBB0_161
# BB#160:                               #   in Loop: Header=BB0_281 Depth=1
	cmpb	$10, %al
	jne	.LBB0_164
.LBB0_161:                              #   in Loop: Header=BB0_281 Depth=1
	leaq	-1(%r15), %rax
	testl	%r14d, %r14d
	je	.LBB0_163
# BB#162:                               #   in Loop: Header=BB0_281 Depth=1
	cmpb	$13, -2(%r15)
	leaq	-2(%r15), %rcx
	cmoveq	%rcx, %rax
.LBB0_163:                              #   in Loop: Header=BB0_281 Depth=1
	movq	%rax, %r15
.LBB0_164:                              #   in Loop: Header=BB0_281 Depth=1
	cmpq	%r13, %r15
	jbe	.LBB0_172
# BB#165:                               #   in Loop: Header=BB0_281 Depth=1
	subl	%r13d, %r15d
	movslq	%r15d, %r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	je	.LBB0_167
# BB#166:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_167:                              #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movl	76(%rsp), %ecx          # 4-byte Reload
	movq	160(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %r8d
	callq	cli_dbgmsg
	testl	%ebx, %ebx
	je	.LBB0_173
# BB#168:                               #   in Loop: Header=BB0_281 Depth=1
	leaq	(%r14,%r14,4), %rdi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%rax, %rax
	je	.LBB0_307
# BB#169:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.53, %eax
	cmpq	%rax, %r13
	je	.LBB0_183
# BB#170:                               #   in Loop: Header=BB0_281 Depth=1
	cmpq	$2, %r14
	jae	.LBB0_177
.LBB0_171:                              # %cli_pmemstr.exit.thread.i
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_183
.LBB0_172:                              #   in Loop: Header=BB0_281 Depth=1
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	close
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	224(%rsp), %rdi
	callq	unlink
	jmp	.LBB0_277
.LBB0_173:                              #   in Loop: Header=BB0_281 Depth=1
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	je	.LBB0_184
# BB#174:                               #   in Loop: Header=BB0_281 Depth=1
	movq	%r13, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	48(%rsp), %edx          # 4-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
	callq	flatedecode
	movl	%eax, %ebx
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	testl	%ebx, %ebx
	je	.LBB0_272
# BB#175:                               #   in Loop: Header=BB0_281 Depth=1
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB0_229
# BB#176:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.40, %edi
	jmp	.LBB0_231
.LBB0_177:                              #   in Loop: Header=BB0_281 Depth=1
	movzwl	(%r13), %eax
	cmpl	$15998, %eax            # imm = 0x3E7E
	je	.LBB0_183
# BB#178:                               # %.preheader.i.i
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$126, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	memchr
	testq	%rax, %rax
	je	.LBB0_171
# BB#179:                               # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_281 Depth=1
	movq	%r14, %rbx
	movq	%r13, %rbp
.LBB0_180:                              # %.lr.ph.i.i
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rcx
	subq	%rbp, %rcx
	subq	%rcx, %rbx
	cmpq	$2, %rbx
	jb	.LBB0_171
# BB#181:                               #   in Loop: Header=BB0_180 Depth=2
	movzwl	(%rax), %ecx
	cmpl	$15998, %ecx            # imm = 0x3E7E
	je	.LBB0_183
# BB#182:                               # %.backedge.i.i
                                        #   in Loop: Header=BB0_180 Depth=2
	cmpq	%rax, %rbp
	leaq	1(%rbp), %rbp
	cmovneq	%rax, %rbp
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbx
	movl	$126, %esi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memchr
	testq	%rax, %rax
	jne	.LBB0_180
	jmp	.LBB0_171
.LBB0_183:                              # %cli_pmemstr.exit.thread99.i
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	%r14, %rdx
	movq	%r15, %r12
	movq	%r13, %rax
	jmp	.LBB0_186
.LBB0_184:                              #   in Loop: Header=BB0_281 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movl	48(%rsp), %edi          # 4-byte Reload
	movq	%r13, %rsi
	movl	%ebx, %edx
	callq	cli_writen
	jmp	.LBB0_272
.LBB0_185:                              # %.thread101.outer.backedge.i
                                        #   in Loop: Header=BB0_186 Depth=2
	addq	$4, %r12
	addl	$4, 16(%rsp)            # 4-byte Folded Spill
	movq	%rbp, %rax
.LBB0_186:                              # %.thread101.outer.i
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_187 Depth 3
                                        #       Child Loop BB0_195 Depth 3
                                        #       Child Loop BB0_204 Depth 3
                                        #       Child Loop BB0_212 Depth 3
                                        #       Child Loop BB0_220 Depth 3
	leaq	1(%rax), %rbp
	leaq	2(%rax), %rbx
	leaq	3(%rax), %rdi
	leaq	4(%rax), %r9
	leaq	5(%rax), %r8
.LBB0_187:                              # %.thread101.i
                                        #   Parent Loop BB0_281 Depth=1
                                        #     Parent Loop BB0_186 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	jle	.LBB0_263
# BB#188:                               #   in Loop: Header=BB0_187 Depth=3
	movsbl	-1(%rbp), %esi
	cmpl	$126, %esi
	jne	.LBB0_190
# BB#189:                               #   in Loop: Header=BB0_187 Depth=3
	cmpb	$62, (%rbp)
	movl	$-1, %eax
	cmovel	%eax, %esi
.LBB0_190:                              #   in Loop: Header=BB0_187 Depth=3
	leal	-33(%rsi), %eax
	cmpl	$84, %eax
	jbe	.LBB0_194
# BB#191:                               #   in Loop: Header=BB0_187 Depth=3
	cmpl	$-1, %esi
	je	.LBB0_228
# BB#192:                               #   in Loop: Header=BB0_187 Depth=3
	cmpl	$122, %esi
	je	.LBB0_202
# BB#193:                               #   in Loop: Header=BB0_187 Depth=3
	decq	%rdx
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movslq	%esi, %rcx
	incq	%rbp
	incq	%rbx
	incq	%rdi
	incq	%r9
	incq	%r8
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_187
	jmp	.LBB0_300
.LBB0_194:                              # %.thread101.1.i.preheader
                                        #   in Loop: Header=BB0_186 Depth=2
	decq	%rdx
.LBB0_195:                              # %.thread101.1.i
                                        #   Parent Loop BB0_281 Depth=1
                                        #     Parent Loop BB0_186 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	jle	.LBB0_263
# BB#196:                               #   in Loop: Header=BB0_195 Depth=3
	movsbl	-1(%rbx), %esi
	cmpl	$126, %esi
	jne	.LBB0_198
# BB#197:                               #   in Loop: Header=BB0_195 Depth=3
	cmpb	$62, (%rbx)
	movl	$-1, %ecx
	cmovel	%ecx, %esi
.LBB0_198:                              #   in Loop: Header=BB0_195 Depth=3
	leal	-33(%rsi), %ebp
	cmpl	$85, %ebp
	jb	.LBB0_203
# BB#199:                               #   in Loop: Header=BB0_195 Depth=3
	cmpl	$-1, %esi
	je	.LBB0_232
# BB#200:                               #   in Loop: Header=BB0_195 Depth=3
	cmpl	$122, %esi
	je	.LBB0_303
# BB#201:                               #   in Loop: Header=BB0_195 Depth=3
	decq	%rdx
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movslq	%esi, %rbp
	incq	%rbx
	incq	%rdi
	incq	%r9
	incq	%r8
	testb	$32, 1(%rcx,%rbp,2)
	jne	.LBB0_195
	jmp	.LBB0_300
.LBB0_202:                              # %.loopexit.i
                                        #   in Loop: Header=BB0_186 Depth=2
	movq	%r12, %rax
	movl	$0, (%rax)
	decq	%rdx
	jmp	.LBB0_185
.LBB0_203:                              # %.thread101.outer105.2175.i
                                        #   in Loop: Header=BB0_186 Depth=2
	imull	$85, %eax, %eax
	addl	%eax, %ebp
	decq	%rdx
.LBB0_204:                              # %.thread101.2.i
                                        #   Parent Loop BB0_281 Depth=1
                                        #     Parent Loop BB0_186 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	jle	.LBB0_263
# BB#205:                               #   in Loop: Header=BB0_204 Depth=3
	movsbl	-1(%rdi), %esi
	cmpl	$126, %esi
	jne	.LBB0_207
# BB#206:                               #   in Loop: Header=BB0_204 Depth=3
	cmpb	$62, (%rdi)
	movl	$-1, %eax
	cmovel	%eax, %esi
.LBB0_207:                              #   in Loop: Header=BB0_204 Depth=3
	leal	-33(%rsi), %eax
	cmpl	$85, %eax
	jb	.LBB0_211
# BB#208:                               #   in Loop: Header=BB0_204 Depth=3
	cmpl	$-1, %esi
	je	.LBB0_233
# BB#209:                               #   in Loop: Header=BB0_204 Depth=3
	cmpl	$122, %esi
	je	.LBB0_303
# BB#210:                               #   in Loop: Header=BB0_204 Depth=3
	decq	%rdx
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movslq	%esi, %rcx
	incq	%rdi
	incq	%r9
	incq	%r8
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_204
	jmp	.LBB0_300
.LBB0_211:                              # %.thread101.outer105.3176.i
                                        #   in Loop: Header=BB0_186 Depth=2
	imull	$85, %ebp, %ecx
	addl	%ecx, %eax
	decq	%rdx
.LBB0_212:                              # %.thread101.3.i
                                        #   Parent Loop BB0_281 Depth=1
                                        #     Parent Loop BB0_186 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	jle	.LBB0_263
# BB#213:                               #   in Loop: Header=BB0_212 Depth=3
	movsbl	-1(%r9), %esi
	cmpl	$126, %esi
	jne	.LBB0_215
# BB#214:                               #   in Loop: Header=BB0_212 Depth=3
	cmpb	$62, (%r9)
	movl	$-1, %ecx
	cmovel	%ecx, %esi
.LBB0_215:                              #   in Loop: Header=BB0_212 Depth=3
	leal	-33(%rsi), %ebp
	cmpl	$85, %ebp
	jb	.LBB0_219
# BB#216:                               #   in Loop: Header=BB0_212 Depth=3
	cmpl	$-1, %esi
	je	.LBB0_234
# BB#217:                               #   in Loop: Header=BB0_212 Depth=3
	cmpl	$122, %esi
	je	.LBB0_303
# BB#218:                               #   in Loop: Header=BB0_212 Depth=3
	decq	%rdx
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movslq	%esi, %rdi
	incq	%r9
	incq	%r8
	testb	$32, 1(%rcx,%rdi,2)
	jne	.LBB0_212
	jmp	.LBB0_300
.LBB0_219:                              # %.thread101.outer105.4177.i
                                        #   in Loop: Header=BB0_186 Depth=2
	imull	$85, %eax, %eax
	addl	%eax, %ebp
	decq	%rdx
.LBB0_220:                              # %.thread101.4.i
                                        #   Parent Loop BB0_281 Depth=1
                                        #     Parent Loop BB0_186 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	jle	.LBB0_263
# BB#221:                               #   in Loop: Header=BB0_220 Depth=3
	movsbl	-1(%r8), %esi
	cmpl	$126, %esi
	jne	.LBB0_223
# BB#222:                               #   in Loop: Header=BB0_220 Depth=3
	cmpb	$62, (%r8)
	movl	$-1, %eax
	cmovel	%eax, %esi
.LBB0_223:                              #   in Loop: Header=BB0_220 Depth=3
	leal	-33(%rsi), %eax
	cmpl	$85, %eax
	jb	.LBB0_227
# BB#224:                               #   in Loop: Header=BB0_220 Depth=3
	cmpl	$-1, %esi
	je	.LBB0_280
# BB#225:                               #   in Loop: Header=BB0_220 Depth=3
	cmpl	$122, %esi
	je	.LBB0_303
# BB#226:                               #   in Loop: Header=BB0_220 Depth=3
	decq	%rdx
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movslq	%esi, %rcx
	incq	%r8
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_220
	jmp	.LBB0_300
.LBB0_227:                              #   in Loop: Header=BB0_186 Depth=2
	imull	$85, %ebp, %ecx
	addl	%ecx, %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movq	%r12, %rsi
	movb	%cl, (%rsi)
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 1(%rsi)
	movb	%ah, 2(%rsi)  # NOREX
	movb	%al, 3(%rsi)
	decq	%rdx
	movq	%r8, %rbp
	jmp	.LBB0_185
.LBB0_228:                              # %.loopexit178.i.loopexit1430
                                        #   in Loop: Header=BB0_281 Depth=1
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	jmp	.LBB0_236
.LBB0_229:                              #   in Loop: Header=BB0_281 Depth=1
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	48(%rsp), %edx          # 4-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
	callq	flatedecode
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB0_272
# BB#230:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.41, %edi
.LBB0_231:                              # %try_flatedecode.exit442
                                        #   in Loop: Header=BB0_281 Depth=1
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	jmp	.LBB0_272
.LBB0_232:                              # %.loopexit178.i.loopexit1427
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$1, %ebx
.LBB0_235:                              # %.loopexit178.i
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	%eax, %ebp
.LBB0_236:                              # %.loopexit178.i
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movl	%ebx, %eax
	andb	$7, %al
	je	.LBB0_263
# BB#237:                               # %.loopexit178.i
                                        #   in Loop: Header=BB0_281 Depth=1
	cmpb	$1, %al
	je	.LBB0_308
# BB#238:                               # %.lr.ph142.i.preheader
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$5, %ecx
	subl	%ebx, %ecx
	cmpl	$8, %ecx
	jae	.LBB0_240
# BB#239:                               #   in Loop: Header=BB0_281 Depth=1
	movl	%ebx, %eax
	jmp	.LBB0_251
.LBB0_240:                              # %min.iters.checked1398
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	%ecx, %eax
	andl	$-8, %eax
	movl	%ecx, %edx
	andl	$-8, %edx
	je	.LBB0_244
# BB#241:                               # %vector.ph1402
                                        #   in Loop: Header=BB0_281 Depth=1
	movd	%ebp, %xmm0
	punpckldq	208(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	punpckldq	192(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	leal	-8(%rdx), %ebp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andl	$7, %esi
	je	.LBB0_245
# BB#242:                               # %vector.body1394.prol.preheader
                                        #   in Loop: Header=BB0_281 Depth=1
	negl	%esi
	xorl	%edi, %edi
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [1,1,1,1]
.LBB0_243:                              # %vector.body1394.prol
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	pshufd	$245, %xmm0, %xmm2      # xmm2 = xmm0[1,1,3,3]
	movdqa	.LCPI0_1(%rip), %xmm3   # xmm3 = [85,85,85,85]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	addl	$8, %edi
	incl	%esi
	jne	.LBB0_243
	jmp	.LBB0_246
.LBB0_244:                              #   in Loop: Header=BB0_281 Depth=1
	movl	%ebx, %eax
	jmp	.LBB0_251
.LBB0_245:                              #   in Loop: Header=BB0_281 Depth=1
	xorl	%edi, %edi
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [1,1,1,1]
.LBB0_246:                              # %vector.body1394.prol.loopexit
                                        #   in Loop: Header=BB0_281 Depth=1
	cmpl	$56, %ebp
	jb	.LBB0_249
# BB#247:                               # %vector.ph1402.new
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	%edx, %esi
	subl	%edi, %esi
.LBB0_248:                              # %vector.body1394
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	pshufd	$245, %xmm0, %xmm2      # xmm2 = xmm0[1,1,3,3]
	movdqa	.LCPI0_2(%rip), %xmm3   # xmm3 = [1904149089,1904149089,1904149089,1904149089]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	addl	$-64, %esi
	jne	.LBB0_248
.LBB0_249:                              # %middle.block1395
                                        #   in Loop: Header=BB0_281 Depth=1
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movd	%xmm0, %ebp
	cmpl	%edx, %ecx
	je	.LBB0_253
# BB#250:                               #   in Loop: Header=BB0_281 Depth=1
	orl	%ebx, %eax
.LBB0_251:                              # %.lr.ph142.i.preheader1433
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$5, %ecx
	subl	%eax, %ecx
.LBB0_252:                              # %.lr.ph142.i
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imull	$85, %ebp, %ebp
	decl	%ecx
	jne	.LBB0_252
.LBB0_253:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_281 Depth=1
	cmpl	$1, %ebx
	jbe	.LBB0_262
# BB#254:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_281 Depth=1
	leal	-16(,%rbx,8), %ecx
	movl	$16777215, %eax         # imm = 0xFFFFFF
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	addl	%eax, %ebp
	leal	-2(%rbx), %r8d
	incq	%r8
	cmpq	$4, %r8
	jb	.LBB0_259
# BB#255:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_281 Depth=1
	movq	%r8, %rax
	movabsq	$8589934588, %rdx       # imm = 0x1FFFFFFFC
	movq	%rdx, %rsi
	andq	%rsi, %rax
	movq	%r8, %rdx
	andq	%rsi, %rdx
	je	.LBB0_259
# BB#256:                               # %vector.ph
                                        #   in Loop: Header=BB0_281 Depth=1
	movq	%r12, %rsi
	addq	%rsi, %rax
	movd	%ebp, %xmm0
	xorl	%esi, %esi
.LBB0_257:                              # %vector.body
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	pshufd	$68, %xmm0, %xmm1       # xmm1 = xmm0[0,1,0,1]
	movdqa	.LCPI0_3(%rip), %xmm2   # xmm2 = [4294967295,0,4294967295,0]
	movdqa	%xmm2, %xmm5
	pand	%xmm5, %xmm1
	movd	%rsi, %xmm2
	pshufd	$68, %xmm2, %xmm2       # xmm2 = xmm2[0,1,0,1]
	psllq	$3, %xmm2
	movdqa	.LCPI0_4(%rip), %xmm3   # xmm3 = [24,16]
	psubq	%xmm2, %xmm3
	pand	%xmm5, %xmm3
	movl	$8, %edi
	movd	%rdi, %xmm4
	psubq	%xmm2, %xmm4
	pand	%xmm5, %xmm4
	pshufd	$78, %xmm3, %xmm2       # xmm2 = xmm3[2,3,0,1]
	movdqa	%xmm1, %xmm5
	psrlq	%xmm2, %xmm5
	movdqa	%xmm1, %xmm2
	psrlq	%xmm3, %xmm2
	movsd	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1]
	pshufd	$78, %xmm4, %xmm2       # xmm2 = xmm4[2,3,0,1]
	movdqa	%xmm1, %xmm3
	psrlq	%xmm2, %xmm3
	psrlq	%xmm4, %xmm1
	movsd	%xmm1, %xmm3            # xmm3 = xmm1[0],xmm3[1]
	movapd	.LCPI0_5(%rip), %xmm1   # xmm1 = [255,255]
	andpd	%xmm1, %xmm5
	packuswb	%xmm5, %xmm5
	packuswb	%xmm5, %xmm5
	packuswb	%xmm5, %xmm5
	movd	%xmm5, %edi
	movq	%r12, %rcx
	movw	%di, (%rcx,%rsi)
	andpd	%xmm1, %xmm3
	packuswb	%xmm3, %xmm3
	packuswb	%xmm3, %xmm3
	packuswb	%xmm3, %xmm3
	movd	%xmm3, %edi
	movw	%di, 2(%rcx,%rsi)
	addq	$4, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB0_257
# BB#258:                               # %middle.block
                                        #   in Loop: Header=BB0_281 Depth=1
	cmpq	%rdx, %r8
	jne	.LBB0_260
	jmp	.LBB0_262
.LBB0_259:                              #   in Loop: Header=BB0_281 Depth=1
	xorl	%edx, %edx
	movq	%r12, %rax
.LBB0_260:                              # %.lr.ph.i440.preheader
                                        #   in Loop: Header=BB0_281 Depth=1
	leal	-1(%rbx), %esi
	subl	%edx, %esi
	shll	$3, %edx
	movl	$24, %ecx
	subl	%edx, %ecx
.LBB0_261:                              # %.lr.ph.i440
                                        #   Parent Loop BB0_281 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %edx
	shrl	%cl, %edx
	movb	%dl, (%rax)
	incq	%rax
	addl	$-8, %ecx
	decl	%esi
	jne	.LBB0_261
.LBB0_262:                              # %ascii85decode.exit.loopexit750
                                        #   in Loop: Header=BB0_281 Depth=1
	addl	16(%rsp), %ebx          # 4-byte Folded Reload
	movl	%ebx, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
.LBB0_263:                              # %ascii85decode.exit
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB0_269
# BB#264:                               # %ascii85decode.exit
                                        #   in Loop: Header=BB0_281 Depth=1
	cmpl	$-1, 16(%rsp)           # 4-byte Folded Reload
	je	.LBB0_301
# BB#265:                               #   in Loop: Header=BB0_281 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	cli_realloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_306
# BB#266:                               #   in Loop: Header=BB0_281 Depth=1
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	je	.LBB0_270
# BB#267:                               #   in Loop: Header=BB0_281 Depth=1
	movslq	16(%rsp), %rsi          # 4-byte Folded Reload
	movq	%rbp, %rdi
	movl	48(%rsp), %edx          # 4-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
	callq	flatedecode
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB0_271
# BB#268:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	jmp	.LBB0_271
.LBB0_269:                              #   in Loop: Header=BB0_281 Depth=1
	movq	%r15, %rbp
	jmp	.LBB0_271
.LBB0_270:                              #   in Loop: Header=BB0_281 Depth=1
	movl	48(%rsp), %edi          # 4-byte Reload
	movq	%r13, %rsi
	movl	16(%rsp), %edx          # 4-byte Reload
	callq	cli_writen
.LBB0_271:                              # %try_flatedecode.exit.thread
                                        #   in Loop: Header=BB0_281 Depth=1
	movq	%rbp, %rdi
	callq	free
.LBB0_272:                              # %try_flatedecode.exit442
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	close
	leaq	224(%rsp), %rdi
	callq	cli_md5file
	movq	%rax, %rbx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	tableFind
	testl	%eax, %eax
	js	.LBB0_274
# BB#273:                               #   in Loop: Header=BB0_281 Depth=1
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	leaq	224(%rsp), %rbp
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	unlink
	jmp	.LBB0_275
.LBB0_274:                              #   in Loop: Header=BB0_281 Depth=1
	movl	$1, %edx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	tableInsert
.LBB0_275:                              #   in Loop: Header=BB0_281 Depth=1
	movq	%rbx, %rdi
	callq	free
	movl	92(%rsp), %esi          # 4-byte Reload
	incl	%esi
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	movl	%esi, 92(%rsp)          # 4-byte Spill
	leaq	224(%rsp), %rdx
	callq	cli_dbgmsg
	cmpq	$0, 168(%rsp)           # 8-byte Folded Reload
	je	.LBB0_277
# BB#276:                               #   in Loop: Header=BB0_281 Depth=1
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	4(%rax), %esi
	leal	-1(%rsi), %eax
	cmpl	92(%rsp), %eax          # 4-byte Folded Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	jae	.LBB0_278
	jmp	.LBB0_305
.LBB0_277:                              #   in Loop: Header=BB0_281 Depth=1
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
.LBB0_278:                              # %.thread476
                                        #   in Loop: Header=BB0_281 Depth=1
	movq	%r12, 64(%rsp)          # 8-byte Spill
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB0_293
# BB#279:                               # %.thread476
                                        #   in Loop: Header=BB0_281 Depth=1
	cmpq	%rbp, 96(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_281
	jmp	.LBB0_293
.LBB0_233:                              # %.loopexit178.i.loopexit1423
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$2, %ebx
	jmp	.LBB0_236
.LBB0_234:                              # %.loopexit178.i.loopexit1419
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$3, %ebx
	jmp	.LBB0_235
.LBB0_280:                              # %.loopexit178.i.loopexit
                                        #   in Loop: Header=BB0_281 Depth=1
	movl	$4, %ebx
	jmp	.LBB0_236
.LBB0_281:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_56 Depth 2
                                        #     Child Loop BB0_67 Depth 2
                                        #     Child Loop BB0_75 Depth 2
                                        #       Child Loop BB0_92 Depth 3
                                        #       Child Loop BB0_84 Depth 3
                                        #       Child Loop BB0_98 Depth 3
                                        #       Child Loop BB0_107 Depth 3
                                        #       Child Loop BB0_113 Depth 3
                                        #     Child Loop BB0_129 Depth 2
                                        #     Child Loop BB0_135 Depth 2
                                        #     Child Loop BB0_152 Depth 2
                                        #     Child Loop BB0_145 Depth 2
                                        #     Child Loop BB0_180 Depth 2
                                        #     Child Loop BB0_186 Depth 2
                                        #       Child Loop BB0_187 Depth 3
                                        #       Child Loop BB0_195 Depth 3
                                        #       Child Loop BB0_204 Depth 3
                                        #       Child Loop BB0_212 Depth 3
                                        #       Child Loop BB0_220 Depth 3
                                        #     Child Loop BB0_243 Depth 2
                                        #     Child Loop BB0_248 Depth 2
                                        #     Child Loop BB0_252 Depth 2
                                        #     Child Loop BB0_257 Depth 2
                                        #     Child Loop BB0_261 Depth 2
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rsi
	callq	pdf_nextobject
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB0_43
# BB#282:
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	testq	%r13, %r13
	jne	.LBB0_294
	jmp	.LBB0_297
.LBB0_283:                              # %cli_pmemstr.exit
	testq	%r13, %r13
	je	.LBB0_25
.LBB0_284:                              # %cli_pmemstr.exit.thread444
	movq	32(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_286
# BB#285:
	callq	free
	jmp	.LBB0_287
.LBB0_286:
	movq	%rbp, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	munmap
.LBB0_287:
	movl	$-124, %ebp
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_14
.LBB0_288:                              # %.loopexit504
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r13, %r14
	movq	32(%rsp), %r13          # 8-byte Reload
	testq	%r13, %r13
	jne	.LBB0_294
	jmp	.LBB0_297
.LBB0_289:
	movl	$.L.str.10, %edi
	jmp	.LBB0_292
.LBB0_290:
	movl	$.L.str.12, %edi
	jmp	.LBB0_292
.LBB0_291:
	movl	$.L.str.9, %edi
.LBB0_292:                              # %pdf_nextlinestart.exit.thread
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$-124, 8(%rsp)          # 4-byte Folded Spill
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB0_293:                              # %.critedge
	testq	%r13, %r13
	je	.LBB0_297
.LBB0_294:
	movq	%r13, %rdi
	callq	free
	jmp	.LBB0_298
.LBB0_295:
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
.LBB0_296:                              # %pdf_nextlinestart.exit.thread
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	testq	%r13, %r13
	jne	.LBB0_294
.LBB0_297:
	movq	176(%rsp), %rdi         # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	munmap
.LBB0_298:
	movq	%r14, %rdi
	callq	tableDestroy
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	movl	8(%rsp), %ebp           # 4-byte Reload
	movl	%ebp, %esi
	callq	cli_dbgmsg
	jmp	.LBB0_14
.LBB0_299:                              # %.loopexit
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_296
.LBB0_300:                              # %.loopexit179.i
	movzbl	%sil, %esi
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB0_301:                              # %ascii85decode.exit.thread
	movq	%r15, %rdi
	callq	free
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	close
	leaq	224(%rsp), %rdi
	callq	unlink
	movl	$-124, 8(%rsp)          # 4-byte Folded Spill
	jmp	.LBB0_296
.LBB0_302:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	leaq	224(%rsp), %rsi
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	cli_errmsg
	movl	$-112, 8(%rsp)          # 4-byte Folded Spill
	jmp	.LBB0_296
.LBB0_303:                              # %.loopexit243.i
	movl	$.L.str.56, %edi
.LBB0_304:                              # %ascii85decode.exit.thread
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_301
.LBB0_305:
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	$-102, 8(%rsp)          # 4-byte Folded Spill
	testq	%r13, %r13
	jne	.LBB0_294
	jmp	.LBB0_297
.LBB0_306:                              # %try_flatedecode.exit
	movq	%r15, %rdi
	callq	free
.LBB0_307:
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	close
	leaq	224(%rsp), %rdi
	callq	unlink
	movl	$-114, 8(%rsp)          # 4-byte Folded Spill
	jmp	.LBB0_296
.LBB0_308:
	movl	$.L.str.58, %edi
	jmp	.LBB0_304
.Lfunc_end0:
	.size	cli_pdf, .Lfunc_end0-cli_pdf
	.cfi_endproc

	.p2align	4, 0x90
	.type	pdf_nextobject,@function
pdf_nextobject:                         # @pdf_nextobject
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB1_23
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
.LBB1_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #     Child Loop BB1_16 Depth 2
                                        #     Child Loop BB1_12 Depth 2
	testb	$1, %cl
	je	.LBB1_5
# BB#3:                                 # %.lr.ph.split.us
                                        #   in Loop: Header=BB1_2 Depth=1
	movsbl	(%rdi), %ecx
	leal	-9(%rcx), %eax
	cmpl	$82, %eax
	ja	.LBB1_24
# BB#4:                                 # %.lr.ph.split.us
                                        #   in Loop: Header=BB1_2 Depth=1
	jmpq	*.LJTI1_1(,%rax,8)
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph.split
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rdi), %ecx
	leal	-9(%rcx), %eax
	cmpl	$82, %eax
	ja	.LBB1_22
# BB#6:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB1_5 Depth=2
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_22:                               #   in Loop: Header=BB1_5 Depth=2
	incq	%rdi
	decq	%rsi
	jne	.LBB1_5
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_20:                               # %.us-lcssa41.us
                                        #   in Loop: Header=BB1_2 Depth=1
	incq	%rdi
	decq	%rsi
	movq	%rdi, %r9
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_7:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %edx
	shll	%cl, %edx
	cmpb	$15, %cl
	movq	%rsi, %r8
	movq	%rdi, %r9
	ja	.LBB1_16
# BB#8:                                 # %.us-lcssa.us
                                        #   in Loop: Header=BB1_2 Depth=1
	andl	$9217, %edx             # imm = 0x2401
	testw	%dx, %dx
	movq	%rdi, %r9
	movq	%rsi, %r8
	jne	.LBB1_9
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph30.i
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$1, %r8
	je	.LBB1_23
# BB#17:                                #   in Loop: Header=BB1_16 Depth=2
	decq	%r8
	movb	1(%r9), %cl
	incq	%r9
	movl	$1, %edx
	shll	%cl, %edx
	cmpb	$15, %cl
	ja	.LBB1_16
# BB#18:                                #   in Loop: Header=BB1_16 Depth=2
	andl	$9217, %edx             # imm = 0x2401
	testw	%dx, %dx
	je	.LBB1_16
.LBB1_9:                                # %.preheader.i
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %eax
	shll	%cl, %eax
	cmpb	$15, %cl
	ja	.LBB1_19
# BB#10:                                # %.preheader.i
                                        #   in Loop: Header=BB1_2 Depth=1
	andl	$9217, %eax             # imm = 0x2401
	testw	%ax, %ax
	je	.LBB1_19
# BB#11:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %edx
	subq	%r8, %rdx
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph.i
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rdx, %rdx
	je	.LBB1_23
# BB#13:                                #   in Loop: Header=BB1_12 Depth=2
	movzbl	1(%r9), %ecx
	incq	%r9
	movl	$1, %eax
	shll	%cl, %eax
	cmpb	$15, %cl
	ja	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_12 Depth=2
	andl	$9217, %eax             # imm = 0x2401
	incq	%rdx
	testw	%ax, %ax
	jne	.LBB1_12
	jmp	.LBB1_15
.LBB1_19:                               # %pdf_nextlinestart.exit
                                        #   in Loop: Header=BB1_2 Depth=1
	testq	%r9, %r9
	je	.LBB1_23
.LBB1_15:                               # %pdf_nextlinestart.exit.thread21
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%r9, %rax
	subq	%rdi, %rax
	subq	%rax, %rsi
.LBB1_21:                               # %.outer.backedge
                                        #   in Loop: Header=BB1_2 Depth=1
	movb	$1, %cl
	testq	%rsi, %rsi
	movq	%r9, %rdi
	jne	.LBB1_2
.LBB1_23:
	xorl	%edi, %edi
.LBB1_24:                               # %pdf_nextlinestart.exit.thread
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	pdf_nextobject, .Lfunc_end1-pdf_nextobject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_20
	.quad	.LBB1_7
	.quad	.LBB1_20
	.quad	.LBB1_20
	.quad	.LBB1_7
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_20
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_7
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_24
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_20
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_20
.LJTI1_1:
	.quad	.LBB1_20
	.quad	.LBB1_7
	.quad	.LBB1_20
	.quad	.LBB1_20
	.quad	.LBB1_7
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_20
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_7
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_20
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_20

	.text
	.p2align	4, 0x90
	.type	flatedecode,@function
flatedecode:                            # @flatedecode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$8312, %rsp             # imm = 0x2078
.Lcfi19:
	.cfi_def_cfa_offset 8368
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movl	%edx, %r13d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testq	%rbp, %rbp
	je	.LBB2_3
# BB#1:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rsp)
	movq	$0, 80(%rsp)
	movq	%rbx, (%rsp)
	movl	%ebp, 8(%rsp)
	leaq	112(%rsp), %r12
	movq	%r12, 24(%rsp)
	movl	$8192, 32(%rsp)         # imm = 0x2000
	movq	%rsp, %rdi
	movl	$.L.str.44, %esi
	movl	$112, %edx
	callq	inflateInit_
	testl	%eax, %eax
	je	.LBB2_4
# BB#2:
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB2_30:
	movl	$-104, %r14d
	jmp	.LBB2_31
.LBB2_3:
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB2_31
.LBB2_4:                                # %.preheader
	xorl	%r14d, %r14d
	movq	%rsp, %rbx
	cmpl	$0, 8(%rsp)
	jne	.LBB2_7
	jmp	.LBB2_15
.LBB2_5:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%r12, 24(%rsp)
	movl	$8192, 32(%rsp)         # imm = 0x2000
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_7 Depth=1
	cmpl	$0, 8(%rsp)
	je	.LBB2_15
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	inflate
	movl	%eax, %ecx
	testl	%ecx, %ecx
	jne	.LBB2_14
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	cmpl	$0, 32(%rsp)
	jne	.LBB2_6
# BB#9:                                 #   in Loop: Header=BB2_7 Depth=1
	movl	$8192, %edx             # imm = 0x2000
	movl	%r13d, %edi
	movq	%r12, %rsi
	callq	cli_writen
	cltq
	addq	%rax, %r14
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB2_5
# BB#10:                                #   in Loop: Header=BB2_7 Depth=1
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB2_5
# BB#11:                                #   in Loop: Header=BB2_7 Depth=1
	cmpq	%rax, %r14
	jle	.LBB2_5
# BB#12:
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	%rsp, %rdi
	callq	inflateEnd
	movl	$-104, %r14d
	testb	$1, 41(%r15)
	je	.LBB2_31
# BB#13:
	movq	(%r15), %rax
	movq	$.L.str.47, (%rax)
	movl	$1, %r14d
	jmp	.LBB2_31
.LBB2_14:
	cmpl	$1, %ecx
	jne	.LBB2_25
.LBB2_15:
	movl	32(%rsp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	je	.LBB2_17
# BB#16:
	movl	$8192, %edx             # imm = 0x2000
	subl	%eax, %edx
	leaq	112(%rsp), %rsi
	movl	%r13d, %edi
	callq	cli_writen
	testl	%eax, %eax
	js	.LBB2_27
.LBB2_17:
	movq	16(%rsp), %rsi
	movq	40(%rsp), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	movq	%rax, %rbp
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB2_19
# BB#18:
	movl	12(%rax), %r8d
.LBB2_19:
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	movq	%rbp, %rcx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	cli_dbgmsg
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB2_24
# BB#20:
	movl	12(%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB2_24
# BB#21:
	movq	40(%rsp), %rax
	xorl	%edx, %edx
	divq	16(%rsp)
	cmpq	%rcx, %rax
	jbe	.LBB2_24
# BB#22:
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rsp, %rdi
	callq	inflateEnd
	movl	$-104, %r14d
	testb	$1, 41(%r15)
	je	.LBB2_31
# BB#23:
	movq	(%r15), %rax
	movq	$.L.str.52, (%rax)
	movl	$1, %r14d
	jmp	.LBB2_31
.LBB2_24:
	movq	%rsp, %rdi
	callq	inflateEnd
	testl	%eax, %eax
	movl	$-104, %r14d
	cmovel	%eax, %r14d
	jmp	.LBB2_31
.LBB2_25:
	movq	48(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB2_28
# BB#26:
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB2_29
.LBB2_27:
	movl	$-123, %r14d
.LBB2_31:
	movl	%r14d, %eax
	addq	$8312, %rsp             # imm = 0x2078
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_28:
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	movl	%ecx, %edx
	callq	cli_dbgmsg
.LBB2_29:
	movq	%rsp, %rdi
	callq	inflateEnd
	jmp	.LBB2_30
.Lfunc_end2:
	.size	flatedecode, .Lfunc_end2-flatedecode
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in cli_pdf(%s)\n"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"cli_pdf: scanning %lu bytes\n"
	.size	.L.str.1, 29

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%PDF-1."
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%%EOF"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"trailer"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Encrypt"
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Encrypted PDF files not yet supported\n"
	.size	.L.str.6, 39

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"xref"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"endobj"
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cli_pdf: Object number missing\n"
	.size	.L.str.9, 32

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cli_pdf: Generation number missing\n"
	.size	.L.str.10, 36

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"obj"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Indirect object missing \"obj\"\n"
	.size	.L.str.12, 31

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"No matching endobj\n"
	.size	.L.str.13, 20

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"stream"
	.size	.L.str.14, 7

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Length "
	.size	.L.str.15, 8

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	" 0 R"
	.size	.L.str.16, 5

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Length is in indirect obj %ld\n"
	.size	.L.str.17, 31

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\n%ld 0 obj"
	.size	.L.str.18, 11

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"length in '%s' %ld\n"
	.size	.L.str.19, 20

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Couldn't find '%s'\n"
	.size	.L.str.20, 20

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Length2 "
	.size	.L.str.21, 9

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Predictor "
	.size	.L.str.22, 11

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"FlateDecode"
	.size	.L.str.23, 12

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"ASCII85Decode"
	.size	.L.str.24, 14

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Embedded fonts not yet supported\n"
	.size	.L.str.25, 34

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Predictor %d not honoured for embedded image\n"
	.size	.L.str.26, 46

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"endstream\n"
	.size	.L.str.27, 11

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"endstream\r"
	.size	.L.str.28, 11

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"No endstream\n"
	.size	.L.str.29, 14

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%s/pdfXXXXXX"
	.size	.L.str.30, 13

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"cli_pdf: can't create temporary file %s: %s\n"
	.size	.L.str.31, 45

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Empty stream\n"
	.size	.L.str.32, 14

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"cli_pdf: Incorrect Length field in file attempting to recover\n"
	.size	.L.str.33, 63

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"length %ld, calculated_streamlen %ld isFlate %d isASCII85 %d\n"
	.size	.L.str.34, 62

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"cli_pdf: writing %lu bytes from the stream\n"
	.size	.L.str.35, 44

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"cli_pdf: not scanning duplicate embedded file '%s'\n"
	.size	.L.str.36, 52

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"cli_pdf: extracted file %d to %s\n"
	.size	.L.str.37, 34

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"cli_pdf: number of files exceeded %u\n"
	.size	.L.str.38, 38

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"cli_pdf: returning %d\n"
	.size	.L.str.39, 23

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"Bad compression in flate stream\n"
	.size	.L.str.40, 33

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"cli_pdf: Bad compressed block length in flate stream\n"
	.size	.L.str.41, 54

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"cli_pdf: flatedecode %lu bytes\n"
	.size	.L.str.42, 32

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"cli_pdf: flatedecode len == 0\n"
	.size	.L.str.43, 31

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"1.2.8"
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"cli_pdf: inflateInit failed"
	.size	.L.str.45, 28

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"cli_pdf: flatedecode size exceeded (%lu)\n"
	.size	.L.str.46, 42

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"PDF.ExceededFileSize"
	.size	.L.str.47, 21

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"pdf: after writing %lu bytes, got error \"%s\" inflating PDF attachment\n"
	.size	.L.str.48, 71

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"pdf: after writing %lu bytes, got error %d inflating PDF attachment\n"
	.size	.L.str.49, 69

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"cli_pdf: flatedecode in=%lu out=%lu ratio %lu (max %u)\n"
	.size	.L.str.50, 56

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"cli_pdf: flatedecode Max ratio reached\n"
	.size	.L.str.51, 40

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"Oversized.PDF"
	.size	.L.str.52, 14

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"~>"
	.size	.L.str.53, 3

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"ascii85decode: no EOF marker found\n"
	.size	.L.str.54, 36

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"cli_pdf: ascii85decode %lu bytes\n"
	.size	.L.str.55, 34

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"ascii85decode: unexpected 'z'\n"
	.size	.L.str.56, 31

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"ascii85decode: quintet %d\n"
	.size	.L.str.57, 27

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"ascii85Decode: only 1 byte in last quintet\n"
	.size	.L.str.58, 44

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"ascii85Decode: invalid character 0x%x, len %lu\n"
	.size	.L.str.59, 48


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
