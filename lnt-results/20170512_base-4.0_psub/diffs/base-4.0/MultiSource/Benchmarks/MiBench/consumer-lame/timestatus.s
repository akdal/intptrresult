	.text
	.file	"timestatus.bc"
	.globl	ts_real_time
	.p2align	4, 0x90
	.type	ts_real_time,@function
ts_real_time:                           # @ts_real_time
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %rdi
	callq	time
	testq	%rbx, %rbx
	movq	8(%rsp), %rdi
	je	.LBB0_2
# BB#1:                                 # %._crit_edge
	movq	ts_real_time.initial_time(%rip), %rsi
	jmp	.LBB0_3
.LBB0_2:
	movq	%rdi, ts_real_time.initial_time(%rip)
	movq	%rdi, %rsi
.LBB0_3:
	callq	difftime
	cvtsd2ss	%xmm0, %xmm0
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	ts_real_time, .Lfunc_end0-ts_real_time
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1232348160              # float 1.0E+6
	.text
	.globl	ts_process_time
	.p2align	4, 0x90
	.type	ts_process_time,@function
ts_process_time:                        # @ts_process_time
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
.Lcfi4:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	clock
	testq	%rbx, %rbx
	je	.LBB1_2
# BB#1:                                 # %._crit_edge
	movq	ts_process_time.initial_time(%rip), %rcx
	jmp	.LBB1_3
.LBB1_2:
	movq	%rax, ts_process_time.initial_time(%rip)
	movq	%rax, %rcx
.LBB1_3:
	subq	%rcx, %rax
	cvtsi2ssq	%rax, %xmm0
	divss	.LCPI1_0(%rip), %xmm0
	popq	%rbx
	retq
.Lfunc_end1:
	.size	ts_process_time, .Lfunc_end1-ts_process_time
	.cfi_endproc

	.globl	ts_calc_times
	.p2align	4, 0x90
	.type	ts_calc_times,@function
ts_calc_times:                          # @ts_calc_times
	.cfi_startproc
# BB#0:
	testq	%rdx, %rdx
	jle	.LBB2_4
# BB#1:
	movss	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	cvtsi2ssq	%rcx, %xmm0
	mulss	%xmm1, %xmm0
	cvtsi2ssq	%rdx, %xmm2
	divss	%xmm2, %xmm0
	movss	%xmm0, 4(%rdi)
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%esi, %xmm2
	mulss	%xmm0, %xmm2
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm2
	jbe	.LBB2_3
# BB#2:
	movslq	%r8d, %rax
	imulq	%rcx, %rax
	xorps	%xmm3, %xmm3
	cvtsi2ssq	%rax, %xmm3
	divss	%xmm2, %xmm3
.LBB2_3:
	movss	%xmm3, 8(%rdi)
	subss	%xmm1, %xmm0
	movss	%xmm0, 12(%rdi)
	retq
.LBB2_4:
	movq	$0, 4(%rdi)
	xorps	%xmm0, %xmm0
	movss	%xmm0, 12(%rdi)
	retq
.Lfunc_end2:
	.size	ts_calc_times, .Lfunc_end2-ts_calc_times
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1232348160              # float 1.0E+6
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_1:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_2:
	.quad	4602678819172646912     # double 0.5
.LCPI3_3:
	.quad	4636737291354636288     # double 100
.LCPI3_4:
	.quad	4633641066610819072     # double 60
	.text
	.globl	timestatus
	.p2align	4, 0x90
	.type	timestatus,@function
timestatus:                             # @timestatus
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 128
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movl	%edi, %r15d
	leaq	64(%rsp), %rdi
	callq	time
	testq	%rbp, %rbp
	movq	64(%rsp), %rdi
	jne	.LBB3_1
# BB#2:
	movq	%rdi, ts_real_time.initial_time(%rip)
	movq	%rdi, %rsi
	jmp	.LBB3_3
.LBB3_1:                                # %._crit_edge.i
	movq	ts_real_time.initial_time(%rip), %rsi
.LBB3_3:                                # %ts_real_time.exit
	callq	difftime
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	clock
	testq	%rbp, %rbp
	jne	.LBB3_5
# BB#4:
	movq	%rax, ts_process_time.initial_time(%rip)
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$74, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB3_14
.LBB3_5:
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm3
	subq	ts_process_time.initial_time(%rip), %rax
	cvtsi2ssq	%rax, %xmm2
	divss	.LCPI3_0(%rip), %xmm2
	testq	%rbp, %rbp
	jle	.LBB3_6
# BB#7:
	cvtsi2ssq	%rbx, %xmm4
	movaps	%xmm4, %xmm1
	mulss	%xmm3, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rbp, %xmm0
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm5
	subss	%xmm3, %xmm5
	mulss	%xmm2, %xmm4
	divss	%xmm0, %xmm4
	cvtsi2ssl	%r15d, %xmm6
	mulss	%xmm4, %xmm6
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm6
	jbe	.LBB3_9
# BB#8:
	movslq	%r14d, %rax
	imulq	%rbx, %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	divss	%xmm6, %xmm0
.LBB3_9:
	cvtss2sd	%xmm4, %xmm4
	addsd	.LCPI3_2(%rip), %xmm4
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	cvtps2pd	%xmm5, %xmm1
	addpd	.LCPI3_1(%rip), %xmm1
	jmp	.LBB3_10
.LBB3_6:
	movapd	.LCPI3_1(%rip), %xmm1   # xmm1 = [5.000000e-01,5.000000e-01]
	movsd	.LCPI3_2(%rip), %xmm4   # xmm4 = mem[0],zero
	xorps	%xmm0, %xmm0
.LBB3_10:                               # %ts_calc_times.exit21
	cmpq	$1, %rbx
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	jle	.LBB3_11
# BB#12:
	xorps	%xmm5, %xmm5
	cvtsi2sdq	%rbp, %xmm5
	mulsd	.LCPI3_3(%rip), %xmm5
	decq	%rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	xorps	%xmm6, %xmm6
	cvtsi2sdq	%rbx, %xmm6
	divsd	%xmm6, %xmm5
	cvttsd2si	%xmm5, %r8d
	jmp	.LBB3_13
.LBB3_11:                               # %ts_calc_times.exit21._crit_edge
	decq	%rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	$100, %r8d
.LBB3_13:
	xorps	%xmm6, %xmm6
	cvtss2sd	%xmm2, %xmm6
	movsd	.LCPI3_2(%rip), %xmm5   # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm6
	cvttsd2si	%xmm6, %rdi
	movabsq	$5247073869855161349, %rsi # imm = 0x48D159E26AF37C05
	movq	%rdi, %rax
	imulq	%rsi
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$10, %rdx
	leal	(%rdx,%rax), %r9d
	movsd	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero
	divsd	%xmm2, %xmm6
	cvttsd2si	%xmm6, %rbp
	movabsq	$-8608480567731124087, %rcx # imm = 0x8888888888888889
	movq	%rbp, %rax
	imulq	%rcx
	addq	%rbp, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$5, %rdx
	addl	%eax, %edx
	imull	$60, %edx, %eax
	subl	%eax, %ebp
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rdi, %rax
	imulq	%rcx
	addq	%rdi, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$5, %rdx
	addl	%eax, %edx
	imull	$60, %edx, %eax
	subl	%eax, %edi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	cvttsd2si	%xmm4, %rdi
	movq	%rdi, %rax
	imulq	%rsi
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$10, %rdx
	addl	%eax, %edx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	divsd	%xmm2, %xmm4
	cvttsd2si	%xmm4, %rbp
	movq	%rbp, %rax
	imulq	%rcx
	addq	%rbp, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$5, %rdx
	addl	%eax, %edx
	imull	$60, %edx, %eax
	subl	%eax, %ebp
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %rax
	imulq	%rcx
	addq	%rdi, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$5, %rdx
	addl	%eax, %edx
	imull	$60, %edx, %eax
	subl	%eax, %edi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	cvtss2sd	%xmm3, %xmm3
	addsd	%xmm5, %xmm3
	cvttsd2si	%xmm3, %r14
	movq	%r14, %rax
	imulq	%rsi
	movq	%rdx, %rbp
	movq	%rbp, %rax
	shrq	$63, %rax
	shrq	$10, %rbp
	addl	%eax, %ebp
	divsd	%xmm2, %xmm3
	cvttsd2si	%xmm3, %rdi
	movq	%rdi, %rax
	imulq	%rcx
	addq	%rdi, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$5, %rdx
	addl	%eax, %edx
	imull	$60, %edx, %eax
	subl	%eax, %edi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%r14, %rax
	imulq	%rcx
	addq	%r14, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$5, %rdx
	addl	%eax, %edx
	imull	$60, %edx, %eax
	subl	%eax, %r14d
	movapd	%xmm1, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	cvttsd2si	%xmm3, %r15
	movq	%r15, %rax
	imulq	%rsi
	movq	%rdx, %r10
	movq	%r10, %rax
	shrq	$63, %rax
	shrq	$10, %r10
	addl	%eax, %r10d
	divsd	%xmm2, %xmm3
	cvttsd2si	%xmm3, %r13
	movq	%r13, %rax
	imulq	%rcx
	addq	%r13, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$5, %rdx
	addl	%eax, %edx
	imull	$60, %edx, %eax
	subl	%eax, %r13d
	movq	%r15, %rax
	imulq	%rcx
	addq	%r15, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$5, %rdx
	addl	%eax, %edx
	imull	$60, %edx, %eax
	subl	%eax, %r15d
	cvttsd2si	%xmm1, %rbx
	movq	%rbx, %rax
	imulq	%rsi
	movq	%rdx, %r12
	movq	%r12, %rax
	shrq	$63, %rax
	shrq	$10, %r12
	addl	%eax, %r12d
	divsd	%xmm2, %xmm1
	cvttsd2si	%xmm1, %r11
	movq	%r11, %rax
	imulq	%rcx
	addq	%r11, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$5, %rdx
	addl	%eax, %edx
	imull	$60, %edx, %eax
	subl	%eax, %r11d
	movq	%rbx, %rax
	imulq	%rcx
	addq	%rbx, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$5, %rdx
	addl	%eax, %edx
	imull	$60, %edx, %eax
	subl	%eax, %ebx
	cvtss2sd	%xmm0, %xmm0
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movb	$1, %al
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	pushq	%rbx
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)               # 8-byte Folded Reload
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	144(%rsp)               # 8-byte Folded Reload
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)               # 8-byte Folded Reload
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$112, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -112
	movq	stderr(%rip), %rdi
	callq	fflush
.LBB3_14:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	timestatus, .Lfunc_end3-timestatus
	.cfi_endproc

	.type	ts_real_time.initial_time,@object # @ts_real_time.initial_time
	.local	ts_real_time.initial_time
	.comm	ts_real_time.initial_time,8,8
	.type	ts_process_time.initial_time,@object # @ts_process_time.initial_time
	.local	ts_process_time.initial_time
	.comm	ts_process_time.initial_time,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"    Frame          |  CPU/estimated  |  time/estimated | play/CPU |   ETA\n"
	.size	.L.str, 75

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\r%6ld/%6ld(%3d%%)|%2d:%02d:%02d/%2d:%02d:%02d|%2d:%02d:%02d/%2d:%02d:%02d|%10.4f|%2d:%02d:%02d "
	.size	.L.str.1, 96


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
