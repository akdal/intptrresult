	.text
	.file	"z03.bc"
	.globl	InitFiles
	.p2align	4, 0x90
	.type	InitFiles,@function
InitFiles:                              # @InitFiles
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	$-88, %rbx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, file_type+88(%rbx)
	addq	$8, %rbx
	jne	.LBB0_1
# BB#5:                                 # %.preheader.preheader
	movq	$-64, %rbx
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_6 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_9:                                #   in Loop: Header=BB0_6 Depth=1
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, file_path+64(%rbx)
	addq	$8, %rbx
	jne	.LBB0_6
# BB#10:
	movl	$56, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB0_12
# BB#11:
	movq	no_fpos(%rip), %r8
	movl	$3, %edi
	movl	$1, %esi
	movl	$.L.str.32, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_12:                               # %ftab_new.exit
	movl	$3, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 36(%rbx)
	movups	%xmm0, 20(%rbx)
	movups	%xmm0, 4(%rbx)
	movl	$0, 52(%rbx)
	movq	%rbx, file_tab(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_13
# BB#14:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_15
.LBB0_13:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_15:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, empty_path(%rip)
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.1, %esi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_16
# BB#17:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_18
.LBB0_16:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_18:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	empty_path(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_21
# BB#19:
	testq	%rcx, %rcx
	je	.LBB0_21
# BB#20:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_21:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_24
# BB#22:
	testq	%rax, %rax
	je	.LBB0_24
# BB#23:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_24:
	popq	%rbx
	retq
.Lfunc_end0:
	.size	InitFiles, .Lfunc_end0-InitFiles
	.cfi_endproc

	.globl	AddToPath
	.p2align	4, 0x90
	.type	AddToPath,@function
AddToPath:                              # @AddToPath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_1
# BB#2:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_3
.LBB1_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_3:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movslq	%ebp, %rcx
	movq	file_path(,%rcx,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_6
# BB#4:
	testq	%rcx, %rcx
	je	.LBB1_6
# BB#5:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_6:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_9
# BB#7:
	testq	%rax, %rax
	je	.LBB1_9
# BB#8:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	AddToPath, .Lfunc_end1-AddToPath
	.cfi_endproc

	.globl	DefineFile
	.p2align	4, 0x90
	.type	DefineFile,@function
DefineFile:                             # @DefineFile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 64
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%ecx, %ebx
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	cmpl	$11, %ebx
	jge	.LBB2_1
# BB#2:
	testl	%ebx, %ebx
	jne	.LBB2_14
# BB#3:
	movq	%r13, %rdi
	callq	strlen
	cmpl	$3, %eax
	jl	.LBB2_14
# BB#4:
	movslq	%eax, %rbp
	cmpb	$46, -3(%r13,%rbp)
	jne	.LBB2_14
# BB#5:
	cmpb	$108, -2(%r13,%rbp)
	jne	.LBB2_9
# BB#6:
	cmpb	$100, -1(%r13,%rbp)
	jne	.LBB2_9
# BB#7:
	cmpb	$0, (%r13,%rbp)
	jne	.LBB2_9
# BB#8:                                 # %.thread98
	movl	$3, %edi
	movl	$3, %esi
	movl	$.L.str.5, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	%r13, %r9
	callq	Error
	cmpb	$46, -3(%r13,%rbp)
	jne	.LBB2_14
.LBB2_9:
	cmpb	$108, -2(%r13,%rbp)
	jne	.LBB2_14
# BB#10:
	cmpb	$105, -1(%r13,%rbp)
	jne	.LBB2_14
# BB#11:
	cmpb	$0, (%r13,%rbp)
	jne	.LBB2_14
# BB#12:
	movl	$3, %edi
	movl	$4, %esi
	movl	$.L.str.7, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	%r13, %r9
	jmp	.LBB2_13
.LBB2_1:                                # %.thread
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
.LBB2_13:                               # %.thread101
	callq	Error
.LBB2_14:                               # %.thread101
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r12, %rdi
	callq	strlen
	addq	%rbp, %rax
	cmpq	$2048, %rax             # imm = 0x800
	jb	.LBB2_16
# BB#15:
	movq	no_fpos(%rip), %r8
	movq	%r12, (%rsp)
	movl	$3, %edi
	movl	$5, %esi
	movl	$.L.str.8, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r13, %r9
	callq	Error
.LBB2_16:
	movl	$11, %edi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	MakeWordTwo
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_17
# BB#18:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_19
.LBB2_17:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_19:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movslq	%ebx, %rcx
	movq	file_type(,%rcx,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_22
# BB#20:
	testq	%rcx, %rcx
	je	.LBB2_22
# BB#21:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_22:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB2_25
# BB#23:
	testq	%rax, %rax
	je	.LBB2_25
# BB#24:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_25:
	movl	%r14d, 48(%rbp)
	movl	$0, 56(%rbp)
	movl	$0, 60(%rbp)
	shll	$12, %ebx
	andl	$4190208, %ebx          # imm = 0x3FF000
	movl	$2143293439, %eax       # imm = 0x7FC00FFF
	andl	40(%rbp), %eax
	orl	%ebx, %eax
	movl	%eax, 40(%rbp)
	movl	$file_tab, %esi
	movq	%rbp, %rdi
	callq	ftab_insert
	movzwl	40(%rbp), %eax
	andl	$4095, %eax             # imm = 0xFFF
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	DefineFile, .Lfunc_end2-DefineFile
	.cfi_endproc

	.p2align	4, 0x90
	.type	ftab_insert,@function
ftab_insert:                            # @ftab_insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 80
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	(%r12), %r14
	movl	4(%r14), %eax
	movslq	(%r14), %rbp
	leal	-1(%rbp), %ecx
	cmpl	%ecx, %eax
	jne	.LBB3_14
# BB#1:
	movq	%r12, 16(%rsp)          # 8-byte Spill
	leal	(%rbp,%rbp), %r12d
	movq	%rbp, %rdi
	shlq	$5, %rdi
	orq	$8, %rdi
	callq	malloc
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB3_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$3, %edi
	movl	$1, %esi
	movl	$.L.str.32, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB3_3:
	movl	%r12d, (%r13)
	movl	$0, 4(%r13)
	testl	%ebp, %ebp
	jle	.LBB3_5
# BB#4:                                 # %.lr.ph.i.i
	movq	%r13, %rdi
	addq	$8, %rdi
	decl	%r12d
	shlq	$4, %r12
	addq	$16, %r12
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
.LBB3_5:                                # %ftab_new.exit.i
	movq	%r13, 8(%rsp)
	cmpl	$0, 4(%r14)
	jle	.LBB3_8
# BB#6:                                 # %.lr.ph42.preheader
	leaq	24(%r14), %rbx
	xorl	%ebp, %ebp
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph42
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	callq	ftab_insert
	movslq	4(%r14), %rax
	incq	%rbp
	addq	$16, %rbx
	cmpq	%rax, %rbp
	jl	.LBB3_7
.LBB3_8:                                # %.preheader
	movl	(%r14), %eax
	testl	%eax, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	jle	.LBB3_13
# BB#9:                                 # %.lr.ph.preheader
	leaq	16(%r14), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_10 Depth=1
	callq	DisposeObject
	movl	(%r14), %eax
.LBB3_12:                               #   in Loop: Header=BB3_10 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	addq	$16, %rbp
	cmpq	%rcx, %rbx
	jl	.LBB3_10
.LBB3_13:                               # %ftab_rehash.exit
	movq	%r14, %rdi
	callq	free
	movq	8(%rsp), %r14
	movq	%r14, (%r12)
	movl	4(%r14), %eax
.LBB3_14:
	leal	1(%rax), %ebp
	movl	%ebp, 4(%r14)
	cmpl	$65535, %eax            # imm = 0xFFFF
	jl	.LBB3_16
# BB#15:
	leaq	32(%r15), %r8
	movl	$3, %edi
	movl	$2, %esi
	movl	$.L.str.33, %edx
	movl	$1, %ecx
	movl	$65535, %r9d            # imm = 0xFFFF
	xorl	%eax, %eax
	callq	Error
.LBB3_16:
	movzbl	64(%r15), %edx
	leaq	65(%r15), %rcx
	.p2align	4, 0x90
.LBB3_17:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %eax
	movzbl	(%rcx), %esi
	leal	(%rsi,%rax), %edx
	incq	%rcx
	testl	%esi, %esi
	jne	.LBB3_17
# BB#18:
	movq	(%r12), %rcx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	(%rcx)
	movslq	%edx, %rbx
	shlq	$4, %rbx
	cmpq	$0, 16(%rcx,%rbx)
	jne	.LBB3_23
# BB#19:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_20
# BB#21:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_22
.LBB3_20:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_22:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	(%r12), %rcx
	movq	%rax, 16(%rcx,%rbx)
.LBB3_23:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_24
# BB#25:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_26
.LBB3_24:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_26:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r12), %rcx
	movq	16(%rcx,%rbx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_29
# BB#27:
	testq	%rcx, %rcx
	je	.LBB3_29
# BB#28:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_29:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB3_32
# BB#30:
	testq	%rax, %rax
	je	.LBB3_32
# BB#31:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_32:
	movslq	%ebp, %rax
	andl	$4095, %ebp             # imm = 0xFFF
	movl	$-4096, %ecx            # imm = 0xF000
	andl	40(%r15), %ecx
	orl	%ebp, %ecx
	movl	%ecx, 40(%r15)
	movq	(%r12), %rcx
	shlq	$4, %rax
	movq	%r15, 8(%rcx,%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	ftab_insert, .Lfunc_end3-ftab_insert
	.cfi_endproc

	.globl	FirstFile
	.p2align	4, 0x90
	.type	FirstFile,@function
FirstFile:                              # @FirstFile
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movq	file_type(,%rax,8), %rax
	movq	8(%rax), %rax
	cmpb	$17, 32(%rax)
	jne	.LBB4_2
# BB#1:
	xorl	%eax, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
	.p2align	4, 0x90
.LBB4_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB4_2
# BB#3:
	movzwl	40(%rax), %eax
	andl	$4095, %eax             # imm = 0xFFF
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.Lfunc_end4:
	.size	FirstFile, .Lfunc_end4-FirstFile
	.cfi_endproc

	.globl	NextFile
	.p2align	4, 0x90
	.type	NextFile,@function
NextFile:                               # @NextFile
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	file_tab(%rip), %rax
	shlq	$4, %rdi
	movq	8(%rax,%rdi), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rax
	cmpb	$17, 32(%rax)
	jne	.LBB5_2
# BB#1:
	xorl	%eax, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
	.p2align	4, 0x90
.LBB5_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB5_2
# BB#3:
	movzwl	40(%rax), %eax
	andl	$4095, %eax             # imm = 0xFFF
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.Lfunc_end5:
	.size	NextFile, .Lfunc_end5-NextFile
	.cfi_endproc

	.globl	FileNum
	.p2align	4, 0x90
	.type	FileNum,@function
FileNum:                                # @FileNum
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 40
	subq	$536, %rsp              # imm = 0x218
.Lcfi37:
	.cfi_def_cfa_offset 576
.Lcfi38:
	.cfi_offset %rbx, -40
.Lcfi39:
	.cfi_offset %r12, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	strlen
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	addq	%rbx, %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB6_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movq	%r14, (%rsp)
	movl	$3, %edi
	movl	$6, %esi
	movl	$.L.str.8, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r9
	callq	Error
.LBB6_2:
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcat
	movq	file_tab(%rip), %rcx
	movzbl	16(%rsp), %esi
	leaq	17(%rsp), %rdx
	.p2align	4, 0x90
.LBB6_3:                                # =>This Inner Loop Header: Depth=1
	movl	%esi, %eax
	movzbl	(%rdx), %edi
	leal	(%rdi,%rax), %esi
	incq	%rdx
	testl	%edi, %edi
	jne	.LBB6_3
# BB#4:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	(%rcx)
	movslq	%edx, %rax
	shlq	$4, %rax
	movq	16(%rcx,%rax), %r15
	testq	%r15, %r15
	je	.LBB6_14
# BB#5:                                 # %.preheader39.i.preheader
	leaq	16(%rsp), %r14
	movq	%r15, %r12
	.p2align	4, 0x90
.LBB6_6:                                # %.preheader39.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
	movq	8(%r12), %r12
	cmpq	%r15, %r12
	je	.LBB6_14
# BB#7:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB6_6 Depth=1
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB6_8:                                # %.preheader.i
                                        #   Parent Loop BB6_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB6_8
# BB#9:                                 #   in Loop: Header=BB6_6 Depth=1
	leaq	64(%rbx), %rsi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB6_6
# BB#10:                                # %ftab_retrieve.exit
	testq	%rbx, %rbx
	je	.LBB6_14
# BB#11:
	movzwl	40(%rbx), %eax
	andl	$4095, %eax             # imm = 0xFFF
	jmp	.LBB6_15
.LBB6_14:
	xorl	%eax, %eax
.LBB6_15:                               # %ftab_retrieve.exit.thread
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	FileNum, .Lfunc_end6-FileNum
	.cfi_endproc

	.globl	DatabaseFileNum
	.p2align	4, 0x90
	.type	DatabaseFileNum,@function
DatabaseFileNum:                        # @DatabaseFileNum
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -40
.Lcfi48:
	.cfi_offset %r12, -32
.Lcfi49:
	.cfi_offset %r14, -24
.Lcfi50:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	jmp	.LBB7_1
	.p2align	4, 0x90
.LBB7_12:                               #   in Loop: Header=BB7_1 Depth=1
	addq	$32, %rbx
	movq	%rbx, %r14
.LBB7_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	file_tab(%rip), %rax
	movzwl	2(%r14), %r15d
	movq	%r15, %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rbx
	movl	40(%rbx), %eax
	shrl	$12, %eax
	andl	$1023, %eax             # imm = 0x3FF
	cmpl	$3, %eax
	je	.LBB7_11
# BB#2:                                 # %tailrecurse
                                        #   in Loop: Header=BB7_1 Depth=1
	movzwl	%ax, %eax
	cmpl	$10, %eax
	jne	.LBB7_3
# BB#13:                                #   in Loop: Header=BB7_1 Depth=1
	cmpw	$0, 34(%rbx)
	leaq	32(%rbx), %r14
	jne	.LBB7_1
# BB#14:                                #   in Loop: Header=BB7_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$3, %edi
	movl	$7, %esi
	movl	$.L.str.9, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB7_1
	.p2align	4, 0x90
.LBB7_11:                               #   in Loop: Header=BB7_1 Depth=1
	cmpw	$0, 34(%rbx)
	jne	.LBB7_12
	jmp	.LBB7_16
.LBB7_3:                                # %tailrecurse
	cmpl	$1, %eax
	ja	.LBB7_15
# BB#4:
	testq	%rbx, %rbx
	jne	.LBB7_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_6:
	movq	8(%rbx), %r12
	cmpq	%rbx, %r12
	je	.LBB7_7
	.p2align	4, 0x90
.LBB7_8:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %r12
	cmpb	$0, 32(%r12)
	je	.LBB7_8
	jmp	.LBB7_9
.LBB7_15:
	movq	no_fpos(%rip), %r8
	xorl	%r15d, %r15d
	movl	$3, %edi
	movl	$8, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB7_16
.LBB7_7:
	movq	%rbx, %r12
.LBB7_9:                                # %FileName.exit
	addq	$64, %r12
	movl	$.L.str.4, %esi
	movq	%r12, %rdi
	callq	FileNum
	movw	%ax, %r15w
	testw	%r15w, %r15w
	je	.LBB7_10
.LBB7_16:                               # %.loopexit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB7_10:
	movl	$.L.str.4, %esi
	movl	$3, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	DefineFile              # TAILCALL
.Lfunc_end7:
	.size	DatabaseFileNum, .Lfunc_end7-DatabaseFileNum
	.cfi_endproc

	.globl	FileName
	.p2align	4, 0x90
	.type	FileName,@function
FileName:                               # @FileName
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 16
.Lcfi52:
	.cfi_offset %rbx, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	file_tab(%rip), %rax
	shlq	$4, %rdi
	movq	8(%rax,%rdi), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_2:
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB8_5
# BB#3:                                 # %.preheader.preheader
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB8_4:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB8_4
.LBB8_5:                                # %.loopexit
	addq	$64, %rbx
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	FileName, .Lfunc_end8-FileName
	.cfi_endproc

	.globl	FullFileName
	.p2align	4, 0x90
	.type	FullFileName,@function
FullFileName:                           # @FullFileName
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -24
.Lcfi57:
	.cfi_offset %r14, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	file_tab(%rip), %rax
	shlq	$4, %rdi
	movq	8(%rax,%rdi), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB9_2:
	movq	8(%rbx), %rax
	cmpl	$0, 40(%rbx)
	js	.LBB9_3
# BB#7:
	cmpq	%rbx, %rax
	je	.LBB9_10
# BB#8:                                 # %.preheader16.preheader
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB9_9:                                # %.preheader16
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB9_9
.LBB9_10:                               # %.loopexit17
	addq	$64, %rbx
	jmp	.LBB9_11
.LBB9_3:
	cmpq	%rbx, %rax
	je	.LBB9_6
# BB#4:                                 # %.preheader.preheader
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB9_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB9_5
.LBB9_6:                                # %.loopexit
	movl	FullFileName.ffbp(%rip), %eax
	leal	1(%rax), %ecx
	shrl	$31, %ecx
	leal	1(%rax,%rcx), %ecx
	andl	$-2, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %eax
	movl	%eax, FullFileName.ffbp(%rip)
	cltq
	shlq	$9, %rax
	leaq	FullFileName.ffbuff(%rax), %rdi
	addq	$64, %rbx
	movq	%rbx, %rsi
	callq	strcpy
	movslq	FullFileName.ffbp(%rip), %r14
	shlq	$9, %r14
	leaq	FullFileName.ffbuff(%r14), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	$7629870, FullFileName.ffbuff(%r14,%rax) # imm = 0x746C2E
.LBB9_11:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	FullFileName, .Lfunc_end9-FullFileName
	.cfi_endproc

	.globl	EchoFilePos
	.p2align	4, 0x90
	.type	EchoFilePos,@function
EchoFilePos:                            # @EchoFilePos
	.cfi_startproc
# BB#0:
	movl	bp(%rip), %eax
	leal	1(%rax), %ecx
	shrl	$31, %ecx
	leal	1(%rax,%rcx), %ecx
	andl	$-2, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %eax
	movl	%eax, bp(%rip)
	movslq	%eax, %rcx
	shlq	$9, %rcx
	movb	$0, buff(%rcx)
	cmpw	$0, 2(%rdi)
	je	.LBB10_2
# BB#1:
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 16
	callq	append_fpos
	movl	bp(%rip), %eax
	addq	$8, %rsp
.LBB10_2:
	cltq
	shlq	$9, %rax
	leaq	buff(%rax), %rax
	retq
.Lfunc_end10:
	.size	EchoFilePos, .Lfunc_end10-EchoFilePos
	.cfi_endproc

	.p2align	4, 0x90
	.type	append_fpos,@function
append_fpos:                            # @append_fpos
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 48
.Lcfi64:
	.cfi_offset %rbx, -48
.Lcfi65:
	.cfi_offset %r12, -40
.Lcfi66:
	.cfi_offset %r13, -32
.Lcfi67:
	.cfi_offset %r14, -24
.Lcfi68:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	file_tab(%rip), %rax
	movzwl	2(%r14), %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %r12
	testq	%r12, %r12
	jne	.LBB11_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.34, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB11_2:
	cmpw	$0, 34(%r12)
	je	.LBB11_3
# BB#4:
	leaq	32(%r12), %rdi
	callq	append_fpos
	movslq	bp(%rip), %r13
	movq	%r13, %rax
	shlq	$9, %rax
	leaq	buff(%rax), %r15
	movq	%r15, %rdi
	callq	strlen
	addq	$2, %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB11_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$3, %edi
	movl	$9, %esi
	movl	$.L.str.35, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r9
	callq	Error
	movl	bp(%rip), %r13d
.LBB11_6:
	movslq	%r13d, %rbx
	shlq	$9, %rbx
	leaq	buff(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movw	$32, buff(%rbx,%rax)
	movq	%r15, %rdi
	callq	strlen
	movw	$47, buff(%rbx,%rax)
	jmp	.LBB11_7
.LBB11_3:                               # %._crit_edge
	movl	bp(%rip), %r13d
.LBB11_7:
	movslq	%r13d, %rax
	shlq	$9, %rax
	leaq	buff(%rax), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	addq	$64, %r12
	movq	%r12, %rdi
	callq	strlen
	leaq	13(%rbx,%rax), %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB11_9
# BB#8:
	movq	no_fpos(%rip), %r8
	movl	$3, %edi
	movl	$10, %esi
	movl	$.L.str.35, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r9
	callq	Error
	movl	bp(%rip), %r13d
.LBB11_9:
	movslq	%r13d, %rbx
	shlq	$9, %rbx
	leaq	buff(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movw	$32, buff(%rbx,%rax)
	movq	%r15, %rdi
	callq	strlen
	movw	$34, buff(%rbx,%rax)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	strcat
	movslq	bp(%rip), %rbx
	shlq	$9, %rbx
	leaq	buff(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movw	$34, buff(%rbx,%rax)
	testl	$1048575, 4(%r14)       # imm = 0xFFFFF
	je	.LBB11_10
# BB#11:
	movq	%r15, %rdi
	callq	strlen
	movw	$32, buff(%rbx,%rax)
	movl	4(%r14), %edi
	andl	$1048575, %edi          # imm = 0xFFFFF
	callq	StringInt
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	strcat
	movslq	bp(%rip), %rbx
	shlq	$9, %rbx
	leaq	buff(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movw	$44, buff(%rbx,%rax)
	movl	4(%r14), %edi
	shrl	$20, %edi
	callq	StringInt
	movq	%r15, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	strcat                  # TAILCALL
.LBB11_10:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	append_fpos, .Lfunc_end11-append_fpos
	.cfi_endproc

	.globl	EchoAltFilePos
	.p2align	4, 0x90
	.type	EchoAltFilePos,@function
EchoAltFilePos:                         # @EchoAltFilePos
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 32
.Lcfi72:
	.cfi_offset %rbx, -32
.Lcfi73:
	.cfi_offset %r14, -24
.Lcfi74:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	bp(%rip), %eax
	leal	1(%rax), %ecx
	shrl	$31, %ecx
	leal	1(%rax,%rcx), %ecx
	andl	$-2, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %eax
	movl	%eax, bp(%rip)
	cltq
	shlq	$9, %rax
	movb	$0, buff(%rax)
	movzwl	2(%r14), %ecx
	testw	%cx, %cx
	je	.LBB12_3
# BB#1:
	leaq	buff(%rax), %rbx
	movzwl	%cx, %edi
	callq	FullFileName
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcat
	testl	$1048575, 4(%r14)       # imm = 0xFFFFF
	je	.LBB12_3
# BB#2:
	movslq	bp(%rip), %rbx
	shlq	$9, %rbx
	leaq	buff(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movw	$58, buff(%rbx,%rax)
	movl	4(%r14), %edi
	andl	$1048575, %edi          # imm = 0xFFFFF
	callq	StringInt
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	strcat
	movslq	bp(%rip), %rbx
	shlq	$9, %rbx
	leaq	buff(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movw	$58, buff(%rbx,%rax)
	movl	4(%r14), %edi
	shrl	$20, %edi
	callq	StringInt
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	strcat
.LBB12_3:
	movslq	bp(%rip), %rax
	shlq	$9, %rax
	leaq	buff(%rax), %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	EchoAltFilePos, .Lfunc_end12-EchoAltFilePos
	.cfi_endproc

	.globl	EchoFileSource
	.p2align	4, 0x90
	.type	EchoFileSource,@function
EchoFileSource:                         # @EchoFileSource
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 48
.Lcfi80:
	.cfi_offset %rbx, -48
.Lcfi81:
	.cfi_offset %r12, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movl	bp(%rip), %eax
	leal	1(%rax), %ecx
	shrl	$31, %ecx
	leal	1(%rax,%rcx), %ecx
	andl	$-2, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %r12d
	movl	%r12d, bp(%rip)
	movslq	%r12d, %rbp
	shlq	$9, %rbp
	movb	$0, buff(%rbp)
	testw	%r14w, %r14w
	je	.LBB13_22
# BB#1:
	leaq	buff(%rbp), %rdi
	callq	strlen
	movw	$32, buff(%rbp,%rax)
	movq	file_tab(%rip), %rax
	movzwl	%r14w, %r14d
	movq	%r14, %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %r15
	testq	%r15, %r15
	jne	.LBB13_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB13_3:
	movl	$4190208, %eax          # imm = 0x3FF000
	andl	40(%r15), %eax
	movl	bp(%rip), %ebx
	cmpl	$40960, %eax            # imm = 0xA000
	jne	.LBB13_8
# BB#4:
	movslq	%ebx, %rax
	shlq	$9, %rax
	leaq	buff(%rax), %rbx
	movq	MsgCat(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB13_5
# BB#6:
	movl	$3, %esi
	movl	$11, %edx
	movl	$.L.str.16, %ecx
	callq	catgets
	movq	%rax, %rsi
	jmp	.LBB13_7
.LBB13_5:
	movl	$.L.str.16, %esi
.LBB13_7:
	movq	%rbx, %rdi
	callq	strcat
	movslq	bp(%rip), %rbx
	movq	%rbx, %rbp
	shlq	$9, %rbp
	leaq	buff(%rbp), %rdi
	callq	strlen
	movw	$32, buff(%rbp,%rax)
.LBB13_8:                               # %._crit_edge
	movslq	%ebx, %rax
	shlq	$9, %rax
	leaq	buff(%rax), %rbx
	movq	MsgCat(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB13_9
# BB#10:
	movl	$3, %esi
	movl	$12, %edx
	movl	$.L.str.17, %ecx
	callq	catgets
	movq	%rax, %rsi
	jmp	.LBB13_11
.LBB13_9:
	movl	$.L.str.17, %esi
.LBB13_11:
	movq	%rbx, %rdi
	callq	strcat
	movslq	bp(%rip), %rbp
	shlq	$9, %rbp
	leaq	buff(%rbp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movw	$32, buff(%rbp,%rax)
	movq	%rbx, %rdi
	callq	strlen
	movw	$34, buff(%rbp,%rax)
	movl	%r14d, %edi
	callq	FullFileName
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcat
	movl	bp(%rip), %r12d
	movslq	%r12d, %rbp
	shlq	$9, %rbp
	leaq	buff(%rbp), %r14
	movq	%r14, %rdi
	callq	strlen
	movw	$34, buff(%rbp,%rax)
	cmpw	$0, 34(%r15)
	je	.LBB13_22
# BB#12:
	movq	%r14, %rdi
	callq	strlen
	movb	$0, buff+2(%rbp,%rax)
	movw	$10272, buff(%rbp,%rax) # imm = 0x2820
	jmp	.LBB13_13
	.p2align	4, 0x90
.LBB13_20:                              #   in Loop: Header=BB13_13 Depth=1
	movb	$0, 2(%rax)
	movw	$8236, (%rax)           # imm = 0x202C
	movq	%r14, %r15
.LBB13_13:                              # =>This Inner Loop Header: Depth=1
	movq	file_tab(%rip), %rax
	movzwl	34(%r15), %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %r14
	movslq	%r12d, %rax
	shlq	$9, %rax
	leaq	buff(%rax), %rbx
	movq	MsgCat(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB13_14
# BB#15:                                #   in Loop: Header=BB13_13 Depth=1
	movl	$3, %esi
	movl	$13, %edx
	movl	$.L.str.20, %ecx
	callq	catgets
	movq	%rax, %rsi
	jmp	.LBB13_16
	.p2align	4, 0x90
.LBB13_14:                              #   in Loop: Header=BB13_13 Depth=1
	movl	$.L.str.20, %esi
.LBB13_16:                              #   in Loop: Header=BB13_13 Depth=1
	movq	%rbx, %rdi
	callq	strcat
	movslq	bp(%rip), %rbp
	shlq	$9, %rbp
	leaq	buff(%rbp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movw	$32, buff(%rbp,%rax)
	movq	%rbx, %rdi
	callq	strlen
	movw	$34, buff(%rbp,%rax)
	leaq	64(%r14), %rsi
	movq	%rbx, %rdi
	callq	strcat
	movslq	bp(%rip), %rbp
	shlq	$9, %rbp
	leaq	buff(%rbp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movw	$34, buff(%rbp,%rax)
	movq	%rbx, %rdi
	callq	strlen
	movw	$32, buff(%rbp,%rax)
	movq	MsgCat(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB13_17
# BB#18:                                #   in Loop: Header=BB13_13 Depth=1
	movl	$3, %esi
	movl	$14, %edx
	movl	$.L.str.21, %ecx
	callq	catgets
	movq	%rax, %rsi
	jmp	.LBB13_19
	.p2align	4, 0x90
.LBB13_17:                              #   in Loop: Header=BB13_13 Depth=1
	movl	$.L.str.21, %esi
.LBB13_19:                              #   in Loop: Header=BB13_13 Depth=1
	movq	%rbx, %rdi
	callq	strcat
	movslq	bp(%rip), %rbp
	shlq	$9, %rbp
	leaq	buff(%rbp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movw	$32, buff(%rbp,%rax)
	movl	36(%r15), %edi
	andl	$1048575, %edi          # imm = 0xFFFFF
	callq	StringInt
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcat
	movzwl	34(%r14), %ebp
	movslq	bp(%rip), %r12
	movq	%r12, %rbx
	shlq	$9, %rbx
	leaq	buff(%rbx), %rdi
	callq	strlen
	cmpw	$0, %bp
	leaq	buff(%rbx,%rax), %rax
	jne	.LBB13_20
# BB#21:
	movw	$41, (%rax)
.LBB13_22:
	movslq	%r12d, %rax
	shlq	$9, %rax
	leaq	buff(%rax), %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	EchoFileSource, .Lfunc_end13-EchoFileSource
	.cfi_endproc

	.globl	EchoFileLine
	.p2align	4, 0x90
	.type	EchoFileLine,@function
EchoFileLine:                           # @EchoFileLine
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 32
.Lcfi88:
	.cfi_offset %rbx, -32
.Lcfi89:
	.cfi_offset %r14, -24
.Lcfi90:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	bp(%rip), %eax
	leal	1(%rax), %ecx
	shrl	$31, %ecx
	leal	1(%rax,%rcx), %ecx
	andl	$-2, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %eax
	movl	%eax, bp(%rip)
	movslq	%eax, %rcx
	shlq	$9, %rcx
	movb	$0, buff(%rcx)
	cmpw	$0, 2(%r15)
	je	.LBB14_3
# BB#1:
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	4(%r15), %edi
	je	.LBB14_3
# BB#2:
	leaq	buff(%rcx), %r14
	callq	StringInt
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	strcat
	movslq	bp(%rip), %rbx
	shlq	$9, %rbx
	leaq	buff(%rbx), %r14
	movq	%r14, %rdi
	callq	strlen
	movw	$44, buff(%rbx,%rax)
	movl	4(%r15), %edi
	shrl	$20, %edi
	callq	StringInt
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	strcat
	movl	bp(%rip), %eax
.LBB14_3:
	cltq
	shlq	$9, %rax
	leaq	buff(%rax), %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	EchoFileLine, .Lfunc_end14-EchoFileLine
	.cfi_endproc

	.globl	PosOfFile
	.p2align	4, 0x90
	.type	PosOfFile,@function
PosOfFile:                              # @PosOfFile
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 16
.Lcfi92:
	.cfi_offset %rbx, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	file_tab(%rip), %rax
	shlq	$4, %rdi
	movq	8(%rax,%rdi), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB15_2:
	addq	$32, %rbx
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end15:
	.size	PosOfFile, .Lfunc_end15-PosOfFile
	.cfi_endproc

	.globl	OpenFile
	.p2align	4, 0x90
	.type	OpenFile,@function
OpenFile:                               # @OpenFile
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi95:
	.cfi_def_cfa_offset 48
.Lcfi96:
	.cfi_offset %rbx, -24
.Lcfi97:
	.cfi_offset %r14, -16
	movl	%edx, %r10d
	movl	%esi, %ecx
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	file_tab(%rip), %rdx
	shlq	$4, %rdi
	movq	8(%rdx,%rdi), %rbx
	movq	8(%rbx), %rdx
	cmpq	%rbx, %rdx
	je	.LBB16_4
# BB#1:
	addq	$16, %rdx
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rdi
	leaq	16(%rdi), %rdx
	cmpb	$0, 32(%rdi)
	je	.LBB16_2
# BB#3:
	addq	$64, %rdi
	movl	40(%rbx), %eax
	shrl	$12, %eax
	andl	$1023, %eax             # imm = 0x3FF
	movq	file_mode(,%rax,8), %rsi
	callq	fopen
	movq	%rax, %r14
	jmp	.LBB16_14
.LBB16_4:
	leaq	64(%rbx), %rdi
	movslq	48(%rbx), %rdx
	movq	file_path(,%rdx,8), %rsi
	leaq	32(%rbx), %r9
	movl	40(%rbx), %eax
	shrl	$12, %eax
	andl	$1023, %eax             # imm = 0x3FF
	leaq	12(%rsp), %r11
	leaq	16(%rsp), %r8
	movl	%ecx, %edx
	movl	%r10d, %ecx
	pushq	%r11
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	pushq	file_mode(,%rax,8)
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	callq	SearchPath
	addq	$16, %rsp
.Lcfi100:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r14
	cmpq	$0, 16(%rsp)
	je	.LBB16_13
# BB#5:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB16_6
# BB#7:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB16_8
.LBB16_6:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB16_8:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB16_10
# BB#9:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB16_10:
	movq	%rax, zz_res(%rip)
	movq	16(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB16_13
# BB#11:
	testq	%rcx, %rcx
	je	.LBB16_13
# BB#12:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB16_13:
	movl	12(%rsp), %eax
	shll	$31, %eax
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	andl	40(%rbx), %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rbx)
.LBB16_14:
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end16:
	.size	OpenFile, .Lfunc_end16-OpenFile
	.cfi_endproc

	.p2align	4, 0x90
	.type	SearchPath,@function
SearchPath:                             # @SearchPath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	subq	$1368, %rsp             # imm = 0x558
.Lcfi107:
	.cfi_def_cfa_offset 1424
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	1432(%rsp), %rax
	movl	$0, (%rax)
	cmpb	$45, (%rbx)
	jne	.LBB17_3
# BB#1:
	cmpb	$0, 1(%rbx)
	je	.LBB17_2
.LBB17_3:                               # %.thread125
	movl	$.L.str.36, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	cmovneq	empty_path(%rip), %rbp
	movq	8(%rbp), %r13
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	cmpq	%rbp, %r13
	je	.LBB17_4
# BB#5:                                 # %.preheader.lr.ph
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r15, 8(%rsp)           # 8-byte Spill
	leaq	848(%rsp), %r15
	.p2align	4, 0x90
.LBB17_6:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_7 Depth 2
	movq	%r13, %r14
	.p2align	4, 0x90
.LBB17_7:                               #   Parent Loop BB17_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB17_7
# BB#8:                                 #   in Loop: Header=BB17_6 Depth=1
	leaq	64(%r14), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movq	32(%rsp), %r12          # 8-byte Reload
	je	.LBB17_9
# BB#10:                                #   in Loop: Header=BB17_6 Depth=1
	movq	%r12, %rdi
	callq	strlen
	leaq	1(%rbp,%rax), %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB17_12
# BB#11:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	32(%r14), %r8
	movl	$3, %edi
	movl	$15, %esi
	movl	$.L.str.38, %edx
	movl	$1, %ecx
	movl	$0, %eax
	movq	%rbx, %r9
	pushq	%r12
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.36
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi116:
	.cfi_adjust_cfa_offset -16
.LBB17_12:                              #   in Loop: Header=BB17_6 Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%r15, %rdi
	callq	strlen
	movw	$47, 848(%rsp,%rax)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	strcat
	jmp	.LBB17_13
	.p2align	4, 0x90
.LBB17_9:                               #   in Loop: Header=BB17_6 Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	strcpy
.LBB17_13:                              #   in Loop: Header=BB17_6 Depth=1
	movq	%r15, %rdi
	movq	1424(%rsp), %rsi
	callq	fopen
	movq	%rax, %r12
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB17_19
# BB#14:                                #   in Loop: Header=BB17_6 Depth=1
	testq	%r12, %r12
	jne	.LBB17_19
# BB#15:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	48(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movl	$6581294, 45(%rsp,%rax) # imm = 0x646C2E
	movl	$.L.str.27, %esi
	movq	%rbx, %rdi
	callq	fopen
	testq	%rax, %rax
	jne	.LBB17_17
# BB#16:                                #   in Loop: Header=BB17_6 Depth=1
	xorl	%ebp, %ebp
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jne	.LBB17_27
	jmp	.LBB17_28
	.p2align	4, 0x90
.LBB17_19:                              #   in Loop: Header=BB17_6 Depth=1
	testq	%r12, %r12
	setne	%bpl
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB17_26
# BB#20:                                #   in Loop: Header=BB17_6 Depth=1
	testq	%r12, %r12
	je	.LBB17_26
# BB#21:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	48(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movl	$6581294, 45(%rsp,%rax) # imm = 0x646C2E
	movl	$1, %edi
	movq	%r15, %rsi
	leaq	704(%rsp), %rdx
	callq	__xstat
	testl	%eax, %eax
	jne	.LBB17_25
# BB#22:                                #   in Loop: Header=BB17_6 Depth=1
	movl	$1, %edi
	leaq	48(%rsp), %rsi
	leaq	560(%rsp), %rdx
	callq	__xstat
	testl	%eax, %eax
	jne	.LBB17_25
# BB#23:                                #   in Loop: Header=BB17_6 Depth=1
	movq	648(%rsp), %rax
	cmpq	792(%rsp), %rax
	jg	.LBB17_24
	.p2align	4, 0x90
.LBB17_25:                              #   in Loop: Header=BB17_6 Depth=1
	movb	$1, %bpl
.LBB17_26:                              # %.thread
                                        #   in Loop: Header=BB17_6 Depth=1
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB17_28
.LBB17_27:                              #   in Loop: Header=BB17_6 Depth=1
	leaq	48(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movl	$7629870, 48(%rsp,%rax) # imm = 0x746C2E
	movl	$.L.str.27, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB17_32
.LBB17_28:                              # %.backedge
                                        #   in Loop: Header=BB17_6 Depth=1
	movq	8(%r13), %r13
	testq	%r12, %r12
	setne	%al
	cmpq	40(%rsp), %r13          # 8-byte Folded Reload
	je	.LBB17_30
# BB#29:                                # %.backedge
                                        #   in Loop: Header=BB17_6 Depth=1
	testb	%al, %al
	je	.LBB17_6
.LBB17_30:                              # %.critedge
	testq	%r12, %r12
	je	.LBB17_36
# BB#31:
	movq	24(%rsp), %r15          # 8-byte Reload
	cmpb	$0, 64(%r14)
	jne	.LBB17_37
	jmp	.LBB17_36
.LBB17_4:
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	jmp	.LBB17_39
.LBB17_2:
	movq	$0, (%r15)
	movq	stdin(%rip), %r12
	jmp	.LBB17_40
.LBB17_32:
	testb	%bpl, %bpl
	movq	24(%rsp), %r15          # 8-byte Reload
	je	.LBB17_34
# BB#33:
	subq	$8, %rsp
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	leaq	856(%rsp), %r9
	movl	$3, %edi
	movl	$16, %esi
	movl	$.L.str.39, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	leaq	56(%rsp), %rbp
	pushq	%rbp
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi119:
	.cfi_adjust_cfa_offset -16
.LBB17_34:                              # %.backedge.thread
	movq	1432(%rsp), %rax
	movl	$1, (%rax)
	movq	%rbx, %r12
	cmpb	$0, 64(%r14)
	je	.LBB17_36
.LBB17_37:
	leaq	848(%rsp), %rsi
	movl	$11, %edi
	movq	%r15, %rdx
	callq	MakeWord
	jmp	.LBB17_38
.LBB17_36:
	xorl	%eax, %eax
.LBB17_38:                              # %.critedge.thread
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB17_39:                              # %.critedge.thread
	movq	%rax, (%r15)
.LBB17_40:
	movq	%r12, %rax
	addq	$1368, %rsp             # imm = 0x558
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_17:
	movq	%rax, %rdi
	callq	fclose
.LBB17_18:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, (%rax)
	xorl	%r12d, %r12d
	jmp	.LBB17_40
.LBB17_24:                              # %.critedge94
	movq	%r12, %rdi
	callq	fclose
	leaq	848(%rsp), %rdi
	callq	remove
	jmp	.LBB17_18
.Lfunc_end17:
	.size	SearchPath, .Lfunc_end17-SearchPath
	.cfi_endproc

	.globl	OpenIncGraphicFile
	.p2align	4, 0x90
	.type	OpenIncGraphicFile,@function
OpenIncGraphicFile:                     # @OpenIncGraphicFile
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 48
	subq	$528, %rsp              # imm = 0x210
.Lcfi125:
	.cfi_def_cfa_offset 576
.Lcfi126:
	.cfi_offset %rbx, -48
.Lcfi127:
	.cfi_offset %r12, -40
.Lcfi128:
	.cfi_offset %r13, -32
.Lcfi129:
	.cfi_offset %r14, -24
.Lcfi130:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %r12
	movl	%esi, %ebx
	movq	%rdi, %r13
	movl	%ebx, %eax
	andb	$-2, %al
	cmpb	$94, %al
	je	.LBB18_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB18_2:
	xorl	%eax, %eax
	cmpb	$94, %bl
	setne	%al
	movq	file_path+8(,%rax,8), %rsi
	leaq	12(%rsp), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r12, %r8
	movq	%r15, %r9
	pushq	%rax
.Lcfi131:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.27
.Lcfi132:
	.cfi_adjust_cfa_offset 8
	callq	SearchPath
	addq	$16, %rsp
.Lcfi133:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %rbx
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	jne	.LBB18_4
# BB#3:
	movl	$11, %edi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	MakeWord
	movq	%rax, %rdi
	movq	%rdi, (%r12)
.LBB18_4:
	testq	%rbx, %rbx
	je	.LBB18_17
# BB#5:                                 # %.preheader
	addq	$64, %rdi
	movl	$.L.str.40, %esi
	callq	StringEndsWith
	testl	%eax, %eax
	jne	.LBB18_7
# BB#6:
	movq	(%r12), %rdi
	addq	$64, %rdi
	movl	$.L.str.41, %esi
	callq	StringEndsWith
	testl	%eax, %eax
	jne	.LBB18_7
# BB#12:
	movq	(%r12), %rdi
	addq	$64, %rdi
	movl	$.L.str.42, %esi
	callq	StringEndsWith
	testl	%eax, %eax
	jne	.LBB18_7
# BB#13:
	movq	(%r12), %rdi
	addq	$64, %rdi
	movl	$.L.str.43, %esi
	callq	StringEndsWith
	testl	%eax, %eax
	jne	.LBB18_7
# BB#14:
	movq	(%r12), %rdi
	addq	$64, %rdi
	movl	$.L.str.44, %esi
	callq	StringEndsWith
	testl	%eax, %eax
	jne	.LBB18_7
# BB#15:
	movq	(%r12), %rdi
	addq	$64, %rdi
	movl	$.L.str.45, %esi
	callq	StringEndsWith
	testl	%eax, %eax
	je	.LBB18_16
.LBB18_7:
	movq	%rbx, %rdi
	callq	fclose
	movq	(%r12), %rdx
	addq	$64, %rdx
	xorl	%r12d, %r12d
	leaq	16(%rsp), %rdi
	movl	$.L.str.28, %esi
	movl	$.L.str.29, %ecx
	xorl	%eax, %eax
	callq	sprintf
	cmpl	$0, SafeExecution(%rip)
	je	.LBB18_9
# BB#8:
	leaq	16(%rsp), %r9
	movl	$3, %edi
	movl	$17, %esi
	movl	$.L.str.30, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	xorl	%ebx, %ebx
	jmp	.LBB18_10
.LBB18_17:
	movl	$0, (%r14)
	xorl	%ebx, %ebx
	jmp	.LBB18_11
.LBB18_9:
	leaq	16(%rsp), %rdi
	callq	system
	movl	$.L.str.29, %edi
	movl	$.L.str.27, %esi
	callq	fopen
	movq	%rax, %rbx
	movl	$1, %r12d
.LBB18_10:
	movl	%r12d, (%r14)
.LBB18_11:
	movq	%rbx, %rax
	addq	$528, %rsp              # imm = 0x210
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB18_16:                              # %.critedge31
	movl	$0, (%r14)
	jmp	.LBB18_11
.Lfunc_end18:
	.size	OpenIncGraphicFile, .Lfunc_end18-OpenIncGraphicFile
	.cfi_endproc

	.globl	FileSetUpdated
	.p2align	4, 0x90
	.type	FileSetUpdated,@function
FileSetUpdated:                         # @FileSetUpdated
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	file_tab(%rip), %rax
	shlq	$4, %rdi
	movq	8(%rax,%rdi), %rax
	movl	$1, 56(%rax)
	movl	%esi, 60(%rax)
	retq
.Lfunc_end19:
	.size	FileSetUpdated, .Lfunc_end19-FileSetUpdated
	.cfi_endproc

	.globl	FileGetLineCount
	.p2align	4, 0x90
	.type	FileGetLineCount,@function
FileGetLineCount:                       # @FileGetLineCount
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	file_tab(%rip), %rax
	shlq	$4, %rdi
	movq	8(%rax,%rdi), %rax
	movl	60(%rax), %eax
	retq
.Lfunc_end20:
	.size	FileGetLineCount, .Lfunc_end20-FileGetLineCount
	.cfi_endproc

	.globl	FileTestUpdated
	.p2align	4, 0x90
	.type	FileTestUpdated,@function
FileTestUpdated:                        # @FileTestUpdated
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	file_tab(%rip), %rax
	shlq	$4, %rdi
	movq	8(%rax,%rdi), %rax
	movl	56(%rax), %eax
	retq
.Lfunc_end21:
	.size	FileTestUpdated, .Lfunc_end21-FileTestUpdated
	.cfi_endproc

	.type	no_fpos,@object         # @no_fpos
	.data
	.globl	no_fpos
	.p2align	3
no_fpos:
	.quad	no_file_pos
	.size	no_fpos, 8

	.type	file_type,@object       # @file_type
	.local	file_type
	.comm	file_type,88,16
	.type	file_path,@object       # @file_path
	.local	file_path
	.comm	file_path,64,16
	.type	file_tab,@object        # @file_tab
	.local	file_tab
	.comm	file_tab,8,8
	.type	empty_path,@object      # @empty_path
	.local	empty_path
	.comm	empty_path,8,8
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.zero	1
	.size	.L.str.1, 1

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"assert failed in %s"
	.size	.L.str.2, 20

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"DefineFile: ftype!"
	.size	.L.str.3, 19

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	".ld"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"database file %s where source file expected"
	.size	.L.str.5, 44

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	".li"
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"database index file %s where source file expected"
	.size	.L.str.7, 50

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"file name %s%s is too long"
	.size	.L.str.8, 27

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"DatabaseFileNum: filter file position unknown"
	.size	.L.str.9, 46

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"DatabaseFileNum: unexpected file type"
	.size	.L.str.10, 38

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"FileName: x == nilobj!"
	.size	.L.str.11, 23

	.type	FullFileName.ffbuff,@object # @FullFileName.ffbuff
	.local	FullFileName.ffbuff
	.comm	FullFileName.ffbuff,1024,16
	.type	FullFileName.ffbp,@object # @FullFileName.ffbp
	.data
	.p2align	2
FullFileName.ffbp:
	.long	1                       # 0x1
	.size	FullFileName.ffbp, 4

	.type	bp,@object              # @bp
	.p2align	2
bp:
	.long	1                       # 0x1
	.size	bp, 4

	.type	buff,@object            # @buff
	.local	buff
	.comm	buff,1024,16
	.type	.L.str.15,@object       # @.str.15
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.15:
	.asciz	"EchoFileSource: x == nilobj!"
	.size	.L.str.15, 29

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"filter"
	.size	.L.str.16, 7

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"file"
	.size	.L.str.17, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	" ("
	.size	.L.str.19, 3

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"from"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"line"
	.size	.L.str.21, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	", "
	.size	.L.str.22, 3

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"PosOfFile: file_tab entry is nilobj!"
	.size	.L.str.25, 37

	.type	file_mode,@object       # @file_mode
	.section	.rodata,"a",@progbits
	.p2align	4
file_mode:
	.quad	.L.str.27
	.quad	.L.str.27
	.quad	.L.str.27
	.quad	.L.str.27
	.quad	.L.str.27
	.quad	.L.str.27
	.quad	.L.str.27
	.quad	.L.str.27
	.quad	.L.str.27
	.quad	.L.str.27
	.quad	.L.str.27
	.size	file_mode, 88

	.type	.L.str.26,@object       # @.str.26
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.26:
	.asciz	"OpenIncGraphicFile!"
	.size	.L.str.26, 20

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"r"
	.size	.L.str.27, 2

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"gunzip -c %s > %s"
	.size	.L.str.28, 18

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"lout.eps"
	.size	.L.str.29, 9

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"safe execution prohibiting command: %s"
	.size	.L.str.30, 39

	.type	no_file_pos,@object     # @no_file_pos
	.local	no_file_pos
	.comm	no_file_pos,8,4
	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"run out of memory when enlarging file table"
	.size	.L.str.32, 44

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"too many files (maximum is %d)"
	.size	.L.str.33, 31

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"EchoFilePos: file_tab entry is nilobj!"
	.size	.L.str.34, 39

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"file position %s... is too long to print"
	.size	.L.str.35, 41

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"/"
	.size	.L.str.36, 2

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"-"
	.size	.L.str.37, 2

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"file path name %s%s%s is too long"
	.size	.L.str.38, 34

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"files %s and %s both exist"
	.size	.L.str.39, 27

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	".gz"
	.size	.L.str.40, 4

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"-gz"
	.size	.L.str.41, 4

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	".z"
	.size	.L.str.42, 3

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"-z"
	.size	.L.str.43, 3

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"_z"
	.size	.L.str.44, 3

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	".Z"
	.size	.L.str.45, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
