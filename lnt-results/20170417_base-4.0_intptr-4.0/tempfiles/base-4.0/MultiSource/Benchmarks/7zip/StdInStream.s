	.text
	.file	"StdInStream.bc"
	.globl	_ZN12CStdInStream4OpenEPKw
	.p2align	4, 0x90
	.type	_ZN12CStdInStream4OpenEPKw,@function
_ZN12CStdInStream4OpenEPKw:             # @_ZN12CStdInStream4OpenEPKw
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 80
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	cmpb	$0, (%r14)
	je	.LBB0_2
# BB#1:
	movq	8(%r14), %rdi
	callq	fclose
	testl	%eax, %eax
	setne	(%r14)
.LBB0_2:                                # %_ZN12CStdInStream5CloseEv.exit
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebx
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB0_3
# BB#4:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbx), %eax
	movslq	%eax, %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	%r15d, 12(%rsp)
	.p2align	4, 0x90
.LBB0_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12), %ecx
	addq	$4, %r12
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_5
# BB#6:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%ebx, 8(%rsp)
.Ltmp0:
	leaq	24(%rsp), %rdi
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp1:
# BB#7:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#8:
	callq	_ZdaPv
.LBB0_9:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	24(%rsp), %rdi
	cmpb	$99, (%rdi)
	jne	.LBB0_11
# BB#10:
	leaq	2(%rdi), %rax
	cmpb	$58, 1(%rdi)
	cmoveq	%rax, %rdi
.LBB0_11:                               # %_ZL16nameWindowToUnixPKc.exit
	movl	$.L.str, %esi
	callq	fopen64
	movq	%rax, %rbx
	movq	%rbx, 8(%r14)
	testq	%rbx, %rbx
	setne	(%r14)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
# BB#12:
	callq	_ZdaPv
.LBB0_13:                               # %_ZN11CStringBaseIcED2Ev.exit
	testq	%rbx, %rbx
	setne	%al
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_14:
.Ltmp2:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_16
# BB#15:
	callq	_ZdaPv
.LBB0_16:                               # %_ZN11CStringBaseIwED2Ev.exit7
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN12CStdInStream4OpenEPKw, .Lfunc_end0-_ZN12CStdInStream4OpenEPKw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN12CStdInStream5CloseEv
	.p2align	4, 0x90
	.type	_ZN12CStdInStream5CloseEv,@function
_ZN12CStdInStream5CloseEv:              # @_ZN12CStdInStream5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$0, (%rbx)
	je	.LBB1_1
# BB#2:
	movq	8(%rbx), %rdi
	callq	fclose
	testl	%eax, %eax
	setne	(%rbx)
	sete	%al
	popq	%rbx
	retq
.LBB1_1:
	movb	$1, %al
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN12CStdInStream5CloseEv, .Lfunc_end1-_ZN12CStdInStream5CloseEv
	.cfi_endproc

	.globl	_ZN12CStdInStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN12CStdInStreamD2Ev,@function
_ZN12CStdInStreamD2Ev:                  # @_ZN12CStdInStreamD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$0, (%rbx)
	je	.LBB2_2
# BB#1:
	movq	8(%rbx), %rdi
	callq	fclose
	testl	%eax, %eax
	setne	(%rbx)
.LBB2_2:                                # %_ZN12CStdInStream5CloseEv.exit
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN12CStdInStreamD2Ev, .Lfunc_end2-_ZN12CStdInStreamD2Ev
	.cfi_endproc

	.globl	_ZN12CStdInStream22ScanStringUntilNewLineEb
	.p2align	4, 0x90
	.type	_ZN12CStdInStream22ScanStringUntilNewLineEb,@function
_ZN12CStdInStream22ScanStringUntilNewLineEb: # @_ZN12CStdInStream22ScanStringUntilNewLineEb
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, (%rbx)
	movb	$0, (%rax)
	movl	$4, 12(%rbx)
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	callq	fgetc
	cmpl	$-1, %eax
	je	.LBB3_2
# BB#6:                                 # %_ZN12CStdInStream7GetCharEv.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpb	$10, %al
	je	.LBB3_13
# BB#7:                                 # %_ZN12CStdInStream7GetCharEv.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	movsbl	%al, %ecx
	testl	%ecx, %ecx
	je	.LBB3_8
# BB#11:                                #   in Loop: Header=BB3_1 Depth=1
.Ltmp6:
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp7:
	jmp	.LBB3_1
.LBB3_2:
	movq	8(%rbp), %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB3_3
# BB#12:
	testb	%r14b, %r14b
	je	.LBB3_18
.LBB3_13:                               # %.loopexit
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB3_8:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.2, (%rax)
.Ltmp3:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp4:
.LBB3_19:
.LBB3_3:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.3, (%rax)
.Ltmp12:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp13:
# BB#4:                                 # %.noexc
.LBB3_18:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.1, (%rax)
.Ltmp9:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp10:
	jmp	.LBB3_19
.LBB3_5:
.Ltmp11:
	jmp	.LBB3_15
.LBB3_14:
.Ltmp14:
	jmp	.LBB3_15
.LBB3_10:                               # %.loopexit.split-lp
.Ltmp5:
	jmp	.LBB3_15
.LBB3_9:                                # %.loopexit19
.Ltmp8:
.LBB3_15:
	movq	%rax, %rbp
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_17
# BB#16:
	callq	_ZdaPv
.LBB3_17:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN12CStdInStream22ScanStringUntilNewLineEb, .Lfunc_end3-_ZN12CStdInStream22ScanStringUntilNewLineEb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\367\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp6-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp7           #   Call between .Ltmp7 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 5 <<
	.long	.Ltmp12-.Ltmp4          #   Call between .Ltmp4 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp9-.Ltmp13          #   Call between .Ltmp13 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 8 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Lfunc_end3-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN12CStdInStream7GetCharEv
	.p2align	4, 0x90
	.type	_ZN12CStdInStream7GetCharEv,@function
_ZN12CStdInStream7GetCharEv:            # @_ZN12CStdInStream7GetCharEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	8(%r14), %rdi
	callq	fgetc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	jne	.LBB4_2
# BB#1:
	movq	8(%r14), %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB4_3
.LBB4_2:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_3:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.3, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end4:
	.size	_ZN12CStdInStream7GetCharEv, .Lfunc_end4-_ZN12CStdInStream7GetCharEv
	.cfi_endproc

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 64
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB5_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB5_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB5_3:                                # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB5_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB5_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB5_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB5_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB5_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB5_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB5_17
.LBB5_7:
	xorl	%ecx, %ecx
.LBB5_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB5_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB5_10
.LBB5_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB5_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB5_13
	jmp	.LBB5_26
.LBB5_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB5_27
.LBB5_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB5_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB5_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB5_20
	jmp	.LBB5_21
.LBB5_18:
	xorl	%ebx, %ebx
.LBB5_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB5_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB5_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB5_23
.LBB5_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB5_8
	jmp	.LBB5_26
.Lfunc_end5:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end5-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.text
	.globl	_ZN12CStdInStream12ReadToStringER11CStringBaseIcE
	.p2align	4, 0x90
	.type	_ZN12CStdInStream12ReadToStringER11CStringBaseIcE,@function
_ZN12CStdInStream12ReadToStringER11CStringBaseIcE: # @_ZN12CStdInStream12ReadToStringER11CStringBaseIcE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$0, 8(%rbx)
	movq	(%rbx), %rax
	movb	$0, (%rax)
	jmp	.LBB6_1
	.p2align	4, 0x90
.LBB6_4:                                # %_ZN12CStdInStream7GetCharEv.exit
                                        #   in Loop: Header=BB6_1 Depth=1
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rdi
	callq	fgetc
	cmpl	$-1, %eax
	jne	.LBB6_4
# BB#2:                                 # %._crit_edge
	movq	8(%r14), %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB6_3
# BB#5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_3:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.3, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end6:
	.size	_ZN12CStdInStream12ReadToStringER11CStringBaseIcE, .Lfunc_end6-_ZN12CStdInStream12ReadToStringER11CStringBaseIcE
	.cfi_endproc

	.globl	_ZN12CStdInStream3EofEv
	.p2align	4, 0x90
	.type	_ZN12CStdInStream3EofEv,@function
_ZN12CStdInStream3EofEv:                # @_ZN12CStdInStream3EofEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rdi
	callq	feof
	testl	%eax, %eax
	setne	%al
	popq	%rcx
	retq
.Lfunc_end7:
	.size	_ZN12CStdInStream3EofEv, .Lfunc_end7-_ZN12CStdInStream3EofEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_StdInStream.ii,@function
_GLOBAL__sub_I_StdInStream.ii:          # @_GLOBAL__sub_I_StdInStream.ii
	.cfi_startproc
# BB#0:
	movq	stdin(%rip), %rax
	movb	$0, g_StdIn(%rip)
	movq	%rax, g_StdIn+8(%rip)
	movl	$_ZN12CStdInStreamD2Ev, %edi
	movl	$g_StdIn, %esi
	movl	$__dso_handle, %edx
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end8:
	.size	_GLOBAL__sub_I_StdInStream.ii, .Lfunc_end8-_GLOBAL__sub_I_StdInStream.ii
	.cfi_endproc

	.type	g_StdIn,@object         # @g_StdIn
	.bss
	.globl	g_StdIn
	.p2align	3
g_StdIn:
	.zero	16
	.size	g_StdIn, 16

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Unexpected end of input stream"
	.size	.L.str.1, 31

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Illegal character in input stream"
	.size	.L.str.2, 34

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Error reading input stream"
	.size	.L.str.3, 27

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_StdInStream.ii

	.globl	_ZN12CStdInStreamD1Ev
	.type	_ZN12CStdInStreamD1Ev,@function
_ZN12CStdInStreamD1Ev = _ZN12CStdInStreamD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
