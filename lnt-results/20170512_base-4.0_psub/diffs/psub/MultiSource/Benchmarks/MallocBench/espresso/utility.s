	.text
	.file	"utility.bc"
	.globl	util_cpu_time
	.p2align	4, 0x90
	.type	util_cpu_time,@function
util_cpu_time:                          # @util_cpu_time
	.cfi_startproc
# BB#0:
	subq	$152, %rsp
.Lcfi0:
	.cfi_def_cfa_offset 160
	leaq	8(%rsp), %rsi
	xorl	%edi, %edi
	callq	getrusage
	imulq	$1000, 8(%rsp), %rcx    # imm = 0x3E8
	movabsq	$2361183241434822607, %rax # imm = 0x20C49BA5E353F7CF
	imulq	16(%rsp)
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$7, %rdx
	leaq	(%rdx,%rax), %rax
	addq	%rcx, %rax
	addq	$152, %rsp
	retq
.Lfunc_end0:
	.size	util_cpu_time, .Lfunc_end0-util_cpu_time
	.cfi_endproc

	.globl	util_print_time
	.p2align	4, 0x90
	.type	util_print_time,@function
util_print_time:                        # @util_print_time
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end1:
	.size	util_print_time, .Lfunc_end1-util_print_time
	.cfi_endproc

	.globl	util_strsav
	.p2align	4, 0x90
	.type	util_strsav,@function
util_strsav:                            # @util_strsav
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	strcpy                  # TAILCALL
.Lfunc_end2:
	.size	util_strsav, .Lfunc_end2-util_strsav
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.zero	1
	.size	.L.str, 1


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
