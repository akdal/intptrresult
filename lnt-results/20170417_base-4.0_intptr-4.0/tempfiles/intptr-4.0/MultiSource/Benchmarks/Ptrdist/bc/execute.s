	.text
	.file	"execute.bc"
	.globl	stop_execution
	.p2align	4, 0x90
	.type	stop_execution,@function
stop_execution:                         # @stop_execution
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$1, had_sigint(%rip)
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	popq	%rcx
	jmp	rt_error                # TAILCALL
.Lfunc_end0:
	.size	stop_execution, .Lfunc_end0-stop_execution
	.cfi_endproc

	.globl	byte
	.p2align	4, 0x90
	.type	byte,@function
byte:                                   # @byte
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %eax
	movl	%eax, %ecx
	sarl	$10, %ecx
	leal	1(%rax), %edx
	movl	%edx, 4(%rdi)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	(%rdi), %rdx
	movslq	%ecx, %rcx
	imulq	$168, %rdx, %rdx
	addq	functions(%rip), %rdx
	movq	8(%rdx,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %al
	retq
.Lfunc_end1:
	.size	byte, .Lfunc_end1-byte
	.cfi_endproc

	.globl	execute
	.p2align	4, 0x90
	.type	execute,@function
execute:                                # @execute
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %r14, -16
	movl	$0, pc(%rip)
	movl	$0, pc+4(%rip)
	movb	$0, runtime_error(%rip)
	movq	%rsp, %rdi
	callq	init_num
	cmpb	$0, interactive(%rip)
	je	.LBB2_3
# BB#1:
	movl	$2, %edi
	movl	$stop_execution, %esi
	callq	signal
	movl	$0, had_sigint(%rip)
	jmp	.LBB2_3
.LBB2_2:                                # %.thread.backedge
                                        #   in Loop: Header=BB2_3 Depth=1
	callq	copy_num
	movq	ex_stack(%rip), %rcx
	movq	%rax, (%rcx)
	jmp	.LBB2_3
.LBB2_28:                               #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_zero
	movl	%eax, %ebx
	movb	%bl, c_code(%rip)
	movq	ex_stack(%rip), %rdi
	callq	free_num
	testb	%bl, %bl
	movl	$_one_, %eax
	movl	$_zero_, %ecx
	cmovneq	%rax, %rcx
	movq	(%rcx), %rdi
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_3:                                # %.thread.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_101 Depth 2
                                        #     Child Loop BB2_12 Depth 2
                                        #     Child Loop BB2_62 Depth 2
                                        #     Child Loop BB2_118 Depth 2
	movl	pc+4(%rip), %r10d
	movq	functions(%rip), %rdi
	movslq	pc(%rip), %rax
	imulq	$168, %rax, %rcx
	cmpl	136(%rdi,%rcx), %r10d
	jge	.LBB2_139
# BB#4:                                 # %.thread.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movb	runtime_error(%rip), %cl
	testb	%cl, %cl
	jne	.LBB2_139
# BB#5:                                 # %.lr.ph113
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	%r10d, %ecx
	sarl	$10, %ecx
	leal	1(%r10), %r9d
	movl	%r9d, pc+4(%rip)
	movl	%r10d, %ebx
	sarl	$31, %ebx
	shrl	$22, %ebx
	addl	%r10d, %ebx
	andl	$-1024, %ebx            # imm = 0xFC00
	movl	%r10d, %esi
	subl	%ebx, %esi
	movslq	%eax, %rbx
	movslq	%ecx, %rdx
	imulq	$168, %rbx, %rcx
	leaq	(%rdi,%rcx), %r8
	movq	8(%r8,%rdx,8), %rdx
	movslq	%esi, %rsi
	movsbl	(%rdx,%rsi), %r14d
	leal	-33(%r14), %ebx
	cmpl	$92, %ebx
	ja	.LBB2_29
# BB#6:                                 # %.lr.ph113
                                        #   in Loop: Header=BB2_3 Depth=1
	jmpq	*.LJTI2_0(,%rbx,8)
.LBB2_7:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	sete	c_code(%rip)
	callq	pop
	movl	pc+4(%rip), %r9d
	movq	functions(%rip), %rdi
	movl	pc(%rip), %eax
.LBB2_8:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %ecx
	sarl	$10, %ecx
	leal	1(%r9), %edx
	movl	%edx, pc+4(%rip)
	movl	%r9d, %esi
	sarl	$31, %esi
	shrl	$22, %esi
	addl	%r9d, %esi
	andl	$-1024, %esi            # imm = 0xFC00
	movl	%r9d, %ebx
	subl	%esi, %ebx
	cltq
	movslq	%ecx, %rsi
	imulq	$168, %rax, %r8
	leaq	(%rdi,%r8), %rcx
	movq	8(%rcx,%rsi,8), %rax
	movslq	%ebx, %rsi
	movzbl	(%rax,%rsi), %eax
	movl	%edx, %esi
	sarl	$10, %esi
	leal	2(%r9), %ebx
	movl	%ebx, pc+4(%rip)
	sarl	$31, %edx
	shrl	$22, %edx
	leal	1(%r9,%rdx), %edx
	andl	$-1024, %edx            # imm = 0xFC00
	negl	%edx
	leal	1(%r9,%rdx), %edx
	movslq	%esi, %rsi
	movq	8(%rcx,%rsi,8), %rcx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	cmpb	$74, %r14b
	je	.LBB2_116
# BB#9:                                 #   in Loop: Header=BB2_3 Depth=1
	cmpb	$66, %r14b
	jne	.LBB2_114
# BB#10:                                #   in Loop: Header=BB2_3 Depth=1
	cmpb	$0, c_code(%rip)
	jne	.LBB2_116
	jmp	.LBB2_3
	.p2align	4, 0x90
.LBB2_11:                               # %.preheader98.backedge
                                        #   in Loop: Header=BB2_12 Depth=2
	movl	pc+4(%rip), %r9d
	movq	functions(%rip), %rdi
	movl	pc(%rip), %eax
.LBB2_12:                               # %.preheader98
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r9d, %edx
	sarl	$10, %edx
	leal	1(%r9), %ecx
	movl	%ecx, pc+4(%rip)
	movl	%r9d, %esi
	sarl	$31, %esi
	shrl	$22, %esi
	addl	%r9d, %esi
	andl	$-1024, %esi            # imm = 0xFC00
	movl	%r9d, %ebx
	subl	%esi, %ebx
	cltq
	movslq	%edx, %rdx
	imulq	$168, %rax, %rax
	addq	%rdi, %rax
	movq	8(%rax,%rdx,8), %rdx
	movslq	%ebx, %rsi
	movzbl	(%rdx,%rsi), %edx
	cmpb	$92, %dl
	je	.LBB2_16
# BB#13:                                # %.preheader98
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpb	$34, %dl
	je	.LBB2_103
# BB#14:                                #   in Loop: Header=BB2_12 Depth=2
	movsbl	%dl, %edi
.LBB2_15:                               # %.preheader98.backedge
                                        #   in Loop: Header=BB2_12 Depth=2
	callq	out_char
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_16:                               #   in Loop: Header=BB2_12 Depth=2
	movl	%ecx, %edx
	sarl	$10, %edx
	addl	$2, %r9d
	movl	%r9d, pc+4(%rip)
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$22, %esi
	addl	%ecx, %esi
	andl	$-1024, %esi            # imm = 0xFC00
	subl	%esi, %ecx
	movslq	%edx, %rdx
	movq	8(%rax,%rdx,8), %rax
	movslq	%ecx, %rcx
	movsbl	(%rax,%rcx), %eax
	cmpl	$34, %eax
	je	.LBB2_103
# BB#17:                                #   in Loop: Header=BB2_12 Depth=2
	addl	$-92, %eax
	rorl	%eax
	cmpl	$12, %eax
	ja	.LBB2_11
# BB#18:                                #   in Loop: Header=BB2_12 Depth=2
	jmpq	*.LJTI2_2(,%rax,8)
.LBB2_19:                               #   in Loop: Header=BB2_12 Depth=2
	movl	$92, %edi
	jmp	.LBB2_15
.LBB2_20:                               #   in Loop: Header=BB2_12 Depth=2
	movl	$7, %edi
	jmp	.LBB2_15
.LBB2_21:                               #   in Loop: Header=BB2_12 Depth=2
	movl	$12, %edi
	jmp	.LBB2_15
.LBB2_22:                               #   in Loop: Header=BB2_12 Depth=2
	movl	$10, %edi
	jmp	.LBB2_15
.LBB2_23:                               #   in Loop: Header=BB2_12 Depth=2
	movl	$13, %edi
	jmp	.LBB2_15
.LBB2_24:                               #   in Loop: Header=BB2_12 Depth=2
	movl	$9, %edi
	jmp	.LBB2_15
.LBB2_25:                               #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	movl	o_base(%rip), %esi
	movl	$out_char, %edx
	callq	out_num
	cmpb	$87, %r14b
	jne	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$10, %edi
	callq	out_char
.LBB2_27:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$3, %edi
	callq	store_var
	cmpb	$0, interactive(%rip)
	jne	.LBB2_104
	jmp	.LBB2_3
.LBB2_29:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	rt_error
	jmp	.LBB2_3
.LBB2_30:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#31:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	bc_compare
	testl	%eax, %eax
	setne	c_code(%rip)
	jmp	.LBB2_129
.LBB2_32:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#33:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	je	.LBB2_124
# BB#34:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.6, %edi
	jmp	.LBB2_123
.LBB2_35:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#36:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	je	.LBB2_126
# BB#37:                                #   in Loop: Header=BB2_3 Depth=1
	xorl	%eax, %eax
	movb	%al, c_code(%rip)
	jmp	.LBB2_129
.LBB2_38:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#39:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movl	scale(%rip), %ecx
	movq	%rsp, %rbx
	movq	%rbx, %rdx
	callq	bc_multiply
	jmp	.LBB2_125
.LBB2_40:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#41:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movq	%rsp, %rbx
	movq	%rbx, %rdx
	callq	bc_add
	jmp	.LBB2_125
.LBB2_42:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#43:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movq	%rsp, %rbx
	movq	%rbx, %rdx
	callq	bc_sub
	jmp	.LBB2_125
.LBB2_44:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#45:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movl	scale(%rip), %ecx
	movq	%rsp, %rdx
	callq	bc_divide
	testl	%eax, %eax
	je	.LBB2_82
# BB#46:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.5, %edi
	jmp	.LBB2_123
.LBB2_47:                               #   in Loop: Header=BB2_3 Depth=1
	movq	_zero_(%rip), %rdi
	callq	push_copy
	jmp	.LBB2_3
.LBB2_48:                               #   in Loop: Header=BB2_3 Depth=1
	movq	_one_(%rip), %rdi
	callq	push_copy
	jmp	.LBB2_3
.LBB2_49:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#50:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	bc_compare
	cmpl	$-1, %eax
	sete	c_code(%rip)
	jmp	.LBB2_129
.LBB2_51:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#52:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	bc_compare
	testl	%eax, %eax
	sete	c_code(%rip)
	jmp	.LBB2_129
.LBB2_53:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#54:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	bc_compare
	cmpl	$1, %eax
	sete	c_code(%rip)
	jmp	.LBB2_129
.LBB2_55:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %ecx
	sarl	$10, %ecx
	leal	2(%r10), %eax
	movl	%eax, pc+4(%rip)
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%r9d, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %r9d
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	movslq	%r9d, %rdx
	movb	(%rcx,%rdx), %cl
	testb	%cl, %cl
	jns	.LBB2_57
# BB#56:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%eax, %ecx
	sarl	$10, %ecx
	addl	$3, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %cl
.LBB2_57:                               #   in Loop: Header=BB2_3 Depth=1
	movzbl	%cl, %edi
	callq	incr_array
	jmp	.LBB2_3
.LBB2_58:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %ecx
	sarl	$10, %ecx
	leal	2(%r10), %eax
	movl	%eax, pc+4(%rip)
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%r9d, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %r9d
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	movslq	%r9d, %rdx
	movb	(%rcx,%rdx), %cl
	testb	%cl, %cl
	jns	.LBB2_60
# BB#59:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%eax, %ecx
	sarl	$10, %ecx
	addl	$3, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %cl
.LBB2_60:                               #   in Loop: Header=BB2_3 Depth=1
	movzbl	%cl, %r14d
	imulq	$168, %r14, %rbx
	cmpb	$0, (%rdi,%rbx)
	je	.LBB2_121
# BB#61:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$pc, %edi
	movl	%r14d, %esi
	callq	process_params
	movq	functions(%rip), %rax
	movq	160(%rax,%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB2_63
	.p2align	4, 0x90
.LBB2_62:                               # %.lr.ph104
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %edi
	callq	auto_var
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_62
.LBB2_63:                               # %._crit_edge105
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	pc(%rip), %edi
	callq	fpush
	movl	pc+4(%rip), %edi
	callq	fpush
	movl	i_base(%rip), %edi
	callq	fpush
	movl	%r14d, pc(%rip)
	movl	$0, pc+4(%rip)
	jmp	.LBB2_3
.LBB2_64:                               #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	push_copy
	jmp	.LBB2_3
.LBB2_65:                               #   in Loop: Header=BB2_3 Depth=1
	testl	%eax, %eax
	movl	$i_base, %eax
	cmovneq	fn_stack(%rip), %rax
	movl	(%rax), %esi
	cmpl	$10, %esi
	jne	.LBB2_120
# BB#66:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$pc, %edi
	callq	push_b10_const
	jmp	.LBB2_3
.LBB2_67:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %ecx
	sarl	$10, %ecx
	leal	2(%r10), %eax
	movl	%eax, pc+4(%rip)
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%r9d, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %r9d
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	movslq	%r9d, %rdx
	movb	(%rcx,%rdx), %cl
	testb	%cl, %cl
	jns	.LBB2_69
# BB#68:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%eax, %ecx
	sarl	$10, %ecx
	addl	$3, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %cl
.LBB2_69:                               #   in Loop: Header=BB2_3 Depth=1
	movzbl	%cl, %edi
	callq	load_array
	jmp	.LBB2_3
.LBB2_70:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %ecx
	sarl	$10, %ecx
	leal	2(%r10), %eax
	movl	%eax, pc+4(%rip)
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%r9d, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %r9d
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	movslq	%r9d, %rdx
	movb	(%rcx,%rdx), %cl
	testb	%cl, %cl
	jns	.LBB2_72
# BB#71:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%eax, %ecx
	sarl	$10, %ecx
	addl	$3, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %cl
.LBB2_72:                               #   in Loop: Header=BB2_3 Depth=1
	movsbl	%cl, %edi
	callq	decr_array
	jmp	.LBB2_3
.LBB2_73:                               #   in Loop: Header=BB2_3 Depth=1
	testl	%eax, %eax
	je	.LBB2_122
# BB#74:                                #   in Loop: Header=BB2_3 Depth=1
	movq	160(%rdi,%rcx), %rdi
	callq	pop_vars
	movq	functions(%rip), %rax
	movslq	pc(%rip), %rcx
	imulq	$168, %rcx, %rcx
	movq	152(%rax,%rcx), %rdi
	callq	pop_vars
	callq	fpop
	callq	fpop
	movl	%eax, pc+4(%rip)
	callq	fpop
	movl	%eax, pc(%rip)
	jmp	.LBB2_3
.LBB2_75:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %ecx
	sarl	$10, %ecx
	leal	2(%r10), %eax
	movl	%eax, pc+4(%rip)
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%r9d, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %r9d
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	movslq	%r9d, %rdx
	movb	(%rcx,%rdx), %cl
	testb	%cl, %cl
	jns	.LBB2_77
# BB#76:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%eax, %ecx
	sarl	$10, %ecx
	addl	$3, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %cl
.LBB2_77:                               #   in Loop: Header=BB2_3 Depth=1
	movzbl	%cl, %edi
	callq	store_array
	jmp	.LBB2_3
.LBB2_78:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#79:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movl	scale(%rip), %ecx
	movq	%rsp, %rdx
	callq	bc_raise
	movq	ex_stack(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	je	.LBB2_82
# BB#80:                                #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_neg
	testb	%al, %al
	je	.LBB2_82
# BB#81:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	rt_error
.LBB2_82:                               #   in Loop: Header=BB2_3 Depth=1
	callq	pop
	callq	pop
	movq	(%rsp), %rdi
	callq	push_num
	movq	%rsp, %rdi
	callq	init_num
	jmp	.LBB2_3
.LBB2_83:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %eax
	sarl	$10, %eax
	addl	$2, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%r9d, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%r9d, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %r9d
	cltq
	movq	8(%r8,%rax,8), %rax
	movslq	%r9d, %rcx
	movb	(%rax,%rcx), %al
	addb	$-73, %al
	cmpb	$10, %al
	ja	.LBB2_3
# BB#84:                                #   in Loop: Header=BB2_3 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI2_1(,%rax,8)
.LBB2_85:                               #   in Loop: Header=BB2_3 Depth=1
	movl	i_base(%rip), %esi
	movl	$input_char, %edi
	callq	push_constant
	jmp	.LBB2_3
.LBB2_86:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %ecx
	sarl	$10, %ecx
	leal	2(%r10), %eax
	movl	%eax, pc+4(%rip)
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%r9d, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %r9d
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	movslq	%r9d, %rdx
	movb	(%rcx,%rdx), %cl
	testb	%cl, %cl
	jns	.LBB2_88
# BB#87:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%eax, %ecx
	sarl	$10, %ecx
	addl	$3, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %cl
.LBB2_88:                               #   in Loop: Header=BB2_3 Depth=1
	movzbl	%cl, %edi
	callq	decr_var
	jmp	.LBB2_3
.LBB2_89:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %ecx
	sarl	$10, %ecx
	leal	2(%r10), %eax
	movl	%eax, pc+4(%rip)
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%r9d, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %r9d
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	movslq	%r9d, %rdx
	movb	(%rcx,%rdx), %cl
	testb	%cl, %cl
	jns	.LBB2_91
# BB#90:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%eax, %ecx
	sarl	$10, %ecx
	addl	$3, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %cl
.LBB2_91:                               #   in Loop: Header=BB2_3 Depth=1
	movzbl	%cl, %edi
	callq	incr_var
	jmp	.LBB2_3
.LBB2_92:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %ecx
	sarl	$10, %ecx
	leal	2(%r10), %eax
	movl	%eax, pc+4(%rip)
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%r9d, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %r9d
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	movslq	%r9d, %rdx
	movb	(%rcx,%rdx), %cl
	testb	%cl, %cl
	jns	.LBB2_94
# BB#93:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%eax, %ecx
	sarl	$10, %ecx
	addl	$3, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %cl
.LBB2_94:                               #   in Loop: Header=BB2_3 Depth=1
	movzbl	%cl, %edi
	callq	load_var
	jmp	.LBB2_3
.LBB2_95:                               #   in Loop: Header=BB2_3 Depth=1
	movq	_zero_(%rip), %rdi
	movq	ex_stack(%rip), %rdx
	movq	(%rdx), %rsi
	callq	bc_sub
	jmp	.LBB2_3
.LBB2_96:                               #   in Loop: Header=BB2_3 Depth=1
	callq	pop
	jmp	.LBB2_3
.LBB2_97:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %ecx
	sarl	$10, %ecx
	leal	2(%r10), %eax
	movl	%eax, pc+4(%rip)
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%r9d, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %r9d
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	movslq	%r9d, %rdx
	movb	(%rcx,%rdx), %cl
	testb	%cl, %cl
	jns	.LBB2_99
# BB#98:                                #   in Loop: Header=BB2_3 Depth=1
	movl	%eax, %ecx
	sarl	$10, %ecx
	addl	$3, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%ecx, %rcx
	movq	8(%r8,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %cl
.LBB2_99:                               #   in Loop: Header=BB2_3 Depth=1
	movzbl	%cl, %edi
	callq	store_var
	jmp	.LBB2_3
.LBB2_100:                              # %.preheader99.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	%r9d, %eax
	sarl	$10, %eax
	addl	$2, %r10d
	movl	%r10d, pc+4(%rip)
	movl	%r9d, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%r9d, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %r9d
	cltq
	movq	8(%r8,%rax,8), %rax
	movslq	%r9d, %rcx
	movb	(%rax,%rcx), %al
	cmpb	$34, %al
	je	.LBB2_103
	.p2align	4, 0x90
.LBB2_101:                              # %.preheader99
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	%al, %edi
	callq	out_char
	movl	pc+4(%rip), %eax
	movl	%eax, %ecx
	sarl	$10, %ecx
	leal	1(%rax), %edx
	movslq	pc(%rip), %rsi
	imulq	$168, %rsi, %rsi
	addq	functions(%rip), %rsi
	movl	%edx, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%ecx, %rcx
	movq	8(%rsi,%rcx,8), %rcx
	cltq
	movzbl	(%rcx,%rax), %eax
	cmpb	$34, %al
	jne	.LBB2_101
.LBB2_103:                              #   in Loop: Header=BB2_3 Depth=1
	cmpb	$0, interactive(%rip)
	je	.LBB2_3
.LBB2_104:                              #   in Loop: Header=BB2_3 Depth=1
	movq	stdout(%rip), %rdi
	callq	fflush
	jmp	.LBB2_3
.LBB2_105:                              #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#106:                               #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	ex_stack(%rip), %rax
	movq	8(%rax), %rax
	movq	%rcx, (%rax)
	jmp	.LBB2_3
.LBB2_107:                              #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#108:                               #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	bc_compare
	testl	%eax, %eax
	setle	c_code(%rip)
	jmp	.LBB2_129
.LBB2_109:                              #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#110:                               #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	je	.LBB2_127
# BB#111:                               #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	sete	%al
	jmp	.LBB2_128
.LBB2_112:                              #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edi
	callq	check_stack
	testb	%al, %al
	je	.LBB2_3
# BB#113:                               #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	bc_compare
	testl	%eax, %eax
	setns	c_code(%rip)
	jmp	.LBB2_129
.LBB2_114:                              #   in Loop: Header=BB2_3 Depth=1
	cmpb	$90, %r14b
	jne	.LBB2_3
# BB#115:                               #   in Loop: Header=BB2_3 Depth=1
	movb	c_code(%rip), %cl
	testb	%cl, %cl
	jne	.LBB2_3
.LBB2_116:                              #   in Loop: Header=BB2_3 Depth=1
	shll	$8, %edx
	orl	%eax, %edx
	shrl	$6, %edx
	andb	$63, %al
	movq	144(%rdi,%r8), %rcx
	testl	%edx, %edx
	je	.LBB2_119
# BB#117:                               # %.lr.ph110.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	incl	%edx
	.p2align	4, 0x90
.LBB2_118:                              # %.lr.ph110
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	512(%rcx), %rcx
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB2_118
.LBB2_119:                              # %._crit_edge111
                                        #   in Loop: Header=BB2_3 Depth=1
	movzbl	%al, %eax
	movl	(%rcx,%rax,8), %eax
	movl	%eax, pc+4(%rip)
	jmp	.LBB2_3
.LBB2_120:                              #   in Loop: Header=BB2_3 Depth=1
	movl	$prog_char, %edi
	callq	push_constant
	jmp	.LBB2_3
.LBB2_121:                              #   in Loop: Header=BB2_3 Depth=1
	movq	f_names(%rip), %rax
	movq	(%rax,%r14,8), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	rt_error
	jmp	.LBB2_3
.LBB2_122:                              #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.3, %edi
.LBB2_123:                              # %.thread.backedge
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%eax, %eax
	callq	rt_error
	jmp	.LBB2_3
.LBB2_124:                              #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movl	scale(%rip), %ecx
	movq	%rsp, %rbx
	movq	%rbx, %rdx
	callq	bc_modulo
.LBB2_125:                              # %.thread.backedge
                                        #   in Loop: Header=BB2_3 Depth=1
	callq	pop
	callq	pop
	movq	(%rsp), %rdi
	callq	push_num
	movq	%rbx, %rdi
	callq	init_num
	jmp	.LBB2_3
.LBB2_126:                              #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	sete	%al
	movb	%al, c_code(%rip)
	jmp	.LBB2_129
.LBB2_127:                              #   in Loop: Header=BB2_3 Depth=1
	movb	$1, %al
.LBB2_128:                              # %.thread.backedge
                                        #   in Loop: Header=BB2_3 Depth=1
	movb	%al, c_code(%rip)
.LBB2_129:                              # %.thread.backedge
                                        #   in Loop: Header=BB2_3 Depth=1
	callq	pop
	cmpb	$0, c_code(%rip)
	movq	ex_stack(%rip), %rdi
	movl	$_one_, %eax
	movl	$_zero_, %ebx
	cmovneq	%rax, %rbx
	callq	free_num
	movq	(%rbx), %rdi
	jmp	.LBB2_2
.LBB2_130:                              #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rdi
	movq	(%rdi), %rcx
	movl	4(%rcx), %eax
	movl	8(%rcx), %esi
	cmpl	$1, %eax
	jne	.LBB2_138
# BB#131:                               #   in Loop: Header=BB2_3 Depth=1
	testl	%esi, %esi
	je	.LBB2_137
# BB#132:                               #   in Loop: Header=BB2_3 Depth=1
	cmpb	$0, 16(%rcx)
	jne	.LBB2_138
# BB#133:                               #   in Loop: Header=BB2_3 Depth=1
	callq	int2num
	jmp	.LBB2_3
.LBB2_134:                              #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rdi
	movl	scale(%rip), %esi
	callq	bc_sqrt
	testl	%eax, %eax
	jne	.LBB2_3
# BB#135:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.4, %edi
	jmp	.LBB2_123
.LBB2_136:                              #   in Loop: Header=BB2_3 Depth=1
	movq	ex_stack(%rip), %rdi
	movq	(%rdi), %rax
	movl	8(%rax), %esi
	callq	int2num
	jmp	.LBB2_3
.LBB2_137:                              #   in Loop: Header=BB2_3 Depth=1
	xorl	%esi, %esi
.LBB2_138:                              # %._crit_edge117
                                        #   in Loop: Header=BB2_3 Depth=1
	addl	%eax, %esi
	callq	int2num
	jmp	.LBB2_3
.LBB2_139:                              # %.critedgethread-pre-split
	testl	%eax, %eax
	jne	.LBB2_141
	jmp	.LBB2_143
	.p2align	4, 0x90
.LBB2_140:                              # %.critedge..critedge_crit_edge
                                        #   in Loop: Header=BB2_141 Depth=1
	movq	functions(%rip), %rdi
.LBB2_141:                              # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	cltq
	imulq	$168, %rax, %rax
	movq	160(%rdi,%rax), %rdi
	callq	pop_vars
	movq	functions(%rip), %rax
	movslq	pc(%rip), %rcx
	imulq	$168, %rcx, %rcx
	movq	152(%rax,%rcx), %rdi
	callq	pop_vars
	callq	fpop
	callq	fpop
	movl	%eax, pc+4(%rip)
	callq	fpop
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%eax, pc(%rip)
	testl	%eax, %eax
	jne	.LBB2_140
	jmp	.LBB2_143
	.p2align	4, 0x90
.LBB2_142:                              # %.lr.ph
                                        #   in Loop: Header=BB2_143 Depth=1
	callq	pop
.LBB2_143:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, ex_stack(%rip)
	jne	.LBB2_142
# BB#144:                               # %._crit_edge
	cmpb	$0, interactive(%rip)
	je	.LBB2_147
# BB#145:
	movl	$2, %edi
	movl	$use_quit, %esi
	callq	signal
	cmpl	$0, had_sigint(%rip)
	je	.LBB2_147
# BB#146:
	movl	$.Lstr, %edi
	callq	puts
.LBB2_147:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_148:
	xorl	%edi, %edi
	callq	exit
.Lfunc_end2:
	.size	execute, .Lfunc_end2-execute
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_28
	.quad	.LBB2_29
	.quad	.LBB2_30
	.quad	.LBB2_29
	.quad	.LBB2_32
	.quad	.LBB2_35
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_38
	.quad	.LBB2_40
	.quad	.LBB2_29
	.quad	.LBB2_42
	.quad	.LBB2_29
	.quad	.LBB2_44
	.quad	.LBB2_47
	.quad	.LBB2_48
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_49
	.quad	.LBB2_51
	.quad	.LBB2_53
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_55
	.quad	.LBB2_7
	.quad	.LBB2_58
	.quad	.LBB2_64
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_8
	.quad	.LBB2_65
	.quad	.LBB2_67
	.quad	.LBB2_70
	.quad	.LBB2_29
	.quad	.LBB2_12
	.quad	.LBB2_25
	.quad	.LBB2_29
	.quad	.LBB2_73
	.quad	.LBB2_75
	.quad	.LBB2_28
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_25
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_7
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_78
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_83
	.quad	.LBB2_86
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_148
	.quad	.LBB2_89
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_92
	.quad	.LBB2_29
	.quad	.LBB2_95
	.quad	.LBB2_29
	.quad	.LBB2_96
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_97
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_100
	.quad	.LBB2_105
	.quad	.LBB2_29
	.quad	.LBB2_29
	.quad	.LBB2_107
	.quad	.LBB2_109
	.quad	.LBB2_112
.LJTI2_1:
	.quad	.LBB2_85
	.quad	.LBB2_3
	.quad	.LBB2_3
	.quad	.LBB2_130
	.quad	.LBB2_3
	.quad	.LBB2_3
	.quad	.LBB2_3
	.quad	.LBB2_3
	.quad	.LBB2_3
	.quad	.LBB2_134
	.quad	.LBB2_136
.LJTI2_2:
	.quad	.LBB2_19
	.quad	.LBB2_11
	.quad	.LBB2_11
	.quad	.LBB2_20
	.quad	.LBB2_11
	.quad	.LBB2_21
	.quad	.LBB2_11
	.quad	.LBB2_11
	.quad	.LBB2_11
	.quad	.LBB2_22
	.quad	.LBB2_11
	.quad	.LBB2_23
	.quad	.LBB2_24

	.text
	.globl	push_b10_const
	.p2align	4, 0x90
	.type	push_b10_const,@function
push_b10_const:                         # @push_b10_const
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	(%r15), %r8
	movq	%r8, %rsi
	shrq	$32, %rsi
	movq	functions(%rip), %r9
	movslq	%r8d, %rdx
	xorl	%edi, %edi
	imulq	$168, %rdx, %r10
	addq	%r9, %r10
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_1:                                #   in Loop: Header=BB3_2 Depth=1
	incl	%edi
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	leal	(%rsi,%rdi), %ebp
	movl	%ebp, %ebx
	sarl	$10, %ebx
	movl	%ebp, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebp, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	movl	%ebp, %eax
	subl	%ecx, %eax
	movslq	%ebx, %rcx
	movq	8(%r10,%rcx,8), %rcx
	cltq
	movzbl	(%rcx,%rax), %ebx
	cmpb	$58, %bl
	je	.LBB3_7
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpb	$46, %bl
	jne	.LBB3_1
# BB#4:                                 # %.preheader43
	leal	1(%rsi,%rdi), %eax
	movl	%eax, %ecx
	sarl	$10, %ecx
	movl	%eax, %esi
	sarl	$31, %esi
	shrl	$22, %esi
	leal	1(%rsi,%rbp), %esi
	andl	$-1024, %esi            # imm = 0xFC00
	subl	%esi, %eax
	movslq	%ecx, %rcx
	movq	8(%r10,%rcx,8), %rcx
	cltq
	xorl	%esi, %esi
	cmpb	$58, (%rcx,%rax)
	je	.LBB3_8
# BB#5:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%rbx), %eax
	leal	2(%rbp,%rbx), %ecx
	movl	%ebx, %esi
	incl	%esi
	leal	2(%rbp,%rbx), %ebx
	sarl	$10, %ebx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	leal	2(%rdx,%rax), %eax
	andl	$-1024, %eax            # imm = 0xFC00
	subl	%eax, %ecx
	movslq	%ebx, %rax
	movq	8(%r10,%rax,8), %rax
	movslq	%ecx, %rcx
	cmpb	$58, (%rax,%rcx)
	movl	%esi, %ebx
	jne	.LBB3_6
	jmp	.LBB3_8
.LBB3_7:                                # %.loopexit.loopexit68
	xorl	%esi, %esi
.LBB3_8:                                # %.loopexit
	movl	4(%r15), %eax
	movl	%eax, %ecx
	sarl	$10, %ecx
	leal	1(%rax), %edx
	movl	%edx, 4(%r15)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	%r8d, %rdx
	movslq	%ecx, %rcx
	imulq	$168, %rdx, %rdx
	addq	%rdx, %r9
	movq	8(%r9,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %bl
	cmpl	$1, %edi
	jne	.LBB3_13
# BB#9:                                 # %.loopexit
	testl	%esi, %esi
	jne	.LBB3_13
# BB#10:
	cmpb	$1, %bl
	je	.LBB3_22
# BB#11:
	testb	%bl, %bl
	jne	.LBB3_24
# BB#12:
	movq	_zero_(%rip), %rdi
	jmp	.LBB3_23
.LBB3_13:
	testl	%edi, %edi
	je	.LBB3_15
.LBB3_14:                               # %.thread
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	new_num
	movq	%rax, (%rsp)
	addq	$16, %rax
	cmpb	$46, %bl
	jne	.LBB3_17
	jmp	.LBB3_16
.LBB3_15:
	movl	$1, %edi
	callq	new_num
	movq	%rax, (%rsp)
	movb	$0, 16(%rax)
	addq	$17, %rax
	cmpb	$46, %bl
	jne	.LBB3_17
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_18:                               # %.sink.split
	cmpb	$10, %bl
	jl	.LBB3_20
# BB#19:                                # %.sink.split
	movb	$9, %bl
.LBB3_20:                               # %.sink.split
	movb	%bl, (%rax)
	incq	%rax
.LBB3_16:                               # =>This Inner Loop Header: Depth=1
	movl	4(%r15), %ecx
	movl	%ecx, %edx
	sarl	$10, %edx
	leal	1(%rcx), %esi
	movl	%esi, 4(%r15)
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$22, %esi
	addl	%ecx, %esi
	andl	$-1024, %esi            # imm = 0xFC00
	subl	%esi, %ecx
	movslq	(%r15), %rsi
	movslq	%edx, %rdx
	imulq	$168, %rsi, %rsi
	addq	functions(%rip), %rsi
	movq	8(%rsi,%rdx,8), %rdx
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx), %ebx
	cmpb	$46, %bl
	je	.LBB3_16
.LBB3_17:                               # %.preheader
	cmpb	$58, %bl
	jne	.LBB3_18
# BB#21:
	movq	(%rsp), %rdi
	callq	push_num
	jmp	.LBB3_27
.LBB3_22:
	movq	_one_(%rip), %rdi
.LBB3_23:
	callq	push_copy
	jmp	.LBB3_26
.LBB3_24:
	cmpb	$10, %bl
	jl	.LBB3_14
# BB#25:
	movsbl	%bl, %ebp
	movq	%rsp, %r14
	movq	%r14, %rdi
	callq	init_num
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	int2num
	movq	(%rsp), %rdi
	callq	push_num
.LBB3_26:
	incl	4(%r15)
.LBB3_27:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	push_b10_const, .Lfunc_end3-push_b10_const
	.cfi_endproc

	.globl	push_constant
	.p2align	4, 0x90
	.type	push_constant,@function
push_constant:                          # @push_constant
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 112
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	leaq	32(%rsp), %rdi
	callq	init_num
	leaq	8(%rsp), %rdi
	callq	init_num
	leaq	40(%rsp), %rbx
	movq	%rbx, %rdi
	callq	init_num
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 16(%rsp)
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	int2num
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	callq	*%r15
	movl	%eax, %ebx
	cmpb	$32, %bl
	je	.LBB4_1
# BB#2:
	cmpb	$45, %bl
	je	.LBB4_5
# BB#3:
	cmpb	$43, %bl
	jne	.LBB4_7
# BB#4:                                 # %.sink.split.loopexit65
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	jmp	.LBB4_6
.LBB4_5:                                # %.sink.split.loopexit
	movb	$1, %al
	movl	%eax, 28(%rsp)          # 4-byte Spill
.LBB4_6:                                # %.sink.split
	callq	*%r15
	movl	%eax, %ebx
	cmpb	$15, %bl
	jle	.LBB4_10
	jmp	.LBB4_9
.LBB4_7:                                # %.loopexit.loopexit
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	cmpb	$15, %bl
	jg	.LBB4_9
.LBB4_10:                               # %.preheader
	movsbl	%bl, %r12d
	callq	*%r15
	movl	%eax, %ebp
	leal	255(%r14), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	cmpb	$15, %bpl
	movl	%ebx, %eax
	jg	.LBB4_12
# BB#11:                                # %.preheader
	movl	4(%rsp), %eax           # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
.LBB4_12:                               # %.preheader
	cmpl	%r14d, %r12d
	jl	.LBB4_14
# BB#13:                                # %.preheader
	movl	%eax, %ebx
.LBB4_14:                               # %.preheader
	movsbl	%bl, %esi
	leaq	16(%rsp), %rdi
	callq	int2num
	cmpb	$15, %bpl
	jg	.LBB4_19
# BB#15:                                # %.lr.ph49
	leaq	8(%rsp), %r12
	leaq	32(%rsp), %r13
	leaq	16(%rsp), %rbx
	.p2align	4, 0x90
.LBB4_16:                               # =>This Inner Loop Header: Depth=1
	movsbl	%bpl, %eax
	cmpl	%r14d, %eax
	jl	.LBB4_18
# BB#17:                                #   in Loop: Header=BB4_16 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, %ebp
.LBB4_18:                               #   in Loop: Header=BB4_16 Depth=1
	movq	16(%rsp), %rdi
	movq	40(%rsp), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	callq	bc_multiply
	movsbl	%bpl, %esi
	movq	%r13, %rdi
	callq	int2num
	movq	8(%rsp), %rdi
	movq	32(%rsp), %rsi
	movq	%rbx, %rdx
	callq	bc_add
	callq	*%r15
	movl	%eax, %ebp
	cmpb	$16, %bpl
	jl	.LBB4_16
	jmp	.LBB4_19
.LBB4_9:
	movl	%ebx, %ebp
.LBB4_19:                               # %._crit_edge50
	cmpb	$46, %bpl
	jne	.LBB4_31
# BB#20:
	callq	*%r15
	movl	%eax, %ebx
	movsbl	%bl, %eax
	leal	255(%r14), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	cmpl	%r14d, %eax
	jl	.LBB4_22
# BB#21:
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, %ebx
.LBB4_22:
	leaq	8(%rsp), %rdi
	callq	free_num
	leaq	32(%rsp), %rdi
	callq	free_num
	movq	_one_(%rip), %rdi
	callq	copy_num
	movq	%rax, %r12
	movq	%r12, 48(%rsp)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, %rdi
	movq	%rdi, 8(%rsp)
	xorl	%ebp, %ebp
	cmpb	$15, %bl
	jg	.LBB4_30
# BB#23:                                # %.lr.ph.preheader
	leaq	8(%rsp), %r12
	leaq	48(%rsp), %r13
	.p2align	4, 0x90
.LBB4_24:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%bl, %ebx
	movq	40(%rsp), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	callq	bc_multiply
	leaq	32(%rsp), %rdi
	movl	%ebx, %esi
	callq	int2num
	movq	8(%rsp), %rdi
	movq	32(%rsp), %rsi
	movq	%r12, %rdx
	callq	bc_add
	movq	48(%rsp), %rdi
	movq	40(%rsp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	callq	bc_multiply
	callq	*%r15
	movl	%eax, %ebx
	movsbl	%bl, %eax
	cmpl	%r14d, %eax
	movl	%ebx, %eax
	jl	.LBB4_26
# BB#25:                                # %.lr.ph
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
.LBB4_26:                               # %.lr.ph
                                        #   in Loop: Header=BB4_24 Depth=1
	cmpb	$15, %bl
	jg	.LBB4_28
# BB#27:                                # %.lr.ph
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	%eax, %ebx
.LBB4_28:                               # %.lr.ph
                                        #   in Loop: Header=BB4_24 Depth=1
	incl	%ebp
	cmpb	$16, %bl
	movq	8(%rsp), %rdi
	jl	.LBB4_24
# BB#29:                                # %._crit_edge.loopexit
	movq	48(%rsp), %r12
.LBB4_30:                               # %._crit_edge
	leaq	8(%rsp), %rdx
	movq	%r12, %rsi
	movl	%ebp, %ecx
	callq	bc_divide
	movq	16(%rsp), %rdi
	movq	8(%rsp), %rsi
	leaq	16(%rsp), %rdx
	callq	bc_add
.LBB4_31:
	cmpb	$0, 28(%rsp)            # 1-byte Folded Reload
	je	.LBB4_33
# BB#32:
	movq	_zero_(%rip), %rdi
	movq	16(%rsp), %rsi
	leaq	16(%rsp), %rdx
	callq	bc_sub
.LBB4_33:
	movq	16(%rsp), %rdi
	callq	push_num
	leaq	32(%rsp), %rdi
	callq	free_num
	leaq	8(%rsp), %rdi
	callq	free_num
	leaq	40(%rsp), %rdi
	callq	free_num
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	push_constant, .Lfunc_end4-push_constant
	.cfi_endproc

	.globl	prog_char
	.p2align	4, 0x90
	.type	prog_char,@function
prog_char:                              # @prog_char
	.cfi_startproc
# BB#0:
	movl	pc+4(%rip), %eax
	movl	%eax, %ecx
	sarl	$10, %ecx
	leal	1(%rax), %edx
	movl	%edx, pc+4(%rip)
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$22, %edx
	addl	%eax, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	subl	%edx, %eax
	movslq	pc(%rip), %rdx
	movslq	%ecx, %rcx
	imulq	$168, %rdx, %rdx
	addq	functions(%rip), %rdx
	movq	8(%rdx,%rcx,8), %rcx
	cltq
	movb	(%rcx,%rax), %al
	retq
.Lfunc_end5:
	.size	prog_char, .Lfunc_end5-prog_char
	.cfi_endproc

	.globl	assign
	.p2align	4, 0x90
	.type	assign,@function
assign:                                 # @assign
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movq	ex_stack(%rip), %rdi
	callq	free_num
	testb	%bl, %bl
	movl	$_one_, %eax
	movl	$_zero_, %ecx
	cmovneq	%rax, %rcx
	movq	(%rcx), %rdi
	callq	copy_num
	movq	ex_stack(%rip), %rcx
	movq	%rax, (%rcx)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	assign, .Lfunc_end6-assign
	.cfi_endproc

	.globl	input_char
	.p2align	4, 0x90
	.type	input_char,@function
input_char:                             # @input_char
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 16
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpb	$92, %bl
	jne	.LBB7_3
# BB#1:
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpb	$10, %bl
	jne	.LBB7_3
# BB#2:
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
.LBB7_3:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	%bl, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB7_4
# BB#5:
	movl	%ebx, %eax
	addb	$-65, %al
	cmpb	$5, %al
	ja	.LBB7_7
# BB#6:
	addb	$-55, %bl
	jmp	.LBB7_13
.LBB7_4:
	addb	$-48, %bl
	jmp	.LBB7_13
.LBB7_7:
	movl	%ebx, %eax
	addb	$-97, %al
	cmpb	$5, %al
	ja	.LBB7_9
# BB#8:
	addb	$-87, %bl
	jmp	.LBB7_13
.LBB7_9:
	cmpb	$46, %bl
	ja	.LBB7_11
# BB#10:
	movzbl	%bl, %eax
	movabsq	$114349209288704, %rcx  # imm = 0x680000000000
	btq	%rax, %rcx
	jae	.LBB7_11
.LBB7_13:
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB7_11:
	cmpb	$33, %bl
	movb	$32, %bl
	jl	.LBB7_13
# BB#12:
	movb	$58, %bl
	jmp	.LBB7_13
.Lfunc_end7:
	.size	input_char, .Lfunc_end7-input_char
	.cfi_endproc

	.type	had_sigint,@object      # @had_sigint
	.comm	had_sigint,4,4
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"interrupted execution"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Function %s not defined."
	.size	.L.str.2, 25

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Return from main program."
	.size	.L.str.3, 26

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Square root of a negative number"
	.size	.L.str.4, 33

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Divide by zero"
	.size	.L.str.5, 15

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Modulo by zero"
	.size	.L.str.6, 15

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"divide by zero"
	.size	.L.str.7, 15

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"bad instruction: inst=%c"
	.size	.L.str.8, 25

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Interruption completed."
	.size	.Lstr, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
