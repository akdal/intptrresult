	.text
	.file	"ZipOut.bc"
	.globl	_ZN8NArchive4NZip11COutArchive6CreateEP10IOutStream
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive6CreateEP10IOutStream,@function
_ZN8NArchive4NZip11COutArchive6CreateEP10IOutStream: # @_ZN8NArchive4NZip11COutArchive6CreateEP10IOutStream
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	leaq	8(%r15), %r14
	movl	$65536, %esi            # imm = 0x10000
	movq	%r14, %rdi
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	je	.LBB0_6
# BB#1:
	testq	%rbx, %rbx
	je	.LBB0_3
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB0_3:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB0_5:                                # %_ZN9CMyComPtrI10IOutStreamEaSEPS0_.exit
	movq	%rbx, (%r15)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
	movq	%r14, %rdi
	callq	_ZN10COutBuffer4InitEv
	movq	$0, 64(%r15)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB0_6:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$-2147024882, (%rax)    # imm = 0x8007000E
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end0:
	.size	_ZN8NArchive4NZip11COutArchive6CreateEP10IOutStream, .Lfunc_end0-_ZN8NArchive4NZip11COutArchive6CreateEP10IOutStream
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive16MoveBasePositionEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive16MoveBasePositionEy,@function
_ZN8NArchive4NZip11COutArchive16MoveBasePositionEy: # @_ZN8NArchive4NZip11COutArchive16MoveBasePositionEy
	.cfi_startproc
# BB#0:
	addq	%rsi, 64(%rdi)
	retq
.Lfunc_end1:
	.size	_ZN8NArchive4NZip11COutArchive16MoveBasePositionEy, .Lfunc_end1-_ZN8NArchive4NZip11COutArchive16MoveBasePositionEy
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive31PrepareWriteCompressedDataZip64Etbb
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive31PrepareWriteCompressedDataZip64Etbb,@function
_ZN8NArchive4NZip11COutArchive31PrepareWriteCompressedDataZip64Etbb: # @_ZN8NArchive4NZip11COutArchive31PrepareWriteCompressedDataZip64Etbb
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movb	%dl, 80(%rdi)
	xorl	%eax, %eax
	testb	%dl, %dl
	movl	$20, %edx
	cmovel	%eax, %edx
	movl	%edx, %eax
	orl	$11, %eax
	testb	%cl, %cl
	cmovel	%edx, %eax
	movl	%eax, 76(%rdi)
	leal	30(%rsi,%rax), %eax
	movl	%eax, 72(%rdi)
	retq
.Lfunc_end2:
	.size	_ZN8NArchive4NZip11COutArchive31PrepareWriteCompressedDataZip64Etbb, .Lfunc_end2-_ZN8NArchive4NZip11COutArchive31PrepareWriteCompressedDataZip64Etbb
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive26PrepareWriteCompressedDataEtyb
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive26PrepareWriteCompressedDataEtyb,@function
_ZN8NArchive4NZip11COutArchive26PrepareWriteCompressedDataEtyb: # @_ZN8NArchive4NZip11COutArchive26PrepareWriteCompressedDataEtyb
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	shrq	$27, %rdx
	xorl	%eax, %eax
	cmpq	$30, %rdx
	seta	80(%rdi)
	movl	$20, %edx
	cmovbel	%eax, %edx
	movl	%edx, %eax
	orl	$11, %eax
	testb	%cl, %cl
	cmovel	%edx, %eax
	movl	%eax, 76(%rdi)
	leal	30(%rsi,%rax), %eax
	movl	%eax, 72(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN8NArchive4NZip11COutArchive26PrepareWriteCompressedDataEtyb, .Lfunc_end3-_ZN8NArchive4NZip11COutArchive26PrepareWriteCompressedDataEtyb
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive27PrepareWriteCompressedData2Etyyb
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive27PrepareWriteCompressedData2Etyyb,@function
_ZN8NArchive4NZip11COutArchive27PrepareWriteCompressedData2Etyyb: # @_ZN8NArchive4NZip11COutArchive27PrepareWriteCompressedData2Etyyb
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	$4294967294, %eax       # imm = 0xFFFFFFFE
	cmpq	%rax, %rdx
	seta	%dl
	cmpq	%rax, %rcx
	seta	%al
	xorl	%ecx, %ecx
	orb	%dl, %al
	movb	%al, 80(%rdi)
	movl	$20, %eax
	cmovel	%ecx, %eax
	movl	%eax, %ecx
	orl	$11, %ecx
	testb	%r8b, %r8b
	cmovel	%eax, %ecx
	movl	%ecx, 76(%rdi)
	leal	30(%rsi,%rcx), %eax
	movl	%eax, 72(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN8NArchive4NZip11COutArchive27PrepareWriteCompressedData2Etyyb, .Lfunc_end4-_ZN8NArchive4NZip11COutArchive27PrepareWriteCompressedData2Etyyb
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj,@function
_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj: # @_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -48
.Lcfi12:
	.cfi_offset %r12, -40
.Lcfi13:
	.cfi_offset %r13, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	%edx, %r12d
	testl	%edx, %edx
	je	.LBB5_5
# BB#1:                                 # %.lr.ph.i
	leaq	8(%r14), %r15
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%r13), %eax
	movq	8(%r14), %rcx
	movl	16(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 16(%r14)
	movb	%al, (%rcx,%rdx)
	movl	16(%r14), %eax
	cmpl	20(%r14), %eax
	jne	.LBB5_4
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB5_4:                                # %_ZN10COutBuffer9WriteByteEh.exit.i
                                        #   in Loop: Header=BB5_2 Depth=1
	incq	%r13
	decq	%rbx
	jne	.LBB5_2
.LBB5_5:                                # %_ZN10COutBuffer10WriteBytesEPKvm.exit
	addq	%r12, 64(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj, .Lfunc_end5-_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive9WriteByteEh
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive9WriteByteEh,@function
_ZN8NArchive4NZip11COutArchive9WriteByteEh: # @_ZN8NArchive4NZip11COutArchive9WriteByteEh
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	16(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%rbx)
	movb	%sil, (%rax,%rcx)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB6_2
# BB#1:
	leaq	8(%rbx), %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB6_2:                                # %_ZN10COutBuffer9WriteByteEh.exit.i.i
	incq	64(%rbx)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZN8NArchive4NZip11COutArchive9WriteByteEh, .Lfunc_end6-_ZN8NArchive4NZip11COutArchive9WriteByteEh
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive11WriteUInt16Et
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive11WriteUInt16Et,@function
_ZN8NArchive4NZip11COutArchive11WriteUInt16Et: # @_ZN8NArchive4NZip11COutArchive11WriteUInt16Et
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %r14
	movq	8(%rbx), %rax
	movl	16(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%rbx)
	movl	%esi, %ebp
	movb	%sil, (%rax,%rcx)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB7_2
# BB#1:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB7_2:                                # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit
	incq	64(%rbx)
	movq	8(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%rbx)
	movl	%eax, %eax
	movl	%ebp, %edx
	movb	%dh, (%rcx,%rax)  # NOREX
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB7_4
# BB#3:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB7_4:                                # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.1
	incq	64(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN8NArchive4NZip11COutArchive11WriteUInt16Et, .Lfunc_end7-_ZN8NArchive4NZip11COutArchive11WriteUInt16Et
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej,@function
_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej: # @_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %r14
	movq	8(%rbx), %rax
	movl	16(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%rbx)
	movl	%esi, %ebp
	movb	%sil, (%rax,%rcx)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB8_2
# BB#1:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB8_2:                                # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit
	incq	64(%rbx)
	movq	8(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%rbx)
	movl	%eax, %eax
	movl	%ebp, %edx
	movb	%dh, (%rcx,%rax)  # NOREX
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB8_4
# BB#3:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB8_4:                                # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.1
	incq	64(%rbx)
	movl	%ebp, %ecx
	shrl	$16, %ecx
	movq	8(%rbx), %rdx
	leal	1(%rax), %esi
	movl	%esi, 16(%rbx)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB8_6
# BB#5:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB8_6:                                # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.2
	incq	64(%rbx)
	shrl	$24, %ebp
	movq	8(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%rbx)
	movl	%eax, %eax
	movb	%bpl, (%rcx,%rax)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB8_8
# BB#7:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB8_8:                                # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.3
	incq	64(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej, .Lfunc_end8-_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey,@function
_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey: # @_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %r14
	movq	8(%rbx), %rax
	movl	16(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%rbx)
	movq	%rsi, %r15
	movb	%sil, (%rax,%rcx)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB9_2
# BB#1:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB9_2:                                # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit
	incq	64(%rbx)
	movq	8(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%rbx)
	movl	%eax, %eax
	movq	%r15, %rdx
	movb	%dh, (%rcx,%rax)  # NOREX
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB9_4
# BB#3:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB9_4:                                # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.1
	incq	64(%rbx)
	movq	%r15, %rcx
	shrq	$16, %rcx
	movq	8(%rbx), %rdx
	leal	1(%rax), %esi
	movl	%esi, 16(%rbx)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB9_6
# BB#5:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB9_6:                                # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.2
	incq	64(%rbx)
	movq	%r15, %rcx
	shrq	$24, %rcx
	movq	8(%rbx), %rdx
	leal	1(%rax), %esi
	movl	%esi, 16(%rbx)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB9_8
# BB#7:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB9_8:                                # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.3
	incq	64(%rbx)
	movq	%r15, %rcx
	shrq	$32, %rcx
	movq	8(%rbx), %rdx
	leal	1(%rax), %esi
	movl	%esi, 16(%rbx)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB9_10
# BB#9:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB9_10:                               # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.4
	incq	64(%rbx)
	movq	%r15, %rcx
	shrq	$40, %rcx
	movq	8(%rbx), %rdx
	leal	1(%rax), %esi
	movl	%esi, 16(%rbx)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB9_12
# BB#11:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB9_12:                               # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.5
	incq	64(%rbx)
	movq	%r15, %rcx
	shrq	$48, %rcx
	movq	8(%rbx), %rdx
	leal	1(%rax), %esi
	movl	%esi, 16(%rbx)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB9_14
# BB#13:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%rbx), %eax
.LBB9_14:                               # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.6
	incq	64(%rbx)
	shrq	$56, %r15
	movq	8(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%rbx)
	movl	%eax, %eax
	movb	%r15b, (%rcx,%rax)
	movl	16(%rbx), %eax
	cmpl	20(%rbx), %eax
	jne	.LBB9_16
# BB#15:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB9_16:                               # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.7
	incq	64(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey, .Lfunc_end9-_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive10WriteExtraERKNS0_11CExtraBlockE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive10WriteExtraERKNS0_11CExtraBlockE,@function
_ZN8NArchive4NZip11COutArchive10WriteExtraERKNS0_11CExtraBlockE: # @_ZN8NArchive4NZip11COutArchive10WriteExtraERKNS0_11CExtraBlockE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	cmpl	$0, 12(%rsi)
	jle	.LBB10_17
# BB#1:                                 # %.lr.ph
	leaq	8(%r13), %r15
	xorl	%r12d, %r12d
	movq	%rsi, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB10_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_12 Depth 2
	movq	16(%rsi), %rax
	movq	(%rax,%r12,8), %rbp
	movw	(%rbp), %bx
	movq	8(%r13), %rax
	movl	16(%r13), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r13)
	movb	%bl, (%rax,%rcx)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB10_4:                               # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i14
                                        #   in Loop: Header=BB10_2 Depth=1
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB10_6
# BB#5:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB10_6:                               # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit15
                                        #   in Loop: Header=BB10_2 Depth=1
	incq	64(%r13)
	movq	16(%rbp), %rbx
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bl, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB10_8
# BB#7:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB10_8:                               # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i
                                        #   in Loop: Header=BB10_2 Depth=1
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB10_10
# BB#9:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB10_10:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	64(%r13), %rax
	incq	%rax
	movq	%rax, 64(%r13)
	movl	16(%rbp), %r14d
	testl	%r14d, %r14d
	movq	(%rsp), %rsi            # 8-byte Reload
	je	.LBB10_16
# BB#11:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	24(%rbp), %rbp
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB10_12:                              # %.lr.ph.i.i
                                        #   Parent Loop BB10_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %eax
	movq	8(%r13), %rcx
	movl	16(%r13), %edx
	leal	1(%rdx), %esi
	movl	%esi, 16(%r13)
	movb	%al, (%rcx,%rdx)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB10_14
# BB#13:                                #   in Loop: Header=BB10_12 Depth=2
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB10_14:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB10_12 Depth=2
	incq	%rbp
	decq	%rbx
	jne	.LBB10_12
# BB#15:                                # %_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj.exit.loopexit
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	64(%r13), %rax
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB10_16:                              # %_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj.exit
                                        #   in Loop: Header=BB10_2 Depth=1
	addq	%r14, %rax
	movq	%rax, 64(%r13)
	incq	%r12
	movslq	12(%rsi), %rax
	cmpq	%rax, %r12
	jl	.LBB10_2
.LBB10_17:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN8NArchive4NZip11COutArchive10WriteExtraERKNS0_11CExtraBlockE, .Lfunc_end10-_ZN8NArchive4NZip11COutArchive10WriteExtraERKNS0_11CExtraBlockE
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive6SeekToEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive6SeekToEy,@function
_ZN8NArchive4NZip11COutArchive6SeekToEy: # @_ZN8NArchive4NZip11COutArchive6SeekToEy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 16
.Lcfi50:
	.cfi_offset %rbx, -16
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jne	.LBB11_2
# BB#1:
	popq	%rbx
	retq
.LBB11_2:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%ebx, (%rax)
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end11:
	.size	_ZN8NArchive4NZip11COutArchive6SeekToEy, .Lfunc_end11-_ZN8NArchive4NZip11COutArchive6SeekToEy
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE,@function
_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE: # @_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 64
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	(%r12), %rdi
	movq	64(%r12), %rsi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB12_1
# BB#3:                                 # %_ZN8NArchive4NZip11COutArchive6SeekToEy.exit
	movb	$1, %r13b
	cmpb	$0, 80(%r12)
	jne	.LBB12_6
# BB#4:
	movl	$4294967294, %eax       # imm = 0xFFFFFFFE
	cmpq	%rax, 16(%r14)
	ja	.LBB12_6
# BB#5:
	cmpq	24(%r14), %rax
	setb	%r13b
.LBB12_6:
	movl	_ZN8NArchive4NZip10NSignature16kLocalFileHeaderE(%rip), %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movb	(%r14), %cl
	cmpb	$45, %cl
	movb	$45, %al
	jb	.LBB12_8
# BB#7:
	movl	%ecx, %eax
.LBB12_8:
	testb	%r13b, %r13b
	jne	.LBB12_10
# BB#9:
	movl	%ecx, %eax
.LBB12_10:
	leaq	8(%r12), %r15
	movq	8(%r12), %rcx
	movl	16(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 16(%r12)
	movb	%al, (%rcx,%rdx)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_12
# BB#11:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r12), %eax
.LBB12_12:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit
	incq	64(%r12)
	movb	1(%r14), %cl
	movq	8(%r12), %rdx
	leal	1(%rax), %esi
	movl	%esi, 16(%r12)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_14
# BB#13:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r12), %eax
.LBB12_14:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit35
	incq	64(%r12)
	movw	2(%r14), %bx
	movq	8(%r12), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %eax
	movb	%bl, (%rcx,%rax)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_16
# BB#15:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r12), %eax
.LBB12_16:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i
	incq	64(%r12)
	movq	8(%r12), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_18
# BB#17:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r12), %eax
.LBB12_18:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit
	incq	64(%r12)
	movw	4(%r14), %bx
	movq	8(%r12), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %eax
	movb	%bl, (%rcx,%rax)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_20
# BB#19:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r12), %eax
.LBB12_20:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i37
	incq	64(%r12)
	movq	8(%r12), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_22
# BB#21:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB12_22:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit38
	incq	64(%r12)
	movl	8(%r14), %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	12(%r14), %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	testb	%r13b, %r13b
	movl	$-1, %ebp
	movl	16(%r14), %esi
	cmovnel	%ebp, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	testb	%r13b, %r13b
	cmovel	24(%r14), %ebp
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	40(%r14), %ebx
	movq	8(%r12), %rax
	movl	16(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r12)
	movb	%bl, (%rax,%rcx)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_24
# BB#23:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r12), %eax
.LBB12_24:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i40
	incq	64(%r12)
	movq	8(%r12), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_26
# BB#25:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB12_26:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit41
	incq	64(%r12)
	xorl	%edx, %edx
	testb	%r13b, %r13b
	movl	$20, %eax
	cmoveq	%rdx, %rax
	movslq	60(%r14), %rcx
	testq	%rcx, %rcx
	jle	.LBB12_32
# BB#27:                                # %.lr.ph.i
	movq	64(%r14), %rsi
	leaq	-1(%rcx), %r8
	movq	%rcx, %rbx
	xorl	%edi, %edi
	xorl	%edx, %edx
	andq	$3, %rbx
	je	.LBB12_29
	.p2align	4, 0x90
.LBB12_28:                              # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rdi,8), %rbp
	movq	16(%rbp), %rbp
	leaq	4(%rdx,%rbp), %rdx
	incq	%rdi
	cmpq	%rdi, %rbx
	jne	.LBB12_28
.LBB12_29:                              # %.prol.loopexit71
	cmpq	$3, %r8
	jb	.LBB12_32
# BB#30:                                # %.lr.ph.i.new
	subq	%rdi, %rcx
	leaq	24(%rsi,%rdi,8), %rsi
	.p2align	4, 0x90
.LBB12_31:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rsi), %rdi
	movq	-16(%rsi), %rbp
	addq	16(%rdi), %rdx
	addq	16(%rbp), %rdx
	movq	-8(%rsi), %rdi
	addq	16(%rdi), %rdx
	movq	(%rsi), %rdi
	movq	16(%rdi), %rdi
	leaq	16(%rdi,%rdx), %rdx
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB12_31
.LBB12_32:                              # %_ZNK8NArchive4NZip11CExtraBlock7GetSizeEv.exit
	addl	%eax, %edx
	movzwl	%dx, %eax
	movl	76(%r12), %ebx
	cmpl	%ebx, %eax
	ja	.LBB12_33
# BB#34:
	movq	8(%r12), %rax
	movl	16(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r12)
	movb	%bl, (%rax,%rcx)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_36
# BB#35:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r12), %eax
.LBB12_36:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i43
	movb	%r13b, 7(%rsp)          # 1-byte Spill
	incq	64(%r12)
	movq	8(%r12), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_38
# BB#37:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB12_38:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit44
	movq	64(%r12), %rax
	incq	%rax
	movq	%rax, 64(%r12)
	movl	40(%r14), %r13d
	testq	%r13, %r13
	je	.LBB12_44
# BB#39:                                # %.lr.ph.i.i.preheader
	movq	32(%r14), %rbp
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB12_40:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %eax
	movq	8(%r12), %rcx
	movl	16(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 16(%r12)
	movb	%al, (%rcx,%rdx)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_42
# BB#41:                                #   in Loop: Header=BB12_40 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB12_42:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB12_40 Depth=1
	incq	%rbp
	decq	%rbx
	jne	.LBB12_40
# BB#43:                                # %_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj.exit.loopexit
	movq	64(%r12), %rax
.LBB12_44:                              # %_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj.exit
	leaq	48(%r14), %rbx
	addq	%r13, %rax
	movq	%rax, 64(%r12)
	xorl	%ebp, %ebp
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movl	$0, %r13d
	je	.LBB12_54
# BB#45:
	movq	8(%r12), %rax
	movl	16(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r12)
	movb	$1, (%rax,%rcx)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_47
# BB#46:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r12), %eax
.LBB12_47:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i46
	incq	64(%r12)
	movq	8(%r12), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_49
# BB#48:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r12), %eax
.LBB12_49:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit47
	incq	64(%r12)
	movq	8(%r12), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %eax
	movb	$16, (%rcx,%rax)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_51
# BB#50:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r12), %eax
.LBB12_51:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i49
	incq	64(%r12)
	movq	8(%r12), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_53
# BB#52:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB12_53:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit50
	incq	64(%r12)
	movq	24(%r14), %rsi
	movq	%r12, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	movq	16(%r14), %rsi
	movq	%r12, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	movl	$20, %r13d
.LBB12_54:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive4NZip11COutArchive10WriteExtraERKNS0_11CExtraBlockE
	movslq	60(%r14), %rax
	testq	%rax, %rax
	jle	.LBB12_60
# BB#55:                                # %.lr.ph.i51
	movq	64(%r14), %rcx
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	andq	$3, %rdi
	je	.LBB12_57
	.p2align	4, 0x90
.LBB12_56:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rbx
	movq	16(%rbx), %rbx
	leaq	4(%rbp,%rbx), %rbp
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB12_56
.LBB12_57:                              # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB12_60
# BB#58:                                # %.lr.ph.i51.new
	subq	%rdx, %rax
	leaq	24(%rcx,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB12_59:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rcx), %rdx
	movq	-16(%rcx), %rsi
	addq	16(%rdx), %rbp
	addq	16(%rsi), %rbp
	movq	-8(%rcx), %rdx
	addq	16(%rdx), %rbp
	movq	(%rcx), %rdx
	movq	16(%rdx), %rdx
	leaq	16(%rdx,%rbp), %rbp
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB12_59
.LBB12_60:                              # %_ZNK8NArchive4NZip11CExtraBlock7GetSizeEv.exit56
	addl	%r13d, %ebp
	cmpl	76(%r12), %ebp
	jb	.LBB12_62
	jmp	.LBB12_65
	.p2align	4, 0x90
.LBB12_64:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit57
                                        #   in Loop: Header=BB12_62 Depth=1
	incq	64(%r12)
	incl	%ebp
	cmpl	76(%r12), %ebp
	jae	.LBB12_65
.LBB12_62:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rax
	movl	16(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r12)
	movb	$0, (%rax,%rcx)
	movl	16(%r12), %eax
	cmpl	20(%r12), %eax
	jne	.LBB12_64
# BB#63:                                #   in Loop: Header=BB12_62 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	jmp	.LBB12_64
.LBB12_65:                              # %._crit_edge
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movq	64(%r12), %rsi
	addq	16(%r14), %rsi
	movq	%rsi, 64(%r12)
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jne	.LBB12_66
# BB#67:                                # %_ZN8NArchive4NZip11COutArchive6SeekToEy.exit58
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_1:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%ebp, (%rax)
	jmp	.LBB12_2
.LBB12_33:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$-2147467259, (%rax)    # imm = 0x80004005
	jmp	.LBB12_2
.LBB12_66:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%ebx, (%rax)
.LBB12_2:
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end12:
	.size	_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE, .Lfunc_end12-_ZN8NArchive4NZip11COutArchive16WriteLocalHeaderERKNS0_10CLocalItemE
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive18WriteCentralHeaderERKNS0_5CItemE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive18WriteCentralHeaderERKNS0_5CItemE,@function
_ZN8NArchive4NZip11COutArchive18WriteCentralHeaderERKNS0_5CItemE: # @_ZN8NArchive4NZip11COutArchive18WriteCentralHeaderERKNS0_5CItemE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 112
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r13
	movl	$4294967294, %edx       # imm = 0xFFFFFFFE
	movq	16(%r15), %rax
	movq	24(%r15), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rcx
	seta	%bl
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rax
	seta	%r12b
	movq	88(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rax
	seta	%bpl
	movl	_ZN8NArchive4NZip10NSignature18kCentralFileHeaderE(%rip), %esi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movb	80(%r15), %al
	leaq	8(%r13), %r14
	movq	8(%r13), %rcx
	movl	16(%r13), %edx
	leal	1(%rdx), %esi
	movl	%esi, 16(%r13)
	movb	%al, (%rcx,%rdx)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_2
# BB#1:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_2:                               # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit
	orb	%bpl, %bl
	incq	64(%r13)
	movb	81(%r15), %cl
	movq	8(%r13), %rdx
	leal	1(%rax), %esi
	movl	%esi, 16(%r13)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_4
# BB#3:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_4:                               # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit63
	incq	64(%r13)
	orb	%bl, %r12b
	movb	(%r15), %dl
	cmpb	$45, %dl
	movb	$45, %cl
	jb	.LBB13_6
# BB#5:                                 # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit63
	movl	%edx, %ecx
.LBB13_6:                               # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit63
	movb	%r12b, 15(%rsp)         # 1-byte Spill
	testb	%r12b, %r12b
	jne	.LBB13_8
# BB#7:                                 # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit63
	movl	%edx, %ecx
.LBB13_8:                               # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit63
	movq	8(%r13), %rdx
	leal	1(%rax), %esi
	movl	%esi, 16(%r13)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	movl	$4294967294, %r12d      # imm = 0xFFFFFFFE
	jne	.LBB13_10
# BB#9:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_10:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit64
	incq	64(%r13)
	movb	1(%r15), %cl
	movq	8(%r13), %rdx
	leal	1(%rax), %esi
	movl	%esi, 16(%r13)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_12
# BB#11:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_12:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit65
	incq	64(%r13)
	movw	2(%r15), %bx
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bl, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_14
# BB#13:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_14:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_16
# BB#15:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_16:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit
	incq	64(%r13)
	movw	4(%r15), %bx
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bl, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_18
# BB#17:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_18:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i67
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_20
# BB#19:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB13_20:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit68
	incq	64(%r13)
	movl	8(%r15), %esi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	12(%r15), %esi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	cmpq	%r12, 24(%rsp)          # 8-byte Folded Reload
	movl	$-1, %ebp
	movl	$-1, %esi
	ja	.LBB13_22
# BB#21:
	movl	16(%r15), %esi
.LBB13_22:
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	cmpq	%r12, 16(%rsp)          # 8-byte Folded Reload
	ja	.LBB13_24
# BB#23:
	movl	24(%r15), %ebp
.LBB13_24:
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	40(%r15), %ebx
	movq	8(%r13), %rax
	movl	16(%r13), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r13)
	movb	%bl, (%rax,%rcx)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_26
# BB#25:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_26:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i70
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_28
# BB#27:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB13_28:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit71
	incq	64(%r13)
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	sbbl	%eax, %eax
	andl	$8, %eax
	cmpq	24(%rsp), %r12          # 8-byte Folded Reload
	sbbl	%ecx, %ecx
	andl	$8, %ecx
	addl	%eax, %ecx
	cmpq	32(%rsp), %r12          # 8-byte Folded Reload
	sbbl	%edx, %edx
	andl	$8, %edx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leal	4(%rdx,%rcx), %eax
	xorl	%ebx, %ebx
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	cmovew	%bx, %ax
	cmpb	$0, 178(%r15)
	movw	$36, %cx
	cmovew	%bx, %cx
	addl	%eax, %ecx
	movzwl	%cx, %r8d
	movslq	132(%r15), %rcx
	testq	%rcx, %rcx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	jle	.LBB13_34
# BB#29:                                # %.lr.ph.i
	movq	136(%r15), %rdx
	leaq	-1(%rcx), %rdi
	movq	%rcx, %rbp
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	andq	$3, %rbp
	je	.LBB13_31
	.p2align	4, 0x90
.LBB13_30:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rax
	movq	16(%rax), %rax
	leaq	4(%rbx,%rax), %rbx
	incq	%rsi
	cmpq	%rsi, %rbp
	jne	.LBB13_30
.LBB13_31:                              # %.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB13_34
# BB#32:                                # %.lr.ph.i.new
	subq	%rsi, %rcx
	leaq	24(%rdx,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB13_33:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rax
	movq	-16(%rdx), %rsi
	addq	16(%rax), %rbx
	addq	16(%rsi), %rbx
	movq	-8(%rdx), %rax
	addq	16(%rax), %rbx
	movq	(%rdx), %rax
	movq	16(%rax), %rax
	leaq	16(%rax,%rbx), %rbx
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB13_33
.LBB13_34:                              # %_ZNK8NArchive4NZip11CExtraBlock7GetSizeEv.exit
	addq	%r8, %rbx
	movq	8(%r13), %rax
	movl	16(%r13), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r13)
	movb	%bl, (%rax,%rcx)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_36
# BB#35:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_36:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i73
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_38
# BB#37:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_38:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit74
	incq	64(%r13)
	movq	160(%r15), %rbx
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bl, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_40
# BB#39:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_40:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i76
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_42
# BB#41:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_42:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit77
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_44
# BB#43:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_44:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i79
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_46
# BB#45:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_46:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit80
	incq	64(%r13)
	movw	82(%r15), %bx
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bl, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_48
# BB#47:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_48:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i82
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_50
# BB#49:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB13_50:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit83
	incq	64(%r13)
	movl	84(%r15), %esi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	cmpq	%r12, 32(%rsp)          # 8-byte Folded Reload
	movl	$-1, %esi
	ja	.LBB13_52
# BB#51:
	movl	88(%r15), %esi
.LBB13_52:
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	40(%r15), %ebp
	testq	%rbp, %rbp
	je	.LBB13_57
# BB#53:                                # %.lr.ph.i.i.preheader
	movq	32(%r15), %r12
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB13_54:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r12), %eax
	movq	8(%r13), %rcx
	movl	16(%r13), %edx
	leal	1(%rdx), %esi
	movl	%esi, 16(%r13)
	movb	%al, (%rcx,%rdx)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_56
# BB#55:                                #   in Loop: Header=BB13_54 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB13_56:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB13_54 Depth=1
	incq	%r12
	decq	%rbx
	jne	.LBB13_54
.LBB13_57:                              # %_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj.exit
	addq	%rbp, 64(%r13)
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	je	.LBB13_72
# BB#58:
	movq	48(%rsp), %rbp          # 8-byte Reload
	addl	40(%rsp), %ebp          # 4-byte Folded Reload
	movq	8(%r13), %rax
	movl	16(%r13), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r13)
	movb	$1, (%rax,%rcx)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_60
# BB#59:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_60:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i85
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	movl	$4294967294, %ebx       # imm = 0xFFFFFFFE
	jne	.LBB13_62
# BB#61:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_62:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit86
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	%bpl, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_64
# BB#63:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_64:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i88
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_66
# BB#65:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB13_66:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit89
	incq	64(%r13)
	cmpq	%rbx, 16(%rsp)          # 8-byte Folded Reload
	jbe	.LBB13_68
# BB#67:
	movq	24(%r15), %rsi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
.LBB13_68:
	cmpq	%rbx, 24(%rsp)          # 8-byte Folded Reload
	jbe	.LBB13_70
# BB#69:
	movq	16(%r15), %rsi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
.LBB13_70:
	cmpq	%rbx, 32(%rsp)          # 8-byte Folded Reload
	jbe	.LBB13_72
# BB#71:
	movq	88(%r15), %rsi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
.LBB13_72:
	leaq	120(%r15), %r12
	cmpb	$0, 178(%r15)
	je	.LBB13_90
# BB#73:
	movq	8(%r13), %rax
	movl	16(%r13), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r13)
	movb	$10, (%rax,%rcx)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_75
# BB#74:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_75:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i91
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_77
# BB#76:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_77:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit92
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	$32, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_79
# BB#78:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_79:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i94
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_81
# BB#80:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB13_81:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit95
	incq	64(%r13)
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movq	8(%r13), %rax
	movl	16(%r13), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r13)
	movb	$1, (%rax,%rcx)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_83
# BB#82:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_83:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i97
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_85
# BB#84:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_85:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit98
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	$24, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_87
# BB#86:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	16(%r13), %eax
.LBB13_87:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i100
	incq	64(%r13)
	movq	8(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_89
# BB#88:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB13_89:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit101
	incq	64(%r13)
	movl	96(%r15), %esi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	100(%r15), %esi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	104(%r15), %esi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	108(%r15), %esi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	112(%r15), %esi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	116(%r15), %esi
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
.LBB13_90:
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	_ZN8NArchive4NZip11COutArchive10WriteExtraERKNS0_11CExtraBlockE
	movq	160(%r15), %rax
	testq	%rax, %rax
	je	.LBB13_97
# BB#91:
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.LBB13_96
# BB#92:                                # %.lr.ph.i.i102.preheader
	movq	168(%r15), %rbp
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB13_93:                              # %.lr.ph.i.i102
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %eax
	movq	8(%r13), %rcx
	movl	16(%r13), %edx
	leal	1(%rdx), %esi
	movl	%esi, 16(%r13)
	movb	%al, (%rcx,%rdx)
	movl	16(%r13), %eax
	cmpl	20(%r13), %eax
	jne	.LBB13_95
# BB#94:                                #   in Loop: Header=BB13_93 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB13_95:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i105
                                        #   in Loop: Header=BB13_93 Depth=1
	incq	%rbp
	decq	%rbx
	jne	.LBB13_93
.LBB13_96:                              # %_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj.exit106
	addq	%r12, 64(%r13)
.LBB13_97:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN8NArchive4NZip11COutArchive18WriteCentralHeaderERKNS0_5CItemE, .Lfunc_end13-_ZN8NArchive4NZip11COutArchive18WriteCentralHeaderERKNS0_5CItemE
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive15WriteCentralDirERK13CObjectVectorINS0_5CItemEEPK7CBufferIhE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive15WriteCentralDirERK13CObjectVectorINS0_5CItemEEPK7CBufferIhE,@function
_ZN8NArchive4NZip11COutArchive15WriteCentralDirERK13CObjectVectorINS0_5CItemEEPK7CBufferIhE: # @_ZN8NArchive4NZip11COutArchive15WriteCentralDirERK13CObjectVectorINS0_5CItemEEPK7CBufferIhE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 96
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	(%r15), %rdi
	movq	64(%r15), %rsi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB14_55
# BB#1:                                 # %_ZN8NArchive4NZip11COutArchive6SeekToEy.exit
	movq	64(%r15), %r14
	movl	12(%r12), %r13d
	movl	$4294967294, %ebx       # imm = 0xFFFFFFFE
	testl	%r13d, %r13d
	movq	%r14, %rax
	jle	.LBB14_5
# BB#2:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_3:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	movq	(%rax,%rbp,8), %rsi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive18WriteCentralHeaderERKNS0_5CItemE
	incq	%rbp
	movslq	12(%r12), %r13
	cmpq	%r13, %rbp
	jl	.LBB14_3
# BB#4:                                 # %._crit_edge.loopexit
	movq	64(%r15), %rax
.LBB14_5:                               # %._crit_edge
	movq	%rax, %rbp
	subq	%r14, %rbp
	movq	%r14, 24(%rsp)          # 8-byte Spill
	cmpq	%rbx, %r14
	ja	.LBB14_9
# BB#6:                                 # %._crit_edge
	cmpl	$65534, %r13d           # imm = 0xFFFE
	jg	.LBB14_9
# BB#7:                                 # %._crit_edge
	cmpq	%rbx, %rbp
	ja	.LBB14_9
# BB#8:                                 # %._crit_edge._crit_edge
	leaq	8(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	16(%r15), %r14
	leaq	20(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB14_18
.LBB14_9:                               # %.critedge
	movq	%rax, %rbx
	movl	_ZN8NArchive4NZip10NSignature21kZip64EndOfCentralDirE(%rip), %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movl	$44, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	leaq	8(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	16(%r15), %r14
	movq	8(%r15), %rax
	movl	16(%r15), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r15)
	movb	$45, (%rax,%rcx)
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_11
# BB#10:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	(%r14), %eax
.LBB14_11:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i46
	incq	64(%r15)
	movq	8(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r15)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_13
# BB#12:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	(%r14), %eax
.LBB14_13:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit47
	incq	64(%r15)
	movq	8(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r15)
	movl	%eax, %eax
	movb	$45, (%rcx,%rax)
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_15
# BB#14:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	(%r14), %eax
.LBB14_15:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i49
	incq	64(%r15)
	movq	8(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r15)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_17
# BB#16:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_17:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit50
	leaq	20(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	incq	64(%r15)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movslq	12(%r12), %rsi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	movslq	12(%r12), %rsi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	movq	%r15, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	movl	_ZN8NArchive4NZip10NSignature28kZip64EndOfCentralDirLocatorE(%rip), %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt64Ey
	movl	$1, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
.LBB14_18:
	movl	_ZN8NArchive4NZip10NSignature16kEndOfCentralDirE(%rip), %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movq	8(%r15), %rax
	movl	16(%r15), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r15)
	movb	$0, (%rax,%rcx)
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_20
# BB#19:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	(%r14), %eax
.LBB14_20:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i52
	incq	64(%r15)
	movq	8(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r15)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_22
# BB#21:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	(%r14), %eax
.LBB14_22:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit53
	incq	64(%r15)
	movq	8(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r15)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_24
# BB#23:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	(%r14), %eax
.LBB14_24:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i55
	incq	64(%r15)
	movq	8(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r15)
	movl	%eax, %eax
	movb	$0, (%rcx,%rax)
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_26
# BB#25:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_26:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit56
	incq	64(%r15)
	cmpl	$65534, %r13d           # imm = 0xFFFE
	jle	.LBB14_27
# BB#32:                                # %.critedge44
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movl	(%r14), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%r14)
	movb	$-1, (%rax,%rcx)
	movl	(%r14), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	jne	.LBB14_34
# BB#33:
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	(%r14), %eax
.LBB14_34:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i61
	incq	64(%r15)
	movq	8(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r15)
	movl	%eax, %eax
	movb	$-1, (%rcx,%rax)
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	movq	32(%rsp), %r12          # 8-byte Reload
	jne	.LBB14_36
# BB#35:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_36:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit62
	incq	64(%r15)
	movw	$-1, %bx
	jmp	.LBB14_37
.LBB14_27:
	movl	12(%r12), %ebx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movl	(%r14), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%r14)
	movb	%bl, (%rax,%rcx)
	movl	(%r14), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	jne	.LBB14_29
# BB#28:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	(%r14), %eax
.LBB14_29:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i58
	incq	64(%r15)
	movq	8(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r15)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_31
# BB#30:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_31:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit59
	incq	64(%r15)
	movw	12(%r12), %bx
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB14_37:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movl	(%r14), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%r14)
	movb	%bl, (%rax,%rcx)
	movl	(%r14), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	jne	.LBB14_39
# BB#38:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	(%r14), %eax
.LBB14_39:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i64
	movq	%rbp, %r13
	incq	64(%r15)
	movq	8(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r15)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_41
# BB#40:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_41:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit65
	incq	64(%r15)
	movl	$4294967294, %ebp       # imm = 0xFFFFFFFE
	movq	%r13, %rsi
	cmpq	%rbp, %rsi
	movl	$-1, %r13d
	cmoval	%r13d, %esi
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	movq	24(%rsp), %rsi          # 8-byte Reload
	cmpq	%rbp, %rsi
	cmoval	%r13d, %esi
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN8NArchive4NZip11COutArchive11WriteUInt32Ej
	testq	%r12, %r12
	je	.LBB14_42
# BB#43:
	movl	8(%r12), %ebx
	jmp	.LBB14_44
.LBB14_42:
	xorl	%ebx, %ebx
.LBB14_44:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movl	(%r14), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%r14)
	movb	%bl, (%rax,%rcx)
	movl	(%r14), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	jne	.LBB14_46
# BB#45:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movl	(%r14), %eax
.LBB14_46:                              # %_ZN8NArchive4NZip11COutArchive9WriteByteEh.exit.i
	incq	64(%r15)
	movq	8(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 16(%r15)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	16(%r15), %eax
	cmpl	20(%r15), %eax
	jne	.LBB14_48
# BB#47:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_48:                              # %_ZN8NArchive4NZip11COutArchive11WriteUInt16Et.exit
	incq	64(%r15)
	testl	%ebx, %ebx
	je	.LBB14_54
# BB#49:                                # %.lr.ph.i.i
	movq	16(%r12), %rbp
	movl	%ebx, %r13d
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB14_50:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %eax
	movq	(%rbx), %rcx
	movl	(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, (%r14)
	movb	%al, (%rcx,%rdx)
	movl	(%r14), %eax
	cmpl	(%r12), %eax
	jne	.LBB14_52
# BB#51:                                #   in Loop: Header=BB14_50 Depth=1
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_52:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB14_50 Depth=1
	incq	%rbp
	decq	%r13
	jne	.LBB14_50
# BB#53:                                # %_ZN8NArchive4NZip11COutArchive10WriteBytesEPKvj.exit
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	%rax, 64(%r15)
.LBB14_54:
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN10COutBuffer14FlushWithCheckEv # TAILCALL
.LBB14_55:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%r14d, (%rax)
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end14:
	.size	_ZN8NArchive4NZip11COutArchive15WriteCentralDirERK13CObjectVectorINS0_5CItemEEPK7CBufferIhE, .Lfunc_end14-_ZN8NArchive4NZip11COutArchive15WriteCentralDirERK13CObjectVectorINS0_5CItemEEPK7CBufferIhE
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive26CreateStreamForCompressingEPP10IOutStream
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive26CreateStreamForCompressingEPP10IOutStream,@function
_ZN8NArchive4NZip11COutArchive26CreateStreamForCompressingEPP10IOutStream: # @_ZN8NArchive4NZip11COutArchive26CreateStreamForCompressingEPP10IOutStream
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 32
.Lcfi93:
	.cfi_offset %rbx, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$0, 8(%rbx)
	movq	$_ZTV16COffsetOutStream+16, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, %rdi
	callq	*_ZTV16COffsetOutStream+24(%rip)
	movq	(%r15), %rsi
	movl	72(%r15), %edx
	addq	64(%r15), %rdx
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN16COffsetOutStream4InitEP10IOutStreamy
.Ltmp1:
# BB#1:                                 # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit8
	movq	%rbx, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB15_2:
.Ltmp2:
	movq	%rax, %r14
	movq	(%rbx), %rax
.Ltmp3:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp4:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN8NArchive4NZip11COutArchive26CreateStreamForCompressingEPP10IOutStream, .Lfunc_end15-_ZN8NArchive4NZip11COutArchive26CreateStreamForCompressingEPP10IOutStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end15-.Ltmp4     #   Call between .Ltmp4 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end16:
	.size	__clang_call_terminate, .Lfunc_end16-__clang_call_terminate

	.text
	.globl	_ZN8NArchive4NZip11COutArchive24SeekToPackedDataPositionEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive24SeekToPackedDataPositionEv,@function
_ZN8NArchive4NZip11COutArchive24SeekToPackedDataPositionEv: # @_ZN8NArchive4NZip11COutArchive24SeekToPackedDataPositionEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 16
.Lcfi97:
	.cfi_offset %rbx, -16
	movl	72(%rdi), %esi
	addq	64(%rdi), %rsi
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jne	.LBB17_2
# BB#1:                                 # %_ZN8NArchive4NZip11COutArchive6SeekToEy.exit
	popq	%rbx
	retq
.LBB17_2:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%ebx, (%rax)
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end17:
	.size	_ZN8NArchive4NZip11COutArchive24SeekToPackedDataPositionEv, .Lfunc_end17-_ZN8NArchive4NZip11COutArchive24SeekToPackedDataPositionEv
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11COutArchive22CreateStreamForCopyingEPP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11COutArchive22CreateStreamForCopyingEPP20ISequentialOutStream,@function
_ZN8NArchive4NZip11COutArchive22CreateStreamForCopyingEPP20ISequentialOutStream: # @_ZN8NArchive4NZip11COutArchive22CreateStreamForCopyingEPP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 32
.Lcfi101:
	.cfi_offset %rbx, -24
.Lcfi102:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB18_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB18_2:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	%rbx, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	_ZN8NArchive4NZip11COutArchive22CreateStreamForCopyingEPP20ISequentialOutStream, .Lfunc_end18-_ZN8NArchive4NZip11COutArchive22CreateStreamForCopyingEPP20ISequentialOutStream
	.cfi_endproc

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
