	.text
	.file	"sim4.init.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	15                      # 0xf
	.long	3                       # 0x3
	.long	10                      # 0xa
	.long	6                       # 0x6
.LCPI0_1:
	.long	15                      # 0xf
	.long	10                      # 0xa
	.long	4294967291              # 0xfffffffb
	.long	2                       # 0x2
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$8424, %rsp             # imm = 0x20E8
.Lcfi6:
	.cfi_def_cfa_offset 8480
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r15d
	movq	(%r14), %rax
	movq	%rax, argv0(%rip)
	movl	$6, %edi
	movl	$.L.str, %esi
	callq	setlocale
	testq	%rax, %rax
	je	.LBB0_1
.LBB0_2:
	movl	$11, %edi
	movl	$bug_handler, %esi
	callq	signal
	movl	$7, %edi
	movl	$bug_handler, %esi
	callq	signal
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [15,3,10,6]
	movups	%xmm0, options+24(%rip)
	movq	$.L.str.2, options(%rip)
	movl	$4, options+16(%rip)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [15,10,4294967291,2]
	movups	%xmm0, options+40(%rip)
	movabsq	$51539607553, %rax      # imm = 0xC00000001
	movq	%rax, options+56(%rip)
	movl	$12, options+64(%rip)
	movl	$75, options+72(%rip)
	movl	$50, options+76(%rip)
	movabsq	$-3689348814741910323, %r12 # imm = 0xCCCCCCCCCCCCCCCD
	movl	$524357, %r13d          # imm = 0x80045
	jmp	.LBB0_3
.LBB0_31:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	testl	%eax, %eax
	js	.LBB0_117
# BB#32:                                #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, options+44(%rip)
	jmp	.LBB0_3
.LBB0_35:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+52(%rip)
	cmpl	$3, %eax
	jb	.LBB0_3
	jmp	.LBB0_36
.LBB0_38:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+60(%rip)
	decl	%eax
	cmpl	$15, %eax
	jb	.LBB0_3
	jmp	.LBB0_39
.LBB0_40:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+64(%rip)
	testl	%eax, %eax
	jg	.LBB0_3
	jmp	.LBB0_41
.LBB0_9:                                #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	testl	%eax, %eax
	js	.LBB0_114
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, options+76(%rip)
	jmp	.LBB0_3
.LBB0_13:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+72(%rip)
	cmpl	$101, %eax
	jb	.LBB0_3
	jmp	.LBB0_14
.LBB0_15:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+32(%rip)
	jmp	.LBB0_3
.LBB0_33:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+68(%rip)
	jmp	.LBB0_3
.LBB0_34:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+48(%rip)
	jmp	.LBB0_3
.LBB0_37:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+56(%rip)
	jmp	.LBB0_3
.LBB0_42:                               #   in Loop: Header=BB0_3 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	fprintf
	.p2align	4, 0x90
.LBB0_3:                                # %.critedge114
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_22 Depth 2
	movl	$.L.str.3, %edx
	movl	%r15d, %edi
	movq	%r14, %rsi
	callq	getopt
	movl	%eax, %ecx
	leal	1(%rcx), %eax
	cmpl	$115, %eax
	ja	.LBB0_42
# BB#4:                                 # %.critedge114
                                        #   in Loop: Header=BB0_3 Depth=1
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+20(%rip)
	cmpl	$5, %eax
	jb	.LBB0_3
	jmp	.LBB0_6
.LBB0_7:                                #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	testl	%eax, %eax
	js	.LBB0_113
# BB#8:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, options+24(%rip)
	jmp	.LBB0_3
.LBB0_11:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+28(%rip)
	addl	$-3, %eax
	cmpl	$8, %eax
	jb	.LBB0_3
	jmp	.LBB0_12
.LBB0_16:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, options+36(%rip)
	jmp	.LBB0_3
.LBB0_17:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	testl	%eax, %eax
	js	.LBB0_115
# BB#18:                                #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, options+40(%rip)
	jmp	.LBB0_3
.LBB0_19:                               #   in Loop: Header=BB0_3 Depth=1
	movq	optarg(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rcx
	movq	%rbx, options(%rip)
	movl	$1, options+16(%rip)
	mulq	%r12
	shrq	$2, %rdx
	leaq	(%rdx,%rdx,4), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	cmpq	$4, %rdx
	jne	.LBB0_116
# BB#20:                                # %.preheader126
                                        #   in Loop: Header=BB0_3 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_3
# BB#21:                                # %.lr.ph158.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %edi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph158
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rax
	mulq	%r12
	shrq	$2, %rdx
	leaq	(%rdx,%rdx,4), %rax
	movq	%rbp, %rdx
	subq	%rax, %rdx
	movsbl	(%rbx,%rbp), %esi
	cmpq	$4, %rdx
	jne	.LBB0_26
# BB#23:                                #   in Loop: Header=BB0_22 Depth=2
	cmpb	$44, %sil
	jne	.LBB0_24
# BB#29:                                #   in Loop: Header=BB0_22 Depth=2
	incl	%edi
	movl	%edi, options+16(%rip)
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_22 Depth=2
	movl	%esi, %eax
	addb	$-65, %al
	cmpb	$19, %al
	ja	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_22 Depth=2
	movzbl	%al, %eax
	btq	%rax, %r13
	jae	.LBB0_28
.LBB0_30:                               #   in Loop: Header=BB0_22 Depth=2
	incq	%rbp
	cmpq	%rcx, %rbp
	jb	.LBB0_22
	jmp	.LBB0_3
.LBB0_43:
	movslq	optind(%rip), %rax
	leal	2(%rax), %ecx
	cmpl	%r15d, %ecx
	jne	.LBB0_44
# BB#46:
	movq	(%r14,%rax,8), %rdi
	leaq	4264(%rsp), %rbx
	movq	%rbx, %rsi
	callq	init_seq
	movl	options+68(%rip), %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	get_next_seq
	testl	%eax, %eax
	jne	.LBB0_47
# BB#49:
	movq	4272(%rsp), %rsi
	movl	$dna_seq_head, %edi
	movl	$256, %edx              # imm = 0x100
	callq	strncpy
	movslq	optind(%rip), %rax
	movq	8(%r14,%rax,8), %rdi
	leaq	104(%rsp), %rbx
	movq	%rbx, %rsi
	callq	init_seq
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	get_next_seq
	testl	%eax, %eax
	jne	.LBB0_50
# BB#51:
	callq	init_encoding
	movl	options+60(%rip), %esi
	movq	4280(%rsp), %rdx
	movl	8412(%rsp), %ecx
	leaq	64(%rsp), %rbx
	movq	%rbx, %rdi
	callq	init_hash_env
	movq	%rsp, %rdi
	movl	$1, %ebp
	movl	$1, %esi
	callq	init_col
	leaq	16(%rsp), %rdi
	movl	$1, %esi
	callq	init_col
	movq	%rbx, %rdi
	callq	bld_table
	movl	options+16(%rip), %edi
	shlq	$3, %rdi
	callq	xmalloc
	movq	%rax, options+8(%rip)
	cmpl	$0, options+16(%rip)
	je	.LBB0_54
# BB#52:                                # %.preheader.preheader.i
	movl	$3, %ecx
	jmp	.LBB0_53
	.p2align	4, 0x90
.LBB0_118:                              # %.preheader..preheader_crit_edge.i
                                        #   in Loop: Header=BB0_53 Depth=1
	movq	options+8(%rip), %rax
	incl	%ebp
	addl	$5, %ecx
.LBB0_53:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rbp), %edx
	leal	-3(%rcx), %esi
	movq	options(%rip), %rdi
	movzbl	(%rdi,%rsi), %esi
	movb	%sil, (%rax,%rdx,8)
	movzbl	dna_complement(%rsi), %eax
	movq	options+8(%rip), %rsi
	movb	%al, 7(%rsi,%rdx,8)
	movq	options(%rip), %rax
	leal	-2(%rcx), %esi
	movzbl	(%rax,%rsi), %eax
	movq	options+8(%rip), %rsi
	movb	%al, 1(%rsi,%rdx,8)
	movzbl	dna_complement(%rax), %eax
	movq	options+8(%rip), %rsi
	movb	%al, 6(%rsi,%rdx,8)
	movq	options(%rip), %rax
	leal	-1(%rcx), %esi
	movzbl	(%rax,%rsi), %eax
	movq	options+8(%rip), %rsi
	movb	%al, 2(%rsi,%rdx,8)
	movzbl	dna_complement(%rax), %eax
	movq	options+8(%rip), %rsi
	movb	%al, 5(%rsi,%rdx,8)
	movq	options(%rip), %rax
	movl	%ecx, %esi
	movzbl	(%rax,%rsi), %eax
	movq	options+8(%rip), %rsi
	movb	%al, 3(%rsi,%rdx,8)
	movzbl	dna_complement(%rax), %eax
	movq	options+8(%rip), %rsi
	movb	%al, 4(%rsi,%rdx,8)
	cmpl	options+16(%rip), %ebp
	jb	.LBB0_118
.LBB0_54:                               # %init_splice_junctions.exit.preheader
	xorl	%r15d, %r15d
	leaq	64(%rsp), %r14
	leaq	104(%rsp), %r12
	leaq	4264(%rsp), %r13
	testl	%r15d, %r15d
	jne	.LBB0_56
	jmp	.LBB0_57
	.p2align	4, 0x90
.LBB0_91:                               # %seq_revcomp_inplace.exit122._crit_edge
                                        #   in Loop: Header=BB0_57 Depth=1
	incl	%r15d
	movl	$0, 8(%rsp)
	testl	%r15d, %r15d
	je	.LBB0_57
.LBB0_56:
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	get_next_seq
	testl	%eax, %eax
	jne	.LBB0_45
.LBB0_57:                               # %.critedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_64 Depth 2
                                        #     Child Loop BB0_70 Depth 2
                                        #     Child Loop BB0_120 Depth 2
                                        #     Child Loop BB0_74 Depth 2
                                        #     Child Loop BB0_76 Depth 2
                                        #     Child Loop BB0_108 Depth 2
                                        #     Child Loop BB0_103 Depth 2
                                        #     Child Loop BB0_82 Depth 2
                                        #     Child Loop BB0_87 Depth 2
                                        #     Child Loop BB0_90 Depth 2
	movq	112(%rsp), %rsi
	movl	$rna_seq_head, %edi
	movl	$256, %edx              # imm = 0x100
	callq	strncpy
	movl	options+52(%rip), %eax
	cmpl	$2, %eax
	je	.LBB0_61
# BB#58:                                # %.critedge
                                        #   in Loop: Header=BB0_57 Depth=1
	cmpl	$1, %eax
	je	.LBB0_62
# BB#59:                                # %.critedge
                                        #   in Loop: Header=BB0_57 Depth=1
	testl	%eax, %eax
	jne	.LBB0_119
# BB#60:                                #   in Loop: Header=BB0_57 Depth=1
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rsp, %rdx
	jmp	.LBB0_66
	.p2align	4, 0x90
.LBB0_61:                               #   in Loop: Header=BB0_57 Depth=1
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rsp, %rdx
	callq	SIM4
.LBB0_62:                               #   in Loop: Header=BB0_57 Depth=1
	movl	4252(%rsp), %ecx
	testq	%rcx, %rcx
	je	.LBB0_65
# BB#63:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_57 Depth=1
	movq	120(%rsp), %rax
	leaq	-1(%rax,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_64:                               # %.lr.ph.i
                                        #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	movzbl	dna_complement(%rdx), %edx
	movzbl	(%rax), %esi
	movzbl	dna_complement(%rsi), %ebx
	movb	%bl, (%rcx)
	movb	%dl, (%rax)
	incq	%rax
	cmpq	%rcx, %rax
	leaq	-1(%rcx), %rcx
	jb	.LBB0_64
.LBB0_65:                               # %seq_revcomp_inplace.exit
                                        #   in Loop: Header=BB0_57 Depth=1
	movq	%r14, %rdi
	movq	%r12, %rsi
	leaq	16(%rsp), %rdx
.LBB0_66:                               #   in Loop: Header=BB0_57 Depth=1
	callq	SIM4
	movl	options+72(%rip), %r9d
	testl	%r9d, %r9d
	movl	24(%rsp), %ebx
	je	.LBB0_80
# BB#67:                                # %.preheader125
                                        #   in Loop: Header=BB0_57 Depth=1
	testl	%ebx, %ebx
	je	.LBB0_68
# BB#69:                                # %.lr.ph
                                        #   in Loop: Header=BB0_57 Depth=1
	movq	16(%rsp), %rcx
	leaq	-1(%rbx), %r8
	movq	%rbx, %rax
	xorl	%edi, %edi
	xorl	%esi, %esi
	andq	$3, %rax
	je	.LBB0_71
	.p2align	4, 0x90
.LBB0_70:                               #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdi,8), %rbp
	movl	36(%rbp), %ebp
	cmpl	%esi, %ebp
	cmoval	%ebp, %esi
	incq	%rdi
	cmpq	%rdi, %rax
	jne	.LBB0_70
.LBB0_71:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_57 Depth=1
	cmpq	$3, %r8
	jb	.LBB0_72
	.p2align	4, 0x90
.LBB0_120:                              #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdi,8), %rax
	movl	36(%rax), %eax
	cmpl	%esi, %eax
	cmoval	%eax, %esi
	movq	8(%rcx,%rdi,8), %rax
	movl	36(%rax), %eax
	cmpl	%esi, %eax
	cmoval	%eax, %esi
	movq	16(%rcx,%rdi,8), %rax
	movl	36(%rax), %eax
	cmpl	%esi, %eax
	cmoval	%eax, %esi
	movq	24(%rcx,%rdi,8), %rax
	movl	36(%rax), %eax
	cmpl	%esi, %eax
	cmoval	%eax, %esi
	addq	$4, %rdi
	cmpq	%rbx, %rdi
	jb	.LBB0_120
	jmp	.LBB0_72
	.p2align	4, 0x90
.LBB0_68:                               #   in Loop: Header=BB0_57 Depth=1
	xorl	%esi, %esi
.LBB0_72:                               # %.preheader124
                                        #   in Loop: Header=BB0_57 Depth=1
	movl	8(%rsp), %ecx
	testq	%rcx, %rcx
	je	.LBB0_77
# BB#73:                                # %.lr.ph142
                                        #   in Loop: Header=BB0_57 Depth=1
	movq	(%rsp), %rdi
	leaq	-1(%rcx), %r8
	movq	%rcx, %rax
	xorl	%ebp, %ebp
	andq	$3, %rax
	je	.LBB0_75
	.p2align	4, 0x90
.LBB0_74:                               #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rbp,8), %rdx
	movl	36(%rdx), %edx
	cmpl	%esi, %edx
	cmoval	%edx, %esi
	incq	%rbp
	cmpq	%rbp, %rax
	jne	.LBB0_74
.LBB0_75:                               # %.prol.loopexit200
                                        #   in Loop: Header=BB0_57 Depth=1
	cmpq	$3, %r8
	jb	.LBB0_77
	.p2align	4, 0x90
.LBB0_76:                               #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rbp,8), %rax
	movl	36(%rax), %eax
	cmpl	%esi, %eax
	cmoval	%eax, %esi
	movq	8(%rdi,%rbp,8), %rax
	movl	36(%rax), %eax
	cmpl	%esi, %eax
	cmoval	%eax, %esi
	movq	16(%rdi,%rbp,8), %rax
	movl	36(%rax), %eax
	cmpl	%esi, %eax
	cmoval	%eax, %esi
	movq	24(%rdi,%rbp,8), %rax
	movl	36(%rax), %eax
	cmpl	%esi, %eax
	cmoval	%eax, %esi
	addq	$4, %rbp
	cmpq	%rcx, %rbp
	jb	.LBB0_76
.LBB0_77:                               # %._crit_edge
                                        #   in Loop: Header=BB0_57 Depth=1
	imull	%esi, %r9d
	imulq	$1374389535, %r9, %rdx  # imm = 0x51EB851F
	shrq	$37, %rdx
	testl	%ebx, %ebx
	je	.LBB0_96
# BB#78:                                # %.lr.ph147
                                        #   in Loop: Header=BB0_57 Depth=1
	movq	16(%rsp), %rsi
	testb	$1, %bl
	jne	.LBB0_92
# BB#79:                                #   in Loop: Header=BB0_57 Depth=1
	xorl	%edi, %edi
	cmpl	$1, %ebx
	jne	.LBB0_108
	jmp	.LBB0_96
	.p2align	4, 0x90
.LBB0_92:                               #   in Loop: Header=BB0_57 Depth=1
	movq	(%rsi), %rax
	cmpl	%edx, 36(%rax)
	jae	.LBB0_94
# BB#93:                                #   in Loop: Header=BB0_57 Depth=1
	movl	$0, 36(%rax)
.LBB0_94:                               # %.prol.loopexit205
                                        #   in Loop: Header=BB0_57 Depth=1
	movl	$1, %edi
	cmpl	$1, %ebx
	je	.LBB0_96
	.p2align	4, 0x90
.LBB0_108:                              #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rdi,8), %rax
	cmpl	%edx, 36(%rax)
	jae	.LBB0_110
# BB#109:                               #   in Loop: Header=BB0_108 Depth=2
	movl	$0, 36(%rax)
.LBB0_110:                              #   in Loop: Header=BB0_108 Depth=2
	movq	8(%rsi,%rdi,8), %rax
	cmpl	%edx, 36(%rax)
	jae	.LBB0_112
# BB#111:                               #   in Loop: Header=BB0_108 Depth=2
	movl	$0, 36(%rax)
.LBB0_112:                              #   in Loop: Header=BB0_108 Depth=2
	addq	$2, %rdi
	cmpq	%rbx, %rdi
	jb	.LBB0_108
.LBB0_96:                               # %.preheader123
                                        #   in Loop: Header=BB0_57 Depth=1
	testl	%ecx, %ecx
	je	.LBB0_80
# BB#97:                                # %.lr.ph149
                                        #   in Loop: Header=BB0_57 Depth=1
	movq	(%rsp), %rsi
	testb	$1, %cl
	jne	.LBB0_99
# BB#98:                                #   in Loop: Header=BB0_57 Depth=1
	xorl	%edi, %edi
	cmpl	$1, %ecx
	jne	.LBB0_103
	jmp	.LBB0_80
	.p2align	4, 0x90
.LBB0_99:                               #   in Loop: Header=BB0_57 Depth=1
	movq	(%rsi), %rax
	cmpl	%edx, 36(%rax)
	jae	.LBB0_101
# BB#100:                               #   in Loop: Header=BB0_57 Depth=1
	movl	$0, 36(%rax)
.LBB0_101:                              # %.prol.loopexit209
                                        #   in Loop: Header=BB0_57 Depth=1
	movl	$1, %edi
	cmpl	$1, %ecx
	je	.LBB0_80
	.p2align	4, 0x90
.LBB0_103:                              #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rdi,8), %rax
	cmpl	%edx, 36(%rax)
	jae	.LBB0_105
# BB#104:                               #   in Loop: Header=BB0_103 Depth=2
	movl	$0, 36(%rax)
.LBB0_105:                              #   in Loop: Header=BB0_103 Depth=2
	movq	8(%rsi,%rdi,8), %rax
	cmpl	%edx, 36(%rax)
	jae	.LBB0_107
# BB#106:                               #   in Loop: Header=BB0_103 Depth=2
	movl	$0, 36(%rax)
.LBB0_107:                              #   in Loop: Header=BB0_103 Depth=2
	addq	$2, %rdi
	cmpq	%rcx, %rdi
	jb	.LBB0_103
	.p2align	4, 0x90
.LBB0_80:                               # %.preheader
                                        #   in Loop: Header=BB0_57 Depth=1
	testl	%ebx, %ebx
	je	.LBB0_83
# BB#81:                                # %.lr.ph151.preheader
                                        #   in Loop: Header=BB0_57 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_82:                               # %.lr.ph151
                                        #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rdi
	movl	$1, %esi
	movq	%r13, %rdx
	movq	%r12, %rcx
	callq	print_res
	incl	%ebx
	cmpl	24(%rsp), %ebx
	jb	.LBB0_82
.LBB0_83:                               # %._crit_edge152
                                        #   in Loop: Header=BB0_57 Depth=1
	movl	$0, 24(%rsp)
	cmpl	$0, options+52(%rip)
	je	.LBB0_88
# BB#84:                                # %._crit_edge152
                                        #   in Loop: Header=BB0_57 Depth=1
	movl	options+20(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_88
# BB#85:                                #   in Loop: Header=BB0_57 Depth=1
	movl	4252(%rsp), %ecx
	testq	%rcx, %rcx
	je	.LBB0_88
# BB#86:                                # %.lr.ph.preheader.i118
                                        #   in Loop: Header=BB0_57 Depth=1
	movq	120(%rsp), %rax
	leaq	-1(%rax,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_87:                               # %.lr.ph.i121
                                        #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	movzbl	dna_complement(%rdx), %edx
	movzbl	(%rax), %esi
	movzbl	dna_complement(%rsi), %ebx
	movb	%bl, (%rcx)
	movb	%dl, (%rax)
	incq	%rax
	cmpq	%rcx, %rax
	leaq	-1(%rcx), %rcx
	jb	.LBB0_87
.LBB0_88:                               # %seq_revcomp_inplace.exit122.preheader
                                        #   in Loop: Header=BB0_57 Depth=1
	cmpl	$0, 8(%rsp)
	je	.LBB0_91
# BB#89:                                # %seq_revcomp_inplace.exit122.preheader191
                                        #   in Loop: Header=BB0_57 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_90:                               # %seq_revcomp_inplace.exit122
                                        #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsp), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rdi
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r12, %rcx
	callq	print_res
	incl	%ebx
	cmpl	8(%rsp), %ebx
	jb	.LBB0_90
	jmp	.LBB0_91
.LBB0_1:
	movq	stderr(%rip), %rdi
	movq	(%r14), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB0_2
.LBB0_44:
	movq	stderr(%rip), %rdi
	movq	(%r14), %rdx
	movl	options+20(%rip), %ecx
	movl	options+24(%rip), %r8d
	movl	options+76(%rip), %r9d
	movl	options+28(%rip), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	options+72(%rip), %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	options+32(%rip), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	options+36(%rip), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	options+40(%rip), %r12d
	movl	options+44(%rip), %r13d
	movl	options+68(%rip), %ebp
	movl	options+48(%rip), %ebx
	movl	options+52(%rip), %r10d
	movl	options+56(%rip), %r11d
	movl	options+60(%rip), %r14d
	movl	options+64(%rip), %r15d
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	$Usage, %esi
	movl	$0, %eax
	pushq	%r15
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	options(%rip)
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)               # 8-byte Folded Reload
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	144(%rsp)               # 8-byte Folded Reload
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	160(%rsp)               # 8-byte Folded Reload
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$112, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -112
	movl	$1, %ebp
.LBB0_45:                               # %.loopexit
	movl	%ebp, %eax
	addq	$8424, %rsp             # imm = 0x20E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_119:
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_24:
	movl	$.L.str.11, %edi
	jmp	.LBB0_25
.LBB0_28:
	movl	$.L.str.12, %edi
.LBB0_25:
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fatal
.LBB0_117:
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_36:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_39:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_41:
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_114:
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_14:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_6:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_113:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_12:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_115:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_116:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	fatal
.LBB0_47:
	movslq	optind(%rip), %rax
	movq	(%r14,%rax,8), %rsi
	jmp	.LBB0_48
.LBB0_50:
	movslq	optind(%rip), %rax
	movq	8(%r14,%rax,8), %rsi
.LBB0_48:
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	fatal
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_43
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_3
	.quad	.LBB0_42
	.quad	.LBB0_5
	.quad	.LBB0_42
	.quad	.LBB0_7
	.quad	.LBB0_42
	.quad	.LBB0_11
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_16
	.quad	.LBB0_42
	.quad	.LBB0_17
	.quad	.LBB0_19
	.quad	.LBB0_31
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_35
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_38
	.quad	.LBB0_40
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_9
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_13
	.quad	.LBB0_15
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_42
	.quad	.LBB0_33
	.quad	.LBB0_42
	.quad	.LBB0_34
	.quad	.LBB0_37

	.text
	.p2align	4, 0x90
	.type	bug_handler,@function
bug_handler:                            # @bug_handler
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.38, %esi
	movl	$dna_seq_head, %ecx
	movl	$rna_seq_head, %r8d
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	callq	abort
.Lfunc_end1:
	.size	bug_handler, .Lfunc_end1-bug_handler
	.cfi_endproc

	.p2align	4, 0x90
	.type	init_seq,@function
init_seq:                               # @init_seq
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%r14, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movl	$4096, %edi             # imm = 0x1000
	callq	xmalloc
	movq	%rax, 24(%rbx)
	movq	$4096, 32(%rbx)         # imm = 0x1000
	movl	$0, 40(%rbx)
	testq	%r14, %r14
	je	.LBB2_3
# BB#1:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	open
	movl	%eax, %esi
	movl	%esi, 4144(%rbx)
	cmpl	$-1, %esi
	jne	.LBB2_4
# BB#2:
	callq	__errno_location
	movq	%rax, %rbx
	movl	(%rbx), %edi
	callq	strerror
	movq	%rax, %rdx
	movl	(%rbx), %ecx
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	fatal
.LBB2_3:
	movl	$0, 4144(%rbx)
	xorl	%esi, %esi
.LBB2_4:
	movq	$0, 4148(%rbx)
	movl	$0, 4156(%rbx)
	leaq	24(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	read_line_buf           # TAILCALL
.Lfunc_end2:
	.size	init_seq, .Lfunc_end2-init_seq
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_next_seq,@function
get_next_seq:                           # @get_next_seq
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 80
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r13
	movl	36(%r13), %r14d
	movl	$-1, %r12d
	testl	%r14d, %r14d
	je	.LBB3_4
# BB#1:                                 # %.lr.ph102
	movl	%edx, 4(%rsp)           # 4-byte Spill
	leaq	24(%r13), %rbx
	movq	24(%r13), %rbp
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	cmpb	$62, (%rbp)
	je	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	4144(%r13), %esi
	movq	%rbx, %rdi
	callq	read_line_buf
	movq	%rax, %rbp
	movl	36(%r13), %r14d
	testl	%r14d, %r14d
	jne	.LBB3_2
	jmp	.LBB3_4
.LBB3_5:                                # %.critedge87
	leal	25(%r14), %eax
	cmpl	4152(%r13), %eax
	jbe	.LBB3_6
# BB#7:
	movl	%eax, 4152(%r13)
	leaq	8(%r13), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	8(%r13), %rdi
	movl	%eax, %esi
	callq	xrealloc
	movq	%rax, %rdi
	movq	%rdi, 8(%r13)
	movl	36(%r13), %r14d
	jmp	.LBB3_8
.LBB3_6:                                # %._crit_edge
	leaq	8(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%r13), %rdi
.LBB3_8:
	movl	%r15d, (%rsp)           # 4-byte Spill
	leal	1(%r14), %edx
	movq	%rbp, %rsi
	callq	memcpy
	movl	$0, 4148(%r13)
	movl	4144(%r13), %esi
	movq	%rbx, %rdi
	callq	read_line_buf
	movq	%rax, %rbp
	movl	36(%r13), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB3_12
# BB#9:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_23 Depth 2
	movb	(%rbp), %r15b
	cmpb	$62, %r15b
	je	.LBB3_11
# BB#19:                                #   in Loop: Header=BB3_10 Depth=1
	movl	4148(%r13), %ecx
	movl	4156(%r13), %esi
	leal	1(%rax,%rcx), %eax
	cmpl	%esi, %eax
	jbe	.LBB3_21
# BB#20:                                #   in Loop: Header=BB3_10 Depth=1
	addl	$262144, %esi           # imm = 0x40000
	cmpl	%esi, %eax
	cmovael	%eax, %esi
	movl	%esi, 4156(%r13)
	movq	16(%r13), %rdi
	callq	xrealloc
	movq	%rax, 16(%r13)
	movb	(%rbp), %r15b
.LBB3_21:                               # %.preheader88
                                        #   in Loop: Header=BB3_10 Depth=1
	testb	%r15b, %r15b
	je	.LBB3_17
# BB#22:                                # %.lr.ph98
                                        #   in Loop: Header=BB3_10 Depth=1
	callq	__ctype_b_loc
	movq	%rax, %rbx
	incq	%rbp
	.p2align	4, 0x90
.LBB3_23:                               #   Parent Loop BB3_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movzbl	%r15b, %r12d
	movzwl	(%rax,%r12,2), %eax
	testb	$1, %ah
	jne	.LBB3_26
# BB#24:                                #   in Loop: Header=BB3_23 Depth=2
	testb	$2, %ah
	je	.LBB3_27
# BB#25:                                #   in Loop: Header=BB3_23 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movzbl	(%rax,%r12,4), %r15d
.LBB3_26:                               # %.backedge.sink.split
                                        #   in Loop: Header=BB3_23 Depth=2
	movq	16(%r13), %rax
	movl	4148(%r13), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 4148(%r13)
	movb	%r15b, (%rax,%rcx)
.LBB3_27:                               # %.backedge
                                        #   in Loop: Header=BB3_23 Depth=2
	movzbl	(%rbp), %r15d
	incq	%rbp
	testb	%r15b, %r15b
	jne	.LBB3_23
.LBB3_17:                               # %.loopexit89
                                        #   in Loop: Header=BB3_10 Depth=1
	movl	4144(%r13), %esi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	read_line_buf
	movq	%rax, %rbp
	movl	36(%r13), %eax
	testl	%eax, %eax
	jne	.LBB3_10
# BB#18:
	xorl	%ecx, %ecx
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jne	.LBB3_13
	jmp	.LBB3_15
.LBB3_11:
	movl	%eax, %ecx
.LBB3_12:                               # %.critedge
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB3_15
.LBB3_13:                               # %.critedge
	testl	%ecx, %ecx
	jne	.LBB3_14
.LBB3_15:
	movq	16(%r13), %rax
	movl	4148(%r13), %ecx
	movb	$0, (%rax,%rcx)
	movq	8(%r13), %r15
	movl	$.L.str.35, %esi
	movq	%r15, %rdi
	callq	strstr
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_16
# BB#28:
	movq	%rbp, %rbx
	addq	$6, %rbx
	addl	$-6, %r14d
	callq	__ctype_b_loc
	movq	(%rax), %rdx
	movsbq	6(%rbp), %rcx
	testb	$8, 1(%rdx,%rcx,2)
	je	.LBB3_30
	.p2align	4, 0x90
.LBB3_38:                               # %.lr.ph96
                                        # =>This Inner Loop Header: Depth=1
	decl	%r14d
	movsbq	1(%rbx), %rcx
	incq	%rbx
	testb	$8, 1(%rdx,%rcx,2)
	jne	.LBB3_38
.LBB3_30:                               # %.preheader
	testb	%cl, %cl
	je	.LBB3_34
# BB#31:                                # %.lr.ph.preheader
	incq	%rbx
	.p2align	4, 0x90
.LBB3_32:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rbp)
	incq	%rbp
	movb	(%rbx), %cl
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB3_32
# BB#33:                                # %.loopexit.loopexit
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %r15
	jmp	.LBB3_34
.LBB3_16:                               # %..loopexit_crit_edge
	callq	__ctype_b_loc
.LBB3_34:                               # %.loopexit
	movl	%r14d, %ecx
	movq	(%rax), %rax
	leaq	1(%r15,%rcx), %rdi
	.p2align	4, 0x90
.LBB3_35:                               # =>This Inner Loop Header: Depth=1
	movsbq	-2(%rdi), %rcx
	movzwl	(%rax,%rcx,2), %ecx
	decq	%rdi
	testw	$8194, %cx              # imm = 0x2002
	jne	.LBB3_35
# BB#36:
	movl	(%rsp), %ecx            # 4-byte Reload
	addl	4148(%r13), %ecx
	xorl	%r12d, %r12d
	movl	$24, %esi
	movl	$.L.str.36, %edx
	xorl	%eax, %eax
	callq	snprintf
	cmpl	$24, %eax
	jae	.LBB3_37
.LBB3_4:                                # %.critedge87.thread
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_14:
	movq	stderr(%rip), %rcx
	movl	$.L.str.34, %edi
	movl	$238, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB3_15
.LBB3_37:
	movl	4148(%r13), %esi
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	fatal
.Lfunc_end3:
	.size	get_next_seq, .Lfunc_end3-get_next_seq
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	65                      # 0x41
	.long	65                      # 0x41
	.long	65                      # 0x41
	.long	65                      # 0x41
.LCPI4_1:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI4_2:
	.long	84                      # 0x54
	.long	84                      # 0x54
	.long	84                      # 0x54
	.long	84                      # 0x54
	.text
	.p2align	4, 0x90
	.type	print_res,@function
print_res:                              # @print_res
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 144
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	36(%rbx), %eax
	cmpl	options+76(%rip), %eax
	jb	.LBB4_94
# BB#1:
	movq	8(%r15), %rsi
	movq	8(%r14), %rdx
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
	testl	%ebp, %ebp
	je	.LBB4_3
# BB#2:
	movl	$.Lstr, %edi
	callq	puts
.LBB4_3:
	movl	options+20(%rip), %eax
	cmpq	$4, %rax
	ja	.LBB4_91
# BB#4:
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_92:
	leaq	8(%rbx), %rdi
	movl	32(%rbx), %esi
	callq	print_exons
	jmp	.LBB4_93
.LBB4_5:
	leaq	8(%rbx), %rdi
	movl	32(%rbx), %esi
	callq	print_exons
.LBB4_6:
	movq	16(%r15), %rdi
	movq	16(%r14), %rsi
	jmp	.LBB4_7
.LBB4_8:
	leaq	8(%rbx), %rbp
	movl	32(%rbx), %esi
	movq	%rbp, %rdi
	callq	print_exons
	cmpl	$0, 40(%rbx)
	je	.LBB4_20
# BB#9:
	movq	8(%rbx), %rax
	movl	16(%rbx), %ecx
	decl	%ecx
	movq	(%rax,%rcx,8), %r12
	movl	12(%r12), %r11d
	movl	4148(%r14), %eax
	xorl	%esi, %esi
	cmpl	%eax, %r11d
	movl	$0, %edx
	jae	.LBB4_10
# BB#23:
	movq	16(%r14), %rcx
	xorl	%edx, %edx
	cmpb	$65, (%rcx,%r11)
	sete	%dl
	leaq	1(%r11), %rdi
	movl	$1, %esi
	cmpq	%rax, %rdi
	jae	.LBB4_10
# BB#24:
	xorl	%esi, %esi
	cmpb	$65, 1(%rcx,%r11)
	sete	%sil
	addl	%esi, %edx
	leaq	2(%r11), %rdi
	movl	$2, %esi
	cmpq	%rax, %rdi
	jae	.LBB4_10
# BB#25:
	xorl	%esi, %esi
	cmpb	$65, 2(%rcx,%r11)
	sete	%sil
	addl	%esi, %edx
	leaq	3(%r11), %rdi
	movl	$3, %esi
	cmpq	%rax, %rdi
	jae	.LBB4_10
# BB#26:
	xorl	%esi, %esi
	cmpb	$65, 3(%rcx,%r11)
	sete	%sil
	addl	%esi, %edx
	leaq	4(%r11), %rdi
	movl	$4, %esi
	cmpq	%rax, %rdi
	jae	.LBB4_10
# BB#27:
	xorl	%esi, %esi
	cmpb	$65, 4(%rcx,%r11)
	sete	%sil
	addl	%esi, %edx
	leaq	5(%r11), %rdi
	movl	$5, %esi
	cmpq	%rax, %rdi
	jae	.LBB4_10
# BB#28:
	xorl	%esi, %esi
	cmpb	$65, 5(%rcx,%r11)
	sete	%sil
	addl	%esi, %edx
	leaq	6(%r11), %rdi
	movl	$6, %esi
	cmpq	%rax, %rdi
	jae	.LBB4_10
# BB#29:
	xorl	%esi, %esi
	cmpb	$65, 6(%rcx,%r11)
	sete	%sil
	addl	%esi, %edx
	leaq	7(%r11), %rdi
	movl	$7, %esi
	cmpq	%rax, %rdi
	jae	.LBB4_10
# BB#30:
	xorl	%esi, %esi
	cmpb	$65, 7(%rcx,%r11)
	sete	%sil
	addl	%esi, %edx
	leaq	8(%r11), %rdi
	movl	$8, %esi
	cmpq	%rax, %rdi
	jae	.LBB4_10
# BB#31:
	xorl	%esi, %esi
	cmpb	$65, 8(%rcx,%r11)
	sete	%sil
	addl	%esi, %edx
	leaq	9(%r11), %rdi
	movl	$9, %esi
	cmpq	%rax, %rdi
	jae	.LBB4_10
# BB#32:
	xorl	%esi, %esi
	cmpb	$65, (%rcx,%rdi)
	sete	%sil
	addl	%esi, %edx
	movl	$10, %esi
.LBB4_10:                               # %.critedge.preheader.i
	leal	(%r11,%rsi), %edi
	cmpl	%eax, %edi
	jae	.LBB4_13
# BB#11:                                # %.lr.ph181.i
	movq	16(%r14), %rcx
	movl	%edi, %edi
	.p2align	4, 0x90
.LBB4_12:                               # =>This Inner Loop Header: Depth=1
	cmpb	$65, (%rcx,%rdi)
	jne	.LBB4_13
# BB#50:                                # %.critedge.i
                                        #   in Loop: Header=BB4_12 Depth=1
	incl	%esi
	incl	%edx
	incq	%rdi
	cmpl	%eax, %edi
	jb	.LBB4_12
.LBB4_13:                               # %.critedge1.preheader.i
	testl	%esi, %esi
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	je	.LBB4_15
# BB#14:                                # %.critedge1.preheader.i
	movl	4148(%r15), %r8d
	testl	%r8d, %r8d
	je	.LBB4_15
# BB#33:                                # %.lr.ph176.i
	movq	%r11, 8(%rsp)           # 8-byte Spill
	movq	16(%r15), %r9
	movl	8(%r12), %r13d
	movl	%esi, %r10d
	movq	%r8, %rdi
	negq	%rdi
	movq	%r10, %rcx
	negq	%rcx
	cmpq	%rcx, %rdi
	movq	%rcx, %r11
	cmovaq	%rdi, %r11
	negq	%r11
	cmpq	$7, %r11
	movq	%r12, 16(%rsp)          # 8-byte Spill
	jbe	.LBB4_34
# BB#39:                                # %min.iters.checked
	movq	%r11, %rbp
	andq	$-8, %rbp
	je	.LBB4_34
# BB#40:                                # %vector.scevcheck
	cmpq	%rcx, %rdi
	cmovaq	%rdi, %rcx
	movl	%ecx, %edi
	notl	%edi
	xorl	%r12d, %r12d
	addl	%r13d, %edi
	jb	.LBB4_35
# BB#41:                                # %vector.scevcheck
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	cmpq	%rax, %rcx
	movl	$0, %ecx
	jb	.LBB4_36
# BB#42:                                # %vector.body.preheader
	leaq	-8(%rbp), %rcx
	movq	%rcx, %r12
	shrq	$3, %r12
	btl	$3, %ecx
	jb	.LBB4_43
# BB#44:                                # %vector.body.prol
	movd	(%r9,%r13), %xmm0       # xmm0 = mem[0],zero,zero,zero
	pxor	%xmm2, %xmm2
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movd	4(%r9,%r13), %xmm1      # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	movdqa	.LCPI4_0(%rip), %xmm2   # xmm2 = [65,65,65,65]
	pcmpeqd	%xmm2, %xmm0
	psrld	$31, %xmm0
	pcmpeqd	%xmm2, %xmm1
	psrld	$31, %xmm1
	movl	$8, %ecx
	testq	%r12, %r12
	jne	.LBB4_46
	jmp	.LBB4_48
.LBB4_15:                               # %.critedge1.preheader..critedge2_crit_edge.i
	movl	8(%r12), %r13d
	addq	$8, %r12
	leaq	16(%r15), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	16(%r15), %r9
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	jmp	.LBB4_16
.LBB4_34:
	xorl	%r12d, %r12d
.LBB4_35:                               # %.critedge1.i.preheader
	xorl	%ecx, %ecx
.LBB4_36:                               # %.critedge1.i.preheader
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB4_37:                               # %.critedge1.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %eax
	leal	(%r13,%rbp), %edi
	xorl	%ecx, %ecx
	cmpb	$65, (%r9,%rdi)
	sete	%cl
	addl	%eax, %ecx
	incq	%rbp
	cmpq	%r10, %rbp
	jae	.LBB4_49
# BB#38:                                # %.critedge1.i
                                        #   in Loop: Header=BB4_37 Depth=1
	cmpq	%r8, %rbp
	jb	.LBB4_37
.LBB4_49:                               # %.critedge2.loopexit.i
	leaq	16(%r15), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	16(%rsp), %r12          # 8-byte Reload
	addq	$8, %r12
	movq	8(%rsp), %r11           # 8-byte Reload
.LBB4_16:                               # %.critedge2.i
	movl	options+68(%rip), %eax
	leal	1(%r13,%rax), %r8d
	movq	%r11, %r10
	addq	16(%r14), %r10
	leal	1(%r11), %r11d
	movl	%r13d, %r13d
	addq	%r9, %r13
	movl	$.L.str.24, %edi
	movl	$0, %eax
	movl	%esi, %r9d
	pushq	%r8
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	%rsi
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$48, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset -48
	movl	(%r12), %esi
	cmpq	$50, %rsi
	movl	$50, %r13d
	cmovbl	%esi, %r13d
	movq	(%rsp), %rax            # 8-byte Reload
	addq	(%rax), %rsi
	subq	%r13, %rsi
	leaq	32(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%r13, %rdx
	callq	strncpy
	movb	$0, 32(%rsp,%r13)
	movl	$.L.str.25, %esi
	movq	%rbp, %rdi
	callq	strstr
	movq	%rax, %rcx
	testq	%rcx, %rcx
	jne	.LBB4_18
# BB#17:
	leaq	32(%rsp), %rdi
	movl	$.L.str.26, %esi
	callq	strstr
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB4_19
.LBB4_18:                               # %.thread.i
	leaq	32(%rsp), %rax
	movl	$1, %edx
	subl	%eax, %edx
	subl	%r13d, %edx
	addl	%edx, %ecx
	addl	(%r12), %ecx
	addl	options+68(%rip), %ecx
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
.LBB4_19:
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB4_20:
	cmpl	$0, 44(%rbx)
	je	.LBB4_21
# BB#51:
	movq	(%rbp), %rax
	movq	(%rax), %r12
	movl	4(%r12), %eax
	addq	$16, %r14
	movl	%eax, %r13d
	decl	%r13d
	je	.LBB4_52
# BB#59:
	movq	(%r14), %rcx
	leal	-2(%rax), %esi
	xorl	%edx, %edx
	cmpb	$84, (%rcx,%rsi)
	sete	%dl
	movl	$1, %esi
	cmpl	$1, %r13d
	je	.LBB4_53
# BB#60:
	leal	-3(%rax), %esi
	xorl	%edi, %edi
	cmpb	$84, (%rcx,%rsi)
	sete	%dil
	addl	%edi, %edx
	movl	$2, %esi
	cmpl	$3, %r13d
	jb	.LBB4_53
# BB#61:
	leal	-4(%rax), %esi
	xorl	%edi, %edi
	cmpb	$84, (%rcx,%rsi)
	sete	%dil
	addl	%edi, %edx
	movl	$3, %esi
	cmpl	$3, %r13d
	je	.LBB4_53
# BB#62:
	leal	-5(%rax), %esi
	xorl	%edi, %edi
	cmpb	$84, (%rcx,%rsi)
	sete	%dil
	addl	%edi, %edx
	movl	$4, %esi
	cmpl	$5, %r13d
	jb	.LBB4_53
# BB#63:
	leal	-6(%rax), %esi
	xorl	%edi, %edi
	cmpb	$84, (%rcx,%rsi)
	sete	%dil
	addl	%edi, %edx
	movl	$5, %esi
	cmpl	$5, %r13d
	je	.LBB4_53
# BB#64:
	leal	-7(%rax), %esi
	xorl	%edi, %edi
	cmpb	$84, (%rcx,%rsi)
	sete	%dil
	addl	%edi, %edx
	movl	$6, %esi
	cmpl	$7, %r13d
	jb	.LBB4_53
# BB#65:
	leal	-8(%rax), %esi
	xorl	%edi, %edi
	cmpb	$84, (%rcx,%rsi)
	sete	%dil
	addl	%edi, %edx
	movl	$7, %esi
	cmpl	$7, %r13d
	je	.LBB4_53
# BB#66:
	leal	-9(%rax), %esi
	xorl	%edi, %edi
	cmpb	$84, (%rcx,%rsi)
	sete	%dil
	addl	%edi, %edx
	movl	$8, %esi
	cmpl	$9, %r13d
	jb	.LBB4_53
# BB#67:
	leal	-10(%rax), %esi
	xorl	%edi, %edi
	cmpb	$84, (%rcx,%rsi)
	sete	%dil
	addl	%edi, %edx
	movl	$9, %esi
	cmpl	$9, %r13d
	je	.LBB4_53
# BB#68:
	leal	-11(%rax), %esi
	xorl	%edi, %edi
	cmpb	$84, (%rcx,%rsi)
	sete	%dil
	addl	%edi, %edx
	movl	$10, %esi
	cmpl	%r13d, %esi
	jb	.LBB4_54
	jmp	.LBB4_57
.LBB4_21:                               # %.print_polyA_info.exit_crit_edge
	addq	$16, %r15
	addq	$16, %r14
	jmp	.LBB4_22
.LBB4_52:
	xorl	%edx, %edx
	xorl	%esi, %esi
.LBB4_53:                               # %.critedge3.preheader.i
	cmpl	%r13d, %esi
	jae	.LBB4_57
.LBB4_54:                               # %.lr.ph164.i
	movq	(%r14), %rdi
	addl	$-2, %eax
	movl	%esi, %ecx
	movl	%r13d, %ebp
	subl	%esi, %eax
	.p2align	4, 0x90
.LBB4_55:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %esi
	cmpb	$84, (%rdi,%rsi)
	jne	.LBB4_56
# BB#90:                                # %.critedge3.i
                                        #   in Loop: Header=BB4_55 Depth=1
	incq	%rcx
	incl	%edx
	decl	%eax
	cmpq	%rbp, %rcx
	jb	.LBB4_55
.LBB4_56:                               # %..critedge4.preheader.i.loopexit_crit_edge
	movl	%ecx, %esi
.LBB4_57:                               # %.critedge4.preheader.i
	movl	(%r12), %r9d
	leal	-1(%r9), %edi
	movq	16(%r15), %r11
	addq	$16, %r15
	xorl	%ecx, %ecx
	testl	%esi, %esi
	je	.LBB4_58
# BB#69:                                # %.critedge4.preheader.i
	testl	%edi, %edi
	movl	$0, %ebp
	je	.LBB4_87
# BB#70:                                # %.critedge4.i.preheader
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	addl	$-2, %r9d
	movl	%edi, %r15d
	movl	%esi, %r13d
	movq	%r13, %rax
	negq	%rax
	movq	%r15, %r8
	negq	%r8
	cmpq	%r8, %rax
	movq	%r8, %rcx
	cmovaq	%rax, %rcx
	negq	%rcx
	cmpq	$7, %rcx
	movq	%rdi, (%rsp)            # 8-byte Spill
	jbe	.LBB4_71
# BB#75:                                # %min.iters.checked54
	movq	%rcx, %rbp
	andq	$-8, %rbp
	je	.LBB4_71
# BB#76:                                # %vector.scevcheck63
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r12, %rdi
	cmpq	%r8, %rax
	cmovaq	%rax, %r8
	movl	%r8d, %eax
	notl	%eax
	movl	%r9d, %r12d
	subl	%eax, %r12d
	xorl	%r10d, %r10d
	cmpl	%r9d, %r12d
	ja	.LBB4_77
# BB#78:                                # %vector.scevcheck63
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	cmpq	%rax, %r8
	movl	$0, %ecx
	movq	%rdi, %r12
	jb	.LBB4_72
# BB#79:                                # %vector.body49.preheader
	leaq	-8(%rbp), %rax
	movq	%rax, %rcx
	shrq	$3, %rcx
	btl	$3, %eax
	jb	.LBB4_80
# BB#81:                                # %vector.body49.prol
	movl	%r9d, %eax
	movd	-3(%r11,%rax), %xmm0    # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqa	.LCPI4_1(%rip), %xmm2   # xmm2 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	pand	%xmm2, %xmm0
	movd	-7(%r11,%rax), %xmm1    # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	pand	%xmm2, %xmm1
	movdqa	.LCPI4_2(%rip), %xmm2   # xmm2 = [84,84,84,84]
	pcmpeqd	%xmm2, %xmm0
	psrld	$31, %xmm0
	pcmpeqd	%xmm2, %xmm1
	psrld	$31, %xmm1
	movl	$8, %eax
	testq	%rcx, %rcx
	jne	.LBB4_83
	jmp	.LBB4_85
.LBB4_71:
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
.LBB4_72:                               # %.critedge4.i.preheader84
	subl	%r10d, %r9d
	movq	%r10, %rbp
	.p2align	4, 0x90
.LBB4_73:                               # %.critedge4.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %eax
	movl	%r9d, %edi
	xorl	%ecx, %ecx
	cmpb	$84, (%r11,%rdi)
	sete	%cl
	addl	%eax, %ecx
	incq	%rbp
	cmpq	%r13, %rbp
	jae	.LBB4_86
# BB#74:                                # %.critedge4.i
                                        #   in Loop: Header=BB4_73 Depth=1
	decl	%r9d
	cmpq	%rbp, %r15
	ja	.LBB4_73
.LBB4_86:                               # %.critedge5.i.loopexit
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB4_87
.LBB4_58:
	xorl	%ebp, %ebp
.LBB4_87:                               # %.critedge5.i
	movl	options+68(%rip), %r8d
	addl	%edi, %r8d
	movl	%r13d, %r10d
	subl	%esi, %r10d
	addq	(%r14), %r10
	subl	%ebp, %edi
	addq	%rdi, %r11
	movl	$.L.str.28, %edi
	movl	$0, %eax
	movl	%esi, %r9d
	pushq	%r8
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	%rsi
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$48, %rsp
.Lcfi74:
	.cfi_adjust_cfa_offset -48
	movq	(%r15), %rax
	movl	(%r12), %ecx
	leaq	-1(%rax,%rcx), %rsi
	leaq	32(%rsp), %rbp
	movl	$50, %edx
	movq	%rbp, %rdi
	callq	strncpy
	movb	$0, 82(%rsp)
	movl	$.L.str.29, %esi
	movq	%rbp, %rdi
	callq	strstr
	movq	%rax, %rcx
	testq	%rcx, %rcx
	jne	.LBB4_89
# BB#88:
	leaq	32(%rsp), %rdi
	movl	$.L.str.30, %esi
	callq	strstr
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB4_22
.LBB4_89:                               # %.thread149.i
	leaq	32(%rsp), %rax
	movl	$5, %edx
	subl	%eax, %edx
	addl	%edx, %ecx
	addl	(%r12), %ecx
	addl	options+68(%rip), %ecx
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
.LBB4_22:                               # %print_polyA_info.exit
	movq	(%r15), %rdi
	movq	(%r14), %rsi
.LBB4_7:
	movq	%rbx, %rdx
	callq	print_align_lat
.LBB4_93:
	movl	$10, %edi
	callq	putchar
.LBB4_94:                               # %.preheader
	cmpl	$0, 16(%rbx)
	movq	8(%rbx), %rdi
	je	.LBB4_97
# BB#95:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_96:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	movq	(%rdi,%rax,8), %rdi
	callq	free
	incl	%ebp
	movq	8(%rbx), %rdi
	cmpl	16(%rbx), %ebp
	jb	.LBB4_96
.LBB4_97:                               # %._crit_edge
	callq	free
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_99
# BB#98:
	callq	free_align
.LBB4_99:
	movq	%rbx, %rdi
	callq	free
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_43:
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	pxor	%xmm1, %xmm1
	testq	%r12, %r12
	je	.LBB4_48
.LBB4_46:                               # %vector.body.preheader.new
	pxor	%xmm2, %xmm2
	movdqa	.LCPI4_0(%rip), %xmm3   # xmm3 = [65,65,65,65]
.LBB4_47:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r13,%rcx), %rdi
	movl	%edi, %eax
	movd	(%r9,%rax), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3],xmm4[4],xmm2[4],xmm4[5],xmm2[5],xmm4[6],xmm2[6],xmm4[7],xmm2[7]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	movd	4(%r9,%rax), %xmm5      # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3],xmm5[4],xmm2[4],xmm5[5],xmm2[5],xmm5[6],xmm2[6],xmm5[7],xmm2[7]
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	pcmpeqd	%xmm3, %xmm4
	psrld	$31, %xmm4
	pcmpeqd	%xmm3, %xmm5
	psrld	$31, %xmm5
	paddd	%xmm0, %xmm4
	paddd	%xmm1, %xmm5
	addl	$8, %edi
	movd	(%r9,%rdi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movd	4(%r9,%rdi), %xmm1      # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	pcmpeqd	%xmm3, %xmm0
	psrld	$31, %xmm0
	pcmpeqd	%xmm3, %xmm1
	psrld	$31, %xmm1
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	addq	$16, %rcx
	cmpq	%rcx, %rbp
	jne	.LBB4_47
.LBB4_48:                               # %middle.block
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ecx
	cmpq	%r11, %rbp
	movq	%rbp, %r12
	jne	.LBB4_36
	jmp	.LBB4_49
.LBB4_77:
	xorl	%ecx, %ecx
	movq	%rdi, %r12
	jmp	.LBB4_72
.LBB4_80:
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	testq	%rcx, %rcx
	je	.LBB4_85
.LBB4_83:                               # %vector.body49.preheader.new
	movq	%rbp, %rcx
	subq	%rax, %rcx
	movl	%r9d, %r12d
	subl	%eax, %r12d
	addl	$8, %eax
	movl	%r9d, %r10d
	subl	%eax, %r10d
	movdqa	.LCPI4_1(%rip), %xmm2   # xmm2 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	movdqa	.LCPI4_2(%rip), %xmm3   # xmm3 = [84,84,84,84]
.LBB4_84:                               # %vector.body49
                                        # =>This Inner Loop Header: Depth=1
	movl	%r12d, %eax
	movd	-3(%r11,%rax), %xmm4    # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	pshufd	$27, %xmm4, %xmm4       # xmm4 = xmm4[3,2,1,0]
	pand	%xmm2, %xmm4
	movd	-7(%r11,%rax), %xmm5    # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3],xmm5[4],xmm0[4],xmm5[5],xmm0[5],xmm5[6],xmm0[6],xmm5[7],xmm0[7]
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	pshufd	$27, %xmm5, %xmm5       # xmm5 = xmm5[3,2,1,0]
	pand	%xmm2, %xmm5
	pcmpeqd	%xmm3, %xmm4
	psrld	$31, %xmm4
	pcmpeqd	%xmm3, %xmm5
	psrld	$31, %xmm5
	paddd	%xmm0, %xmm4
	paddd	%xmm1, %xmm5
	movl	%r10d, %eax
	movd	-3(%r11,%rax), %xmm0    # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	pand	%xmm2, %xmm0
	movd	-7(%r11,%rax), %xmm1    # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	pand	%xmm2, %xmm1
	pcmpeqd	%xmm3, %xmm0
	psrld	$31, %xmm0
	pcmpeqd	%xmm3, %xmm1
	psrld	$31, %xmm1
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	addl	$-16, %r12d
	addl	$-16, %r10d
	addq	$-16, %rcx
	jne	.LBB4_84
.LBB4_85:                               # %middle.block50
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ecx
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	%rbp, %r10
	movq	%rdi, %r12
	jne	.LBB4_72
	jmp	.LBB4_86
.LBB4_91:
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	fatal
.Lfunc_end4:
	.size	print_res, .Lfunc_end4-print_res
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_92
	.quad	.LBB4_6
	.quad	.LBB4_91
	.quad	.LBB4_5
	.quad	.LBB4_8

	.text
	.p2align	4, 0x90
	.type	print_align_lat,@function
print_align_lat:                        # @print_align_lat
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi81:
	.cfi_def_cfa_offset 80
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	(%rdx), %rbx
	testq	%rbx, %rbx
	je	.LBB5_4
# BB#1:                                 # %.lr.ph
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	movl	28(%rbx), %eax
	leal	2(%rax,%rax), %edi
	shlq	$2, %rdi
	callq	xmalloc
	movq	%rax, %r13
	leaq	4(%r13), %r14
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	callq	S2A
	movq	8(%rbx), %rdi
	callq	Free_script
	movl	16(%rbx), %r9d
	movl	20(%rbx), %eax
	leaq	-2(%r12,%r9), %rdi
	leaq	-2(%rbp,%rax), %rsi
	movl	24(%rbx), %edx
	movl	28(%rbx), %ecx
	movq	%rbp, %r10
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	32(%rbp), %ebp
	subq	$8, %rsp
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	movq	%r14, %r8
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rbp
	movq	%r10, %rbp
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	callq	IDISPLAY
	addq	$32, %rsp
.Lcfi92:
	.cfi_adjust_cfa_offset -32
	movq	%r13, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB5_2
# BB#3:                                 # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, (%rax)
.LBB5_4:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	print_align_lat, .Lfunc_end5-print_align_lat
	.cfi_endproc

	.p2align	4, 0x90
	.type	read_line_buf,@function
read_line_buf:                          # @read_line_buf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi98:
	.cfi_def_cfa_offset 64
.Lcfi99:
	.cfi_offset %rbx, -48
.Lcfi100:
	.cfi_offset %r12, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	$0, 8(%rsp)
	movl	$0, 12(%rbx)
	leaq	8(%rsp), %rsi
	callq	shuffle_line
	testq	%rax, %rax
	jne	.LBB6_10
# BB#1:                                 # %.preheader
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movl	16(%rbx), %eax
	leaq	20(%rbx,%rax), %rsi
	movl	$4095, %edx             # imm = 0xFFF
	subl	%eax, %edx
	movl	%r15d, %edi
	callq	read
	movq	%rax, %rbp
	cmpq	$-1, %rbp
	je	.LBB6_3
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	addl	%ebp, 16(%rbx)
	jmp	.LBB6_6
	.p2align	4, 0x90
.LBB6_3:                                #   in Loop: Header=BB6_2 Depth=1
	callq	__errno_location
	movq	%rax, %r12
	movl	(%r12), %edi
	cmpl	$4, %edi
	jne	.LBB6_4
.LBB6_6:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	shuffle_line
	testq	%rbp, %rbp
	jne	.LBB6_9
# BB#7:                                 #   in Loop: Header=BB6_2 Depth=1
	testq	%rax, %rax
	jne	.LBB6_9
# BB#8:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	(%rbx), %rax
	movl	12(%rbx), %ecx
	movb	$0, (%rax,%rcx)
	movq	(%rbx), %rax
.LBB6_9:                                #   in Loop: Header=BB6_2 Depth=1
	testq	%rax, %rax
	je	.LBB6_2
.LBB6_10:                               # %.loopexit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_4:
	callq	strerror
	movq	%rax, %rdx
	movl	(%r12), %ecx
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	fatal
.Lfunc_end6:
	.size	read_line_buf, .Lfunc_end6-read_line_buf
	.cfi_endproc

	.p2align	4, 0x90
	.type	shuffle_line,@function
shuffle_line:                           # @shuffle_line
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -32
.Lcfi108:
	.cfi_offset %r14, -24
.Lcfi109:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	16(%rbx), %ecx
	testq	%rcx, %rcx
	je	.LBB7_12
# BB#1:
	movq	(%r14), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	jbe	.LBB7_12
# BB#2:
	movl	8(%rbx), %esi
	movl	12(%rbx), %edi
	addq	%rdi, %rdx
	cmpq	%rdx, %rsi
	ja	.LBB7_4
# BB#3:
	addl	$4096, %esi             # imm = 0x1000
	movl	%esi, 8(%rbx)
	movq	(%rbx), %rdi
	callq	xrealloc
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movl	16(%rbx), %ecx
.LBB7_4:                                # %.preheader
	movl	%ecx, %edx
	cmpq	%rdx, %rax
	jae	.LBB7_7
	.p2align	4, 0x90
.LBB7_5:                                # =>This Inner Loop Header: Depth=1
	cmpb	$10, 20(%rbx,%rax)
	je	.LBB7_7
# BB#6:                                 #   in Loop: Header=BB7_5 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	20(%rbx,%rax), %eax
	movq	(%rbx), %rcx
	movl	12(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 12(%rbx)
	movb	%al, (%rcx,%rdx)
	movq	(%r14), %rax
	movl	16(%rbx), %ecx
	cmpq	%rcx, %rax
	jb	.LBB7_5
.LBB7_7:                                # %.critedge
	movl	%ecx, %ecx
	cmpq	%rcx, %rax
	jae	.LBB7_11
# BB#8:
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movb	20(%rbx,%rax), %al
	movq	(%rbx), %rcx
	movl	12(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 12(%rbx)
	movb	%al, (%rcx,%rdx)
	movq	(%rbx), %rax
	movl	12(%rbx), %ecx
	movb	$0, (%rax,%rcx)
	movq	(%r14), %rax
	movl	16(%rbx), %r15d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	subq	%rax, %rdx
	jbe	.LBB7_10
# BB#9:
	leaq	20(%rbx), %rdi
	leaq	20(%rbx,%rax), %rsi
	callq	memmove
	subl	(%r14), %r15d
	movl	%r15d, %ecx
.LBB7_10:
	movl	%ecx, 16(%rbx)
	movq	$0, (%r14)
	movq	(%rbx), %rax
	jmp	.LBB7_13
.LBB7_11:
	movl	$0, 16(%rbx)
	movq	$0, (%r14)
.LBB7_12:
	xorl	%eax, %eax
.LBB7_13:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	shuffle_line, .Lfunc_end7-shuffle_line
	.cfi_endproc

	.type	argv0,@object           # @argv0
	.comm	argv0,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"POSIX"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%s: Warning: could not set locale to POSIX\n"
	.size	.L.str.1, 44

	.type	options,@object         # @options
	.comm	options,80,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"GTAG,GCAG,GTAC,ATAC"
	.size	.L.str.2, 20

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"A:C:c:E:f:g:I:K:L:M:o:q:R:r:W:X:"
	.size	.L.str.3, 33

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"A must be one of 0, 1, 2, 3, or 4.\n"
	.size	.L.str.4, 36

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Value for option C must be non-negative.\n"
	.size	.L.str.5, 42

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Value for option c must be non-negative.\n"
	.size	.L.str.6, 42

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Cutoff (E) must be within [3,10].\n"
	.size	.L.str.7, 35

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Filter in percent (f) must be within [0,100].\n"
	.size	.L.str.8, 47

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Value for option K must be non-negative.\n"
	.size	.L.str.9, 42

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Splice types list has illegal length (%zu)\n"
	.size	.L.str.10, 44

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Comma expected instead of %c at position %zuin splice types list.\n"
	.size	.L.str.11, 67

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Expected 'A', 'C', 'G' or 'T' instead of '%c' atposition %zu in splice types list.\n"
	.size	.L.str.12, 84

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Value for option M must be non-negative.\n"
	.size	.L.str.13, 42

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"R must be one of 0, 1, or 2.\n"
	.size	.L.str.14, 30

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"W must be within [1,15].\n"
	.size	.L.str.15, 26

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"X must be positive.\n"
	.size	.L.str.16, 21

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"?? getopt returned character code 0%o ??\n"
	.size	.L.str.17, 42

	.type	Usage,@object           # @Usage
	.section	.rodata,"a",@progbits
	.p2align	4
Usage:
	.asciz	"%s [options] dna est_db\n\nThis is SIBsim4 version 0.14.\n\nAvailable options (default value in braces[]):\n  -A <int>  output format\n             0: exon endpoints only\n             1: alignment text\n             3: both exon endpoints and alignment text\n             4: both exon endpoints and alignment text with polyA info\n            Note that 2 is unimplemented [%d]\n  -C <int>  MSP score threshold for the second pass [%d]\n  -c <int>  minimum score cutoff [%d]\n  -E <int>  cutoff value [%d]\n  -f <int>  score filter in percent (0 to disable filtering) [%d]\n  -g <int>  join exons when gap on genomic and RNA have lengths which\n            differ at most by this percentage [%d]\n  -I <int>  window width in which to search for intron splicing [%d]\n  -K <int>  MSP score threshold for the first pass [%d]\n  -L <str>  a comma separated list of forward splice-types [%s]\n  -M <int>  scoring splice sites, evaluate match within M nucleotides [%d]\n  -o <int>  offset nt positions in dna sequence by this amount [%u]\n  -q <int>  penalty for a nucleotide mismatch [%d]\n  -R <int>  direction of search\n             0: search the '+' (direct) strand only\n             1: search the '-' strand only\n             2: search both strands and report the best match\n            [%d]\n  -r <int>  reward for a nucleotide match [%d]\n  -W <int>  word size [%d]\n  -X <int>  value for terminating word extensions [%d]\n"
	.size	Usage, 1399

	.type	.L.str.18,@object       # @.str.18
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.18:
	.asciz	"Cannot read sequence from %s.\n"
	.size	.L.str.18, 31

	.type	dna_seq_head,@object    # @dna_seq_head
	.comm	dna_seq_head,256,16
	.type	rna_seq_head,@object    # @rna_seq_head
	.comm	rna_seq_head,256,16
	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Unrecognized request for EST orientation.\n"
	.size	.L.str.19, 43

	.type	dna_complement,@object  # @dna_complement
	.section	.rodata,"a",@progbits
	.p2align	4
dna_complement:
	.ascii	"                                                                 TVGH  CD  M KN   YSA BWXR       tvgh  cd  m kn   ysa bwxr                                                                                                                                      "
	.size	dna_complement, 256

	.type	.L.str.20,@object       # @.str.20
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.20:
	.asciz	"\n%s%s\n"
	.size	.L.str.20, 7

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Unrecognized option for alignment output.\n"
	.size	.L.str.22, 43

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"\nPolyA site %u nt, %u/%u A's %u\n R %.*s %u\n D %*.*s %u\n"
	.size	.L.str.24, 56

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"AATAAA"
	.size	.L.str.25, 7

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"ATTAAA"
	.size	.L.str.26, 7

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"PolyA signal %u\n"
	.size	.L.str.27, 17

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"\nPolyA site %u nt, %u/%u A's %u minus strand\n R %.*s %u\n D %*.*s %u\n"
	.size	.L.str.28, 69

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"TTTATT"
	.size	.L.str.29, 7

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"TTTAAT"
	.size	.L.str.30, 7

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"PolyA signal %u minus strand\n"
	.size	.L.str.31, 30

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Could not open file %s: %s(%d)\n"
	.size	.L.str.32, 32

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Could not read from %d: %s(%d)\n"
	.size	.L.str.33, 32

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"\n***  WARNING                                           ***\n***  there appears to be several sequences in the DNA  ***\n***  sequence file.  Only the first one will be used,  ***\n***  which might not be what was intended.             ***\n\n"
	.size	.L.str.34, 239

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"; LEN="
	.size	.L.str.35, 7

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"; LEN=%u\n"
	.size	.L.str.36, 10

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Sequence too long: %u\n"
	.size	.L.str.37, 23

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"\nCaught signal %d while processing:\n%.256s\n%.256s\n"
	.size	.L.str.38, 51

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"(complement)\n"
	.size	.Lstr, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
