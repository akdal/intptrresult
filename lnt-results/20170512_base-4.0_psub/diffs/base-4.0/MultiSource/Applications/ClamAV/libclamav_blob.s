	.text
	.file	"libclamav_blob.bc"
	.globl	blobCreate
	.p2align	4, 0x90
	.type	blobCreate,@function
blobCreate:                             # @blobCreate
	.cfi_startproc
# BB#0:
	movl	$1, %edi
	movl	$40, %esi
	jmp	cli_calloc              # TAILCALL
.Lfunc_end0:
	.size	blobCreate, .Lfunc_end0-blobCreate
	.cfi_endproc

	.globl	blobDestroy
	.p2align	4, 0x90
	.type	blobDestroy,@function
blobDestroy:                            # @blobDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	callq	free
.LBB1_2:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	free
.LBB1_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	blobDestroy, .Lfunc_end1-blobDestroy
	.cfi_endproc

	.globl	blobArrayDestroy
	.p2align	4, 0x90
	.type	blobArrayDestroy,@function
blobArrayDestroy:                       # @blobArrayDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	testl	%ebp, %ebp
	jle	.LBB2_9
# BB#1:                                 # %.lr.ph.preheader
	movslq	%ebp, %rbx
	incq	%rbx
	decl	%ebp
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movq	-16(%r14,%rbx,8), %r15
	testq	%r15, %r15
	je	.LBB2_8
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB2_5
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	callq	free
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=1
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=1
	callq	free
.LBB2_7:                                # %blobDestroy.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%r15, %rdi
	callq	free
	movq	$0, -16(%r14,%rbx,8)
.LBB2_8:                                # %.backedge
                                        #   in Loop: Header=BB2_2 Depth=1
	decq	%rbx
	decl	%ebp
	cmpq	$1, %rbx
	jg	.LBB2_2
.LBB2_9:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	blobArrayDestroy, .Lfunc_end2-blobArrayDestroy
	.cfi_endproc

	.globl	blobSetFilename
	.p2align	4, 0x90
	.type	blobSetFilename,@function
blobSetFilename:                        # @blobSetFilename
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_2
# BB#1:
	callq	free
.LBB3_2:
	movq	%rbx, %rdi
	callq	cli_strdup
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.LBB3_3
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_7:                                #   in Loop: Header=BB3_3 Depth=1
	incq	%rax
.LBB3_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	cmpb	$47, %cl
	je	.LBB3_6
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	testb	%cl, %cl
	jne	.LBB3_7
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=1
	movb	$95, (%rax)
	jmp	.LBB3_7
.LBB3_5:                                # %sanitiseName.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	blobSetFilename, .Lfunc_end3-blobSetFilename
	.cfi_endproc

	.globl	sanitiseName
	.p2align	4, 0x90
	.type	sanitiseName,@function
sanitiseName:                           # @sanitiseName
	.cfi_startproc
# BB#0:
	jmp	.LBB4_1
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_1 Depth=1
	incq	%rdi
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %eax
	cmpb	$47, %al
	je	.LBB4_4
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	testb	%al, %al
	jne	.LBB4_5
	jmp	.LBB4_3
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_1 Depth=1
	movb	$95, (%rdi)
	jmp	.LBB4_5
.LBB4_3:
	retq
.Lfunc_end4:
	.size	sanitiseName, .Lfunc_end4-sanitiseName
	.cfi_endproc

	.globl	blobAddData
	.p2align	4, 0x90
	.type	blobAddData,@function
blobAddData:                            # @blobAddData
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %rbp
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB5_10
# BB#1:
	cmpl	$0, 32(%rbp)
	je	.LBB5_3
# BB#2:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$0, 32(%rbp)
.LBB5_3:
	movq	8(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_7
# BB#4:
	movq	24(%rbp), %rax
	movq	16(%rbp), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rax
	jge	.LBB5_9
# BB#5:
	leaq	(%rax,%rbx,4), %rsi
	callq	cli_realloc
	movq	%rax, %rdi
	testq	%rdi, %rdi
	je	.LBB5_6
# BB#8:                                 # %thread-pre-split.thread29
	leaq	(,%rbx,4), %rax
	addq	%rax, 24(%rbp)
	movq	%rdi, 8(%rbp)
	jmp	.LBB5_9
.LBB5_7:                                # %thread-pre-split
	leaq	(,%rbx,4), %rdi
	movq	%rdi, 24(%rbp)
	callq	cli_malloc
	movq	%rax, %rdi
	movq	%rdi, 8(%rbp)
	testq	%rdi, %rdi
	je	.LBB5_10
.LBB5_9:                                # %thread-pre-split.thread
	addq	16(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, 16(%rbp)
.LBB5_10:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_6:
	movl	$-1, %r14d
	jmp	.LBB5_10
.Lfunc_end5:
	.size	blobAddData, .Lfunc_end5-blobAddData
	.cfi_endproc

	.globl	blobGetData
	.p2align	4, 0x90
	.type	blobGetData,@function
blobGetData:                            # @blobGetData
	.cfi_startproc
# BB#0:
	cmpq	$0, 16(%rdi)
	je	.LBB6_1
# BB#2:
	movq	8(%rdi), %rax
	retq
.LBB6_1:
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	blobGetData, .Lfunc_end6-blobGetData
	.cfi_endproc

	.globl	blobGetDataSize
	.p2align	4, 0x90
	.type	blobGetDataSize,@function
blobGetDataSize:                        # @blobGetDataSize
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end7:
	.size	blobGetDataSize, .Lfunc_end7-blobGetDataSize
	.cfi_endproc

	.globl	blobClose
	.p2align	4, 0x90
	.type	blobClose,@function
blobClose:                              # @blobClose
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpl	$0, 32(%rbx)
	je	.LBB8_1
# BB#8:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	cli_warnmsg             # TAILCALL
.LBB8_1:
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rax
	subq	%rsi, %rax
	cmpq	$64, %rax
	jl	.LBB8_6
# BB#2:
	movq	8(%rbx), %rdi
	testq	%rsi, %rsi
	je	.LBB8_3
# BB#4:
	callq	cli_realloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB8_7
# BB#5:                                 # %.critedge
	movq	24(%rbx), %rdx
	movq	%rdx, %rsi
	subq	16(%rbx), %rsi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	movq	%r14, 8(%rbx)
	jmp	.LBB8_6
.LBB8_3:
	callq	free
	movq	$0, 8(%rbx)
	movq	24(%rbx), %rsi
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	$0, 24(%rbx)
.LBB8_6:
	movl	$1, 32(%rbx)
.LBB8_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	blobClose, .Lfunc_end8-blobClose
	.cfi_endproc

	.globl	blobcmp
	.p2align	4, 0x90
	.type	blobcmp,@function
blobcmp:                                # @blobcmp
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.LBB9_3
# BB#1:
	movq	16(%rdi), %rdx
	movl	$1, %eax
	cmpq	16(%rsi), %rdx
	jne	.LBB9_3
# BB#2:
	testq	%rdx, %rdx
	movl	$0, %eax
	je	.LBB9_3
# BB#4:                                 # %blobGetData.exit
	movq	8(%rdi), %rdi
	movq	8(%rsi), %rsi
	jmp	memcmp                  # TAILCALL
.LBB9_3:
	retq
.Lfunc_end9:
	.size	blobcmp, .Lfunc_end9-blobcmp
	.cfi_endproc

	.globl	blobGrow
	.p2align	4, 0x90
	.type	blobGrow,@function
blobGrow:                               # @blobGrow
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%r14, %r14
	je	.LBB10_1
# BB#2:
	cmpl	$0, 32(%rbx)
	je	.LBB10_4
# BB#3:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$0, 32(%rbx)
.LBB10_4:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_5
# BB#8:
	movq	24(%rbx), %rsi
	addq	%r14, %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB10_9
# BB#10:
	addq	%r14, 24(%rbx)
	movq	%rax, 8(%rbx)
	jmp	.LBB10_11
.LBB10_1:
	xorl	%eax, %eax
	jmp	.LBB10_12
.LBB10_5:
	movq	%r14, %rdi
	callq	cli_malloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB10_6
# BB#7:
	movq	%r14, 24(%rbx)
	jmp	.LBB10_11
.LBB10_9:                               # %._crit_edge
	movq	8(%rbx), %rax
	jmp	.LBB10_11
.LBB10_6:
	xorl	%eax, %eax
.LBB10_11:
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$-114, %eax
	cmovnel	%ecx, %eax
.LBB10_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	blobGrow, .Lfunc_end10-blobGrow
	.cfi_endproc

	.globl	fileblobCreate
	.p2align	4, 0x90
	.type	fileblobCreate,@function
fileblobCreate:                         # @fileblobCreate
	.cfi_startproc
# BB#0:
	movl	$1, %edi
	movl	$80, %esi
	jmp	cli_calloc              # TAILCALL
.Lfunc_end11:
	.size	fileblobCreate, .Lfunc_end11-fileblobCreate
	.cfi_endproc

	.globl	fileblobScanAndDestroy
	.p2align	4, 0x90
	.type	fileblobScanAndDestroy,@function
fileblobScanAndDestroy:                 # @fileblobScanAndDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
.Lcfi36:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB12_1
.LBB12_5:
	movq	%rbx, %rdi
	callq	fileblobDestroy
.LBB12_6:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB12_1:
	movq	%rbx, %rdi
	callq	fileblobScan
	cmpl	$2, %eax
	je	.LBB12_4
# BB#2:
	cmpl	$1, %eax
	jne	.LBB12_5
# BB#3:
	movq	%rbx, %rdi
	callq	fileblobDestructiveDestroy
	movl	$1, %eax
	popq	%rbx
	retq
.LBB12_4:
	movq	%rbx, %rdi
	callq	fileblobDestructiveDestroy
	jmp	.LBB12_6
.Lfunc_end12:
	.size	fileblobScanAndDestroy, .Lfunc_end12-fileblobScanAndDestroy
	.cfi_endproc

	.globl	fileblobDestroy
	.p2align	4, 0x90
	.type	fileblobDestroy,@function
fileblobDestroy:                        # @fileblobDestroy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %r14
	cmpq	$0, 8(%rbx)
	je	.LBB13_7
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_7
# BB#2:
	callq	fclose
	movq	48(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB13_6
# BB#3:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$1, 56(%rbx)
	jne	.LBB13_6
# BB#4:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	48(%rbx), %rdi
	callq	unlink
	testl	%eax, %eax
	jns	.LBB13_6
# BB#5:
	movq	48(%rbx), %rsi
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB13_6:
	movq	(%r14), %rdi
	callq	free
	jmp	.LBB13_11
.LBB13_7:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_11
# BB#8:
	callq	free
	cmpq	$0, (%r14)
	je	.LBB13_10
# BB#9:
	leaq	48(%rbx), %rax
	cmpq	$0, 48(%rbx)
	cmovneq	%rax, %r14
	movq	(%r14), %rsi
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	8(%rbx), %rdi
	callq	free
	jmp	.LBB13_11
.LBB13_10:
	movq	24(%rbx), %rsi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB13_11:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_13
# BB#12:
	callq	free
.LBB13_13:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end13:
	.size	fileblobDestroy, .Lfunc_end13-fileblobDestroy
	.cfi_endproc

	.globl	fileblobScan
	.p2align	4, 0x90
	.type	fileblobScan,@function
fileblobScan:                           # @fileblobScan
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %ebp
	testb	$2, 56(%rbx)
	jne	.LBB14_10
# BB#1:
	cmpq	$0, 48(%rbx)
	je	.LBB14_2
# BB#3:
	cmpq	$0, 72(%rbx)
	je	.LBB14_4
# BB#5:
	movq	(%rbx), %rdi
	callq	fflush
	movq	(%rbx), %rdi
	callq	fileno
	movl	%eax, %edi
	callq	dup
	movl	%eax, %r14d
	cmpl	$-1, %r14d
	je	.LBB14_6
# BB#7:
	movq	72(%rbx), %rsi
	movl	%r14d, %edi
	callq	cli_magic_scandesc
	movl	%eax, %r15d
	movl	%r14d, %edi
	callq	close
	movq	48(%rbx), %rsi
	cmpl	$1, %r15d
	jne	.LBB14_9
# BB#8:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB14_10
.LBB14_2:
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$-111, %ebp
	jmp	.LBB14_10
.LBB14_4:
	xorl	%ebp, %ebp
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB14_10
.LBB14_6:
	movq	48(%rbx), %rsi
	xorl	%ebp, %ebp
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB14_10
.LBB14_9:
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$2, %ebp
.LBB14_10:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	fileblobScan, .Lfunc_end14-fileblobScan
	.cfi_endproc

	.globl	fileblobDestructiveDestroy
	.p2align	4, 0x90
	.type	fileblobDestructiveDestroy,@function
fileblobDestructiveDestroy:             # @fileblobDestructiveDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 16
.Lcfi52:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_5
# BB#1:
	cmpq	$0, 48(%rbx)
	je	.LBB15_5
# BB#2:
	callq	fclose
	movq	48(%rbx), %rsi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	48(%rbx), %rdi
	callq	unlink
	testl	%eax, %eax
	jns	.LBB15_4
# BB#3:
	movq	48(%rbx), %rsi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB15_4:
	movq	48(%rbx), %rdi
	callq	free
	movq	$0, (%rbx)
	movq	$0, 48(%rbx)
.LBB15_5:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_7
# BB#6:
	callq	free
	movq	$0, 8(%rbx)
.LBB15_7:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	fileblobDestroy         # TAILCALL
.Lfunc_end15:
	.size	fileblobDestructiveDestroy, .Lfunc_end15-fileblobDestructiveDestroy
	.cfi_endproc

	.globl	fileblobSetFilename
	.p2align	4, 0x90
	.type	fileblobSetFilename,@function
fileblobSetFilename:                    # @fileblobSetFilename
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 40
	subq	$264, %rsp              # imm = 0x108
.Lcfi57:
	.cfi_def_cfa_offset 304
.Lcfi58:
	.cfi_offset %rbx, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	$0, 8(%rbx)
	jne	.LBB16_21
# BB#1:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB16_3
# BB#2:
	callq	free
.LBB16_3:
	movq	%rbp, %rdi
	callq	cli_strdup
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.LBB16_5
# BB#4:
	xorl	%r15d, %r15d
	jmp	.LBB16_8
	.p2align	4, 0x90
.LBB16_14:                              #   in Loop: Header=BB16_5 Depth=1
	incq	%rax
.LBB16_5:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	cmpb	$47, %cl
	je	.LBB16_13
# BB#6:                                 # %.preheader.i
                                        #   in Loop: Header=BB16_5 Depth=1
	testb	%cl, %cl
	jne	.LBB16_14
	jmp	.LBB16_7
	.p2align	4, 0x90
.LBB16_13:                              #   in Loop: Header=BB16_5 Depth=1
	movb	$95, (%rax)
	jmp	.LBB16_14
.LBB16_7:                               # %blobSetFilename.exit.loopexit
	movq	8(%rbx), %r15
.LBB16_8:                               # %blobSetFilename.exit
	movq	%r14, %rdi
	callq	strlen
	movl	$248, %ecx
	subl	%eax, %ecx
	movq	%rsp, %rbp
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r14, %rdx
	movq	%r15, %r8
	callq	sprintf
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	mkstemp
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB16_9
.LBB16_15:                              # %.thread25
	movq	%rsp, %rsi
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.22, %esi
	movl	%ebp, %edi
	callq	fdopen
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB16_16
# BB#17:
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB16_20
# BB#18:
	movq	24(%rbx), %rdx
	movq	%rbx, %rdi
	callq	fileblobAddData
	testl	%eax, %eax
	jne	.LBB16_20
# BB#19:
	leaq	16(%rbx), %rbp
	movq	16(%rbx), %rdi
	callq	free
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	$0, 16(%rbp)
	orb	$1, 56(%rbx)
.LBB16_20:
	movq	%rsp, %rdi
	callq	cli_strdup
	movq	%rax, 48(%rbx)
	jmp	.LBB16_21
.LBB16_9:
	callq	__errno_location
	movq	%rax, %r15
	movl	(%r15), %edi
	cmpl	$22, %edi
	jne	.LBB16_12
# BB#10:
	movq	%rsp, %rbp
	movl	$257, %esi              # imm = 0x101
	movl	$.L.str.17, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r14, %rcx
	callq	snprintf
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	mkstemp
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jns	.LBB16_15
# BB#11:                                # %..thread_crit_edge
	movl	(%r15), %edi
.LBB16_12:                              # %.thread
	callq	strerror
	movq	%rax, %rcx
	movq	%rsp, %rbx
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	callq	cli_errmsg
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	$.L.str.20, %edi
	movl	$257, %esi              # imm = 0x101
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	cli_dbgmsg
	jmp	.LBB16_21
.LBB16_16:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movq	%rsp, %rbx
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	callq	cli_errmsg
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	$.L.str.20, %edi
	movl	$257, %esi              # imm = 0x101
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	cli_dbgmsg
	movl	%ebp, %edi
	callq	close
.LBB16_21:
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	fileblobSetFilename, .Lfunc_end16-fileblobSetFilename
	.cfi_endproc

	.globl	fileblobAddData
	.p2align	4, 0x90
	.type	fileblobAddData,@function
fileblobAddData:                        # @fileblobAddData
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 48
.Lcfi67:
	.cfi_offset %rbx, -48
.Lcfi68:
	.cfi_offset %r12, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %rbp
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	je	.LBB17_26
# BB#1:
	cmpq	$0, (%rbp)
	je	.LBB17_6
# BB#2:
	testb	$2, 56(%rbp)
	jne	.LBB17_26
# BB#3:
	movq	72(%rbp), %r12
	testq	%r12, %r12
	je	.LBB17_22
# BB#4:
	movq	32(%r12), %rcx
	testq	%rcx, %rcx
	movq	64(%rbp), %rax
	je	.LBB17_15
# BB#5:
	cmpq	24(%rcx), %rax
	setae	%cl
	testb	%cl, %cl
	je	.LBB17_16
	jmp	.LBB17_22
.LBB17_6:
	cmpl	$0, 40(%rbp)
	je	.LBB17_8
# BB#7:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$0, 40(%rbp)
.LBB17_8:
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_12
# BB#9:
	movq	32(%rbp), %rax
	leaq	24(%rbp), %r12
	movq	24(%rbp), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rax
	jge	.LBB17_14
# BB#10:
	leaq	(%rax,%rbx,4), %rsi
	callq	cli_realloc
	movq	%rax, %rdi
	testq	%rdi, %rdi
	je	.LBB17_25
# BB#11:                                # %thread-pre-split.thread29.i
	leaq	(,%rbx,4), %rax
	addq	%rax, 32(%rbp)
	movq	%rdi, 16(%rbp)
	jmp	.LBB17_14
.LBB17_12:                              # %thread-pre-split.i
	leaq	(,%rbx,4), %rdi
	movq	%rdi, 32(%rbp)
	callq	cli_malloc
	movq	%rax, %rdi
	movq	%rdi, 16(%rbp)
	testq	%rdi, %rdi
	je	.LBB17_26
# BB#13:                                # %thread-pre-split.i.thread-pre-split.thread.i_crit_edge
	addq	$24, %rbp
	movq	%rbp, %r12
.LBB17_14:                              # %thread-pre-split.thread.i
	addq	(%r12), %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, (%r12)
	jmp	.LBB17_26
.LBB17_15:
	xorl	%ecx, %ecx
	testb	%cl, %cl
	jne	.LBB17_22
.LBB17_16:                              # %._crit_edge
	cmpq	$20480, %rax            # imm = 0x5000
	ja	.LBB17_22
# BB#17:
	movq	8(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB17_19
# BB#18:
	movq	%rbx, %rax
	shrq	$12, %rax
	addq	%rax, (%rcx)
	movq	64(%rbp), %rax
.LBB17_19:
	addq	%rbx, %rax
	movq	%rax, 64(%rbp)
	cmpq	$6, %rbx
	jb	.LBB17_22
# BB#20:
	movq	(%r12), %rdx
	movq	24(%r12), %rcx
	movl	$501, %r8d              # imm = 0x1F5
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	cli_scanbuff
	cmpl	$1, %eax
	jne	.LBB17_22
# BB#21:
	movq	(%r12), %rax
	movq	(%rax), %rsi
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	orb	$2, 56(%rbp)
.LBB17_22:
	movq	(%rbp), %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB17_24
# BB#23:
	orb	$1, 56(%rbp)
	jmp	.LBB17_26
.LBB17_24:
	movq	8(%rbp), %rbp
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	cli_errmsg
.LBB17_25:
	movl	$-1, %r15d
.LBB17_26:                              # %blobAddData.exit
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	fileblobAddData, .Lfunc_end17-fileblobAddData
	.cfi_endproc

	.globl	fileblobGetFilename
	.p2align	4, 0x90
	.type	fileblobGetFilename,@function
fileblobGetFilename:                    # @fileblobGetFilename
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	retq
.Lfunc_end18:
	.size	fileblobGetFilename, .Lfunc_end18-fileblobGetFilename
	.cfi_endproc

	.globl	fileblobSetCTX
	.p2align	4, 0x90
	.type	fileblobSetCTX,@function
fileblobSetCTX:                         # @fileblobSetCTX
	.cfi_startproc
# BB#0:
	movq	%rsi, 72(%rdi)
	retq
.Lfunc_end19:
	.size	fileblobSetCTX, .Lfunc_end19-fileblobSetCTX
	.cfi_endproc

	.globl	fileblobInfected
	.p2align	4, 0x90
	.type	fileblobInfected,@function
fileblobInfected:                       # @fileblobInfected
	.cfi_startproc
# BB#0:
	movb	56(%rdi), %al
	shrb	%al
	andb	$1, %al
	movzbl	%al, %eax
	retq
.Lfunc_end20:
	.size	fileblobInfected, .Lfunc_end20-fileblobInfected
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"blobDestroy\n"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"blobArrayDestroy: %d\n"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"blobSetFilename: %s\n"
	.size	.L.str.2, 21

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Reopening closed blob\n"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Attempt to close a previously closed blob\n"
	.size	.L.str.4, 43

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"blobClose: recovered all %lu bytes\n"
	.size	.L.str.5, 36

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"blobClose: recovered %lu bytes from %lu\n"
	.size	.L.str.6, 41

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Growing closed blob\n"
	.size	.L.str.7, 21

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"fileblobDestructiveDestroy: %s\n"
	.size	.L.str.8, 32

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"fileblobDestructiveDestroy: Can't delete file %s\n"
	.size	.L.str.9, 50

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"fileblobDestroy: %s\n"
	.size	.L.str.10, 21

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"fileblobDestroy: not saving empty file\n"
	.size	.L.str.11, 40

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"fileblobDestroy: Can't delete empty file %s\n"
	.size	.L.str.12, 45

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"fileblobDestroy: %s not saved: report to http://bugs.clamav.net\n"
	.size	.L.str.13, 65

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"fileblobDestroy: file not saved (%lu bytes): report to http://bugs.clamav.net\n"
	.size	.L.str.14, 79

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"%s/%.*sXXXXXX"
	.size	.L.str.15, 14

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"fileblobSetFilename: mkstemp(%s)\n"
	.size	.L.str.16, 34

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%s/clamavtmpXXXXXXXXXXXXX"
	.size	.L.str.17, 26

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"fileblobSetFilename: retry as mkstemp(%s)\n"
	.size	.L.str.18, 43

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Can't create temporary file %s: %s\n"
	.size	.L.str.19, 36

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"%lu %lu\n"
	.size	.L.str.20, 9

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Creating %s\n"
	.size	.L.str.21, 13

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"wb"
	.size	.L.str.22, 3

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Can't create file %s: %s\n"
	.size	.L.str.23, 26

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"fileblobAddData: found %s\n"
	.size	.L.str.24, 27

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"fileblobAddData: Can't write %lu bytes to temporary file %s: %s\n"
	.size	.L.str.25, 65

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"fileblobScan, fullname == NULL\n"
	.size	.L.str.26, 32

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"fileblobScan, ctx == NULL\n"
	.size	.L.str.27, 27

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"%s: dup failed\n"
	.size	.L.str.28, 16

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"%s is infected\n"
	.size	.L.str.29, 16

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%s is clean\n"
	.size	.L.str.30, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
