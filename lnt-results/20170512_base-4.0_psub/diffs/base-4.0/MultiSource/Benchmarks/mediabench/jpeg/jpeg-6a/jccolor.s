	.text
	.file	"jccolor.bc"
	.globl	jinit_color_converter
	.p2align	4, 0x90
	.type	jinit_color_converter,@function
jinit_color_converter:                  # @jinit_color_converter
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$24, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 464(%rbx)
	movq	$null_method, (%r14)
	movl	52(%rbx), %eax
	leal	-2(%rax), %ecx
	cmpl	$2, %ecx
	jb	.LBB0_4
# BB#1:
	leal	-4(%rax), %ecx
	cmpl	$2, %ecx
	jae	.LBB0_2
# BB#5:
	cmpl	$4, 48(%rbx)
	jne	.LBB0_7
	jmp	.LBB0_8
.LBB0_4:
	cmpl	$3, 48(%rbx)
	jne	.LBB0_7
	jmp	.LBB0_8
.LBB0_2:
	cmpl	$1, %eax
	jne	.LBB0_6
# BB#3:
	cmpl	$1, 48(%rbx)
	jne	.LBB0_7
	jmp	.LBB0_8
.LBB0_6:
	cmpl	$0, 48(%rbx)
	jg	.LBB0_8
.LBB0_7:
	movq	(%rbx), %rax
	movl	$7, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_8:
	movl	72(%rbx), %eax
	leal	-1(%rax), %ecx
	cmpl	$4, %ecx
	ja	.LBB0_35
# BB#9:
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_10:
	cmpl	$1, 68(%rbx)
	je	.LBB0_12
# BB#11:
	movq	(%rbx), %rax
	movl	$8, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_12:
	movl	52(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB0_17
# BB#13:
	cmpl	$2, %eax
	je	.LBB0_16
# BB#14:
	cmpl	$1, %eax
	jne	.LBB0_21
# BB#15:
	movq	$grayscale_convert, 8(%r14)
	jmp	.LBB0_39
.LBB0_18:
	cmpl	$3, 68(%rbx)
	je	.LBB0_20
# BB#19:
	movq	(%rbx), %rax
	movl	$8, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_20:
	cmpl	$2, 52(%rbx)
	jne	.LBB0_21
	jmp	.LBB0_38
.LBB0_22:
	cmpl	$3, 68(%rbx)
	je	.LBB0_24
# BB#23:
	movq	(%rbx), %rax
	movl	$8, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_24:
	movl	52(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB0_38
# BB#25:
	cmpl	$2, %eax
	jne	.LBB0_21
# BB#26:
	movq	$rgb_ycc_start, (%r14)
	movq	$rgb_ycc_convert, 8(%r14)
	jmp	.LBB0_39
.LBB0_27:
	cmpl	$4, 68(%rbx)
	je	.LBB0_29
# BB#28:
	movq	(%rbx), %rax
	movl	$8, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_29:
	cmpl	$4, 52(%rbx)
	jne	.LBB0_21
	jmp	.LBB0_38
.LBB0_30:
	cmpl	$4, 68(%rbx)
	je	.LBB0_32
# BB#31:
	movq	(%rbx), %rax
	movl	$8, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_32:
	movl	52(%rbx), %eax
	cmpl	$5, %eax
	je	.LBB0_38
# BB#33:
	cmpl	$4, %eax
	jne	.LBB0_21
# BB#34:
	movq	$rgb_ycc_start, (%r14)
	movq	$cmyk_ycck_convert, 8(%r14)
	jmp	.LBB0_39
.LBB0_21:
	movq	(%rbx), %rax
	movl	$25, 40(%rax)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*(%rax)                 # TAILCALL
.LBB0_35:
	cmpl	52(%rbx), %eax
	jne	.LBB0_37
# BB#36:
	movl	68(%rbx), %eax
	cmpl	48(%rbx), %eax
	je	.LBB0_38
.LBB0_37:
	movq	(%rbx), %rax
	movl	$25, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_38:
	movq	$null_convert, 8(%r14)
.LBB0_39:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_17:
	movq	$grayscale_convert, 8(%r14)
	jmp	.LBB0_39
.LBB0_16:
	movq	$rgb_ycc_start, (%r14)
	movq	$rgb_gray_convert, 8(%r14)
	jmp	.LBB0_39
.Lfunc_end0:
	.size	jinit_color_converter, .Lfunc_end0-jinit_color_converter
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_10
	.quad	.LBB0_18
	.quad	.LBB0_22
	.quad	.LBB0_27
	.quad	.LBB0_30

	.text
	.p2align	4, 0x90
	.type	null_method,@function
null_method:                            # @null_method
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	null_method, .Lfunc_end1-null_method
	.cfi_endproc

	.p2align	4, 0x90
	.type	grayscale_convert,@function
grayscale_convert:                      # @grayscale_convert
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
.Lcfi9:
	.cfi_offset %rbx, -40
.Lcfi10:
	.cfi_offset %r12, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %r15, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	testl	%r8d, %r8d
	jle	.LBB2_11
# BB#1:                                 # %.lr.ph31
	movl	40(%rdi), %r9d
	testl	%r9d, %r9d
	je	.LBB2_11
# BB#2:                                 # %.lr.ph31.split.us.preheader
	movslq	48(%rdi), %r12
	leaq	-1(%r9), %r10
	movl	%r9d, %r11d
	andl	$7, %r11d
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph31.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_6 Depth 2
                                        #     Child Loop BB2_9 Depth 2
	testq	%r11, %r11
	movq	(%rsi), %rax
	movq	(%rdx), %r14
	movl	%ecx, %ebx
	movq	(%r14,%rbx,8), %r15
	je	.LBB2_4
# BB#5:                                 # %.prol.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_6:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ebx
	movb	%bl, (%r15,%rdi)
	addq	%r12, %rax
	incq	%rdi
	cmpq	%rdi, %r11
	jne	.LBB2_6
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_3 Depth=1
	xorl	%edi, %edi
.LBB2_7:                                # %.prol.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpq	$7, %r10
	jb	.LBB2_10
# BB#8:                                 # %.lr.ph31.split.us.new
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r9, %r14
	subq	%rdi, %r14
	leaq	7(%r15,%rdi), %rdi
	.p2align	4, 0x90
.LBB2_9:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ebx
	movb	%bl, -7(%rdi)
	movzbl	(%rax,%r12), %ebx
	addq	%r12, %rax
	movb	%bl, -6(%rdi)
	movzbl	(%r12,%rax), %ebx
	addq	%r12, %rax
	movb	%bl, -5(%rdi)
	movzbl	(%r12,%rax), %ebx
	addq	%r12, %rax
	movb	%bl, -4(%rdi)
	movzbl	(%r12,%rax), %ebx
	addq	%r12, %rax
	movb	%bl, -3(%rdi)
	movzbl	(%r12,%rax), %ebx
	addq	%r12, %rax
	movb	%bl, -2(%rdi)
	movzbl	(%r12,%rax), %ebx
	addq	%r12, %rax
	movb	%bl, -1(%rdi)
	movzbl	(%r12,%rax), %ebx
	addq	%r12, %rax
	movb	%bl, (%rdi)
	addq	$8, %rdi
	addq	%r12, %rax
	addq	$-8, %r14
	jne	.LBB2_9
.LBB2_10:                               # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB2_3 Depth=1
	addq	$8, %rsi
	incl	%ecx
	cmpl	$1, %r8d
	leal	-1(%r8), %eax
	movl	%eax, %r8d
	jg	.LBB2_3
.LBB2_11:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	grayscale_convert, .Lfunc_end2-grayscale_convert
	.cfi_endproc

	.p2align	4, 0x90
	.type	rgb_ycc_start,@function
rgb_ycc_start:                          # @rgb_ycc_start
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	8(%rdi), %rax
	movq	464(%rdi), %rbx
	movl	$1, %esi
	movl	$16384, %edx            # imm = 0x4000
	callq	*(%rax)
	movq	%rax, 16(%rbx)
	addq	$14336, %rax            # imm = 0x3800
	movl	$8421375, %ecx          # imm = 0x807FFF
	movl	$32768, %r8d            # imm = 0x8000
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdx, -14336(%rax)
	movq	%rdi, -12288(%rax)
	movq	%r8, -10240(%rax)
	movq	%rsi, -8192(%rax)
	movq	%r11, -6144(%rax)
	movq	%rcx, -4096(%rax)
	movq	%r10, -2048(%rax)
	movq	%r9, (%rax)
	addq	$32768, %rcx            # imm = 0x8000
	addq	$8, %rax
	addq	$19595, %rdx            # imm = 0x4C8B
	addq	$38470, %rdi            # imm = 0x9646
	addq	$7471, %r8              # imm = 0x1D2F
	addq	$-11059, %rsi           # imm = 0xD4CD
	addq	$-21709, %r11           # imm = 0xAB33
	addq	$-27439, %r10           # imm = 0x94D1
	addq	$-5329, %r9             # imm = 0xEB2F
	cmpq	$16809983, %rcx         # imm = 0x1007FFF
	jne	.LBB3_1
# BB#2:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	rgb_ycc_start, .Lfunc_end3-rgb_ycc_start
	.cfi_endproc

	.p2align	4, 0x90
	.type	rgb_gray_convert,@function
rgb_gray_convert:                       # @rgb_gray_convert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	testl	%r8d, %r8d
	jle	.LBB4_6
# BB#1:
	movl	40(%rdi), %r9d
	testl	%r9d, %r9d
	je	.LBB4_6
# BB#2:                                 # %.lr.ph47.split.us.preheader
	movq	464(%rdi), %rax
	movq	16(%rax), %r11
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph47.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	leal	-1(%r8), %r10d
	movq	(%rsi), %rax
	movq	(%rdx), %rdi
	movl	%ecx, %ebx
	movq	(%rdi,%rbx,8), %rdi
	movq	%r9, %rbx
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	1(%rax), %r14d
	movzbl	2(%rax), %r15d
	movzbl	(%rax), %r12d
	movl	2048(%r11,%r14,8), %ebp
	addl	(%r11,%r12,8), %ebp
	addl	4096(%r11,%r15,8), %ebp
	shrl	$16, %ebp
	movb	%bpl, (%rdi)
	incq	%rdi
	addq	$3, %rax
	decq	%rbx
	jne	.LBB4_4
# BB#5:                                 # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB4_3 Depth=1
	addq	$8, %rsi
	incl	%ecx
	cmpl	$1, %r8d
	movl	%r10d, %r8d
	jg	.LBB4_3
.LBB4_6:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	rgb_gray_convert, .Lfunc_end4-rgb_gray_convert
	.cfi_endproc

	.p2align	4, 0x90
	.type	null_convert,@function
null_convert:                           # @null_convert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	testl	%r8d, %r8d
	jle	.LBB5_14
# BB#1:                                 # %.preheader.lr.ph
	movslq	68(%rdi), %r10
	testl	%r10d, %r10d
	jle	.LBB5_14
# BB#2:                                 # %.preheader.us.preheader
	movl	40(%rdi), %r15d
	movl	%r10d, %edx
	leaq	-1(%r15), %r11
	movq	%r15, -16(%rsp)         # 8-byte Spill
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	andl	$7, %r15d
	leaq	(,%r10,8), %r14
	.p2align	4, 0x90
.LBB5_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_5 Depth 2
                                        #       Child Loop BB5_8 Depth 3
                                        #       Child Loop BB5_11 Depth 3
	cmpl	$0, -16(%rsp)           # 4-byte Folded Reload
	movl	%ecx, %ecx
	je	.LBB5_13
# BB#4:                                 # %.lr.ph.us.us.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph.us.us
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_8 Depth 3
                                        #       Child Loop BB5_11 Depth 3
	testq	%r15, %r15
	movq	(%rsi), %rdi
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rbx
	movq	(%rbx,%rcx,8), %r9
	je	.LBB5_6
# BB#7:                                 # %.prol.preheader
                                        #   in Loop: Header=BB5_5 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_8:                                #   Parent Loop BB5_3 Depth=1
                                        #     Parent Loop BB5_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rdi,%r12), %ebx
	movb	%bl, (%r9,%rbp)
	addq	%r10, %rdi
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB5_8
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_5 Depth=2
	xorl	%ebp, %ebp
.LBB5_9:                                # %.prol.loopexit
                                        #   in Loop: Header=BB5_5 Depth=2
	cmpq	$7, %r11
	jb	.LBB5_12
# BB#10:                                # %.lr.ph.us.us.new
                                        #   in Loop: Header=BB5_5 Depth=2
	movq	-16(%rsp), %r13         # 8-byte Reload
	subq	%rbp, %r13
	leaq	7(%r9,%rbp), %rbp
	addq	%r12, %rdi
	.p2align	4, 0x90
.LBB5_11:                               #   Parent Loop BB5_3 Depth=1
                                        #     Parent Loop BB5_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rdi), %eax
	movb	%al, -7(%rbp)
	leaq	(%rdi,%r10), %rax
	movzbl	(%rdi,%r10), %ebx
	movb	%bl, -6(%rbp)
	movzbl	(%r10,%rax), %ebx
	addq	%r10, %rax
	movb	%bl, -5(%rbp)
	movzbl	(%r10,%rax), %ebx
	addq	%r10, %rax
	movb	%bl, -4(%rbp)
	movzbl	(%r10,%rax), %ebx
	addq	%r10, %rax
	movb	%bl, -3(%rbp)
	movzbl	(%r10,%rax), %ebx
	addq	%r10, %rax
	movb	%bl, -2(%rbp)
	movzbl	(%r10,%rax), %ebx
	addq	%r10, %rax
	movb	%bl, -1(%rbp)
	movzbl	(%r10,%rax), %eax
	movb	%al, (%rbp)
	addq	$8, %rbp
	addq	%r14, %rdi
	addq	$-8, %r13
	jne	.LBB5_11
.LBB5_12:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB5_5 Depth=2
	incq	%r12
	cmpq	%rdx, %r12
	jne	.LBB5_5
.LBB5_13:                               # %._crit_edge39.us
                                        #   in Loop: Header=BB5_3 Depth=1
	addq	$8, %rsi
	incl	%ecx
	cmpl	$1, %r8d
	leal	-1(%r8), %edi
	movl	%edi, %r8d
	jg	.LBB5_3
.LBB5_14:                               # %._crit_edge44
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	null_convert, .Lfunc_end5-null_convert
	.cfi_endproc

	.p2align	4, 0x90
	.type	rgb_ycc_convert,@function
rgb_ycc_convert:                        # @rgb_ycc_convert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	testl	%r8d, %r8d
	jle	.LBB6_6
# BB#1:                                 # %.lr.ph71
	movl	40(%rdi), %eax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	testl	%eax, %eax
	je	.LBB6_6
# BB#2:                                 # %.lr.ph71.split.us.preheader
	movq	464(%rdi), %rax
	movq	16(%rax), %rdi
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph71.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	movq	%r8, -8(%rsp)           # 8-byte Spill
	leal	-1(%r8), %eax
	movl	%eax, -20(%rsp)         # 4-byte Spill
	movq	(%rsi), %rax
	movl	%ecx, %ebx
	movq	(%rdx), %rbp
	movq	8(%rdx), %r14
	movq	(%rbp,%rbx,8), %r11
	movq	(%r14,%rbx,8), %r14
	movq	16(%rdx), %rbp
	movq	(%rbp,%rbx,8), %r10
	movq	-16(%rsp), %r8          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_4:                                #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %r13d
	movzbl	1(%rax), %r15d
	movzbl	2(%rax), %r12d
	leal	256(%r15), %r9d
	movl	(%rdi,%r9,8), %ebx
	addl	(%rdi,%r13,8), %ebx
	leal	512(%r12), %ebp
	addl	(%rdi,%rbp,8), %ebx
	shrl	$16, %ebx
	movb	%bl, (%r11)
	leal	768(%r13), %ebp
	leal	1024(%r15), %ebx
	movl	(%rdi,%rbx,8), %ebx
	addl	(%rdi,%rbp,8), %ebx
	leal	1280(%r12), %ebp
	addl	(%rdi,%rbp,8), %ebx
	shrl	$16, %ebx
	movb	%bl, (%r14)
	leal	1280(%r13), %ebp
	movl	12288(%rdi,%r15,8), %ebx
	addl	(%rdi,%rbp,8), %ebx
	addl	14336(%rdi,%r12,8), %ebx
	shrl	$16, %ebx
	movb	%bl, (%r10)
	incq	%r11
	incq	%r14
	incq	%r10
	addq	$3, %rax
	decq	%r8
	jne	.LBB6_4
# BB#5:                                 # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB6_3 Depth=1
	addq	$8, %rsi
	incl	%ecx
	cmpl	$1, -8(%rsp)            # 4-byte Folded Reload
	movl	-20(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r8d
	jg	.LBB6_3
.LBB6_6:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	rgb_ycc_convert, .Lfunc_end6-rgb_ycc_convert
	.cfi_endproc

	.p2align	4, 0x90
	.type	cmyk_ycck_convert,@function
cmyk_ycck_convert:                      # @cmyk_ycck_convert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	testl	%r8d, %r8d
	jle	.LBB7_6
# BB#1:                                 # %.lr.ph78
	movl	40(%rdi), %eax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	testl	%eax, %eax
	je	.LBB7_6
# BB#2:                                 # %.lr.ph78.split.us.preheader
	movq	464(%rdi), %rax
	movq	16(%rax), %r11
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph78.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
	movq	%r8, -8(%rsp)           # 8-byte Spill
	leal	-1(%r8), %eax
	movl	%eax, -32(%rsp)         # 4-byte Spill
	movq	%rsi, %r8
	movq	(%rsi), %rax
	movl	%ecx, -28(%rsp)         # 4-byte Spill
	movl	%ecx, %ebp
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rbx
	movq	8(%rcx), %r9
	movq	(%rbx,%rbp,8), %rsi
	movq	(%r9,%rbp,8), %r14
	movq	16(%rcx), %rbx
	movq	(%rbx,%rbp,8), %r15
	movq	24(%rcx), %rbx
	movq	(%rbx,%rbp,8), %r13
	movq	-24(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB7_4:                                #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %r10d
	movl	%r10d, %ebx
	notb	%bl
	movzbl	%bl, %ebp
	movzbl	1(%rax), %r9d
	xorl	$255, %r9d
	movzbl	2(%rax), %ebx
	xorl	$255, %ebx
	movzbl	3(%rax), %r12d
	movb	%r12b, (%r13)
	xorq	$255, %r10
	leal	256(%r9), %edi
	movl	(%r11,%rdi,8), %edi
	addl	(%r11,%r10,8), %edi
	leal	512(%rbx), %edx
	addl	(%r11,%rdx,8), %edi
	shrl	$16, %edi
	movb	%dil, (%rsi)
	leal	768(%rbp), %edx
	leal	1024(%r9), %edi
	movl	(%r11,%rdi,8), %edi
	addl	(%r11,%rdx,8), %edi
	leal	1280(%rbx), %edx
	addl	(%r11,%rdx,8), %edi
	shrl	$16, %edi
	movb	%dil, (%r14)
	orl	$1280, %ebp             # imm = 0x500
	orl	$1536, %r9d             # imm = 0x600
	movl	(%r11,%r9,8), %edx
	addl	(%r11,%rbp,8), %edx
	orl	$1792, %ebx             # imm = 0x700
	addl	(%r11,%rbx,8), %edx
	shrl	$16, %edx
	movb	%dl, (%r15)
	incq	%r13
	incq	%rsi
	incq	%r14
	incq	%r15
	addq	$4, %rax
	decq	%rcx
	jne	.LBB7_4
# BB#5:                                 # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	%r8, %rsi
	addq	$8, %rsi
	movl	-28(%rsp), %ecx         # 4-byte Reload
	incl	%ecx
	cmpl	$1, -8(%rsp)            # 4-byte Folded Reload
	movl	-32(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r8d
	jg	.LBB7_3
.LBB7_6:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	cmyk_ycck_convert, .Lfunc_end7-cmyk_ycck_convert
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
