	.text
	.file	"btActivatingCollisionAlgorithm.bc"
	.globl	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	.p2align	4, 0x90
	.type	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo,@function
_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo: # @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	movq	$_ZTV30btActivatingCollisionAlgorithm+16, (%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo, .Lfunc_end0-_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	.cfi_endproc

	.globl	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	movq	$_ZTV30btActivatingCollisionAlgorithm+16, (%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end1-_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.globl	_ZN30btActivatingCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN30btActivatingCollisionAlgorithmD2Ev,@function
_ZN30btActivatingCollisionAlgorithmD2Ev: # @_ZN30btActivatingCollisionAlgorithmD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	_ZN30btActivatingCollisionAlgorithmD2Ev, .Lfunc_end2-_ZN30btActivatingCollisionAlgorithmD2Ev
	.cfi_endproc

	.globl	_ZN30btActivatingCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN30btActivatingCollisionAlgorithmD0Ev,@function
_ZN30btActivatingCollisionAlgorithmD0Ev: # @_ZN30btActivatingCollisionAlgorithmD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end3:
	.size	_ZN30btActivatingCollisionAlgorithmD0Ev, .Lfunc_end3-_ZN30btActivatingCollisionAlgorithmD0Ev
	.cfi_endproc

	.type	_ZTV30btActivatingCollisionAlgorithm,@object # @_ZTV30btActivatingCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV30btActivatingCollisionAlgorithm
	.p2align	3
_ZTV30btActivatingCollisionAlgorithm:
	.quad	0
	.quad	_ZTI30btActivatingCollisionAlgorithm
	.quad	_ZN30btActivatingCollisionAlgorithmD2Ev
	.quad	_ZN30btActivatingCollisionAlgorithmD0Ev
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV30btActivatingCollisionAlgorithm, 56

	.type	_ZTS30btActivatingCollisionAlgorithm,@object # @_ZTS30btActivatingCollisionAlgorithm
	.globl	_ZTS30btActivatingCollisionAlgorithm
	.p2align	4
_ZTS30btActivatingCollisionAlgorithm:
	.asciz	"30btActivatingCollisionAlgorithm"
	.size	_ZTS30btActivatingCollisionAlgorithm, 33

	.type	_ZTS20btCollisionAlgorithm,@object # @_ZTS20btCollisionAlgorithm
	.section	.rodata._ZTS20btCollisionAlgorithm,"aG",@progbits,_ZTS20btCollisionAlgorithm,comdat
	.weak	_ZTS20btCollisionAlgorithm
	.p2align	4
_ZTS20btCollisionAlgorithm:
	.asciz	"20btCollisionAlgorithm"
	.size	_ZTS20btCollisionAlgorithm, 23

	.type	_ZTI20btCollisionAlgorithm,@object # @_ZTI20btCollisionAlgorithm
	.section	.rodata._ZTI20btCollisionAlgorithm,"aG",@progbits,_ZTI20btCollisionAlgorithm,comdat
	.weak	_ZTI20btCollisionAlgorithm
	.p2align	3
_ZTI20btCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS20btCollisionAlgorithm
	.size	_ZTI20btCollisionAlgorithm, 16

	.type	_ZTI30btActivatingCollisionAlgorithm,@object # @_ZTI30btActivatingCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTI30btActivatingCollisionAlgorithm
	.p2align	4
_ZTI30btActivatingCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30btActivatingCollisionAlgorithm
	.quad	_ZTI20btCollisionAlgorithm
	.size	_ZTI30btActivatingCollisionAlgorithm, 24


	.globl	_ZN30btActivatingCollisionAlgorithmD1Ev
	.type	_ZN30btActivatingCollisionAlgorithmD1Ev,@function
_ZN30btActivatingCollisionAlgorithmD1Ev = _ZN30btActivatingCollisionAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
