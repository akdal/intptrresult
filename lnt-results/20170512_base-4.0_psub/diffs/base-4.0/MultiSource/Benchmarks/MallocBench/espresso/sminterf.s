	.text
	.file	"sminterf.bc"
	.globl	do_sm_minimum_cover
	.p2align	4, 0x90
	.type	do_sm_minimum_cover,@function
do_sm_minimum_cover:                    # @do_sm_minimum_cover
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%eax, %eax
	callq	sm_alloc
	movq	%rax, %r12
	movslq	(%rbx), %rcx
	movq	%rbx, (%rsp)            # 8-byte Spill
	movslq	12(%rbx), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB0_11
# BB#1:                                 # %.lr.ph71.preheader
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	24(%rcx), %rcx
	leaq	(%rcx,%rax,4), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph71
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
                                        #       Child Loop BB0_7 Depth 3
	movl	(%rcx), %r13d
	testw	$1023, %r13w            # imm = 0x3FF
	je	.LBB0_10
# BB#3:                                 # %.lr.ph66.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	andl	$1023, %r13d            # imm = 0x3FF
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph66
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_7 Depth 3
	movq	%r13, %r14
	movl	(%rcx,%r14,4), %r15d
	leaq	-1(%r14), %r13
	testl	%r15d, %r15d
	je	.LBB0_4
# BB#6:                                 # %.lr.ph63.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	%r13d, %ebx
	shll	$5, %ebx
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph63
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$1, %r15b
	je	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=3
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	sm_insert
.LBB0_9:                                #   in Loop: Header=BB0_7 Depth=3
	incl	%ebx
	shrl	%r15d
	jne	.LBB0_7
.LBB0_4:                                # %.loopexit
                                        #   in Loop: Header=BB0_5 Depth=2
	cmpq	$2, %r14
	movq	16(%rsp), %rcx          # 8-byte Reload
	jge	.LBB0_5
.LBB0_10:                               # %._crit_edge67
                                        #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	movq	(%rsp), %rax            # 8-byte Reload
	movslq	(%rax), %rax
	leaq	(%rcx,%rax,4), %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	jb	.LBB0_2
.LBB0_11:                               # %._crit_edge72
	xorl	%esi, %esi
	movl	$1, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_minimum_cover
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_free
	movq	(%rsp), %rax            # 8-byte Reload
	movl	4(%rax), %ebx
	cmpl	$33, %ebx
	jge	.LBB0_13
# BB#12:
	movl	$8, %edi
	jmp	.LBB0_14
.LBB0_13:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_14:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %rbx
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_17
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rax), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%edx, 4(%rbx,%rcx,4)
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_15
.LBB0_17:                               # %._crit_edge
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sm_row_free
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	do_sm_minimum_cover, .Lfunc_end0-do_sm_minimum_cover
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
