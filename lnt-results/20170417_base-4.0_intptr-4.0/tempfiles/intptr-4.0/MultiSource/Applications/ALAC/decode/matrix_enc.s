	.text
	.file	"matrix_enc.bc"
	.globl	mix16
	.p2align	4, 0x90
	.type	mix16,@function
mix16:                                  # @mix16
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%r9d, %r11d
	movq	%rcx, %r9
	movl	48(%rsp), %r10d
	testl	%r10d, %r10d
	je	.LBB0_1
# BB#8:
	movl	$1, %r15d
	movl	%r11d, %ecx
	shll	%cl, %r15d
	testl	%r8d, %r8d
	jle	.LBB0_15
# BB#9:                                 # %.lr.ph51
	subl	%r10d, %r15d
	movl	%esi, %r12d
	movl	%r8d, %ebx
	testb	$1, %bl
	jne	.LBB0_11
# BB#10:
	xorl	%eax, %eax
	cmpl	$1, %r8d
	jne	.LBB0_13
	jmp	.LBB0_15
.LBB0_1:                                # %.preheader
	testl	%r8d, %r8d
	jle	.LBB0_15
# BB#2:                                 # %.lr.ph
	movl	%esi, %eax
	movl	%r8d, %ecx
	testb	$1, %cl
	jne	.LBB0_4
# BB#3:
	xorl	%esi, %esi
	cmpl	$1, %r8d
	jne	.LBB0_6
	jmp	.LBB0_15
.LBB0_11:
	movswl	(%rdi), %r14d
	movswl	2(%rdi), %eax
	leaq	(%rdi,%r12,2), %rdi
	movl	%r14d, %ecx
	imull	%r10d, %ecx
	subl	%eax, %r14d
	imull	%r15d, %eax
	addl	%ecx, %eax
	movl	%r11d, %ecx
	sarl	%cl, %eax
	movl	%eax, (%rdx)
	movl	%r14d, (%r9)
	movl	$1, %eax
	cmpl	$1, %r8d
	je	.LBB0_15
.LBB0_13:                               # %.lr.ph51.new
	subq	%rax, %rbx
	leaq	4(%rdx,%rax,4), %rdx
	leaq	4(%r9,%rax,4), %rax
	addq	%r12, %r12
	.p2align	4, 0x90
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	movswl	(%rdi), %ebp
	movswl	2(%rdi), %esi
	movl	%ebp, %ecx
	imull	%r10d, %ecx
	subl	%esi, %ebp
	imull	%r15d, %esi
	addl	%ecx, %esi
	movl	%r11d, %ecx
	sarl	%cl, %esi
	movl	%esi, -4(%rdx)
	movl	%ebp, -4(%rax)
	movswl	(%rdi,%r12), %esi
	movswl	2(%rdi,%r12), %ebp
	leaq	(%rdi,%r12), %rdi
	movl	%esi, %ecx
	imull	%r10d, %ecx
	subl	%ebp, %esi
	imull	%r15d, %ebp
	addl	%ecx, %ebp
	movl	%r11d, %ecx
	sarl	%cl, %ebp
	movl	%ebp, (%rdx)
	movl	%esi, (%rax)
	addq	$8, %rdx
	addq	$8, %rax
	addq	%r12, %rdi
	addq	$-2, %rbx
	jne	.LBB0_14
	jmp	.LBB0_15
.LBB0_4:
	movswl	(%rdi), %esi
	movl	%esi, (%rdx)
	movswl	2(%rdi), %esi
	movl	%esi, (%r9)
	leaq	(%rdi,%rax,2), %rdi
	movl	$1, %esi
	cmpl	$1, %r8d
	je	.LBB0_15
.LBB0_6:                                # %.lr.ph.new
	subq	%rsi, %rcx
	leaq	4(%rdx,%rsi,4), %rdx
	leaq	4(%r9,%rsi,4), %rsi
	addq	%rax, %rax
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movswl	(%rdi), %ebp
	movl	%ebp, -4(%rdx)
	movswl	2(%rdi), %ebp
	movl	%ebp, -4(%rsi)
	movswl	(%rdi,%rax), %ebp
	movl	%ebp, (%rdx)
	movswl	2(%rdi,%rax), %ebp
	leaq	(%rdi,%rax), %rdi
	movl	%ebp, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	%rax, %rdi
	addq	$-2, %rcx
	jne	.LBB0_7
.LBB0_15:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	mix16, .Lfunc_end0-mix16
	.cfi_endproc

	.globl	mix20
	.p2align	4, 0x90
	.type	mix20,@function
mix20:                                  # @mix20
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rcx, %r15
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	32(%rsp), %r10d
	testl	%r10d, %r10d
	je	.LBB1_1
# BB#4:
	movl	$1, %r11d
	movl	%r9d, %ecx
	shll	%cl, %r11d
	testl	%r8d, %r8d
	jle	.LBB1_7
# BB#5:                                 # %.lr.ph65
	subl	%r10d, %r11d
	leal	-3(%rsi,%rsi,2), %r14d
	movl	%r8d, %esi
	addq	$5, %rdi
	addq	$3, %r14
	.p2align	4, 0x90
.LBB1_6:                                # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rdi), %ecx
	shll	$16, %ecx
	movzbl	-4(%rdi), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	-5(%rdi), %ebx
	orl	%eax, %ebx
	shll	$8, %ebx
	sarl	$12, %ebx
	movzbl	(%rdi), %eax
	shll	$16, %eax
	movzbl	-1(%rdi), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movzbl	-2(%rdi), %eax
	orl	%ecx, %eax
	shll	$8, %eax
	sarl	$12, %eax
	movl	%ebx, %ecx
	imull	%r10d, %ecx
	subl	%eax, %ebx
	imull	%r11d, %eax
	addl	%ecx, %eax
	movl	%r9d, %ecx
	sarl	%cl, %eax
	movl	%eax, (%rdx)
	movl	%ebx, (%r15)
	addq	$4, %rdx
	addq	$4, %r15
	addq	%r14, %rdi
	decq	%rsi
	jne	.LBB1_6
	jmp	.LBB1_7
.LBB1_1:                                # %.preheader
	testl	%r8d, %r8d
	jle	.LBB1_7
# BB#2:                                 # %.lr.ph
	leal	-3(%rsi,%rsi,2), %ecx
	movl	%r8d, %esi
	addq	$5, %rdi
	addq	$3, %rcx
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rdi), %eax
	shll	$16, %eax
	movzbl	-4(%rdi), %ebx
	shll	$8, %ebx
	orl	%eax, %ebx
	movzbl	-5(%rdi), %eax
	orl	%ebx, %eax
	shll	$8, %eax
	sarl	$12, %eax
	movl	%eax, (%rdx)
	movzbl	(%rdi), %eax
	shll	$16, %eax
	movzbl	-1(%rdi), %ebx
	shll	$8, %ebx
	orl	%eax, %ebx
	movzbl	-2(%rdi), %eax
	orl	%ebx, %eax
	shll	$8, %eax
	sarl	$12, %eax
	movl	%eax, (%r15)
	addq	$4, %rdx
	addq	$4, %r15
	addq	%rcx, %rdi
	decq	%rsi
	jne	.LBB1_3
.LBB1_7:                                # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	mix20, .Lfunc_end1-mix20
	.cfi_endproc

	.globl	mix24
	.p2align	4, 0x90
	.type	mix24,@function
mix24:                                  # @mix24
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rcx, %r11
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	72(%rsp), %ebx
	movq	64(%rsp), %rax
	movl	56(%rsp), %r14d
	leal	(,%rbx,8), %r10d
	movl	$1, %r13d
	movl	%r10d, %ecx
	shlq	%cl, %r13
	decl	%r13d
	testl	%r14d, %r14d
	je	.LBB2_8
# BB#1:
	movl	$1, %r12d
	movl	%r9d, %ecx
	shll	%cl, %r12d
	subl	%r14d, %r12d
	testl	%ebx, %ebx
	je	.LBB2_5
# BB#2:                                 # %.preheader149
	testl	%r8d, %r8d
	jle	.LBB2_15
# BB#3:                                 # %.lr.ph163
	leal	-3(%rsi,%rsi,2), %r15d
	movl	%r8d, %r8d
	addq	$2, %rax
	addq	$5, %rdi
	addq	$3, %r15
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rdi), %ecx
	shll	$16, %ecx
	movzbl	-4(%rdi), %ebx
	shll	$8, %ebx
	orl	%ecx, %ebx
	movzbl	-5(%rdi), %ebp
	orl	%ebx, %ebp
	shll	$8, %ebp
	sarl	$8, %ebp
	movzbl	(%rdi), %ecx
	shll	$16, %ecx
	movzbl	-1(%rdi), %esi
	shll	$8, %esi
	orl	%ecx, %esi
	movzbl	-2(%rdi), %ebx
	orl	%esi, %ebx
	shll	$8, %ebx
	sarl	$8, %ebx
	movl	%ebp, %ecx
	andl	%r13d, %ecx
	movw	%cx, -2(%rax)
	movl	%ebx, %ecx
	andl	%r13d, %ecx
	movw	%cx, (%rax)
	movl	%r10d, %ecx
	sarl	%cl, %ebp
	movl	%r10d, %ecx
	sarl	%cl, %ebx
	movl	%ebp, %ecx
	imull	%r14d, %ecx
	subl	%ebx, %ebp
	imull	%r12d, %ebx
	addl	%ecx, %ebx
	movl	%r9d, %ecx
	sarl	%cl, %ebx
	movl	%ebx, (%rdx)
	movl	%ebp, (%r11)
	addq	$4, %rdx
	addq	$4, %r11
	addq	$4, %rax
	addq	%r15, %rdi
	decq	%r8
	jne	.LBB2_4
	jmp	.LBB2_15
.LBB2_8:
	testl	%ebx, %ebx
	je	.LBB2_12
# BB#9:                                 # %.preheader145
	testl	%r8d, %r8d
	jle	.LBB2_15
# BB#10:                                # %.lr.ph156
	leal	-3(%rsi,%rsi,2), %r9d
	movl	%r8d, %r8d
	addq	$2, %rax
	addq	$5, %rdi
	addq	$3, %r9
	.p2align	4, 0x90
.LBB2_11:                               # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rdi), %ecx
	shll	$16, %ecx
	movzbl	-4(%rdi), %ebx
	shll	$8, %ebx
	orl	%ecx, %ebx
	movzbl	-5(%rdi), %esi
	orl	%ebx, %esi
	shll	$8, %esi
	sarl	$8, %esi
	movzbl	(%rdi), %ecx
	shll	$16, %ecx
	movzbl	-1(%rdi), %ebx
	shll	$8, %ebx
	orl	%ecx, %ebx
	movzbl	-2(%rdi), %ebp
	orl	%ebx, %ebp
	shll	$8, %ebp
	sarl	$8, %ebp
	movl	%esi, %ecx
	andl	%r13d, %ecx
	movw	%cx, -2(%rax)
	movl	%ebp, %ecx
	andl	%r13d, %ecx
	movw	%cx, (%rax)
	movl	%r10d, %ecx
	sarl	%cl, %esi
	movl	%r10d, %ecx
	sarl	%cl, %ebp
	movl	%esi, (%rdx)
	movl	%ebp, (%r11)
	addq	$4, %rdx
	addq	$4, %r11
	addq	$4, %rax
	addq	%r9, %rdi
	decq	%r8
	jne	.LBB2_11
	jmp	.LBB2_15
.LBB2_5:                                # %.preheader147
	testl	%r8d, %r8d
	jle	.LBB2_15
# BB#6:                                 # %.lr.ph159
	leal	-3(%rsi,%rsi,2), %r10d
	movl	%r8d, %esi
	addq	$5, %rdi
	addq	$3, %r10
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rdi), %ecx
	shll	$16, %ecx
	movzbl	-4(%rdi), %ebx
	shll	$8, %ebx
	orl	%ecx, %ebx
	movzbl	-5(%rdi), %ebp
	orl	%ebx, %ebp
	shll	$8, %ebp
	sarl	$8, %ebp
	movzbl	(%rdi), %ecx
	shll	$16, %ecx
	movzbl	-1(%rdi), %ebx
	shll	$8, %ebx
	orl	%ecx, %ebx
	movzbl	-2(%rdi), %eax
	orl	%ebx, %eax
	shll	$8, %eax
	sarl	$8, %eax
	movl	%ebp, %ecx
	imull	%r14d, %ecx
	subl	%eax, %ebp
	imull	%r12d, %eax
	addl	%ecx, %eax
	movl	%r9d, %ecx
	sarl	%cl, %eax
	movl	%eax, (%rdx)
	movl	%ebp, (%r11)
	addq	$4, %rdx
	addq	$4, %r11
	addq	%r10, %rdi
	decq	%rsi
	jne	.LBB2_7
	jmp	.LBB2_15
.LBB2_12:                               # %.preheader
	testl	%r8d, %r8d
	jle	.LBB2_15
# BB#13:                                # %.lr.ph
	leal	-3(%rsi,%rsi,2), %eax
	movl	%r8d, %ecx
	addq	$5, %rdi
	addq	$3, %rax
	.p2align	4, 0x90
.LBB2_14:                               # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rdi), %esi
	shll	$16, %esi
	movzbl	-4(%rdi), %ebp
	shll	$8, %ebp
	orl	%esi, %ebp
	movzbl	-5(%rdi), %esi
	orl	%ebp, %esi
	shll	$8, %esi
	sarl	$8, %esi
	movl	%esi, (%rdx)
	movzbl	(%rdi), %esi
	shll	$16, %esi
	movzbl	-1(%rdi), %ebp
	shll	$8, %ebp
	orl	%esi, %ebp
	movzbl	-2(%rdi), %esi
	orl	%ebp, %esi
	shll	$8, %esi
	sarl	$8, %esi
	movl	%esi, (%r11)
	addq	$4, %rdx
	addq	$4, %r11
	addq	%rax, %rdi
	decq	%rcx
	jne	.LBB2_14
.LBB2_15:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	mix24, .Lfunc_end2-mix24
	.cfi_endproc

	.globl	mix32
	.p2align	4, 0x90
	.type	mix32,@function
mix32:                                  # @mix32
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movl	72(%rsp), %ebp
	movq	64(%rsp), %rbx
	movl	56(%rsp), %r14d
	leal	(,%rbp,8), %r10d
	movl	$1, %r11d
	movl	%r10d, %ecx
	shlq	%cl, %r11
	decl	%r11d
	testl	%r14d, %r14d
	je	.LBB3_4
# BB#1:
	movl	$1, %r12d
	movl	%r9d, %ecx
	shll	%cl, %r12d
	testl	%r8d, %r8d
	jle	.LBB3_20
# BB#2:                                 # %.lr.ph109
	subl	%r14d, %r12d
	movl	%esi, %r15d
	movl	%r8d, %esi
	addq	$2, %rbx
	addq	$4, %rdi
	shlq	$2, %r15
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi), %ebp
	movl	(%rdi), %eax
	movl	%ebp, %ecx
	andl	%r11d, %ecx
	movw	%cx, -2(%rbx)
	movl	%eax, %ecx
	andl	%r11d, %ecx
	movw	%cx, (%rbx)
	movl	%r10d, %ecx
	sarl	%cl, %ebp
	movl	%r10d, %ecx
	sarl	%cl, %eax
	movl	%ebp, %ecx
	imull	%r14d, %ecx
	subl	%eax, %ebp
	imull	%r12d, %eax
	addl	%ecx, %eax
	movl	%r9d, %ecx
	sarl	%cl, %eax
	movl	%eax, (%rdx)
	movl	%ebp, (%r13)
	addq	$4, %rdx
	addq	$4, %r13
	addq	$4, %rbx
	addq	%r15, %rdi
	decq	%rsi
	jne	.LBB3_3
.LBB3_20:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_4:
	testl	%ebp, %ebp
	je	.LBB3_12
# BB#5:                                 # %.preheader97
	testl	%r8d, %r8d
	jle	.LBB3_20
# BB#6:                                 # %.lr.ph105
	movl	%esi, %r9d
	movl	%r8d, %r14d
	testb	$1, %r14b
	jne	.LBB3_8
# BB#7:
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	cmpl	$1, %r8d
	je	.LBB3_20
	jmp	.LBB3_10
.LBB3_12:                               # %.preheader
	testl	%r8d, %r8d
	jle	.LBB3_20
# BB#13:                                # %.lr.ph
	movl	%esi, %ecx
	movl	%r8d, %esi
	leaq	-1(%rsi), %r8
	movq	%rsi, %rbp
	andq	$3, %rbp
	je	.LBB3_14
# BB#15:                                # %.prol.preheader
	leaq	(,%rcx,4), %r9
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_16:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %eax
	movl	%eax, (%rdx,%rbx,4)
	movl	4(%rdi), %eax
	movl	%eax, (%r13,%rbx,4)
	incq	%rbx
	addq	%r9, %rdi
	cmpq	%rbx, %rbp
	jne	.LBB3_16
	jmp	.LBB3_17
.LBB3_8:
	movl	(%rdi), %eax
	movl	4(%rdi), %ebp
	leaq	(%rdi,%r9,4), %rdi
	movl	%eax, %ecx
	andl	%r11d, %ecx
	movw	%cx, (%rbx)
	movl	%ebp, %ecx
	andl	%r11d, %ecx
	movw	%cx, 2(%rbx)
	movl	%r10d, %ecx
	sarl	%cl, %eax
	movl	%r10d, %ecx
	sarl	%cl, %ebp
	movl	%eax, (%rdx)
	movl	%ebp, (%r13)
	movl	$2, %ebp
	movl	$1, %ecx
	cmpl	$1, %r8d
	je	.LBB3_20
.LBB3_10:                               # %.lr.ph105.new
	subq	%rcx, %r14
	leaq	4(%rdx,%rcx,4), %rdx
	leaq	4(%r13,%rcx,4), %rax
	leaq	6(%rbx,%rbp,2), %rbp
	shlq	$2, %r9
	.p2align	4, 0x90
.LBB3_11:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %ebx
	movl	4(%rdi), %esi
	movl	%ebx, %ecx
	andl	%r11d, %ecx
	movw	%cx, -6(%rbp)
	movl	%esi, %ecx
	andl	%r11d, %ecx
	movw	%cx, -4(%rbp)
	movl	%r10d, %ecx
	sarl	%cl, %ebx
	movl	%r10d, %ecx
	sarl	%cl, %esi
	movl	%ebx, -4(%rdx)
	movl	%esi, -4(%rax)
	movl	(%rdi,%r9), %esi
	movl	4(%rdi,%r9), %ebx
	leaq	(%rdi,%r9), %rdi
	movl	%esi, %ecx
	andl	%r11d, %ecx
	movw	%cx, -2(%rbp)
	movl	%ebx, %ecx
	andl	%r11d, %ecx
	movw	%cx, (%rbp)
	movl	%r10d, %ecx
	sarl	%cl, %esi
	movl	%r10d, %ecx
	sarl	%cl, %ebx
	movl	%esi, (%rdx)
	movl	%ebx, (%rax)
	addq	$8, %rdx
	addq	$8, %rax
	addq	$8, %rbp
	addq	%r9, %rdi
	addq	$-2, %r14
	jne	.LBB3_11
	jmp	.LBB3_20
.LBB3_14:
	xorl	%ebx, %ebx
.LBB3_17:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_20
# BB#18:                                # %.lr.ph.new
	subq	%rbx, %rsi
	leaq	12(%r13,%rbx,4), %rax
	leaq	12(%rdx,%rbx,4), %rdx
	shlq	$2, %rcx
	.p2align	4, 0x90
.LBB3_19:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %ebp
	movl	%ebp, -12(%rdx)
	movl	4(%rdi), %ebp
	movl	%ebp, -12(%rax)
	movl	(%rdi,%rcx), %ebp
	movl	%ebp, -8(%rdx)
	movl	4(%rdi,%rcx), %ebp
	leaq	(%rdi,%rcx), %rdi
	movl	%ebp, -8(%rax)
	movl	(%rcx,%rdi), %ebp
	movl	%ebp, -4(%rdx)
	movl	4(%rcx,%rdi), %ebp
	leaq	(%rdi,%rcx), %rdi
	movl	%ebp, -4(%rax)
	movl	(%rcx,%rdi), %ebp
	movl	%ebp, (%rdx)
	movl	4(%rcx,%rdi), %ebp
	leaq	(%rdi,%rcx), %rdi
	movl	%ebp, (%rax)
	addq	$16, %rax
	addq	$16, %rdx
	addq	%rcx, %rdi
	addq	$-4, %rsi
	jne	.LBB3_19
	jmp	.LBB3_20
.Lfunc_end3:
	.size	mix32, .Lfunc_end3-mix32
	.cfi_endproc

	.globl	copy20ToPredictor
	.p2align	4, 0x90
	.type	copy20ToPredictor,@function
copy20ToPredictor:                      # @copy20ToPredictor
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%ecx, %ecx
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph
	leal	(%rsi,%rsi,2), %r8d
	movl	%ecx, %ecx
	addq	$2, %rdi
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %esi
	shll	$16, %esi
	movzbl	-1(%rdi), %eax
	shll	$8, %eax
	orl	%esi, %eax
	movzbl	-2(%rdi), %esi
	orl	%eax, %esi
	shll	$8, %esi
	sarl	$12, %esi
	movl	%esi, (%rdx)
	addq	$4, %rdx
	addq	%r8, %rdi
	decq	%rcx
	jne	.LBB4_2
.LBB4_3:                                # %._crit_edge
	retq
.Lfunc_end4:
	.size	copy20ToPredictor, .Lfunc_end4-copy20ToPredictor
	.cfi_endproc

	.globl	copy24ToPredictor
	.p2align	4, 0x90
	.type	copy24ToPredictor,@function
copy24ToPredictor:                      # @copy24ToPredictor
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%ecx, %ecx
	jle	.LBB5_3
# BB#1:                                 # %.lr.ph
	leal	(%rsi,%rsi,2), %r8d
	movl	%ecx, %ecx
	addq	$2, %rdi
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %esi
	shll	$16, %esi
	movzbl	-1(%rdi), %eax
	shll	$8, %eax
	orl	%esi, %eax
	movzbl	-2(%rdi), %esi
	orl	%eax, %esi
	shll	$8, %esi
	sarl	$8, %esi
	movl	%esi, (%rdx)
	addq	$4, %rdx
	addq	%r8, %rdi
	decq	%rcx
	jne	.LBB5_2
.LBB5_3:                                # %._crit_edge
	retq
.Lfunc_end5:
	.size	copy24ToPredictor, .Lfunc_end5-copy24ToPredictor
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
