	.text
	.file	"FileDir.bc"
	.globl	_Z17nameWindowToUnix2PKw
	.p2align	4, 0x90
	.type	_Z17nameWindowToUnix2PKw,@function
_Z17nameWindowToUnix2PKw:               # @_Z17nameWindowToUnix2PKw
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 80
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB0_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%r12), %eax
	movslq	%eax, %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	%r15d, 12(%rsp)
	.p2align	4, 0x90
.LBB0_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%r12d, 8(%rsp)
.Ltmp0:
	leaq	16(%rsp), %rdi
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp1:
# BB#5:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_7
# BB#6:
	callq	_ZdaPv
.LBB0_7:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	16(%rsp), %r15
	cmpb	$99, (%r15)
	movq	%r15, %rbx
	jne	.LBB0_9
# BB#8:
	leaq	2(%r15), %rbx
	cmpb	$58, 1(%r15)
	cmovneq	%r15, %rbx
.LBB0_9:                                # %_ZL16nameWindowToUnixPKc.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB0_10:                               # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_10
# BB#11:                                # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	1(%r12), %ebp
	movslq	%ebp, %rax
	cmpl	$-1, %r12d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp3:
	callq	_Znam
.Ltmp4:
# BB#12:                                # %.noexc
	movq	%rax, (%r14)
	movb	$0, (%rax)
	movl	%ebp, 12(%r14)
	.p2align	4, 0x90
.LBB0_13:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB0_13
# BB#14:
	movl	%r12d, 8(%r14)
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_16
# BB#15:
	callq	_ZdaPv
.LBB0_16:                               # %_ZN11CStringBaseIcED2Ev.exit8
	movq	%r14, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_18:
.Ltmp5:
	movq	%rax, %r14
	testq	%r15, %r15
	je	.LBB0_21
# BB#19:
	movq	%r15, %rdi
	jmp	.LBB0_20
.LBB0_17:
.Ltmp2:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_21
.LBB0_20:
	callq	_ZdaPv
.LBB0_21:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z17nameWindowToUnix2PKw, .Lfunc_end0-_Z17nameWindowToUnix2PKw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_Z15GetFullPathNamePKwjPwPS1_
	.p2align	4, 0x90
	.type	_Z15GetFullPathNamePKwjPwPS1_,@function
_Z15GetFullPathNamePKwjPwPS1_:          # @_Z15GetFullPathNamePKwjPwPS1_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	subq	$1096, %rsp             # imm = 0x448
.Lcfi17:
	.cfi_def_cfa_offset 1152
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movl	%esi, %r13d
	movq	%rdi, %r15
	xorl	%ebp, %ebp
	testq	%r15, %r15
	je	.LBB2_41
# BB#1:
	movq	%r15, %rdi
	callq	wcslen
	movq	%rax, %r12
	movl	(%r15), %eax
	cmpl	$47, %eax
	jne	.LBB2_9
# BB#2:
	addl	$2, %r12d
	cmpl	%r13d, %r12d
	jae	.LBB2_41
# BB#3:
	movl	$.L.str, %esi
	movq	%rbx, %rdi
	callq	wcscpy
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	wcscat
	jmp	.LBB2_4
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_4 Depth=1
	addq	$4, %rbx
.LBB2_4:                                # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
	movq	%rbx, (%r14)
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_7:                                # %._crit_edge87
                                        #   in Loop: Header=BB2_5 Depth=2
	addq	$4, %rbx
.LBB2_5:                                # %.backedge
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %eax
	cmpl	$47, %eax
	je	.LBB2_8
# BB#6:                                 # %.backedge
                                        #   in Loop: Header=BB2_5 Depth=2
	testl	%eax, %eax
	jne	.LBB2_7
.LBB2_40:                               # %.loopexit.loopexit96
	movl	%r12d, %ebp
	jmp	.LBB2_41
.LBB2_9:
	cmpl	$127, %eax
	ja	.LBB2_18
# BB#10:
	cmpl	$58, 4(%r15)
	jne	.LBB2_18
# BB#11:
	cmpl	%r13d, %r12d
	jae	.LBB2_41
# BB#12:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	wcscpy
	jmp	.LBB2_13
	.p2align	4, 0x90
.LBB2_17:                               #   in Loop: Header=BB2_13 Depth=1
	addq	$4, %rbx
.LBB2_13:                               # %.backedge93
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_14 Depth 2
	movq	%rbx, (%r14)
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_16:                               # %._crit_edge86
                                        #   in Loop: Header=BB2_14 Depth=2
	addq	$4, %rbx
.LBB2_14:                               # %.backedge93
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %eax
	cmpl	$47, %eax
	je	.LBB2_17
# BB#15:                                # %.backedge93
                                        #   in Loop: Header=BB2_14 Depth=2
	testl	%eax, %eax
	jne	.LBB2_16
	jmp	.LBB2_40
.LBB2_18:
	cmpl	$2, %r13d
	jb	.LBB2_41
# BB#19:
	movw	$14947, 64(%rsp)        # imm = 0x3A63
	leaq	66(%rsp), %rdi
	movl	$1021, %esi             # imm = 0x3FD
	callq	getcwd
	xorl	%ebp, %ebp
	testq	%rax, %rax
	je	.LBB2_41
# BB#20:
	movq	%r12, 8(%rsp)           # 8-byte Spill
	leaq	64(%rsp), %r12
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rcx
	testl	%ecx, %ecx
	je	.LBB2_41
# BB#21:
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	1(%rax,%rcx), %eax
	cmpl	%r13d, %eax
	jae	.LBB2_41
# BB#22:
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%eax, 8(%rsp)           # 4-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %r13d
	.p2align	4, 0x90
.LBB2_23:                               # =>This Inner Loop Header: Depth=1
	incl	%r13d
	cmpb	$0, (%r12)
	leaq	1(%r12), %r12
	jne	.LBB2_23
# BB#24:                                # %_Z11MyStringLenIcEiPKT_.exit.i
	leaq	64(%rsp), %rbp
	leal	1(%r13), %eax
	movslq	%eax, %r12
	cmpl	$-1, %r13d
	movq	$-1, %rdi
	cmovgeq	%r12, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	%r12d, 28(%rsp)
	.p2align	4, 0x90
.LBB2_25:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %ecx
	incq	%rbp
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB2_25
# BB#26:                                # %_ZN11CStringBaseIcEC2EPKc.exit
	movl	%r13d, 24(%rsp)
.Ltmp6:
	leaq	48(%rsp), %rdi
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp7:
# BB#27:                                # %_Z16GetUnicodeStringRK11CStringBaseIcE.exit
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_29
# BB#28:
	callq	_ZdaPv
.LBB2_29:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	48(%rsp), %rsi
	movq	%rbx, %rdi
	callq	wcscpy
	movl	$.L.str.1, %esi
	movq	%rbx, %rdi
	callq	wcscat
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	wcscat
	movl	40(%rsp), %eax          # 4-byte Reload
	leaq	4(%rbx,%rax,4), %rax
	movq	%rax, (%r14)
	jmp	.LBB2_30
	.p2align	4, 0x90
.LBB2_33:                               #   in Loop: Header=BB2_30 Depth=1
	addq	$4, %rbx
	movq	%rbx, (%r14)
.LBB2_30:                               # %.backedge94
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	cmpl	$47, %eax
	je	.LBB2_33
# BB#31:                                # %.backedge94
                                        #   in Loop: Header=BB2_30 Depth=1
	testl	%eax, %eax
	je	.LBB2_37
# BB#32:                                # %._crit_edge
                                        #   in Loop: Header=BB2_30 Depth=1
	addq	$4, %rbx
	jmp	.LBB2_30
.LBB2_37:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_39
# BB#38:
	callq	_ZdaPv
.LBB2_39:                               # %_ZN11CStringBaseIwED2Ev.exit
	movl	8(%rsp), %ebp           # 4-byte Reload
.LBB2_41:                               # %.loopexit
	movl	%ebp, %eax
	addq	$1096, %rsp             # imm = 0x448
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_34:
.Ltmp8:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_36
# BB#35:
	callq	_ZdaPv
.LBB2_36:                               # %_ZN11CStringBaseIcED2Ev.exit83
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_Z15GetFullPathNamePKwjPwPS1_, .Lfunc_end2-_Z15GetFullPathNamePKwjPwPS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp6-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory21MySetCurrentDirectoryEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory21MySetCurrentDirectoryEPKw,@function
_ZN8NWindows5NFile10NDirectory21MySetCurrentDirectoryEPKw: # @_ZN8NWindows5NFile10NDirectory21MySetCurrentDirectoryEPKw
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 64
.Lcfi28:
	.cfi_offset %rbx, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %r15d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB3_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%r15), %eax
	movslq	%eax, %r14
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	%r14d, 12(%rsp)
	.p2align	4, 0x90
.LBB3_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB3_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%r15d, 8(%rsp)
.Ltmp9:
	leaq	16(%rsp), %rdi
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp10:
# BB#5:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_7
# BB#6:
	callq	_ZdaPv
.LBB3_7:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	16(%rsp), %rdi
	callq	chdir
	movl	%eax, %ebx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_9
# BB#8:
	callq	_ZdaPv
.LBB3_9:                                # %_ZN11CStringBaseIcED2Ev.exit
	testl	%ebx, %ebx
	sete	%al
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB3_10:
.Ltmp11:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_12
# BB#11:
	callq	_ZdaPv
.LBB3_12:                               # %_ZN11CStringBaseIwED2Ev.exit2
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN8NWindows5NFile10NDirectory21MySetCurrentDirectoryEPKw, .Lfunc_end3-_ZN8NWindows5NFile10NDirectory21MySetCurrentDirectoryEPKw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp9-.Lfunc_begin2    #   Call between .Lfunc_begin2 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin2   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory11GetOnlyNameEPKwR11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory11GetOnlyNameEPKwR11CStringBaseIwE,@function
_ZN8NWindows5NFile10NDirectory11GetOnlyNameEPKwR11CStringBaseIwE: # @_ZN8NWindows5NFile10NDirectory11GetOnlyNameEPKwR11CStringBaseIwE
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 80
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	leaq	12(%rsp), %rdx
	callq	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi
	testb	%al, %al
	je	.LBB4_1
# BB#2:
	movl	12(%rsp), %edx
	movl	8(%r14), %ecx
	subl	%edx, %ecx
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%r14, %rbx
	je	.LBB4_3
# BB#4:
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movslq	24(%rsp), %r15
	incq	%r15
	movl	12(%r14), %ebp
	cmpl	%ebp, %r15d
	je	.LBB4_10
# BB#5:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp12:
	callq	_Znam
	movq	%rax, %r12
.Ltmp13:
# BB#6:                                 # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB4_9
# BB#7:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB4_9
# BB#8:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB4_9:                                # %._crit_edge16.i.i
	movq	%r12, (%r14)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
	movq	%r12, %rbx
.LBB4_10:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_11:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB4_11
# BB#12:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	24(%rsp), %eax
	movl	%eax, 8(%r14)
	testq	%rdi, %rdi
	jne	.LBB4_14
	jmp	.LBB4_15
.LBB4_1:
	xorl	%eax, %eax
	jmp	.LBB4_16
.LBB4_3:                                # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_15
.LBB4_14:
	callq	_ZdaPv
.LBB4_15:                               # %_ZN11CStringBaseIwED2Ev.exit
	movb	$1, %al
.LBB4_16:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_17:
.Ltmp14:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_19
# BB#18:
	callq	_ZdaPv
.LBB4_19:                               # %_ZN11CStringBaseIwED2Ev.exit6
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN8NWindows5NFile10NDirectory11GetOnlyNameEPKwR11CStringBaseIwE, .Lfunc_end4-_ZN8NWindows5NFile10NDirectory11GetOnlyNameEPKwR11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp12-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin3   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end4-.Ltmp13     #   Call between .Ltmp13 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi,@function
_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi: # @_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi47:
	.cfi_def_cfa_offset 64
.Lcfi48:
	.cfi_offset %rbx, -48
.Lcfi49:
	.cfi_offset %r12, -40
.Lcfi50:
	.cfi_offset %r13, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	$0, 8(%rsp)
	movl	12(%r12), %ebx
	cmpl	$4097, %ebx             # imm = 0x1001
	jl	.LBB5_2
# BB#1:                                 # %._ZN11CStringBaseIwE9GetBufferEi.exit_crit_edge
	movq	(%r12), %r13
	jmp	.LBB5_26
.LBB5_2:
	movl	$16388, %edi            # imm = 0x4004
	callq	_Znam
	movq	%rax, %r13
	testl	%ebx, %ebx
	jle	.LBB5_25
# BB#3:                                 # %.preheader.i.i
	movslq	8(%r12), %rax
	testq	%rax, %rax
	movq	(%r12), %rdi
	jle	.LBB5_23
# BB#4:                                 # %.lr.ph.i.i
	cmpl	$7, %eax
	jbe	.LBB5_5
# BB#12:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB5_5
# BB#13:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r13
	jae	.LBB5_15
# BB#14:                                # %vector.memcheck
	leaq	(%r13,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB5_15
.LBB5_5:
	xorl	%ecx, %ecx
.LBB5_6:                                # %scalar.ph.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB5_9
# BB#7:                                 # %scalar.ph.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB5_8:                                # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %ebx
	movl	%ebx, (%r13,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB5_8
.LBB5_9:                                # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB5_24
# BB#10:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r13,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB5_11:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB5_11
	jmp	.LBB5_24
.LBB5_23:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB5_25
.LBB5_24:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB5_25:                               # %._crit_edge16.i.i
	movq	%r13, (%r12)
	movslq	8(%r12), %rax
	movl	$0, (%r13,%rax,4)
	movl	$4097, 12(%r12)         # imm = 0x1001
.LBB5_26:                               # %_ZN11CStringBaseIwE9GetBufferEi.exit
	leaq	8(%rsp), %rcx
	movl	$4097, %esi             # imm = 0x1001
	movq	%r15, %rdi
	movq	%r13, %rdx
	callq	_Z15GetFullPathNamePKwjPwPS1_
	movq	(%r12), %rbx
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	movl	$-1, %edx
	movabsq	$4294967296, %rdi       # imm = 0x100000000
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB5_27:                               # =>This Inner Loop Header: Depth=1
	addq	%rdi, %rsi
	incl	%edx
	cmpl	$0, (%rcx)
	leaq	4(%rcx), %rcx
	jne	.LBB5_27
# BB#28:                                # %_ZN11CStringBaseIwE13ReleaseBufferEv.exit
	sarq	$30, %rsi
	movl	$0, (%rbx,%rsi)
	movl	%edx, 8(%r12)
	decl	%eax
	cmpl	$4094, %eax             # imm = 0xFFE
	jbe	.LBB5_30
# BB#29:
	xorl	%eax, %eax
	jmp	.LBB5_34
.LBB5_30:
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_31
# BB#32:
	subq	%r13, %rax
	shrq	$2, %rax
	jmp	.LBB5_33
.LBB5_31:
	movq	%r15, %rdi
	callq	wcslen
.LBB5_33:
	movl	%eax, (%r14)
	movb	$1, %al
.LBB5_34:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_15:                               # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_16
# BB#17:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_18:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r13,%rbx,4)
	movups	%xmm1, 16(%r13,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB5_18
	jmp	.LBB5_19
.LBB5_16:
	xorl	%ebx, %ebx
.LBB5_19:                               # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB5_22
# BB#20:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r13,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB5_21:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB5_21
.LBB5_22:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB5_6
	jmp	.LBB5_24
.Lfunc_end5:
	.size	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi, .Lfunc_end5-_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi
	.cfi_endproc

	.globl	_ZN8NWindows5NFile10NDirectory16GetOnlyDirPrefixEPKwR11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory16GetOnlyDirPrefixEPKwR11CStringBaseIwE,@function
_ZN8NWindows5NFile10NDirectory16GetOnlyDirPrefixEPKwR11CStringBaseIwE: # @_ZN8NWindows5NFile10NDirectory16GetOnlyDirPrefixEPKwR11CStringBaseIwE
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 80
.Lcfi59:
	.cfi_offset %rbx, -48
.Lcfi60:
	.cfi_offset %r12, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	leaq	12(%rsp), %rdx
	callq	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi
	testb	%al, %al
	je	.LBB6_1
# BB#2:
	movl	12(%rsp), %ecx
	leaq	16(%rsp), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%r14, %rbx
	je	.LBB6_3
# BB#4:
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movslq	24(%rsp), %r15
	incq	%r15
	movl	12(%r14), %ebp
	cmpl	%ebp, %r15d
	je	.LBB6_10
# BB#5:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp15:
	callq	_Znam
	movq	%rax, %r12
.Ltmp16:
# BB#6:                                 # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB6_9
# BB#7:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB6_9
# BB#8:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB6_9:                                # %._crit_edge16.i.i
	movq	%r12, (%r14)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
	movq	%r12, %rbx
.LBB6_10:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_11:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB6_11
# BB#12:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	24(%rsp), %eax
	movl	%eax, 8(%r14)
	testq	%rdi, %rdi
	jne	.LBB6_14
	jmp	.LBB6_15
.LBB6_1:
	xorl	%eax, %eax
	jmp	.LBB6_16
.LBB6_3:                                # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_15
.LBB6_14:
	callq	_ZdaPv
.LBB6_15:                               # %_ZN11CStringBaseIwED2Ev.exit
	movb	$1, %al
.LBB6_16:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_17:
.Ltmp17:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_19
# BB#18:
	callq	_ZdaPv
.LBB6_19:                               # %_ZN11CStringBaseIwED2Ev.exit6
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN8NWindows5NFile10NDirectory16GetOnlyDirPrefixEPKwR11CStringBaseIwE, .Lfunc_end6-_ZN8NWindows5NFile10NDirectory16GetOnlyDirPrefixEPKwR11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp15-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin4   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Lfunc_end6-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory21MyGetCurrentDirectoryER11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory21MyGetCurrentDirectoryER11CStringBaseIwE,@function
_ZN8NWindows5NFile10NDirectory21MyGetCurrentDirectoryER11CStringBaseIwE: # @_ZN8NWindows5NFile10NDirectory21MyGetCurrentDirectoryER11CStringBaseIwE
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 48
	subq	$1056, %rsp             # imm = 0x420
.Lcfi69:
	.cfi_def_cfa_offset 1104
.Lcfi70:
	.cfi_offset %rbx, -48
.Lcfi71:
	.cfi_offset %r12, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movw	$14947, 32(%rsp)        # imm = 0x3A63
	leaq	34(%rsp), %rdi
	movl	$1021, %esi             # imm = 0x3FD
	callq	getcwd
	testq	%rax, %rax
	je	.LBB7_1
# BB#2:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %r15d
	leaq	32(%rsp), %rax
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB7_3
# BB#4:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leaq	32(%rsp), %rbx
	leal	1(%r15), %eax
	movslq	%eax, %rbp
	cmpl	$-1, %r15d
	movq	$-1, %rdi
	cmovgeq	%rbp, %rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	%ebp, 12(%rsp)
	.p2align	4, 0x90
.LBB7_5:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB7_5
# BB#6:                                 # %_ZN11CStringBaseIcEC2EPKc.exit
	movl	%r15d, 8(%rsp)
.Ltmp18:
	leaq	16(%rsp), %rbx
	movq	%rsp, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp19:
# BB#7:                                 # %_Z16GetUnicodeStringRK11CStringBaseIcE.exit
	cmpq	%r14, %rbx
	je	.LBB7_8
# BB#9:
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movslq	24(%rsp), %r15
	incq	%r15
	movl	12(%r14), %ebp
	cmpl	%ebp, %r15d
	je	.LBB7_15
# BB#10:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp21:
	callq	_Znam
	movq	%rax, %r12
.Ltmp22:
# BB#11:                                # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB7_14
# BB#12:                                # %.noexc
	testl	%ebp, %ebp
	jle	.LBB7_14
# BB#13:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB7_14:                               # %._crit_edge16.i.i
	movq	%r12, (%r14)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
	movq	%r12, %rbx
.LBB7_15:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_16:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB7_16
# BB#17:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	24(%rsp), %eax
	movl	%eax, 8(%r14)
	testq	%rdi, %rdi
	jne	.LBB7_19
	jmp	.LBB7_20
.LBB7_1:
	xorl	%eax, %eax
	jmp	.LBB7_23
.LBB7_8:                                # %_Z16GetUnicodeStringRK11CStringBaseIcE.exit._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB7_20
.LBB7_19:
	callq	_ZdaPv
.LBB7_20:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_22
# BB#21:
	callq	_ZdaPv
.LBB7_22:                               # %_ZN11CStringBaseIcED2Ev.exit
	movb	$1, %al
.LBB7_23:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$1056, %rsp             # imm = 0x420
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_25:
.Ltmp23:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_27
# BB#26:
	callq	_ZdaPv
	jmp	.LBB7_27
.LBB7_24:
.Ltmp20:
	movq	%rax, %rbx
.LBB7_27:                               # %_ZN11CStringBaseIwED2Ev.exit10
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_29
# BB#28:
	callq	_ZdaPv
.LBB7_29:                               # %_ZN11CStringBaseIcED2Ev.exit11
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN8NWindows5NFile10NDirectory21MyGetCurrentDirectoryER11CStringBaseIwE, .Lfunc_end7-_ZN8NWindows5NFile10NDirectory21MyGetCurrentDirectoryER11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp18-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin5   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin5   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory10MyMoveFileEPKwS3_
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory10MyMoveFileEPKwS3_,@function
_ZN8NWindows5NFile10NDirectory10MyMoveFileEPKwS3_: # @_ZN8NWindows5NFile10NDirectory10MyMoveFileEPKwS3_
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	subq	$16440, %rsp            # imm = 0x4038
.Lcfi81:
	.cfi_def_cfa_offset 16496
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rax
	leaq	16(%rsp), %rdi
	movq	%rax, %rsi
	callq	_Z17nameWindowToUnix2PKw
.Ltmp24:
	leaq	32(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_Z17nameWindowToUnix2PKw
.Ltmp25:
# BB#1:
	movq	16(%rsp), %r14
	movq	32(%rsp), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	rename
	testl	%eax, %eax
	je	.LBB8_31
# BB#2:
	callq	__errno_location
	movq	%rax, %rbx
	cmpl	$18, (%rbx)
	jne	.LBB8_16
# BB#3:
.Ltmp27:
	movl	$193, %esi
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	open64
	movl	%eax, %r12d
.Ltmp28:
# BB#4:                                 # %.noexc
	cmpl	$-1, %r12d
	je	.LBB8_16
# BB#5:
.Ltmp29:
	xorl	%esi, %esi
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	open64
	movl	%eax, %ebp
.Ltmp30:
# BB#6:                                 # %.noexc22
	cmpl	$-1, %ebp
	je	.LBB8_19
# BB#7:
	movq	%r14, 8(%rsp)           # 8-byte Spill
	leaq	48(%rsp), %r13
	.p2align	4, 0x90
.LBB8_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_12 Depth 2
.Ltmp31:
	movl	$16384, %edx            # imm = 0x4000
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	read
	movq	%rax, %r14
.Ltmp32:
# BB#9:                                 # %.noexc23
                                        #   in Loop: Header=BB8_8 Depth=1
	testq	%r14, %r14
	jns	.LBB8_11
# BB#10:                                #   in Loop: Header=BB8_8 Depth=1
	cmpl	$4, (%rbx)
	je	.LBB8_8
	jmp	.LBB8_18
.LBB8_11:                               # %.critedge.i.i
                                        #   in Loop: Header=BB8_8 Depth=1
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	testq	%r14, %r14
	je	.LBB8_21
	.p2align	4, 0x90
.LBB8_12:                               # %.preheader.i.i
                                        #   Parent Loop BB8_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp34:
	movl	%r12d, %edi
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	write
.Ltmp35:
# BB#13:                                # %.noexc24
                                        #   in Loop: Header=BB8_12 Depth=2
	testq	%rax, %rax
	jns	.LBB8_15
# BB#14:                                #   in Loop: Header=BB8_12 Depth=2
	cmpl	$4, (%rbx)
	je	.LBB8_12
	jmp	.LBB8_20
.LBB8_15:                               # %.critedge1.i.i
                                        #   in Loop: Header=BB8_8 Depth=1
	jne	.LBB8_8
	jmp	.LBB8_21
.LBB8_18:
	movl	$-1, 4(%rsp)            # 4-byte Folded Spill
	jmp	.LBB8_21
.LBB8_19:
	movl	$-1, %ebx
	jmp	.LBB8_23
.LBB8_20:                               # %_ZL7copy_fdii.exit.loopexit.i
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB8_21:                               # %_ZL7copy_fdii.exit.i
.Ltmp37:
	movl	%ebp, %edi
	movq	8(%rsp), %r14           # 8-byte Reload
	callq	close
	movl	%eax, %ebx
.Ltmp38:
# BB#22:                                # %.noexc25
	movl	4(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	cmovnel	%eax, %ebx
.LBB8_23:
.Ltmp39:
	movl	%r12d, %edi
	callq	close
.Ltmp40:
# BB#24:
	testl	%ebx, %ebx
	cmovnel	%ebx, %eax
	testl	%eax, %eax
	jne	.LBB8_16
# BB#26:
	leaq	48(%rsp), %rdx
	movl	$1, %edi
	movq	%r14, %rsi
	callq	__xstat64
	testl	%eax, %eax
	jne	.LBB8_16
# BB#27:
	movl	_ZL9gbl_umask+4(%rip), %esi
	andl	72(%rsp), %esi
	movq	%r15, %rdi
	callq	chmod
	testl	%eax, %eax
	jne	.LBB8_16
# BB#29:                                # %.thread29
	movq	%r14, %rdi
	callq	unlink
	testl	%eax, %eax
	je	.LBB8_31
.LBB8_16:
	xorl	%ebx, %ebx
	testq	%r15, %r15
	jne	.LBB8_32
	jmp	.LBB8_33
.LBB8_31:
	movb	$1, %bl
	testq	%r15, %r15
	je	.LBB8_33
.LBB8_32:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB8_33:                               # %_ZN11CStringBaseIcED2Ev.exit21
	testq	%r14, %r14
	je	.LBB8_35
# BB#34:
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB8_35:                               # %_ZN11CStringBaseIcED2Ev.exit20
	movl	%ebx, %eax
	addq	$16440, %rsp            # imm = 0x4038
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_36:                               # %.loopexit.split-lp.loopexit.split-lp
.Ltmp41:
	movq	%r14, 8(%rsp)           # 8-byte Spill
	jmp	.LBB8_40
.LBB8_37:
.Ltmp26:
	movq	%rax, %rbx
	movq	16(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	jne	.LBB8_43
	jmp	.LBB8_44
.LBB8_38:                               # %.loopexit
.Ltmp36:
	jmp	.LBB8_40
.LBB8_39:                               # %.loopexit.split-lp.loopexit
.Ltmp33:
.LBB8_40:                               # %.loopexit.split-lp
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.LBB8_42
# BB#41:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB8_42:
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB8_44
.LBB8_43:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
.LBB8_44:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN8NWindows5NFile10NDirectory10MyMoveFileEPKwS3_, .Lfunc_end8-_ZN8NWindows5NFile10NDirectory10MyMoveFileEPKwS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp24-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin6   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp30-.Ltmp27         #   Call between .Ltmp27 and .Ltmp30
	.long	.Ltmp41-.Lfunc_begin6   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin6   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin6   # >> Call Site 5 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin6   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin6   # >> Call Site 6 <<
	.long	.Ltmp40-.Ltmp37         #   Call between .Ltmp37 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin6   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin6   # >> Call Site 7 <<
	.long	.Lfunc_end8-.Ltmp40     #   Call between .Ltmp40 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory17MyRemoveDirectoryEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory17MyRemoveDirectoryEPKw,@function
_ZN8NWindows5NFile10NDirectory17MyRemoveDirectoryEPKw: # @_ZN8NWindows5NFile10NDirectory17MyRemoveDirectoryEPKw
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 48
.Lcfi91:
	.cfi_offset %rbx, -24
.Lcfi92:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB9_2
# BB#1:
	cmpl	$0, (%rax)
	je	.LBB9_2
# BB#3:
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	_Z17nameWindowToUnix2PKw
	movq	8(%rsp), %r14
	movq	%r14, %rdi
	callq	rmdir
	testl	%eax, %eax
	sete	%bl
	testq	%r14, %r14
	je	.LBB9_5
# BB#4:
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB9_5
.LBB9_2:
	callq	__errno_location
	movl	$2, (%rax)
	xorl	%ebx, %ebx
.LBB9_5:                                # %_ZL15RemoveDirectoryPKw.exit
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN8NWindows5NFile10NDirectory17MyRemoveDirectoryEPKw, .Lfunc_end9-_ZN8NWindows5NFile10NDirectory17MyRemoveDirectoryEPKw
	.cfi_endproc

	.globl	_ZN8NWindows5NFile10NDirectory10SetDirTimeEPKwPK9_FILETIMES6_S6_
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory10SetDirTimeEPKwPK9_FILETIMES6_S6_,@function
_ZN8NWindows5NFile10NDirectory10SetDirTimeEPKwPK9_FILETIMES6_S6_: # @_ZN8NWindows5NFile10NDirectory10SetDirTimeEPKwPK9_FILETIMES6_S6_
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 48
	subq	$192, %rsp
.Lcfi98:
	.cfi_def_cfa_offset 240
.Lcfi99:
	.cfi_offset %rbx, -48
.Lcfi100:
	.cfi_offset %r12, -40
.Lcfi101:
	.cfi_offset %r13, -32
.Lcfi102:
	.cfi_offset %r14, -24
.Lcfi103:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movl	$-1, %r13d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB10_1:                               # =>This Inner Loop Header: Depth=1
	incl	%r13d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB10_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%r13), %eax
	movslq	%eax, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 48(%rsp)
	movl	$0, (%rax)
	movl	%r12d, 60(%rsp)
	.p2align	4, 0x90
.LBB10_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB10_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%r13d, 56(%rsp)
.Ltmp42:
	leaq	32(%rsp), %rdi
	leaq	48(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp43:
# BB#5:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_7
# BB#6:
	callq	_ZdaPv
.LBB10_7:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	32(%rsp), %rbx
	cmpb	$99, (%rbx)
	jne	.LBB10_9
# BB#8:
	leaq	2(%rbx), %rax
	cmpb	$58, 1(%rbx)
	cmoveq	%rax, %rbx
.LBB10_9:                               # %_ZL16nameWindowToUnixPKc.exit
	leaq	48(%rsp), %rdx
	movl	$1, %edi
	movq	%rbx, %rsi
	callq	__xstat64
	testl	%eax, %eax
	je	.LBB10_10
# BB#12:
	xorl	%edi, %edi
	callq	time
	movq	%rax, %rcx
	jmp	.LBB10_13
.LBB10_10:
	movq	120(%rsp), %rax
	movq	136(%rsp), %rcx
.LBB10_13:
	movq	%rax, 16(%rsp)
	movq	%rcx, 24(%rsp)
	testq	%r15, %r15
	je	.LBB10_16
# BB#14:
	movl	(%r15), %eax
	movl	4(%r15), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	movq	%rax, 8(%rsp)
.Ltmp45:
	leaq	8(%rsp), %rdi
	leaq	4(%rsp), %rsi
	callq	_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj
.Ltmp46:
# BB#15:
	movl	4(%rsp), %eax
	movq	%rax, 16(%rsp)
.LBB10_16:
	testq	%r14, %r14
	je	.LBB10_19
# BB#17:
	movl	(%r14), %eax
	movl	4(%r14), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	movq	%rax, 8(%rsp)
.Ltmp48:
	leaq	8(%rsp), %rdi
	leaq	4(%rsp), %rsi
	callq	_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj
.Ltmp49:
# BB#18:
	movl	4(%rsp), %eax
	movq	%rax, 24(%rsp)
.LBB10_19:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	utime
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_21
# BB#20:
	callq	_ZdaPv
.LBB10_21:                              # %_ZN11CStringBaseIcED2Ev.exit23
	movb	$1, %al
	addq	$192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB10_27:
.Ltmp50:
	jmp	.LBB10_23
.LBB10_22:
.Ltmp47:
.LBB10_23:
	movq	%rax, %rbx
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB10_25
	jmp	.LBB10_26
.LBB10_11:
.Ltmp44:
	movq	%rax, %rbx
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_26
.LBB10_25:
	callq	_ZdaPv
.LBB10_26:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN8NWindows5NFile10NDirectory10SetDirTimeEPKwPK9_FILETIMES6_S6_, .Lfunc_end10-_ZN8NWindows5NFile10NDirectory10SetDirTimeEPKwPK9_FILETIMES6_S6_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp42-.Lfunc_begin7   #   Call between .Lfunc_begin7 and .Ltmp42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin7   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin7   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin7   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin7   # >> Call Site 5 <<
	.long	.Lfunc_end10-.Ltmp49    #   Call between .Ltmp49 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory19MySetFileAttributesEPKwj
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory19MySetFileAttributesEPKwj,@function
_ZN8NWindows5NFile10NDirectory19MySetFileAttributesEPKwj: # @_ZN8NWindows5NFile10NDirectory19MySetFileAttributesEPKwj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 32
	subq	$1200, %rsp             # imm = 0x4B0
.Lcfi107:
	.cfi_def_cfa_offset 1232
.Lcfi108:
	.cfi_offset %rbx, -32
.Lcfi109:
	.cfi_offset %r14, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB11_4
# BB#1:
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	_Z17nameWindowToUnix2PKw
	cmpl	$0, global_use_lstat(%rip)
	movq	(%rsp), %rbx
	leaq	16(%rsp), %rdx
	movl	$1, %edi
	movq	%rbx, %rsi
	je	.LBB11_5
# BB#2:
	callq	__lxstat64
	testl	%eax, %eax
	jne	.LBB11_3
	jmp	.LBB11_7
.LBB11_4:
	callq	__errno_location
	movl	$2, (%rax)
	xorl	%ebp, %ebp
	jmp	.LBB11_25
.LBB11_5:
	callq	__xstat64
	testl	%eax, %eax
	je	.LBB11_7
.LBB11_3:
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	jne	.LBB11_24
	jmp	.LBB11_25
.LBB11_7:
	testw	%bp, %bp
	js	.LBB11_13
# BB#8:
	movl	40(%rsp), %esi
	movl	%esi, %eax
	andl	$61440, %eax            # imm = 0xF000
	cmpl	$40960, %eax            # imm = 0xA000
	je	.LBB11_23
# BB#9:
	movzwl	%ax, %eax
	cmpl	$16384, %eax            # imm = 0x4000
	je	.LBB11_12
# BB#10:
	testb	$1, %bpl
	je	.LBB11_12
# BB#11:
	andl	$-147, %esi
	movl	%esi, 40(%rsp)
.LBB11_12:
	andl	_ZL9gbl_umask+4(%rip), %esi
	movq	%rbx, %rdi
	jmp	.LBB11_22
.LBB11_13:
	shrl	$16, %ebp
	movl	%ebp, 40(%rsp)
	movl	%ebp, %eax
	andl	$61440, %eax            # imm = 0xF000
	cmpl	$32768, %eax            # imm = 0x8000
	je	.LBB11_21
# BB#14:
	movzwl	%ax, %eax
	cmpl	$16384, %eax            # imm = 0x4000
	je	.LBB11_20
# BB#15:
	cmpl	$40960, %eax            # imm = 0xA000
	jne	.LBB11_23
# BB#16:
	movl	$.L.str.10, %esi
	movq	%rbx, %rdi
	callq	fopen64
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB11_3
# BB#17:
	leaq	160(%rsp), %rdi
	movl	$1024, %esi             # imm = 0x400
	movq	%rbp, %rdx
	callq	fgets
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	fclose
	testq	%r14, %r14
	je	.LBB11_3
# BB#18:
	movq	%rbx, %rdi
	callq	unlink
	testl	%eax, %eax
	jne	.LBB11_3
# BB#27:                                # %_ZN8NWindows5NFile10NDirectoryL18convert_to_symlinkEPKc.exit
	leaq	160(%rsp), %rdi
	movq	%rbx, %rsi
	callq	symlink
	testl	%eax, %eax
	jne	.LBB11_3
	jmp	.LBB11_23
.LBB11_20:
	orl	$448, %ebp              # imm = 0x1C0
	movl	%ebp, 40(%rsp)
.LBB11_21:
	andl	_ZL9gbl_umask+4(%rip), %ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
.LBB11_22:
	callq	chmod
.LBB11_23:
	movb	$1, %bpl
	testq	%rbx, %rbx
	je	.LBB11_25
.LBB11_24:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB11_25:
	movl	%ebp, %eax
	addq	$1200, %rsp             # imm = 0x4B0
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN8NWindows5NFile10NDirectory19MySetFileAttributesEPKwj, .Lfunc_end11-_ZN8NWindows5NFile10NDirectory19MySetFileAttributesEPKwj
	.cfi_endproc

	.globl	_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw,@function
_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw: # @_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi113:
	.cfi_def_cfa_offset 48
.Lcfi114:
	.cfi_offset %rbx, -24
.Lcfi115:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB12_2
# BB#1:
	cmpl	$0, (%rax)
	je	.LBB12_2
# BB#3:
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	_Z17nameWindowToUnix2PKw
	movq	8(%rsp), %r14
	movl	$448, %esi              # imm = 0x1C0
	movq	%r14, %rdi
	callq	mkdir
	testl	%eax, %eax
	sete	%bl
	testq	%r14, %r14
	je	.LBB12_5
# BB#4:
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB12_5
.LBB12_2:
	callq	__errno_location
	movl	$2, (%rax)
	xorl	%ebx, %ebx
.LBB12_5:
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw, .Lfunc_end12-_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw
	.cfi_endproc

	.globl	_ZN8NWindows5NFile10NDirectory22CreateComplexDirectoryEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory22CreateComplexDirectoryEPKw,@function
_ZN8NWindows5NFile10NDirectory22CreateComplexDirectoryEPKw: # @_ZN8NWindows5NFile10NDirectory22CreateComplexDirectoryEPKw
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi119:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi120:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi122:
	.cfi_def_cfa_offset 144
.Lcfi123:
	.cfi_offset %rbx, -56
.Lcfi124:
	.cfi_offset %r12, -48
.Lcfi125:
	.cfi_offset %r13, -40
.Lcfi126:
	.cfi_offset %r14, -32
.Lcfi127:
	.cfi_offset %r15, -24
.Lcfi128:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rbp,%rbx,4)
	leaq	1(%rbx), %rbx
	jne	.LBB13_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leaq	-1(%rbx), %r13
	movslq	%ebx, %r14
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	movq	%r12, (%rsp)
	movl	$0, (%r12)
	movl	%r14d, 12(%rsp)
	.p2align	4, 0x90
.LBB13_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB13_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%r13d, 8(%rsp)
	cmpl	$1, %ebx
	jne	.LBB13_6
# BB#5:
	xorl	%r13d, %r13d
	movl	$1, %eax
	jmp	.LBB13_16
.LBB13_6:
	leal	-1(%rbx), %eax
	cltq
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB13_7:                               # =>This Inner Loop Header: Depth=1
	cmpl	$47, -4(%r12,%rax)
	je	.LBB13_9
# BB#8:                                 #   in Loop: Header=BB13_7 Depth=1
	addq	$-4, %rax
	jne	.LBB13_7
	jmp	.LBB13_15
.LBB13_9:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	leaq	-4(%r12,%rax), %rdi
	subq	%r12, %rdi
	movq	%rdi, %rcx
	shrq	$2, %rcx
	testl	%ecx, %ecx
	jle	.LBB13_15
# BB#10:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	movl	%ebx, %eax
	subl	%ecx, %eax
	addl	$-2, %eax
	jne	.LBB13_15
# BB#11:
	cmpl	$4, %ebx
	jne	.LBB13_13
# BB#12:
	movb	$1, %bpl
	cmpl	$58, 4(%r12)
	je	.LBB13_77
.LBB13_13:
	movl	%ecx, %eax
	notl	%eax
	leal	(%rbx,%rax), %eax
	cmpl	%ecx, %r13d
	movl	$1, %ebp
	cmovlel	%eax, %ebp
	testl	%ebp, %ebp
	jle	.LBB13_15
# BB#14:
	leal	(%rbp,%rcx), %eax
	shlq	$30, %rdi
	sarq	$30, %rdi
	addq	%r12, %rdi
	cltq
	leaq	(%r12,%rax,4), %rsi
	movl	%ebp, %eax
	negl	%eax
	subl	%ecx, %eax
	addl	%ebx, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memmove
	notl	%ebp
	addl	%ebx, %ebp
	movl	%ebp, 8(%rsp)
	movl	%ebp, %r13d
.LBB13_15:                              # %_ZN11CStringBaseIwE6DeleteEii.exit
	movl	%r13d, %eax
	incl	%eax
	je	.LBB13_47
.LBB13_16:                              # %._crit_edge16.i.i
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp51:
	callq	_Znam
.Ltmp52:
# BB#17:                                # %.noexc
	movl	$0, (%rax)
	movq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rax, %r14
.LBB13_18:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i30
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_19:                              # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rax), %ecx
	movl	%ecx, (%r14,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB13_19
# BB#20:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r13d, %r15d
	testq	%r12, %r12
	jne	.LBB13_23
	jmp	.LBB13_29
	.p2align	4, 0x90
.LBB13_21:
	callq	_ZdaPv
	movq	(%rsp), %r12
	testq	%r12, %r12
	jne	.LBB13_23
	.p2align	4, 0x90
.LBB13_29:                              # %_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw.exit38.thread
	callq	__errno_location
	movl	$2, (%rax)
	cmpl	$17, (%rax)
	jne	.LBB13_30
	jmp	.LBB13_48
	.p2align	4, 0x90
.LBB13_22:
	testq	%r12, %r12
	je	.LBB13_29
.LBB13_23:
	cmpl	$0, (%r12)
	je	.LBB13_29
# BB#24:
.Ltmp54:
	leaq	32(%rsp), %rdi
	movq	%r12, %rsi
	callq	_Z17nameWindowToUnix2PKw
.Ltmp55:
# BB#25:                                # %.noexc37
	movq	32(%rsp), %rbx
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbx, %rdi
	callq	mkdir
	movl	%eax, %ebp
	testq	%rbx, %rbx
	je	.LBB13_27
# BB#26:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB13_27:                              # %_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw.exit38
	testl	%ebp, %ebp
	je	.LBB13_48
# BB#28:                                # %_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw.exit38._crit_edge
	callq	__errno_location
	cmpl	$17, (%rax)
	je	.LBB13_48
.LBB13_30:
	movslq	8(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB13_75
# BB#31:
	movq	(%rsp), %rax
	shlq	$2, %rcx
	.p2align	4, 0x90
.LBB13_32:                              # =>This Inner Loop Header: Depth=1
	cmpl	$47, -4(%rax,%rcx)
	je	.LBB13_34
# BB#33:                                #   in Loop: Header=BB13_32 Depth=1
	addq	$-4, %rcx
	jne	.LBB13_32
	jmp	.LBB13_75
	.p2align	4, 0x90
.LBB13_34:                              # %_ZNK11CStringBaseIwE11ReverseFindEw.exit41
	leaq	-4(%rax,%rcx), %rcx
	subq	%rax, %rcx
	movq	%rcx, %r15
	shrq	$2, %r15
	testl	%r15d, %r15d
	jle	.LBB13_75
# BB#35:
	shlq	$30, %rcx
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	addq	%rdx, %rcx
	sarq	$30, %rcx
	cmpl	$58, (%rax,%rcx)
	je	.LBB13_75
# BB#36:
.Ltmp57:
	xorl	%edx, %edx
	leaq	32(%rsp), %rdi
	movq	%rsp, %rsi
	movl	%r15d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp58:
# BB#37:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
	movl	$0, 8(%rsp)
	movq	(%rsp), %r12
	movl	$0, (%r12)
	movslq	40(%rsp), %rbp
	incq	%rbp
	movl	12(%rsp), %ebx
	cmpl	%ebx, %ebp
	je	.LBB13_44
# BB#38:
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp60:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp61:
# BB#39:                                # %.noexc47
	testq	%r12, %r12
	je	.LBB13_42
# BB#40:                                # %.noexc47
	testl	%ebx, %ebx
	movl	$0, %eax
	jle	.LBB13_43
# BB#41:                                # %._crit_edge.thread.i.i
	movq	%r12, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	jmp	.LBB13_43
.LBB13_42:
	xorl	%eax, %eax
.LBB13_43:                              # %._crit_edge16.i.i43
	movq	%r13, (%rsp)
	movl	$0, (%r13,%rax,4)
	movl	%ebp, 12(%rsp)
	movq	%r13, %r12
	movq	64(%rsp), %r13          # 8-byte Reload
.LBB13_44:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i44
	movq	32(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_45:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%r12,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB13_45
# BB#46:
	movl	40(%rsp), %eax
	movl	%eax, 8(%rsp)
	testq	%rdi, %rdi
	jne	.LBB13_21
	jmp	.LBB13_22
.LBB13_47:
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$-1, %r13d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	jmp	.LBB13_18
.LBB13_48:
	movl	$0, 8(%rsp)
	movq	(%rsp), %rbx
	movl	$0, (%rbx)
	movl	12(%rsp), %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	%ebp, %eax
	je	.LBB13_54
# BB#49:
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp63:
	callq	_Znam
	movq	%rax, %r12
.Ltmp64:
# BB#50:                                # %.noexc58
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB13_53
# BB#51:                                # %.noexc58
	testl	%ebp, %ebp
	jle	.LBB13_53
# BB#52:                                # %._crit_edge.thread.i.i53
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
.LBB13_53:                              # %._crit_edge16.i.i54
	movq	%r12, (%rsp)
	movl	$0, (%r12,%rax,4)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 12(%rsp)
	movq	%r12, %rbx
.LBB13_54:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i55.preheader
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB13_55:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i55
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB13_55
# BB#56:                                # %_ZN11CStringBaseIwEaSERKS0_.exit59
	movl	%r13d, 8(%rsp)
	cmpl	%r13d, %r15d
	jge	.LBB13_80
# BB#57:                                # %.lr.ph.preheader
	leaq	48(%rsp), %r12
	movq	%rsp, %rbx
	.p2align	4, 0x90
.LBB13_58:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_60 Depth 2
	movq	(%rsp), %rax
	movslq	%r15d, %rcx
	leaq	4(%rax,%rcx,4), %r15
	movl	4(%rax,%rcx,4), %ecx
	cmpl	$47, %ecx
	je	.LBB13_61
	.p2align	4, 0x90
.LBB13_60:                              # %.lr.ph.i
                                        #   Parent Loop BB13_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ecx, %ecx
	je	.LBB13_62
# BB#59:                                #   in Loop: Header=BB13_60 Depth=2
	movl	4(%r15), %ecx
	addq	$4, %r15
	cmpl	$47, %ecx
	jne	.LBB13_60
.LBB13_61:                              # %_ZNK11CStringBaseIwE4FindEwi.exit
                                        #   in Loop: Header=BB13_58 Depth=1
	subq	%rax, %r15
	shrq	$2, %r15
	testl	%r15d, %r15d
	jns	.LBB13_63
.LBB13_62:                              # %_ZNK11CStringBaseIwE4FindEwi.exit.thread
                                        #   in Loop: Header=BB13_58 Depth=1
	movl	%r13d, %r15d
.LBB13_63:                              #   in Loop: Header=BB13_58 Depth=1
.Ltmp66:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%r15d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp67:
# BB#64:                                # %_ZNK11CStringBaseIwE4LeftEi.exit61
                                        #   in Loop: Header=BB13_58 Depth=1
	movq	48(%rsp), %r13
	testq	%r13, %r13
	je	.LBB13_70
# BB#65:                                #   in Loop: Header=BB13_58 Depth=1
	cmpl	$0, (%r13)
	je	.LBB13_70
# BB#66:                                #   in Loop: Header=BB13_58 Depth=1
.Ltmp69:
	movq	%rbx, %r12
	leaq	72(%rsp), %rdi
	movq	%r13, %rsi
	callq	_Z17nameWindowToUnix2PKw
.Ltmp70:
# BB#67:                                # %.noexc34
                                        #   in Loop: Header=BB13_58 Depth=1
	movq	72(%rsp), %rbx
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbx, %rdi
	callq	mkdir
	testl	%eax, %eax
	sete	%bpl
	testq	%rbx, %rbx
	je	.LBB13_69
# BB#68:                                #   in Loop: Header=BB13_58 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB13_69:                              # %_ZN11CStringBaseIcED2Ev.exit.i
                                        #   in Loop: Header=BB13_58 Depth=1
	movq	48(%rsp), %r13
	movq	%r12, %rbx
	leaq	48(%rsp), %r12
	testq	%r13, %r13
	jne	.LBB13_71
	jmp	.LBB13_72
	.p2align	4, 0x90
.LBB13_70:                              #   in Loop: Header=BB13_58 Depth=1
	callq	__errno_location
	movl	$2, (%rax)
	xorl	%ebp, %ebp
	testq	%r13, %r13
	je	.LBB13_72
.LBB13_71:                              #   in Loop: Header=BB13_58 Depth=1
	movq	%r13, %rdi
	callq	_ZdaPv
.LBB13_72:                              # %_ZN11CStringBaseIwED2Ev.exit33
                                        #   in Loop: Header=BB13_58 Depth=1
	testb	%bpl, %bpl
	je	.LBB13_75
# BB#73:                                # %_ZN11CStringBaseIwED2Ev.exit33._crit_edge
                                        #   in Loop: Header=BB13_58 Depth=1
	movl	8(%rsp), %r13d
	cmpl	%r13d, %r15d
	jl	.LBB13_58
.LBB13_80:
	movb	$1, %bpl
	testq	%r14, %r14
	jne	.LBB13_76
	jmp	.LBB13_77
.LBB13_75:
	xorl	%ebp, %ebp
	testq	%r14, %r14
	je	.LBB13_77
.LBB13_76:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB13_77:                              # %_ZN11CStringBaseIwED2Ev.exit28
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_79
# BB#78:
	callq	_ZdaPv
.LBB13_79:                              # %_ZN11CStringBaseIwED2Ev.exit26
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_81:                              # %.loopexit.split-lp.loopexit.split-lp
.Ltmp65:
	jmp	.LBB13_86
.LBB13_82:
.Ltmp71:
	movq	%rax, %rbx
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB13_84
	jmp	.LBB13_89
.LBB13_83:
.Ltmp62:
	movq	%rax, %rbx
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_89
.LBB13_84:
	callq	_ZdaPv
	testq	%r14, %r14
	jne	.LBB13_90
	jmp	.LBB13_92
.LBB13_85:                              # %.loopexit
.Ltmp68:
	jmp	.LBB13_86
.LBB13_87:                              # %.loopexit.split-lp.loopexit
.Ltmp56:
.LBB13_86:                              # %.loopexit
	movq	%rax, %rbx
	testq	%r14, %r14
	jne	.LBB13_90
	jmp	.LBB13_92
.LBB13_88:
.Ltmp59:
	movq	%rax, %rbx
.LBB13_89:                              # %_ZN11CStringBaseIwED2Ev.exit29
	testq	%r14, %r14
	je	.LBB13_92
.LBB13_90:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	jmp	.LBB13_92
.LBB13_91:
.Ltmp53:
	movq	%rax, %rbx
.LBB13_92:                              # %_ZN11CStringBaseIwED2Ev.exit27
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_94
# BB#93:
	callq	_ZdaPv
.LBB13_94:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN8NWindows5NFile10NDirectory22CreateComplexDirectoryEPKw, .Lfunc_end13-_ZN8NWindows5NFile10NDirectory22CreateComplexDirectoryEPKw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\367\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp51-.Lfunc_begin8   #   Call between .Lfunc_begin8 and .Ltmp51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin8   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin8   # >> Call Site 3 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin8   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin8   # >> Call Site 4 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin8   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin8   # >> Call Site 5 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin8   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin8   # >> Call Site 6 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin8   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin8   # >> Call Site 7 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin8   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin8   # >> Call Site 8 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin8   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin8   # >> Call Site 9 <<
	.long	.Lfunc_end13-.Ltmp70    #   Call between .Ltmp70 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw,@function
_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw: # @_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi131:
	.cfi_def_cfa_offset 48
.Lcfi132:
	.cfi_offset %rbx, -24
.Lcfi133:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB14_2
# BB#1:
	cmpl	$0, (%rax)
	je	.LBB14_2
# BB#3:
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	_Z17nameWindowToUnix2PKw
	movq	8(%rsp), %r14
	movq	%r14, %rdi
	callq	remove
	testl	%eax, %eax
	sete	%bl
	testq	%r14, %r14
	je	.LBB14_5
# BB#4:
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB14_5
.LBB14_2:
	callq	__errno_location
	movl	$2, (%rax)
	xorl	%ebx, %ebx
.LBB14_5:
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw, .Lfunc_end14-_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.zero	16
	.text
	.globl	_ZN8NWindows5NFile10NDirectory27RemoveDirectoryWithSubItemsERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory27RemoveDirectoryWithSubItemsERK11CStringBaseIwE,@function
_ZN8NWindows5NFile10NDirectory27RemoveDirectoryWithSubItemsERK11CStringBaseIwE: # @_ZN8NWindows5NFile10NDirectory27RemoveDirectoryWithSubItemsERK11CStringBaseIwE
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi137:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi138:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi140:
	.cfi_def_cfa_offset 336
.Lcfi141:
	.cfi_offset %rbx, -56
.Lcfi142:
	.cfi_offset %r12, -48
.Lcfi143:
	.cfi_offset %r13, -40
.Lcfi144:
	.cfi_offset %r14, -32
.Lcfi145:
	.cfi_offset %r15, -24
.Lcfi146:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	$0, 256(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, 248(%rsp)
	movl	$0, (%rax)
	movl	$4, 260(%rsp)
	movslq	8(%rbp), %r12
	leaq	1(%r12), %rbx
	testl	%ebx, %ebx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB15_3
# BB#1:                                 # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp72:
	callq	_Znam
	movq	%rax, %r14
.Ltmp73:
# BB#2:                                 # %.noexc
	movl	$0, (%r14)
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	movq	%r14, %r13
	jmp	.LBB15_4
.LBB15_3:
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.LBB15_4:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	(%rbp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_5:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, (%r13,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB15_5
# BB#6:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	%ebx, %eax
	subl	%r12d, %eax
	cmpl	$1, %eax
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	jle	.LBB15_8
# BB#7:
	movq	%r14, 24(%rsp)          # 8-byte Spill
	jmp	.LBB15_19
.LBB15_8:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB15_10
# BB#9:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB15_10:                              # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rbx,%rsi), %eax
	cmpl	%ebx, %eax
	jne	.LBB15_12
# BB#11:
	movq	%r14, 24(%rsp)          # 8-byte Spill
	jmp	.LBB15_19
.LBB15_12:
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp75:
	callq	_Znam
	movq	%rax, 24(%rsp)          # 8-byte Spill
.Ltmp76:
# BB#13:                                # %.noexc48
	testl	%ebx, %ebx
	jle	.LBB15_18
# BB#14:                                # %.preheader.i.i
	testl	%r12d, %r12d
	jle	.LBB15_16
# BB#15:                                # %.lr.ph.i.i
	leaq	(,%r12,4), %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	callq	memcpy
	jmp	.LBB15_17
.LBB15_16:                              # %._crit_edge.i.i
	testq	%r13, %r13
	je	.LBB15_18
.LBB15_17:                              # %._crit_edge.thread.i.i
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB15_18:                              # %._crit_edge16.i.i
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	$0, (%r13,%r12,4)
.LBB15_19:                              # %._crit_edge16.i.i.i13
	movl	$47, (%r13,%r12,4)
	movq	16(%rsp), %r14          # 8-byte Reload
	movslq	%r14d, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$0, (%r13,%rax,4)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	leaq	2(%r12), %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp78:
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	callq	_Znam
	movq	%rax, %rbp
.Ltmp79:
# BB#20:                                # %.noexc19
	movq	%rbp, 48(%rsp)
	movl	$0, (%rbp)
	movl	%r15d, 60(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB15_21:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i14
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB15_21
# BB#22:
	movl	%r14d, 56(%rsp)
	cmpl	$8, %r15d
	movl	$16, %ecx
	movl	$4, %eax
	cmovgl	%ecx, %eax
	cmpl	$65, %r15d
	jl	.LBB15_24
# BB#23:                                # %select.true.sink394
	movl	%r15d, %eax
	shrl	$31, %eax
	addl	%r15d, %eax
	sarl	%eax
.LBB15_24:                              # %select.end393
	testl	%eax, %eax
	movl	$1, %ecx
	cmovgl	%eax, %ecx
	leal	3(%r12,%rcx), %ebx
	cmpl	%r15d, %ebx
	je	.LBB15_44
# BB#25:
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp81:
	callq	_Znam
	movq	%rax, %r14
.Ltmp82:
# BB#26:                                # %.noexc61
	cmpl	$-1, %r12d
	jl	.LBB15_43
# BB#27:                                # %.preheader.i.i52
	testl	%r12d, %r12d
	js	.LBB15_42
# BB#28:                                # %.lr.ph.i.i53.preheader
	cmpl	$8, 16(%rsp)            # 4-byte Folded Reload
	jae	.LBB15_30
# BB#29:
	xorl	%eax, %eax
	jmp	.LBB15_40
.LBB15_30:                              # %min.iters.checked
	movq	8(%rsp), %rax           # 8-byte Reload
	andq	$-8, %rax
	je	.LBB15_34
# BB#31:                                # %vector.body.preheader
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB15_35
# BB#32:                                # %vector.body.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB15_33:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rsi,4), %xmm0
	movups	16(%rbp,%rsi,4), %xmm1
	movups	%xmm0, (%r14,%rsi,4)
	movups	%xmm1, 16(%r14,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB15_33
	jmp	.LBB15_36
.LBB15_34:
	xorl	%eax, %eax
	jmp	.LBB15_40
.LBB15_35:
	xorl	%esi, %esi
.LBB15_36:                              # %vector.body.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB15_39
# BB#37:                                # %vector.body.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r14,%rsi,4), %rdx
	leaq	112(%rbp,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB15_38:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB15_38
.LBB15_39:                              # %middle.block
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	je	.LBB15_42
.LBB15_40:                              # %.lr.ph.i.i53.preheader215
	movq	8(%rsp), %rcx           # 8-byte Reload
	subq	%rax, %rcx
	leaq	(%r14,%rax,4), %rdx
	leaq	(%rbp,%rax,4), %rax
	.p2align	4, 0x90
.LBB15_41:                              # %.lr.ph.i.i53
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$4, %rdx
	addq	$4, %rax
	decq	%rcx
	jne	.LBB15_41
.LBB15_42:                              # %._crit_edge.thread.i.i59
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB15_43:                              # %._crit_edge16.i.i60
	movq	%r14, 48(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, (%r14,%rax,4)
	movl	%ebx, 60(%rsp)
	movq	%r14, %rbp
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB15_44:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$42, (%rbp,%rax,4)
	leal	2(%r12), %eax
	movl	%eax, 56(%rsp)
	movl	$0, 8(%rbp,%r12,4)
.Ltmp84:
	leaq	152(%rsp), %rdi
	leaq	48(%rsp), %rsi
	callq	_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE
.Ltmp85:
# BB#45:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_47
# BB#46:
	callq	_ZdaPv
.LBB15_47:                              # %_ZN11CStringBaseIwED2Ev.exit
	cmpl	$8, %r15d
	movl	$16, %eax
	movl	$4, %ecx
	cmovgl	%eax, %ecx
	movl	%ecx, 68(%rsp)          # 4-byte Spill
	cmpl	$65, %r15d
	jl	.LBB15_49
# BB#48:                                # %select.true.sink421
	movl	%r15d, %eax
	shrl	$31, %eax
	addl	%r15d, %eax
	sarl	%eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
.LBB15_49:                              # %select.end420
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(,%rax,4), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	leal	3(%r12), %ecx
	movl	%ecx, 100(%rsp)         # 4-byte Spill
	leaq	-8(%rax), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	shrl	$3, %ecx
	incl	%ecx
	leaq	-1(%rax), %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	andq	$-8, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	andl	$3, %ecx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	negq	%rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%r12, 80(%rsp)          # 8-byte Spill
	jmp	.LBB15_113
	.p2align	4, 0x90
.LBB15_50:                              #   in Loop: Header=BB15_113 Depth=1
	testb	$16, 240(%rsp)
	jne	.LBB15_54
# BB#51:                                #   in Loop: Header=BB15_113 Depth=1
	testl	%r15d, %r15d
	je	.LBB15_73
# BB#52:                                # %._crit_edge16.i.i.i73
                                        #   in Loop: Header=BB15_113 Depth=1
.Ltmp97:
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	_Znam
	movq	%rax, %rbx
.Ltmp98:
# BB#53:                                # %.noexc82
                                        #   in Loop: Header=BB15_113 Depth=1
	movl	$0, (%rbx)
	movl	%r15d, %r12d
	movq	%rbx, %rbp
	jmp	.LBB15_74
	.p2align	4, 0x90
.LBB15_54:                              # %._crit_edge16.i.i.i63
                                        #   in Loop: Header=BB15_113 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
.Ltmp89:
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	_Znam
	movq	%rax, %rbx
.Ltmp90:
# BB#55:                                # %.noexc70
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	%rbx, 32(%rsp)
	movl	$0, (%rbx)
	movl	%r15d, 44(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB15_56:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i64
                                        #   Parent Loop BB15_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB15_56
# BB#57:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i67
                                        #   in Loop: Header=BB15_113 Depth=1
	movl	%r14d, 40(%rsp)
	movl	256(%rsp), %ebp
	testl	%ebp, %ebp
	jle	.LBB15_99
# BB#58:                                #   in Loop: Header=BB15_113 Depth=1
	movl	68(%rsp), %eax          # 4-byte Reload
	cmpl	%ebp, %eax
	cmovgel	%eax, %ebp
	addl	100(%rsp), %ebp         # 4-byte Folded Reload
	cmpl	%r15d, %ebp
	je	.LBB15_99
# BB#59:                                #   in Loop: Header=BB15_113 Depth=1
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp91:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp92:
# BB#60:                                # %.noexc98
                                        #   in Loop: Header=BB15_113 Depth=1
	cmpl	$-1, %r12d
	jl	.LBB15_98
# BB#61:                                # %.preheader.i.i89
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	32(%rsp), %rdi
	testl	%r12d, %r12d
	js	.LBB15_96
# BB#62:                                # %.lr.ph.i.i90.preheader
                                        #   in Loop: Header=BB15_113 Depth=1
	cmpl	$7, 16(%rsp)            # 4-byte Folded Reload
	jbe	.LBB15_66
# BB#63:                                # %min.iters.checked196
                                        #   in Loop: Header=BB15_113 Depth=1
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB15_66
# BB#64:                                # %vector.memcheck
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rdi,%rax,4), %rax
	cmpq	%rax, %r14
	jae	.LBB15_105
# BB#65:                                # %vector.memcheck
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%r14,%rax), %rax
	cmpq	%rax, %rdi
	jae	.LBB15_105
.LBB15_66:                              #   in Loop: Header=BB15_113 Depth=1
	xorl	%eax, %eax
.LBB15_67:                              # %.lr.ph.i.i90.preheader214
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%eax, %edx
	movq	136(%rsp), %rcx         # 8-byte Reload
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB15_70
# BB#68:                                # %.lr.ph.i.i90.prol.preheader
                                        #   in Loop: Header=BB15_113 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB15_69:                              # %.lr.ph.i.i90.prol
                                        #   Parent Loop BB15_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%r14,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB15_69
.LBB15_70:                              # %.lr.ph.i.i90.prol.loopexit
                                        #   in Loop: Header=BB15_113 Depth=1
	cmpq	$7, %rcx
	jb	.LBB15_97
# BB#71:                                # %.lr.ph.i.i90.preheader214.new
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	subq	%rax, %rcx
	leaq	28(%r14,%rax,4), %rdx
	leaq	28(%rdi,%rax,4), %rax
	.p2align	4, 0x90
.LBB15_72:                              # %.lr.ph.i.i90
                                        #   Parent Loop BB15_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB15_72
	jmp	.LBB15_97
.LBB15_73:                              #   in Loop: Header=BB15_113 Depth=1
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
.LBB15_74:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i74
                                        #   in Loop: Header=BB15_113 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB15_75:                              #   Parent Loop BB15_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB15_75
# BB#76:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i77
                                        #   in Loop: Header=BB15_113 Depth=1
	movl	256(%rsp), %eax
	movl	%r12d, %ecx
	subl	%r14d, %ecx
	cmpl	%eax, %ecx
	jg	.LBB15_80
# BB#77:                                #   in Loop: Header=BB15_113 Depth=1
	decl	%ecx
	cmpl	$8, %r12d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r12d
	jl	.LBB15_79
# BB#78:                                # %select.true.sink468
                                        #   in Loop: Header=BB15_113 Depth=1
	movl	%r12d, %edx
	shrl	$31, %edx
	addl	%r12d, %edx
	sarl	%edx
.LBB15_79:                              # %select.end467
                                        #   in Loop: Header=BB15_113 Depth=1
	leal	(%rdx,%rcx), %esi
	movl	%eax, %edi
	subl	%ecx, %edi
	cmpl	%eax, %esi
	cmovgel	%edx, %edi
	leal	1(%r12,%rdi), %eax
	cmpl	%r12d, %eax
	jne	.LBB15_81
.LBB15_80:                              #   in Loop: Header=BB15_113 Depth=1
	movq	%rbx, %r14
	movq	80(%rsp), %r12          # 8-byte Reload
	jmp	.LBB15_88
.LBB15_81:                              #   in Loop: Header=BB15_113 Depth=1
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp100:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp101:
# BB#82:                                # %.noexc112
                                        #   in Loop: Header=BB15_113 Depth=1
	testl	%r12d, %r12d
	movq	80(%rsp), %r12          # 8-byte Reload
	jle	.LBB15_87
# BB#83:                                # %.preheader.i.i103
                                        #   in Loop: Header=BB15_113 Depth=1
	testl	%r12d, %r12d
	js	.LBB15_85
# BB#84:                                # %.lr.ph.i.i104.preheader
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	88(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	jmp	.LBB15_86
.LBB15_85:                              # %._crit_edge.i.i105
                                        #   in Loop: Header=BB15_113 Depth=1
	testq	%rbp, %rbp
	je	.LBB15_87
.LBB15_86:                              # %._crit_edge.thread.i.i110
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB15_87:                              # %._crit_edge16.i.i111
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, (%r14,%rax,4)
	movq	%r14, %rbp
.LBB15_88:                              # %.noexc.i78
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rbp,%rax,4), %rcx
	movq	248(%rsp), %rsi
	.p2align	4, 0x90
.LBB15_89:                              #   Parent Loop BB15_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB15_89
# BB#90:                                # %.noexc44
                                        #   in Loop: Header=BB15_113 Depth=1
	testq	%rbp, %rbp
	je	.LBB15_116
# BB#91:                                #   in Loop: Header=BB15_113 Depth=1
	cmpl	$0, (%rbp)
	je	.LBB15_117
# BB#92:                                #   in Loop: Header=BB15_113 Depth=1
.Ltmp103:
	leaq	48(%rsp), %rdi
	movq	%rbp, %rsi
	callq	_Z17nameWindowToUnix2PKw
.Ltmp104:
# BB#93:                                # %.noexc.i
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	48(%rsp), %rbp
	movq	%rbp, %rdi
	callq	remove
	movl	%eax, %ebx
	testq	%rbp, %rbp
	je	.LBB15_95
# BB#94:                                #   in Loop: Header=BB15_113 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB15_95:                              # %_ZN8NWindows5NFile10NDirectoryL24RemoveDirectorySubItems2ERK11CStringBaseIwERKNS0_5NFind10CFileInfoWE.exit
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	%r14, %rdi
	callq	_ZdaPv
	testl	%ebx, %ebx
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB15_113
	jmp	.LBB15_118
.LBB15_96:                              # %._crit_edge.i.i91
                                        #   in Loop: Header=BB15_113 Depth=1
	testq	%rdi, %rdi
	je	.LBB15_98
.LBB15_97:                              # %._crit_edge.thread.i.i96
                                        #   in Loop: Header=BB15_113 Depth=1
	callq	_ZdaPv
.LBB15_98:                              # %._crit_edge16.i.i97
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	%r14, 32(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 44(%rsp)
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB15_99:                              # %.noexc.i68
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	32(%rsp), %rcx
	addq	88(%rsp), %rcx          # 8-byte Folded Reload
	movq	248(%rsp), %rsi
	.p2align	4, 0x90
.LBB15_100:                             #   Parent Loop BB15_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB15_100
# BB#101:                               # %.noexc43
                                        #   in Loop: Header=BB15_113 Depth=1
	movl	256(%rsp), %eax
	addl	%r14d, %eax
	movl	%eax, 40(%rsp)
.Ltmp94:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows5NFile10NDirectory27RemoveDirectoryWithSubItemsERK11CStringBaseIwE
	movl	%eax, %ebx
.Ltmp95:
# BB#102:                               #   in Loop: Header=BB15_113 Depth=1
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_104
# BB#103:                               #   in Loop: Header=BB15_113 Depth=1
	callq	_ZdaPv
.LBB15_104:                             # %_ZN11CStringBaseIwED2Ev.exit.i41
                                        #   in Loop: Header=BB15_113 Depth=1
	testb	%bl, %bl
	jne	.LBB15_113
	jmp	.LBB15_118
.LBB15_105:                             # %vector.body192.preheader
                                        #   in Loop: Header=BB15_113 Depth=1
	cmpq	$0, 120(%rsp)           # 8-byte Folded Reload
	je	.LBB15_108
# BB#106:                               # %vector.body192.prol.preheader
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_107:                             # %vector.body192.prol
                                        #   Parent Loop BB15_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rdx,4), %xmm0
	movups	16(%rdi,%rdx,4), %xmm1
	movups	%xmm0, (%r14,%rdx,4)
	movups	%xmm1, 16(%r14,%rdx,4)
	addq	$8, %rdx
	incq	%rax
	jne	.LBB15_107
	jmp	.LBB15_109
.LBB15_108:                             #   in Loop: Header=BB15_113 Depth=1
	xorl	%edx, %edx
.LBB15_109:                             # %vector.body192.prol.loopexit
                                        #   in Loop: Header=BB15_113 Depth=1
	cmpq	$24, 128(%rsp)          # 8-byte Folded Reload
	jb	.LBB15_112
# BB#110:                               # %vector.body192.preheader.new
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	subq	%rdx, %rax
	leaq	112(%r14,%rdx,4), %rcx
	leaq	112(%rdi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB15_111:                             # %vector.body192
                                        #   Parent Loop BB15_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-32, %rax
	jne	.LBB15_111
.LBB15_112:                             # %middle.block193
                                        #   in Loop: Header=BB15_113 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB15_67
	jmp	.LBB15_97
	.p2align	4, 0x90
.LBB15_113:                             # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_56 Depth 2
                                        #     Child Loop BB15_107 Depth 2
                                        #     Child Loop BB15_111 Depth 2
                                        #     Child Loop BB15_69 Depth 2
                                        #     Child Loop BB15_72 Depth 2
                                        #     Child Loop BB15_100 Depth 2
                                        #     Child Loop BB15_75 Depth 2
                                        #     Child Loop BB15_89 Depth 2
.Ltmp87:
	leaq	152(%rsp), %rdi
	leaq	208(%rsp), %rsi
	callq	_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWE
.Ltmp88:
# BB#114:                               #   in Loop: Header=BB15_113 Depth=1
	testb	%al, %al
	jne	.LBB15_50
# BB#115:
	xorl	%ebx, %ebx
	jmp	.LBB15_119
.LBB15_116:                             # %_ZN8NWindows5NFile10NDirectoryL24RemoveDirectorySubItems2ERK11CStringBaseIwERKNS0_5NFind10CFileInfoWE.exit.thread
	callq	__errno_location
	movl	$2, (%rax)
	jmp	.LBB15_118
.LBB15_117:                             # %_ZN8NWindows5NFile10NDirectoryL24RemoveDirectorySubItems2ERK11CStringBaseIwERKNS0_5NFind10CFileInfoWE.exit.thread144
	callq	__errno_location
	movl	$2, (%rax)
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB15_118:                             # %.loopexit
	movl	$1, %ebx
.LBB15_119:                             # %.loopexit
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	movq	144(%rsp), %rbp         # 8-byte Reload
	je	.LBB15_121
# BB#120:
	callq	_ZdaPv
.LBB15_121:                             # %_ZN11CStringBaseIwED2Ev.exit.i28
.Ltmp109:
	leaq	152(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
.Ltmp110:
# BB#122:
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_124
# BB#123:
	callq	_ZdaPv
.LBB15_124:                             # %_ZN11CStringBaseIcED2Ev.exit.i.i29
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_126
# BB#125:
	callq	_ZdaPv
.LBB15_126:                             # %_ZN8NWindows5NFile5NFind12CEnumeratorWD2Ev.exit34
	testl	%ebx, %ebx
	jne	.LBB15_127
# BB#128:
	movq	(%rbp), %rdi
.Ltmp112:
	xorl	%esi, %esi
	callq	_ZN8NWindows5NFile10NDirectory19MySetFileAttributesEPKwj
.Ltmp113:
# BB#129:
	testb	%al, %al
	je	.LBB15_127
# BB#130:
	movq	(%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB15_136
# BB#131:
	cmpl	$0, (%rsi)
	je	.LBB15_136
# BB#132:
.Ltmp114:
	leaq	264(%rsp), %rdi
	callq	_Z17nameWindowToUnix2PKw
.Ltmp115:
# BB#133:                               # %.noexc36
	movq	264(%rsp), %rbp
	movq	%rbp, %rdi
	callq	rmdir
	testl	%eax, %eax
	sete	%bl
	testq	%rbp, %rbp
	je	.LBB15_137
# BB#134:
	movq	%rbp, %rdi
	callq	_ZdaPv
	testq	%r13, %r13
	jne	.LBB15_138
	jmp	.LBB15_139
.LBB15_127:
	xorl	%ebx, %ebx
	testq	%r13, %r13
	jne	.LBB15_138
	jmp	.LBB15_139
.LBB15_136:
	callq	__errno_location
	movl	$2, (%rax)
	xorl	%ebx, %ebx
.LBB15_137:                             # %_ZN8NWindows5NFile10NDirectory17MyRemoveDirectoryEPKw.exit
	testq	%r13, %r13
	je	.LBB15_139
.LBB15_138:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB15_139:                             # %_ZN11CStringBaseIwED2Ev.exit37
	movq	248(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_141
# BB#140:
	callq	_ZdaPv
.LBB15_141:                             # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit39
	movl	%ebx, %eax
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_142:
.Ltmp77:
	movq	%rax, %r15
	testq	%r13, %r13
	je	.LBB15_172
# BB#143:
	movq	%r14, %rdi
	jmp	.LBB15_171
.LBB15_144:
.Ltmp116:
	jmp	.LBB15_154
.LBB15_145:
.Ltmp83:
	movq	%rax, %r15
	movq	%rbp, %rdi
	jmp	.LBB15_152
.LBB15_146:
.Ltmp74:
	movq	%rax, %r15
	jmp	.LBB15_172
.LBB15_147:
.Ltmp111:
	movq	%rax, %r15
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_149
# BB#148:
	callq	_ZdaPv
.LBB15_149:                             # %_ZN11CStringBaseIcED2Ev.exit3.i.i30
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB15_152
	jmp	.LBB15_169
.LBB15_151:
.Ltmp86:
	movq	%rax, %r15
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_169
.LBB15_152:
	callq	_ZdaPv
	testq	%r13, %r13
	jne	.LBB15_170
	jmp	.LBB15_172
.LBB15_153:
.Ltmp80:
.LBB15_154:
	movq	%rax, %r15
	testq	%r13, %r13
	jne	.LBB15_170
	jmp	.LBB15_172
.LBB15_155:
.Ltmp102:
	movq	%rax, %r15
	testq	%rbp, %rbp
	jne	.LBB15_157
	jmp	.LBB15_162
.LBB15_156:
.Ltmp93:
	movq	%rax, %r15
.LBB15_157:
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB15_162
.LBB15_158:                             # %_ZN11CStringBaseIwED2Ev.exit11.i
.Ltmp105:
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB15_162
.LBB15_159:
.Ltmp96:
	movq	%rax, %r15
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_162
# BB#160:
	callq	_ZdaPv
	jmp	.LBB15_162
.LBB15_161:
.Ltmp99:
	movq	%rax, %r15
.LBB15_162:                             # %.body45
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_164
# BB#163:
	callq	_ZdaPv
.LBB15_164:                             # %_ZN11CStringBaseIwED2Ev.exit.i25
.Ltmp106:
	leaq	152(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
.Ltmp107:
# BB#165:
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_167
# BB#166:
	callq	_ZdaPv
.LBB15_167:                             # %_ZN11CStringBaseIcED2Ev.exit.i.i
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_169
# BB#168:
	callq	_ZdaPv
.LBB15_169:
	testq	%r13, %r13
	je	.LBB15_172
.LBB15_170:
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB15_171:                             # %_ZN11CStringBaseIwED2Ev.exit40
	callq	_ZdaPv
.LBB15_172:                             # %_ZN11CStringBaseIwED2Ev.exit40
	movq	248(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_174
# BB#173:
	callq	_ZdaPv
.LBB15_174:                             # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB15_175:
.Ltmp108:
	movq	%rax, %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_177
# BB#176:
	callq	_ZdaPv
.LBB15_177:                             # %_ZN11CStringBaseIcED2Ev.exit3.i.i
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_179
# BB#178:
	callq	_ZdaPv
.LBB15_179:                             # %.body26
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN8NWindows5NFile10NDirectory27RemoveDirectoryWithSubItemsERK11CStringBaseIwE, .Lfunc_end15-_ZN8NWindows5NFile10NDirectory27RemoveDirectoryWithSubItemsERK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Lfunc_begin9-.Lfunc_begin9 # >> Call Site 1 <<
	.long	.Ltmp72-.Lfunc_begin9   #   Call between .Lfunc_begin9 and .Ltmp72
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin9   # >> Call Site 2 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin9   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin9   # >> Call Site 3 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin9   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin9   # >> Call Site 4 <<
	.long	.Ltmp78-.Ltmp76         #   Call between .Ltmp76 and .Ltmp78
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin9   # >> Call Site 5 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin9   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin9   # >> Call Site 6 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin9   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin9   # >> Call Site 7 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin9   #     jumps to .Ltmp86
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin9   # >> Call Site 8 <<
	.long	.Ltmp90-.Ltmp97         #   Call between .Ltmp97 and .Ltmp90
	.long	.Ltmp99-.Lfunc_begin9   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin9   # >> Call Site 9 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin9   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin9  # >> Call Site 10 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin9  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin9  # >> Call Site 11 <<
	.long	.Ltmp103-.Ltmp101       #   Call between .Ltmp101 and .Ltmp103
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin9  # >> Call Site 12 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin9  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin9   # >> Call Site 13 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin9   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin9   # >> Call Site 14 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp99-.Lfunc_begin9   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin9  # >> Call Site 15 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin9  #     jumps to .Ltmp111
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin9  # >> Call Site 16 <<
	.long	.Ltmp115-.Ltmp112       #   Call between .Ltmp112 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin9  #     jumps to .Ltmp116
	.byte	0                       #   On action: cleanup
	.long	.Ltmp106-.Lfunc_begin9  # >> Call Site 17 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin9  #     jumps to .Ltmp108
	.byte	1                       #   On action: 1
	.long	.Ltmp107-.Lfunc_begin9  # >> Call Site 18 <<
	.long	.Lfunc_end15-.Ltmp107   #   Call between .Ltmp107 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.zero	16
	.section	.text._ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE,"axG",@progbits,_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE,comdat
	.weak	_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE,@function
_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE: # @_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r15
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi151:
	.cfi_def_cfa_offset 48
.Lcfi152:
	.cfi_offset %rbx, -40
.Lcfi153:
	.cfi_offset %r12, -32
.Lcfi154:
	.cfi_offset %r14, -24
.Lcfi155:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
	movq	%r15, 8(%rbx)
	movb	$0, (%r15)
	movl	$4, 20(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
.Ltmp117:
	movl	$4, %edi
	callq	_Znam
.Ltmp118:
# BB#1:                                 # %_ZN8NWindows5NFile5NFind9CFindFileC2Ev.exit
	movq	%rax, 24(%rbx)
	movb	$0, (%rax)
	movl	$4, 36(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbx)
	movslq	8(%r14), %r12
	leaq	1(%r12), %r15
	testl	%r15d, %r15d
	je	.LBB16_2
# BB#3:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp120:
	callq	_Znam
.Ltmp121:
# BB#4:                                 # %.noexc
	movq	%rax, 40(%rbx)
	movl	$0, (%rax)
	movl	%r15d, 52(%rbx)
	jmp	.LBB16_5
.LBB16_2:
	xorl	%eax, %eax
.LBB16_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r14), %rcx
	.p2align	4, 0x90
.LBB16_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB16_6
# BB#7:
	movl	%r12d, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB16_8:
.Ltmp122:
	movq	%rax, %r14
.Ltmp123:
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
.Ltmp124:
# BB#9:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB16_11
# BB#10:
	callq	_ZdaPv
.LBB16_11:                              # %_ZN11CStringBaseIcED2Ev.exit.i3
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB16_18
	jmp	.LBB16_19
.LBB16_12:
.Ltmp125:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB16_14
# BB#13:
	callq	_ZdaPv
.LBB16_14:                              # %_ZN11CStringBaseIcED2Ev.exit3.i
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB16_16
# BB#15:
	callq	_ZdaPv
.LBB16_16:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB16_17:                              # %_ZN11CStringBaseIcED2Ev.exit.i
.Ltmp119:
	movq	%rax, %r14
	movq	%r15, %rdi
.LBB16_18:                              # %unwind_resume
	callq	_ZdaPv
.LBB16_19:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE, .Lfunc_end16-_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin10-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp117-.Lfunc_begin10 #   Call between .Lfunc_begin10 and .Ltmp117
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp118-.Ltmp117       #   Call between .Ltmp117 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin10 #     jumps to .Ltmp119
	.byte	0                       #   On action: cleanup
	.long	.Ltmp120-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin10 #     jumps to .Ltmp122
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin10 #     jumps to .Ltmp125
	.byte	1                       #   On action: 1
	.long	.Ltmp124-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Lfunc_end16-.Ltmp124   #   Call between .Ltmp124 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwE,@function
_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwE: # @_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi156:
	.cfi_def_cfa_offset 16
	leaq	4(%rsp), %rdx
	callq	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi
	popq	%rcx
	retq
.Lfunc_end17:
	.size	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwE, .Lfunc_end17-_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwE
	.cfi_endproc

	.globl	_ZN8NWindows5NFile10NDirectory12MySearchPathEPKwS3_S3_R11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory12MySearchPathEPKwS3_S3_R11CStringBaseIwE,@function
_ZN8NWindows5NFile10NDirectory12MySearchPathEPKwS3_S3_R11CStringBaseIwE: # @_ZN8NWindows5NFile10NDirectory12MySearchPathEPKwS3_S3_R11CStringBaseIwE
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%rbp
.Lcfi157:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi158:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi160:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi162:
	.cfi_def_cfa_offset 96
.Lcfi163:
	.cfi_offset %rbx, -48
.Lcfi164:
	.cfi_offset %r12, -40
.Lcfi165:
	.cfi_offset %r14, -32
.Lcfi166:
	.cfi_offset %r15, -24
.Lcfi167:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	testq	%rdi, %rdi
	jne	.LBB18_1
# BB#3:
	testq	%rdx, %rdx
	jne	.LBB18_4
# BB#5:
	testq	%r15, %r15
	je	.LBB18_6
# BB#7:
	movl	$.L.str.5, %edi
	callq	getenv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB18_56
# BB#8:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB18_9:                               # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB18_9
# BB#10:                                # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	1(%r12), %eax
	movslq	%eax, %rbp
	cmpl	$-1, %r12d
	movq	$-1, %rdi
	cmovgeq	%rbp, %rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	%ebp, 12(%rsp)
	.p2align	4, 0x90
.LBB18_11:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB18_11
# BB#12:                                # %_ZN11CStringBaseIcEC2EPKc.exit
	movl	%r12d, 8(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movl	$-1, %ebx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB18_13:                              # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB18_13
# BB#14:                                # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbx), %ebp
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp126:
	callq	_Znam
.Ltmp127:
# BB#15:                                # %.noexc
	movq	%rax, 32(%rsp)
	movl	$0, (%rax)
	movl	%ebp, 44(%rsp)
	.p2align	4, 0x90
.LBB18_16:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15), %ecx
	addq	$4, %r15
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB18_16
# BB#17:
	movl	%ebx, 40(%rsp)
.Ltmp129:
	leaq	16(%rsp), %rdi
	leaq	32(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp130:
# BB#18:
.Ltmp132:
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN11CStringBaseIcEpLERKS0_
.Ltmp133:
# BB#19:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_21
# BB#20:
	callq	_ZdaPv
.LBB18_21:                              # %_ZN11CStringBaseIcED2Ev.exit
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_23
# BB#22:
	callq	_ZdaPv
.LBB18_23:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	(%rsp), %rdi
	movl	$.L.str.6, %esi
	callq	fopen64
	testq	%rax, %rax
	je	.LBB18_54
# BB#24:
	movq	%rax, %rdi
	callq	fclose
.Ltmp135:
	leaq	16(%rsp), %rbx
	movq	%rsp, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp136:
# BB#25:
	cmpq	%r14, %rbx
	je	.LBB18_26
# BB#27:
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movslq	24(%rsp), %r15
	incq	%r15
	movl	12(%r14), %ebp
	cmpl	%ebp, %r15d
	je	.LBB18_33
# BB#28:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp138:
	callq	_Znam
	movq	%rax, %r12
.Ltmp139:
# BB#29:                                # %.noexc35
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB18_32
# BB#30:                                # %.noexc35
	testl	%ebp, %ebp
	jle	.LBB18_32
# BB#31:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB18_32:                              # %._crit_edge16.i.i
	movq	%r12, (%r14)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
	movq	%r12, %rbx
.LBB18_33:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i32
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB18_34:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB18_34
# BB#35:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	24(%rsp), %eax
	movl	%eax, 8(%r14)
	testq	%rdi, %rdi
	jne	.LBB18_37
	jmp	.LBB18_38
.LBB18_54:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_56
# BB#55:
	callq	_ZdaPv
.LBB18_56:                              # %_ZN11CStringBaseIcED2Ev.exit41
	xorl	%eax, %eax
	jmp	.LBB18_57
.LBB18_26:                              # %..critedge_crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_38
.LBB18_37:
	callq	_ZdaPv
.LBB18_38:                              # %_ZN11CStringBaseIwED2Ev.exit36
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_40
# BB#39:
	callq	_ZdaPv
.LBB18_40:                              # %_ZN11CStringBaseIcED2Ev.exit37
	movb	$1, %al
.LBB18_57:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_1:
	movl	$.Lstr.2, %edi
	jmp	.LBB18_2
.LBB18_4:
	movl	$.Lstr.1, %edi
	jmp	.LBB18_2
.LBB18_6:
	movl	$.Lstr, %edi
.LBB18_2:
	callq	puts
	movl	$1, %edi
	callq	exit
.LBB18_52:
.Ltmp140:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_43
# BB#53:
	callq	_ZdaPv
	jmp	.LBB18_43
.LBB18_51:
.Ltmp137:
	jmp	.LBB18_42
.LBB18_47:
.Ltmp134:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_49
# BB#48:
	callq	_ZdaPv
	jmp	.LBB18_49
.LBB18_46:
.Ltmp131:
	movq	%rax, %rbx
.LBB18_49:                              # %_ZN11CStringBaseIcED2Ev.exit38
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_43
# BB#50:
	callq	_ZdaPv
	jmp	.LBB18_43
.LBB18_41:
.Ltmp128:
.LBB18_42:
	movq	%rax, %rbx
.LBB18_43:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_45
# BB#44:
	callq	_ZdaPv
.LBB18_45:                              # %_ZN11CStringBaseIcED2Ev.exit31
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZN8NWindows5NFile10NDirectory12MySearchPathEPKwS3_S3_R11CStringBaseIwE, .Lfunc_end18-_ZN8NWindows5NFile10NDirectory12MySearchPathEPKwS3_S3_R11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp126-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp126
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin11 #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin11 #     jumps to .Ltmp131
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp133-.Ltmp132       #   Call between .Ltmp132 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin11 #     jumps to .Ltmp134
	.byte	0                       #   On action: cleanup
	.long	.Ltmp135-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin11 #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin11 # >> Call Site 6 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin11 #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin11 # >> Call Site 7 <<
	.long	.Lfunc_end18-.Ltmp139   #   Call between .Ltmp139 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLERKS0_,"axG",@progbits,_ZN11CStringBaseIcEpLERKS0_,comdat
	.weak	_ZN11CStringBaseIcEpLERKS0_
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLERKS0_,@function
_ZN11CStringBaseIcEpLERKS0_:            # @_ZN11CStringBaseIcEpLERKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi171:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi172:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi174:
	.cfi_def_cfa_offset 64
.Lcfi175:
	.cfi_offset %rbx, -56
.Lcfi176:
	.cfi_offset %r12, -48
.Lcfi177:
	.cfi_offset %r13, -40
.Lcfi178:
	.cfi_offset %r14, -32
.Lcfi179:
	.cfi_offset %r15, -24
.Lcfi180:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	8(%r14), %eax
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %ecx
	subl	%ebp, %ecx
	cmpl	%eax, %ecx
	jg	.LBB19_28
# BB#1:
	decl	%ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB19_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB19_3:                               # %select.end
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%rsi,%rbx), %r12d
	cmpl	%ebx, %r12d
	je	.LBB19_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r12d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	testl	%ebx, %ebx
	jle	.LBB19_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB19_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB19_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB19_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r15
	jae	.LBB19_17
# BB#16:                                # %vector.memcheck
	leaq	(%r15,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB19_17
.LBB19_7:
	xorl	%ecx, %ecx
.LBB19_8:                               # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB19_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB19_10:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r15,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB19_10
.LBB19_11:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB19_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB19_13:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB19_13
	jmp	.LBB19_26
.LBB19_25:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB19_27
.LBB19_26:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB19_27:
	movq	%r15, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r15,%rax)
	movl	%r12d, 12(%r13)
.LBB19_28:                              # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movslq	%ebp, %rax
	addq	(%r13), %rax
	movq	(%r14), %rcx
	.p2align	4, 0x90
.LBB19_29:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB19_29
# BB#30:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit
	movl	8(%r14), %eax
	addl	%eax, 8(%r13)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_17:                              # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB19_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r15,%rbx)
	movups	%xmm1, 16(%r15,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB19_20
	jmp	.LBB19_21
.LBB19_18:
	xorl	%ebx, %ebx
.LBB19_21:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB19_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r15,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB19_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB19_23
.LBB19_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB19_8
	jmp	.LBB19_26
.Lfunc_end19:
	.size	_ZN11CStringBaseIcEpLERKS0_, .Lfunc_end19-_ZN11CStringBaseIcEpLERKS0_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI20_0:
	.long	99                      # 0x63
	.long	58                      # 0x3a
	.long	47                      # 0x2f
	.long	116                     # 0x74
.LCPI20_1:
	.long	109                     # 0x6d
	.long	112                     # 0x70
	.long	47                      # 0x2f
	.long	0                       # 0x0
	.text
	.globl	_ZN8NWindows5NFile10NDirectory13MyGetTempPathER11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory13MyGetTempPathER11CStringBaseIwE,@function
_ZN8NWindows5NFile10NDirectory13MyGetTempPathER11CStringBaseIwE: # @_ZN8NWindows5NFile10NDirectory13MyGetTempPathER11CStringBaseIwE
	.cfi_startproc
# BB#0:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	pushq	%rbp
.Lcfi181:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi182:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi183:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi184:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi185:
	.cfi_def_cfa_offset 48
.Lcfi186:
	.cfi_offset %rbx, -40
.Lcfi187:
	.cfi_offset %r14, -32
.Lcfi188:
	.cfi_offset %r15, -24
.Lcfi189:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movl	12(%r14), %ebp
	cmpl	$8, %ebp
	je	.LBB20_5
# BB#1:
	movl	$32, %edi
	callq	_Znam
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB20_4
# BB#2:
	testl	%ebp, %ebp
	jle	.LBB20_4
# BB#3:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB20_4:                               # %._crit_edge16.i.i
	movq	%r15, (%r14)
	movl	$0, (%r15,%rax,4)
	movl	$8, 12(%r14)
	movq	%r15, %rbx
.LBB20_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
	movaps	.LCPI20_0(%rip), %xmm0  # xmm0 = [99,58,47,116]
	movups	%xmm0, (%rbx)
	movaps	.LCPI20_1(%rip), %xmm0  # xmm0 = [109,112,47,0]
	movups	%xmm0, 16(%rbx)
	movl	$7, 8(%r14)
	movb	$1, %al
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZN8NWindows5NFile10NDirectory13MyGetTempPathER11CStringBaseIwE, .Lfunc_end20-_ZN8NWindows5NFile10NDirectory13MyGetTempPathER11CStringBaseIwE
	.cfi_endproc

	.section	.text._ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,"axG",@progbits,_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,comdat
	.weak	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,@function
_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev: # @_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.cfi_startproc
# BB#0:
	jmp	pthread_mutex_destroy   # TAILCALL
.Lfunc_end21:
	.size	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev, .Lfunc_end21-_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI22_0:
	.long	46                      # 0x2e
	.long	116                     # 0x74
	.long	109                     # 0x6d
	.long	112                     # 0x70
	.text
	.globl	_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwE,@function
_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwE: # @_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwE
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%rbp
.Lcfi190:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi191:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi193:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi194:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi196:
	.cfi_def_cfa_offset 224
.Lcfi197:
	.cfi_offset %rbx, -56
.Lcfi198:
	.cfi_offset %r12, -48
.Lcfi199:
	.cfi_offset %r13, -40
.Lcfi200:
	.cfi_offset %r14, -32
.Lcfi201:
	.cfi_offset %r15, -24
.Lcfi202:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	$_ZN8NWindows5NFile10NDirectoryL22g_CountCriticalSectionE, %edi
	callq	pthread_mutex_lock
	movl	_ZZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwEE10memo_count(%rip), %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leal	1(%rax), %eax
	movl	%eax, _ZZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwEE10memo_count(%rip)
	movl	$_ZN8NWindows5NFile10NDirectoryL22g_CountCriticalSectionE, %edi
	callq	pthread_mutex_unlock
	cmpb	$0, (%r12)
	je	.LBB22_7
# BB#1:
	movq	8(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB22_3
# BB#2:
	cmpl	$0, (%rsi)
	je	.LBB22_3
# BB#4:
	leaq	32(%rsp), %rdi
	callq	_Z17nameWindowToUnix2PKw
	movq	32(%rsp), %r15
	movq	%r15, %rdi
	callq	remove
	testl	%eax, %eax
	sete	%r14b
	testq	%r15, %r15
	je	.LBB22_6
# BB#5:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB22_6
.LBB22_3:
	callq	__errno_location
	movl	$2, (%rax)
	xorl	%r14d, %r14d
.LBB22_6:                               # %_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw.exit.i
	xorb	$1, %r14b
	movb	%r14b, (%r12)
.LBB22_7:                               # %_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv.exit
	movq	%r12, 16(%rsp)          # 8-byte Spill
	callq	getpid
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	$0, 8(%r13)
	movq	(%r13), %r15
	movl	$0, (%r15)
	xorl	%r14d, %r14d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB22_8:                               # =>This Inner Loop Header: Depth=1
	incl	%r14d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB22_8
# BB#9:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	movl	12(%r13), %r12d
	cmpl	%r14d, %r12d
	jne	.LBB22_11
# BB#10:
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB22_16
.LBB22_11:
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	xorl	%ecx, %ecx
	testq	%r15, %r15
	je	.LBB22_12
# BB#13:
	testl	%r12d, %r12d
	movq	16(%rsp), %r12          # 8-byte Reload
	jle	.LBB22_15
# BB#14:                                # %._crit_edge.thread.i.i
	movq	%r15, %rdi
	movq	%rax, %r15
	callq	_ZdaPv
	movq	%r15, %rax
	movslq	8(%r13), %rcx
	jmp	.LBB22_15
.LBB22_12:
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB22_15:                              # %._crit_edge16.i.i
	movq	%rax, (%r13)
	movl	$0, (%rax,%rcx,4)
	movl	%r14d, 12(%r13)
	movq	%rax, %r15
.LBB22_16:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
	decl	%r14d
	.p2align	4, 0x90
.LBB22_17:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%r15)
	addq	$4, %r15
	testl	%eax, %eax
	jne	.LBB22_17
# BB#18:                                # %_ZN11CStringBaseIwEaSEPKw.exit
	movl	%r14d, 8(%r13)
	movl	$-1, %ebx
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB22_19:                              # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB22_19
# BB#20:                                # %_Z11MyStringLenIwEiPKT_.exit.i21
	movq	%r13, %rdi
	movl	%ebx, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
	movslq	8(%r13), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r13), %rcx
	.p2align	4, 0x90
.LBB22_21:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %edx
	addq	$4, %rbp
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB22_21
# BB#22:                                # %_ZN11CStringBaseIwEpLEPKw.exit
	addl	%eax, %ebx
	movl	%ebx, 8(%r13)
	movl	$1, %esi
	movq	%r13, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	movl	$35, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movl	$0, 4(%rax,%rcx,4)
	leaq	32(%rsp), %rbp
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%rbp, %rsi
	callq	_Z21ConvertUInt32ToStringjPw
	movl	$-1, %ebx
	movabsq	$4294967296, %r15       # imm = 0x100000000
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_23:                              # =>This Inner Loop Header: Depth=1
	addq	%r15, %rax
	incl	%ebx
	cmpl	$0, (%rbp)
	leaq	4(%rbp), %rbp
	jne	.LBB22_23
# BB#24:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
	movl	$0, (%rbp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_25:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	32(%rsp,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB22_25
# BB#26:                                # %_ZN8NWindows5NFile10NDirectoryL25CSysConvertUInt32ToStringEj.exit
.Ltmp141:
	movq	%r13, %rdi
	movl	%ebx, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp142:
# BB#27:                                # %.noexc
	movslq	8(%r13), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r13), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB22_28:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rdx), %esi
	movl	%esi, (%rcx,%rdx)
	addq	$4, %rdx
	testl	%esi, %esi
	jne	.LBB22_28
# BB#29:                                # %_ZN11CStringBaseIwED2Ev.exit
	addl	%eax, %ebx
	movl	%ebx, 8(%r13)
	movq	%rbp, %rdi
	callq	_ZdaPv
	movl	$1, %esi
	movq	%r13, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	movl	$64, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movl	$0, 4(%rax,%rcx,4)
	leaq	32(%rsp), %rbp
	movq	24(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%rbp, %rsi
	callq	_Z21ConvertUInt32ToStringjPw
	movl	$-1, %ebx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_30:                              # =>This Inner Loop Header: Depth=1
	addq	%r15, %rax
	incl	%ebx
	cmpl	$0, (%rbp)
	leaq	4(%rbp), %rbp
	jne	.LBB22_30
# BB#31:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i28
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
	movl	$0, (%rbp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i31
                                        # =>This Inner Loop Header: Depth=1
	movl	32(%rsp,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB22_32
# BB#33:                                # %_ZN8NWindows5NFile10NDirectoryL25CSysConvertUInt32ToStringEj.exit32
.Ltmp144:
	movq	%r13, %rdi
	movl	%ebx, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp145:
# BB#34:                                # %.noexc35
	movslq	8(%r13), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r13), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB22_35:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rdx), %esi
	movl	%esi, (%rcx,%rdx)
	addq	$4, %rdx
	testl	%esi, %esi
	jne	.LBB22_35
# BB#36:                                # %_ZN11CStringBaseIwED2Ev.exit37
	addl	%eax, %ebx
	movl	%ebx, 8(%r13)
	movq	%rbp, %rdi
	callq	_ZdaPv
	movl	$4, %esi
	movq	%r13, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	movaps	.LCPI22_0(%rip), %xmm0  # xmm0 = [46,116,109,112]
	movups	%xmm0, (%rax,%rcx,4)
	movl	$0, 16(%rax,%rcx,4)
	leal	4(%rcx), %ecx
	movl	%ecx, 8(%r13)
	leaq	8(%r12), %rcx
	cmpq	%r13, %rcx
	je	.LBB22_44
# BB#37:
	movl	$0, 16(%r12)
	movq	8(%r12), %rbx
	movl	$0, (%rbx)
	movslq	8(%r13), %rbp
	incq	%rbp
	movl	20(%r12), %r14d
	cmpl	%r14d, %ebp
	je	.LBB22_42
# BB#38:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB22_41
# BB#39:
	testl	%r14d, %r14d
	jle	.LBB22_41
# BB#40:                                # %._crit_edge.thread.i.i47
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	16(%r12), %rax
.LBB22_41:                              # %._crit_edge16.i.i48
	movq	%r15, 8(%r12)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 20(%r12)
	movq	(%r13), %rax
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB22_42:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i49
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB22_42
# BB#43:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%r13), %eax
	movl	%eax, 16(%r12)
.LBB22_44:                              # %_ZN11CStringBaseIwEaSERKS0_.exit
	movb	$1, (%r12)
	movl	12(%rsp), %eax          # 4-byte Reload
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_46:                              # %_ZN11CStringBaseIwED2Ev.exit53
.Ltmp146:
	jmp	.LBB22_47
.LBB22_45:                              # %_ZN11CStringBaseIwED2Ev.exit52
.Ltmp143:
.LBB22_47:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwE, .Lfunc_end22-_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin12-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp141-.Lfunc_begin12 #   Call between .Lfunc_begin12 and .Ltmp141
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin12 #     jumps to .Ltmp143
	.byte	0                       #   On action: cleanup
	.long	.Ltmp142-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp144-.Ltmp142       #   Call between .Ltmp142 and .Ltmp144
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin12 #     jumps to .Ltmp146
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Lfunc_end22-.Ltmp145   #   Call between .Ltmp145 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv,@function
_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv: # @_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi203:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi204:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi205:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi206:
	.cfi_def_cfa_offset 48
.Lcfi207:
	.cfi_offset %rbx, -32
.Lcfi208:
	.cfi_offset %r14, -24
.Lcfi209:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpb	$0, (%rbx)
	je	.LBB23_1
# BB#2:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB23_4
# BB#3:
	cmpl	$0, (%rsi)
	je	.LBB23_4
# BB#5:
	movq	%rsp, %rdi
	callq	_Z17nameWindowToUnix2PKw
	movq	(%rsp), %r14
	movq	%r14, %rdi
	callq	remove
	testl	%eax, %eax
	sete	%bpl
	testq	%r14, %r14
	je	.LBB23_7
# BB#6:
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB23_7
.LBB23_1:
	movb	$1, %bpl
	jmp	.LBB23_8
.LBB23_4:
	callq	__errno_location
	movl	$2, (%rax)
	xorl	%ebp, %ebp
.LBB23_7:                               # %_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw.exit
	movl	%ebp, %eax
	xorb	$1, %al
	movb	%al, (%rbx)
.LBB23_8:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end23:
	.size	_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv, .Lfunc_end23-_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI24_0:
	.long	99                      # 0x63
	.long	58                      # 0x3a
	.long	47                      # 0x2f
	.long	116                     # 0x74
.LCPI24_1:
	.long	109                     # 0x6d
	.long	112                     # 0x70
	.long	47                      # 0x2f
	.long	0                       # 0x0
	.text
	.globl	_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwR11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwR11CStringBaseIwE,@function
_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwR11CStringBaseIwE: # @_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwR11CStringBaseIwE
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%rbp
.Lcfi210:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi211:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi212:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi213:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi214:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi215:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi216:
	.cfi_def_cfa_offset 64
.Lcfi217:
	.cfi_offset %rbx, -56
.Lcfi218:
	.cfi_offset %r12, -48
.Lcfi219:
	.cfi_offset %r13, -40
.Lcfi220:
	.cfi_offset %r14, -32
.Lcfi221:
	.cfi_offset %r15, -24
.Lcfi222:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
.Ltmp147:
	movl	$32, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp148:
# BB#1:
	movq	%r13, %rdi
	callq	_ZdaPv
	movaps	.LCPI24_0(%rip), %xmm0  # xmm0 = [99,58,47,116]
	movups	%xmm0, (%rbx)
	movaps	.LCPI24_1(%rip), %xmm0  # xmm0 = [109,112,47,0]
	movups	%xmm0, 16(%rbx)
.Ltmp149:
	movq	%rbx, %r13
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwE
.Ltmp150:
# BB#2:                                 # %_ZN11CStringBaseIwED2Ev.exit5
	testl	%eax, %eax
	setne	%bpl
	movq	%rbx, %rdi
	callq	_ZdaPv
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_3:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp151:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end24:
	.size	_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwR11CStringBaseIwE, .Lfunc_end24-_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwR11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp147-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp147
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp147-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp150-.Ltmp147       #   Call between .Ltmp147 and .Ltmp150
	.long	.Ltmp151-.Lfunc_begin13 #     jumps to .Ltmp151
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Lfunc_end24-.Ltmp150   #   Call between .Ltmp150 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI25_0:
	.long	99                      # 0x63
	.long	58                      # 0x3a
	.long	47                      # 0x2f
	.long	116                     # 0x74
.LCPI25_1:
	.long	109                     # 0x6d
	.long	112                     # 0x70
	.long	47                      # 0x2f
	.long	0                       # 0x0
	.text
	.globl	_ZN8NWindows5NFile10NDirectory19CreateTempDirectoryEPKwR11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory19CreateTempDirectoryEPKwR11CStringBaseIwE,@function
_ZN8NWindows5NFile10NDirectory19CreateTempDirectoryEPKwR11CStringBaseIwE: # @_ZN8NWindows5NFile10NDirectory19CreateTempDirectoryEPKwR11CStringBaseIwE
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%rbp
.Lcfi223:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi224:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi225:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi226:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi227:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi228:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi229:
	.cfi_def_cfa_offset 96
.Lcfi230:
	.cfi_offset %rbx, -56
.Lcfi231:
	.cfi_offset %r12, -48
.Lcfi232:
	.cfi_offset %r13, -40
.Lcfi233:
	.cfi_offset %r14, -32
.Lcfi234:
	.cfi_offset %r15, -24
.Lcfi235:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB25_1:                               # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movb	$0, (%rsp)
	movq	$0, 16(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, 8(%rsp)
	movl	$0, (%rax)
	movl	$4, 20(%rsp)
.Ltmp152:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp153:
# BB#2:                                 # %.noexc
                                        #   in Loop: Header=BB25_1 Depth=1
	movl	$0, (%rbx)
.Ltmp154:
	movl	$32, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp155:
# BB#3:                                 #   in Loop: Header=BB25_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movaps	.LCPI25_0(%rip), %xmm0  # xmm0 = [99,58,47,116]
	movups	%xmm0, (%rbp)
	movaps	.LCPI25_1(%rip), %xmm0  # xmm0 = [109,112,47,0]
	movups	%xmm0, 16(%rbp)
.Ltmp156:
	movq	%rbp, %rbx
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	movq	%r13, %rcx
	callq	_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwE
	movl	%eax, %r12d
.Ltmp157:
# BB#4:                                 #   in Loop: Header=BB25_1 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
	testl	%r12d, %r12d
	je	.LBB25_9
# BB#5:                                 #   in Loop: Header=BB25_1 Depth=1
	cmpb	$0, (%rsp)
	je	.LBB25_17
# BB#6:                                 #   in Loop: Header=BB25_1 Depth=1
	movq	8(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB25_8
# BB#7:                                 #   in Loop: Header=BB25_1 Depth=1
	cmpl	$0, (%rsi)
	je	.LBB25_8
# BB#13:                                #   in Loop: Header=BB25_1 Depth=1
.Ltmp159:
	leaq	24(%rsp), %rdi
	callq	_Z17nameWindowToUnix2PKw
.Ltmp160:
# BB#14:                                # %.noexc8
                                        #   in Loop: Header=BB25_1 Depth=1
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	callq	remove
	movl	%eax, %ebp
	testq	%rbx, %rbx
	je	.LBB25_16
# BB#15:                                #   in Loop: Header=BB25_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB25_16:                              # %_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv.exit
                                        #   in Loop: Header=BB25_1 Depth=1
	testl	%ebp, %ebp
	setne	(%rsp)
	jne	.LBB25_9
.LBB25_17:                              # %_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv.exit.thread
                                        #   in Loop: Header=BB25_1 Depth=1
	movq	%r15, %rdi
	callq	_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev
	movq	(%r13), %rdi
	callq	_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKw
	testb	%al, %al
	jne	.LBB25_1
# BB#18:                                #   in Loop: Header=BB25_1 Depth=1
	movq	(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB25_20
# BB#19:                                #   in Loop: Header=BB25_1 Depth=1
	cmpl	$0, (%rsi)
	je	.LBB25_20
# BB#21:                                #   in Loop: Header=BB25_1 Depth=1
	movq	%r15, %rdi
	callq	_Z17nameWindowToUnix2PKw
	movq	(%rsp), %rbx
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbx, %rdi
	callq	mkdir
	movl	%eax, %ebp
	testq	%rbx, %rbx
	je	.LBB25_23
# BB#22:                                #   in Loop: Header=BB25_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB25_23:                              # %_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw.exit
                                        #   in Loop: Header=BB25_1 Depth=1
	testl	%ebp, %ebp
	je	.LBB25_24
# BB#25:                                # %_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw.exit._crit_edge
                                        #   in Loop: Header=BB25_1 Depth=1
	callq	__errno_location
	cmpl	$17, (%rax)
	je	.LBB25_1
	jmp	.LBB25_27
	.p2align	4, 0x90
.LBB25_20:                              # %_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw.exit.thread
                                        #   in Loop: Header=BB25_1 Depth=1
	callq	__errno_location
	movl	$2, (%rax)
	cmpl	$17, (%rax)
	je	.LBB25_1
	jmp	.LBB25_27
.LBB25_8:                               # %_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv.exit.thread9
	callq	__errno_location
	movl	$2, (%rax)
	movb	$1, (%rsp)
.LBB25_9:                               # %.critedge
	movq	%rsp, %rdi
	callq	_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev
.LBB25_27:                              # %.loopexit
	xorl	%eax, %eax
.LBB25_28:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_24:
	movb	$1, %al
	jmp	.LBB25_28
.LBB25_10:
.Ltmp161:
	movq	%rax, %rbp
	jmp	.LBB25_11
.LBB25_30:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp158:
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB25_11:                              # %.body
.Ltmp162:
	movq	%rsp, %rdi
	callq	_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev
.Ltmp163:
# BB#12:
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB25_29:
.Ltmp164:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZN8NWindows5NFile10NDirectory19CreateTempDirectoryEPKwR11CStringBaseIwE, .Lfunc_end25-_ZN8NWindows5NFile10NDirectory19CreateTempDirectoryEPKwR11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin14-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp152-.Lfunc_begin14 #   Call between .Lfunc_begin14 and .Ltmp152
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp152-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp153-.Ltmp152       #   Call between .Ltmp152 and .Ltmp153
	.long	.Ltmp161-.Lfunc_begin14 #     jumps to .Ltmp161
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp157-.Ltmp154       #   Call between .Ltmp154 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin14 #     jumps to .Ltmp158
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Ltmp160-.Ltmp159       #   Call between .Ltmp159 and .Ltmp160
	.long	.Ltmp161-.Lfunc_begin14 #     jumps to .Ltmp161
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin14 # >> Call Site 5 <<
	.long	.Ltmp162-.Ltmp160       #   Call between .Ltmp160 and .Ltmp162
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin14 # >> Call Site 6 <<
	.long	.Ltmp163-.Ltmp162       #   Call between .Ltmp162 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin14 #     jumps to .Ltmp164
	.byte	1                       #   On action: 1
	.long	.Ltmp163-.Lfunc_begin14 # >> Call Site 7 <<
	.long	.Lfunc_end25-.Ltmp163   #   Call between .Ltmp163 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NWindows5NFile10NDirectory9CTempFileD2Ev,"axG",@progbits,_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev,comdat
	.weak	_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev,@function
_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev: # @_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%rbp
.Lcfi236:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi237:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi238:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi239:
	.cfi_def_cfa_offset 48
.Lcfi240:
	.cfi_offset %rbx, -32
.Lcfi241:
	.cfi_offset %r14, -24
.Lcfi242:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpb	$0, (%rbx)
	je	.LBB26_9
# BB#1:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB26_3
# BB#2:
	cmpl	$0, (%rsi)
	je	.LBB26_3
# BB#4:
.Ltmp165:
	movq	%rsp, %rdi
	callq	_Z17nameWindowToUnix2PKw
.Ltmp166:
# BB#5:                                 # %.noexc
	movq	(%rsp), %r14
	movq	%r14, %rdi
	callq	remove
	movl	%eax, %ebp
	testq	%r14, %r14
	je	.LBB26_7
# BB#6:
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB26_7:                               # %_ZN11CStringBaseIcED2Ev.exit.i.i
	testl	%ebp, %ebp
	setne	%al
	jmp	.LBB26_8
.LBB26_3:
	callq	__errno_location
	movl	$2, (%rax)
	movb	$1, %al
.LBB26_8:                               # %_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw.exit.i
	movb	%al, (%rbx)
.LBB26_9:                               # %_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv.exit
	movq	8(%rbx), %rdi
	addq	$16, %rsp
	testq	%rdi, %rdi
	je	.LBB26_10
# BB#14:
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_ZdaPv                  # TAILCALL
.LBB26_10:                              # %_ZN11CStringBaseIwED2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB26_11:
.Ltmp167:
	movq	%rax, %r14
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_13
# BB#12:
	callq	_ZdaPv
.LBB26_13:                              # %_ZN11CStringBaseIwED2Ev.exit2
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev, .Lfunc_end26-_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp165-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin15 #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp166   #   Call between .Ltmp166 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile10NDirectory14CTempDirectory6CreateEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile10NDirectory14CTempDirectory6CreateEPKw,@function
_ZN8NWindows5NFile10NDirectory14CTempDirectory6CreateEPKw: # @_ZN8NWindows5NFile10NDirectory14CTempDirectory6CreateEPKw
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi243:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi244:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi245:
	.cfi_def_cfa_offset 32
.Lcfi246:
	.cfi_offset %rbx, -32
.Lcfi247:
	.cfi_offset %r14, -24
.Lcfi248:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	8(%rbx), %r15
	cmpb	$0, (%rbx)
	je	.LBB27_2
# BB#1:
	movq	%r15, %rdi
	callq	_ZN8NWindows5NFile10NDirectory27RemoveDirectoryWithSubItemsERK11CStringBaseIwE
	xorb	$1, %al
	movb	%al, (%rbx)
.LBB27_2:                               # %_ZN8NWindows5NFile10NDirectory14CTempDirectory6RemoveEv.exit
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN8NWindows5NFile10NDirectory19CreateTempDirectoryEPKwR11CStringBaseIwE
	movb	%al, (%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end27:
	.size	_ZN8NWindows5NFile10NDirectory14CTempDirectory6CreateEPKw, .Lfunc_end27-_ZN8NWindows5NFile10NDirectory14CTempDirectory6CreateEPKw
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%rbp
.Lcfi249:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi250:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi251:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi252:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi253:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi254:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi255:
	.cfi_def_cfa_offset 64
.Lcfi256:
	.cfi_offset %rbx, -56
.Lcfi257:
	.cfi_offset %r12, -48
.Lcfi258:
	.cfi_offset %r13, -40
.Lcfi259:
	.cfi_offset %r14, -32
.Lcfi260:
	.cfi_offset %r15, -24
.Lcfi261:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB28_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB28_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB28_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB28_5
.LBB28_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB28_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp168:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp169:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB28_35
# BB#12:
	movq	%rbx, %r13
.LBB28_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB28_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB28_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB28_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB28_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB28_15
.LBB28_14:
	xorl	%esi, %esi
.LBB28_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB28_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB28_16
.LBB28_29:
	movq	%r13, %rbx
.LBB28_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp170:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp171:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB28_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB28_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB28_8
.LBB28_3:
	xorl	%eax, %eax
.LBB28_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB28_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB28_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB28_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB28_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB28_30
.LBB28_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB28_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB28_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB28_24
	jmp	.LBB28_25
.LBB28_22:
	xorl	%ecx, %ecx
.LBB28_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB28_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB28_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB28_27
.LBB28_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB28_15
	jmp	.LBB28_29
.LBB28_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp172:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end28-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin16-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp168-.Lfunc_begin16 #   Call between .Lfunc_begin16 and .Ltmp168
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp171-.Ltmp168       #   Call between .Ltmp168 and .Ltmp171
	.long	.Ltmp172-.Lfunc_begin16 #     jumps to .Ltmp172
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Lfunc_end28-.Ltmp171   #   Call between .Ltmp171 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi262:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi263:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi264:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi265:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi266:
	.cfi_def_cfa_offset 48
.Lcfi267:
	.cfi_offset %rbx, -48
.Lcfi268:
	.cfi_offset %r12, -40
.Lcfi269:
	.cfi_offset %r14, -32
.Lcfi270:
	.cfi_offset %r15, -24
.Lcfi271:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB29_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB29_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB29_3:                               # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB29_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB29_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB29_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB29_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB29_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB29_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB29_17
.LBB29_7:
	xorl	%ecx, %ecx
.LBB29_8:                               # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB29_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB29_10:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB29_10
.LBB29_11:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB29_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB29_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB29_13
	jmp	.LBB29_26
.LBB29_25:                              # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB29_27
.LBB29_26:                              # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB29_27:                              # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB29_28:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_17:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB29_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB29_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB29_20
	jmp	.LBB29_21
.LBB29_18:
	xorl	%ebx, %ebx
.LBB29_21:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB29_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB29_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB29_23
.LBB29_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB29_8
	jmp	.LBB29_26
.Lfunc_end29:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end29-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_FileDir.ii,@function
_GLOBAL__sub_I_FileDir.ii:              # @_GLOBAL__sub_I_FileDir.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi272:
	.cfi_def_cfa_offset 16
	xorl	%edi, %edi
	callq	umask
	movl	%eax, _ZL9gbl_umask(%rip)
	movl	%eax, %edi
	callq	umask
	movl	_ZL9gbl_umask(%rip), %eax
	notl	%eax
	andl	$511, %eax              # imm = 0x1FF
	movl	%eax, _ZL9gbl_umask+4(%rip)
	movl	$_ZN8NWindows5NFile10NDirectoryL22g_CountCriticalSectionE, %edi
	callq	CriticalSection_Init
	movl	$_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev, %edi
	movl	$_ZN8NWindows5NFile10NDirectoryL22g_CountCriticalSectionE, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end30:
	.size	_GLOBAL__sub_I_FileDir.ii, .Lfunc_end30-_GLOBAL__sub_I_FileDir.ii
	.cfi_endproc

	.type	_ZL9gbl_umask,@object   # @_ZL9gbl_umask
	.local	_ZL9gbl_umask
	.comm	_ZL9gbl_umask,8,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	99                      # 0x63
	.long	58                      # 0x3a
	.long	0                       # 0x0
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	47                      # 0x2f
	.long	0                       # 0x0
	.size	.L.str.1, 8

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"P7ZIP_HOME_DIR"
	.size	.L.str.5, 15

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"r"
	.size	.L.str.6, 2

	.type	_ZN8NWindows5NFile10NDirectoryL22g_CountCriticalSectionE,@object # @_ZN8NWindows5NFile10NDirectoryL22g_CountCriticalSectionE
	.local	_ZN8NWindows5NFile10NDirectoryL22g_CountCriticalSectionE
	.comm	_ZN8NWindows5NFile10NDirectoryL22g_CountCriticalSectionE,40,8
	.type	_ZZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwEE10memo_count,@object # @_ZZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwEE10memo_count
	.local	_ZZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwEE10memo_count
	.comm	_ZZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwEE10memo_count,4,4
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"rb"
	.size	.L.str.10, 3

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_FileDir.ii
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"NOT EXPECTED : MySearchPath : fileName == NULL"
	.size	.Lstr, 47

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"NOT EXPECTED : MySearchPath : extension != NULL"
	.size	.Lstr.1, 48

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"NOT EXPECTED : MySearchPath : path != NULL"
	.size	.Lstr.2, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
