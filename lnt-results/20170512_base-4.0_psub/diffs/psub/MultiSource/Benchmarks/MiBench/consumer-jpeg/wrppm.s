	.text
	.file	"wrppm.bc"
	.globl	jinit_write_ppm
	.p2align	4, 0x90
	.type	jinit_write_ppm,@function
jinit_write_ppm:                        # @jinit_write_ppm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$80, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	$start_output_ppm, (%r14)
	movq	$finish_output_ppm, 16(%r14)
	movq	%rbx, %rdi
	callq	jpeg_calc_output_dimensions
	movl	136(%rbx), %edx
	imull	128(%rbx), %edx
	movl	%edx, 72(%r14)
	movq	%rdx, 64(%r14)
	movq	8(%rbx), %rax
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 48(%r14)
	cmpl	$0, 100(%rbx)
	je	.LBB0_4
# BB#1:
	movq	8(%rbx), %rax
	movl	140(%rbx), %edx
	imull	128(%rbx), %edx
	movl	$1, %esi
	movl	$1, %ecx
	movq	%rbx, %rdi
	callq	*16(%rax)
	movq	%rax, 32(%r14)
	movl	$1, 40(%r14)
	cmpl	$0, 100(%rbx)
	je	.LBB0_2
# BB#3:
	cmpl	$1, 56(%rbx)
	movl	$put_demapped_gray, %eax
	movl	$put_demapped_rgb, %ecx
	cmoveq	%rax, %rcx
	movq	%rcx, 8(%r14)
	jmp	.LBB0_5
.LBB0_4:
	movq	%r14, %rcx
	addq	$56, %rcx
	movq	%rax, 56(%r14)
	movq	%rcx, 32(%r14)
	movl	$1, 40(%r14)
	movq	$put_pixel_rows, 8(%r14)
	jmp	.LBB0_5
.LBB0_2:
	movq	$copy_pixel_rows, 8(%r14)
.LBB0_5:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	jinit_write_ppm, .Lfunc_end0-jinit_write_ppm
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_output_ppm,@function
start_output_ppm:                       # @start_output_ppm
	.cfi_startproc
# BB#0:
	movl	56(%rdi), %eax
	cmpl	$2, %eax
	je	.LBB1_4
# BB#1:
	cmpl	$1, %eax
	jne	.LBB1_5
# BB#2:
	movq	24(%rsi), %r9
	movl	128(%rdi), %edx
	movl	132(%rdi), %ecx
	movl	$.L.str, %esi
	jmp	.LBB1_3
.LBB1_4:
	movq	24(%rsi), %r9
	movl	128(%rdi), %edx
	movl	132(%rdi), %ecx
	movl	$.L.str.1, %esi
.LBB1_3:
	movl	$255, %r8d
	xorl	%eax, %eax
	movq	%r9, %rdi
	jmp	fprintf                 # TAILCALL
.LBB1_5:
	movq	(%rdi), %rax
	movl	$1025, 40(%rax)         # imm = 0x401
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end1:
	.size	start_output_ppm, .Lfunc_end1-start_output_ppm
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_output_ppm,@function
finish_output_ppm:                      # @finish_output_ppm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%rbx), %rdi
	callq	fflush
	movq	24(%rbx), %rdi
	callq	ferror
	testl	%eax, %eax
	je	.LBB2_1
# BB#2:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*(%rax)                 # TAILCALL
.LBB2_1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	finish_output_ppm, .Lfunc_end2-finish_output_ppm
	.cfi_endproc

	.p2align	4, 0x90
	.type	copy_pixel_rows,@function
copy_pixel_rows:                        # @copy_pixel_rows
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	48(%rsi), %rdi
	movl	72(%rsi), %ebx
	testl	%ebx, %ebx
	je	.LBB3_20
# BB#1:                                 # %.lr.ph.preheader
	movq	32(%rsi), %rcx
	movq	(%rcx), %rcx
	leal	-1(%rbx), %r9d
	leaq	1(%r9), %r10
	cmpq	$32, %r10
	jb	.LBB3_14
# BB#2:                                 # %min.iters.checked
	movabsq	$8589934560, %r8        # imm = 0x1FFFFFFE0
	andq	%r10, %r8
	je	.LBB3_14
# BB#3:                                 # %vector.memcheck
	leaq	1(%rcx,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB3_5
# BB#4:                                 # %vector.memcheck
	leaq	1(%rdi,%r9), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB3_14
.LBB3_5:                                # %vector.body.preheader
	leaq	-32(%r8), %r9
	movl	%r9d, %r11d
	shrl	$5, %r11d
	incl	%r11d
	andq	$3, %r11
	je	.LBB3_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%r11
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdx), %xmm0
	movups	16(%rcx,%rdx), %xmm1
	movups	%xmm0, (%rdi,%rdx)
	movups	%xmm1, 16(%rdi,%rdx)
	addq	$32, %rdx
	incq	%r11
	jne	.LBB3_8
	jmp	.LBB3_9
.LBB3_6:
	xorl	%edx, %edx
.LBB3_9:                                # %vector.body.prol.loopexit
	cmpq	$96, %r9
	jb	.LBB3_12
# BB#10:                                # %vector.body.preheader.new
	movq	%r8, %r9
	subq	%rdx, %r9
	leaq	112(%rdi,%rdx), %r11
	leaq	112(%rcx,%rdx), %rdx
	.p2align	4, 0x90
.LBB3_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%r11)
	movups	%xmm1, -96(%r11)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%r11)
	movups	%xmm1, -64(%r11)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%r11)
	movups	%xmm1, -32(%r11)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%r11)
	movups	%xmm1, (%r11)
	subq	$-128, %r11
	subq	$-128, %rdx
	addq	$-128, %r9
	jne	.LBB3_11
.LBB3_12:                               # %middle.block
	cmpq	%r8, %r10
	je	.LBB3_19
# BB#13:
	subl	%r8d, %ebx
	addq	%r8, %rcx
	addq	%r8, %rdi
.LBB3_14:                               # %.lr.ph.preheader32
	leal	-1(%rbx), %r8d
	movl	%ebx, %edx
	andl	$7, %edx
	je	.LBB3_17
# BB#15:                                # %.lr.ph.prol.preheader
	negl	%edx
	.p2align	4, 0x90
.LBB3_16:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %eax
	incq	%rcx
	movb	%al, (%rdi)
	incq	%rdi
	decl	%ebx
	incl	%edx
	jne	.LBB3_16
.LBB3_17:                               # %.lr.ph.prol.loopexit
	cmpl	$7, %r8d
	jb	.LBB3_19
	.p2align	4, 0x90
.LBB3_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %eax
	movb	%al, (%rdi)
	movzbl	1(%rcx), %eax
	movb	%al, 1(%rdi)
	movzbl	2(%rcx), %eax
	movb	%al, 2(%rdi)
	movzbl	3(%rcx), %eax
	movb	%al, 3(%rdi)
	movzbl	4(%rcx), %eax
	movb	%al, 4(%rdi)
	movzbl	5(%rcx), %eax
	movb	%al, 5(%rdi)
	movzbl	6(%rcx), %eax
	movb	%al, 6(%rdi)
	movzbl	7(%rcx), %eax
	movb	%al, 7(%rdi)
	addq	$8, %rcx
	addq	$8, %rdi
	addl	$-8, %ebx
	jne	.LBB3_18
.LBB3_19:                               # %._crit_edge.loopexit
	movq	48(%rsi), %rdi
.LBB3_20:                               # %._crit_edge
	movq	24(%rsi), %rcx
	movq	64(%rsi), %rdx
	movl	$1, %esi
	popq	%rbx
	jmp	fwrite                  # TAILCALL
.Lfunc_end3:
	.size	copy_pixel_rows, .Lfunc_end3-copy_pixel_rows
	.cfi_endproc

	.p2align	4, 0x90
	.type	put_demapped_gray,@function
put_demapped_gray:                      # @put_demapped_gray
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	48(%rsi), %rdi
	movl	128(%rax), %r11d
	testl	%r11d, %r11d
	je	.LBB4_7
# BB#1:                                 # %.lr.ph.preheader
	movq	152(%rax), %rax
	movq	(%rax), %r9
	movq	32(%rsi), %rax
	movq	(%rax), %rdx
	leal	-1(%r11), %r8d
	movl	%r11d, %eax
	andl	$3, %eax
	je	.LBB4_4
# BB#2:                                 # %.lr.ph.prol.preheader
	negl	%eax
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %r10d
	incq	%rdx
	movzbl	(%r9,%r10), %ecx
	movb	%cl, (%rdi)
	incq	%rdi
	decl	%r11d
	incl	%eax
	jne	.LBB4_3
.LBB4_4:                                # %.lr.ph.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB4_6
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %eax
	movzbl	(%r9,%rax), %eax
	movb	%al, (%rdi)
	movzbl	1(%rdx), %eax
	movzbl	(%r9,%rax), %eax
	movb	%al, 1(%rdi)
	movzbl	2(%rdx), %eax
	movzbl	(%r9,%rax), %eax
	movb	%al, 2(%rdi)
	movzbl	3(%rdx), %eax
	movzbl	(%r9,%rax), %eax
	movb	%al, 3(%rdi)
	addq	$4, %rdx
	addq	$4, %rdi
	addl	$-4, %r11d
	jne	.LBB4_5
.LBB4_6:                                # %._crit_edge.loopexit
	movq	48(%rsi), %rdi
.LBB4_7:                                # %._crit_edge
	movq	24(%rsi), %rcx
	movq	64(%rsi), %rdx
	movl	$1, %esi
	jmp	fwrite                  # TAILCALL
.Lfunc_end4:
	.size	put_demapped_gray, .Lfunc_end4-put_demapped_gray
	.cfi_endproc

	.p2align	4, 0x90
	.type	put_demapped_rgb,@function
put_demapped_rgb:                       # @put_demapped_rgb
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	48(%rsi), %rdi
	movl	128(%rax), %r10d
	testl	%r10d, %r10d
	je	.LBB5_7
# BB#1:                                 # %.lr.ph.preheader
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	152(%rax), %rax
	movq	(%rax), %r8
	movq	8(%rax), %r9
	movq	16(%rax), %r14
	movq	32(%rsi), %rax
	movq	(%rax), %rax
	testb	$1, %r10b
	jne	.LBB5_3
# BB#2:
	movl	%r10d, %ecx
	cmpl	$1, %r10d
	jne	.LBB5_5
	jmp	.LBB5_6
.LBB5_3:                                # %.lr.ph.prol
	movzbl	(%rax), %r11d
	incq	%rax
	movb	(%r8,%r11), %cl
	movb	%cl, (%rdi)
	movb	(%r9,%r11), %cl
	movb	%cl, 1(%rdi)
	movb	(%r14,%r11), %cl
	movb	%cl, 2(%rdi)
	addq	$3, %rdi
	leal	-1(%r10), %ecx
	cmpl	$1, %r10d
	je	.LBB5_6
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	movzbl	(%r8,%rdx), %ebx
	movb	%bl, (%rdi)
	movzbl	(%r9,%rdx), %ebx
	movb	%bl, 1(%rdi)
	movzbl	(%r14,%rdx), %edx
	movb	%dl, 2(%rdi)
	movzbl	1(%rax), %edx
	movzbl	(%r8,%rdx), %ebx
	movb	%bl, 3(%rdi)
	movzbl	(%r9,%rdx), %ebx
	movb	%bl, 4(%rdi)
	movzbl	(%r14,%rdx), %edx
	movb	%dl, 5(%rdi)
	addq	$2, %rax
	addq	$6, %rdi
	addl	$-2, %ecx
	jne	.LBB5_5
.LBB5_6:                                # %._crit_edge.loopexit
	movq	48(%rsi), %rdi
	popq	%rbx
	popq	%r14
.LBB5_7:                                # %._crit_edge
	movq	24(%rsi), %rcx
	movq	64(%rsi), %rdx
	movl	$1, %esi
	jmp	fwrite                  # TAILCALL
.Lfunc_end5:
	.size	put_demapped_rgb, .Lfunc_end5-put_demapped_rgb
	.cfi_endproc

	.p2align	4, 0x90
	.type	put_pixel_rows,@function
put_pixel_rows:                         # @put_pixel_rows
	.cfi_startproc
# BB#0:
	movq	24(%rsi), %rcx
	movq	48(%rsi), %rdi
	movq	64(%rsi), %rdx
	movl	$1, %esi
	jmp	fwrite                  # TAILCALL
.Lfunc_end6:
	.size	put_pixel_rows, .Lfunc_end6-put_pixel_rows
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"P5\n%ld %ld\n%d\n"
	.size	.L.str, 15

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"P6\n%ld %ld\n%d\n"
	.size	.L.str.1, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
