	.text
	.file	"libclamav_pdf.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_1:
	.long	85                      # 0x55
	.long	85                      # 0x55
	.long	85                      # 0x55
	.long	85                      # 0x55
.LCPI0_2:
	.long	1904149089              # 0x717f0261
	.long	1904149089              # 0x717f0261
	.long	1904149089              # 0x717f0261
	.long	1904149089              # 0x717f0261
.LCPI0_3:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
.LCPI0_4:
	.quad	24                      # 0x18
	.quad	16                      # 0x10
.LCPI0_5:
	.quad	255                     # 0xff
	.quad	255                     # 0xff
	.text
	.globl	cli_pdf
	.p2align	4, 0x90
	.type	cli_pdf,@function
cli_pdf:                                # @cli_pdf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$616, %rsp              # imm = 0x268
.Lcfi6:
	.cfi_def_cfa_offset 672
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movl	%esi, %ebx
	movq	%rdi, %r13
	xorl	%ebp, %ebp
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_dbgmsg
	leaq	472(%rsp), %rdx
	movl	$1, %edi
	movl	%ebx, %esi
	callq	__fxstat
	testl	%eax, %eax
	js	.LBB0_14
# BB#1:
	movq	520(%rsp), %r15
	testq	%r15, %r15
	je	.LBB0_15
# BB#2:
	movl	$-124, %ebp
	cmpq	$8, %r15
	jl	.LBB0_15
# BB#3:
	xorl	%edi, %edi
	movl	$1, %edx
	movl	$2, %ecx
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	movl	%ebx, %r8d
	callq	mmap
	movq	%rax, %r14
	cmpq	$-1, %r14
	je	.LBB0_16
# BB#4:
	movq	%r13, 168(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	movq	%r14, %rbp
	je	.LBB0_6
# BB#5:
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	munmap
	movq	%r13, %rbp
.LBB0_6:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.2, %esi
	movl	$7, %edx
	movq	%rbp, %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB0_11
# BB#7:
	movq	%r12, 128(%rsp)         # 8-byte Spill
	leaq	-6(%r15), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	$7, %rax
	jl	.LBB0_11
# BB#8:                                 # %.lr.ph727.preheader
	leaq	6(%rbp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	-13(%r15), %r12
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph727
                                        # =>This Inner Loop Header: Depth=1
	leaq	7(%rbp,%r12), %rbx
	movl	$.L.str.3, %esi
	movl	$5, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_19
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	leaq	-1(%r12), %rax
	addq	$6, %r12
	cmpq	$6, %r12
	movq	%rax, %r12
	jg	.LBB0_9
.LBB0_11:
	testq	%r13, %r13
	je	.LBB0_17
# BB#12:
	movq	%r13, %rdi
.LBB0_13:
	callq	free
	movl	$-124, %ebp
	jmp	.LBB0_15
.LBB0_14:
	movl	$-115, %ebp
.LBB0_15:
	movl	%ebp, %eax
	addq	$616, %rsp              # imm = 0x268
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_16:
	movl	$-114, %ebp
	jmp	.LBB0_15
.LBB0_17:
	movq	%r14, %rdi
	movq	%r15, %rsi
.LBB0_18:
	callq	munmap
	movl	$-124, %ebp
	jmp	.LBB0_15
.LBB0_19:
	cmpq	$7, %r12
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%rbp, %r15
	movq	%r14, 112(%rsp)         # 8-byte Spill
	movq	%r13, 120(%rsp)         # 8-byte Spill
	jl	.LBB0_22
.LBB0_20:                               # %.lr.ph717
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rbp,%r12), %rbp
	movl	$.L.str.4, %esi
	movl	$7, %edx
	movq	%rbp, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_23
# BB#21:                                #   in Loop: Header=BB0_20 Depth=1
	decq	%r12
	cmpq	$6, %r12
	movq	%r15, %rbp
	jg	.LBB0_20
.LBB0_22:                               # %._crit_edge.loopexitsplit
	leaq	(%rbp,%r12), %rbp
.LBB0_23:                               # %._crit_edge
	movl	$.L.str.5, %eax
	cmpq	%rax, %rbp
	je	.LBB0_279
# BB#24:
	subq	%rbp, %rbx
	cmpq	$7, %rbx
	jae	.LBB0_31
.LBB0_25:                               # %cli_pmemstr.exit.thread.preheader
	cmpq	$7, %r12
	jl	.LBB0_36
# BB#26:
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %r13
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB0_27:                               # %.lr.ph711
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1717924472, (%r13,%r12) # imm = 0x66657278
	jne	.LBB0_30
# BB#28:                                #   in Loop: Header=BB0_27 Depth=1
	movzbl	-1(%r13,%r12), %eax
	cmpb	$10, %al
	je	.LBB0_40
# BB#29:                                #   in Loop: Header=BB0_27 Depth=1
	cmpb	$13, %al
	je	.LBB0_40
.LBB0_30:                               # %cli_pmemstr.exit.thread
                                        #   in Loop: Header=BB0_27 Depth=1
	decq	%r12
	cmpq	$6, %r12
	jg	.LBB0_27
	jmp	.LBB0_37
.LBB0_31:
	movl	$.L.str.5, %esi
	movl	$7, %edx
	movq	%rbp, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_278
# BB#32:                                # %.preheader.i
	movl	$69, %esi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memchr
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_25
.LBB0_33:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %r13
	subq	%r14, %r13
	addq	%rbx, %r13
	cmpq	$6, %r13
	jbe	.LBB0_25
# BB#34:                                #   in Loop: Header=BB0_33 Depth=1
	movl	$.L.str.5, %esi
	movl	$7, %edx
	movq	%r14, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_279
# BB#35:                                # %.backedge.i
                                        #   in Loop: Header=BB0_33 Depth=1
	cmpq	%r14, %rbp
	leaq	1(%rbp), %rbp
	cmovneq	%r14, %rbp
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %r13
	movl	$69, %esi
	movq	%rbp, %rdi
	movq	%r13, %rdx
	callq	memchr
	movq	%rax, %r14
	testq	%r14, %r14
	movq	%r13, %rbx
	jne	.LBB0_33
	jmp	.LBB0_25
.LBB0_36:
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %r13
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB0_37:                               # %cli_pmemstr.exit.thread._crit_edge
	cmpq	$6, %r12
	jne	.LBB0_40
# BB#38:
	movq	120(%rsp), %rdi         # 8-byte Reload
	testq	%rdi, %rdi
	jne	.LBB0_13
# BB#39:
	movq	112(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB0_18
.LBB0_40:                               # %.thread
	callq	tableCreate
	movq	%rax, %r14
	movl	$0, (%rsp)              # 4-byte Folded Spill
	cmpq	$7, %r12
	jl	.LBB0_292
# BB#41:                                # %.lr.ph
	addq	%r12, %r13
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	32(%rax), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	movq	112(%rsp), %rcx         # 8-byte Reload
	cmovneq	%rax, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movd	%eax, %xmm0
	movdqa	%xmm0, 192(%rsp)        # 16-byte Spill
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqa	%xmm0, 176(%rsp)        # 16-byte Spill
	movl	$0, 104(%rsp)           # 4-byte Folded Spill
	movl	$0, 108(%rsp)           # 4-byte Folded Spill
	movl	$0, 84(%rsp)            # 4-byte Folded Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
.LBB0_42:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_56 Depth 2
                                        #     Child Loop BB0_67 Depth 2
                                        #     Child Loop BB0_75 Depth 2
                                        #       Child Loop BB0_92 Depth 3
                                        #       Child Loop BB0_84 Depth 3
                                        #       Child Loop BB0_98 Depth 3
                                        #       Child Loop BB0_107 Depth 3
                                        #       Child Loop BB0_113 Depth 3
                                        #     Child Loop BB0_127 Depth 2
                                        #     Child Loop BB0_133 Depth 2
                                        #     Child Loop BB0_150 Depth 2
                                        #     Child Loop BB0_143 Depth 2
                                        #     Child Loop BB0_178 Depth 2
                                        #     Child Loop BB0_184 Depth 2
                                        #       Child Loop BB0_185 Depth 3
                                        #       Child Loop BB0_193 Depth 3
                                        #       Child Loop BB0_202 Depth 3
                                        #       Child Loop BB0_210 Depth 3
                                        #       Child Loop BB0_218 Depth 3
                                        #     Child Loop BB0_241 Depth 2
                                        #     Child Loop BB0_246 Depth 2
                                        #     Child Loop BB0_250 Depth 2
                                        #     Child Loop BB0_255 Depth 2
                                        #     Child Loop BB0_259 Depth 2
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%rbx, %r15
	movq	%rbx, %rsi
	callq	pdf_nextobject
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_287
# BB#43:                                #   in Loop: Header=BB0_42 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	cmpq	%r13, %rbx
	je	.LBB0_292
# BB#44:                                #   in Loop: Header=BB0_42 Depth=1
	cmpl	$1717924472, (%rbx)     # imm = 0x66657278
	je	.LBB0_292
# BB#45:                                #   in Loop: Header=BB0_42 Depth=1
	subq	%rbx, %rbp
	addq	%rbp, %r15
	movl	$.L.str.8, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	memcmp
	movl	$0, (%rsp)              # 4-byte Folded Spill
	testl	%eax, %eax
	je	.LBB0_59
# BB#46:                                #   in Loop: Header=BB0_42 Depth=1
	callq	__ctype_b_loc
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	(%rax), %r14
	movsbq	(%rbx), %rax
	testb	$8, 1(%r14,%rax,2)
	je	.LBB0_288
# BB#47:                                #   in Loop: Header=BB0_42 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	pdf_nextobject
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movq	$-1, %r12
	je	.LBB0_285
# BB#48:                                #   in Loop: Header=BB0_42 Depth=1
	movsbq	(%rbp), %rax
	testb	$8, 1(%r14,%rax,2)
	je	.LBB0_285
# BB#49:                                #   in Loop: Header=BB0_42 Depth=1
	subq	%rbp, %rbx
	addq	%r15, %rbx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	pdf_nextobject
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_286
# BB#50:                                #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.11, %esi
	movl	$3, %edx
	movq	%r14, %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB0_286
# BB#51:                                #   in Loop: Header=BB0_42 Depth=1
	leaq	-3(%rbp,%rbx), %rbx
	subq	%r14, %rbx
	addq	$3, %r14
	movl	$.L.str.8, %eax
	cmpq	%rax, %r14
	movl	$.L.str.8, %r13d
	je	.LBB0_61
# BB#52:                                #   in Loop: Header=BB0_42 Depth=1
	movq	%rbx, %r15
	cmpq	$6, %rbx
	jb	.LBB0_283
# BB#53:                                #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.8, %esi
	movl	$6, %edx
	movq	%r14, %rdi
	callq	memcmp
	testl	%eax, %eax
	movq	%r14, %r13
	movq	%r15, %rbx
	je	.LBB0_61
# BB#54:                                # %.preheader.i384
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$101, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memchr
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_283
# BB#55:                                # %.lr.ph.i387.preheader
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	%r15, %rax
	movq	%r14, %rbx
.LBB0_56:                               # %.lr.ph.i387
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rbp
	subq	%r13, %rbp
	addq	%rax, %rbp
	cmpq	$6, %rbp
	jb	.LBB0_283
# BB#57:                                #   in Loop: Header=BB0_56 Depth=2
	movl	$.L.str.8, %esi
	movl	$6, %edx
	movq	%r13, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_60
# BB#58:                                # %.backedge.i390
                                        #   in Loop: Header=BB0_56 Depth=2
	cmpq	%r13, %rbx
	leaq	1(%rbx), %rbx
	cmovneq	%r13, %rbx
	movl	$0, %eax
	cmoveq	%r12, %rax
	addq	%rax, %rbp
	movl	$101, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memchr
	movq	%rax, %r13
	testq	%r13, %r13
	movq	%rbp, %rax
	jne	.LBB0_56
	jmp	.LBB0_283
.LBB0_59:                               #   in Loop: Header=BB0_42 Depth=1
	movq	%rbx, %r12
	movq	%r15, %rbx
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB0_277
	jmp	.LBB0_292
.LBB0_60:                               #   in Loop: Header=BB0_42 Depth=1
	movq	%r15, %rbx
.LBB0_61:                               # %.loopexit500
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	%r13, %r15
	subq	%r14, %r15
	addq	$-6, %rbx
	subq	%r15, %rbx
	movq	%r13, 160(%rsp)         # 8-byte Spill
	leaq	6(%r13), %r12
	movl	$.L.str.14, %eax
	cmpq	%rax, %r14
	movl	$.L.str.14, %ecx
	movq	48(%rsp), %r13          # 8-byte Reload
	je	.LBB0_74
# BB#62:                                #   in Loop: Header=BB0_42 Depth=1
	cmpq	$6, %r15
	jae	.LBB0_64
.LBB0_63:                               #   in Loop: Header=BB0_42 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB0_277
	jmp	.LBB0_292
.LBB0_64:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.14, %esi
	movl	$6, %edx
	movq	%r14, %rdi
	callq	memcmp
	testl	%eax, %eax
	movq	%r14, %rcx
	je	.LBB0_74
# BB#65:                                # %.preheader.i393
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$115, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	memchr
	testq	%rax, %rax
	je	.LBB0_63
# BB#66:                                # %.lr.ph.i396.preheader
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%r14, %rbp
	movq	$-1, %r13
.LBB0_67:                               # %.lr.ph.i396
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rbx
	subq	%rax, %rbx
	addq	%r15, %rbx
	cmpq	$6, %rbx
	jb	.LBB0_71
# BB#68:                                #   in Loop: Header=BB0_67 Depth=2
	movl	$.L.str.14, %esi
	movl	$6, %edx
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_73
# BB#69:                                # %.backedge.i399
                                        #   in Loop: Header=BB0_67 Depth=2
	cmpq	%r15, %rbp
	leaq	1(%rbp), %rbp
	cmovneq	%r15, %rbp
	movl	$0, %eax
	cmoveq	%r13, %rax
	addq	%rax, %rbx
	movl	$115, %esi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memchr
	testq	%rax, %rax
	movq	%rbx, %r15
	jne	.LBB0_67
# BB#70:                                #   in Loop: Header=BB0_42 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
.LBB0_71:                               #   in Loop: Header=BB0_42 Depth=1
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB0_277
	jmp	.LBB0_292
.LBB0_73:                               #   in Loop: Header=BB0_42 Depth=1
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %rcx
.LBB0_74:                               # %cli_pmemstr.exit401.thread449
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	%r12, 72(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movq	%rcx, %r12
.LBB0_75:                               #   Parent Loop BB0_42 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_92 Depth 3
                                        #       Child Loop BB0_84 Depth 3
                                        #       Child Loop BB0_98 Depth 3
                                        #       Child Loop BB0_107 Depth 3
                                        #       Child Loop BB0_113 Depth 3
	cmpq	%rcx, %r14
	jae	.LBB0_119
# BB#76:                                #   in Loop: Header=BB0_75 Depth=2
	cmpb	$47, (%r14)
	jne	.LBB0_82
# BB#77:                                #   in Loop: Header=BB0_75 Depth=2
	leaq	1(%r14), %r15
	movl	$.L.str.15, %esi
	movl	$7, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_83
# BB#78:                                #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.21, %esi
	movl	$8, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_90
# BB#79:                                #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.22, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_91
# BB#80:                                #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.23, %esi
	movl	$11, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_93
# BB#81:                                #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.24, %esi
	movl	$13, %edx
	movq	%r15, %rdi
	callq	strncmp
	addq	$14, %r14
	testl	%eax, %eax
	movl	$1, %eax
	movq	144(%rsp), %rcx         # 8-byte Reload
	cmovel	%eax, %ecx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	cmoveq	%r14, %r15
	jmp	.LBB0_94
.LBB0_82:                               #   in Loop: Header=BB0_75 Depth=2
	movq	%r14, %r15
	jmp	.LBB0_118
.LBB0_83:                               #   in Loop: Header=BB0_75 Depth=2
	leaq	8(%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	shlq	$32, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	addq	$7, %r14
	movq	%r14, %r15
.LBB0_84:                               #   Parent Loop BB0_42 Depth=1
                                        #     Parent Loop BB0_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	1(%r15), %rcx
	incq	%r15
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB0_84
# BB#85:                                #   in Loop: Header=BB0_75 Depth=2
	sarq	$32, 8(%rsp)            # 8-byte Folded Spill
	cmpq	$12, %rbx
	jl	.LBB0_117
# BB#86:                                #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.16, %esi
	movl	$4, %edx
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB0_117
# BB#87:                                #   in Loop: Header=BB0_75 Depth=2
	movl	%ebp, 36(%rsp)          # 4-byte Spill
	addq	$4, %r15
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movl	$14, %esi
	movl	$.L.str.18, %edx
	xorl	%eax, %eax
	leaq	90(%rsp), %r14
	movq	%r14, %rdi
	movq	%rbp, %rcx
	callq	snprintf
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %r13
	movq	56(%rsp), %rbp          # 8-byte Reload
	cmpq	%r14, %rbp
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%r13, 8(%rsp)           # 8-byte Spill
	je	.LBB0_101
# BB#88:                                #   in Loop: Header=BB0_75 Depth=2
	cmpq	%r13, 40(%rsp)          # 8-byte Folded Reload
	jae	.LBB0_95
.LBB0_89:                               # %cli_pmemstr.exit410.thread.thread
                                        #   in Loop: Header=BB0_75 Depth=2
	movb	$13, 90(%rsp)
	jmp	.LBB0_103
.LBB0_90:                               #   in Loop: Header=BB0_75 Depth=2
	movl	$1, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB0_94
.LBB0_91:                               #   in Loop: Header=BB0_75 Depth=2
	leaq	11(%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	addq	$9, %r14
	movq	%r14, %r15
.LBB0_92:                               #   Parent Loop BB0_42 Depth=1
                                        #     Parent Loop BB0_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	2(%r15), %rcx
	incq	%r15
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB0_92
	jmp	.LBB0_94
.LBB0_93:                               #   in Loop: Header=BB0_75 Depth=2
	addq	$12, %r14
	movl	$1, %ebp
	movq	%r14, %r15
.LBB0_94:                               #   in Loop: Header=BB0_75 Depth=2
	movq	%r12, %rcx
	jmp	.LBB0_118
.LBB0_95:                               #   in Loop: Header=BB0_75 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	leaq	90(%rsp), %rsi
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r13, %rdx
	callq	memcmp
	testl	%eax, %eax
	movq	40(%rsp), %rdx          # 8-byte Reload
	je	.LBB0_101
# BB#96:                                # %.preheader.i402
                                        #   in Loop: Header=BB0_75 Depth=2
	movsbl	90(%rsp), %r14d
	movq	%rbp, %rdi
	movl	%r14d, %esi
	callq	memchr
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_89
# BB#97:                                # %.lr.ph.i405.preheader
                                        #   in Loop: Header=BB0_75 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB0_98:                               # %.lr.ph.i405
                                        #   Parent Loop BB0_42 Depth=1
                                        #     Parent Loop BB0_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbp, %rbx
	subq	%r13, %rbx
	addq	%rax, %rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	jb	.LBB0_102
# BB#99:                                #   in Loop: Header=BB0_98 Depth=3
	movq	%r13, %rdi
	leaq	90(%rsp), %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_111
# BB#100:                               # %.backedge.i408
                                        #   in Loop: Header=BB0_98 Depth=3
	cmpq	%r13, %rbp
	leaq	1(%rbp), %rbp
	cmovneq	%r13, %rbp
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbx
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movq	%rbx, %rdx
	callq	memchr
	movq	%rax, %r13
	testq	%r13, %r13
	movq	%rbx, %rax
	jne	.LBB0_98
	jmp	.LBB0_102
.LBB0_101:                              # %cli_pmemstr.exit410
                                        #   in Loop: Header=BB0_75 Depth=2
	testq	%rbp, %rbp
	movq	%rbp, %r13
	jne	.LBB0_111
.LBB0_102:                              # %cli_pmemstr.exit410.thread
                                        #   in Loop: Header=BB0_75 Depth=2
	leaq	90(%rsp), %rax
	cmpq	%rax, 56(%rsp)          # 8-byte Folded Reload
	movb	$13, 90(%rsp)
	je	.LBB0_110
.LBB0_103:                              #   in Loop: Header=BB0_75 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	%rax, 40(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_115
# BB#104:                               #   in Loop: Header=BB0_75 Depth=2
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	90(%rsp), %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_110
# BB#105:                               # %.preheader.i411
                                        #   in Loop: Header=BB0_75 Depth=2
	movl	$13, %esi
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	memchr
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_115
# BB#106:                               # %.lr.ph.i414.preheader
                                        #   in Loop: Header=BB0_75 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB0_107:                              # %.lr.ph.i414
                                        #   Parent Loop BB0_42 Depth=1
                                        #     Parent Loop BB0_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbp, %rbx
	subq	%r13, %rbx
	addq	%rax, %rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	jb	.LBB0_115
# BB#108:                               #   in Loop: Header=BB0_107 Depth=3
	movq	%r13, %rdi
	leaq	90(%rsp), %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_111
# BB#109:                               # %.backedge.i417
                                        #   in Loop: Header=BB0_107 Depth=3
	cmpq	%r13, %rbp
	leaq	1(%rbp), %rbp
	cmovneq	%r13, %rbp
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbx
	movl	$13, %esi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memchr
	movq	%rax, %r13
	testq	%r13, %r13
	movq	%rbx, %rax
	jne	.LBB0_107
	jmp	.LBB0_115
.LBB0_110:                              # %cli_pmemstr.exit419
                                        #   in Loop: Header=BB0_75 Depth=2
	movq	56(%rsp), %r13          # 8-byte Reload
	testq	%r13, %r13
	je	.LBB0_115
.LBB0_111:                              # %cli_pmemstr.exit419.thread453
                                        #   in Loop: Header=BB0_75 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	-1(%r13,%rax), %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%r15,%rax), %rsi
	subq	%rdi, %rsi
	callq	pdf_nextobject
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movq	48(%rsp), %r13          # 8-byte Reload
	movl	36(%rsp), %ebp          # 4-byte Reload
	je	.LBB0_116
# BB#112:                               #   in Loop: Header=BB0_75 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	shlq	$32, %rax
	movq	%rax, %rdx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
.LBB0_113:                              #   Parent Loop BB0_42 Depth=1
                                        #     Parent Loop BB0_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%rbx), %rcx
	incq	%rbx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB0_113
# BB#114:                               #   in Loop: Header=BB0_75 Depth=2
	sarq	$32, %rdx
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	leaq	91(%rsp), %rsi
	movq	%rdx, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	callq	cli_dbgmsg
	jmp	.LBB0_116
.LBB0_115:                              # %cli_pmemstr.exit419.thread
                                        #   in Loop: Header=BB0_75 Depth=2
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	leaq	91(%rsp), %rsi
	callq	cli_warnmsg
	movq	48(%rsp), %r13          # 8-byte Reload
	movl	36(%rsp), %ebp          # 4-byte Reload
.LBB0_116:                              #   in Loop: Header=BB0_75 Depth=2
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB0_117:                              #   in Loop: Header=BB0_75 Depth=2
	movq	%r12, %rcx
	decq	%r15
.LBB0_118:                              #   in Loop: Header=BB0_75 Depth=2
	movq	%rcx, %rsi
	subq	%r15, %rsi
	movq	%r15, %rdi
	callq	pdf_nextobject
	movq	%r12, %rcx
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB0_75
.LBB0_119:                              #   in Loop: Header=BB0_42 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB0_122
# BB#120:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	cmpl	$0, 108(%rsp)           # 4-byte Folded Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	72(%rsp), %r12          # 8-byte Reload
	jne	.LBB0_276
# BB#121:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, 108(%rsp)           # 4-byte Folded Spill
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB0_277
	jmp	.LBB0_292
.LBB0_122:                              #   in Loop: Header=BB0_42 Depth=1
	cmpl	$2, 136(%rsp)           # 4-byte Folded Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	72(%rsp), %r12          # 8-byte Reload
	jl	.LBB0_125
# BB#123:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_276
# BB#124:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movq	136(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	$1, 104(%rsp)           # 4-byte Folded Spill
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB0_277
	jmp	.LBB0_292
.LBB0_125:                              #   in Loop: Header=BB0_42 Depth=1
	leaq	6(%rcx), %rsi
	movq	160(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%esi, %eax
	cltq
	movb	6(%rcx), %cl
	jmp	.LBB0_127
.LBB0_126:                              #   in Loop: Header=BB0_127 Depth=2
	decq	%rax
	movzbl	1(%rsi), %ecx
	incq	%rsi
.LBB0_127:                              #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %edx
	shll	%cl, %edx
	cmpb	$15, %cl
	ja	.LBB0_129
# BB#128:                               #   in Loop: Header=BB0_127 Depth=2
	andl	$9217, %edx             # imm = 0x2401
	testw	%dx, %dx
	jne	.LBB0_130
.LBB0_129:                              # %.lr.ph30.i
                                        #   in Loop: Header=BB0_127 Depth=2
	cmpq	$1, %rax
	jne	.LBB0_126
	jmp	.LBB0_296
.LBB0_130:                              # %.preheader.i420
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$1, %edx
	shll	%cl, %edx
	cmpb	$15, %cl
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	%ebp, 36(%rsp)          # 4-byte Spill
	ja	.LBB0_136
# BB#131:                               # %.preheader.i420
                                        #   in Loop: Header=BB0_42 Depth=1
	andl	$9217, %edx             # imm = 0x2401
	testw	%dx, %dx
	je	.LBB0_136
# BB#132:                               # %.lr.ph.i421.preheader
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$1, %edx
	subq	%rax, %rdx
.LBB0_133:                              # %.lr.ph.i421
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rdx, %rdx
	je	.LBB0_297
# BB#134:                               #   in Loop: Header=BB0_133 Depth=2
	movzbl	1(%rsi), %ecx
	incq	%rsi
	movl	$1, %eax
	shll	%cl, %eax
	cmpb	$15, %cl
	ja	.LBB0_136
# BB#135:                               #   in Loop: Header=BB0_133 Depth=2
	andl	$9217, %eax             # imm = 0x2401
	incq	%rdx
	testw	%ax, %ax
	jne	.LBB0_133
.LBB0_136:                              # %.loopexit497
                                        #   in Loop: Header=BB0_42 Depth=1
	xorl	%r13d, %r13d
	movl	$.L.str.27, %eax
	cmpq	%rax, %rsi
	je	.LBB0_146
# BB#137:                               #   in Loop: Header=BB0_42 Depth=1
	movq	%rsi, %r12
	movq	160(%rsp), %rax         # 8-byte Reload
	subl	%esi, %eax
	movslq	%eax, %r15
	cmpq	$10, %r15
	jae	.LBB0_147
.LBB0_138:                              # %.loopexit495
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$1, %r13d
	movl	$.L.str.28, %eax
	cmpq	%rax, %r12
	je	.LBB0_153
# BB#139:                               #   in Loop: Header=BB0_42 Depth=1
	cmpq	$10, %r15
	jb	.LBB0_298
# BB#140:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.28, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_155
# BB#141:                               # %.preheader.i431
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$101, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	callq	memchr
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_298
# BB#142:                               # %.lr.ph.i434.preheader
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	%r12, %rbx
.LBB0_143:                              # %.lr.ph.i434
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rbp
	subq	%r14, %rbp
	addq	%r15, %rbp
	cmpq	$10, %rbp
	jb	.LBB0_298
# BB#144:                               #   in Loop: Header=BB0_143 Depth=2
	movl	$.L.str.28, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_156
# BB#145:                               # %.backedge.i437
                                        #   in Loop: Header=BB0_143 Depth=2
	cmpq	%r14, %rbx
	leaq	1(%rbx), %rbx
	cmovneq	%r14, %rbx
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbp
	movl	$101, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memchr
	movq	%rax, %r14
	testq	%r14, %r14
	movq	%rbp, %r15
	jne	.LBB0_143
	jmp	.LBB0_298
.LBB0_146:                              #   in Loop: Header=BB0_42 Depth=1
	movq	%rsi, %r12
	movl	$.L.str.27, %r14d
	jmp	.LBB0_156
.LBB0_147:                              #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.27, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_155
# BB#148:                               # %.preheader.i422
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$101, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	callq	memchr
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_138
# BB#149:                               # %.lr.ph.i425.preheader
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	%r15, %rax
	movq	%r12, %rbx
.LBB0_150:                              # %.lr.ph.i425
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rbp
	subq	%r14, %rbp
	addq	%rax, %rbp
	cmpq	$10, %rbp
	jb	.LBB0_138
# BB#151:                               #   in Loop: Header=BB0_150 Depth=2
	movl	$.L.str.27, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_156
# BB#152:                               # %.backedge.i428
                                        #   in Loop: Header=BB0_150 Depth=2
	cmpq	%r14, %rbx
	leaq	1(%rbx), %rbx
	cmovneq	%r14, %rbx
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbp
	movl	$101, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memchr
	movq	%rax, %r14
	testq	%r14, %r14
	movq	%rbp, %rax
	jne	.LBB0_150
	jmp	.LBB0_138
.LBB0_155:                              #   in Loop: Header=BB0_42 Depth=1
	movq	%r12, %r14
	jmp	.LBB0_156
.LBB0_153:                              #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.28, %r14d
.LBB0_156:                              # %cli_pmemstr.exit430.thread459
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$257, %esi              # imm = 0x101
	movl	$.L.str.30, %edx
	xorl	%eax, %eax
	leaq	208(%rsp), %rbx
	movq	%rbx, %rdi
	movq	168(%rsp), %rcx         # 8-byte Reload
	callq	snprintf
	movq	%rbx, %rdi
	callq	mkstemp
	movl	%eax, %r15d
	testl	%eax, %eax
	js	.LBB0_301
# BB#157:                               #   in Loop: Header=BB0_42 Depth=1
	movb	-1(%r14), %al
	cmpb	$13, %al
	je	.LBB0_159
# BB#158:                               #   in Loop: Header=BB0_42 Depth=1
	cmpb	$10, %al
	jne	.LBB0_162
.LBB0_159:                              #   in Loop: Header=BB0_42 Depth=1
	leaq	-1(%r14), %rax
	testl	%r13d, %r13d
	je	.LBB0_161
# BB#160:                               #   in Loop: Header=BB0_42 Depth=1
	cmpb	$13, -2(%r14)
	leaq	-2(%r14), %rcx
	cmoveq	%rcx, %rax
.LBB0_161:                              #   in Loop: Header=BB0_42 Depth=1
	movq	%rax, %r14
.LBB0_162:                              #   in Loop: Header=BB0_42 Depth=1
	cmpq	%r12, %r14
	jbe	.LBB0_170
# BB#163:                               #   in Loop: Header=BB0_42 Depth=1
	subl	%r12d, %r14d
	movslq	%r14d, %r14
	cmpq	8(%rsp), %r14           # 8-byte Folded Reload
	je	.LBB0_165
# BB#164:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_165:                              #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %rdx
	movl	36(%rsp), %ecx          # 4-byte Reload
	movq	144(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %r8d
	callq	cli_dbgmsg
	testl	%ebx, %ebx
	je	.LBB0_171
# BB#166:                               #   in Loop: Header=BB0_42 Depth=1
	leaq	(%r14,%r14,4), %rdi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_306
# BB#167:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.53, %eax
	cmpq	%rax, %r12
	je	.LBB0_181
# BB#168:                               #   in Loop: Header=BB0_42 Depth=1
	cmpq	$2, %r14
	jae	.LBB0_175
.LBB0_169:                              # %cli_pmemstr.exit.thread.i
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_181
.LBB0_170:                              #   in Loop: Header=BB0_42 Depth=1
	movl	%r15d, %edi
	callq	close
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	208(%rsp), %rdi
	callq	unlink
	jmp	.LBB0_275
.LBB0_171:                              #   in Loop: Header=BB0_42 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB0_182
# BB#172:                               #   in Loop: Header=BB0_42 Depth=1
	movq	%r12, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%r15d, %edx
	movq	128(%rsp), %rcx         # 8-byte Reload
	callq	flatedecode
	movl	%eax, %ebx
	movl	$0, (%rsp)              # 4-byte Folded Spill
	testl	%ebx, %ebx
	je	.LBB0_270
# BB#173:                               #   in Loop: Header=BB0_42 Depth=1
	cmpq	8(%rsp), %r14           # 8-byte Folded Reload
	jne	.LBB0_227
# BB#174:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.40, %edi
	jmp	.LBB0_229
.LBB0_175:                              #   in Loop: Header=BB0_42 Depth=1
	movzwl	(%r12), %eax
	cmpl	$15998, %eax            # imm = 0x3E7E
	je	.LBB0_181
# BB#176:                               # %.preheader.i.i
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$126, %esi
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	memchr
	testq	%rax, %rax
	je	.LBB0_169
# BB#177:                               # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	%r14, %rcx
	movq	%r12, %rbx
.LBB0_178:                              # %.lr.ph.i.i
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rbp
	subq	%rax, %rbp
	addq	%rcx, %rbp
	cmpq	$2, %rbp
	jb	.LBB0_169
# BB#179:                               #   in Loop: Header=BB0_178 Depth=2
	movzwl	(%rax), %ecx
	cmpl	$15998, %ecx            # imm = 0x3E7E
	je	.LBB0_181
# BB#180:                               # %.backedge.i.i
                                        #   in Loop: Header=BB0_178 Depth=2
	cmpq	%rax, %rbx
	leaq	1(%rbx), %rbx
	cmovneq	%rax, %rbx
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	addq	%rax, %rbp
	movl	$126, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memchr
	testq	%rax, %rax
	movq	%rbp, %rcx
	jne	.LBB0_178
	jmp	.LBB0_169
.LBB0_181:                              # %cli_pmemstr.exit.thread99.i
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	%r14, %rdx
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%r12, %rax
	jmp	.LBB0_184
.LBB0_182:                              #   in Loop: Header=BB0_42 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movl	%r15d, %edi
	movq	%r12, %rsi
	movl	%ebx, %edx
	callq	cli_writen
	jmp	.LBB0_270
.LBB0_183:                              # %.thread101.outer.backedge.i
                                        #   in Loop: Header=BB0_184 Depth=2
	addq	$4, (%rsp)              # 8-byte Folded Spill
	addl	$4, 8(%rsp)             # 4-byte Folded Spill
	movq	%rbp, %rax
.LBB0_184:                              # %.thread101.outer.i
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_185 Depth 3
                                        #       Child Loop BB0_193 Depth 3
                                        #       Child Loop BB0_202 Depth 3
                                        #       Child Loop BB0_210 Depth 3
                                        #       Child Loop BB0_218 Depth 3
	leaq	1(%rax), %rbp
	leaq	2(%rax), %rbx
	leaq	3(%rax), %rdi
	leaq	4(%rax), %r9
	leaq	5(%rax), %r8
.LBB0_185:                              # %.thread101.i
                                        #   Parent Loop BB0_42 Depth=1
                                        #     Parent Loop BB0_184 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	jle	.LBB0_261
# BB#186:                               #   in Loop: Header=BB0_185 Depth=3
	movsbl	-1(%rbp), %esi
	cmpl	$126, %esi
	jne	.LBB0_188
# BB#187:                               #   in Loop: Header=BB0_185 Depth=3
	cmpb	$62, (%rbp)
	movl	$-1, %eax
	cmovel	%eax, %esi
.LBB0_188:                              #   in Loop: Header=BB0_185 Depth=3
	leal	-33(%rsi), %eax
	cmpl	$84, %eax
	jbe	.LBB0_192
# BB#189:                               #   in Loop: Header=BB0_185 Depth=3
	cmpl	$-1, %esi
	je	.LBB0_226
# BB#190:                               #   in Loop: Header=BB0_185 Depth=3
	cmpl	$122, %esi
	je	.LBB0_200
# BB#191:                               #   in Loop: Header=BB0_185 Depth=3
	decq	%rdx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movslq	%esi, %rcx
	incq	%rbp
	incq	%rbx
	incq	%rdi
	incq	%r9
	incq	%r8
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_185
	jmp	.LBB0_299
.LBB0_192:                              # %.thread101.1.i.preheader
                                        #   in Loop: Header=BB0_184 Depth=2
	decq	%rdx
.LBB0_193:                              # %.thread101.1.i
                                        #   Parent Loop BB0_42 Depth=1
                                        #     Parent Loop BB0_184 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	jle	.LBB0_261
# BB#194:                               #   in Loop: Header=BB0_193 Depth=3
	movsbl	-1(%rbx), %esi
	cmpl	$126, %esi
	jne	.LBB0_196
# BB#195:                               #   in Loop: Header=BB0_193 Depth=3
	cmpb	$62, (%rbx)
	movl	$-1, %ecx
	cmovel	%ecx, %esi
.LBB0_196:                              #   in Loop: Header=BB0_193 Depth=3
	leal	-33(%rsi), %ebp
	cmpl	$85, %ebp
	jb	.LBB0_201
# BB#197:                               #   in Loop: Header=BB0_193 Depth=3
	cmpl	$-1, %esi
	je	.LBB0_230
# BB#198:                               #   in Loop: Header=BB0_193 Depth=3
	cmpl	$122, %esi
	je	.LBB0_302
# BB#199:                               #   in Loop: Header=BB0_193 Depth=3
	decq	%rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movslq	%esi, %rbp
	incq	%rbx
	incq	%rdi
	incq	%r9
	incq	%r8
	testb	$32, 1(%rcx,%rbp,2)
	jne	.LBB0_193
	jmp	.LBB0_299
.LBB0_200:                              # %.loopexit.i
                                        #   in Loop: Header=BB0_184 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$0, (%rax)
	decq	%rdx
	jmp	.LBB0_183
.LBB0_201:                              # %.thread101.outer105.2175.i
                                        #   in Loop: Header=BB0_184 Depth=2
	imull	$85, %eax, %eax
	addl	%eax, %ebp
	decq	%rdx
.LBB0_202:                              # %.thread101.2.i
                                        #   Parent Loop BB0_42 Depth=1
                                        #     Parent Loop BB0_184 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	jle	.LBB0_261
# BB#203:                               #   in Loop: Header=BB0_202 Depth=3
	movsbl	-1(%rdi), %esi
	cmpl	$126, %esi
	jne	.LBB0_205
# BB#204:                               #   in Loop: Header=BB0_202 Depth=3
	cmpb	$62, (%rdi)
	movl	$-1, %eax
	cmovel	%eax, %esi
.LBB0_205:                              #   in Loop: Header=BB0_202 Depth=3
	leal	-33(%rsi), %eax
	cmpl	$85, %eax
	jb	.LBB0_209
# BB#206:                               #   in Loop: Header=BB0_202 Depth=3
	cmpl	$-1, %esi
	je	.LBB0_231
# BB#207:                               #   in Loop: Header=BB0_202 Depth=3
	cmpl	$122, %esi
	je	.LBB0_302
# BB#208:                               #   in Loop: Header=BB0_202 Depth=3
	decq	%rdx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movslq	%esi, %rcx
	incq	%rdi
	incq	%r9
	incq	%r8
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_202
	jmp	.LBB0_299
.LBB0_209:                              # %.thread101.outer105.3176.i
                                        #   in Loop: Header=BB0_184 Depth=2
	imull	$85, %ebp, %ecx
	addl	%ecx, %eax
	decq	%rdx
.LBB0_210:                              # %.thread101.3.i
                                        #   Parent Loop BB0_42 Depth=1
                                        #     Parent Loop BB0_184 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	jle	.LBB0_261
# BB#211:                               #   in Loop: Header=BB0_210 Depth=3
	movsbl	-1(%r9), %esi
	cmpl	$126, %esi
	jne	.LBB0_213
# BB#212:                               #   in Loop: Header=BB0_210 Depth=3
	cmpb	$62, (%r9)
	movl	$-1, %ecx
	cmovel	%ecx, %esi
.LBB0_213:                              #   in Loop: Header=BB0_210 Depth=3
	leal	-33(%rsi), %ebp
	cmpl	$85, %ebp
	jb	.LBB0_217
# BB#214:                               #   in Loop: Header=BB0_210 Depth=3
	cmpl	$-1, %esi
	je	.LBB0_232
# BB#215:                               #   in Loop: Header=BB0_210 Depth=3
	cmpl	$122, %esi
	je	.LBB0_302
# BB#216:                               #   in Loop: Header=BB0_210 Depth=3
	decq	%rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movslq	%esi, %rdi
	incq	%r9
	incq	%r8
	testb	$32, 1(%rcx,%rdi,2)
	jne	.LBB0_210
	jmp	.LBB0_299
.LBB0_217:                              # %.thread101.outer105.4177.i
                                        #   in Loop: Header=BB0_184 Depth=2
	imull	$85, %eax, %eax
	addl	%eax, %ebp
	decq	%rdx
.LBB0_218:                              # %.thread101.4.i
                                        #   Parent Loop BB0_42 Depth=1
                                        #     Parent Loop BB0_184 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	jle	.LBB0_261
# BB#219:                               #   in Loop: Header=BB0_218 Depth=3
	movsbl	-1(%r8), %esi
	cmpl	$126, %esi
	jne	.LBB0_221
# BB#220:                               #   in Loop: Header=BB0_218 Depth=3
	cmpb	$62, (%r8)
	movl	$-1, %eax
	cmovel	%eax, %esi
.LBB0_221:                              #   in Loop: Header=BB0_218 Depth=3
	leal	-33(%rsi), %eax
	cmpl	$85, %eax
	jb	.LBB0_225
# BB#222:                               #   in Loop: Header=BB0_218 Depth=3
	cmpl	$-1, %esi
	je	.LBB0_233
# BB#223:                               #   in Loop: Header=BB0_218 Depth=3
	cmpl	$122, %esi
	je	.LBB0_302
# BB#224:                               #   in Loop: Header=BB0_218 Depth=3
	decq	%rdx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movslq	%esi, %rcx
	incq	%r8
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_218
	jmp	.LBB0_299
.LBB0_225:                              #   in Loop: Header=BB0_184 Depth=2
	imull	$85, %ebp, %ecx
	addl	%ecx, %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movq	(%rsp), %rsi            # 8-byte Reload
	movb	%cl, (%rsi)
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 1(%rsi)
	movb	%ah, 2(%rsi)  # NOREX
	movb	%al, 3(%rsi)
	decq	%rdx
	movq	%r8, %rbp
	jmp	.LBB0_183
.LBB0_226:                              # %.loopexit178.i.loopexit1425
                                        #   in Loop: Header=BB0_42 Depth=1
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	jmp	.LBB0_234
.LBB0_227:                              #   in Loop: Header=BB0_42 Depth=1
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	movq	128(%rsp), %rcx         # 8-byte Reload
	callq	flatedecode
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB0_270
# BB#228:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.41, %edi
.LBB0_229:                              # %try_flatedecode.exit442
                                        #   in Loop: Header=BB0_42 Depth=1
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	%ebx, (%rsp)            # 4-byte Spill
	jmp	.LBB0_270
.LBB0_230:                              # %.loopexit178.i.loopexit1422
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$1, %ebx
	movl	%eax, %ebp
	jmp	.LBB0_234
.LBB0_231:                              # %.loopexit178.i.loopexit1418
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$2, %ebx
	jmp	.LBB0_234
.LBB0_232:                              # %.loopexit178.i.loopexit1414
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$3, %ebx
	movl	%eax, %ebp
	jmp	.LBB0_234
.LBB0_233:                              # %.loopexit178.i.loopexit
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$4, %ebx
.LBB0_234:                              # %.loopexit178.i
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movl	%ebx, %eax
	andb	$7, %al
	je	.LBB0_261
# BB#235:                               # %.loopexit178.i
                                        #   in Loop: Header=BB0_42 Depth=1
	cmpb	$1, %al
	je	.LBB0_307
# BB#236:                               # %.lr.ph142.i.preheader
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$5, %ecx
	subl	%ebx, %ecx
	cmpl	$8, %ecx
	jae	.LBB0_238
# BB#237:                               #   in Loop: Header=BB0_42 Depth=1
	movl	%ebx, %eax
	jmp	.LBB0_249
.LBB0_238:                              # %min.iters.checked1393
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	%ecx, %eax
	andl	$-8, %eax
	movl	%ecx, %edx
	andl	$-8, %edx
	je	.LBB0_242
# BB#239:                               # %vector.ph1397
                                        #   in Loop: Header=BB0_42 Depth=1
	movd	%ebp, %xmm0
	punpckldq	192(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	punpckldq	176(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	leal	-8(%rdx), %ebp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andl	$7, %esi
	je	.LBB0_243
# BB#240:                               # %vector.body1389.prol.preheader
                                        #   in Loop: Header=BB0_42 Depth=1
	negl	%esi
	xorl	%edi, %edi
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [1,1,1,1]
.LBB0_241:                              # %vector.body1389.prol
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	pshufd	$245, %xmm0, %xmm2      # xmm2 = xmm0[1,1,3,3]
	movdqa	.LCPI0_1(%rip), %xmm3   # xmm3 = [85,85,85,85]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	addl	$8, %edi
	incl	%esi
	jne	.LBB0_241
	jmp	.LBB0_244
.LBB0_242:                              #   in Loop: Header=BB0_42 Depth=1
	movl	%ebx, %eax
	jmp	.LBB0_249
.LBB0_243:                              #   in Loop: Header=BB0_42 Depth=1
	xorl	%edi, %edi
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [1,1,1,1]
.LBB0_244:                              # %vector.body1389.prol.loopexit
                                        #   in Loop: Header=BB0_42 Depth=1
	cmpl	$56, %ebp
	jb	.LBB0_247
# BB#245:                               # %vector.ph1397.new
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	%edx, %esi
	subl	%edi, %esi
.LBB0_246:                              # %vector.body1389
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	pshufd	$245, %xmm0, %xmm2      # xmm2 = xmm0[1,1,3,3]
	movdqa	.LCPI0_2(%rip), %xmm3   # xmm3 = [1904149089,1904149089,1904149089,1904149089]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	addl	$-64, %esi
	jne	.LBB0_246
.LBB0_247:                              # %middle.block1390
                                        #   in Loop: Header=BB0_42 Depth=1
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movd	%xmm0, %ebp
	cmpl	%edx, %ecx
	je	.LBB0_251
# BB#248:                               #   in Loop: Header=BB0_42 Depth=1
	orl	%ebx, %eax
.LBB0_249:                              # %.lr.ph142.i.preheader1428
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$5, %ecx
	subl	%eax, %ecx
.LBB0_250:                              # %.lr.ph142.i
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imull	$85, %ebp, %ebp
	decl	%ecx
	jne	.LBB0_250
.LBB0_251:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_42 Depth=1
	cmpl	$1, %ebx
	jbe	.LBB0_260
# BB#252:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_42 Depth=1
	leal	-16(,%rbx,8), %ecx
	movl	$16777215, %eax         # imm = 0xFFFFFF
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	addl	%eax, %ebp
	leal	-2(%rbx), %r8d
	incq	%r8
	cmpq	$4, %r8
	jb	.LBB0_257
# BB#253:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	%r8, %rax
	movabsq	$8589934588, %rdx       # imm = 0x1FFFFFFFC
	movq	%rdx, %rsi
	andq	%rsi, %rax
	movq	%r8, %rdx
	andq	%rsi, %rdx
	je	.LBB0_257
# BB#254:                               # %vector.ph
                                        #   in Loop: Header=BB0_42 Depth=1
	addq	(%rsp), %rax            # 8-byte Folded Reload
	movd	%ebp, %xmm0
	xorl	%esi, %esi
.LBB0_255:                              # %vector.body
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	pshufd	$68, %xmm0, %xmm1       # xmm1 = xmm0[0,1,0,1]
	movdqa	.LCPI0_3(%rip), %xmm2   # xmm2 = [4294967295,0,4294967295,0]
	movdqa	%xmm2, %xmm5
	pand	%xmm5, %xmm1
	movd	%rsi, %xmm2
	pshufd	$68, %xmm2, %xmm2       # xmm2 = xmm2[0,1,0,1]
	psllq	$3, %xmm2
	movdqa	.LCPI0_4(%rip), %xmm3   # xmm3 = [24,16]
	psubq	%xmm2, %xmm3
	pand	%xmm5, %xmm3
	movl	$8, %edi
	movd	%rdi, %xmm4
	psubq	%xmm2, %xmm4
	pand	%xmm5, %xmm4
	pshufd	$78, %xmm3, %xmm2       # xmm2 = xmm3[2,3,0,1]
	movdqa	%xmm1, %xmm5
	psrlq	%xmm2, %xmm5
	movdqa	%xmm1, %xmm2
	psrlq	%xmm3, %xmm2
	movsd	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1]
	pshufd	$78, %xmm4, %xmm2       # xmm2 = xmm4[2,3,0,1]
	movdqa	%xmm1, %xmm3
	psrlq	%xmm2, %xmm3
	psrlq	%xmm4, %xmm1
	movsd	%xmm1, %xmm3            # xmm3 = xmm1[0],xmm3[1]
	movapd	.LCPI0_5(%rip), %xmm1   # xmm1 = [255,255]
	andpd	%xmm1, %xmm5
	packuswb	%xmm5, %xmm5
	packuswb	%xmm5, %xmm5
	packuswb	%xmm5, %xmm5
	movd	%xmm5, %edi
	movq	(%rsp), %rcx            # 8-byte Reload
	movw	%di, (%rcx,%rsi)
	andpd	%xmm1, %xmm3
	packuswb	%xmm3, %xmm3
	packuswb	%xmm3, %xmm3
	packuswb	%xmm3, %xmm3
	movd	%xmm3, %edi
	movw	%di, 2(%rcx,%rsi)
	addq	$4, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB0_255
# BB#256:                               # %middle.block
                                        #   in Loop: Header=BB0_42 Depth=1
	cmpq	%rdx, %r8
	jne	.LBB0_258
	jmp	.LBB0_260
.LBB0_257:                              #   in Loop: Header=BB0_42 Depth=1
	xorl	%edx, %edx
	movq	(%rsp), %rax            # 8-byte Reload
.LBB0_258:                              # %.lr.ph.i440.preheader
                                        #   in Loop: Header=BB0_42 Depth=1
	leal	-1(%rbx), %esi
	subl	%edx, %esi
	shll	$3, %edx
	movl	$24, %ecx
	subl	%edx, %ecx
.LBB0_259:                              # %.lr.ph.i440
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %edx
	shrl	%cl, %edx
	movb	%dl, (%rax)
	incq	%rax
	addl	$-8, %ecx
	decl	%esi
	jne	.LBB0_259
.LBB0_260:                              # %ascii85decode.exit.loopexit745
                                        #   in Loop: Header=BB0_42 Depth=1
	addl	8(%rsp), %ebx           # 4-byte Folded Reload
	movl	%ebx, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB0_261:                              # %ascii85decode.exit
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB0_267
# BB#262:                               # %ascii85decode.exit
                                        #   in Loop: Header=BB0_42 Depth=1
	cmpl	$-1, 8(%rsp)            # 4-byte Folded Reload
	je	.LBB0_300
# BB#263:                               #   in Loop: Header=BB0_42 Depth=1
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	cli_realloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_305
# BB#264:                               #   in Loop: Header=BB0_42 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB0_268
# BB#265:                               #   in Loop: Header=BB0_42 Depth=1
	movslq	8(%rsp), %rsi           # 4-byte Folded Reload
	movq	%rbx, %rdi
	movl	%r15d, %edx
	movq	128(%rsp), %rcx         # 8-byte Reload
	callq	flatedecode
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB0_269
# BB#266:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	%ebp, (%rsp)            # 4-byte Spill
	jmp	.LBB0_269
.LBB0_267:                              #   in Loop: Header=BB0_42 Depth=1
	movq	%r13, %rbx
	jmp	.LBB0_269
.LBB0_268:                              #   in Loop: Header=BB0_42 Depth=1
	movl	%r15d, %edi
	movq	%r12, %rsi
	movl	8(%rsp), %edx           # 4-byte Reload
	callq	cli_writen
.LBB0_269:                              # %try_flatedecode.exit.thread
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	%rbx, %rdi
	callq	free
.LBB0_270:                              # %try_flatedecode.exit442
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	%r15d, %edi
	callq	close
	leaq	208(%rsp), %rdi
	callq	cli_md5file
	movq	%rax, %rbx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	tableFind
	testl	%eax, %eax
	js	.LBB0_272
# BB#271:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	leaq	208(%rsp), %rbp
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	unlink
	jmp	.LBB0_273
.LBB0_272:                              #   in Loop: Header=BB0_42 Depth=1
	movl	$1, %edx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	tableInsert
.LBB0_273:                              #   in Loop: Header=BB0_42 Depth=1
	movq	%rbx, %rdi
	callq	free
	movl	84(%rsp), %esi          # 4-byte Reload
	incl	%esi
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	movl	%esi, 84(%rsp)          # 4-byte Spill
	leaq	208(%rsp), %rdx
	callq	cli_dbgmsg
	cmpq	$0, 152(%rsp)           # 8-byte Folded Reload
	je	.LBB0_275
# BB#274:                               #   in Loop: Header=BB0_42 Depth=1
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	4(%rax), %esi
	leal	-1(%rsi), %eax
	cmpl	84(%rsp), %eax          # 4-byte Folded Reload
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	72(%rsp), %r12          # 8-byte Reload
	jae	.LBB0_276
	jmp	.LBB0_304
.LBB0_275:                              #   in Loop: Header=BB0_42 Depth=1
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	72(%rsp), %r12          # 8-byte Reload
.LBB0_276:                              # %.thread467
                                        #   in Loop: Header=BB0_42 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB0_292
.LBB0_277:                              # %.thread467
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	%r12, 72(%rsp)          # 8-byte Spill
	cmpq	%r13, %r12
	jb	.LBB0_42
	jmp	.LBB0_292
.LBB0_278:                              # %cli_pmemstr.exit
	testq	%rbp, %rbp
	je	.LBB0_25
.LBB0_279:                              # %cli_pmemstr.exit.thread444
	movq	120(%rsp), %rdi         # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_281
# BB#280:
	callq	free
	jmp	.LBB0_282
.LBB0_281:
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	munmap
.LBB0_282:
	movl	$-124, %ebp
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_15
.LBB0_283:                              # %.loopexit499
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	$.L.str.13, %edi
.LBB0_284:                              # %pdf_nextlinestart.exit.thread
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_291
.LBB0_285:
	movl	$.L.str.10, %edi
	jmp	.LBB0_289
.LBB0_286:
	movl	$.L.str.12, %edi
	jmp	.LBB0_289
.LBB0_287:
	movl	$0, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB0_292
.LBB0_288:
	movl	$.L.str.9, %edi
.LBB0_289:                              # %pdf_nextlinestart.exit.thread
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB0_290:                              # %.critedge
	movl	$-124, (%rsp)           # 4-byte Folded Spill
.LBB0_291:                              # %.critedge
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB0_292:                              # %.critedge
	movq	120(%rsp), %rdi         # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_294
# BB#293:
	callq	free
	jmp	.LBB0_295
.LBB0_294:
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	munmap
.LBB0_295:
	movl	(%rsp), %ebp            # 4-byte Reload
	movq	%r14, %rdi
	callq	tableDestroy
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	jmp	.LBB0_15
.LBB0_296:
	movl	$0, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB0_292
.LBB0_297:
	movl	$0, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB0_291
.LBB0_298:                              # %.loopexit
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	$.L.str.29, %edi
	jmp	.LBB0_284
.LBB0_299:                              # %.loopexit179.i
	movzbl	%sil, %esi
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB0_300:                              # %ascii85decode.exit.thread
	movq	%r13, %rdi
	callq	free
	movl	%r15d, %edi
	callq	close
	leaq	208(%rsp), %rdi
	callq	unlink
	jmp	.LBB0_290
.LBB0_301:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	leaq	208(%rsp), %rsi
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	cli_errmsg
	movl	$-112, (%rsp)           # 4-byte Folded Spill
	jmp	.LBB0_291
.LBB0_302:                              # %.loopexit243.i
	movl	$.L.str.56, %edi
.LBB0_303:                              # %ascii85decode.exit.thread
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_300
.LBB0_304:
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	$-102, (%rsp)           # 4-byte Folded Spill
	jmp	.LBB0_292
.LBB0_305:                              # %try_flatedecode.exit
	movq	%r13, %rdi
	callq	free
.LBB0_306:
	movl	%r15d, %edi
	callq	close
	leaq	208(%rsp), %rdi
	callq	unlink
	movl	$-114, (%rsp)           # 4-byte Folded Spill
	jmp	.LBB0_291
.LBB0_307:
	movl	$.L.str.58, %edi
	jmp	.LBB0_303
.Lfunc_end0:
	.size	cli_pdf, .Lfunc_end0-cli_pdf
	.cfi_endproc

	.p2align	4, 0x90
	.type	pdf_nextobject,@function
pdf_nextobject:                         # @pdf_nextobject
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB1_23
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
.LBB1_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #     Child Loop BB1_16 Depth 2
                                        #     Child Loop BB1_12 Depth 2
	testb	$1, %cl
	je	.LBB1_5
# BB#3:                                 # %.lr.ph.split.us
                                        #   in Loop: Header=BB1_2 Depth=1
	movsbl	(%rdi), %ecx
	leal	-9(%rcx), %eax
	cmpl	$82, %eax
	ja	.LBB1_24
# BB#4:                                 # %.lr.ph.split.us
                                        #   in Loop: Header=BB1_2 Depth=1
	jmpq	*.LJTI1_1(,%rax,8)
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph.split
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rdi), %ecx
	leal	-9(%rcx), %eax
	cmpl	$82, %eax
	ja	.LBB1_22
# BB#6:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB1_5 Depth=2
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_22:                               #   in Loop: Header=BB1_5 Depth=2
	incq	%rdi
	decq	%rsi
	jne	.LBB1_5
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_20:                               # %.us-lcssa41.us
                                        #   in Loop: Header=BB1_2 Depth=1
	incq	%rdi
	decq	%rsi
	movq	%rdi, %r9
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_7:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %edx
	shll	%cl, %edx
	cmpb	$15, %cl
	movq	%rsi, %r8
	movq	%rdi, %r9
	ja	.LBB1_16
# BB#8:                                 # %.us-lcssa.us
                                        #   in Loop: Header=BB1_2 Depth=1
	andl	$9217, %edx             # imm = 0x2401
	testw	%dx, %dx
	movq	%rdi, %r9
	movq	%rsi, %r8
	jne	.LBB1_9
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph30.i
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$1, %r8
	je	.LBB1_23
# BB#17:                                #   in Loop: Header=BB1_16 Depth=2
	decq	%r8
	movb	1(%r9), %cl
	incq	%r9
	movl	$1, %edx
	shll	%cl, %edx
	cmpb	$15, %cl
	ja	.LBB1_16
# BB#18:                                #   in Loop: Header=BB1_16 Depth=2
	andl	$9217, %edx             # imm = 0x2401
	testw	%dx, %dx
	je	.LBB1_16
.LBB1_9:                                # %.preheader.i
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %eax
	shll	%cl, %eax
	cmpb	$15, %cl
	ja	.LBB1_19
# BB#10:                                # %.preheader.i
                                        #   in Loop: Header=BB1_2 Depth=1
	andl	$9217, %eax             # imm = 0x2401
	testw	%ax, %ax
	je	.LBB1_19
# BB#11:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %edx
	subq	%r8, %rdx
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph.i
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rdx, %rdx
	je	.LBB1_23
# BB#13:                                #   in Loop: Header=BB1_12 Depth=2
	movzbl	1(%r9), %ecx
	incq	%r9
	movl	$1, %eax
	shll	%cl, %eax
	cmpb	$15, %cl
	ja	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_12 Depth=2
	andl	$9217, %eax             # imm = 0x2401
	incq	%rdx
	testw	%ax, %ax
	jne	.LBB1_12
	jmp	.LBB1_15
.LBB1_19:                               # %pdf_nextlinestart.exit
                                        #   in Loop: Header=BB1_2 Depth=1
	testq	%r9, %r9
	je	.LBB1_23
.LBB1_15:                               # %pdf_nextlinestart.exit.thread21
                                        #   in Loop: Header=BB1_2 Depth=1
	addq	%rdi, %rsi
	subq	%r9, %rsi
.LBB1_21:                               # %.outer.backedge
                                        #   in Loop: Header=BB1_2 Depth=1
	movb	$1, %cl
	testq	%rsi, %rsi
	movq	%r9, %rdi
	jne	.LBB1_2
.LBB1_23:
	xorl	%edi, %edi
.LBB1_24:                               # %pdf_nextlinestart.exit.thread
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	pdf_nextobject, .Lfunc_end1-pdf_nextobject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_20
	.quad	.LBB1_7
	.quad	.LBB1_20
	.quad	.LBB1_20
	.quad	.LBB1_7
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_20
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_7
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_24
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_20
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_20
.LJTI1_1:
	.quad	.LBB1_20
	.quad	.LBB1_7
	.quad	.LBB1_20
	.quad	.LBB1_20
	.quad	.LBB1_7
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_20
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_7
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_20
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_20

	.text
	.p2align	4, 0x90
	.type	flatedecode,@function
flatedecode:                            # @flatedecode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$8312, %rsp             # imm = 0x2078
.Lcfi19:
	.cfi_def_cfa_offset 8368
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movl	%edx, %r13d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testq	%rbp, %rbp
	je	.LBB2_3
# BB#1:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rsp)
	movq	$0, 80(%rsp)
	movq	%rbx, (%rsp)
	movl	%ebp, 8(%rsp)
	leaq	112(%rsp), %r12
	movq	%r12, 24(%rsp)
	movl	$8192, 32(%rsp)         # imm = 0x2000
	movq	%rsp, %rdi
	movl	$.L.str.44, %esi
	movl	$112, %edx
	callq	inflateInit_
	testl	%eax, %eax
	je	.LBB2_4
# BB#2:
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB2_30:
	movl	$-104, %r14d
	jmp	.LBB2_31
.LBB2_3:
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB2_31
.LBB2_4:                                # %.preheader
	xorl	%r14d, %r14d
	movq	%rsp, %rbx
	cmpl	$0, 8(%rsp)
	jne	.LBB2_7
	jmp	.LBB2_15
.LBB2_5:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%r12, 24(%rsp)
	movl	$8192, 32(%rsp)         # imm = 0x2000
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_7 Depth=1
	cmpl	$0, 8(%rsp)
	je	.LBB2_15
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	inflate
	movl	%eax, %ecx
	testl	%ecx, %ecx
	jne	.LBB2_14
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	cmpl	$0, 32(%rsp)
	jne	.LBB2_6
# BB#9:                                 #   in Loop: Header=BB2_7 Depth=1
	movl	$8192, %edx             # imm = 0x2000
	movl	%r13d, %edi
	movq	%r12, %rsi
	callq	cli_writen
	cltq
	addq	%rax, %r14
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB2_5
# BB#10:                                #   in Loop: Header=BB2_7 Depth=1
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB2_5
# BB#11:                                #   in Loop: Header=BB2_7 Depth=1
	cmpq	%rax, %r14
	jle	.LBB2_5
# BB#12:
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	%rsp, %rdi
	callq	inflateEnd
	movl	$-104, %r14d
	testb	$1, 41(%r15)
	je	.LBB2_31
# BB#13:
	movq	(%r15), %rax
	movq	$.L.str.47, (%rax)
	movl	$1, %r14d
	jmp	.LBB2_31
.LBB2_14:
	cmpl	$1, %ecx
	jne	.LBB2_25
.LBB2_15:
	movl	32(%rsp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	je	.LBB2_17
# BB#16:
	movl	$8192, %edx             # imm = 0x2000
	subl	%eax, %edx
	leaq	112(%rsp), %rsi
	movl	%r13d, %edi
	callq	cli_writen
	testl	%eax, %eax
	js	.LBB2_27
.LBB2_17:
	movq	16(%rsp), %rsi
	movq	40(%rsp), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	movq	%rax, %rbp
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB2_19
# BB#18:
	movl	12(%rax), %r8d
.LBB2_19:
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	movq	%rbp, %rcx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	cli_dbgmsg
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB2_24
# BB#20:
	movl	12(%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB2_24
# BB#21:
	movq	40(%rsp), %rax
	xorl	%edx, %edx
	divq	16(%rsp)
	cmpq	%rcx, %rax
	jbe	.LBB2_24
# BB#22:
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rsp, %rdi
	callq	inflateEnd
	movl	$-104, %r14d
	testb	$1, 41(%r15)
	je	.LBB2_31
# BB#23:
	movq	(%r15), %rax
	movq	$.L.str.52, (%rax)
	movl	$1, %r14d
	jmp	.LBB2_31
.LBB2_24:
	movq	%rsp, %rdi
	callq	inflateEnd
	testl	%eax, %eax
	movl	$-104, %r14d
	cmovel	%eax, %r14d
	jmp	.LBB2_31
.LBB2_25:
	movq	48(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB2_28
# BB#26:
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB2_29
.LBB2_27:
	movl	$-123, %r14d
.LBB2_31:
	movl	%r14d, %eax
	addq	$8312, %rsp             # imm = 0x2078
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_28:
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	movl	%ecx, %edx
	callq	cli_dbgmsg
.LBB2_29:
	movq	%rsp, %rdi
	callq	inflateEnd
	jmp	.LBB2_30
.Lfunc_end2:
	.size	flatedecode, .Lfunc_end2-flatedecode
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in cli_pdf(%s)\n"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"cli_pdf: scanning %lu bytes\n"
	.size	.L.str.1, 29

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%PDF-1."
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%%EOF"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"trailer"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Encrypt"
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Encrypted PDF files not yet supported\n"
	.size	.L.str.6, 39

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"xref"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"endobj"
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cli_pdf: Object number missing\n"
	.size	.L.str.9, 32

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cli_pdf: Generation number missing\n"
	.size	.L.str.10, 36

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"obj"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Indirect object missing \"obj\"\n"
	.size	.L.str.12, 31

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"No matching endobj\n"
	.size	.L.str.13, 20

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"stream"
	.size	.L.str.14, 7

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Length "
	.size	.L.str.15, 8

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	" 0 R"
	.size	.L.str.16, 5

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Length is in indirect obj %ld\n"
	.size	.L.str.17, 31

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\n%ld 0 obj"
	.size	.L.str.18, 11

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"length in '%s' %ld\n"
	.size	.L.str.19, 20

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Couldn't find '%s'\n"
	.size	.L.str.20, 20

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Length2 "
	.size	.L.str.21, 9

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Predictor "
	.size	.L.str.22, 11

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"FlateDecode"
	.size	.L.str.23, 12

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"ASCII85Decode"
	.size	.L.str.24, 14

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Embedded fonts not yet supported\n"
	.size	.L.str.25, 34

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Predictor %d not honoured for embedded image\n"
	.size	.L.str.26, 46

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"endstream\n"
	.size	.L.str.27, 11

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"endstream\r"
	.size	.L.str.28, 11

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"No endstream\n"
	.size	.L.str.29, 14

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%s/pdfXXXXXX"
	.size	.L.str.30, 13

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"cli_pdf: can't create temporary file %s: %s\n"
	.size	.L.str.31, 45

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Empty stream\n"
	.size	.L.str.32, 14

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"cli_pdf: Incorrect Length field in file attempting to recover\n"
	.size	.L.str.33, 63

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"length %ld, calculated_streamlen %ld isFlate %d isASCII85 %d\n"
	.size	.L.str.34, 62

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"cli_pdf: writing %lu bytes from the stream\n"
	.size	.L.str.35, 44

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"cli_pdf: not scanning duplicate embedded file '%s'\n"
	.size	.L.str.36, 52

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"cli_pdf: extracted file %d to %s\n"
	.size	.L.str.37, 34

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"cli_pdf: number of files exceeded %u\n"
	.size	.L.str.38, 38

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"cli_pdf: returning %d\n"
	.size	.L.str.39, 23

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"Bad compression in flate stream\n"
	.size	.L.str.40, 33

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"cli_pdf: Bad compressed block length in flate stream\n"
	.size	.L.str.41, 54

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"cli_pdf: flatedecode %lu bytes\n"
	.size	.L.str.42, 32

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"cli_pdf: flatedecode len == 0\n"
	.size	.L.str.43, 31

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"1.2.8"
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"cli_pdf: inflateInit failed"
	.size	.L.str.45, 28

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"cli_pdf: flatedecode size exceeded (%lu)\n"
	.size	.L.str.46, 42

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"PDF.ExceededFileSize"
	.size	.L.str.47, 21

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"pdf: after writing %lu bytes, got error \"%s\" inflating PDF attachment\n"
	.size	.L.str.48, 71

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"pdf: after writing %lu bytes, got error %d inflating PDF attachment\n"
	.size	.L.str.49, 69

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"cli_pdf: flatedecode in=%lu out=%lu ratio %lu (max %u)\n"
	.size	.L.str.50, 56

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"cli_pdf: flatedecode Max ratio reached\n"
	.size	.L.str.51, 40

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"Oversized.PDF"
	.size	.L.str.52, 14

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"~>"
	.size	.L.str.53, 3

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"ascii85decode: no EOF marker found\n"
	.size	.L.str.54, 36

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"cli_pdf: ascii85decode %lu bytes\n"
	.size	.L.str.55, 34

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"ascii85decode: unexpected 'z'\n"
	.size	.L.str.56, 31

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"ascii85decode: quintet %d\n"
	.size	.L.str.57, 27

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"ascii85Decode: only 1 byte in last quintet\n"
	.size	.L.str.58, 44

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"ascii85Decode: invalid character 0x%x, len %lu\n"
	.size	.L.str.59, 48


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
