	.text
	.file	"d8-ret.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	39                      # 0x27
.LCPI0_1:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	16                      # 0x10
	.text
	.globl	_Z8doreturnP9Classfile
	.p2align	4, 0x90
	.type	_Z8doreturnP9Classfile,@function
_Z8doreturnP9Classfile:                 # @_Z8doreturnP9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %ebp
	decl	%ebp
	cmpl	$177, ch(%rip)
	jne	.LBB0_7
# BB#1:
	cmpl	$0, bufflength(%rip)
	jle	.LBB0_14
# BB#2:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp3:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp4:
# BB#3:                                 # %.noexc
	movq	$.L.str, (%rbx)
	movl	$0, 8(%rbx)
	movl	$1, 12(%rbx)
	movl	$0, 16(%rbx)
	movl	$1, 8(%r14)
	movl	%ebp, 16(%r14)
	movl	%ebp, 12(%r14)
.Ltmp5:
	movl	$24, %edi
	callq	_Znwm
.Ltmp6:
# BB#4:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,0,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r14)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$1, 8(%rbx)
	movl	%ebp, 16(%rbx)
	movl	%ebp, 12(%rbx)
.Ltmp8:
	movl	$24, %edi
	callq	_Znwm
.Ltmp9:
	jmp	.LBB0_13
.LBB0_7:
	movq	stkptr(%rip), %rax
	movq	-8(%rax), %r14
	movq	(%r14), %rcx
	cmpl	$4, 8(%rcx)
	jne	.LBB0_12
# BB#8:
	movq	miptr(%rip), %rdx
	cmpl	$10, 128(%rdx)
	jne	.LBB0_12
# BB#9:
	movl	$std_exps+48, %edx
	cmpq	%rdx, %rcx
	je	.LBB0_11
# BB#10:
	movl	$std_exps+72, %edx
	cmpq	%rdx, %rcx
	jne	.LBB0_12
.LBB0_11:
	addq	$312, %rcx              # imm = 0x138
	movq	%rcx, (%r14)
	movq	stkptr(%rip), %rax
.LBB0_12:
	addq	$-8, %rax
	movq	%rax, stkptr(%rip)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r14), %eax
	cmpl	%eax, %ebp
	cmovbl	%ebp, %eax
	movl	$1, 8(%rbx)
	movl	%ebp, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp0:
	movl	$24, %edi
	callq	_Znwm
.Ltmp1:
.LBB0_13:                               # %.sink.split
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,2,0,16]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%r14, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	donestkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, donestkptr(%rip)
	movq	%rbx, (%rax)
.LBB0_14:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:
.Ltmp10:
	jmp	.LBB0_6
.LBB0_17:
.Ltmp2:
.LBB0_6:
	movq	%rax, %r15
	movq	%rbx, %rdi
	jmp	.LBB0_16
.LBB0_15:
.Ltmp7:
	movq	%rax, %r15
	movq	%r14, %rdi
.LBB0_16:
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z8doreturnP9Classfile, .Lfunc_end0-_Z8doreturnP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp6           #   Call between .Ltmp6 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp0-.Ltmp9           #   Call between .Ltmp9 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/* void */"
	.size	.L.str, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
