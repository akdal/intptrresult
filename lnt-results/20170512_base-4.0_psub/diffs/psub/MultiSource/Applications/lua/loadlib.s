	.text
	.file	"loadlib.bc"
	.globl	luaopen_package
	.p2align	4, 0x90
	.type	luaopen_package,@function
luaopen_package:                        # @luaopen_package
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str, %esi
	callq	luaL_newmetatable
	movl	$gctm, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-2, %esi
	movl	$.L.str.1, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$.L.str.2, %esi
	movl	$pk_funcs, %edx
	movq	%rbx, %rdi
	callq	luaL_register
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-10001, %esi           # imm = 0xD8EF
	movq	%rbx, %rdi
	callq	lua_replace
	xorl	%esi, %esi
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$loader_preload, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-2, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_rawseti
	movl	$loader_Lua, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-2, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	lua_rawseti
	movl	$loader_C, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-2, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	lua_rawseti
	movl	$loader_Croot, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-2, %esi
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	lua_rawseti
	movl	$-2, %esi
	movl	$.L.str.3, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$.L.str.5, %edi
	callq	getenv
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	movl	$.L.str.38, %edx
	movl	$.L.str.39, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaL_gsub
	movl	$.L.str.40, %edx
	movl	$.L.str.6, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaL_gsub
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_remove
	jmp	.LBB0_3
.LBB0_1:
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	lua_pushstring
.LBB0_3:                                # %setpath.exit
	movl	$-2, %esi
	movl	$.L.str.4, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$.L.str.8, %edi
	callq	getenv
	testq	%rax, %rax
	je	.LBB0_4
# BB#5:
	movl	$.L.str.38, %edx
	movl	$.L.str.39, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaL_gsub
	movl	$.L.str.40, %edx
	movl	$.L.str.9, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaL_gsub
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_remove
	jmp	.LBB0_6
.LBB0_4:
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	callq	lua_pushstring
.LBB0_6:                                # %setpath.exit24
	movl	$-2, %esi
	movl	$.L.str.7, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$.L.str.10, %esi
	movl	$9, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	movl	$-2, %esi
	movl	$.L.str.11, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.12, %edx
	movl	$2, %ecx
	movq	%rbx, %rdi
	callq	luaL_findtable
	movl	$-2, %esi
	movl	$.L.str.13, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$-2, %esi
	movl	$.L.str.14, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$-10002, %esi           # imm = 0xD8EE
	movq	%rbx, %rdi
	callq	lua_pushvalue
	xorl	%esi, %esi
	movl	$ll_funcs, %edx
	movq	%rbx, %rdi
	callq	luaL_register
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	luaopen_package, .Lfunc_end0-luaopen_package
	.cfi_endproc

	.p2align	4, 0x90
	.type	gctm,@function
gctm:                                   # @gctm
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movl	$1, %esi
	movl	$.L.str, %edx
	callq	luaL_checkudata
	movq	$0, (%rax)
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	gctm, .Lfunc_end1-gctm
	.cfi_endproc

	.p2align	4, 0x90
	.type	ll_loadlib,@function
ll_loadlib:                             # @ll_loadlib
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %r14d
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %rbp
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaL_checklstring
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	ll_loadfunc
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB2_2
# BB#1:
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	cmpl	$1, %ebp
	movl	$.L.str.17, %eax
	movl	$.L.str.18, %esi
	cmoveq	%rax, %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$3, %r14d
.LBB2_2:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	ll_loadlib, .Lfunc_end2-ll_loadlib
	.cfi_endproc

	.p2align	4, 0x90
	.type	ll_seeall,@function
ll_seeall:                              # @ll_seeall
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_getmetatable
	testl	%eax, %eax
	jne	.LBB3_2
# BB#1:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
.LBB3_2:
	movl	$-10002, %esi           # imm = 0xD8EE
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-2, %esi
	movl	$.L.str.22, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	ll_seeall, .Lfunc_end3-ll_seeall
	.cfi_endproc

	.p2align	4, 0x90
	.type	ll_loadfunc,@function
ll_loadfunc:                            # @ll_loadfunc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$.L.str.19, %esi
	movl	$.L.str.20, %edx
	xorl	%eax, %eax
	movq	%r14, %rcx
	callq	lua_pushfstring
	movl	$-10000, %esi           # imm = 0xD8F0
	movq	%rbx, %rdi
	callq	lua_gettable
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB4_2
# BB#1:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_touserdata
	movq	%rax, %r15
	jmp	.LBB4_3
.LBB4_2:
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	lua_newuserdata
	movq	%rax, %r15
	movq	$0, (%r15)
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movl	$.L.str.19, %esi
	movl	$.L.str.20, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rcx
	callq	lua_pushfstring
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-10000, %esi           # imm = 0xD8F0
	movq	%rbx, %rdi
	callq	lua_settable
.LBB4_3:                                # %ll_register.exit
	movq	(%r15), %r14
	movl	$.L.str.21, %esi
	movl	$58, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	cmpq	$0, %r14
	movl	$2, %eax
	jne	.LBB4_5
# BB#4:                                 # %.thread
	movq	$0, (%r15)
	movl	$1, %eax
.LBB4_5:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	ll_loadfunc, .Lfunc_end4-ll_loadfunc
	.cfi_endproc

	.p2align	4, 0x90
	.type	loader_preload,@function
loader_preload:                         # @loader_preload
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$-10001, %esi           # imm = 0xD8EF
	movl	$.L.str.14, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$5, %eax
	je	.LBB5_2
# BB#1:
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB5_2:
	movl	$-1, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	jne	.LBB5_4
# BB#3:
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_pushfstring
.LBB5_4:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	loader_preload, .Lfunc_end5-loader_preload
	.cfi_endproc

	.p2align	4, 0x90
	.type	loader_Lua,@function
loader_Lua:                             # @loader_Lua
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movl	$.L.str.4, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	findfile
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB6_3
# BB#1:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	luaL_loadfile
	testl	%eax, %eax
	je	.LBB6_3
# BB#2:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movq	%rax, %r15
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movq	%rax, %r8
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	luaL_error
.LBB6_3:
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	loader_Lua, .Lfunc_end6-loader_Lua
	.cfi_endproc

	.p2align	4, 0x90
	.type	loader_C,@function
loader_C:                               # @loader_C
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -40
.Lcfi34:
	.cfi_offset %r12, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %rbx
	movl	$.L.str.7, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	findfile
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB7_3
# BB#1:
	movl	$45, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbx, %rsi
	movl	$.L.str.25, %edx
	movl	$.L.str.35, %ecx
	movq	%r15, %rdi
	callq	luaL_gsub
	movq	%rax, %rcx
	movl	$.L.str.36, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_remove
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	ll_loadfunc
	testl	%eax, %eax
	je	.LBB7_3
# BB#2:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	lua_tolstring
	movq	%rax, %r12
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	lua_tolstring
	movq	%rax, %rbx
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%rbx, %r8
	callq	luaL_error
.LBB7_3:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	loader_C, .Lfunc_end7-loader_C
	.cfi_endproc

	.p2align	4, 0x90
	.type	loader_Croot,@function
loader_Croot:                           # @loader_Croot
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -48
.Lcfi43:
	.cfi_offset %r12, -40
.Lcfi44:
	.cfi_offset %r13, -32
.Lcfi45:
	.cfi_offset %r14, -24
.Lcfi46:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$46, %esi
	movq	%r14, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB8_7
# BB#1:
	subq	%r14, %rax
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	lua_pushlstring
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	lua_tolstring
	movl	$.L.str.7, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	findfile
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB8_6
# BB#2:
	movl	$45, %esi
	movq	%r14, %rdi
	callq	strchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%r14, %rsi
	movl	$.L.str.25, %edx
	movl	$.L.str.35, %ecx
	movq	%r12, %rdi
	callq	luaL_gsub
	movq	%rax, %rcx
	movl	$.L.str.36, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movl	$-2, %esi
	movq	%r12, %rdi
	callq	lua_remove
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	ll_loadfunc
	testl	%eax, %eax
	je	.LBB8_6
# BB#3:
	cmpl	$2, %eax
	je	.LBB8_5
# BB#4:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	lua_tolstring
	movq	%rax, %r13
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	lua_tolstring
	movq	%rax, %rbx
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r8
	callq	luaL_error
.LBB8_5:
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	lua_pushfstring
.LBB8_6:
	movl	$1, %ebx
.LBB8_7:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	loader_Croot, .Lfunc_end8-loader_Croot
	.cfi_endproc

	.p2align	4, 0x90
	.type	findfile,@function
findfile:                               # @findfile
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 48
.Lcfi52:
	.cfi_offset %rbx, -48
.Lcfi53:
	.cfi_offset %r12, -40
.Lcfi54:
	.cfi_offset %r13, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movq	%rdx, %r12
	movq	%rdi, %r15
	movl	$.L.str.25, %edx
	movl	$.L.str.26, %ecx
	callq	luaL_gsub
	movq	%rax, %r14
	movl	$-10001, %esi           # imm = 0xD8EF
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	lua_getfield
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	lua_tolstring
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB9_2
# BB#1:
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	luaL_error
.LBB9_2:
	movl	$.L.str.28, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	lua_pushlstring
	jmp	.LBB9_3
	.p2align	4, 0x90
.LBB9_10:                               # %.thread
                                        #   in Loop: Header=BB9_3 Depth=1
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	lua_pushfstring
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_remove
	movl	$2, %esi
	movq	%r15, %rdi
	callq	lua_concat
.LBB9_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_4 Depth 2
	movq	%r13, %rbx
	jmp	.LBB9_4
	.p2align	4, 0x90
.LBB9_14:                               #   in Loop: Header=BB9_4 Depth=2
	incq	%rbx
.LBB9_4:                                #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %eax
	cmpb	$59, %al
	je	.LBB9_14
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=1
	testb	%al, %al
	je	.LBB9_12
# BB#6:                                 #   in Loop: Header=BB9_3 Depth=1
	movl	$59, %esi
	movq	%rbx, %rdi
	callq	strchr
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_3 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r13
	addq	%rbx, %r13
.LBB9_8:                                # %pushnexttemplate.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%r13, %rdx
	subq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	lua_pushlstring
	testq	%r13, %r13
	je	.LBB9_12
# BB#9:                                 #   in Loop: Header=BB9_3 Depth=1
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	lua_tolstring
	movl	$.L.str.29, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%r14, %rcx
	callq	luaL_gsub
	movq	%rax, %rbx
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_remove
	movl	$.L.str.32, %esi
	movq	%rbx, %rdi
	callq	fopen
	testq	%rax, %rax
	je	.LBB9_10
# BB#11:
	movq	%rax, %rdi
	callq	fclose
	jmp	.LBB9_13
.LBB9_12:                               # %pushnexttemplate.exit.thread.loopexit
	xorl	%ebx, %ebx
.LBB9_13:                               # %pushnexttemplate.exit.thread
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	findfile, .Lfunc_end9-findfile
	.cfi_endproc

	.p2align	4, 0x90
	.type	ll_module,@function
ll_module:                              # @ll_module
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 40
	subq	$120, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 160
.Lcfi62:
	.cfi_offset %rbx, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	lua_gettop
	movl	%eax, %r14d
	leal	1(%r14), %ebp
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.12, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$5, %eax
	je	.LBB10_3
# BB#1:
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$-10002, %esi           # imm = 0xD8EE
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	luaL_findtable
	testq	%rax, %rax
	je	.LBB10_2
# BB#13:
	movl	$.L.str.43, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaL_error              # TAILCALL
.LBB10_2:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	lua_setfield
.LBB10_3:
	movl	$-1, %esi
	movl	$.L.str.44, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	movl	%eax, %ebp
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	testl	%ebp, %ebp
	jne	.LBB10_5
# BB#4:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-2, %esi
	movl	$.L.str.45, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	lua_pushstring
	movl	$-2, %esi
	movl	$.L.str.44, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$46, %esi
	movq	%r15, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmoveq	%r15, %rdx
	subq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	lua_pushlstring
	movl	$-2, %esi
	movl	$.L.str.46, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
.LBB10_5:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movq	%rsp, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_getstack
	testl	%eax, %eax
	je	.LBB10_8
# BB#6:
	movq	%rsp, %rdx
	movl	$.L.str.47, %esi
	movq	%rbx, %rdi
	callq	lua_getinfo
	testl	%eax, %eax
	je	.LBB10_8
# BB#7:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_iscfunction
	testl	%eax, %eax
	je	.LBB10_9
.LBB10_8:
	movl	$.L.str.48, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB10_9:                               # %setfenv.exit
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setfenv
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	cmpl	$2, %r14d
	jl	.LBB10_12
# BB#10:                                # %.lr.ph.i.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB10_11:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_pushvalue
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_call
	cmpl	%ebp, %r14d
	jne	.LBB10_11
.LBB10_12:                              # %dooptions.exit
	xorl	%eax, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	ll_module, .Lfunc_end10-ll_module
	.cfi_endproc

	.p2align	4, 0x90
	.type	ll_require,@function
ll_require:                             # @ll_require
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -32
.Lcfi70:
	.cfi_offset %r14, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.12, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_toboolean
	testl	%eax, %eax
	je	.LBB11_3
# BB#1:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_touserdata
	movl	$sentinel_, %ecx
	cmpq	%rcx, %rax
	jne	.LBB11_16
# BB#2:
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	luaL_error
	jmp	.LBB11_16
.LBB11_3:
	movl	$-10001, %esi           # imm = 0xD8EF
	movl	$.L.str.3, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$5, %eax
	je	.LBB11_5
# BB#4:
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB11_5:
	movl	$.L.str.28, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	movl	$1, %ebp
	jmp	.LBB11_6
	.p2align	4, 0x90
.LBB11_10:                              #   in Loop: Header=BB11_6 Depth=1
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_concat
	incl	%ebp
.LBB11_6:                               # =>This Inner Loop Header: Depth=1
	movl	$-2, %esi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	lua_rawgeti
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	jne	.LBB11_8
# BB#7:                                 #   in Loop: Header=BB11_6 Depth=1
	movl	$-2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movq	%rax, %rcx
	movl	$.L.str.51, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	luaL_error
.LBB11_8:                               #   in Loop: Header=BB11_6 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lua_pushstring
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$6, %eax
	je	.LBB11_12
# BB#9:                                 #   in Loop: Header=BB11_6 Depth=1
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_isstring
	testl	%eax, %eax
	jne	.LBB11_10
# BB#11:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	incl	%ebp
	jmp	.LBB11_6
.LBB11_12:
	movl	$sentinel_, %esi
	movq	%rbx, %rdi
	callq	lua_pushlightuserdata
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_setfield
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lua_pushstring
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB11_14
# BB#13:
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_setfield
.LBB11_14:
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_touserdata
	movl	$sentinel_, %ecx
	cmpq	%rcx, %rax
	jne	.LBB11_16
# BB#15:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushboolean
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_setfield
.LBB11_16:
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	ll_require, .Lfunc_end11-ll_require
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"_LOADLIB"
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"__gc"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"package"
	.size	.L.str.2, 8

	.type	pk_funcs,@object        # @pk_funcs
	.section	.rodata,"a",@progbits
	.p2align	4
pk_funcs:
	.quad	.L.str.15
	.quad	ll_loadlib
	.quad	.L.str.16
	.quad	ll_seeall
	.zero	16
	.size	pk_funcs, 48

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"loaders"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"path"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"LUA_PATH"
	.size	.L.str.5, 9

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"./?.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/share/lua/5.1/?/init.lua;/usr/local/lib/lua/5.1/?.lua;/usr/local/lib/lua/5.1/?/init.lua"
	.size	.L.str.6, 138

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"cpath"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"LUA_CPATH"
	.size	.L.str.8, 10

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"./?.so;/usr/local/lib/lua/5.1/?.so;/usr/local/lib/lua/5.1/loadall.so"
	.size	.L.str.9, 69

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"/\n;\n?\n!\n-"
	.size	.L.str.10, 10

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"config"
	.size	.L.str.11, 7

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"_LOADED"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"loaded"
	.size	.L.str.13, 7

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"preload"
	.size	.L.str.14, 8

	.type	ll_funcs,@object        # @ll_funcs
	.section	.rodata,"a",@progbits
	.p2align	4
ll_funcs:
	.quad	.L.str.41
	.quad	ll_module
	.quad	.L.str.42
	.quad	ll_require
	.zero	16
	.size	ll_funcs, 48

	.type	.L.str.15,@object       # @.str.15
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.15:
	.asciz	"loadlib"
	.size	.L.str.15, 8

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"seeall"
	.size	.L.str.16, 7

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"absent"
	.size	.L.str.17, 7

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"init"
	.size	.L.str.18, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%s%s"
	.size	.L.str.19, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"LOADLIB: "
	.size	.L.str.20, 10

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"dynamic libraries not enabled; check your Lua installation"
	.size	.L.str.21, 59

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"__index"
	.size	.L.str.22, 8

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"'package.preload' must be a table"
	.size	.L.str.23, 34

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"\n\tno field package.preload['%s']"
	.size	.L.str.24, 33

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"."
	.size	.L.str.25, 2

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"/"
	.size	.L.str.26, 2

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"'package.%s' must be a string"
	.size	.L.str.27, 30

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.zero	1
	.size	.L.str.28, 1

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"?"
	.size	.L.str.29, 2

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"\n\tno file '%s'"
	.size	.L.str.30, 15

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"r"
	.size	.L.str.32, 2

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"error loading module '%s' from file '%s':\n\t%s"
	.size	.L.str.33, 46

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"_"
	.size	.L.str.35, 2

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"luaopen_%s"
	.size	.L.str.36, 11

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"\n\tno module '%s' in file '%s'"
	.size	.L.str.37, 30

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	";;"
	.size	.L.str.38, 3

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	";\001;"
	.size	.L.str.39, 4

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"\001"
	.size	.L.str.40, 2

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"module"
	.size	.L.str.41, 7

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"require"
	.size	.L.str.42, 8

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"name conflict for module '%s'"
	.size	.L.str.43, 30

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"_NAME"
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"_M"
	.size	.L.str.45, 3

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"_PACKAGE"
	.size	.L.str.46, 9

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"f"
	.size	.L.str.47, 2

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"'module' not called from a Lua function"
	.size	.L.str.48, 40

	.type	sentinel_,@object       # @sentinel_
	.section	.rodata,"a",@progbits
	.p2align	2
sentinel_:
	.long	0                       # 0x0
	.size	sentinel_, 4

	.type	.L.str.49,@object       # @.str.49
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.49:
	.asciz	"loop or previous error loading module '%s'"
	.size	.L.str.49, 43

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"'package.loaders' must be a table"
	.size	.L.str.50, 34

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"module '%s' not found:%s"
	.size	.L.str.51, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
