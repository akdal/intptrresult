	.text
	.file	"mb_access.bc"
	.globl	mb_is_available
	.p2align	4, 0x90
	.type	mb_is_available,@function
mb_is_available:                        # @mb_is_available
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testl	%edi, %edi
	js	.LBB0_5
# BB#1:
	movq	dec_picture(%rip), %rcx
	cmpl	%edi, 316912(%rcx)
	jle	.LBB0_5
# BB#2:
	movq	img(%rip), %rcx
	cmpl	$0, 5628(%rcx)
	jne	.LBB0_4
# BB#3:
	movq	5600(%rcx), %rcx
	movslq	%edi, %rdx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movl	12(%rcx,%rdx), %edx
	movslq	%esi, %rsi
	imulq	$408, %rsi, %rsi        # imm = 0x198
	cmpl	12(%rcx,%rsi), %edx
	jne	.LBB0_5
.LBB0_4:
	movl	$1, %eax
.LBB0_5:
	retq
.Lfunc_end0:
	.size	mb_is_available, .Lfunc_end0-mb_is_available
	.cfi_endproc

	.globl	CheckAvailabilityOfNeighbors
	.p2align	4, 0x90
	.type	CheckAvailabilityOfNeighbors,@function
CheckAvailabilityOfNeighbors:           # @CheckAvailabilityOfNeighbors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	img(%rip), %r8
	movq	5600(%r8), %rax
	movslq	4(%r8), %rdi
	imulq	$408, %rdi, %rcx        # imm = 0x198
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rax,%rcx)
	movq	dec_picture(%rip), %r14
	cmpl	$0, 316904(%r14)
	je	.LBB1_6
# BB#1:
	movl	%edi, %r10d
	sarl	%r10d
	andl	$-2, %edi
	leal	-2(%rdi), %edx
	movl	%edx, 364(%rax,%rcx)
	movl	%r10d, %ebp
	subl	316908(%r14), %ebp
	leal	(%rbp,%rbp), %ebx
	xorl	%r11d, %r11d
	addl	$-2, %edi
	movl	%ebx, 368(%rax,%rcx)
	leal	2(%rbp,%rbp), %edx
	movl	%edx, 372(%rax,%rcx)
	leal	-2(%rbp,%rbp), %r9d
	movl	%r9d, 376(%rax,%rcx)
	js	.LBB1_12
# BB#2:
	cmpl	%edi, 316912(%r14)
	jle	.LBB1_12
# BB#3:
	cmpl	$0, 5628(%r8)
	jne	.LBB1_5
# BB#4:
	movslq	%edi, %rdi
	imulq	$408, %rdi, %rdi        # imm = 0x198
	movl	12(%rax,%rdi), %edi
	cmpl	12(%rax,%rcx), %edi
	jne	.LBB1_12
.LBB1_5:                                # %mb_is_available.exit
	movq	PicPos(%rip), %rdi
	movslq	%r10d, %rbp
	movq	(%rdi,%rbp,8), %rdi
	cmpl	$0, (%rdi)
	setne	%dil
	jmp	.LBB1_13
.LBB1_12:
	xorl	%edi, %edi
.LBB1_13:                               # %mb_is_available.exit.thread
	movzbl	%dil, %edi
	movl	%edi, 380(%rax,%rcx)
	testl	%ebx, %ebx
	js	.LBB1_18
# BB#14:
	cmpl	%ebx, 316912(%r14)
	jle	.LBB1_18
# BB#15:
	cmpl	$0, 5628(%r8)
	jne	.LBB1_17
# BB#16:
	movslq	%ebx, %rdi
	imulq	$408, %rdi, %rdi        # imm = 0x198
	movl	12(%rax,%rdi), %edi
	cmpl	12(%rax,%rcx), %edi
	jne	.LBB1_18
.LBB1_17:
	movl	$1, %r11d
.LBB1_18:                               # %mb_is_available.exit69
	movl	%r11d, 384(%rax,%rcx)
	testl	%edx, %edx
	js	.LBB1_24
# BB#19:
	cmpl	%edx, 316912(%r14)
	jle	.LBB1_24
# BB#20:
	cmpl	$0, 5628(%r8)
	jne	.LBB1_22
# BB#21:
	movslq	%edx, %rdx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movl	12(%rax,%rdx), %edx
	cmpl	12(%rax,%rcx), %edx
	jne	.LBB1_24
.LBB1_22:                               # %mb_is_available.exit67
	movq	PicPos(%rip), %rdx
	movslq	%r10d, %rdi
	movq	8(%rdx,%rdi,8), %rdx
	cmpl	$0, (%rdx)
	setne	%dl
	jmp	.LBB1_25
.LBB1_24:
	xorl	%edx, %edx
.LBB1_25:                               # %mb_is_available.exit67.thread
	movzbl	%dl, %edx
	movl	%edx, 388(%rax,%rcx)
	testl	%r9d, %r9d
	js	.LBB1_59
# BB#26:
	cmpl	%r9d, 316912(%r14)
	jle	.LBB1_59
# BB#27:
	cmpl	$0, 5628(%r8)
	jne	.LBB1_29
# BB#28:
	movslq	%r9d, %rdx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movl	12(%rax,%rdx), %edx
	cmpl	12(%rax,%rcx), %edx
	jne	.LBB1_59
.LBB1_29:                               # %mb_is_available.exit65
	movq	PicPos(%rip), %rdx
	movslq	%r10d, %rsi
	movq	(%rdx,%rsi,8), %rdx
	jmp	.LBB1_51
.LBB1_6:
	leal	-1(%rdi), %ebp
	movl	%ebp, 364(%rax,%rcx)
	movl	316908(%r14), %esi
	movl	%edi, %ebx
	subl	%esi, %ebx
	movl	%ebx, 368(%rax,%rcx)
	leal	1(%rdi), %r9d
	movl	%r9d, %edx
	subl	%esi, %edx
	movl	%edx, 372(%rax,%rcx)
	movl	%ebp, %r10d
	subl	%esi, %r10d
	movl	%r10d, 376(%rax,%rcx)
	xorl	%r11d, %r11d
	testl	%ebp, %ebp
	js	.LBB1_33
# BB#7:
	cmpl	%ebp, 316912(%r14)
	jle	.LBB1_33
# BB#8:
	cmpl	$0, 5628(%r8)
	jne	.LBB1_10
# BB#9:
	movslq	%ebp, %rsi
	imulq	$408, %rsi, %rsi        # imm = 0x198
	movl	12(%rax,%rsi), %esi
	cmpl	12(%rax,%rcx), %esi
	jne	.LBB1_33
.LBB1_10:                               # %mb_is_available.exit63
	movq	PicPos(%rip), %rsi
	movq	(%rsi,%rdi,8), %rsi
	cmpl	$0, (%rsi)
	setne	%bpl
	jmp	.LBB1_34
.LBB1_33:
	xorl	%ebp, %ebp
.LBB1_34:                               # %mb_is_available.exit63.thread
	movzbl	%bpl, %esi
	movl	%esi, 380(%rax,%rcx)
	testl	%ebx, %ebx
	js	.LBB1_39
# BB#35:
	cmpl	%ebx, 316912(%r14)
	jle	.LBB1_39
# BB#36:
	cmpl	$0, 5628(%r8)
	jne	.LBB1_38
# BB#37:
	movslq	%ebx, %rsi
	imulq	$408, %rsi, %rsi        # imm = 0x198
	movl	12(%rax,%rsi), %esi
	cmpl	12(%rax,%rcx), %esi
	jne	.LBB1_39
.LBB1_38:
	movl	$1, %r11d
.LBB1_39:                               # %mb_is_available.exit61
	movl	%r11d, 384(%rax,%rcx)
	testl	%edx, %edx
	js	.LBB1_45
# BB#40:
	cmpl	%edx, 316912(%r14)
	jle	.LBB1_45
# BB#41:
	cmpl	$0, 5628(%r8)
	jne	.LBB1_43
# BB#42:
	movslq	%edx, %rdx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movl	12(%rax,%rdx), %edx
	cmpl	12(%rax,%rcx), %edx
	jne	.LBB1_45
.LBB1_43:                               # %mb_is_available.exit59
	movq	PicPos(%rip), %rdx
	movslq	%r9d, %rsi
	movq	(%rdx,%rsi,8), %rdx
	cmpl	$0, (%rdx)
	setne	%dl
	jmp	.LBB1_46
.LBB1_45:
	xorl	%edx, %edx
.LBB1_46:                               # %mb_is_available.exit59.thread
	movzbl	%dl, %edx
	movl	%edx, 388(%rax,%rcx)
	testl	%r10d, %r10d
	js	.LBB1_59
# BB#47:
	cmpl	%r10d, 316912(%r14)
	jle	.LBB1_59
# BB#48:
	cmpl	$0, 5628(%r8)
	jne	.LBB1_50
# BB#49:
	movslq	%r10d, %rdx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movl	12(%rax,%rdx), %edx
	cmpl	12(%rax,%rcx), %edx
	jne	.LBB1_59
.LBB1_50:                               # %mb_is_available.exit57
	movq	PicPos(%rip), %rdx
	movq	(%rdx,%rdi,8), %rdx
.LBB1_51:                               # %mb_is_available.exit65.thread
	cmpl	$0, (%rdx)
	setne	%dl
	jmp	.LBB1_60
.LBB1_59:
	xorl	%edx, %edx
.LBB1_60:                               # %mb_is_available.exit65.thread
	movzbl	%dl, %edx
	movl	%edx, 392(%rax,%rcx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	CheckAvailabilityOfNeighbors, .Lfunc_end1-CheckAvailabilityOfNeighbors
	.cfi_endproc

	.globl	get_mb_block_pos_normal
	.p2align	4, 0x90
	.type	get_mb_block_pos_normal,@function
get_mb_block_pos_normal:                # @get_mb_block_pos_normal
	.cfi_startproc
# BB#0:
	movq	PicPos(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %ecx
	movl	%ecx, (%rsi)
	movl	4(%rax), %eax
	movl	%eax, (%rdx)
	retq
.Lfunc_end2:
	.size	get_mb_block_pos_normal, .Lfunc_end2-get_mb_block_pos_normal
	.cfi_endproc

	.globl	get_mb_block_pos_mbaff
	.p2align	4, 0x90
	.type	get_mb_block_pos_mbaff,@function
get_mb_block_pos_mbaff:                 # @get_mb_block_pos_mbaff
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	PicPos(%rip), %rax
	movl	%edi, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %ecx
	movl	%ecx, (%rsi)
	movl	4(%rax), %eax
	andl	$1, %edi
	leal	(%rdi,%rax,2), %eax
	movl	%eax, (%rdx)
	retq
.Lfunc_end3:
	.size	get_mb_block_pos_mbaff, .Lfunc_end3-get_mb_block_pos_mbaff
	.cfi_endproc

	.globl	get_mb_pos
	.p2align	4, 0x90
	.type	get_mb_pos,@function
get_mb_pos:                             # @get_mb_pos
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	callq	*get_mb_block_pos(%rip)
	movq	img(%rip), %rax
	movslq	%r14d, %rcx
	movl	(%rbp), %edx
	imull	5948(%rax,%rcx,8), %edx
	movl	%edx, (%rbp)
	movl	(%rbx), %edx
	imull	5952(%rax,%rcx,8), %edx
	movl	%edx, (%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	get_mb_pos, .Lfunc_end4-get_mb_pos
	.cfi_endproc

	.globl	getNonAffNeighbour
	.p2align	4, 0x90
	.type	getNonAffNeighbour,@function
getNonAffNeighbour:                     # @getNonAffNeighbour
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	img(%rip), %r9
	movq	5600(%r9), %r10
	movl	%edi, %r11d
	movslq	%ecx, %rax
	movl	5948(%r9,%rax,8), %ecx
	movl	5952(%r9,%rax,8), %ebx
	testl	%esi, %esi
	js	.LBB5_1
# BB#5:
	cmpl	%esi, %ecx
	jle	.LBB5_10
# BB#6:
	testl	%edx, %edx
	js	.LBB5_7
# BB#8:
	cmpl	%edx, %ebx
	jle	.LBB5_17
# BB#9:                                 # %.thread72
	movl	%edi, 4(%r8)
	movl	$1, (%r8)
	jmp	.LBB5_15
.LBB5_1:
	testl	%edx, %edx
	js	.LBB5_2
# BB#3:
	cmpl	%edx, %ebx
	jle	.LBB5_17
# BB#4:
	imulq	$408, %r11, %r11        # imm = 0x198
	movl	364(%r10,%r11), %edi
	movl	%edi, 4(%r8)
	leaq	380(%r10,%r11), %r10
	jmp	.LBB5_12
.LBB5_10:
	testl	%edx, %edx
	js	.LBB5_11
.LBB5_17:                               # %.thread
	movl	$0, (%r8)
	cmpl	$0, 5628(%r9)
	jne	.LBB5_14
	jmp	.LBB5_16
.LBB5_2:
	imulq	$408, %r11, %r11        # imm = 0x198
	movl	376(%r10,%r11), %edi
	movl	%edi, 4(%r8)
	leaq	392(%r10,%r11), %r10
	jmp	.LBB5_12
.LBB5_7:
	imulq	$408, %r11, %r11        # imm = 0x198
	movl	368(%r10,%r11), %edi
	movl	%edi, 4(%r8)
	leaq	384(%r10,%r11), %r10
	jmp	.LBB5_12
.LBB5_11:
	imulq	$408, %r11, %r11        # imm = 0x198
	movl	372(%r10,%r11), %edi
	movl	%edi, 4(%r8)
	leaq	388(%r10,%r11), %r10
.LBB5_12:
	movl	(%r10), %eax
	movl	%eax, (%r8)
	testl	%eax, %eax
	jne	.LBB5_15
# BB#13:
	cmpl	$0, 5628(%r9)
	je	.LBB5_16
.LBB5_14:                               # %._crit_edge
	movl	4(%r8), %edi
.LBB5_15:                               # %._crit_edge73
	movq	PicPos(%rip), %rax
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rax
	leal	-1(%rcx), %edi
	andl	%esi, %edi
	movl	%edi, 8(%r8)
	leal	-1(%rbx), %esi
	andl	%edx, %esi
	movl	%esi, 12(%r8)
	imull	(%rax), %ecx
	addl	%edi, %ecx
	movl	%ecx, 16(%r8)
	imull	4(%rax), %ebx
	addl	%esi, %ebx
	movl	%ebx, 20(%r8)
.LBB5_16:
	popq	%rbx
	retq
.Lfunc_end5:
	.size	getNonAffNeighbour, .Lfunc_end5-getNonAffNeighbour
	.cfi_endproc

	.globl	getAffNeighbour
	.p2align	4, 0x90
	.type	getAffNeighbour,@function
getAffNeighbour:                        # @getAffNeighbour
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	img(%rip), %rax
	movq	5600(%rax), %r9
	movslq	%ecx, %r14
	movl	5948(%rax,%r14,8), %r8d
	movl	5952(%rax,%r14,8), %ebp
	movl	$0, (%rbx)
	cmpl	%edx, %ebp
	jle	.LBB6_77
# BB#1:
	testl	%edx, %edx
	js	.LBB6_3
# BB#2:
	cmpl	%esi, %r8d
	jle	.LBB6_77
.LBB6_3:
	movl	%edi, %ecx
	testl	%esi, %esi
	js	.LBB6_9
# BB#4:
	cmpl	%esi, %r8d
	jle	.LBB6_16
# BB#5:
	testl	%edx, %edx
	js	.LBB6_27
# BB#6:
	jne	.LBB6_66
# BB#7:
	cmpl	$2, 5628(%rax)
	jne	.LBB6_66
# BB#8:
	imulq	$408, %rcx, %rax        # imm = 0x198
	movl	368(%r9,%rax), %eax
	incl	%eax
	movl	%eax, 4(%rbx)
	movl	$1, (%rbx)
	movl	$-1, %edx
	jmp	.LBB6_76
.LBB6_9:
	imulq	$408, %rcx, %r11        # imm = 0x198
	movl	356(%r9,%r11), %ecx
	andl	$1, %edi
	testl	%edx, %edx
	js	.LBB6_22
# BB#10:
	movslq	364(%r9,%r11), %r10
	movl	%r10d, 4(%rbx)
	movl	380(%r9,%r11), %r11d
	movl	%r11d, (%rbx)
	testl	%ecx, %ecx
	je	.LBB6_32
# BB#11:
	testl	%edi, %edi
	jne	.LBB6_37
# BB#12:
	testl	%r11d, %r11d
	je	.LBB6_74
# BB#13:
	imulq	$408, %r10, %rax        # imm = 0x198
	cmpl	$0, 356(%r9,%rax)
	jne	.LBB6_76
# BB#14:
	movl	%ebp, %eax
	sarl	%eax
	cmpl	%edx, %eax
	jle	.LBB6_78
# BB#15:
	addl	%edx, %edx
	jmp	.LBB6_76
.LBB6_16:
	movl	$-1, %r10d
	testl	%edx, %edx
	jns	.LBB6_75
# BB#17:
	imulq	$408, %rcx, %rcx        # imm = 0x198
	andl	$1, %edi
	cmpl	$0, 356(%r9,%rcx)
	je	.LBB6_43
# BB#18:
	movl	372(%r9,%rcx), %r11d
	testl	%edi, %edi
	jne	.LBB6_54
# BB#19:
	movl	%r11d, 4(%rbx)
	movl	388(%r9,%rcx), %ecx
	movl	%ecx, (%rbx)
	testl	%ecx, %ecx
	je	.LBB6_75
# BB#20:
	movslq	%r11d, %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%r9,%rax)
	jne	.LBB6_76
# BB#21:
	incl	%r11d
	movl	%r11d, 4(%rbx)
	addl	%edx, %edx
	jmp	.LBB6_76
.LBB6_22:
	testl	%ecx, %ecx
	je	.LBB6_40
# BB#23:
	movl	376(%r9,%r11), %ecx
	testl	%edi, %edi
	jne	.LBB6_42
# BB#24:
	movl	%ecx, 4(%rbx)
	movl	392(%r9,%r11), %edi
	movl	%edi, (%rbx)
	testl	%edi, %edi
	je	.LBB6_74
# BB#25:
	movslq	%ecx, %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%r9,%rax)
	jne	.LBB6_76
# BB#26:
	incl	%ecx
	movl	%ecx, 4(%rbx)
	addl	%edx, %edx
	jmp	.LBB6_76
.LBB6_27:
	imulq	$408, %rcx, %r11        # imm = 0x198
	movl	%edi, %ecx
	andl	$1, %ecx
	cmpl	$0, 356(%r9,%r11)
	je	.LBB6_45
# BB#28:
	movl	368(%r9,%r11), %edi
	testl	%ecx, %ecx
	jne	.LBB6_56
# BB#29:
	movl	%edi, 4(%rbx)
	movl	384(%r9,%r11), %ecx
	movl	%ecx, (%rbx)
	testl	%ecx, %ecx
	je	.LBB6_74
# BB#30:
	movslq	%edi, %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%r9,%rax)
	jne	.LBB6_76
# BB#31:
	incl	%edi
	movl	%edi, 4(%rbx)
	addl	%edx, %edx
	jmp	.LBB6_76
.LBB6_32:
	testl	%edi, %edi
	jne	.LBB6_50
# BB#33:
	testl	%r11d, %r11d
	je	.LBB6_74
# BB#34:
	imulq	$408, %r10, %rax        # imm = 0x198
	cmpl	$0, 356(%r9,%rax)
	je	.LBB6_76
# BB#35:
	testb	$1, %dl
	jne	.LBB6_79
# BB#36:
	shrl	%edx
	jmp	.LBB6_76
.LBB6_37:
	testl	%r11d, %r11d
	je	.LBB6_74
# BB#38:
	imulq	$408, %r10, %rax        # imm = 0x198
	cmpl	$0, 356(%r9,%rax)
	je	.LBB6_69
# BB#39:
	leal	1(%r10), %eax
	movl	%eax, 4(%rbx)
	jmp	.LBB6_76
.LBB6_40:
	testl	%edi, %edi
	jne	.LBB6_60
# BB#41:
	movl	376(%r9,%r11), %ecx
.LBB6_42:
	incl	%ecx
	movl	%ecx, 4(%rbx)
	movl	392(%r9,%r11), %edi
	jmp	.LBB6_57
.LBB6_43:
	testl	%edi, %edi
	jne	.LBB6_64
# BB#44:
	movl	372(%r9,%rcx), %edi
	incl	%edi
	movl	%edi, 4(%rbx)
	jmp	.LBB6_55
.LBB6_45:
	testl	%ecx, %ecx
	jne	.LBB6_65
# BB#46:
	movslq	368(%r9,%r11), %r10
	movl	%r10d, 4(%rbx)
	movl	384(%r9,%r11), %edi
	testl	%edi, %edi
	je	.LBB6_57
# BB#47:
	cmpl	$1, 5628(%rax)
	jne	.LBB6_49
# BB#48:
	imulq	$408, %r10, %rcx        # imm = 0x198
	cmpl	$0, 356(%r9,%rcx)
	jne	.LBB6_57
.LBB6_49:
	incl	%r10d
	movl	%r10d, 4(%rbx)
	jmp	.LBB6_57
.LBB6_50:
	testl	%r11d, %r11d
	je	.LBB6_74
# BB#51:
	imulq	$408, %r10, %rax        # imm = 0x198
	cmpl	$0, 356(%r9,%rax)
	je	.LBB6_73
# BB#52:
	testb	$1, %dl
	je	.LBB6_63
# BB#53:
	leal	1(%r10), %eax
	movl	%eax, 4(%rbx)
	jmp	.LBB6_63
.LBB6_54:
	incl	%r11d
	movl	%r11d, 4(%rbx)
.LBB6_55:                               # %thread-pre-split
	movl	388(%r9,%rcx), %edi
	jmp	.LBB6_57
.LBB6_56:
	incl	%edi
	movl	%edi, 4(%rbx)
	movl	384(%r9,%r11), %edi
.LBB6_57:                               # %thread-pre-split
	movl	%edi, (%rbx)
	testl	%edi, %edi
	movl	%edx, %r10d
	jne	.LBB6_76
	jmp	.LBB6_75
.LBB6_60:
	movslq	364(%r9,%r11), %rcx
	movl	%ecx, 4(%rbx)
	movl	380(%r9,%r11), %edi
	movl	%edi, (%rbx)
	testl	%edi, %edi
	je	.LBB6_74
# BB#61:
	imulq	$408, %rcx, %rax        # imm = 0x198
	cmpl	$0, 356(%r9,%rax)
	je	.LBB6_76
# BB#62:
	incl	%ecx
	movl	%ecx, 4(%rbx)
.LBB6_63:
	addl	%ebp, %edx
	sarl	%edx
	jmp	.LBB6_76
.LBB6_74:
	movl	$-1, %r10d
.LBB6_75:                               # %thread-pre-split.thread176
	cmpl	$0, 5628(%rax)
	movl	%r10d, %edx
	jne	.LBB6_76
	jmp	.LBB6_77
.LBB6_64:                               # %.thread172
	movl	$0, (%rbx)
	jmp	.LBB6_75
.LBB6_65:
	decl	%edi
.LBB6_66:
	movl	%edi, 4(%rbx)
	movl	$1, (%rbx)
.LBB6_76:                               # %.thread
	decl	%ebp
	decl	%r8d
	andl	%esi, %r8d
	movl	%r8d, 8(%rbx)
	andl	%edx, %ebp
	movl	%ebp, 12(%rbx)
	movl	4(%rbx), %edi
	leaq	16(%rbx), %rsi
	leaq	20(%rbx), %rdx
	callq	*get_mb_block_pos(%rip)
	movq	img(%rip), %rax
	movl	16(%rbx), %ecx
	imull	5948(%rax,%r14,8), %ecx
	movl	%ecx, 16(%rbx)
	movl	20(%rbx), %edx
	imull	5952(%rax,%r14,8), %edx
	addl	8(%rbx), %ecx
	movl	%ecx, 16(%rbx)
	addl	12(%rbx), %edx
	movl	%edx, 20(%rbx)
.LBB6_77:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB6_69:
	movl	%ebp, %eax
	sarl	%eax
	cmpl	%edx, %eax
	jle	.LBB6_80
# BB#70:
	leal	1(%rdx,%rdx), %edx
	jmp	.LBB6_76
.LBB6_73:
	incl	%r10d
	movl	%r10d, 4(%rbx)
	jmp	.LBB6_76
.LBB6_78:
	incl	%r10d
	movl	%r10d, 4(%rbx)
	addl	%edx, %edx
	subl	%ebp, %edx
	jmp	.LBB6_76
.LBB6_79:
	incl	%r10d
	movl	%r10d, 4(%rbx)
	shrl	%edx
	jmp	.LBB6_76
.LBB6_80:
	incl	%r10d
	movl	%r10d, 4(%rbx)
	leal	1(%rdx,%rdx), %edx
	subl	%ebp, %edx
	jmp	.LBB6_76
.Lfunc_end6:
	.size	getAffNeighbour, .Lfunc_end6-getAffNeighbour
	.cfi_endproc

	.globl	getLuma4x4Neighbour
	.p2align	4, 0x90
	.type	getLuma4x4Neighbour,@function
getLuma4x4Neighbour:                    # @getLuma4x4Neighbour
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
.Lcfi21:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	callq	*getNeighbour(%rip)
	cmpl	$0, (%rbx)
	je	.LBB7_2
# BB#1:
	movdqu	8(%rbx), %xmm0
	psrad	$2, %xmm0
	movdqu	%xmm0, 8(%rbx)
.LBB7_2:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	getLuma4x4Neighbour, .Lfunc_end7-getLuma4x4Neighbour
	.cfi_endproc

	.globl	getChroma4x4Neighbour
	.p2align	4, 0x90
	.type	getChroma4x4Neighbour,@function
getChroma4x4Neighbour:                  # @getChroma4x4Neighbour
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	movl	$1, %ecx
	movq	%rbx, %r8
	callq	*getNeighbour(%rip)
	cmpl	$0, (%rbx)
	je	.LBB8_2
# BB#1:
	movdqu	8(%rbx), %xmm0
	psrad	$2, %xmm0
	movdqu	%xmm0, 8(%rbx)
.LBB8_2:
	popq	%rbx
	retq
.Lfunc_end8:
	.size	getChroma4x4Neighbour, .Lfunc_end8-getChroma4x4Neighbour
	.cfi_endproc

	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
