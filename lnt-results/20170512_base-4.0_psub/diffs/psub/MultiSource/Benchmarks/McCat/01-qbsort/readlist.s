	.text
	.file	"readlist.bc"
	.globl	ReadList
	.p2align	4, 0x90
	.type	ReadList,@function
ReadList:                               # @ReadList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movb	$45, 11(%rsp)
	leaq	11(%rsp), %rbx
	movl	$7, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	scanf
	cmpl	$-1, %eax
	je	.LBB0_22
# BB#1:                                 # %thread-pre-split
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpb	$91, 11(%rsp)
	jne	.LBB0_2
# BB#3:
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movb	$44, 11(%rsp)
	movq	$0, (%r12)
	movb	$44, %al
	leaq	12(%rsp), %r15
	leaq	11(%rsp), %rbx
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	cmpb	$44, %al
	je	.LBB0_9
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_9 Depth=1
	incl	%r14d
	incq	%r13
	cmpb	$44, %al
	jne	.LBB0_5
.LBB0_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	scanf
	cmpl	$-1, %eax
	je	.LBB0_22
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	movl	$16, %edi
	callq	malloc
	movl	12(%rsp), %ecx
	movl	%ecx, (%rax)
	movq	(%r12), %rcx
	movq	%rcx, 8(%rax)
	movq	%rax, (%r12)
	movb	$32, 11(%rsp)
	movb	$32, %al
	cmpb	$32, %al
	jne	.LBB0_13
	jmp	.LBB0_14
	.p2align	4, 0x90
.LBB0_11:                               # %thread-pre-split33
                                        #   in Loop: Header=BB0_14 Depth=2
	movzbl	11(%rsp), %eax
	cmpb	$32, %al
	je	.LBB0_14
.LBB0_13:                               #   in Loop: Header=BB0_9 Depth=1
	cmpb	$10, %al
	jne	.LBB0_15
.LBB0_14:                               # %.critedge
                                        #   Parent Loop BB0_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	scanf
	cmpl	$-1, %eax
	jne	.LBB0_11
.LBB0_22:                               # %.loopexit
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:
	movl	$42, %ebp
	cmpb	$93, %al
	jne	.LBB0_22
# BB#6:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbp
	movl	%r14d, (%rbp)
	movslq	%r14d, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, 8(%rbp)
	testl	%r14d, %r14d
	jle	.LBB0_21
# BB#7:                                 # %.lr.ph.preheader
	movl	%r14d, %edx
	leaq	-1(%rdx), %rcx
	testb	$3, %dl
	je	.LBB0_8
# BB#16:                                # %.lr.ph.prol.preheader
	andb	$3, %r14b
	movzbl	%r14b, %esi
	xorl	%edx, %edx
.LBB0_17:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %r12
	movl	(%r12), %edi
	movl	%edi, (%rax,%rdx,4)
	addq	$8, %r12
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB0_17
	jmp	.LBB0_18
.LBB0_8:
	xorl	%edx, %edx
.LBB0_18:                               # %.lr.ph.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB0_21
# BB#19:                                # %.lr.ph.preheader.new
	subq	%rdx, %r13
	leaq	12(%rax,%rdx,4), %rax
.LBB0_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	movl	(%rcx), %edx
	movl	%edx, -12(%rax)
	movq	8(%rcx), %rcx
	movl	(%rcx), %edx
	movl	%edx, -8(%rax)
	movq	8(%rcx), %rcx
	movl	(%rcx), %edx
	movl	%edx, -4(%rax)
	movq	8(%rcx), %r12
	movl	(%r12), %ecx
	movl	%ecx, (%rax)
	addq	$8, %r12
	addq	$16, %rax
	addq	$-4, %r13
	jne	.LBB0_20
.LBB0_21:                               # %._crit_edge
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	xorl	%ebp, %ebp
	jmp	.LBB0_22
.Lfunc_end0:
	.size	ReadList, .Lfunc_end0-ReadList
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%c"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d"
	.size	.L.str.1, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
