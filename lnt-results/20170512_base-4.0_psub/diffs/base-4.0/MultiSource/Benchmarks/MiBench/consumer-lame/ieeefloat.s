	.text
	.file	"ieeefloat.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	9218868437227405312     # double +Inf
	.text
	.globl	ConvertFromIeeeSingle
	.p2align	4, 0x90
	.type	ConvertFromIeeeSingle,@function
ConvertFromIeeeSingle:                  # @ConvertFromIeeeSingle
	.cfi_startproc
# BB#0:
	movzbl	(%rdi), %eax
	shlq	$24, %rax
	movzbl	1(%rdi), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	2(%rdi), %edx
	shlq	$8, %rdx
	orq	%rcx, %rdx
	movzbl	3(%rdi), %eax
	orq	%rdx, %rax
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	je	.LBB0_1
# BB#3:
	shrl	$23, %ecx
	movzbl	%cl, %edi
	cmpl	$255, %edi
	jne	.LBB0_5
# BB#4:
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	retq
.LBB0_1:
	xorps	%xmm0, %xmm0
	retq
.LBB0_5:
	andl	$8388607, %eax          # imm = 0x7FFFFF
	testq	%rdi, %rdi
	je	.LBB0_7
# BB#6:
	orl	$8388608, %eax          # imm = 0x800000
	cvtsi2sdl	%eax, %xmm0
	addl	$-150, %edi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	jmp	ldexp                   # TAILCALL
.LBB0_7:
	cvtsi2sdl	%eax, %xmm0
	movl	$-149, %edi
	jmp	ldexp                   # TAILCALL
.Lfunc_end0:
	.size	ConvertFromIeeeSingle, .Lfunc_end0-ConvertFromIeeeSingle
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_1:
	.quad	4607182418800017408     # double 1
.LCPI1_2:
	.quad	4715268809856909312     # double 16777216
	.text
	.globl	ConvertToIeeeSingle
	.p2align	4, 0x90
	.type	ConvertToIeeeSingle,@function
ConvertToIeeeSingle:                    # @ConvertToIeeeSingle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	xorpd	%xmm1, %xmm1
	xorl	%ebx, %ebx
	ucomisd	%xmm0, %xmm1
	seta	%al
	jbe	.LBB1_2
# BB#1:
	xorpd	.LCPI1_0(%rip), %xmm0
.LBB1_2:
	ucomisd	%xmm1, %xmm0
	jne	.LBB1_4
	jp	.LBB1_4
# BB#3:
	xorl	%ebx, %ebx
	jmp	.LBB1_11
.LBB1_4:
	movb	%al, %bl
	shlq	$63, %rbx
	leaq	12(%rsp), %rdi
	callq	frexp
	movsd	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB1_6
# BB#5:
	movl	12(%rsp), %ecx
	cmpl	$130, %ecx
	jge	.LBB1_6
# BB#7:
	cmpl	$-126, %ecx
	jg	.LBB1_10
# BB#8:
	cmpl	$-149, %ecx
	jl	.LBB1_11
# BB#9:
	addl	$149, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rax
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %rax
	orq	%rax, %rbx
	jmp	.LBB1_11
.LBB1_6:
	orq	$2139095040, %rbx       # imm = 0x7F800000
	jmp	.LBB1_11
.LBB1_10:
	movslq	%ecx, %r14
	mulsd	.LCPI1_2(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %rax
	addq	$-8388608, %rax         # imm = 0xFF800000
	shlq	$23, %r14
	addq	$1056964608, %r14       # imm = 0x3F000000
	orq	%rbx, %r14
	orq	%rax, %r14
	movq	%r14, %rbx
.LBB1_11:
	movq	%rbx, %rax
	shrq	$24, %rax
	movb	%al, (%rbp)
	movq	%rbx, %rax
	shrq	$16, %rax
	movb	%al, 1(%rbp)
	movb	%bh, 2(%rbp)  # NOREX
	movb	%bl, 3(%rbp)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	ConvertToIeeeSingle, .Lfunc_end1-ConvertToIeeeSingle
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	9218868437227405312     # double +Inf
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_1:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	ConvertFromIeeeDouble
	.p2align	4, 0x90
	.type	ConvertFromIeeeDouble,@function
ConvertFromIeeeDouble:                  # @ConvertFromIeeeDouble
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movzbl	(%rdi), %r14d
	shlq	$24, %r14
	movzbl	1(%rdi), %ebx
	shlq	$16, %rbx
	orq	%r14, %rbx
	movzbl	2(%rdi), %ecx
	shlq	$8, %rcx
	orq	%rbx, %rcx
	movzbl	3(%rdi), %eax
	orq	%rcx, %rax
	movzbl	4(%rdi), %ecx
	shlq	$24, %rcx
	movzbl	5(%rdi), %edx
	shlq	$16, %rdx
	orq	%rcx, %rdx
	movzbl	6(%rdi), %ecx
	shlq	$8, %rcx
	orq	%rdx, %rcx
	movzbl	7(%rdi), %r15d
	orq	%rcx, %r15
	movq	%r15, %rcx
	orq	%rax, %rcx
	je	.LBB2_1
# BB#2:
	shrl	$20, %ebx
	andl	$2047, %ebx             # imm = 0x7FF
	cmpl	$2047, %ebx             # imm = 0x7FF
	jne	.LBB2_4
# BB#3:
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	testl	%r14d, %r14d
	js	.LBB2_9
	jmp	.LBB2_10
.LBB2_1:
	xorpd	%xmm0, %xmm0
	testl	%r14d, %r14d
	js	.LBB2_9
	jmp	.LBB2_10
.LBB2_4:
	andl	$1048575, %eax          # imm = 0xFFFFF
	testq	%rbx, %rbx
	je	.LBB2_5
# BB#6:
	orl	$1048576, %eax          # imm = 0x100000
	cvtsi2sdl	%eax, %xmm0
	addl	$-1075, %ebx            # imm = 0xFBCD
	leal	32(%rbx), %edi
	callq	ldexp
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%r15, %xmm0
	movl	%ebx, %edi
	jmp	.LBB2_7
.LBB2_5:
	cvtsi2sdl	%eax, %xmm0
	movl	$-1042, %edi            # imm = 0xFBEE
	callq	ldexp
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%r15, %xmm0
	movl	$-1074, %edi            # imm = 0xFBCE
.LBB2_7:
	callq	ldexp
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	testl	%r14d, %r14d
	jns	.LBB2_10
.LBB2_9:
	xorpd	.LCPI2_1(%rip), %xmm0
.LBB2_10:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	ConvertFromIeeeDouble, .Lfunc_end2-ConvertFromIeeeDouble
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_1:
	.quad	4607182418800017408     # double 1
.LCPI3_2:
	.quad	-4526117625507348480    # double -1048576
.LCPI3_3:
	.quad	-4476578029606273024    # double -2147483648
	.text
	.globl	ConvertToIeeeDouble
	.p2align	4, 0x90
	.type	ConvertToIeeeDouble,@function
ConvertToIeeeDouble:                    # @ConvertToIeeeDouble
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorpd	%xmm1, %xmm1
	xorl	%r15d, %r15d
	ucomisd	%xmm0, %xmm1
	seta	%al
	jbe	.LBB3_2
# BB#1:
	xorpd	.LCPI3_0(%rip), %xmm0
.LBB3_2:
	ucomisd	%xmm1, %xmm0
	jne	.LBB3_4
	jp	.LBB3_4
# BB#3:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.LBB3_17
.LBB3_4:
	movb	%al, %r15b
	shlq	$63, %r15
	leaq	12(%rsp), %rdi
	callq	frexp
	movsd	.LCPI3_1(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB3_6
# BB#5:
	movl	12(%rsp), %edi
	cmpl	$1026, %edi             # imm = 0x402
	jge	.LBB3_6
# BB#7:
	cmpl	$-1022, %edi            # imm = 0xFC02
	jg	.LBB3_13
# BB#8:
	cmpl	$-1043, %edi            # imm = 0xFBED
	jg	.LBB3_12
# BB#9:
	cmpl	$-1074, %edi            # imm = 0xFBCE
	jge	.LBB3_11
# BB#10:
	xorl	%eax, %eax
	jmp	.LBB3_16
.LBB3_6:
	movq	%r15, %rdx
	orq	$2146435072, %rdx       # imm = 0x7FF00000
	xorl	%eax, %eax
	jmp	.LBB3_17
.LBB3_13:
	movslq	%edi, %r14
	movl	$21, %edi
	callq	ldexp
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	floor
	cvttsd2si	%xmm0, %rax
	addq	$-1048576, %rax         # imm = 0xFFF00000
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI3_2(%rip), %xmm1
	shlq	$20, %r14
	addq	$1071644672, %r14       # imm = 0x3FE00000
	orq	%rax, %r15
	orq	%r14, %r15
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.LBB3_14
.LBB3_12:
	addl	$1042, %edi             # imm = 0x412
	callq	ldexp
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	floor
	cvttsd2si	%xmm0, %rax
	orq	%rax, %r15
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
.LBB3_14:                               # %.sink.split64
	movl	$32, %edi
.LBB3_15:                               # %.sink.split64
	callq	ldexp
	callq	floor
	addsd	.LCPI3_3(%rip), %xmm0
	cvttsd2si	%xmm0, %rax
	subq	$-2147483648, %rax      # imm = 0x80000000
.LBB3_16:
	movq	%r15, %rdx
.LBB3_17:
	movq	%rdx, %rcx
	shrq	$24, %rcx
	movb	%cl, (%rbx)
	movq	%rdx, %rcx
	shrq	$16, %rcx
	movb	%cl, 1(%rbx)
	movb	%dh, 2(%rbx)  # NOREX
	movb	%dl, 3(%rbx)
	movq	%rax, %rcx
	shrq	$24, %rcx
	movb	%cl, 4(%rbx)
	movq	%rax, %rcx
	shrq	$16, %rcx
	movb	%cl, 5(%rbx)
	movb	%ah, 6(%rbx)  # NOREX
	movb	%al, 7(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB3_11:
	addl	$1074, %edi             # imm = 0x432
	jmp	.LBB3_15
.Lfunc_end3:
	.size	ConvertToIeeeDouble, .Lfunc_end3-ConvertToIeeeDouble
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	9218868437227405312     # double +Inf
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_1:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	ConvertFromIeeeExtended
	.p2align	4, 0x90
	.type	ConvertFromIeeeExtended,@function
ConvertFromIeeeExtended:                # @ConvertFromIeeeExtended
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 48
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movzbl	(%rbx), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	shlq	$8, %rcx
	movzbl	1(%rbx), %r14d
	orq	%rcx, %r14
	movzbl	2(%rbx), %ecx
	shlq	$24, %rcx
	movzbl	3(%rbx), %edx
	shlq	$16, %rdx
	orq	%rcx, %rdx
	movzbl	4(%rbx), %esi
	shlq	$8, %rsi
	orq	%rdx, %rsi
	movzbl	5(%rbx), %ecx
	orq	%rsi, %rcx
	movzbl	6(%rbx), %edx
	shlq	$24, %rdx
	movzbl	7(%rbx), %esi
	shlq	$16, %rsi
	orq	%rdx, %rsi
	movzbl	8(%rbx), %edx
	shlq	$8, %rdx
	orq	%rsi, %rdx
	movzbl	9(%rbx), %r15d
	orq	%rdx, %r15
	movq	%rcx, %rdx
	orq	%r14, %rdx
	orq	%r15, %rdx
	je	.LBB4_1
# BB#2:
	cmpq	$32767, %r14            # imm = 0x7FFF
	jne	.LBB4_4
# BB#3:
	movsd	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero
	testb	%al, %al
	js	.LBB4_6
	jmp	.LBB4_7
.LBB4_1:
	xorpd	%xmm0, %xmm0
	testb	%al, %al
	js	.LBB4_6
	jmp	.LBB4_7
.LBB4_4:
	cvtsi2sdq	%rcx, %xmm0
	addl	$-16446, %r14d          # imm = 0xBFC2
	leal	32(%r14), %edi
	callq	ldexp
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%r15, %xmm0
	movl	%r14d, %edi
	callq	ldexp
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movb	(%rbx), %al
	testb	%al, %al
	jns	.LBB4_7
.LBB4_6:
	xorpd	.LCPI4_1(%rip), %xmm0
.LBB4_7:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	ConvertFromIeeeExtended, .Lfunc_end4-ConvertFromIeeeExtended
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_1:
	.quad	4607182418800017408     # double 1
.LCPI5_2:
	.quad	-4476578029606273024    # double -2147483648
	.text
	.globl	ConvertToIeeeExtended
	.p2align	4, 0x90
	.type	ConvertToIeeeExtended,@function
ConvertToIeeeExtended:                  # @ConvertToIeeeExtended
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	xorpd	%xmm1, %xmm1
	xorl	%ebx, %ebx
	ucomisd	%xmm0, %xmm1
	seta	%al
	jbe	.LBB5_2
# BB#1:
	xorpd	.LCPI5_0(%rip), %xmm0
.LBB5_2:
	ucomisd	%xmm1, %xmm0
	jne	.LBB5_5
	jp	.LBB5_5
# BB#3:
	movl	$0, 12(%rsp)
	xorl	%ecx, %ecx
	jmp	.LBB5_4
.LBB5_5:
	movb	%al, %bl
	shll	$15, %ebx
	leaq	12(%rsp), %rdi
	callq	frexp
	movsd	.LCPI5_1(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_7
# BB#6:
	movl	12(%rsp), %eax
	cmpl	$16385, %eax            # imm = 0x4001
	jge	.LBB5_7
# BB#8:
	leal	16382(%rax), %edi
	movl	%edi, 12(%rsp)
	movl	$2147483648, %r14d      # imm = 0x80000000
	cmpl	$-16383, %eax           # imm = 0xC001
	jg	.LBB5_10
# BB#9:
	callq	ldexp
	movl	$0, 12(%rsp)
	xorl	%edi, %edi
.LBB5_10:
	orl	%edi, %ebx
	movl	%ebx, 12(%rsp)
	movl	$32, %edi
	movq	%rbx, %r15
	callq	ldexp
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	callq	floor
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	addsd	.LCPI5_2(%rip), %xmm0
	cvttsd2si	%xmm0, %rbx
	addq	%r14, %rbx
	movl	$32, %edi
	movapd	%xmm1, %xmm0
	callq	ldexp
	callq	floor
	movq	%r15, %rcx
	addsd	.LCPI5_2(%rip), %xmm0
	cvttsd2si	%xmm0, %rax
	addq	%r14, %rax
	jmp	.LBB5_11
.LBB5_7:
	movq	%rbx, %rcx
	orl	$32767, %ecx            # imm = 0x7FFF
	movl	%ecx, 12(%rsp)
.LBB5_4:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
.LBB5_11:
	movb	%ch, (%rbp)  # NOREX
	movb	%cl, 1(%rbp)
	movq	%rbx, %rcx
	shrq	$24, %rcx
	movb	%cl, 2(%rbp)
	movq	%rbx, %rcx
	shrq	$16, %rcx
	movb	%cl, 3(%rbp)
	movb	%bh, 4(%rbp)  # NOREX
	movb	%bl, 5(%rbp)
	movq	%rax, %rcx
	shrq	$24, %rcx
	movb	%cl, 6(%rbp)
	movq	%rax, %rcx
	shrq	$16, %rcx
	movb	%cl, 7(%rbp)
	movb	%ah, 8(%rbp)  # NOREX
	movb	%al, 9(%rbp)
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	ConvertToIeeeExtended, .Lfunc_end5-ConvertToIeeeExtended
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
