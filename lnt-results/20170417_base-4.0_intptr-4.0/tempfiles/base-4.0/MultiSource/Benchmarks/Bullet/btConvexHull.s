	.text
	.file	"btConvexHull.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	3212836864              # float -1
	.text
	.globl	_Z22ThreePlaneIntersectionRK7btPlaneS1_S1_
	.p2align	4, 0x90
	.type	_Z22ThreePlaneIntersectionRK7btPlaneS1_S1_,@function
_Z22ThreePlaneIntersectionRK7btPlaneS1_S1_: # @_Z22ThreePlaneIntersectionRK7btPlaneS1_S1_
	.cfi_startproc
# BB#0:
	movq	4(%rdi), %xmm9          # xmm9 = mem[0],zero
	movss	(%rsi), %xmm11          # xmm11 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movq	4(%rdx), %xmm6          # xmm6 = mem[0],zero
	movsd	4(%rsi), %xmm10         # xmm10 = mem[0],zero
	pshufd	$229, %xmm6, %xmm1      # xmm1 = xmm6[1,1,2,3]
	movss	(%rdx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	16(%rdx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	shufps	$0, %xmm1, %xmm0        # xmm0 = xmm0[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm0      # xmm0 = xmm0[2,0],xmm1[2,3]
	pshufd	$229, %xmm9, %xmm5      # xmm5 = xmm9[1,1,2,3]
	movss	(%rdi), %xmm14          # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm7
	shufps	$0, %xmm5, %xmm7        # xmm7 = xmm7[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm7      # xmm7 = xmm7[2,0],xmm5[2,3]
	movaps	%xmm7, %xmm13
	mulps	%xmm10, %xmm7
	mulps	%xmm0, %xmm10
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm11, %xmm1   # xmm1 = xmm1[0],xmm11[0],xmm1[1],xmm11[1]
	movaps	%xmm1, %xmm4
	mulps	%xmm6, %xmm4
	subps	%xmm4, %xmm10
	movaps	%xmm11, %xmm4
	mulss	%xmm6, %xmm4
	movaps	%xmm12, %xmm3
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm4
	movss	16(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulps	%xmm6, %xmm13
	mulps	%xmm9, %xmm0
	subps	%xmm0, %xmm13
	mulss	%xmm9, %xmm2
	mulss	%xmm14, %xmm6
	subss	%xmm6, %xmm2
	mulps	%xmm9, %xmm1
	subps	%xmm7, %xmm1
	mulss	%xmm14, %xmm12
	mulss	%xmm9, %xmm11
	subss	%xmm11, %xmm12
	mulss	%xmm10, %xmm14
	mulss	%xmm4, %xmm5
	mulss	%xmm3, %xmm4
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm10, %xmm3
	shufps	$229, %xmm10, %xmm10    # xmm10 = xmm10[1,1,2,3]
	mulss	%xmm9, %xmm10
	addss	%xmm14, %xmm10
	addss	%xmm10, %xmm5
	movss	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm5, %xmm0
	movss	16(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm13, %xmm5
	mulss	%xmm8, %xmm12
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm1, %xmm8
	addps	%xmm3, %xmm5
	addss	%xmm4, %xmm2
	addps	%xmm8, %xmm5
	addss	%xmm12, %xmm2
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	retq
.Lfunc_end0:
	.size	_Z22ThreePlaneIntersectionRK7btPlaneS1_S1_, .Lfunc_end0-_Z22ThreePlaneIntersectionRK7btPlaneS1_S1_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_
	.p2align	4, 0x90
	.type	_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_,@function
_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_: # @_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movb	_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif(%rip), %al
	testb	%al, %al
	jne	.LBB1_3
# BB#1:
	movl	$_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB1_3
# BB#2:
	movl	$_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif, %edi
	callq	__cxa_guard_release
.LBB1_3:
	movsd	(%r15), %xmm3           # xmm3 = mem[0],zero
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	subps	%xmm0, %xmm3
	movss	8(%r15), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	8(%rbx), %xmm2
	movaps	%xmm3, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	xorps	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm3, _ZZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif(%rip)
	movlps	%xmm0, _ZZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif+8(%rip)
	movss	(%r14), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm3, %xmm0
	mulss	%xmm8, %xmm4
	addss	%xmm0, %xmm4
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm7
	mulss	%xmm2, %xmm7
	addss	%xmm4, %xmm7
	movsd	(%rbx), %xmm4           # xmm4 = mem[0],zero
	mulss	%xmm4, %xmm5
	pshufd	$229, %xmm4, %xmm6      # xmm6 = xmm4[1,1,2,3]
	mulss	%xmm8, %xmm6
	addss	%xmm5, %xmm6
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	addss	%xmm6, %xmm0
	addss	16(%r14), %xmm0
	xorps	.LCPI1_0(%rip), %xmm0
	divss	%xmm7, %xmm0
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm4, %xmm0
	addss	%xmm5, %xmm2
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_, .Lfunc_end1-_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_
	.cfi_endproc

	.globl	_Z12PlaneProjectRK7btPlaneRK9btVector3
	.p2align	4, 0x90
	.type	_Z12PlaneProjectRK7btPlaneRK9btVector3,@function
_Z12PlaneProjectRK7btPlaneRK9btVector3: # @_Z12PlaneProjectRK7btPlaneRK9btVector3
	.cfi_startproc
# BB#0:
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rdi), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$229, %xmm0, %xmm3      # xmm3 = xmm0[1,1,2,3]
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	mulss	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	addss	16(%rdi), %xmm5
	mulss	%xmm5, %xmm3
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm1, %xmm5
	subps	%xmm5, %xmm0
	subss	%xmm3, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	retq
.Lfunc_end2:
	.size	_Z12PlaneProjectRK7btPlaneRK9btVector3, .Lfunc_end2-_Z12PlaneProjectRK7btPlaneRK9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1065353216              # float 1
	.text
	.globl	_Z9TriNormalRK9btVector3S1_S1_
	.p2align	4, 0x90
	.type	_Z9TriNormalRK9btVector3S1_S1_,@function
_Z9TriNormalRK9btVector3S1_S1_:         # @_Z9TriNormalRK9btVector3S1_S1_
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 48
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm6
	subss	(%rdi), %xmm6
	movsd	4(%rsi), %xmm2          # xmm2 = mem[0],zero
	movsd	4(%rdi), %xmm3          # xmm3 = mem[0],zero
	movss	4(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	pshufd	$229, %xmm2, %xmm4      # xmm4 = xmm2[1,1,2,3]
	subps	%xmm3, %xmm2
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	shufps	$0, %xmm4, %xmm1        # xmm1 = xmm1[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm1      # xmm1 = xmm1[2,0],xmm4[2,3]
	subps	%xmm1, %xmm5
	movaps	%xmm2, %xmm3
	mulps	%xmm5, %xmm3
	movaps	%xmm6, %xmm1
	mulss	%xmm0, %xmm6
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm2, %xmm5
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	shufps	$0, %xmm2, %xmm1        # xmm1 = xmm1[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm1      # xmm1 = xmm1[2,0],xmm2[2,3]
	mulps	%xmm1, %xmm0
	subps	%xmm0, %xmm3
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	%xmm5, %xmm6
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB3_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	movaps	%xmm3, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm3           # 16-byte Reload
	movaps	16(%rsp), %xmm6         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB3_2:                                # %.split
	movss	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	jne	.LBB3_4
	jp	.LBB3_4
# BB#3:
	xorps	%xmm1, %xmm1
	addq	$40, %rsp
	retq
.LBB3_4:
	divss	%xmm1, %xmm0
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	mulss	%xmm6, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movaps	%xmm2, %xmm0
	addq	$40, %rsp
	retq
.Lfunc_end3:
	.size	_Z9TriNormalRK9btVector3S1_S1_, .Lfunc_end3-_Z9TriNormalRK9btVector3S1_S1_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI4_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_Z20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_
	.p2align	4, 0x90
	.type	_Z20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_,@function
_Z20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_: # @_Z20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 144
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %rbp
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movb	_ZGVZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp(%rip), %al
	testb	%al, %al
	jne	.LBB4_3
# BB#1:
	movl	$_ZGVZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB4_3
# BB#2:
	movl	$_ZGVZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp, %edi
	callq	__cxa_guard_release
.LBB4_3:
	movsd	4(%r13), %xmm0          # xmm0 = mem[0],zero
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1]
	mulps	%xmm0, %xmm7
	movss	(%r13), %xmm4           # xmm4 = mem[0],zero,zero,zero
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	movaps	%xmm4, %xmm3
	shufps	$0, %xmm2, %xmm3        # xmm3 = xmm3[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm3      # xmm3 = xmm3[2,0],xmm2[2,3]
	movsd	4(%r15), %xmm2          # xmm2 = mem[0],zero
	mulps	%xmm3, %xmm2
	subps	%xmm2, %xmm7
	movaps	%xmm7, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	mulss	4(%r15), %xmm4
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm4
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB4_5
# BB#4:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	movaps	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm4         # 16-byte Reload
	movaps	64(%rsp), %xmm7         # 16-byte Reload
.LBB4_5:                                # %.split
	movss	.LCPI4_0(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm1
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm7
	mulss	%xmm4, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm7, _ZZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp(%rip)
	movaps	%xmm7, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movlps	%xmm0, _ZZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp+8(%rip)
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	addss	%xmm0, %xmm2
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	movss	(%r12), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	movss	4(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	addss	%xmm2, %xmm3
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	subss	%xmm0, %xmm2
	andps	.LCPI4_1(%rip), %xmm2
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	testq	%rbp, %rbp
	je	.LBB4_12
# BB#6:
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm9
	mulss	%xmm2, %xmm9
	movss	8(%r15), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	mulss	%xmm8, %xmm3
	subss	%xmm3, %xmm9
	mulss	%xmm7, %xmm8
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm8
	mulss	%xmm0, %xmm6
	mulss	%xmm7, %xmm2
	subss	%xmm2, %xmm6
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm8, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_8
# BB#7:                                 # %call.sqrt150
	movaps	%xmm9, 16(%rsp)         # 16-byte Spill
	movss	%xmm8, 4(%rsp)          # 4-byte Spill
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	32(%rsp), %xmm6         # 16-byte Reload
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm9         # 16-byte Reload
	movss	.LCPI4_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm11
	movaps	%xmm0, %xmm1
.LBB4_8:                                # %.split149
	movaps	%xmm11, %xmm7
	divss	%xmm1, %xmm7
	mulss	%xmm7, %xmm9
	mulss	%xmm7, %xmm8
	mulss	%xmm6, %xmm7
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm0
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%r12), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm5
	addss	%xmm1, %xmm5
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	(%r13), %xmm4           # xmm4 = mem[0],zero
	addps	%xmm0, %xmm4
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	addss	8(%r13), %xmm6
	movb	_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif(%rip), %al
	testb	%al, %al
	jne	.LBB4_11
# BB#9:
	movl	$_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif, %edi
	movaps	%xmm9, 16(%rsp)         # 16-byte Spill
	movss	%xmm8, 4(%rsp)          # 4-byte Spill
	movaps	%xmm7, 32(%rsp)         # 16-byte Spill
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movaps	%xmm4, 48(%rsp)         # 16-byte Spill
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	callq	__cxa_guard_acquire
	movaps	48(%rsp), %xmm4         # 16-byte Reload
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm7         # 16-byte Reload
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm9         # 16-byte Reload
	movss	.LCPI4_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm11
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	testl	%eax, %eax
	je	.LBB4_11
# BB#10:
	movl	$_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif, %edi
	callq	__cxa_guard_release
	movaps	48(%rsp), %xmm4         # 16-byte Reload
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm7         # 16-byte Reload
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm9         # 16-byte Reload
	movss	.LCPI4_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm11
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
.LBB4_11:                               # %_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_.exit78
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	subps	%xmm0, %xmm4
	subss	8(%rbx), %xmm6
	movaps	%xmm4, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm10, %xmm10
	xorps	%xmm2, %xmm2
	movss	%xmm6, %xmm2            # xmm2 = xmm6[0],xmm2[1,2,3]
	movlps	%xmm4, _ZZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif(%rip)
	movlps	%xmm2, _ZZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif+8(%rip)
	movaps	%xmm9, %xmm2
	mulss	%xmm4, %xmm2
	mulss	%xmm8, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm7, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm1, %xmm2
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	mulss	%xmm1, %xmm9
	pshufd	$229, %xmm1, %xmm3      # xmm3 = xmm1[1,1,2,3]
	mulss	%xmm8, %xmm3
	addss	%xmm9, %xmm3
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	addss	%xmm3, %xmm7
	subss	%xmm5, %xmm7
	xorps	.LCPI4_2(%rip), %xmm7
	divss	%xmm2, %xmm7
	mulss	%xmm7, %xmm6
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm4, %xmm7
	addps	%xmm1, %xmm7
	addss	%xmm0, %xmm6
	movss	%xmm6, %xmm10           # xmm10 = xmm6[0],xmm10[1,2,3]
	movlps	%xmm7, (%rbp)
	movlps	%xmm10, 8(%rbp)
.LBB4_12:
	testq	%r14, %r14
	je	.LBB4_19
# BB#13:
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	_ZZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp+8(%rip), %xmm2 # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm2, %xmm4
	movss	8(%r13), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	_ZZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp+4(%rip), %xmm5 # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	mulss	%xmm5, %xmm3
	subss	%xmm3, %xmm4
	movss	_ZZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp(%rip), %xmm3 # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm6
	mulss	%xmm0, %xmm5
	mulss	%xmm3, %xmm1
	subss	%xmm1, %xmm5
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_15
# BB#14:                                # %call.sqrt152
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	movss	%xmm5, 32(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	.LCPI4_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm11
	movss	32(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB4_15:                               # %.split151
	divss	%xmm1, %xmm11
	mulss	%xmm11, %xmm4
	mulss	%xmm11, %xmm6
	mulss	%xmm5, %xmm11
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbx), %xmm8          # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm8
	addss	%xmm1, %xmm8
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	movsd	(%r15), %xmm5           # xmm5 = mem[0],zero
	addps	%xmm0, %xmm5
	movss	8(%r12), %xmm7          # xmm7 = mem[0],zero,zero,zero
	addss	8(%r15), %xmm7
	movb	_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif(%rip), %al
	testb	%al, %al
	jne	.LBB4_18
# BB#16:
	movl	$_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif, %edi
	movaps	%xmm11, 32(%rsp)        # 16-byte Spill
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	movss	%xmm7, 12(%rsp)         # 4-byte Spill
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	movss	%xmm8, 8(%rsp)          # 4-byte Spill
	callq	__cxa_guard_acquire
	movss	8(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm11        # 16-byte Reload
	testl	%eax, %eax
	je	.LBB4_18
# BB#17:
	movl	$_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif, %edi
	callq	__cxa_guard_release
	movss	8(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm11        # 16-byte Reload
.LBB4_18:                               # %_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_.exit
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	subps	%xmm0, %xmm5
	subss	8(%r12), %xmm7
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm0, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm7, %xmm2            # xmm2 = xmm7[0],xmm2[1,2,3]
	movlps	%xmm5, _ZZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif(%rip)
	movlps	%xmm2, _ZZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif+8(%rip)
	movaps	%xmm4, %xmm2
	mulss	%xmm5, %xmm2
	mulss	%xmm6, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm11, %xmm2
	mulss	%xmm7, %xmm2
	addss	%xmm1, %xmm2
	movsd	(%r12), %xmm1           # xmm1 = mem[0],zero
	mulss	%xmm1, %xmm4
	pshufd	$229, %xmm1, %xmm3      # xmm3 = xmm1[1,1,2,3]
	mulss	%xmm6, %xmm3
	addss	%xmm4, %xmm3
	movss	8(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm11
	addss	%xmm3, %xmm11
	subss	%xmm8, %xmm11
	xorps	.LCPI4_2(%rip), %xmm11
	divss	%xmm2, %xmm11
	mulss	%xmm11, %xmm7
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm5, %xmm11
	addps	%xmm1, %xmm11
	addss	%xmm4, %xmm7
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movlps	%xmm11, (%r14)
	movlps	%xmm0, 8(%r14)
.LBB4_19:
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_Z20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_, .Lfunc_end4-_Z20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_
	.cfi_endproc

	.globl	_ZN7ConvexHC2Eiii
	.p2align	4, 0x90
	.type	_ZN7ConvexHC2Eiii,@function
_ZN7ConvexHC2Eiii:                      # @_ZN7ConvexHC2Eiii
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 80
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movl	%esi, %r12d
	movq	%rdi, %rbx
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movl	$0, 4(%rbx)
	movl	$0, 8(%rbx)
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movl	$0, 36(%rbx)
	movl	$0, 40(%rbx)
	movb	$1, 88(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 68(%rbx)
	testl	%r12d, %r12d
	jle	.LBB5_1
# BB#2:
	movslq	%r12d, %r13
	movq	%r13, %rdi
	shlq	$4, %rdi
.Ltmp0:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp1:
# BB#3:                                 # %_ZN20btAlignedObjectArrayI9btVector3E8allocateEi.exit.i.i
	movslq	4(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB5_11
# BB#4:                                 # %.lr.ph.i.i.i
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB5_5
# BB#6:                                 # %.prol.preheader81
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_7:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbp,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB5_7
	jmp	.LBB5_8
.LBB5_1:
	xorl	%eax, %eax
	jmp	.LBB5_25
.LBB5_5:
	xorl	%ecx, %ecx
.LBB5_8:                                # %.prol.loopexit82
	cmpq	$3, %r8
	jb	.LBB5_11
# BB#9:                                 # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB5_10:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbp,%rcx)
	movq	16(%rbx), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbp,%rcx)
	movq	16(%rbx), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbp,%rcx)
	movq	16(%rbx), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbp,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB5_10
.LBB5_11:                               # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_15
# BB#12:
	cmpb	$0, 24(%rbx)
	je	.LBB5_14
# BB#13:
.Ltmp2:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp3:
.LBB5_14:                               # %.noexc13
	movq	$0, 16(%rbx)
.LBB5_15:                               # %.lr.ph.i
	movb	$1, 24(%rbx)
	movq	%rbp, 16(%rbx)
	movl	%r12d, 8(%rbx)
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rbp)
	cmpl	$1, %r12d
	je	.LBB5_24
# BB#16:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.preheader
	leal	3(%r12), %edx
	leaq	-2(%r13), %rcx
	andq	$3, %rdx
	je	.LBB5_17
# BB#18:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.prol.preheader
	xorl	%eax, %eax
	movl	$16, %esi
	.p2align	4, 0x90
.LBB5_19:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rdi,%rsi)
	incq	%rax
	addq	$16, %rsi
	cmpq	%rax, %rdx
	jne	.LBB5_19
# BB#20:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$3, %rcx
	jae	.LBB5_22
	jmp	.LBB5_24
.LBB5_17:
	movl	$1, %eax
	cmpq	$3, %rcx
	jb	.LBB5_24
.LBB5_22:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.preheader.new
	subq	%rax, %r13
	shlq	$4, %rax
	addq	$48, %rax
	.p2align	4, 0x90
.LBB5_23:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, -48(%rcx,%rax)
	movq	16(%rbx), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, -32(%rcx,%rax)
	movq	16(%rbx), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, -16(%rcx,%rax)
	movq	16(%rbx), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$64, %rax
	addq	$-4, %r13
	jne	.LBB5_23
.LBB5_24:                               # %.loopexit66.loopexit
	movl	36(%rbx), %eax
.LBB5_25:                               # %.loopexit66
	movl	%r12d, 4(%rbx)
	cmpl	%r15d, %eax
	jge	.LBB5_41
# BB#26:
	cmpl	%r15d, 40(%rbx)
	jge	.LBB5_41
# BB#27:
	testl	%r15d, %r15d
	je	.LBB5_28
# BB#29:
	movslq	%r15d, %rdi
	shlq	$2, %rdi
.Ltmp5:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp6:
# BB#30:                                # %.noexc27
	movl	36(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB5_32
	jmp	.LBB5_36
.LBB5_28:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jle	.LBB5_36
.LBB5_32:                               # %.lr.ph.i.i.i17
	cltq
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB5_34
	.p2align	4, 0x90
.LBB5_33:                               # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rdi
	movl	(%rdi,%rcx,4), %edi
	movl	%edi, (%rbp,%rcx,4)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB5_33
.LBB5_34:                               # %.prol.loopexit74
	cmpq	$3, %rdx
	jb	.LBB5_36
	.p2align	4, 0x90
.LBB5_35:                               # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rdx
	movl	(%rdx,%rcx,4), %edx
	movl	%edx, (%rbp,%rcx,4)
	movq	48(%rbx), %rdx
	movl	4(%rdx,%rcx,4), %edx
	movl	%edx, 4(%rbp,%rcx,4)
	movq	48(%rbx), %rdx
	movl	8(%rdx,%rcx,4), %edx
	movl	%edx, 8(%rbp,%rcx,4)
	movq	48(%rbx), %rdx
	movl	12(%rdx,%rcx,4), %edx
	movl	%edx, 12(%rbp,%rcx,4)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB5_35
.LBB5_36:                               # %_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4copyEiiPS1_.exit.i.i
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_40
# BB#37:
	cmpb	$0, 56(%rbx)
	je	.LBB5_39
# BB#38:
.Ltmp7:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp8:
.LBB5_39:                               # %.noexc28
	movq	$0, 48(%rbx)
.LBB5_40:                               # %_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE7reserveEi.exit.preheader.i
	movb	$1, 56(%rbx)
	movq	%rbp, 48(%rbx)
	movl	%r15d, 40(%rbx)
.LBB5_41:                               # %.loopexit65
	movl	%r15d, 36(%rbx)
	movl	68(%rbx), %r15d
	cmpl	%r14d, %r15d
	jge	.LBB5_66
# BB#42:
	cmpl	%r14d, 72(%rbx)
	jge	.LBB5_60
# BB#43:
	testl	%r14d, %r14d
	je	.LBB5_44
# BB#45:
	movslq	%r14d, %rax
	shlq	$2, %rax
	leaq	(%rax,%rax,4), %rdi
.Ltmp10:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp11:
# BB#46:                                # %.noexc42
	movl	68(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB5_48
	jmp	.LBB5_55
.LBB5_44:
	xorl	%ebp, %ebp
	movl	%r15d, %eax
	testl	%eax, %eax
	jle	.LBB5_55
.LBB5_48:                               # %.lr.ph.i.i.i32
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %r9
	andq	$3, %r9
	je	.LBB5_49
# BB#50:                                # %.prol.preheader
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_51:                               # =>This Inner Loop Header: Depth=1
	movq	80(%rbx), %rcx
	movl	16(%rcx,%rdi), %esi
	movl	%esi, 16(%rbp,%rdi)
	movups	(%rcx,%rdi), %xmm0
	movups	%xmm0, (%rbp,%rdi)
	incq	%rdx
	addq	$20, %rdi
	cmpq	%rdx, %r9
	jne	.LBB5_51
	jmp	.LBB5_52
.LBB5_49:
	xorl	%edx, %edx
.LBB5_52:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB5_55
# BB#53:                                # %.lr.ph.i.i.i32.new
	subq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rcx
	leaq	60(,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB5_54:                               # =>This Inner Loop Header: Depth=1
	movq	80(%rbx), %rdx
	movl	-44(%rdx,%rcx), %esi
	movl	%esi, -44(%rbp,%rcx)
	movups	-60(%rdx,%rcx), %xmm0
	movups	%xmm0, -60(%rbp,%rcx)
	movq	80(%rbx), %rdx
	movl	-24(%rdx,%rcx), %esi
	movl	%esi, -24(%rbp,%rcx)
	movups	-40(%rdx,%rcx), %xmm0
	movups	%xmm0, -40(%rbp,%rcx)
	movq	80(%rbx), %rdx
	movl	-4(%rdx,%rcx), %esi
	movl	%esi, -4(%rbp,%rcx)
	movups	-20(%rdx,%rcx), %xmm0
	movups	%xmm0, -20(%rbp,%rcx)
	movq	80(%rbx), %rdx
	movl	16(%rdx,%rcx), %esi
	movl	%esi, 16(%rbp,%rcx)
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbp,%rcx)
	addq	$80, %rcx
	addq	$-4, %rax
	jne	.LBB5_54
.LBB5_55:                               # %_ZNK20btAlignedObjectArrayI7btPlaneE4copyEiiPS0_.exit.i.i
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_59
# BB#56:
	cmpb	$0, 88(%rbx)
	je	.LBB5_58
# BB#57:
.Ltmp12:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp13:
.LBB5_58:                               # %.noexc43
	movq	$0, 80(%rbx)
.LBB5_59:                               # %_ZN20btAlignedObjectArrayI7btPlaneE7reserveEi.exit.preheader.i
	movb	$1, 88(%rbx)
	movq	%rbp, 80(%rbx)
	movl	%r14d, 72(%rbx)
.LBB5_60:                               # %.lr.ph.i38
	movslq	%r15d, %rcx
	movslq	%r14d, %rax
	movl	%r14d, %esi
	subl	%r15d, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB5_63
# BB#61:                                # %_ZN20btAlignedObjectArrayI7btPlaneE7reserveEi.exit.i.prol.preheader
	leaq	(,%rcx,4), %rdi
	leaq	(%rdi,%rdi,4), %rdi
	negq	%rsi
	.p2align	4, 0x90
.LBB5_62:                               # %_ZN20btAlignedObjectArrayI7btPlaneE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	80(%rbx), %rbp
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rbp,%rdi)
	movl	$0, 16(%rbp,%rdi)
	incq	%rcx
	addq	$20, %rdi
	incq	%rsi
	jne	.LBB5_62
.LBB5_63:                               # %_ZN20btAlignedObjectArrayI7btPlaneE7reserveEi.exit.i.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB5_66
# BB#64:                                # %.lr.ph.i38.new
	subq	%rcx, %rax
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB5_65:                               # %_ZN20btAlignedObjectArrayI7btPlaneE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	80(%rbx), %rdx
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rdx,%rcx)
	movl	$0, 16(%rdx,%rcx)
	movq	80(%rbx), %rdx
	movups	8(%rsp), %xmm0
	movups	%xmm0, 20(%rdx,%rcx)
	movl	$0, 36(%rdx,%rcx)
	movq	80(%rbx), %rdx
	movups	8(%rsp), %xmm0
	movups	%xmm0, 40(%rdx,%rcx)
	movl	$0, 56(%rdx,%rcx)
	movq	80(%rbx), %rdx
	movups	8(%rsp), %xmm0
	movups	%xmm0, 60(%rdx,%rcx)
	movl	$0, 76(%rdx,%rcx)
	addq	$80, %rcx
	addq	$-4, %rax
	jne	.LBB5_65
.LBB5_66:                               # %.loopexit
	movl	%r14d, 68(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_69:
.Ltmp14:
	jmp	.LBB5_70
.LBB5_68:
.Ltmp9:
	jmp	.LBB5_70
.LBB5_67:
.Ltmp4:
.LBB5_70:
	movq	%rax, %rbp
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_74
# BB#71:
	cmpb	$0, 88(%rbx)
	je	.LBB5_73
# BB#72:
.Ltmp15:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
.LBB5_73:                               # %.noexc11
	movq	$0, 80(%rbx)
.LBB5_74:
	movb	$1, 88(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 68(%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_78
# BB#75:
	cmpb	$0, 56(%rbx)
	je	.LBB5_77
# BB#76:
.Ltmp17:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp18:
.LBB5_77:                               # %.noexc9
	movq	$0, 48(%rbx)
.LBB5_78:
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 36(%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_82
# BB#79:
	cmpb	$0, 24(%rbx)
	je	.LBB5_81
# BB#80:
.Ltmp19:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp20:
.LBB5_81:                               # %.noexc
	movq	$0, 16(%rbx)
.LBB5_82:
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB5_83:
.Ltmp21:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN7ConvexHC2Eiii, .Lfunc_end5-_ZN7ConvexHC2Eiii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp8-.Ltmp5           #   Call between .Ltmp5 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp10         #   Call between .Ltmp10 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp20-.Ltmp15         #   Call between .Ltmp15 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Lfunc_end5-.Ltmp20     #   Call between .Ltmp20 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end6:
	.size	__clang_call_terminate, .Lfunc_end6-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_Z9PlaneTestRK7btPlaneRK9btVector3
	.p2align	4, 0x90
	.type	_Z9PlaneTestRK7btPlaneRK9btVector3,@function
_Z9PlaneTestRK7btPlaneRK9btVector3:     # @_Z9PlaneTestRK7btPlaneRK9btVector3
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi), %xmm0
	mulss	4(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	8(%rdi), %xmm0
	addss	%xmm1, %xmm0
	addss	16(%rdi), %xmm0
	movss	planetestepsilon(%rip), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	.LCPI7_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm2
	xorl	%ecx, %ecx
	ucomiss	%xmm0, %xmm2
	seta	%cl
	ucomiss	%xmm1, %xmm0
	movl	$2, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end7:
	.size	_Z9PlaneTestRK7btPlaneRK9btVector3, .Lfunc_end7-_Z9PlaneTestRK7btPlaneRK9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI8_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI8_2:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.text
	.globl	_Z9SplitTestR7ConvexHRK7btPlane
	.p2align	4, 0x90
	.type	_Z9SplitTestR7ConvexHRK7btPlane,@function
_Z9SplitTestR7ConvexHRK7btPlane:        # @_Z9SplitTestR7ConvexHRK7btPlane
	.cfi_startproc
# BB#0:
	movslq	4(%rdi), %r8
	testq	%r8, %r8
	jle	.LBB8_1
# BB#2:                                 # %.lr.ph
	movq	16(%rdi), %rdi
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	planetestepsilon(%rip), %xmm6 # xmm6 = mem[0],zero,zero,zero
	movaps	.LCPI8_0(%rip), %xmm7   # xmm7 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm6, %xmm7
	cmpl	$7, %r8d
	jbe	.LBB8_3
# BB#7:                                 # %min.iters.checked
	movq	%r8, %rax
	andq	$7, %rax
	movl	$8, %esi
	cmovneq	%rax, %rsi
	movq	%r8, %rdx
	subq	%rsi, %rdx
	je	.LBB8_3
# BB#8:                                 # %vector.ph
	subq	$56, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 64
	movaps	%xmm2, -48(%rsp)        # 16-byte Spill
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movaps	%xmm2, 32(%rsp)         # 16-byte Spill
	movaps	%xmm3, -64(%rsp)        # 16-byte Spill
	shufps	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0]
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, -80(%rsp)        # 16-byte Spill
	shufps	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	movaps	%xmm4, (%rsp)           # 16-byte Spill
	movaps	%xmm5, -96(%rsp)        # 16-byte Spill
	shufps	$0, %xmm5, %xmm5        # xmm5 = xmm5[0,0,0,0]
	movaps	%xmm5, -16(%rsp)        # 16-byte Spill
	movaps	%xmm6, -112(%rsp)       # 16-byte Spill
	shufps	$0, %xmm6, %xmm6        # xmm6 = xmm6[0,0,0,0]
	movaps	%xmm6, -32(%rsp)        # 16-byte Spill
	movaps	%xmm7, -128(%rsp)       # 16-byte Spill
	movaps	%xmm7, %xmm5
	shufps	$0, %xmm5, %xmm5        # xmm5 = xmm5[0,0,0,0]
	leaq	64(%rdi), %rax
	xorps	%xmm6, %xmm6
	movq	%rdx, %rsi
	xorps	%xmm7, %xmm7
	.p2align	4, 0x90
.LBB8_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-64(%rax), %xmm14
	movups	-48(%rax), %xmm4
	movups	-32(%rax), %xmm0
	movups	-16(%rax), %xmm2
	movups	16(%rax), %xmm13
	movups	(%rax), %xmm15
	movups	32(%rax), %xmm3
	movups	48(%rax), %xmm12
	movaps	%xmm2, %xmm1
	shufps	$0, %xmm0, %xmm1        # xmm1 = xmm1[0,0],xmm0[0,0]
	movaps	%xmm14, %xmm8
	unpcklps	%xmm4, %xmm8    # xmm8 = xmm8[0],xmm4[0],xmm8[1],xmm4[1]
	shufps	$36, %xmm1, %xmm8       # xmm8 = xmm8[0,1],xmm1[2,0]
	movaps	%xmm12, %xmm1
	shufps	$0, %xmm3, %xmm1        # xmm1 = xmm1[0,0],xmm3[0,0]
	movaps	%xmm15, %xmm9
	unpcklps	%xmm13, %xmm9   # xmm9 = xmm9[0],xmm13[0],xmm9[1],xmm13[1]
	shufps	$36, %xmm1, %xmm9       # xmm9 = xmm9[0,1],xmm1[2,0]
	movaps	%xmm0, %xmm1
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movaps	%xmm4, %xmm10
	shufps	$17, %xmm14, %xmm10     # xmm10 = xmm10[1,0],xmm14[1,0]
	shufps	$226, %xmm1, %xmm10     # xmm10 = xmm10[2,0],xmm1[2,3]
	movaps	%xmm3, %xmm1
	unpcklps	%xmm12, %xmm1   # xmm1 = xmm1[0],xmm12[0],xmm1[1],xmm12[1]
	movaps	%xmm13, %xmm11
	shufps	$17, %xmm15, %xmm11     # xmm11 = xmm11[1,0],xmm15[1,0]
	shufps	$226, %xmm1, %xmm11     # xmm11 = xmm11[2,0],xmm1[2,3]
	shufps	$34, %xmm0, %xmm2       # xmm2 = xmm2[2,0],xmm0[2,0]
	unpckhps	%xmm4, %xmm14   # xmm14 = xmm14[2],xmm4[2],xmm14[3],xmm4[3]
	shufps	$36, %xmm2, %xmm14      # xmm14 = xmm14[0,1],xmm2[2,0]
	shufps	$34, %xmm3, %xmm12      # xmm12 = xmm12[2,0],xmm3[2,0]
	unpckhps	%xmm13, %xmm15  # xmm15 = xmm15[2],xmm13[2],xmm15[3],xmm13[3]
	shufps	$36, %xmm12, %xmm15     # xmm15 = xmm15[0,1],xmm12[2,0]
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	mulps	%xmm0, %xmm8
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	mulps	%xmm1, %xmm10
	addps	%xmm8, %xmm10
	mulps	%xmm0, %xmm9
	mulps	%xmm1, %xmm11
	addps	%xmm9, %xmm11
	movaps	(%rsp), %xmm0           # 16-byte Reload
	mulps	%xmm0, %xmm14
	mulps	%xmm0, %xmm15
	addps	%xmm10, %xmm14
	addps	%xmm11, %xmm15
	movaps	-16(%rsp), %xmm0        # 16-byte Reload
	addps	%xmm0, %xmm14
	addps	%xmm0, %xmm15
	movaps	-32(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm1, %xmm0
	cmpltps	%xmm14, %xmm0
	cmpltps	%xmm15, %xmm1
	cmpltps	%xmm5, %xmm14
	movaps	.LCPI8_1(%rip), %xmm2   # xmm2 = [1,1,1,1]
	andps	%xmm2, %xmm14
	cmpltps	%xmm5, %xmm15
	andps	%xmm2, %xmm15
	movaps	.LCPI8_2(%rip), %xmm2   # xmm2 = [2,2,2,2]
	movaps	%xmm2, %xmm3
	andps	%xmm0, %xmm2
	andnps	%xmm14, %xmm0
	andps	%xmm1, %xmm3
	andnps	%xmm15, %xmm1
	orps	%xmm2, %xmm6
	orps	%xmm0, %xmm6
	orps	%xmm3, %xmm7
	orps	%xmm1, %xmm7
	subq	$-128, %rax
	addq	$-8, %rsi
	jne	.LBB8_9
# BB#10:                                # %middle.block
	orps	%xmm6, %xmm7
	pshufd	$78, %xmm7, %xmm0       # xmm0 = xmm7[2,3,0,1]
	por	%xmm7, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	por	%xmm0, %xmm1
	movd	%xmm1, %eax
	movaps	-48(%rsp), %xmm2        # 16-byte Reload
	movaps	-64(%rsp), %xmm3        # 16-byte Reload
	movaps	-80(%rsp), %xmm4        # 16-byte Reload
	movaps	-96(%rsp), %xmm5        # 16-byte Reload
	movaps	-112(%rsp), %xmm6       # 16-byte Reload
	movaps	-128(%rsp), %xmm7       # 16-byte Reload
	addq	$56, %rsp
	jmp	.LBB8_4
.LBB8_3:
	xorl	%edx, %edx
	xorl	%eax, %eax
.LBB8_4:                                # %scalar.ph.preheader
	movq	%rdx, %rsi
	shlq	$4, %rsi
	leaq	8(%rdi,%rsi), %rsi
	movl	$2, %edi
	.p2align	4, 0x90
.LBB8_5:                                # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-8(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	addss	%xmm5, %xmm0
	xorl	%ecx, %ecx
	ucomiss	%xmm0, %xmm7
	seta	%cl
	ucomiss	%xmm6, %xmm0
	cmoval	%edi, %ecx
	orl	%ecx, %eax
	incq	%rdx
	addq	$16, %rsi
	cmpq	%r8, %rdx
	jl	.LBB8_5
# BB#6:                                 # %._crit_edge
	retq
.LBB8_1:
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	_Z9SplitTestR7ConvexHRK7btPlane, .Lfunc_end8-_Z9SplitTestR7ConvexHRK7btPlane
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1065353216              # float 1
	.text
	.globl	_Z4orthRK9btVector3
	.p2align	4, 0x90
	.type	_Z4orthRK9btVector3,@function
_Z4orthRK9btVector3:                    # @_Z4orthRK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
	subq	$80, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 96
.Lcfi36:
	.cfi_offset %rbx, -16
	movsd	4(%rdi), %xmm3          # xmm3 = mem[0],zero
	xorps	%xmm9, %xmm9
	movaps	%xmm3, %xmm2
	mulps	%xmm9, %xmm2
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	pshufd	$229, %xmm3, %xmm8      # xmm8 = xmm3[1,1,2,3]
	subss	%xmm1, %xmm3
	movss	(%rdi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	xorps	%xmm5, %xmm5
	mulss	%xmm4, %xmm5
	movaps	%xmm5, %xmm0
	subss	%xmm2, %xmm0
	movaps	%xmm3, %xmm6
	unpcklps	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	xorps	%xmm7, %xmm7
	movss	%xmm0, %xmm7            # xmm7 = xmm0[0],xmm7[1,2,3]
	movlps	%xmm6, 32(%rsp)
	movlps	%xmm7, 40(%rsp)
	shufps	$0, %xmm8, %xmm5        # xmm5 = xmm5[0,0],xmm8[0,0]
	shufps	$226, %xmm8, %xmm5      # xmm5 = xmm5[2,0],xmm8[2,3]
	subss	%xmm2, %xmm4
	subps	%xmm5, %xmm2
	movss	%xmm4, %xmm9            # xmm9 = xmm4[0],xmm9[1,2,3]
	movlps	%xmm2, 16(%rsp)
	movaps	%xmm2, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movlps	%xmm9, 24(%rsp)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB9_2
# BB#1:                                 # %call.sqrt
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB9_2:                                # %.split
	mulss	%xmm2, %xmm2
	mulss	%xmm5, %xmm5
	addss	%xmm2, %xmm5
	mulss	%xmm4, %xmm4
	addss	%xmm5, %xmm4
	xorps	%xmm0, %xmm0
	sqrtss	%xmm4, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB9_4
# BB#3:                                 # %call.sqrt34
	movaps	%xmm4, %xmm0
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
.LBB9_4:                                # %.split.split
	ucomiss	%xmm0, %xmm1
	leaq	32(%rsp), %rax
	leaq	16(%rsp), %rbx
	cmovaq	%rax, %rbx
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movq	%rbx, %rax
	orq	$4, %rax
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB9_6
# BB#5:                                 # %call.sqrt35
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB9_6:                                # %.split.split.split
	movss	.LCPI9_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm2
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	mulss	8(%rbx), %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	addq	$80, %rsp
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_Z4orthRK9btVector3, .Lfunc_end9-_Z4orthRK9btVector3
	.cfi_endproc

	.globl	_ZeqRK4int3S1_
	.p2align	4, 0x90
	.type	_ZeqRK4int3S1_,@function
_ZeqRK4int3S1_:                         # @_ZeqRK4int3S1_
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	cmpl	(%rsi), %eax
	jne	.LBB10_2
# BB#1:
	movl	4(%rdi), %eax
	cmpl	4(%rsi), %eax
	jne	.LBB10_2
# BB#3:
	movl	8(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	8(%rsi), %ecx
	sete	%al
	retq
.LBB10_2:
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	_ZeqRK4int3S1_, .Lfunc_end10-_ZeqRK4int3S1_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1065353216              # float 1
	.text
	.globl	_Z5aboveP9btVector3RK4int3RKS_f
	.p2align	4, 0x90
	.type	_Z5aboveP9btVector3RK4int3RKS_f,@function
_Z5aboveP9btVector3RK4int3RKS_f:        # @_Z5aboveP9btVector3RK4int3RKS_f
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 80
.Lcfi41:
	.cfi_offset %rbx, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %r15, -16
	movaps	%xmm0, %xmm6
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movslq	(%r15), %rax
	movslq	4(%r15), %rcx
	movslq	8(%r15), %rdx
	shlq	$4, %rcx
	movss	(%rbx,%rcx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	shlq	$4, %rax
	movaps	%xmm1, %xmm7
	subss	(%rbx,%rax), %xmm7
	movsd	4(%rbx,%rcx), %xmm2     # xmm2 = mem[0],zero
	movsd	4(%rbx,%rax), %xmm3     # xmm3 = mem[0],zero
	shlq	$4, %rdx
	movss	4(%rbx,%rdx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	pshufd	$229, %xmm2, %xmm4      # xmm4 = xmm2[1,1,2,3]
	subps	%xmm3, %xmm2
	movss	(%rbx,%rdx), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movss	8(%rbx,%rdx), %xmm5     # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	shufps	$0, %xmm4, %xmm1        # xmm1 = xmm1[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm1      # xmm1 = xmm1[2,0],xmm4[2,3]
	subps	%xmm1, %xmm5
	movaps	%xmm2, %xmm3
	mulps	%xmm5, %xmm3
	movaps	%xmm7, %xmm1
	mulss	%xmm0, %xmm7
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm2, %xmm5
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	shufps	$0, %xmm2, %xmm1        # xmm1 = xmm1[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm1      # xmm1 = xmm1[2,0],xmm2[2,3]
	mulps	%xmm1, %xmm0
	subps	%xmm0, %xmm3
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm5, %xmm7
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm7, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB11_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movaps	%xmm7, 32(%rsp)         # 16-byte Spill
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	32(%rsp), %xmm7         # 16-byte Reload
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
.LBB11_2:                               # %.split
	movss	.LCPI11_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	jne	.LBB11_4
	jp	.LBB11_4
# BB#3:
	xorps	%xmm0, %xmm0
	jmp	.LBB11_5
.LBB11_4:
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	mulss	%xmm7, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movaps	%xmm2, %xmm1
.LBB11_5:                               # %_Z9TriNormalRK9btVector3S1_S1_.exit
	movslq	(%r15), %rax
	shlq	$4, %rax
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	(%rbx,%rax), %xmm2
	subss	4(%rbx,%rax), %xmm3
	movss	8(%r14), %xmm4          # xmm4 = mem[0],zero,zero,zero
	subss	8(%rbx,%rax), %xmm4
	mulss	%xmm1, %xmm2
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm3, %xmm1
	addss	%xmm2, %xmm1
	mulss	%xmm0, %xmm4
	addss	%xmm1, %xmm4
	xorl	%eax, %eax
	ucomiss	%xmm6, %xmm4
	seta	%al
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	_Z5aboveP9btVector3RK4int3RKS_f, .Lfunc_end11-_Z5aboveP9btVector3RK4int3RKS_f
	.cfi_endproc

	.globl	_Z7hasedgeRK4int3ii
	.p2align	4, 0x90
	.type	_Z7hasedgeRK4int3ii,@function
_Z7hasedgeRK4int3ii:                    # @_Z7hasedgeRK4int3ii
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	movl	4(%rdi), %ecx
	cmpl	%esi, %eax
	jne	.LBB12_2
# BB#1:
	cmpl	%edx, %ecx
	je	.LBB12_4
.LBB12_2:                               # %.thread.backedge
	movl	8(%rdi), %edi
	cmpl	%esi, %ecx
	jne	.LBB12_5
# BB#3:                                 # %.thread.backedge
	cmpl	%edx, %edi
	jne	.LBB12_5
.LBB12_4:                               # %.thread.backedge.2
	movl	$1, %eax
	retq
.LBB12_5:                               # %.thread.backedge.1
	cmpl	%esi, %edi
	sete	%cl
	cmpl	%edx, %eax
	sete	%al
	andb	%cl, %al
	movzbl	%al, %eax
	retq
.Lfunc_end12:
	.size	_Z7hasedgeRK4int3ii, .Lfunc_end12-_Z7hasedgeRK4int3ii
	.cfi_endproc

	.globl	_Z7hasvertRK4int3i
	.p2align	4, 0x90
	.type	_Z7hasvertRK4int3i,@function
_Z7hasvertRK4int3i:                     # @_Z7hasvertRK4int3i
	.cfi_startproc
# BB#0:
	movb	$1, %al
	cmpl	%esi, (%rdi)
	je	.LBB13_3
# BB#1:
	cmpl	%esi, 4(%rdi)
	je	.LBB13_3
# BB#2:
	cmpl	%esi, 8(%rdi)
	sete	%al
.LBB13_3:
	movzbl	%al, %eax
	retq
.Lfunc_end13:
	.size	_Z7hasvertRK4int3i, .Lfunc_end13-_Z7hasvertRK4int3i
	.cfi_endproc

	.globl	_Z9shareedgeRK4int3S1_
	.p2align	4, 0x90
	.type	_Z9shareedgeRK4int3S1_,@function
_Z9shareedgeRK4int3S1_:                 # @_Z9shareedgeRK4int3S1_
	.cfi_startproc
# BB#0:
	movl	(%rdi), %r9d
	movl	4(%rdi), %ecx
	movl	8(%rdi), %r8d
	movl	(%rsi), %edx
	movl	4(%rsi), %edi
	movl	$1, %eax
	cmpl	%edi, %r9d
	jne	.LBB14_2
# BB#1:
	cmpl	%edx, %ecx
	je	.LBB14_19
.LBB14_2:                               # %.thread.backedge.i
	cmpl	%edi, %ecx
	jne	.LBB14_4
# BB#3:                                 # %.thread.backedge.i
	cmpl	%edx, %r8d
	je	.LBB14_19
.LBB14_4:                               # %_Z7hasedgeRK4int3ii.exit
	cmpl	%edx, %r9d
	jne	.LBB14_6
# BB#5:                                 # %_Z7hasedgeRK4int3ii.exit
	cmpl	%edi, %r8d
	je	.LBB14_19
.LBB14_6:
	movl	4(%rsi), %edx
	movl	8(%rsi), %edi
	cmpl	%edi, %r9d
	jne	.LBB14_8
# BB#7:
	cmpl	%edx, %ecx
	je	.LBB14_19
.LBB14_8:                               # %.thread.backedge.i.1
	cmpl	%edi, %ecx
	jne	.LBB14_10
# BB#9:                                 # %.thread.backedge.i.1
	cmpl	%edx, %r8d
	je	.LBB14_19
.LBB14_10:                              # %_Z7hasedgeRK4int3ii.exit.1
	cmpl	%edx, %r9d
	jne	.LBB14_12
# BB#11:                                # %_Z7hasedgeRK4int3ii.exit.1
	cmpl	%edi, %r8d
	je	.LBB14_19
.LBB14_12:
	movl	(%rsi), %edi
	movl	8(%rsi), %edx
	cmpl	%edi, %r9d
	jne	.LBB14_14
# BB#13:
	cmpl	%edx, %ecx
	je	.LBB14_19
.LBB14_14:                              # %.thread.backedge.i.2
	cmpl	%edi, %ecx
	jne	.LBB14_16
# BB#15:                                # %.thread.backedge.i.2
	cmpl	%edx, %r8d
	je	.LBB14_19
.LBB14_16:                              # %_Z7hasedgeRK4int3ii.exit.2
	cmpl	%edx, %r9d
	jne	.LBB14_18
# BB#17:                                # %_Z7hasedgeRK4int3ii.exit.2
	cmpl	%edi, %r8d
	je	.LBB14_19
.LBB14_18:
	xorl	%eax, %eax
.LBB14_19:                              # %_Z7hasedgeRK4int3ii.exit.thread
	retq
.Lfunc_end14:
	.size	_Z9shareedgeRK4int3S1_, .Lfunc_end14-_Z9shareedgeRK4int3S1_
	.cfi_endproc

	.globl	_ZN14btHullTriangle4neibEii
	.p2align	4, 0x90
	.type	_ZN14btHullTriangle4neibEii,@function
_ZN14btHullTriangle4neibEii:            # @_ZN14btHullTriangle4neibEii
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	cmpl	%esi, %eax
	jne	.LBB15_2
# BB#1:
	cmpl	%edx, 4(%rdi)
	je	.LBB15_4
.LBB15_2:
	movl	4(%rdi), %ecx
	cmpl	%edx, %eax
	jne	.LBB15_7
# BB#3:
	cmpl	%esi, %ecx
	jne	.LBB15_7
.LBB15_4:
	movl	$2, %eax
.LBB15_5:
	leaq	12(%rdi,%rax,4), %rax
.LBB15_6:                               # %.loopexit
	retq
.LBB15_7:                               # %.thread.backedge
	cmpl	%esi, %ecx
	jne	.LBB15_10
# BB#8:
	cmpl	%edx, 8(%rdi)
	jne	.LBB15_10
# BB#9:
	xorl	%eax, %eax
	jmp	.LBB15_5
.LBB15_10:
	movl	8(%rdi), %r8d
	cmpl	%edx, %ecx
	jne	.LBB15_13
# BB#11:
	cmpl	%esi, %r8d
	jne	.LBB15_13
# BB#12:
	xorl	%eax, %eax
	jmp	.LBB15_5
.LBB15_13:                              # %.thread.backedge.1
	cmpl	%edx, %eax
	setne	%cl
	cmpl	%esi, %r8d
	jne	.LBB15_16
# BB#14:                                # %.thread.backedge.1
	testb	%cl, %cl
	jne	.LBB15_16
# BB#15:
	movl	$1, %eax
	jmp	.LBB15_5
.LBB15_16:
	cmpl	%esi, %eax
	setne	%cl
	cmpl	%edx, %r8d
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %eax
	jne	.LBB15_6
# BB#17:
	testb	%cl, %cl
	jne	.LBB15_6
# BB#18:
	movl	$1, %eax
	jmp	.LBB15_5
.Lfunc_end15:
	.size	_ZN14btHullTriangle4neibEii, .Lfunc_end15-_ZN14btHullTriangle4neibEii
	.cfi_endproc

	.globl	_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_
	.p2align	4, 0x90
	.type	_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_,@function
_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_: # @_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 48
.Lcfi49:
	.cfi_offset %rbx, -48
.Lcfi50:
	.cfi_offset %r12, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	leaq	12(%rdx), %r9
	leaq	12(%rsi), %r10
	movq	16(%rdi), %r11
	movl	$1, %r14d
	xorl	%r8d, %r8d
	jmp	.LBB16_1
	.p2align	4, 0x90
.LBB16_39:                              # %.thread.backedge.i91
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	8(%rdx), %ebx
	cmpl	%edi, %ebp
	jne	.LBB16_42
# BB#40:                                # %.thread.backedge.i91
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebx
	jne	.LBB16_42
# BB#41:                                #   in Loop: Header=BB16_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB16_6
	.p2align	4, 0x90
.LBB16_52:                              # %.thread.backedge.i77
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	8(%rsi), %ebp
	cmpl	%ecx, %ebx
	jne	.LBB16_55
# BB#53:                                # %.thread.backedge.i77
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebp
	jne	.LBB16_55
# BB#54:                                #   in Loop: Header=BB16_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB16_12
	.p2align	4, 0x90
.LBB16_65:                              # %.thread.backedge.i63
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	8(%rax), %r12d
	cmpl	%edi, %ebx
	jne	.LBB16_68
# BB#66:                                # %.thread.backedge.i63
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %r12d
	jne	.LBB16_68
# BB#67:                                #   in Loop: Header=BB16_1 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB16_18
	.p2align	4, 0x90
.LBB16_78:                              # %.thread.backedge.i49
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	8(%rsi), %ebx
	cmpl	%ecx, %ebp
	jne	.LBB16_81
# BB#79:                                # %.thread.backedge.i49
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebx
	jne	.LBB16_81
# BB#80:                                #   in Loop: Header=BB16_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB16_24
	.p2align	4, 0x90
.LBB16_91:                              # %.thread.backedge.i35
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	8(%rdx), %ebp
	cmpl	%edi, %ebx
	jne	.LBB16_94
# BB#92:                                # %.thread.backedge.i35
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebp
	jne	.LBB16_94
# BB#93:                                #   in Loop: Header=BB16_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB16_30
	.p2align	4, 0x90
.LBB16_104:                             # %.thread.backedge.i
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	8(%rax), %r12d
	cmpl	%ecx, %ebx
	jne	.LBB16_107
# BB#105:                               # %.thread.backedge.i
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %r12d
	jne	.LBB16_107
# BB#106:                               #   in Loop: Header=BB16_1 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB16_36
.LBB16_42:                              # %.thread.backedge.i91._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebp
	jne	.LBB16_45
# BB#43:                                # %.thread.backedge.i91._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebx
	jne	.LBB16_45
# BB#44:                                #   in Loop: Header=BB16_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB16_6
.LBB16_55:                              # %.thread.backedge.i77._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebx
	jne	.LBB16_58
# BB#56:                                # %.thread.backedge.i77._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebp
	jne	.LBB16_58
# BB#57:                                #   in Loop: Header=BB16_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB16_12
.LBB16_68:                              # %.thread.backedge.i63._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebx
	jne	.LBB16_71
# BB#69:                                # %.thread.backedge.i63._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %r12d
	jne	.LBB16_71
# BB#70:                                #   in Loop: Header=BB16_1 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB16_18
.LBB16_81:                              # %.thread.backedge.i49._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebp
	jne	.LBB16_84
# BB#82:                                # %.thread.backedge.i49._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebx
	jne	.LBB16_84
# BB#83:                                #   in Loop: Header=BB16_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB16_24
.LBB16_94:                              # %.thread.backedge.i35._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebx
	jne	.LBB16_97
# BB#95:                                # %.thread.backedge.i35._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebp
	jne	.LBB16_97
# BB#96:                                #   in Loop: Header=BB16_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB16_30
.LBB16_107:                             # %.thread.backedge.i._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebx
	jne	.LBB16_110
# BB#108:                               # %.thread.backedge.i._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %r12d
	jne	.LBB16_110
# BB#109:                               #   in Loop: Header=BB16_1 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB16_36
.LBB16_45:                              # %.thread.backedge.1.i98
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %eax
	setne	%bpl
	cmpl	%edi, %ebx
	jne	.LBB16_48
# BB#46:                                # %.thread.backedge.1.i98
                                        #   in Loop: Header=BB16_1 Depth=1
	testb	%bpl, %bpl
	jne	.LBB16_48
# BB#47:                                #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %eax
	jmp	.LBB16_6
.LBB16_58:                              # %.thread.backedge.1.i84
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %eax
	setne	%bl
	cmpl	%ecx, %ebp
	jne	.LBB16_61
# BB#59:                                # %.thread.backedge.1.i84
                                        #   in Loop: Header=BB16_1 Depth=1
	testb	%bl, %bl
	jne	.LBB16_61
# BB#60:                                #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %eax
	jmp	.LBB16_12
.LBB16_71:                              # %.thread.backedge.1.i70
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebp
	setne	%bl
	cmpl	%edi, %r12d
	jne	.LBB16_74
# BB#72:                                # %.thread.backedge.1.i70
                                        #   in Loop: Header=BB16_1 Depth=1
	testb	%bl, %bl
	jne	.LBB16_74
# BB#73:                                #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %ebx
	jmp	.LBB16_18
.LBB16_84:                              # %.thread.backedge.1.i56
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %eax
	setne	%bpl
	cmpl	%ecx, %ebx
	jne	.LBB16_87
# BB#85:                                # %.thread.backedge.1.i56
                                        #   in Loop: Header=BB16_1 Depth=1
	testb	%bpl, %bpl
	jne	.LBB16_87
# BB#86:                                #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %eax
	jmp	.LBB16_24
.LBB16_97:                              # %.thread.backedge.1.i42
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %eax
	setne	%bl
	cmpl	%edi, %ebp
	jne	.LBB16_100
# BB#98:                                # %.thread.backedge.1.i42
                                        #   in Loop: Header=BB16_1 Depth=1
	testb	%bl, %bl
	jne	.LBB16_100
# BB#99:                                #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %eax
	jmp	.LBB16_30
.LBB16_110:                             # %.thread.backedge.1.i
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebp
	setne	%bl
	cmpl	%ecx, %r12d
	jne	.LBB16_113
# BB#111:                               # %.thread.backedge.1.i
                                        #   in Loop: Header=BB16_1 Depth=1
	testb	%bl, %bl
	jne	.LBB16_113
# BB#112:                               #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %ecx
	jmp	.LBB16_36
.LBB16_48:                              #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %eax
	setne	%al
	cmpl	%ecx, %ebx
	jne	.LBB16_49
# BB#50:                                #   in Loop: Header=BB16_1 Depth=1
	testb	%al, %al
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %eax
	jne	.LBB16_7
# BB#51:                                #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %eax
	jmp	.LBB16_6
.LBB16_61:                              #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %eax
	setne	%al
	cmpl	%edi, %ebp
	jne	.LBB16_62
# BB#63:                                #   in Loop: Header=BB16_1 Depth=1
	testb	%al, %al
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %eax
	jne	.LBB16_13
# BB#64:                                #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %eax
	jmp	.LBB16_12
.LBB16_74:                              #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebp
	setne	%bl
	cmpl	%ecx, %r12d
	jne	.LBB16_75
# BB#76:                                #   in Loop: Header=BB16_1 Depth=1
	testb	%bl, %bl
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %ebx
	jne	.LBB16_19
# BB#77:                                #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %ebx
	jmp	.LBB16_18
.LBB16_87:                              #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %eax
	setne	%al
	cmpl	%edi, %ebx
	jne	.LBB16_88
# BB#89:                                #   in Loop: Header=BB16_1 Depth=1
	testb	%al, %al
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %eax
	jne	.LBB16_25
# BB#90:                                #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %eax
	jmp	.LBB16_24
.LBB16_100:                             #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %eax
	setne	%al
	cmpl	%ecx, %ebp
	jne	.LBB16_101
# BB#102:                               #   in Loop: Header=BB16_1 Depth=1
	testb	%al, %al
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %eax
	jne	.LBB16_31
# BB#103:                               #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %eax
	jmp	.LBB16_30
.LBB16_113:                             #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebp
	setne	%cl
	cmpl	%edi, %r12d
	jne	.LBB16_114
# BB#115:                               #   in Loop: Header=BB16_1 Depth=1
	testb	%cl, %cl
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %ecx
	jne	.LBB16_37
# BB#116:                               #   in Loop: Header=BB16_1 Depth=1
	movl	$1, %ecx
	jmp	.LBB16_36
.LBB16_49:                              #   in Loop: Header=BB16_1 Depth=1
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %eax
	jmp	.LBB16_7
.LBB16_62:                              #   in Loop: Header=BB16_1 Depth=1
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %eax
	jmp	.LBB16_13
.LBB16_75:                              #   in Loop: Header=BB16_1 Depth=1
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %ebx
	jmp	.LBB16_19
.LBB16_88:                              #   in Loop: Header=BB16_1 Depth=1
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %eax
	jmp	.LBB16_25
.LBB16_101:                             #   in Loop: Header=BB16_1 Depth=1
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %eax
	jmp	.LBB16_31
.LBB16_114:                             #   in Loop: Header=BB16_1 Depth=1
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %ecx
	jmp	.LBB16_37
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rax
	leaq	1(%rax), %r14
	movslq	%r14d, %rdi
	imulq	$1431655766, %rdi, %rcx # imm = 0x55555556
	movq	%rcx, %rbx
	shrq	$63, %rbx
	shrq	$32, %rcx
	addl	%ebx, %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %edi
	cmpl	$3, %eax
	cmoveq	%r8, %rax
	movl	(%rsi,%rax,4), %ecx
	movslq	%edi, %rax
	movl	(%rsi,%rax,4), %edi
	movl	(%rdx), %eax
	movl	4(%rdx), %ebp
	cmpl	%edi, %eax
	jne	.LBB16_3
# BB#2:                                 #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebp
	je	.LBB16_5
.LBB16_3:                               # %._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %eax
	jne	.LBB16_39
# BB#4:                                 # %._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebp
	jne	.LBB16_39
.LBB16_5:                               #   in Loop: Header=BB16_1 Depth=1
	movl	$2, %eax
.LBB16_6:                               #   in Loop: Header=BB16_1 Depth=1
	leaq	(%r9,%rax,4), %rax
.LBB16_7:                               # %_ZN14btHullTriangle4neibEii.exit102
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	(%rax), %r15d
	movl	(%rsi), %eax
	movl	4(%rsi), %ebx
	cmpl	%ecx, %eax
	jne	.LBB16_9
# BB#8:                                 # %_ZN14btHullTriangle4neibEii.exit102
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebx
	je	.LBB16_11
.LBB16_9:                               # %_ZN14btHullTriangle4neibEii.exit102._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %eax
	jne	.LBB16_52
# BB#10:                                # %_ZN14btHullTriangle4neibEii.exit102._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebx
	jne	.LBB16_52
.LBB16_11:                              #   in Loop: Header=BB16_1 Depth=1
	movl	$2, %eax
.LBB16_12:                              #   in Loop: Header=BB16_1 Depth=1
	leaq	(%r10,%rax,4), %rax
.LBB16_13:                              # %_ZN14btHullTriangle4neibEii.exit88
                                        #   in Loop: Header=BB16_1 Depth=1
	movslq	(%rax), %rax
	movq	(%r11,%rax,8), %rax
	movl	(%rax), %ebp
	movl	4(%rax), %ebx
	cmpl	%edi, %ebp
	jne	.LBB16_15
# BB#14:                                # %_ZN14btHullTriangle4neibEii.exit88
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebx
	je	.LBB16_17
.LBB16_15:                              # %_ZN14btHullTriangle4neibEii.exit88._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebp
	jne	.LBB16_65
# BB#16:                                # %_ZN14btHullTriangle4neibEii.exit88._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebx
	jne	.LBB16_65
.LBB16_17:                              #   in Loop: Header=BB16_1 Depth=1
	movl	$2, %ebx
.LBB16_18:                              #   in Loop: Header=BB16_1 Depth=1
	leaq	12(%rax,%rbx,4), %rbx
.LBB16_19:                              # %_ZN14btHullTriangle4neibEii.exit74
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	%r15d, (%rbx)
	movl	(%rsi), %eax
	movl	4(%rsi), %ebp
	cmpl	%ecx, %eax
	jne	.LBB16_21
# BB#20:                                # %_ZN14btHullTriangle4neibEii.exit74
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebp
	je	.LBB16_23
.LBB16_21:                              # %_ZN14btHullTriangle4neibEii.exit74._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %eax
	jne	.LBB16_78
# BB#22:                                # %_ZN14btHullTriangle4neibEii.exit74._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebp
	jne	.LBB16_78
.LBB16_23:                              #   in Loop: Header=BB16_1 Depth=1
	movl	$2, %eax
.LBB16_24:                              #   in Loop: Header=BB16_1 Depth=1
	leaq	(%r10,%rax,4), %rax
.LBB16_25:                              # %_ZN14btHullTriangle4neibEii.exit60
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	(%rax), %r15d
	movl	(%rdx), %eax
	movl	4(%rdx), %ebx
	cmpl	%edi, %eax
	jne	.LBB16_27
# BB#26:                                # %_ZN14btHullTriangle4neibEii.exit60
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebx
	je	.LBB16_29
.LBB16_27:                              # %_ZN14btHullTriangle4neibEii.exit60._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %eax
	jne	.LBB16_91
# BB#28:                                # %_ZN14btHullTriangle4neibEii.exit60._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebx
	jne	.LBB16_91
.LBB16_29:                              #   in Loop: Header=BB16_1 Depth=1
	movl	$2, %eax
.LBB16_30:                              #   in Loop: Header=BB16_1 Depth=1
	leaq	(%r9,%rax,4), %rax
.LBB16_31:                              # %_ZN14btHullTriangle4neibEii.exit46
                                        #   in Loop: Header=BB16_1 Depth=1
	movslq	(%rax), %rax
	movq	(%r11,%rax,8), %rax
	movl	(%rax), %ebp
	movl	4(%rax), %ebx
	cmpl	%ecx, %ebp
	jne	.LBB16_33
# BB#32:                                # %_ZN14btHullTriangle4neibEii.exit46
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebx
	je	.LBB16_35
.LBB16_33:                              # %_ZN14btHullTriangle4neibEii.exit46._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%edi, %ebp
	jne	.LBB16_104
# BB#34:                                # %_ZN14btHullTriangle4neibEii.exit46._crit_edge
                                        #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ecx, %ebx
	jne	.LBB16_104
.LBB16_35:                              #   in Loop: Header=BB16_1 Depth=1
	movl	$2, %ecx
.LBB16_36:                              #   in Loop: Header=BB16_1 Depth=1
	leaq	12(%rax,%rcx,4), %rcx
.LBB16_37:                              # %_ZN14btHullTriangle4neibEii.exit
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	%r15d, (%rcx)
	cmpl	$4, %r14d
	jne	.LBB16_1
# BB#38:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_, .Lfunc_end16-_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_
	.cfi_endproc

	.globl	_ZN11HullLibrary9removeb2bEP14btHullTriangleS1_
	.p2align	4, 0x90
	.type	_ZN11HullLibrary9removeb2bEP14btHullTriangleS1_,@function
_ZN11HullLibrary9removeb2bEP14btHullTriangleS1_: # @_ZN11HullLibrary9removeb2bEP14btHullTriangleS1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	callq	_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_
	movq	16(%r15), %rax
	movslq	24(%rbx), %rcx
	movq	$0, (%rax,%rcx,8)
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%r15), %rax
	movslq	24(%r14), %rcx
	movq	$0, (%rax,%rcx,8)
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.Lfunc_end17:
	.size	_ZN11HullLibrary9removeb2bEP14btHullTriangleS1_, .Lfunc_end17-_ZN11HullLibrary9removeb2bEP14btHullTriangleS1_
	.cfi_endproc

	.globl	_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle
	.p2align	4, 0x90
	.type	_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle,@function
_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle: # @_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movslq	24(%rsi), %rcx
	movq	$0, (%rax,%rcx,8)
	movq	%rsi, %rdi
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.Lfunc_end18:
	.size	_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle, .Lfunc_end18-_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle
	.cfi_endproc

	.globl	_ZN11HullLibrary7checkitEP14btHullTriangle
	.p2align	4, 0x90
	.type	_ZN11HullLibrary7checkitEP14btHullTriangle,@function
_ZN11HullLibrary7checkitEP14btHullTriangle: # @_ZN11HullLibrary7checkitEP14btHullTriangle
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end19:
	.size	_ZN11HullLibrary7checkitEP14btHullTriangle, .Lfunc_end19-_ZN11HullLibrary7checkitEP14btHullTriangle
	.cfi_endproc

	.globl	_ZN11HullLibrary16allocateTriangleEiii
	.p2align	4, 0x90
	.type	_ZN11HullLibrary16allocateTriangleEiii,@function
_ZN11HullLibrary16allocateTriangleEiii: # @_ZN11HullLibrary16allocateTriangleEiii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -48
.Lcfi66:
	.cfi_offset %r12, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%edx, %ebp
	movl	%esi, %ebx
	movq	%rdi, %r15
	movl	$36, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
	movl	%ebx, (%r14)
	movl	%ebp, 4(%r14)
	movl	%r12d, 8(%r14)
	movl	$-1, 12(%r14)
	movl	$-1, 16(%r14)
	movl	$-1, 20(%r14)
	movl	$-1, 28(%r14)
	movl	$0, 32(%r14)
	movl	4(%r15), %eax
	movl	%eax, 24(%r14)
	cmpl	8(%r15), %eax
	jne	.LBB20_15
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB20_15
# BB#2:
	testl	%ebp, %ebp
	je	.LBB20_3
# BB#4:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	4(%r15), %eax
	testl	%eax, %eax
	jg	.LBB20_6
	jmp	.LBB20_10
.LBB20_3:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB20_10
.LBB20_6:                               # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB20_8
	.p2align	4, 0x90
.LBB20_7:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB20_7
.LBB20_8:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB20_10
	.p2align	4, 0x90
.LBB20_9:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	16(%r15), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	16(%r15), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	16(%r15), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB20_9
.LBB20_10:                              # %_ZNK20btAlignedObjectArrayIP14btHullTriangleE4copyEiiPS1_.exit.i.i
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_14
# BB#11:
	cmpb	$0, 24(%r15)
	je	.LBB20_13
# BB#12:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%r15), %eax
.LBB20_13:
	movq	$0, 16(%r15)
.LBB20_14:                              # %_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv.exit.i.i
	movb	$1, 24(%r15)
	movq	%rbx, 16(%r15)
	movl	%ebp, 8(%r15)
.LBB20_15:                              # %_ZN20btAlignedObjectArrayIP14btHullTriangleE9push_backERKS1_.exit
	movq	16(%r15), %rcx
	cltq
	movq	%r14, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 4(%r15)
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZN11HullLibrary16allocateTriangleEiii, .Lfunc_end20-_ZN11HullLibrary16allocateTriangleEiii
	.cfi_endproc

	.globl	_ZN11HullLibrary7extrudeEP14btHullTrianglei
	.p2align	4, 0x90
	.type	_ZN11HullLibrary7extrudeEP14btHullTrianglei,@function
_ZN11HullLibrary7extrudeEP14btHullTrianglei: # @_ZN11HullLibrary7extrudeEP14btHullTrianglei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 96
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movl	(%rbx), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	4(%rbx), %ebp
	movl	8(%rbx), %r12d
	movl	4(%r13), %r14d
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%edx, %esi
	movl	%ebp, %edx
	movl	%r12d, %ecx
	callq	_ZN11HullLibrary16allocateTriangleEiii
	movl	12(%rbx), %ecx
	leal	1(%r14), %r8d
	leal	2(%r14), %edi
	movl	%ecx, 12(%rax)
	movl	%r8d, 16(%rax)
	movl	%edi, 20(%rax)
	movq	16(%r13), %rdx
	movslq	12(%rbx), %rcx
	movq	(%rdx,%rcx,8), %rdx
	movl	(%rdx), %ecx
	movl	4(%rdx), %esi
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	cmpl	%ebp, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%r8d, 24(%rsp)          # 4-byte Spill
	jne	.LBB21_2
# BB#1:
	cmpl	%r12d, %esi
	je	.LBB21_4
.LBB21_2:                               # %._crit_edge
	cmpl	%r12d, %ecx
	jne	.LBB21_18
# BB#3:                                 # %._crit_edge
	cmpl	8(%rsp), %esi           # 4-byte Folded Reload
	jne	.LBB21_18
.LBB21_4:
	movl	%edi, %ebp
	movl	$2, %eax
.LBB21_5:
	movl	(%rsp), %ecx            # 4-byte Reload
.LBB21_6:
	leaq	12(%rdx,%rax,4), %rax
.LBB21_7:                               # %_ZN14btHullTriangle4neibEii.exit
	movl	%r14d, (%rax)
	movq	%r13, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	%r12d, %edx
	callq	_ZN11HullLibrary16allocateTriangleEiii
	movl	(%rsp), %edx            # 4-byte Reload
	movq	%rax, %r15
	movl	16(%rbx), %eax
	movl	%eax, 12(%r15)
	movl	%ebp, 16(%r15)
	movl	%r14d, 20(%r15)
	movq	16(%r13), %rax
	movslq	16(%rbx), %rcx
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %ecx
	movl	4(%rax), %esi
	cmpl	%r12d, %ecx
	movl	%ebp, 28(%rsp)          # 4-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	jne	.LBB21_9
# BB#8:                                 # %_ZN14btHullTriangle4neibEii.exit
	cmpl	%edx, %esi
	je	.LBB21_11
.LBB21_9:                               # %_ZN14btHullTriangle4neibEii.exit._crit_edge
	cmpl	%edx, %ecx
	jne	.LBB21_30
# BB#10:                                # %_ZN14btHullTriangle4neibEii.exit._crit_edge
	cmpl	%r12d, %esi
	jne	.LBB21_30
.LBB21_11:
	movl	$2, %ecx
.LBB21_12:
	movl	8(%rsp), %ebp           # 4-byte Reload
.LBB21_13:
	leaq	12(%rax,%rcx,4), %rcx
.LBB21_14:                              # %_ZN14btHullTriangle4neibEii.exit74
	movl	24(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, (%rcx)
	movq	%r13, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	%ebp, %ecx
	callq	_ZN11HullLibrary16allocateTriangleEiii
	movl	(%rsp), %esi            # 4-byte Reload
	movq	%rax, %r12
	movl	20(%rbx), %eax
	movl	%eax, 12(%r12)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, 16(%r12)
	movl	%r14d, 20(%r12)
	movq	16(%r13), %rax
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movslq	20(%rbx), %rcx
	movq	(%rax,%rcx,8), %rcx
	movl	(%rcx), %edx
	movl	4(%rcx), %edi
	cmpl	%esi, %edx
	jne	.LBB21_43
# BB#15:                                # %_ZN14btHullTriangle4neibEii.exit74
	cmpl	%ebp, %edi
	je	.LBB21_16
.LBB21_43:                              # %_ZN14btHullTriangle4neibEii.exit74._crit_edge
	cmpl	%ebp, %edx
	jne	.LBB21_45
# BB#44:                                # %_ZN14btHullTriangle4neibEii.exit74._crit_edge
	cmpl	%esi, %edi
	jne	.LBB21_45
.LBB21_16:
	movl	$2, %edx
.LBB21_17:
	movl	4(%rsp), %ebx           # 4-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB21_58:
	leaq	12(%rcx,%rdx,4), %rdx
.LBB21_59:                              # %_ZN14btHullTriangle4neibEii.exit60
	movl	28(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, (%rdx)
	movslq	12(%r14), %rcx
	movq	(%rax,%rcx,8), %rbp
	cmpl	%ebx, (%rbp)
	je	.LBB21_62
# BB#60:
	cmpl	%ebx, 4(%rbp)
	je	.LBB21_62
# BB#61:                                # %_Z7hasvertRK4int3i.exit46
	cmpl	%ebx, 8(%rbp)
	jne	.LBB21_63
.LBB21_62:                              # %_Z7hasvertRK4int3i.exit46.thread
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_
	movq	16(%r13), %rax
	movslq	24(%r14), %rcx
	movq	$0, (%rax,%rcx,8)
	movq	%r14, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%r13), %rax
	movslq	24(%rbp), %rcx
	movq	$0, (%rax,%rcx,8)
	movq	%rbp, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%r13), %rax
.LBB21_63:
	movslq	12(%r15), %rcx
	movq	(%rax,%rcx,8), %rbp
	cmpl	%ebx, (%rbp)
	je	.LBB21_66
# BB#64:
	cmpl	%ebx, 4(%rbp)
	je	.LBB21_66
# BB#65:                                # %_Z7hasvertRK4int3i.exit45
	cmpl	%ebx, 8(%rbp)
	jne	.LBB21_67
.LBB21_66:                              # %_Z7hasvertRK4int3i.exit45.thread
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_
	movq	16(%r13), %rax
	movslq	24(%r15), %rcx
	movq	$0, (%rax,%rcx,8)
	movq	%r15, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%r13), %rax
	movslq	24(%rbp), %rcx
	movq	$0, (%rax,%rcx,8)
	movq	%rbp, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%r13), %rax
.LBB21_67:
	movslq	12(%r12), %rcx
	movq	(%rax,%rcx,8), %rbp
	cmpl	%ebx, (%rbp)
	je	.LBB21_70
# BB#68:
	cmpl	%ebx, 4(%rbp)
	je	.LBB21_70
# BB#69:                                # %_Z7hasvertRK4int3i.exit
	cmpl	%ebx, 8(%rbp)
	jne	.LBB21_71
.LBB21_70:                              # %_Z7hasvertRK4int3i.exit.thread
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_
	movq	16(%r13), %rax
	movslq	24(%r12), %rcx
	movq	$0, (%rax,%rcx,8)
	movq	%r12, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%r13), %rax
	movslq	24(%rbp), %rcx
	movq	$0, (%rax,%rcx,8)
	movq	%rbp, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%r13), %rax
.LBB21_71:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movslq	24(%rdi), %rcx
	movq	$0, (%rax,%rcx,8)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB21_18:                              # %.thread.backedge.i
	movl	8(%rdx), %r9d
	cmpl	8(%rsp), %esi           # 4-byte Folded Reload
	jne	.LBB21_21
# BB#19:                                # %.thread.backedge.i
	cmpl	%r12d, %r9d
	je	.LBB21_20
.LBB21_21:                              # %.thread.backedge.i._crit_edge
	cmpl	%r12d, %esi
	jne	.LBB21_23
# BB#22:                                # %.thread.backedge.i._crit_edge
	cmpl	8(%rsp), %r9d           # 4-byte Folded Reload
	jne	.LBB21_23
.LBB21_20:
	movl	%edi, %ebp
	xorl	%eax, %eax
	jmp	.LBB21_5
.LBB21_30:                              # %.thread.backedge.i63
	movl	8(%rax), %edi
	cmpl	%r12d, %esi
	jne	.LBB21_33
# BB#31:                                # %.thread.backedge.i63
	cmpl	%edx, %edi
	jne	.LBB21_33
# BB#32:
	xorl	%ecx, %ecx
	jmp	.LBB21_12
.LBB21_45:                              # %.thread.backedge.i49
	movl	8(%rcx), %ebx
	cmpl	%esi, %edi
	jne	.LBB21_48
# BB#46:                                # %.thread.backedge.i49
	cmpl	%ebp, %ebx
	jne	.LBB21_48
# BB#47:
	xorl	%edx, %edx
	jmp	.LBB21_17
.LBB21_33:                              # %.thread.backedge.i63._crit_edge
	cmpl	%edx, %esi
	jne	.LBB21_36
# BB#34:                                # %.thread.backedge.i63._crit_edge
	cmpl	%r12d, %edi
	jne	.LBB21_36
# BB#35:
	xorl	%ecx, %ecx
	jmp	.LBB21_12
.LBB21_48:                              # %.thread.backedge.i49._crit_edge
	cmpl	%ebp, %edi
	jne	.LBB21_51
# BB#49:                                # %.thread.backedge.i49._crit_edge
	cmpl	%esi, %ebx
	jne	.LBB21_51
# BB#50:
	xorl	%edx, %edx
	jmp	.LBB21_17
.LBB21_23:                              # %.thread.backedge.1.i
	cmpl	%r12d, %ecx
	setne	%sil
	cmpl	8(%rsp), %r9d           # 4-byte Folded Reload
	jne	.LBB21_26
# BB#24:                                # %.thread.backedge.1.i
	testb	%sil, %sil
	jne	.LBB21_26
# BB#25:
	movl	%edi, %ebp
	movl	$1, %eax
	jmp	.LBB21_5
.LBB21_36:                              # %.thread.backedge.1.i70
	cmpl	%edx, %ecx
	setne	%sil
	cmpl	%r12d, %edi
	jne	.LBB21_39
# BB#37:                                # %.thread.backedge.1.i70
	testb	%sil, %sil
	jne	.LBB21_39
# BB#38:
	movl	$1, %ecx
	jmp	.LBB21_12
.LBB21_51:                              # %.thread.backedge.1.i56
	cmpl	%ebp, %edx
	setne	%dil
	cmpl	%esi, %ebx
	jne	.LBB21_54
# BB#52:                                # %.thread.backedge.1.i56
	testb	%dil, %dil
	jne	.LBB21_54
# BB#53:
	movl	$1, %edx
	jmp	.LBB21_17
.LBB21_26:
	movl	%edi, %ebp
	cmpl	8(%rsp), %ecx           # 4-byte Folded Reload
	setne	%sil
	cmpl	%r12d, %r9d
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %eax
	jne	.LBB21_27
# BB#28:
	testb	%sil, %sil
	movl	(%rsp), %ecx            # 4-byte Reload
	jne	.LBB21_7
# BB#29:
	movl	$1, %eax
	jmp	.LBB21_6
.LBB21_39:
	cmpl	%r12d, %ecx
	setne	%sil
	cmpl	%edx, %edi
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %ecx
	jne	.LBB21_40
# BB#41:
	testb	%sil, %sil
	movl	8(%rsp), %ebp           # 4-byte Reload
	jne	.LBB21_14
# BB#42:
	movl	$1, %ecx
	jmp	.LBB21_13
.LBB21_54:
	cmpl	%esi, %edx
	setne	%dil
	cmpl	%ebp, %ebx
	movl	$_ZZN14btHullTriangle4neibEiiE2er, %edx
	jne	.LBB21_55
# BB#56:
	testb	%dil, %dil
	movl	4(%rsp), %ebx           # 4-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	jne	.LBB21_59
# BB#57:
	movl	$1, %edx
	jmp	.LBB21_58
.LBB21_27:
	movl	(%rsp), %ecx            # 4-byte Reload
	jmp	.LBB21_7
.LBB21_40:
	movl	8(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB21_14
.LBB21_55:
	movl	4(%rsp), %ebx           # 4-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB21_59
.Lfunc_end21:
	.size	_ZN11HullLibrary7extrudeEP14btHullTrianglei, .Lfunc_end21-_ZN11HullLibrary7extrudeEP14btHullTrianglei
	.cfi_endproc

	.globl	_ZN11HullLibrary10extrudableEf
	.p2align	4, 0x90
	.type	_ZN11HullLibrary10extrudableEf,@function
_ZN11HullLibrary10extrudableEf:         # @_ZN11HullLibrary10extrudableEf
	.cfi_startproc
# BB#0:                                 # %.lr.ph
	movslq	4(%rdi), %rax
	movq	16(%rdi), %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB22_1:                               # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	movq	(%rcx,%rsi,8), %rdi
	je	.LBB22_4
# BB#2:                                 #   in Loop: Header=BB22_1 Depth=1
	testq	%rdi, %rdi
	je	.LBB22_5
# BB#3:                                 #   in Loop: Header=BB22_1 Depth=1
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	32(%rdx), %xmm1
	jbe	.LBB22_5
.LBB22_4:                               # %._crit_edge16
                                        #   in Loop: Header=BB22_1 Depth=1
	movq	%rdi, %rdx
.LBB22_5:                               #   in Loop: Header=BB22_1 Depth=1
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB22_1
# BB#6:                                 # %._crit_edge
	movss	32(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm1
	cmovaq	%rdx, %rax
	retq
.Lfunc_end22:
	.size	_ZN11HullLibrary10extrudableEf, .Lfunc_end22-_ZN11HullLibrary10extrudableEf
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI23_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI23_1:
	.long	1017370378              # float 0.0199999996
.LCPI23_2:
	.long	3164854026              # float -0.0199999996
.LCPI23_3:
	.long	1065353216              # float 1
	.text
	.globl	_ZN11HullLibrary11FindSimplexEP9btVector3iR20btAlignedObjectArrayIiE
	.p2align	4, 0x90
	.type	_ZN11HullLibrary11FindSimplexEP9btVector3iR20btAlignedObjectArrayIiE,@function
_ZN11HullLibrary11FindSimplexEP9btVector3iR20btAlignedObjectArrayIiE: # @_ZN11HullLibrary11FindSimplexEP9btVector3iR20btAlignedObjectArrayIiE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 208
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movl	%edx, %r15d
	movq	%rsi, %rbp
	movabsq	$4369572502438139658, %rax # imm = 0x3CA3D70A3C23D70A
	movq	%rax, 16(%rsp)
	movq	$1065353216, 24(%rsp)   # imm = 0x3F800000
	leaq	16(%rsp), %rdx
	movq	%rbp, %rdi
	movl	%r15d, %esi
	callq	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
	movl	%eax, %ebx
	movaps	.LCPI23_0(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	16(%rsp), %xmm0
	xorps	%xmm2, %xmm0
	movss	24(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 72(%rsp)
	movlps	%xmm2, 80(%rsp)
	leaq	72(%rsp), %rdx
	movq	%rbp, %rdi
	movl	%r15d, %esi
	movq	%r12, %rcx
	callq	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movslq	%ebx, %rcx
	movq	%rax, %rbx
	movslq	%ebx, %r14
	movq	%rcx, %r13
	shlq	$4, %r13
	shlq	$4, %r14
	movsd	(%rbp,%r13), %xmm2      # xmm2 = mem[0],zero
	movsd	(%rbp,%r14), %xmm0      # xmm0 = mem[0],zero
	subps	%xmm0, %xmm2
	movss	8(%rbp,%r13), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	8(%rbp,%r14), %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	movq	$-1, %rsi
	cmpl	%ebx, %ecx
	jne	.LBB23_2
# BB#1:
	movq	$-1, %rdx
	jmp	.LBB23_31
.LBB23_2:
	movaps	%xmm2, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm1
	jne	.LBB23_6
	jp	.LBB23_6
# BB#3:
	ucomiss	%xmm3, %xmm4
	jne	.LBB23_6
	jp	.LBB23_6
# BB#4:
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm2
	jne	.LBB23_6
	jp	.LBB23_6
# BB#5:
	movq	$-1, %rdx
	jmp	.LBB23_31
.LBB23_6:                               # %.critedge90.thread
	movss	.LCPI23_1(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm7
	mulss	%xmm6, %xmm7
	movaps	%xmm4, %xmm8
	mulss	%xmm3, %xmm8
	movaps	%xmm7, %xmm10
	subss	%xmm8, %xmm10
	mulss	%xmm2, %xmm3
	addss	%xmm3, %xmm7
	subss	%xmm1, %xmm3
	mulss	%xmm2, %xmm6
	movaps	%xmm4, %xmm0
	subss	%xmm6, %xmm0
	movaps	%xmm10, %xmm6
	unpcklps	%xmm3, %xmm6    # xmm6 = xmm6[0],xmm3[0],xmm6[1],xmm3[1]
	xorps	%xmm9, %xmm9
	xorps	%xmm5, %xmm5
	movss	%xmm0, %xmm5            # xmm5 = xmm0[0],xmm5[1,2,3]
	movlps	%xmm6, 32(%rsp)
	movlps	%xmm5, 40(%rsp)
	subss	%xmm8, %xmm1
	mulss	.LCPI23_2(%rip), %xmm4
	subss	%xmm2, %xmm4
	unpcklps	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1]
	movss	%xmm4, %xmm9            # xmm9 = xmm4[0],xmm9[1,2,3]
	movlps	%xmm1, 48(%rsp)
	movlps	%xmm9, 56(%rsp)
	mulss	%xmm10, %xmm10
	mulss	%xmm3, %xmm3
	addss	%xmm10, %xmm3
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	xorps	%xmm2, %xmm2
	sqrtss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm2
	jnp	.LBB23_8
# BB#7:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm2
.LBB23_8:                               # %.critedge90.thread.split
	movss	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	56(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB23_10
# BB#9:                                 # %call.sqrt302
	movss	%xmm2, 88(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	88(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB23_10:                              # %.critedge90.thread.split.split
	leaq	32(%rsp), %rdx
	leaq	48(%rsp), %rax
	ucomiss	%xmm1, %xmm2
	ja	.LBB23_12
# BB#11:
	movaps	(%rax), %xmm0
	movaps	%xmm0, (%rdx)
.LBB23_12:
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	40(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB23_14
# BB#13:                                # %call.sqrt303
	callq	sqrtf
	leaq	32(%rsp), %rdx
	movaps	%xmm0, %xmm1
.LBB23_14:                              # %.split
	movss	.LCPI23_3(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	32(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 32(%rsp)
	movss	36(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 36(%rsp)
	mulss	40(%rsp), %xmm0
	movss	%xmm0, 40(%rsp)
	movq	%rbp, %rdi
	movl	%r15d, %esi
	movq	%r12, %rcx
	callq	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB23_16
# BB#15:                                # %.split
	cmpl	%ebx, %eax
	jne	.LBB23_17
.LBB23_16:
	movaps	32(%rsp), %xmm0
	movaps	.LCPI23_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	40(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 72(%rsp)
	movlps	%xmm2, 80(%rsp)
	leaq	72(%rsp), %rdx
	movq	%rbp, %rdi
	movl	%r15d, %esi
	movq	%r12, %rcx
	callq	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
.LBB23_17:
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB23_18
# BB#19:
	cmpl	%ebx, %eax
	je	.LBB23_18
# BB#20:
	leaq	(%rbp,%r13), %rdx
	leaq	8(%rbp,%r13), %rcx
	movslq	%eax, %rsi
	shlq	$4, %rsi
	movsd	(%rbp,%rsi), %xmm1      # xmm1 = mem[0],zero
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	subps	%xmm0, %xmm1
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movss	8(%rbp,%rsi), %xmm0     # xmm0 = mem[0],zero,zero,zero
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	subss	(%rcx), %xmm0
	xorps	%xmm2, %xmm2
	movaps	%xmm0, %xmm3
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movlps	%xmm1, 32(%rsp)
	movlps	%xmm3, 40(%rsp)
	movaps	%xmm0, %xmm2
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movss	20(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	16(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	shufps	$0, %xmm1, %xmm2        # xmm2 = xmm2[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm2      # xmm2 = xmm2[2,0],xmm1[2,3]
	movss	24(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulps	%xmm2, %xmm5
	movsd	20(%rsp), %xmm2         # xmm2 = mem[0],zero
	mulps	%xmm0, %xmm2
	subps	%xmm2, %xmm5
	movaps	%xmm5, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm3, %xmm1
	subss	%xmm1, %xmm4
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jnp	.LBB23_22
# BB#21:                                # %call.sqrt305
	movaps	%xmm1, %xmm0
	movss	%xmm4, 100(%rsp)        # 4-byte Spill
	movaps	%xmm5, 128(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	128(%rsp), %xmm5        # 16-byte Reload
	movss	100(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
.LBB23_22:                              # %.split304
	movss	.LCPI23_3(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm5
	mulss	%xmm4, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm5, 48(%rsp)
	movlps	%xmm0, 56(%rsp)
	movq	%rbp, %rdi
	movl	%r15d, %esi
	leaq	48(%rsp), %rdx
	movq	%r12, %rcx
	callq	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
	movq	88(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, %eax
	movq	%rcx, %r10
	je	.LBB23_25
# BB#23:                                # %.split304
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB23_25
# BB#24:                                # %.split304
	cmpl	%ebx, %eax
	jne	.LBB23_26
.LBB23_25:
	movaps	48(%rsp), %xmm0
	movaps	.LCPI23_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	56(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 72(%rsp)
	movlps	%xmm2, 80(%rsp)
	leaq	72(%rsp), %rdx
	movq	%rbp, %rdi
	movl	%r15d, %esi
	movq	%r12, %rcx
	movq	%r10, %r15
	callq	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
	movq	%r15, %r10
.LBB23_26:
	cmpl	%r10d, %eax
	je	.LBB23_18
# BB#27:
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	movq	$-1, %rsi
	je	.LBB23_28
# BB#29:
	cmpl	%ebx, %eax
	movq	$-1, %rdx
	je	.LBB23_31
# BB#30:
	leaq	(%rbp,%r14), %r9
	leaq	4(%rbp,%r13), %rdx
	leaq	4(%rbp,%r14), %rsi
	movq	104(%rsp), %rcx         # 8-byte Reload
	leaq	(%rbp,%rcx), %r8
	leaq	4(%rbp,%rcx), %r11
	movslq	%eax, %rdi
	shlq	$4, %rdi
	movq	112(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	(%rbp,%rdi), %xmm8      # xmm8 = mem[0],zero,zero,zero
	movss	4(%rbp,%rdi), %xmm9     # xmm9 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm8
	movss	(%rdx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm9
	movss	8(%rbp,%rdi), %xmm2     # xmm2 = mem[0],zero,zero,zero
	movq	120(%rsp), %rdx         # 8-byte Reload
	movss	(%rdx), %xmm7           # xmm7 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm2
	movss	(%r9), %xmm4            # xmm4 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm4
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm0
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm3
	movss	(%r8), %xmm1            # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	movss	(%r11), %xmm5           # xmm5 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm5
	movss	4(%r11), %xmm6          # xmm6 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm6
	movaps	%xmm3, %xmm7
	mulss	%xmm1, %xmm3
	mulss	%xmm0, %xmm1
	mulss	%xmm6, %xmm0
	mulss	%xmm5, %xmm7
	subss	%xmm7, %xmm0
	mulss	%xmm4, %xmm6
	subss	%xmm6, %xmm3
	mulss	%xmm4, %xmm5
	subss	%xmm1, %xmm5
	mulss	%xmm8, %xmm0
	mulss	%xmm9, %xmm3
	addss	%xmm0, %xmm3
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	xorps	%xmm0, %xmm0
	ucomiss	%xmm5, %xmm0
	movl	%eax, %edx
	cmoval	%r10d, %edx
	cmoval	%eax, %r10d
	movl	12(%rsp), %eax          # 4-byte Reload
	shlq	$32, %rbx
	orq	%rax, %rbx
	shlq	$32, %rdx
	orq	%r10, %rdx
	movq	%rbx, %rsi
	jmp	.LBB23_31
.LBB23_18:
	movq	$-1, %rdx
	movq	$-1, %rsi
.LBB23_31:                              # %.critedge90.thread287
	movq	%rsi, %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_28:
	movq	$-1, %rdx
	jmp	.LBB23_31
.Lfunc_end23:
	.size	_ZN11HullLibrary11FindSimplexEP9btVector3iR20btAlignedObjectArrayIiE, .Lfunc_end23-_ZN11HullLibrary11FindSimplexEP9btVector3iR20btAlignedObjectArrayIiE
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI24_0:
	.long	1065353216              # float 1
.LCPI24_1:
	.long	1016003125              # float 0.0174532924
.LCPI24_2:
	.long	1020054733              # float 0.0250000004
.LCPI24_3:
	.long	3256877056              # float -40
.LCPI24_4:
	.long	1084227584              # float 5
	.section	.text._Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE,"axG",@progbits,_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE,comdat
	.weak	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
	.p2align	4, 0x90
	.type	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE,@function
_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE: # @_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 208
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	movl	%esi, %ebx
	leaq	8(%rbp), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorps	%xmm9, %xmm9
	xorps	%xmm8, %xmm8
	leaq	136(%rsp), %r15
	leaq	120(%rsp), %r12
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	jmp	.LBB24_1
.LBB24_46:                              # %.thread269
                                        #   in Loop: Header=BB24_1 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	16(%r14), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	$0, (%rax,%rcx,4)
	movl	12(%rsp), %esi          # 4-byte Reload
	xorps	%xmm9, %xmm9
	xorps	%xmm8, %xmm8
	leaq	136(%rsp), %r15
	leaq	120(%rsp), %r12
	.p2align	4, 0x90
.LBB24_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_4 Depth 2
                                        #     Child Loop BB24_17 Depth 2
                                        #       Child Loop BB24_19 Depth 3
                                        #       Child Loop BB24_41 Depth 3
                                        #       Child Loop BB24_32 Depth 3
                                        #         Child Loop BB24_33 Depth 4
	movq	16(%r14), %rax
	testl	%esi, %esi
	jle	.LBB24_2
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB24_1 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	8(%rcx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movl	$-1, %r13d
	movq	80(%rsp), %rcx          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB24_4:                               #   Parent Loop BB24_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rax,%rdx,4)
	je	.LBB24_8
# BB#5:                                 #   in Loop: Header=BB24_4 Depth=2
	cmpl	$-1, %r13d
	je	.LBB24_7
# BB#6:                                 #   in Loop: Header=BB24_4 Depth=2
	movslq	%r13d, %rdi
	shlq	$4, %rdi
	movss	(%rbp,%rdi), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movss	4(%rbp,%rdi), %xmm4     # xmm4 = mem[0],zero,zero,zero
	movss	-8(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	-4(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulps	%xmm0, %xmm5
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm1, %xmm6
	addps	%xmm5, %xmm6
	movss	8(%rbp,%rdi), %xmm3     # xmm3 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm2, %xmm4
	addps	%xmm6, %xmm4
	movaps	%xmm4, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	ucomiss	%xmm3, %xmm4
	jbe	.LBB24_8
.LBB24_7:                               #   in Loop: Header=BB24_4 Depth=2
	movl	%edx, %r13d
.LBB24_8:                               #   in Loop: Header=BB24_4 Depth=2
	incq	%rdx
	addq	$16, %rcx
	cmpq	%rdx, %rbx
	jne	.LBB24_4
	jmp	.LBB24_9
	.p2align	4, 0x90
.LBB24_2:                               #   in Loop: Header=BB24_1 Depth=1
	movl	$-1, %r13d
.LBB24_9:                               # %_Z14maxdirfilteredI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE.exit
                                        #   in Loop: Header=BB24_1 Depth=1
	movslq	%r13d, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	cmpl	$3, (%rax,%rcx,4)
	je	.LBB24_27
# BB#10:                                #   in Loop: Header=BB24_1 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movsd	4(%rax), %xmm7          # xmm7 = mem[0],zero
	movaps	%xmm7, %xmm2
	mulps	%xmm9, %xmm2
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	pshufd	$229, %xmm7, %xmm3      # xmm3 = xmm7[1,1,2,3]
	subss	%xmm1, %xmm7
	movss	(%rax), %xmm9           # xmm9 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm1
	movaps	%xmm9, %xmm4
	mulss	%xmm8, %xmm4
	movaps	%xmm4, %xmm0
	subss	%xmm2, %xmm0
	movaps	%xmm7, %xmm5
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	xorps	%xmm6, %xmm6
	movss	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1,2,3]
	movlps	%xmm5, 136(%rsp)
	movlps	%xmm6, 144(%rsp)
	shufps	$0, %xmm3, %xmm4        # xmm4 = xmm4[0,0],xmm3[0,0]
	shufps	$226, %xmm3, %xmm4      # xmm4 = xmm4[2,0],xmm3[2,3]
	subss	%xmm2, %xmm9
	subps	%xmm4, %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm9, %xmm3            # xmm3 = xmm9[0],xmm3[1,2,3]
	movlps	%xmm2, 120(%rsp)
	movaps	%xmm2, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movlps	%xmm3, 128(%rsp)
	mulss	%xmm7, %xmm7
	mulss	%xmm1, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB24_12
# BB#11:                                # %call.sqrt
                                        #   in Loop: Header=BB24_1 Depth=1
	movss	%xmm9, 8(%rsp)          # 4-byte Spill
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	movaps	96(%rsp), %xmm2         # 16-byte Reload
	movss	8(%rsp), %xmm9          # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB24_12:                              # %.split
                                        #   in Loop: Header=BB24_1 Depth=1
	mulss	%xmm2, %xmm2
	mulss	%xmm4, %xmm4
	addss	%xmm2, %xmm4
	mulss	%xmm9, %xmm9
	addss	%xmm4, %xmm9
	xorps	%xmm0, %xmm0
	sqrtss	%xmm9, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB24_14
# BB#13:                                # %call.sqrt353
                                        #   in Loop: Header=BB24_1 Depth=1
	movaps	%xmm9, %xmm0
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
.LBB24_14:                              # %.split.split
                                        #   in Loop: Header=BB24_1 Depth=1
	ucomiss	%xmm0, %xmm1
	movq	%r12, %r14
	cmovaq	%r15, %r14
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movq	%r14, %rax
	orq	$4, %rax
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB24_16
# BB#15:                                # %call.sqrt354
                                        #   in Loop: Header=BB24_1 Depth=1
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB24_16:                              # %.split.split.split
                                        #   in Loop: Header=BB24_1 Depth=1
	movss	.LCPI24_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	divss	%xmm1, %xmm3
	movss	(%r14), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	movss	4(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	mulss	8(%r14), %xmm3
	movq	24(%rsp), %rax          # 8-byte Reload
	movss	8(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm6
	mulss	%xmm0, %xmm6
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	mulss	%xmm7, %xmm2
	subss	%xmm2, %xmm6
	movss	%xmm6, 40(%rsp)         # 4-byte Spill
	movss	%xmm3, 52(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm3
	mulss	%xmm4, %xmm0
	subss	%xmm0, %xmm3
	movss	%xmm3, 32(%rsp)         # 4-byte Spill
	movss	%xmm4, 48(%rsp)         # 4-byte Spill
	mulss	%xmm4, %xmm7
	movss	%xmm5, 44(%rsp)         # 4-byte Spill
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm7
	movss	%xmm7, 36(%rsp)         # 4-byte Spill
	movl	$-1, %r14d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB24_17:                              #   Parent Loop BB24_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_19 Depth 3
                                        #       Child Loop BB24_41 Depth 3
                                        #       Child Loop BB24_32 Depth 3
                                        #         Child Loop BB24_33 Depth 4
	movl	%r14d, %r12d
	movl	%eax, 88(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	mulss	.LCPI24_1(%rip), %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	callq	sinf
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movl	$-1, %r14d
	jle	.LBB24_24
# BB#18:                                # %.lr.ph.i148
                                        #   in Loop: Header=BB24_17 Depth=2
	movss	52(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	64(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	movss	36(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI24_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	movq	24(%rsp), %rax          # 8-byte Reload
	addss	8(%rax), %xmm1
	movss	44(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm4, %xmm2
	addss	4(%rax), %xmm2
	mulss	48(%rsp), %xmm5         # 4-byte Folded Reload
	mulss	40(%rsp), %xmm0         # 4-byte Folded Reload
	addss	%xmm5, %xmm0
	mulss	%xmm4, %xmm0
	addss	(%rax), %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movl	$-1, %r14d
	movq	80(%rsp), %rcx          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB24_19:                              #   Parent Loop BB24_1 Depth=1
                                        #     Parent Loop BB24_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$0, (%rax,%rdx,4)
	je	.LBB24_23
# BB#20:                                #   in Loop: Header=BB24_19 Depth=3
	cmpl	$-1, %r14d
	je	.LBB24_22
# BB#21:                                #   in Loop: Header=BB24_19 Depth=3
	movslq	%r14d, %rsi
	shlq	$4, %rsi
	movss	(%rbp,%rsi), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movss	4(%rbp,%rsi), %xmm4     # xmm4 = mem[0],zero,zero,zero
	movss	-8(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	-4(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulps	%xmm0, %xmm5
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm2, %xmm6
	addps	%xmm5, %xmm6
	movss	8(%rbp,%rsi), %xmm3     # xmm3 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm1, %xmm4
	addps	%xmm6, %xmm4
	movaps	%xmm4, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	ucomiss	%xmm3, %xmm4
	jbe	.LBB24_23
.LBB24_22:                              #   in Loop: Header=BB24_19 Depth=3
	movl	%edx, %r14d
.LBB24_23:                              #   in Loop: Header=BB24_19 Depth=3
	incq	%rdx
	addq	$16, %rcx
	cmpq	%rdx, %rbx
	jne	.LBB24_19
.LBB24_24:                              # %_Z14maxdirfilteredI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE.exit155
                                        #   in Loop: Header=BB24_17 Depth=2
	cmpl	%r13d, %r12d
	jne	.LBB24_28
# BB#25:                                # %_Z14maxdirfilteredI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE.exit155
                                        #   in Loop: Header=BB24_17 Depth=2
	cmpl	%r13d, %r14d
	je	.LBB24_26
.LBB24_28:                              #   in Loop: Header=BB24_17 Depth=2
	cmpl	$-1, %r12d
	je	.LBB24_45
# BB#29:                                #   in Loop: Header=BB24_17 Depth=2
	cmpl	%r14d, %r12d
	je	.LBB24_45
# BB#30:                                #   in Loop: Header=BB24_17 Depth=2
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	.LCPI24_3(%rip), %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB24_45
# BB#31:                                # %.lr.ph
                                        #   in Loop: Header=BB24_17 Depth=2
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB24_41
	.p2align	4, 0x90
.LBB24_32:                              # %.lr.ph.split.us
                                        #   Parent Loop BB24_1 Depth=1
                                        #     Parent Loop BB24_17 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB24_33 Depth 4
	movl	%r12d, %r15d
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	movaps	%xmm1, %xmm0
	mulss	.LCPI24_1(%rip), %xmm0
	movss	%xmm0, 92(%rsp)         # 4-byte Spill
	callq	sinf
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	movss	92(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movss	48(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	64(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	44(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	mulss	52(%rsp), %xmm1         # 4-byte Folded Reload
	movaps	%xmm1, %xmm5
	movss	40(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	36(%rsp), %xmm0         # 4-byte Folded Reload
	addss	%xmm3, %xmm1
	addss	%xmm4, %xmm2
	addss	%xmm5, %xmm0
	movss	.LCPI24_2(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm0
	movq	24(%rsp), %rax          # 8-byte Reload
	addss	(%rax), %xmm1
	addss	4(%rax), %xmm2
	addss	8(%rax), %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movl	$-1, %r12d
	movq	80(%rsp), %rcx          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB24_33:                              #   Parent Loop BB24_1 Depth=1
                                        #     Parent Loop BB24_17 Depth=2
                                        #       Parent Loop BB24_32 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$0, (%rax,%rdx,4)
	je	.LBB24_37
# BB#34:                                #   in Loop: Header=BB24_33 Depth=4
	cmpl	$-1, %r12d
	je	.LBB24_36
# BB#35:                                #   in Loop: Header=BB24_33 Depth=4
	movslq	%r12d, %rsi
	shlq	$4, %rsi
	movss	(%rbp,%rsi), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movss	4(%rbp,%rsi), %xmm4     # xmm4 = mem[0],zero,zero,zero
	movss	-8(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	-4(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulps	%xmm1, %xmm5
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm2, %xmm6
	addps	%xmm5, %xmm6
	movss	8(%rbp,%rsi), %xmm3     # xmm3 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm0, %xmm4
	addps	%xmm6, %xmm4
	movaps	%xmm4, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	ucomiss	%xmm3, %xmm4
	jbe	.LBB24_37
.LBB24_36:                              #   in Loop: Header=BB24_33 Depth=4
	movl	%edx, %r12d
.LBB24_37:                              #   in Loop: Header=BB24_33 Depth=4
	incq	%rdx
	addq	$16, %rcx
	cmpq	%rdx, %rbx
	jne	.LBB24_33
# BB#38:                                # %_Z14maxdirfilteredI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE.exit110.loopexit.us
                                        #   in Loop: Header=BB24_32 Depth=3
	cmpl	%r13d, %r15d
	jne	.LBB24_40
# BB#39:                                # %_Z14maxdirfilteredI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE.exit110.loopexit.us
                                        #   in Loop: Header=BB24_32 Depth=3
	cmpl	%r13d, %r12d
	je	.LBB24_26
.LBB24_40:                              #   in Loop: Header=BB24_32 Depth=3
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	.LCPI24_4(%rip), %xmm1
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jae	.LBB24_32
	jmp	.LBB24_45
	.p2align	4, 0x90
.LBB24_41:                              # %.lr.ph.split
                                        #   Parent Loop BB24_1 Depth=1
                                        #     Parent Loop BB24_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	movaps	%xmm1, %xmm0
	mulss	.LCPI24_1(%rip), %xmm0
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	callq	sinf
	movss	64(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	cmpl	%r13d, %r12d
	jne	.LBB24_44
# BB#42:                                # %.lr.ph.split
                                        #   in Loop: Header=BB24_41 Depth=3
	cmpl	$-1, %r13d
	je	.LBB24_43
.LBB24_44:                              #   in Loop: Header=BB24_41 Depth=3
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	.LCPI24_4(%rip), %xmm1
	movl	$-1, %r12d
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jae	.LBB24_41
	.p2align	4, 0x90
.LBB24_45:                              # %.thread
                                        #   in Loop: Header=BB24_17 Depth=2
	movl	88(%rsp), %eax          # 4-byte Reload
	addl	$45, %eax
	cmpl	$361, %eax              # imm = 0x169
	jl	.LBB24_17
	jmp	.LBB24_46
.LBB24_43:
	movl	$-1, %r13d
.LBB24_26:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	16(%rax), %rax
	movl	$3, (%rax,%rcx,4)
.LBB24_27:                              # %.loopexit
	movl	%r13d, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE, .Lfunc_end24-_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI25_0:
	.long	981668463               # float 0.00100000005
.LCPI25_1:
	.long	1048576000              # float 0.25
.LCPI25_2:
	.long	1065353216              # float 1
.LCPI25_3:
	.long	1008981770              # float 0.00999999977
.LCPI25_4:
	.long	1036831949              # float 0.100000001
.LCPI25_5:
	.long	0                       # float 0
	.text
	.globl	_ZN11HullLibrary11calchullgenEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN11HullLibrary11calchullgenEP9btVector3ii,@function
_ZN11HullLibrary11calchullgenEP9btVector3ii: # @_ZN11HullLibrary11calchullgenEP9btVector3ii
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi115:
	.cfi_def_cfa_offset 256
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %ebx
	movq	%rsi, %r15
	xorl	%ecx, %ecx
	cmpl	$4, %ebx
	jl	.LBB25_178
# BB#1:
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	4(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 112(%rsp)        # 4-byte Spill
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movslq	%ebx, %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leaq	(,%rax,4), %rbp
.Ltmp22:
	movl	$16, %esi
	movq	%rbp, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, (%rsp)            # 8-byte Spill
.Ltmp23:
# BB#2:                                 # %_ZN20btAlignedObjectArrayIiE10deallocateEv.exit.i
	movb	$1, 88(%rsp)
	movq	$0, 80(%rsp)
	movq	$0, 68(%rsp)
.Ltmp25:
	movl	$16, %esi
	movq	%rbp, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp26:
	movss	16(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	112(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
# BB#3:                                 # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i188
	movslq	68(%rsp), %rax
	testq	%rax, %rax
	movq	80(%rsp), %rdi
	movl	%ebx, 144(%rsp)         # 4-byte Spill
	movq	%r14, 152(%rsp)         # 8-byte Spill
	jle	.LBB25_6
# BB#4:                                 # %.lr.ph.i.i190
	cmpl	$8, %eax
	jae	.LBB25_8
# BB#5:
	xorl	%ecx, %ecx
	jmp	.LBB25_21
.LBB25_6:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i194
	testq	%rdi, %rdi
	jne	.LBB25_27
# BB#7:                                 # %_ZN20btAlignedObjectArrayIiE10deallocateEv.exit.i199.thread
	movb	$1, 88(%rsp)
	movq	%rbp, 80(%rsp)
	movl	%ebx, 72(%rsp)
	jmp	.LBB25_30
.LBB25_8:                               # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB25_12
# BB#9:                                 # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB25_13
# BB#10:                                # %vector.memcheck
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB25_13
.LBB25_12:
	xorl	%ecx, %ecx
.LBB25_21:                              # %scalar.ph.preheader
	movl	%ebx, %r8d
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB25_24
# BB#22:                                # %scalar.ph.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB25_23:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %ebx
	movl	%ebx, (%rbp,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB25_23
.LBB25_24:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	movl	%r8d, %ebx
	jb	.LBB25_27
# BB#25:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%rdi,%rcx,4), %rdx
	leaq	28(%rbp,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB25_26:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rcx)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rcx)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	movl	(%rdx), %esi
	movl	%esi, (%rcx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB25_26
.LBB25_27:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i197
	cmpb	$0, 88(%rsp)
	je	.LBB25_29
# BB#28:
.Ltmp27:
	callq	_Z21btAlignedFreeInternalPv
	movss	96(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	112(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
.Ltmp28:
.LBB25_29:                              # %_ZN20btAlignedObjectArrayIiE10deallocateEv.exit.i199
	movb	$1, 88(%rsp)
	movq	%rbp, 80(%rsp)
	movl	%ebx, 72(%rsp)
	testl	%ebx, %ebx
	jle	.LBB25_96
.LBB25_30:                              # %.lr.ph604
	movq	$-8, %r9
	xorl	%r14d, %r14d
	movl	$1, %r8d
	movl	%ebx, %eax
	movaps	%xmm9, %xmm11
	movaps	%xmm10, %xmm7
	movaps	%xmm8, %xmm6
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	%ebx, %edx
	movq	%rbp, %rbx
	movl	%edx, %esi
	jmp	.LBB25_32
	.p2align	4, 0x90
.LBB25_31:                              # %_Z8btSetMinIfEvRT_RKS0_.exit.i._crit_edge
                                        #   in Loop: Header=BB25_32 Depth=1
	movl	72(%rsp), %eax
	incq	%r9
	movq	%rdx, %r14
	movaps	%xmm2, %xmm9
	movaps	%xmm1, %xmm10
	movaps	%xmm0, %xmm8
	movaps	%xmm5, %xmm11
	movaps	%xmm4, %xmm7
	movaps	%xmm3, %xmm6
	movq	%rcx, %rbx
	movl	%r13d, %esi
.LBB25_32:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_50 Depth 2
                                        #     Child Loop BB25_54 Depth 2
                                        #     Child Loop BB25_58 Depth 2
                                        #     Child Loop BB25_61 Depth 2
                                        #     Child Loop BB25_89 Depth 2
                                        #     Child Loop BB25_93 Depth 2
                                        #     Child Loop BB25_80 Depth 2
                                        #     Child Loop BB25_82 Depth 2
	movl	68(%rsp), %ecx
	cmpl	%eax, %ecx
	jne	.LBB25_66
# BB#33:                                #   in Loop: Header=BB25_32 Depth=1
	leal	(%rax,%rax), %r13d
	testl	%eax, %eax
	cmovel	%r8d, %r13d
	cmpl	%r13d, %eax
	jge	.LBB25_37
# BB#34:                                #   in Loop: Header=BB25_32 Depth=1
	movl	%esi, 148(%rsp)         # 4-byte Spill
	movq	%r9, 128(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	movss	%xmm9, 16(%rsp)         # 4-byte Spill
	movss	%xmm10, 112(%rsp)       # 4-byte Spill
	movss	%xmm8, 96(%rsp)         # 4-byte Spill
	movss	%xmm11, 160(%rsp)       # 4-byte Spill
	movss	%xmm7, 12(%rsp)         # 4-byte Spill
	movss	%xmm6, 8(%rsp)          # 4-byte Spill
	je	.LBB25_38
# BB#35:                                #   in Loop: Header=BB25_32 Depth=1
	movslq	%r13d, %rdi
	shlq	$2, %rdi
.Ltmp30:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movss	8(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	160(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	112(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movq	%rax, %r12
.Ltmp31:
# BB#36:                                # %.noexc203
                                        #   in Loop: Header=BB25_32 Depth=1
	movl	68(%rsp), %eax
	jmp	.LBB25_39
	.p2align	4, 0x90
.LBB25_37:                              #   in Loop: Header=BB25_32 Depth=1
	movl	%eax, %ecx
	jmp	.LBB25_66
.LBB25_38:                              #   in Loop: Header=BB25_32 Depth=1
	xorl	%r12d, %r12d
.LBB25_39:                              # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i
                                        #   in Loop: Header=BB25_32 Depth=1
	movq	80(%rsp), %rdi
	testl	%eax, %eax
	jle	.LBB25_42
# BB#40:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB25_32 Depth=1
	movslq	%eax, %rcx
	cmpl	$8, %eax
	jae	.LBB25_43
# BB#41:                                #   in Loop: Header=BB25_32 Depth=1
	xorl	%edx, %edx
	jmp	.LBB25_56
.LBB25_42:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
                                        #   in Loop: Header=BB25_32 Depth=1
	testq	%rdi, %rdi
	jne	.LBB25_62
	jmp	.LBB25_65
.LBB25_43:                              # %min.iters.checked683
                                        #   in Loop: Header=BB25_32 Depth=1
	movq	%rcx, %rdx
	andq	$-8, %rdx
	je	.LBB25_47
# BB#44:                                # %vector.memcheck695
                                        #   in Loop: Header=BB25_32 Depth=1
	leaq	(%rdi,%rcx,4), %rsi
	cmpq	%rsi, %r12
	jae	.LBB25_48
# BB#45:                                # %vector.memcheck695
                                        #   in Loop: Header=BB25_32 Depth=1
	leaq	(%r12,%rcx,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB25_48
.LBB25_47:                              #   in Loop: Header=BB25_32 Depth=1
	xorl	%edx, %edx
.LBB25_56:                              # %scalar.ph681.preheader
                                        #   in Loop: Header=BB25_32 Depth=1
	movq	%rbp, %r8
	subl	%edx, %eax
	leaq	-1(%rcx), %rsi
	subq	%rdx, %rsi
	andq	$7, %rax
	je	.LBB25_59
# BB#57:                                # %scalar.ph681.prol.preheader
                                        #   in Loop: Header=BB25_32 Depth=1
	negq	%rax
	.p2align	4, 0x90
.LBB25_58:                              # %scalar.ph681.prol
                                        #   Parent Loop BB25_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r12,%rdx,4)
	incq	%rdx
	incq	%rax
	jne	.LBB25_58
.LBB25_59:                              # %scalar.ph681.prol.loopexit
                                        #   in Loop: Header=BB25_32 Depth=1
	cmpq	$7, %rsi
	movq	%r8, %rbp
	jb	.LBB25_62
# BB#60:                                # %scalar.ph681.preheader.new
                                        #   in Loop: Header=BB25_32 Depth=1
	subq	%rdx, %rcx
	leaq	28(%rdi,%rdx,4), %rax
	leaq	28(%r12,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB25_61:                              # %scalar.ph681
                                        #   Parent Loop BB25_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-8, %rcx
	jne	.LBB25_61
.LBB25_62:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
                                        #   in Loop: Header=BB25_32 Depth=1
	cmpb	$0, 88(%rsp)
	je	.LBB25_64
# BB#63:                                #   in Loop: Header=BB25_32 Depth=1
.Ltmp32:
	callq	_Z21btAlignedFreeInternalPv
	movss	8(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	160(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	112(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
.Ltmp33:
.LBB25_64:                              # %.noexc204
                                        #   in Loop: Header=BB25_32 Depth=1
	movq	$0, 80(%rsp)
	movl	68(%rsp), %eax
.LBB25_65:                              # %_ZN20btAlignedObjectArrayIiE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB25_32 Depth=1
	movb	$1, 88(%rsp)
	movq	%r12, 80(%rsp)
	movl	%r13d, 72(%rsp)
	movl	%eax, %ecx
	movq	128(%rsp), %r9          # 8-byte Reload
	movl	$1, %r8d
	movl	148(%rsp), %esi         # 4-byte Reload
.LBB25_66:                              #   in Loop: Header=BB25_32 Depth=1
	movq	80(%rsp), %rax
	movslq	%ecx, %rcx
	movl	$1, (%rax,%rcx,4)
	incl	68(%rsp)
	movl	%esi, %eax
	cmpq	%rax, %r14
	jne	.LBB25_70
# BB#67:                                #   in Loop: Header=BB25_32 Depth=1
	leal	(%r14,%r14), %r13d
	testq	%r14, %r14
	cmovel	%r8d, %r13d
	movslq	%r13d, %rdi
	cmpq	%rdi, %r14
	jge	.LBB25_70
# BB#68:                                #   in Loop: Header=BB25_32 Depth=1
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	testl	%r13d, %r13d
	movss	%xmm9, 16(%rsp)         # 4-byte Spill
	movss	%xmm10, 112(%rsp)       # 4-byte Spill
	movss	%xmm8, 96(%rsp)         # 4-byte Spill
	movss	%xmm11, 160(%rsp)       # 4-byte Spill
	movss	%xmm7, 12(%rsp)         # 4-byte Spill
	movss	%xmm6, 8(%rsp)          # 4-byte Spill
	je	.LBB25_71
# BB#69:                                #   in Loop: Header=BB25_32 Depth=1
	shlq	$2, %rdi
.Ltmp35:
	movq	%r9, %rbp
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movss	8(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	160(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movq	%rbp, %r9
	movss	96(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	112(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movq	%rax, %rbp
.Ltmp36:
	jmp	.LBB25_72
	.p2align	4, 0x90
.LBB25_70:                              #   in Loop: Header=BB25_32 Depth=1
	movl	%esi, %r13d
	movq	%rbx, %rcx
	jmp	.LBB25_85
.LBB25_71:                              #   in Loop: Header=BB25_32 Depth=1
	xorl	%ebp, %ebp
.LBB25_72:                              # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i207
                                        #   in Loop: Header=BB25_32 Depth=1
	testq	%r14, %r14
	jle	.LBB25_83
# BB#73:                                # %.lr.ph.i.i.i209.preheader
                                        #   in Loop: Header=BB25_32 Depth=1
	cmpq	$8, %r14
	jb	.LBB25_77
# BB#74:                                # %min.iters.checked654
                                        #   in Loop: Header=BB25_32 Depth=1
	movq	%r14, %rax
	andq	$-8, %rax
	je	.LBB25_77
# BB#75:                                # %vector.memcheck667
                                        #   in Loop: Header=BB25_32 Depth=1
	leaq	(%rbx,%r14,4), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB25_87
# BB#76:                                # %vector.memcheck667
                                        #   in Loop: Header=BB25_32 Depth=1
	leaq	(,%r14,4), %rcx
	addq	%rbp, %rcx
	cmpq	%rcx, (%rsp)            # 8-byte Folded Reload
	jae	.LBB25_87
.LBB25_77:                              #   in Loop: Header=BB25_32 Depth=1
	movq	%r9, %r12
	xorl	%eax, %eax
.LBB25_78:                              # %.lr.ph.i.i.i209.preheader707
                                        #   in Loop: Header=BB25_32 Depth=1
	leaq	-1(%r14), %rcx
	movl	%r14d, %edx
	subl	%eax, %edx
	subq	%rax, %rcx
	testb	$7, %dl
	je	.LBB25_81
# BB#79:                                # %.lr.ph.i.i.i209.prol.preheader
                                        #   in Loop: Header=BB25_32 Depth=1
	movl	%r14d, %edx
	subl	%eax, %edx
	andl	$7, %edx
	negq	%rdx
	.p2align	4, 0x90
.LBB25_80:                              # %.lr.ph.i.i.i209.prol
                                        #   Parent Loop BB25_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rax,4), %esi
	movl	%esi, (%rbp,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB25_80
.LBB25_81:                              # %.lr.ph.i.i.i209.prol.loopexit
                                        #   in Loop: Header=BB25_32 Depth=1
	cmpq	$7, %rcx
	jb	.LBB25_84
	.p2align	4, 0x90
.LBB25_82:                              # %.lr.ph.i.i.i209
                                        #   Parent Loop BB25_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rax,4), %ecx
	movl	%ecx, (%rbp,%rax,4)
	movl	4(%rbx,%rax,4), %ecx
	movl	%ecx, 4(%rbp,%rax,4)
	movl	8(%rbx,%rax,4), %ecx
	movl	%ecx, 8(%rbp,%rax,4)
	movl	12(%rbx,%rax,4), %ecx
	movl	%ecx, 12(%rbp,%rax,4)
	movl	16(%rbx,%rax,4), %ecx
	movl	%ecx, 16(%rbp,%rax,4)
	movl	20(%rbx,%rax,4), %ecx
	movl	%ecx, 20(%rbp,%rax,4)
	movl	24(%rbx,%rax,4), %ecx
	movl	%ecx, 24(%rbp,%rax,4)
	movl	28(%rbx,%rax,4), %ecx
	movl	%ecx, 28(%rbp,%rax,4)
	addq	$8, %rax
	cmpq	%rax, %r14
	jne	.LBB25_82
	jmp	.LBB25_84
.LBB25_83:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i213
                                        #   in Loop: Header=BB25_32 Depth=1
	movq	%r9, %r12
	testq	%rbx, %rbx
	je	.LBB25_86
.LBB25_84:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i216
                                        #   in Loop: Header=BB25_32 Depth=1
.Ltmp37:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
	movss	8(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	160(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	112(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
.Ltmp38:
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r12, %r9
	movl	$1, %r8d
.LBB25_85:                              # %_Z8btSetMinIfEvRT_RKS0_.exit.i
                                        #   in Loop: Header=BB25_32 Depth=1
	movl	$0, (%rcx,%r14,4)
	leaq	1(%r14), %rdx
	shlq	$4, %r14
	movss	(%r15,%r14), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movss	4(%r15,%r14), %xmm4     # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	minss	%xmm8, %xmm0
	movaps	%xmm4, %xmm1
	minss	%xmm10, %xmm1
	movss	8(%r15,%r14), %xmm5     # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	minss	%xmm9, %xmm2
	maxss	%xmm6, %xmm3
	maxss	%xmm7, %xmm4
	maxss	%xmm11, %xmm5
	cmpq	176(%rsp), %rdx         # 8-byte Folded Reload
	jl	.LBB25_31
	jmp	.LBB25_95
.LBB25_86:                              #   in Loop: Header=BB25_32 Depth=1
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r12, %r9
	movl	$1, %r8d
	jmp	.LBB25_85
.LBB25_48:                              # %vector.body679.preheader
                                        #   in Loop: Header=BB25_32 Depth=1
	movq	%rbp, %r9
	movq	%rbx, %r8
	leaq	-8(%rdx), %rbx
	movl	%ebx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB25_51
# BB#49:                                # %vector.body679.prol.preheader
                                        #   in Loop: Header=BB25_32 Depth=1
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB25_50:                              # %vector.body679.prol
                                        #   Parent Loop BB25_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r12,%rbp,4)
	movups	%xmm1, 16(%r12,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB25_50
	jmp	.LBB25_52
.LBB25_87:                              # %vector.body650.preheader
                                        #   in Loop: Header=BB25_32 Depth=1
	leaq	-8(%r14), %rcx
	movq	%rcx, %rdx
	shrq	$3, %rdx
	incq	%rdx
	testb	$3, %dl
	je	.LBB25_90
# BB#88:                                # %vector.body650.prol.preheader
                                        #   in Loop: Header=BB25_32 Depth=1
	movq	%r9, %r12
	movl	%r9d, %edx
	shrl	$3, %edx
	incl	%edx
	andl	$3, %edx
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB25_89:                              # %vector.body650.prol
                                        #   Parent Loop BB25_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rsi,4), %xmm0
	movups	16(%rbx,%rsi,4), %xmm1
	movups	%xmm0, (%rbp,%rsi,4)
	movups	%xmm1, 16(%rbp,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB25_89
	jmp	.LBB25_91
.LBB25_51:                              #   in Loop: Header=BB25_32 Depth=1
	xorl	%ebp, %ebp
.LBB25_52:                              # %vector.body679.prol.loopexit
                                        #   in Loop: Header=BB25_32 Depth=1
	cmpq	$24, %rbx
	jb	.LBB25_55
# BB#53:                                # %vector.body679.preheader.new
                                        #   in Loop: Header=BB25_32 Depth=1
	movq	%rdx, %rsi
	subq	%rbp, %rsi
	leaq	112(%rdi,%rbp,4), %rbx
	leaq	112(%r12,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB25_54:                              # %vector.body679
                                        #   Parent Loop BB25_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbx
	subq	$-128, %rbp
	addq	$-32, %rsi
	jne	.LBB25_54
.LBB25_55:                              # %middle.block680
                                        #   in Loop: Header=BB25_32 Depth=1
	cmpq	%rdx, %rcx
	movq	%r8, %rbx
	movq	%r9, %rbp
	je	.LBB25_62
	jmp	.LBB25_56
.LBB25_90:                              #   in Loop: Header=BB25_32 Depth=1
	movq	%r9, %r12
	xorl	%esi, %esi
.LBB25_91:                              # %vector.body650.prol.loopexit
                                        #   in Loop: Header=BB25_32 Depth=1
	cmpq	$24, %rcx
	jb	.LBB25_94
# BB#92:                                # %vector.body650.preheader.new
                                        #   in Loop: Header=BB25_32 Depth=1
	movq	%r14, %rcx
	andq	$-8, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbx,%rsi,4), %rdx
	leaq	112(%rbp,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB25_93:                              # %vector.body650
                                        #   Parent Loop BB25_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB25_93
.LBB25_94:                              # %middle.block651
                                        #   in Loop: Header=BB25_32 Depth=1
	cmpq	%rax, %r14
	jne	.LBB25_78
	jmp	.LBB25_84
.LBB25_95:
	movq	%rcx, %rbx
	movaps	%xmm3, %xmm8
	movaps	%xmm4, %xmm10
	movaps	%xmm5, %xmm9
	jmp	.LBB25_97
.LBB25_96:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rbp
	movq	%rax, %rbx
	movaps	%xmm8, %xmm0
	movaps	%xmm10, %xmm1
	movaps	%xmm9, %xmm2
.LBB25_97:                              # %_ZN20btAlignedObjectArrayIiE7reserveEi.exit202._crit_edge
	subss	%xmm0, %xmm8
	subss	%xmm1, %xmm10
	subss	%xmm2, %xmm9
	mulss	%xmm8, %xmm8
	mulss	%xmm10, %xmm10
	addss	%xmm8, %xmm10
	mulss	%xmm9, %xmm9
	addss	%xmm10, %xmm9
	xorps	%xmm0, %xmm0
	sqrtss	%xmm9, %xmm0
	movss	%xmm0, 52(%rsp)         # 4-byte Spill
	ucomiss	%xmm0, %xmm0
	jnp	.LBB25_99
# BB#98:                                # %call.sqrt
	movaps	%xmm9, %xmm0
	callq	sqrtf
	movss	%xmm0, 52(%rsp)         # 4-byte Spill
.LBB25_99:                              # %_ZN20btAlignedObjectArrayIiE7reserveEi.exit202._crit_edge.split
.Ltmp40:
	leaq	64(%rsp), %rcx
	movq	%r15, %rsi
	movl	144(%rsp), %edx         # 4-byte Reload
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	callq	_ZN11HullLibrary11FindSimplexEP9btVector3iR20btAlignedObjectArrayIiE
	movq	%rax, %r14
.Ltmp41:
# BB#100:
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	cmpl	$-1, %r14d
	movq	136(%rsp), %rdi         # 8-byte Reload
	je	.LBB25_169
# BB#101:
	movq	%r14, %rbp
	shrq	$32, %rbp
	movslq	%r14d, %rax
	movq	%r14, %rsi
	sarq	$32, %rsi
	movq	%rax, %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	shlq	$4, %rax
	movq	%rsi, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	shlq	$4, %rcx
	movss	(%r15,%rax), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%r15,%rax), %xmm1     # xmm1 = mem[0],zero,zero,zero
	addss	(%r15,%rcx), %xmm0
	addss	4(%r15,%rcx), %xmm1
	movss	8(%r15,%rax), %xmm2     # xmm2 = mem[0],zero,zero,zero
	addss	8(%r15,%rcx), %xmm2
	movslq	%edx, %rsi
	movq	%rsi, %rax
	shlq	$4, %rax
	addss	(%r15,%rax), %xmm0
	addss	4(%r15,%rax), %xmm1
	addss	8(%r15,%rax), %xmm2
	movq	%rdx, %r13
	shrq	$32, %r13
	movq	%rdx, %rax
	sarq	$32, %rax
	movq	%rax, %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	shlq	$4, %rax
	addss	(%r15,%rax), %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	addss	4(%r15,%rax), %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	addss	8(%r15,%rax), %xmm2
	movss	%xmm2, 176(%rsp)        # 4-byte Spill
.Ltmp43:
	movq	%rsi, %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%rdx, %r12
	movl	%r13d, %edx
	movq	%rbp, %rbx
	movl	%ebp, %ecx
	movq	%rdi, %rbp
	callq	_ZN11HullLibrary16allocateTriangleEiii
.Ltmp44:
# BB#102:
	movabsq	$12884901890, %rcx      # imm = 0x300000002
	movq	%rcx, 12(%rax)
	movl	$1, 20(%rax)
.Ltmp46:
	movq	%rbp, %rdi
	movl	%r13d, %esi
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	%r12d, %edx
	movl	%r14d, %ecx
	callq	_ZN11HullLibrary16allocateTriangleEiii
.Ltmp47:
# BB#103:
	movabsq	$8589934595, %rcx       # imm = 0x200000003
	movq	%rcx, 12(%rax)
	movl	$0, 20(%rax)
.Ltmp49:
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movl	%ebx, %edx
	movl	%r13d, %ecx
	callq	_ZN11HullLibrary16allocateTriangleEiii
.Ltmp50:
# BB#104:
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	movq	%rcx, 12(%rax)
	movl	$3, 20(%rax)
.Ltmp52:
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movl	%r14d, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZN11HullLibrary16allocateTriangleEiii
.Ltmp53:
# BB#105:
	movl	$1, 12(%rax)
	movl	$0, 16(%rax)
	movl	$2, 20(%rax)
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	$1, (%rax,%rcx,4)
	movq	160(%rsp), %rcx         # 8-byte Reload
	movl	$1, (%rax,%rcx,4)
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	$1, (%rax,%rcx,4)
	movq	112(%rsp), %rcx         # 8-byte Reload
	movl	$1, (%rax,%rcx,4)
	movl	4(%rbp), %eax
	testl	%eax, %eax
	movl	144(%rsp), %r12d        # 4-byte Reload
	movq	%rbp, %rdi
	jle	.LBB25_114
# BB#106:                               # %.lr.ph589
	xorl	%ebx, %ebx
	leaq	184(%rsp), %r13
	leaq	64(%rsp), %r14
	.p2align	4, 0x90
.LBB25_107:                             # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %rax
	movq	(%rax,%rbx,8), %rbp
	movslq	(%rbp), %rax
	movslq	4(%rbp), %rcx
	movslq	8(%rbp), %rdx
	shlq	$4, %rcx
	movss	(%r15,%rcx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	shlq	$4, %rax
	movaps	%xmm1, %xmm6
	subss	(%r15,%rax), %xmm6
	movsd	4(%r15,%rcx), %xmm2     # xmm2 = mem[0],zero
	movsd	4(%r15,%rax), %xmm3     # xmm3 = mem[0],zero
	shlq	$4, %rdx
	movss	4(%r15,%rdx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	pshufd	$229, %xmm2, %xmm4      # xmm4 = xmm2[1,1,2,3]
	subps	%xmm3, %xmm2
	movss	(%r15,%rdx), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movss	8(%r15,%rdx), %xmm5     # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	shufps	$0, %xmm4, %xmm1        # xmm1 = xmm1[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm1      # xmm1 = xmm1[2,0],xmm4[2,3]
	subps	%xmm1, %xmm5
	movaps	%xmm2, %xmm3
	mulps	%xmm5, %xmm3
	movaps	%xmm6, %xmm1
	mulss	%xmm0, %xmm6
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm2, %xmm5
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	shufps	$0, %xmm2, %xmm1        # xmm1 = xmm1[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm1      # xmm1 = xmm1[2,0],xmm2[2,3]
	mulps	%xmm1, %xmm0
	subps	%xmm0, %xmm3
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm5, %xmm6
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB25_109
# BB#108:                               # %call.sqrt970
                                        #   in Loop: Header=BB25_107 Depth=1
	movaps	%xmm1, %xmm0
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	112(%rsp), %xmm3        # 16-byte Reload
	movaps	16(%rsp), %xmm6         # 16-byte Reload
.LBB25_109:                             # %.split
                                        #   in Loop: Header=BB25_107 Depth=1
	ucomiss	.LCPI25_5, %xmm0
	jne	.LBB25_111
	jp	.LBB25_111
# BB#110:                               #   in Loop: Header=BB25_107 Depth=1
	xorps	%xmm1, %xmm1
	movss	.LCPI25_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB25_112
	.p2align	4, 0x90
.LBB25_111:                             #   in Loop: Header=BB25_107 Depth=1
	movss	.LCPI25_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	mulss	%xmm6, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
.LBB25_112:                             #   in Loop: Header=BB25_107 Depth=1
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movupd	%xmm0, 184(%rsp)
.Ltmp55:
	movq	%r15, %rdi
	movl	%r12d, %esi
	movq	%r13, %rdx
	movq	%r14, %rcx
	callq	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
.Ltmp56:
# BB#113:                               #   in Loop: Header=BB25_107 Depth=1
	movl	%eax, 28(%rbp)
	cltq
	movslq	(%rbp), %rcx
	shlq	$4, %rax
	shlq	$4, %rcx
	movss	(%r15,%rax), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%r15,%rax), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	(%r15,%rcx), %xmm0
	subss	4(%r15,%rcx), %xmm1
	movss	8(%r15,%rax), %xmm2     # xmm2 = mem[0],zero,zero,zero
	subss	8(%r15,%rcx), %xmm2
	mulss	184(%rsp), %xmm0
	mulss	188(%rsp), %xmm1
	addss	%xmm0, %xmm1
	mulss	192(%rsp), %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, 32(%rbp)
	incq	%rbx
	movq	136(%rsp), %rdi         # 8-byte Reload
	movslq	4(%rdi), %rax
	cmpq	%rax, %rbx
	jl	.LBB25_107
.LBB25_114:                             # %._crit_edge590
	movq	152(%rsp), %rdx         # 8-byte Reload
	leal	-4(%rdx), %ecx
	testl	%edx, %edx
	movl	$999999996, %ebx        # imm = 0x3B9AC9FC
	cmovnel	%ecx, %ebx
	testl	%ebx, %ebx
	jle	.LBB25_170
# BB#115:                               # %.lr.ph582
	movss	52(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	.LCPI25_0(%rip), %xmm1
	movss	.LCPI25_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movss	176(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	%xmm2, 176(%rsp)        # 4-byte Spill
	movss	.LCPI25_3(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	%xmm0, 112(%rsp)        # 4-byte Spill
	movss	%xmm1, 52(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm1
	mulss	.LCPI25_4(%rip), %xmm1
	movss	%xmm1, 128(%rsp)        # 4-byte Spill
	jmp	.LBB25_118
	.p2align	4, 0x90
.LBB25_116:                             # %._crit_edge579
                                        #   in Loop: Header=BB25_118 Depth=1
	movl	148(%rsp), %ebx         # 4-byte Reload
	cmpl	$2, %ebx
	jl	.LBB25_171
# BB#117:                               # %._crit_edge579._crit_edge
                                        #   in Loop: Header=BB25_118 Depth=1
	decl	%ebx
	movl	4(%rdi), %eax
.LBB25_118:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_119 Depth 2
                                        #     Child Loop BB25_129 Depth 2
                                        #     Child Loop BB25_139 Depth 2
                                        #     Child Loop BB25_157 Depth 2
	movslq	%eax, %rdx
	movq	16(%rdi), %rax
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	movq	56(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB25_119:                             #   Parent Loop BB25_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbp, %rbp
	movq	(%rax,%rsi,8), %rcx
	je	.LBB25_122
# BB#120:                               #   in Loop: Header=BB25_119 Depth=2
	testq	%rcx, %rcx
	je	.LBB25_123
# BB#121:                               #   in Loop: Header=BB25_119 Depth=2
	movss	32(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rbp), %xmm0
	jbe	.LBB25_123
.LBB25_122:                             # %._crit_edge16.i
                                        #   in Loop: Header=BB25_119 Depth=2
	movq	%rcx, %rbp
.LBB25_123:                             #   in Loop: Header=BB25_119 Depth=2
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB25_119
# BB#124:                               #   in Loop: Header=BB25_118 Depth=1
	testq	%rbp, %rbp
	movl	$1, 152(%rsp)           # 4-byte Folded Spill
	je	.LBB25_171
# BB#125:                               #   in Loop: Header=BB25_118 Depth=1
	movss	32(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	52(%rsp), %xmm0         # 4-byte Folded Reload
	jbe	.LBB25_171
# BB#126:                               #   in Loop: Header=BB25_118 Depth=1
	movl	%ebx, 148(%rsp)         # 4-byte Spill
	movslq	28(%rbp), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	$1, (%r8,%rcx,4)
	movslq	4(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB25_116
# BB#127:                               # %.lr.ph
                                        #   in Loop: Header=BB25_118 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	shlq	$4, %rdx
	leaq	(%r15,%rdx), %r12
	leaq	4(%r15,%rdx), %r13
	movq	%rcx, %rbx
	decq	%rbx
	movl	$1, %ebp
	subl	%ecx, %ebp
	jmp	.LBB25_129
	.p2align	4, 0x90
.LBB25_128:                             # %.backedge547._crit_edge
                                        #   in Loop: Header=BB25_129 Depth=2
	movq	16(%rdi), %rax
	decq	%rbx
	incl	%ebp
.LBB25_129:                             #   Parent Loop BB25_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB25_137
# BB#130:                               #   in Loop: Header=BB25_129 Depth=2
	movslq	(%rax), %r14
	movslq	4(%rax), %rcx
	movslq	8(%rax), %rax
	shlq	$4, %rcx
	movss	(%r15,%rcx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	shlq	$4, %r14
	movaps	%xmm1, %xmm6
	subss	(%r15,%r14), %xmm6
	movsd	4(%r15,%rcx), %xmm2     # xmm2 = mem[0],zero
	movsd	4(%r15,%r14), %xmm3     # xmm3 = mem[0],zero
	shlq	$4, %rax
	movss	4(%r15,%rax), %xmm0     # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	pshufd	$229, %xmm2, %xmm4      # xmm4 = xmm2[1,1,2,3]
	subps	%xmm3, %xmm2
	movss	(%r15,%rax), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movss	8(%r15,%rax), %xmm5     # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	shufps	$0, %xmm4, %xmm1        # xmm1 = xmm1[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm1      # xmm1 = xmm1[2,0],xmm4[2,3]
	subps	%xmm1, %xmm5
	movaps	%xmm2, %xmm3
	mulps	%xmm5, %xmm3
	movaps	%xmm6, %xmm1
	mulss	%xmm0, %xmm6
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm2, %xmm5
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	shufps	$0, %xmm2, %xmm1        # xmm1 = xmm1[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm1      # xmm1 = xmm1[2,0],xmm2[2,3]
	mulps	%xmm1, %xmm0
	subps	%xmm0, %xmm3
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm5, %xmm6
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB25_132
# BB#131:                               # %call.sqrt972
                                        #   in Loop: Header=BB25_129 Depth=2
	movaps	%xmm1, %xmm0
	movaps	%xmm6, 96(%rsp)         # 16-byte Spill
	movaps	%xmm3, 160(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	160(%rsp), %xmm3        # 16-byte Reload
	movaps	96(%rsp), %xmm6         # 16-byte Reload
	movq	136(%rsp), %rdi         # 8-byte Reload
.LBB25_132:                             # %.split971
                                        #   in Loop: Header=BB25_129 Depth=2
	leaq	(%r15,%r14), %rcx
	leaq	4(%r15,%r14), %rax
	ucomiss	.LCPI25_5, %xmm0
	jne	.LBB25_134
	jp	.LBB25_134
# BB#133:                               #   in Loop: Header=BB25_129 Depth=2
	xorps	%xmm1, %xmm1
	movss	.LCPI25_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB25_135
	.p2align	4, 0x90
.LBB25_134:                             #   in Loop: Header=BB25_129 Depth=2
	movss	.LCPI25_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	mulss	%xmm6, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
.LBB25_135:                             #   in Loop: Header=BB25_129 Depth=2
	movss	(%r12), %xmm2           # xmm2 = mem[0],zero,zero,zero
	subss	(%rcx), %xmm2
	movss	(%r13), %xmm3           # xmm3 = mem[0],zero,zero,zero
	subss	(%rax), %xmm3
	movss	4(%r13), %xmm4          # xmm4 = mem[0],zero,zero,zero
	subss	4(%rax), %xmm4
	mulss	%xmm0, %xmm2
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm3, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	ucomiss	112(%rsp), %xmm4        # 4-byte Folded Reload
	jbe	.LBB25_137
# BB#136:                               #   in Loop: Header=BB25_129 Depth=2
	movq	16(%rdi), %rax
	movq	(%rax,%rbx,8), %rsi
.Ltmp58:
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZN11HullLibrary7extrudeEP14btHullTrianglei
	movq	136(%rsp), %rdi         # 8-byte Reload
.Ltmp59:
.LBB25_137:                             # %.backedge547
                                        #   in Loop: Header=BB25_129 Depth=2
	testl	%ebp, %ebp
	jne	.LBB25_128
# BB#138:                               # %._crit_edge
                                        #   in Loop: Header=BB25_118 Depth=1
	movl	4(%rdi), %r13d
	testl	%r13d, %r13d
	je	.LBB25_116
	.p2align	4, 0x90
.LBB25_139:                             # %.lr.ph574
                                        #   Parent Loop BB25_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%r13d, %r12
	decl	%r13d
	movq	16(%rdi), %rax
	movq	-8(%rax,%r12,8), %rax
	testq	%rax, %rax
	je	.LBB25_154
# BB#140:                               #   in Loop: Header=BB25_139 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, (%rax)
	je	.LBB25_143
# BB#141:                               #   in Loop: Header=BB25_139 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, 4(%rax)
	je	.LBB25_143
# BB#142:                               #   in Loop: Header=BB25_139 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, 8(%rax)
	jne	.LBB25_155
.LBB25_143:                             # %.thread
                                        #   in Loop: Header=BB25_139 Depth=2
	movslq	(%rax), %rbp
	movslq	4(%rax), %r14
	movslq	8(%rax), %rbx
	shlq	$4, %r14
	movss	(%r15,%r14), %xmm1      # xmm1 = mem[0],zero,zero,zero
	shlq	$4, %rbp
	movaps	%xmm1, %xmm6
	subss	(%r15,%rbp), %xmm6
	movsd	4(%r15,%r14), %xmm2     # xmm2 = mem[0],zero
	movsd	4(%r15,%rbp), %xmm3     # xmm3 = mem[0],zero
	shlq	$4, %rbx
	movss	4(%r15,%rbx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	pshufd	$229, %xmm2, %xmm4      # xmm4 = xmm2[1,1,2,3]
	subps	%xmm3, %xmm2
	movss	(%r15,%rbx), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movss	8(%r15,%rbx), %xmm5     # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	shufps	$0, %xmm4, %xmm1        # xmm1 = xmm1[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm1      # xmm1 = xmm1[2,0],xmm4[2,3]
	subps	%xmm1, %xmm5
	movaps	%xmm2, %xmm3
	mulps	%xmm5, %xmm3
	movaps	%xmm6, %xmm1
	mulss	%xmm0, %xmm6
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm2, %xmm5
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	shufps	$0, %xmm2, %xmm1        # xmm1 = xmm1[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm1      # xmm1 = xmm1[2,0],xmm2[2,3]
	mulps	%xmm1, %xmm0
	subps	%xmm0, %xmm3
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm5, %xmm6
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB25_145
# BB#144:                               # %call.sqrt973
                                        #   in Loop: Header=BB25_139 Depth=2
	movaps	%xmm1, %xmm0
	movaps	%xmm6, 96(%rsp)         # 16-byte Spill
	movaps	%xmm3, 160(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	160(%rsp), %xmm3        # 16-byte Reload
	movaps	96(%rsp), %xmm6         # 16-byte Reload
	movq	136(%rsp), %rdi         # 8-byte Reload
.LBB25_145:                             # %.thread.split
                                        #   in Loop: Header=BB25_139 Depth=2
	leaq	(%r15,%rbp), %rcx
	leaq	4(%r15,%rbp), %rax
	ucomiss	.LCPI25_5, %xmm0
	jne	.LBB25_147
	jp	.LBB25_147
# BB#146:                               #   in Loop: Header=BB25_139 Depth=2
	xorps	%xmm4, %xmm4
	movss	.LCPI25_2(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	jmp	.LBB25_148
	.p2align	4, 0x90
.LBB25_147:                             #   in Loop: Header=BB25_139 Depth=2
	movss	.LCPI25_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm3, %xmm1
	mulss	%xmm6, %xmm2
	xorps	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
.LBB25_148:                             #   in Loop: Header=BB25_139 Depth=2
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm5
	movss	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm6
	movss	4(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	176(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm7
	mulss	%xmm1, %xmm5
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm6, %xmm1
	addss	%xmm5, %xmm1
	mulss	%xmm4, %xmm7
	addss	%xmm1, %xmm7
	ucomiss	112(%rsp), %xmm7        # 4-byte Folded Reload
	ja	.LBB25_152
# BB#149:                               # %.critedge183
                                        #   in Loop: Header=BB25_139 Depth=2
	leaq	(%r15,%r14), %rax
	leaq	4(%r15,%r14), %rcx
	leaq	(%r15,%rbx), %rdx
	leaq	4(%r15,%rbx), %rsi
	movss	(%rax), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm6
	subss	%xmm0, %xmm5
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm0
	subss	%xmm3, %xmm4
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm3
	subss	%xmm2, %xmm1
	movaps	%xmm4, %xmm2
	mulss	%xmm3, %xmm2
	movaps	%xmm1, %xmm7
	mulss	%xmm0, %xmm7
	subss	%xmm7, %xmm2
	mulss	%xmm6, %xmm1
	mulss	%xmm5, %xmm3
	subss	%xmm3, %xmm1
	mulss	%xmm5, %xmm0
	mulss	%xmm6, %xmm4
	subss	%xmm4, %xmm0
	mulss	%xmm2, %xmm2
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB25_151
# BB#150:                               # %call.sqrt974
                                        #   in Loop: Header=BB25_139 Depth=2
	callq	sqrtf
	movq	136(%rsp), %rdi         # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB25_151:                             # %.critedge183.split
                                        #   in Loop: Header=BB25_139 Depth=2
	movss	128(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB25_154
.LBB25_152:                             # %.critedge183.thread
                                        #   in Loop: Header=BB25_139 Depth=2
	decq	%r12
	movq	16(%rdi), %rax
	movq	(%rax,%r12,8), %rcx
	movslq	12(%rcx), %rcx
	movq	(%rax,%rcx,8), %rsi
.Ltmp61:
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%rdi, %rbx
	callq	_ZN11HullLibrary7extrudeEP14btHullTrianglei
.Ltmp62:
# BB#153:                               #   in Loop: Header=BB25_139 Depth=2
	movl	4(%rbx), %r13d
	movq	%rbx, %rdi
.LBB25_154:                             # %.backedge
                                        #   in Loop: Header=BB25_139 Depth=2
	testl	%r13d, %r13d
	jne	.LBB25_139
.LBB25_155:                             # %._crit_edge575
                                        #   in Loop: Header=BB25_118 Depth=1
	movslq	4(%rdi), %r14
	testq	%r14, %r14
	movl	144(%rsp), %r12d        # 4-byte Reload
	je	.LBB25_116
# BB#156:                               # %.lr.ph578.preheader
                                        #   in Loop: Header=BB25_118 Depth=1
	leaq	-8(,%r14,8), %rbx
	.p2align	4, 0x90
.LBB25_157:                             # %.lr.ph578
                                        #   Parent Loop BB25_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rax
	movq	(%rax,%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB25_168
# BB#158:                               #   in Loop: Header=BB25_157 Depth=2
	cmpl	$0, 28(%rbp)
	jns	.LBB25_116
# BB#159:                               #   in Loop: Header=BB25_157 Depth=2
	movslq	(%rbp), %rax
	movslq	4(%rbp), %rcx
	movslq	8(%rbp), %rdx
	shlq	$4, %rcx
	movss	(%r15,%rcx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	shlq	$4, %rax
	movaps	%xmm1, %xmm6
	subss	(%r15,%rax), %xmm6
	movsd	4(%r15,%rcx), %xmm2     # xmm2 = mem[0],zero
	movsd	4(%r15,%rax), %xmm3     # xmm3 = mem[0],zero
	shlq	$4, %rdx
	movss	4(%r15,%rdx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	pshufd	$229, %xmm2, %xmm4      # xmm4 = xmm2[1,1,2,3]
	subps	%xmm3, %xmm2
	movss	(%r15,%rdx), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movss	8(%r15,%rdx), %xmm5     # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	shufps	$0, %xmm4, %xmm1        # xmm1 = xmm1[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm1      # xmm1 = xmm1[2,0],xmm4[2,3]
	subps	%xmm1, %xmm5
	movaps	%xmm2, %xmm3
	mulps	%xmm5, %xmm3
	movaps	%xmm6, %xmm1
	mulss	%xmm0, %xmm6
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm2, %xmm5
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	shufps	$0, %xmm2, %xmm1        # xmm1 = xmm1[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm1      # xmm1 = xmm1[2,0],xmm2[2,3]
	mulps	%xmm1, %xmm0
	subps	%xmm0, %xmm3
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm5, %xmm6
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB25_161
# BB#160:                               # %call.sqrt976
                                        #   in Loop: Header=BB25_157 Depth=2
	movaps	%xmm1, %xmm0
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	movaps	%xmm3, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm3         # 16-byte Reload
	movaps	16(%rsp), %xmm6         # 16-byte Reload
.LBB25_161:                             # %.split975
                                        #   in Loop: Header=BB25_157 Depth=2
	ucomiss	.LCPI25_5, %xmm0
	jne	.LBB25_163
	jp	.LBB25_163
# BB#162:                               #   in Loop: Header=BB25_157 Depth=2
	xorps	%xmm1, %xmm1
	movss	.LCPI25_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB25_164
	.p2align	4, 0x90
.LBB25_163:                             #   in Loop: Header=BB25_157 Depth=2
	movss	.LCPI25_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	mulss	%xmm6, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
.LBB25_164:                             #   in Loop: Header=BB25_157 Depth=2
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movupd	%xmm0, 184(%rsp)
.Ltmp64:
	movq	%r15, %rdi
	movl	%r12d, %esi
	leaq	184(%rsp), %rdx
	leaq	64(%rsp), %rcx
	callq	_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE
.Ltmp65:
# BB#165:                               #   in Loop: Header=BB25_157 Depth=2
	movl	%eax, 28(%rbp)
	cltq
	movq	56(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, (%rcx,%rax,4)
	movq	136(%rsp), %rdi         # 8-byte Reload
	je	.LBB25_167
# BB#166:                               #   in Loop: Header=BB25_157 Depth=2
	movl	$-1, 28(%rbp)
	jmp	.LBB25_168
	.p2align	4, 0x90
.LBB25_167:                             #   in Loop: Header=BB25_157 Depth=2
	movslq	(%rbp), %rcx
	shlq	$4, %rax
	shlq	$4, %rcx
	movss	(%r15,%rax), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%r15,%rax), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	(%r15,%rcx), %xmm0
	subss	4(%r15,%rcx), %xmm1
	movss	8(%r15,%rax), %xmm2     # xmm2 = mem[0],zero,zero,zero
	subss	8(%r15,%rcx), %xmm2
	mulss	184(%rsp), %xmm0
	mulss	188(%rsp), %xmm1
	addss	%xmm0, %xmm1
	mulss	192(%rsp), %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, 32(%rbp)
.LBB25_168:                             # %.critedge185.backedge
                                        #   in Loop: Header=BB25_157 Depth=2
	addq	$-8, %rbx
	decl	%r14d
	jne	.LBB25_157
	jmp	.LBB25_116
.LBB25_169:
	movl	$0, 152(%rsp)           # 4-byte Folded Spill
	jmp	.LBB25_171
.LBB25_170:
	movl	$1, 152(%rsp)           # 4-byte Folded Spill
.LBB25_171:                             # %.critedge
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	movq	56(%rsp), %rbx          # 8-byte Reload
	je	.LBB25_175
# BB#172:
	cmpb	$0, 88(%rsp)
	je	.LBB25_174
# BB#173:
.Ltmp69:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp70:
.LBB25_174:                             # %.noexc237
	movq	$0, 80(%rsp)
.LBB25_175:
	testq	%rbx, %rbx
	je	.LBB25_177
# BB#176:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.LBB25_177:                             # %_ZN20btAlignedObjectArrayIiED2Ev.exit235
	movl	152(%rsp), %ecx         # 4-byte Reload
.LBB25_178:                             # %_ZN20btAlignedObjectArrayIiED2Ev.exit235
	movl	%ecx, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_13:                              # %vector.body.preheader
	movl	%ebx, %r8d
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB25_16
# BB#14:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB25_15:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%rbp,%rbx,4)
	movups	%xmm1, 16(%rbp,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB25_15
	jmp	.LBB25_17
.LBB25_16:
	xorl	%ebx, %ebx
.LBB25_17:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB25_20
# BB#18:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%rdi,%rbx,4), %rsi
	leaq	112(%rbp,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB25_19:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB25_19
.LBB25_20:                              # %middle.block
	cmpq	%rcx, %rax
	movl	%r8d, %ebx
	je	.LBB25_27
	jmp	.LBB25_21
.LBB25_179:
.Ltmp71:
	movq	%rax, %rbx
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)            # 8-byte Spill
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB25_201
	jmp	.LBB25_202
.LBB25_180:
.Ltmp54:
	jmp	.LBB25_194
.LBB25_181:
.Ltmp51:
	jmp	.LBB25_194
.LBB25_182:
.Ltmp48:
	jmp	.LBB25_194
.LBB25_183:
.Ltmp45:
	jmp	.LBB25_194
.LBB25_184:
.Ltmp42:
	jmp	.LBB25_188
.LBB25_185:                             # %.thread537
.Ltmp24:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB25_186:
.Ltmp29:
	movq	%rax, %rbx
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jmp	.LBB25_196
.LBB25_187:
.Ltmp34:
	movq	%rbx, 56(%rsp)          # 8-byte Spill
.LBB25_188:
	movq	%rax, %rbx
	movq	%rbp, (%rsp)            # 8-byte Spill
	jmp	.LBB25_196
.LBB25_189:
.Ltmp39:
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	%rax, %rbx
	movq	128(%rsp), %rax         # 8-byte Reload
	jmp	.LBB25_195
.LBB25_190:
.Ltmp57:
	jmp	.LBB25_194
.LBB25_191:
.Ltmp66:
	jmp	.LBB25_194
.LBB25_192:
.Ltmp63:
	jmp	.LBB25_194
.LBB25_193:
.Ltmp60:
.LBB25_194:
	movq	%rax, %rbx
	movq	40(%rsp), %rax          # 8-byte Reload
.LBB25_195:
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB25_196:
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB25_200
# BB#197:
	cmpb	$0, 88(%rsp)
	je	.LBB25_199
# BB#198:
.Ltmp67:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp68:
.LBB25_199:                             # %.noexc226
	movq	$0, 80(%rsp)
.LBB25_200:                             # %_ZN20btAlignedObjectArrayIiED2Ev.exit227
	movb	$1, 88(%rsp)
	movq	$0, 80(%rsp)
	movq	$0, 68(%rsp)
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB25_202
.LBB25_201:
.Ltmp72:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.Ltmp73:
.LBB25_202:                             # %_ZN20btAlignedObjectArrayIiED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB25_203:
.Ltmp74:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZN11HullLibrary11calchullgenEP9btVector3ii, .Lfunc_end25-_ZN11HullLibrary11calchullgenEP9btVector3ii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\346\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\335\001"              # Call site table length
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp28-.Ltmp25         #   Call between .Ltmp25 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin1   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp33-.Ltmp30         #   Call between .Ltmp30 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp38-.Ltmp35         #   Call between .Ltmp35 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin1   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin1   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin1   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin1   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin1   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin1   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin1   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin1   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin1   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin1   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin1   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin1   # >> Call Site 15 <<
	.long	.Ltmp67-.Ltmp70         #   Call between .Ltmp70 and .Ltmp67
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin1   # >> Call Site 16 <<
	.long	.Ltmp73-.Ltmp67         #   Call between .Ltmp67 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin1   #     jumps to .Ltmp74
	.byte	1                       #   On action: 1
	.long	.Ltmp73-.Lfunc_begin1   # >> Call Site 17 <<
	.long	.Lfunc_end25-.Ltmp73    #   Call between .Ltmp73 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN11HullLibrary8calchullEP9btVector3iR20btAlignedObjectArrayIjERii
	.p2align	4, 0x90
	.type	_ZN11HullLibrary8calchullEP9btVector3iR20btAlignedObjectArrayIjERii,@function
_ZN11HullLibrary8calchullEP9btVector3iR20btAlignedObjectArrayIjERii: # @_ZN11HullLibrary8calchullEP9btVector3iR20btAlignedObjectArrayIjERii
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi126:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi128:
	.cfi_def_cfa_offset 144
.Lcfi129:
	.cfi_offset %rbx, -56
.Lcfi130:
	.cfi_offset %r12, -48
.Lcfi131:
	.cfi_offset %r13, -40
.Lcfi132:
	.cfi_offset %r14, -32
.Lcfi133:
	.cfi_offset %r15, -24
.Lcfi134:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %rbp
	movq	%rdi, %rbx
	movl	%r9d, %ecx
	callq	_ZN11HullLibrary11calchullgenEP9btVector3ii
	testl	%eax, %eax
	je	.LBB26_3
# BB#1:                                 # %.preheader102
	movq	%r14, 72(%rsp)          # 8-byte Spill
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movl	4(%rbx), %eax
	testl	%eax, %eax
	movq	%rbx, %r10
	movq	%r10, 40(%rsp)          # 8-byte Spill
	jle	.LBB26_100
# BB#2:                                 # %.lr.ph140
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	xorl	%r9d, %r9d
	jmp	.LBB26_18
.LBB26_3:
	xorl	%ebp, %ebp
	jmp	.LBB26_167
.LBB26_4:                               #   in Loop: Header=BB26_18 Depth=1
	movq	%r8, %r9
	movq	%r8, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r8, %rbx
	movq	24(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB26_72
.LBB26_36:                              # %vector.body208.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB26_39
# BB#37:                                # %vector.body208.prol.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB26_38:                              # %vector.body208.prol
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r13,%rsi,4), %xmm0
	movups	16(%r13,%rsi,4), %xmm1
	movups	%xmm0, (%r8,%rsi,4)
	movups	%xmm1, 16(%r8,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB26_38
	jmp	.LBB26_40
.LBB26_5:                               # %vector.body177.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB26_8
# BB#6:                                 # %vector.body177.prol.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB26_7:                               # %vector.body177.prol
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rsi,4), %xmm0
	movups	16(%rbx,%rsi,4), %xmm1
	movups	%xmm0, (%r8,%rsi,4)
	movups	%xmm1, 16(%r8,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB26_7
	jmp	.LBB26_9
.LBB26_39:                              #   in Loop: Header=BB26_18 Depth=1
	xorl	%esi, %esi
.LBB26_40:                              # %vector.body208.prol.loopexit
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	$24, %rcx
	jb	.LBB26_43
# BB#41:                                # %vector.body208.preheader.new
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r13,%rsi,4), %rdx
	leaq	112(%r8,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB26_42:                              # %vector.body208
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB26_42
.LBB26_43:                              # %middle.block209
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	%rax, %r11
	je	.LBB26_50
	jmp	.LBB26_44
.LBB26_8:                               #   in Loop: Header=BB26_18 Depth=1
	xorl	%esi, %esi
.LBB26_9:                               # %vector.body177.prol.loopexit
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	$24, %rcx
	jb	.LBB26_12
# BB#10:                                # %vector.body177.preheader.new
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbx,%rsi,4), %rdx
	leaq	112(%r8,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB26_11:                              # %vector.body177
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB26_11
.LBB26_12:                              # %middle.block178
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	%rax, %rbp
	jne	.LBB26_64
	jmp	.LBB26_71
.LBB26_13:                              #   in Loop: Header=BB26_18 Depth=1
	xorl	%esi, %esi
.LBB26_14:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	$24, %rcx
	jb	.LBB26_17
# BB#15:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbx,%rsi,4), %rdx
	leaq	112(%r8,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB26_16:                              # %vector.body
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB26_16
.LBB26_17:                              # %middle.block
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	%rax, %r12
	jne	.LBB26_90
	jmp	.LBB26_96
	.p2align	4, 0x90
.LBB26_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_38 Depth 2
                                        #     Child Loop BB26_42 Depth 2
                                        #     Child Loop BB26_46 Depth 2
                                        #     Child Loop BB26_49 Depth 2
                                        #     Child Loop BB26_7 Depth 2
                                        #     Child Loop BB26_11 Depth 2
                                        #     Child Loop BB26_66 Depth 2
                                        #     Child Loop BB26_69 Depth 2
                                        #     Child Loop BB26_85 Depth 2
                                        #     Child Loop BB26_16 Depth 2
                                        #     Child Loop BB26_92 Depth 2
                                        #     Child Loop BB26_95 Depth 2
	movq	16(%r10), %rcx
	movq	(%rcx,%rsi,8), %r14
	testq	%r14, %r14
	je	.LBB26_25
# BB#19:                                # %.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	movslq	%r9d, %r11
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, %r11d
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	jne	.LBB26_24
# BB#20:                                #   in Loop: Header=BB26_18 Depth=1
	leal	(%rcx,%rcx), %r12d
	testl	%ecx, %ecx
	movl	$1, %eax
	cmovel	%eax, %r12d
	cmpl	%r12d, %r9d
	jge	.LBB26_24
# BB#21:                                #   in Loop: Header=BB26_18 Depth=1
	testl	%r12d, %r12d
	je	.LBB26_26
# BB#22:                                #   in Loop: Header=BB26_18 Depth=1
	movslq	%r12d, %rdi
	shlq	$2, %rdi
.Ltmp75:
	movl	$16, %esi
	movq	%r11, %rbx
	callq	_Z22btAlignedAllocInternalmi
	movq	%rbx, %r11
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%rax, %r8
.Ltmp76:
# BB#23:                                #   in Loop: Header=BB26_18 Depth=1
	movq	48(%rsp), %r9           # 8-byte Reload
	testl	%r9d, %r9d
	jg	.LBB26_27
	jmp	.LBB26_29
	.p2align	4, 0x90
.LBB26_24:                              #   in Loop: Header=BB26_18 Depth=1
	movl	%ecx, %r12d
	movq	%r15, %r9
	movq	%r13, %rbx
	jmp	.LBB26_51
	.p2align	4, 0x90
.LBB26_25:                              #   in Loop: Header=BB26_18 Depth=1
	movq	%r15, %rdi
	movq	%r13, %r14
	jmp	.LBB26_99
.LBB26_26:                              #   in Loop: Header=BB26_18 Depth=1
	xorl	%r8d, %r8d
	testl	%r9d, %r9d
	jle	.LBB26_29
.LBB26_27:                              # %.lr.ph.i.i.i53.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpl	$8, %r9d
	jae	.LBB26_31
# BB#28:                                #   in Loop: Header=BB26_18 Depth=1
	xorl	%eax, %eax
	jmp	.LBB26_44
.LBB26_29:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
                                        #   in Loop: Header=BB26_18 Depth=1
	testq	%r13, %r13
	jne	.LBB26_50
# BB#30:                                #   in Loop: Header=BB26_18 Depth=1
	movq	%r8, %r9
	movq	%r8, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r8, %rbx
	jmp	.LBB26_51
.LBB26_31:                              # %min.iters.checked212
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%r11, %rax
	andq	$-8, %rax
	je	.LBB26_35
# BB#32:                                # %vector.memcheck225
                                        #   in Loop: Header=BB26_18 Depth=1
	leaq	(%r13,%r11,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB26_36
# BB#33:                                # %vector.memcheck225
                                        #   in Loop: Header=BB26_18 Depth=1
	leaq	(%r8,%r11,4), %rcx
	cmpq	%rcx, 8(%rsp)           # 8-byte Folded Reload
	jae	.LBB26_36
.LBB26_35:                              #   in Loop: Header=BB26_18 Depth=1
	xorl	%eax, %eax
.LBB26_44:                              # %.lr.ph.i.i.i53.preheader297
                                        #   in Loop: Header=BB26_18 Depth=1
	movl	%r9d, %edx
	subl	%eax, %edx
	leaq	-1(%r11), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB26_47
# BB#45:                                # %.lr.ph.i.i.i53.prol.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB26_46:                              # %.lr.ph.i.i.i53.prol
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rax,4), %esi
	movl	%esi, (%r8,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB26_46
.LBB26_47:                              # %.lr.ph.i.i.i53.prol.loopexit
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	$7, %rcx
	jb	.LBB26_50
# BB#48:                                # %.lr.ph.i.i.i53.preheader297.new
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%r11, %rcx
	subq	%rax, %rcx
	leaq	28(%r13,%rax,4), %rdx
	leaq	28(%r8,%rax,4), %rax
	.p2align	4, 0x90
.LBB26_49:                              # %.lr.ph.i.i.i53
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rax)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rax)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rax)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rax)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rax)
	movl	(%rdx), %esi
	movl	%esi, (%rax)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB26_49
.LBB26_50:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
                                        #   in Loop: Header=BB26_18 Depth=1
.Ltmp77:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r8, %rbx
	movq	%r11, %rbp
	callq	_Z21btAlignedFreeInternalPv
	movq	%rbp, %r11
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %r8
	movq	40(%rsp), %r10          # 8-byte Reload
.Ltmp78:
	movq	%r8, %r9
	movq	%r8, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
.LBB26_51:                              #   in Loop: Header=BB26_18 Depth=1
	movl	(%r14), %eax
	movl	%eax, (%rbx,%r11,4)
	leaq	1(%r11), %rbp
	movq	16(%r10), %rax
	movq	(%rax,%rsi,8), %rcx
	cmpl	%r12d, %ebp
	jne	.LBB26_56
# BB#52:                                #   in Loop: Header=BB26_18 Depth=1
	leal	(%r12,%r12), %r14d
	testl	%r12d, %r12d
	movl	$1, %eax
	cmovel	%eax, %r14d
	movslq	%r14d, %rdi
	cmpq	%rdi, %rbp
	jge	.LBB26_56
# BB#53:                                #   in Loop: Header=BB26_18 Depth=1
	testl	%r14d, %r14d
	je	.LBB26_57
# BB#54:                                #   in Loop: Header=BB26_18 Depth=1
	movq	%r9, %r15
	movq	%rbx, %r13
	shlq	$2, %rdi
.Ltmp79:
	movl	$16, %esi
	movq	%r11, 8(%rsp)           # 8-byte Spill
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r9, %rbp
	movq	%rcx, %r12
	callq	_Z22btAlignedAllocInternalmi
	movq	%r12, %rcx
	movq	%rbp, %r9
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%rax, %r8
.Ltmp80:
	jmp	.LBB26_58
	.p2align	4, 0x90
.LBB26_56:                              #   in Loop: Header=BB26_18 Depth=1
	movl	%r12d, %r14d
	jmp	.LBB26_72
.LBB26_57:                              #   in Loop: Header=BB26_18 Depth=1
	xorl	%r8d, %r8d
.LBB26_58:                              # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i.1
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	testl	%edi, %edi
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	js	.LBB26_70
# BB#59:                                # %.lr.ph.i.i.i53.1.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	$7, %rbp
	jbe	.LBB26_63
# BB#60:                                # %min.iters.checked181
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%rbp, %rax
	andq	$-8, %rax
	je	.LBB26_63
# BB#61:                                # %vector.memcheck196
                                        #   in Loop: Header=BB26_18 Depth=1
	leaq	4(%rbx,%r11,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB26_5
# BB#62:                                # %vector.memcheck196
                                        #   in Loop: Header=BB26_18 Depth=1
	leaq	4(%r8,%r11,4), %rcx
	cmpq	%rcx, 64(%rsp)          # 8-byte Folded Reload
	jae	.LBB26_5
.LBB26_63:                              #   in Loop: Header=BB26_18 Depth=1
	xorl	%eax, %eax
.LBB26_64:                              # %.lr.ph.i.i.i53.1.preheader296
                                        #   in Loop: Header=BB26_18 Depth=1
	leal	1(%rdi), %edx
	subl	%eax, %edx
	movq	%r11, %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB26_67
# BB#65:                                # %.lr.ph.i.i.i53.1.prol.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB26_66:                              # %.lr.ph.i.i.i53.1.prol
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rax,4), %esi
	movl	%esi, (%r8,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB26_66
.LBB26_67:                              # %.lr.ph.i.i.i53.1.prol.loopexit
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	$7, %rcx
	jb	.LBB26_71
# BB#68:                                # %.lr.ph.i.i.i53.1.preheader296.new
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%rbp, %rcx
	subq	%rax, %rcx
	leaq	28(%rbx,%rax,4), %rdx
	leaq	28(%r8,%rax,4), %rax
	.p2align	4, 0x90
.LBB26_69:                              # %.lr.ph.i.i.i53.1
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rax)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rax)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rax)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rax)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rax)
	movl	(%rdx), %esi
	movl	%esi, (%rax)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB26_69
	jmp	.LBB26_71
.LBB26_70:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i.1
                                        #   in Loop: Header=BB26_18 Depth=1
	testq	%rbx, %rbx
	je	.LBB26_4
.LBB26_71:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i.1
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%r9, %r15
	movq	%rbx, %r13
.Ltmp81:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r8, %rbx
	movq	%rbp, %r12
	movq	%r11, %rbp
	callq	_Z21btAlignedFreeInternalPv
	movq	%rbp, %r11
	movq	%r12, %rbp
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %r8
	movq	40(%rsp), %r10          # 8-byte Reload
.Ltmp82:
	movq	%r8, %r9
	movq	%r8, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
.LBB26_72:                              #   in Loop: Header=BB26_18 Depth=1
	movl	4(%rcx), %eax
	movl	%eax, 4(%rbx,%r11,4)
	leaq	2(%r11), %r12
	movq	16(%r10), %rax
	movq	(%rax,%rsi,8), %rdx
	cmpl	%r14d, %r12d
	jne	.LBB26_76
# BB#73:                                #   in Loop: Header=BB26_18 Depth=1
	leal	(%r14,%r14), %ecx
	testl	%r14d, %r14d
	movl	$1, %eax
	cmovel	%eax, %ecx
	movslq	%ecx, %rdi
	cmpq	%rdi, %r12
	jge	.LBB26_76
# BB#74:                                #   in Loop: Header=BB26_18 Depth=1
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	je	.LBB26_77
# BB#75:                                #   in Loop: Header=BB26_18 Depth=1
	movq	%r9, %r15
	movq	%rbx, %r13
	shlq	$2, %rdi
.Ltmp83:
	movl	$16, %esi
	movq	%r11, 8(%rsp)           # 8-byte Spill
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r9, %rbp
	movq	%rdx, %r14
	callq	_Z22btAlignedAllocInternalmi
	movq	%r14, %rdx
	movq	%rbp, %r9
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%rax, %r8
.Ltmp84:
	jmp	.LBB26_78
	.p2align	4, 0x90
.LBB26_76:                              #   in Loop: Header=BB26_18 Depth=1
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	movq	%r14, 24(%rsp)          # 8-byte Spill
	jmp	.LBB26_97
.LBB26_77:                              #   in Loop: Header=BB26_18 Depth=1
	xorl	%r8d, %r8d
.LBB26_78:                              # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i.2
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	cmpl	$-1, %edi
	jl	.LBB26_86
# BB#79:                                # %.lr.ph.i.i.i53.2.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	$7, %r12
	jbe	.LBB26_88
# BB#80:                                # %min.iters.checked
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%r12, %rax
	andq	$-8, %rax
	je	.LBB26_88
# BB#81:                                # %vector.memcheck
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%rdx, %r14
	leaq	8(%rbx,%r11,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB26_83
# BB#82:                                # %vector.memcheck
                                        #   in Loop: Header=BB26_18 Depth=1
	leaq	8(%r8,%r11,4), %rcx
	cmpq	%rcx, 56(%rsp)          # 8-byte Folded Reload
	jb	.LBB26_89
.LBB26_83:                              # %vector.body.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB26_13
# BB#84:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB26_85:                              # %vector.body.prol
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rsi,4), %xmm0
	movups	16(%rbx,%rsi,4), %xmm1
	movups	%xmm0, (%r8,%rsi,4)
	movups	%xmm1, 16(%r8,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB26_85
	jmp	.LBB26_14
.LBB26_86:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i.2
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%rdx, %r14
	testq	%rbx, %rbx
	jne	.LBB26_96
# BB#87:                                #   in Loop: Header=BB26_18 Depth=1
	movq	%r8, %r9
	movq	%r8, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r8, %rbx
	movq	%r14, %rdx
	jmp	.LBB26_97
.LBB26_88:                              #   in Loop: Header=BB26_18 Depth=1
	movq	%rdx, %r14
.LBB26_89:                              # %.lr.ph.i.i.i53.2.preheader295
                                        #   in Loop: Header=BB26_18 Depth=1
	xorl	%eax, %eax
.LBB26_90:                              # %.lr.ph.i.i.i53.2.preheader295
                                        #   in Loop: Header=BB26_18 Depth=1
	leal	2(%rdi), %ecx
	subl	%eax, %ecx
	subq	%rax, %rbp
	andq	$7, %rcx
	je	.LBB26_93
# BB#91:                                # %.lr.ph.i.i.i53.2.prol.preheader
                                        #   in Loop: Header=BB26_18 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB26_92:                              # %.lr.ph.i.i.i53.2.prol
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rax,4), %edx
	movl	%edx, (%r8,%rax,4)
	incq	%rax
	incq	%rcx
	jne	.LBB26_92
.LBB26_93:                              # %.lr.ph.i.i.i53.2.prol.loopexit
                                        #   in Loop: Header=BB26_18 Depth=1
	cmpq	$7, %rbp
	jb	.LBB26_96
# BB#94:                                # %.lr.ph.i.i.i53.2.preheader295.new
                                        #   in Loop: Header=BB26_18 Depth=1
	subq	%rax, %r12
	leaq	28(%rbx,%rax,4), %rcx
	leaq	28(%r8,%rax,4), %rax
	.p2align	4, 0x90
.LBB26_95:                              # %.lr.ph.i.i.i53.2
                                        #   Parent Loop BB26_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %edx
	movl	%edx, -28(%rax)
	movl	-24(%rcx), %edx
	movl	%edx, -24(%rax)
	movl	-20(%rcx), %edx
	movl	%edx, -20(%rax)
	movl	-16(%rcx), %edx
	movl	%edx, -16(%rax)
	movl	-12(%rcx), %edx
	movl	%edx, -12(%rax)
	movl	-8(%rcx), %edx
	movl	%edx, -8(%rax)
	movl	-4(%rcx), %edx
	movl	%edx, -4(%rax)
	movl	(%rcx), %edx
	movl	%edx, (%rax)
	addq	$32, %rcx
	addq	$32, %rax
	addq	$-8, %r12
	jne	.LBB26_95
.LBB26_96:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i.2
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	%r9, %r15
	movq	%rbx, %r13
.Ltmp85:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r8, %rbx
	movq	%r11, %rbp
	callq	_Z21btAlignedFreeInternalPv
	movq	%rbp, %r11
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %r8
	movq	40(%rsp), %r10          # 8-byte Reload
.Ltmp86:
	movq	%r8, %r9
	movq	%r8, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r14, %rdx
.LBB26_97:                              #   in Loop: Header=BB26_18 Depth=1
	movq	%r9, %r15
	movq	%r8, %rbp
	movl	8(%rdx), %eax
	movq	%rbx, %r14
	movl	%eax, 8(%rbx,%r11,4)
	movq	16(%r10), %rax
	movq	(%rax,%rsi,8), %rdi
	movslq	24(%rdi), %rcx
	movq	$0, (%rax,%rcx,8)
.Ltmp88:
	movq	%r10, %rbx
	callq	_Z21btAlignedFreeInternalPv
.Ltmp89:
# BB#98:                                # %._ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle.exit_crit_edge
                                        #   in Loop: Header=BB26_18 Depth=1
	movq	48(%rsp), %r9           # 8-byte Reload
	addl	$3, %r9d
	movl	4(%rbx), %eax
	movq	%rbx, %r10
	movq	%rbp, %r8
	movq	%r15, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
.LBB26_99:                              # %_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle.exit
                                        #   in Loop: Header=BB26_18 Depth=1
	incq	%rsi
	movslq	%eax, %rcx
	cmpq	%rcx, %rsi
	movq	%r14, %r13
	movq	%rdi, %r15
	jl	.LBB26_18
	jmp	.LBB26_101
.LBB26_100:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r14d, %r14d
.LBB26_101:                             # %._crit_edge141
	movslq	%r9d, %rdx
	imulq	$1431655766, %rdx, %rax # imm = 0x55555556
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movq	80(%rsp), %r13          # 8-byte Reload
	movl	4(%r13), %eax
	cmpl	%edx, %eax
	movq	%rdi, %r15
	jge	.LBB26_136
# BB#102:
	cmpl	%r9d, 8(%r13)
	movq	%r9, 48(%rsp)           # 8-byte Spill
	jge	.LBB26_106
# BB#103:
	testl	%r9d, %r9d
	movl	%eax, 32(%rsp)          # 4-byte Spill
	je	.LBB26_107
# BB#104:
	movq	%r8, %rbx
	leaq	(,%rdx,4), %rdi
.Ltmp91:
	movl	$16, %esi
	movq	%rdx, %rbp
	callq	_Z22btAlignedAllocInternalmi
	movq	%rbp, %rdx
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	%rax, %r12
.Ltmp92:
# BB#105:                               # %.noexc48
	movl	4(%r13), %ecx
	movq	%rbx, %r8
	jmp	.LBB26_108
.LBB26_106:                             # %..lr.ph.i44_crit_edge
	movq	%r8, %rbp
	movq	16(%r13), %r12
	jmp	.LBB26_135
.LBB26_107:
	xorl	%r12d, %r12d
	movl	%eax, %ecx
.LBB26_108:                             # %_ZN20btAlignedObjectArrayIjE8allocateEi.exit.i.i
	movq	16(%r13), %rdi
	testl	%ecx, %ecx
	jle	.LBB26_111
# BB#109:                               # %.lr.ph.i.i.i39
	movslq	%ecx, %r11
	cmpl	$8, %ecx
	jae	.LBB26_113
# BB#110:
	xorl	%eax, %eax
	jmp	.LBB26_126
.LBB26_111:                             # %_ZNK20btAlignedObjectArrayIjE4copyEiiPj.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB26_132
# BB#112:                               # %_ZN20btAlignedObjectArrayIjE7reserveEi.exit.preheader.thread26.i
	movq	%r8, %rbp
	leaq	24(%r13), %rbx
	jmp	.LBB26_134
.LBB26_113:                             # %min.iters.checked241
	movq	%r11, %rax
	andq	$-8, %rax
	je	.LBB26_117
# BB#114:                               # %vector.memcheck253
	leaq	(%rdi,%r11,4), %rsi
	cmpq	%rsi, %r12
	jae	.LBB26_118
# BB#115:                               # %vector.memcheck253
	leaq	(%r12,%r11,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB26_118
.LBB26_117:
	xorl	%eax, %eax
.LBB26_126:                             # %scalar.ph239.preheader
	subl	%eax, %ecx
	leaq	-1(%r11), %rsi
	subq	%rax, %rsi
	andq	$7, %rcx
	je	.LBB26_129
# BB#127:                               # %scalar.ph239.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB26_128:                             # %scalar.ph239.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax,4), %ebp
	movl	%ebp, (%r12,%rax,4)
	incq	%rax
	incq	%rcx
	jne	.LBB26_128
.LBB26_129:                             # %scalar.ph239.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB26_132
# BB#130:                               # %scalar.ph239.preheader.new
	subq	%rax, %r11
	leaq	28(%rdi,%rax,4), %rcx
	leaq	28(%r12,%rax,4), %rax
	.p2align	4, 0x90
.LBB26_131:                             # %scalar.ph239
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rax)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rax)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rax)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rax)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rax)
	movl	(%rcx), %esi
	movl	%esi, (%rax)
	addq	$32, %rcx
	addq	$32, %rax
	addq	$-8, %r11
	jne	.LBB26_131
.LBB26_132:                             # %_ZNK20btAlignedObjectArrayIjE4copyEiiPj.exit.thread.i.i
	movq	%r8, %rbp
	leaq	24(%r13), %rbx
	cmpb	$0, 24(%r13)
	je	.LBB26_134
# BB#133:
.Ltmp93:
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
.Ltmp94:
.LBB26_134:                             # %.lr.ph.i44.sink.split
	movb	$1, (%rbx)
	movq	%r12, 16(%r13)
	movl	%r9d, 8(%r13)
	movl	32(%rsp), %eax          # 4-byte Reload
.LBB26_135:                             # %.lr.ph.i44
	cltq
	leaq	(%r12,%rax,4), %rdi
	subq	%rax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%rbp, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
.LBB26_136:                             # %.loopexit101
	movl	%r9d, 4(%r13)
	testl	%r9d, %r9d
	jle	.LBB26_153
# BB#137:                               # %.lr.ph
	movq	16(%r13), %rax
	movl	%r9d, %ecx
	cmpl	$8, %r9d
	jae	.LBB26_139
# BB#138:
	xorl	%edx, %edx
	jmp	.LBB26_147
.LBB26_139:                             # %min.iters.checked269
	andl	$7, %r9d
	movq	%rcx, %rdx
	subq	%r9, %rdx
	je	.LBB26_143
# BB#140:                               # %vector.memcheck283
	leaq	(%r14,%rcx,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB26_144
# BB#141:                               # %vector.memcheck283
	leaq	(%rax,%rcx,4), %rsi
	cmpq	%rsi, (%rsp)            # 8-byte Folded Reload
	jae	.LBB26_144
.LBB26_143:
	xorl	%edx, %edx
	jmp	.LBB26_147
.LBB26_144:                             # %vector.body265.preheader
	leaq	16(%r14), %rsi
	leaq	16(%rax), %rdi
	movq	%rdx, %rbp
	.p2align	4, 0x90
.LBB26_145:                             # %vector.body265
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rbp
	jne	.LBB26_145
# BB#146:                               # %middle.block266
	testl	%r9d, %r9d
	je	.LBB26_153
.LBB26_147:                             # %scalar.ph267.preheader
	movl	%ecx, %edi
	subl	%edx, %edi
	leaq	-1(%rcx), %rsi
	subq	%rdx, %rsi
	andq	$7, %rdi
	je	.LBB26_150
# BB#148:                               # %scalar.ph267.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB26_149:                             # %scalar.ph267.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rdx,4), %ebp
	movl	%ebp, (%rax,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB26_149
.LBB26_150:                             # %scalar.ph267.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB26_153
# BB#151:                               # %scalar.ph267.preheader.new
	subq	%rdx, %rcx
	leaq	28(%rax,%rdx,4), %rsi
	leaq	28(%r14,%rdx,4), %rdi
	.p2align	4, 0x90
.LBB26_152:                             # %scalar.ph267
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdi), %eax
	movl	%eax, -28(%rsi)
	movl	-24(%rdi), %eax
	movl	%eax, -24(%rsi)
	movl	-20(%rdi), %eax
	movl	%eax, -20(%rsi)
	movl	-16(%rdi), %eax
	movl	%eax, -16(%rsi)
	movl	-12(%rdi), %eax
	movl	%eax, -12(%rsi)
	movl	-8(%rdi), %eax
	movl	%eax, -8(%rsi)
	movl	-4(%rdi), %eax
	movl	%eax, -4(%rsi)
	movl	(%rdi), %eax
	movl	%eax, (%rsi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rcx
	jne	.LBB26_152
.LBB26_153:                             # %._crit_edge
	movslq	4(%r10), %rbp
	testq	%rbp, %rbp
	jns	.LBB26_165
# BB#154:
	cmpl	$0, 8(%r10)
	jns	.LBB26_160
# BB#155:                               # %_ZNK20btAlignedObjectArrayIP14btHullTriangleE4copyEiiPS1_.exit.i.i
	movq	16(%r10), %rdi
	testq	%rdi, %rdi
	je	.LBB26_159
# BB#156:
	cmpb	$0, 24(%r10)
	je	.LBB26_158
# BB#157:
.Ltmp96:
	movq	%r8, %rbx
	callq	_Z21btAlignedFreeInternalPv
	movq	%rbx, %r8
	movq	40(%rsp), %r10          # 8-byte Reload
.Ltmp97:
.LBB26_158:                             # %.noexc36
	movq	$0, 16(%r10)
.LBB26_159:                             # %_ZN20btAlignedObjectArrayIP14btHullTriangleE7reserveEi.exit.preheader.i
	movb	$1, 24(%r10)
	movq	$0, 16(%r10)
	movl	$0, 8(%r10)
.LBB26_160:                             # %.lr.ph.i
	movl	%ebp, %ecx
	negl	%ecx
	andq	$7, %rcx
	movq	%rbp, %rax
	je	.LBB26_163
# BB#161:                               # %_ZN20btAlignedObjectArrayIP14btHullTriangleE7reserveEi.exit.i.prol.preheader
	negq	%rcx
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB26_162:                             # %_ZN20btAlignedObjectArrayIP14btHullTriangleE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r10), %rdx
	movq	$0, (%rdx,%rax,8)
	incq	%rax
	incq	%rcx
	jne	.LBB26_162
.LBB26_163:                             # %_ZN20btAlignedObjectArrayIP14btHullTriangleE7reserveEi.exit.i.prol.loopexit
	cmpl	$-8, %ebp
	ja	.LBB26_165
	.p2align	4, 0x90
.LBB26_164:                             # %_ZN20btAlignedObjectArrayIP14btHullTriangleE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r10), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	16(%r10), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	16(%r10), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	16(%r10), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	16(%r10), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	16(%r10), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	16(%r10), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	16(%r10), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	jne	.LBB26_164
.LBB26_165:                             # %.loopexit
	movl	$0, 4(%r10)
	movl	$1, %ebp
	testq	%r14, %r14
	je	.LBB26_167
# BB#166:
	movq	%r8, %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB26_167:                             # %_ZN20btAlignedObjectArrayIiED2Ev.exit35
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB26_118:                             # %vector.body237.preheader
	leaq	-8(%rax), %r10
	movl	%r10d, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB26_121
# BB#119:                               # %vector.body237.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB26_120:                             # %vector.body237.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r12,%rbp,4)
	movups	%xmm1, 16(%r12,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB26_120
	jmp	.LBB26_122
.LBB26_121:
	xorl	%ebp, %ebp
.LBB26_122:                             # %vector.body237.prol.loopexit
	cmpq	$24, %r10
	jb	.LBB26_125
# BB#123:                               # %vector.body237.preheader.new
	movq	%rax, %r10
	subq	%rbp, %r10
	leaq	112(%rdi,%rbp,4), %rsi
	leaq	112(%r12,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB26_124:                             # %vector.body237
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-32, %r10
	jne	.LBB26_124
.LBB26_125:                             # %middle.block238
	cmpq	%rax, %r11
	je	.LBB26_132
	jmp	.LBB26_126
.LBB26_168:
.Ltmp98:
	jmp	.LBB26_171
.LBB26_169:
.Ltmp95:
	jmp	.LBB26_171
.LBB26_170:
.Ltmp90:
.LBB26_171:
	movq	%rax, %rbx
	testq	%r14, %r14
	jne	.LBB26_173
	jmp	.LBB26_174
.LBB26_172:
.Ltmp87:
	movq	%rax, %rbx
	movq	%r13, %r14
	testq	%r14, %r14
	je	.LBB26_174
.LBB26_173:
.Ltmp99:
	movq	%r15, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp100:
.LBB26_174:                             # %_ZN20btAlignedObjectArrayIiED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB26_175:
.Ltmp101:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end26:
	.size	_ZN11HullLibrary8calchullEP9btVector3iR20btAlignedObjectArrayIjERii, .Lfunc_end26-_ZN11HullLibrary8calchullEP9btVector3iR20btAlignedObjectArrayIjERii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp75-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp75
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp86-.Ltmp75         #   Call between .Ltmp75 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin2   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin2   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp94-.Ltmp91         #   Call between .Ltmp91 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin2   #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp96-.Ltmp94         #   Call between .Ltmp94 and .Ltmp96
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin2   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp99-.Ltmp97         #   Call between .Ltmp97 and .Ltmp99
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin2  #     jumps to .Ltmp101
	.byte	1                       #   On action: 1
	.long	.Ltmp100-.Lfunc_begin2  # >> Call Site 9 <<
	.long	.Lfunc_end26-.Ltmp100   #   Call between .Ltmp100 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN11HullLibrary11ComputeHullEjPK9btVector3R11PHullResultj
	.p2align	4, 0x90
	.type	_ZN11HullLibrary11ComputeHullEjPK9btVector3R11PHullResultj,@function
_ZN11HullLibrary11ComputeHullEjPK9btVector3R11PHullResultj: # @_ZN11HullLibrary11ComputeHullEjPK9btVector3R11PHullResultj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi138:
	.cfi_def_cfa_offset 48
.Lcfi139:
	.cfi_offset %rbx, -32
.Lcfi140:
	.cfi_offset %r14, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movl	%r8d, %eax
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movl	%esi, %ebp
	leaq	24(%rbx), %rcx
	leaq	12(%rsp), %r8
	movq	%r14, %rsi
	movl	%ebp, %edx
	movl	%eax, %r9d
	callq	_ZN11HullLibrary8calchullEP9btVector3iR20btAlignedObjectArrayIjERii
	testl	%eax, %eax
	je	.LBB27_1
# BB#2:
	movl	12(%rsp), %eax
	leal	(%rax,%rax,2), %ecx
	movl	%ecx, 4(%rbx)
	movl	%eax, 8(%rbx)
	movq	%r14, 16(%rbx)
	movl	%ebp, (%rbx)
	movb	$1, %al
	jmp	.LBB27_3
.LBB27_1:
	xorl	%eax, %eax
.LBB27_3:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end27:
	.size	_ZN11HullLibrary11ComputeHullEjPK9btVector3R11PHullResultj, .Lfunc_end27-_ZN11HullLibrary11ComputeHullEjPK9btVector3R11PHullResultj
	.cfi_endproc

	.globl	_Z11ReleaseHullR11PHullResult
	.p2align	4, 0x90
	.type	_Z11ReleaseHullR11PHullResult,@function
_Z11ReleaseHullR11PHullResult:          # @_Z11ReleaseHullR11PHullResult
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 16
.Lcfi143:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, 28(%rbx)
	je	.LBB28_6
# BB#1:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_5
# BB#2:
	cmpb	$0, 48(%rbx)
	je	.LBB28_4
# BB#3:
	callq	_Z21btAlignedFreeInternalPv
.LBB28_4:
	movq	$0, 40(%rbx)
.LBB28_5:                               # %_ZN20btAlignedObjectArrayIjE5clearEv.exit
	movb	$1, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 28(%rbx)
.LBB28_6:
	movl	$0, (%rbx)
	movl	$0, 4(%rbx)
	movq	$0, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end28:
	.size	_Z11ReleaseHullR11PHullResult, .Lfunc_end28-_Z11ReleaseHullR11PHullResult
	.cfi_endproc

	.globl	_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult
	.p2align	4, 0x90
	.type	_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult,@function
_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult: # @_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi144:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi145:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi147:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi148:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi149:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi150:
	.cfi_def_cfa_offset 208
.Lcfi151:
	.cfi_offset %rbx, -56
.Lcfi152:
	.cfi_offset %r12, -48
.Lcfi153:
	.cfi_offset %r13, -40
.Lcfi154:
	.cfi_offset %r14, -32
.Lcfi155:
	.cfi_offset %r15, -24
.Lcfi156:
	.cfi_offset %rbp, -16
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movb	$1, 128(%rsp)
	movq	$0, 120(%rsp)
	movl	$0, 108(%rsp)
	movl	$0, 112(%rsp)
	movl	$0, 80(%rsp)
	movl	$0, 84(%rsp)
	movl	$0, 88(%rsp)
	movq	$0, 96(%rsp)
	movl	4(%r14), %esi
	cmpl	$8, %esi
	movl	$8, %eax
	cmoval	%esi, %eax
	testl	%eax, %eax
	jle	.LBB29_1
# BB#2:
	movslq	%eax, %rbx
	movq	%rbx, %rdi
	shlq	$4, %rdi
.Ltmp102:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
.Ltmp103:
# BB#3:                                 # %.lr.ph.i
	leaq	-1(%rbx), %rcx
	movq	%rbx, %rdx
	xorl	%eax, %eax
	andq	$7, %rdx
	je	.LBB29_6
# BB#4:                                 # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.preheader
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB29_5:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	64(%rsp), %xmm0
	movups	%xmm0, (%rsi)
	incq	%rax
	addq	$16, %rsi
	cmpq	%rax, %rdx
	jne	.LBB29_5
.LBB29_6:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB29_9
# BB#7:                                 # %.lr.ph.i.new
	subq	%rax, %rbx
	shlq	$4, %rax
	leaq	112(%r13,%rax), %rax
	.p2align	4, 0x90
.LBB29_8:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movups	64(%rsp), %xmm0
	movups	%xmm0, -112(%rax)
	movups	64(%rsp), %xmm0
	movups	%xmm0, -96(%rax)
	movups	64(%rsp), %xmm0
	movups	%xmm0, -80(%rax)
	movups	64(%rsp), %xmm0
	movups	%xmm0, -64(%rax)
	movups	64(%rsp), %xmm0
	movups	%xmm0, -48(%rax)
	movups	64(%rsp), %xmm0
	movups	%xmm0, -32(%rax)
	movups	64(%rsp), %xmm0
	movups	%xmm0, -16(%rax)
	movups	64(%rsp), %xmm0
	movups	%xmm0, (%rax)
	subq	$-128, %rax
	addq	$-8, %rbx
	jne	.LBB29_8
.LBB29_9:                               # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit.loopexit
	movl	4(%r14), %esi
	jmp	.LBB29_10
.LBB29_1:
	xorl	%r13d, %r13d
.LBB29_10:                              # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit
	movq	8(%r14), %rdx
	movl	16(%r14), %ecx
	movss	20(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
.Ltmp105:
	leaq	64(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	20(%rsp), %r8
	movq	%rbp, %rdi
	movq	%r13, %r9
	callq	_ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_
.Ltmp106:
# BB#11:
	movl	$1, %ebx
	testb	%al, %al
	je	.LBB29_179
# BB#12:                                # %.preheader
	movl	20(%rsp), %r12d
	testl	%r12d, %r12d
	je	.LBB29_13
# BB#19:                                # %.lr.ph330
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB29_20:                              # =>This Inner Loop Header: Depth=1
	cltq
	movss	64(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movq	%rax, %rcx
	shlq	$4, %rcx
	mulss	(%r13,%rcx), %xmm0
	movss	%xmm0, (%r13,%rcx)
	movss	68(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	4(%r13,%rcx), %xmm0
	movss	%xmm0, 4(%r13,%rcx)
	movss	72(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	8(%r13,%rcx), %xmm0
	movss	%xmm0, 8(%r13,%rcx)
	incl	%eax
	cmpl	%r12d, %eax
	jb	.LBB29_20
	jmp	.LBB29_14
.LBB29_13:
	xorl	%r12d, %r12d
.LBB29_14:                              # %._crit_edge
	movl	24(%r14), %r9d
	leaq	104(%rsp), %rcx
.Ltmp108:
	leaq	48(%rsp), %r8
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movl	%r12d, %edx
	callq	_ZN11HullLibrary8calchullEP9btVector3iR20btAlignedObjectArrayIjERii
.Ltmp109:
# BB#15:                                # %.noexc111
	testl	%eax, %eax
	je	.LBB29_179
# BB#16:
	movslq	48(%rsp), %rax
	leaq	(%rax,%rax,2), %rcx
	movl	%ecx, 84(%rsp)
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	%eax, 88(%rsp)
	movq	%r13, 96(%rsp)
	movl	%r12d, 80(%rsp)
	testl	%r12d, %r12d
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	jle	.LBB29_17
# BB#21:
	movslq	%r12d, %rbx
	movq	%rbx, %rdi
	shlq	$4, %rdi
.Ltmp111:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp112:
# BB#22:                                # %.lr.ph.i190
	leaq	-1(%rbx), %rcx
	movq	%rbx, %rdx
	xorl	%eax, %eax
	andq	$7, %rdx
	je	.LBB29_25
# BB#23:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i194.prol.preheader
	movq	%r15, %rsi
	.p2align	4, 0x90
.LBB29_24:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i194.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	48(%rsp), %xmm0
	movups	%xmm0, (%rsi)
	incq	%rax
	addq	$16, %rsi
	cmpq	%rax, %rdx
	jne	.LBB29_24
.LBB29_25:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i194.prol.loopexit
	cmpq	$7, %rcx
	jae	.LBB29_27
# BB#26:
	movq	40(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB29_29
.LBB29_17:
	xorl	%r15d, %r15d
	jmp	.LBB29_29
.LBB29_27:                              # %.lr.ph.i190.new
	subq	%rax, %rbx
	shlq	$4, %rax
	leaq	112(%r15,%rax), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB29_28:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i194
                                        # =>This Inner Loop Header: Depth=1
	movups	48(%rsp), %xmm0
	movups	%xmm0, -112(%rax)
	movups	48(%rsp), %xmm0
	movups	%xmm0, -96(%rax)
	movups	48(%rsp), %xmm0
	movups	%xmm0, -80(%rax)
	movups	48(%rsp), %xmm0
	movups	%xmm0, -64(%rax)
	movups	48(%rsp), %xmm0
	movups	%xmm0, -48(%rax)
	movups	48(%rsp), %xmm0
	movups	%xmm0, -32(%rax)
	movups	48(%rsp), %xmm0
	movups	%xmm0, -16(%rax)
	movups	48(%rsp), %xmm0
	movups	%xmm0, (%rax)
	subq	$-128, %rax
	addq	$-8, %rbx
	jne	.LBB29_28
.LBB29_29:                              # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit197
	movq	120(%rsp), %rbx
.Ltmp114:
	movl	%ecx, (%rsp)
	leaq	20(%rsp), %r8
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movl	%r12d, %edx
	movq	%r15, %rcx
	movq	%rbx, %r9
	movq	%r15, 136(%rsp)         # 8-byte Spill
	callq	_ZN11HullLibrary16BringOutYourDeadEPK9btVector3jPS0_RjPjj
.Ltmp115:
# BB#30:
	testb	$1, (%r14)
	movq	24(%rsp), %r9           # 8-byte Reload
	jne	.LBB29_31
# BB#101:
	movb	$1, (%r9)
	movl	20(%rsp), %ebp
	movl	%ebp, 4(%r9)
	movl	12(%r9), %ecx
	cmpl	%ebp, %ecx
	jge	.LBB29_127
# BB#102:
	movslq	%ebp, %r12
	cmpl	%ebp, 16(%r9)
	jge	.LBB29_103
# BB#104:
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	je	.LBB29_105
# BB#106:
	movq	%r12, %rdi
	shlq	$4, %rdi
.Ltmp126:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	%rax, %rbp
.Ltmp127:
# BB#107:                               # %.noexc152
	movl	12(%r9), %eax
	jmp	.LBB29_108
.LBB29_31:
	movb	$0, (%r9)
	movl	20(%rsp), %ebx
	movl	%ebx, 4(%r9)
	movl	12(%r9), %r15d
	cmpl	%ebx, %r15d
	jge	.LBB29_57
# BB#32:
	movslq	%ebx, %rbp
	cmpl	%ebx, 16(%r9)
	jge	.LBB29_33
# BB#34:
	movl	%ebx, 36(%rsp)          # 4-byte Spill
	testl	%ebx, %ebx
	je	.LBB29_35
# BB#36:
	movq	%rbp, %rdi
	shlq	$4, %rdi
.Ltmp116:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	%rax, %rbx
.Ltmp117:
# BB#37:                                # %.noexc215
	movl	12(%r9), %eax
	jmp	.LBB29_38
.LBB29_103:                             # %..lr.ph.i147_crit_edge
	leaq	24(%r9), %r15
	jmp	.LBB29_121
.LBB29_33:                              # %..lr.ph.i210_crit_edge
	leaq	24(%r9), %r12
	jmp	.LBB29_51
.LBB29_105:
	xorl	%ebp, %ebp
	movl	%ecx, %eax
.LBB29_108:                             # %_ZN20btAlignedObjectArrayI9btVector3E8allocateEi.exit.i.i137
	leaq	24(%r9), %r15
	testl	%eax, %eax
	jle	.LBB29_116
# BB#109:                               # %.lr.ph.i.i.i139
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB29_110
# BB#111:                               # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB29_112:                             # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbp,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB29_112
	jmp	.LBB29_113
.LBB29_35:
	xorl	%ebx, %ebx
	movl	%r15d, %eax
.LBB29_38:                              # %_ZN20btAlignedObjectArrayI9btVector3E8allocateEi.exit.i.i200
	leaq	24(%r9), %r12
	testl	%eax, %eax
	jle	.LBB29_46
# BB#39:                                # %.lr.ph.i.i.i202
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB29_40
# BB#41:                                # %.prol.preheader402
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB29_42:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB29_42
	jmp	.LBB29_43
.LBB29_110:
	xorl	%ecx, %ecx
.LBB29_113:                             # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB29_116
# BB#114:                               # %.lr.ph.i.i.i139.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB29_115:                             # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbp,%rcx)
	movq	(%r15), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbp,%rcx)
	movq	(%r15), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbp,%rcx)
	movq	(%r15), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbp,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB29_115
.LBB29_116:                             # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i144
	movq	24(%r9), %rdi
	testq	%rdi, %rdi
	je	.LBB29_120
# BB#117:
	cmpb	$0, 32(%r9)
	je	.LBB29_119
# BB#118:
.Ltmp128:
	callq	_Z21btAlignedFreeInternalPv
	movq	24(%rsp), %r9           # 8-byte Reload
.Ltmp129:
.LBB29_119:                             # %.noexc153
	movq	$0, (%r15)
.LBB29_120:                             # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.preheader.i145
	movb	$1, 32(%r9)
	movq	%rbp, 24(%r9)
	movl	40(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, 16(%r9)
	movl	36(%rsp), %ecx          # 4-byte Reload
.LBB29_121:                             # %.lr.ph.i147
	movslq	%ecx, %rax
	movl	%r12d, %edx
	subl	%ecx, %edx
	leaq	-1(%r12), %rcx
	subq	%rax, %rcx
	andq	$3, %rdx
	je	.LBB29_124
# BB#122:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i151.prol.preheader
	movq	%rax, %rsi
	shlq	$4, %rsi
	negq	%rdx
	.p2align	4, 0x90
.LBB29_123:                             # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i151.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rdi
	movups	48(%rsp), %xmm0
	movups	%xmm0, (%rdi,%rsi)
	incq	%rax
	addq	$16, %rsi
	incq	%rdx
	jne	.LBB29_123
.LBB29_124:                             # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i151.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB29_127
# BB#125:                               # %.lr.ph.i147.new
	subq	%rax, %r12
	shlq	$4, %rax
	addq	$48, %rax
	.p2align	4, 0x90
.LBB29_126:                             # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i151
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rcx
	movups	48(%rsp), %xmm0
	movups	%xmm0, -48(%rcx,%rax)
	movq	(%r15), %rcx
	movups	48(%rsp), %xmm0
	movups	%xmm0, -32(%rcx,%rax)
	movq	(%r15), %rcx
	movups	48(%rsp), %xmm0
	movups	%xmm0, -16(%rcx,%rax)
	movq	(%r15), %rcx
	movups	48(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$64, %rax
	addq	$-4, %r12
	jne	.LBB29_126
.LBB29_127:                             # %.loopexit318
	movl	%ebp, 12(%r9)
	movq	144(%rsp), %rbp         # 8-byte Reload
	movl	%ebp, 40(%r9)
	leal	(,%rbp,4), %ecx
	movl	%ecx, 44(%r9)
	movl	52(%r9), %eax
	cmpl	%ecx, %eax
	jge	.LBB29_128
# BB#129:
	cmpl	%ecx, 56(%r9)
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	jge	.LBB29_130
# BB#131:
	testl	%ecx, %ecx
	movl	%eax, 36(%rsp)          # 4-byte Spill
	je	.LBB29_132
# BB#133:
	movslq	%ecx, %rdi
	shlq	$2, %rdi
.Ltmp131:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	%rax, %r15
.Ltmp132:
# BB#134:                               # %.noexc133
	movl	52(%r9), %ecx
	jmp	.LBB29_135
.LBB29_128:                             # %.loopexit318..loopexit317_crit_edge
	leaq	64(%r9), %r12
	jmp	.LBB29_163
.LBB29_130:                             # %..lr.ph.i129_crit_edge
	leaq	64(%r9), %r12
	movq	64(%r9), %r15
	jmp	.LBB29_162
.LBB29_132:
	xorl	%r15d, %r15d
	movl	%eax, %ecx
.LBB29_135:                             # %_ZN20btAlignedObjectArrayIjE8allocateEi.exit.i.i
	movq	64(%r9), %rdi
	testl	%ecx, %ecx
	jle	.LBB29_159
# BB#136:                               # %.lr.ph.i.i.i123
	movslq	%ecx, %rax
	cmpl	$8, %ecx
	jae	.LBB29_138
# BB#137:
	xorl	%edx, %edx
	jmp	.LBB29_151
.LBB29_159:                             # %_ZNK20btAlignedObjectArrayIjE4copyEiiPj.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB29_157
# BB#160:                               # %_ZN20btAlignedObjectArrayIjE7reserveEi.exit.preheader.thread26.i
	leaq	72(%r9), %rbx
	jmp	.LBB29_161
.LBB29_138:                             # %min.iters.checked359
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB29_139
# BB#140:                               # %vector.memcheck371
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r15
	jae	.LBB29_143
# BB#141:                               # %vector.memcheck371
	leaq	(%r15,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB29_143
# BB#142:
	xorl	%edx, %edx
	jmp	.LBB29_151
.LBB29_40:
	xorl	%ecx, %ecx
.LBB29_43:                              # %.prol.loopexit403
	cmpq	$3, %r8
	jb	.LBB29_46
# BB#44:                                # %.lr.ph.i.i.i202.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB29_45:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movq	(%r12), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movq	(%r12), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movq	(%r12), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB29_45
.LBB29_46:                              # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i207
	movq	24(%r9), %rdi
	testq	%rdi, %rdi
	je	.LBB29_50
# BB#47:
	cmpb	$0, 32(%r9)
	je	.LBB29_49
# BB#48:
.Ltmp118:
	callq	_Z21btAlignedFreeInternalPv
	movq	24(%rsp), %r9           # 8-byte Reload
.Ltmp119:
.LBB29_49:                              # %.noexc216
	movq	$0, (%r12)
.LBB29_50:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.preheader.i208
	movb	$1, 32(%r9)
	movq	%rbx, 24(%r9)
	movl	36(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, 16(%r9)
.LBB29_51:                              # %.lr.ph.i210
	movslq	%r15d, %rax
	movl	%ebp, %edx
	subl	%r15d, %edx
	leaq	-1(%rbp), %rcx
	subq	%rax, %rcx
	andq	$3, %rdx
	je	.LBB29_54
# BB#52:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i214.prol.preheader
	movq	%rax, %rsi
	shlq	$4, %rsi
	negq	%rdx
	.p2align	4, 0x90
.LBB29_53:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i214.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	movups	48(%rsp), %xmm0
	movups	%xmm0, (%rdi,%rsi)
	incq	%rax
	addq	$16, %rsi
	incq	%rdx
	jne	.LBB29_53
.LBB29_54:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i214.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB29_57
# BB#55:                                # %.lr.ph.i210.new
	subq	%rax, %rbp
	shlq	$4, %rax
	addq	$48, %rax
	.p2align	4, 0x90
.LBB29_56:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i214
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	movups	48(%rsp), %xmm0
	movups	%xmm0, -48(%rcx,%rax)
	movq	(%r12), %rcx
	movups	48(%rsp), %xmm0
	movups	%xmm0, -32(%rcx,%rax)
	movq	(%r12), %rcx
	movups	48(%rsp), %xmm0
	movups	%xmm0, -16(%rcx,%rax)
	movq	(%r12), %rcx
	movups	48(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$64, %rax
	addq	$-4, %rbp
	jne	.LBB29_56
.LBB29_57:                              # %.loopexit321
	movl	%ebx, 12(%r9)
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	%eax, 40(%r9)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 44(%r9)
	movl	52(%r9), %r15d
	cmpl	%ecx, %r15d
	jge	.LBB29_92
# BB#58:
	cmpl	%ecx, 56(%r9)
	jge	.LBB29_59
# BB#60:
	testl	%eax, %eax
	je	.LBB29_61
# BB#62:
	movq	%rcx, %rdi
	shlq	$2, %rdi
.Ltmp121:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	%rax, %r12
.Ltmp122:
# BB#63:                                # %.noexc175
	movl	52(%r9), %ecx
	jmp	.LBB29_64
.LBB29_59:                              # %..lr.ph.i170_crit_edge
	movq	64(%r9), %r12
	jmp	.LBB29_91
.LBB29_61:
	xorl	%r12d, %r12d
	movl	%r15d, %ecx
.LBB29_64:                              # %_ZN20btAlignedObjectArrayIjE8allocateEi.exit.i.i158
	movq	64(%r9), %rdi
	testl	%ecx, %ecx
	jle	.LBB29_88
# BB#65:                                # %.lr.ph.i.i.i160
	movslq	%ecx, %rax
	cmpl	$8, %ecx
	jae	.LBB29_67
# BB#66:
	xorl	%edx, %edx
	jmp	.LBB29_80
.LBB29_88:                              # %_ZNK20btAlignedObjectArrayIjE4copyEiiPj.exit.i.i164
	testq	%rdi, %rdi
	jne	.LBB29_86
# BB#89:                                # %_ZN20btAlignedObjectArrayIjE7reserveEi.exit.preheader.thread26.i166
	leaq	72(%r9), %rbx
	jmp	.LBB29_90
.LBB29_67:                              # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB29_68
# BB#69:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r12
	jae	.LBB29_72
# BB#70:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB29_72
# BB#71:
	xorl	%edx, %edx
	jmp	.LBB29_80
.LBB29_139:
	xorl	%edx, %edx
	jmp	.LBB29_151
.LBB29_68:
	xorl	%edx, %edx
	jmp	.LBB29_80
.LBB29_143:                             # %vector.body355.preheader
	leaq	-8(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB29_144
# BB#145:                               # %vector.body355.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
.LBB29_146:                             # %vector.body355.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r15,%rbx,4)
	movups	%xmm1, 16(%r15,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB29_146
	jmp	.LBB29_147
.LBB29_72:                              # %vector.body.preheader
	leaq	-8(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB29_73
# BB#74:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
.LBB29_75:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB29_75
	jmp	.LBB29_76
.LBB29_144:
	xorl	%ebx, %ebx
.LBB29_147:                             # %vector.body355.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB29_150
# BB#148:                               # %vector.body355.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rdi,%rbx,4), %rbp
	leaq	112(%r15,%rbx,4), %rbx
.LBB29_149:                             # %vector.body355
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rsi
	jne	.LBB29_149
.LBB29_150:                             # %middle.block356
	cmpq	%rdx, %rax
	je	.LBB29_157
.LBB29_151:                             # %scalar.ph357.preheader
	subl	%edx, %ecx
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$7, %rcx
	je	.LBB29_154
# BB#152:                               # %scalar.ph357.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB29_153:                             # %scalar.ph357.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r15,%rdx,4)
	incq	%rdx
	incq	%rcx
	jne	.LBB29_153
.LBB29_154:                             # %scalar.ph357.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB29_157
# BB#155:                               # %scalar.ph357.preheader.new
	subq	%rdx, %rax
	leaq	28(%rdi,%rdx,4), %rcx
	leaq	28(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB29_156:                             # %scalar.ph357
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rax
	jne	.LBB29_156
.LBB29_157:                             # %_ZNK20btAlignedObjectArrayIjE4copyEiiPj.exit.thread.i.i
	leaq	72(%r9), %rbx
	cmpb	$0, 72(%r9)
	je	.LBB29_161
# BB#158:
.Ltmp133:
	callq	_Z21btAlignedFreeInternalPv
	movq	24(%rsp), %r9           # 8-byte Reload
.Ltmp134:
.LBB29_161:                             # %.lr.ph.i129.sink.split
	leaq	64(%r9), %r12
	movb	$1, (%rbx)
	movq	%r15, 64(%r9)
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 56(%r9)
	movl	36(%rsp), %eax          # 4-byte Reload
.LBB29_162:                             # %.lr.ph.i129
	cltq
	movslq	%ecx, %rdx
	leaq	(%r15,%rax,4), %rdi
	subq	%rax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movl	40(%rsp), %ecx          # 4-byte Reload
	movq	120(%rsp), %rbx
	movl	88(%rsp), %ebp
	movq	24(%rsp), %r9           # 8-byte Reload
.LBB29_163:                             # %.loopexit317
	movl	%ecx, 52(%r9)
	movq	24(%r9), %rdi
	movl	20(%rsp), %edx
	shlq	$4, %rdx
	movq	136(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rsi
	callq	memcpy
	testl	%ebp, %ebp
	je	.LBB29_171
# BB#164:                               # %.lr.ph.preheader
	movq	(%r12), %rcx
	addq	$4, %rcx
	leaq	8(%rbx), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB29_165:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$3, -4(%rcx)
	testb	$2, (%r14)
	jne	.LBB29_166
# BB#169:                               #   in Loop: Header=BB29_165 Depth=1
	movl	-8(%rax), %esi
	movl	%esi, (%rcx)
	movq	%rax, %rsi
	jmp	.LBB29_170
	.p2align	4, 0x90
.LBB29_166:                             #   in Loop: Header=BB29_165 Depth=1
	movl	(%rax), %esi
	movl	%esi, (%rcx)
	movq	%rbx, %rsi
.LBB29_170:                             #   in Loop: Header=BB29_165 Depth=1
	movl	-4(%rax), %edi
	movl	%edi, 4(%rcx)
	movl	(%rsi), %esi
	movl	%esi, 8(%rcx)
	addq	$12, %rbx
	incl	%edx
	addq	$16, %rcx
	addq	$12, %rax
	cmpl	88(%rsp), %edx
	jb	.LBB29_165
	jmp	.LBB29_171
.LBB29_73:
	xorl	%ebx, %ebx
.LBB29_76:                              # %vector.body.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB29_79
# BB#77:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rdi,%rbx,4), %rbp
	leaq	112(%r12,%rbx,4), %rbx
.LBB29_78:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rsi
	jne	.LBB29_78
.LBB29_79:                              # %middle.block
	cmpq	%rdx, %rax
	je	.LBB29_86
.LBB29_80:                              # %scalar.ph.preheader
	subl	%edx, %ecx
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$7, %rcx
	je	.LBB29_83
# BB#81:                                # %scalar.ph.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB29_82:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r12,%rdx,4)
	incq	%rdx
	incq	%rcx
	jne	.LBB29_82
.LBB29_83:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB29_86
# BB#84:                                # %scalar.ph.preheader.new
	subq	%rdx, %rax
	leaq	28(%rdi,%rdx,4), %rcx
	leaq	28(%r12,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB29_85:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rax
	jne	.LBB29_85
.LBB29_86:                              # %_ZNK20btAlignedObjectArrayIjE4copyEiiPj.exit.thread.i.i167
	leaq	72(%r9), %rbx
	cmpb	$0, 72(%r9)
	je	.LBB29_90
# BB#87:
.Ltmp123:
	callq	_Z21btAlignedFreeInternalPv
	movq	24(%rsp), %r9           # 8-byte Reload
.Ltmp124:
.LBB29_90:                              # %.lr.ph.i170.sink.split
	movb	$1, (%rbx)
	movq	%r12, 64(%r9)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 56(%r9)
.LBB29_91:                              # %.lr.ph.i170
	movslq	%r15d, %rax
	movslq	%ecx, %rdx
	leaq	(%r12,%rax,4), %rdi
	subq	%rax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
.LBB29_92:                              # %.loopexit320
	movl	%ecx, 52(%r9)
	movq	24(%r9), %rdi
	movl	20(%rsp), %edx
	shlq	$4, %rdx
	movq	136(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rsi
	movq	%r9, %rbx
	callq	memcpy
	testb	$2, (%r14)
	jne	.LBB29_93
# BB#100:
	movq	64(%rbx), %rdi
	movq	120(%rsp), %rsi
	movl	84(%rsp), %edx
	shlq	$2, %rdx
	callq	memcpy
	cmpl	$0, 108(%rsp)
	jne	.LBB29_172
	jmp	.LBB29_177
.LBB29_93:
	cmpl	$0, 88(%rsp)
	je	.LBB29_171
# BB#94:                                # %.lr.ph328.preheader
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	64(%rax), %rax
	movq	120(%rsp), %rcx
	addq	$8, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB29_95:                              # %.lr.ph328
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %esi
	movl	%esi, (%rax)
	movl	-4(%rcx), %esi
	movl	%esi, 4(%rax)
	movl	-8(%rcx), %esi
	movl	%esi, 8(%rax)
	incl	%edx
	addq	$12, %rcx
	addq	$12, %rax
	cmpl	88(%rsp), %edx
	jb	.LBB29_95
.LBB29_171:                             # %.loopexit
	cmpl	$0, 108(%rsp)
	je	.LBB29_177
.LBB29_172:
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB29_176
# BB#173:
	cmpb	$0, 128(%rsp)
	je	.LBB29_175
# BB#174:
.Ltmp136:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp137:
.LBB29_175:                             # %.noexc119
	movq	$0, 120(%rsp)
.LBB29_176:                             # %_ZN20btAlignedObjectArrayIjE5clearEv.exit.i
	movb	$1, 128(%rsp)
	movq	$0, 120(%rsp)
	movq	$0, 108(%rsp)
.LBB29_177:
	movl	$0, 80(%rsp)
	movl	$0, 84(%rsp)
	movq	$0, 96(%rsp)
	xorl	%ebx, %ebx
	testq	%r15, %r15
	je	.LBB29_179
# BB#178:
.Ltmp141:
	movq	%r15, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp142:
.LBB29_179:                             # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit117
	testq	%r13, %r13
	je	.LBB29_181
# BB#180:
.Ltmp146:
	movq	%r13, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp147:
.LBB29_181:                             # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit110
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB29_185
# BB#182:
	cmpb	$0, 128(%rsp)
	je	.LBB29_184
# BB#183:
	callq	_Z21btAlignedFreeInternalPv
.LBB29_184:
	movq	$0, 120(%rsp)
.LBB29_185:                             # %_ZN11PHullResultD2Ev.exit107
	movl	%ebx, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_98:
.Ltmp120:
	jmp	.LBB29_188
.LBB29_167:
.Ltmp130:
	jmp	.LBB29_188
.LBB29_99:
.Ltmp125:
	jmp	.LBB29_188
.LBB29_168:
.Ltmp135:
	jmp	.LBB29_188
.LBB29_186:
.Ltmp143:
	jmp	.LBB29_192
.LBB29_97:                              # %.thread
.Ltmp113:
	jmp	.LBB29_192
.LBB29_187:
.Ltmp138:
.LBB29_188:
	movq	%rax, %rbx
	cmpq	$0, 136(%rsp)           # 8-byte Folded Reload
	je	.LBB29_193
# BB#189:
.Ltmp139:
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.Ltmp140:
	jmp	.LBB29_193
.LBB29_96:
.Ltmp110:
	jmp	.LBB29_192
.LBB29_190:
.Ltmp148:
	movq	%rax, %rbx
	jmp	.LBB29_195
.LBB29_18:                              # %.thread305
.Ltmp104:
	movq	%rax, %rbx
	jmp	.LBB29_195
.LBB29_191:
.Ltmp107:
.LBB29_192:
	movq	%rax, %rbx
.LBB29_193:
	testq	%r13, %r13
	je	.LBB29_195
# BB#194:
.Ltmp144:
	movq	%r13, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp145:
.LBB29_195:                             # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB29_199
# BB#196:
	cmpb	$0, 128(%rsp)
	je	.LBB29_198
# BB#197:
.Ltmp149:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp150:
.LBB29_198:                             # %.noexc
	movq	$0, 120(%rsp)
.LBB29_199:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB29_200:
.Ltmp151:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end29:
	.size	_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult, .Lfunc_end29-_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\232\202\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\221\002"              # Call site table length
	.long	.Ltmp102-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin3  #     jumps to .Ltmp104
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp107-.Lfunc_begin3  #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin3  #     jumps to .Ltmp110
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin3  #     jumps to .Ltmp113
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp138-.Lfunc_begin3  #     jumps to .Ltmp138
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp130-.Lfunc_begin3  #     jumps to .Ltmp130
	.byte	0                       #   On action: cleanup
	.long	.Ltmp116-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp120-.Lfunc_begin3  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin3  #     jumps to .Ltmp130
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp135-.Lfunc_begin3  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp118-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin3  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin3  # >> Call Site 11 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp125-.Lfunc_begin3  #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin3  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp123-.Ltmp134       #   Call between .Ltmp134 and .Ltmp123
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin3  #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp136-.Ltmp124       #   Call between .Ltmp124 and .Ltmp136
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin3  #     jumps to .Ltmp138
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin3  #     jumps to .Ltmp143
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp147-.Ltmp146       #   Call between .Ltmp146 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin3  #     jumps to .Ltmp148
	.byte	0                       #   On action: cleanup
	.long	.Ltmp147-.Lfunc_begin3  # >> Call Site 19 <<
	.long	.Ltmp139-.Ltmp147       #   Call between .Ltmp147 and .Ltmp139
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin3  # >> Call Site 20 <<
	.long	.Ltmp150-.Ltmp139       #   Call between .Ltmp139 and .Ltmp150
	.long	.Ltmp151-.Lfunc_begin3  #     jumps to .Ltmp151
	.byte	1                       #   On action: 1
	.long	.Ltmp150-.Lfunc_begin3  # >> Call Site 21 <<
	.long	.Lfunc_end29-.Ltmp150   #   Call between .Ltmp150 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI30_0:
	.long	2139095039              # float 3.40282347E+38
.LCPI30_1:
	.long	4286578687              # float -3.40282347E+38
.LCPI30_2:
	.long	1056964608              # float 0.5
.LCPI30_3:
	.long	897988541               # float 9.99999997E-7
.LCPI30_4:
	.long	1065353216              # float 1
.LCPI30_6:
	.long	1008981770              # float 0.00999999977
.LCPI30_7:
	.long	1028443341              # float 0.0500000007
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI30_5:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_
	.p2align	4, 0x90
	.type	_ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_,@function
_ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_: # @_ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi157:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi158:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi160:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi161:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi163:
	.cfi_def_cfa_offset 176
.Lcfi164:
	.cfi_offset %rbx, -56
.Lcfi165:
	.cfi_offset %r12, -48
.Lcfi166:
	.cfi_offset %r13, -40
.Lcfi167:
	.cfi_offset %r14, -32
.Lcfi168:
	.cfi_offset %r15, -24
.Lcfi169:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%rdx, %r13
	movl	%esi, %ebx
	movq	%rdi, %r9
	testl	%ebx, %ebx
	je	.LBB30_4
# BB#1:
	movq	%r8, %r15
	movq	176(%rsp), %rsi
	movslq	36(%r9), %rbp
	testq	%rbp, %rbp
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	jns	.LBB30_10
# BB#2:
	movq	48(%r9), %rdi
	cmpl	$0, 40(%r9)
	js	.LBB30_5
# BB#3:
	movl	%ecx, %r14d
	jmp	.LBB30_9
.LBB30_4:
	xorl	%eax, %eax
	jmp	.LBB30_88
.LBB30_5:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
	testq	%rdi, %rdi
	je	.LBB30_8
# BB#6:                                 # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
	cmpb	$0, 56(%r9)
	je	.LBB30_8
# BB#7:
	movl	%ecx, %r14d
	callq	_Z21btAlignedFreeInternalPv
	movl	%r14d, %ecx
	movq	24(%rsp), %r9           # 8-byte Reload
.LBB30_8:                               # %.lr.ph.i.sink.split
	movl	%ecx, %r14d
	movb	$1, 56(%r9)
	movq	$0, 48(%r9)
	movl	$0, 40(%r9)
	xorl	%edi, %edi
.LBB30_9:                               # %.lr.ph.i
	leaq	(%rdi,%rbp,4), %rdi
	shlq	$2, %rbp
	negq	%rbp
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	24(%rsp), %r9           # 8-byte Reload
	movl	%r14d, %ecx
	movq	176(%rsp), %rsi
.LBB30_10:                              # %.lr.ph470
	movl	$0, 36(%r9)
	movq	%r15, %rbp
	movl	$0, (%rbp)
	movabsq	$4575657222473777152, %rax # imm = 0x3F8000003F800000
	movq	%rax, (%rsi)
	movl	$1065353216, 8(%rsi)    # imm = 0x3F800000
	movl	%ecx, %edx
	leaq	8(%r13), %rax
	movss	.LCPI30_0(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI30_1(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movl	%ebx, %ecx
	movaps	%xmm4, %xmm7
	movaps	%xmm8, %xmm9
	movaps	%xmm4, %xmm3
	movaps	%xmm8, %xmm10
	.p2align	4, 0x90
.LBB30_11:                              # =>This Inner Loop Header: Depth=1
	movss	-8(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm10
	jbe	.LBB30_13
# BB#12:                                #   in Loop: Header=BB30_11 Depth=1
	movss	-8(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm10
	jmp	.LBB30_14
	.p2align	4, 0x90
.LBB30_13:                              #   in Loop: Header=BB30_11 Depth=1
	movaps	%xmm1, %xmm5
.LBB30_14:                              #   in Loop: Header=BB30_11 Depth=1
	movss	-4(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm9
	jbe	.LBB30_16
# BB#15:                                #   in Loop: Header=BB30_11 Depth=1
	movss	-4(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm9
	jmp	.LBB30_17
	.p2align	4, 0x90
.LBB30_16:                              #   in Loop: Header=BB30_11 Depth=1
	movaps	%xmm2, %xmm1
.LBB30_17:                              #   in Loop: Header=BB30_11 Depth=1
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm8
	jbe	.LBB30_19
# BB#18:                                #   in Loop: Header=BB30_11 Depth=1
	movss	(%rax), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm8
	jmp	.LBB30_20
	.p2align	4, 0x90
.LBB30_19:                              #   in Loop: Header=BB30_11 Depth=1
	movaps	%xmm2, %xmm6
.LBB30_20:                              #   in Loop: Header=BB30_11 Depth=1
	maxss	%xmm3, %xmm5
	maxss	%xmm7, %xmm1
	maxss	%xmm4, %xmm6
	addq	%rdx, %rax
	decl	%ecx
	movaps	%xmm6, %xmm4
	movaps	%xmm1, %xmm7
	movaps	%xmm5, %xmm3
	jne	.LBB30_11
# BB#21:                                # %._crit_edge471
	subss	%xmm10, %xmm5
	subss	%xmm9, %xmm1
	subss	%xmm8, %xmm6
	movss	.LCPI30_2(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	mulss	%xmm3, %xmm2
	addss	%xmm10, %xmm2
	movaps	%xmm2, %xmm10
	movaps	%xmm1, %xmm2
	mulss	%xmm3, %xmm2
	addss	%xmm9, %xmm2
	movaps	%xmm2, %xmm9
	movaps	%xmm6, %xmm2
	mulss	%xmm3, %xmm2
	addss	%xmm8, %xmm2
	movaps	%xmm2, %xmm8
	cmpl	$3, %ebx
	jb	.LBB30_73
# BB#22:                                # %._crit_edge471
	movss	.LCPI30_3(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm2
	ja	.LBB30_73
# BB#23:                                # %._crit_edge471
	ucomiss	%xmm1, %xmm2
	ja	.LBB30_73
# BB#24:                                # %._crit_edge471
	ucomiss	%xmm6, %xmm2
	ja	.LBB30_73
# BB#25:                                # %.lr.ph464
	movss	%xmm5, (%rsi)
	movss	%xmm1, 4(%rsi)
	movss	%xmm6, 8(%rsi)
	movss	.LCPI30_4(%rip), %xmm11 # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm12
	divss	%xmm5, %xmm12
	movaps	%xmm11, %xmm13
	divss	%xmm1, %xmm13
	divss	%xmm6, %xmm11
	mulss	%xmm12, %xmm10
	mulss	%xmm13, %xmm9
	mulss	%xmm11, %xmm8
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	movaps	%xmm10, 96(%rsp)        # 16-byte Spill
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	movaps	%xmm9, 80(%rsp)         # 16-byte Spill
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	movaps	%xmm8, 64(%rsp)         # 16-byte Spill
	leaq	8(%r12), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edi
	movaps	.LCPI30_5(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	xorl	%r15d, %r15d
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movss	%xmm11, 20(%rsp)        # 4-byte Spill
	movss	%xmm12, 16(%rsp)        # 4-byte Spill
	movss	%xmm13, 12(%rsp)        # 4-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	jmp	.LBB30_26
.LBB30_73:
	movss	.LCPI30_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	minss	%xmm0, %xmm2
	movss	.LCPI30_3(%rip), %xmm11 # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm3
	cmpltss	%xmm5, %xmm3
	andps	%xmm3, %xmm2
	andnps	%xmm0, %xmm3
	orps	%xmm2, %xmm3
	movaps	%xmm11, %xmm2
	cmpltss	%xmm1, %xmm2
	movaps	%xmm1, %xmm7
	minss	%xmm3, %xmm7
	andps	%xmm2, %xmm7
	andnps	%xmm3, %xmm2
	orps	%xmm2, %xmm7
	movaps	%xmm11, %xmm2
	cmpltss	%xmm6, %xmm2
	movaps	%xmm6, %xmm4
	minss	%xmm7, %xmm4
	andps	%xmm2, %xmm4
	andnps	%xmm7, %xmm2
	orps	%xmm2, %xmm4
	ucomiss	%xmm0, %xmm4
	jne	.LBB30_82
	jp	.LBB30_82
# BB#74:
	movss	.LCPI30_6(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	movaps	%xmm4, %xmm2
	jmp	.LBB30_84
.LBB30_50:                              # %vector.body.preheader
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	%rbp, %r8
	leaq	-8(%rdx), %rbx
	movl	%ebx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB30_53
# BB#51:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB30_26 Depth=1
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB30_52:                              # %vector.body.prol
                                        #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbp,4), %xmm2
	movups	16(%rdi,%rbp,4), %xmm3
	movups	%xmm2, (%r12,%rbp,4)
	movups	%xmm3, 16(%r12,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB30_52
	jmp	.LBB30_54
.LBB30_53:                              #   in Loop: Header=BB30_26 Depth=1
	xorl	%ebp, %ebp
.LBB30_54:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB30_26 Depth=1
	cmpq	$24, %rbx
	jb	.LBB30_57
# BB#55:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	%rdx, %rsi
	subq	%rbp, %rsi
	leaq	112(%rdi,%rbp,4), %rbx
	leaq	112(%r12,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB30_56:                              # %vector.body
                                        #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm2
	movups	-96(%rbx), %xmm3
	movups	%xmm2, -112(%rbp)
	movups	%xmm3, -96(%rbp)
	movups	-80(%rbx), %xmm2
	movups	-64(%rbx), %xmm3
	movups	%xmm2, -80(%rbp)
	movups	%xmm3, -64(%rbp)
	movups	-48(%rbx), %xmm2
	movups	-32(%rbx), %xmm3
	movups	%xmm2, -48(%rbp)
	movups	%xmm3, -32(%rbp)
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	movups	%xmm2, -16(%rbp)
	movups	%xmm3, (%rbp)
	subq	$-128, %rbx
	subq	$-128, %rbp
	addq	$-32, %rsi
	jne	.LBB30_56
.LBB30_57:                              # %middle.block
                                        #   in Loop: Header=BB30_26 Depth=1
	cmpq	%rdx, %rcx
	movq	%r8, %rbp
	je	.LBB30_64
	jmp	.LBB30_58
	.p2align	4, 0x90
.LBB30_26:                              # %_ZN20btAlignedObjectArrayIiE9push_backERKi.exit._crit_edge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_28 Depth 2
                                        #     Child Loop BB30_52 Depth 2
                                        #     Child Loop BB30_56 Depth 2
                                        #     Child Loop BB30_60 Depth 2
                                        #     Child Loop BB30_63 Depth 2
	movss	(%r13), %xmm7           # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm7
	movss	4(%r13), %xmm8          # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm8
	movss	8(%r13), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	testl	%ecx, %ecx
	movl	$0, %r14d
	je	.LBB30_34
# BB#27:                                # %.lr.ph459
                                        #   in Loop: Header=BB30_26 Depth=1
	movl	(%rbp), %ecx
	movq	%rsi, %rax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB30_28:                              #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-8(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	subss	%xmm7, %xmm4
	andps	%xmm1, %xmm4
	ucomiss	%xmm4, %xmm0
	jbe	.LBB30_31
# BB#29:                                #   in Loop: Header=BB30_28 Depth=2
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	subss	%xmm2, %xmm5
	andps	%xmm1, %xmm5
	ucomiss	%xmm5, %xmm0
	jbe	.LBB30_31
# BB#30:                                #   in Loop: Header=BB30_28 Depth=2
	movss	-4(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm6
	subss	%xmm8, %xmm6
	andps	%xmm1, %xmm6
	ucomiss	%xmm6, %xmm0
	ja	.LBB30_32
.LBB30_31:                              #   in Loop: Header=BB30_28 Depth=2
	incq	%r14
	addq	$16, %rax
	cmpq	%rcx, %r14
	jb	.LBB30_28
	jmp	.LBB30_34
	.p2align	4, 0x90
.LBB30_32:                              #   in Loop: Header=BB30_26 Depth=1
	movaps	%xmm7, %xmm6
	unpcklps	%xmm3, %xmm6    # xmm6 = xmm6[0],xmm3[0],xmm6[1],xmm3[1]
	subps	96(%rsp), %xmm6         # 16-byte Folded Reload
	movaps	%xmm8, %xmm3
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	subps	80(%rsp), %xmm3         # 16-byte Folded Reload
	movaps	%xmm2, %xmm5
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	subps	64(%rsp), %xmm5         # 16-byte Folded Reload
	mulps	%xmm6, %xmm6
	mulps	%xmm3, %xmm3
	addps	%xmm6, %xmm3
	mulps	%xmm5, %xmm5
	addps	%xmm3, %xmm5
	movaps	%xmm5, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	ucomiss	%xmm3, %xmm5
	jbe	.LBB30_34
# BB#33:                                # %.critedge
                                        #   in Loop: Header=BB30_26 Depth=1
	movss	%xmm7, -8(%rax)
	movss	%xmm8, -4(%rax)
	movss	%xmm2, (%rax)
	.p2align	4, 0x90
.LBB30_34:                              # %.critedge409
                                        #   in Loop: Header=BB30_26 Depth=1
	cmpl	(%rbp), %r14d
	jne	.LBB30_36
# BB#35:                                #   in Loop: Header=BB30_26 Depth=1
	movl	%r14d, %eax
	shlq	$4, %rax
	movss	%xmm7, (%r12,%rax)
	movss	%xmm8, 4(%r12,%rax)
	movss	%xmm2, 8(%r12,%rax)
	leal	1(%r14), %eax
	movl	%eax, (%rbp)
.LBB30_36:                              #   in Loop: Header=BB30_26 Depth=1
	movl	36(%r9), %eax
	cmpl	40(%r9), %eax
	jne	.LBB30_68
# BB#37:                                #   in Loop: Header=BB30_26 Depth=1
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	cmovel	%edi, %ecx
	cmpl	%ecx, %eax
	jge	.LBB30_68
# BB#38:                                #   in Loop: Header=BB30_26 Depth=1
	testl	%ecx, %ecx
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	je	.LBB30_40
# BB#39:                                #   in Loop: Header=BB30_26 Depth=1
	movslq	%ecx, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movaps	.LCPI30_5(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movq	24(%rsp), %r9           # 8-byte Reload
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	%rax, %r12
	movl	36(%r9), %eax
	jmp	.LBB30_41
.LBB30_40:                              #   in Loop: Header=BB30_26 Depth=1
	xorl	%r12d, %r12d
.LBB30_41:                              # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	48(%r9), %rdi
	testl	%eax, %eax
	jle	.LBB30_44
# BB#42:                                # %.lr.ph.i.i.i416
                                        #   in Loop: Header=BB30_26 Depth=1
	movslq	%eax, %rcx
	cmpl	$8, %eax
	jae	.LBB30_45
# BB#43:                                #   in Loop: Header=BB30_26 Depth=1
	xorl	%edx, %edx
	jmp	.LBB30_58
.LBB30_44:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i420
                                        #   in Loop: Header=BB30_26 Depth=1
	testq	%rdi, %rdi
	jne	.LBB30_64
	jmp	.LBB30_67
.LBB30_45:                              # %min.iters.checked
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	%rcx, %rdx
	andq	$-8, %rdx
	je	.LBB30_49
# BB#46:                                # %vector.memcheck
                                        #   in Loop: Header=BB30_26 Depth=1
	leaq	(%rdi,%rcx,4), %rsi
	cmpq	%rsi, %r12
	jae	.LBB30_50
# BB#47:                                # %vector.memcheck
                                        #   in Loop: Header=BB30_26 Depth=1
	leaq	(%r12,%rcx,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB30_50
.LBB30_49:                              #   in Loop: Header=BB30_26 Depth=1
	xorl	%edx, %edx
.LBB30_58:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB30_26 Depth=1
	subl	%edx, %eax
	leaq	-1(%rcx), %rsi
	subq	%rdx, %rsi
	movq	%rbp, %rbx
	andq	$7, %rax
	je	.LBB30_61
# BB#59:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB30_26 Depth=1
	negq	%rax
	.p2align	4, 0x90
.LBB30_60:                              # %scalar.ph.prol
                                        #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r12,%rdx,4)
	incq	%rdx
	incq	%rax
	jne	.LBB30_60
.LBB30_61:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB30_26 Depth=1
	cmpq	$7, %rsi
	movq	%rbx, %rbp
	jb	.LBB30_64
# BB#62:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB30_26 Depth=1
	subq	%rdx, %rcx
	leaq	28(%rdi,%rdx,4), %rax
	leaq	28(%r12,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB30_63:                              # %scalar.ph
                                        #   Parent Loop BB30_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-8, %rcx
	jne	.LBB30_63
.LBB30_64:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i422
                                        #   in Loop: Header=BB30_26 Depth=1
	cmpb	$0, 56(%r9)
	je	.LBB30_66
# BB#65:                                #   in Loop: Header=BB30_26 Depth=1
	callq	_Z21btAlignedFreeInternalPv
	movaps	.LCPI30_5(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movq	24(%rsp), %r9           # 8-byte Reload
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB30_66:                              #   in Loop: Header=BB30_26 Depth=1
	movq	$0, 48(%r9)
	movl	36(%r9), %eax
.LBB30_67:                              # %_ZN20btAlignedObjectArrayIiE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB30_26 Depth=1
	movb	$1, 56(%r9)
	movq	%r12, 48(%r9)
	movl	36(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 40(%r9)
	movq	56(%rsp), %r12          # 8-byte Reload
	movl	32(%rsp), %ebx          # 4-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	$1, %edi
.LBB30_68:                              # %_ZN20btAlignedObjectArrayIiE9push_backERKi.exit
                                        #   in Loop: Header=BB30_26 Depth=1
	movq	48(%r9), %rcx
	cltq
	movl	%r14d, (%rcx,%rax,4)
	incl	36(%r9)
	addq	%rdx, %r13
	incl	%r15d
	movl	(%rbp), %ecx
	cmpl	%ebx, %r15d
	jne	.LBB30_26
# BB#69:                                # %._crit_edge465
	movb	$1, %al
	movss	.LCPI30_1(%rip), %xmm11 # xmm11 = mem[0],zero,zero,zero
	movss	.LCPI30_0(%rip), %xmm10 # xmm10 = mem[0],zero,zero,zero
	testl	%ecx, %ecx
	je	.LBB30_75
# BB#70:                                # %.lr.ph
	xorl	%edx, %edx
	movaps	%xmm10, %xmm9
	movaps	%xmm11, %xmm12
	movaps	%xmm10, %xmm14
	movaps	%xmm11, %xmm13
	movaps	%xmm11, %xmm15
	movaps	%xmm10, %xmm8
	movaps	%xmm11, %xmm3
	movaps	%xmm10, %xmm4
	movaps	%xmm11, %xmm6
	movaps	%xmm10, %xmm2
	.p2align	4, 0x90
.LBB30_71:                              # =>This Inner Loop Header: Depth=1
	movss	-8(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	-4(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm7
	cmpltss	%xmm2, %xmm7
	movaps	%xmm7, %xmm1
	andps	%xmm5, %xmm1
	andnps	%xmm14, %xmm7
	orps	%xmm1, %xmm7
	movaps	%xmm5, %xmm14
	minss	%xmm2, %xmm14
	movaps	%xmm6, %xmm1
	cmpltss	%xmm5, %xmm1
	movaps	%xmm1, %xmm2
	andps	%xmm5, %xmm2
	andnps	%xmm13, %xmm1
	orps	%xmm2, %xmm1
	movaps	%xmm1, %xmm13
	maxss	%xmm6, %xmm5
	movaps	%xmm0, %xmm1
	cmpltss	%xmm4, %xmm1
	movaps	%xmm1, %xmm2
	andps	%xmm0, %xmm2
	andnps	%xmm9, %xmm1
	orps	%xmm2, %xmm1
	movaps	%xmm1, %xmm9
	movaps	%xmm0, %xmm2
	minss	%xmm4, %xmm2
	movaps	%xmm3, %xmm1
	cmpltss	%xmm0, %xmm1
	movaps	%xmm1, %xmm4
	andps	%xmm0, %xmm4
	andnps	%xmm12, %xmm1
	orps	%xmm4, %xmm1
	movaps	%xmm1, %xmm12
	maxss	%xmm3, %xmm0
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	cmpltss	%xmm8, %xmm1
	movaps	%xmm1, %xmm4
	andps	%xmm3, %xmm4
	andnps	%xmm10, %xmm1
	orps	%xmm4, %xmm1
	movaps	%xmm1, %xmm10
	movaps	%xmm3, %xmm1
	minss	%xmm8, %xmm1
	movaps	%xmm15, %xmm4
	cmpltss	%xmm3, %xmm4
	movaps	%xmm4, %xmm6
	andps	%xmm3, %xmm6
	andnps	%xmm11, %xmm4
	orps	%xmm6, %xmm4
	movaps	%xmm4, %xmm11
	maxss	%xmm15, %xmm3
	incq	%rdx
	addq	$16, %rsi
	cmpq	%rcx, %rdx
	movaps	%xmm3, %xmm15
	movaps	%xmm1, %xmm8
	movaps	%xmm0, %xmm3
	movaps	%xmm2, %xmm4
	movaps	%xmm5, %xmm6
	movaps	%xmm14, %xmm2
	movaps	%xmm7, %xmm14
	jb	.LBB30_71
# BB#72:                                # %._crit_edge.loopexit
	cmpl	$3, %ecx
	setb	%cl
	jmp	.LBB30_76
.LBB30_75:
	movaps	%xmm10, %xmm9
	movaps	%xmm11, %xmm12
	movaps	%xmm10, %xmm14
	movaps	%xmm11, %xmm13
	movb	$1, %cl
.LBB30_76:                              # %._crit_edge
	subss	%xmm14, %xmm13
	subss	%xmm9, %xmm12
	subss	%xmm10, %xmm11
	testb	%cl, %cl
	movss	.LCPI30_3(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	jne	.LBB30_80
# BB#77:                                # %._crit_edge
	ucomiss	%xmm13, %xmm1
	ja	.LBB30_80
# BB#78:                                # %._crit_edge
	ucomiss	%xmm12, %xmm1
	ja	.LBB30_80
# BB#79:                                # %._crit_edge
	ucomiss	%xmm11, %xmm1
	jbe	.LBB30_88
.LBB30_80:
	movaps	%xmm13, %xmm0
	movss	.LCPI30_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm0, %xmm14
	movaps	%xmm12, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm0, %xmm9
	mulss	%xmm11, %xmm2
	addss	%xmm2, %xmm10
	movaps	%xmm1, %xmm4
	movss	.LCPI30_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm0
	minss	%xmm1, %xmm0
	movaps	%xmm4, %xmm2
	cmpless	%xmm13, %xmm2
	andps	%xmm2, %xmm0
	andnps	%xmm1, %xmm2
	orps	%xmm0, %xmm2
	movaps	%xmm4, %xmm0
	cmpless	%xmm12, %xmm0
	movaps	%xmm12, %xmm3
	minss	%xmm2, %xmm3
	andps	%xmm0, %xmm3
	andnps	%xmm2, %xmm0
	orps	%xmm0, %xmm3
	movaps	%xmm4, %xmm2
	cmpless	%xmm11, %xmm2
	movaps	%xmm11, %xmm0
	minss	%xmm3, %xmm0
	andps	%xmm2, %xmm0
	andnps	%xmm3, %xmm2
	orps	%xmm2, %xmm0
	ucomiss	%xmm1, %xmm0
	jne	.LBB30_85
	jp	.LBB30_85
# BB#81:
	movss	.LCPI30_6(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	movaps	%xmm1, %xmm0
	jmp	.LBB30_87
.LBB30_82:
	mulss	.LCPI30_7(%rip), %xmm4
	movaps	%xmm5, %xmm2
	cmpltss	%xmm11, %xmm2
	movaps	%xmm2, %xmm0
	andps	%xmm4, %xmm0
	andnps	%xmm5, %xmm2
	orps	%xmm0, %xmm2
	movaps	%xmm1, %xmm3
	cmpltss	%xmm11, %xmm3
	movaps	%xmm3, %xmm0
	andps	%xmm4, %xmm0
	andnps	%xmm1, %xmm3
	orps	%xmm0, %xmm3
	ucomiss	%xmm6, %xmm11
	ja	.LBB30_84
# BB#83:
	movaps	%xmm6, %xmm4
.LBB30_84:
	movaps	%xmm10, %xmm0
	subss	%xmm2, %xmm0
	addss	%xmm2, %xmm10
	movaps	%xmm9, %xmm1
	subss	%xmm3, %xmm1
	addss	%xmm3, %xmm9
	movaps	%xmm8, %xmm2
	subss	%xmm4, %xmm2
	movl	(%rbp), %eax
	leal	1(%rax), %r8d
	leal	2(%rax), %r11d
	leal	3(%rax), %esi
	leal	4(%rax), %edi
	movq	%rbp, %rcx
	leal	5(%rax), %ebp
	movq	%rcx, %r10
	leal	6(%rax), %edx
	leal	7(%rax), %ecx
	leal	8(%rax), %r9d
	shlq	$4, %rax
	movss	%xmm0, (%r12,%rax)
	movss	%xmm1, 4(%r12,%rax)
	movss	%xmm2, 8(%r12,%rax)
	shlq	$4, %r8
	movss	%xmm10, (%r12,%r8)
	movss	%xmm1, 4(%r12,%r8)
	movss	%xmm2, 8(%r12,%r8)
	shlq	$4, %r11
	movss	%xmm10, (%r12,%r11)
	movss	%xmm9, 4(%r12,%r11)
	movss	%xmm2, 8(%r12,%r11)
	shlq	$4, %rsi
	movss	%xmm0, (%r12,%rsi)
	movss	%xmm9, 4(%r12,%rsi)
	movss	%xmm2, 8(%r12,%rsi)
	addss	%xmm4, %xmm8
	shlq	$4, %rdi
	movss	%xmm0, (%r12,%rdi)
	movss	%xmm1, 4(%r12,%rdi)
	movss	%xmm8, 8(%r12,%rdi)
	shlq	$4, %rbp
	movss	%xmm10, (%r12,%rbp)
	movss	%xmm1, 4(%r12,%rbp)
	movss	%xmm8, 8(%r12,%rbp)
	shlq	$4, %rdx
	movss	%xmm10, (%r12,%rdx)
	movss	%xmm9, 4(%r12,%rdx)
	movss	%xmm8, 8(%r12,%rdx)
	shlq	$4, %rcx
	movss	%xmm0, (%r12,%rcx)
	movss	%xmm9, 4(%r12,%rcx)
	movss	%xmm8, 8(%r12,%rcx)
	movl	%r9d, (%r10)
	movb	$1, %al
	jmp	.LBB30_88
.LBB30_85:
	mulss	.LCPI30_7(%rip), %xmm0
	movaps	%xmm13, %xmm1
	cmpltss	%xmm4, %xmm1
	movaps	%xmm1, %xmm2
	andps	%xmm0, %xmm2
	andnps	%xmm13, %xmm1
	orps	%xmm2, %xmm1
	movaps	%xmm12, %xmm2
	cmpltss	%xmm4, %xmm2
	movaps	%xmm2, %xmm3
	andps	%xmm0, %xmm3
	andnps	%xmm12, %xmm2
	orps	%xmm3, %xmm2
	ucomiss	%xmm11, %xmm4
	ja	.LBB30_87
# BB#86:
	movaps	%xmm11, %xmm0
.LBB30_87:                              # %.critedge414
	movaps	%xmm14, %xmm3
	subss	%xmm1, %xmm3
	addss	%xmm1, %xmm14
	movaps	%xmm9, %xmm1
	subss	%xmm2, %xmm1
	addss	%xmm2, %xmm9
	movaps	%xmm10, %xmm2
	subss	%xmm0, %xmm2
	addss	%xmm0, %xmm10
	movss	%xmm3, (%r12)
	movss	%xmm1, 4(%r12)
	movss	%xmm2, 8(%r12)
	movss	%xmm14, 16(%r12)
	movss	%xmm1, 20(%r12)
	movss	%xmm2, 24(%r12)
	movss	%xmm14, 32(%r12)
	movss	%xmm9, 36(%r12)
	movss	%xmm2, 40(%r12)
	movss	%xmm3, 48(%r12)
	movss	%xmm9, 52(%r12)
	movss	%xmm2, 56(%r12)
	movss	%xmm3, 64(%r12)
	movss	%xmm1, 68(%r12)
	movss	%xmm10, 72(%r12)
	movss	%xmm14, 80(%r12)
	movss	%xmm1, 84(%r12)
	movss	%xmm10, 88(%r12)
	movss	%xmm14, 96(%r12)
	movss	%xmm9, 100(%r12)
	movss	%xmm10, 104(%r12)
	movss	%xmm3, 112(%r12)
	movss	%xmm9, 116(%r12)
	movss	%xmm10, 120(%r12)
	movl	$8, (%rbp)
.LBB30_88:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	_ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_, .Lfunc_end30-_ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_
	.cfi_endproc

	.globl	_ZN11HullLibrary16BringOutYourDeadEPK9btVector3jPS0_RjPjj
	.p2align	4, 0x90
	.type	_ZN11HullLibrary16BringOutYourDeadEPK9btVector3jPS0_RjPjj,@function
_ZN11HullLibrary16BringOutYourDeadEPK9btVector3jPS0_RjPjj: # @_ZN11HullLibrary16BringOutYourDeadEPK9btVector3jPS0_RjPjj
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi173:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi174:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi175:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi176:
	.cfi_def_cfa_offset 96
.Lcfi177:
	.cfi_offset %rbx, -56
.Lcfi178:
	.cfi_offset %r12, -48
.Lcfi179:
	.cfi_offset %r13, -40
.Lcfi180:
	.cfi_offset %r14, -32
.Lcfi181:
	.cfi_offset %r15, -24
.Lcfi182:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movl	%edx, %r8d
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	movl	96(%rsp), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movslq	36(%rbp), %r12
	testq	%r12, %r12
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	jle	.LBB31_1
# BB#2:
	movq	%rcx, %rbx
	shlq	$2, %r12
.Ltmp152:
	movl	$16, %esi
	movq	%r12, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
.Ltmp153:
# BB#3:                                 # %_ZN20btAlignedObjectArrayIiE6resizeEiRKi.exit
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	memset
	cmpl	$0, 36(%rbp)
	jle	.LBB31_4
# BB#5:                                 # %.lr.ph139
	movq	48(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB31_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	movslq	36(%rbp), %rdx
	cmpq	%rdx, %rsi
	jl	.LBB31_6
	jmp	.LBB31_7
.LBB31_1:
	xorl	%r13d, %r13d
	jmp	.LBB31_8
.LBB31_4:
	movq	%rbx, %rcx
.LBB31_7:
	movl	8(%rsp), %r8d           # 4-byte Reload
.LBB31_8:                               # %._crit_edge140
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	testl	%r8d, %r8d
	jle	.LBB31_9
# BB#10:
	movslq	%r8d, %rdi
	shlq	$2, %rdi
.Ltmp155:
	movl	$16, %esi
	movq	%rdi, %rbx
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r12
.Ltmp156:
# BB#11:                                # %.lr.ph.i72
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	memset
	movl	8(%rsp), %r8d           # 4-byte Reload
	jmp	.LBB31_12
.LBB31_9:
	xorl	%r12d, %r12d
.LBB31_12:                              # %_ZN20btAlignedObjectArrayIjE6resizeEiRKj.exit
	movl	%r8d, %edx
	shlq	$2, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
	movl	$0, (%r15)
	movl	12(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB31_26
# BB#13:                                # %.lr.ph136
	movl	%eax, %ebx
	xorl	%ecx, %ecx
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	24(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB31_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_23 Depth 2
	movslq	(%r14,%rcx,4), %r10
	movl	(%r12,%r10,4), %eax
	testl	%eax, %eax
	je	.LBB31_18
# BB#15:                                #   in Loop: Header=BB31_14 Depth=1
	decl	%eax
	movl	%eax, (%r14,%rcx,4)
	jmp	.LBB31_16
	.p2align	4, 0x90
.LBB31_18:                              #   in Loop: Header=BB31_14 Depth=1
	movq	%rbx, %r12
	movl	%r10d, %esi
	movl	(%r15), %eax
	movq	%r14, %r8
	movl	%eax, (%r14,%rcx,4)
	movq	%rsi, %rdi
	shlq	$4, %rdi
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	(%r14,%rdi), %r9d
	movl	(%r15), %edx
	movq	%rdx, %rax
	shlq	$4, %rax
	movl	%r9d, (%r11,%rax)
	movl	4(%r14,%rdi), %ebx
	movl	%ebx, 4(%r11,%rax)
	movl	8(%r14,%rdi), %edi
	movl	%edi, 8(%r11,%rax)
	movl	36(%rbp), %edi
	testl	%edi, %edi
	jle	.LBB31_19
# BB#22:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB31_14 Depth=1
	xorl	%eax, %eax
	movq	%r8, %r14
	movq	%r12, %rbx
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB31_23:                              # %.lr.ph
                                        #   Parent Loop BB31_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%esi, (%r13,%rax,4)
	jne	.LBB31_25
# BB#24:                                #   in Loop: Header=BB31_23 Depth=2
	movl	(%r15), %edx
	movq	48(%rbp), %rdi
	movl	%edx, (%rdi,%rax,4)
	movl	36(%rbp), %edi
.LBB31_25:                              #   in Loop: Header=BB31_23 Depth=2
	incq	%rax
	movslq	%edi, %rdx
	cmpq	%rdx, %rax
	jl	.LBB31_23
# BB#20:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB31_14 Depth=1
	movl	(%r15), %edx
	jmp	.LBB31_21
.LBB31_19:                              #   in Loop: Header=BB31_14 Depth=1
	movq	%r8, %r14
	movq	%r12, %rbx
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB31_21:                              # %._crit_edge
                                        #   in Loop: Header=BB31_14 Depth=1
	incl	%edx
	movl	%edx, (%r15)
	movl	%edx, (%r12,%r10,4)
.LBB31_16:                              #   in Loop: Header=BB31_14 Depth=1
	incq	%rcx
	cmpq	%rbx, %rcx
	jne	.LBB31_14
	jmp	.LBB31_27
.LBB31_26:                              # %._crit_edge137
	testq	%r12, %r12
	je	.LBB31_28
.LBB31_27:                              # %._crit_edge137.thread
.Ltmp158:
	movq	%r12, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp159:
.LBB31_28:                              # %_ZN20btAlignedObjectArrayIjED2Ev.exit
	testq	%r13, %r13
	je	.LBB31_29
# BB#36:
	movq	%r13, %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB31_29:                              # %_ZN20btAlignedObjectArrayIiED2Ev.exit57
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB31_17:                              # %_ZN20btAlignedObjectArrayIjED2Ev.exit62
.Ltmp157:
	jmp	.LBB31_31
.LBB31_35:                              # %.thread
.Ltmp154:
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB31_30:
.Ltmp160:
.LBB31_31:
	movq	%rax, %rbp
	testq	%r13, %r13
	je	.LBB31_33
# BB#32:
.Ltmp161:
	movq	%r13, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp162:
.LBB31_33:                              # %_ZN20btAlignedObjectArrayIiED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB31_34:
.Ltmp163:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end31:
	.size	_ZN11HullLibrary16BringOutYourDeadEPK9btVector3jPS0_RjPjj, .Lfunc_end31-_ZN11HullLibrary16BringOutYourDeadEPK9btVector3jPS0_RjPjj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp152-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp153-.Ltmp152       #   Call between .Ltmp152 and .Ltmp153
	.long	.Ltmp154-.Lfunc_begin4  #     jumps to .Ltmp154
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp155-.Ltmp153       #   Call between .Ltmp153 and .Ltmp155
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp155-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp156-.Ltmp155       #   Call between .Ltmp155 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin4  #     jumps to .Ltmp157
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp158-.Ltmp156       #   Call between .Ltmp156 and .Ltmp158
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp159-.Ltmp158       #   Call between .Ltmp158 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin4  #     jumps to .Ltmp160
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp161-.Ltmp159       #   Call between .Ltmp159 and .Ltmp161
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp162-.Ltmp161       #   Call between .Ltmp161 and .Ltmp162
	.long	.Ltmp163-.Lfunc_begin4  #     jumps to .Ltmp163
	.byte	1                       #   On action: 1
	.long	.Ltmp162-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Lfunc_end31-.Ltmp162   #   Call between .Ltmp162 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN11HullLibrary13ReleaseResultER10HullResult
	.p2align	4, 0x90
	.type	_ZN11HullLibrary13ReleaseResultER10HullResult,@function
_ZN11HullLibrary13ReleaseResultER10HullResult: # @_ZN11HullLibrary13ReleaseResultER10HullResult
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi183:
	.cfi_def_cfa_offset 16
.Lcfi184:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	cmpl	$0, 12(%rbx)
	je	.LBB32_6
# BB#1:
	movl	$0, 4(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_5
# BB#2:
	cmpb	$0, 32(%rbx)
	je	.LBB32_4
# BB#3:
	callq	_Z21btAlignedFreeInternalPv
.LBB32_4:
	movq	$0, 24(%rbx)
.LBB32_5:                               # %_ZN20btAlignedObjectArrayI9btVector3E5clearEv.exit
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
.LBB32_6:
	cmpl	$0, 52(%rbx)
	je	.LBB32_12
# BB#7:
	movl	$0, 44(%rbx)
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_11
# BB#8:
	cmpb	$0, 72(%rbx)
	je	.LBB32_10
# BB#9:
	callq	_Z21btAlignedFreeInternalPv
.LBB32_10:
	movq	$0, 64(%rbx)
.LBB32_11:                              # %_ZN20btAlignedObjectArrayIjE5clearEv.exit
	movb	$1, 72(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 52(%rbx)
.LBB32_12:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end32:
	.size	_ZN11HullLibrary13ReleaseResultER10HullResult, .Lfunc_end32-_ZN11HullLibrary13ReleaseResultER10HullResult
	.cfi_endproc

	.globl	_Z7GetDistfffPKf
	.p2align	4, 0x90
	.type	_Z7GetDistfffPKf,@function
_Z7GetDistfffPKf:                       # @_Z7GetDistfffPKf
	.cfi_startproc
# BB#0:
	subss	(%rdi), %xmm0
	subss	4(%rdi), %xmm1
	subss	8(%rdi), %xmm2
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm1, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm2, %xmm0
	retq
.Lfunc_end33:
	.size	_Z7GetDistfffPKf, .Lfunc_end33-_Z7GetDistfffPKf
	.cfi_endproc

	.type	_ZZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif,@object # @_ZZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif
	.local	_ZZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif
	.comm	_ZZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif,16,8
	.type	_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif,@object # @_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif
	.local	_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif
	.comm	_ZGVZ21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_E3dif,8,8
	.type	_ZZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp,@object # @_ZZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp
	.local	_ZZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp
	.comm	_ZZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp,16,8
	.type	_ZGVZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp,@object # @_ZGVZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp
	.local	_ZGVZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp
	.comm	_ZGVZ20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_E2cp,8,8
	.type	planetestepsilon,@object # @planetestepsilon
	.data
	.globl	planetestepsilon
	.p2align	2
planetestepsilon:
	.long	981668463               # float 0.00100000005
	.size	planetestepsilon, 4

	.type	_ZZN14btHullTriangle4neibEiiE2er,@object # @_ZZN14btHullTriangle4neibEiiE2er
	.p2align	2
_ZZN14btHullTriangle4neibEiiE2er:
	.long	4294967295              # 0xffffffff
	.size	_ZZN14btHullTriangle4neibEiiE2er, 4


	.globl	_ZN7ConvexHC1Eiii
	.type	_ZN7ConvexHC1Eiii,@function
_ZN7ConvexHC1Eiii = _ZN7ConvexHC2Eiii
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
