	.text
	.file	"hcg.bc"
	.globl	AllocHCG
	.p2align	4, 0x90
	.type	AllocHCG,@function
AllocHCG:                               # @AllocHCG
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	channelNets(%rip), %rbx
	leaq	(%rbx,%rbx,2), %rax
	leaq	24(,%rax,8), %rdi
	callq	malloc
	movq	%rax, HCG(%rip)
	incq	%rbx
	imulq	%rbx, %rbx
	leaq	(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, storageRootHCG(%rip)
	movq	%rax, storageHCG(%rip)
	movq	%rbx, storageLimitHCG(%rip)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	AllocHCG, .Lfunc_end0-AllocHCG
	.cfi_endproc

	.globl	FreeHCG
	.p2align	4, 0x90
	.type	FreeHCG,@function
FreeHCG:                                # @FreeHCG
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movq	HCG(%rip), %rdi
	callq	free
	movq	storageRootHCG(%rip), %rdi
	callq	free
	movq	$0, storageLimitHCG(%rip)
	popq	%rax
	retq
.Lfunc_end1:
	.size	FreeHCG, .Lfunc_end1-FreeHCG
	.cfi_endproc

	.globl	BuildHCG
	.p2align	4, 0x90
	.type	BuildHCG,@function
BuildHCG:                               # @BuildHCG
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	channelNets(%rip), %r15
	leaq	(%r15,%r15,2), %rax
	leaq	24(,%rax,8), %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, HCG(%rip)
	leaq	1(%r15), %rbx
	imulq	%rbx, %rbx
	leaq	(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, storageRootHCG(%rip)
	movq	%rax, storageHCG(%rip)
	movq	%rbx, storageLimitHCG(%rip)
	testq	%r15, %r15
	je	.LBB2_13
# BB#1:                                 # %.lr.ph54.preheader
	movl	$1, %r8d
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%r15d, %r15d
	xorl	%eax, %eax
.LBB2_11:                               # %._crit_edge50
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	HCG(%rip), %r14
	movq	%rax, 8(%r14,%r9,8)
	incq	%r8
	cmpq	%r15, %r8
	ja	.LBB2_13
# BB#12:                                # %._crit_edge50..lr.ph54_crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	storageHCG(%rip), %rax
.LBB2_2:                                # %.lr.ph54
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
	movq	FIRST(%rip), %rcx
	movq	(%rcx,%r8,8), %rsi
	movq	LAST(%rip), %rcx
	movq	(%rcx,%r8,8), %rdi
	leaq	(%r8,%r8,2), %r9
	movq	%rax, (%r14,%r9,8)
	testq	%r15, %r15
	je	.LBB2_3
# BB#4:                                 # %.lr.ph49.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	FIRST(%rip), %r10
	movl	$1, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph49
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10,%rdx,8), %rbx
	cmpq	%rsi, %rbx
	jae	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=2
	movq	LAST(%rip), %rcx
	cmpq	%rsi, (%rcx,%rdx,8)
	jb	.LBB2_10
.LBB2_7:                                #   in Loop: Header=BB2_5 Depth=2
	cmpq	%rdi, %rbx
	jbe	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_5 Depth=2
	movq	LAST(%rip), %rcx
	cmpq	%rdi, (%rcx,%rdx,8)
	ja	.LBB2_10
.LBB2_9:                                # %._crit_edge
                                        #   in Loop: Header=BB2_5 Depth=2
	movq	HCG(%rip), %rcx
	movq	(%rcx,%r9,8), %rcx
	movq	%rdx, (%rcx,%rax,8)
	addq	$8, storageHCG(%rip)
	decq	storageLimitHCG(%rip)
	incq	%rax
	movq	channelNets(%rip), %r15
.LBB2_10:                               #   in Loop: Header=BB2_5 Depth=2
	incq	%rdx
	cmpq	%r15, %rdx
	jbe	.LBB2_5
	jmp	.LBB2_11
.LBB2_13:                               # %._crit_edge55
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	BuildHCG, .Lfunc_end2-BuildHCG
	.cfi_endproc

	.globl	DFSClearHCG
	.p2align	4, 0x90
	.type	DFSClearHCG,@function
DFSClearHCG:                            # @DFSClearHCG
	.cfi_startproc
# BB#0:
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_3
# BB#1:                                 # %.lr.ph.preheader
	addq	$40, %rdi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, (%rdi)
	incq	%rcx
	addq	$24, %rdi
	cmpq	%rax, %rcx
	jbe	.LBB3_2
.LBB3_3:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	DFSClearHCG, .Lfunc_end3-DFSClearHCG
	.cfi_endproc

	.globl	DumpHCG
	.p2align	4, 0x90
	.type	DumpHCG,@function
DumpHCG:                                # @DumpHCG
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpq	$0, channelNets(%rip)
	je	.LBB4_6
# BB#1:                                 # %.lr.ph16.preheader
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph16
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	leaq	(%r15,%r15,2), %rax
	cmpq	$0, 8(%r14,%rax,8)
	je	.LBB4_5
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_2 Depth=1
	leaq	8(%r14,%rax,8), %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%r12), %rax
	movq	(%rax,%rbx,8), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	cmpq	(%r12), %rbx
	jb	.LBB4_4
.LBB4_5:                                # %._crit_edge
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	incq	%r15
	cmpq	channelNets(%rip), %r15
	jbe	.LBB4_2
.LBB4_6:                                # %._crit_edge17
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	DumpHCG, .Lfunc_end4-DumpHCG
	.cfi_endproc

	.globl	NoHCV
	.p2align	4, 0x90
	.type	NoHCV,@function
NoHCV:                                  # @NoHCV
	.cfi_startproc
# BB#0:
	cmpq	$0, channelTracks(%rip)
	je	.LBB5_13
# BB#1:                                 # %.preheader28.lr.ph
	leaq	(%rsi,%rsi,2), %rax
	leaq	8(%rdi,%rax,8), %r8
	movl	$1, %r10d
	.p2align	4, 0x90
.LBB5_2:                                # %.preheader28
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
                                        #       Child Loop BB5_8 Depth 3
	movq	channelNets(%rip), %r9
	testq	%r9, %r9
	je	.LBB5_11
# BB#3:                                 # %.lr.ph32.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	$1, %eax
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph32
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_8 Depth 3
	cmpq	%r10, (%rdx,%rax,8)
	jne	.LBB5_10
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB5_4 Depth=2
	movq	(%r8), %r11
	testq	%r11, %r11
	je	.LBB5_10
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_4 Depth=2
	movq	-8(%r8), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_8:                                #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rax, (%rsi,%rdi,8)
	je	.LBB5_9
# BB#7:                                 #   in Loop: Header=BB5_8 Depth=3
	incq	%rdi
	cmpq	%r11, %rdi
	jb	.LBB5_8
	.p2align	4, 0x90
.LBB5_10:                               # %.loopexit
                                        #   in Loop: Header=BB5_4 Depth=2
	incq	%rax
	cmpq	%r9, %rax
	jbe	.LBB5_4
	.p2align	4, 0x90
.LBB5_11:                               #   in Loop: Header=BB5_2 Depth=1
	movl	$1, %eax
	jmp	.LBB5_12
	.p2align	4, 0x90
.LBB5_9:                                #   in Loop: Header=BB5_2 Depth=1
	xorl	%eax, %eax
.LBB5_12:                               # %.thread
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rax, (%rcx,%r10,8)
	incq	%r10
	cmpq	channelTracks(%rip), %r10
	jbe	.LBB5_2
.LBB5_13:                               # %._crit_edge
	retq
.Lfunc_end5:
	.size	NoHCV, .Lfunc_end5-NoHCV
	.cfi_endproc

	.type	HCG,@object             # @HCG
	.comm	HCG,8,8
	.type	storageRootHCG,@object  # @storageRootHCG
	.comm	storageRootHCG,8,8
	.type	storageHCG,@object      # @storageHCG
	.comm	storageHCG,8,8
	.type	storageLimitHCG,@object # @storageLimitHCG
	.comm	storageLimitHCG,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"[%d]\n"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d "
	.size	.L.str.1, 4

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"\n"
	.size	.Lstr, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
