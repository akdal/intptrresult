	.text
	.file	"TarIn.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.byte	84                      # 0x54
	.byte	104                     # 0x68
	.byte	101                     # 0x65
	.byte	114                     # 0x72
	.byte	101                     # 0x65
	.byte	32                      # 0x20
	.byte	97                      # 0x61
	.byte	114                     # 0x72
	.byte	101                     # 0x65
	.byte	32                      # 0x20
	.byte	100                     # 0x64
	.byte	97                      # 0x61
	.byte	116                     # 0x74
	.byte	97                      # 0x61
	.byte	32                      # 0x20
	.byte	97                      # 0x61
.LCPI0_1:
	.byte	102                     # 0x66
	.byte	116                     # 0x74
	.byte	101                     # 0x65
	.byte	114                     # 0x72
	.byte	32                      # 0x20
	.byte	101                     # 0x65
	.byte	110                     # 0x6e
	.byte	100                     # 0x64
	.byte	32                      # 0x20
	.byte	111                     # 0x6f
	.byte	102                     # 0x66
	.byte	32                      # 0x20
	.byte	97                      # 0x61
	.byte	114                     # 0x72
	.byte	99                      # 0x63
	.byte	104                     # 0x68
.LCPI0_2:
	.byte	84                      # 0x54
	.byte	104                     # 0x68
	.byte	101                     # 0x65
	.byte	114                     # 0x72
	.byte	101                     # 0x65
	.byte	32                      # 0x20
	.byte	97                      # 0x61
	.byte	114                     # 0x72
	.byte	101                     # 0x65
	.byte	32                      # 0x20
	.byte	110                     # 0x6e
	.byte	111                     # 0x6f
	.byte	32                      # 0x20
	.byte	116                     # 0x74
	.byte	114                     # 0x72
	.byte	97                      # 0x61
.LCPI0_3:
	.byte	105                     # 0x69
	.byte	108                     # 0x6c
	.byte	105                     # 0x69
	.byte	110                     # 0x6e
	.byte	103                     # 0x67
	.byte	32                      # 0x20
	.byte	122                     # 0x7a
	.byte	101                     # 0x65
	.byte	114                     # 0x72
	.byte	111                     # 0x6f
	.byte	45                      # 0x2d
	.byte	102                     # 0x66
	.byte	105                     # 0x69
	.byte	108                     # 0x6c
	.byte	108                     # 0x6c
	.byte	101                     # 0x65
.LCPI0_4:
	.byte	84                      # 0x54
	.byte	104                     # 0x68
	.byte	101                     # 0x65
	.byte	114                     # 0x72
	.byte	101                     # 0x65
	.byte	32                      # 0x20
	.byte	105                     # 0x69
	.byte	115                     # 0x73
	.byte	32                      # 0x20
	.byte	110                     # 0x6e
	.byte	111                     # 0x6f
	.byte	32                      # 0x20
	.byte	99                      # 0x63
	.byte	111                     # 0x6f
	.byte	114                     # 0x72
	.byte	114                     # 0x72
.LCPI0_5:
	.byte	101                     # 0x65
	.byte	99                      # 0x63
	.byte	116                     # 0x74
	.byte	32                      # 0x20
	.byte	114                     # 0x72
	.byte	101                     # 0x65
	.byte	99                      # 0x63
	.byte	111                     # 0x6f
	.byte	114                     # 0x72
	.byte	100                     # 0x64
	.byte	32                      # 0x20
	.byte	97                      # 0x61
	.byte	116                     # 0x74
	.byte	32                      # 0x20
	.byte	116                     # 0x74
	.byte	104                     # 0x68
.LCPI0_6:
	.zero	16
	.text
	.globl	_ZN8NArchive4NTar8ReadItemEP19ISequentialInStreamRbRNS0_7CItemExER11CStringBaseIcE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8ReadItemEP19ISequentialInStreamRbRNS0_7CItemExER11CStringBaseIcE,@function
_ZN8NArchive4NTar8ReadItemEP19ISequentialInStreamRbRNS0_7CItemExER11CStringBaseIcE: # @_ZN8NArchive4NTar8ReadItemEP19ISequentialInStreamRbRNS0_7CItemExER11CStringBaseIcE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1240, %rsp             # imm = 0x4D8
.Lcfi6:
	.cfi_def_cfa_offset 1296
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	movq	%rdi, %r13
	movl	$0, 120(%r12)
	movq	$0, 72(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbp
	movq	%rbp, 64(%rsp)
	movb	$0, (%rbp)
	movl	$4, 76(%rsp)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 32(%rsp)
.Ltmp0:
	movl	$4, %edi
	callq	_Znam
.Ltmp1:
# BB#1:                                 # %_ZN11CStringBaseIcEC2Ev.exit
	movq	%rax, 32(%rsp)
	movb	$0, (%rax)
	movl	$4, 44(%rsp)
	xorl	%eax, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	208(%rsp), %rbx
	xorl	%eax, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	%r12, 144(%rsp)         # 8-byte Spill
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_3 Depth=1
	sarq	$32, %rdx
	movb	$0, (%rax,%rdx)
	movl	%ecx, 8(%r15)
	movq	56(%rsp), %r14          # 8-byte Reload
	leaq	208(%rsp), %rbx
.LBB0_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #       Child Loop BB0_9 Depth 3
                                        #     Child Loop BB0_329 Depth 2
                                        #     Child Loop BB0_361 Depth 2
                                        #     Child Loop BB0_74 Depth 2
                                        #     Child Loop BB0_77 Depth 2
                                        #     Child Loop BB0_332 Depth 2
                                        #     Child Loop BB0_366 Depth 2
                                        #     Child Loop BB0_88 Depth 2
                                        #     Child Loop BB0_91 Depth 2
                                        #     Child Loop BB0_317 Depth 2
                                        #     Child Loop BB0_341 Depth 2
                                        #     Child Loop BB0_60 Depth 2
                                        #     Child Loop BB0_63 Depth 2
                                        #     Child Loop BB0_22 Depth 2
                                        #     Child Loop BB0_25 Depth 2
                                        #     Child Loop BB0_314 Depth 2
                                        #     Child Loop BB0_336 Depth 2
                                        #     Child Loop BB0_46 Depth 2
                                        #     Child Loop BB0_49 Depth 2
                                        #     Child Loop BB0_96 Depth 2
                                        #     Child Loop BB0_106 Depth 2
                                        #     Child Loop BB0_119 Depth 2
                                        #     Child Loop BB0_133 Depth 2
                                        #     Child Loop BB0_142 Depth 2
                                        #     Child Loop BB0_145 Depth 2
                                        #     Child Loop BB0_149 Depth 2
                                        #     Child Loop BB0_152 Depth 2
                                        #     Child Loop BB0_165 Depth 2
                                        #     Child Loop BB0_170 Depth 2
                                        #     Child Loop BB0_173 Depth 2
                                        #     Child Loop BB0_320 Depth 2
                                        #     Child Loop BB0_346 Depth 2
                                        #     Child Loop BB0_186 Depth 2
                                        #     Child Loop BB0_189 Depth 2
                                        #     Child Loop BB0_198 Depth 2
                                        #     Child Loop BB0_200 Depth 2
                                        #     Child Loop BB0_203 Depth 2
                                        #     Child Loop BB0_323 Depth 2
                                        #     Child Loop BB0_351 Depth 2
                                        #     Child Loop BB0_216 Depth 2
                                        #     Child Loop BB0_219 Depth 2
                                        #     Child Loop BB0_224 Depth 2
                                        #     Child Loop BB0_226 Depth 2
                                        #     Child Loop BB0_229 Depth 2
                                        #     Child Loop BB0_326 Depth 2
                                        #     Child Loop BB0_356 Depth 2
                                        #     Child Loop BB0_242 Depth 2
                                        #     Child Loop BB0_245 Depth 2
                                        #     Child Loop BB0_250 Depth 2
                                        #     Child Loop BB0_260 Depth 2
                                        #     Child Loop BB0_273 Depth 2
                                        #     Child Loop BB0_279 Depth 2
                                        #     Child Loop BB0_282 Depth 2
                                        #     Child Loop BB0_287 Depth 2
                                        #     Child Loop BB0_298 Depth 2
                                        #     Child Loop BB0_370 Depth 2
                                        #     Child Loop BB0_453 Depth 2
                                        #     Child Loop BB0_457 Depth 2
                                        #     Child Loop BB0_385 Depth 2
                                        #     Child Loop BB0_388 Depth 2
                                        #     Child Loop BB0_393 Depth 2
                                        #     Child Loop BB0_404 Depth 2
                                        #     Child Loop BB0_445 Depth 2
                                        #     Child Loop BB0_449 Depth 2
                                        #     Child Loop BB0_432 Depth 2
                                        #     Child Loop BB0_435 Depth 2
                                        #     Child Loop BB0_442 Depth 2
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movb	$0, (%rax)
	movq	120(%rsp), %rax         # 8-byte Reload
	movb	$0, (%rax)
	xorl	%r15d, %r15d
                                        # implicit-def: %EBP
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_9 Depth 3
	movq	$512, 136(%rsp)         # imm = 0x200
.Ltmp3:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	leaq	136(%rsp), %rdx
	callq	_Z10ReadStreamP19ISequentialInStreamPvPm
.Ltmp4:
# BB#5:                                 # %.noexc
                                        #   in Loop: Header=BB0_4 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	je	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=2
	movl	$1, %ecx
	movl	%eax, %ebp
	andb	$3, %cl
	je	.LBB0_4
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_4 Depth=2
	movq	136(%rsp), %rax
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB0_28
# BB#8:                                 #   in Loop: Header=BB0_4 Depth=2
	addl	$512, 120(%r12)         # imm = 0x200
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_9:                                #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, 208(%rsp,%rax)
	jne	.LBB0_15
# BB#10:                                #   in Loop: Header=BB0_9 Depth=3
	cmpb	$0, 209(%rsp,%rax)
	jne	.LBB0_15
# BB#11:                                #   in Loop: Header=BB0_9 Depth=3
	cmpb	$0, 210(%rsp,%rax)
	jne	.LBB0_15
# BB#12:                                #   in Loop: Header=BB0_9 Depth=3
	cmpb	$0, 211(%rsp,%rax)
	jne	.LBB0_15
# BB#13:                                #   in Loop: Header=BB0_9 Depth=3
	addq	$4, %rax
	cmpq	$512, %rax              # imm = 0x200
	jl	.LBB0_9
# BB#14:                                #   in Loop: Header=BB0_4 Depth=2
	movb	$1, %al
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_4 Depth=2
	xorl	%eax, %eax
.LBB0_16:                               # %_ZN8NArchive4NTarL12IsRecordLastEPKc.exit.i
                                        #   in Loop: Header=BB0_4 Depth=2
	testb	%al, %al
	movb	$1, %al
	jne	.LBB0_18
# BB#17:                                # %_ZN8NArchive4NTarL12IsRecordLastEPKc.exit.i
                                        #   in Loop: Header=BB0_4 Depth=2
	movl	%r15d, %eax
.LBB0_18:                               # %_ZN8NArchive4NTarL12IsRecordLastEPKc.exit.i
                                        #   in Loop: Header=BB0_4 Depth=2
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %ecx
	movb	%al, %r15b
	andb	$3, %cl
	je	.LBB0_4
.LBB0_19:                               #   in Loop: Header=BB0_3 Depth=1
	cmpb	$2, %cl
	jne	.LBB0_408
# BB#20:                                #   in Loop: Header=BB0_3 Depth=1
	testb	$1, %r15b
	jne	.LBB0_32
# BB#21:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph.i.i149.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	207(%rsp,%rax), %ecx
	movb	%cl, 719(%rsp,%rax)
	testb	%cl, %cl
	je	.LBB0_24
# BB#23:                                # %.lr.ph.i.i149.i
                                        #   in Loop: Header=BB0_22 Depth=2
	cmpq	$100, %rax
	leaq	1(%rax), %rax
	jl	.LBB0_22
.LBB0_24:                               # %_ZN8NArchive4NTarL10ReadStringEPKciR11CStringBaseIcE.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 820(%rsp)
	movl	$0, 8(%r12)
	movq	(%r12), %rax
	movb	$0, (%rax)
	xorl	%ebx, %ebx
	leaq	720(%rsp), %rcx
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_25:                               #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_25
# BB#26:                                # %_Z11MyStringLenIcEiPKT_.exit.i152.i
                                        #   in Loop: Header=BB0_3 Depth=1
	leal	-1(%rbx), %r14d
	movl	12(%r12), %r15d
	cmpl	%ebx, %r15d
	jne	.LBB0_36
# BB#27:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i154.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	(%r12), %rbp
	jmp	.LBB0_95
	.p2align	4, 0x90
.LBB0_28:                               #   in Loop: Header=BB0_3 Depth=1
	testq	%rax, %rax
	jne	.LBB0_34
# BB#29:                                #   in Loop: Header=BB0_3 Depth=1
	testb	$1, %r15b
	jne	.LBB0_409
# BB#30:                                # %_Z11MyStringLenIcEiPKT_.exit.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movb	$0, (%rax)
	movl	12(%r14), %ebx
	cmpl	$42, %ebx
	jne	.LBB0_78
# BB#31:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	(%r14), %rbp
	jmp	.LBB0_310
.LBB0_32:                               # %_Z11MyStringLenIcEiPKT_.exit.i131.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movb	$0, (%rax)
	movl	12(%r14), %ebx
	cmpl	$36, %ebx
	jne	.LBB0_50
# BB#33:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i133.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	(%r14), %rbp
	jmp	.LBB0_193
.LBB0_34:                               # %_Z11MyStringLenIcEiPKT_.exit.i113.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movb	$0, (%rax)
	movl	12(%r14), %ebx
	cmpl	$49, %ebx
	jne	.LBB0_64
# BB#35:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i115.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	(%r14), %rbp
	jmp	.LBB0_306
.LBB0_36:                               #   in Loop: Header=BB0_3 Depth=1
	movslq	%ebx, %rdi
	cmpl	$-1, %r14d
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp8:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp9:
# BB#37:                                # %.noexc85
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%r15d, %r15d
	jle	.LBB0_94
# BB#38:                                # %.preheader.i.i155.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	8(%r12), %r9
	testq	%r9, %r9
	movq	(%r12), %rdi
	jle	.LBB0_92
# BB#39:                                # %.lr.ph.preheader.i.i156.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$31, %r9d
	jbe	.LBB0_43
# BB#40:                                # %min.iters.checked674
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB0_43
# BB#41:                                # %vector.memcheck685
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB0_312
# BB#42:                                # %vector.memcheck685
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rbp,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_312
.LBB0_43:                               #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
.LBB0_44:                               # %.lr.ph.i.i161.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r9d, %esi
	subl	%ecx, %esi
	leaq	-1(%r9), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB0_47
# BB#45:                                # %.lr.ph.i.i161.i.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph.i.i161.i.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_46
.LBB0_47:                               # %.lr.ph.i.i161.i.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$7, %r8
	jb	.LBB0_93
# BB#48:                                # %.lr.ph.i.i161.i.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	subq	%rcx, %r9
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_49:                               # %.lr.ph.i.i161.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB0_49
	jmp	.LBB0_93
.LBB0_50:                               #   in Loop: Header=BB0_3 Depth=1
.Ltmp6:
	movl	$36, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp7:
# BB#51:                                # %.noexc84
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%ebx, %ebx
	movq	56(%rsp), %r14          # 8-byte Reload
	jle	.LBB0_192
# BB#52:                                # %.preheader.i.i134.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	8(%r14), %rax
	testq	%rax, %rax
	movq	(%r14), %rdi
	jle	.LBB0_190
# BB#53:                                # %.lr.ph.preheader.i.i135.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$31, %eax
	jbe	.LBB0_57
# BB#54:                                # %min.iters.checked701
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_57
# BB#55:                                # %vector.memcheck712
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB0_315
# BB#56:                                # %vector.memcheck712
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_315
.LBB0_57:                               #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
.LBB0_58:                               # %.lr.ph.i.i140.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_61
# BB#59:                                # %.lr.ph.i.i140.i.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_60:                               # %.lr.ph.i.i140.i.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_60
.LBB0_61:                               # %.lr.ph.i.i140.i.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$7, %rdx
	jb	.LBB0_191
# BB#62:                                # %.lr.ph.i.i140.i.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	subq	%rcx, %rax
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_63:                               # %.lr.ph.i.i140.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_63
	jmp	.LBB0_191
.LBB0_64:                               #   in Loop: Header=BB0_3 Depth=1
.Ltmp57:
	movl	$49, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp58:
# BB#65:                                # %.noexc83
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%ebx, %ebx
	movq	56(%rsp), %r14          # 8-byte Reload
	jle	.LBB0_305
# BB#66:                                # %.preheader.i.i116.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	8(%r14), %rax
	testq	%rax, %rax
	movq	(%r14), %rdi
	jle	.LBB0_303
# BB#67:                                # %.lr.ph.preheader.i.i117.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$31, %eax
	jbe	.LBB0_71
# BB#68:                                # %min.iters.checked493
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_71
# BB#69:                                # %vector.memcheck504
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB0_327
# BB#70:                                # %vector.memcheck504
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_327
.LBB0_71:                               #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
.LBB0_72:                               # %.lr.ph.i.i122.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_75
# BB#73:                                # %.lr.ph.i.i122.i.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_74:                               # %.lr.ph.i.i122.i.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_74
.LBB0_75:                               # %.lr.ph.i.i122.i.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$7, %rdx
	jb	.LBB0_304
# BB#76:                                # %.lr.ph.i.i122.i.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	subq	%rcx, %rax
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_77:                               # %.lr.ph.i.i122.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_77
	jmp	.LBB0_304
.LBB0_78:                               #   in Loop: Header=BB0_3 Depth=1
.Ltmp55:
	movl	$42, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp56:
# BB#79:                                # %.noexc82
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%ebx, %ebx
	movq	56(%rsp), %r14          # 8-byte Reload
	jle	.LBB0_309
# BB#80:                                # %.preheader.i.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	8(%r14), %rax
	testq	%rax, %rax
	movq	(%r14), %rdi
	jle	.LBB0_307
# BB#81:                                # %.lr.ph.preheader.i.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$31, %eax
	jbe	.LBB0_85
# BB#82:                                # %min.iters.checked520
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_85
# BB#83:                                # %vector.memcheck531
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB0_330
# BB#84:                                # %vector.memcheck531
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_330
.LBB0_85:                               #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
.LBB0_86:                               # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_89
# BB#87:                                # %.lr.ph.i.i.i.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_88:                               # %.lr.ph.i.i.i.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_88
.LBB0_89:                               # %.lr.ph.i.i.i.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$7, %rdx
	jb	.LBB0_308
# BB#90:                                # %.lr.ph.i.i.i.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	subq	%rcx, %rax
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_91:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_91
	jmp	.LBB0_308
.LBB0_92:                               # %._crit_edge.i.i157.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_94
.LBB0_93:                               # %._crit_edge.thread.i.i162.i
                                        #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
.LBB0_94:                               # %._crit_edge17.i.i163.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, (%r12)
	movslq	8(%r12), %rax
	movb	$0, (%rbp,%rax)
	movl	%ebx, 12(%r12)
	leaq	720(%rsp), %rcx
.LBB0_95:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i166.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_96:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i166.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB0_96
# BB#97:                                # %.lr.ph.i.i.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r14d, 8(%r12)
	movb	308(%rsp), %al
	movb	%al, (%rsp)
	testb	%al, %al
	je	.LBB0_105
# BB#98:                                # %.lr.ph.i.i.i.i.1
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	309(%rsp), %al
	movb	%al, 1(%rsp)
	testb	%al, %al
	je	.LBB0_105
# BB#99:                                # %.lr.ph.i.i.i.i.2
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	310(%rsp), %al
	movb	%al, 2(%rsp)
	testb	%al, %al
	je	.LBB0_105
# BB#100:                               # %.lr.ph.i.i.i.i.3
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	311(%rsp), %al
	movb	%al, 3(%rsp)
	testb	%al, %al
	je	.LBB0_105
# BB#101:                               # %.lr.ph.i.i.i.i.4
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	312(%rsp), %al
	movb	%al, 4(%rsp)
	testb	%al, %al
	je	.LBB0_105
# BB#102:                               # %.lr.ph.i.i.i.i.5
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	313(%rsp), %al
	movb	%al, 5(%rsp)
	testb	%al, %al
	je	.LBB0_105
# BB#103:                               # %.lr.ph.i.i.i.i.6
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	314(%rsp), %al
	movb	%al, 6(%rsp)
	testb	%al, %al
	je	.LBB0_105
# BB#104:                               # %.lr.ph.i.i.i.i.7
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	315(%rsp), %al
	movb	%al, 7(%rsp)
.LBB0_105:                              # %_ZN8NArchive4NTarL9MyStrNCpyEPcPKci.exit.i.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 8(%rsp)
	leaq	-1(%rsp), %rdi
	.p2align	4, 0x90
.LBB0_106:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$32, 1(%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB0_106
# BB#107:                               # %_ZN8NArchive4NTarL13OctalToNumberEPKciRy.exit.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
.Ltmp10:
	leaq	152(%rsp), %rsi
	callq	_Z24ConvertOctStringToUInt64PKcPS0_
.Ltmp11:
# BB#108:                               # %.noexc86
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	152(%rsp), %rcx
	movb	(%rcx), %cl
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB0_460
# BB#109:                               # %_ZN8NArchive4NTarL15OctalToNumber32EPKciRj.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, 24(%r12)
	shrq	$32, %rax
	jne	.LBB0_460
# BB#110:                               # %.lr.ph.i.i.i174.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	316(%rsp), %al
	movb	%al, (%rsp)
	testb	%al, %al
	je	.LBB0_118
# BB#111:                               # %.lr.ph.i.i.i174.i.1
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	317(%rsp), %al
	movb	%al, 1(%rsp)
	testb	%al, %al
	je	.LBB0_118
# BB#112:                               # %.lr.ph.i.i.i174.i.2
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	318(%rsp), %al
	movb	%al, 2(%rsp)
	testb	%al, %al
	je	.LBB0_118
# BB#113:                               # %.lr.ph.i.i.i174.i.3
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	319(%rsp), %al
	movb	%al, 3(%rsp)
	testb	%al, %al
	je	.LBB0_118
# BB#114:                               # %.lr.ph.i.i.i174.i.4
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	320(%rsp), %al
	movb	%al, 4(%rsp)
	testb	%al, %al
	je	.LBB0_118
# BB#115:                               # %.lr.ph.i.i.i174.i.5
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	321(%rsp), %al
	movb	%al, 5(%rsp)
	testb	%al, %al
	je	.LBB0_118
# BB#116:                               # %.lr.ph.i.i.i174.i.6
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	322(%rsp), %al
	movb	%al, 6(%rsp)
	testb	%al, %al
	je	.LBB0_118
# BB#117:                               # %.lr.ph.i.i.i174.i.7
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	323(%rsp), %al
	movb	%al, 7(%rsp)
.LBB0_118:                              # %_ZN8NArchive4NTarL9MyStrNCpyEPcPKci.exit.i.i175.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 8(%rsp)
	leaq	-1(%rsp), %rdi
	.p2align	4, 0x90
.LBB0_119:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$32, 1(%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB0_119
# BB#120:                               # %_ZN8NArchive4NTarL13OctalToNumberEPKciRy.exit.i178.i
                                        #   in Loop: Header=BB0_3 Depth=1
.Ltmp12:
	leaq	160(%rsp), %rsi
	callq	_Z24ConvertOctStringToUInt64PKcPS0_
.Ltmp13:
# BB#121:                               # %.noexc87
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	160(%rsp), %rcx
	movb	(%rcx), %cl
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB0_123
# BB#122:                               # %_ZN8NArchive4NTarL15OctalToNumber32EPKciRj.exit180.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, %rcx
	shrq	$32, %rcx
	je	.LBB0_124
.LBB0_123:                              # %_ZN8NArchive4NTarL15OctalToNumber32EPKciRj.exit180.thread.i
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
.LBB0_124:                              # %.lr.ph.i.i.i185.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, 28(%r12)
	movb	324(%rsp), %al
	movb	%al, (%rsp)
	testb	%al, %al
	je	.LBB0_132
# BB#125:                               # %.lr.ph.i.i.i185.i.1
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	325(%rsp), %al
	movb	%al, 1(%rsp)
	testb	%al, %al
	je	.LBB0_132
# BB#126:                               # %.lr.ph.i.i.i185.i.2
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	326(%rsp), %al
	movb	%al, 2(%rsp)
	testb	%al, %al
	je	.LBB0_132
# BB#127:                               # %.lr.ph.i.i.i185.i.3
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	327(%rsp), %al
	movb	%al, 3(%rsp)
	testb	%al, %al
	je	.LBB0_132
# BB#128:                               # %.lr.ph.i.i.i185.i.4
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	328(%rsp), %al
	movb	%al, 4(%rsp)
	testb	%al, %al
	je	.LBB0_132
# BB#129:                               # %.lr.ph.i.i.i185.i.5
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	329(%rsp), %al
	movb	%al, 5(%rsp)
	testb	%al, %al
	je	.LBB0_132
# BB#130:                               # %.lr.ph.i.i.i185.i.6
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	330(%rsp), %al
	movb	%al, 6(%rsp)
	testb	%al, %al
	je	.LBB0_132
# BB#131:                               # %.lr.ph.i.i.i185.i.7
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	331(%rsp), %al
	movb	%al, 7(%rsp)
.LBB0_132:                              # %_ZN8NArchive4NTarL9MyStrNCpyEPcPKci.exit.i.i186.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 8(%rsp)
	leaq	-1(%rsp), %rdi
	.p2align	4, 0x90
.LBB0_133:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$32, 1(%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB0_133
# BB#134:                               # %_ZN8NArchive4NTarL13OctalToNumberEPKciRy.exit.i189.i
                                        #   in Loop: Header=BB0_3 Depth=1
.Ltmp14:
	leaq	168(%rsp), %rsi
	callq	_Z24ConvertOctStringToUInt64PKcPS0_
.Ltmp15:
# BB#135:                               # %.noexc88
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	168(%rsp), %rcx
	movb	(%rcx), %cl
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB0_137
# BB#136:                               # %_ZN8NArchive4NTarL15OctalToNumber32EPKciRj.exit191.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, 32(%r12)
	shrq	$32, %rax
	je	.LBB0_138
.LBB0_137:                              # %_ZN8NArchive4NTarL15OctalToNumber32EPKciRj.exit191.thread.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$0, 32(%r12)
.LBB0_138:                              #   in Loop: Header=BB0_3 Depth=1
	movl	332(%rsp), %eax
	movl	%eax, %ecx
	shll	$24, %ecx
	movzbl	333(%rsp), %edx
	shll	$16, %edx
	orl	%ecx, %edx
	movzbl	334(%rsp), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	movzbl	335(%rsp), %edx
	orl	%ecx, %edx
	cmpl	$-2147483648, %edx      # imm = 0x80000000
	jne	.LBB0_140
# BB#139:                               #   in Loop: Header=BB0_3 Depth=1
	movl	336(%rsp), %eax
	shll	$24, %eax
	movzbl	337(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	338(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	339(%rsp), %ecx
	orl	%eax, %ecx
	shlq	$32, %rcx
	movzbl	340(%rsp), %eax
	shlq	$24, %rax
	movzbl	341(%rsp), %edx
	shlq	$16, %rdx
	orq	%rax, %rdx
	movzbl	342(%rsp), %eax
	shlq	$8, %rax
	movzbl	343(%rsp), %esi
	orq	%rcx, %rdx
	orq	%rax, %rdx
	orq	%rsi, %rdx
	movq	%rdx, 16(%r12)
	jmp	.LBB0_148
.LBB0_140:                              #   in Loop: Header=BB0_3 Depth=1
	movb	%al, (%rsp)
	testb	%al, %al
	je	.LBB0_144
# BB#141:                               # %.lr.ph.i.i196..lr.ph.i.i196_crit_edge.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_142:                              # %.lr.ph.i.i196..lr.ph.i.i196_crit_edge.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	333(%rsp,%rax), %ecx
	movb	%cl, 1(%rsp,%rax)
	testb	%cl, %cl
	je	.LBB0_144
# BB#143:                               # %.lr.ph.i.i196..lr.ph.i.i196_crit_edge.i
                                        #   in Loop: Header=BB0_142 Depth=2
	leaq	2(%rax), %rcx
	incq	%rax
	cmpq	$12, %rcx
	jl	.LBB0_142
.LBB0_144:                              # %_ZN8NArchive4NTarL9MyStrNCpyEPcPKci.exit.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 12(%rsp)
	leaq	-1(%rsp), %rdi
	.p2align	4, 0x90
.LBB0_145:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$32, 1(%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB0_145
# BB#146:                               # %_ZN8NArchive4NTarL13OctalToNumberEPKciRy.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
.Ltmp16:
	leaq	176(%rsp), %rsi
	callq	_Z24ConvertOctStringToUInt64PKcPS0_
.Ltmp17:
# BB#147:                               # %.noexc89
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, 16(%r12)
	movq	176(%rsp), %rax
	movb	(%rax), %al
	orb	$32, %al
	cmpb	$32, %al
	jne	.LBB0_460
.LBB0_148:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_149:                              # %.lr.ph.i.i.i203.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	344(%rsp,%rax), %ecx
	movb	%cl, (%rsp,%rax)
	testb	%cl, %cl
	je	.LBB0_151
# BB#150:                               # %.lr.ph.i.i.i203.i
                                        #   in Loop: Header=BB0_149 Depth=2
	incq	%rax
	cmpq	$12, %rax
	jl	.LBB0_149
.LBB0_151:                              # %_ZN8NArchive4NTarL9MyStrNCpyEPcPKci.exit.i.i204.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 12(%rsp)
	leaq	-1(%rsp), %rdi
	.p2align	4, 0x90
.LBB0_152:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$32, 1(%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB0_152
# BB#153:                               # %_ZN8NArchive4NTarL13OctalToNumberEPKciRy.exit.i207.i
                                        #   in Loop: Header=BB0_3 Depth=1
.Ltmp18:
	leaq	184(%rsp), %rsi
	callq	_Z24ConvertOctStringToUInt64PKcPS0_
.Ltmp19:
# BB#154:                               # %.noexc90
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	184(%rsp), %rcx
	movb	(%rcx), %cl
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB0_460
# BB#155:                               # %_ZN8NArchive4NTarL15OctalToNumber32EPKciRj.exit209.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, 36(%r12)
	shrq	$32, %rax
	jne	.LBB0_460
# BB#156:                               # %.lr.ph.i.i.i214.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	356(%rsp), %al
	movb	%al, (%rsp)
	testb	%al, %al
	je	.LBB0_164
# BB#157:                               # %.lr.ph.i.i.i214.i.1
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	357(%rsp), %al
	movb	%al, 1(%rsp)
	testb	%al, %al
	je	.LBB0_164
# BB#158:                               # %.lr.ph.i.i.i214.i.2
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	358(%rsp), %al
	movb	%al, 2(%rsp)
	testb	%al, %al
	je	.LBB0_164
# BB#159:                               # %.lr.ph.i.i.i214.i.3
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	359(%rsp), %al
	movb	%al, 3(%rsp)
	testb	%al, %al
	je	.LBB0_164
# BB#160:                               # %.lr.ph.i.i.i214.i.4
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	360(%rsp), %al
	movb	%al, 4(%rsp)
	testb	%al, %al
	je	.LBB0_164
# BB#161:                               # %.lr.ph.i.i.i214.i.5
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	361(%rsp), %al
	movb	%al, 5(%rsp)
	testb	%al, %al
	je	.LBB0_164
# BB#162:                               # %.lr.ph.i.i.i214.i.6
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	362(%rsp), %al
	movb	%al, 6(%rsp)
	testb	%al, %al
	je	.LBB0_164
# BB#163:                               # %.lr.ph.i.i.i214.i.7
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	363(%rsp), %al
	movb	%al, 7(%rsp)
.LBB0_164:                              # %_ZN8NArchive4NTarL9MyStrNCpyEPcPKci.exit.i.i215.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 8(%rsp)
	leaq	-1(%rsp), %rdi
	.p2align	4, 0x90
.LBB0_165:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$32, 1(%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB0_165
# BB#166:                               # %_ZN8NArchive4NTarL13OctalToNumberEPKciRy.exit.i218.i
                                        #   in Loop: Header=BB0_3 Depth=1
.Ltmp20:
	leaq	192(%rsp), %rsi
	callq	_Z24ConvertOctStringToUInt64PKcPS0_
	movq	%rax, 128(%rsp)         # 8-byte Spill
.Ltmp21:
# BB#167:                               # %.noexc91
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	192(%rsp), %rax
	movb	(%rax), %al
	orb	$32, %al
	cmpb	$32, %al
	jne	.LBB0_460
# BB#168:                               # %_ZN8NArchive4NTarL15OctalToNumber32EPKciRj.exit220.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	shrq	$32, %rax
	jne	.LBB0_460
# BB#169:                               #   in Loop: Header=BB0_3 Depth=1
	movq	_ZN8NArchive4NTar11NFileHeader15kCheckSumBlanksE(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, 356(%rsp)
	movb	364(%rsp), %al
	movb	%al, 104(%r12)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_170:                              # %.lr.ph.i.i225.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	365(%rsp,%rax), %ecx
	movb	%cl, 720(%rsp,%rax)
	testb	%cl, %cl
	je	.LBB0_172
# BB#171:                               # %.lr.ph.i.i225.i
                                        #   in Loop: Header=BB0_170 Depth=2
	incq	%rax
	cmpq	$100, %rax
	jl	.LBB0_170
.LBB0_172:                              # %_ZN8NArchive4NTarL10ReadStringEPKciR11CStringBaseIcE.exit227.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 820(%rsp)
	movl	$0, 56(%r12)
	movq	48(%r12), %rax
	movb	$0, (%rax)
	xorl	%ebx, %ebx
	leaq	720(%rsp), %rcx
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_173:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_173
# BB#174:                               # %_Z11MyStringLenIcEiPKT_.exit.i230.i
                                        #   in Loop: Header=BB0_3 Depth=1
	leal	-1(%rbx), %r14d
	movl	60(%r12), %r15d
	cmpl	%ebx, %r15d
	jne	.LBB0_176
# BB#175:                               # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i232.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	48(%r12), %rbp
	jmp	.LBB0_197
.LBB0_176:                              #   in Loop: Header=BB0_3 Depth=1
	movslq	%ebx, %rdi
	cmpl	$-1, %r14d
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp22:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp23:
# BB#177:                               # %.noexc92
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%r15d, %r15d
	jle	.LBB0_196
# BB#178:                               # %.preheader.i.i233.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	56(%r12), %r9
	testq	%r9, %r9
	movq	48(%r12), %rdi
	jle	.LBB0_194
# BB#179:                               # %.lr.ph.preheader.i.i234.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$31, %r9d
	jbe	.LBB0_183
# BB#180:                               # %min.iters.checked647
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB0_183
# BB#181:                               # %vector.memcheck658
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB0_318
# BB#182:                               # %vector.memcheck658
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rbp,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_318
.LBB0_183:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
.LBB0_184:                              # %.lr.ph.i.i239.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r9d, %esi
	subl	%ecx, %esi
	leaq	-1(%r9), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB0_187
# BB#185:                               # %.lr.ph.i.i239.i.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_186:                              # %.lr.ph.i.i239.i.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_186
.LBB0_187:                              # %.lr.ph.i.i239.i.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$7, %r8
	jb	.LBB0_195
# BB#188:                               # %.lr.ph.i.i239.i.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	subq	%rcx, %r9
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_189:                              # %.lr.ph.i.i239.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB0_189
	jmp	.LBB0_195
.LBB0_190:                              # %._crit_edge.i.i136.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_192
.LBB0_191:                              # %._crit_edge.thread.i.i141.i
                                        #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
.LBB0_192:                              # %._crit_edge17.i.i142.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, (%r14)
	movslq	8(%r14), %rax
	movb	$0, (%rbp,%rax)
	movl	$36, 12(%r14)
.LBB0_193:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i145.preheader.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [84,104,101,114,101,32,97,114,101,32,100,97,116,97,32,97]
	movups	%xmm0, (%rbp)
	movdqa	.LCPI0_1(%rip), %xmm0   # xmm0 = [102,116,101,114,32,101,110,100,32,111,102,32,97,114,99,104]
	movdqu	%xmm0, 16(%rbp)
	movb	$105, 32(%rbp)
	movb	$118, 33(%rbp)
	movb	$101, 34(%rbp)
	movb	$0, 35(%rbp)
	movl	$35, 8(%r14)
	jmp	.LBB0_409
.LBB0_194:                              # %._crit_edge.i.i235.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_196
.LBB0_195:                              # %._crit_edge.thread.i.i240.i
                                        #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
.LBB0_196:                              # %._crit_edge17.i.i241.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, 48(%r12)
	movslq	56(%r12), %rax
	movb	$0, (%rbp,%rax)
	movl	%ebx, 60(%r12)
	leaq	720(%rsp), %rcx
.LBB0_197:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i244.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_198:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i244.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB0_198
# BB#199:                               # %_ZN11CStringBaseIcEaSEPKc.exit245.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r14d, 56(%r12)
	movq	465(%rsp), %rax
	movq	%rax, 96(%r12)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_200:                              # %.lr.ph.i.i250.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	473(%rsp,%rax), %ecx
	movb	%cl, 720(%rsp,%rax)
	testb	%cl, %cl
	je	.LBB0_202
# BB#201:                               # %.lr.ph.i.i250.i
                                        #   in Loop: Header=BB0_200 Depth=2
	incq	%rax
	cmpq	$32, %rax
	jl	.LBB0_200
.LBB0_202:                              # %_ZN8NArchive4NTarL10ReadStringEPKciR11CStringBaseIcE.exit252.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 752(%rsp)
	movl	$0, 72(%r12)
	movq	64(%r12), %rax
	movb	$0, (%rax)
	xorl	%ebx, %ebx
	leaq	720(%rsp), %rcx
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_203:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_203
# BB#204:                               # %_Z11MyStringLenIcEiPKT_.exit.i255.i
                                        #   in Loop: Header=BB0_3 Depth=1
	leal	-1(%rbx), %r14d
	movl	76(%r12), %r15d
	cmpl	%ebx, %r15d
	jne	.LBB0_206
# BB#205:                               # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i257.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	64(%r12), %rbp
	jmp	.LBB0_223
.LBB0_206:                              #   in Loop: Header=BB0_3 Depth=1
	movslq	%ebx, %rdi
	cmpl	$-1, %r14d
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp24:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp25:
# BB#207:                               # %.noexc93
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%r15d, %r15d
	jle	.LBB0_222
# BB#208:                               # %.preheader.i.i258.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	72(%r12), %r9
	testq	%r9, %r9
	movq	64(%r12), %rdi
	jle	.LBB0_220
# BB#209:                               # %.lr.ph.preheader.i.i259.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$31, %r9d
	jbe	.LBB0_213
# BB#210:                               # %min.iters.checked620
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB0_213
# BB#211:                               # %vector.memcheck631
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB0_321
# BB#212:                               # %vector.memcheck631
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rbp,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_321
.LBB0_213:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
.LBB0_214:                              # %.lr.ph.i.i264.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r9d, %esi
	subl	%ecx, %esi
	leaq	-1(%r9), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB0_217
# BB#215:                               # %.lr.ph.i.i264.i.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_216:                              # %.lr.ph.i.i264.i.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_216
.LBB0_217:                              # %.lr.ph.i.i264.i.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$7, %r8
	jb	.LBB0_221
# BB#218:                               # %.lr.ph.i.i264.i.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	subq	%rcx, %r9
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_219:                              # %.lr.ph.i.i264.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB0_219
	jmp	.LBB0_221
.LBB0_220:                              # %._crit_edge.i.i260.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_222
.LBB0_221:                              # %._crit_edge.thread.i.i265.i
                                        #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
.LBB0_222:                              # %._crit_edge17.i.i266.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, 64(%r12)
	movslq	72(%r12), %rax
	movb	$0, (%rbp,%rax)
	movl	%ebx, 76(%r12)
	leaq	720(%rsp), %rcx
.LBB0_223:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i269.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_224:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i269.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB0_224
# BB#225:                               # %_ZN11CStringBaseIcEaSEPKc.exit270.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r14d, 72(%r12)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_226:                              # %.lr.ph.i.i275.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	505(%rsp,%rax), %ecx
	movb	%cl, 720(%rsp,%rax)
	testb	%cl, %cl
	je	.LBB0_228
# BB#227:                               # %.lr.ph.i.i275.i
                                        #   in Loop: Header=BB0_226 Depth=2
	incq	%rax
	cmpq	$32, %rax
	jl	.LBB0_226
.LBB0_228:                              # %_ZN8NArchive4NTarL10ReadStringEPKciR11CStringBaseIcE.exit277.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 752(%rsp)
	movl	$0, 88(%r12)
	movq	80(%r12), %rax
	movb	$0, (%rax)
	xorl	%ebx, %ebx
	leaq	720(%rsp), %rcx
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_229:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_229
# BB#230:                               # %_Z11MyStringLenIcEiPKT_.exit.i280.i
                                        #   in Loop: Header=BB0_3 Depth=1
	leal	-1(%rbx), %r14d
	movl	92(%r12), %r15d
	cmpl	%ebx, %r15d
	jne	.LBB0_232
# BB#231:                               # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i282.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	80(%r12), %rbp
	jmp	.LBB0_249
.LBB0_232:                              #   in Loop: Header=BB0_3 Depth=1
	movslq	%ebx, %rdi
	cmpl	$-1, %r14d
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp26:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp27:
# BB#233:                               # %.noexc94
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%r15d, %r15d
	jle	.LBB0_248
# BB#234:                               # %.preheader.i.i283.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	88(%r12), %r9
	testq	%r9, %r9
	movq	80(%r12), %rdi
	jle	.LBB0_246
# BB#235:                               # %.lr.ph.preheader.i.i284.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$31, %r9d
	jbe	.LBB0_239
# BB#236:                               # %min.iters.checked593
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB0_239
# BB#237:                               # %vector.memcheck604
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB0_324
# BB#238:                               # %vector.memcheck604
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rbp,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_324
.LBB0_239:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
.LBB0_240:                              # %.lr.ph.i.i289.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r9d, %esi
	subl	%ecx, %esi
	leaq	-1(%r9), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB0_243
# BB#241:                               # %.lr.ph.i.i289.i.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_242:                              # %.lr.ph.i.i289.i.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_242
.LBB0_243:                              # %.lr.ph.i.i289.i.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$7, %r8
	jb	.LBB0_247
# BB#244:                               # %.lr.ph.i.i289.i.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	subq	%rcx, %r9
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_245:                              # %.lr.ph.i.i289.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB0_245
	jmp	.LBB0_247
.LBB0_246:                              # %._crit_edge.i.i285.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_248
.LBB0_247:                              # %._crit_edge.thread.i.i290.i
                                        #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
.LBB0_248:                              # %._crit_edge17.i.i291.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, 80(%r12)
	movslq	88(%r12), %rax
	movb	$0, (%rbp,%rax)
	movl	%ebx, 92(%r12)
	leaq	720(%rsp), %rcx
.LBB0_249:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i294.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_250:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i294.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB0_250
# BB#251:                               # %_ZN11CStringBaseIcEaSEPKc.exit295.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r14d, 88(%r12)
	movb	537(%rsp), %al
	testb	%al, %al
	setne	105(%r12)
	movb	%al, (%rsp)
	je	.LBB0_259
# BB#252:                               # %.lr.ph.i.i.i300..lr.ph.i.i.i300_crit_edge.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	538(%rsp), %al
	movb	%al, 1(%rsp)
	testb	%al, %al
	je	.LBB0_259
# BB#253:                               # %.lr.ph.i.i.i300..lr.ph.i.i.i300_crit_edge.i.1
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	539(%rsp), %al
	movb	%al, 2(%rsp)
	testb	%al, %al
	je	.LBB0_259
# BB#254:                               # %.lr.ph.i.i.i300..lr.ph.i.i.i300_crit_edge.i.2
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	540(%rsp), %al
	movb	%al, 3(%rsp)
	testb	%al, %al
	je	.LBB0_259
# BB#255:                               # %.lr.ph.i.i.i300..lr.ph.i.i.i300_crit_edge.i.3
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	541(%rsp), %al
	movb	%al, 4(%rsp)
	testb	%al, %al
	je	.LBB0_259
# BB#256:                               # %.lr.ph.i.i.i300..lr.ph.i.i.i300_crit_edge.i.4
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	542(%rsp), %al
	movb	%al, 5(%rsp)
	testb	%al, %al
	je	.LBB0_259
# BB#257:                               # %.lr.ph.i.i.i300..lr.ph.i.i.i300_crit_edge.i.5
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	543(%rsp), %al
	movb	%al, 6(%rsp)
	testb	%al, %al
	je	.LBB0_259
# BB#258:                               # %.lr.ph.i.i.i300..lr.ph.i.i.i300_crit_edge.i.6
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	544(%rsp), %al
	movb	%al, 7(%rsp)
.LBB0_259:                              # %_ZN8NArchive4NTarL9MyStrNCpyEPcPKci.exit.i.i301.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 8(%rsp)
	leaq	-1(%rsp), %rdi
	.p2align	4, 0x90
.LBB0_260:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$32, 1(%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB0_260
# BB#261:                               # %_ZN8NArchive4NTarL13OctalToNumberEPKciRy.exit.i304.i
                                        #   in Loop: Header=BB0_3 Depth=1
.Ltmp28:
	leaq	200(%rsp), %rsi
	callq	_Z24ConvertOctStringToUInt64PKcPS0_
.Ltmp29:
# BB#262:                               # %.noexc95
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	200(%rsp), %rcx
	movb	(%rcx), %cl
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB0_460
# BB#263:                               # %_ZN8NArchive4NTarL15OctalToNumber32EPKciRj.exit306.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, 40(%r12)
	shrq	$32, %rax
	jne	.LBB0_460
# BB#264:                               #   in Loop: Header=BB0_3 Depth=1
	movb	545(%rsp), %al
	testb	%al, %al
	setne	106(%r12)
	movb	%al, (%rsp)
	je	.LBB0_272
# BB#265:                               # %.lr.ph.i.i.i311..lr.ph.i.i.i311_crit_edge.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	546(%rsp), %al
	movb	%al, 1(%rsp)
	testb	%al, %al
	je	.LBB0_272
# BB#266:                               # %.lr.ph.i.i.i311..lr.ph.i.i.i311_crit_edge.i.1
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	547(%rsp), %al
	movb	%al, 2(%rsp)
	testb	%al, %al
	je	.LBB0_272
# BB#267:                               # %.lr.ph.i.i.i311..lr.ph.i.i.i311_crit_edge.i.2
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	548(%rsp), %al
	movb	%al, 3(%rsp)
	testb	%al, %al
	je	.LBB0_272
# BB#268:                               # %.lr.ph.i.i.i311..lr.ph.i.i.i311_crit_edge.i.3
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	549(%rsp), %al
	movb	%al, 4(%rsp)
	testb	%al, %al
	je	.LBB0_272
# BB#269:                               # %.lr.ph.i.i.i311..lr.ph.i.i.i311_crit_edge.i.4
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	550(%rsp), %al
	movb	%al, 5(%rsp)
	testb	%al, %al
	je	.LBB0_272
# BB#270:                               # %.lr.ph.i.i.i311..lr.ph.i.i.i311_crit_edge.i.5
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	551(%rsp), %al
	movb	%al, 6(%rsp)
	testb	%al, %al
	je	.LBB0_272
# BB#271:                               # %.lr.ph.i.i.i311..lr.ph.i.i.i311_crit_edge.i.6
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	552(%rsp), %al
	movb	%al, 7(%rsp)
.LBB0_272:                              # %_ZN8NArchive4NTarL9MyStrNCpyEPcPKci.exit.i.i312.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 8(%rsp)
	leaq	-1(%rsp), %rdi
	.p2align	4, 0x90
.LBB0_273:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$32, 1(%rdi)
	leaq	1(%rdi), %rdi
	je	.LBB0_273
# BB#274:                               # %_ZN8NArchive4NTarL13OctalToNumberEPKciRy.exit.i315.i
                                        #   in Loop: Header=BB0_3 Depth=1
.Ltmp30:
	leaq	80(%rsp), %rsi
	callq	_Z24ConvertOctStringToUInt64PKcPS0_
.Ltmp31:
# BB#275:                               # %.noexc96
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	80(%rsp), %rcx
	movb	(%rcx), %cl
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB0_460
# BB#276:                               # %_ZN8NArchive4NTarL15OctalToNumber32EPKciRj.exit317.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, 44(%r12)
	shrq	$32, %rax
	jne	.LBB0_460
# BB#277:                               #   in Loop: Header=BB0_3 Depth=1
.Ltmp32:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp33:
# BB#278:                               # %.noexc97
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, (%r15)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_279:                              # %.lr.ph.i.i323.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	553(%rsp,%rax), %ecx
	movb	%cl, 720(%rsp,%rax)
	testb	%cl, %cl
	je	.LBB0_281
# BB#280:                               # %.lr.ph.i.i323.i
                                        #   in Loop: Header=BB0_279 Depth=2
	incq	%rax
	cmpq	$155, %rax
	jl	.LBB0_279
.LBB0_281:                              # %_ZN8NArchive4NTarL9MyStrNCpyEPcPKci.exit.i324.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	$0, 875(%rsp)
	movb	$0, (%r15)
	movl	$-1, %ebp
	leaq	720(%rsp), %rax
	pxor	%xmm4, %xmm4
	.p2align	4, 0x90
.LBB0_282:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_282
# BB#283:                               # %_Z11MyStringLenIcEiPKT_.exit.i328.i
                                        #   in Loop: Header=BB0_3 Depth=1
	leal	1(%rbp), %r14d
	cmpl	$3, %ebp
	je	.LBB0_286
# BB#284:                               #   in Loop: Header=BB0_3 Depth=1
	movslq	%r14d, %rdi
	cmpl	$-1, %ebp
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp34:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp35:
# BB#285:                               # %._crit_edge17.i.i339.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
	movb	$0, (%rbx)
	movq	%rbx, %r15
	pxor	%xmm4, %xmm4
.LBB0_286:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i342.preheader.i
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_287:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i342.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	720(%rsp,%rax), %ecx
	movb	%cl, (%r15,%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB0_287
# BB#288:                               #   in Loop: Header=BB0_3 Depth=1
	testl	%ebp, %ebp
	je	.LBB0_401
# BB#289:                               #   in Loop: Header=BB0_3 Depth=1
	movq	_ZN8NArchive4NTar11NFileHeader6NMagic6kUsTarE(%rip), %rax
	movb	96(%r12), %cl
	cmpb	(%rax), %cl
	jne	.LBB0_401
# BB#290:                               #   in Loop: Header=BB0_3 Depth=1
	movb	97(%r12), %cl
	cmpb	1(%rax), %cl
	jne	.LBB0_401
# BB#291:                               #   in Loop: Header=BB0_3 Depth=1
	movb	98(%r12), %cl
	cmpb	2(%rax), %cl
	jne	.LBB0_401
# BB#292:                               #   in Loop: Header=BB0_3 Depth=1
	movb	99(%r12), %cl
	cmpb	3(%rax), %cl
	jne	.LBB0_401
# BB#293:                               # %_ZNK8NArchive4NTar5CItem7IsMagicEv.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	100(%r12), %cl
	cmpb	4(%rax), %cl
	jne	.LBB0_401
# BB#294:                               #   in Loop: Header=BB0_3 Depth=1
	cmpb	$76, 104(%r12)
	je	.LBB0_403
# BB#295:                               #   in Loop: Header=BB0_3 Depth=1
	movdqa	%xmm4, 80(%rsp)
.Ltmp37:
	movl	$2, %edi
	callq	_Znam
.Ltmp38:
# BB#296:                               #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, 80(%rsp)
	movl	$2, 92(%rsp)
	movb	$47, (%rax)
	movb	$0, 1(%rax)
	movl	$1, 88(%rsp)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, (%rsp)
	movslq	%r14d, %rdi
	cmpl	$-1, %ebp
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp40:
	callq	_Znam
.Ltmp41:
# BB#297:                               # %.noexc346.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	%r14d, 12(%rsp)
	movq	%r15, %rcx
.LBB0_298:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB0_298
# BB#299:                               # %_ZN11CStringBaseIcEC2ERKS0_.exit.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, 8(%rsp)
.Ltmp43:
	movq	%rsp, %rdi
	leaq	80(%rsp), %rsi
	callq	_ZN11CStringBaseIcEpLERKS0_
.Ltmp44:
# BB#300:                               # %_ZplIcE11CStringBaseIT_ERKS2_S4_.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 720(%rsp)
	movslq	8(%rsp), %rax
	leaq	1(%rax), %rbx
	testl	%ebx, %ebx
	je	.LBB0_368
# BB#301:                               #   in Loop: Header=BB0_3 Depth=1
	cmpl	$-1, %eax
	movq	%rbx, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp46:
	callq	_Znam
.Ltmp47:
# BB#302:                               # %.noexc352.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, 720(%rsp)
	movb	$0, (%rax)
	movl	%ebx, 732(%rsp)
	jmp	.LBB0_369
.LBB0_303:                              # %._crit_edge.i.i118.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_305
.LBB0_304:                              # %._crit_edge.thread.i.i123.i
                                        #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
.LBB0_305:                              # %._crit_edge17.i.i124.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, (%r14)
	movslq	8(%r14), %rax
	movb	$0, (%rbp,%rax)
	movl	$49, 12(%r14)
.LBB0_306:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i127.preheader.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [84,104,101,114,101,32,105,115,32,110,111,32,99,111,114,114]
	movups	%xmm0, (%rbp)
	movdqa	.LCPI0_5(%rip), %xmm0   # xmm0 = [101,99,116,32,114,101,99,111,114,100,32,97,116,32,116,104]
	movdqu	%xmm0, 16(%rbp)
	movabsq	$7381153930004471909, %rax # imm = 0x666F20646E652065
	movq	%rax, 32(%rbp)
	movl	$1668440352, 40(%rbp)   # imm = 0x63726120
	movw	$26984, 44(%rbp)        # imm = 0x6968
	movb	$118, 46(%rbp)
	leaq	47(%rbp), %rax
	addq	$48, %rbp
	movl	$48, %ecx
	movb	$101, %dl
	jmp	.LBB0_311
.LBB0_307:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_309
.LBB0_308:                              # %._crit_edge.thread.i.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
.LBB0_309:                              # %._crit_edge17.i.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, (%r14)
	movslq	8(%r14), %rax
	movb	$0, (%rbp,%rax)
	movl	$42, 12(%r14)
.LBB0_310:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.preheader.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [84,104,101,114,101,32,97,114,101,32,110,111,32,116,114,97]
	movups	%xmm0, (%rbp)
	movdqa	.LCPI0_3(%rip), %xmm0   # xmm0 = [105,108,105,110,103,32,122,101,114,111,45,102,105,108,108,101]
	movdqu	%xmm0, 16(%rbp)
	movabsq	$7237970023832232036, %rax # imm = 0x64726F6365722064
	movq	%rax, 32(%rbp)
	leaq	40(%rbp), %rax
	addq	$41, %rbp
	movl	$41, %ecx
	movb	$115, %dl
.LBB0_311:                              # %.thread.sink.split.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movb	%dl, (%rax)
	movb	$0, (%rbp)
	movl	%ecx, 8(%r14)
	jmp	.LBB0_409
.LBB0_312:                              # %vector.body670.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_333
# BB#313:                               # %vector.body670.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_314:                              # %vector.body670.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rdx), %xmm0
	movdqu	16(%rdi,%rdx), %xmm1
	movdqu	%xmm0, (%rbp,%rdx)
	movdqu	%xmm1, 16(%rbp,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB0_314
	jmp	.LBB0_334
.LBB0_315:                              # %vector.body697.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_338
# BB#316:                               # %vector.body697.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_317:                              # %vector.body697.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rbx), %xmm0
	movdqu	16(%rdi,%rbx), %xmm1
	movdqu	%xmm0, (%rbp,%rbx)
	movdqu	%xmm1, 16(%rbp,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB0_317
	jmp	.LBB0_339
.LBB0_318:                              # %vector.body643.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_343
# BB#319:                               # %vector.body643.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_320:                              # %vector.body643.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rdx), %xmm0
	movdqu	16(%rdi,%rdx), %xmm1
	movdqu	%xmm0, (%rbp,%rdx)
	movdqu	%xmm1, 16(%rbp,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB0_320
	jmp	.LBB0_344
.LBB0_321:                              # %vector.body616.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_348
# BB#322:                               # %vector.body616.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_323:                              # %vector.body616.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rdx), %xmm0
	movdqu	16(%rdi,%rdx), %xmm1
	movdqu	%xmm0, (%rbp,%rdx)
	movdqu	%xmm1, 16(%rbp,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB0_323
	jmp	.LBB0_349
.LBB0_324:                              # %vector.body589.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_353
# BB#325:                               # %vector.body589.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_326:                              # %vector.body589.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rdx), %xmm0
	movdqu	16(%rdi,%rdx), %xmm1
	movdqu	%xmm0, (%rbp,%rdx)
	movdqu	%xmm1, 16(%rbp,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB0_326
	jmp	.LBB0_354
.LBB0_327:                              # %vector.body489.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_358
# BB#328:                               # %vector.body489.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_329:                              # %vector.body489.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rbx), %xmm0
	movdqu	16(%rdi,%rbx), %xmm1
	movdqu	%xmm0, (%rbp,%rbx)
	movdqu	%xmm1, 16(%rbp,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB0_329
	jmp	.LBB0_359
.LBB0_330:                              # %vector.body516.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_363
# BB#331:                               # %vector.body516.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	xorl	%ebx, %ebx
.LBB0_332:                              # %vector.body516.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rbx), %xmm0
	movdqu	16(%rdi,%rbx), %xmm1
	movdqu	%xmm0, (%rbp,%rbx)
	movdqu	%xmm1, 16(%rbp,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB0_332
	jmp	.LBB0_364
.LBB0_333:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%edx, %edx
.LBB0_334:                              # %vector.body670.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$96, %r8
	jb	.LBB0_337
# BB#335:                               # %vector.body670.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbp,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB0_336:                              # %vector.body670
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdx), %xmm0
	movdqu	(%rdx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB0_336
.LBB0_337:                              # %middle.block671
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%rcx, %r9
	jne	.LBB0_44
	jmp	.LBB0_93
.LBB0_338:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%ebx, %ebx
.LBB0_339:                              # %vector.body697.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$96, %rdx
	jb	.LBB0_342
# BB#340:                               # %vector.body697.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%rbp,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB0_341:                              # %vector.body697
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbx), %xmm0
	movdqu	(%rbx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB0_341
.LBB0_342:                              # %middle.block698
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB0_58
	jmp	.LBB0_191
.LBB0_343:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%edx, %edx
.LBB0_344:                              # %vector.body643.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$96, %r8
	jb	.LBB0_347
# BB#345:                               # %vector.body643.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbp,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB0_346:                              # %vector.body643
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdx), %xmm0
	movdqu	(%rdx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB0_346
.LBB0_347:                              # %middle.block644
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%rcx, %r9
	jne	.LBB0_184
	jmp	.LBB0_195
.LBB0_348:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%edx, %edx
.LBB0_349:                              # %vector.body616.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$96, %r8
	jb	.LBB0_352
# BB#350:                               # %vector.body616.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbp,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB0_351:                              # %vector.body616
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdx), %xmm0
	movdqu	(%rdx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB0_351
.LBB0_352:                              # %middle.block617
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%rcx, %r9
	jne	.LBB0_214
	jmp	.LBB0_221
.LBB0_353:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%edx, %edx
.LBB0_354:                              # %vector.body589.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$96, %r8
	jb	.LBB0_357
# BB#355:                               # %vector.body589.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbp,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB0_356:                              # %vector.body589
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdx), %xmm0
	movdqu	(%rdx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB0_356
.LBB0_357:                              # %middle.block590
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%rcx, %r9
	jne	.LBB0_240
	jmp	.LBB0_247
.LBB0_358:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%ebx, %ebx
.LBB0_359:                              # %vector.body489.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$96, %rdx
	jb	.LBB0_362
# BB#360:                               # %vector.body489.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%rbp,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB0_361:                              # %vector.body489
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbx), %xmm0
	movdqu	(%rbx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB0_361
.LBB0_362:                              # %middle.block490
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB0_72
	jmp	.LBB0_304
.LBB0_363:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%ebx, %ebx
.LBB0_364:                              # %vector.body516.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$96, %rdx
	jb	.LBB0_367
# BB#365:                               # %vector.body516.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%rbp,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
.LBB0_366:                              # %vector.body516
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbx), %xmm0
	movdqu	(%rbx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB0_366
.LBB0_367:                              # %middle.block517
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB0_86
	jmp	.LBB0_308
.LBB0_368:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
.LBB0_369:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i347.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	(%rsp), %rcx
.LBB0_370:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB0_370
# BB#371:                               # %_ZN11CStringBaseIcEC2ERKS0_.exit.i350.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	8(%rsp), %eax
	movl	%eax, 728(%rsp)
.Ltmp49:
	leaq	720(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZN11CStringBaseIcEpLERKS0_
.Ltmp50:
# BB#372:                               # %_ZplIcE11CStringBaseIT_ERKS2_S4_.exit355.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%r12, %rbx
	pxor	%xmm4, %xmm4
	je	.LBB0_395
# BB#373:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$0, 8(%r12)
	movq	(%r12), %rax
	movb	$0, (%rax)
	movslq	728(%rsp), %rax
	leaq	1(%rax), %r14
	movl	12(%r12), %ebx
	cmpl	%ebx, %r14d
	jne	.LBB0_375
# BB#374:                               # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	(%r12), %rbp
	jmp	.LBB0_392
.LBB0_375:                              #   in Loop: Header=BB0_3 Depth=1
	cmpl	$-1, %eax
	movq	%r14, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp52:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp53:
# BB#376:                               # %.noexc369.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%ebx, %ebx
	pxor	%xmm4, %xmm4
	jle	.LBB0_391
# BB#377:                               # %.preheader.i.i357.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	8(%r12), %rax
	testq	%rax, %rax
	movq	(%r12), %rdi
	jle	.LBB0_389
# BB#378:                               # %.lr.ph.preheader.i.i358.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$31, %eax
	jbe	.LBB0_382
# BB#379:                               # %min.iters.checked566
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_382
# BB#380:                               # %vector.memcheck577
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB0_451
# BB#381:                               # %vector.memcheck577
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_451
.LBB0_382:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
.LBB0_383:                              # %.lr.ph.i.i363.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_386
# BB#384:                               # %.lr.ph.i.i363.i.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
.LBB0_385:                              # %.lr.ph.i.i363.i.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_385
.LBB0_386:                              # %.lr.ph.i.i363.i.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$7, %rdx
	jb	.LBB0_390
# BB#387:                               # %.lr.ph.i.i363.i.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	subq	%rcx, %rax
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB0_388:                              # %.lr.ph.i.i363.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_388
	jmp	.LBB0_390
.LBB0_389:                              # %._crit_edge.i.i359.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_391
.LBB0_390:                              # %._crit_edge.thread.i.i364.i
                                        #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
	pxor	%xmm4, %xmm4
.LBB0_391:                              # %._crit_edge17.i.i365.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, (%r12)
	movslq	8(%r12), %rax
	movb	$0, (%rbp,%rax)
	movl	%r14d, 12(%r12)
.LBB0_392:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i366.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	720(%rsp), %rax
.LBB0_393:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB0_393
# BB#394:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	728(%rsp), %eax
	movl	%eax, 8(%r12)
.LBB0_395:                              # %_ZN11CStringBaseIcEaSERKS0_.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	720(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_397
# BB#396:                               #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
	pxor	%xmm4, %xmm4
.LBB0_397:                              # %_ZN11CStringBaseIcED2Ev.exit370.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_399
# BB#398:                               #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
	pxor	%xmm4, %xmm4
.LBB0_399:                              # %_ZN11CStringBaseIcED2Ev.exit371.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_401
# BB#400:                               #   in Loop: Header=BB0_3 Depth=1
	callq	_ZdaPv
	pxor	%xmm4, %xmm4
.LBB0_401:                              # %_ZNK8NArchive4NTar5CItem7IsMagicEv.exit.thread.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpb	$49, 104(%r12)
	jne	.LBB0_403
# BB#402:                               #   in Loop: Header=BB0_3 Depth=1
	movq	$0, 16(%r12)
.LBB0_403:                              # %vector.body543.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB0_404:                              # %vector.body543
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	208(%rsp,%rax), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movd	212(%rsp,%rax), %xmm3   # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3],xmm2[4],xmm4[4],xmm2[5],xmm4[5],xmm2[6],xmm4[6],xmm2[7],xmm4[7]
	punpcklwd	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3]
	punpcklbw	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1],xmm3[2],xmm4[2],xmm3[3],xmm4[3],xmm3[4],xmm4[4],xmm3[5],xmm4[5],xmm3[6],xmm4[6],xmm3[7],xmm4[7]
	punpcklwd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1],xmm3[2],xmm4[2],xmm3[3],xmm4[3]
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movd	216(%rsp,%rax), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movd	220(%rsp,%rax), %xmm1   # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3],xmm0[4],xmm4[4],xmm0[5],xmm4[5],xmm0[6],xmm4[6],xmm0[7],xmm4[7]
	punpcklwd	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3]
	punpcklbw	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3],xmm1[4],xmm4[4],xmm1[5],xmm4[5],xmm1[6],xmm4[6],xmm1[7],xmm4[7]
	punpcklwd	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3]
	paddd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	addq	$16, %rax
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB0_404
# BB#405:                               # %middle.block544
                                        #   in Loop: Header=BB0_3 Depth=1
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %eax
	movl	$1, %ebp
	cmpl	128(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB0_407
# BB#406:                               #   in Loop: Header=BB0_3 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movb	$1, (%rax)
	xorl	%ebp, %ebp
.LBB0_407:                              # %_ZN11CStringBaseIcED2Ev.exit376.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB0_408:                              # %.loopexit151
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%ebp, %ebp
	jne	.LBB0_517
.LBB0_409:                              #   in Loop: Header=BB0_3 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	cmpb	$0, (%rax)
	je	.LBB0_516
# BB#410:                               #   in Loop: Header=BB0_3 Depth=1
	movb	104(%r12), %al
	movl	%eax, %ecx
	addb	$-68, %cl
	cmpb	$52, %cl
	ja	.LBB0_483
# BB#411:                               #   in Loop: Header=BB0_3 Depth=1
	movzbl	%cl, %ecx
	cmpq	$7, %rcx
	je	.LBB0_414
# BB#412:                               #   in Loop: Header=BB0_3 Depth=1
	cmpq	$8, %rcx
	jne	.LBB0_461
# BB#413:                               #   in Loop: Header=BB0_3 Depth=1
	testb	$1, 104(%rsp)           # 1-byte Folded Reload
	leaq	64(%rsp), %r15
	movb	$1, %al
	movq	%rax, 104(%rsp)         # 8-byte Spill
	je	.LBB0_415
	jmp	.LBB0_468
	.p2align	4, 0x90
.LBB0_414:                              #   in Loop: Header=BB0_3 Depth=1
	testb	$1, 112(%rsp)           # 1-byte Folded Reload
	leaq	32(%rsp), %r15
	movb	$1, %al
	movq	%rax, 112(%rsp)         # 8-byte Spill
	jne	.LBB0_468
.LBB0_415:                              #   in Loop: Header=BB0_3 Depth=1
	movq	_ZN8NArchive4NTar11NFileHeader9kLongLinkE(%rip), %rsi
	movq	(%r12), %rdi
.Ltmp60:
	callq	_Z15MyStringComparePKcS0_
.Ltmp61:
# BB#416:                               # %_ZNK11CStringBaseIcE7CompareEPKc.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%eax, %eax
	je	.LBB0_419
# BB#417:                               #   in Loop: Header=BB0_3 Depth=1
	movq	_ZN8NArchive4NTar11NFileHeader10kLongLink2E(%rip), %rsi
	movq	(%r12), %rdi
.Ltmp62:
	callq	_Z15MyStringComparePKcS0_
.Ltmp63:
# BB#418:                               # %_ZNK11CStringBaseIcE7CompareEPKc.exit100
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%eax, %eax
	jne	.LBB0_468
.LBB0_419:                              #   in Loop: Header=BB0_3 Depth=1
	movq	16(%r12), %r14
	cmpq	$16384, %r14            # imm = 0x4000
	ja	.LBB0_460
# BB#420:                               #   in Loop: Header=BB0_3 Depth=1
	addq	$511, %r14              # imm = 0x1FF
	andq	$-512, %r14             # imm = 0xFE00
	movl	12(%r15), %ebx
	cmpl	%r14d, %ebx
	jg	.LBB0_439
# BB#421:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%r14d, %r12d
	orl	$1, %r12d
	cmpl	%ebx, %r12d
	je	.LBB0_439
# BB#422:                               #   in Loop: Header=BB0_3 Depth=1
	movslq	%r12d, %rdi
.Ltmp65:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp66:
# BB#423:                               # %.noexc105
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	8(%r15), %r8
	testl	%ebx, %ebx
	jle	.LBB0_438
# BB#424:                               # %.preheader.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	8(%r15), %rax
	testq	%rax, %rax
	movq	(%r15), %rdi
	jle	.LBB0_436
# BB#425:                               # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$31, %eax
	jbe	.LBB0_429
# BB#426:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_429
# BB#427:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB0_443
# BB#428:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_443
.LBB0_429:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
.LBB0_430:                              # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_433
# BB#431:                               # %.lr.ph.i.i.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_432:                              # %.lr.ph.i.i.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_432
.LBB0_433:                              # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$7, %rdx
	jb	.LBB0_437
# BB#434:                               # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	subq	%rcx, %rax
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_435:                              # %.lr.ph.i.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_435
	jmp	.LBB0_437
.LBB0_436:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_438
.LBB0_437:                              # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r8, %rbx
	callq	_ZdaPv
	movq	%rbx, %r8
.LBB0_438:                              #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, (%r15)
	movslq	(%r8), %rax
	movb	$0, (%rbp,%rax)
	movl	%r12d, 12(%r15)
.LBB0_439:                              #   in Loop: Header=BB0_3 Depth=1
	movq	(%r15), %rbx
	movslq	%r14d, %rdx
.Ltmp68:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movl	%eax, %ebp
.Ltmp69:
# BB#440:                               #   in Loop: Header=BB0_3 Depth=1
	testl	%ebp, %ebp
	movq	144(%rsp), %r12         # 8-byte Reload
	jne	.LBB0_517
# BB#441:                               #   in Loop: Header=BB0_3 Depth=1
	addl	%r14d, 120(%r12)
	movq	16(%r12), %rax
	movb	$0, (%rbx,%rax)
	movq	(%r15), %rax
	movl	$-1, %ecx
	movq	%rax, %rsi
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	movabsq	$4294967296, %rdi       # imm = 0x100000000
	.p2align	4, 0x90
.LBB0_442:                              #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rdi, %rdx
	incl	%ecx
	cmpb	$0, (%rsi)
	leaq	1(%rsi), %rsi
	jne	.LBB0_442
	jmp	.LBB0_2
.LBB0_443:                              # %vector.body.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_446
# BB#444:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_445:                              # %vector.body.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rbx), %xmm0
	movdqu	16(%rdi,%rbx), %xmm1
	movdqu	%xmm0, (%rbp,%rbx)
	movdqu	%xmm1, 16(%rbp,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB0_445
	jmp	.LBB0_447
.LBB0_446:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%ebx, %ebx
.LBB0_447:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$96, %rdx
	jb	.LBB0_450
# BB#448:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%rbp,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB0_449:                              # %vector.body
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbx), %xmm0
	movdqu	(%rbx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB0_449
.LBB0_450:                              # %middle.block
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB0_430
	jmp	.LBB0_437
.LBB0_451:                              # %vector.body562.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_454
# BB#452:                               # %vector.body562.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	negq	%rsi
	xorl	%ebx, %ebx
.LBB0_453:                              # %vector.body562.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rbx), %xmm0
	movdqu	16(%rdi,%rbx), %xmm1
	movdqu	%xmm0, (%rbp,%rbx)
	movdqu	%xmm1, 16(%rbp,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB0_453
	jmp	.LBB0_455
.LBB0_454:                              #   in Loop: Header=BB0_3 Depth=1
	xorl	%ebx, %ebx
.LBB0_455:                              # %vector.body562.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$96, %rdx
	jb	.LBB0_458
# BB#456:                               # %vector.body562.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%rbp,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
.LBB0_457:                              # %vector.body562
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbx), %xmm0
	movdqu	(%rbx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB0_457
.LBB0_458:                              # %middle.block563
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB0_383
	jmp	.LBB0_390
.LBB0_460:
	movl	$1, %ebp
	jmp	.LBB0_517
.LBB0_468:
	movl	$1, %ebp
	jmp	.LBB0_517
.LBB0_461:
	movabsq	$4503633988157441, %rdx # imm = 0x10000800100001
	btq	%rcx, %rdx
	jae	.LBB0_483
.LBB0_462:                              # %.loopexit155
	leaq	64(%rsp), %rax
	cmpq	%r12, %rax
	je	.LBB0_492
# BB#463:                               # %.loopexit155
	movq	104(%rsp), %rax         # 8-byte Reload
	andb	$1, %al
	je	.LBB0_492
# BB#464:
	movl	$0, 8(%r12)
	movq	(%r12), %rax
	movb	$0, (%rax)
	movslq	72(%rsp), %rax
	leaq	1(%rax), %r14
	movl	12(%r12), %ebp
	cmpl	%ebp, %r14d
	jne	.LBB0_469
# BB#465:                               # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	(%r12), %rbx
	jmp	.LBB0_489
.LBB0_469:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r14, %rdi
	cmovlq	%rax, %rdi
.Ltmp71:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp72:
# BB#470:                               # %.noexc118
	testl	%ebp, %ebp
	jle	.LBB0_488
# BB#471:                               # %.preheader.i.i109
	movslq	8(%r12), %r8
	testq	%r8, %r8
	movq	(%r12), %rdi
	jle	.LBB0_486
# BB#472:                               # %.lr.ph.preheader.i.i110
	cmpl	$31, %r8d
	jbe	.LBB0_476
# BB#473:                               # %min.iters.checked728
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB0_476
# BB#474:                               # %vector.memcheck739
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB0_522
# BB#475:                               # %vector.memcheck739
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_522
.LBB0_476:
	xorl	%ecx, %ecx
.LBB0_477:                              # %.lr.ph.i.i115.preheader
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_480
# BB#478:                               # %.lr.ph.i.i115.prol.preheader
	negq	%rsi
.LBB0_479:                              # %.lr.ph.i.i115.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_479
.LBB0_480:                              # %.lr.ph.i.i115.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB0_487
# BB#481:                               # %.lr.ph.i.i115.preheader.new
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB0_482:                              # %.lr.ph.i.i115
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB0_482
	jmp	.LBB0_487
.LBB0_483:
	movl	$1, %ebp
	cmpb	$55, %al
	jg	.LBB0_517
# BB#484:
	cmpb	$47, %al
	jg	.LBB0_462
# BB#485:
	testb	%al, %al
	jne	.LBB0_517
	jmp	.LBB0_462
.LBB0_486:                              # %._crit_edge.i.i111
	testq	%rdi, %rdi
	je	.LBB0_488
.LBB0_487:                              # %._crit_edge.thread.i.i116
	callq	_ZdaPv
.LBB0_488:                              # %._crit_edge17.i.i117
	movq	%rbx, (%r12)
	movslq	8(%r12), %rax
	movb	$0, (%rbx,%rax)
	movl	%r14d, 12(%r12)
.LBB0_489:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	64(%rsp), %rax
	.p2align	4, 0x90
.LBB0_490:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB0_490
# BB#491:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
	movl	72(%rsp), %eax
	movl	%eax, 8(%r12)
.LBB0_492:                              # %_ZN11CStringBaseIcEaSERKS0_.exit
	testb	$1, 112(%rsp)           # 1-byte Folded Reload
	je	.LBB0_516
# BB#493:
	leaq	48(%r12), %rax
	leaq	32(%rsp), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_516
# BB#494:
	movl	$0, 56(%r12)
	movq	48(%r12), %rcx
	movb	$0, (%rcx)
	movslq	40(%rsp), %rcx
	leaq	1(%rcx), %r14
	movl	60(%r12), %ebp
	cmpl	%ebp, %r14d
	jne	.LBB0_496
# BB#495:                               # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i120
	movq	(%rax), %rbx
	jmp	.LBB0_513
.LBB0_496:
	cmpl	$-1, %ecx
	movq	$-1, %rax
	movq	%r14, %rdi
	cmovlq	%rax, %rdi
.Ltmp73:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp74:
# BB#497:                               # %.noexc134
	testl	%ebp, %ebp
	jle	.LBB0_512
# BB#498:                               # %.preheader.i.i121
	movslq	56(%r12), %r8
	testq	%r8, %r8
	movq	48(%r12), %rdi
	jle	.LBB0_510
# BB#499:                               # %.lr.ph.preheader.i.i122
	cmpl	$31, %r8d
	jbe	.LBB0_503
# BB#500:                               # %min.iters.checked755
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB0_503
# BB#501:                               # %vector.memcheck766
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB0_525
# BB#502:                               # %vector.memcheck766
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_525
.LBB0_503:
	xorl	%ecx, %ecx
.LBB0_504:                              # %.lr.ph.i.i127.preheader
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_507
# BB#505:                               # %.lr.ph.i.i127.prol.preheader
	negq	%rsi
.LBB0_506:                              # %.lr.ph.i.i127.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_506
.LBB0_507:                              # %.lr.ph.i.i127.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB0_511
# BB#508:                               # %.lr.ph.i.i127.preheader.new
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB0_509:                              # %.lr.ph.i.i127
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB0_509
	jmp	.LBB0_511
.LBB0_510:                              # %._crit_edge.i.i123
	testq	%rdi, %rdi
	je	.LBB0_512
.LBB0_511:                              # %._crit_edge.thread.i.i128
	callq	_ZdaPv
.LBB0_512:                              # %._crit_edge17.i.i129
	movq	%rbx, 48(%r12)
	movslq	56(%r12), %rax
	movb	$0, (%rbx,%rax)
	movl	%r14d, 60(%r12)
.LBB0_513:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i130
	movq	32(%rsp), %rax
	.p2align	4, 0x90
.LBB0_514:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB0_514
# BB#515:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i133
	movl	40(%rsp), %eax
	movl	%eax, 56(%r12)
.LBB0_516:                              # %_ZN11CStringBaseIcEaSERKS0_.exit135
	xorl	%ebp, %ebp
.LBB0_517:                              # %_ZN11CStringBaseIcEaSERKS0_.exit135
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_519
# BB#518:
	callq	_ZdaPv
.LBB0_519:                              # %_ZN11CStringBaseIcED2Ev.exit
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_521
# BB#520:
	callq	_ZdaPv
.LBB0_521:                              # %_ZN11CStringBaseIcED2Ev.exit136
	movl	%ebp, %eax
	addq	$1240, %rsp             # imm = 0x4D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_522:                              # %vector.body724.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_528
# BB#523:                               # %vector.body724.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB0_524:                              # %vector.body724.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rdi,%rbp), %xmm0
	movdqu	16(%rdi,%rbp), %xmm1
	movdqu	%xmm0, (%rbx,%rbp)
	movdqu	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_524
	jmp	.LBB0_529
.LBB0_525:                              # %vector.body751.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_533
# BB#526:                               # %vector.body751.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB0_527:                              # %vector.body751.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rdi,%rbp), %xmm0
	movdqu	16(%rdi,%rbp), %xmm1
	movdqu	%xmm0, (%rbx,%rbp)
	movdqu	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_527
	jmp	.LBB0_534
.LBB0_528:
	xorl	%ebp, %ebp
.LBB0_529:                              # %vector.body724.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB0_532
# BB#530:                               # %vector.body724.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB0_531:                              # %vector.body724
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_531
.LBB0_532:                              # %middle.block725
	cmpq	%rcx, %r8
	jne	.LBB0_477
	jmp	.LBB0_487
.LBB0_533:
	xorl	%ebp, %ebp
.LBB0_534:                              # %vector.body751.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB0_537
# BB#535:                               # %vector.body751.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB0_536:                              # %vector.body751
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_536
.LBB0_537:                              # %middle.block752
	cmpq	%rcx, %r8
	jne	.LBB0_504
	jmp	.LBB0_511
.LBB0_538:
.Ltmp54:
	jmp	.LBB0_541
.LBB0_539:
.Ltmp48:
	jmp	.LBB0_544
.LBB0_540:
.Ltmp51:
.LBB0_541:
	movq	%rax, %rbx
	movq	720(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_545
# BB#542:
	callq	_ZdaPv
	jmp	.LBB0_545
.LBB0_543:
.Ltmp45:
.LBB0_544:                              # %_ZN11CStringBaseIcED2Ev.exit373.i
	movq	%rax, %rbx
.LBB0_545:                              # %_ZN11CStringBaseIcED2Ev.exit373.i
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_548
# BB#546:
	callq	_ZdaPv
	jmp	.LBB0_548
.LBB0_547:
.Ltmp42:
	movq	%rax, %rbx
.LBB0_548:                              # %_ZN11CStringBaseIcED2Ev.exit374.i
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_554
# BB#549:
	callq	_ZdaPv
	jmp	.LBB0_554
.LBB0_550:
.Ltmp39:
	jmp	.LBB0_553
.LBB0_551:                              # %.loopexit.split-lp.loopexit.split-lp
.Ltmp75:
	jmp	.LBB0_561
.LBB0_552:
.Ltmp36:
.LBB0_553:                              # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	%rax, %rbx
.LBB0_554:                              # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB0_562
.LBB0_555:
.Ltmp67:
	jmp	.LBB0_561
.LBB0_556:                              # %.thread345
.Ltmp2:
	movq	%rax, %rbx
	jmp	.LBB0_565
.LBB0_557:
.Ltmp70:
	jmp	.LBB0_561
.LBB0_558:
.Ltmp64:
	jmp	.LBB0_561
.LBB0_559:                              # %.loopexit.split-lp.loopexit
.Ltmp59:
	jmp	.LBB0_561
.LBB0_560:                              # %.loopexit
.Ltmp5:
.LBB0_561:                              # %.body
	movq	%rax, %rbx
.LBB0_562:                              # %.body
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_564
# BB#563:
	callq	_ZdaPv
.LBB0_564:
	movq	64(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB0_566
.LBB0_565:
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB0_566:                              # %_ZN11CStringBaseIcED2Ev.exit138
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN8NArchive4NTar8ReadItemEP19ISequentialInStreamRbRNS0_7CItemExER11CStringBaseIcE, .Lfunc_end0-_ZN8NArchive4NTar8ReadItemEP19ISequentialInStreamRbRNS0_7CItemExER11CStringBaseIcE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\323\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp33-.Ltmp8          #   Call between .Ltmp8 and .Ltmp33
	.long	.Ltmp59-.Lfunc_begin0   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin0   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin0   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin0   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin0   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin0   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin0   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin0   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp63-.Ltmp60         #   Call between .Ltmp60 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin0   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin0   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin0   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp74-.Ltmp71         #   Call between .Ltmp71 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin0   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Lfunc_end0-.Ltmp74     #   Call between .Ltmp74 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLERKS0_,"axG",@progbits,_ZN11CStringBaseIcEpLERKS0_,comdat
	.weak	_ZN11CStringBaseIcEpLERKS0_
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLERKS0_,@function
_ZN11CStringBaseIcEpLERKS0_:            # @_ZN11CStringBaseIcEpLERKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	8(%r14), %eax
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %ecx
	subl	%ebp, %ecx
	cmpl	%eax, %ecx
	jg	.LBB1_28
# BB#1:
	decl	%ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB1_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB1_3:                                # %select.end
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%rsi,%rbx), %r12d
	cmpl	%ebx, %r12d
	je	.LBB1_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r12d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	testl	%ebx, %ebx
	jle	.LBB1_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB1_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB1_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB1_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r15
	jae	.LBB1_17
# BB#16:                                # %vector.memcheck
	leaq	(%r15,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB1_17
.LBB1_7:
	xorl	%ecx, %ecx
.LBB1_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB1_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB1_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r15,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB1_10
.LBB1_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB1_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB1_13
	jmp	.LBB1_26
.LBB1_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB1_27
.LBB1_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB1_27:
	movq	%r15, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r15,%rax)
	movl	%r12d, 12(%r13)
.LBB1_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movslq	%ebp, %rax
	addq	(%r13), %rax
	movq	(%r14), %rcx
	.p2align	4, 0x90
.LBB1_29:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB1_29
# BB#30:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit
	movl	8(%r14), %eax
	addl	%eax, 8(%r13)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB1_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r15,%rbx)
	movups	%xmm1, 16(%r15,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB1_20
	jmp	.LBB1_21
.LBB1_18:
	xorl	%ebx, %ebx
.LBB1_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB1_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r15,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB1_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB1_23
.LBB1_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB1_8
	jmp	.LBB1_26
.Lfunc_end1:
	.size	_ZN11CStringBaseIcEpLERKS0_, .Lfunc_end1-_ZN11CStringBaseIcEpLERKS0_
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
