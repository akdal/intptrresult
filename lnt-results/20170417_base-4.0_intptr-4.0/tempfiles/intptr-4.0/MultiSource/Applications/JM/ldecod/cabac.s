	.text
	.file	"cabac.bc"
	.globl	CheckAvailabilityOfNeighborsCABAC
	.p2align	4, 0x90
	.type	CheckAvailabilityOfNeighborsCABAC,@function
CheckAvailabilityOfNeighborsCABAC:      # @CheckAvailabilityOfNeighborsCABAC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 80
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	img(%rip), %rax
	movq	5600(%rax), %r14
	movl	4(%rax), %r15d
	xorl	%ebx, %ebx
	movq	%rsp, %r8
	movl	$-1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	callq	*getNeighbour(%rip)
	movq	img(%rip), %rax
	movl	4(%rax), %edi
	leaq	24(%rsp), %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	callq	*getNeighbour(%rip)
	cmpl	$0, 24(%rsp)
	je	.LBB0_2
# BB#1:
	movq	img(%rip), %rax
	movslq	28(%rsp), %rcx
	imulq	$408, %rcx, %rbx        # imm = 0x198
	addq	5600(%rax), %rbx
.LBB0_2:
	imulq	$408, %r15, %rax        # imm = 0x198
	movq	%rbx, 24(%r14,%rax)
	cmpl	$0, (%rsp)
	je	.LBB0_3
# BB#4:
	movq	img(%rip), %rdx
	movslq	4(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	addq	5600(%rdx), %rcx
	jmp	.LBB0_5
.LBB0_3:
	xorl	%ecx, %ecx
.LBB0_5:
	movq	%rcx, 32(%r14,%rax)
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	CheckAvailabilityOfNeighborsCABAC, .Lfunc_end0-CheckAvailabilityOfNeighborsCABAC
	.cfi_endproc

	.globl	cabac_new_slice
	.p2align	4, 0x90
	.type	cabac_new_slice,@function
cabac_new_slice:                        # @cabac_new_slice
	.cfi_startproc
# BB#0:
	movl	$0, last_dquant(%rip)
	retq
.Lfunc_end1:
	.size	cabac_new_slice, .Lfunc_end1-cabac_new_slice
	.cfi_endproc

	.globl	create_contexts_MotionInfo
	.p2align	4, 0x90
	.type	create_contexts_MotionInfo,@function
create_contexts_MotionInfo:             # @create_contexts_MotionInfo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$420, %esi              # imm = 0x1A4
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB2_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	create_contexts_MotionInfo, .Lfunc_end2-create_contexts_MotionInfo
	.cfi_endproc

	.globl	create_contexts_TextureInfo
	.p2align	4, 0x90
	.type	create_contexts_TextureInfo,@function
create_contexts_TextureInfo:            # @create_contexts_TextureInfo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$3032, %esi             # imm = 0xBD8
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB3_2
# BB#1:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB3_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	create_contexts_TextureInfo, .Lfunc_end3-create_contexts_TextureInfo
	.cfi_endproc

	.globl	delete_contexts_MotionInfo
	.p2align	4, 0x90
	.type	delete_contexts_MotionInfo,@function
delete_contexts_MotionInfo:             # @delete_contexts_MotionInfo
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB4_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB4_1:
	retq
.Lfunc_end4:
	.size	delete_contexts_MotionInfo, .Lfunc_end4-delete_contexts_MotionInfo
	.cfi_endproc

	.globl	delete_contexts_TextureInfo
	.p2align	4, 0x90
	.type	delete_contexts_TextureInfo,@function
delete_contexts_TextureInfo:            # @delete_contexts_TextureInfo
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB5_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB5_1:
	retq
.Lfunc_end5:
	.size	delete_contexts_TextureInfo, .Lfunc_end5-delete_contexts_TextureInfo
	.cfi_endproc

	.globl	readFieldModeInfo_CABAC
	.p2align	4, 0x90
	.type	readFieldModeInfo_CABAC,@function
readFieldModeInfo_CABAC:                # @readFieldModeInfo_CABAC
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	5592(%rsi), %rax
	movq	48(%rax), %rax
	movq	5600(%rsi), %rcx
	movl	4(%rsi), %esi
	imulq	$408, %rsi, %rbx        # imm = 0x198
	xorl	%esi, %esi
	cmpl	$0, 380(%rcx,%rbx)
	movl	$0, %edi
	je	.LBB6_2
# BB#1:
	movslq	364(%rcx,%rbx), %rdi
	imulq	$408, %rdi, %rdi        # imm = 0x198
	movl	356(%rcx,%rdi), %edi
.LBB6_2:
	cmpl	$0, 384(%rcx,%rbx)
	je	.LBB6_4
# BB#3:
	movslq	368(%rcx,%rbx), %rsi
	imulq	$408, %rsi, %rsi        # imm = 0x198
	movl	356(%rcx,%rsi), %esi
.LBB6_4:
	addl	%edi, %esi
	movslq	%esi, %rcx
	leaq	392(%rax,%rcx,4), %rsi
	movq	%rdx, %rdi
	callq	biari_decode_symbol
	movl	%eax, 4(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	readFieldModeInfo_CABAC, .Lfunc_end6-readFieldModeInfo_CABAC
	.cfi_endproc

	.globl	check_next_mb_and_get_field_mode_CABAC
	.p2align	4, 0x90
	.type	check_next_mb_and_get_field_mode_CABAC,@function
check_next_mb_and_get_field_mode_CABAC: # @check_next_mb_and_get_field_mode_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 160
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	4(%r12), %eax
	movl	44(%r12), %ecx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	leal	1(%rax), %ecx
	movl	%ecx, 4(%r12)
	movq	5600(%r12), %rdx
	movl	12(%r12), %esi
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movl	%esi, 12(%rdx,%rcx)
	imulq	$408, %rax, %rax        # imm = 0x198
	movl	356(%rdx,%rax), %eax
	movl	%eax, 356(%rdx,%rcx)
	callq	CheckAvailabilityOfNeighbors
	movq	img(%rip), %rax
	movq	5600(%rax), %r14
	movl	4(%rax), %ebp
	xorl	%ebx, %ebx
	leaq	56(%rsp), %r8
	movl	$-1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%ebp, %edi
	callq	*getNeighbour(%rip)
	movq	img(%rip), %rax
	movl	4(%rax), %edi
	leaq	80(%rsp), %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	callq	*getNeighbour(%rip)
	cmpl	$0, 80(%rsp)
	je	.LBB7_2
# BB#1:
	movq	img(%rip), %rax
	movslq	84(%rsp), %rcx
	imulq	$408, %rcx, %rbx        # imm = 0x198
	addq	5600(%rax), %rbx
.LBB7_2:
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	imulq	$408, %rbp, %rax        # imm = 0x198
	movq	%rbx, 24(%r14,%rax)
	cmpl	$0, 56(%rsp)
	je	.LBB7_3
# BB#4:
	movq	img(%rip), %rdx
	movslq	60(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	addq	5600(%rdx), %rcx
	jmp	.LBB7_5
.LBB7_3:
	xorl	%ecx, %ecx
.LBB7_5:                                # %CheckAvailabilityOfNeighborsCABAC.exit
	movq	%rcx, 32(%r14,%rax)
	movl	$1, %edi
	movl	$40, %esi
	callq	calloc
	movq	%rax, %rbp
	movl	$11, %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %r15
	movl	$11, %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %rbx
	movl	$11, %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %r13
	movl	$11, %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %r14
	movl	$4, %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	32(%rcx), %rax
	movq	%rax, 32(%rbp)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	%xmm1, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	40(%rax), %rax
	movl	(%rax), %ecx
	movq	%rbp, %r8
	movq	32(%rbp), %rax
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movl	%ecx, (%rax)
	movq	5592(%r12), %rax
	movq	48(%rax), %rcx
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	28(%rcx), %xmm2
	movups	%xmm2, 28(%r15)
	movups	%xmm1, 16(%r15)
	movq	%r15, %rbp
	movups	%xmm0, (%r15)
	movups	44(%rcx), %xmm0
	movups	60(%rcx), %xmm1
	movups	72(%rcx), %xmm2
	movups	%xmm2, 28(%rbx)
	movups	%xmm1, 16(%rbx)
	movq	%rbx, %r15
	movups	%xmm0, (%rbx)
	movups	88(%rcx), %xmm0
	movups	104(%rcx), %xmm1
	movups	116(%rcx), %xmm2
	movups	%xmm2, 28(%r13)
	movups	%xmm1, 16(%r13)
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movups	%xmm0, (%r13)
	movq	%r14, %r13
	movups	132(%rcx), %xmm0
	movups	148(%rcx), %xmm1
	movups	160(%rcx), %xmm2
	movups	%xmm2, 28(%r13)
	movups	%xmm1, 16(%r13)
	movups	%xmm0, (%r13)
	movq	48(%rax), %rax
	movups	392(%rax), %xmm0
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movups	%xmm0, (%rdx)
	movl	$0, last_dquant(%rip)
	movl	4(%r12), %edx
	movq	5600(%r12), %rcx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movq	24(%rcx,%rdx), %rdi
	cmpl	$1, 44(%r12)
	jne	.LBB7_12
# BB#6:
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.LBB7_8
# BB#7:
	cmpl	$0, 360(%rdi)
	sete	%sil
.LBB7_8:
	movq	%r8, %r14
	movq	32(%rcx,%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB7_9
# BB#10:
	cmpl	$1, 360(%rcx)
	movl	$7, %ecx
	adcq	$0, %rcx
	jmp	.LBB7_11
.LBB7_12:
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.LBB7_14
# BB#13:
	cmpl	$0, 360(%rdi)
	sete	%sil
.LBB7_14:
	movq	%r8, %r14
	movq	32(%rcx,%rdx), %rdx
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB7_16
# BB#15:
	cmpl	$0, 360(%rdx)
	sete	%cl
.LBB7_16:
	addq	%rsi, %rcx
	leaq	44(%rax,%rcx,4), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	setne	%cl
	movq	16(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB7_17
.LBB7_9:
	movl	$7, %ecx
.LBB7_11:
	addq	%rsi, %rcx
	leaq	88(%rax,%rcx,4), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	setne	%cl
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%ecx, 8(%rsi)
.LBB7_17:
	movl	%ecx, 4(%rsi)
	testl	%ecx, %ecx
	je	.LBB7_19
# BB#18:                                # %readMB_skip_flagInfo_CABAC.exit.thread.thread
	sete	%al
	addq	$4, %rsi
	cmpl	$1, 32(%rsp)            # 4-byte Folded Reload
	setne	%cl
	andb	%al, %cl
	movzbl	%cl, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	%rsi, %rbx
.LBB7_23:
	movq	5592(%r12), %rax
	movq	48(%rax), %rax
	movq	5600(%r12), %rcx
	movl	4(%r12), %edx
	imulq	$408, %rdx, %rdi        # imm = 0x198
	xorl	%edx, %edx
	cmpl	$0, 380(%rcx,%rdi)
	movl	$0, %esi
	je	.LBB7_25
# BB#24:
	movslq	364(%rcx,%rdi), %rsi
	imulq	$408, %rsi, %rsi        # imm = 0x198
	movl	356(%rcx,%rsi), %esi
.LBB7_25:
	cmpl	$0, 384(%rcx,%rdi)
	je	.LBB7_27
# BB#26:
	movslq	368(%rcx,%rdi), %rdx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movl	356(%rcx,%rdx), %edx
.LBB7_27:                               # %readFieldModeInfo_CABAC.exit
	addl	%esi, %edx
	movslq	%edx, %rcx
	leaq	392(%rax,%rcx,4), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	biari_decode_symbol
	movl	%eax, (%rbx)
	movq	5600(%r12), %rcx
	movl	4(%r12), %edx
	decl	%edx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movl	%eax, 356(%rcx,%rdx)
.LBB7_28:
	decl	4(%r12)
	movq	32(%r14), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, 32(%rcx)
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	%xmm1, 16(%rcx)
	movups	%xmm0, (%rcx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	40(%rax), %rax
	movl	36(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, (%rax)
	movq	5592(%r12), %rax
	movq	48(%rax), %rax
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movups	28(%rbp), %xmm2
	movups	%xmm2, 28(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	5592(%r12), %rax
	movq	48(%rax), %rax
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movups	28(%r15), %xmm2
	movups	%xmm2, 72(%rax)
	movups	%xmm1, 60(%rax)
	movups	%xmm0, 44(%rax)
	movq	5592(%r12), %rax
	movq	48(%rax), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	28(%rcx), %xmm2
	movups	%xmm2, 116(%rax)
	movups	%xmm1, 104(%rax)
	movups	%xmm0, 88(%rax)
	movq	5592(%r12), %rax
	movq	48(%rax), %rax
	movups	(%r13), %xmm0
	movups	16(%r13), %xmm1
	movups	28(%r13), %xmm2
	movups	%xmm2, 160(%rax)
	movups	%xmm1, 148(%rax)
	movups	%xmm0, 132(%rax)
	movq	5592(%r12), %rax
	movq	48(%rax), %rax
	movq	48(%rsp), %r12          # 8-byte Reload
	movups	(%r12), %xmm0
	movups	%xmm0, 392(%rax)
	movq	img(%rip), %rax
	movq	5600(%rax), %r15
	movl	4(%rax), %ebx
	xorl	%ebp, %ebp
	leaq	56(%rsp), %r8
	movl	$-1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	callq	*getNeighbour(%rip)
	movq	img(%rip), %rax
	movl	4(%rax), %edi
	leaq	80(%rsp), %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	callq	*getNeighbour(%rip)
	cmpl	$0, 80(%rsp)
	je	.LBB7_30
# BB#29:
	movq	img(%rip), %rax
	movslq	84(%rsp), %rcx
	imulq	$408, %rcx, %rbp        # imm = 0x198
	addq	5600(%rax), %rbp
.LBB7_30:
	imulq	$408, %rbx, %rax        # imm = 0x198
	movq	%rbp, 24(%r15,%rax)
	cmpl	$0, 56(%rsp)
	je	.LBB7_31
# BB#32:
	movq	img(%rip), %rdx
	movslq	60(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	addq	5600(%rdx), %rcx
	jmp	.LBB7_33
.LBB7_19:                               # %readMB_skip_flagInfo_CABAC.exit
	leaq	4(%rsi), %rbx
	movl	32(%rsp), %edi          # 4-byte Reload
	cmpl	$1, %edi
	setne	%al
	testl	%ecx, %ecx
	sete	%cl
	setne	%dl
	cmpl	$1, %edi
	movl	$0, last_dquant(%rip)
	jne	.LBB7_22
# BB#20:                                # %readMB_skip_flagInfo_CABAC.exit
	testb	%dl, %dl
	jne	.LBB7_22
# BB#21:
	xorl	%eax, %eax
	cmpl	$0, 8(%rsi)
	sete	%al
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jne	.LBB7_23
	jmp	.LBB7_28
.LBB7_31:
	xorl	%ecx, %ecx
.LBB7_33:                               # %CheckAvailabilityOfNeighborsCABAC.exit76
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rcx, 32(%r15,%rax)
	movq	%r14, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movl	16(%rsp), %eax          # 4-byte Reload
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_22:
	andb	%cl, %al
	movzbl	%al, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	testb	%al, %al
	je	.LBB7_23
	jmp	.LBB7_28
.Lfunc_end7:
	.size	check_next_mb_and_get_field_mode_CABAC, .Lfunc_end7-check_next_mb_and_get_field_mode_CABAC
	.cfi_endproc

	.globl	readMB_skip_flagInfo_CABAC
	.p2align	4, 0x90
	.type	readMB_skip_flagInfo_CABAC,@function
readMB_skip_flagInfo_CABAC:             # @readMB_skip_flagInfo_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
.Lcfi30:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	4(%rsi), %eax
	movq	5592(%rsi), %rcx
	movq	48(%rcx), %r8
	movq	5600(%rsi), %rcx
	imulq	$408, %rax, %rdi        # imm = 0x198
	movq	24(%rcx,%rdi), %rax
	cmpl	$1, 44(%rsi)
	jne	.LBB8_7
# BB#1:
	xorl	%esi, %esi
	testq	%rax, %rax
	je	.LBB8_3
# BB#2:
	cmpl	$0, 360(%rax)
	sete	%sil
.LBB8_3:
	movq	32(%rcx,%rdi), %rax
	testq	%rax, %rax
	je	.LBB8_4
# BB#5:
	cmpl	$1, 360(%rax)
	movl	$7, %eax
	adcq	$0, %rax
	jmp	.LBB8_6
.LBB8_7:
	xorl	%esi, %esi
	testq	%rax, %rax
	je	.LBB8_9
# BB#8:
	cmpl	$0, 360(%rax)
	sete	%sil
.LBB8_9:
	movq	32(%rcx,%rdi), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.LBB8_11
# BB#10:
	cmpl	$0, 360(%rax)
	sete	%cl
.LBB8_11:
	addq	%rsi, %rcx
	leaq	44(%r8,%rcx,4), %rsi
	movq	%rdx, %rdi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	setne	%cl
	jmp	.LBB8_12
.LBB8_4:
	movl	$7, %eax
.LBB8_6:
	addq	%rsi, %rax
	leaq	88(%r8,%rax,4), %rsi
	movq	%rdx, %rdi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	setne	%cl
	movl	%ecx, 8(%rbx)
.LBB8_12:
	movl	%ecx, 4(%rbx)
	testl	%ecx, %ecx
	jne	.LBB8_14
# BB#13:
	movl	$0, last_dquant(%rip)
.LBB8_14:
	popq	%rbx
	retq
.Lfunc_end8:
	.size	readMB_skip_flagInfo_CABAC, .Lfunc_end8-readMB_skip_flagInfo_CABAC
	.cfi_endproc

	.globl	readMVD_CABAC
	.p2align	4, 0x90
	.type	readMVD_CABAC,@function
readMVD_CABAC:                          # @readMVD_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 144
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movl	5608(%rbx), %eax
	movl	5612(%rbx), %r15d
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movl	8(%rdi), %r14d
	movl	%r14d, %r13d
	andl	$1, %r13d
	sarl	%r14d
	movq	5592(%rbx), %rcx
	movq	48(%rcx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	5600(%rbx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	4(%rbx), %r12d
	leal	(,%rax,4), %ebp
	leal	-1(,%rax,4), %esi
	leal	(,%r15,4), %edx
	leaq	64(%rsp), %rcx
	movl	%r12d, %edi
	callq	getLuma4x4Neighbour
	movl	4(%rbx), %edi
	leal	-1(,%r15,4), %edx
	leaq	40(%rsp), %rcx
	movl	%ebp, %esi
	callq	getLuma4x4Neighbour
	xorl	%eax, %eax
	cmpl	$0, 40(%rsp)
	movl	$0, %edx
	je	.LBB9_8
# BB#1:
	movq	5600(%rbx), %r8
	movslq	44(%rsp), %rdx
	movslq	52(%rsp), %rdi
	movslq	48(%rsp), %rbp
	movslq	%r14d, %rcx
	imulq	$408, %rdx, %r9         # imm = 0x198
	leaq	(%r8,%r9), %rdx
	movq	%r13, %rsi
	shlq	$7, %rsi
	addq	%rdx, %rsi
	shlq	$5, %rdi
	addq	%rsi, %rdi
	leaq	(%rdi,%rbp,8), %rdx
	movl	44(%rdx,%rcx,4), %esi
	movl	%esi, %edx
	negl	%edx
	cmovll	%esi, %edx
	cmpl	$1, %ecx
	jne	.LBB9_8
# BB#2:
	movl	5624(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB9_8
# BB#3:
	imulq	$408, %r12, %rcx        # imm = 0x198
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	356(%rsi,%rcx), %edi
	cmpl	$1, %edi
	je	.LBB9_6
# BB#4:
	testl	%edi, %edi
	jne	.LBB9_8
# BB#5:
	cmpl	$1, 356(%r8,%r9)
	sete	%cl
	shll	%cl, %edx
	jmp	.LBB9_8
.LBB9_6:
	cmpl	$0, 356(%r8,%r9)
	jne	.LBB9_8
# BB#7:
	movl	%edx, %ecx
	shrl	$31, %ecx
	addl	%edx, %ecx
	sarl	%ecx
	movl	%ecx, %edx
.LBB9_8:                                # %thread-pre-split.thread
	cmpl	$0, 64(%rsp)
	movq	32(%rsp), %r15          # 8-byte Reload
	je	.LBB9_16
# BB#9:
	movq	5600(%rbx), %r8
	movslq	68(%rsp), %rax
	movslq	76(%rsp), %rdi
	movslq	72(%rsp), %rbp
	movslq	%r14d, %rcx
	imulq	$408, %rax, %rsi        # imm = 0x198
	leaq	(%r8,%rsi), %rax
	shlq	$7, %r13
	addq	%rax, %r13
	shlq	$5, %rdi
	addq	%r13, %rdi
	leaq	(%rdi,%rbp,8), %rax
	movl	44(%rax,%rcx,4), %ecx
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	cmpl	$1, %r14d
	jne	.LBB9_16
# BB#10:
	movl	5624(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB9_16
# BB#11:
	imulq	$408, %r12, %rcx        # imm = 0x198
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	356(%rdi,%rcx), %edi
	cmpl	$1, %edi
	je	.LBB9_14
# BB#12:
	testl	%edi, %edi
	jne	.LBB9_16
# BB#13:
	cmpl	$1, 356(%r8,%rsi)
	sete	%cl
	shll	%cl, %eax
	jmp	.LBB9_16
.LBB9_14:
	cmpl	$0, 356(%r8,%rsi)
	jne	.LBB9_16
# BB#15:
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movl	%ecx, %eax
.LBB9_16:                               # %thread-pre-split84.thread
	addl	%edx, %eax
	leal	(%r14,%r14,4), %ecx
	cmpl	$2, %eax
	jg	.LBB9_18
# BB#17:
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB9_21
.LBB9_18:
	cmpl	$33, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	jl	.LBB9_20
# BB#19:
	addl	$3, %ecx
	jmp	.LBB9_21
.LBB9_20:
	addl	$2, %ecx
.LBB9_21:
	movl	%ecx, 24(%r15)
	movslq	%ecx, %rax
	leaq	248(%rbx,%rax,4), %rsi
	movq	%rbp, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB9_22
# BB#23:
	leal	(%r14,%r14,4), %eax
	cltq
	leaq	288(%rbx,%rax,4), %rsi
	movl	$3, %edx
	movq	%rbp, %rdi
	callq	unary_exp_golomb_mv_decode
	movl	%eax, %ebx
	leal	1(%rbx), %r14d
	movq	%rbp, %rdi
	callq	biari_decode_symbol_eq_prob
	testl	%eax, %eax
	notl	%ebx
	cmovel	%r14d, %ebx
	jmp	.LBB9_24
.LBB9_22:
	xorl	%ebx, %ebx
.LBB9_24:
	movl	%ebx, 4(%r15)
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	readMVD_CABAC, .Lfunc_end9-readMVD_CABAC
	.cfi_endproc

	.globl	unary_exp_golomb_mv_decode
	.p2align	4, 0x90
	.type	unary_exp_golomb_mv_decode,@function
unary_exp_golomb_mv_decode:             # @unary_exp_golomb_mv_decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 64
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB10_1
# BB#2:
	addq	$4, %rbx
	movl	$2, %r15d
	subl	%ebp, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	movl	%r13d, %ebp
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	leaq	4(%rbx), %rcx
	testl	%ebp, %ebp
	cmovneq	%rbx, %rcx
	leaq	4(%rcx), %rbx
	cmpl	%ebp, %r15d
	cmovneq	%rcx, %rbx
	testl	%eax, %eax
	sete	%cl
	leal	-1(%rbp), %r13d
	cmpl	$-6, %ebp
	je	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=1
	testb	%cl, %cl
	je	.LBB10_3
.LBB10_5:                               # %.critedge
	testl	%eax, %eax
	je	.LBB10_6
# BB#7:                                 # %.outer19.i.preheader
	xorl	%r15d, %r15d
	movl	$3, %ebx
	jmp	.LBB10_8
.LBB10_16:                              # %.thread.i
                                        #   in Loop: Header=BB10_8 Depth=1
	movl	$1, %eax
	movl	%ebx, %ecx
	shll	%cl, %eax
	addl	%eax, %r15d
	incl	%ebx
	.p2align	4, 0x90
.LBB10_8:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	biari_decode_symbol_eq_prob
	cmpl	$1, %eax
	je	.LBB10_16
# BB#9:                                 #   in Loop: Header=BB10_8 Depth=1
	testl	%eax, %eax
	jne	.LBB10_8
# BB#10:                                # %.outer.i.preheader
	xorl	%r12d, %r12d
	testl	%ebx, %ebx
	jne	.LBB10_12
	jmp	.LBB10_14
.LBB10_1:
	xorl	%r13d, %r13d
	jmp	.LBB10_15
.LBB10_13:                              #   in Loop: Header=BB10_12 Depth=1
	movl	$1, %eax
	movl	%ebx, %ecx
	shll	%cl, %eax
	orl	%eax, %r12d
	.p2align	4, 0x90
.LBB10_11:                              #   in Loop: Header=BB10_12 Depth=1
	testl	%ebx, %ebx
	je	.LBB10_14
.LBB10_12:                              # =>This Inner Loop Header: Depth=1
	decl	%ebx
	movq	%r14, %rdi
	callq	biari_decode_symbol_eq_prob
	cmpl	$1, %eax
	jne	.LBB10_11
	jmp	.LBB10_13
.LBB10_14:                              # %exp_golomb_decode_eq_prob.exit
	addl	%r12d, %r15d
	subl	%r13d, %r15d
	incl	%r15d
	movl	%r15d, %r13d
	jmp	.LBB10_15
.LBB10_6:
	negl	%r13d
.LBB10_15:
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	unary_exp_golomb_mv_decode, .Lfunc_end10-unary_exp_golomb_mv_decode
	.cfi_endproc

	.globl	readB8_typeInfo_CABAC
	.p2align	4, 0x90
	.type	readB8_typeInfo_CABAC,@function
readB8_typeInfo_CABAC:                  # @readB8_typeInfo_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 48
.Lcfi62:
	.cfi_offset %rbx, -48
.Lcfi63:
	.cfi_offset %r12, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rdi, %r14
	cmpl	$1, 44(%rsi)
	movq	5592(%rsi), %rax
	movq	48(%rax), %rbx
	jne	.LBB11_1
# BB#5:
	leaq	212(%rbx), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB11_6
# BB#7:
	leaq	216(%rbx), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB11_13
# BB#8:
	leaq	220(%rbx), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	movl	%eax, %ebp
	addq	$224, %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	movl	%eax, %r12d
	testl	%ebp, %ebp
	je	.LBB11_12
# BB#9:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	testl	%r12d, %r12d
	je	.LBB11_11
# BB#10:
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	orl	$10, %ecx
	jmp	.LBB11_14
.LBB11_1:
	leaq	180(%rbx), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jne	.LBB11_15
# BB#2:
	leaq	188(%rbx), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB11_3
# BB#4:
	addq	$192, %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	orl	$2, %ecx
	jmp	.LBB11_15
.LBB11_6:
	xorl	%ecx, %ecx
	jmp	.LBB11_15
.LBB11_13:
	addq	$224, %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	jmp	.LBB11_14
.LBB11_12:
	xorl	%ebp, %ebp
	testl	%r12d, %r12d
	setne	%bpl
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	2(%rcx,%rbp,2), %ecx
	jmp	.LBB11_14
.LBB11_3:
	movl	$1, %ecx
	jmp	.LBB11_15
.LBB11_11:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	setne	%bpl
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	6(%rcx,%rbp,2), %ecx
.LBB11_14:
	incl	%ecx
.LBB11_15:
	movl	%ecx, 4(%r14)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	readB8_typeInfo_CABAC, .Lfunc_end11-readB8_typeInfo_CABAC
	.cfi_endproc

	.globl	readMB_transform_size_flag_CABAC
	.p2align	4, 0x90
	.type	readMB_transform_size_flag_CABAC,@function
readMB_transform_size_flag_CABAC:       # @readMB_transform_size_flag_CABAC
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 32
.Lcfi70:
	.cfi_offset %rbx, -24
.Lcfi71:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	5592(%rsi), %rax
	movq	48(%rax), %r8
	movq	5600(%rsi), %rdi
	movl	4(%rsi), %eax
	imulq	$408, %rax, %rbx        # imm = 0x198
	movq	24(%rdi,%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$0, %esi
	je	.LBB12_2
# BB#1:
	movl	396(%rax), %esi
.LBB12_2:
	movq	32(%rdi,%rbx), %rax
	testq	%rax, %rax
	je	.LBB12_4
# BB#3:
	movl	396(%rax), %ecx
.LBB12_4:
	addl	%esi, %ecx
	movslq	%ecx, %rax
	leaq	408(%r8,%rax,4), %rsi
	movq	%rdx, %rdi
	callq	biari_decode_symbol
	movl	%eax, 4(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	readMB_transform_size_flag_CABAC, .Lfunc_end12-readMB_transform_size_flag_CABAC
	.cfi_endproc

	.globl	readMB_typeInfo_CABAC
	.p2align	4, 0x90
	.type	readMB_typeInfo_CABAC,@function
readMB_typeInfo_CABAC:                  # @readMB_typeInfo_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 80
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movl	4(%rbx), %eax
	movl	44(%rbx), %r14d
	movq	5592(%rbx), %rcx
	movq	48(%rcx), %r12
	movq	5600(%rbx), %rbp
	cmpl	$4, %r14d
	movq	%r13, 16(%rsp)          # 8-byte Spill
	je	.LBB13_9
# BB#1:
	cmpl	$2, %r14d
	jne	.LBB13_25
# BB#2:
	imulq	$408, %rax, %rcx        # imm = 0x198
	movq	24(%rbp,%rcx), %rdx
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	movl	$0, %eax
	je	.LBB13_4
# BB#3:
	movl	40(%rdx), %eax
	cmpl	$13, %eax
	setne	%dl
	cmpl	$9, %eax
	setne	%al
	andb	%dl, %al
	movzbl	%al, %eax
.LBB13_4:
	movq	32(%rbp,%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB13_6
# BB#5:
	movl	40(%rcx), %ecx
	cmpl	$13, %ecx
	setne	%dl
	cmpl	$9, %ecx
	setne	%cl
	andb	%dl, %cl
	movzbl	%cl, %ebx
.LBB13_6:
	addl	%eax, %ebx
	leaq	(%r12,%rbx,4), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	movl	%ebx, 24(%r13)
	testl	%eax, %eax
	je	.LBB13_41
# BB#7:
	movq	%r15, %rdi
	callq	biari_decode_final
	movl	$25, %r13d
	cmpl	$1, %eax
	je	.LBB13_55
# BB#8:
	leaq	16(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shll	$2, %eax
	leal	(%rax,%rax,2), %ebx
	orl	$1, %ebx
	jmp	.LBB13_21
.LBB13_9:
	imulq	$408, %rax, %r14        # imm = 0x198
	movq	24(%rbp,%r14), %rcx
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	movl	$0, %eax
	je	.LBB13_11
# BB#10:
	xorl	%eax, %eax
	cmpl	$12, 40(%rcx)
	setne	%al
.LBB13_11:
	movq	32(%rbp,%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB13_13
# BB#12:
	xorl	%ebx, %ebx
	cmpl	$12, 40(%rcx)
	setne	%bl
.LBB13_13:
	addl	%eax, %ebx
	leaq	44(%r12,%rbx,4), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	movl	%ebx, 24(%r13)
	testl	%eax, %eax
	je	.LBB13_41
# BB#14:
	leaq	24(%rbp,%r14), %rcx
	movq	(%rcx), %rdx
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	movl	$0, %eax
	je	.LBB13_16
# BB#15:
	xorl	%eax, %eax
	cmpl	$9, 40(%rdx)
	setne	%al
.LBB13_16:
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB13_18
# BB#17:
	xorl	%ebx, %ebx
	cmpl	$9, 40(%rcx)
	setne	%bl
.LBB13_18:
	addl	%eax, %ebx
	leaq	(%r12,%rbx,4), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	movl	%ebx, 24(%r13)
	testl	%eax, %eax
	je	.LBB13_42
# BB#19:
	movq	%r15, %rdi
	callq	biari_decode_final
	movl	$26, %r13d
	cmpl	$1, %eax
	je	.LBB13_55
# BB#20:
	leaq	16(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shll	$2, %eax
	leal	(%rax,%rax,2), %ebx
	orl	$2, %ebx
.LBB13_21:
	leaq	20(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB13_23
# BB#22:
	leaq	24(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	4(%rbx,%rcx,4), %ebx
.LBB13_23:
	leaq	28(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rbx,%rax,2), %ebx
	addq	$32, %r12
.LBB13_24:                              # %.thread
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	biari_decode_symbol
	movl	%eax, %r13d
	addl	%ebx, %r13d
	jmp	.LBB13_55
.LBB13_25:
	cmpl	$1, %r14d
	jne	.LBB13_37
# BB#26:
	imulq	$408, %rax, %rcx        # imm = 0x198
	movq	24(%rbp,%rcx), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.LBB13_28
# BB#27:
	cmpl	$0, 40(%rdx)
	setne	%al
.LBB13_28:
	movq	32(%rbp,%rcx), %rdx
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB13_30
# BB#29:
	cmpl	$0, 40(%rdx)
	setne	%cl
.LBB13_30:
	addq	%rax, %rcx
	leaq	88(%r12,%rcx,4), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB13_55
# BB#31:
	leaq	104(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB13_45
# BB#32:
	leaq	108(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	movl	%eax, %r13d
	leaq	112(%r12), %rsi
	movq	%r15, %rdi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	biari_decode_symbol
	xorl	%ebp, %ebp
	testl	%r13d, %r13d
	je	.LBB13_46
# BB#33:
	testl	%eax, %eax
	setne	%bpl
	leal	12(,%rbp,8), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%r15, %rdi
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r13, %rsi
	callq	biari_decode_symbol
	testl	%eax, %eax
	leal	16(,%rbp,8), %ebp
	cmovel	4(%rsp), %ebp           # 4-byte Folded Reload
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	biari_decode_symbol
	movl	%ebp, %r13d
	orl	$2, %r13d
	testl	%eax, %eax
	cmovel	%ebp, %r13d
	movl	%r13d, %eax
	andb	$62, %al
	cmpb	$22, %al
	je	.LBB13_48
# BB#34:
	cmpb	$24, %r13b
	je	.LBB13_50
# BB#35:
	cmpb	$26, %r13b
	jne	.LBB13_49
# BB#36:
	movl	$22, %r13d
	cmpl	$23, %r13d
	jle	.LBB13_52
	jmp	.LBB13_53
.LBB13_41:
	xorl	%r13d, %r13d
	jmp	.LBB13_55
.LBB13_37:
	leaq	60(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB13_43
# BB#38:
	leaq	72(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	movl	$7, %r13d
	testl	%eax, %eax
	je	.LBB13_39
.LBB13_51:                              # %.thread221
	cmpl	$23, %r13d
	jg	.LBB13_53
.LBB13_52:                              # %.thread221
	cmpl	$1, 44(%rbx)
	je	.LBB13_55
.LBB13_53:
	movq	%r15, %rdi
	callq	biari_decode_final
	cmpl	$1, %eax
	jne	.LBB13_56
# BB#54:
	cmpl	$1, %r14d
	movl	$48, %eax
	movl	$31, %r13d
	cmovel	%eax, %r13d
	jmp	.LBB13_55
.LBB13_42:
	movl	$1, %r13d
	jmp	.LBB13_55
.LBB13_43:
	leaq	64(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB13_47
# BB#44:
	addq	$72, %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	biari_decode_symbol
	xorl	%r13d, %r13d
	testl	%eax, %eax
	sete	%r13b
	orl	$2, %r13d
	jmp	.LBB13_55
.LBB13_45:
	addq	$112, %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	biari_decode_symbol
	cmpl	$1, %eax
	movl	$1, %r13d
	sbbl	$-1, %r13d
	jmp	.LBB13_55
.LBB13_39:
	movl	$6, %r13d
	jmp	.LBB13_55
.LBB13_56:
	leaq	76(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%rax,2), %eax
	leal	(%r13,%rax,4), %ebp
	leaq	80(%r12), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB13_58
# BB#57:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	4(%rbp,%rcx,4), %ebp
.LBB13_58:
	addq	$84, %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	biari_decode_symbol
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rbp,%rax,2), %ebx
	jmp	.LBB13_24
.LBB13_46:
	testl	%eax, %eax
	setne	%bpl
	leal	3(,%rbp,4), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	biari_decode_symbol
	testl	%eax, %eax
	leal	5(,%rbp,4), %r13d
	cmovel	4(%rsp), %r13d          # 4-byte Folded Reload
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	biari_decode_symbol
	cmpl	$1, %eax
	sbbl	$-1, %r13d
	cmpl	$7, %r13d
	jae	.LBB13_51
	jmp	.LBB13_55
.LBB13_47:
	addq	$68, %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	1(%rcx,%rcx,2), %r13d
.LBB13_55:                              # %.thread
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r13d, 4(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_48:
	movl	$23, %r13d
.LBB13_49:
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	biari_decode_symbol
	cmpl	$1, %eax
	sbbl	$-1, %r13d
	cmpl	$23, %r13d
	jle	.LBB13_52
	jmp	.LBB13_53
.LBB13_50:
	movl	$11, %r13d
	cmpl	$23, %r13d
	jle	.LBB13_52
	jmp	.LBB13_53
.Lfunc_end13:
	.size	readMB_typeInfo_CABAC, .Lfunc_end13-readMB_typeInfo_CABAC
	.cfi_endproc

	.globl	readIntraPredMode_CABAC
	.p2align	4, 0x90
	.type	readIntraPredMode_CABAC,@function
readIntraPredMode_CABAC:                # @readIntraPredMode_CABAC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 32
.Lcfi88:
	.cfi_offset %rbx, -32
.Lcfi89:
	.cfi_offset %r14, -24
.Lcfi90:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rdi, %r14
	movq	5592(%rsi), %rax
	movq	56(%rax), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	movl	%eax, %ecx
	movl	$-1, %eax
	cmpl	$1, %ecx
	je	.LBB14_2
# BB#1:
	movl	$0, 4(%r14)
	addq	$4, %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	orl	%eax, 4(%r14)
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	addl	%eax, %eax
	orl	%eax, 4(%r14)
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	shll	$2, %eax
	orl	4(%r14), %eax
.LBB14_2:
	movl	%eax, 4(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	readIntraPredMode_CABAC, .Lfunc_end14-readIntraPredMode_CABAC
	.cfi_endproc

	.globl	readRefFrame_CABAC
	.p2align	4, 0x90
	.type	readRefFrame_CABAC,@function
readRefFrame_CABAC:                     # @readRefFrame_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi97:
	.cfi_def_cfa_offset 128
.Lcfi98:
	.cfi_offset %rbx, -56
.Lcfi99:
	.cfi_offset %r12, -48
.Lcfi100:
	.cfi_offset %r13, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	movq	5592(%r12), %rax
	movq	48(%rax), %rbx
	movq	5600(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	4(%r12), %r13d
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movslq	8(%r14), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	5608(%r12), %eax
	movl	5612(%r12), %edx
	leal	-1(,%rax,4), %esi
	shll	$2, %edx
	leaq	48(%rsp), %rcx
	movl	%r13d, %edi
	callq	getLuma4x4Neighbour
	movl	4(%r12), %edi
	movl	5608(%r12), %esi
	shll	$2, %esi
	movl	5612(%r12), %eax
	leal	-1(,%rax,4), %edx
	leaq	24(%rsp), %rcx
	callq	getLuma4x4Neighbour
	movl	56(%rsp), %r9d
	movl	60(%rsp), %r8d
	xorl	%r10d, %r10d
	cmpl	$0, 24(%rsp)
	je	.LBB15_12
# BB#1:
	movq	5600(%r12), %rsi
	movslq	28(%rsp), %rcx
	imulq	$408, %rcx, %rdi        # imm = 0x198
	movl	40(%rsi,%rdi), %edx
	cmpl	$14, %edx
	je	.LBB15_12
# BB#2:
	movl	32(%rsp), %ecx
	movl	36(%rsp), %ebp
	testl	%edx, %edx
	jne	.LBB15_4
# BB#3:
	cmpl	$1, 44(%r12)
	je	.LBB15_12
.LBB15_4:
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	movl	%edx, %ecx
	sarl	%ecx
	shrl	$31, %edx
	addl	%ecx, %edx
	andl	$-2, %edx
	subl	%edx, %ecx
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	movl	%edx, %ebp
	sarl	%ebp
	shrl	$31, %edx
	addl	%ebp, %edx
	andl	$2147483646, %edx       # imm = 0x7FFFFFFE
	subl	%edx, %ebp
	leal	(%rcx,%rbp,2), %ecx
	movslq	%ecx, %rcx
	leaq	(%rsi,%rdi), %rdx
	cmpb	$0, 328(%rcx,%rdx)
	jne	.LBB15_6
# BB#5:
	cmpb	$2, 332(%rcx,%rdx)
	je	.LBB15_12
.LBB15_6:
	cmpl	$0, 5624(%r12)
	je	.LBB15_10
# BB#7:
	imulq	$408, %r13, %rax        # imm = 0x198
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 356(%rcx,%rax)
	jne	.LBB15_10
# BB#8:
	cmpl	$1, 356(%rsi,%rdi)
	jne	.LBB15_10
# BB#9:
	movslq	44(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movslq	40(%rsp), %rdx
	xorl	%r10d, %r10d
	cmpb	$1, (%rcx,%rdx)
	jmp	.LBB15_11
.LBB15_10:
	movslq	44(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movslq	40(%rsp), %rdx
	xorl	%r10d, %r10d
	cmpb	$0, (%rcx,%rdx)
.LBB15_11:
	setg	%r10b
	addl	%r10d, %r10d
.LBB15_12:
	xorl	%ebp, %ebp
	cmpl	$0, 48(%rsp)
	je	.LBB15_27
# BB#13:
	movq	5600(%r12), %r11
	movslq	52(%rsp), %rcx
	imulq	$408, %rcx, %rdi        # imm = 0x198
	movl	40(%r11,%rdi), %ecx
	cmpl	$14, %ecx
	je	.LBB15_27
# BB#14:
	testl	%ecx, %ecx
	jne	.LBB15_18
# BB#15:
	cmpl	$1, 44(%r12)
	je	.LBB15_27
.LBB15_18:
	movl	%r9d, %ecx
	shrl	$31, %ecx
	movl	%r8d, %edx
	shrl	$31, %edx
	addl	%r9d, %ecx
	addl	%r8d, %edx
	movl	%ecx, %eax
	sarl	%eax
	shrl	$31, %ecx
	movl	%edx, %esi
	sarl	%esi
	shrl	$31, %edx
	addl	%eax, %ecx
	addl	%esi, %edx
	andl	$-2, %ecx
	andl	$2147483646, %edx       # imm = 0x7FFFFFFE
	subl	%ecx, %eax
	subl	%edx, %esi
	leal	(%rax,%rsi,2), %eax
	movslq	%eax, %rcx
	leaq	(%r11,%rdi), %rdx
	cmpb	$0, 328(%rcx,%rdx)
	jne	.LBB15_21
# BB#19:
	cmpb	$2, 332(%rcx,%rdx)
	jne	.LBB15_21
.LBB15_27:
	xorl	%ecx, %ecx
	jmp	.LBB15_28
.LBB15_21:
	cmpl	$0, 5624(%r12)
	je	.LBB15_25
# BB#22:
	imulq	$408, %r13, %rax        # imm = 0x198
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 356(%rcx,%rax)
	jne	.LBB15_25
# BB#23:
	cmpl	$1, 356(%r11,%rdi)
	jne	.LBB15_25
# BB#24:
	movslq	68(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movslq	64(%rsp), %rcx
	cmpb	$1, (%rax,%rcx)
	jmp	.LBB15_26
.LBB15_25:
	movslq	68(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movslq	64(%rsp), %rcx
	cmpb	$0, (%rax,%rcx)
.LBB15_26:
	setg	%cl
.LBB15_28:
	movzbl	%cl, %eax
	orl	%r10d, %eax
	movl	%eax, 24(%r14)
	leaq	328(%rbx,%rax,4), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB15_33
# BB#29:
	leaq	344(%rbx), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB15_32
# BB#30:
	addq	$348, %rbx              # imm = 0x15C
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB15_31:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	incl	%ebp
	testl	%eax, %eax
	jne	.LBB15_31
	jmp	.LBB15_33
.LBB15_32:
	movl	$1, %ebp
.LBB15_33:                              # %unary_bin_decode.exit
	movl	%ebp, 4(%r14)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	readRefFrame_CABAC, .Lfunc_end15-readRefFrame_CABAC
	.cfi_endproc

	.globl	unary_bin_decode
	.p2align	4, 0x90
	.type	unary_bin_decode,@function
unary_bin_decode:                       # @unary_bin_decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -32
.Lcfi108:
	.cfi_offset %r14, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB16_1
# BB#2:
	movslq	%ebp, %rax
	leaq	(%rbx,%rax,4), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_3:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	biari_decode_symbol
	incl	%ebp
	testl	%eax, %eax
	jne	.LBB16_3
	jmp	.LBB16_4
.LBB16_1:
	xorl	%ebp, %ebp
.LBB16_4:                               # %.loopexit
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end16:
	.size	unary_bin_decode, .Lfunc_end16-unary_bin_decode
	.cfi_endproc

	.globl	readDquant_CABAC
	.p2align	4, 0x90
	.type	readDquant_CABAC,@function
readDquant_CABAC:                       # @readDquant_CABAC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 48
.Lcfi115:
	.cfi_offset %rbx, -40
.Lcfi116:
	.cfi_offset %r12, -32
.Lcfi117:
	.cfi_offset %r14, -24
.Lcfi118:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rdi, %r14
	movq	5592(%rsi), %rax
	movq	48(%rax), %r12
	xorl	%eax, %eax
	cmpl	$0, last_dquant(%rip)
	setne	%al
	leaq	376(%r12,%rax,4), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB17_1
# BB#2:
	leaq	384(%r12), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB17_3
# BB#4:
	addq	$388, %r12              # imm = 0x184
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB17_5:                               # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	biari_decode_symbol
	incl	%ebx
	testl	%eax, %eax
	jne	.LBB17_5
	jmp	.LBB17_6
.LBB17_1:
	xorl	%ebx, %ebx
	jmp	.LBB17_6
.LBB17_3:
	movl	$1, %ebx
.LBB17_6:                               # %unary_bin_decode.exit
	leal	1(%rbx), %eax
	shrl	$31, %eax
	leal	1(%rbx,%rax), %eax
	sarl	%eax
	movl	%eax, %ecx
	negl	%ecx
	testb	$1, %bl
	cmovnel	%eax, %ecx
	movl	%ecx, 4(%r14)
	movl	%ecx, last_dquant(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	readDquant_CABAC, .Lfunc_end17-readDquant_CABAC
	.cfi_endproc

	.globl	readCBP_CABAC
	.p2align	4, 0x90
	.type	readCBP_CABAC,@function
readCBP_CABAC:                          # @readCBP_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi125:
	.cfi_def_cfa_offset 144
.Lcfi126:
	.cfi_offset %rbx, -56
.Lcfi127:
	.cfi_offset %r12, -48
.Lcfi128:
	.cfi_offset %r13, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	5592(%rbp), %rax
	movq	56(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	5600(%rbp), %rcx
	movl	4(%rbp), %eax
	imulq	$408, %rax, %rax        # imm = 0x198
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	24(%rcx,%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	jmp	.LBB18_2
	.p2align	4, 0x90
.LBB18_1:                               # %.us-lcssa.us.thread
                                        #   in Loop: Header=BB18_2 Depth=1
	movzbl	%al, %eax
	xorl	%ecx, %ecx
	testl	%r14d, %r15d
	sete	%cl
	leaq	(%rcx,%rax,2), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	24(%rcx,%rax,4), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	(%r15,%rcx,2), %r15d
	movl	$2, %ebx
	jmp	.LBB18_2
.LBB18_23:                              #   in Loop: Header=BB18_2 Depth=1
	testb	$8, 300(%rax)
	sete	%al
	jmp	.LBB18_1
	.p2align	4, 0x90
.LBB18_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leal	(,%rbx,4), %edx
	movl	$1, %r14d
	movl	%ebx, %ecx
	shll	%cl, %r14d
	testl	%ebx, %ebx
	je	.LBB18_6
# BB#3:                                 # %.preheader.split.preheader
                                        #   in Loop: Header=BB18_2 Depth=1
	movl	4(%rbp), %edi
	movl	$-1, %esi
	leaq	56(%rsp), %rcx
	callq	getLuma4x4Neighbour
	cmpl	$0, 56(%rsp)
	je	.LBB18_9
# BB#4:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	5600(%rbp), %rax
	movslq	60(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$14, 40(%rax,%rcx)
	jne	.LBB18_10
# BB#5:                                 #   in Loop: Header=BB18_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB18_11
	.p2align	4, 0x90
.LBB18_6:                               # %.preheader.split.us.preheader
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB18_12
# BB#7:                                 #   in Loop: Header=BB18_2 Depth=1
	cmpl	$14, 40(%rax)
	jne	.LBB18_13
# BB#8:                                 #   in Loop: Header=BB18_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB18_14
	.p2align	4, 0x90
.LBB18_9:                               #   in Loop: Header=BB18_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB18_11
.LBB18_10:                              #   in Loop: Header=BB18_2 Depth=1
	movl	300(%rax,%rcx), %eax
	movl	68(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	orl	$1, %edx
	btl	%edx, %eax
	setae	%al
.LBB18_11:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB18_2 Depth=1
	movzbl	%al, %eax
	leal	(%r15,%r15), %ecx
	notl	%ecx
	andl	$2, %ecx
	orq	%rax, %rcx
	leaq	24(%r12,%rcx,4), %rsi
	movq	%r13, %rdi
	callq	biari_decode_symbol
	movl	%eax, %ebp
	testl	%ebp, %ebp
	cmovnel	%r14d, %ebp
	addl	%r15d, %ebp
	movl	%ebp, %eax
	notl	%eax
	movl	%r14d, %edx
	movl	%ebx, %r14d
	orl	$1, %r14d
	andl	$2, %eax
	xorl	%ecx, %ecx
	testl	%edx, %ebp
	sete	%cl
	orq	%rcx, %rax
	leaq	24(%r12,%rax,4), %rsi
	movq	%r13, %rdi
	callq	biari_decode_symbol
	movl	$1, %r15d
	movl	%r14d, %ecx
	shll	%cl, %r15d
	testl	%eax, %eax
	cmovel	%eax, %r15d
	addl	%ebp, %r15d
	addl	$2, %ebx
	cmpl	$4, %ebx
	movq	80(%rsp), %rbp          # 8-byte Reload
	jl	.LBB18_2
	jmp	.LBB18_24
.LBB18_12:                              #   in Loop: Header=BB18_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB18_14
.LBB18_13:                              #   in Loop: Header=BB18_2 Depth=1
	testb	$4, 300(%rax)
	sete	%al
.LBB18_14:                              #   in Loop: Header=BB18_2 Depth=1
	movzbl	%al, %ebx
	movl	4(%rbp), %edi
	movl	$-1, %esi
	leaq	56(%rsp), %rcx
	callq	getLuma4x4Neighbour
	cmpl	$0, 56(%rsp)
	je	.LBB18_17
# BB#15:                                #   in Loop: Header=BB18_2 Depth=1
	movq	5600(%rbp), %rax
	movslq	60(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$14, 40(%rax,%rcx)
	jne	.LBB18_18
.LBB18_17:                              #   in Loop: Header=BB18_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB18_19
.LBB18_18:                              #   in Loop: Header=BB18_2 Depth=1
	movl	300(%rax,%rcx), %eax
	movl	68(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	orl	$1, %edx
	btl	%edx, %eax
	setae	%al
.LBB18_19:                              # %.preheader.split.us.1102
                                        #   in Loop: Header=BB18_2 Depth=1
	movzbl	%al, %eax
	leaq	(%rax,%rbx,2), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	24(%rcx,%rax,4), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	biari_decode_symbol
	cmpl	$1, %eax
	sbbl	$-1, %r15d
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB18_22
# BB#20:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	$14, 40(%rax)
	jne	.LBB18_23
.LBB18_22:                              #   in Loop: Header=BB18_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB18_1
.LBB18_24:
	movq	dec_picture(%rip), %rax
	cmpl	$0, 317044(%rax)
	je	.LBB18_28
# BB#25:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB18_29
# BB#26:
	cmpl	$14, 40(%rcx)
	jne	.LBB18_30
# BB#27:
	movl	$2, %eax
	jmp	.LBB18_31
.LBB18_28:
	movq	24(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB18_52
.LBB18_29:
	xorl	%eax, %eax
	jmp	.LBB18_31
.LBB18_30:
	xorl	%eax, %eax
	cmpl	$15, 300(%rcx)
	setg	%al
	addq	%rax, %rax
.LBB18_31:
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	32(%rcx,%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB18_34
# BB#32:
	cmpl	$14, 40(%rdx)
	jne	.LBB18_35
# BB#33:
	movl	$1, %ecx
	jmp	.LBB18_36
.LBB18_34:
	xorl	%ecx, %ecx
	jmp	.LBB18_36
.LBB18_35:
	xorl	%ecx, %ecx
	cmpl	$15, 300(%rdx)
	setg	%cl
.LBB18_36:
	orq	%rax, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	40(%rax,%rcx,4), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB18_52
# BB#37:
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	32(%rax,%rcx), %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB18_43
# BB#38:
	cmpl	$14, 40(%rax)
	jne	.LBB18_41
# BB#39:
	movl	$2, %eax
	jmp	.LBB18_44
.LBB18_41:
	movl	300(%rax), %edx
	cmpl	$16, %edx
	jl	.LBB18_43
# BB#42:
	andl	$-16, %edx
	xorl	%eax, %eax
	cmpl	$32, %edx
	sete	%al
	addq	%rax, %rax
	jmp	.LBB18_44
.LBB18_43:
	xorl	%eax, %eax
.LBB18_44:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB18_50
# BB#45:
	cmpl	$14, 40(%rcx)
	jne	.LBB18_48
# BB#46:
	movl	$1, %ecx
	jmp	.LBB18_51
.LBB18_48:
	movl	300(%rcx), %edx
	cmpl	$16, %edx
	jl	.LBB18_50
# BB#49:
	andl	$-16, %edx
	xorl	%ecx, %ecx
	cmpl	$32, %edx
	sete	%cl
	jmp	.LBB18_51
.LBB18_50:
	xorl	%ecx, %ecx
.LBB18_51:
	orq	%rax, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	56(%rax,%rcx,4), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	biari_decode_symbol
	cmpl	$1, %eax
	movl	$32, %eax
	movl	$16, %ecx
	cmovel	%eax, %ecx
	addl	%r15d, %ecx
	movl	%ecx, %r15d
.LBB18_52:
	movl	%r15d, 4(%rbx)
	testl	%r15d, %r15d
	jne	.LBB18_54
# BB#53:
	movl	$0, last_dquant(%rip)
.LBB18_54:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	readCBP_CABAC, .Lfunc_end18-readCBP_CABAC
	.cfi_endproc

	.globl	readCIPredMode_CABAC
	.p2align	4, 0x90
	.type	readCIPredMode_CABAC,@function
readCIPredMode_CABAC:                   # @readCIPredMode_CABAC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 48
.Lcfi137:
	.cfi_offset %rbx, -40
.Lcfi138:
	.cfi_offset %r12, -32
.Lcfi139:
	.cfi_offset %r14, -24
.Lcfi140:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rdi, %r14
	movq	5592(%rsi), %rax
	movq	56(%rax), %r12
	movq	5600(%rsi), %rcx
	movl	4(%rsi), %eax
	imulq	$408, %rax, %rdx        # imm = 0x198
	movq	24(%rcx,%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB19_3
# BB#1:
	cmpl	$14, 40(%rsi)
	jne	.LBB19_4
.LBB19_3:
	xorl	%eax, %eax
	jmp	.LBB19_5
.LBB19_4:
	xorl	%eax, %eax
	cmpl	$0, 352(%rsi)
	setne	%al
.LBB19_5:
	movq	32(%rcx,%rdx), %rdx
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	je	.LBB19_8
# BB#6:
	cmpl	$14, 40(%rdx)
	jne	.LBB19_9
.LBB19_8:
	xorl	%ecx, %ecx
	jmp	.LBB19_10
.LBB19_9:
	xorl	%ecx, %ecx
	cmpl	$0, 352(%rdx)
	setne	%cl
.LBB19_10:
	addq	%rax, %rcx
	leaq	8(%r12,%rcx,4), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB19_14
# BB#11:
	addq	$20, %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB19_13
# BB#12:
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	biari_decode_symbol
	xorl	%ebx, %ebx
	testl	%eax, %eax
	setne	%bl
	orl	$2, %ebx
	jmp	.LBB19_14
.LBB19_13:
	movl	$1, %ebx
.LBB19_14:                              # %unary_bin_max_decode.exit
	movl	%ebx, 4(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	readCIPredMode_CABAC, .Lfunc_end19-readCIPredMode_CABAC
	.cfi_endproc

	.globl	unary_bin_max_decode
	.p2align	4, 0x90
	.type	unary_bin_max_decode,@function
unary_bin_max_decode:                   # @unary_bin_max_decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi144:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 48
.Lcfi146:
	.cfi_offset %rbx, -48
.Lcfi147:
	.cfi_offset %r12, -40
.Lcfi148:
	.cfi_offset %r14, -32
.Lcfi149:
	.cfi_offset %r15, -24
.Lcfi150:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r15
	callq	biari_decode_symbol
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	testl	%eax, %eax
	je	.LBB20_1
# BB#2:
	cmpl	$1, %r14d
	je	.LBB20_7
# BB#3:
	movslq	%ebp, %rax
	leaq	(%rbx,%rax,4), %r12
	leal	-1(%r14), %ebp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB20_4:                               # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	biari_decode_symbol
	movl	%eax, %ecx
	leal	1(%rbx), %eax
	testl	%ecx, %ecx
	je	.LBB20_6
# BB#5:                                 #   in Loop: Header=BB20_4 Depth=1
	cmpl	%ebp, %ebx
	movl	%eax, %ebx
	jb	.LBB20_4
.LBB20_6:                               # %.critedge
	leal	-1(%rax), %edx
	cmpl	%eax, %r14d
	cmovnel	%edx, %eax
	testl	%ecx, %ecx
	cmovel	%edx, %eax
	jmp	.LBB20_7
.LBB20_1:
	xorl	%eax, %eax
.LBB20_7:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	unary_bin_max_decode, .Lfunc_end20-unary_bin_max_decode
	.cfi_endproc

	.globl	read_and_store_CBP_block_bit
	.p2align	4, 0x90
	.type	read_and_store_CBP_block_bit,@function
read_and_store_CBP_block_bit:           # @read_and_store_CBP_block_bit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi153:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi154:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi155:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi157:
	.cfi_def_cfa_offset 160
.Lcfi158:
	.cfi_offset %rbx, -56
.Lcfi159:
	.cfi_offset %r12, -48
.Lcfi160:
	.cfi_offset %r13, -40
.Lcfi161:
	.cfi_offset %r14, -32
.Lcfi162:
	.cfi_offset %r15, -24
.Lcfi163:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	cmpl	$7, %ebp
	jne	.LBB21_1
# BB#2:
	cmpl	$0, 5620(%r14)
	sete	%r9b
	setne	%bl
	jmp	.LBB21_3
.LBB21_1:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
.LBB21_3:
	leal	-1(%rbp), %r13d
	leal	-6(%rbp), %esi
	movl	%esi, %ecx
	andb	$15, %cl
	movb	$13, %al
	shrb	%cl, %al
	xorl	%edx, %edx
	cmpl	$3, %esi
	ja	.LBB21_4
# BB#5:
	andb	$1, %al
	movl	$0, %r10d
	movl	$0, %edi
	je	.LBB21_7
# BB#6:
	cmpl	$0, 5620(%r14)
	sete	%r10b
	setne	%dil
	jmp	.LBB21_7
.LBB21_4:
	xorl	%r10d, %r10d
	xorl	%edi, %edi
.LBB21_7:                               # %.thread156
	cmpl	$5, %r13d
	setb	%al
	orb	%r9b, %al
	movl	%ebx, 28(%rsp)          # 4-byte Spill
	orb	%bl, %al
	cmpb	$1, %al
	movl	$0, %r11d
	jne	.LBB21_9
# BB#8:
	movl	5608(%r14), %r11d
	movl	5612(%r14), %edx
.LBB21_9:                               # %.thread157
	cmpl	$5, %r13d
	setb	%r8b
	xorl	%ecx, %ecx
	testl	%ebp, %ebp
	sete	%al
	setne	%cl
	movl	%eax, %ebx
	orb	%r8b, %bl
	movl	$17, %ebx
	cmovnel	%ecx, %ebx
	testb	%r9b, %r9b
	movl	$19, %ecx
	movl	$35, %esi
	cmovnel	%ecx, %esi
	movl	%edi, 20(%rsp)          # 4-byte Spill
	testb	%dil, %dil
	movl	$18, %r12d
	cmovel	%esi, %r12d
	movl	%r10d, 24(%rsp)         # 4-byte Spill
	testb	%r10b, %r10b
	cmovnel	%ebx, %r12d
	orb	%r8b, %al
	cmovnel	%ebx, %r12d
	movl	%r9d, %ebx
	cmpl	$0, 5616(%r14)
	setne	3(%rsp)                 # 1-byte Folded Spill
	movl	4(%r14), %edi
	leal	(,%r11,4), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%r11, 80(%rsp)          # 8-byte Spill
	leal	-1(,%r11,4), %esi
	shll	$2, %edx
	cmpl	$5, %ebp
	movl	%ebx, 16(%rsp)          # 4-byte Spill
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	leaq	56(%rsp), %rcx
	ja	.LBB21_11
# BB#10:
	movq	%rdx, %rbx
	callq	getLuma4x4Neighbour
	movl	4(%r14), %edi
	leal	-1(%rbx), %edx
	leaq	32(%rsp), %rcx
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	getLuma4x4Neighbour
	xorl	%ecx, %ecx
	cmpl	$4, %r13d
	movl	$0, %edx
	movl	$0, %ebx
	jbe	.LBB21_12
	jmp	.LBB21_17
.LBB21_11:
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movl	%r13d, %ebp
	movq	%r15, %r13
	movq	%rdx, %r15
	callq	getChroma4x4Neighbour
	movl	4(%r14), %edi
	leal	-1(%r15), %edx
	movq	%r13, %r15
	movl	%ebp, %r13d
	movq	96(%rsp), %rbp          # 8-byte Reload
	leaq	32(%rsp), %rcx
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	getChroma4x4Neighbour
	movl	28(%rsp), %eax          # 4-byte Reload
	orb	%bl, %al
	xorl	%ecx, %ecx
	cmpb	$1, %al
	movl	$0, %edx
	movl	$0, %ebx
	jne	.LBB21_17
.LBB21_12:
	xorl	%ecx, %ecx
	cmpl	$0, 56(%rsp)
	movl	$0, %edx
	je	.LBB21_14
# BB#13:
	movl	68(%rsp), %edx
	shll	$2, %edx
	addl	64(%rsp), %edx
.LBB21_14:
	cmpl	$0, 32(%rsp)
	je	.LBB21_16
# BB#15:
	movl	44(%rsp), %ecx
	shll	$2, %ecx
	addl	40(%rsp), %ecx
.LBB21_16:
	movl	$1, %eax
	cmpl	$2, %ebp
	je	.LBB21_24
.LBB21_17:                              # %.thread
	movb	3(%rsp), %al            # 1-byte Reload
	movb	%al, %bl
	cmpl	$0, 32(%rsp)
	movl	%ebx, %eax
	je	.LBB21_20
# BB#18:
	movq	5600(%r14), %rsi
	movslq	36(%rsp), %rax
	imulq	$408, %rax, %rdi        # imm = 0x198
	movl	$1, %eax
	cmpl	$14, 40(%rsi,%rdi)
	je	.LBB21_20
# BB#19:
	addl	%r12d, %ecx
	movl	$1, %eax
	shlq	%cl, %rax
	andq	312(%rsi,%rdi), %rax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarq	%cl, %rax
.LBB21_20:
	cmpl	$0, 56(%rsp)
	je	.LBB21_23
# BB#21:
	movq	5600(%r14), %rsi
	movslq	60(%rsp), %rcx
	imulq	$408, %rcx, %rdi        # imm = 0x198
	movl	$1, %ebx
	cmpl	$14, 40(%rsi,%rdi)
	je	.LBB21_23
# BB#22:
	addl	%r12d, %edx
	movl	$1, %ebx
	movl	%edx, %ecx
	shlq	%cl, %rbx
	andq	312(%rsi,%rdi), %rbx
	sarq	%cl, %rbx
.LBB21_23:
	leal	(%rbx,%rax,2), %eax
	movq	5592(%r14), %rcx
	movslq	%ebp, %rdx
	movslq	type2ctx_bcbp(,%rdx,4), %rdx
	cltq
	shlq	$4, %rdx
	addq	56(%rcx), %rdx
	leaq	72(%rdx,%rax,4), %rsi
	movq	%r15, %rdi
	callq	biari_decode_symbol
.LBB21_24:
	testl	%ebp, %ebp
	je	.LBB21_25
# BB#26:
	cmpl	$4, %r13d
	ja	.LBB21_28
# BB#27:
	movq	88(%rsp), %rcx          # 8-byte Reload
	orl	$1, %ecx
	addl	80(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, %edx
	testl	%eax, %eax
	jne	.LBB21_33
	jmp	.LBB21_40
.LBB21_25:
	xorl	%edx, %edx
	testl	%eax, %eax
	jne	.LBB21_33
	jmp	.LBB21_40
.LBB21_28:
	movl	24(%rsp), %ecx          # 4-byte Reload
	cmpb	$1, %cl
	movl	$17, %edx
	adcl	$0, %edx
	movl	20(%rsp), %esi          # 4-byte Reload
	orb	%cl, %sil
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	jne	.LBB21_32
# BB#29:
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB21_31
# BB#30:
	leal	19(%rcx,%rsi), %edx
	testl	%eax, %eax
	jne	.LBB21_33
	jmp	.LBB21_40
.LBB21_31:
	leal	35(%rcx,%rsi), %edx
.LBB21_32:
	testl	%eax, %eax
	je	.LBB21_40
.LBB21_33:
	movl	$1, %esi
	cmpl	$3, %ebp
	je	.LBB21_36
# BB#34:
	cmpl	$2, %ebp
	jne	.LBB21_38
# BB#35:
	movl	$1, %edi
	movl	%edx, %ecx
	shlq	%cl, %rdi
	leal	1(%rdx), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rbp
	leal	4(%rdx), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rbx
	addl	$5, %edx
	movl	%edx, %ecx
	shlq	%cl, %rsi
	orq	%rdi, %rbp
	orq	%rbx, %rsi
	orq	%rbp, %rsi
	jmp	.LBB21_37
.LBB21_36:
	movl	$1, %edi
	movl	%edx, %ecx
	shlq	%cl, %rdi
	incl	%edx
	movl	%edx, %ecx
	shlq	%cl, %rsi
	orq	%rdi, %rsi
.LBB21_37:
	movq	8(%rsp), %rcx           # 8-byte Reload
	orq	%rsi, 312(%rcx)
.LBB21_40:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_38:
	movl	%edx, %ecx
	shlq	%cl, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	orq	312(%rcx), %rsi
	movq	%rsi, 312(%rcx)
	cmpl	$4, %ebp
	jne	.LBB21_40
# BB#39:
	addl	$4, %edx
	movl	$1, %edi
	movl	%edx, %ecx
	shlq	%cl, %rdi
	orq	%rdi, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rsi, 312(%rcx)
	jmp	.LBB21_40
.Lfunc_end21:
	.size	read_and_store_CBP_block_bit, .Lfunc_end21-read_and_store_CBP_block_bit
	.cfi_endproc

	.globl	read_significance_map
	.p2align	4, 0x90
	.type	read_significance_map,@function
read_significance_map:                  # @read_significance_map
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi166:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi167:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi168:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi170:
	.cfi_def_cfa_offset 112
.Lcfi171:
	.cfi_offset %rbx, -56
.Lcfi172:
	.cfi_offset %r12, -48
.Lcfi173:
	.cfi_offset %r13, -40
.Lcfi174:
	.cfi_offset %r14, -32
.Lcfi175:
	.cfi_offset %r15, -24
.Lcfi176:
	.cfi_offset %rbp, -16
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%rsi, %r12
	movslq	%ecx, %r10
	movb	$1, %al
	cmpl	$0, 5584(%rdx)
	jne	.LBB22_2
# BB#1:
	cmpl	$0, 356(%rdi)
	setne	%al
.LBB22_2:
	movl	maxpos(,%r10,4), %ebx
	movl	$pos2ctx_map_int, %r8d
	movl	$pos2ctx_map, %r9d
	movq	5592(%rdx), %rdx
	movq	56(%rdx), %rdx
	leaq	232(%rdx), %rsi
	leaq	1832(%rdx), %rbp
	leaq	832(%rdx), %rdi
	leaq	2432(%rdx), %rdx
	testb	%al, %al
	cmovneq	%r8, %r9
	cmovneq	%rbp, %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	cmoveq	%rdi, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	cmpl	$7, %r10d
	je	.LBB22_5
# BB#3:
	cmpl	$1, %ecx
	jne	.LBB22_4
.LBB22_5:
	addq	$-4, (%rsp)             # 8-byte Folded Spill
	movl	$1, %r15d
	jmp	.LBB22_6
.LBB22_4:
	decl	%ebx
	xorl	%r15d, %r15d
.LBB22_6:
	xorl	%ebp, %ebp
	cmpl	%ebx, %r15d
	jge	.LBB22_15
# BB#7:                                 # %.lr.ph82
	movslq	type2ctx_last(,%r10,4), %rax
	movslq	%ebx, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leal	1(%rbx), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	4(%rcx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r10, 48(%rsp)          # 8-byte Spill
	movq	%r10, %rsi
	movq	(%r9,%rsi,8), %r14
	xorl	%ebp, %ebp
	imulq	$60, %rax, %rax
	addq	%rax, 24(%rsp)          # 8-byte Folded Spill
	addq	%rax, 16(%rsp)          # 8-byte Folded Spill
	.p2align	4, 0x90
.LBB22_8:                               # =>This Inner Loop Header: Depth=1
	movslq	%r15d, %r13
	movslq	(%r14,%r13,4), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rsi
	movq	%r12, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB22_13
# BB#9:                                 #   in Loop: Header=BB22_8 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$1, (%rax,%r13,4)
	incl	%ebp
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	pos2ctx_last(,%rax,8), %rax
	movslq	(%rax,%r13,4), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rsi
	movq	%r12, %rdi
	callq	biari_decode_symbol
	testl	%eax, %eax
	je	.LBB22_14
# BB#10:                                # %.preheader
                                        #   in Loop: Header=BB22_8 Depth=1
	cmpl	%ebx, %r15d
	jge	.LBB22_11
# BB#12:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB22_8 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r13,4), %rdi
	movq	40(%rsp), %rdx          # 8-byte Reload
	subq	%r13, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movl	12(%rsp), %r15d         # 4-byte Reload
	jmp	.LBB22_14
	.p2align	4, 0x90
.LBB22_13:                              #   in Loop: Header=BB22_8 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$0, (%rax,%r13,4)
	jmp	.LBB22_14
.LBB22_11:                              #   in Loop: Header=BB22_8 Depth=1
	incl	%r15d
	.p2align	4, 0x90
.LBB22_14:                              # %.loopexit
                                        #   in Loop: Header=BB22_8 Depth=1
	incl	%r15d
	cmpl	%ebx, %r15d
	jl	.LBB22_8
.LBB22_15:                              # %._crit_edge
	cmpl	%ebx, %r15d
	jg	.LBB22_17
# BB#16:
	movslq	%r15d, %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	$1, (%rcx,%rax,4)
	incl	%ebp
.LBB22_17:
	movl	%ebp, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	read_significance_map, .Lfunc_end22-read_significance_map
	.cfi_endproc

	.globl	read_significant_coefficients
	.p2align	4, 0x90
	.type	read_significant_coefficients,@function
read_significant_coefficients:          # @read_significant_coefficients
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi180:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi181:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi183:
	.cfi_def_cfa_offset 96
.Lcfi184:
	.cfi_offset %rbx, -56
.Lcfi185:
	.cfi_offset %r12, -48
.Lcfi186:
	.cfi_offset %r13, -40
.Lcfi187:
	.cfi_offset %r14, -32
.Lcfi188:
	.cfi_offset %r15, -24
.Lcfi189:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movslq	%edx, %r12
	movl	maxpos(,%r12,4), %r13d
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$1, %ecx
	jmp	.LBB23_1
.LBB23_9:                               # %.critedge.i
                                        #   in Loop: Header=BB23_1 Depth=1
	testl	%eax, %eax
	je	.LBB23_10
# BB#11:                                # %.outer19.i.i.preheader
                                        #   in Loop: Header=BB23_1 Depth=1
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	jmp	.LBB23_12
	.p2align	4, 0x90
.LBB23_21:                              #   in Loop: Header=BB23_1 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	1(%rcx), %eax
	testl	%ecx, %ecx
	cmovel	%ecx, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r15, %rbx
	jmp	.LBB23_22
.LBB23_14:                              # %.thread.i.i
                                        #   in Loop: Header=BB23_12 Depth=2
	movl	$1, %eax
	movl	%ebp, %ecx
	shll	%cl, %eax
	addl	%eax, %r15d
	incl	%ebp
	.p2align	4, 0x90
.LBB23_12:                              #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	biari_decode_symbol_eq_prob
	testl	%eax, %eax
	je	.LBB23_15
# BB#13:                                #   in Loop: Header=BB23_12 Depth=2
	cmpl	$1, %eax
	jne	.LBB23_12
	jmp	.LBB23_14
.LBB23_15:                              # %.outer.i.i.loopexit
                                        #   in Loop: Header=BB23_1 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	testl	%ebp, %ebp
	jne	.LBB23_17
	jmp	.LBB23_19
.LBB23_18:                              #   in Loop: Header=BB23_17 Depth=2
	movl	$1, %eax
	movl	%ebp, %ecx
	shll	%cl, %eax
	orl	%eax, (%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB23_16:                              #   in Loop: Header=BB23_17 Depth=2
	testl	%ebp, %ebp
	je	.LBB23_19
.LBB23_17:                              #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%ebp
	movq	%rbx, %rdi
	callq	biari_decode_symbol_eq_prob
	cmpl	$1, %eax
	jne	.LBB23_16
	jmp	.LBB23_18
.LBB23_19:                              # %exp_golomb_decode_eq_prob.exit.i
                                        #   in Loop: Header=BB23_1 Depth=1
	addl	(%rsp), %r15d           # 4-byte Folded Reload
	subl	%r12d, %r15d
	incl	%r15d
	movl	%r15d, %r12d
	jmp	.LBB23_20
.LBB23_10:                              #   in Loop: Header=BB23_1 Depth=1
	negl	%r12d
.LBB23_20:                              # %unary_exp_golomb_level_decode.exit
                                        #   in Loop: Header=BB23_1 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	addl	%r12d, -4(%rbp,%r14,4)
	incl	4(%rsp)                 # 4-byte Folded Spill
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB23_22:                              #   in Loop: Header=BB23_1 Depth=1
	movq	%rbx, %rdi
	callq	biari_decode_symbol_eq_prob
	movq	%rbx, %rdi
	testl	%eax, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	je	.LBB23_1
# BB#23:                                #   in Loop: Header=BB23_1 Depth=1
	negl	-4(%rbp,%r14,4)
.LBB23_1:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_2 Depth 2
                                        #     Child Loop BB23_7 Depth 2
                                        #     Child Loop BB23_12 Depth 2
                                        #     Child Loop BB23_17 Depth 2
	movslq	%r13d, %r13
	.p2align	4, 0x90
.LBB23_2:                               #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %r14
	testq	%r14, %r14
	jle	.LBB23_24
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=2
	leaq	-1(%r14), %r13
	cmpl	$0, -4(%rbp,%r14,4)
	je	.LBB23_2
# BB#4:                                 #   in Loop: Header=BB23_1 Depth=1
	cmpl	$5, %ecx
	movl	$4, %eax
	cmovll	%ecx, %eax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	5592(%rsi), %rcx
	movq	56(%rcx), %rcx
	movslq	type2ctx_abs(,%r12,4), %rdx
	cltq
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rcx,%rdx,4), %rcx
	movq	%rsi, %rbx
	leaq	1432(%rcx,%rax,4), %rsi
	movq	%rdi, %r15
	callq	biari_decode_symbol
	addl	-4(%rbp,%r14,4), %eax
	movl	%eax, -4(%rbp,%r14,4)
	cmpl	$2, %eax
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jne	.LBB23_21
# BB#5:                                 #   in Loop: Header=BB23_1 Depth=1
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	max_c2(,%r12,4), %eax
	movl	4(%rsp), %ecx           # 4-byte Reload
	cmpl	%eax, %ecx
	cmovlel	%ecx, %eax
	movq	5592(%rbx), %rcx
	movq	56(%rcx), %rcx
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movslq	type2ctx_abs(,%r12,4), %rdx
	cltq
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rcx,%rdx,4), %rcx
	leaq	1632(%rcx,%rax,4), %rbp
	movq	%r15, %rbx
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	biari_decode_symbol
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	movl	$0, %r12d
	je	.LBB23_20
# BB#6:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB23_1 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB23_7:                               # %.preheader.i
                                        #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r12d, %r15d
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	biari_decode_symbol
	testl	%eax, %eax
	sete	%cl
	leal	-1(%r15), %r12d
	cmpl	$-11, %r15d
	je	.LBB23_9
# BB#8:                                 # %.preheader.i
                                        #   in Loop: Header=BB23_7 Depth=2
	testb	%cl, %cl
	je	.LBB23_7
	jmp	.LBB23_9
.LBB23_24:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	read_significant_coefficients, .Lfunc_end23-read_significant_coefficients
	.cfi_endproc

	.globl	unary_exp_golomb_level_decode
	.p2align	4, 0x90
	.type	unary_exp_golomb_level_decode,@function
unary_exp_golomb_level_decode:          # @unary_exp_golomb_level_decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi190:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi191:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi193:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi194:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi196:
	.cfi_def_cfa_offset 64
.Lcfi197:
	.cfi_offset %rbx, -56
.Lcfi198:
	.cfi_offset %r12, -48
.Lcfi199:
	.cfi_offset %r13, -40
.Lcfi200:
	.cfi_offset %r14, -32
.Lcfi201:
	.cfi_offset %r15, -24
.Lcfi202:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	callq	biari_decode_symbol
	xorl	%ebp, %ebp
	testl	%eax, %eax
	je	.LBB24_13
	.p2align	4, 0x90
.LBB24_1:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ebx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	biari_decode_symbol
	testl	%eax, %eax
	sete	%cl
	leal	-1(%rbx), %ebp
	cmpl	$-11, %ebx
	je	.LBB24_3
# BB#2:                                 # %.preheader
                                        #   in Loop: Header=BB24_1 Depth=1
	testb	%cl, %cl
	je	.LBB24_1
.LBB24_3:                               # %.critedge
	testl	%eax, %eax
	je	.LBB24_4
# BB#5:                                 # %.outer19.i.preheader
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.LBB24_6
.LBB24_14:                              # %.thread.i
                                        #   in Loop: Header=BB24_6 Depth=1
	movl	$1, %eax
	movl	%r14d, %ecx
	shll	%cl, %eax
	addl	%eax, %r15d
	incl	%r14d
	.p2align	4, 0x90
.LBB24_6:                               # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	callq	biari_decode_symbol_eq_prob
	cmpl	$1, %eax
	je	.LBB24_14
# BB#7:                                 #   in Loop: Header=BB24_6 Depth=1
	testl	%eax, %eax
	jne	.LBB24_6
# BB#8:                                 # %.outer.i.preheader
	xorl	%r12d, %r12d
	testl	%r14d, %r14d
	jne	.LBB24_10
	jmp	.LBB24_12
.LBB24_11:                              #   in Loop: Header=BB24_10 Depth=1
	movl	$1, %eax
	movl	%r14d, %ecx
	shll	%cl, %eax
	orl	%eax, %r12d
	.p2align	4, 0x90
.LBB24_9:                               #   in Loop: Header=BB24_10 Depth=1
	testl	%r14d, %r14d
	je	.LBB24_12
.LBB24_10:                              # =>This Inner Loop Header: Depth=1
	decl	%r14d
	movq	%r13, %rdi
	callq	biari_decode_symbol_eq_prob
	cmpl	$1, %eax
	jne	.LBB24_9
	jmp	.LBB24_11
.LBB24_12:                              # %exp_golomb_decode_eq_prob.exit
	addl	%r12d, %r15d
	subl	%ebp, %r15d
	incl	%r15d
	movl	%r15d, %ebp
	jmp	.LBB24_13
.LBB24_4:
	negl	%ebp
.LBB24_13:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	unary_exp_golomb_level_decode, .Lfunc_end24-unary_exp_golomb_level_decode
	.cfi_endproc

	.globl	readRunLevel_CABAC
	.p2align	4, 0x90
	.type	readRunLevel_CABAC,@function
readRunLevel_CABAC:                     # @readRunLevel_CABAC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi203:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi204:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi205:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi206:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi207:
	.cfi_def_cfa_offset 48
.Lcfi208:
	.cfi_offset %rbx, -40
.Lcfi209:
	.cfi_offset %r12, -32
.Lcfi210:
	.cfi_offset %r14, -24
.Lcfi211:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	readRunLevel_CABAC.coeff_ctr(%rip), %eax
	testl	%eax, %eax
	jns	.LBB25_5
# BB#1:
	movl	4(%r15), %eax
	imulq	$408, %rax, %r12        # imm = 0x198
	addq	5600(%r15), %r12
	movl	24(%rbx), %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	read_and_store_CBP_block_bit
	movl	%eax, readRunLevel_CABAC.coeff_ctr(%rip)
	testl	%eax, %eax
	je	.LBB25_2
# BB#4:
	movl	24(%rbx), %ecx
	movl	$readRunLevel_CABAC.coeff, %r8d
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	read_significance_map
	movl	%eax, readRunLevel_CABAC.coeff_ctr(%rip)
	movl	24(%rbx), %edx
	movl	$readRunLevel_CABAC.coeff, %ecx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	read_significant_coefficients
	movl	readRunLevel_CABAC.coeff_ctr(%rip), %eax
.LBB25_5:
	testl	%eax, %eax
	movl	$0, 8(%rbx)
	je	.LBB25_3
# BB#6:                                 # %.preheader
	movslq	readRunLevel_CABAC.pos(%rip), %rcx
	movl	readRunLevel_CABAC.coeff(,%rcx,4), %edx
	incq	%rcx
	testl	%edx, %edx
	movl	%ecx, readRunLevel_CABAC.pos(%rip)
	je	.LBB25_8
# BB#7:                                 # %.thread24
	movl	%edx, 4(%rbx)
	decl	%eax
	movl	%eax, readRunLevel_CABAC.coeff_ctr(%rip)
	jmp	.LBB25_12
.LBB25_8:                               # %.lr.ph
	leaq	readRunLevel_CABAC.coeff(,%rcx,4), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB25_9:                               # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rdi
	leaq	1(%rdi), %rdx
	movl	%edx, 8(%rbx)
	movl	(%rsi,%rdi,4), %edi
	testl	%edi, %edi
	je	.LBB25_9
# BB#10:
	addl	%ecx, %edx
	movl	%edx, readRunLevel_CABAC.pos(%rip)
	movl	%edi, 4(%rbx)
	leal	-1(%rax), %ecx
	movl	%ecx, readRunLevel_CABAC.coeff_ctr(%rip)
	testl	%eax, %eax
	jne	.LBB25_12
	jmp	.LBB25_11
.LBB25_2:                               # %.thread
	movl	$0, 8(%rbx)
.LBB25_3:                               # %.thread23
	movl	$0, 4(%rbx)
	movl	$-1, readRunLevel_CABAC.coeff_ctr(%rip)
.LBB25_11:
	movl	$0, readRunLevel_CABAC.pos(%rip)
.LBB25_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end25:
	.size	readRunLevel_CABAC, .Lfunc_end25-readRunLevel_CABAC
	.cfi_endproc

	.globl	readSyntaxElement_CABAC
	.p2align	4, 0x90
	.type	readSyntaxElement_CABAC,@function
readSyntaxElement_CABAC:                # @readSyntaxElement_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi212:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi213:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi214:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi215:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi216:
	.cfi_def_cfa_offset 48
.Lcfi217:
	.cfi_offset %rbx, -40
.Lcfi218:
	.cfi_offset %r14, -32
.Lcfi219:
	.cfi_offset %r15, -24
.Lcfi220:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	addq	$8, %rbx
	movq	%rbx, %rdi
	callq	arideco_bits_read
	movl	%eax, %ebp
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	*40(%r15)
	movq	%rbx, %rdi
	callq	arideco_bits_read
	subl	%ebp, %eax
	movl	%eax, 12(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	readSyntaxElement_CABAC, .Lfunc_end26-readSyntaxElement_CABAC
	.cfi_endproc

	.globl	cabac_startcode_follows
	.p2align	4, 0x90
	.type	cabac_startcode_follows,@function
cabac_startcode_follows:                # @cabac_startcode_follows
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	je	.LBB27_1
# BB#2:
	pushq	%rax
.Lcfi221:
	.cfi_def_cfa_offset 16
	movq	5592(%rdi), %rax
	movq	40(%rax), %rcx
	movslq	28(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	movslq	assignSE2partition+8(%rax), %rax
	imulq	$56, %rax, %rax
	leaq	8(%rcx,%rax), %rdi
	callq	biari_decode_final
	cmpl	$1, %eax
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	retq
.LBB27_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end27:
	.size	cabac_startcode_follows, .Lfunc_end27-cabac_startcode_follows
	.cfi_endproc

	.globl	exp_golomb_decode_eq_prob
	.p2align	4, 0x90
	.type	exp_golomb_decode_eq_prob,@function
exp_golomb_decode_eq_prob:              # @exp_golomb_decode_eq_prob
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi222:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi223:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi224:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi225:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi226:
	.cfi_def_cfa_offset 48
.Lcfi227:
	.cfi_offset %rbx, -40
.Lcfi228:
	.cfi_offset %r14, -32
.Lcfi229:
	.cfi_offset %r15, -24
.Lcfi230:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	jmp	.LBB28_1
.LBB28_8:                               # %.thread
                                        #   in Loop: Header=BB28_1 Depth=1
	movl	$1, %eax
	movl	%ebx, %ecx
	shll	%cl, %eax
	addl	%eax, %r15d
	incl	%ebx
	.p2align	4, 0x90
.LBB28_1:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	biari_decode_symbol_eq_prob
	cmpl	$1, %eax
	je	.LBB28_8
# BB#2:                                 #   in Loop: Header=BB28_1 Depth=1
	testl	%eax, %eax
	jne	.LBB28_1
# BB#3:                                 # %.outer.preheader
	xorl	%ebp, %ebp
	testl	%ebx, %ebx
	jne	.LBB28_5
	jmp	.LBB28_7
.LBB28_6:                               #   in Loop: Header=BB28_5 Depth=1
	movl	$1, %eax
	movl	%ebx, %ecx
	shll	%cl, %eax
	orl	%eax, %ebp
	.p2align	4, 0x90
.LBB28_4:                               #   in Loop: Header=BB28_5 Depth=1
	testl	%ebx, %ebx
	je	.LBB28_7
.LBB28_5:                               # =>This Inner Loop Header: Depth=1
	decl	%ebx
	movq	%r14, %rdi
	callq	biari_decode_symbol_eq_prob
	cmpl	$1, %eax
	jne	.LBB28_4
	jmp	.LBB28_6
.LBB28_7:
	addl	%r15d, %ebp
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	exp_golomb_decode_eq_prob, .Lfunc_end28-exp_golomb_decode_eq_prob
	.cfi_endproc

	.globl	readIPCMBytes_CABAC
	.p2align	4, 0x90
	.type	readIPCMBytes_CABAC,@function
readIPCMBytes_CABAC:                    # @readIPCMBytes_CABAC
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movq	16(%rsi), %rcx
	cmpl	4(%rsi), %eax
	movl	$8, 12(%rdi)
	jge	.LBB29_1
# BB#2:
	movslq	%eax, %rdx
	incl	%eax
	movzbl	(%rcx,%rdx), %ecx
	movl	%ecx, 16(%rdi)
	jmp	.LBB29_3
.LBB29_1:                               # %._crit_edge
	movl	16(%rdi), %ecx
.LBB29_3:
	movl	%ecx, 4(%rdi)
	movl	%eax, (%rsi)
	retq
.Lfunc_end29:
	.size	readIPCMBytes_CABAC, .Lfunc_end29-readIPCMBytes_CABAC
	.cfi_endproc

	.type	symbolCount,@object     # @symbolCount
	.bss
	.globl	symbolCount
	.p2align	2
symbolCount:
	.long	0                       # 0x0
	.size	symbolCount, 4

	.type	last_dquant,@object     # @last_dquant
	.globl	last_dquant
	.p2align	2
last_dquant:
	.long	0                       # 0x0
	.size	last_dquant, 4

	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"create_contexts_MotionInfo: deco_ctx"
	.size	.L.str, 37

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"create_contexts_TextureInfo: deco_ctx"
	.size	.L.str.1, 38

	.type	type2ctx_bcbp,@object   # @type2ctx_bcbp
	.section	.rodata,"a",@progbits
	.p2align	4
type2ctx_bcbp:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	5                       # 0x5
	.size	type2ctx_bcbp, 40

	.type	maxpos,@object          # @maxpos
	.p2align	4
maxpos:
	.long	16                      # 0x10
	.long	15                      # 0xf
	.long	64                      # 0x40
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	16                      # 0x10
	.long	4                       # 0x4
	.long	15                      # 0xf
	.long	8                       # 0x8
	.long	16                      # 0x10
	.size	maxpos, 40

	.type	pos2ctx_map_int,@object # @pos2ctx_map_int
	.p2align	4
pos2ctx_map_int:
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map8x8i
	.quad	pos2ctx_map8x4i
	.quad	pos2ctx_map4x8i
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map2x4c
	.quad	pos2ctx_map4x4c
	.size	pos2ctx_map_int, 80

	.type	pos2ctx_map,@object     # @pos2ctx_map
	.p2align	4
pos2ctx_map:
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map8x8
	.quad	pos2ctx_map8x4
	.quad	pos2ctx_map8x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map2x4c
	.quad	pos2ctx_map4x4c
	.size	pos2ctx_map, 80

	.type	type2ctx_last,@object   # @type2ctx_last
	.p2align	4
type2ctx_last:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	6                       # 0x6
	.size	type2ctx_last, 40

	.type	pos2ctx_last,@object    # @pos2ctx_last
	.p2align	4
pos2ctx_last:
	.quad	pos2ctx_last4x4
	.quad	pos2ctx_last4x4
	.quad	pos2ctx_last8x8
	.quad	pos2ctx_last8x4
	.quad	pos2ctx_last8x4
	.quad	pos2ctx_last4x4
	.quad	pos2ctx_last4x4
	.quad	pos2ctx_last4x4
	.quad	pos2ctx_last2x4c
	.quad	pos2ctx_last4x4c
	.size	pos2ctx_last, 80

	.type	max_c2,@object          # @max_c2
	.p2align	4
max_c2:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.size	max_c2, 40

	.type	type2ctx_abs,@object    # @type2ctx_abs
	.p2align	4
type2ctx_abs:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	5                       # 0x5
	.size	type2ctx_abs, 40

	.type	readRunLevel_CABAC.coeff,@object # @readRunLevel_CABAC.coeff
	.local	readRunLevel_CABAC.coeff
	.comm	readRunLevel_CABAC.coeff,256,16
	.type	readRunLevel_CABAC.coeff_ctr,@object # @readRunLevel_CABAC.coeff_ctr
	.data
	.p2align	2
readRunLevel_CABAC.coeff_ctr:
	.long	4294967295              # 0xffffffff
	.size	readRunLevel_CABAC.coeff_ctr, 4

	.type	readRunLevel_CABAC.pos,@object # @readRunLevel_CABAC.pos
	.local	readRunLevel_CABAC.pos
	.comm	readRunLevel_CABAC.pos,4,4
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	pos2ctx_map4x4,@object  # @pos2ctx_map4x4
	.section	.rodata,"a",@progbits
	.p2align	4
pos2ctx_map4x4:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.size	pos2ctx_map4x4, 64

	.type	pos2ctx_map8x8i,@object # @pos2ctx_map8x8i
	.p2align	4
pos2ctx_map8x8i:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.size	pos2ctx_map8x8i, 256

	.type	pos2ctx_map8x4i,@object # @pos2ctx_map8x4i
	.p2align	4
pos2ctx_map8x4i:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.size	pos2ctx_map8x4i, 128

	.type	pos2ctx_map4x8i,@object # @pos2ctx_map4x8i
	.p2align	4
pos2ctx_map4x8i:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	2                       # 0x2
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.size	pos2ctx_map4x8i, 128

	.type	pos2ctx_map2x4c,@object # @pos2ctx_map2x4c
	.p2align	4
pos2ctx_map2x4c:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.size	pos2ctx_map2x4c, 64

	.type	pos2ctx_map4x4c,@object # @pos2ctx_map4x4c
	.p2align	4
pos2ctx_map4x4c:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.size	pos2ctx_map4x4c, 64

	.type	pos2ctx_map8x8,@object  # @pos2ctx_map8x8
	.p2align	4
pos2ctx_map8x8:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	11                      # 0xb
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	11                      # 0xb
	.long	6                       # 0x6
	.long	9                       # 0x9
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	12                      # 0xc
	.long	14                      # 0xe
	.size	pos2ctx_map8x8, 256

	.type	pos2ctx_map8x4,@object  # @pos2ctx_map8x4
	.p2align	4
pos2ctx_map8x4:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.size	pos2ctx_map8x4, 128

	.type	pos2ctx_last4x4,@object # @pos2ctx_last4x4
	.p2align	4
pos2ctx_last4x4:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	15                      # 0xf
	.size	pos2ctx_last4x4, 64

	.type	pos2ctx_last8x8,@object # @pos2ctx_last8x8
	.p2align	4
pos2ctx_last8x8:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.size	pos2ctx_last8x8, 256

	.type	pos2ctx_last8x4,@object # @pos2ctx_last8x4
	.p2align	4
pos2ctx_last8x4:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.size	pos2ctx_last8x4, 128

	.type	pos2ctx_last2x4c,@object # @pos2ctx_last2x4c
	.p2align	4
pos2ctx_last2x4c:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.size	pos2ctx_last2x4c, 64

	.type	pos2ctx_last4x4c,@object # @pos2ctx_last4x4c
	.p2align	4
pos2ctx_last4x4c:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.size	pos2ctx_last4x4c, 64


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
