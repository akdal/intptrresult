	.text
	.file	"position_values.bc"
	.globl	set_position_values
	.p2align	4, 0x90
	.type	set_position_values,@function
set_position_values:                    # @set_position_values
	.cfi_startproc
# BB#0:                                 # %.preheader118.preheader
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$g_first_move, %edi
	xorl	%esi, %esi
	movl	$8192, %edx             # imm = 0x2000
	callq	memset
	movl	g_board_size(%rip), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	3(%rax), %eax
	movslq	g_board_size+4(%rip), %rdx
	movl	%eax, 20(%rsp)          # 4-byte Spill
	cmpl	$6, %eax
	setl	%al
	movq	%rdx, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpq	$2, %rdx
	jl	.LBB0_10
# BB#1:                                 # %.preheader118.preheader
	testb	%al, %al
	jne	.LBB0_10
# BB#2:                                 # %.preheader117.preheader
	movl	20(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shrl	%eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	leal	1(%rdx), %eax
	cmpl	$2, %eax
	movl	$3, %esi
	cmovgl	%eax, %esi
	addl	$-2, %esi
	shrl	%esi
	movl	%esi, 28(%rsp)          # 4-byte Spill
	leaq	1(%rdx), %rsi
	cmpq	$2, %rsi
	movl	$3, %eax
	cmovleq	%rax, %rsi
	addq	$-2, %rsi
	shrq	%rsi
	movq	%rsi, 56(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	andl	$1, %esi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	decq	%rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	$127, %esi
	movl	$2, %r13d
	movl	$g_first_move+264, %r15d
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader117
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
	movl	$1, %eax
	subl	%r13d, %eax
	addl	32(%rsp), %eax          # 4-byte Folded Reload
	movslq	%eax, %r12
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_4
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	leal	-1(%rsi), %edi
	movq	%r13, %rax
	shlq	$7, %rax
	movl	%edi, g_first_move+4(%rax)
	movq	%r12, %rbp
	shlq	$7, %rbp
	movl	%edi, g_first_move+4(%rbp)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%edi, g_first_move(%rbp,%rcx,4)
	movl	%edi, g_first_move(%rax,%rcx,4)
	movl	$3, %eax
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_7
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %eax
	movl	%esi, %edi
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB0_9
.LBB0_7:                                # %.preheader117.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	24(%rsp), %rbp          # 4-byte Folded Reload
	shlq	$5, %rbp
	decl	%edi
	addq	%rax, %rbp
	leaq	g_first_move+8(,%rbp,4), %rbp
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, %r9d
	subl	%eax, %r9d
	leal	2(%rax), %r10d
	movl	%ecx, %r14d
	subl	%r10d, %r14d
	leaq	(%r15,%rax,4), %r10
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_8:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, -8(%r10,%r11,4)
	movl	%edi, -8(%rbp,%r11,4)
	movslq	%r9d, %r9
	movq	%r12, %rbx
	shlq	$7, %rbx
	movl	%edi, g_first_move(%rbx,%r9,4)
	movq	%r13, %rcx
	shlq	$7, %rcx
	movl	%edi, g_first_move(%rcx,%r9,4)
	leal	-1(%rdi), %r8d
	movl	%r8d, (%r10,%r11,4)
	movl	%r8d, (%rbp,%r11,4)
	movslq	%r14d, %r14
	movl	%r8d, g_first_move(%rbx,%r14,4)
	movl	%r8d, g_first_move(%rcx,%r14,4)
	leaq	4(%rax,%r11), %rcx
	addq	$4, %r11
	addl	$-2, %edi
	addl	$-4, %r9d
	addl	$-4, %r14d
	cmpq	%rdx, %rcx
	jle	.LBB0_8
.LBB0_9:                                # %._crit_edge158
                                        #   in Loop: Header=BB0_3 Depth=1
	decl	%esi
	subl	28(%rsp), %esi          # 4-byte Folded Reload
	addq	$2, %r13
	addl	$-2, 24(%rsp)           # 4-byte Folded Spill
	addq	$256, %r15              # imm = 0x100
	cmpq	64(%rsp), %r13          # 8-byte Folded Reload
	jl	.LBB0_3
.LBB0_10:                               # %._crit_edge162
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	3(%rax), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cmpl	$6, %eax
	setl	%al
	movq	32(%rsp), %r13          # 8-byte Reload
	cmpl	$2, %r13d
	jl	.LBB0_83
# BB#11:                                # %._crit_edge162
	testb	%al, %al
	jne	.LBB0_83
# BB#12:                                # %.preheader117.preheader.1
	movslq	%r13d, %rcx
	movl	16(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shrl	%eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	shrl	$31, %eax
	addl	%r13d, %eax
	sarl	%eax
	movslq	%eax, %rsi
	leal	1(%rsi), %eax
	cmpl	$2, %eax
	movl	$3, %edx
	cmovgl	%eax, %edx
	addl	$-2, %edx
	shrl	%edx
	movl	%edx, 28(%rsp)          # 4-byte Spill
	leaq	1(%rsi), %rdx
	cmpq	$2, %rdx
	movl	$3, %eax
	cmovleq	%rax, %rdx
	addq	$-2, %rdx
	shrq	%rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$1, %edx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	decq	%rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-1(%rax), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	$127, %r8d
	movl	$2, %r14d
	movl	$g_first_move+4352, %ebp
	.p2align	4, 0x90
.LBB0_13:                               # %.preheader117.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_81 Depth 2
	movl	$1, %ecx
	subl	%r14d, %ecx
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	movslq	%ecx, %rcx
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_14
# BB#78:                                #   in Loop: Header=BB0_13 Depth=1
	leal	-1(%r8), %ebx
	movq	%r14, %rdx
	shlq	$7, %rdx
	movl	%ebx, g_first_move+4100(%rdx)
	movq	%rcx, %rax
	shlq	$7, %rax
	movl	%ebx, g_first_move+4100(%rax)
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, g_first_move+4096(%rax,%rdi,4)
	movl	%ebx, g_first_move+4096(%rdx,%rdi,4)
	movl	$3, %r9d
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_80
	jmp	.LBB0_82
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_13 Depth=1
	movl	$1, %r9d
	movl	%r8d, %ebx
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB0_82
.LBB0_80:                               # %.preheader117.1.new
                                        #   in Loop: Header=BB0_13 Depth=1
	movslq	24(%rsp), %rax          # 4-byte Folded Reload
	shlq	$5, %rax
	leal	2(%r9), %edx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %r15d
	subl	%edx, %r15d
	addq	%r9, %rax
	leaq	g_first_move+4104(,%rax,4), %r10
	decl	%ebx
	movl	%edi, %edx
	subl	%r9d, %edx
	leaq	(%rbp,%r9,4), %r11
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_81:                               #   Parent Loop BB0_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, (%r11,%r12,4)
	movl	%ebx, -8(%r10,%r12,4)
	movslq	%edx, %rdx
	movq	%rcx, %rax
	shlq	$7, %rax
	movl	%ebx, g_first_move+4096(%rax,%rdx,4)
	movq	%r14, %rdi
	shlq	$7, %rdi
	movl	%ebx, g_first_move+4096(%rdi,%rdx,4)
	leal	-1(%rbx), %r13d
	movl	%r13d, 8(%r11,%r12,4)
	movl	%r13d, (%r10,%r12,4)
	movslq	%r15d, %r15
	movl	%r13d, g_first_move+4096(%rax,%r15,4)
	movl	%r13d, g_first_move+4096(%rdi,%r15,4)
	leaq	4(%r9,%r12), %rax
	addq	$4, %r12
	addl	$-4, %r15d
	addl	$-2, %ebx
	addl	$-4, %edx
	cmpq	%rsi, %rax
	jle	.LBB0_81
.LBB0_82:                               # %._crit_edge158.1
                                        #   in Loop: Header=BB0_13 Depth=1
	decl	%r8d
	subl	28(%rsp), %r8d          # 4-byte Folded Reload
	addq	$2, %r14
	addl	$-2, 24(%rsp)           # 4-byte Folded Spill
	addq	$256, %rbp              # imm = 0x100
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	jl	.LBB0_13
.LBB0_83:                               # %._crit_edge162.1
	cmpl	$6, 20(%rsp)            # 4-byte Folded Reload
	setl	%al
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jl	.LBB0_21
# BB#84:                                # %._crit_edge162.1
	testb	%al, %al
	jne	.LBB0_21
# BB#15:                                # %.preheader114.preheader
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r8d
	shrl	%r8d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rcx
	leal	-1(%r13), %r10d
	leal	-1(%rdx), %r9d
	movl	$90, %edi
	movl	$2, %r15d
	movl	$g_first_move+260, %r11d
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader114
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_17 Depth 2
	movslq	%r10d, %rax
	shlq	$7, %rax
	leaq	g_first_move+4(%rax), %r14
	movl	$1, %eax
	subl	%r15d, %eax
	addl	%r13d, %eax
	movslq	%eax, %r12
	movl	%r9d, %edx
	movq	%r11, %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_17:                               #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rbx)
	jne	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_17 Depth=2
	decl	%edi
	movl	%edi, (%rbx)
	movl	%edi, (%r14,%rax,4)
	movslq	%edx, %rbp
	movq	%r12, %rsi
	shlq	$7, %rsi
	movl	%edi, g_first_move(%rsi,%rbp,4)
	movq	%r15, %rsi
	shlq	$7, %rsi
	movl	%edi, g_first_move(%rsi,%rbp,4)
.LBB0_19:                               #   in Loop: Header=BB0_17 Depth=2
	incq	%rax
	addq	$4, %rbx
	decl	%edx
	cmpq	%rcx, %rax
	jl	.LBB0_17
# BB#20:                                # %._crit_edge149
                                        #   in Loop: Header=BB0_16 Depth=1
	addq	$2, %r15
	addq	$256, %r11              # imm = 0x100
	addl	$-2, %r10d
	cmpq	%r8, %r15
	jl	.LBB0_16
.LBB0_21:                               # %._crit_edge153
	cmpl	$6, 16(%rsp)            # 4-byte Folded Reload
	setl	%al
	cmpl	$2, %r13d
	jl	.LBB0_29
# BB#22:                                # %._crit_edge153
	testb	%al, %al
	jne	.LBB0_29
# BB#23:                                # %.preheader114.preheader.1
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r8d
	shrl	%r8d
	movl	%r13d, %eax
	shrl	$31, %eax
	addl	%r13d, %eax
	sarl	%eax
	movslq	%eax, %rcx
	leal	-1(%r13), %r9d
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-1(%rax), %r10d
	movl	$90, %edi
	movl	$2, %r15d
	movl	$g_first_move+4356, %r11d
	.p2align	4, 0x90
.LBB0_24:                               # %.preheader114.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_25 Depth 2
	movslq	%r10d, %rax
	shlq	$7, %rax
	leaq	g_first_move+4100(%rax), %r14
	movl	$1, %eax
	subl	%r15d, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	movslq	%eax, %r12
	movq	%r11, %rbx
	movl	%r9d, %esi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_25:                               #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rbx)
	jne	.LBB0_27
# BB#26:                                #   in Loop: Header=BB0_25 Depth=2
	decl	%edi
	movl	%edi, (%rbx)
	movl	%edi, (%r14,%rax,4)
	movslq	%esi, %rbp
	movq	%r12, %rdx
	shlq	$7, %rdx
	movl	%edi, g_first_move+4096(%rdx,%rbp,4)
	movq	%r15, %rdx
	shlq	$7, %rdx
	movl	%edi, g_first_move+4096(%rdx,%rbp,4)
.LBB0_27:                               #   in Loop: Header=BB0_25 Depth=2
	incq	%rax
	decl	%esi
	addq	$4, %rbx
	cmpq	%rcx, %rax
	jl	.LBB0_25
# BB#28:                                # %._crit_edge149.1
                                        #   in Loop: Header=BB0_24 Depth=1
	addq	$2, %r15
	addl	$-2, %r10d
	addq	$256, %r11              # imm = 0x100
	cmpq	%r8, %r15
	jl	.LBB0_24
.LBB0_29:                               # %._crit_edge153.1
	cmpl	$8, 20(%rsp)            # 4-byte Folded Reload
	setl	%al
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jl	.LBB0_37
# BB#30:                                # %._crit_edge153.1
	testb	%al, %al
	jne	.LBB0_37
# BB#31:                                # %.preheader111.preheader
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r8d
	shrl	%r8d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rcx
	leal	-2(%r13), %r10d
	leal	-1(%rdx), %r9d
	movl	$70, %edi
	movl	$3, %r15d
	movl	$g_first_move+388, %r11d
	.p2align	4, 0x90
.LBB0_32:                               # %.preheader111
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_33 Depth 2
	movslq	%r10d, %rax
	shlq	$7, %rax
	leaq	g_first_move(%rax), %r14
	movl	$1, %eax
	subl	%r15d, %eax
	addl	%r13d, %eax
	movslq	%eax, %r12
	movl	%r9d, %edx
	movq	%r11, %rbx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_33:                               #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rbx)
	jne	.LBB0_35
# BB#34:                                #   in Loop: Header=BB0_33 Depth=2
	decl	%edi
	movl	%edi, (%rbx)
	movl	%edi, (%r14,%rax,4)
	movslq	%edx, %rbp
	movq	%r12, %rsi
	shlq	$7, %rsi
	movl	%edi, g_first_move(%rsi,%rbp,4)
	movq	%r15, %rsi
	shlq	$7, %rsi
	movl	%edi, g_first_move(%rsi,%rbp,4)
.LBB0_35:                               #   in Loop: Header=BB0_33 Depth=2
	addq	$2, %rax
	addq	$8, %rbx
	addl	$-2, %edx
	cmpq	%rcx, %rax
	jle	.LBB0_33
# BB#36:                                # %._crit_edge140
                                        #   in Loop: Header=BB0_32 Depth=1
	incq	%r15
	subq	$-128, %r11
	decl	%r10d
	cmpq	%r8, %r15
	jl	.LBB0_32
.LBB0_37:                               # %._crit_edge144
	cmpl	$8, 16(%rsp)            # 4-byte Folded Reload
	setl	%al
	cmpl	$2, %r13d
	jl	.LBB0_45
# BB#38:                                # %._crit_edge144
	testb	%al, %al
	jne	.LBB0_45
# BB#39:                                # %.preheader111.preheader.1
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r8d
	shrl	%r8d
	movl	%r13d, %eax
	shrl	$31, %eax
	addl	%r13d, %eax
	sarl	%eax
	movslq	%eax, %rcx
	leal	-1(%r13), %r9d
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-2(%rax), %r10d
	movl	$70, %edi
	movl	$3, %r15d
	movl	$g_first_move+4484, %r11d
	.p2align	4, 0x90
.LBB0_40:                               # %.preheader111.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_41 Depth 2
	movslq	%r10d, %rax
	shlq	$7, %rax
	leaq	g_first_move+4100(%rax), %r14
	movl	$1, %eax
	subl	%r15d, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	movslq	%eax, %r12
	movq	%r11, %rbx
	movl	%r9d, %esi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_41:                               #   Parent Loop BB0_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rbx)
	jne	.LBB0_43
# BB#42:                                #   in Loop: Header=BB0_41 Depth=2
	decl	%edi
	movl	%edi, (%rbx)
	movl	%edi, (%r14,%rax,4)
	movslq	%esi, %rbp
	movq	%r12, %rdx
	shlq	$7, %rdx
	movl	%edi, g_first_move+4096(%rdx,%rbp,4)
	movq	%r15, %rdx
	shlq	$7, %rdx
	movl	%edi, g_first_move+4096(%rdx,%rbp,4)
.LBB0_43:                               #   in Loop: Header=BB0_41 Depth=2
	leaq	2(%rax), %rdx
	addq	$3, %rax
	addl	$-2, %esi
	addq	$8, %rbx
	cmpq	%rcx, %rax
	movq	%rdx, %rax
	jle	.LBB0_41
# BB#44:                                # %._crit_edge140.1
                                        #   in Loop: Header=BB0_40 Depth=1
	incq	%r15
	decl	%r10d
	subq	$-128, %r11
	cmpq	%r8, %r15
	jl	.LBB0_40
.LBB0_45:                               # %._crit_edge144.1
	cmpl	$8, 20(%rsp)            # 4-byte Folded Reload
	setl	%al
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jl	.LBB0_53
# BB#46:                                # %._crit_edge144.1
	testb	%al, %al
	jne	.LBB0_53
# BB#47:                                # %.preheader108.preheader
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r8d
	shrl	%r8d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rcx
	leal	-2(%r13), %r10d
	leal	-1(%rdx), %r9d
	movl	$50, %edi
	movl	$3, %r15d
	movl	$g_first_move+388, %r11d
	.p2align	4, 0x90
.LBB0_48:                               # %.preheader108
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_49 Depth 2
	movslq	%r10d, %rax
	shlq	$7, %rax
	leaq	g_first_move+4(%rax), %r14
	movl	$1, %eax
	subl	%r15d, %eax
	addl	%r13d, %eax
	movslq	%eax, %r12
	movl	%r9d, %edx
	movq	%r11, %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_49:                               #   Parent Loop BB0_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rbx)
	jne	.LBB0_51
# BB#50:                                #   in Loop: Header=BB0_49 Depth=2
	decl	%edi
	movl	%edi, (%rbx)
	movl	%edi, (%r14,%rax,4)
	movslq	%edx, %rbp
	movq	%r12, %rsi
	shlq	$7, %rsi
	movl	%edi, g_first_move(%rsi,%rbp,4)
	movq	%r15, %rsi
	shlq	$7, %rsi
	movl	%edi, g_first_move(%rsi,%rbp,4)
.LBB0_51:                               #   in Loop: Header=BB0_49 Depth=2
	incq	%rax
	addq	$4, %rbx
	decl	%edx
	cmpq	%rcx, %rax
	jl	.LBB0_49
# BB#52:                                # %._crit_edge131
                                        #   in Loop: Header=BB0_48 Depth=1
	incq	%r15
	subq	$-128, %r11
	decl	%r10d
	cmpq	%r8, %r15
	jl	.LBB0_48
.LBB0_53:                               # %._crit_edge135
	cmpl	$8, 16(%rsp)            # 4-byte Folded Reload
	setl	%al
	cmpl	$2, %r13d
	jl	.LBB0_61
# BB#54:                                # %._crit_edge135
	testb	%al, %al
	jne	.LBB0_61
# BB#55:                                # %.preheader108.preheader.1
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r8d
	shrl	%r8d
	movl	%r13d, %eax
	shrl	$31, %eax
	addl	%r13d, %eax
	sarl	%eax
	movslq	%eax, %rcx
	leal	-1(%r13), %r9d
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-2(%rax), %r10d
	movl	$50, %edi
	movl	$3, %r15d
	movl	$g_first_move+4484, %r11d
	.p2align	4, 0x90
.LBB0_56:                               # %.preheader108.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_57 Depth 2
	movslq	%r10d, %rax
	shlq	$7, %rax
	leaq	g_first_move+4100(%rax), %r14
	movl	$1, %eax
	subl	%r15d, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	movslq	%eax, %r12
	movq	%r11, %rbx
	movl	%r9d, %esi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_57:                               #   Parent Loop BB0_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rbx)
	jne	.LBB0_59
# BB#58:                                #   in Loop: Header=BB0_57 Depth=2
	decl	%edi
	movl	%edi, (%rbx)
	movl	%edi, (%r14,%rax,4)
	movslq	%esi, %rbp
	movq	%r12, %rdx
	shlq	$7, %rdx
	movl	%edi, g_first_move+4096(%rdx,%rbp,4)
	movq	%r15, %rdx
	shlq	$7, %rdx
	movl	%edi, g_first_move+4096(%rdx,%rbp,4)
.LBB0_59:                               #   in Loop: Header=BB0_57 Depth=2
	incq	%rax
	decl	%esi
	addq	$4, %rbx
	cmpq	%rcx, %rax
	jl	.LBB0_57
# BB#60:                                # %._crit_edge131.1
                                        #   in Loop: Header=BB0_56 Depth=1
	incq	%r15
	decl	%r10d
	subq	$-128, %r11
	cmpq	%r8, %r15
	jl	.LBB0_56
.LBB0_61:                               # %._crit_edge135.1
	testl	%r13d, %r13d
	jle	.LBB0_69
# BB#62:                                # %._crit_edge135.1
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jl	.LBB0_69
# BB#63:                                # %.preheader.preheader
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movslq	%eax, %r8
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rcx
	leal	-1(%rdx), %r9d
	movl	$30, %esi
	movl	$1, %r15d
	movl	$g_first_move+132, %r10d
	movl	%r13d, %r11d
	.p2align	4, 0x90
.LBB0_64:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_65 Depth 2
	movslq	%r11d, %rax
	shlq	$7, %rax
	leaq	g_first_move+4(%rax), %r14
	movl	$1, %eax
	subl	%r15d, %eax
	addl	%r13d, %eax
	movslq	%eax, %r12
	movl	%r9d, %ebp
	movq	%r10, %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_65:                               #   Parent Loop BB0_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rbx)
	jne	.LBB0_67
# BB#66:                                #   in Loop: Header=BB0_65 Depth=2
	decl	%esi
	movl	%esi, (%rbx)
	movl	%esi, (%r14,%rax,4)
	movslq	%ebp, %rdi
	movq	%r12, %rdx
	shlq	$7, %rdx
	movl	%esi, g_first_move(%rdx,%rdi,4)
	movq	%r15, %rdx
	shlq	$7, %rdx
	movl	%esi, g_first_move(%rdx,%rdi,4)
.LBB0_67:                               #   in Loop: Header=BB0_65 Depth=2
	incq	%rax
	addq	$4, %rbx
	decl	%ebp
	cmpq	%rcx, %rax
	jl	.LBB0_65
# BB#68:                                # %._crit_edge
                                        #   in Loop: Header=BB0_64 Depth=1
	incq	%r15
	subq	$-128, %r10
	decl	%r11d
	cmpq	%r8, %r15
	jl	.LBB0_64
.LBB0_69:                               # %._crit_edge126
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB0_77
# BB#70:                                # %._crit_edge126
	cmpl	$2, %r13d
	jl	.LBB0_77
# BB#71:                                # %.preheader.preheader.1
	movl	16(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movslq	%eax, %r8
	movl	%r13d, %eax
	shrl	$31, %eax
	addl	%r13d, %eax
	sarl	%eax
	movslq	%eax, %rcx
	decl	%r13d
	movl	$30, %edx
	movl	$1, %r14d
	movl	$g_first_move+4228, %r9d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %r10d
	.p2align	4, 0x90
.LBB0_72:                               # %.preheader.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_73 Depth 2
	movslq	%r10d, %rax
	shlq	$7, %rax
	leaq	g_first_move+4100(%rax), %r11
	movl	$1, %eax
	subl	%r14d, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	movslq	%eax, %r15
	movq	%r9, %rdi
	movl	%r13d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_73:                               #   Parent Loop BB0_72 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rdi)
	jne	.LBB0_75
# BB#74:                                #   in Loop: Header=BB0_73 Depth=2
	decl	%edx
	movl	%edx, (%rdi)
	movl	%edx, (%r11,%rbx,4)
	movslq	%ebp, %rsi
	movq	%r15, %rax
	shlq	$7, %rax
	movl	%edx, g_first_move+4096(%rax,%rsi,4)
	movq	%r14, %rax
	shlq	$7, %rax
	movl	%edx, g_first_move+4096(%rax,%rsi,4)
.LBB0_75:                               #   in Loop: Header=BB0_73 Depth=2
	incq	%rbx
	decl	%ebp
	addq	$4, %rdi
	cmpq	%rcx, %rbx
	jl	.LBB0_73
# BB#76:                                # %._crit_edge.1
                                        #   in Loop: Header=BB0_72 Depth=1
	incq	%r14
	decl	%r10d
	subq	$-128, %r9
	cmpq	%r8, %r14
	jl	.LBB0_72
.LBB0_77:                               # %._crit_edge126.1
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	set_position_values, .Lfunc_end0-set_position_values
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
