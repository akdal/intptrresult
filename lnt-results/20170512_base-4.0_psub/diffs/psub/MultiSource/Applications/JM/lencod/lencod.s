	.text
	.file	"lencod.bc"
	.globl	init_stats
	.p2align	4, 0x90
	.type	init_stats,@function
init_stats:                             # @init_stats
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rax
	movl	2096(%rax), %eax
	movq	stats(%rip), %rcx
	movl	%eax, 700(%rcx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 720(%rcx)
	movq	$0, 736(%rcx)
	movq	snr(%rip), %rax
	movups	%xmm0, (%rax)
	movq	$0, 16(%rax)
	movups	%xmm0, 100(%rax)
	movups	%xmm0, 84(%rax)
	movq	$0, 116(%rax)
	retq
.Lfunc_end0:
	.size	init_stats, .Lfunc_end0-init_stats
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
.LCPI1_1:
	.quad	4602678801158248403     # double 0.49999900000000003
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 80
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	$0, giRDOpt_B8OnlyFlag(%rip)
	movl	$-1, p_in(%rip)
	movl	$-1, p_dec(%rip)
	movq	$0, p_trace(%rip)
	movq	$0, p_log(%rip)
	movq	$0, p_stat(%rip)
	movl	$1, frame_statistic_start(%rip)
	callq	Configure
	callq	Init_QMatrix
	callq	Init_QOffsetMatrix
	callq	AllocNalPayloadBuffer
	movq	input(%rip), %rcx
	movl	5088(%rcx), %edx
	movq	img(%rip), %rax
	movl	%edx, 15272(%rax)
	movl	$0, 15276(%rax)
	movl	$1, 15288(%rax)
	cmpl	$1, 2964(%rcx)
	jne	.LBB1_2
# BB#1:
	movl	$2, %esi
	xorl	%edx, %edx
	jmp	.LBB1_3
.LBB1_2:
	movl	2096(%rcx), %esi
	leal	(%rsi,%rsi), %edx
	negl	%edx
	leal	2(%rsi,%rsi), %esi
.LBB1_3:
	movl	%edx, 15280(%rax)
	movl	%esi, 15292(%rax)
	movl	$1, %edx
	cmpl	$0, 4704(%rcx)
	jne	.LBB1_5
# BB#4:
	xorl	%edx, %edx
	cmpl	$0, 4708(%rcx)
	setne	%dl
.LBB1_5:                                # %init_poc.exit
	movl	%edx, 15284(%rax)
	movl	%edx, 15356(%rax)
	movl	%edx, 15300(%rax)
	callq	GenerateParameterSets
	callq	SetLevelIndices
	callq	init_img
	movl	$1, %edi
	movl	$824, %esi              # imm = 0x338
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_7
# BB#6:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB1_7:                                # %malloc_picture.exit
	movq	%rbx, frame_pic_1(%rip)
	movq	input(%rip), %rax
	cmpl	$0, 2940(%rax)
	je	.LBB1_13
# BB#8:
	movl	$1, %edi
	movl	$824, %esi              # imm = 0x338
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_10
# BB#9:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB1_10:                               # %malloc_picture.exit40
	movq	%rbx, frame_pic_2(%rip)
	movl	$1, %edi
	movl	$824, %esi              # imm = 0x338
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_12
# BB#11:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB1_12:                               # %malloc_picture.exit41
	movq	%rbx, frame_pic_3(%rip)
	movq	input(%rip), %rax
.LBB1_13:
	cmpl	$0, 2148(%rax)
	je	.LBB1_17
# BB#14:
	movl	$0, si_frame_indicator(%rip)
	movl	$0, number_sp2_frames(%rip)
	movl	$1, %edi
	movl	$824, %esi              # imm = 0x338
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_16
# BB#15:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB1_16:                               # %malloc_picture.exit42
	movq	%rbx, frame_pic_si(%rip)
	movq	img(%rip), %rax
	movl	52(%rax), %edx
	movl	68(%rax), %esi
	movl	$lrec, %edi
	callq	get_mem2Dint
	movq	img(%rip), %rax
	movl	52(%rax), %ecx
	movl	68(%rax), %edx
	movl	$lrec_uv, %edi
	movl	$2, %esi
	callq	get_mem3Dint
	movq	input(%rip), %rax
.LBB1_17:
	cmpl	$0, 4704(%rax)
	je	.LBB1_23
# BB#18:
	movl	$1, %edi
	movl	$824, %esi              # imm = 0x338
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_20
# BB#19:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB1_20:                               # %malloc_picture.exit43
	movq	%rbx, top_pic(%rip)
	movl	$1, %edi
	movl	$824, %esi              # imm = 0x338
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_22
# BB#21:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB1_22:                               # %malloc_picture.exit44
	movq	%rbx, bottom_pic(%rip)
.LBB1_23:
	callq	init_rdopt
	movq	input(%rip), %rbx
	movslq	2096(%rbx), %rax
	incq	%rax
	movslq	8(%rbx), %rcx
	imulq	%rax, %rcx
	leaq	8(,%rcx,8), %rdi
	callq	malloc
	movq	%rax, Bit_Buffer(%rip)
	cmpl	$0, 2968(%rbx)
	je	.LBB1_28
# BB#24:
	callq	init_gop_structure
	movq	input(%rip), %rax
	cmpl	$0, 2096(%rax)
	je	.LBB1_27
# BB#25:
	cmpl	$3, 2968(%rax)
	jne	.LBB1_27
# BB#26:
	callq	interpret_gop_structure
	jmp	.LBB1_28
.LBB1_27:
	callq	create_hierarchy
.LBB1_28:
	movl	$0, dpb+48(%rip)
	callq	init_dpb
	callq	init_out_buffer
	movq	input(%rip), %rax
	movl	2096(%rax), %eax
	movq	stats(%rip), %rcx
	movl	%eax, 700(%rcx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 720(%rcx)
	movq	$0, 736(%rcx)
	movq	snr(%rip), %rax
	movups	%xmm0, (%rax)
	movq	$0, 16(%rax)
	movups	%xmm0, 100(%rax)
	movups	%xmm0, 84(%rax)
	movq	$0, 116(%rax)
	movq	$0, enc_bottom_picture(%rip)
	movq	$0, enc_top_picture(%rip)
	movq	$0, enc_frame_picture(%rip)
	movq	$0, enc_picture(%rip)
	callq	init_global_buffers
	callq	create_context_memory
	callq	Init_Motion_Search_Module
	movq	.Linformation_init.yuv_types+32(%rip), %rax
	movq	%rax, 32(%rsp)
	movaps	.Linformation_init.yuv_types+16(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movapd	.Linformation_init.yuv_types(%rip), %xmm0
	movapd	%xmm0, (%rsp)
	movq	input(%rip), %rax
	cmpl	$1, 5112(%rax)
	ja	.LBB1_30
# BB#29:
	movl	$.L.str.213, %edi
	jmp	.LBB1_31
.LBB1_30:
	movl	$.L.str.214, %edi
.LBB1_31:
	movl	$.L.str.15, %esi
	movl	$.L.str.16, %edx
	xorl	%eax, %eax
	callq	printf
	movq	input(%rip), %rbx
	addq	$280, %rbx              # imm = 0x118
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbx, %rsi
	movl	$.L.str.215, %edi
	xorl	%eax, %eax
	callq	printf
	movq	input(%rip), %rsi
	addq	$536, %rsi              # imm = 0x218
	movl	$.L.str.216, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$-1, p_dec(%rip)
	je	.LBB1_33
# BB#32:
	movq	input(%rip), %rsi
	addq	$792, %rsi              # imm = 0x318
	movl	$.L.str.217, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_33:
	movq	img(%rip), %rax
	movslq	15536(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rsp,%rax,2), %rsi
	movl	$.L.str.218, %edi
	xorl	%eax, %eax
	callq	printf
	movq	input(%rip), %rax
	movl	8(%rax), %esi
	leal	-1(%rsi), %edx
	imull	2096(%rax), %edx
	movl	$.L.str.219, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movq	input(%rip), %rax
	movl	4704(%rax), %esi
	movl	4708(%rax), %edx
	movl	$.L.str.220, %edi
	xorl	%eax, %eax
	callq	printf
	movq	input(%rip), %rax
	movl	5100(%rax), %esi
	movl	$.L.str.221, %edi
	xorl	%eax, %eax
	callq	printf
	movq	input(%rip), %rax
	movl	5112(%rax), %eax
	cmpl	$2, %eax
	je	.LBB1_36
# BB#34:
	movl	$.Lstr.6, %edi
	cmpl	$1, %eax
	jne	.LBB1_37
# BB#35:
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.6, %edi
	jmp	.LBB1_38
.LBB1_36:
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	jmp	.LBB1_38
.LBB1_37:
	callq	puts
	movl	$.Lstr.7, %edi
.LBB1_38:                               # %information_init.exit
	callq	puts
	movq	input(%rip), %rcx
	cmpl	$0, 5116(%rcx)
	je	.LBB1_40
# BB#39:
	movq	quadratic_RC(%rip), %rdi
	callq	rc_init_seq
	movq	input(%rip), %rcx
.LBB1_40:
	cmpl	$1, 5244(%rcx)
	jne	.LBB1_42
# BB#41:
	callq	UMHEX_DefineThreshold
	movq	input(%rip), %rcx
.LBB1_42:
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, frame_ctr(%rip)
	movl	$0, frame_ctr+16(%rip)
	movq	img(%rip), %rax
	movl	$0, 15596(%rax)
	movq	$0, tot_time(%rip)
	movl	4144(%rcx), %eax
	testl	%eax, %eax
	jle	.LBB1_44
# BB#43:
	movl	20(%rcx), %esi
	addl	%esi, %eax
	incl	%esi
	cltd
	idivl	%esi
	incl	%eax
	movl	%eax, 8(%rcx)
.LBB1_44:
	movl	2096(%rcx), %eax
	movl	%eax, initial_Bframes(%rip)
	callq	PatchInputNoFrames
	movq	stats(%rip), %rax
	movl	$0, 2256(%rax)
	callq	start_sequence
	movq	stats(%rip), %rcx
	movl	%eax, 32(%rcx)
	movl	2260(%rcx), %eax
	addl	%eax, 2256(%rcx)
	movl	$0, start_frame_no_in_this_IGOP(%rip)
	movq	input(%rip), %rbx
	cmpl	$0, 5772(%rbx)
	movq	img(%rip), %r10
	je	.LBB1_51
# BB#45:
	movl	15536(%r10), %ecx
	cmpl	$1, %ecx
	jne	.LBB1_47
# BB#46:
	movl	$10, img_pad_size_uv_x(%rip)
	movl	$10, img_pad_size_uv_y(%rip)
	movb	$7, chroma_mask_mv_y(%rip)
	movb	$7, chroma_mask_mv_x(%rip)
	movl	$3, chroma_shift_x(%rip)
	movl	$3, chroma_shift_y(%rip)
	movl	$1, %ecx
	movl	$2, %eax
	jmp	.LBB1_50
.LBB1_47:
	xorl	%eax, %eax
	cmpl	$2, %ecx
	sete	%sil
	movl	$10, %edx
	movl	$20, %ecx
	cmovel	%edx, %ecx
	movb	$7, %dl
	je	.LBB1_49
# BB#48:
	movb	$3, %dl
.LBB1_49:
	movb	%sil, %al
	leal	2(%rax), %esi
	movl	%ecx, img_pad_size_uv_x(%rip)
	movl	$20, img_pad_size_uv_y(%rip)
	movb	$3, chroma_mask_mv_y(%rip)
	movb	%dl, chroma_mask_mv_x(%rip)
	movl	$2, chroma_shift_y(%rip)
	movl	%esi, chroma_shift_x(%rip)
	incl	%eax
	xorl	%ecx, %ecx
.LBB1_50:                               # %chroma_mc_setup.exit
	movl	%ecx, shift_cr_y(%rip)
	movl	%eax, shift_cr_x(%rip)
.LBB1_51:                               # %._crit_edge62
	movl	$0, (%r10)
	cmpl	$0, 8(%rbx)
	jle	.LBB1_133
# BB#52:                                # %.lr.ph.preheader
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_53:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	1560(%rbx), %r9d
	testl	%r9d, %r9d
	je	.LBB1_59
# BB#54:                                #   in Loop: Header=BB1_53 Depth=1
	movl	%esi, %eax
	subl	start_frame_no_in_this_IGOP(%rip), %eax
	cltd
	idivl	%r9d
	movl	$1, %eax
	testl	%edx, %edx
	je	.LBB1_57
# BB#55:                                #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 5752(%rbx)
	je	.LBB1_57
# BB#56:                                #   in Loop: Header=BB1_53 Depth=1
	leal	1(%rsi), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	leal	1(%rsi,%rcx), %ecx
	andl	$-2, %ecx
	subl	%ecx, %eax
.LBB1_57:                               #   in Loop: Header=BB1_53 Depth=1
	movl	%eax, 15360(%r10)
	cmpl	$0, 1568(%rbx)
	je	.LBB1_63
# BB#58:                                #   in Loop: Header=BB1_53 Depth=1
	movl	start_frame_no_in_this_IGOP(%rip), %r8d
	movl	%esi, %eax
	subl	%r8d, %eax
	cltd
	idivl	%r9d
	jmp	.LBB1_64
	.p2align	4, 0x90
.LBB1_59:                               #   in Loop: Header=BB1_53 Depth=1
	movl	$1, %eax
	testl	%esi, %esi
	je	.LBB1_62
# BB#60:                                #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 5752(%rbx)
	je	.LBB1_62
# BB#61:                                #   in Loop: Header=BB1_53 Depth=1
	leal	1(%rsi), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	leal	1(%rsi,%rcx), %ecx
	andl	$-2, %ecx
	subl	%ecx, %eax
.LBB1_62:                               # %.thread
                                        #   in Loop: Header=BB1_53 Depth=1
	movl	%eax, 15360(%r10)
.LBB1_63:                               #   in Loop: Header=BB1_53 Depth=1
	movl	start_frame_no_in_this_IGOP(%rip), %r8d
	movl	%esi, %edx
	subl	%r8d, %edx
.LBB1_64:                               #   in Loop: Header=BB1_53 Depth=1
	movl	20(%rbx), %eax
	leal	2(%rax,%rax), %eax
	imull	%edx, %eax
	movl	%eax, 15316(%r10)
	cmpl	$0, 4704(%rbx)
	jne	.LBB1_66
# BB#65:                                #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 4708(%rbx)
	movl	%eax, %ecx
	je	.LBB1_67
.LBB1_66:                               #   in Loop: Header=BB1_53 Depth=1
	movl	%eax, %ecx
	orl	$1, %ecx
.LBB1_67:                               #   in Loop: Header=BB1_53 Depth=1
	movl	%ecx, 15320(%r10)
	cmpl	%ecx, %eax
	cmovlel	%eax, %ecx
	movl	%ecx, 15324(%r10)
	cmpl	$1, 2964(%rbx)
	je	.LBB1_69
# BB#68:                                #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 2968(%rbx)
	je	.LBB1_71
.LBB1_69:                               #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 2096(%rbx)
	je	.LBB1_71
# BB#70:                                #   in Loop: Header=BB1_53 Depth=1
	cmpl	$1, %esi
	jle	.LBB1_71
# BB#75:                                #   in Loop: Header=BB1_53 Depth=1
	testl	%r9d, %r9d
	je	.LBB1_79
# BB#76:                                #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 1568(%rbx)
	je	.LBB1_79
# BB#77:                                #   in Loop: Header=BB1_53 Depth=1
	movl	%esi, %eax
	cltd
	idivl	%r9d
	testl	%edx, %edx
	jne	.LBB1_79
# BB#78:                                #   in Loop: Header=BB1_53 Depth=1
	movl	$0, 15332(%r10)
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_79:                               #   in Loop: Header=BB1_53 Depth=1
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	decl	%eax
	andl	%eax, 15332(%r10)
	jmp	.LBB1_80
	.p2align	4, 0x90
.LBB1_71:                               #   in Loop: Header=BB1_53 Depth=1
	testl	%r9d, %r9d
	je	.LBB1_74
# BB#72:                                #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 1568(%rbx)
	je	.LBB1_74
# BB#73:                                #   in Loop: Header=BB1_53 Depth=1
	addl	%esi, %edi
	subl	%r8d, %edi
	movl	%edi, %eax
	cltd
	idivl	%r9d
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movl	%edx, %eax
	cltd
	idivl	%edi
	movl	%edx, %ecx
	movl	%esi, %eax
	subl	%r8d, %eax
	cltd
	idivl	%r9d
	testl	%edx, %edx
	cmovel	%edx, %ecx
	movl	%ecx, 15332(%r10)
	cmovel	%edx, %r14d
	jmp	.LBB1_80
	.p2align	4, 0x90
.LBB1_74:                               #   in Loop: Header=BB1_53 Depth=1
	addl	%esi, %edi
	subl	%r8d, %edi
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movl	%edi, %eax
	cltd
	idivl	%ebp
	movl	%edx, 15332(%r10)
.LBB1_80:                               #   in Loop: Header=BB1_53 Depth=1
	movl	$0, 15304(%r10)
	cmpl	$1, 2964(%rbx)
	jne	.LBB1_83
# BB#81:                                #   in Loop: Header=BB1_53 Depth=1
	testl	%esi, %esi
	je	.LBB1_83
# BB#82:                                #   in Loop: Header=BB1_53 Depth=1
	movl	2096(%rbx), %eax
	addl	%eax, %eax
	movl	%eax, 15304(%r10)
.LBB1_83:                               # %._crit_edge72
                                        #   in Loop: Header=BB1_53 Depth=1
	movl	1560(%rbx), %edi
	movl	%esi, %ecx
	subl	%r8d, %ecx
	testl	%edi, %edi
	movl	%ecx, %edx
	je	.LBB1_85
# BB#84:                                #   in Loop: Header=BB1_53 Depth=1
	movl	%ecx, %eax
	cltd
	idivl	%edi
.LBB1_85:                               #   in Loop: Header=BB1_53 Depth=1
	testl	%edx, %edx
	je	.LBB1_86
# BB#87:                                #   in Loop: Header=BB1_53 Depth=1
	movl	2136(%rbx), %edi
	testl	%edi, %edi
	je	.LBB1_90
# BB#88:                                #   in Loop: Header=BB1_53 Depth=1
	movl	%ecx, %eax
	cltd
	idivl	%edi
	testl	%edx, %edx
	je	.LBB1_89
.LBB1_90:                               #   in Loop: Header=BB1_53 Depth=1
	xorl	%eax, %eax
	cmpl	$2, 2964(%rbx)
	sete	%al
	jmp	.LBB1_91
	.p2align	4, 0x90
.LBB1_86:                               #   in Loop: Header=BB1_53 Depth=1
	movl	$2, %eax
	jmp	.LBB1_91
.LBB1_89:                               #   in Loop: Header=BB1_53 Depth=1
	movl	$3, %eax
	.p2align	4, 0x90
.LBB1_91:                               # %SetImgType.exit
                                        #   in Loop: Header=BB1_53 Depth=1
	movl	%eax, 20(%r10)
	cmpl	$2, %eax
	jne	.LBB1_95
# BB#92:                                # %SetImgType.exit
                                        #   in Loop: Header=BB1_53 Depth=1
	cmpl	$1, 2088(%rbx)
	jne	.LBB1_95
# BB#93:                                # %SetImgType.exit
                                        #   in Loop: Header=BB1_53 Depth=1
	testl	%esi, %esi
	je	.LBB1_95
# BB#94:                                #   in Loop: Header=BB1_53 Depth=1
	callq	rewrite_paramsets
	movq	stats(%rip), %rcx
	movl	%eax, 32(%rcx)
	movl	2260(%rcx), %eax
	addl	%eax, 2256(%rcx)
	movq	input(%rip), %rbx
.LBB1_95:                               #   in Loop: Header=BB1_53 Depth=1
	movl	2096(%rbx), %esi
	testl	%esi, %esi
	je	.LBB1_99
# BB#96:                                #   in Loop: Header=BB1_53 Depth=1
	movl	4144(%rbx), %eax
	testl	%eax, %eax
	je	.LBB1_99
# BB#97:                                #   in Loop: Header=BB1_53 Depth=1
	movq	img(%rip), %rcx
	movl	(%rcx), %edx
	leal	1(%rdx), %edi
	subl	start_frame_no_in_this_IGOP(%rip), %edi
	cmpl	8(%rbx), %edi
	jne	.LBB1_99
# BB#98:                                #   in Loop: Header=BB1_53 Depth=1
	movl	20(%rbx), %edi
	incl	%edi
	cvtsi2ssl	%edi, %xmm0
	cvtss2sd	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm1
	addsd	.LCPI1_0(%rip), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI1_1(%rip), %xmm0
	cvttsd2si	%xmm0, %esi
	decl	%edx
	imull	%edi, %edx
	subl	%edx, %eax
	cltd
	idivl	%esi
	decl	%eax
	movl	%eax, 2096(%rbx)
	subl	initial_Bframes(%rip), %eax
	addl	%eax, %eax
	movl	%eax, 15304(%rcx)
	movl	15316(%rcx), %edx
	addl	%eax, %edx
	movl	%edx, 15316(%rcx)
	addl	15320(%rcx), %eax
	movl	%eax, 15320(%rcx)
	cmpl	%eax, %edx
	cmovlel	%edx, %eax
	movl	%eax, 15324(%rcx)
	.p2align	4, 0x90
.LBB1_99:                               #   in Loop: Header=BB1_53 Depth=1
	movq	img(%rip), %rcx
	cmpl	$2, 20(%rcx)
	jne	.LBB1_113
# BB#100:                               #   in Loop: Header=BB1_53 Depth=1
	movl	5136(%rbx), %eax
	orl	$2, %eax
	cmpl	$3, %eax
	jne	.LBB1_102
# BB#101:                               #   in Loop: Header=BB1_53 Depth=1
	movl	(%rcx), %edx
	cmpl	start_frame_no_in_this_IGOP(%rip), %edx
	jne	.LBB1_113
.LBB1_102:                              #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 5116(%rbx)
	je	.LBB1_113
# BB#103:                               #   in Loop: Header=BB1_53 Depth=1
	movl	1560(%rbx), %edi
	testl	%edi, %edi
	je	.LBB1_111
# BB#104:                               #   in Loop: Header=BB1_53 Depth=1
	cmpl	$3, %eax
	jne	.LBB1_105
.LBB1_111:                              #   in Loop: Header=BB1_53 Depth=1
	movl	8(%rbx), %esi
	decl	%esi
	movl	2096(%rbx), %edx
	imull	%esi, %edx
	jmp	.LBB1_112
.LBB1_105:                              #   in Loop: Header=BB1_53 Depth=1
	movl	2096(%rbx), %esi
	leal	1(%rsi), %r9d
	movl	(%rcx), %r10d
	testl	%r10d, %r10d
	movl	$0, %ecx
	cmovel	%esi, %ecx
	movl	%r10d, %eax
	cltd
	idivl	%edi
	movl	%eax, %r8d
	movl	8(%rbx), %ebp
	movl	%ebp, %eax
	cltd
	idivl	%edi
	cmpl	%eax, %r8d
	jge	.LBB1_107
# BB#106:                               #   in Loop: Header=BB1_53 Depth=1
	imull	%r9d, %edi
	subl	%ecx, %edi
	jmp	.LBB1_110
.LBB1_107:                              #   in Loop: Header=BB1_53 Depth=1
	testl	%r10d, %r10d
	je	.LBB1_109
# BB#108:                               #   in Loop: Header=BB1_53 Depth=1
	subl	%r10d, %ebp
	leal	-1(%rbp), %eax
	imull	%esi, %eax
	addl	%esi, %ebp
	addl	%eax, %ebp
	movl	%ebp, %edi
	jmp	.LBB1_110
.LBB1_109:                              #   in Loop: Header=BB1_53 Depth=1
	leal	-1(%rbp), %edi
	imull	%esi, %edi
	addl	%ebp, %edi
.LBB1_110:                              #   in Loop: Header=BB1_53 Depth=1
	testl	%r10d, %r10d
	sete	%cl
	shll	%cl, %esi
	addl	%edi, %esi
	movl	%esi, %eax
	cltd
	idivl	%r9d
	movl	%eax, %esi
	movl	$1, %eax
	subl	%esi, %eax
	decl	%esi
	leal	-1(%rdi,%rax), %edx
.LBB1_112:                              #   in Loop: Header=BB1_53 Depth=1
	movq	quadratic_RC(%rip), %rdi
	callq	rc_init_GOP
	movq	img(%rip), %rcx
	movq	input(%rip), %rbx
.LBB1_113:                              #   in Loop: Header=BB1_53 Depth=1
	movl	(%rcx), %eax
	subl	start_frame_no_in_this_IGOP(%rip), %eax
	movl	4736(%rbx), %esi
	incl	%esi
	cltd
	idivl	%esi
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	movl	%eax, 15248(%rcx)
	cmpl	$0, 5084(%rbx)
	je	.LBB1_115
# BB#114:                               #   in Loop: Header=BB1_53 Depth=1
	callq	Init_redundant_frame
	callq	Set_redundant_frame
.LBB1_115:                              #   in Loop: Header=BB1_53 Depth=1
	callq	encode_one_frame
	movq	input(%rip), %rax
	cmpl	$0, 5084(%rax)
	je	.LBB1_120
# BB#116:                               #   in Loop: Header=BB1_53 Depth=1
	movl	key_frame(%rip), %eax
	testl	%eax, %eax
	je	.LBB1_120
# BB#117:                               #   in Loop: Header=BB1_53 Depth=1
	movl	$0, key_frame(%rip)
	movl	$1, redundant_coding(%rip)
	movq	img(%rip), %rax
	movl	$1, 15264(%rax)
	cmpl	$2, 20(%rax)
	jne	.LBB1_119
# BB#118:                               #   in Loop: Header=BB1_53 Depth=1
	movl	$0, 20(%rax)
.LBB1_119:                              # %encode_one_redundant_frame.exit
                                        #   in Loop: Header=BB1_53 Depth=1
	callq	encode_one_frame
.LBB1_120:                              #   in Loop: Header=BB1_53 Depth=1
	movq	img(%rip), %rax
	movq	input(%rip), %rcx
	cmpl	$2, 20(%rax)
	jne	.LBB1_123
# BB#121:                               #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 1564(%rcx)
	je	.LBB1_123
# BB#122:                               #   in Loop: Header=BB1_53 Depth=1
	movl	15328(%rax), %edx
	movl	%edx, 15596(%rax)
.LBB1_123:                              # %._crit_edge67
                                        #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 5104(%rcx)
	je	.LBB1_125
# BB#124:                               #   in Loop: Header=BB1_53 Depth=1
	callq	report_frame_statistic
	movq	img(%rip), %rax
.LBB1_125:                              #   in Loop: Header=BB1_53 Depth=1
	cmpl	$0, 15360(%rax)
	jne	.LBB1_127
# BB#126:                               #   in Loop: Header=BB1_53 Depth=1
	incl	%r14d
	movl	15332(%rax), %edx
	decl	%edx
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	decl	%esi
	andl	%edx, %esi
	movl	%esi, 15332(%rax)
.LBB1_127:                              #   in Loop: Header=BB1_53 Depth=1
	callq	encode_enhancement_layer
	movq	input(%rip), %rbx
	movl	4740(%rbx), %ecx
	movq	img(%rip), %r10
	testl	%ecx, %ecx
	je	.LBB1_128
# BB#129:                               #   in Loop: Header=BB1_53 Depth=1
	movl	8(%rbx), %eax
	cmpl	$0, In2ndIGOP(%rip)
	jne	.LBB1_132
# BB#130:                               #   in Loop: Header=BB1_53 Depth=1
	movl	(%r10), %edx
	leal	-1(%rax), %esi
	cmpl	%esi, %edx
	jne	.LBB1_132
# BB#131:                               #   in Loop: Header=BB1_53 Depth=1
	movl	$1, In2ndIGOP(%rip)
	movl	%eax, start_frame_no_in_this_IGOP(%rip)
	movl	20(%rbx), %esi
	incl	%esi
	imull	%edx, %esi
	incl	%esi
	movl	%esi, start_tr_in_this_IGOP(%rip)
	addl	%eax, %ecx
	movl	%ecx, 8(%rbx)
	movl	%ecx, %eax
	jmp	.LBB1_132
	.p2align	4, 0x90
.LBB1_128:                              # %.process_2nd_IGOP.exit_crit_edge
                                        #   in Loop: Header=BB1_53 Depth=1
	movl	8(%rbx), %eax
.LBB1_132:                              # %process_2nd_IGOP.exit
                                        #   in Loop: Header=BB1_53 Depth=1
	movl	(%r10), %esi
	incl	%esi
	movl	%esi, (%r10)
	movl	%r14d, %edi
	negl	%edi
	cmpl	%eax, %esi
	jl	.LBB1_53
.LBB1_133:                              # %._crit_edge
	callq	terminate_sequence
	callq	flush_dpb
	movl	p_in(%rip), %edi
	callq	close
	movl	p_dec(%rip), %edi
	cmpl	$-1, %edi
	je	.LBB1_135
# BB#134:
	callq	close
.LBB1_135:
	movq	p_trace(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_137
# BB#136:
	callq	fclose
.LBB1_137:
	callq	Clear_Motion_Search_Module
	callq	RandomIntraUninit
	callq	FmoUninit
	movq	input(%rip), %rax
	cmpl	$0, 2968(%rax)
	je	.LBB1_139
# BB#138:
	callq	clear_gop_structure
.LBB1_139:
	callq	clear_rdopt
	xorl	%eax, %eax
	callq	calc_buffer
	callq	report
	movq	Bit_Buffer(%rip), %rdi
	callq	free
	movq	frame_pic_1(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB1_141
# BB#140:
	movq	%rbx, %rdi
	callq	free_slice_list
	movq	%rbx, %rdi
	callq	free
.LBB1_141:                              # %free_picture.exit
	movq	input(%rip), %rax
	cmpl	$0, 2940(%rax)
	je	.LBB1_146
# BB#142:
	movq	frame_pic_2(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB1_144
# BB#143:
	movq	%rbx, %rdi
	callq	free_slice_list
	movq	%rbx, %rdi
	callq	free
.LBB1_144:                              # %free_picture.exit48
	movq	frame_pic_3(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB1_146
# BB#145:
	movq	%rbx, %rdi
	callq	free_slice_list
	movq	%rbx, %rdi
	callq	free
.LBB1_146:                              # %free_picture.exit49
	movq	input(%rip), %rax
	cmpl	$0, 2148(%rax)
	je	.LBB1_150
# BB#147:
	movq	frame_pic_si(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB1_149
# BB#148:
	movq	%rbx, %rdi
	callq	free_slice_list
	movq	%rbx, %rdi
	callq	free
.LBB1_149:                              # %free_picture.exit50
	movq	lrec(%rip), %rdi
	callq	free_mem2Dint
	movq	lrec_uv(%rip), %rdi
	movl	$2, %esi
	callq	free_mem3Dint
.LBB1_150:
	movq	top_pic(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB1_152
# BB#151:                               # %free_picture.exit51
	movq	%rbx, %rdi
	callq	free_slice_list
	movq	%rbx, %rdi
	callq	free
.LBB1_152:
	movq	bottom_pic(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB1_154
# BB#153:                               # %free_picture.exit52
	movq	%rbx, %rdi
	callq	free_slice_list
	movq	%rbx, %rdi
	callq	free
.LBB1_154:
	callq	free_dpb
	movq	Co_located(%rip), %rdi
	callq	free_colocated
	callq	uninit_out_buffer
	callq	free_global_buffers
	callq	free_img
	callq	free_context_memory
	callq	FreeNalPayloadBuffer
	callq	FreeParameterSets
	xorl	%eax, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.globl	init_poc
	.p2align	4, 0x90
	.type	init_poc,@function
init_poc:                               # @init_poc
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rcx
	movl	5088(%rcx), %edx
	movq	img(%rip), %rax
	movl	%edx, 15272(%rax)
	movl	$0, 15276(%rax)
	movl	$1, 15288(%rax)
	cmpl	$1, 2964(%rcx)
	jne	.LBB2_2
# BB#1:
	movl	$0, 15280(%rax)
	movl	$2, %edx
	jmp	.LBB2_3
.LBB2_2:
	movl	2096(%rcx), %edx
	leal	(%rdx,%rdx), %esi
	negl	%esi
	movl	%esi, 15280(%rax)
	leal	2(%rdx,%rdx), %edx
.LBB2_3:
	movl	%edx, 15292(%rax)
	cmpl	$0, 4704(%rcx)
	je	.LBB2_5
# BB#4:
	movl	$1, 15284(%rax)
	movl	$1, %edx
	jmp	.LBB2_6
.LBB2_5:
	xorl	%edx, %edx
	cmpl	$0, 4708(%rcx)
	setne	%dl
	movl	%edx, 15284(%rax)
	xorl	%edx, %edx
	cmpl	$0, 4708(%rcx)
	setne	%dl
.LBB2_6:
	movl	%edx, 15356(%rax)
	movl	%edx, 15300(%rax)
	retq
.Lfunc_end2:
	.size	init_poc, .Lfunc_end2-init_poc
	.cfi_endproc

	.globl	SetLevelIndices
	.p2align	4, 0x90
	.type	SetLevelIndices,@function
SetLevelIndices:                        # @SetLevelIndices
	.cfi_startproc
# BB#0:
	movq	active_sps(%rip), %rax
	movl	24(%rax), %ecx
	addl	$-9, %ecx
	cmpl	$42, %ecx
	ja	.LBB3_20
# BB#1:
	jmpq	*.LJTI3_0(,%rcx,8)
.LBB3_2:
	movq	img(%rip), %rax
	movl	$1, 8(%rax)
	retq
.LBB3_14:
	movq	img(%rip), %rax
	movl	$10, 8(%rax)
	retq
.LBB3_16:
	movq	img(%rip), %rax
	movl	$12, 8(%rax)
	retq
.LBB3_17:
	xorl	%ecx, %ecx
	cmpl	$88, 4(%rax)
	movq	img(%rip), %rax
	seta	%cl
	addl	$13, %ecx
	movl	%ecx, 8(%rax)
	retq
.LBB3_3:
	movq	img(%rip), %rax
	movl	$0, 8(%rax)
	retq
.LBB3_4:
	movl	$1, %ecx
	cmpl	$99, 4(%rax)
	ja	.LBB3_6
# BB#5:
	cmpl	$1, 20(%rax)
	movl	$1, %ecx
	adcl	$0, %ecx
.LBB3_6:
	movq	img(%rip), %rax
	movl	%ecx, 8(%rax)
	retq
.LBB3_7:
	movq	img(%rip), %rax
	movl	$3, 8(%rax)
	retq
.LBB3_8:
	movq	img(%rip), %rax
	movl	$4, 8(%rax)
	retq
.LBB3_20:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.207, %edi
	movl	$52, %esi
	movl	$1, %edx
	callq	fwrite
	movq	img(%rip), %rax
	movl	$16, 8(%rax)
	addq	$8, %rsp
	retq
.LBB3_9:
	movq	img(%rip), %rax
	movl	$5, 8(%rax)
	retq
.LBB3_10:
	movq	img(%rip), %rax
	movl	$6, 8(%rax)
	retq
.LBB3_11:
	movq	img(%rip), %rax
	movl	$7, 8(%rax)
	retq
.LBB3_12:
	movq	img(%rip), %rax
	movl	$8, 8(%rax)
	retq
.LBB3_13:
	movq	img(%rip), %rax
	movl	$9, 8(%rax)
	retq
.LBB3_15:
	movq	img(%rip), %rax
	movl	$11, 8(%rax)
	retq
.LBB3_18:
	movq	img(%rip), %rax
	movl	$15, 8(%rax)
	retq
.LBB3_19:
	movq	img(%rip), %rax
	movl	$16, 8(%rax)
	retq
.Lfunc_end3:
	.size	SetLevelIndices, .Lfunc_end3-SetLevelIndices
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_2
	.quad	.LBB3_3
	.quad	.LBB3_4
	.quad	.LBB3_7
	.quad	.LBB3_8
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_9
	.quad	.LBB3_10
	.quad	.LBB3_11
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_12
	.quad	.LBB3_13
	.quad	.LBB3_14
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_15
	.quad	.LBB3_16
	.quad	.LBB3_17
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_18
	.quad	.LBB3_19

	.text
	.globl	init_img
	.p2align	4, 0x90
	.type	init_img,@function
init_img:                               # @init_img
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -48
.Lcfi14:
	.cfi_offset %r12, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	input(%rip), %r8
	movl	64(%r8), %r10d
	movq	img(%rip), %rdi
	movl	%r10d, 15536(%rdi)
	movl	5256(%r8), %eax
	movl	%eax, 15444(%rdi)
	leal	(%rax,%rax,2), %ecx
	leal	-48(%rcx,%rcx), %ebx
	movl	%ebx, 15452(%rdi)
	leal	-16(%rax,%rax), %ecx
	movl	%ecx, 15460(%rdi)
	leal	-1(%rax), %ecx
	movl	$1, %ebp
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movl	%esi, 15512(%rdi)
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	movl	%esi, 15520(%rdi)
	movl	$16, 15556(%rdi)
	movl	$16, 15552(%rdi)
	testl	%r10d, %r10d
	je	.LBB4_2
# BB#1:
	movl	5260(%r8), %esi
	movl	%esi, 15448(%rdi)
	leal	-1(%rsi), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movl	%edx, 15516(%rdi)
	movl	$1, %edx
	movl	%esi, %ecx
	shll	%cl, %edx
	decl	%edx
	movl	%edx, 15524(%rdi)
	movl	%r10d, %ecx
	shll	%cl, %ebp
	andl	$-2, %ebp
	movl	%ebp, 15528(%rdi)
	addl	%ebp, %ebp
	movl	%ebp, 15532(%rdi)
	leal	-1(%r10), %ecx
	xorl	%edx, %edx
	cmpl	$1, %ecx
	seta	%dl
	leal	8(,%rdx,8), %ecx
	movl	%ecx, 15544(%rdi)
	movl	%ecx, 15568(%rdi)
	movl	%ecx, 15560(%rdi)
	movl	%r10d, %edx
	orl	$1, %edx
	xorl	%ebp, %ebp
	cmpl	$3, %edx
	sete	%bpl
	leal	(,%rbp,8), %r9d
	leal	8(,%rbp,8), %ebp
	movl	%ebp, 15548(%rdi)
	movl	%ebp, 15572(%rdi)
	movl	%ebp, 15564(%rdi)
	leal	(%rsi,%rsi,2), %ebp
	leal	-48(%rbp,%rbp), %ebp
	movl	%ebp, 15456(%rdi)
	movq	active_pps(%rip), %rbp
	movl	212(%rbp), %edx
	movl	%edx, 15576(%rdi)
	movl	216(%rbp), %ebp
	leal	16(%r9,%r9), %r9d
	jmp	.LBB4_3
.LBB4_2:
	movl	$0, 15448(%rdi)
	movl	$0, 15524(%rdi)
	movl	$0, 15528(%rdi)
	movl	$0, 15532(%rdi)
	movl	$0, 15544(%rdi)
	movl	$0, 15548(%rdi)
	movl	$0, 15456(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 15560(%rdi)
	movl	$0, 15576(%rdi)
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	xorl	%ebp, %ebp
.LBB4_3:
	testl	%r10d, %r10d
	movl	%ebp, 15580(%rdi)
	movl	%esi, %edx
	cmovel	%eax, %edx
	cmpl	%esi, %eax
	cmovgl	%eax, %edx
	xorl	%ebp, %ebp
	cmpl	$8, %edx
	setg	%bpl
	leal	8(,%rbp,8), %edx
	movl	%edx, 15440(%rdi)
	shll	$8, %eax
	imull	%esi, %ecx
	imull	%r9d, %ecx
	leal	128(%rcx,%rax), %eax
	movl	%eax, 15464(%rdi)
	sarl	%ebx
	leal	25(%rbx), %eax
	movl	%eax, 15468(%rdi)
	addl	$26, %ebx
	movl	%ebx, 15472(%rdi)
	movq	active_sps(%rip), %rax
	movl	1132(%rax), %edx
	movl	%edx, 28(%rdi)
	cmpl	$0, 1148(%rax)
	sete	%cl
	shll	%cl, %edx
	movl	%edx, 32(%rdi)
	movl	32(%r8), %eax
	movl	%eax, 15240(%rdi)
	movl	$0, 15420(%rdi)
	movsd	4080(%r8), %xmm0        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 48(%rdi)
	addq	$14376, %rdi            # imm = 0x3828
	callq	get_mem_mv
	movq	img(%rip), %rdi
	addq	$14384, %rdi            # imm = 0x3830
	callq	get_mem_mv
	movq	input(%rip), %rax
	cmpl	$0, 2120(%rax)
	je	.LBB4_5
# BB#4:
	movq	img(%rip), %rdi
	addq	$14392, %rdi            # imm = 0x3838
	callq	get_mem_mv
	movq	img(%rip), %rdi
	addq	$14400, %rdi            # imm = 0x3840
	callq	get_mem_mv
.LBB4_5:
	movq	img(%rip), %rdi
	addq	$14160, %rdi            # imm = 0x3750
	callq	get_mem_ACcoeff
	movq	img(%rip), %rdi
	addq	$14168, %rdi            # imm = 0x3758
	callq	get_mem_DCcoeff
	movq	input(%rip), %rax
	cmpl	$0, 5652(%rax)
	je	.LBB4_7
# BB#6:
	movq	img(%rip), %rdi
	addq	$14176, %rdi            # imm = 0x3760
	movl	$4, %esi
	movl	$16, %edx
	movl	$16, %ecx
	callq	get_mem3Dint
	movq	img(%rip), %rdi
	addq	$14184, %rdi            # imm = 0x3768
	movl	$3, %esi
	movl	$16, %edx
	movl	$16, %ecx
	callq	get_mem3Dint
	movq	img(%rip), %rax
	movl	15544(%rax), %r8d
	movl	15548(%rax), %ecx
	leaq	14192(%rax), %rdi
	movl	$4, %esi
	movl	$2, %edx
	callq	get_mem4Dint
	movq	img(%rip), %rax
	movl	15544(%rax), %r8d
	movl	15548(%rax), %ecx
	leaq	14200(%rax), %rdi
	movl	$1, %esi
	movl	$2, %edx
	callq	get_mem4Dint
	movq	input(%rip), %rax
.LBB4_7:
	cmpl	$0, 4708(%rax)
	je	.LBB4_10
# BB#8:
	movl	$rddata_top_frame_mb+1664, %edi
	callq	get_mem_mv
	movl	$rddata_top_frame_mb+1672, %edi
	callq	get_mem_mv
	movl	$rddata_bot_frame_mb+1664, %edi
	callq	get_mem_mv
	movl	$rddata_bot_frame_mb+1672, %edi
	callq	get_mem_mv
	movl	$rddata_top_frame_mb+1544, %edi
	callq	get_mem_ACcoeff
	movl	$rddata_top_frame_mb+1552, %edi
	callq	get_mem_DCcoeff
	movl	$rddata_bot_frame_mb+1544, %edi
	callq	get_mem_ACcoeff
	movl	$rddata_bot_frame_mb+1552, %edi
	callq	get_mem_DCcoeff
	movq	input(%rip), %rax
	cmpl	$3, 4708(%rax)
	je	.LBB4_10
# BB#9:
	movl	$rddata_top_field_mb+1664, %edi
	callq	get_mem_mv
	movl	$rddata_top_field_mb+1672, %edi
	callq	get_mem_mv
	movl	$rddata_bot_field_mb+1664, %edi
	callq	get_mem_mv
	movl	$rddata_bot_field_mb+1672, %edi
	callq	get_mem_mv
	movl	$rddata_top_field_mb+1544, %edi
	callq	get_mem_ACcoeff
	movl	$rddata_top_field_mb+1552, %edi
	callq	get_mem_DCcoeff
	movl	$rddata_bot_field_mb+1544, %edi
	callq	get_mem_ACcoeff
	movl	$rddata_bot_field_mb+1552, %edi
	callq	get_mem_DCcoeff
.LBB4_10:
	movq	img(%rip), %rbx
	movl	15520(%rbx), %eax
	movl	15524(%rbx), %ecx
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	leal	2(%rcx,%rcx), %ebp
	movslq	%ebp, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, 14232(%rbx)
	testq	%rax, %rax
	jne	.LBB4_12
# BB#11:
	movl	$.L.str, %edi
	callq	no_mem_exit
	movq	img(%rip), %rbx
	movq	14232(%rbx), %rax
.LBB4_12:
	movl	%ebp, %ecx
	shrl	$31, %ecx
	addl	%ebp, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, 14232(%rbx)
	cmpl	$2, %ebp
	jl	.LBB4_15
# BB#13:                                # %.lr.ph.preheader
	xorl	%edx, %edx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %edi
	imull	%edi, %edi
	movl	%edi, (%rsi)
	movl	%edi, (%rax,%rdx,4)
	incq	%rdx
	addq	$-4, %rsi
	cmpq	%rcx, %rdx
	jl	.LBB4_14
.LBB4_15:                               # %._crit_edge
	movq	input(%rip), %r15
	movl	56(%r15), %r8d
	movl	15584(%rbx), %eax
	leal	(%rax,%r8), %esi
	movl	%esi, 52(%rbx)
	movl	60(%r15), %r9d
	movl	15588(%rbx), %ecx
	leal	(%rcx,%r9), %r11d
	movl	%r11d, 68(%rbx)
	movl	%esi, %edi
	sarl	$31, %edi
	movl	%edi, %edx
	shrl	$30, %edx
	addl	%esi, %edx
	sarl	$2, %edx
	movl	%edx, 60(%rbx)
	movl	%r11d, %ebp
	sarl	$31, %ebp
	movl	%ebp, %edx
	shrl	$30, %edx
	addl	%r11d, %edx
	sarl	$2, %edx
	movl	%edx, 76(%rbx)
	leal	40(%rax,%r8), %eax
	movl	%eax, 56(%rbx)
	leal	40(%rcx,%r9), %eax
	movl	%eax, 72(%rbx)
	movslq	15536(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_17
# BB#16:
	movl	$16, %eax
	xorl	%edx, %edx
	idivl	init_img.mb_width_cr(,%rcx,4)
	movl	%eax, %r14d
	movl	%esi, %eax
	cltd
	idivl	%r14d
	movl	%eax, %r10d
	movl	$16, %eax
	xorl	%edx, %edx
	idivl	init_img.mb_height_cr(,%rcx,4)
	movl	%eax, %r12d
	movl	%r11d, %eax
	cltd
	idivl	%r12d
	movl	%eax, %ecx
	movl	%ecx, 80(%rbx)
	movl	%r8d, %eax
	cltd
	idivl	%r14d
	movl	%eax, 5268(%r15)
	movl	%r9d, %eax
	cltd
	idivl	%r12d
	jmp	.LBB4_18
.LBB4_17:
	movl	$0, 80(%rbx)
	movl	$0, 5268(%r15)
	xorl	%ecx, %ecx
	xorl	%r10d, %r10d
	xorl	%eax, %eax
.LBB4_18:
	movl	%r10d, 64(%rbx)
	movl	%eax, 5264(%r15)
	movl	%ecx, 84(%rbx)
	movl	%r11d, %eax
	imull	%esi, %eax
	movl	%eax, 88(%rbx)
	imull	%r10d, %ecx
	movl	%ecx, 92(%rbx)
	shrl	$28, %edi
	addl	%esi, %edi
	sarl	$4, %edi
	movl	%edi, 15336(%rbx)
	shrl	$28, %ebp
	addl	%r11d, %ebp
	sarl	$4, %ebp
	movl	%ebp, 15344(%rbx)
	imull	%ebp, %edi
	movl	%edi, 15352(%rbx)
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	sete	%cl
	shrl	%cl, %ebp
	movl	%ebp, 15340(%rbx)
	movl	$536, %esi              # imm = 0x218
	callq	calloc
	movq	%rax, 14224(%rbx)
	testq	%rax, %rax
	jne	.LBB4_20
# BB#19:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
	movq	input(%rip), %r15
.LBB4_20:
	cmpl	$0, 272(%r15)
	je	.LBB4_23
# BB#21:
	movq	img(%rip), %rbx
	movl	15352(%rbx), %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, 14240(%rbx)
	testq	%rax, %rax
	jne	.LBB4_23
# BB#22:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
	movq	input(%rip), %r15
.LBB4_23:
	cmpl	$1, 4172(%r15)
	jne	.LBB4_26
# BB#24:
	movq	img(%rip), %rax
	movl	15352(%rax), %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, mb16x16_cost_frame(%rip)
	testq	%rax, %rax
	jne	.LBB4_26
# BB#25:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
.LBB4_26:
	movq	img(%rip), %rdi
	movl	60(%rdi), %edx
	movl	76(%rdi), %esi
	subq	$-128, %rdi
	callq	get_mem2D
	movq	img(%rip), %rax
	movl	60(%rax), %edx
	movl	76(%rax), %esi
	leaq	136(%rax), %rdi
	callq	get_mem2D
	movq	img(%rip), %rax
	movq	128(%rax), %rcx
	movq	(%rcx), %rdi
	movslq	60(%rax), %rcx
	movslq	76(%rax), %rdx
	imulq	%rcx, %rdx
	movl	$255, %esi
	callq	memset
	movq	img(%rip), %rax
	movq	136(%rax), %rcx
	movq	(%rcx), %rdi
	movslq	60(%rax), %rcx
	movslq	76(%rax), %rdx
	imulq	%rcx, %rdx
	movl	$255, %esi
	callq	memset
	movq	img(%rip), %rax
	movl	60(%rax), %edx
	movl	76(%rax), %esi
	movl	$rddata_top_frame_mb+1600, %edi
	callq	get_mem2D
	movq	input(%rip), %rax
	cmpl	$0, 4708(%rax)
	je	.LBB4_28
# BB#27:
	movq	img(%rip), %rax
	movl	60(%rax), %edx
	movl	76(%rax), %esi
	movl	$rddata_bot_frame_mb+1600, %edi
	callq	get_mem2D
	movq	img(%rip), %rax
	movl	60(%rax), %edx
	movl	76(%rax), %esi
	movl	$rddata_top_field_mb+1600, %edi
	callq	get_mem2D
	movq	img(%rip), %rax
	movl	60(%rax), %edx
	movl	76(%rax), %esi
	movl	$rddata_bot_field_mb+1600, %edi
	callq	get_mem2D
.LBB4_28:
	movq	img(%rip), %rax
	movl	15352(%rax), %esi
	movl	15528(%rax), %ecx
	leaq	152(%rax), %rdi
	addl	$4, %ecx
	movl	$4, %edx
	callq	get_mem3Dint
	movq	img(%rip), %rdi
	movl	15452(%rdi), %ecx
	addq	$15480, %rdi            # imm = 0x3C78
	leal	52(%rcx), %edx
	movl	$10, %esi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	get_mem2Ddb_offset
	movq	img(%rip), %rdi
	movl	15452(%rdi), %r8d
	addq	$15488, %rdi            # imm = 0x3C80
	leal	52(%r8), %edx
	movl	$10, %esi
	movl	$3, %ecx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	get_mem3Ddb_offset
	movq	img(%rip), %rdi
	movl	15452(%rdi), %r8d
	addq	$15496, %rdi            # imm = 0x3C88
	leal	52(%r8), %edx
	movl	$10, %esi
	movl	$3, %ecx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	get_mem3Dint_offset
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB4_30
# BB#29:
	movq	img(%rip), %rdi
	movl	15452(%rdi), %ecx
	addq	$15504, %rdi            # imm = 0x3C90
	leal	52(%rcx), %edx
	movl	$10, %esi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	get_mem2Ddb_offset
.LBB4_30:
	callq	CAVLC_init
	movq	input(%rip), %rax
	xorl	%ecx, %ecx
	cmpl	$0, 2096(%rax)
	setne	%cl
	movq	img(%rip), %rdx
	movl	%ecx, 15612(%rdx)
	movl	$0, 112(%rdx)
	movl	15336(%rdx), %edi
	movl	15344(%rdx), %esi
	movl	4744(%rax), %edx
	callq	RandomIntraInit
	callq	InitSEIMessages
	movq	input(%rip), %rax
	cmpl	$0, 4748(%rax)
	je	.LBB4_32
# BB#31:
	shll	4756(%rax)
	shll	4760(%rax)
	jmp	.LBB4_33
.LBB4_32:
	movq	$0, 4752(%rax)
	movl	$0, 4760(%rax)
.LBB4_33:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	init_img, .Lfunc_end4-init_img
	.cfi_endproc

	.globl	malloc_picture
	.p2align	4, 0x90
	.type	malloc_picture,@function
malloc_picture:                         # @malloc_picture
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$824, %esi              # imm = 0x338
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB5_2
# BB#1:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB5_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	malloc_picture, .Lfunc_end5-malloc_picture
	.cfi_endproc

	.globl	init_global_buffers
	.p2align	4, 0x90
	.type	init_global_buffers,@function
init_global_buffers:                    # @init_global_buffers
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 48
.Lcfi25:
	.cfi_offset %rbx, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movslq	32(%rax), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, last_P_no_frm(%rip)
	testq	%rax, %rax
	jne	.LBB6_2
# BB#1:
	movl	$.L.str.202, %edi
	callq	no_mem_exit
.LBB6_2:
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	jne	.LBB6_5
# BB#3:
	movq	img(%rip), %rax
	movslq	32(%rax), %rdi
	shlq	$4, %rdi
	callq	malloc
	movq	%rax, last_P_no_fld(%rip)
	testq	%rax, %rax
	jne	.LBB6_5
# BB#4:
	movl	$.L.str.202, %edi
	callq	no_mem_exit
.LBB6_5:
	callq	init_orig_buffers
	movl	%eax, %ebp
	movq	img(%rip), %rax
	movl	15352(%rax), %esi
	incl	%esi
	movl	$PicPos, %edi
	movl	$2, %edx
	callq	get_mem2Dint
	movl	%eax, %r14d
	movq	img(%rip), %rsi
	cmpl	$0, 15352(%rsi)
	js	.LBB6_8
# BB#6:                                 # %.lr.ph45
	movq	PicPos(%rip), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_7:                                # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	15336(%rsi)
	movq	(%rdi,%rcx,8), %rbx
	movl	%edx, (%rbx)
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	15336(%rsi)
	movl	%eax, 4(%rbx)
	movslq	15352(%rsi), %rax
	cmpq	%rax, %rcx
	leaq	1(%rcx), %rcx
	jl	.LBB6_7
.LBB6_8:                                # %._crit_edge46
	addl	%ebp, %r14d
	movq	input(%rip), %rax
	cmpl	$0, 2928(%rax)
	jne	.LBB6_11
# BB#9:
	cmpl	$0, 2932(%rax)
	jne	.LBB6_11
# BB#10:
	cmpl	$0, 1576(%rax)
	je	.LBB6_12
.LBB6_11:
	movl	$wp_weight, %edi
	movl	$6, %esi
	movl	$32, %edx
	movl	$3, %ecx
	callq	get_mem3Dint
	movl	%eax, %ebp
	addl	%r14d, %ebp
	movl	$wp_offset, %edi
	movl	$6, %esi
	movl	$32, %edx
	movl	$3, %ecx
	callq	get_mem3Dint
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movl	$wbp_weight, %edi
	movl	$6, %esi
	movl	$32, %edx
	movl	$32, %ecx
	movl	$3, %r8d
	callq	get_mem4Dint
	movl	%eax, %r14d
	addl	%ebx, %r14d
	movq	input(%rip), %rax
.LBB6_12:
	cmpl	$0, 2096(%rax)
	jne	.LBB6_14
# BB#13:
	cmpl	$0, 2964(%rax)
	jle	.LBB6_15
.LBB6_14:
	movq	img(%rip), %rax
	movl	60(%rax), %ecx
	movl	76(%rax), %edx
	movl	$direct_ref_idx, %edi
	movl	$2, %esi
	callq	get_mem3D
	movl	%eax, %ebx
	addl	%r14d, %ebx
	movq	img(%rip), %rax
	movl	60(%rax), %edx
	movl	76(%rax), %esi
	movl	$direct_pdir, %edi
	callq	get_mem2D
	movl	%eax, %r14d
	addl	%ebx, %r14d
	movq	input(%rip), %rax
.LBB6_15:
	cmpl	$3, 4168(%rax)
	jne	.LBB6_22
# BB#16:
	movq	decs(%rip), %rdi
	movl	$16, %esi
	movl	$16, %edx
	callq	get_mem2Dint
	movl	%eax, %r15d
	movq	input(%rip), %rbx
	movslq	4728(%rbx), %rdi
	movl	$8, %esi
	callq	calloc
	movq	decs(%rip), %rdi
	movq	%rax, 16(%rdi)
	testq	%rax, %rax
	jne	.LBB6_18
# BB#17:
	movl	$.L.str.203, %edi
	callq	no_mem_exit
	movq	input(%rip), %rbx
	movq	decs(%rip), %rdi
.LBB6_18:                               # %.preheader
	addl	%r14d, %r15d
	cmpl	$0, 4728(%rbx)
	jle	.LBB6_21
# BB#19:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %rdi
	addq	%rbx, %rdi
	movq	img(%rip), %rax
	movl	32(%rax), %esi
	movl	52(%rax), %ecx
	incl	%esi
	movl	68(%rax), %edx
	callq	get_mem3Dpel
	addl	%eax, %r15d
	incq	%rbp
	movq	input(%rip), %rax
	movslq	4728(%rax), %rax
	movq	decs(%rip), %rdi
	addq	$8, %rbx
	cmpq	%rax, %rbp
	jl	.LBB6_20
.LBB6_21:                               # %._crit_edge
	addq	$32, %rdi
	movl	$4, %esi
	movl	$4, %edx
	callq	get_mem2Dpel
	movl	%eax, %ebx
	addl	%r15d, %ebx
	movq	decs(%rip), %rdi
	addq	$8, %rdi
	movq	input(%rip), %rax
	movl	4728(%rax), %esi
	movq	img(%rip), %rax
	movl	52(%rax), %ecx
	movl	68(%rax), %edx
	callq	get_mem3Dpel
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	decs(%rip), %rdi
	addq	$24, %rdi
	movq	input(%rip), %rax
	movl	4728(%rax), %esi
	movq	img(%rip), %rax
	movl	52(%rax), %ecx
	movl	68(%rax), %edx
	callq	get_mem3Dpel
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movq	decs(%rip), %rdi
	addq	$40, %rdi
	movq	img(%rip), %rax
	movl	15336(%rax), %edx
	movl	15344(%rax), %esi
	callq	get_mem2D
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	decs(%rip), %rdi
	addq	$48, %rdi
	movq	img(%rip), %rax
	movl	15336(%rax), %edx
	movl	15344(%rax), %esi
	callq	get_mem2D
	movl	%eax, %r14d
	addl	%ebp, %r14d
	movq	input(%rip), %rax
.LBB6_22:
	cmpl	$0, 4732(%rax)
	je	.LBB6_24
# BB#23:
	movq	img(%rip), %rax
	movl	52(%rax), %edx
	movl	68(%rax), %esi
	movl	$pixel_map, %edi
	callq	get_mem2D
	movl	%eax, %ebx
	addl	%r14d, %ebx
	movq	img(%rip), %rax
	movl	52(%rax), %ecx
	movl	68(%rax), %eax
	movl	%eax, %esi
	sarl	$31, %esi
	shrl	$29, %esi
	addl	%eax, %esi
	sarl	$3, %esi
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	sarl	$3, %edx
	movl	$refresh_map, %edi
	callq	get_mem2D
	movl	%eax, %r14d
	addl	%ebx, %r14d
.LBB6_24:
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	jne	.LBB6_27
# BB#25:
	movq	img(%rip), %rax
	movl	52(%rax), %edx
	movl	68(%rax), %esi
	movl	$imgY_com, %edi
	callq	get_mem2Dpel
	addl	%eax, %r14d
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB6_27
# BB#26:
	movl	64(%rax), %ecx
	movl	80(%rax), %edx
	movl	$imgUV_com, %edi
	movl	$2, %esi
	callq	get_mem3Dpel
	addl	%eax, %r14d
.LBB6_27:
	movq	input(%rip), %rax
	movl	5244(%rax), %eax
	cmpl	$3, %eax
	je	.LBB6_32
# BB#28:
	cmpl	$2, %eax
	je	.LBB6_31
# BB#29:
	cmpl	$1, %eax
	jne	.LBB6_34
# BB#30:
	callq	UMHEX_get_mem
	jmp	.LBB6_33
.LBB6_32:
	callq	EPZSInit
	jmp	.LBB6_33
.LBB6_31:
	callq	smpUMHEX_init
	callq	smpUMHEX_get_mem
.LBB6_33:
	addl	%eax, %r14d
.LBB6_34:
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB6_38
# BB#35:
	movl	$generic_RC, %edi
	callq	generic_alloc
	movl	$quadratic_RC, %edi
	callq	rc_alloc
	movq	input(%rip), %rax
	cmpl	$0, 2940(%rax)
	jne	.LBB6_37
# BB#36:
	cmpl	$2, 4708(%rax)
	jne	.LBB6_38
.LBB6_37:
	movl	$generic_RC_init, %edi
	callq	generic_alloc
	movl	$quadratic_RC_init, %edi
	callq	rc_alloc
	movl	$generic_RC_best, %edi
	callq	generic_alloc
	movl	$quadratic_RC_best, %edi
	callq	rc_alloc
	movq	input(%rip), %rax
.LBB6_38:
	cmpl	$0, 5084(%rax)
	je	.LBB6_40
# BB#39:
	movq	img(%rip), %rcx
	movl	68(%rcx), %esi
	movl	56(%rax), %edx
	movl	$imgY_tmp, %edi
	callq	get_mem2Dpel
	movl	%eax, %ebx
	addl	%r14d, %ebx
	movq	input(%rip), %rax
	movl	56(%rax), %ecx
	movl	60(%rax), %eax
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	$imgUV_tmp, %edi
	callq	get_mem2Dpel
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	input(%rip), %rax
	movl	56(%rax), %ecx
	movl	60(%rax), %eax
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	$imgUV_tmp+8, %edi
	callq	get_mem2Dpel
	movl	%eax, %r14d
	addl	%ebp, %r14d
.LBB6_40:
	movq	img(%rip), %rax
	movl	56(%rax), %edx
	movl	72(%rax), %esi
	movl	$imgY_sub_tmp, %edi
	callq	get_mem2Dint
	addl	%r14d, %eax
	movq	img(%rip), %rcx
	movl	52(%rcx), %edx
	addl	$40, %edx
	movl	%edx, img_padded_size_x(%rip)
	movl	img_pad_size_uv_x(%rip), %edx
	addl	%edx, %edx
	addl	64(%rcx), %edx
	movl	%edx, img_cr_padded_size_x(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	init_global_buffers, .Lfunc_end6-init_global_buffers
	.cfi_endproc

	.globl	chroma_mc_setup
	.p2align	4, 0x90
	.type	chroma_mc_setup,@function
chroma_mc_setup:                        # @chroma_mc_setup
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movl	15536(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB7_2
# BB#1:
	movl	$10, img_pad_size_uv_x(%rip)
	movl	$10, img_pad_size_uv_y(%rip)
	movb	$7, chroma_mask_mv_y(%rip)
	movb	$7, chroma_mask_mv_x(%rip)
	movl	$3, chroma_shift_x(%rip)
	movl	$3, chroma_shift_y(%rip)
	movl	$1, %ecx
	movl	$2, %eax
	jmp	.LBB7_5
.LBB7_2:
	xorl	%eax, %eax
	cmpl	$2, %ecx
	sete	%sil
	movl	$10, %edx
	movl	$20, %ecx
	cmovel	%edx, %ecx
	movb	$7, %dl
	je	.LBB7_4
# BB#3:
	movb	$3, %dl
.LBB7_4:
	movb	%sil, %al
	leal	2(%rax), %esi
	movl	%ecx, img_pad_size_uv_x(%rip)
	movl	$20, img_pad_size_uv_y(%rip)
	movb	$3, chroma_mask_mv_y(%rip)
	movb	%dl, chroma_mask_mv_x(%rip)
	movl	$2, chroma_shift_y(%rip)
	movl	%esi, chroma_shift_x(%rip)
	incl	%eax
	xorl	%ecx, %ecx
.LBB7_5:
	movl	%ecx, shift_cr_y(%rip)
	movl	%eax, shift_cr_x(%rip)
	retq
.Lfunc_end7:
	.size	chroma_mc_setup, .Lfunc_end7-chroma_mc_setup
	.cfi_endproc

	.globl	SetImgType
	.p2align	4, 0x90
	.type	SetImgType,@function
SetImgType:                             # @SetImgType
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rdi
	movl	1560(%rdi), %esi
	movq	img(%rip), %r8
	movl	(%r8), %ecx
	subl	start_frame_no_in_this_IGOP(%rip), %ecx
	testl	%esi, %esi
	movl	%ecx, %edx
	je	.LBB8_2
# BB#1:
	movl	%ecx, %eax
	cltd
	idivl	%esi
.LBB8_2:
	testl	%edx, %edx
	je	.LBB8_3
# BB#4:
	movl	2136(%rdi), %esi
	testl	%esi, %esi
	je	.LBB8_7
# BB#5:
	movl	%ecx, %eax
	cltd
	idivl	%esi
	testl	%edx, %edx
	je	.LBB8_6
.LBB8_7:
	xorl	%eax, %eax
	cmpl	$2, 2964(%rdi)
	sete	%al
	movl	%eax, 20(%r8)
	retq
.LBB8_3:
	movl	$2, %eax
	movl	%eax, 20(%r8)
	retq
.LBB8_6:
	movl	$3, %eax
	movl	%eax, 20(%r8)
	retq
.Lfunc_end8:
	.size	SetImgType, .Lfunc_end8-SetImgType
	.cfi_endproc

	.globl	Init_redundant_frame
	.p2align	4, 0x90
	.type	Init_redundant_frame,@function
Init_redundant_frame:                   # @Init_redundant_frame
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 16
	movq	input(%rip), %rsi
	cmpl	$0, 5084(%rsi)
	je	.LBB9_11
# BB#1:
	cmpl	$0, 2096(%rsi)
	je	.LBB9_3
# BB#2:
	movl	$.L.str.208, %edi
	movl	$100, %esi
	callq	error
	movq	input(%rip), %rsi
.LBB9_3:
	cmpl	$0, 4704(%rsi)
	je	.LBB9_5
# BB#4:
	movl	$.L.str.209, %edi
	movl	$100, %esi
	callq	error
	movq	input(%rip), %rsi
.LBB9_5:
	movl	5764(%rsi), %eax
	cmpl	%eax, 32(%rsi)
	jge	.LBB9_7
# BB#6:
	movl	$.L.str.210, %edi
	movl	$100, %esi
	callq	error
	movq	input(%rip), %rsi
	movl	5764(%rsi), %eax
.LBB9_7:
	movb	5760(%rsi), %cl
	movl	$1, %edx
	shll	%cl, %edx
	cmpl	%eax, %edx
	jle	.LBB9_9
# BB#8:
	movl	$.L.str.211, %edi
	movl	$100, %esi
	callq	error
	movq	input(%rip), %rsi
.LBB9_9:
	cmpl	$1, 5112(%rsi)
	je	.LBB9_11
# BB#10:
	movl	$.L.str.212, %edi
	movl	$100, %esi
	callq	error
	movq	input(%rip), %rsi
.LBB9_11:
	movl	$0, key_frame(%rip)
	movl	$0, redundant_coding(%rip)
	movq	img(%rip), %rcx
	movl	$0, 15264(%rcx)
	movl	(%rcx), %eax
	cltd
	idivl	5764(%rsi)
	movl	%edx, frameNuminGOP(%rip)
	cmpl	$1, (%rcx)
	sbbl	%eax, %eax
	orl	%edx, %eax
	movl	%eax, frameNuminGOP(%rip)
	popq	%rax
	retq
.Lfunc_end9:
	.size	Init_redundant_frame, .Lfunc_end9-Init_redundant_frame
	.cfi_endproc

	.globl	Set_redundant_frame
	.p2align	4, 0x90
	.type	Set_redundant_frame,@function
Set_redundant_frame:                    # @Set_redundant_frame
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rdx
	movl	5764(%rdx), %ecx
	movl	frameNuminGOP(%rip), %eax
	testl	%eax, %eax
	jne	.LBB10_2
# BB#1:
	movl	$0, redundant_coding(%rip)
	movl	$1, key_frame(%rip)
	movl	%ecx, redundant_ref_idx(%rip)
.LBB10_2:
	movl	5760(%rdx), %esi
	testl	%esi, %esi
	jle	.LBB10_24
# BB#3:
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	cmpl	%edx, %eax
	jne	.LBB10_5
# BB#4:
	movl	$0, redundant_coding(%rip)
	movl	$1, key_frame(%rip)
	movl	%eax, redundant_ref_idx(%rip)
.LBB10_5:
	cmpl	$2, %esi
	jl	.LBB10_24
# BB#6:
	movl	%ecx, %r8d
	sarl	$31, %r8d
	movl	%r8d, %edi
	shrl	$30, %edi
	addl	%ecx, %edi
	sarl	$2, %edi
	cmpl	%edi, %eax
	je	.LBB10_8
# BB#7:
	leal	(%rcx,%rcx,2), %r9d
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%r9d, %edx
	sarl	$2, %edx
	cmpl	%edx, %eax
	jne	.LBB10_9
.LBB10_8:
	movl	$0, redundant_coding(%rip)
	movl	$1, key_frame(%rip)
	movl	%edi, redundant_ref_idx(%rip)
.LBB10_9:
	cmpl	$3, %esi
	jl	.LBB10_24
# BB#10:
	movl	%r8d, %edi
	shrl	$29, %edi
	addl	%ecx, %edi
	sarl	$3, %edi
	cmpl	%edi, %eax
	je	.LBB10_14
# BB#11:
	leal	(%rcx,%rcx,2), %r9d
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%r9d, %edx
	sarl	$3, %edx
	cmpl	%edx, %eax
	je	.LBB10_14
# BB#12:
	leal	(%rcx,%rcx,4), %r9d
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%r9d, %edx
	sarl	$3, %edx
	cmpl	%edx, %eax
	je	.LBB10_14
# BB#13:
	leal	(,%rcx,8), %r9d
	subl	%ecx, %r9d
	movl	%r9d, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%r9d, %edx
	sarl	$3, %edx
	cmpl	%edx, %eax
	jne	.LBB10_15
.LBB10_14:
	movl	$0, redundant_coding(%rip)
	movl	$1, key_frame(%rip)
	movl	%edi, redundant_ref_idx(%rip)
.LBB10_15:
	cmpl	$4, %esi
	jl	.LBB10_24
# BB#16:
	shrl	$28, %r8d
	addl	%ecx, %r8d
	sarl	$4, %r8d
	cmpl	%r8d, %eax
	je	.LBB10_23
# BB#17:
	leal	(%rcx,%rcx,2), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%edx, %esi
	sarl	$4, %esi
	cmpl	%esi, %eax
	je	.LBB10_23
# BB#18:
	leal	(%rcx,%rcx,4), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%edx, %esi
	sarl	$4, %esi
	cmpl	%esi, %eax
	je	.LBB10_23
# BB#19:
	leal	(,%rcx,8), %edx
	subl	%ecx, %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%edx, %esi
	sarl	$4, %esi
	cmpl	%esi, %eax
	je	.LBB10_23
# BB#20:
	leal	(%rcx,%rcx,8), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%edx, %esi
	sarl	$4, %esi
	cmpl	%esi, %eax
	je	.LBB10_23
# BB#21:
	imull	$11, %ecx, %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%edx, %esi
	sarl	$4, %esi
	cmpl	%esi, %eax
	je	.LBB10_23
# BB#22:
	imull	$13, %ecx, %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	cmpl	%edx, %eax
	jne	.LBB10_24
.LBB10_23:
	movl	$0, redundant_coding(%rip)
	movl	$1, key_frame(%rip)
	movl	%r8d, redundant_ref_idx(%rip)
.LBB10_24:                              # %.thread22
	retq
.Lfunc_end10:
	.size	Set_redundant_frame, .Lfunc_end10-Set_redundant_frame
	.cfi_endproc

	.globl	encode_one_redundant_frame
	.p2align	4, 0x90
	.type	encode_one_redundant_frame,@function
encode_one_redundant_frame:             # @encode_one_redundant_frame
	.cfi_startproc
# BB#0:
	movl	$0, key_frame(%rip)
	movl	$1, redundant_coding(%rip)
	movq	img(%rip), %rax
	movl	$1, 15264(%rax)
	cmpl	$2, 20(%rax)
	jne	.LBB11_2
# BB#1:
	movl	$0, 20(%rax)
.LBB11_2:
	jmp	encode_one_frame        # TAILCALL
.Lfunc_end11:
	.size	encode_one_redundant_frame, .Lfunc_end11-encode_one_redundant_frame
	.cfi_endproc

	.globl	report_frame_statistic
	.p2align	4, 0x90
	.type	report_frame_statistic,@function
report_frame_statistic:                 # @report_frame_statistic
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 48
	subq	$1056, %rsp             # imm = 0x420
.Lcfi35:
	.cfi_def_cfa_offset 1104
.Lcfi36:
	.cfi_offset %rbx, -48
.Lcfi37:
	.cfi_offset %r12, -40
.Lcfi38:
	.cfi_offset %r13, -32
.Lcfi39:
	.cfi_offset %r14, -24
.Lcfi40:
	.cfi_offset %r15, -16
	movl	$.L.str.5, %edi
	movl	$.L.str.6, %esi
	callq	fopen64
	testq	%rax, %rax
	je	.LBB12_1
# BB#3:
	movq	%rax, %rdi
	callq	fclose
	movl	$.L.str.5, %edi
	movl	$.L.str.7, %esi
	callq	fopen64
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB12_5
	jmp	.LBB12_4
.LBB12_1:
	movl	$.L.str.5, %edi
	movl	$.L.str.7, %esi
	callq	fopen64
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB12_4
# BB#2:
	movl	$.L.str.10, %edi
	movl	$468, %esi              # imm = 0x1D4
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.11, %edi
	movl	$468, %esi              # imm = 0x1D4
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.10, %edi
	movl	$468, %esi              # imm = 0x1D4
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpl	$0, frame_statistic_start(%rip)
	jne	.LBB12_6
	jmp	.LBB12_7
.LBB12_4:
	xorl	%r14d, %r14d
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.8, %edx
	movl	$.L.str.9, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB12_5:
	cmpl	$0, frame_statistic_start(%rip)
	je	.LBB12_7
.LBB12_6:
	movl	$.L.str.12, %edi
	movl	$472, %esi              # imm = 0x1D8
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.13, %edi
	movl	$472, %esi              # imm = 0x1D8
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB12_7:
	xorl	%r12d, %r12d
	movl	$.L.str.14, %esi
	movl	$.L.str.15, %edx
	movl	$.L.str.16, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	xorl	%edi, %edi
	callq	time
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	callq	time
	movq	%rbx, %rdi
	callq	localtime
	movq	%rax, %rbx
	leaq	48(%rsp), %r15
	movl	$1000, %esi             # imm = 0x3E8
	movl	$.L.str.17, %edx
	movq	%r15, %rdi
	movq	%rbx, %rcx
	callq	strftime
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	fprintf
	movl	$1000, %esi             # imm = 0x3E8
	movl	$.L.str.19, %edx
	movq	%r15, %rdi
	movq	%rbx, %rcx
	callq	strftime
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	fprintf
	movq	input(%rip), %r13
	leaq	280(%r13), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_8:                               # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	strlen
	addl	$-20, %eax
	cmovsl	%r12d, %eax
	leal	(%rbx,%rax), %eax
	cltq
	movzbl	280(%r13,%rax), %eax
	movb	%al, 16(%rsp,%rbx)
	incq	%rbx
	cmpq	$20, %rbx
	jne	.LBB12_8
# BB#9:
	leaq	16(%rsp), %rdx
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	frame_no(%rip), %edx
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	img(%rip), %rax
	movl	36(%rax), %edx
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movl	4704(%rax), %edx
	movl	4708(%rax), %ecx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	img(%rip), %rax
	cmpl	$0, (%rax)
	jne	.LBB12_12
# BB#10:
	cmpl	$0, 15332(%rax)
	je	.LBB12_11
.LBB12_12:
	movq	stats(%rip), %rax
	movq	24(%rax), %rax
	movq	%rax, %rdx
	subq	report_frame_statistic.last_bit_ctr_n(%rip), %rdx
	movq	%rax, report_frame_statistic.last_bit_ctr_n(%rip)
.LBB12_13:
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movq	snr(%rip), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.25, %esi
	movb	$3, %al
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	1072(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+312(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	1104(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+344(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	1080(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+320(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	684(%rax), %edx
	subl	report_frame_statistic.last_mode_chroma_use(%rip), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	688(%rax), %edx
	subl	report_frame_statistic.last_mode_chroma_use+4(%rip), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	692(%rax), %edx
	subl	report_frame_statistic.last_mode_chroma_use+8(%rip), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	696(%rax), %edx
	subl	report_frame_statistic.last_mode_chroma_use+12(%rip), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	832(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+72(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	864(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+104(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	840(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+80(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	760(%rax), %rdx
	subq	report_frame_statistic.last_mode_use(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	768(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+8(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	776(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+16(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	784(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+24(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	88(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	388(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	92(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	392(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	96(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	396(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	824(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+64(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	792(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+32(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	100(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	400(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	800(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+40(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	808(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+48(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	816(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+56(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	952(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+192(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	984(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+224(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	960(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+200(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	880(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+120(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	888(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+128(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	896(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+136(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	904(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+144(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	144(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	444(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	148(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	448(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	152(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	452(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	156(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	456(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	944(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+184(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	56(%rax), %edx
	addl	52(%rax), %edx
	subl	report_frame_statistic.last_b8_mode_0+8(%rip), %edx
	subl	report_frame_statistic.last_b8_mode_0+12(%rip), %edx
	movl	$.L.str.28, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	56(%rax), %edx
	subl	report_frame_statistic.last_b8_mode_0+12(%rip), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	52(%rax), %edx
	subl	report_frame_statistic.last_b8_mode_0+8(%rip), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	912(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+152(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	160(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movl	460(%rax), %edx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	920(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+160(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	928(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+168(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stats(%rip), %rax
	movq	936(%rax), %rdx
	subq	report_frame_statistic.last_mode_use+176(%rip), %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$10, %edi
	movq	%r14, %rsi
	callq	fputc
	movq	stats(%rip), %rax
	movq	1112(%rax), %rcx
	movq	%rcx, report_frame_statistic.last_mode_use+352(%rip)
	movups	1096(%rax), %xmm0
	movaps	%xmm0, report_frame_statistic.last_mode_use+336(%rip)
	movups	1080(%rax), %xmm0
	movaps	%xmm0, report_frame_statistic.last_mode_use+320(%rip)
	movups	1064(%rax), %xmm0
	movaps	%xmm0, report_frame_statistic.last_mode_use+304(%rip)
	movups	1000(%rax), %xmm0
	movups	1016(%rax), %xmm1
	movups	1032(%rax), %xmm2
	movups	1048(%rax), %xmm3
	movaps	%xmm3, report_frame_statistic.last_mode_use+288(%rip)
	movaps	%xmm2, report_frame_statistic.last_mode_use+272(%rip)
	movaps	%xmm1, report_frame_statistic.last_mode_use+256(%rip)
	movaps	%xmm0, report_frame_statistic.last_mode_use+240(%rip)
	movq	872(%rax), %rcx
	movq	%rcx, report_frame_statistic.last_mode_use+112(%rip)
	movups	856(%rax), %xmm0
	movaps	%xmm0, report_frame_statistic.last_mode_use+96(%rip)
	movups	840(%rax), %xmm0
	movaps	%xmm0, report_frame_statistic.last_mode_use+80(%rip)
	movups	824(%rax), %xmm0
	movaps	%xmm0, report_frame_statistic.last_mode_use+64(%rip)
	movups	760(%rax), %xmm0
	movups	776(%rax), %xmm1
	movups	792(%rax), %xmm2
	movups	808(%rax), %xmm3
	movaps	%xmm3, report_frame_statistic.last_mode_use+48(%rip)
	movaps	%xmm2, report_frame_statistic.last_mode_use+32(%rip)
	movaps	%xmm1, report_frame_statistic.last_mode_use+16(%rip)
	movaps	%xmm0, report_frame_statistic.last_mode_use(%rip)
	movq	992(%rax), %rcx
	movq	%rcx, report_frame_statistic.last_mode_use+232(%rip)
	movups	976(%rax), %xmm0
	movups	%xmm0, report_frame_statistic.last_mode_use+216(%rip)
	movups	960(%rax), %xmm0
	movups	%xmm0, report_frame_statistic.last_mode_use+200(%rip)
	movups	944(%rax), %xmm0
	movups	%xmm0, report_frame_statistic.last_mode_use+184(%rip)
	movups	880(%rax), %xmm0
	movups	896(%rax), %xmm1
	movups	912(%rax), %xmm2
	movups	928(%rax), %xmm3
	movups	%xmm3, report_frame_statistic.last_mode_use+168(%rip)
	movups	%xmm2, report_frame_statistic.last_mode_use+152(%rip)
	movups	%xmm1, report_frame_statistic.last_mode_use+136(%rip)
	movups	%xmm0, report_frame_statistic.last_mode_use+120(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 180(%rax)
	movups	%xmm0, 164(%rax)
	movups	%xmm0, 148(%rax)
	movups	%xmm0, 132(%rax)
	movups	%xmm0, 116(%rax)
	movups	%xmm0, 100(%rax)
	movups	%xmm0, 84(%rax)
	movq	$0, 196(%rax)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 464(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 384(%rax)
	movq	$0, 496(%rax)
	movq	52(%rax), %rcx
	movq	%rcx, report_frame_statistic.last_b8_mode_0+8(%rip)
	movups	684(%rax), %xmm0
	movaps	%xmm0, report_frame_statistic.last_mode_chroma_use(%rip)
	movl	$0, frame_statistic_start(%rip)
	movq	%r14, %rdi
	callq	fclose
	addq	$1056, %rsp             # imm = 0x420
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB12_11:
	movq	stats(%rip), %rax
	movq	720(%rax), %rdx
	jmp	.LBB12_13
.Lfunc_end12:
	.size	report_frame_statistic, .Lfunc_end12-report_frame_statistic
	.cfi_endproc

	.globl	process_2nd_IGOP
	.p2align	4, 0x90
	.type	process_2nd_IGOP,@function
process_2nd_IGOP:                       # @process_2nd_IGOP
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rax
	movl	4740(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB13_4
# BB#1:
	cmpl	$0, In2ndIGOP(%rip)
	jne	.LBB13_4
# BB#2:
	movq	img(%rip), %rdx
	movl	(%rdx), %esi
	movl	8(%rax), %edx
	leal	-1(%rdx), %edi
	cmpl	%edi, %esi
	jne	.LBB13_4
# BB#3:
	movl	$1, In2ndIGOP(%rip)
	movl	%edx, start_frame_no_in_this_IGOP(%rip)
	movl	20(%rax), %edi
	incl	%edi
	imull	%esi, %edi
	incl	%edi
	movl	%edi, start_tr_in_this_IGOP(%rip)
	addl	%edx, %ecx
	movl	%ecx, 8(%rax)
.LBB13_4:
	retq
.Lfunc_end13:
	.size	process_2nd_IGOP, .Lfunc_end13-process_2nd_IGOP
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI14_0:
	.quad	4607182418800017408     # double 1
.LCPI14_1:
	.quad	4621819117588971520     # double 10
.LCPI14_3:
	.quad	4562254508917369340     # double 0.001
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_2:
	.long	1148846080              # float 1000
.LCPI14_4:
	.long	0                       # float 0
	.text
	.globl	report
	.p2align	4, 0x90
	.type	report,@function
report:                                 # @report
	.cfi_startproc
# BB#0:                                 # %.preheader138173
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 40
	subq	$1192, %rsp             # imm = 0x4A8
.Lcfi45:
	.cfi_def_cfa_offset 1232
.Lcfi46:
	.cfi_offset %rbx, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movslq	frame_ctr+8(%rip), %rax
	movq	%rax, 112(%rsp)
	movl	frame_ctr+12(%rip), %eax
	addl	frame_ctr(%rip), %eax
	testl	%eax, %eax
	movl	$1, %ecx
	cmovgl	%eax, %ecx
	movq	%rcx, 80(%rsp)
	movslq	frame_ctr+4(%rip), %rcx
	movq	%rcx, 96(%rsp)
	movq	$0, 88(%rsp)
	movq	$0, 104(%rsp)
	movq	$0, 120(%rsp)
	movq	$0, 136(%rsp)
	movq	$0, 152(%rsp)
	movq	stats(%rip), %rax
	leaq	1472(%rax), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	jmp	.LBB14_1
	.p2align	4, 0x90
.LBB14_2:                               # %.preheader137..preheader137_crit_edge
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	104(%rsp,%rsi,2), %rdi
	addq	$120, %rdx
	addq	$8, %rsi
.LBB14_1:                               # %.preheader137
                                        # =>This Inner Loop Header: Depth=1
	addq	-112(%rdx), %rdi
	addq	-104(%rdx), %rdi
	addq	-96(%rdx), %rdi
	addq	-88(%rdx), %rdi
	addq	-80(%rdx), %rdi
	addq	-72(%rdx), %rdi
	addq	-64(%rdx), %rdi
	addq	-56(%rdx), %rdi
	addq	-48(%rdx), %rdi
	addq	-40(%rdx), %rdi
	addq	-32(%rdx), %rdi
	addq	-24(%rdx), %rdi
	addq	-16(%rdx), %rdi
	addq	-8(%rdx), %rdi
	addq	(%rdx), %rdi
	addq	2040(%rax,%rsi), %rdi
	addq	2000(%rax,%rsi), %rdi
	addq	2080(%rax,%rsi), %rdi
	addq	2120(%rax,%rsi), %rdi
	addq	2160(%rax,%rsi), %rdi
	addq	2200(%rax,%rsi), %rdi
	addq	1960(%rax,%rsi), %rdi
	movq	%rdi, 88(%rsp,%rsi,2)
	cmpq	$32, %rsi
	jne	.LBB14_2
# BB#3:
	movq	img(%rip), %rdx
	movl	700(%rax), %esi
	incl	%esi
	cvtsi2ssl	%esi, %xmm0
	mulss	48(%rdx), %xmm0
	movq	input(%rip), %rdx
	movl	20(%rdx), %esi
	incl	%esi
	cvtsi2ssl	%esi, %xmm1
	divss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssq	720(%rax), %xmm1
	mulss	%xmm0, %xmm1
	movl	8(%rdx), %edx
	addl	%ecx, %edx
	cvtsi2ssl	%edx, %xmm2
	divss	%xmm2, %xmm1
	movss	%xmm1, 744(%rax)
	xorps	%xmm1, %xmm1
	cvtsi2ssq	728(%rax), %xmm1
	mulss	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	movss	%xmm1, 748(%rax)
	xorps	%xmm1, %xmm1
	cvtsi2ssq	736(%rax), %xmm1
	mulss	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	movss	%xmm1, 752(%rax)
	movq	stdout(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$80, %esi
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rax
	cmpl	$0, 5112(%rax)
	je	.LBB14_66
# BB#4:
	movq	stdout(%rip), %rdi
	movq	img(%rip), %rcx
	movss	48(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movl	20(%rax), %eax
	incl	%eax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.31, %esi
	movb	$1, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	movslq	5780(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	DistortionType(,%rax,4), %rcx
	movl	$.L.str.32, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	movslq	5784(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	DistortionType(,%rax,4), %rcx
	movl	$.L.str.32, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	movslq	5788(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	DistortionType(,%rax,4), %rcx
	movl	$.L.str.32, %esi
	movl	$2, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	movslq	5792(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	DistortionType(,%rax,4), %rdx
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %rax
	movq	stdout(%rip), %rcx
	cmpl	$1, 5776(%rax)
	jne	.LBB14_6
# BB#5:
	movl	$.L.str.34, %edi
	movl	$43, %esi
	jmp	.LBB14_7
.LBB14_6:
	movl	$.L.str.35, %edi
	movl	$39, %esi
.LBB14_7:
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	movl	56(%rax), %edx
	movl	60(%rax), %ecx
	movl	$.L.str.36, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %rax
	cmpl	$0, 68(%rax)
	movq	stdout(%rip), %rcx
	je	.LBB14_9
# BB#8:
	movl	$.L.str.37, %edi
	movl	$40, %esi
	jmp	.LBB14_10
.LBB14_9:
	movl	$.L.str.38, %edi
	movl	$41, %esi
.LBB14_10:
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	movl	28(%rax), %edx
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	movl	32(%rax), %edx
	movl	$.L.str.40, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	movl	36(%rax), %edx
	testl	%edx, %edx
	jne	.LBB14_12
# BB#11:
	movl	32(%rax), %edx
.LBB14_12:
	movl	$.L.str.41, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stats(%rip), %rax
	cmpl	$0, 700(%rax)
	je	.LBB14_16
# BB#13:
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	cmpl	$0, 40(%rax)
	leaq	40(%rax), %rcx
	leaq	32(%rax), %rax
	cmoveq	%rax, %rcx
	movl	(%rcx), %edx
	movl	$.L.str.42, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	movl	44(%rax), %edx
	testl	%edx, %edx
	jne	.LBB14_15
# BB#14:
	movl	32(%rax), %edx
.LBB14_15:
	movl	$.L.str.43, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB14_16:
	movq	stdout(%rip), %rcx
	movl	$.L.str.44, %edi
	movl	$36, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stats(%rip), %rax
	movl	700(%rax), %eax
	testl	%eax, %eax
	jle	.LBB14_29
# BB#17:
	movq	input(%rip), %rax
	cmpl	$0, 2968(%rax)
	je	.LBB14_19
# BB#18:
	movq	stdout(%rip), %rdi
	movl	12(%rax), %edx
	movl	16(%rax), %ecx
	movl	2104(%rax), %r8d
	movl	$.L.str.45, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB14_33
.LBB14_29:
	movq	input(%rip), %rax
	jne	.LBB14_32
# BB#30:
	cmpl	$0, 2136(%rax)
	je	.LBB14_31
.LBB14_32:                              # %._crit_edge175
	movq	stdout(%rip), %rdi
	movl	12(%rax), %edx
	movl	16(%rax), %ecx
	movl	2140(%rax), %r8d
	movl	2144(%rax), %r9d
	movl	$.L.str.53, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB14_33
.LBB14_19:                              # %.lr.ph.preheader
	movw	$73, 192(%rsp)
	xorl	%r14d, %r14d
	leaq	192(%rsp), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_20:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	input(%rip), %rax
	movl	2964(%rax), %ebx
	movq	%r15, %rdi
	callq	strlen
	movl	$79, %ecx
	subq	%rax, %rcx
	testl	%ecx, %ecx
	movl	%ecx, %edx
	cmovleq	%r14, %rdx
	cmpl	$0, %ebx
	je	.LBB14_22
# BB#21:                                #   in Loop: Header=BB14_20 Depth=1
	movl	$.L.str.47, %esi
	jmp	.LBB14_23
	.p2align	4, 0x90
.LBB14_22:                              #   in Loop: Header=BB14_20 Depth=1
	movl	$.L.str.48, %esi
.LBB14_23:                              #   in Loop: Header=BB14_20 Depth=1
	movq	%r15, %rdi
	callq	strncat
	incl	%ebp
	movq	stats(%rip), %rax
	cmpl	700(%rax), %ebp
	jl	.LBB14_20
# BB#24:                                # %._crit_edge
	leaq	192(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	$79, %ecx
	subq	%rax, %rcx
	xorl	%r14d, %r14d
	testl	%ecx, %ecx
	movl	%ecx, %edx
	cmovleq	%r14, %rdx
	movl	$.L.str.49, %esi
	movq	%rbx, %rdi
	callq	strncat
	movq	stats(%rip), %rax
	cmpl	$0, 700(%rax)
	jle	.LBB14_186
# BB#25:                                # %.lr.ph.1.preheader
	leaq	192(%rsp), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_26:                              # %.lr.ph.1
                                        # =>This Inner Loop Header: Depth=1
	movq	input(%rip), %rax
	movl	2964(%rax), %ebx
	movq	%r15, %rdi
	callq	strlen
	movl	$79, %ecx
	subq	%rax, %rcx
	testl	%ecx, %ecx
	movl	%ecx, %edx
	cmovleq	%r14, %rdx
	cmpl	$0, %ebx
	je	.LBB14_184
# BB#27:                                #   in Loop: Header=BB14_26 Depth=1
	movl	$.L.str.47, %esi
	jmp	.LBB14_185
	.p2align	4, 0x90
.LBB14_184:                             #   in Loop: Header=BB14_26 Depth=1
	movl	$.L.str.48, %esi
.LBB14_185:                             #   in Loop: Header=BB14_26 Depth=1
	movq	%r15, %rdi
	callq	strncat
	incl	%ebp
	movq	stats(%rip), %rax
	cmpl	700(%rax), %ebp
	jl	.LBB14_26
.LBB14_186:                             # %._crit_edge.1
	leaq	192(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	$79, %ecx
	subq	%rax, %rcx
	xorl	%ebp, %ebp
	testl	%ecx, %ecx
	movl	%ecx, %edx
	cmovleq	%rbp, %rdx
	movl	$.L.str.49, %esi
	movq	%rbx, %rdi
	callq	strncat
	movq	input(%rip), %rax
	cmpl	$0, 2964(%rax)
	movq	stdout(%rip), %rdi
	movl	12(%rax), %ecx
	movl	16(%rax), %r8d
	movl	2104(%rax), %r9d
	je	.LBB14_28
# BB#187:
	addl	2108(%rax), %r9d
	cmovsl	%ebp, %r9d
	cmpl	$52, %r9d
	movl	$51, %ebx
	cmovll	%r9d, %ebx
	leaq	192(%rsp), %rdx
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	movl	%ebx, %r9d
	callq	fprintf
	jmp	.LBB14_33
.LBB14_28:
	leaq	192(%rsp), %rdx
	movl	$.L.str.51, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB14_33
.LBB14_31:
	movq	stdout(%rip), %rdi
	movl	12(%rax), %edx
	movl	16(%rax), %ecx
	movl	$.L.str.52, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB14_33:
	movq	input(%rip), %rax
	movq	stdout(%rip), %rcx
	cmpl	$0, 4008(%rax)
	je	.LBB14_34
# BB#35:
	movl	$.L.str.55, %edi
	jmp	.LBB14_36
.LBB14_34:
	movl	$.L.str.54, %edi
.LBB14_36:
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movq	input(%rip), %rax
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	movl	$.L.str.56, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %rax
	movl	5244(%rax), %eax
	cmpl	$3, %eax
	je	.LBB14_41
# BB#37:
	cmpl	$2, %eax
	je	.LBB14_40
# BB#38:
	cmpl	$1, %eax
	jne	.LBB14_42
# BB#39:
	movq	stdout(%rip), %rcx
	movl	$.L.str.57, %edi
	movl	$41, %esi
	jmp	.LBB14_45
.LBB14_41:
	movq	stdout(%rip), %rcx
	movl	$.L.str.59, %edi
	movl	$42, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	xorl	%esi, %esi
	callq	EPZSOutputStats
	jmp	.LBB14_46
.LBB14_40:
	movq	stdout(%rip), %rcx
	movl	$.L.str.58, %edi
	movl	$42, %esi
	jmp	.LBB14_45
.LBB14_42:
	movq	stdout(%rip), %rcx
	testl	%eax, %eax
	je	.LBB14_43
# BB#44:
	movl	$.L.str.61, %edi
	movl	$49, %esi
	jmp	.LBB14_45
.LBB14_43:
	movl	$.L.str.60, %edi
	movl	$54, %esi
.LBB14_45:
	movl	$1, %edx
	callq	fwrite
.LBB14_46:
	movq	input(%rip), %rax
	movl	4140(%rax), %eax
	cmpl	$2, %eax
	jne	.LBB14_48
# BB#47:
	movq	stdout(%rip), %rcx
	movl	$.L.str.62, %edi
	movl	$42, %esi
	jmp	.LBB14_51
.LBB14_48:
	movq	stdout(%rip), %rcx
	cmpl	$1, %eax
	jne	.LBB14_50
# BB#49:
	movl	$.L.str.63, %edi
	movl	$60, %esi
	jmp	.LBB14_51
.LBB14_50:
	movl	$.L.str.64, %edi
	movl	$79, %esi
.LBB14_51:
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rax
	cmpl	$0, 4168(%rax)
	movq	stdout(%rip), %rcx
	je	.LBB14_53
# BB#52:
	movl	$.L.str.65, %edi
	movl	$42, %esi
	jmp	.LBB14_54
.LBB14_53:
	movl	$.L.str.66, %edi
	movl	$46, %esi
.LBB14_54:
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rax
	movl	4016(%rax), %eax
	cmpl	$1, %eax
	je	.LBB14_57
# BB#55:
	testl	%eax, %eax
	jne	.LBB14_58
# BB#56:
	movq	stdout(%rip), %rcx
	movl	$.L.str.67, %edi
	movl	$50, %esi
	jmp	.LBB14_60
.LBB14_57:
	movq	stdout(%rip), %rcx
	movl	$.L.str.68, %edi
	jmp	.LBB14_59
.LBB14_58:
	movq	stdout(%rip), %rcx
	movl	$.L.str.69, %edi
.LBB14_59:
	movl	$51, %esi
.LBB14_60:
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rax
	movl	4012(%rax), %eax
	cmpl	$1, %eax
	je	.LBB14_63
# BB#61:
	testl	%eax, %eax
	jne	.LBB14_64
# BB#62:
	movq	stdout(%rip), %rcx
	movl	$.L.str.70, %edi
	movl	$67, %esi
	jmp	.LBB14_65
.LBB14_63:
	movq	stdout(%rip), %rcx
	movl	$.L.str.71, %edi
	movl	$61, %esi
	jmp	.LBB14_65
.LBB14_64:
	movq	stdout(%rip), %rcx
	movl	$.L.str.72, %edi
	movl	$51, %esi
.LBB14_65:
	movl	$1, %edx
	callq	fwrite
.LBB14_66:
	movq	stdout(%rip), %rcx
	movl	$.L.str.73, %edi
	movl	$80, %esi
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rax
	cmpl	$0, 5112(%rax)
	je	.LBB14_78
# BB#67:
	movl	56(%rax), %ebp
	imull	60(%rax), %ebp
	movl	5268(%rax), %r14d
	imull	5264(%rax), %r14d
	movq	img(%rip), %rax
	movl	15520(%rax), %ecx
	movl	15524(%rax), %ebx
	imull	%ecx, %ecx
	imull	%ebx, %ebx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	movq	snr(%rip), %rax
	movss	108(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	ucomiss	%xmm2, %xmm3
	cvtss2sd	%xmm2, %xmm2
	jne	.LBB14_70
# BB#68:
	jp	.LBB14_70
# BB#69:
	movsd	.LCPI14_0(%rip), %xmm2  # xmm2 = mem[0],zero
.LBB14_70:
	divsd	%xmm2, %xmm1
	mulsd	%xmm1, %xmm0
	callq	log10
	mulsd	.LCPI14_1(%rip), %xmm0
	xorps	%xmm3, %xmm3
	cvtsi2sdq	%rbx, %xmm3
	cvtsi2sdl	%r14d, %xmm4
	movq	snr(%rip), %rax
	movss	112(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorpd	%xmm2, %xmm2
	ucomiss	%xmm1, %xmm2
	cvtss2sd	%xmm1, %xmm1
	jne	.LBB14_73
# BB#71:
	jp	.LBB14_73
# BB#72:
	movsd	.LCPI14_0(%rip), %xmm1  # xmm1 = mem[0],zero
.LBB14_73:
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movsd	%xmm4, 56(%rsp)         # 8-byte Spill
	movapd	%xmm4, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm3, 48(%rsp)         # 8-byte Spill
	mulsd	%xmm3, %xmm0
	callq	log10
	mulsd	.LCPI14_1(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
	movq	snr(%rip), %rax
	movss	116(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorpd	%xmm2, %xmm2
	ucomiss	%xmm0, %xmm2
	cvtss2sd	%xmm0, %xmm0
	movss	%xmm1, 40(%rsp)         # 4-byte Spill
	jne	.LBB14_74
# BB#75:
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	56(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	jp	.LBB14_77
# BB#76:
	movsd	.LCPI14_0(%rip), %xmm0  # xmm0 = mem[0],zero
	jmp	.LBB14_77
.LBB14_74:
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	56(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB14_77:
	divsd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	callq	log10
	mulsd	.LCPI14_1(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	movq	stdout(%rip), %rdi
	movq	snr(%rip), %rax
	movss	84(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.74, %esi
	movb	$1, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	snr(%rip), %rax
	movss	88(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.75, %esi
	movb	$1, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	snr(%rip), %rax
	movss	92(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.76, %esi
	movb	$1, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	movss	44(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	snr(%rip), %rax
	movss	108(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ebp, %xmm2
	divss	%xmm2, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.77, %esi
	movb	$2, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	snr(%rip), %rax
	movss	112(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%r14d, %xmm2
	movss	%xmm2, 40(%rsp)         # 4-byte Spill
	divss	%xmm2, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.78, %esi
	movb	$2, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	snr(%rip), %rax
	movss	116(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	divss	40(%rsp), %xmm1         # 4-byte Folded Reload
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.79, %esi
	movb	$2, %al
	callq	fprintf
.LBB14_78:
	movq	stats(%rip), %rax
	movq	720(%rax), %rcx
	movq	728(%rax), %r8
	leaq	(%rcx,%r8), %rbx
	cmpl	$0, frame_ctr+4(%rip)
	movq	stdout(%rip), %rdi
	movslq	2256(%rax), %r9
	je	.LBB14_80
# BB#79:
	movq	736(%rax), %r10
	addq	%r10, %rbx
	addq	%r9, %rbx
	movl	%r9d, (%rsp)
	movl	$.L.str.80, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	movq	%r10, %r9
	callq	fprintf
	movq	img(%rip), %rax
	movq	stats(%rip), %rcx
	movl	700(%rcx), %edx
	incl	%edx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%edx, %xmm2
	mulss	48(%rax), %xmm2
	movq	input(%rip), %rax
	movl	20(%rax), %eax
	incl	%eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	%xmm0, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rbx, %xmm1
	mulss	%xmm2, %xmm1
	movl	frame_ctr(%rip), %eax
	addl	frame_ctr+8(%rip), %eax
	addl	frame_ctr+4(%rip), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	%xmm0, %xmm1
	movss	%xmm1, 12(%rcx)
	jmp	.LBB14_81
.LBB14_80:
	addq	%r9, %rbx
	movl	$.L.str.82, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	callq	fprintf
	movq	img(%rip), %rax
	movss	48(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movq	input(%rip), %rax
	movl	20(%rax), %ecx
	incl	%ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	divss	%xmm0, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rbx, %xmm1
	mulss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	8(%rax), %xmm0
	divss	%xmm0, %xmm1
	movq	stats(%rip), %rax
	movss	%xmm1, 12(%rax)
.LBB14_81:
	movq	stdout(%rip), %rdi
	movss	%xmm2, 40(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm0
	divss	.LCPI14_2(%rip), %xmm1
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.81, %esi
	movb	$2, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	stats(%rip), %rax
	movl	40(%rax), %edx
	movl	$.L.str.83, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	stats(%rip), %rax
	movl	2256(%rax), %edx
	movl	$.L.str.84, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$80, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movl	$.L.str.85, %esi
	movl	$.L.str.86, %edx
	movl	$.L.str.15, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$.L.str.87, %edi
	movl	$.L.str.88, %esi
	callq	fopen64
	movq	%rax, p_stat(%rip)
	testq	%rax, %rax
	jne	.LBB14_83
# BB#82:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.89, %edx
	movl	$.L.str.87, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	p_stat(%rip), %rax
.LBB14_83:
	movl	$.L.str.90, %edi
	movl	$65, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.91, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.90, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movl	$280, %edx              # imm = 0x118
	addq	input(%rip), %rdx
	movl	$.L.str.92, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movl	frame_ctr+4(%rip), %edx
	addl	8(%rax), %edx
	movl	$.L.str.93, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.94, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movss	744(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	divss	.LCPI14_2(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.95, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movss	748(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	divss	.LCPI14_2(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.96, %esi
	movb	$1, %al
	callq	fprintf
	movq	stats(%rip), %rax
	cmpl	$0, 700(%rax)
	je	.LBB14_85
# BB#84:
	movq	p_stat(%rip), %rdi
	movss	752(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	divss	.LCPI14_2(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.97, %esi
	movb	$1, %al
	callq	fprintf
	movq	stats(%rip), %rax
.LBB14_85:
	movq	p_stat(%rip), %rdi
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	divss	.LCPI14_2(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.98, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movslq	5780(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	DistortionType(,%rax,4), %rcx
	movl	$.L.str.99, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movslq	5784(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	DistortionType(,%rax,4), %rcx
	movl	$.L.str.99, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movslq	5788(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	DistortionType(,%rax,4), %rcx
	movl	$.L.str.99, %esi
	movl	$2, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movslq	5792(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	DistortionType(,%rax,4), %rdx
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %rax
	movq	p_stat(%rip), %rcx
	cmpl	$1, 5776(%rax)
	jne	.LBB14_87
# BB#86:
	movl	$.L.str.34, %edi
	movl	$43, %esi
	jmp	.LBB14_88
.LBB14_87:
	movl	$.L.str.35, %edi
	movl	$39, %esi
.LBB14_88:
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movl	56(%rax), %edx
	movl	60(%rax), %ecx
	movl	$.L.str.100, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %rax
	cmpl	$0, 68(%rax)
	movq	p_stat(%rip), %rcx
	je	.LBB14_90
# BB#89:
	movl	$.L.str.101, %edi
	movl	$35, %esi
	jmp	.LBB14_91
.LBB14_90:
	movl	$.L.str.102, %edi
	movl	$36, %esi
.LBB14_91:
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movl	28(%rax), %edx
	movl	$.L.str.103, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movl	32(%rax), %edx
	movl	$.L.str.104, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movl	36(%rax), %edx
	testl	%edx, %edx
	jne	.LBB14_93
# BB#92:
	movl	32(%rax), %edx
.LBB14_93:
	movl	$.L.str.105, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stats(%rip), %rax
	cmpl	$0, 700(%rax)
	je	.LBB14_97
# BB#94:
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	cmpl	$0, 40(%rax)
	leaq	40(%rax), %rcx
	leaq	32(%rax), %rax
	cmoveq	%rax, %rcx
	movl	(%rcx), %edx
	movl	$.L.str.106, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movl	44(%rax), %edx
	testl	%edx, %edx
	jne	.LBB14_96
# BB#95:
	movl	32(%rax), %edx
.LBB14_96:
	movl	$.L.str.107, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB14_97:
	movq	input(%rip), %rax
	movq	p_stat(%rip), %rcx
	cmpl	$0, 4008(%rax)
	je	.LBB14_98
# BB#99:
	movl	$.L.str.109, %edi
	jmp	.LBB14_100
.LBB14_98:
	movl	$.L.str.108, %edi
.LBB14_100:
	movl	$38, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	movl	$.L.str.110, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %rax
	cmpl	$0, 4708(%rax)
	je	.LBB14_102
# BB#101:
	movq	p_stat(%rip), %rcx
	movl	$.L.str.111, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rax
.LBB14_102:
	cmpl	$3, 5244(%rax)
	jne	.LBB14_104
# BB#103:
	movq	p_stat(%rip), %rdi
	movl	$1, %esi
	callq	EPZSOutputStats
	movq	input(%rip), %rax
.LBB14_104:
	movl	4140(%rax), %eax
	cmpl	$2, %eax
	jne	.LBB14_106
# BB#105:
	movq	p_stat(%rip), %rcx
	movl	$.L.str.112, %edi
	movl	$37, %esi
	jmp	.LBB14_109
.LBB14_106:
	movq	p_stat(%rip), %rcx
	cmpl	$1, %eax
	jne	.LBB14_108
# BB#107:
	movl	$.L.str.113, %edi
	movl	$55, %esi
	jmp	.LBB14_109
.LBB14_108:
	movl	$.L.str.114, %edi
	movl	$74, %esi
.LBB14_109:
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rax
	cmpl	$0, 4168(%rax)
	movq	p_stat(%rip), %rcx
	je	.LBB14_111
# BB#110:
	movl	$.L.str.115, %edi
	movl	$37, %esi
	jmp	.LBB14_112
.LBB14_111:
	movl	$.L.str.116, %edi
	movl	$41, %esi
.LBB14_112:
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.117, %edi
	movl	$57, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.118, %edi
	movl	$57, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.117, %edi
	movl	$57, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.119, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	snr(%rip), %rax
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.120, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	snr(%rip), %rax
	movss	84(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.121, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rcx
	movl	$.L.str.122, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	snr(%rip), %rax
	movss	16(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.123, %esi
	movb	$2, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	snr(%rip), %rax
	movss	88(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	92(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.124, %esi
	movb	$2, %al
	callq	fprintf
	movq	p_stat(%rip), %rcx
	movl	$.L.str.125, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	input(%rip), %rax
	movl	12(%rax), %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	movl	$.L.str.126, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	4(%rax), %xmm0
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	(%rax), %xmm1
	cvtss2sd	%xmm1, %xmm1
	movsd	.LCPI14_0(%rip), %xmm2  # xmm2 = mem[0],zero
	maxsd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movl	$.L.str.121, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rcx
	movl	$.L.str.127, %edi
	movl	$74, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.128, %edi
	movl	$73, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.129, %edi
	movl	$73, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	snr(%rip), %rax
	movss	32(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	24(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	28(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.130, %esi
	movb	$3, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	snr(%rip), %rax
	movss	52(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	44(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	48(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.131, %esi
	movb	$3, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	snr(%rip), %rax
	movss	72(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	64(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	68(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.132, %esi
	movb	$3, %al
	callq	fprintf
	movq	p_stat(%rip), %rcx
	movl	$.L.str.133, %edi
	movl	$42, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.134, %edi
	movl	$41, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.135, %edi
	movl	$41, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	1072(%rax), %rdx
	movl	$.L.str.136, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	1104(%rax), %rdx
	movl	$.L.str.137, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	1080(%rax), %rdx
	movl	$.L.str.138, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	1112(%rax), %rdx
	movl	$.L.str.139, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rcx
	movl	$.L.str.140, %edi
	movl	$60, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.141, %edi
	movl	$59, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.142, %edi
	movl	$58, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	760(%rax), %rdx
	cvtsi2sdq	1360(%rax), %xmm0
	movq	80(%rsp), %r14
	cvtsi2sdq	%r14, %xmm1
	movsd	%xmm1, 48(%rsp)         # 8-byte Spill
	divsd	%xmm1, %xmm0
	movl	$.L.str.143, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	768(%rax), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	1368(%rax), %xmm0
	divsd	48(%rsp), %xmm0         # 8-byte Folded Reload
	movl	$.L.str.144, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	776(%rax), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	1376(%rax), %xmm0
	divsd	48(%rsp), %xmm0         # 8-byte Folded Reload
	movl	$.L.str.145, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	784(%rax), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	1384(%rax), %xmm0
	divsd	48(%rsp), %xmm0         # 8-byte Folded Reload
	movl	$.L.str.146, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	824(%rax), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	1424(%rax), %xmm0
	divsd	48(%rsp), %xmm0         # 8-byte Folded Reload
	movl	$.L.str.147, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	832(%rax), %rdx
	movl	$.L.str.148, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	864(%rax), %rdx
	movl	$.L.str.149, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	840(%rax), %rdx
	movl	$.L.str.150, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	872(%rax), %rdx
	movl	$.L.str.151, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stats(%rip), %rax
	movq	1368(%rax), %rbx
	addq	1360(%rax), %rbx
	addq	1376(%rax), %rbx
	addq	1384(%rax), %rbx
	addq	1424(%rax), %rbx
	xorpd	%xmm1, %xmm1
	cmpl	$0, 700(%rax)
	je	.LBB14_115
# BB#113:
	movl	frame_ctr+4(%rip), %eax
	testl	%eax, %eax
	je	.LBB14_115
# BB#114:
	movq	p_stat(%rip), %rcx
	movl	$.L.str.152, %edi
	movl	$61, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.153, %edi
	movl	$59, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.142, %edi
	movl	$58, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	880(%rax), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	1480(%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	frame_ctr+4(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movl	$.L.str.143, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	888(%rax), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	1488(%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	frame_ctr+4(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movl	$.L.str.144, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	896(%rax), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	1496(%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	frame_ctr+4(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movl	$.L.str.145, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	904(%rax), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	1504(%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	frame_ctr+4(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movl	$.L.str.146, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	944(%rax), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	1544(%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	frame_ctr+4(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movl	$.L.str.147, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	952(%rax), %rdx
	movl	$.L.str.148, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	984(%rax), %rdx
	movl	$.L.str.149, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	960(%rax), %rdx
	movl	$.L.str.150, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	movq	992(%rax), %rdx
	movl	$.L.str.151, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stats(%rip), %rax
	movq	1488(%rax), %rcx
	addq	1480(%rax), %rcx
	addq	1496(%rax), %rcx
	addq	1504(%rax), %rcx
	addq	1544(%rax), %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	frame_ctr+4(%rip), %xmm0
	divsd	%xmm0, %xmm1
	movq	80(%rsp), %r14
.LBB14_115:
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rbx, %xmm0
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movq	p_stat(%rip), %rcx
	movl	$.L.str.154, %edi
	movl	$77, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.155, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.156, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.157, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	2056(%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssq	112(%rsp), %xmm1
	movss	%xmm1, 40(%rsp)         # 4-byte Spill
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	2040(%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%r14, %xmm1
	movss	%xmm1, 44(%rsp)         # 4-byte Spill
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	movb	$1, %al
	callq	fprintf
	movq	stats(%rip), %rax
	movq	p_stat(%rip), %rdi
	cmpl	$0, 700(%rax)
	je	.LBB14_118
# BB#116:
	movl	frame_ctr+4(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB14_118
# BB#117:
	movq	2048(%rax), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	jmp	.LBB14_119
.LBB14_118:
	movl	$.L.str.158, %esi
	xorps	%xmm0, %xmm0
.LBB14_119:
	movb	$1, %al
	callq	fprintf
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	48(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movq	p_stat(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	p_stat(%rip), %rcx
	movl	$.L.str.159, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	2016(%rax), %xmm0
	divss	40(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	2000(%rax), %xmm0
	divss	44(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	movb	$1, %al
	callq	fprintf
	movq	stats(%rip), %rax
	movq	p_stat(%rip), %rdi
	cmpl	$0, 700(%rax)
	je	.LBB14_122
# BB#120:
	movl	frame_ctr+4(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB14_122
# BB#121:
	movq	2008(%rax), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	jmp	.LBB14_123
.LBB14_122:
	movl	$.L.str.158, %esi
	xorps	%xmm0, %xmm0
.LBB14_123:
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	p_stat(%rip), %rcx
	movl	$.L.str.160, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.161, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movl	$.L.str.158, %esi
	movb	$1, %al
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	fprintf
	movq	stats(%rip), %rax
	movq	p_stat(%rip), %rdi
	cmpl	$0, 700(%rax)
	je	.LBB14_126
# BB#124:
	movl	frame_ctr+4(%rip), %eax
	testl	%eax, %eax
	je	.LBB14_126
# BB#125:
	movl	$.L.str.158, %esi
	movb	$1, %al
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jmp	.LBB14_127
.LBB14_126:
	movl	$.L.str.158, %esi
	xorps	%xmm0, %xmm0
	movb	$1, %al
.LBB14_127:
	callq	fprintf
	movq	p_stat(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	p_stat(%rip), %rcx
	movl	$.L.str.162, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	2096(%rax), %xmm0
	divss	40(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	movq	stats(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	2080(%rax), %xmm0
	divss	44(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	movb	$1, %al
	callq	fprintf
	movq	stats(%rip), %rax
	cmpl	$0, 700(%rax)
	je	.LBB14_130
# BB#128:
	movq	96(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB14_130
# BB#129:
	movq	p_stat(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssq	2088(%rax), %xmm0
	cvtsi2ssq	%rcx, %xmm1
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	jmp	.LBB14_131
.LBB14_130:
	movq	p_stat(%rip), %rdi
	movl	$.L.str.158, %esi
	xorps	%xmm0, %xmm0
.LBB14_131:
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stats(%rip), %rax
	movq	p_stat(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssq	2136(%rax), %xmm0
	divss	40(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssq	2120(%rax), %xmm1
	divss	44(%rsp), %xmm1         # 4-byte Folded Reload
	cvtss2sd	%xmm1, %xmm1
	cmpl	$0, 700(%rax)
	je	.LBB14_134
# BB#132:
	movl	frame_ctr+4(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB14_134
# BB#133:
	movq	2128(%rax), %rax
	cvtsi2ssq	%rax, %xmm2
	cvtsi2ssl	%ecx, %xmm3
	divss	%xmm3, %xmm2
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.163, %esi
	jmp	.LBB14_135
.LBB14_134:
	movl	$.L.str.163, %esi
	xorps	%xmm2, %xmm2
.LBB14_135:
	movb	$3, %al
	callq	fprintf
	movq	stats(%rip), %rax
	movq	p_stat(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssq	2176(%rax), %xmm0
	divss	40(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssq	2160(%rax), %xmm1
	divss	44(%rsp), %xmm1         # 4-byte Folded Reload
	cvtss2sd	%xmm1, %xmm1
	cmpl	$0, 700(%rax)
	je	.LBB14_138
# BB#136:
	movl	frame_ctr+4(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB14_138
# BB#137:
	movq	2168(%rax), %rax
	xorps	%xmm2, %xmm2
	cvtsi2ssq	%rax, %xmm2
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	divss	%xmm3, %xmm2
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.164, %esi
	jmp	.LBB14_139
.LBB14_138:
	movl	$.L.str.164, %esi
	xorps	%xmm2, %xmm2
.LBB14_139:
	movb	$3, %al
	callq	fprintf
	movq	stats(%rip), %rax
	movq	p_stat(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssq	2216(%rax), %xmm0
	divss	40(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssq	2200(%rax), %xmm1
	divss	44(%rsp), %xmm1         # 4-byte Folded Reload
	cvtss2sd	%xmm1, %xmm1
	cmpl	$0, 700(%rax)
	je	.LBB14_142
# BB#140:
	movl	frame_ctr+4(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB14_142
# BB#141:
	movq	2208(%rax), %rax
	xorps	%xmm2, %xmm2
	cvtsi2ssq	%rax, %xmm2
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	divss	%xmm3, %xmm2
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.165, %esi
	jmp	.LBB14_143
.LBB14_142:
	movl	$.L.str.165, %esi
	xorps	%xmm2, %xmm2
.LBB14_143:
	movb	$3, %al
	callq	fprintf
	movq	stats(%rip), %rax
	movq	p_stat(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssq	1976(%rax), %xmm0
	divss	40(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssq	1960(%rax), %xmm1
	divss	44(%rsp), %xmm1         # 4-byte Folded Reload
	cvtss2sd	%xmm1, %xmm1
	cmpl	$0, 700(%rax)
	je	.LBB14_146
# BB#144:
	movl	frame_ctr+4(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB14_146
# BB#145:
	movq	1968(%rax), %rax
	xorps	%xmm2, %xmm2
	cvtsi2ssq	%rax, %xmm2
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	divss	%xmm3, %xmm2
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.166, %esi
	jmp	.LBB14_147
.LBB14_146:
	movl	$.L.str.166, %esi
	xorps	%xmm2, %xmm2
.LBB14_147:
	movb	$3, %al
	callq	fprintf
	movq	p_stat(%rip), %rcx
	movl	$.L.str.156, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rcx
	movl	$.L.str.167, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssq	120(%rsp), %xmm0
	divss	40(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssq	88(%rsp), %xmm0
	divss	44(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	movb	$1, %al
	callq	fprintf
	movq	stats(%rip), %rax
	movq	p_stat(%rip), %rdi
	cmpl	$0, 700(%rax)
	je	.LBB14_150
# BB#148:
	movl	frame_ctr+4(%rip), %eax
	testl	%eax, %eax
	je	.LBB14_150
# BB#149:
	movq	104(%rsp), %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rcx, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.158, %esi
	jmp	.LBB14_151
.LBB14_150:
	movl	$.L.str.158, %esi
	xorps	%xmm0, %xmm0
.LBB14_151:
	movb	$1, %al
	callq	fprintf
	movq	p_stat(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	p_stat(%rip), %rcx
	movl	$.L.str.156, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_stat(%rip), %rdi
	callq	fclose
	movl	$.L.str.168, %edi
	movl	$.L.str.6, %esi
	callq	fopen64
	movq	%rax, p_log(%rip)
	testq	%rax, %rax
	je	.LBB14_152
# BB#154:
	movq	%rax, %rdi
	callq	fclose
	movl	$.L.str.168, %edi
	movl	$.L.str.7, %esi
	callq	fopen64
	movq	%rax, p_log(%rip)
	testq	%rax, %rax
	jne	.LBB14_156
	jmp	.LBB14_155
.LBB14_152:
	movl	$.L.str.168, %edi
	movl	$.L.str.7, %esi
	callq	fopen64
	movq	%rax, p_log(%rip)
	testq	%rax, %rax
	je	.LBB14_155
# BB#153:
	movl	$.L.str.169, %edi
	movl	$297, %esi              # imm = 0x129
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	p_log(%rip), %rcx
	movl	$.L.str.170, %edi
	movl	$297, %esi              # imm = 0x129
	movl	$1, %edx
	callq	fwrite
	movq	p_log(%rip), %rcx
	movl	$.L.str.169, %edi
	movl	$297, %esi              # imm = 0x129
	movl	$1, %edx
	callq	fwrite
	movq	p_log(%rip), %rcx
	movl	$.L.str.171, %edi
	movl	$297, %esi              # imm = 0x129
	movl	$1, %edx
	callq	fwrite
	movq	p_log(%rip), %rcx
	movl	$.L.str.169, %edi
	movl	$297, %esi              # imm = 0x129
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB14_156
.LBB14_155:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.8, %edx
	movl	$.L.str.168, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB14_156:
	movq	p_log(%rip), %rdi
	xorl	%r15d, %r15d
	movl	$.L.str.172, %esi
	movl	$.L.str.15, %edx
	movl	$.L.str.16, %ecx
	xorl	%eax, %eax
	callq	fprintf
	xorl	%edi, %edi
	callq	time
	movq	%rax, 72(%rsp)
	leaq	72(%rsp), %rbx
	movq	%rbx, %rdi
	callq	time
	movq	%rbx, %rdi
	callq	localtime
	movq	%rax, %rbx
	leaq	192(%rsp), %r14
	movl	$1000, %esi             # imm = 0x3E8
	movl	$.L.str.17, %edx
	movq	%r14, %rdi
	movq	%rbx, %rcx
	callq	strftime
	movq	p_log(%rip), %rdi
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fprintf
	movl	$1000, %esi             # imm = 0x3E8
	movl	$.L.str.19, %edx
	movq	%r14, %rdi
	movq	%rbx, %rcx
	callq	strftime
	movq	p_log(%rip), %rdi
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fprintf
	movq	input(%rip), %rbp
	leaq	280(%rbp), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_157:                             # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	strlen
	addl	$-30, %eax
	cmovsl	%r15d, %eax
	addl	%ebx, %eax
	cltq
	movzbl	280(%rbp,%rax), %eax
	movb	%al, 160(%rsp,%rbx)
	incq	%rbx
	cmpq	$30, %rbx
	jne	.LBB14_157
# BB#158:
	movq	p_log(%rip), %rdi
	leaq	160(%rsp), %rdx
	movl	$.L.str.173, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	8(%rax), %edx
	movl	$.L.str.174, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	4704(%rax), %edx
	movl	4708(%rax), %ecx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	12(%rax), %edx
	movl	$.L.str.175, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	16(%rax), %edx
	movl	$.L.str.175, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	2104(%rax), %edx
	movl	$.L.str.175, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	56(%rax), %edx
	movl	60(%rax), %ecx
	movl	$.L.str.176, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	1560(%rax), %edx
	movl	$.L.str.177, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	stats(%rip), %rax
	movl	700(%rax), %edx
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %rax
	movl	5244(%rax), %eax
	cmpl	$3, %eax
	je	.LBB14_163
# BB#159:
	cmpl	$2, %eax
	je	.LBB14_162
# BB#160:
	cmpl	$1, %eax
	jne	.LBB14_164
# BB#161:
	movq	p_log(%rip), %rcx
	movl	$.L.str.178, %edi
	jmp	.LBB14_167
.LBB14_163:
	movq	p_log(%rip), %rcx
	movl	$.L.str.180, %edi
	jmp	.LBB14_167
.LBB14_162:
	movq	p_log(%rip), %rcx
	movl	$.L.str.179, %edi
	jmp	.LBB14_167
.LBB14_164:
	movq	p_log(%rip), %rcx
	testl	%eax, %eax
	je	.LBB14_165
# BB#166:
	movl	$.L.str.182, %edi
	jmp	.LBB14_167
.LBB14_165:
	movl	$.L.str.181, %edi
.LBB14_167:
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	5780(%rax), %edx
	movl	5784(%rax), %ecx
	movl	5788(%rax), %r8d
	movl	$.L.str.183, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	28(%rax), %edx
	movl	$.L.str.184, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	32(%rax), %edx
	movl	$.L.str.185, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	img(%rip), %rax
	movq	stats(%rip), %rcx
	movl	700(%rcx), %ecx
	incl	%ecx
	cvtsi2ssl	%ecx, %xmm0
	mulss	48(%rax), %xmm0
	movq	input(%rip), %rax
	movl	20(%rax), %eax
	incl	%eax
	cvtsi2ssl	%eax, %xmm1
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.186, %esi
	movb	$1, %al
	callq	fprintf
	movq	input(%rip), %rax
	movq	p_log(%rip), %rcx
	cmpl	$0, 4008(%rax)
	je	.LBB14_168
# BB#169:
	movl	$.L.str.188, %edi
	jmp	.LBB14_170
.LBB14_168:
	movl	$.L.str.187, %edi
.LBB14_170:
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	4168(%rax), %edx
	movl	$.L.str.189, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %rax
	movq	p_log(%rip), %rcx
	cmpl	$1, 68(%rax)
	jne	.LBB14_172
# BB#171:
	movl	$.L.str.190, %edi
	jmp	.LBB14_173
.LBB14_172:
	movl	$.L.str.191, %edi
.LBB14_173:
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	p_log(%rip), %rdi
	movq	input(%rip), %rax
	movl	5100(%rax), %edx
	movl	$.L.str.192, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	snr(%rip), %rax
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.193, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	snr(%rip), %rax
	movss	16(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.193, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	snr(%rip), %rax
	movss	20(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.193, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	snr(%rip), %rax
	movss	84(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.193, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	snr(%rip), %rax
	movss	88(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.193, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	snr(%rip), %rax
	movss	92(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.193, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	stats(%rip), %rax
	movss	744(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.194, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	stats(%rip), %rax
	movss	748(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.194, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	stats(%rip), %rax
	movss	752(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.194, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_log(%rip), %rdi
	movq	stats(%rip), %rax
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.195, %esi
	movb	$1, %al
	callq	fprintf
	movq	p_log(%rip), %rdi
	movl	tot_time(%rip), %edx
	movl	$.L.str.196, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rdi
	movl	me_tot_time(%rip), %edx
	movl	$.L.str.196, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	p_log(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	p_log(%rip), %rdi
	callq	fclose
	movl	$.L.str.197, %edi
	movl	$.L.str.7, %esi
	callq	fopen64
	movq	%rax, %r10
	movq	%r10, p_log(%rip)
	movq	stats(%rip), %rax
	movq	input(%rip), %rsi
	movslq	8(%rsi), %rbx
	cmpl	$0, 700(%rax)
	je	.LBB14_176
# BB#174:
	movl	frame_ctr+4(%rip), %edx
	testl	%edx, %edx
	je	.LBB14_176
# BB#175:
	movslq	%edx, %r11
	movl	12(%rsi), %ecx
	movl	16(%rsi), %r8d
	movq	snr(%rip), %rsi
	movss	12(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	16(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	20(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	84(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm6
	movss	88(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm7
	movss	92(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movq	720(%rax), %r9
	movq	736(%rax), %rsi
	movq	16(%rax), %rax
	addq	%r9, %rax
	addl	%ebx, %edx
	movslq	%edx, %rbp
	cqto
	idivq	%rbp
	movq	%rax, %rdi
	movq	%rsi, %rax
	cqto
	idivq	%r11
	cvtsi2sdq	tot_time(%rip), %xmm4
	mulsd	.LCPI14_3(%rip), %xmm4
	cvtsi2sdl	%ebp, %xmm5
	divsd	%xmm5, %xmm4
	movsd	%xmm4, 32(%rsp)
	movq	%rax, 24(%rsp)
	movq	%rdi, 16(%rsp)
	movsd	%xmm3, 8(%rsp)
	movl	$0, (%rsp)
	movl	$.L.str.198, %esi
	jmp	.LBB14_178
.LBB14_176:
	testl	%ebx, %ebx
	je	.LBB14_179
# BB#177:
	movl	12(%rsi), %ecx
	movl	16(%rsi), %r8d
	movq	snr(%rip), %rdx
	movss	12(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	16(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	20(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movq	720(%rax), %r9
	movss	84(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm6
	movss	88(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm7
	movss	92(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movq	16(%rax), %rax
	addq	%r9, %rax
	cqto
	idivq	%rbx
	cvtsi2sdq	tot_time(%rip), %xmm4
	mulsd	.LCPI14_3(%rip), %xmm4
	cvtsi2sdl	%ebx, %xmm5
	divsd	%xmm5, %xmm4
	movsd	%xmm4, 32(%rsp)
	movq	%rax, 16(%rsp)
	movsd	%xmm3, 8(%rsp)
	movl	$0, 24(%rsp)
	movl	$0, (%rsp)
	movl	$.L.str.199, %esi
.LBB14_178:
	xorpd	%xmm3, %xmm3
	xorpd	%xmm4, %xmm4
	xorpd	%xmm5, %xmm5
	movb	$8, %al
	movq	%r10, %rdi
	movl	%ebx, %edx
	callq	fprintf
.LBB14_179:
	movq	p_log(%rip), %rdi
	callq	fclose
	movq	input(%rip), %rax
	cmpl	$0, 5104(%rax)
	je	.LBB14_183
# BB#180:
	movl	$.L.str.5, %edi
	movl	$.L.str.7, %esi
	callq	fopen64
	movq	%rax, p_log(%rip)
	testq	%rax, %rax
	je	.LBB14_181
# BB#182:
	movl	$.L.str.10, %edi
	movl	$468, %esi              # imm = 0x1D4
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	p_log(%rip), %rdi
	callq	fclose
	jmp	.LBB14_183
.LBB14_181:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.8, %edx
	movl	$.L.str.9, %ecx
	xorl	%eax, %eax
	callq	snprintf
.LBB14_183:
	addq	$1192, %rsp             # imm = 0x4A8
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	report, .Lfunc_end14-report
	.cfi_endproc

	.globl	free_picture
	.p2align	4, 0x90
	.type	free_picture,@function
free_picture:                           # @free_picture
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 16
.Lcfi51:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB15_1
# BB#2:
	movq	%rbx, %rdi
	callq	free_slice_list
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB15_1:
	popq	%rbx
	retq
.Lfunc_end15:
	.size	free_picture, .Lfunc_end15-free_picture
	.cfi_endproc

	.globl	free_global_buffers
	.p2align	4, 0x90
	.type	free_global_buffers,@function
free_global_buffers:                    # @free_global_buffers
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -24
.Lcfi56:
	.cfi_offset %r14, -16
	movq	last_P_no_frm(%rip), %rdi
	callq	free
	movq	last_P_no_fld(%rip), %rdi
	callq	free
	callq	free_orig_planes
	movq	PicPos(%rip), %rdi
	callq	free_mem2Dint
	callq	free_QMatrix
	callq	free_QOffsets
	movq	input(%rip), %rax
	cmpl	$0, 2928(%rax)
	jne	.LBB16_3
# BB#1:
	cmpl	$0, 2932(%rax)
	jne	.LBB16_3
# BB#2:
	cmpl	$0, 1576(%rax)
	je	.LBB16_4
.LBB16_3:
	movq	wp_weight(%rip), %rdi
	movl	$6, %esi
	callq	free_mem3Dint
	movq	wp_offset(%rip), %rdi
	movl	$6, %esi
	callq	free_mem3Dint
	movq	wbp_weight(%rip), %rdi
	movl	$6, %esi
	movl	$32, %edx
	callq	free_mem4Dint
.LBB16_4:
	movq	stats(%rip), %rax
	cmpl	$0, 700(%rax)
	jne	.LBB16_6
# BB#5:
	movq	input(%rip), %rax
	cmpl	$0, 2964(%rax)
	jle	.LBB16_7
.LBB16_6:
	movq	direct_ref_idx(%rip), %rdi
	movl	$2, %esi
	callq	free_mem3D
	movq	direct_pdir(%rip), %rdi
	callq	free_mem2D
.LBB16_7:
	movq	imgY_sub_tmp(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB16_9
# BB#8:
	callq	free_mem2Dint
	movq	$0, imgY_sub_tmp(%rip)
.LBB16_9:
	movq	img(%rip), %rax
	movq	128(%rax), %rdi
	callq	free_mem2D
	movq	img(%rip), %rax
	movq	136(%rax), %rdi
	callq	free_mem2D
	movq	img(%rip), %rax
	movq	14224(%rax), %rdi
	callq	free
	movq	rddata_top_frame_mb+1600(%rip), %rdi
	callq	free_mem2D
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	je	.LBB16_11
# BB#10:
	movq	img(%rip), %rax
	movq	14240(%rax), %rdi
	callq	free
	movq	input(%rip), %rax
.LBB16_11:
	cmpl	$1, 4172(%rax)
	jne	.LBB16_13
# BB#12:
	movq	mb16x16_cost_frame(%rip), %rdi
	callq	free
	movq	input(%rip), %rax
.LBB16_13:
	cmpl	$3, 4168(%rax)
	jne	.LBB16_21
# BB#14:
	movq	decs(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	32(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	32(%rax), %rdi
	callq	free
	movq	input(%rip), %rax
	movq	decs(%rip), %rcx
	movq	8(%rcx), %rdi
	cmpl	$0, 4728(%rax)
	jle	.LBB16_20
# BB#15:                                # %.lr.ph20.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB16_16:                              # %.lr.ph20
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_18 Depth 2
	movq	(%rdi,%r14,8), %rax
	movq	(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r14,8), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	24(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	24(%rax), %rax
	movq	(%rax,%r14,8), %rdi
	callq	free
	movq	img(%rip), %rax
	cmpl	$0, 32(%rax)
	movq	decs(%rip), %rax
	movq	16(%rax), %rax
	movq	(%rax,%r14,8), %rdi
	js	.LBB16_19
# BB#17:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB16_16 Depth=1
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB16_18:                              # %.lr.ph
                                        #   Parent Loop BB16_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdi,%rbx,8), %rax
	movq	(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	16(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	8(%rax,%rbx,8), %rdi
	callq	free
	movq	img(%rip), %rax
	movslq	32(%rax), %rax
	movq	decs(%rip), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx,%r14,8), %rdi
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB16_18
.LBB16_19:                              # %._crit_edge
                                        #   in Loop: Header=BB16_16 Depth=1
	callq	free
	incq	%r14
	movq	input(%rip), %rax
	movslq	4728(%rax), %rax
	movq	decs(%rip), %rcx
	movq	8(%rcx), %rdi
	cmpq	%rax, %r14
	jl	.LBB16_16
.LBB16_20:                              # %._crit_edge21
	callq	free
	movq	decs(%rip), %rax
	movq	24(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	40(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	40(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	48(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	decs(%rip), %rax
	movq	48(%rax), %rdi
	callq	free
	movq	input(%rip), %rax
.LBB16_21:
	cmpl	$0, 4732(%rax)
	je	.LBB16_23
# BB#22:
	movq	pixel_map(%rip), %rax
	movq	(%rax), %rdi
	callq	free
	movq	pixel_map(%rip), %rdi
	callq	free
	movq	refresh_map(%rip), %rax
	movq	(%rax), %rdi
	callq	free
	movq	refresh_map(%rip), %rdi
	callq	free
.LBB16_23:
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	jne	.LBB16_26
# BB#24:
	movq	imgY_com(%rip), %rdi
	callq	free_mem2Dpel
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB16_26
# BB#25:
	movq	imgUV_com(%rip), %rdi
	movl	$2, %esi
	callq	free_mem3Dpel
.LBB16_26:
	movq	img(%rip), %rax
	movq	152(%rax), %rdi
	movl	15352(%rax), %esi
	callq	free_mem3Dint
	movq	img(%rip), %rax
	movq	15480(%rax), %rdi
	movl	15452(%rax), %esi
	callq	free_mem2Ddb_offset
	movq	img(%rip), %rax
	movq	15488(%rax), %rdi
	movl	15452(%rax), %ecx
	leal	52(%rcx), %edx
	movl	$10, %esi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	free_mem3Ddb_offset
	movq	img(%rip), %rax
	movq	15496(%rax), %rdi
	movl	15452(%rax), %ecx
	leal	52(%rcx), %edx
	movl	$10, %esi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	free_mem3Dint_offset
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB16_28
# BB#27:
	movq	img(%rip), %rax
	movq	15504(%rax), %rdi
	movl	15452(%rax), %esi
	callq	free_mem2Ddb_offset
	movq	input(%rip), %rax
.LBB16_28:
	movl	5244(%rax), %eax
	cmpl	$3, %eax
	je	.LBB16_33
# BB#29:
	cmpl	$2, %eax
	je	.LBB16_32
# BB#30:
	cmpl	$1, %eax
	jne	.LBB16_34
# BB#31:
	callq	UMHEX_free_mem
	jmp	.LBB16_34
.LBB16_33:
	callq	EPZSDelete
	jmp	.LBB16_34
.LBB16_32:
	callq	smpUMHEX_free_mem
.LBB16_34:
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB16_38
# BB#35:
	movl	$generic_RC, %edi
	callq	generic_free
	movl	$quadratic_RC, %edi
	callq	rc_free
	movq	input(%rip), %rax
	cmpl	$0, 2940(%rax)
	jne	.LBB16_37
# BB#36:
	cmpl	$2, 4708(%rax)
	jne	.LBB16_38
.LBB16_37:
	movl	$generic_RC_init, %edi
	callq	generic_free
	movl	$quadratic_RC_init, %edi
	callq	rc_free
	movl	$generic_RC_best, %edi
	callq	generic_free
	movl	$quadratic_RC_best, %edi
	callq	rc_free
	movq	input(%rip), %rax
.LBB16_38:
	cmpl	$0, 5084(%rax)
	je	.LBB16_39
# BB#40:
	movq	imgY_tmp(%rip), %rdi
	callq	free_mem2Dpel
	movq	imgUV_tmp(%rip), %rdi
	callq	free_mem2Dpel
	movq	imgUV_tmp+8(%rip), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free_mem2Dpel           # TAILCALL
.LBB16_39:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end16:
	.size	free_global_buffers, .Lfunc_end16-free_global_buffers
	.cfi_endproc

	.globl	free_img
	.p2align	4, 0x90
	.type	free_img,@function
free_img:                               # @free_img
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 16
	callq	CloseSEIMessages
	movq	img(%rip), %rax
	movq	14376(%rax), %rdi
	callq	free_mem_mv
	movq	img(%rip), %rax
	movq	14384(%rax), %rdi
	callq	free_mem_mv
	movq	input(%rip), %rax
	cmpl	$0, 2120(%rax)
	je	.LBB17_2
# BB#1:
	movq	img(%rip), %rax
	movq	14392(%rax), %rdi
	callq	free_mem_mv
	movq	img(%rip), %rax
	movq	14400(%rax), %rdi
	callq	free_mem_mv
.LBB17_2:
	movq	img(%rip), %rax
	movq	14160(%rax), %rdi
	callq	free_mem_ACcoeff
	movq	img(%rip), %rax
	movq	14168(%rax), %rdi
	callq	free_mem_DCcoeff
	movq	input(%rip), %rax
	cmpl	$0, 5652(%rax)
	je	.LBB17_4
# BB#3:
	movq	img(%rip), %rax
	movq	14176(%rax), %rdi
	movl	$4, %esi
	callq	free_mem3Dint
	movq	img(%rip), %rax
	movq	14184(%rax), %rdi
	movl	$3, %esi
	callq	free_mem3Dint
	movq	img(%rip), %rax
	movq	14192(%rax), %rdi
	movl	$4, %esi
	movl	$2, %edx
	callq	free_mem4Dint
	movq	img(%rip), %rax
	movq	14200(%rax), %rdi
	movl	$1, %esi
	movl	$2, %edx
	callq	free_mem4Dint
	movq	input(%rip), %rax
.LBB17_4:
	cmpl	$0, 4708(%rax)
	je	.LBB17_7
# BB#5:
	movq	rddata_top_frame_mb+1664(%rip), %rdi
	callq	free_mem_mv
	movq	rddata_top_frame_mb+1672(%rip), %rdi
	callq	free_mem_mv
	movq	rddata_bot_frame_mb+1664(%rip), %rdi
	callq	free_mem_mv
	movq	rddata_bot_frame_mb+1672(%rip), %rdi
	callq	free_mem_mv
	movq	rddata_top_frame_mb+1544(%rip), %rdi
	callq	free_mem_ACcoeff
	movq	rddata_top_frame_mb+1552(%rip), %rdi
	callq	free_mem_DCcoeff
	movq	rddata_bot_frame_mb+1544(%rip), %rdi
	callq	free_mem_ACcoeff
	movq	rddata_bot_frame_mb+1552(%rip), %rdi
	callq	free_mem_DCcoeff
	movq	input(%rip), %rax
	cmpl	$3, 4708(%rax)
	je	.LBB17_7
# BB#6:
	movq	rddata_top_field_mb+1664(%rip), %rdi
	callq	free_mem_mv
	movq	rddata_top_field_mb+1672(%rip), %rdi
	callq	free_mem_mv
	movq	rddata_bot_field_mb+1664(%rip), %rdi
	callq	free_mem_mv
	movq	rddata_bot_field_mb+1672(%rip), %rdi
	callq	free_mem_mv
	movq	rddata_top_field_mb+1544(%rip), %rdi
	callq	free_mem_ACcoeff
	movq	rddata_top_field_mb+1552(%rip), %rdi
	callq	free_mem_DCcoeff
	movq	rddata_bot_field_mb+1544(%rip), %rdi
	callq	free_mem_ACcoeff
	movq	rddata_bot_field_mb+1552(%rip), %rdi
	callq	free_mem_DCcoeff
.LBB17_7:
	movq	img(%rip), %rdx
	movslq	15520(%rdx), %rax
	movslq	15524(%rdx), %rcx
	movq	14232(%rdx), %rdi
	cmpl	%ecx, %eax
	jle	.LBB17_9
# BB#8:
	leaq	4(,%rax,4), %rax
	jmp	.LBB17_10
.LBB17_9:
	leaq	4(,%rcx,4), %rax
.LBB17_10:
	subq	%rax, %rdi
	callq	free
	movq	input(%rip), %rax
	cmpl	$0, 4708(%rax)
	je	.LBB17_11
# BB#12:
	movq	rddata_bot_frame_mb+1600(%rip), %rdi
	callq	free_mem2D
	movq	rddata_top_field_mb+1600(%rip), %rdi
	callq	free_mem2D
	movq	rddata_bot_field_mb+1600(%rip), %rdi
	popq	%rax
	jmp	free_mem2D              # TAILCALL
.LBB17_11:
	popq	%rax
	retq
.Lfunc_end17:
	.size	free_img, .Lfunc_end17-free_img
	.cfi_endproc

	.globl	report_stats_on_error
	.p2align	4, 0x90
	.type	report_stats_on_error,@function
report_stats_on_error:                  # @report_stats_on_error
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 16
.Lcfi59:
	.cfi_offset %rbx, -16
	movq	img(%rip), %rax
	movl	(%rax), %eax
	movq	input(%rip), %rcx
	movl	%eax, 8(%rcx)
	callq	terminate_sequence
	callq	flush_dpb
	movl	p_in(%rip), %edi
	callq	close
	movl	p_dec(%rip), %edi
	cmpl	$-1, %edi
	je	.LBB18_2
# BB#1:
	callq	close
.LBB18_2:
	movq	p_trace(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB18_4
# BB#3:
	callq	fclose
.LBB18_4:
	callq	Clear_Motion_Search_Module
	callq	RandomIntraUninit
	callq	FmoUninit
	movq	input(%rip), %rax
	cmpl	$0, 2968(%rax)
	je	.LBB18_6
# BB#5:
	callq	clear_gop_structure
.LBB18_6:
	callq	clear_rdopt
	xorl	%eax, %eax
	callq	calc_buffer
	movq	input(%rip), %rax
	cmpl	$0, 5104(%rax)
	je	.LBB18_8
# BB#7:
	callq	report_frame_statistic
.LBB18_8:
	callq	report
	movq	frame_pic_1(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB18_10
# BB#9:
	movq	%rbx, %rdi
	callq	free_slice_list
	movq	%rbx, %rdi
	callq	free
.LBB18_10:                              # %free_picture.exit
	movq	top_pic(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB18_12
# BB#11:                                # %free_picture.exit1
	movq	%rbx, %rdi
	callq	free_slice_list
	movq	%rbx, %rdi
	callq	free
.LBB18_12:
	movq	bottom_pic(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB18_14
# BB#13:                                # %free_picture.exit2
	movq	%rbx, %rdi
	callq	free_slice_list
	movq	%rbx, %rdi
	callq	free
.LBB18_14:
	callq	free_dpb
	movq	Co_located(%rip), %rdi
	callq	free_colocated
	callq	uninit_out_buffer
	callq	free_global_buffers
	callq	free_img
	callq	free_context_memory
	callq	FreeNalPayloadBuffer
	popq	%rbx
	jmp	FreeParameterSets       # TAILCALL
.Lfunc_end18:
	.size	report_stats_on_error, .Lfunc_end18-report_stats_on_error
	.cfi_endproc

	.globl	CAVLC_init
	.p2align	4, 0x90
	.type	CAVLC_init,@function
CAVLC_init:                             # @CAVLC_init
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	cmpl	$0, 15348(%rax)
	je	.LBB19_15
# BB#1:                                 # %.preheader16.lr.ph
	movl	15528(%rax), %r9d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB19_2:                               # %.preheader16
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_4 Depth 2
                                        #     Child Loop BB19_7 Depth 2
                                        #     Child Loop BB19_10 Depth 2
                                        #     Child Loop BB19_13 Depth 2
	movl	%r9d, %ecx
	movl	%r8d, %r8d
	cmpl	$-4, %ecx
	movl	$-4, %r9d
	je	.LBB19_14
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	152(%rax), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB19_4:                               #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %ecx
	movl	$0, (%rsi,%rcx,4)
	incl	%edi
	movl	15528(%rax), %ecx
	leal	4(%rcx), %edx
	cmpl	%edx, %edi
	jb	.LBB19_4
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB19_2 Depth=1
	cmpl	$-4, %ecx
	je	.LBB19_14
# BB#6:                                 # %.lr.ph.1
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	152(%rax), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	8(%rcx), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB19_7:                               #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %ecx
	movl	$0, (%rsi,%rcx,4)
	incl	%edi
	movl	15528(%rax), %ecx
	leal	4(%rcx), %edx
	cmpl	%edx, %edi
	jb	.LBB19_7
# BB#8:                                 # %._crit_edge.1
                                        #   in Loop: Header=BB19_2 Depth=1
	cmpl	$-4, %ecx
	je	.LBB19_14
# BB#9:                                 # %.lr.ph.2
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	152(%rax), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	16(%rcx), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB19_10:                              #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %ecx
	movl	$0, (%rsi,%rcx,4)
	incl	%edi
	movl	15528(%rax), %ecx
	leal	4(%rcx), %edx
	cmpl	%edx, %edi
	jb	.LBB19_10
# BB#11:                                # %._crit_edge.2
                                        #   in Loop: Header=BB19_2 Depth=1
	cmpl	$-4, %ecx
	je	.LBB19_14
# BB#12:                                # %.lr.ph.3
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	152(%rax), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	24(%rcx), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB19_13:                              #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %ecx
	movl	$0, (%rsi,%rcx,4)
	incl	%edi
	movl	15528(%rax), %r9d
	leal	4(%r9), %ecx
	cmpl	%ecx, %edi
	jb	.LBB19_13
	.p2align	4, 0x90
.LBB19_14:                              # %._crit_edge.3
                                        #   in Loop: Header=BB19_2 Depth=1
	incl	%r8d
	cmpl	15348(%rax), %r8d
	jb	.LBB19_2
.LBB19_15:                              # %._crit_edge20
	retq
.Lfunc_end19:
	.size	CAVLC_init, .Lfunc_end19-CAVLC_init
	.cfi_endproc

	.globl	get_mem_mv
	.p2align	4, 0x90
	.type	get_mem_mv,@function
get_mem_mv:                             # @get_mem_mv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -48
.Lcfi66:
	.cfi_offset %r12, -40
.Lcfi67:
	.cfi_offset %r13, -32
.Lcfi68:
	.cfi_offset %r14, -24
.Lcfi69:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$4, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.LBB20_2
# BB#1:
	movl	$.L.str.204, %edi
	callq	no_mem_exit
.LBB20_2:                               # %.preheader57.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB20_3:                               # %.preheader57
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_6 Depth 2
                                        #       Child Loop BB20_9 Depth 3
                                        #         Child Loop BB20_13 Depth 4
	movl	$4, %edi
	movl	$8, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	%rax, (%rcx,%r15,8)
	testq	%rax, %rax
	jne	.LBB20_5
# BB#4:                                 #   in Loop: Header=BB20_3 Depth=1
	movl	$.L.str.204, %edi
	callq	no_mem_exit
.LBB20_5:                               # %.preheader56.preheader
                                        #   in Loop: Header=BB20_3 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB20_6:                               # %.preheader56
                                        #   Parent Loop BB20_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB20_9 Depth 3
                                        #         Child Loop BB20_13 Depth 4
	movl	$2, %edi
	movl	$8, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	%rax, (%rcx,%r12,8)
	testq	%rax, %rax
	jne	.LBB20_8
# BB#7:                                 #   in Loop: Header=BB20_6 Depth=2
	movl	$.L.str.204, %edi
	callq	no_mem_exit
.LBB20_8:                               # %.preheader55.preheader
                                        #   in Loop: Header=BB20_6 Depth=2
	movq	img(%rip), %rax
	movl	32(%rax), %eax
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB20_9:                               # %.preheader55
                                        #   Parent Loop BB20_3 Depth=1
                                        #     Parent Loop BB20_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB20_13 Depth 4
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	%rax, (%rcx,%r13,8)
	testq	%rax, %rax
	jne	.LBB20_11
# BB#10:                                #   in Loop: Header=BB20_9 Depth=3
	movl	$.L.str.204, %edi
	callq	no_mem_exit
.LBB20_11:                              # %.preheader54
                                        #   in Loop: Header=BB20_9 Depth=3
	movq	img(%rip), %rax
	movl	32(%rax), %eax
	testl	%eax, %eax
	jle	.LBB20_18
# BB#12:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB20_9 Depth=3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_13:                              # %.lr.ph
                                        #   Parent Loop BB20_3 Depth=1
                                        #     Parent Loop BB20_6 Depth=2
                                        #       Parent Loop BB20_9 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	$9, %edi
	movl	$8, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	%rax, (%rcx,%rbx,8)
	testq	%rax, %rax
	jne	.LBB20_15
# BB#14:                                #   in Loop: Header=BB20_13 Depth=4
	movl	$.L.str.204, %edi
	callq	no_mem_exit
.LBB20_15:                              #   in Loop: Header=BB20_13 Depth=4
	movl	$18, %edi
	movl	$2, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB20_17
# BB#16:                                #   in Loop: Header=BB20_13 Depth=4
	movl	$.L.str.204, %edi
	callq	no_mem_exit
.LBB20_17:                              # %.preheader.preheader
                                        #   in Loop: Header=BB20_13 Depth=4
	movq	(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, 8(%rax)
	movq	(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, 16(%rax)
	movq	(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	16(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, 24(%rax)
	movq	(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, 32(%rax)
	movq	(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	32(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, 40(%rax)
	movq	(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	40(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, 48(%rax)
	movq	(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	48(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, 56(%rax)
	movq	(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	56(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, 64(%rax)
	incq	%rbx
	movq	img(%rip), %rax
	movslq	32(%rax), %rax
	cmpq	%rax, %rbx
	jl	.LBB20_13
.LBB20_18:                              # %._crit_edge
                                        #   in Loop: Header=BB20_9 Depth=3
	incq	%r13
	cmpq	$2, %r13
	jne	.LBB20_9
# BB#19:                                #   in Loop: Header=BB20_6 Depth=2
	incq	%r12
	cmpq	$4, %r12
	jne	.LBB20_6
# BB#20:                                #   in Loop: Header=BB20_3 Depth=1
	incq	%r15
	cmpq	$4, %r15
	jne	.LBB20_3
# BB#21:
	shll	$6, %eax
	leal	(%rax,%rax,8), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	get_mem_mv, .Lfunc_end20-get_mem_mv
	.cfi_endproc

	.globl	get_mem_ACcoeff
	.p2align	4, 0x90
	.type	get_mem_ACcoeff,@function
get_mem_ACcoeff:                        # @get_mem_ACcoeff
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 32
.Lcfi73:
	.cfi_offset %rbx, -32
.Lcfi74:
	.cfi_offset %r14, -24
.Lcfi75:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	img(%rip), %rax
	movslq	15528(%rax), %rbx
	movq	%rbx, %r14
	addq	$4, %r14
	movl	$8, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.LBB21_2
# BB#1:
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_2:                               # %.preheader28
	cmpl	$-3, %ebx
	jl	.LBB21_31
# BB#3:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB21_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$4, %edi
	movl	$8, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	%rax, (%rcx,%rbx,8)
	testq	%rax, %rax
	jne	.LBB21_6
# BB#5:                                 #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_6:                               # %.preheader27
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$2, %edi
	movl	$8, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB21_8
# BB#7:                                 #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_8:                               # %.preheader.preheader
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB21_10
# BB#9:                                 #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_10:                              # %.preheader.132
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB21_12
# BB#11:                                #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_12:                              # %.preheader27.135
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$2, %edi
	movl	$8, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB21_14
# BB#13:                                #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_14:                              # %.preheader.preheader.1
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	8(%rcx), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB21_16
# BB#15:                                #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_16:                              # %.preheader.132.1
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	8(%rcx), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB21_18
# BB#17:                                #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_18:                              # %.preheader27.236
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$2, %edi
	movl	$8, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	%rax, 16(%rcx)
	testq	%rax, %rax
	jne	.LBB21_20
# BB#19:                                #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_20:                              # %.preheader.preheader.2
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	16(%rcx), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB21_22
# BB#21:                                #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_22:                              # %.preheader.132.2
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	16(%rcx), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB21_24
# BB#23:                                #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_24:                              # %.preheader27.337
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$2, %edi
	movl	$8, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	%rax, 24(%rcx)
	testq	%rax, %rax
	jne	.LBB21_26
# BB#25:                                #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_26:                              # %.preheader.preheader.3
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	24(%rcx), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB21_28
# BB#27:                                #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_28:                              # %.preheader.132.3
                                        #   in Loop: Header=BB21_4 Depth=1
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r15), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	24(%rcx), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB21_30
# BB#29:                                #   in Loop: Header=BB21_4 Depth=1
	movl	$.L.str.205, %edi
	callq	no_mem_exit
.LBB21_30:                              #   in Loop: Header=BB21_4 Depth=1
	incq	%rbx
	cmpq	%r14, %rbx
	jl	.LBB21_4
.LBB21_31:                              # %._crit_edge
	imull	$2080, %r14d, %eax      # imm = 0x820
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	get_mem_ACcoeff, .Lfunc_end21-get_mem_ACcoeff
	.cfi_endproc

	.globl	get_mem_DCcoeff
	.p2align	4, 0x90
	.type	get_mem_DCcoeff,@function
get_mem_DCcoeff:                        # @get_mem_DCcoeff
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 32
.Lcfi79:
	.cfi_offset %rbx, -24
.Lcfi80:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$3, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, (%r14)
	testq	%rbx, %rbx
	jne	.LBB22_2
# BB#1:
	movl	$.L.str.206, %edi
	callq	no_mem_exit
	movq	(%r14), %rbx
.LBB22_2:                               # %.preheader13.preheader
	movl	$2, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.LBB22_4
# BB#3:
	movl	$.L.str.206, %edi
	callq	no_mem_exit
.LBB22_4:                               # %.preheader.preheader
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	(%rcx), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB22_6
# BB#5:
	movl	$.L.str.206, %edi
	callq	no_mem_exit
.LBB22_6:                               # %.preheader.116
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	(%rcx), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB22_8
# BB#7:
	movl	$.L.str.206, %edi
	callq	no_mem_exit
.LBB22_8:                               # %.preheader13.117
	movl	$2, %edi
	movl	$8, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB22_10
# BB#9:
	movl	$.L.str.206, %edi
	callq	no_mem_exit
.LBB22_10:                              # %.preheader.preheader.1
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB22_12
# BB#11:
	movl	$.L.str.206, %edi
	callq	no_mem_exit
.LBB22_12:                              # %.preheader.116.1
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB22_14
# BB#13:
	movl	$.L.str.206, %edi
	callq	no_mem_exit
.LBB22_14:                              # %.preheader13.218
	movl	$2, %edi
	movl	$8, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	%rax, 16(%rcx)
	testq	%rax, %rax
	jne	.LBB22_16
# BB#15:
	movl	$.L.str.206, %edi
	callq	no_mem_exit
.LBB22_16:                              # %.preheader.preheader.2
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	16(%rcx), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB22_18
# BB#17:
	movl	$.L.str.206, %edi
	callq	no_mem_exit
.LBB22_18:                              # %.preheader.116.2
	movl	$65, %edi
	movl	$4, %esi
	callq	calloc
	movq	(%r14), %rcx
	movq	16(%rcx), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB22_20
# BB#19:
	movl	$.L.str.206, %edi
	callq	no_mem_exit
.LBB22_20:
	movl	$1560, %eax             # imm = 0x618
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end22:
	.size	get_mem_DCcoeff, .Lfunc_end22-get_mem_DCcoeff
	.cfi_endproc

	.globl	free_mem_mv
	.p2align	4, 0x90
	.type	free_mem_mv,@function
free_mem_mv:                            # @free_mem_mv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 48
.Lcfi86:
	.cfi_offset %rbx, -40
.Lcfi87:
	.cfi_offset %r12, -32
.Lcfi88:
	.cfi_offset %r14, -24
.Lcfi89:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB23_1:                               # %.preheader34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_2 Depth 2
                                        #       Child Loop BB23_4 Depth 3
                                        #       Child Loop BB23_7 Depth 3
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB23_2:                               # %.preheader33
                                        #   Parent Loop BB23_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB23_4 Depth 3
                                        #       Child Loop BB23_7 Depth 3
	movq	img(%rip), %rax
	movq	(%r14,%r15,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx), %rdi
	cmpl	$0, 32(%rax)
	jle	.LBB23_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB23_2 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB23_4:                               # %.lr.ph
                                        #   Parent Loop BB23_1 Depth=1
                                        #     Parent Loop BB23_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdi,%rbx,8), %rax
	movq	(%rax), %rdi
	callq	free
	movq	(%r14,%r15,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	free
	incq	%rbx
	movq	img(%rip), %rax
	movslq	32(%rax), %rax
	movq	(%r14,%r15,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx), %rdi
	cmpq	%rax, %rbx
	jl	.LBB23_4
.LBB23_5:                               # %._crit_edge
                                        #   in Loop: Header=BB23_2 Depth=2
	callq	free
	movq	img(%rip), %rax
	movq	(%r14,%r15,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	8(%rcx), %rdi
	cmpl	$0, 32(%rax)
	jle	.LBB23_8
# BB#6:                                 # %.lr.ph.1.preheader
                                        #   in Loop: Header=BB23_2 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB23_7:                               # %.lr.ph.1
                                        #   Parent Loop BB23_1 Depth=1
                                        #     Parent Loop BB23_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdi,%rbx,8), %rax
	movq	(%rax), %rdi
	callq	free
	movq	(%r14,%r15,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	free
	incq	%rbx
	movq	img(%rip), %rax
	movslq	32(%rax), %rax
	movq	(%r14,%r15,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	8(%rcx), %rdi
	cmpq	%rax, %rbx
	jl	.LBB23_7
.LBB23_8:                               # %._crit_edge.1
                                        #   in Loop: Header=BB23_2 Depth=2
	callq	free
	movq	(%r14,%r15,8), %rax
	movq	(%rax,%r12,8), %rdi
	callq	free
	incq	%r12
	cmpq	$4, %r12
	jne	.LBB23_2
# BB#9:                                 #   in Loop: Header=BB23_1 Depth=1
	movq	(%r14,%r15,8), %rdi
	callq	free
	incq	%r15
	cmpq	$4, %r15
	jne	.LBB23_1
# BB#10:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end23:
	.size	free_mem_mv, .Lfunc_end23-free_mem_mv
	.cfi_endproc

	.globl	free_mem_ACcoeff
	.p2align	4, 0x90
	.type	free_mem_ACcoeff,@function
free_mem_ACcoeff:                       # @free_mem_ACcoeff
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 32
.Lcfi93:
	.cfi_offset %rbx, -24
.Lcfi94:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	img(%rip), %rax
	cmpl	$-3, 15528(%rax)
	jl	.LBB24_3
# BB#1:                                 # %.preheader19.preheader
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB24_2:                               # %.preheader19
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r14,%rbx,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	free
	movq	8(%r14,%rbx,8), %rdi
	callq	free
	movq	img(%rip), %rax
	movslq	15528(%rax), %rax
	addq	$3, %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB24_2
.LBB24_3:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end24:
	.size	free_mem_ACcoeff, .Lfunc_end24-free_mem_ACcoeff
	.cfi_endproc

	.globl	free_mem_DCcoeff
	.p2align	4, 0x90
	.type	free_mem_DCcoeff,@function
free_mem_DCcoeff:                       # @free_mem_DCcoeff
	.cfi_startproc
# BB#0:                                 # %.preheader
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 16
.Lcfi96:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	callq	free
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	(%rbx), %rdi
	callq	free
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	callq	free
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%rbx), %rdi
	callq	free
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	callq	free
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end25:
	.size	free_mem_DCcoeff, .Lfunc_end25-free_mem_DCcoeff
	.cfi_endproc

	.globl	init_orig_buffers
	.p2align	4, 0x90
	.type	init_orig_buffers,@function
init_orig_buffers:                      # @init_orig_buffers
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 16
.Lcfi98:
	.cfi_offset %rbx, -16
	movq	img(%rip), %rax
	movl	52(%rax), %edx
	movl	68(%rax), %esi
	movl	$imgY_org_frm, %edi
	callq	get_mem2Dpel
	movl	%eax, %ebx
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB26_2
# BB#1:
	movl	64(%rax), %ecx
	movl	80(%rax), %edx
	movl	$imgUV_org_frm, %edi
	movl	$2, %esi
	callq	get_mem3Dpel
	addl	%ebx, %eax
	movl	%eax, %ebx
.LBB26_2:
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	jne	.LBB26_9
# BB#3:
	movq	imgY_org_frm(%rip), %rdi
	movq	img(%rip), %rax
	movl	52(%rax), %edx
	movl	68(%rax), %esi
	movl	$imgY_org_top, %ecx
	movl	$imgY_org_bot, %r8d
	callq	init_top_bot_planes
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB26_9
# BB#4:
	movl	$2, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, imgUV_org_top(%rip)
	testq	%rax, %rax
	jne	.LBB26_6
# BB#5:
	movl	$.L.str.200, %edi
	callq	no_mem_exit
.LBB26_6:
	movl	$2, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, imgUV_org_bot(%rip)
	testq	%rax, %rax
	jne	.LBB26_8
# BB#7:
	movl	$.L.str.201, %edi
	callq	no_mem_exit
	movq	imgUV_org_bot(%rip), %rax
.LBB26_8:
	movq	imgUV_org_frm(%rip), %rcx
	movq	(%rcx), %rdi
	movq	img(%rip), %rcx
	movl	64(%rcx), %edx
	movl	80(%rcx), %esi
	movq	imgUV_org_top(%rip), %rcx
	movq	%rax, %r8
	callq	init_top_bot_planes
	addl	%eax, %ebx
	movq	imgUV_org_frm(%rip), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movl	64(%rax), %edx
	movl	80(%rax), %esi
	movq	imgUV_org_top(%rip), %rcx
	addq	$8, %rcx
	movq	imgUV_org_bot(%rip), %r8
	addq	$8, %r8
	callq	init_top_bot_planes
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	32(%rax,%rbx), %ebx
.LBB26_9:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end26:
	.size	init_orig_buffers, .Lfunc_end26-init_orig_buffers
	.cfi_endproc

	.globl	free_orig_planes
	.p2align	4, 0x90
	.type	free_orig_planes,@function
free_orig_planes:                       # @free_orig_planes
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 16
	movq	imgY_org_frm(%rip), %rdi
	callq	free_mem2Dpel
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB27_2
# BB#1:
	movq	imgUV_org_frm(%rip), %rdi
	movl	$2, %esi
	callq	free_mem3Dpel
.LBB27_2:
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	je	.LBB27_3
.LBB27_4:
	popq	%rax
	retq
.LBB27_3:
	movq	imgY_org_top(%rip), %rdi
	movq	imgY_org_bot(%rip), %rsi
	callq	free_top_bot_planes
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB27_4
# BB#5:
	movq	imgUV_org_top(%rip), %rax
	movq	(%rax), %rdi
	movq	imgUV_org_bot(%rip), %rax
	movq	(%rax), %rsi
	callq	free_top_bot_planes
	movq	imgUV_org_top(%rip), %rax
	movq	8(%rax), %rdi
	movq	imgUV_org_bot(%rip), %rax
	movq	8(%rax), %rsi
	callq	free_top_bot_planes
	movq	imgUV_org_top(%rip), %rdi
	callq	free
	movq	imgUV_org_bot(%rip), %rdi
	popq	%rax
	jmp	free                    # TAILCALL
.Lfunc_end27:
	.size	free_orig_planes, .Lfunc_end27-free_orig_planes
	.cfi_endproc

	.globl	combine_field
	.p2align	4, 0x90
	.type	combine_field,@function
combine_field:                          # @combine_field
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 32
.Lcfi103:
	.cfi_offset %rbx, -24
.Lcfi104:
	.cfi_offset %r14, -16
	movq	img(%rip), %rax
	cmpl	$2, 68(%rax)
	jl	.LBB28_3
# BB#1:                                 # %.lr.ph19.preheader
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB28_2:                               # %.lr.ph19
                                        # =>This Inner Loop Header: Depth=1
	movq	imgY_com(%rip), %rcx
	movq	(%rcx,%rbx,2), %rdi
	movq	enc_top_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	(%rcx,%rbx), %rsi
	movslq	52(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	imgY_com(%rip), %rax
	movq	8(%rax,%rbx,2), %rdi
	movq	enc_bottom_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movq	img(%rip), %rax
	movslq	52(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%r14
	movq	img(%rip), %rax
	movl	68(%rax), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movslq	%edx, %rcx
	addq	$8, %rbx
	cmpq	%rcx, %r14
	jl	.LBB28_2
.LBB28_3:                               # %._crit_edge
	cmpl	$0, 15536(%rax)
	je	.LBB28_7
# BB#4:                                 # %.preheader
	cmpl	$2, 80(%rax)
	jl	.LBB28_7
# BB#5:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB28_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	imgUV_com(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,2), %rdi
	movq	enc_top_picture(%rip), %rcx
	movq	6472(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx), %rsi
	movslq	64(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	imgUV_com(%rip), %rax
	movq	(%rax), %rax
	movq	8(%rax,%rbx,2), %rdi
	movq	enc_bottom_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movq	img(%rip), %rax
	movslq	64(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	imgUV_com(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,2), %rdi
	movq	enc_top_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movq	img(%rip), %rax
	movslq	64(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	imgUV_com(%rip), %rax
	movq	8(%rax), %rax
	movq	8(%rax,%rbx,2), %rdi
	movq	enc_bottom_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movq	img(%rip), %rax
	movslq	64(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%r14
	movq	img(%rip), %rax
	movl	80(%rax), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movslq	%edx, %rcx
	addq	$8, %rbx
	cmpq	%rcx, %r14
	jl	.LBB28_6
.LBB28_7:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end28:
	.size	combine_field, .Lfunc_end28-combine_field
	.cfi_endproc

	.globl	decide_fld_frame
	.p2align	4, 0x90
	.type	decide_fld_frame,@function
decide_fld_frame:                       # @decide_fld_frame
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%esi, %xmm3
	mulsd	%xmm2, %xmm3
	cvtss2sd	%xmm0, %xmm0
	addsd	%xmm3, %xmm0
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edi, %xmm3
	mulsd	%xmm2, %xmm3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm1
	setbe	%al
	retq
.Lfunc_end29:
	.size	decide_fld_frame, .Lfunc_end29-decide_fld_frame
	.cfi_endproc

	.type	inputs,@object          # @inputs
	.comm	inputs,5800,8
	.type	input,@object           # @input
	.data
	.globl	input
	.p2align	3
input:
	.quad	inputs
	.size	input, 8

	.type	images,@object          # @images
	.comm	images,15616,8
	.type	img,@object             # @img
	.globl	img
	.p2align	3
img:
	.quad	images
	.size	img, 8

	.type	statistics,@object      # @statistics
	.comm	statistics,2264,8
	.type	stats,@object           # @stats
	.globl	stats
	.p2align	3
stats:
	.quad	statistics
	.size	stats, 8

	.type	snrs,@object            # @snrs
	.comm	snrs,124,4
	.type	snr,@object             # @snr
	.globl	snr
	.p2align	3
snr:
	.quad	snrs
	.size	snr, 8

	.type	decoders,@object        # @decoders
	.comm	decoders,56,8
	.type	decs,@object            # @decs
	.globl	decs
	.p2align	3
decs:
	.quad	decoders
	.size	decs, 8

	.type	initial_Bframes,@object # @initial_Bframes
	.bss
	.globl	initial_Bframes
	.p2align	2
initial_Bframes:
	.long	0                       # 0x0
	.size	initial_Bframes, 4

	.type	In2ndIGOP,@object       # @In2ndIGOP
	.globl	In2ndIGOP
	.p2align	2
In2ndIGOP:
	.long	0                       # 0x0
	.size	In2ndIGOP, 4

	.type	start_frame_no_in_this_IGOP,@object # @start_frame_no_in_this_IGOP
	.globl	start_frame_no_in_this_IGOP
	.p2align	2
start_frame_no_in_this_IGOP:
	.long	0                       # 0x0
	.size	start_frame_no_in_this_IGOP, 4

	.type	start_tr_in_this_IGOP,@object # @start_tr_in_this_IGOP
	.globl	start_tr_in_this_IGOP
	.p2align	2
start_tr_in_this_IGOP:
	.long	0                       # 0x0
	.size	start_tr_in_this_IGOP, 4

	.type	FirstFrameIn2ndIGOP,@object # @FirstFrameIn2ndIGOP
	.globl	FirstFrameIn2ndIGOP
	.p2align	2
FirstFrameIn2ndIGOP:
	.long	0                       # 0x0
	.size	FirstFrameIn2ndIGOP, 4

	.type	cabac_encoding,@object  # @cabac_encoding
	.globl	cabac_encoding
	.p2align	2
cabac_encoding:
	.long	0                       # 0x0
	.size	cabac_encoding, 4

	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	frame_statistic_start,@object # @frame_statistic_start
	.comm	frame_statistic_start,4,4
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	quadratic_RC,@object    # @quadratic_RC
	.comm	quadratic_RC,8,8
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	init_img.mb_width_cr,@object # @init_img.mb_width_cr
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
init_img.mb_width_cr:
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	16                      # 0x10
	.size	init_img.mb_width_cr, 16

	.type	init_img.mb_height_cr,@object # @init_img.mb_height_cr
	.p2align	4
init_img.mb_height_cr:
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	16                      # 0x10
	.size	init_img.mb_height_cr, 16

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"init_img: img->quad"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"init_img: img->mb_data"
	.size	.L.str.1, 23

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"init_img: img->intra_block"
	.size	.L.str.2, 27

	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"init mb16x16_cost_frame"
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"malloc_picture: Picture structure"
	.size	.L.str.4, 34

	.type	report_frame_statistic.last_mode_use,@object # @report_frame_statistic.last_mode_use
	.local	report_frame_statistic.last_mode_use
	.comm	report_frame_statistic.last_mode_use,600,16
	.type	report_frame_statistic.last_b8_mode_0,@object # @report_frame_statistic.last_b8_mode_0
	.local	report_frame_statistic.last_b8_mode_0
	.comm	report_frame_statistic.last_b8_mode_0,40,16
	.type	report_frame_statistic.last_mode_chroma_use,@object # @report_frame_statistic.last_mode_chroma_use
	.local	report_frame_statistic.last_mode_chroma_use
	.comm	report_frame_statistic.last_mode_chroma_use,16,16
	.type	report_frame_statistic.last_bit_ctr_n,@object # @report_frame_statistic.last_bit_ctr_n
	.local	report_frame_statistic.last_bit_ctr_n
	.comm	report_frame_statistic.last_bit_ctr_n,8,8
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"stat_frame.dat"
	.size	.L.str.5, 15

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"r"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"a"
	.size	.L.str.7, 2

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Error open file %s  \n"
	.size	.L.str.8, 22

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"stat_frame.dat.dat"
	.size	.L.str.9, 19

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	" --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- \n"
	.size	.L.str.10, 469

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"|            Encoder statistics. This file is generated during first encoding session, new sessions will be appended                                                                                                                                                                                                                                                                                                                                                              |\n"
	.size	.L.str.11, 469

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"|  ver       | Date  | Time  |    Sequence        |Frm | QP |P/MbInt|   Bits   |  SNRY  |  SNRU  |  SNRV  |  I4  |  I8  | I16  | IC0  | IC1  | IC2  | IC3  | PI4  | PI8  | PI16 |  P0  |  P1  |  P2  |  P3  | P1*8*| P1*4*| P2*8*| P2*4*| P3*8*| P3*4*|  P8  | P8:4 | P4*8*| P4*4*| P8:5 | P8:6 | P8:7 | BI4  | BI8  | BI16 |  B0  |  B1  |  B2  |  B3  | B0*8*| B0*4*| B1*8*| B1*4*| B2*8*| B2*4*| B3*8*| B3*4*|  B8  | B8:0 |B80*8*|B80*4*| B8:4 | B4*8*| B4*4*| B8:5 | B8:6 | B8:7 |\n"
	.size	.L.str.12, 473

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	" ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- \n"
	.size	.L.str.13, 473

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"|%4s/%s"
	.size	.L.str.14, 8

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"12.1"
	.size	.L.str.15, 5

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"(FRExt)"
	.size	.L.str.16, 8

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%d-%b-%Y"
	.size	.L.str.17, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"| %1.5s |"
	.size	.L.str.18, 10

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%H:%M:%S"
	.size	.L.str.19, 9

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	" %1.5s |"
	.size	.L.str.20, 9

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%20.20s|"
	.size	.L.str.21, 9

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%3d |"
	.size	.L.str.22, 6

	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"  %d/%d  |"
	.size	.L.str.23, 11

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	" %9d|"
	.size	.L.str.24, 6

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	" %2.4f| %2.4f| %2.4f|"
	.size	.L.str.25, 22

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	" %5lld|"
	.size	.L.str.26, 8

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	" %5d|"
	.size	.L.str.27, 6

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	" %d|"
	.size	.L.str.28, 5

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"-------------------------------------------------------------------------------\n"
	.size	.L.str.30, 81

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	" Freq. for encoded bitstream       : %1.0f\n"
	.size	.L.str.31, 44

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	" ME Metric for Refinement Level %1d  : %s\n"
	.size	.L.str.32, 43

	.type	DistortionType,@object  # @DistortionType
	.data
	.p2align	4
DistortionType:
	.asciz	"SAD\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"SSE\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"Hadamard SAD\000\000\000\000\000\000\000"
	.size	DistortionType, 60

	.type	.L.str.33,@object       # @.str.33
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.33:
	.asciz	" Mode Decision Metric              : %s\n"
	.size	.L.str.33, 41

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	" Motion Estimation for components  : YCbCr\n"
	.size	.L.str.34, 44

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	" Motion Estimation for components  : Y\n"
	.size	.L.str.35, 40

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	" Image format                      : %dx%d\n"
	.size	.L.str.36, 44

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	" Error robustness                  : On\n"
	.size	.L.str.37, 41

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	" Error robustness                  : Off\n"
	.size	.L.str.38, 42

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	" Search range                      : %d\n"
	.size	.L.str.39, 41

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	" Total number of references        : %d\n"
	.size	.L.str.40, 41

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	" References for P slices           : %d\n"
	.size	.L.str.41, 41

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	" List0 references for B slices     : %d\n"
	.size	.L.str.42, 41

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	" List1 references for B slices     : %d\n"
	.size	.L.str.43, 41

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	" Sequence type                     :"
	.size	.L.str.44, 37

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	" Hierarchy (QP: I %d, P %d, B %d) \n"
	.size	.L.str.45, 36

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"-RB"
	.size	.L.str.47, 4

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"-B"
	.size	.L.str.48, 3

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"-P"
	.size	.L.str.49, 3

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	" %s (QP: I %d, P %d, RB %d) \n"
	.size	.L.str.50, 30

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	" %s (QP: I %d, P %d, B %d) \n"
	.size	.L.str.51, 29

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	" IPPP (QP: I %d, P %d) \n"
	.size	.L.str.52, 25

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	" I-P-P-SP-P (QP: I %d, P %d, SP (%d, %d)) \n"
	.size	.L.str.53, 44

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	" Entropy coding method             : CAVLC\n"
	.size	.L.str.54, 44

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	" Entropy coding method             : CABAC\n"
	.size	.L.str.55, 44

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	" Profile/Level IDC                 : (%d,%d)\n"
	.size	.L.str.56, 46

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	" Motion Estimation Scheme          : HEX\n"
	.size	.L.str.57, 42

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	" Motion Estimation Scheme          : SHEX\n"
	.size	.L.str.58, 43

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	" Motion Estimation Scheme          : EPZS\n"
	.size	.L.str.59, 43

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	" Motion Estimation Scheme          : Fast Full Search\n"
	.size	.L.str.60, 55

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	" Motion Estimation Scheme          : Full Search\n"
	.size	.L.str.61, 50

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	" Search range restrictions         : none\n"
	.size	.L.str.62, 43

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	" Search range restrictions         : older reference frames\n"
	.size	.L.str.63, 61

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	" Search range restrictions         : smaller blocks and older reference frames\n"
	.size	.L.str.64, 80

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	" RD-optimized mode decision        : used\n"
	.size	.L.str.65, 43

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	" RD-optimized mode decision        : not used\n"
	.size	.L.str.66, 47

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	" Data Partitioning Mode            : 1 partition \n"
	.size	.L.str.67, 51

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	" Data Partitioning Mode            : 3 partitions \n"
	.size	.L.str.68, 52

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	" Data Partitioning Mode            : not supported\n"
	.size	.L.str.69, 52

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	" Output File Format                : H.264 Bit Stream File Format \n"
	.size	.L.str.70, 68

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	" Output File Format                : RTP Packet File Format \n"
	.size	.L.str.71, 62

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	" Output File Format                : not supported\n"
	.size	.L.str.72, 52

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"------------------ Average data all frames  -----------------------------------\n"
	.size	.L.str.73, 81

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	" PSNR Y(dB)                        : %5.2f\n"
	.size	.L.str.74, 44

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	" PSNR U(dB)                        : %5.2f\n"
	.size	.L.str.75, 44

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	" PSNR V(dB)                        : %5.2f\n"
	.size	.L.str.76, 44

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	" cSNR Y(dB)                        : %5.2f (%5.2f)\n"
	.size	.L.str.77, 52

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	" cSNR U(dB)                        : %5.2f (%5.2f)\n"
	.size	.L.str.78, 52

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	" cSNR V(dB)                        : %5.2f (%5.2f)\n"
	.size	.L.str.79, 52

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	" Total bits                        : %lld (I %lld, P %lld, B %lld NVB %d) \n"
	.size	.L.str.80, 76

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	" Bit rate (kbit/s)  @ %2.2f Hz     : %5.2f\n"
	.size	.L.str.81, 44

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	" Total bits                        : %lld (I %lld, P %lld, NVB %d) \n"
	.size	.L.str.82, 69

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	" Bits to avoid Startcode Emulation : %d \n"
	.size	.L.str.83, 42

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	" Bits for parameter sets           : %d \n"
	.size	.L.str.84, 42

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"Exit JM %s encoder ver %s "
	.size	.L.str.85, 27

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"12 (FRExt)"
	.size	.L.str.86, 11

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"stats.dat"
	.size	.L.str.87, 10

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"wt"
	.size	.L.str.88, 3

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"Error open file %s"
	.size	.L.str.89, 19

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	" -------------------------------------------------------------- \n"
	.size	.L.str.90, 66

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"  This file contains statistics for the last encoded sequence   \n"
	.size	.L.str.91, 66

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	" Sequence                     : %s\n"
	.size	.L.str.92, 36

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	" No.of coded pictures         : %4d\n"
	.size	.L.str.93, 37

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	" Freq. for encoded bitstream  : %4.0f\n"
	.size	.L.str.94, 39

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	" I Slice Bitrate(kb/s)        : %6.2f\n"
	.size	.L.str.95, 39

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	" P Slice Bitrate(kb/s)        : %6.2f\n"
	.size	.L.str.96, 39

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	" B Slice Bitrate(kb/s)        : %6.2f\n"
	.size	.L.str.97, 39

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	" Total Bitrate(kb/s)          : %6.2f\n"
	.size	.L.str.98, 39

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	" ME Metric for Refinement Level %1d : %s\n"
	.size	.L.str.99, 42

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	" Image format                 : %dx%d\n"
	.size	.L.str.100, 39

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	" Error robustness             : On\n"
	.size	.L.str.101, 36

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	" Error robustness             : Off\n"
	.size	.L.str.102, 37

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	" Search range                 : %d\n"
	.size	.L.str.103, 36

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	" Total number of references   : %d\n"
	.size	.L.str.104, 36

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	" References for P slices      : %d\n"
	.size	.L.str.105, 36

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	" List0 refs for B slices      : %d\n"
	.size	.L.str.106, 36

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	" List1 refs for B slices      : %d\n"
	.size	.L.str.107, 36

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	" Entropy coding method        : CAVLC\n"
	.size	.L.str.108, 39

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	" Entropy coding method        : CABAC\n"
	.size	.L.str.109, 39

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	" Profile/Level IDC            : (%d,%d)\n"
	.size	.L.str.110, 41

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	" MB Field Coding : On \n"
	.size	.L.str.111, 24

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	" Search range restrictions    : none\n"
	.size	.L.str.112, 38

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	" Search range restrictions    : older reference frames\n"
	.size	.L.str.113, 56

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	" Search range restrictions    : smaller blocks and older reference frames\n"
	.size	.L.str.114, 75

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	" RD-optimized mode decision   : used\n"
	.size	.L.str.115, 38

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	" RD-optimized mode decision   : not used\n"
	.size	.L.str.116, 42

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	" ---------------------|----------------|---------------|\n"
	.size	.L.str.117, 58

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"     Item             |     Intra      |   All frames  |\n"
	.size	.L.str.118, 58

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	" SNR Y(dB)            |"
	.size	.L.str.119, 24

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	" %5.2f          |"
	.size	.L.str.120, 18

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	" %5.2f         |\n"
	.size	.L.str.121, 18

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	" SNR U/V (dB)         |"
	.size	.L.str.122, 24

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	" %5.2f/%5.2f    |"
	.size	.L.str.123, 18

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	" %5.2f/%5.2f   |\n"
	.size	.L.str.124, 18

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	" Average quant        |"
	.size	.L.str.125, 24

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	" %5d          |"
	.size	.L.str.126, 16

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"\n ---------------------|----------------|---------------|---------------|\n"
	.size	.L.str.127, 75

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"     SNR              |        I       |       P       |       B       |\n"
	.size	.L.str.128, 74

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	" ---------------------|----------------|---------------|---------------|\n"
	.size	.L.str.129, 74

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	" SNR Y(dB)            |      %5.3f    |     %5.3f    |     %5.3f    |\n"
	.size	.L.str.130, 71

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	" SNR U(dB)            |      %5.3f    |     %5.3f    |     %5.3f    |\n"
	.size	.L.str.131, 71

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	" SNR V(dB)            |      %5.3f    |     %5.3f    |     %5.3f    |\n"
	.size	.L.str.132, 71

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"\n ---------------------|----------------|\n"
	.size	.L.str.133, 43

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"   Intra              |   Mode used    |\n"
	.size	.L.str.134, 42

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	" ---------------------|----------------|\n"
	.size	.L.str.135, 42

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	" Mode 0  intra 4x4    |  %5lld         |\n"
	.size	.L.str.136, 42

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	" Mode 1  intra 8x8    |  %5lld         |\n"
	.size	.L.str.137, 42

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	" Mode 2+ intra 16x16  |  %5lld         |\n"
	.size	.L.str.138, 42

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	" Mode    intra IPCM   |  %5lld         |\n"
	.size	.L.str.139, 42

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"\n ---------------------|----------------|-----------------|\n"
	.size	.L.str.140, 61

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"   Inter              |   Mode used    | MotionInfo bits |\n"
	.size	.L.str.141, 60

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	" ---------------------|----------------|-----------------|"
	.size	.L.str.142, 59

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"\n Mode  0  (copy)      |  %5lld         |    %8.2f     |"
	.size	.L.str.143, 57

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"\n Mode  1  (16x16)     |  %5lld         |    %8.2f     |"
	.size	.L.str.144, 57

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"\n Mode  2  (16x8)      |  %5lld         |    %8.2f     |"
	.size	.L.str.145, 57

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"\n Mode  3  (8x16)      |  %5lld         |    %8.2f     |"
	.size	.L.str.146, 57

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"\n Mode  4  (8x8)       |  %5lld         |    %8.2f     |"
	.size	.L.str.147, 57

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"\n Mode  5  intra 4x4   |  %5lld         |-----------------|"
	.size	.L.str.148, 60

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"\n Mode  6  intra 8x8   |  %5lld         |"
	.size	.L.str.149, 42

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"\n Mode  7+ intra 16x16 |  %5lld         |"
	.size	.L.str.150, 42

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"\n Mode     intra IPCM  |  %5lld         |"
	.size	.L.str.151, 42

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"\n\n ---------------------|----------------|-----------------|\n"
	.size	.L.str.152, 62

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"   B frame            |   Mode used    | MotionInfo bits |\n"
	.size	.L.str.153, 60

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"\n\n ---------------------|----------------|----------------|----------------|\n"
	.size	.L.str.154, 78

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"  Bit usage:          |      Intra     |      Inter     |    B frame     |\n"
	.size	.L.str.155, 76

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	" ---------------------|----------------|----------------|----------------|\n"
	.size	.L.str.156, 76

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	" Header               |"
	.size	.L.str.157, 24

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	" %10.2f     |"
	.size	.L.str.158, 14

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	" Mode                 |"
	.size	.L.str.159, 24

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	" Motion Info          |"
	.size	.L.str.160, 24

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"        ./.     |"
	.size	.L.str.161, 18

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	" CBP Y/C              |"
	.size	.L.str.162, 24

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	" Coeffs. Y            | %10.2f     | %10.2f     | %10.2f     |\n"
	.size	.L.str.163, 64

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	" Coeffs. C            | %10.2f     | %10.2f     | %10.2f     |\n"
	.size	.L.str.164, 64

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	" Delta quant          | %10.2f     | %10.2f     | %10.2f     |\n"
	.size	.L.str.165, 64

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	" Stuffing Bits        | %10.2f     | %10.2f     | %10.2f     |\n"
	.size	.L.str.166, 64

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	" average bits/frame   |"
	.size	.L.str.167, 24

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"log.dat"
	.size	.L.str.168, 8

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	" ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ \n"
	.size	.L.str.169, 298

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"|                   Encoder statistics. This file is generated during first encoding session, new sessions will be appended                                                                                                                                                                            |\n"
	.size	.L.str.170, 298

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"|    ver    | Date  | Time  |         Sequence             | #Img |P/MbInt| QPI| QPP| QPB| Format  |Iperiod| #B | FMES | Hdmd | S.R |#Ref | Freq |Coding|RD-opt|Intra upd|8x8Tr| SNRY 1| SNRU 1| SNRV 1| SNRY N| SNRU N| SNRV N|#Bitr I|#Bitr P|#Bitr B|#Bitr IPB|     Total Time   |      Me Time     |\n"
	.size	.L.str.171, 298

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"|%s/%-4s"
	.size	.L.str.172, 9

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"%30.30s|"
	.size	.L.str.173, 9

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"%5d |"
	.size	.L.str.174, 6

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	" %-3d|"
	.size	.L.str.175, 7

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"%4dx%-4d|"
	.size	.L.str.176, 10

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"  %3d  |"
	.size	.L.str.177, 9

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"  HEX |"
	.size	.L.str.178, 8

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	" SHEX |"
	.size	.L.str.179, 8

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	" EPZS |"
	.size	.L.str.180, 8

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"  FFS |"
	.size	.L.str.181, 8

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"  FS  |"
	.size	.L.str.182, 8

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"  %1d%1d%1d |"
	.size	.L.str.183, 14

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	" %3d |"
	.size	.L.str.184, 7

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	" %2d  |"
	.size	.L.str.185, 8

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	" %5.2f|"
	.size	.L.str.186, 8

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	" CAVLC|"
	.size	.L.str.187, 8

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	" CABAC|"
	.size	.L.str.188, 8

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"   %d  |"
	.size	.L.str.189, 9

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"   ON    |"
	.size	.L.str.190, 11

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"   OFF   |"
	.size	.L.str.191, 11

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"  %d  |"
	.size	.L.str.192, 8

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"%7.3f|"
	.size	.L.str.193, 7

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	"%7.0f|"
	.size	.L.str.194, 7

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"%9.0f|"
	.size	.L.str.195, 7

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"   %12d   |"
	.size	.L.str.196, 12

	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	"data.txt"
	.size	.L.str.197, 9

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"%3d %2d %2d %2.2f %2.2f %2.2f %5lld %2.2f %2.2f %2.2f %5d %2.2f %2.2f %2.2f %5lld %5lld %.3f\n"
	.size	.L.str.198, 94

	.type	.L.str.199,@object      # @.str.199
.L.str.199:
	.asciz	"%3d %2d %2d %2.2f %2.2f %2.2f %5lld %2.2f %2.2f %2.2f %5d %2.2f %2.2f %2.2f %5lld %5d %.3f\n"
	.size	.L.str.199, 92

	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	.L.str.200,@object      # @.str.200
.L.str.200:
	.asciz	"init_global_buffers: imgUV_org_top"
	.size	.L.str.200, 35

	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	.L.str.201,@object      # @.str.201
.L.str.201:
	.asciz	"init_global_buffers: imgUV_org_bot"
	.size	.L.str.201, 35

	.type	.L.str.202,@object      # @.str.202
.L.str.202:
	.asciz	"init_global_buffers: last_P_no"
	.size	.L.str.202, 31

	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	.L.str.203,@object      # @.str.203
.L.str.203:
	.asciz	"init_global_buffers: decref"
	.size	.L.str.203, 28

	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	quadratic_RC_init,@object # @quadratic_RC_init
	.comm	quadratic_RC_init,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	quadratic_RC_best,@object # @quadratic_RC_best
	.comm	quadratic_RC_best,8,8
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	.L.str.204,@object      # @.str.204
.L.str.204:
	.asciz	"get_mem_mv: mv"
	.size	.L.str.204, 15

	.type	.L.str.205,@object      # @.str.205
.L.str.205:
	.asciz	"get_mem_ACcoeff: cofAC"
	.size	.L.str.205, 23

	.type	.L.str.206,@object      # @.str.206
.L.str.206:
	.asciz	"get_mem_DCcoeff: cofDC"
	.size	.L.str.206, 23

	.type	.L.str.207,@object      # @.str.207
.L.str.207:
	.asciz	"Warning: unknown LevelIDC, using maximum level 5.1 \n"
	.size	.L.str.207, 53

	.type	.L.str.208,@object      # @.str.208
.L.str.208:
	.asciz	"B frame not supported when redundant picture used!"
	.size	.L.str.208, 51

	.type	.L.str.209,@object      # @.str.209
.L.str.209:
	.asciz	"Interlace not supported when redundant picture used!"
	.size	.L.str.209, 53

	.type	.L.str.210,@object      # @.str.210
.L.str.210:
	.asciz	"NumberReferenceFrames must be no less than PrimaryGOPLength"
	.size	.L.str.210, 60

	.type	.L.str.211,@object      # @.str.211
.L.str.211:
	.asciz	"PrimaryGOPLength must be greater than 2^NumRedundantHeirarchy"
	.size	.L.str.211, 62

	.type	.L.str.212,@object      # @.str.212
.L.str.212:
	.asciz	"Redundant slices not supported when Verbose!=1"
	.size	.L.str.212, 47

	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	configinput,@object     # @configinput
	.comm	configinput,5800,8
	.type	seiHasBufferingPeriod_info,@object # @seiHasBufferingPeriod_info
	.comm	seiHasBufferingPeriod_info,4,4
	.type	seiBufferingPeriod,@object # @seiBufferingPeriod
	.comm	seiBufferingPeriod,280,8
	.type	seiHasPicTiming_info,@object # @seiHasPicTiming_info
	.comm	seiHasPicTiming_info,4,4
	.type	seiPicTiming,@object    # @seiPicTiming
	.comm	seiPicTiming,152,8
	.type	WriteNALU,@object       # @WriteNALU
	.comm	WriteNALU,8,8
	.type	McostState,@object      # @McostState
	.comm	McostState,8,8
	.type	SearchState,@object     # @SearchState
	.comm	SearchState,8,8
	.type	fastme_ref_cost,@object # @fastme_ref_cost
	.comm	fastme_ref_cost,8,8
	.type	fastme_l0_cost,@object  # @fastme_l0_cost
	.comm	fastme_l0_cost,8,8
	.type	fastme_l1_cost,@object  # @fastme_l1_cost
	.comm	fastme_l1_cost,8,8
	.type	fastme_l0_cost_bipred,@object # @fastme_l0_cost_bipred
	.comm	fastme_l0_cost_bipred,8,8
	.type	fastme_l1_cost_bipred,@object # @fastme_l1_cost_bipred
	.comm	fastme_l1_cost_bipred,8,8
	.type	bipred_flag,@object     # @bipred_flag
	.comm	bipred_flag,4,4
	.type	fastme_best_cost,@object # @fastme_best_cost
	.comm	fastme_best_cost,8,8
	.type	pred_SAD,@object        # @pred_SAD
	.comm	pred_SAD,4,4
	.type	pred_MV_ref,@object     # @pred_MV_ref
	.comm	pred_MV_ref,8,4
	.type	pred_MV_uplayer,@object # @pred_MV_uplayer
	.comm	pred_MV_uplayer,8,4
	.type	UMHEX_blocktype,@object # @UMHEX_blocktype
	.comm	UMHEX_blocktype,4,4
	.type	predict_point,@object   # @predict_point
	.comm	predict_point,40,16
	.type	SAD_a,@object           # @SAD_a
	.comm	SAD_a,4,4
	.type	SAD_b,@object           # @SAD_b
	.comm	SAD_b,4,4
	.type	SAD_c,@object           # @SAD_c
	.comm	SAD_c,4,4
	.type	SAD_d,@object           # @SAD_d
	.comm	SAD_d,4,4
	.type	Threshold_DSR_MB,@object # @Threshold_DSR_MB
	.comm	Threshold_DSR_MB,32,16
	.type	Bsize,@object           # @Bsize
	.comm	Bsize,32,16
	.type	AlphaFourth_1,@object   # @AlphaFourth_1
	.comm	AlphaFourth_1,32,16
	.type	AlphaFourth_2,@object   # @AlphaFourth_2
	.comm	AlphaFourth_2,32,16
	.type	flag_intra,@object      # @flag_intra
	.comm	flag_intra,8,8
	.type	flag_intra_SAD,@object  # @flag_intra_SAD
	.comm	flag_intra_SAD,4,4
	.type	SymmetricalCrossSearchThreshold1,@object # @SymmetricalCrossSearchThreshold1
	.comm	SymmetricalCrossSearchThreshold1,2,2
	.type	SymmetricalCrossSearchThreshold2,@object # @SymmetricalCrossSearchThreshold2
	.comm	SymmetricalCrossSearchThreshold2,2,2
	.type	ConvergeThreshold,@object # @ConvergeThreshold
	.comm	ConvergeThreshold,2,2
	.type	SubPelThreshold1,@object # @SubPelThreshold1
	.comm	SubPelThreshold1,2,2
	.type	SubPelThreshold3,@object # @SubPelThreshold3
	.comm	SubPelThreshold3,2,2
	.type	smpUMHEX_SearchState,@object # @smpUMHEX_SearchState
	.comm	smpUMHEX_SearchState,8,8
	.type	smpUMHEX_l0_cost,@object # @smpUMHEX_l0_cost
	.comm	smpUMHEX_l0_cost,8,8
	.type	smpUMHEX_l1_cost,@object # @smpUMHEX_l1_cost
	.comm	smpUMHEX_l1_cost,8,8
	.type	smpUMHEX_flag_intra,@object # @smpUMHEX_flag_intra
	.comm	smpUMHEX_flag_intra,8,8
	.type	smpUMHEX_flag_intra_SAD,@object # @smpUMHEX_flag_intra_SAD
	.comm	smpUMHEX_flag_intra_SAD,4,4
	.type	smpUMHEX_pred_SAD_uplayer,@object # @smpUMHEX_pred_SAD_uplayer
	.comm	smpUMHEX_pred_SAD_uplayer,4,4
	.type	smpUMHEX_pred_MV_uplayer_X,@object # @smpUMHEX_pred_MV_uplayer_X
	.comm	smpUMHEX_pred_MV_uplayer_X,2,2
	.type	smpUMHEX_pred_MV_uplayer_Y,@object # @smpUMHEX_pred_MV_uplayer_Y
	.comm	smpUMHEX_pred_MV_uplayer_Y,2,2
	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	updateQP,@object        # @updateQP
	.comm	updateQP,8,8
	.type	.Linformation_init.yuv_types,@object # @information_init.yuv_types
	.section	.rodata,"a",@progbits
	.p2align	4
.Linformation_init.yuv_types:
	.asciz	"YUV 4:0:0"
	.asciz	"YUV 4:2:0"
	.asciz	"YUV 4:2:2"
	.asciz	"YUV 4:4:4"
	.size	.Linformation_init.yuv_types, 40

	.type	.L.str.213,@object      # @.str.213
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.213:
	.asciz	"------------------------------- JM %s %s --------------------------------\n"
	.size	.L.str.213, 75

	.type	.L.str.214,@object      # @.str.214
.L.str.214:
	.asciz	"------------------------------- JM %s %s ------------------------------------------\n"
	.size	.L.str.214, 85

	.type	.L.str.215,@object      # @.str.215
.L.str.215:
	.asciz	" Input YUV file                    : %s \n"
	.size	.L.str.215, 42

	.type	.L.str.216,@object      # @.str.216
.L.str.216:
	.asciz	" Output H.264 bitstream            : %s \n"
	.size	.L.str.216, 42

	.type	.L.str.217,@object      # @.str.217
.L.str.217:
	.asciz	" Output YUV file                   : %s \n"
	.size	.L.str.217, 42

	.type	.L.str.218,@object      # @.str.218
.L.str.218:
	.asciz	" YUV Format                        : %s \n"
	.size	.L.str.218, 42

	.type	.L.str.219,@object      # @.str.219
.L.str.219:
	.asciz	" Frames to be encoded I-P/B        : %d/%d\n"
	.size	.L.str.219, 44

	.type	.L.str.220,@object      # @.str.220
.L.str.220:
	.asciz	" PicInterlace / MbInterlace        : %d/%d\n"
	.size	.L.str.220, 44

	.type	.L.str.221,@object      # @.str.221
.L.str.221:
	.asciz	" Transform8x8Mode                  : %d\n"
	.size	.L.str.221, 41

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"  Frame  Bit/pic WP QP   SnrY    SnrU    SnrV    Time(ms) MET(ms) Frm/Fld   I D L0 L1 RDP Ref"
	.size	.Lstr.1, 94

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"---------------------------------------------------------------------------------------------"
	.size	.Lstr.2, 94

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"  Frame  Bit/pic    QP   SnrY    SnrU    SnrV    Time(ms) MET(ms) Frm/Fld Ref  "
	.size	.Lstr.4, 80

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"-------------------------------------------------------------------------------"
	.size	.Lstr.6, 80

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"\nEncoding. Please Wait.\n"
	.size	.Lstr.7, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
