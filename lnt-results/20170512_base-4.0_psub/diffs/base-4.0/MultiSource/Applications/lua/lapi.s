	.text
	.file	"lapi.bc"
	.hidden	luaA_pushobject
	.globl	luaA_pushobject
	.p2align	4, 0x90
	.type	luaA_pushobject,@function
luaA_pushobject:                        # @luaA_pushobject
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movq	(%rsi), %rcx
	movq	%rcx, (%rax)
	movl	8(%rsi), %ecx
	movl	%ecx, 8(%rax)
	addq	$16, 16(%rdi)
	retq
.Lfunc_end0:
	.size	luaA_pushobject, .Lfunc_end0-luaA_pushobject
	.cfi_endproc

	.globl	lua_checkstack
	.p2align	4, 0x90
	.type	lua_checkstack,@function
lua_checkstack:                         # @lua_checkstack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	cmpl	$8000, %esi             # imm = 0x1F40
	jg	.LBB1_7
# BB#1:
	movq	16(%rbx), %rax
	movq	%rax, %rcx
	subq	24(%rbx), %rcx
	sarq	$4, %rcx
	movslq	%esi, %rbp
	addq	%rbp, %rcx
	cmpq	$8000, %rcx             # imm = 0x1F40
	jg	.LBB1_7
# BB#2:
	movl	$1, %r14d
	testl	%esi, %esi
	jle	.LBB1_7
# BB#3:
	movq	56(%rbx), %rcx
	subq	%rax, %rcx
	movl	%esi, %edx
	shll	$4, %edx
	movslq	%edx, %rdx
	cmpq	%rdx, %rcx
	jg	.LBB1_5
# BB#4:
	movq	%rbx, %rdi
	callq	luaD_growstack
	movq	16(%rbx), %rax
.LBB1_5:
	movq	40(%rbx), %rcx
	shlq	$4, %rbp
	addq	%rax, %rbp
	cmpq	%rbp, 16(%rcx)
	jae	.LBB1_7
# BB#6:
	movq	%rbp, 16(%rcx)
.LBB1_7:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	lua_checkstack, .Lfunc_end1-lua_checkstack
	.cfi_endproc

	.globl	lua_xmove
	.p2align	4, 0x90
	.type	lua_xmove,@function
lua_xmove:                              # @lua_xmove
	.cfi_startproc
# BB#0:
	cmpq	%rsi, %rdi
	je	.LBB2_9
# BB#1:
	movq	16(%rdi), %r8
	movslq	%edx, %rax
	movq	%rax, %rcx
	shlq	$4, %rcx
	movq	%r8, %r9
	subq	%rcx, %r9
	movq	%r9, 16(%rdi)
	testl	%eax, %eax
	jle	.LBB2_9
# BB#2:                                 # %.lr.ph
	negq	%rax
	movq	16(%rsi), %r10
	leaq	16(%r10), %rcx
	movq	%rcx, 16(%rsi)
	movq	(%r9), %rcx
	movq	%rcx, (%r10)
	shlq	$4, %rax
	movl	8(%r8,%rax), %eax
	movl	%eax, 8(%r10)
	cmpl	$1, %edx
	je	.LBB2_9
# BB#3:                                 # %._crit_edge.preheader
	movl	%edx, %r10d
	testb	$1, %r10b
	jne	.LBB2_4
# BB#5:                                 # %._crit_edge.prol
	movq	16(%rdi), %r8
	movq	16(%rsi), %r9
	leaq	16(%r9), %rcx
	movq	%rcx, 16(%rsi)
	movq	16(%r8), %rcx
	movq	%rcx, (%r9)
	movl	24(%r8), %ecx
	movl	%ecx, 8(%r9)
	movl	$2, %ecx
	cmpl	$2, %edx
	jne	.LBB2_7
	jmp	.LBB2_9
.LBB2_4:
	movl	$1, %ecx
	cmpl	$2, %edx
	je	.LBB2_9
.LBB2_7:                                # %._crit_edge.preheader.new
	subq	%rcx, %r10
	shlq	$4, %rcx
	.p2align	4, 0x90
.LBB2_8:                                # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %r8
	movq	16(%rsi), %rax
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	(%r8,%rcx), %rdx
	movq	%rdx, (%rax)
	movl	8(%r8,%rcx), %edx
	movl	%edx, 8(%rax)
	movq	16(%rdi), %r8
	movq	16(%rsi), %rdx
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rsi)
	movq	16(%r8,%rcx), %rax
	movq	%rax, (%rdx)
	movl	24(%r8,%rcx), %eax
	movl	%eax, 8(%rdx)
	addq	$32, %rcx
	addq	$-2, %r10
	jne	.LBB2_8
.LBB2_9:                                # %.loopexit
	retq
.Lfunc_end2:
	.size	lua_xmove, .Lfunc_end2-lua_xmove
	.cfi_endproc

	.globl	lua_setlevel
	.p2align	4, 0x90
	.type	lua_setlevel,@function
lua_setlevel:                           # @lua_setlevel
	.cfi_startproc
# BB#0:
	movzwl	96(%rdi), %eax
	movw	%ax, 96(%rsi)
	retq
.Lfunc_end3:
	.size	lua_setlevel, .Lfunc_end3-lua_setlevel
	.cfi_endproc

	.globl	lua_atpanic
	.p2align	4, 0x90
	.type	lua_atpanic,@function
lua_atpanic:                            # @lua_atpanic
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rcx
	movq	152(%rcx), %rax
	movq	%rsi, 152(%rcx)
	retq
.Lfunc_end4:
	.size	lua_atpanic, .Lfunc_end4-lua_atpanic
	.cfi_endproc

	.globl	lua_newthread
	.p2align	4, 0x90
	.type	lua_newthread,@function
lua_newthread:                          # @lua_newthread
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB5_2
# BB#1:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB5_2:
	movq	%rbx, %rdi
	callq	luaE_newthread
	movq	16(%rbx), %rcx
	movq	%rax, (%rcx)
	movl	$8, 8(%rcx)
	addq	$16, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end5:
	.size	lua_newthread, .Lfunc_end5-lua_newthread
	.cfi_endproc

	.globl	lua_gettop
	.p2align	4, 0x90
	.type	lua_gettop,@function
lua_gettop:                             # @lua_gettop
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	subq	24(%rdi), %rax
	shrq	$4, %rax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end6:
	.size	lua_gettop, .Lfunc_end6-lua_gettop
	.cfi_endproc

	.globl	lua_settop
	.p2align	4, 0x90
	.type	lua_settop,@function
lua_settop:                             # @lua_settop
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	js	.LBB7_8
# BB#1:                                 # %.preheader
	movq	16(%rdi), %r8
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	24(%rdi), %rax
	cmpq	%rax, %r8
	jae	.LBB7_9
# BB#2:                                 # %.lr.ph
	leaq	16(%r8), %rdx
	cmpq	%rdx, %rax
	cmovaq	%rax, %rdx
	movq	%r8, %r9
	notq	%r9
	addq	%r9, %rdx
	shrq	$4, %rdx
	addq	%rax, %r9
	movl	%r9d, %ecx
	shrl	$4, %ecx
	incl	%ecx
	andq	$7, %rcx
	movq	%r8, %rsi
	je	.LBB7_5
# BB#3:                                 # %.prol.preheader
	negq	%rcx
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movl	$0, 8(%rsi)
	addq	$16, %rsi
	incq	%rcx
	jne	.LBB7_4
.LBB7_5:                                # %.prol.loopexit
	incq	%rdx
	cmpq	$112, %r9
	jb	.LBB7_7
	.p2align	4, 0x90
.LBB7_6:                                # =>This Inner Loop Header: Depth=1
	movl	$0, 8(%rsi)
	movl	$0, 24(%rsi)
	movl	$0, 40(%rsi)
	movl	$0, 56(%rsi)
	movl	$0, 72(%rsi)
	movl	$0, 88(%rsi)
	movl	$0, 104(%rsi)
	movl	$0, 120(%rsi)
	subq	$-128, %rsi
	cmpq	%rax, %rsi
	jb	.LBB7_6
.LBB7_7:                                # %._crit_edge
	shlq	$4, %rdx
	addq	%rdx, %r8
	movq	%r8, 16(%rdi)
	movq	%rax, 16(%rdi)
	retq
.LBB7_8:
	movslq	%esi, %rax
	shlq	$4, %rax
	movq	16(%rdi), %rcx
	leaq	16(%rcx,%rax), %rax
.LBB7_9:
	movq	%rax, 16(%rdi)
	retq
.Lfunc_end7:
	.size	lua_settop, .Lfunc_end7-lua_settop
	.cfi_endproc

	.globl	lua_remove
	.p2align	4, 0x90
	.type	lua_remove,@function
lua_remove:                             # @lua_remove
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB8_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB8_13
.LBB8_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB8_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB8_13
.LBB8_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB8_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB8_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB8_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB8_13
.LBB8_9:
	leaq	120(%rdi), %rax
	jmp	.LBB8_13
.LBB8_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB8_13
.LBB8_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB8_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB8_13
.LBB8_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB8_13:                               # %index2adr.exit.preheader
	addq	$16, %rax
	movq	16(%rdi), %rcx
	cmpq	%rcx, %rax
	jae	.LBB8_16
	.p2align	4, 0x90
.LBB8_14:                               # %index2adr.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	%rcx, -16(%rax)
	movl	8(%rax), %ecx
	movl	%ecx, -8(%rax)
	movq	16(%rdi), %rcx
	addq	$16, %rax
	cmpq	%rcx, %rax
	jb	.LBB8_14
.LBB8_16:                               # %index2adr.exit._crit_edge
	addq	$-16, %rcx
	movq	%rcx, 16(%rdi)
	retq
.Lfunc_end8:
	.size	lua_remove, .Lfunc_end8-lua_remove
	.cfi_endproc

	.globl	lua_insert
	.p2align	4, 0x90
	.type	lua_insert,@function
lua_insert:                             # @lua_insert
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB9_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB9_13
.LBB9_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB9_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB9_13
.LBB9_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB9_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB9_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB9_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB9_13
.LBB9_9:
	leaq	120(%rdi), %rax
	jmp	.LBB9_13
.LBB9_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB9_13
.LBB9_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB9_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB9_13
.LBB9_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB9_13:                               # %index2adr.exit
	movq	16(%rdi), %rcx
	cmpq	%rax, %rcx
	jbe	.LBB9_16
	.p2align	4, 0x90
.LBB9_14:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rcx), %rdx
	movq	%rdx, (%rcx)
	movl	-8(%rcx), %edx
	movl	%edx, 8(%rcx)
	leaq	-16(%rcx), %rcx
	cmpq	%rax, %rcx
	ja	.LBB9_14
# BB#15:                                # %._crit_edge.loopexit
	movq	16(%rdi), %rcx
.LBB9_16:                               # %._crit_edge
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	8(%rcx), %ecx
	movl	%ecx, 8(%rax)
	retq
.Lfunc_end9:
	.size	lua_insert, .Lfunc_end9-lua_insert
	.cfi_endproc

	.globl	lua_replace
	.p2align	4, 0x90
	.type	lua_replace,@function
lua_replace:                            # @lua_replace
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpl	$-10001, %esi           # imm = 0xD8EF
	jne	.LBB10_3
# BB#1:
	leaq	40(%rbx), %r14
	movq	40(%rbx), %rax
	cmpq	80(%rbx), %rax
	jne	.LBB10_11
# BB#2:
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaG_runerror
.LBB10_11:                              # %.thread40
	movq	(%r14), %rax
	movq	8(%rax), %rcx
	movq	(%rcx), %rcx
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rbx)
	movl	$5, 144(%rbx)
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	leaq	16(%rbx), %r14
	movq	16(%rbx), %rax
	movq	-16(%rax), %rcx
	movq	%rcx, 24(%rsi)
	cmpl	$4, -8(%rax)
	jl	.LBB10_24
# BB#12:
	movq	-16(%rax), %rdx
	testb	$3, 9(%rdx)
	jne	.LBB10_22
	jmp	.LBB10_24
.LBB10_3:
	testl	%esi, %esi
	jle	.LBB10_5
# BB#4:
	movq	24(%rbx), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rbx), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB10_18
.LBB10_5:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB10_7
# BB#6:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rbx), %rax
	jmp	.LBB10_18
.LBB10_7:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB10_13
# BB#8:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	je	.LBB10_17
# BB#9:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	jne	.LBB10_14
# BB#10:                                # %..thread40_crit_edge
	leaq	40(%rbx), %r14
	jmp	.LBB10_11
.LBB10_13:
	leaq	120(%rbx), %rax
	jmp	.LBB10_18
.LBB10_17:
	movl	$160, %eax
	addq	32(%rbx), %rax
	jmp	.LBB10_18
.LBB10_14:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB10_16
# BB#15:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB10_18
.LBB10_16:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB10_18:
	leaq	16(%rbx), %r14
	movq	16(%rbx), %rcx
	movq	-16(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	-8(%rcx), %ecx
	movl	%ecx, 8(%rax)
	cmpl	$-10003, %esi           # imm = 0xD8ED
	jg	.LBB10_24
# BB#19:
	movq	(%r14), %rax
	cmpl	$4, -8(%rax)
	jl	.LBB10_24
# BB#20:
	movq	-16(%rax), %rdx
	testb	$3, 9(%rdx)
	je	.LBB10_24
# BB#21:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
.LBB10_22:
	testb	$4, 9(%rsi)
	je	.LBB10_24
# BB#23:
	movq	%rbx, %rdi
	callq	luaC_barrierf
.LBB10_24:
	addq	$-16, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	lua_replace, .Lfunc_end10-lua_replace
	.cfi_endproc

	.globl	lua_pushvalue
	.p2align	4, 0x90
	.type	lua_pushvalue,@function
lua_pushvalue:                          # @lua_pushvalue
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB11_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB11_13
.LBB11_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB11_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB11_13
.LBB11_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB11_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB11_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB11_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB11_13
.LBB11_9:
	leaq	120(%rdi), %rax
	jmp	.LBB11_13
.LBB11_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB11_13
.LBB11_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB11_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB11_13
.LBB11_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB11_13:                              # %index2adr.exit
	movq	16(%rdi), %rcx
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rcx)
	addq	$16, 16(%rdi)
	retq
.Lfunc_end11:
	.size	lua_pushvalue, .Lfunc_end11-lua_pushvalue
	.cfi_endproc

	.globl	lua_type
	.p2align	4, 0x90
	.type	lua_type,@function
lua_type:                               # @lua_type
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB12_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	movl	$-1, %eax
	cmpq	16(%rdi), %rcx
	jb	.LBB12_12
	jmp	.LBB12_15
.LBB12_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB12_4
# BB#3:
	movslq	%esi, %rcx
	shlq	$4, %rcx
	addq	16(%rdi), %rcx
	jmp	.LBB12_12
.LBB12_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB12_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB12_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB12_10
# BB#7:
	movl	$160, %ecx
	addq	32(%rdi), %rcx
	jmp	.LBB12_12
.LBB12_9:
	addq	$120, %rdi
	movq	%rdi, %rcx
	jmp	.LBB12_12
.LBB12_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rdi), %rcx
	movq	24(%rax), %rax
	movq	%rax, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB12_12
.LBB12_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	movl	$-10002, %edx           # imm = 0xD8EE
	subl	%esi, %edx
	movzbl	11(%rcx), %esi
	movl	$-1, %eax
	cmpl	%esi, %edx
	jg	.LBB12_15
# BB#11:
	movslq	%edx, %rax
	shlq	$4, %rax
	leaq	24(%rcx,%rax), %rcx
.LBB12_12:                              # %index2adr.exit
	movl	$luaO_nilobject_, %eax
	cmpq	%rax, %rcx
	je	.LBB12_13
# BB#14:
	movl	8(%rcx), %eax
.LBB12_15:                              # %index2adr.exit.thread
	retq
.LBB12_13:
	movl	$-1, %eax
	retq
.Lfunc_end12:
	.size	lua_type, .Lfunc_end12-lua_type
	.cfi_endproc

	.globl	lua_typename
	.p2align	4, 0x90
	.type	lua_typename,@function
lua_typename:                           # @lua_typename
	.cfi_startproc
# BB#0:
	cmpl	$-1, %esi
	je	.LBB13_1
# BB#2:
	movslq	%esi, %rax
	movq	luaT_typenames(,%rax,8), %rax
	retq
.LBB13_1:
	movl	$.L.str.1, %eax
	retq
.Lfunc_end13:
	.size	lua_typename, .Lfunc_end13-lua_typename
	.cfi_endproc

	.globl	lua_iscfunction
	.p2align	4, 0x90
	.type	lua_iscfunction,@function
lua_iscfunction:                        # @lua_iscfunction
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB14_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	cmpl	$6, 8(%rax)
	je	.LBB14_15
	jmp	.LBB14_14
.LBB14_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB14_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	cmpl	$6, 8(%rax)
	je	.LBB14_15
	jmp	.LBB14_14
.LBB14_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB14_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB14_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB14_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	cmpl	$6, 8(%rax)
	je	.LBB14_15
	jmp	.LBB14_14
.LBB14_9:
	addq	$120, %rdi
	movq	%rdi, %rax
	cmpl	$6, 8(%rax)
	je	.LBB14_15
	jmp	.LBB14_14
.LBB14_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	cmpl	$6, 8(%rax)
	je	.LBB14_15
	jmp	.LBB14_14
.LBB14_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB14_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	cmpl	$6, 8(%rax)
	je	.LBB14_15
	jmp	.LBB14_14
.LBB14_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
	cmpl	$6, 8(%rax)
	jne	.LBB14_14
.LBB14_15:
	movq	(%rax), %rax
	cmpb	$0, 10(%rax)
	setne	%al
	movzbl	%al, %eax
	retq
.LBB14_14:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end14:
	.size	lua_iscfunction, .Lfunc_end14-lua_iscfunction
	.cfi_endproc

	.globl	lua_isnumber
	.p2align	4, 0x90
	.type	lua_isnumber,@function
lua_isnumber:                           # @lua_isnumber
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB15_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB15_13
.LBB15_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB15_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB15_13
.LBB15_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB15_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB15_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB15_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB15_13
.LBB15_9:
	addq	$120, %rdi
	movq	%rdi, %rax
	jmp	.LBB15_13
.LBB15_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB15_13
.LBB15_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB15_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB15_13
.LBB15_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB15_13:                              # %index2adr.exit
	movb	$1, %cl
	cmpl	$3, 8(%rax)
	je	.LBB15_15
# BB#14:
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 32
	leaq	8(%rsp), %rsi
	movq	%rax, %rdi
	callq	luaV_tonumber
	testq	%rax, %rax
	setne	%cl
	addq	$24, %rsp
.LBB15_15:
	movzbl	%cl, %eax
	retq
.Lfunc_end15:
	.size	lua_isnumber, .Lfunc_end15-lua_isnumber
	.cfi_endproc

	.globl	lua_isstring
	.p2align	4, 0x90
	.type	lua_isstring,@function
lua_isstring:                           # @lua_isstring
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB16_3
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rdi), %rax
	jb	.LBB16_14
# BB#2:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB16_3:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB16_5
# BB#4:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB16_14
.LBB16_5:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB16_10
# BB#6:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB16_9
# BB#7:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB16_11
# BB#8:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB16_14
.LBB16_10:
	addq	$120, %rdi
	movq	%rdi, %rax
	jmp	.LBB16_14
.LBB16_9:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB16_14
.LBB16_11:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB16_13
# BB#12:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB16_13:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB16_14:                              # %index2adr.exit.i
	movl	$luaO_nilobject_, %ecx
	cmpq	%rcx, %rax
	je	.LBB16_15
# BB#16:
	movl	8(%rax), %eax
	addl	$-3, %eax
	cmpl	$2, %eax
	setb	%al
	movzbl	%al, %eax
	retq
.LBB16_15:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end16:
	.size	lua_isstring, .Lfunc_end16-lua_isstring
	.cfi_endproc

	.globl	lua_isuserdata
	.p2align	4, 0x90
	.type	lua_isuserdata,@function
lua_isuserdata:                         # @lua_isuserdata
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB17_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB17_13
.LBB17_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB17_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB17_13
.LBB17_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB17_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB17_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB17_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB17_13
.LBB17_9:
	addq	$120, %rdi
	movq	%rdi, %rax
	jmp	.LBB17_13
.LBB17_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB17_13
.LBB17_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB17_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB17_13
.LBB17_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB17_13:                              # %index2adr.exit
	movl	8(%rax), %eax
	cmpl	$7, %eax
	sete	%cl
	cmpl	$2, %eax
	sete	%al
	orb	%cl, %al
	movzbl	%al, %eax
	retq
.Lfunc_end17:
	.size	lua_isuserdata, .Lfunc_end17-lua_isuserdata
	.cfi_endproc

	.globl	lua_rawequal
	.p2align	4, 0x90
	.type	lua_rawequal,@function
lua_rawequal:                           # @lua_rawequal
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB18_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	testl	%edx, %edx
	jg	.LBB18_14
	jmp	.LBB18_15
.LBB18_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB18_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	testl	%edx, %edx
	jg	.LBB18_14
	jmp	.LBB18_15
.LBB18_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB18_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB18_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB18_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	testl	%edx, %edx
	jg	.LBB18_14
	jmp	.LBB18_15
.LBB18_9:
	leaq	120(%rdi), %rax
	testl	%edx, %edx
	jg	.LBB18_14
	jmp	.LBB18_15
.LBB18_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	testl	%edx, %edx
	jg	.LBB18_14
	jmp	.LBB18_15
.LBB18_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %esi
	cmpl	%esi, %ecx
	jle	.LBB18_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	testl	%edx, %edx
	jg	.LBB18_14
	jmp	.LBB18_15
.LBB18_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
	testl	%edx, %edx
	jle	.LBB18_15
.LBB18_14:
	movq	24(%rdi), %rcx
	movslq	%edx, %rdx
	shlq	$4, %rdx
	leaq	-16(%rcx,%rdx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %esi
	cmovbq	%rcx, %rsi
	jmp	.LBB18_25
.LBB18_15:
	cmpl	$-9999, %edx            # imm = 0xD8F1
	jl	.LBB18_17
# BB#16:
	movslq	%edx, %rsi
	shlq	$4, %rsi
	addq	16(%rdi), %rsi
	jmp	.LBB18_25
.LBB18_17:
	cmpl	$-10002, %edx           # imm = 0xD8EE
	je	.LBB18_22
# BB#18:
	cmpl	$-10001, %edx           # imm = 0xD8EF
	je	.LBB18_21
# BB#19:
	cmpl	$-10000, %edx           # imm = 0xD8F0
	jne	.LBB18_23
# BB#20:
	movl	$160, %esi
	addq	32(%rdi), %rsi
	jmp	.LBB18_25
.LBB18_22:
	addq	$120, %rdi
	movq	%rdi, %rsi
	jmp	.LBB18_25
.LBB18_21:
	movq	40(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	leaq	136(%rdi), %rsi
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB18_25
.LBB18_23:
	movq	40(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	$-10002, %esi           # imm = 0xD8EE
	subl	%edx, %esi
	movzbl	11(%rcx), %edx
	cmpl	%edx, %esi
	jg	.LBB18_27
# BB#24:
	movslq	%esi, %rdx
	shlq	$4, %rdx
	leaq	24(%rcx,%rdx), %rsi
.LBB18_25:                              # %index2adr.exit13
	movl	$luaO_nilobject_, %ecx
	cmpq	%rcx, %rax
	je	.LBB18_27
# BB#26:                                # %index2adr.exit13
	movl	$luaO_nilobject_, %ecx
	cmpq	%rcx, %rsi
	je	.LBB18_27
# BB#28:
	movq	%rax, %rdi
	jmp	luaO_rawequalObj        # TAILCALL
.LBB18_27:                              # %index2adr.exit13.thread
	xorl	%eax, %eax
	retq
.Lfunc_end18:
	.size	lua_rawequal, .Lfunc_end18-lua_rawequal
	.cfi_endproc

	.globl	lua_equal
	.p2align	4, 0x90
	.type	lua_equal,@function
lua_equal:                              # @lua_equal
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB19_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rdi), %rax
	movl	$luaO_nilobject_, %esi
	cmovbq	%rax, %rsi
	testl	%edx, %edx
	jg	.LBB19_14
	jmp	.LBB19_15
.LBB19_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB19_4
# BB#3:
	movslq	%esi, %rsi
	shlq	$4, %rsi
	addq	16(%rdi), %rsi
	testl	%edx, %edx
	jg	.LBB19_14
	jmp	.LBB19_15
.LBB19_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB19_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB19_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB19_10
# BB#7:
	movl	$160, %esi
	addq	32(%rdi), %rsi
	testl	%edx, %edx
	jg	.LBB19_14
	jmp	.LBB19_15
.LBB19_9:
	leaq	120(%rdi), %rsi
	testl	%edx, %edx
	jg	.LBB19_14
	jmp	.LBB19_15
.LBB19_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rdi), %rsi
	movq	24(%rax), %rax
	movq	%rax, 136(%rdi)
	movl	$5, 144(%rdi)
	testl	%edx, %edx
	jg	.LBB19_14
	jmp	.LBB19_15
.LBB19_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %esi
	cmpl	%esi, %ecx
	jle	.LBB19_12
# BB#11:
	movl	$luaO_nilobject_, %esi
	testl	%edx, %edx
	jg	.LBB19_14
	jmp	.LBB19_15
.LBB19_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rsi
	testl	%edx, %edx
	jle	.LBB19_15
.LBB19_14:
	movq	24(%rdi), %rax
	movslq	%edx, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rdi), %rax
	movl	$luaO_nilobject_, %edx
	cmovbq	%rax, %rdx
	jmp	.LBB19_25
.LBB19_15:
	cmpl	$-9999, %edx            # imm = 0xD8F1
	jl	.LBB19_17
# BB#16:
	movslq	%edx, %rdx
	shlq	$4, %rdx
	addq	16(%rdi), %rdx
	jmp	.LBB19_25
.LBB19_17:
	cmpl	$-10002, %edx           # imm = 0xD8EE
	je	.LBB19_22
# BB#18:
	cmpl	$-10001, %edx           # imm = 0xD8EF
	je	.LBB19_21
# BB#19:
	cmpl	$-10000, %edx           # imm = 0xD8F0
	jne	.LBB19_23
# BB#20:
	movl	$160, %edx
	addq	32(%rdi), %rdx
	jmp	.LBB19_25
.LBB19_22:
	leaq	120(%rdi), %rdx
	jmp	.LBB19_25
.LBB19_21:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rdi), %rdx
	movq	24(%rax), %rax
	movq	%rax, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB19_25
.LBB19_23:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %r8
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%edx, %ecx
	movzbl	11(%r8), %edx
	xorl	%eax, %eax
	cmpl	%edx, %ecx
	jg	.LBB19_31
# BB#24:
	movslq	%ecx, %rax
	shlq	$4, %rax
	leaq	24(%r8,%rax), %rdx
.LBB19_25:                              # %index2adr.exit19
	movl	$luaO_nilobject_, %ecx
	xorl	%eax, %eax
	cmpq	%rcx, %rsi
	je	.LBB19_31
# BB#26:                                # %index2adr.exit19
	movl	$luaO_nilobject_, %ecx
	cmpq	%rcx, %rdx
	je	.LBB19_31
# BB#27:
	movl	8(%rsi), %eax
	cmpl	8(%rdx), %eax
	jne	.LBB19_28
# BB#29:
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 16
	callq	luaV_equalval
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	jmp	.LBB19_30
.LBB19_28:
	xorl	%eax, %eax
.LBB19_30:
	movzbl	%al, %eax
.LBB19_31:                              # %index2adr.exit19.thread
	retq
.Lfunc_end19:
	.size	lua_equal, .Lfunc_end19-lua_equal
	.cfi_endproc

	.globl	lua_lessthan
	.p2align	4, 0x90
	.type	lua_lessthan,@function
lua_lessthan:                           # @lua_lessthan
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB20_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rdi), %rax
	movl	$luaO_nilobject_, %esi
	cmovbq	%rax, %rsi
	testl	%edx, %edx
	jg	.LBB20_14
	jmp	.LBB20_15
.LBB20_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB20_4
# BB#3:
	movslq	%esi, %rsi
	shlq	$4, %rsi
	addq	16(%rdi), %rsi
	testl	%edx, %edx
	jg	.LBB20_14
	jmp	.LBB20_15
.LBB20_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB20_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB20_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB20_10
# BB#7:
	movl	$160, %esi
	addq	32(%rdi), %rsi
	testl	%edx, %edx
	jg	.LBB20_14
	jmp	.LBB20_15
.LBB20_9:
	leaq	120(%rdi), %rsi
	testl	%edx, %edx
	jg	.LBB20_14
	jmp	.LBB20_15
.LBB20_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rdi), %rsi
	movq	24(%rax), %rax
	movq	%rax, 136(%rdi)
	movl	$5, 144(%rdi)
	testl	%edx, %edx
	jg	.LBB20_14
	jmp	.LBB20_15
.LBB20_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %esi
	cmpl	%esi, %ecx
	jle	.LBB20_12
# BB#11:
	movl	$luaO_nilobject_, %esi
	testl	%edx, %edx
	jg	.LBB20_14
	jmp	.LBB20_15
.LBB20_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rsi
	testl	%edx, %edx
	jle	.LBB20_15
.LBB20_14:
	movq	24(%rdi), %rax
	movslq	%edx, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rdi), %rax
	movl	$luaO_nilobject_, %edx
	cmovbq	%rax, %rdx
	jmp	.LBB20_25
.LBB20_15:
	cmpl	$-9999, %edx            # imm = 0xD8F1
	jl	.LBB20_17
# BB#16:
	movslq	%edx, %rdx
	shlq	$4, %rdx
	addq	16(%rdi), %rdx
	jmp	.LBB20_25
.LBB20_17:
	cmpl	$-10002, %edx           # imm = 0xD8EE
	je	.LBB20_22
# BB#18:
	cmpl	$-10001, %edx           # imm = 0xD8EF
	je	.LBB20_21
# BB#19:
	cmpl	$-10000, %edx           # imm = 0xD8F0
	jne	.LBB20_23
# BB#20:
	movl	$160, %edx
	addq	32(%rdi), %rdx
	jmp	.LBB20_25
.LBB20_22:
	leaq	120(%rdi), %rdx
	jmp	.LBB20_25
.LBB20_21:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rdi), %rdx
	movq	24(%rax), %rax
	movq	%rax, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB20_25
.LBB20_23:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%edx, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jg	.LBB20_27
# BB#24:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rdx
.LBB20_25:                              # %index2adr.exit17
	movl	$luaO_nilobject_, %eax
	cmpq	%rax, %rsi
	je	.LBB20_27
# BB#26:                                # %index2adr.exit17
	movl	$luaO_nilobject_, %eax
	cmpq	%rax, %rdx
	je	.LBB20_27
# BB#28:
	jmp	luaV_lessthan           # TAILCALL
.LBB20_27:                              # %index2adr.exit17.thread
	xorl	%eax, %eax
	retq
.Lfunc_end20:
	.size	lua_lessthan, .Lfunc_end20-lua_lessthan
	.cfi_endproc

	.globl	lua_tonumber
	.p2align	4, 0x90
	.type	lua_tonumber,@function
lua_tonumber:                           # @lua_tonumber
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB21_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	cmpl	$3, 8(%rax)
	jne	.LBB21_14
	jmp	.LBB21_16
.LBB21_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB21_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	cmpl	$3, 8(%rax)
	jne	.LBB21_14
	jmp	.LBB21_16
.LBB21_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB21_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB21_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB21_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	cmpl	$3, 8(%rax)
	jne	.LBB21_14
	jmp	.LBB21_16
.LBB21_9:
	addq	$120, %rdi
	movq	%rdi, %rax
	cmpl	$3, 8(%rax)
	jne	.LBB21_14
	jmp	.LBB21_16
.LBB21_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	cmpl	$3, 8(%rax)
	jne	.LBB21_14
	jmp	.LBB21_16
.LBB21_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB21_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	cmpl	$3, 8(%rax)
	jne	.LBB21_14
	jmp	.LBB21_16
.LBB21_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
	cmpl	$3, 8(%rax)
	je	.LBB21_16
.LBB21_14:
	subq	$24, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 32
	leaq	8(%rsp), %rsi
	movq	%rax, %rdi
	callq	luaV_tonumber
	testq	%rax, %rax
	leaq	24(%rsp), %rsp
	je	.LBB21_15
.LBB21_16:
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	retq
.LBB21_15:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end21:
	.size	lua_tonumber, .Lfunc_end21-lua_tonumber
	.cfi_endproc

	.globl	lua_tointeger
	.p2align	4, 0x90
	.type	lua_tointeger,@function
lua_tointeger:                          # @lua_tointeger
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB22_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	cmpl	$3, 8(%rax)
	jne	.LBB22_14
	jmp	.LBB22_16
.LBB22_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB22_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	cmpl	$3, 8(%rax)
	jne	.LBB22_14
	jmp	.LBB22_16
.LBB22_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB22_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB22_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB22_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	cmpl	$3, 8(%rax)
	jne	.LBB22_14
	jmp	.LBB22_16
.LBB22_9:
	addq	$120, %rdi
	movq	%rdi, %rax
	cmpl	$3, 8(%rax)
	jne	.LBB22_14
	jmp	.LBB22_16
.LBB22_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	cmpl	$3, 8(%rax)
	jne	.LBB22_14
	jmp	.LBB22_16
.LBB22_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB22_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	cmpl	$3, 8(%rax)
	jne	.LBB22_14
	jmp	.LBB22_16
.LBB22_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
	cmpl	$3, 8(%rax)
	je	.LBB22_16
.LBB22_14:
	subq	$24, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 32
	leaq	8(%rsp), %rsi
	movq	%rax, %rdi
	callq	luaV_tonumber
	testq	%rax, %rax
	leaq	24(%rsp), %rsp
	je	.LBB22_15
.LBB22_16:
	cvttsd2si	(%rax), %rax
	retq
.LBB22_15:
	xorl	%eax, %eax
	retq
.Lfunc_end22:
	.size	lua_tointeger, .Lfunc_end22-lua_tointeger
	.cfi_endproc

	.globl	lua_toboolean
	.p2align	4, 0x90
	.type	lua_toboolean,@function
lua_toboolean:                          # @lua_toboolean
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB23_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB23_13
.LBB23_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB23_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB23_13
.LBB23_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB23_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB23_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB23_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB23_13
.LBB23_9:
	addq	$120, %rdi
	movq	%rdi, %rax
	jmp	.LBB23_13
.LBB23_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB23_13
.LBB23_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB23_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB23_13
.LBB23_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB23_13:                              # %index2adr.exit
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB23_14
# BB#15:                                # %index2adr.exit
	cmpl	$1, %ecx
	jne	.LBB23_17
# BB#16:
	cmpl	$0, (%rax)
	setne	%al
	movzbl	%al, %eax
	retq
.LBB23_14:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB23_17:                              # %.fold.split
	movb	$1, %al
	movzbl	%al, %eax
	retq
.Lfunc_end23:
	.size	lua_toboolean, .Lfunc_end23-lua_toboolean
	.cfi_endproc

	.globl	lua_tolstring
	.p2align	4, 0x90
	.type	lua_tolstring,@function
lua_tolstring:                          # @lua_tolstring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %rbx
	testl	%ebp, %ebp
	jle	.LBB24_2
# BB#1:
	movq	24(%rbx), %rax
	movslq	%ebp, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rbx), %rax
	movl	$luaO_nilobject_, %esi
	cmovbq	%rax, %rsi
	cmpl	$4, 8(%rsi)
	jne	.LBB24_14
	jmp	.LBB24_33
.LBB24_2:
	cmpl	$-9999, %ebp            # imm = 0xD8F1
	jl	.LBB24_4
# BB#3:
	movslq	%ebp, %rsi
	shlq	$4, %rsi
	addq	16(%rbx), %rsi
	cmpl	$4, 8(%rsi)
	jne	.LBB24_14
	jmp	.LBB24_33
.LBB24_4:
	cmpl	$-10002, %ebp           # imm = 0xD8EE
	je	.LBB24_9
# BB#5:
	cmpl	$-10001, %ebp           # imm = 0xD8EF
	je	.LBB24_8
# BB#6:
	cmpl	$-10000, %ebp           # imm = 0xD8F0
	jne	.LBB24_10
# BB#7:
	movl	$160, %esi
	addq	32(%rbx), %rsi
	cmpl	$4, 8(%rsi)
	jne	.LBB24_14
	jmp	.LBB24_33
.LBB24_9:
	leaq	120(%rbx), %rsi
	cmpl	$4, 8(%rsi)
	jne	.LBB24_14
	jmp	.LBB24_33
.LBB24_8:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rbx), %rsi
	movq	24(%rax), %rax
	movq	%rax, 136(%rbx)
	movl	$5, 144(%rbx)
	cmpl	$4, 8(%rsi)
	jne	.LBB24_14
	jmp	.LBB24_33
.LBB24_10:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%ebp, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB24_12
# BB#11:
	movl	$luaO_nilobject_, %esi
	cmpl	$4, 8(%rsi)
	jne	.LBB24_14
	jmp	.LBB24_33
.LBB24_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rsi
	cmpl	$4, 8(%rsi)
	je	.LBB24_33
.LBB24_14:
	movq	%rbx, %rdi
	callq	luaV_tostring
	testl	%eax, %eax
	je	.LBB24_15
# BB#18:
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB24_20
# BB#19:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB24_20:
	testl	%ebp, %ebp
	jle	.LBB24_22
# BB#21:
	movq	24(%rbx), %rax
	movslq	%ebp, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rbx), %rax
	movl	$luaO_nilobject_, %esi
	cmovbq	%rax, %rsi
	testq	%r14, %r14
	jne	.LBB24_35
	jmp	.LBB24_34
.LBB24_15:
	testq	%r14, %r14
	je	.LBB24_16
# BB#17:
	movq	$0, (%r14)
	xorl	%eax, %eax
	jmp	.LBB24_37
.LBB24_22:
	cmpl	$-9999, %ebp            # imm = 0xD8F1
	jl	.LBB24_24
# BB#23:
	movslq	%ebp, %rsi
	shlq	$4, %rsi
	addq	16(%rbx), %rsi
	testq	%r14, %r14
	jne	.LBB24_35
	jmp	.LBB24_34
.LBB24_16:
	xorl	%eax, %eax
	jmp	.LBB24_37
.LBB24_24:
	cmpl	$-10002, %ebp           # imm = 0xD8EE
	je	.LBB24_29
# BB#25:
	cmpl	$-10001, %ebp           # imm = 0xD8EF
	je	.LBB24_28
# BB#26:
	cmpl	$-10000, %ebp           # imm = 0xD8F0
	jne	.LBB24_30
# BB#27:
	movl	$160, %esi
	addq	32(%rbx), %rsi
	testq	%r14, %r14
	jne	.LBB24_35
	jmp	.LBB24_34
.LBB24_29:
	addq	$120, %rbx
	movq	%rbx, %rsi
	testq	%r14, %r14
	jne	.LBB24_35
	jmp	.LBB24_34
.LBB24_28:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rbx), %rsi
	movq	24(%rax), %rax
	movq	%rax, 136(%rbx)
	movl	$5, 144(%rbx)
	testq	%r14, %r14
	jne	.LBB24_35
	jmp	.LBB24_34
.LBB24_30:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%ebp, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB24_32
# BB#31:
	movl	$luaO_nilobject_, %esi
	testq	%r14, %r14
	jne	.LBB24_35
	jmp	.LBB24_34
.LBB24_32:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rsi
.LBB24_33:                              # %index2adr.exit20
	testq	%r14, %r14
	je	.LBB24_34
.LBB24_35:
	movq	(%rsi), %rax
	movq	16(%rax), %rcx
	movq	%rcx, (%r14)
	jmp	.LBB24_36
.LBB24_34:                              # %index2adr.exit20._crit_edge
	movq	(%rsi), %rax
.LBB24_36:
	addq	$24, %rax
.LBB24_37:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end24:
	.size	lua_tolstring, .Lfunc_end24-lua_tolstring
	.cfi_endproc

	.globl	lua_objlen
	.p2align	4, 0x90
	.type	lua_objlen,@function
lua_objlen:                             # @lua_objlen
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	testl	%esi, %esi
	jle	.LBB25_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rdi), %rax
	movl	$luaO_nilobject_, %ebx
	cmovbq	%rax, %rbx
	jmp	.LBB25_13
.LBB25_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB25_4
# BB#3:
	movslq	%esi, %rbx
	shlq	$4, %rbx
	addq	16(%rdi), %rbx
	jmp	.LBB25_13
.LBB25_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB25_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB25_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB25_10
# BB#7:
	movl	$160, %ebx
	addq	32(%rdi), %rbx
	jmp	.LBB25_13
.LBB25_9:
	leaq	120(%rdi), %rbx
	jmp	.LBB25_13
.LBB25_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rdi), %rbx
	movq	24(%rax), %rax
	movq	%rax, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB25_13
.LBB25_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB25_12
# BB#11:
	movl	$luaO_nilobject_, %ebx
	jmp	.LBB25_13
.LBB25_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rbx
.LBB25_13:                              # %index2adr.exit
	movl	8(%rbx), %ecx
	xorl	%eax, %eax
	addl	$-3, %ecx
	cmpl	$4, %ecx
	ja	.LBB25_20
# BB#14:                                # %index2adr.exit
	jmpq	*.LJTI25_0(,%rcx,8)
.LBB25_17:
	movq	%rbx, %rsi
	callq	luaV_tostring
	testl	%eax, %eax
	je	.LBB25_18
.LBB25_19:
	movq	(%rbx), %rax
	movq	16(%rax), %rax
.LBB25_20:
	popq	%rbx
	retq
.LBB25_16:
	movq	(%rbx), %rdi
	callq	luaH_getn
	cltq
	popq	%rbx
	retq
.LBB25_15:
	movq	(%rbx), %rax
	movq	32(%rax), %rax
	popq	%rbx
	retq
.LBB25_18:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end25:
	.size	lua_objlen, .Lfunc_end25-lua_objlen
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI25_0:
	.quad	.LBB25_17
	.quad	.LBB25_19
	.quad	.LBB25_16
	.quad	.LBB25_20
	.quad	.LBB25_15

	.text
	.globl	lua_tocfunction
	.p2align	4, 0x90
	.type	lua_tocfunction,@function
lua_tocfunction:                        # @lua_tocfunction
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB26_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	cmpl	$6, 8(%rax)
	je	.LBB26_15
	jmp	.LBB26_14
.LBB26_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB26_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	cmpl	$6, 8(%rax)
	je	.LBB26_15
	jmp	.LBB26_14
.LBB26_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB26_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB26_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB26_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	cmpl	$6, 8(%rax)
	je	.LBB26_15
	jmp	.LBB26_14
.LBB26_9:
	addq	$120, %rdi
	movq	%rdi, %rax
	cmpl	$6, 8(%rax)
	je	.LBB26_15
	jmp	.LBB26_14
.LBB26_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	cmpl	$6, 8(%rax)
	je	.LBB26_15
	jmp	.LBB26_14
.LBB26_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB26_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	cmpl	$6, 8(%rax)
	je	.LBB26_15
	jmp	.LBB26_14
.LBB26_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
	cmpl	$6, 8(%rax)
	jne	.LBB26_14
.LBB26_15:
	movq	(%rax), %rax
	cmpb	$0, 10(%rax)
	je	.LBB26_16
# BB#17:
	movq	32(%rax), %rax
	retq
.LBB26_14:
	xorl	%eax, %eax
	retq
.LBB26_16:
	xorl	%eax, %eax
	retq
.Lfunc_end26:
	.size	lua_tocfunction, .Lfunc_end26-lua_tocfunction
	.cfi_endproc

	.globl	lua_touserdata
	.p2align	4, 0x90
	.type	lua_touserdata,@function
lua_touserdata:                         # @lua_touserdata
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB27_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB27_13
.LBB27_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB27_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB27_13
.LBB27_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB27_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB27_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB27_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB27_13
.LBB27_9:
	addq	$120, %rdi
	movq	%rdi, %rax
	jmp	.LBB27_13
.LBB27_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB27_13
.LBB27_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB27_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB27_13
.LBB27_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB27_13:                              # %index2adr.exit
	movl	8(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB27_17
# BB#14:                                # %index2adr.exit
	cmpl	$7, %ecx
	jne	.LBB27_15
# BB#16:
	movq	(%rax), %rax
	addq	$40, %rax
	retq
.LBB27_17:
	movq	(%rax), %rax
	retq
.LBB27_15:
	xorl	%eax, %eax
	retq
.Lfunc_end27:
	.size	lua_touserdata, .Lfunc_end27-lua_touserdata
	.cfi_endproc

	.globl	lua_tothread
	.p2align	4, 0x90
	.type	lua_tothread,@function
lua_tothread:                           # @lua_tothread
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB28_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	cmpl	$8, 8(%rax)
	je	.LBB28_15
	jmp	.LBB28_14
.LBB28_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB28_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	cmpl	$8, 8(%rax)
	je	.LBB28_15
	jmp	.LBB28_14
.LBB28_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB28_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB28_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB28_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	cmpl	$8, 8(%rax)
	je	.LBB28_15
	jmp	.LBB28_14
.LBB28_9:
	addq	$120, %rdi
	movq	%rdi, %rax
	cmpl	$8, 8(%rax)
	je	.LBB28_15
	jmp	.LBB28_14
.LBB28_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	cmpl	$8, 8(%rax)
	je	.LBB28_15
	jmp	.LBB28_14
.LBB28_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB28_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	cmpl	$8, 8(%rax)
	je	.LBB28_15
	jmp	.LBB28_14
.LBB28_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
	cmpl	$8, 8(%rax)
	jne	.LBB28_14
.LBB28_15:
	movq	(%rax), %rax
	retq
.LBB28_14:
	xorl	%eax, %eax
	retq
.Lfunc_end28:
	.size	lua_tothread, .Lfunc_end28-lua_tothread
	.cfi_endproc

	.globl	lua_topointer
	.p2align	4, 0x90
	.type	lua_topointer,@function
lua_topointer:                          # @lua_topointer
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB29_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rdi), %rax
	movl	$luaO_nilobject_, %ecx
	cmovbq	%rax, %rcx
	jmp	.LBB29_13
.LBB29_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB29_4
# BB#3:
	movslq	%esi, %rcx
	shlq	$4, %rcx
	addq	16(%rdi), %rcx
.LBB29_13:                              # %index2adr.exit
	movl	8(%rcx), %edx
	xorl	%eax, %eax
	addl	$-2, %edx
	cmpl	$6, %edx
	ja	.LBB29_34
# BB#14:                                # %index2adr.exit
	jmpq	*.LJTI29_0(,%rdx,8)
.LBB29_15:
	movq	(%rcx), %rax
	retq
.LBB29_16:
	testl	%esi, %esi
	jle	.LBB29_18
# BB#17:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB29_29
.LBB29_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB29_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB29_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB29_10
# BB#7:
	movl	$160, %ecx
	addq	32(%rdi), %rcx
	jmp	.LBB29_13
.LBB29_18:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB29_20
# BB#19:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB29_29
.LBB29_20:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB29_25
# BB#21:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB29_24
# BB#22:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB29_26
# BB#23:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB29_29
.LBB29_9:
	leaq	120(%rdi), %rcx
	jmp	.LBB29_13
.LBB29_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rdi), %rcx
	movq	24(%rax), %rax
	movq	%rax, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB29_13
.LBB29_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB29_12
# BB#11:
	movl	$luaO_nilobject_, %ecx
	jmp	.LBB29_13
.LBB29_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rcx
	jmp	.LBB29_13
.LBB29_25:
	addq	$120, %rdi
	movq	%rdi, %rax
	jmp	.LBB29_29
.LBB29_24:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB29_29
.LBB29_26:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB29_28
# BB#27:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB29_29
.LBB29_28:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB29_29:                              # %index2adr.exit.i
	movl	8(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB29_33
# BB#30:                                # %index2adr.exit.i
	cmpl	$7, %ecx
	jne	.LBB29_31
# BB#32:
	movq	(%rax), %rax
	addq	$40, %rax
	retq
.LBB29_33:
	movq	(%rax), %rax
.LBB29_34:                              # %lua_touserdata.exit
	retq
.LBB29_31:
	xorl	%eax, %eax
	retq
.Lfunc_end29:
	.size	lua_topointer, .Lfunc_end29-lua_topointer
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI29_0:
	.quad	.LBB29_16
	.quad	.LBB29_34
	.quad	.LBB29_34
	.quad	.LBB29_15
	.quad	.LBB29_15
	.quad	.LBB29_16
	.quad	.LBB29_15

	.text
	.globl	lua_pushnil
	.p2align	4, 0x90
	.type	lua_pushnil,@function
lua_pushnil:                            # @lua_pushnil
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movl	$0, 8(%rax)
	addq	$16, %rax
	movq	%rax, 16(%rdi)
	retq
.Lfunc_end30:
	.size	lua_pushnil, .Lfunc_end30-lua_pushnil
	.cfi_endproc

	.globl	lua_pushnumber
	.p2align	4, 0x90
	.type	lua_pushnumber,@function
lua_pushnumber:                         # @lua_pushnumber
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movsd	%xmm0, (%rax)
	movl	$3, 8(%rax)
	addq	$16, %rax
	movq	%rax, 16(%rdi)
	retq
.Lfunc_end31:
	.size	lua_pushnumber, .Lfunc_end31-lua_pushnumber
	.cfi_endproc

	.globl	lua_pushinteger
	.p2align	4, 0x90
	.type	lua_pushinteger,@function
lua_pushinteger:                        # @lua_pushinteger
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	cvtsi2sdq	%rsi, %xmm0
	movsd	%xmm0, (%rax)
	movl	$3, 8(%rax)
	addq	$16, %rax
	movq	%rax, 16(%rdi)
	retq
.Lfunc_end32:
	.size	lua_pushinteger, .Lfunc_end32-lua_pushinteger
	.cfi_endproc

	.globl	lua_pushlstring
	.p2align	4, 0x90
	.type	lua_pushlstring,@function
lua_pushlstring:                        # @lua_pushlstring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 48
.Lcfi30:
	.cfi_offset %rbx, -40
.Lcfi31:
	.cfi_offset %r12, -32
.Lcfi32:
	.cfi_offset %r14, -24
.Lcfi33:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB33_2
# BB#1:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB33_2:
	movq	16(%rbx), %r12
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	luaS_newlstr
	movq	%rax, (%r12)
	movl	$4, 8(%r12)
	addq	$16, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end33:
	.size	lua_pushlstring, .Lfunc_end33-lua_pushlstring
	.cfi_endproc

	.globl	lua_pushstring
	.p2align	4, 0x90
	.type	lua_pushstring,@function
lua_pushstring:                         # @lua_pushstring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 48
.Lcfi39:
	.cfi_offset %rbx, -40
.Lcfi40:
	.cfi_offset %r12, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%r14, %r14
	je	.LBB34_1
# BB#2:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %r15
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB34_4
# BB#3:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB34_4:                               # %lua_pushlstring.exit
	movq	16(%rbx), %r12
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	luaS_newlstr
	movq	%rax, (%r12)
	movl	$4, 8(%r12)
	movq	16(%rbx), %rax
	leaq	16(%rbx), %rbx
	jmp	.LBB34_5
.LBB34_1:
	movq	16(%rbx), %rax
	addq	$16, %rbx
	movl	$0, 8(%rax)
.LBB34_5:
	addq	$16, %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end34:
	.size	lua_pushstring, .Lfunc_end34-lua_pushstring
	.cfi_endproc

	.globl	lua_pushvfstring
	.p2align	4, 0x90
	.type	lua_pushvfstring,@function
lua_pushvfstring:                       # @lua_pushvfstring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB35_2
# BB#1:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB35_2:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	luaO_pushvfstring       # TAILCALL
.Lfunc_end35:
	.size	lua_pushvfstring, .Lfunc_end35-lua_pushvfstring
	.cfi_endproc

	.globl	lua_pushfstring
	.p2align	4, 0x90
	.type	lua_pushfstring,@function
lua_pushfstring:                        # @lua_pushfstring
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	subq	$216, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 240
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testb	%al, %al
	je	.LBB36_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB36_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB36_4
# BB#3:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB36_4:
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	240(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$16, (%rsp)
	movq	%rsp, %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	luaO_pushvfstring
	addq	$216, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end36:
	.size	lua_pushfstring, .Lfunc_end36-lua_pushfstring
	.cfi_endproc

	.globl	lua_pushcclosure
	.p2align	4, 0x90
	.type	lua_pushcclosure,@function
lua_pushcclosure:                       # @lua_pushcclosure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB37_2
# BB#1:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB37_2:
	movq	40(%rbx), %rax
	cmpq	80(%rbx), %rax
	je	.LBB37_3
# BB#4:
	movq	8(%rax), %rax
	movq	(%rax), %rax
	addq	$24, %rax
	jmp	.LBB37_5
.LBB37_3:
	leaq	120(%rbx), %rax
.LBB37_5:                               # %getcurrenv.exit
	movq	(%rax), %rdx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	luaF_newCclosure
	movq	%r14, 32(%rax)
	movq	16(%rbx), %rdx
	movslq	%ebp, %rcx
	movq	%rcx, %rsi
	shlq	$4, %rsi
	subq	%rsi, %rdx
	movq	%rdx, 16(%rbx)
	testl	%ecx, %ecx
	je	.LBB37_11
# BB#6:                                 # %.lr.ph.preheader
	testb	$1, %bpl
	je	.LBB37_8
# BB#7:                                 # %.lr.ph.prol
	decq	%rcx
	movq	%rcx, %rsi
	shlq	$4, %rsi
	movq	(%rdx,%rsi), %rdi
	movq	%rdi, 40(%rax,%rsi)
	movl	8(%rdx,%rsi), %edx
	movl	%edx, 48(%rax,%rsi)
	movq	16(%rbx), %rdx
.LBB37_8:                               # %.lr.ph.prol.loopexit
	cmpl	$1, %ebp
	je	.LBB37_11
# BB#9:                                 # %.lr.ph.preheader.new
	movl	%ecx, %esi
	negl	%esi
	shlq	$4, %rcx
	.p2align	4, 0x90
.LBB37_10:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rdx,%rcx), %rdi
	movq	%rdi, 24(%rax,%rcx)
	movl	-8(%rdx,%rcx), %edx
	movl	%edx, 32(%rax,%rcx)
	movq	16(%rbx), %rdx
	movq	-32(%rdx,%rcx), %rdi
	movq	%rdi, 8(%rax,%rcx)
	movl	-24(%rdx,%rcx), %edx
	movl	%edx, 16(%rax,%rcx)
	movq	16(%rbx), %rdx
	addq	$-32, %rcx
	addl	$2, %esi
	jne	.LBB37_10
.LBB37_11:                              # %._crit_edge
	movq	%rax, (%rdx)
	movl	$6, 8(%rdx)
	addq	$16, 16(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end37:
	.size	lua_pushcclosure, .Lfunc_end37-lua_pushcclosure
	.cfi_endproc

	.globl	lua_pushboolean
	.p2align	4, 0x90
	.type	lua_pushboolean,@function
lua_pushboolean:                        # @lua_pushboolean
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	xorl	%ecx, %ecx
	testl	%esi, %esi
	setne	%cl
	movl	%ecx, (%rax)
	movl	$1, 8(%rax)
	addq	$16, %rax
	movq	%rax, 16(%rdi)
	retq
.Lfunc_end38:
	.size	lua_pushboolean, .Lfunc_end38-lua_pushboolean
	.cfi_endproc

	.globl	lua_pushlightuserdata
	.p2align	4, 0x90
	.type	lua_pushlightuserdata,@function
lua_pushlightuserdata:                  # @lua_pushlightuserdata
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movq	%rsi, (%rax)
	movl	$2, 8(%rax)
	addq	$16, 16(%rdi)
	retq
.Lfunc_end39:
	.size	lua_pushlightuserdata, .Lfunc_end39-lua_pushlightuserdata
	.cfi_endproc

	.globl	lua_pushthread
	.p2align	4, 0x90
	.type	lua_pushthread,@function
lua_pushthread:                         # @lua_pushthread
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movq	%rdi, (%rax)
	movl	$8, 8(%rax)
	addq	$16, 16(%rdi)
	movq	32(%rdi), %rcx
	xorl	%eax, %eax
	cmpq	%rdi, 176(%rcx)
	sete	%al
	retq
.Lfunc_end40:
	.size	lua_pushthread, .Lfunc_end40-lua_pushthread
	.cfi_endproc

	.globl	lua_gettable
	.p2align	4, 0x90
	.type	lua_gettable,@function
lua_gettable:                           # @lua_gettable
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB41_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rdi), %rax
	movl	$luaO_nilobject_, %esi
	cmovbq	%rax, %rsi
	jmp	.LBB41_13
.LBB41_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB41_4
# BB#3:
	movslq	%esi, %rsi
	shlq	$4, %rsi
	addq	16(%rdi), %rsi
	jmp	.LBB41_13
.LBB41_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB41_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB41_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB41_10
# BB#7:
	movl	$160, %esi
	addq	32(%rdi), %rsi
	jmp	.LBB41_13
.LBB41_9:
	leaq	120(%rdi), %rsi
	jmp	.LBB41_13
.LBB41_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rdi), %rsi
	movq	24(%rax), %rax
	movq	%rax, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB41_13
.LBB41_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB41_12
# BB#11:
	movl	$luaO_nilobject_, %esi
	jmp	.LBB41_13
.LBB41_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rsi
.LBB41_13:                              # %index2adr.exit
	movq	16(%rdi), %rdx
	addq	$-16, %rdx
	movq	%rdx, %rcx
	jmp	luaV_gettable           # TAILCALL
.Lfunc_end41:
	.size	lua_gettable, .Lfunc_end41-lua_gettable
	.cfi_endproc

	.globl	lua_getfield
	.p2align	4, 0x90
	.type	lua_getfield,@function
lua_getfield:                           # @lua_getfield
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 48
.Lcfi64:
	.cfi_offset %rbx, -32
.Lcfi65:
	.cfi_offset %r14, -24
.Lcfi66:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %r15
	testl	%esi, %esi
	jle	.LBB42_2
# BB#1:
	movq	24(%r15), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%r15), %rax
	movl	$luaO_nilobject_, %ebx
	cmovbq	%rax, %rbx
	jmp	.LBB42_13
.LBB42_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB42_4
# BB#3:
	movslq	%esi, %rbx
	shlq	$4, %rbx
	addq	16(%r15), %rbx
	jmp	.LBB42_13
.LBB42_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB42_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB42_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB42_10
# BB#7:
	movl	$160, %ebx
	addq	32(%r15), %rbx
	jmp	.LBB42_13
.LBB42_9:
	leaq	120(%r15), %rbx
	jmp	.LBB42_13
.LBB42_8:
	movq	40(%r15), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%r15), %rbx
	movq	24(%rax), %rax
	movq	%rax, 136(%r15)
	movl	$5, 144(%r15)
	jmp	.LBB42_13
.LBB42_10:
	movq	40(%r15), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB42_12
# BB#11:
	movl	$luaO_nilobject_, %ebx
	jmp	.LBB42_13
.LBB42_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rbx
.LBB42_13:                              # %index2adr.exit
	movq	%r14, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	luaS_newlstr
	movq	%rax, (%rsp)
	movl	$4, 8(%rsp)
	movq	16(%r15), %rcx
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	luaV_gettable
	addq	$16, 16(%r15)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end42:
	.size	lua_getfield, .Lfunc_end42-lua_getfield
	.cfi_endproc

	.globl	lua_rawget
	.p2align	4, 0x90
	.type	lua_rawget,@function
lua_rawget:                             # @lua_rawget
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 16
.Lcfi68:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testl	%esi, %esi
	jle	.LBB43_2
# BB#1:
	movq	24(%rbx), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rbx), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB43_13
.LBB43_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB43_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rbx), %rax
	jmp	.LBB43_13
.LBB43_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB43_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB43_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB43_10
# BB#7:
	movl	$160, %eax
	addq	32(%rbx), %rax
	jmp	.LBB43_13
.LBB43_9:
	leaq	120(%rbx), %rax
	jmp	.LBB43_13
.LBB43_8:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rbx), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rbx)
	movl	$5, 144(%rbx)
	jmp	.LBB43_13
.LBB43_10:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB43_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB43_13
.LBB43_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB43_13:                              # %index2adr.exit
	movq	(%rax), %rdi
	movq	16(%rbx), %rsi
	addq	$-16, %rsi
	callq	luaH_get
	movq	16(%rbx), %rcx
	movq	(%rax), %rdx
	movq	%rdx, -16(%rcx)
	movl	8(%rax), %eax
	movl	%eax, -8(%rcx)
	popq	%rbx
	retq
.Lfunc_end43:
	.size	lua_rawget, .Lfunc_end43-lua_rawget
	.cfi_endproc

	.globl	lua_rawgeti
	.p2align	4, 0x90
	.type	lua_rawgeti,@function
lua_rawgeti:                            # @lua_rawgeti
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 16
.Lcfi70:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testl	%esi, %esi
	jle	.LBB44_2
# BB#1:
	movq	24(%rbx), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rbx), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB44_13
.LBB44_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB44_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rbx), %rax
	jmp	.LBB44_13
.LBB44_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB44_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB44_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB44_10
# BB#7:
	movl	$160, %eax
	addq	32(%rbx), %rax
	jmp	.LBB44_13
.LBB44_9:
	leaq	120(%rbx), %rax
	jmp	.LBB44_13
.LBB44_8:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rbx), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rbx)
	movl	$5, 144(%rbx)
	jmp	.LBB44_13
.LBB44_10:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %esi
	cmpl	%esi, %ecx
	jle	.LBB44_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB44_13
.LBB44_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB44_13:                              # %index2adr.exit
	movq	(%rax), %rdi
	movl	%edx, %esi
	callq	luaH_getnum
	movq	16(%rbx), %rcx
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rcx)
	addq	$16, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end44:
	.size	lua_rawgeti, .Lfunc_end44-lua_rawgeti
	.cfi_endproc

	.globl	lua_createtable
	.p2align	4, 0x90
	.type	lua_createtable,@function
lua_createtable:                        # @lua_createtable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 48
.Lcfi76:
	.cfi_offset %rbx, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB45_2
# BB#1:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB45_2:
	movq	16(%rbx), %rbp
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movl	%r14d, %edx
	callq	luaH_new
	movq	%rax, (%rbp)
	movl	$5, 8(%rbp)
	addq	$16, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end45:
	.size	lua_createtable, .Lfunc_end45-lua_createtable
	.cfi_endproc

	.globl	lua_getmetatable
	.p2align	4, 0x90
	.type	lua_getmetatable,@function
lua_getmetatable:                       # @lua_getmetatable
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB46_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB46_13
.LBB46_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB46_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB46_13
.LBB46_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB46_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB46_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB46_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB46_13
.LBB46_9:
	leaq	120(%rdi), %rax
	jmp	.LBB46_13
.LBB46_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB46_13
.LBB46_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB46_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB46_13
.LBB46_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB46_13:                              # %index2adr.exit
	movslq	8(%rax), %rcx
	cmpq	$7, %rcx
	je	.LBB46_15
# BB#14:                                # %index2adr.exit
	cmpl	$5, %ecx
	jne	.LBB46_16
.LBB46_15:
	movq	(%rax), %rax
	addq	$16, %rax
	jmp	.LBB46_17
.LBB46_16:
	movq	32(%rdi), %rax
	leaq	224(%rax,%rcx,8), %rax
.LBB46_17:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB46_18
# BB#19:
	movq	16(%rdi), %rcx
	movq	%rax, (%rcx)
	movl	$5, 8(%rcx)
	addq	$16, 16(%rdi)
	movl	$1, %eax
	retq
.LBB46_18:
	xorl	%eax, %eax
	retq
.Lfunc_end46:
	.size	lua_getmetatable, .Lfunc_end46-lua_getmetatable
	.cfi_endproc

	.globl	lua_getfenv
	.p2align	4, 0x90
	.type	lua_getfenv,@function
lua_getfenv:                            # @lua_getfenv
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB47_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB47_13
.LBB47_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB47_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB47_13
.LBB47_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB47_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB47_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB47_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB47_13
.LBB47_9:
	leaq	120(%rdi), %rax
	jmp	.LBB47_13
.LBB47_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB47_13
.LBB47_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB47_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB47_13
.LBB47_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB47_13:                              # %index2adr.exit
	movl	8(%rax), %ecx
	cmpl	$8, %ecx
	je	.LBB47_17
# BB#14:                                # %index2adr.exit
	cmpl	$7, %ecx
	je	.LBB47_16
# BB#15:                                # %index2adr.exit
	cmpl	$6, %ecx
	jne	.LBB47_18
.LBB47_16:
	movq	16(%rdi), %rcx
	addq	$16, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movq	%rax, (%rcx)
	movl	$5, %eax
	jmp	.LBB47_19
.LBB47_17:
	movq	(%rax), %rax
	movq	16(%rdi), %rcx
	addq	$16, %rdi
	movq	120(%rax), %rdx
	movq	%rdx, (%rcx)
	movl	128(%rax), %eax
	jmp	.LBB47_19
.LBB47_18:
	movq	16(%rdi), %rcx
	addq	$16, %rdi
	xorl	%eax, %eax
.LBB47_19:
	movl	%eax, 8(%rcx)
	addq	$16, (%rdi)
	retq
.Lfunc_end47:
	.size	lua_getfenv, .Lfunc_end47-lua_getfenv
	.cfi_endproc

	.globl	lua_settable
	.p2align	4, 0x90
	.type	lua_settable,@function
lua_settable:                           # @lua_settable
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 16
.Lcfi81:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testl	%esi, %esi
	jle	.LBB48_2
# BB#1:
	movq	24(%rbx), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rbx), %rax
	movl	$luaO_nilobject_, %esi
	cmovbq	%rax, %rsi
	jmp	.LBB48_13
.LBB48_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB48_4
# BB#3:
	movslq	%esi, %rsi
	shlq	$4, %rsi
	addq	16(%rbx), %rsi
	jmp	.LBB48_13
.LBB48_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB48_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB48_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB48_10
# BB#7:
	movl	$160, %esi
	addq	32(%rbx), %rsi
	jmp	.LBB48_13
.LBB48_9:
	leaq	120(%rbx), %rsi
	jmp	.LBB48_13
.LBB48_8:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rbx), %rsi
	movq	24(%rax), %rax
	movq	%rax, 136(%rbx)
	movl	$5, 144(%rbx)
	jmp	.LBB48_13
.LBB48_10:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB48_12
# BB#11:
	movl	$luaO_nilobject_, %esi
	jmp	.LBB48_13
.LBB48_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rsi
.LBB48_13:                              # %index2adr.exit
	movq	16(%rbx), %rcx
	leaq	-32(%rcx), %rdx
	addq	$-16, %rcx
	movq	%rbx, %rdi
	callq	luaV_settable
	addq	$-32, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end48:
	.size	lua_settable, .Lfunc_end48-lua_settable
	.cfi_endproc

	.globl	lua_setfield
	.p2align	4, 0x90
	.type	lua_setfield,@function
lua_setfield:                           # @lua_setfield
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 48
.Lcfi86:
	.cfi_offset %rbx, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %r15
	testl	%esi, %esi
	jle	.LBB49_2
# BB#1:
	movq	24(%r15), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%r15), %rax
	movl	$luaO_nilobject_, %ebx
	cmovbq	%rax, %rbx
	jmp	.LBB49_13
.LBB49_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB49_4
# BB#3:
	movslq	%esi, %rbx
	shlq	$4, %rbx
	addq	16(%r15), %rbx
	jmp	.LBB49_13
.LBB49_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB49_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB49_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB49_10
# BB#7:
	movl	$160, %ebx
	addq	32(%r15), %rbx
	jmp	.LBB49_13
.LBB49_9:
	leaq	120(%r15), %rbx
	jmp	.LBB49_13
.LBB49_8:
	movq	40(%r15), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%r15), %rbx
	movq	24(%rax), %rax
	movq	%rax, 136(%r15)
	movl	$5, 144(%r15)
	jmp	.LBB49_13
.LBB49_10:
	movq	40(%r15), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB49_12
# BB#11:
	movl	$luaO_nilobject_, %ebx
	jmp	.LBB49_13
.LBB49_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rbx
.LBB49_13:                              # %index2adr.exit
	movq	%r14, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	luaS_newlstr
	movq	%rax, (%rsp)
	movl	$4, 8(%rsp)
	movq	16(%r15), %rcx
	addq	$-16, %rcx
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	luaV_settable
	addq	$-16, 16(%r15)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end49:
	.size	lua_setfield, .Lfunc_end49-lua_setfield
	.cfi_endproc

	.globl	lua_rawset
	.p2align	4, 0x90
	.type	lua_rawset,@function
lua_rawset:                             # @lua_rawset
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 32
.Lcfi92:
	.cfi_offset %rbx, -32
.Lcfi93:
	.cfi_offset %r14, -24
.Lcfi94:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.LBB50_2
# BB#1:
	movq	24(%r14), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%r14), %rax
	movl	$luaO_nilobject_, %r15d
	cmovbq	%rax, %r15
	jmp	.LBB50_13
.LBB50_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB50_4
# BB#3:
	movslq	%esi, %r15
	shlq	$4, %r15
	addq	16(%r14), %r15
	jmp	.LBB50_13
.LBB50_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB50_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB50_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB50_10
# BB#7:
	movl	$160, %r15d
	addq	32(%r14), %r15
	jmp	.LBB50_13
.LBB50_9:
	leaq	120(%r14), %r15
	jmp	.LBB50_13
.LBB50_8:
	movq	40(%r14), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%r14), %r15
	movq	24(%rax), %rax
	movq	%rax, 136(%r14)
	movl	$5, 144(%r14)
	jmp	.LBB50_13
.LBB50_10:
	movq	40(%r14), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB50_12
# BB#11:
	movl	$luaO_nilobject_, %r15d
	jmp	.LBB50_13
.LBB50_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %r15
.LBB50_13:                              # %index2adr.exit
	movq	16(%r14), %rbx
	movq	(%r15), %rsi
	leaq	-32(%rbx), %rdx
	movq	%r14, %rdi
	callq	luaH_set
	movq	-16(%rbx), %rcx
	movq	%rcx, (%rax)
	movl	-8(%rbx), %ecx
	movl	%ecx, 8(%rax)
	movq	16(%r14), %rax
	cmpl	$4, -8(%rax)
	jl	.LBB50_17
# BB#14:
	movq	-16(%rax), %rcx
	testb	$3, 9(%rcx)
	je	.LBB50_17
# BB#15:
	movq	(%r15), %rsi
	testb	$4, 9(%rsi)
	je	.LBB50_17
# BB#16:
	movq	%r14, %rdi
	callq	luaC_barrierback
	movq	16(%r14), %rax
.LBB50_17:
	addq	$-32, %rax
	movq	%rax, 16(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end50:
	.size	lua_rawset, .Lfunc_end50-lua_rawset
	.cfi_endproc

	.globl	lua_rawseti
	.p2align	4, 0x90
	.type	lua_rawseti,@function
lua_rawseti:                            # @lua_rawseti
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 32
.Lcfi98:
	.cfi_offset %rbx, -32
.Lcfi99:
	.cfi_offset %r14, -24
.Lcfi100:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.LBB51_2
# BB#1:
	movq	24(%r14), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%r14), %rax
	movl	$luaO_nilobject_, %r15d
	cmovbq	%rax, %r15
	jmp	.LBB51_13
.LBB51_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB51_4
# BB#3:
	movslq	%esi, %r15
	shlq	$4, %r15
	addq	16(%r14), %r15
	jmp	.LBB51_13
.LBB51_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB51_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB51_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB51_10
# BB#7:
	movl	$160, %r15d
	addq	32(%r14), %r15
	jmp	.LBB51_13
.LBB51_9:
	leaq	120(%r14), %r15
	jmp	.LBB51_13
.LBB51_8:
	movq	40(%r14), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%r14), %r15
	movq	24(%rax), %rax
	movq	%rax, 136(%r14)
	movl	$5, 144(%r14)
	jmp	.LBB51_13
.LBB51_10:
	movq	40(%r14), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %esi
	cmpl	%esi, %ecx
	jle	.LBB51_12
# BB#11:
	movl	$luaO_nilobject_, %r15d
	jmp	.LBB51_13
.LBB51_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %r15
.LBB51_13:                              # %index2adr.exit
	movq	16(%r14), %rbx
	movq	(%r15), %rsi
	movq	%r14, %rdi
	callq	luaH_setnum
	movq	-16(%rbx), %rcx
	movq	%rcx, (%rax)
	movl	-8(%rbx), %ecx
	movl	%ecx, 8(%rax)
	movq	16(%r14), %rax
	cmpl	$4, -8(%rax)
	jl	.LBB51_17
# BB#14:
	movq	-16(%rax), %rcx
	testb	$3, 9(%rcx)
	je	.LBB51_17
# BB#15:
	movq	(%r15), %rsi
	testb	$4, 9(%rsi)
	je	.LBB51_17
# BB#16:
	movq	%r14, %rdi
	callq	luaC_barrierback
	movq	16(%r14), %rax
.LBB51_17:
	addq	$-16, %rax
	movq	%rax, 16(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end51:
	.size	lua_rawseti, .Lfunc_end51-lua_rawseti
	.cfi_endproc

	.globl	lua_setmetatable
	.p2align	4, 0x90
	.type	lua_setmetatable,@function
lua_setmetatable:                       # @lua_setmetatable
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 16
.Lcfi102:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testl	%esi, %esi
	jle	.LBB52_2
# BB#1:
	movq	24(%rbx), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rbx), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB52_13
.LBB52_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB52_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rbx), %rax
	jmp	.LBB52_13
.LBB52_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB52_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB52_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB52_10
# BB#7:
	movl	$160, %eax
	addq	32(%rbx), %rax
	jmp	.LBB52_13
.LBB52_9:
	leaq	120(%rbx), %rax
	jmp	.LBB52_13
.LBB52_8:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rbx), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rbx)
	movl	$5, 144(%rbx)
	jmp	.LBB52_13
.LBB52_10:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB52_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB52_13
.LBB52_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB52_13:                              # %index2adr.exit
	movq	16(%rbx), %rcx
	cmpl	$0, -8(%rcx)
	je	.LBB52_14
# BB#15:
	movq	-16(%rcx), %rdx
	jmp	.LBB52_16
.LBB52_14:
	xorl	%edx, %edx
.LBB52_16:
	movslq	8(%rax), %rcx
	cmpq	$7, %rcx
	je	.LBB52_22
# BB#17:
	cmpl	$5, %ecx
	jne	.LBB52_26
# BB#18:
	movq	(%rax), %rcx
	movq	%rdx, 16(%rcx)
	testq	%rdx, %rdx
	je	.LBB52_27
# BB#19:
	testb	$3, 9(%rdx)
	je	.LBB52_27
# BB#20:
	movq	(%rax), %rsi
	testb	$4, 9(%rsi)
	je	.LBB52_27
# BB#21:
	movq	%rbx, %rdi
	callq	luaC_barrierback
	jmp	.LBB52_27
.LBB52_22:
	movq	(%rax), %rcx
	movq	%rdx, 16(%rcx)
	testq	%rdx, %rdx
	je	.LBB52_27
# BB#23:
	testb	$3, 9(%rdx)
	je	.LBB52_27
# BB#24:
	movq	(%rax), %rsi
	testb	$4, 9(%rsi)
	je	.LBB52_27
# BB#25:
	movq	%rbx, %rdi
	callq	luaC_barrierf
	jmp	.LBB52_27
.LBB52_26:
	movq	32(%rbx), %rax
	movq	%rdx, 224(%rax,%rcx,8)
.LBB52_27:
	addq	$-16, 16(%rbx)
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end52:
	.size	lua_setmetatable, .Lfunc_end52-lua_setmetatable
	.cfi_endproc

	.globl	lua_setfenv
	.p2align	4, 0x90
	.type	lua_setfenv,@function
lua_setfenv:                            # @lua_setfenv
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB53_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB53_13
.LBB53_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB53_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB53_13
.LBB53_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB53_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB53_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB53_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB53_13
.LBB53_9:
	leaq	120(%rdi), %rax
	jmp	.LBB53_13
.LBB53_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB53_13
.LBB53_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB53_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB53_13
.LBB53_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB53_13:                              # %index2adr.exit
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 32
.Lcfi106:
	.cfi_offset %rbx, -24
.Lcfi107:
	.cfi_offset %r14, -16
	movl	8(%rax), %ecx
	cmpl	$6, %ecx
	je	.LBB53_17
# BB#14:                                # %index2adr.exit
	cmpl	$7, %ecx
	je	.LBB53_17
# BB#15:                                # %index2adr.exit
	cmpl	$8, %ecx
	jne	.LBB53_16
# BB#18:
	movq	(%rax), %rcx
	leaq	16(%rdi), %r14
	movq	16(%rdi), %rdx
	movq	-16(%rdx), %rdx
	movq	%rdx, 120(%rcx)
	movl	$5, 128(%rcx)
	movq	16(%rdi), %rcx
	jmp	.LBB53_19
.LBB53_17:
	leaq	16(%rdi), %r14
	movq	16(%rdi), %rcx
	movq	-16(%rcx), %rdx
	movq	(%rax), %rsi
	movq	%rdx, 24(%rsi)
.LBB53_19:
	movq	-16(%rcx), %rdx
	movl	$1, %ebx
	testb	$3, 9(%rdx)
	je	.LBB53_22
# BB#20:
	movq	(%rax), %rsi
	testb	$4, 9(%rsi)
	je	.LBB53_22
# BB#21:
	callq	luaC_barrierf
	jmp	.LBB53_22
.LBB53_16:                              # %index2adr.exit._crit_edge
	addq	$16, %rdi
	xorl	%ebx, %ebx
	movq	%rdi, %r14
.LBB53_22:
	addq	$-16, (%r14)
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end53:
	.size	lua_setfenv, .Lfunc_end53-lua_setfenv
	.cfi_endproc

	.globl	lua_call
	.p2align	4, 0x90
	.type	lua_call,@function
lua_call:                               # @lua_call
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi110:
	.cfi_def_cfa_offset 32
.Lcfi111:
	.cfi_offset %rbx, -24
.Lcfi112:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rdi, %rbx
	movq	16(%rbx), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	addq	$16, %rcx
	subq	%rcx, %rax
	movq	%rax, %rsi
	callq	luaD_call
	cmpl	$-1, %ebp
	jne	.LBB54_3
# BB#1:
	movq	16(%rbx), %rax
	movq	40(%rbx), %rcx
	cmpq	16(%rcx), %rax
	jb	.LBB54_3
# BB#2:
	movq	%rax, 16(%rcx)
.LBB54_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end54:
	.size	lua_call, .Lfunc_end54-lua_call
	.cfi_endproc

	.globl	lua_pcall
	.p2align	4, 0x90
	.type	lua_pcall,@function
lua_pcall:                              # @lua_pcall
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi115:
	.cfi_def_cfa_offset 48
.Lcfi116:
	.cfi_offset %rbx, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rdi, %rbx
	testl	%ecx, %ecx
	je	.LBB55_1
# BB#2:
	jle	.LBB55_4
# BB#3:
	movq	24(%rbx), %rax
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rax
	cmpq	16(%rbx), %rax
	movl	$luaO_nilobject_, %r8d
	cmovbq	%rax, %r8
	jmp	.LBB55_15
.LBB55_1:                               # %._crit_edge
	movq	64(%rbx), %rax
	xorl	%r8d, %r8d
	jmp	.LBB55_16
.LBB55_4:
	cmpl	$-9999, %ecx            # imm = 0xD8F1
	jl	.LBB55_6
# BB#5:
	movslq	%ecx, %r8
	shlq	$4, %r8
	addq	16(%rbx), %r8
	jmp	.LBB55_15
.LBB55_6:
	cmpl	$-10002, %ecx           # imm = 0xD8EE
	je	.LBB55_11
# BB#7:
	cmpl	$-10001, %ecx           # imm = 0xD8EF
	je	.LBB55_10
# BB#8:
	cmpl	$-10000, %ecx           # imm = 0xD8F0
	jne	.LBB55_12
# BB#9:
	movl	$160, %r8d
	addq	32(%rbx), %r8
	jmp	.LBB55_15
.LBB55_11:
	leaq	120(%rbx), %r8
	jmp	.LBB55_15
.LBB55_10:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	leaq	136(%rbx), %r8
	movq	24(%rax), %rax
	movq	%rax, 136(%rbx)
	movl	$5, 144(%rbx)
	jmp	.LBB55_15
.LBB55_12:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %edx           # imm = 0xD8EE
	subl	%ecx, %edx
	movzbl	11(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.LBB55_14
# BB#13:
	movl	$luaO_nilobject_, %r8d
	jmp	.LBB55_15
.LBB55_14:
	movslq	%edx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %r8
.LBB55_15:                              # %index2adr.exit
	movq	64(%rbx), %rax
	subq	%rax, %r8
.LBB55_16:
	movq	16(%rbx), %rcx
	movslq	%esi, %rdx
	shlq	$4, %rdx
	addq	$16, %rdx
	subq	%rdx, %rcx
	movq	%rcx, 8(%rsp)
	movl	%ebp, 16(%rsp)
	subq	%rax, %rcx
	leaq	8(%rsp), %rdx
	movl	$f_call, %esi
	movq	%rbx, %rdi
	callq	luaD_pcall
	cmpl	$-1, %ebp
	jne	.LBB55_19
# BB#17:
	movq	16(%rbx), %rcx
	movq	40(%rbx), %rdx
	cmpq	16(%rdx), %rcx
	jb	.LBB55_19
# BB#18:
	movq	%rcx, 16(%rdx)
.LBB55_19:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end55:
	.size	lua_pcall, .Lfunc_end55-lua_pcall
	.cfi_endproc

	.p2align	4, 0x90
	.type	f_call,@function
f_call:                                 # @f_call
	.cfi_startproc
# BB#0:
	movq	(%rsi), %rax
	movl	8(%rsi), %edx
	movq	%rax, %rsi
	jmp	luaD_call               # TAILCALL
.Lfunc_end56:
	.size	f_call, .Lfunc_end56-f_call
	.cfi_endproc

	.globl	lua_cpcall
	.p2align	4, 0x90
	.type	lua_cpcall,@function
lua_cpcall:                             # @lua_cpcall
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi118:
	.cfi_def_cfa_offset 32
	movq	%rsi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movq	16(%rdi), %rcx
	subq	64(%rdi), %rcx
	leaq	8(%rsp), %rdx
	movl	$f_Ccall, %esi
	xorl	%r8d, %r8d
	callq	luaD_pcall
	addq	$24, %rsp
	retq
.Lfunc_end57:
	.size	lua_cpcall, .Lfunc_end57-lua_cpcall
	.cfi_endproc

	.p2align	4, 0x90
	.type	f_Ccall,@function
f_Ccall:                                # @f_Ccall
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 32
.Lcfi122:
	.cfi_offset %rbx, -24
.Lcfi123:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	40(%rbx), %rax
	cmpq	80(%rbx), %rax
	je	.LBB58_1
# BB#2:
	movq	8(%rax), %rax
	movq	(%rax), %rax
	addq	$24, %rax
	jmp	.LBB58_3
.LBB58_1:
	leaq	120(%rbx), %rax
.LBB58_3:                               # %getcurrenv.exit
	movq	(%rax), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	luaF_newCclosure
	movq	(%r14), %rcx
	movq	%rcx, 32(%rax)
	movq	16(%rbx), %rcx
	movq	%rax, (%rcx)
	movl	$6, 8(%rcx)
	movq	16(%rbx), %rax
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%rbx)
	movq	8(%r14), %rcx
	movq	%rcx, 16(%rax)
	movl	$2, 24(%rax)
	movq	16(%rbx), %rsi
	leaq	16(%rsi), %rax
	movq	%rax, 16(%rbx)
	addq	$-16, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	luaD_call               # TAILCALL
.Lfunc_end58:
	.size	f_Ccall, .Lfunc_end58-f_Ccall
	.cfi_endproc

	.globl	lua_load
	.p2align	4, 0x90
	.type	lua_load,@function
lua_load:                               # @lua_load
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi127:
	.cfi_def_cfa_offset 80
.Lcfi128:
	.cfi_offset %rbx, -32
.Lcfi129:
	.cfi_offset %r14, -24
.Lcfi130:
	.cfi_offset %r15, -16
	movq	%rdx, %rax
	movq	%rsi, %rdx
	movq	%rdi, %r14
	testq	%rcx, %rcx
	movl	$.L.str.2, %ebx
	cmovneq	%rcx, %rbx
	leaq	8(%rsp), %r15
	movq	%r15, %rsi
	movq	%rax, %rcx
	callq	luaZ_init
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	luaD_protectedparser
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end59:
	.size	lua_load, .Lfunc_end59-lua_load
	.cfi_endproc

	.globl	lua_dump
	.p2align	4, 0x90
	.type	lua_dump,@function
lua_dump:                               # @lua_dump
	.cfi_startproc
# BB#0:
	movq	%rdx, %rax
	movq	%rsi, %rcx
	movq	16(%rdi), %rdx
	cmpl	$6, -8(%rdx)
	jne	.LBB60_2
# BB#1:
	movq	-16(%rdx), %rdx
	cmpb	$0, 10(%rdx)
	je	.LBB60_3
.LBB60_2:
	movl	$1, %eax
	retq
.LBB60_3:
	movq	32(%rdx), %rsi
	xorl	%r8d, %r8d
	movq	%rcx, %rdx
	movq	%rax, %rcx
	jmp	luaU_dump               # TAILCALL
.Lfunc_end60:
	.size	lua_dump, .Lfunc_end60-lua_dump
	.cfi_endproc

	.globl	lua_status
	.p2align	4, 0x90
	.type	lua_status,@function
lua_status:                             # @lua_status
	.cfi_startproc
# BB#0:
	movzbl	10(%rdi), %eax
	retq
.Lfunc_end61:
	.size	lua_status, .Lfunc_end61-lua_status
	.cfi_endproc

	.globl	lua_gc
	.p2align	4, 0x90
	.type	lua_gc,@function
lua_gc:                                 # @lua_gc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 32
.Lcfi134:
	.cfi_offset %rbx, -32
.Lcfi135:
	.cfi_offset %r14, -24
.Lcfi136:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpl	$7, %esi
	ja	.LBB62_1
# BB#2:
	movq	32(%r15), %rbx
	movl	%esi, %eax
	jmpq	*.LJTI62_0(,%rax,8)
.LBB62_3:
	movq	$-3, 112(%rbx)
	xorl	%r14d, %r14d
	jmp	.LBB62_14
.LBB62_1:
	movl	$-1, %r14d
	jmp	.LBB62_14
.LBB62_4:
	movq	120(%rbx), %rax
	movq	%rax, 112(%rbx)
	xorl	%r14d, %r14d
	jmp	.LBB62_14
.LBB62_5:
	movq	%r15, %rdi
	callq	luaC_fullgc
	xorl	%r14d, %r14d
	jmp	.LBB62_14
.LBB62_6:
	movq	120(%rbx), %r14
	shrq	$10, %r14
	jmp	.LBB62_14
.LBB62_7:
	movl	$1023, %r14d            # imm = 0x3FF
	andl	120(%rbx), %r14d
	jmp	.LBB62_14
.LBB62_8:
	movslq	%edx, %rax
	shlq	$10, %rax
	movq	120(%rbx), %rcx
	xorl	%r14d, %r14d
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movl	$0, %eax
	cmovaeq	%rdx, %rax
	movq	%rax, 112(%rbx)
	cmpq	%rcx, %rax
	ja	.LBB62_14
	.p2align	4, 0x90
.LBB62_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	luaC_step
	cmpb	$0, 33(%rbx)
	je	.LBB62_10
# BB#11:                                # %._crit_edge
                                        #   in Loop: Header=BB62_9 Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jbe	.LBB62_9
	jmp	.LBB62_14
.LBB62_12:
	movl	144(%rbx), %r14d
	movl	%edx, 144(%rbx)
	jmp	.LBB62_14
.LBB62_13:
	movl	148(%rbx), %r14d
	movl	%edx, 148(%rbx)
	jmp	.LBB62_14
.LBB62_10:
	movl	$1, %r14d
.LBB62_14:                              # %.loopexit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end62:
	.size	lua_gc, .Lfunc_end62-lua_gc
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI62_0:
	.quad	.LBB62_3
	.quad	.LBB62_4
	.quad	.LBB62_5
	.quad	.LBB62_6
	.quad	.LBB62_7
	.quad	.LBB62_8
	.quad	.LBB62_12
	.quad	.LBB62_13

	.text
	.globl	lua_error
	.p2align	4, 0x90
	.type	lua_error,@function
lua_error:                              # @lua_error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi137:
	.cfi_def_cfa_offset 16
	callq	luaG_errormsg
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end63:
	.size	lua_error, .Lfunc_end63-lua_error
	.cfi_endproc

	.globl	lua_next
	.p2align	4, 0x90
	.type	lua_next,@function
lua_next:                               # @lua_next
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 16
.Lcfi139:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testl	%esi, %esi
	jle	.LBB64_2
# BB#1:
	movq	24(%rbx), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rbx), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB64_13
.LBB64_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB64_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rbx), %rax
	jmp	.LBB64_13
.LBB64_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB64_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB64_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB64_10
# BB#7:
	movl	$160, %eax
	addq	32(%rbx), %rax
	jmp	.LBB64_13
.LBB64_9:
	leaq	120(%rbx), %rax
	jmp	.LBB64_13
.LBB64_8:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rbx), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rbx)
	movl	$5, 144(%rbx)
	jmp	.LBB64_13
.LBB64_10:
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %edx
	cmpl	%edx, %ecx
	jle	.LBB64_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB64_13
.LBB64_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB64_13:                              # %index2adr.exit
	movq	(%rax), %rsi
	movq	16(%rbx), %rdx
	addq	$-16, %rdx
	movq	%rbx, %rdi
	callq	luaH_next
	cmpl	$1, %eax
	sbbq	%rcx, %rcx
	orq	$1, %rcx
	shlq	$4, %rcx
	addq	%rcx, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end64:
	.size	lua_next, .Lfunc_end64-lua_next
	.cfi_endproc

	.globl	lua_concat
	.p2align	4, 0x90
	.type	lua_concat,@function
lua_concat:                             # @lua_concat
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 32
.Lcfi143:
	.cfi_offset %rbx, -32
.Lcfi144:
	.cfi_offset %r14, -24
.Lcfi145:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpl	$2, %ebp
	jl	.LBB65_4
# BB#1:
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB65_3
# BB#2:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB65_3:
	leaq	16(%rbx), %r14
	movq	16(%rbx), %rdx
	subq	24(%rbx), %rdx
	shrq	$4, %rdx
	decl	%edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	luaV_concat
	movslq	%ebp, %rcx
	shlq	$4, %rcx
	movq	16(%rbx), %rax
	addq	$-16, %rcx
	subq	%rcx, %rax
	jmp	.LBB65_6
.LBB65_4:
	testl	%ebp, %ebp
	jne	.LBB65_7
# BB#5:
	movq	16(%rbx), %rbp
	movl	$.L.str.3, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaS_newlstr
	movq	%rax, (%rbp)
	movl	$4, 8(%rbp)
	movq	16(%rbx), %rax
	leaq	16(%rbx), %r14
	addq	$16, %rax
.LBB65_6:                               # %.sink.split
	movq	%rax, (%r14)
.LBB65_7:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end65:
	.size	lua_concat, .Lfunc_end65-lua_concat
	.cfi_endproc

	.globl	lua_getallocf
	.p2align	4, 0x90
	.type	lua_getallocf,@function
lua_getallocf:                          # @lua_getallocf
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB66_2
# BB#1:
	movq	32(%rdi), %rax
	movq	24(%rax), %rax
	movq	%rax, (%rsi)
.LBB66_2:                               # %._crit_edge
	movq	32(%rdi), %rax
	movq	16(%rax), %rax
	retq
.Lfunc_end66:
	.size	lua_getallocf, .Lfunc_end66-lua_getallocf
	.cfi_endproc

	.globl	lua_setallocf
	.p2align	4, 0x90
	.type	lua_setallocf,@function
lua_setallocf:                          # @lua_setallocf
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	movq	%rdx, 24(%rax)
	movq	%rsi, 16(%rax)
	retq
.Lfunc_end67:
	.size	lua_setallocf, .Lfunc_end67-lua_setallocf
	.cfi_endproc

	.globl	lua_newuserdata
	.p2align	4, 0x90
	.type	lua_newuserdata,@function
lua_newuserdata:                        # @lua_newuserdata
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi148:
	.cfi_def_cfa_offset 32
.Lcfi149:
	.cfi_offset %rbx, -24
.Lcfi150:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB68_2
# BB#1:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB68_2:
	movq	40(%rbx), %rax
	cmpq	80(%rbx), %rax
	je	.LBB68_3
# BB#4:
	movq	8(%rax), %rax
	movq	(%rax), %rax
	addq	$24, %rax
	jmp	.LBB68_5
.LBB68_3:
	leaq	120(%rbx), %rax
.LBB68_5:                               # %getcurrenv.exit
	movq	(%rax), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	luaS_newudata
	movq	16(%rbx), %rcx
	movq	%rax, (%rcx)
	movl	$7, 8(%rcx)
	addq	$16, 16(%rbx)
	addq	$40, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end68:
	.size	lua_newuserdata, .Lfunc_end68-lua_newuserdata
	.cfi_endproc

	.globl	lua_getupvalue
	.p2align	4, 0x90
	.type	lua_getupvalue,@function
lua_getupvalue:                         # @lua_getupvalue
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB69_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	cmpl	$6, 8(%rax)
	je	.LBB69_15
	jmp	.LBB69_14
.LBB69_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB69_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	cmpl	$6, 8(%rax)
	je	.LBB69_15
	jmp	.LBB69_14
.LBB69_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB69_9
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB69_8
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB69_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	cmpl	$6, 8(%rax)
	je	.LBB69_15
	jmp	.LBB69_14
.LBB69_9:
	leaq	120(%rdi), %rax
	cmpl	$6, 8(%rax)
	je	.LBB69_15
	jmp	.LBB69_14
.LBB69_8:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	cmpl	$6, 8(%rax)
	je	.LBB69_15
	jmp	.LBB69_14
.LBB69_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %esi
	cmpl	%esi, %ecx
	jle	.LBB69_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	cmpl	$6, 8(%rax)
	je	.LBB69_15
	jmp	.LBB69_14
.LBB69_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
	cmpl	$6, 8(%rax)
	jne	.LBB69_14
.LBB69_15:
	movq	(%rax), %rax
	cmpb	$0, 10(%rax)
	je	.LBB69_21
# BB#16:
	testl	%edx, %edx
	jle	.LBB69_17
# BB#18:
	movzbl	11(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.LBB69_20
# BB#19:
	xorl	%eax, %eax
	retq
.LBB69_14:
	xorl	%eax, %eax
	retq
.LBB69_21:
	testl	%edx, %edx
	jle	.LBB69_22
# BB#23:
	movq	32(%rax), %rsi
	cmpl	%edx, 72(%rsi)
	jge	.LBB69_25
# BB#24:
	xorl	%eax, %eax
	retq
.LBB69_17:
	xorl	%eax, %eax
	retq
.LBB69_20:                              # %aux_upvalue.exit.thread19
	movslq	%edx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rcx
	movl	$.L.str.3, %eax
	jmp	.LBB69_26
.LBB69_22:
	xorl	%eax, %eax
	retq
.LBB69_25:                              # %aux_upvalue.exit
	movslq	%edx, %rdx
	movq	32(%rax,%rdx,8), %rax
	movq	16(%rax), %rcx
	movq	56(%rsi), %rax
	movq	-8(%rax,%rdx,8), %rax
	addq	$24, %rax
.LBB69_26:
	movq	16(%rdi), %rdx
	movq	(%rcx), %rsi
	movq	%rsi, (%rdx)
	movl	8(%rcx), %ecx
	movl	%ecx, 8(%rdx)
	addq	$16, 16(%rdi)
	retq
.Lfunc_end69:
	.size	lua_getupvalue, .Lfunc_end69-lua_getupvalue
	.cfi_endproc

	.globl	lua_setupvalue
	.p2align	4, 0x90
	.type	lua_setupvalue,@function
lua_setupvalue:                         # @lua_setupvalue
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB70_2
# BB#1:
	movq	24(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rcx
	cmpq	16(%rdi), %rcx
	movl	$luaO_nilobject_, %eax
	cmovbq	%rcx, %rax
	jmp	.LBB70_13
.LBB70_2:
	cmpl	$-9999, %esi            # imm = 0xD8F1
	jl	.LBB70_4
# BB#3:
	movslq	%esi, %rax
	shlq	$4, %rax
	addq	16(%rdi), %rax
	jmp	.LBB70_13
.LBB70_4:
	cmpl	$-10002, %esi           # imm = 0xD8EE
	je	.LBB70_8
# BB#5:
	cmpl	$-10001, %esi           # imm = 0xD8EF
	je	.LBB70_9
# BB#6:
	cmpl	$-10000, %esi           # imm = 0xD8F0
	jne	.LBB70_10
# BB#7:
	movl	$160, %eax
	addq	32(%rdi), %rax
	jmp	.LBB70_13
.LBB70_8:
	leaq	120(%rdi), %rax
	jmp	.LBB70_13
.LBB70_9:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	leaq	136(%rdi), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, 136(%rdi)
	movl	$5, 144(%rdi)
	jmp	.LBB70_13
.LBB70_10:
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$-10002, %ecx           # imm = 0xD8EE
	subl	%esi, %ecx
	movzbl	11(%rax), %esi
	cmpl	%esi, %ecx
	jle	.LBB70_12
# BB#11:
	movl	$luaO_nilobject_, %eax
	jmp	.LBB70_13
.LBB70_12:
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	leaq	24(%rax,%rcx), %rax
.LBB70_13:                              # %index2adr.exit
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 16
.Lcfi152:
	.cfi_offset %rbx, -16
	cmpl	$6, 8(%rax)
	jne	.LBB70_18
# BB#14:
	movq	(%rax), %rcx
	cmpb	$0, 10(%rcx)
	je	.LBB70_20
# BB#15:
	testl	%edx, %edx
	jle	.LBB70_18
# BB#16:
	movzbl	11(%rcx), %esi
	cmpl	%edx, %esi
	jl	.LBB70_18
# BB#24:                                # %aux_upvalue.exit.thread28
	movslq	%edx, %rdx
	shlq	$4, %rdx
	leaq	24(%rcx,%rdx), %rcx
	movl	$.L.str.3, %ebx
	jmp	.LBB70_27
.LBB70_20:
	testl	%edx, %edx
	jle	.LBB70_18
# BB#21:
	movq	32(%rcx), %rsi
	cmpl	%edx, 72(%rsi)
	jge	.LBB70_26
.LBB70_18:
	xorl	%ebx, %ebx
.LBB70_19:                              # %aux_upvalue.exit.thread
	movq	%rbx, %rax
	popq	%rbx
	retq
.LBB70_26:                              # %aux_upvalue.exit
	movslq	%edx, %rdx
	movq	32(%rcx,%rdx,8), %rcx
	movq	16(%rcx), %rcx
	movq	56(%rsi), %rsi
	movq	-8(%rsi,%rdx,8), %rbx
	addq	$24, %rbx
.LBB70_27:
	movq	16(%rdi), %rdx
	leaq	-16(%rdx), %rsi
	movq	%rsi, 16(%rdi)
	movq	-16(%rdx), %rsi
	movq	%rsi, (%rcx)
	movl	-8(%rdx), %edx
	movl	%edx, 8(%rcx)
	movq	16(%rdi), %rcx
	cmpl	$4, 8(%rcx)
	jl	.LBB70_19
# BB#28:
	movq	(%rcx), %rdx
	testb	$3, 9(%rdx)
	je	.LBB70_19
# BB#29:
	movq	(%rax), %rsi
	testb	$4, 9(%rsi)
	je	.LBB70_19
# BB#30:
	callq	luaC_barrierf
	jmp	.LBB70_19
.Lfunc_end70:
	.size	lua_setupvalue, .Lfunc_end70-lua_setupvalue
	.cfi_endproc

	.type	lua_ident,@object       # @lua_ident
	.section	.rodata,"a",@progbits
	.globl	lua_ident
	.p2align	4
lua_ident:
	.asciz	"$Lua: Lua 5.1.4 Copyright (C) 1994-2008 Lua.org, PUC-Rio $\n$Authors: R. Ierusalimschy, L. H. de Figueiredo & W. Celes $\n$URL: www.lua.org $\n"
	.size	lua_ident, 141

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"no calling environment"
	.size	.L.str, 23

	.hidden	luaO_nilobject_
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"no value"
	.size	.L.str.1, 9

	.hidden	luaT_typenames
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"?"
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.zero	1
	.size	.L.str.3, 1

	.hidden	luaD_growstack
	.hidden	luaC_step
	.hidden	luaE_newthread
	.hidden	luaG_runerror
	.hidden	luaC_barrierf
	.hidden	luaV_tonumber
	.hidden	luaO_rawequalObj
	.hidden	luaV_equalval
	.hidden	luaV_lessthan
	.hidden	luaV_tostring
	.hidden	luaH_getn
	.hidden	luaS_newlstr
	.hidden	luaO_pushvfstring
	.hidden	luaF_newCclosure
	.hidden	luaV_gettable
	.hidden	luaH_get
	.hidden	luaH_getnum
	.hidden	luaH_new
	.hidden	luaV_settable
	.hidden	luaH_set
	.hidden	luaC_barrierback
	.hidden	luaH_setnum
	.hidden	luaD_call
	.hidden	luaD_pcall
	.hidden	luaZ_init
	.hidden	luaD_protectedparser
	.hidden	luaU_dump
	.hidden	luaC_fullgc
	.hidden	luaG_errormsg
	.hidden	luaH_next
	.hidden	luaV_concat
	.hidden	luaS_newudata

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
