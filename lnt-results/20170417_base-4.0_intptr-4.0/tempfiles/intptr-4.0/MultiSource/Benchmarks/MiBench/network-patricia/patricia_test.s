	.text
	.file	"patricia_test.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	cmpl	$1, %edi
	jle	.LBB0_19
# BB#1:
	movq	8(%rbx), %rdi
	movl	$.L.str.1, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_20
# BB#2:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_3
# BB#5:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 32(%rbx)
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbp, 8(%rbx)
	testq	%rbp, %rbp
	je	.LBB0_6
# BB#7:
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	malloc
	movq	%rax, 8(%rbp)
	testq	%rax, %rax
	je	.LBB0_8
# BB#9:
	movb	$0, (%rax)
	movb	$1, 16(%rbx)
	movq	%rbx, 32(%rbx)
	movq	%rbx, 24(%rbx)
	leaq	16(%rsp), %rdi
	movl	$128, %esi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB0_18
# BB#10:                                # %.lr.ph
	leaq	16(%rsp), %r15
	movl	$4294967295, %r13d      # imm = 0xFFFFFFFF
	.p2align	4, 0x90
.LBB0_11:                               # =>This Inner Loop Header: Depth=1
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	12(%rsp), %rdx
	leaq	8(%rsp), %rcx
	callq	sscanf
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_3
# BB#12:                                #   in Loop: Header=BB0_11 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	$0, 32(%rbp)
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r12
	movq	%r12, 8(%rbp)
	testq	%r12, %r12
	je	.LBB0_6
# BB#13:                                #   in Loop: Header=BB0_11 Depth=1
	movq	$0, (%r12)
	movl	$16, %edi
	callq	malloc
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.LBB0_8
# BB#14:                                #   in Loop: Header=BB0_11 Depth=1
	movb	$0, (%rax)
	movl	8(%rsp), %edi
	movq	%rdi, (%rbp)
	movq	%r13, (%r12)
	movq	%rbx, %rsi
	callq	pat_search
	movl	8(%rsp), %esi
	cmpq	%rsi, (%rax)
	jne	.LBB0_16
# BB#15:                                # %.thread
                                        #   in Loop: Header=BB0_11 Depth=1
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.7, %edi
	movb	$1, %al
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movl	$.Lstr, %edi
	callq	puts
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_11 Depth=1
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	pat_insert
	testq	%rax, %rax
	je	.LBB0_21
.LBB0_17:                               # %.backedge
                                        #   in Loop: Header=BB0_11 Depth=1
	movl	$128, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB0_11
.LBB0_18:                               # %._crit_edge
	xorl	%edi, %edi
	callq	exit
.LBB0_3:
	movl	$.L.str.3, %edi
	jmp	.LBB0_4
.LBB0_6:
	movl	$.L.str.4, %edi
	jmp	.LBB0_4
.LBB0_8:
	movl	$.L.str.5, %edi
.LBB0_4:
	callq	perror
	movl	$1, %edi
	callq	exit
.LBB0_21:
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$21, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB0_19:
	movq	(%rbx), %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.LBB0_20:
	movq	8(%rbx), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Usage: %s <TCP stream>\n"
	.size	.L.str, 24

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"r"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"File %s doesn't seem to exist\n"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Allocating p-trie node"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Allocating p-trie mask data"
	.size	.L.str.4, 28

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Allocating p-trie mask's node data"
	.size	.L.str.5, 35

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%f %d"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%f %08x: "
	.size	.L.str.7, 10

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Failed on pat_insert\n"
	.size	.L.str.9, 22

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"Found."
	.size	.Lstr, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
