	.text
	.file	"jmemnobs.bc"
	.globl	jpeg_get_small
	.p2align	4, 0x90
	.type	jpeg_get_small,@function
jpeg_get_small:                         # @jpeg_get_small
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	malloc                  # TAILCALL
.Lfunc_end0:
	.size	jpeg_get_small, .Lfunc_end0-jpeg_get_small
	.cfi_endproc

	.globl	jpeg_free_small
	.p2align	4, 0x90
	.type	jpeg_free_small,@function
jpeg_free_small:                        # @jpeg_free_small
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	jpeg_free_small, .Lfunc_end1-jpeg_free_small
	.cfi_endproc

	.globl	jpeg_get_large
	.p2align	4, 0x90
	.type	jpeg_get_large,@function
jpeg_get_large:                         # @jpeg_get_large
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	malloc                  # TAILCALL
.Lfunc_end2:
	.size	jpeg_get_large, .Lfunc_end2-jpeg_get_large
	.cfi_endproc

	.globl	jpeg_free_large
	.p2align	4, 0x90
	.type	jpeg_free_large,@function
jpeg_free_large:                        # @jpeg_free_large
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	jpeg_free_large, .Lfunc_end3-jpeg_free_large
	.cfi_endproc

	.globl	jpeg_mem_available
	.p2align	4, 0x90
	.type	jpeg_mem_available,@function
jpeg_mem_available:                     # @jpeg_mem_available
	.cfi_startproc
# BB#0:
	movq	%rdx, %rax
	retq
.Lfunc_end4:
	.size	jpeg_mem_available, .Lfunc_end4-jpeg_mem_available
	.cfi_endproc

	.globl	jpeg_open_backing_store
	.p2align	4, 0x90
	.type	jpeg_open_backing_store,@function
jpeg_open_backing_store:                # @jpeg_open_backing_store
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	$48, 40(%rax)
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end5:
	.size	jpeg_open_backing_store, .Lfunc_end5-jpeg_open_backing_store
	.cfi_endproc

	.globl	jpeg_mem_init
	.p2align	4, 0x90
	.type	jpeg_mem_init,@function
jpeg_mem_init:                          # @jpeg_mem_init
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	jpeg_mem_init, .Lfunc_end6-jpeg_mem_init
	.cfi_endproc

	.globl	jpeg_mem_term
	.p2align	4, 0x90
	.type	jpeg_mem_term,@function
jpeg_mem_term:                          # @jpeg_mem_term
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	jpeg_mem_term, .Lfunc_end7-jpeg_mem_term
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
