	.text
	.file	"computation.bc"
	.globl	hypre_CreateComputeInfo
	.p2align	4, 0x90
	.type	hypre_CreateComputeInfo,@function
hypre_CreateComputeInfo:                # @hypre_CreateComputeInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	8(%rdi), %r15
	leaq	64(%rsp), %rdx
	leaq	56(%rsp), %rcx
	leaq	48(%rsp), %r8
	leaq	40(%rsp), %r9
	callq	hypre_CreateCommInfoFromStencil
	movl	8(%r15), %edi
	callq	hypre_BoxArrayArrayCreate
	movq	%rax, %rbx
	movl	8(%r15), %edi
	callq	hypre_BoxArrayArrayCreate
	movq	%rax, %rbp
	cmpl	$0, 8(%r15)
	jle	.LBB0_3
# BB#1:                                 # %.lr.ph
	xorl	%r12d, %r12d
	movl	$20, %r13d
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	(%rax,%r12,8), %r14
	movl	$1, %esi
	movq	%r14, %rdi
	callq	hypre_BoxArraySetSize
	movq	(%r14), %rax
	movq	(%r15), %rcx
	movl	-20(%rcx,%r13), %edx
	movl	%edx, (%rax)
	movl	-16(%rcx,%r13), %edx
	movl	%edx, 4(%rax)
	movl	-12(%rcx,%r13), %edx
	movl	%edx, 8(%rax)
	movl	-8(%rcx,%r13), %edx
	movl	%edx, 12(%rax)
	movl	-4(%rcx,%r13), %edx
	movl	%edx, 16(%rax)
	movl	(%rcx,%r13), %ecx
	movl	%ecx, 20(%rax)
	incq	%r12
	movslq	8(%r15), %rax
	addq	$24, %r13
	cmpq	%rax, %r12
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge
	movq	64(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%rcx)
	movq	56(%rsp), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	48(%rsp), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	40(%rsp), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	128(%rsp), %rax
	movq	%rbx, (%rax)
	movq	136(%rsp), %rax
	movq	%rbp, (%rax)
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_CreateComputeInfo, .Lfunc_end0-hypre_CreateComputeInfo
	.cfi_endproc

	.globl	hypre_ComputePkgCreate
	.p2align	4, 0x90
	.type	hypre_ComputePkgCreate,@function
hypre_ComputePkgCreate:                 # @hypre_ComputePkgCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	104(%rsp), %rbp
	movl	$1, %edi
	movl	$64, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	(%rbp), %eax
	leaq	56(%rbp), %rbp
	subq	$8, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	120(%rsp), %r8
	movq	%r8, %r9
	movq	%r8, %r14
	pushq	%rbp
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	movl	144(%rsp), %ebp
	pushq	%rbp
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CommPkgCreate
	addq	$48, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -48
	movq	%rax, (%rbx)
	movq	80(%rsp), %rax
	movq	%rax, 8(%rbx)
	movq	88(%rsp), %rax
	movq	%rax, 16(%rbx)
	movq	96(%rsp), %rcx
	movl	(%rcx), %eax
	movl	%eax, 24(%rbx)
	movl	4(%rcx), %eax
	movl	%eax, 28(%rbx)
	movl	8(%rcx), %eax
	movl	%eax, 32(%rbx)
	leaq	40(%rbx), %rsi
	movq	104(%rsp), %rdi
	callq	hypre_StructGridRef
	movq	%r14, 48(%rbx)
	movl	%ebp, 56(%rbx)
	movq	128(%rsp), %rax
	movq	%rbx, (%rax)
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_ComputePkgCreate, .Lfunc_end1-hypre_ComputePkgCreate
	.cfi_endproc

	.globl	hypre_ComputePkgDestroy
	.p2align	4, 0x90
	.type	hypre_ComputePkgDestroy,@function
hypre_ComputePkgDestroy:                # @hypre_ComputePkgDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
.Lcfi34:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_2
# BB#1:
	movq	(%rbx), %rdi
	callq	hypre_CommPkgDestroy
	movq	8(%rbx), %rdi
	callq	hypre_BoxArrayArrayDestroy
	movq	16(%rbx), %rdi
	callq	hypre_BoxArrayArrayDestroy
	movq	40(%rbx), %rdi
	callq	hypre_StructGridDestroy
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB2_2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	hypre_ComputePkgDestroy, .Lfunc_end2-hypre_ComputePkgDestroy
	.cfi_endproc

	.globl	hypre_InitializeIndtComputations
	.p2align	4, 0x90
	.type	hypre_InitializeIndtComputations,@function
hypre_InitializeIndtComputations:       # @hypre_InitializeIndtComputations
	.cfi_startproc
# BB#0:
	movq	%rdx, %rax
	movq	(%rdi), %rdi
	movq	%rsi, %rdx
	movq	%rax, %rcx
	jmp	hypre_InitializeCommunication # TAILCALL
.Lfunc_end3:
	.size	hypre_InitializeIndtComputations, .Lfunc_end3-hypre_InitializeIndtComputations
	.cfi_endproc

	.globl	hypre_FinalizeIndtComputations
	.p2align	4, 0x90
	.type	hypre_FinalizeIndtComputations,@function
hypre_FinalizeIndtComputations:         # @hypre_FinalizeIndtComputations
	.cfi_startproc
# BB#0:
	jmp	hypre_FinalizeCommunication # TAILCALL
.Lfunc_end4:
	.size	hypre_FinalizeIndtComputations, .Lfunc_end4-hypre_FinalizeIndtComputations
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
