	.text
	.file	"btCollisionShape.bc"
	.globl	btBulletCollisionProbe
	.p2align	4, 0x90
	.type	btBulletCollisionProbe,@function
btBulletCollisionProbe:                 # @btBulletCollisionProbe
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	btBulletCollisionProbe, .Lfunc_end0-btBulletCollisionProbe
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_1:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.text
	.globl	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.p2align	4, 0x90
	.type	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf,@function
_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf: # @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$104, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 128
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movl	$1065353216, 40(%rsp)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 44(%rsp)
	movl	$1065353216, 60(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 64(%rsp)
	movl	$1065353216, 80(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 84(%rsp)
	movl	$0, 100(%rsp)
	movq	(%rdi), %rax
	leaq	40(%rsp), %rsi
	leaq	24(%rsp), %rdx
	leaq	8(%rsp), %rcx
	callq	*16(%rax)
	movss	8(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	24(%rsp), %xmm1
	subss	28(%rsp), %xmm2
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	32(%rsp), %xmm0
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB1_2:                                # %.split
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%r14)
	movsd	24(%rsp), %xmm1         # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm2          # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	movss	32(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	16(%rsp), %xmm1
	mulps	.LCPI1_1(%rip), %xmm2
	mulss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, (%rbx)
	movlps	%xmm0, 8(%rbx)
	addq	$104, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf, .Lfunc_end1-_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.cfi_endproc

	.globl	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.p2align	4, 0x90
	.type	_ZNK16btCollisionShape27getContactBreakingThresholdEv,@function
_ZNK16btCollisionShape27getContactBreakingThresholdEv: # @_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movq	(%rdi), %rax
	callq	*32(%rax)
	mulss	gContactThresholdFactor(%rip), %xmm0
	popq	%rax
	retq
.Lfunc_end2:
	.size	_ZNK16btCollisionShape27getContactBreakingThresholdEv, .Lfunc_end2-_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.cfi_endproc

	.globl	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.p2align	4, 0x90
	.type	_ZNK16btCollisionShape20getAngularMotionDiscEv,@function
_ZNK16btCollisionShape20getAngularMotionDiscEv: # @_ZNK16btCollisionShape20getAngularMotionDiscEv
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 32
	movq	(%rdi), %rax
	leaq	8(%rsp), %rsi
	leaq	4(%rsp), %rdx
	callq	*24(%rax)
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB3_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB3_2:                                # %.split
	addss	4(%rsp), %xmm1
	movaps	%xmm1, %xmm0
	addq	$24, %rsp
	retq
.Lfunc_end3:
	.size	_ZNK16btCollisionShape20getAngularMotionDiscEv, .Lfunc_end3-_ZNK16btCollisionShape20getAngularMotionDiscEv
	.cfi_endproc

	.globl	_ZNK16btCollisionShape21calculateTemporalAabbERK11btTransformRK9btVector3S5_fRS3_S6_
	.p2align	4, 0x90
	.type	_ZNK16btCollisionShape21calculateTemporalAabbERK11btTransformRK9btVector3S5_fRS3_S6_,@function
_ZNK16btCollisionShape21calculateTemporalAabbERK11btTransformRK9btVector3S5_fRS3_S6_: # @_ZNK16btCollisionShape21calculateTemporalAabbERK11btTransformRK9btVector3S5_fRS3_S6_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 80
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %rbx
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rdi, %r15
	movq	(%r15), %rax
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	*16(%rax)
	movl	(%r14), %eax
	movl	4(%r14), %ecx
	movl	8(%r14), %edx
	movl	(%rbx), %esi
	movl	4(%rbx), %edi
	movl	8(%rbx), %ebp
	movss	(%r13), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	movss	4(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	8(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm2
	jbe	.LBB4_2
# BB#1:
	movd	%eax, %xmm3
	addss	%xmm2, %xmm3
	movd	%xmm3, (%rsp)           # 4-byte Folded Spill
	jmp	.LBB4_3
.LBB4_2:
	movd	%esi, %xmm3
	addss	%xmm2, %xmm3
	movd	%xmm3, %esi
	movl	%eax, (%rsp)            # 4-byte Spill
.LBB4_3:
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	jbe	.LBB4_5
# BB#4:
	movd	%ecx, %xmm2
	addss	%xmm1, %xmm2
	movd	%xmm2, %ecx
	jmp	.LBB4_6
.LBB4_5:
	movd	%edi, %xmm2
	addss	%xmm1, %xmm2
	movd	%xmm2, %edi
.LBB4_6:
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movl	%edi, 16(%rsp)          # 4-byte Spill
	jbe	.LBB4_8
# BB#7:
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movd	%edx, %xmm1
	addss	%xmm0, %xmm1
	movd	%xmm1, %edx
	jmp	.LBB4_9
.LBB4_8:
	movd	%ebp, %xmm1
	addss	%xmm0, %xmm1
	movd	%xmm1, 4(%rsp)          # 4-byte Folded Spill
.LBB4_9:
	movl	%edx, %ebp
	movl	%ecx, %r13d
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_11
# BB#10:                                # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB4_11:                               # %.split
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	mulss	12(%rsp), %xmm0         # 4-byte Folded Reload
	mulss	8(%rsp), %xmm0          # 4-byte Folded Reload
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rbx)
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, 4(%rbx)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 8(%rbx)
	movl	$0, 12(%rbx)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, (%r14)
	movl	%r13d, 4(%r14)
	movl	%ebp, 8(%r14)
	movl	$0, 12(%r14)
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 4(%rbx)
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 8(%rbx)
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r14)
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r14)
	addss	8(%r14), %xmm0
	movss	%xmm0, 8(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZNK16btCollisionShape21calculateTemporalAabbERK11btTransformRK9btVector3S5_fRS3_S6_, .Lfunc_end4-_ZNK16btCollisionShape21calculateTemporalAabbERK11btTransformRK9btVector3S5_fRS3_S6_
	.cfi_endproc

	.section	.text._ZN16btCollisionShapeD2Ev,"axG",@progbits,_ZN16btCollisionShapeD2Ev,comdat
	.weak	_ZN16btCollisionShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN16btCollisionShapeD2Ev,@function
_ZN16btCollisionShapeD2Ev:              # @_ZN16btCollisionShapeD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZN16btCollisionShapeD2Ev, .Lfunc_end5-_ZN16btCollisionShapeD2Ev
	.cfi_endproc

	.section	.text._ZN16btCollisionShapeD0Ev,"axG",@progbits,_ZN16btCollisionShapeD0Ev,comdat
	.weak	_ZN16btCollisionShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN16btCollisionShapeD0Ev,@function
_ZN16btCollisionShapeD0Ev:              # @_ZN16btCollisionShapeD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end6:
	.size	_ZN16btCollisionShapeD0Ev, .Lfunc_end6-_ZN16btCollisionShapeD0Ev
	.cfi_endproc

	.type	gContactThresholdFactor,@object # @gContactThresholdFactor
	.data
	.globl	gContactThresholdFactor
	.p2align	2
gContactThresholdFactor:
	.long	1017370378              # float 0.0199999996
	.size	gContactThresholdFactor, 4

	.type	_ZTV16btCollisionShape,@object # @_ZTV16btCollisionShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV16btCollisionShape
	.p2align	3
_ZTV16btCollisionShape:
	.quad	0
	.quad	_ZTI16btCollisionShape
	.quad	_ZN16btCollisionShapeD2Ev
	.quad	_ZN16btCollisionShapeD0Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV16btCollisionShape, 112

	.type	_ZTS16btCollisionShape,@object # @_ZTS16btCollisionShape
	.globl	_ZTS16btCollisionShape
	.p2align	4
_ZTS16btCollisionShape:
	.asciz	"16btCollisionShape"
	.size	_ZTS16btCollisionShape, 19

	.type	_ZTI16btCollisionShape,@object # @_ZTI16btCollisionShape
	.globl	_ZTI16btCollisionShape
	.p2align	3
_ZTI16btCollisionShape:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16btCollisionShape
	.size	_ZTI16btCollisionShape, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
