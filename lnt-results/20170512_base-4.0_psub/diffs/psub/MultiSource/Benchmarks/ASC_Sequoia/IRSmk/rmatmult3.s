	.text
	.file	"rmatmult3.bc"
	.globl	rmatmult3
	.p2align	4, 0x90
	.type	rmatmult3,@function
rmatmult3:                              # @rmatmult3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 224
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movl	16(%rdi), %r10d
	movl	28(%rdi), %eax
	movl	%eax, -108(%rsp)        # 4-byte Spill
	cmpl	%eax, %r10d
	jge	.LBB0_9
# BB#1:                                 # %.preheader359.lr.ph
	movslq	8(%rdi), %rbx
	movslq	20(%rdi), %r9
	movl	12(%rdi), %r11d
	movl	24(%rdi), %eax
	movl	%eax, -112(%rsp)        # 4-byte Spill
	movslq	68(%rdi), %rdx
	movslq	72(%rdi), %rdi
	movq	(%r14), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	(,%rdi,8), %rax
	movq	%rbp, %rsi
	subq	%rax, %rsi
	leaq	(,%rdx,8), %rcx
	movq	%rsi, %r13
	subq	%rcx, %r13
	movq	%rbp, %r12
	subq	%rcx, %r12
	leaq	(%rbp,%rdi,8), %r15
	movq	%r15, %r8
	subq	%rcx, %r8
	movl	%r10d, %ecx
	movq	%rdi, -96(%rsp)         # 8-byte Spill
	imull	%edi, %ecx
	addl	%ebx, %ecx
	movl	%r11d, -116(%rsp)       # 4-byte Spill
	imull	%edx, %r11d
	addl	%ecx, %r11d
	movl	%r11d, -124(%rsp)       # 4-byte Spill
	movq	8(%r14), %rcx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movq	16(%r14), %rcx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	movq	24(%r14), %rcx
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	movq	32(%r14), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	40(%r14), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	48(%r14), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	56(%r14), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	64(%r14), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	72(%r14), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	80(%r14), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	88(%r14), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	96(%r14), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	104(%r14), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	112(%r14), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	120(%r14), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	128(%r14), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	136(%r14), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	144(%r14), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	152(%r14), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	160(%r14), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	168(%r14), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	176(%r14), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	184(%r14), %rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movq	192(%r14), %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movq	200(%r14), %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movq	208(%r14), %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	%r9, %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movq	%rbx, -40(%rsp)         # 8-byte Spill
	subq	%rbx, %r9
	movq	%r9, -88(%rsp)          # 8-byte Spill
	leaq	(%rsi,%rdx,8), %rcx
	leaq	(%rbp,%rdx,8), %r9
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	leaq	(%r15,%rdx,8), %r14
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader359
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #       Child Loop BB0_6 Depth 3
	movl	%r10d, -104(%rsp)       # 4-byte Spill
	movl	-112(%rsp), %edi        # 4-byte Reload
	cmpl	%edi, -116(%rsp)        # 4-byte Folded Reload
	jge	.LBB0_8
# BB#3:                                 # %.preheader.lr.ph
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	-124(%rsp), %edi        # 4-byte Reload
	movl	%edi, -120(%rsp)        # 4-byte Spill
	movl	-116(%rsp), %ebx        # 4-byte Reload
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_6 Depth 3
	movl	%ebx, -100(%rsp)        # 4-byte Spill
	movq	-48(%rsp), %rdi         # 8-byte Reload
	cmpl	%edi, -40(%rsp)         # 4-byte Folded Reload
	movq	-64(%rsp), %rbx         # 8-byte Reload
	movq	-72(%rsp), %rdx         # 8-byte Reload
	movq	-80(%rsp), %rax         # 8-byte Reload
	jge	.LBB0_7
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_4 Depth=2
	movq	-88(%rsp), %r11         # 8-byte Reload
	movl	-120(%rsp), %edi        # 4-byte Reload
	movl	%edi, %r10d
	.p2align	4, 0x90
.LBB0_6:                                #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%r10d, %r10
	movq	%r13, %rdi
	movq	%rbx, %r13
	movq	152(%rsp), %rbx         # 8-byte Reload
	movsd	(%rbx,%r10,8), %xmm0    # xmm0 = mem[0],zero
	movq	%r13, %rbx
	movq	%rdi, %r13
	mulsd	-8(%r13,%r10,8), %xmm0
	movsd	(%rbx,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%r13,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	(%rdx,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	8(%r13,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movsd	(%rax,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	-8(%rsi,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	144(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rsi,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	136(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	8(%rsi,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	128(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	-8(%rcx,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	120(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rcx,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	112(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	8(%rcx,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	104(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	-8(%r12,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	96(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r12,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	88(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	8(%r12,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	80(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	-8(%rbp,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	72(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rbp,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	64(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	8(%rbp,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	56(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	-8(%r9,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r9,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	40(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	8(%r9,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	-8(%r8,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	24(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%r8,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	8(%r8,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	8(%rsp), %rdi           # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	-8(%r15,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	(%rsp), %rdi            # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r15,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	-8(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	8(%r15,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	-16(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	-8(%r14,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%r14,%r10,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	-32(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	8(%r14,%r10,8), %xmm0
	addsd	%xmm1, %xmm0
	movq	160(%rsp), %rdi         # 8-byte Reload
	movsd	%xmm0, (%rdi,%r10,8)
	incl	%r10d
	decq	%r11
	jne	.LBB0_6
.LBB0_7:                                # %._crit_edge
                                        #   in Loop: Header=BB0_4 Depth=2
	movl	-100(%rsp), %ebx        # 4-byte Reload
	incl	%ebx
	movl	-120(%rsp), %edi        # 4-byte Reload
	addl	-56(%rsp), %edi         # 4-byte Folded Reload
	movl	%edi, -120(%rsp)        # 4-byte Spill
	cmpl	-112(%rsp), %ebx        # 4-byte Folded Reload
	jne	.LBB0_4
.LBB0_8:                                # %._crit_edge362
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	-104(%rsp), %r10d       # 4-byte Reload
	incl	%r10d
	movl	-124(%rsp), %edi        # 4-byte Reload
	addl	-96(%rsp), %edi         # 4-byte Folded Reload
	movl	%edi, -124(%rsp)        # 4-byte Spill
	cmpl	-108(%rsp), %r10d       # 4-byte Folded Reload
	jne	.LBB0_2
.LBB0_9:                                # %._crit_edge364
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	rmatmult3, .Lfunc_end0-rmatmult3
	.cfi_endproc

	.type	kmin,@object            # @kmin
	.comm	kmin,4,4
	.type	kmax,@object            # @kmax
	.comm	kmax,4,4
	.type	jmin,@object            # @jmin
	.comm	jmin,4,4
	.type	jmax,@object            # @jmax
	.comm	jmax,4,4
	.type	imin,@object            # @imin
	.comm	imin,4,4
	.type	imax,@object            # @imax
	.comm	imax,4,4
	.type	kp,@object              # @kp
	.comm	kp,4,4
	.type	jp,@object              # @jp
	.comm	jp,4,4
	.type	i_lb,@object            # @i_lb
	.comm	i_lb,4,4
	.type	i_ub,@object            # @i_ub
	.comm	i_ub,4,4
	.type	x_size,@object          # @x_size
	.comm	x_size,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
