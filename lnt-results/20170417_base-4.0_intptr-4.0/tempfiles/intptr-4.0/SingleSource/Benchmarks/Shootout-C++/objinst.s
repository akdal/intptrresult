	.text
	.file	"objinst.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jne	.LBB0_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
.LBB0_2:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	$_ZTV6Toggle+16, (%r14)
	movb	$1, 8(%r14)
	movq	%r14, %rdi
	callq	_ZN6Toggle8activateEv
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_9
# BB#3:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit41.preheader
	xorl	%ebp, %ebp
	movl	$.L.str, %r15d
	.p2align	4, 0x90
.LBB0_4:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit41
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 56(%rbx)
	je	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movzbl	67(%rbx), %eax
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_7:                                # %_ZNKSt5ctypeIcE5widenEc.exit38
                                        #   in Loop: Header=BB0_4 Depth=1
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	incl	%ebp
	cmpl	$4, %ebp
	jg	.LBB0_10
# BB#8:                                 # %_ZNKSt5ctypeIcE5widenEc.exit38._crit_edge
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str.1, %esi
	cmovneq	%r15, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_4
.LBB0_9:                                # %._crit_edge55
	callq	_ZSt16__throw_bad_castv
.LBB0_10:                               # %._crit_edge48
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_23
# BB#11:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%rbx)
	je	.LBB0_13
# BB#12:
	movb	67(%rbx), %al
	jmp	.LBB0_14
.LBB0_13:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_14:                               # %_ZNKSt5ctypeIcE5widenEc.exit
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r14
	movb	$1, 8(%r14)
	movq	$_ZTV9NthToggle+16, (%r14)
	movq	$3, 12(%r14)
	movq	%r14, %rdi
	callq	_ZN9NthToggle8activateEv
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_21
# BB#15:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit42.preheader
	xorl	%ebp, %ebp
	movl	$.L.str, %r15d
	.p2align	4, 0x90
.LBB0_16:                               # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit42
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 56(%rbx)
	je	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_16 Depth=1
	movzbl	67(%rbx), %eax
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_16 Depth=1
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_19:                               # %_ZNKSt5ctypeIcE5widenEc.exit40
                                        #   in Loop: Header=BB0_16 Depth=1
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	incl	%ebp
	cmpl	$7, %ebp
	jg	.LBB0_22
# BB#20:                                # %_ZNKSt5ctypeIcE5widenEc.exit40._crit_edge
                                        #   in Loop: Header=BB0_16 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str.1, %esi
	cmovneq	%r15, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_16
.LBB0_21:                               # %._crit_edge53
	callq	_ZSt16__throw_bad_castv
.LBB0_22:                               # %._crit_edge
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_23:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.text._ZN6ToggleD2Ev,"axG",@progbits,_ZN6ToggleD2Ev,comdat
	.weak	_ZN6ToggleD2Ev
	.p2align	4, 0x90
	.type	_ZN6ToggleD2Ev,@function
_ZN6ToggleD2Ev:                         # @_ZN6ToggleD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN6ToggleD2Ev, .Lfunc_end1-_ZN6ToggleD2Ev
	.cfi_endproc

	.section	.text._ZN6ToggleD0Ev,"axG",@progbits,_ZN6ToggleD0Ev,comdat
	.weak	_ZN6ToggleD0Ev
	.p2align	4, 0x90
	.type	_ZN6ToggleD0Ev,@function
_ZN6ToggleD0Ev:                         # @_ZN6ToggleD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN6ToggleD0Ev, .Lfunc_end2-_ZN6ToggleD0Ev
	.cfi_endproc

	.section	.text._ZN6Toggle8activateEv,"axG",@progbits,_ZN6Toggle8activateEv,comdat
	.weak	_ZN6Toggle8activateEv
	.p2align	4, 0x90
	.type	_ZN6Toggle8activateEv,@function
_ZN6Toggle8activateEv:                  # @_ZN6Toggle8activateEv
	.cfi_startproc
# BB#0:
	xorb	$1, 8(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end3:
	.size	_ZN6Toggle8activateEv, .Lfunc_end3-_ZN6Toggle8activateEv
	.cfi_endproc

	.section	.text._ZN9NthToggleD0Ev,"axG",@progbits,_ZN9NthToggleD0Ev,comdat
	.weak	_ZN9NthToggleD0Ev
	.p2align	4, 0x90
	.type	_ZN9NthToggleD0Ev,@function
_ZN9NthToggleD0Ev:                      # @_ZN9NthToggleD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end4:
	.size	_ZN9NthToggleD0Ev, .Lfunc_end4-_ZN9NthToggleD0Ev
	.cfi_endproc

	.section	.text._ZN9NthToggle8activateEv,"axG",@progbits,_ZN9NthToggle8activateEv,comdat
	.weak	_ZN9NthToggle8activateEv
	.p2align	4, 0x90
	.type	_ZN9NthToggle8activateEv,@function
_ZN9NthToggle8activateEv:               # @_ZN9NthToggle8activateEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	cmpl	12(%rdi), %eax
	jl	.LBB5_2
# BB#1:
	xorb	$1, 8(%rdi)
	movl	$0, 16(%rdi)
.LBB5_2:
	movq	%rdi, %rax
	retq
.Lfunc_end5:
	.size	_ZN9NthToggle8activateEv, .Lfunc_end5-_ZN9NthToggle8activateEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_objinst.ii,@function
_GLOBAL__sub_I_objinst.ii:              # @_GLOBAL__sub_I_objinst.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end6:
	.size	_GLOBAL__sub_I_objinst.ii, .Lfunc_end6-_GLOBAL__sub_I_objinst.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"true"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"false"
	.size	.L.str.1, 6

	.type	_ZTV6Toggle,@object     # @_ZTV6Toggle
	.section	.rodata._ZTV6Toggle,"aG",@progbits,_ZTV6Toggle,comdat
	.weak	_ZTV6Toggle
	.p2align	3
_ZTV6Toggle:
	.quad	0
	.quad	_ZTI6Toggle
	.quad	_ZN6ToggleD2Ev
	.quad	_ZN6ToggleD0Ev
	.quad	_ZN6Toggle8activateEv
	.size	_ZTV6Toggle, 40

	.type	_ZTS6Toggle,@object     # @_ZTS6Toggle
	.section	.rodata._ZTS6Toggle,"aG",@progbits,_ZTS6Toggle,comdat
	.weak	_ZTS6Toggle
_ZTS6Toggle:
	.asciz	"6Toggle"
	.size	_ZTS6Toggle, 8

	.type	_ZTI6Toggle,@object     # @_ZTI6Toggle
	.section	.rodata._ZTI6Toggle,"aG",@progbits,_ZTI6Toggle,comdat
	.weak	_ZTI6Toggle
	.p2align	3
_ZTI6Toggle:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS6Toggle
	.size	_ZTI6Toggle, 16

	.type	_ZTV9NthToggle,@object  # @_ZTV9NthToggle
	.section	.rodata._ZTV9NthToggle,"aG",@progbits,_ZTV9NthToggle,comdat
	.weak	_ZTV9NthToggle
	.p2align	3
_ZTV9NthToggle:
	.quad	0
	.quad	_ZTI9NthToggle
	.quad	_ZN6ToggleD2Ev
	.quad	_ZN9NthToggleD0Ev
	.quad	_ZN9NthToggle8activateEv
	.size	_ZTV9NthToggle, 40

	.type	_ZTS9NthToggle,@object  # @_ZTS9NthToggle
	.section	.rodata._ZTS9NthToggle,"aG",@progbits,_ZTS9NthToggle,comdat
	.weak	_ZTS9NthToggle
_ZTS9NthToggle:
	.asciz	"9NthToggle"
	.size	_ZTS9NthToggle, 11

	.type	_ZTI9NthToggle,@object  # @_ZTI9NthToggle
	.section	.rodata._ZTI9NthToggle,"aG",@progbits,_ZTI9NthToggle,comdat
	.weak	_ZTI9NthToggle
	.p2align	4
_ZTI9NthToggle:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9NthToggle
	.quad	_ZTI6Toggle
	.size	_ZTI9NthToggle, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_objinst.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
