	.text
	.file	"getString.bc"
	.globl	getString
	.p2align	4, 0x90
	.type	getString,@function
getString:                              # @getString
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	fgetc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB0_8
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpl	$10, %ebp
	je	.LBB0_7
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpl	$13, %ebp
	je	.LBB0_7
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	callq	__ctype_b_loc
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movslq	%ebp, %rcx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_1
# BB#5:                                 # %.critedge
	cmpl	$13, %ebp
	je	.LBB0_7
# BB#6:                                 # %.critedge
	cmpl	$10, %ebp
	jne	.LBB0_9
.LBB0_7:                                # %.loopexit46
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	ungetc
.LBB0_8:                                # %.loopexit.loopexit
	xorl	%ebx, %ebx
.LBB0_20:                               # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_9:
	movb	%bpl, getString.buffer(%rip)
	movq	%r14, %rdi
	callq	fgetc
	movl	%eax, %ebp
	movl	$1, %r15d
	cmpl	$-1, %ebp
	jne	.LBB0_11
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_11 Depth=1
	movb	%bpl, getString.buffer(%r15)
	incq	%r15
	movq	%r14, %rdi
	callq	fgetc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB0_19
.LBB0_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movslq	%ebp, %rcx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_14
# BB#12:                                #   in Loop: Header=BB0_11 Depth=1
	cmpq	$1024, %r15             # imm = 0x400
	jl	.LBB0_18
# BB#13:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getString.name, %edi
	movl	$1, %esi
	callq	errorMessage
	callq	flushErrorMessage
	movl	$getString.buffer, %ebx
	movb	$0, getString.buffer+1(%r15)
	jmp	.LBB0_20
.LBB0_19:                               # %.critedge44
	movl	$getString.buffer, %ebx
	movb	$0, getString.buffer(%r15)
	jmp	.LBB0_20
.LBB0_14:
	movb	$0, getString.buffer(%r15)
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	ungetc
	cmpl	%ebp, %eax
	jne	.LBB0_17
# BB#15:
	cmpl	$-1, %eax
	je	.LBB0_17
# BB#16:
	movl	$getString.buffer, %ebx
	jmp	.LBB0_20
.LBB0_17:                               # %.critedge45
	xorl	%ebx, %ebx
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getString.name, %edi
	movl	$1, %esi
	callq	errorMessage
	callq	flushErrorMessage
	jmp	.LBB0_20
.Lfunc_end0:
	.size	getString, .Lfunc_end0-getString
	.cfi_endproc

	.type	getString.buffer,@object # @getString.buffer
	.local	getString.buffer
	.comm	getString.buffer,1025,16
	.type	getString.name,@object  # @getString.name
	.data
getString.name:
	.asciz	"getString"
	.size	getString.name, 10

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"maximum buffer length exceeded"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"error pushing character back onto stream"
	.size	.L.str.1, 41


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
