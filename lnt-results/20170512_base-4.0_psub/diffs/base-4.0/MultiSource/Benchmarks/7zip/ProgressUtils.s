	.text
	.file	"ProgressUtils.bc"
	.globl	_ZN14CLocalProgressC2Ev
	.p2align	4, 0x90
	.type	_ZN14CLocalProgressC2Ev,@function
_ZN14CLocalProgressC2Ev:                # @_ZN14CLocalProgressC2Ev
	.cfi_startproc
# BB#0:
	movl	$0, 8(%rdi)
	movq	$_ZTV14CLocalProgress+16, (%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 40(%rdi)
	movq	$0, 56(%rdi)
	movw	$257, 64(%rdi)          # imm = 0x101
	retq
.Lfunc_end0:
	.size	_ZN14CLocalProgressC2Ev, .Lfunc_end0-_ZN14CLocalProgressC2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN14CLocalProgress4InitEP9IProgressb
	.p2align	4, 0x90
	.type	_ZN14CLocalProgress4InitEP9IProgressb,@function
_ZN14CLocalProgress4InitEP9IProgressb:  # @_ZN14CLocalProgress4InitEP9IProgressb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	leaq	24(%rbx), %r15
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, (%r15)
.LBB2_2:                                # %_ZN9CMyComPtrI21ICompressProgressInfoE7ReleaseEv.exit
	testq	%rbp, %rbp
	je	.LBB2_4
# BB#3:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*8(%rax)
.LBB2_4:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_6
# BB#5:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB2_6:                                # %_ZN9CMyComPtrI9IProgressEaSEPS0_.exit
	movq	%rbp, 16(%rbx)
	movq	(%rbp), %rax
	movl	$IID_ICompressProgressInfo, %esi
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	*(%rax)
	movb	%r14b, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN14CLocalProgress4InitEP9IProgressb, .Lfunc_end2-_ZN14CLocalProgress4InitEP9IProgressb
	.cfi_endproc

	.globl	_ZN14CLocalProgress12SetRatioInfoEPKyS1_
	.p2align	4, 0x90
	.type	_ZN14CLocalProgress12SetRatioInfoEPKyS1_,@function
_ZN14CLocalProgress12SetRatioInfoEPKyS1_: # @_ZN14CLocalProgress12SetRatioInfoEPKyS1_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	48(%rbx), %rcx
	movq	%rcx, 8(%rsp)
	movq	56(%rbx), %rax
	movq	%rax, (%rsp)
	testq	%rsi, %rsi
	je	.LBB3_2
# BB#1:
	addq	(%rsi), %rcx
	movq	%rcx, 8(%rsp)
.LBB3_2:
	testq	%rdx, %rdx
	je	.LBB3_4
# BB#3:
	addq	(%rdx), %rax
	movq	%rax, (%rsp)
.LBB3_4:
	cmpb	$0, 64(%rbx)
	je	.LBB3_8
# BB#5:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#6:
	movq	(%rdi), %rax
	leaq	8(%rsp), %rsi
	movq	%rsp, %rdx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB3_11
# BB#7:                                 # %._crit_edge
	movq	8(%rsp), %rcx
	movq	(%rsp), %rax
.LBB3_8:
	movq	40(%rbx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, 8(%rsp)
	addq	%rdx, %rax
	movq	%rax, (%rsp)
	cmpb	$0, 65(%rbx)
	je	.LBB3_9
# BB#10:
	movq	16(%rbx), %rdi
	cmpb	$0, 32(%rbx)
	movq	(%rdi), %rax
	leaq	8(%rsp), %rcx
	movq	%rsp, %rsi
	cmovneq	%rcx, %rsi
	callq	*48(%rax)
	jmp	.LBB3_11
.LBB3_9:
	xorl	%eax, %eax
.LBB3_11:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZN14CLocalProgress12SetRatioInfoEPKyS1_, .Lfunc_end3-_ZN14CLocalProgress12SetRatioInfoEPKyS1_
	.cfi_endproc

	.globl	_ZN14CLocalProgress6SetCurEv
	.p2align	4, 0x90
	.type	_ZN14CLocalProgress6SetCurEv,@function
_ZN14CLocalProgress6SetCurEv:           # @_ZN14CLocalProgress6SetCurEv
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end4:
	.size	_ZN14CLocalProgress6SetCurEv, .Lfunc_end4-_ZN14CLocalProgress6SetCurEv
	.cfi_endproc

	.section	.text._ZN14CLocalProgress14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN14CLocalProgress14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN14CLocalProgress14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN14CLocalProgress14QueryInterfaceERK4GUIDPPv,@function
_ZN14CLocalProgress14QueryInterfaceERK4GUIDPPv: # @_ZN14CLocalProgress14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB5_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB5_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB5_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB5_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB5_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB5_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB5_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB5_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB5_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB5_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB5_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB5_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB5_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB5_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB5_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB5_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB5_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end5:
	.size	_ZN14CLocalProgress14QueryInterfaceERK4GUIDPPv, .Lfunc_end5-_ZN14CLocalProgress14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN14CLocalProgress6AddRefEv,"axG",@progbits,_ZN14CLocalProgress6AddRefEv,comdat
	.weak	_ZN14CLocalProgress6AddRefEv
	.p2align	4, 0x90
	.type	_ZN14CLocalProgress6AddRefEv,@function
_ZN14CLocalProgress6AddRefEv:           # @_ZN14CLocalProgress6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end6:
	.size	_ZN14CLocalProgress6AddRefEv, .Lfunc_end6-_ZN14CLocalProgress6AddRefEv
	.cfi_endproc

	.section	.text._ZN14CLocalProgress7ReleaseEv,"axG",@progbits,_ZN14CLocalProgress7ReleaseEv,comdat
	.weak	_ZN14CLocalProgress7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN14CLocalProgress7ReleaseEv,@function
_ZN14CLocalProgress7ReleaseEv:          # @_ZN14CLocalProgress7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB7_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB7_2:
	popq	%rcx
	retq
.Lfunc_end7:
	.size	_ZN14CLocalProgress7ReleaseEv, .Lfunc_end7-_ZN14CLocalProgress7ReleaseEv
	.cfi_endproc

	.section	.text._ZN14CLocalProgressD2Ev,"axG",@progbits,_ZN14CLocalProgressD2Ev,comdat
	.weak	_ZN14CLocalProgressD2Ev
	.p2align	4, 0x90
	.type	_ZN14CLocalProgressD2Ev,@function
_ZN14CLocalProgressD2Ev:                # @_ZN14CLocalProgressD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV14CLocalProgress+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp0:
	callq	*16(%rax)
.Ltmp1:
.LBB8_2:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp6:
	callq	*16(%rax)
.Ltmp7:
.LBB8_4:                                # %_ZN9CMyComPtrI9IProgressED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB8_7:
.Ltmp8:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_5:
.Ltmp2:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp3:
	callq	*16(%rax)
.Ltmp4:
.LBB8_8:                                # %_ZN9CMyComPtrI9IProgressED2Ev.exit5
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_9:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN14CLocalProgressD2Ev, .Lfunc_end8-_ZN14CLocalProgressD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp7           #   Call between .Ltmp7 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end8-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN14CLocalProgressD0Ev,"axG",@progbits,_ZN14CLocalProgressD0Ev,comdat
	.weak	_ZN14CLocalProgressD0Ev
	.p2align	4, 0x90
	.type	_ZN14CLocalProgressD0Ev,@function
_ZN14CLocalProgressD0Ev:                # @_ZN14CLocalProgressD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV14CLocalProgress+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp9:
	callq	*16(%rax)
.Ltmp10:
.LBB9_2:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB9_4:                                # %_ZN14CLocalProgressD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB9_8:
.Ltmp17:
	movq	%rax, %r14
	jmp	.LBB9_9
.LBB9_5:
.Ltmp11:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp12:
	callq	*16(%rax)
.Ltmp13:
.LBB9_9:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_7:
.Ltmp14:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN14CLocalProgressD0Ev, .Lfunc_end9-_ZN14CLocalProgressD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	1                       #   On action: 1
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp13     #   Call between .Ltmp13 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	_ZTV14CLocalProgress,@object # @_ZTV14CLocalProgress
	.section	.rodata,"a",@progbits
	.globl	_ZTV14CLocalProgress
	.p2align	3
_ZTV14CLocalProgress:
	.quad	0
	.quad	_ZTI14CLocalProgress
	.quad	_ZN14CLocalProgress14QueryInterfaceERK4GUIDPPv
	.quad	_ZN14CLocalProgress6AddRefEv
	.quad	_ZN14CLocalProgress7ReleaseEv
	.quad	_ZN14CLocalProgressD2Ev
	.quad	_ZN14CLocalProgressD0Ev
	.quad	_ZN14CLocalProgress12SetRatioInfoEPKyS1_
	.size	_ZTV14CLocalProgress, 64

	.type	_ZTS14CLocalProgress,@object # @_ZTS14CLocalProgress
	.globl	_ZTS14CLocalProgress
	.p2align	4
_ZTS14CLocalProgress:
	.asciz	"14CLocalProgress"
	.size	_ZTS14CLocalProgress, 17

	.type	_ZTS21ICompressProgressInfo,@object # @_ZTS21ICompressProgressInfo
	.section	.rodata._ZTS21ICompressProgressInfo,"aG",@progbits,_ZTS21ICompressProgressInfo,comdat
	.weak	_ZTS21ICompressProgressInfo
	.p2align	4
_ZTS21ICompressProgressInfo:
	.asciz	"21ICompressProgressInfo"
	.size	_ZTS21ICompressProgressInfo, 24

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI21ICompressProgressInfo,@object # @_ZTI21ICompressProgressInfo
	.section	.rodata._ZTI21ICompressProgressInfo,"aG",@progbits,_ZTI21ICompressProgressInfo,comdat
	.weak	_ZTI21ICompressProgressInfo
	.p2align	4
_ZTI21ICompressProgressInfo:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21ICompressProgressInfo
	.quad	_ZTI8IUnknown
	.size	_ZTI21ICompressProgressInfo, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI14CLocalProgress,@object # @_ZTI14CLocalProgress
	.section	.rodata,"a",@progbits
	.globl	_ZTI14CLocalProgress
	.p2align	4
_ZTI14CLocalProgress:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS14CLocalProgress
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI21ICompressProgressInfo
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI14CLocalProgress, 56


	.globl	_ZN14CLocalProgressC1Ev
	.type	_ZN14CLocalProgressC1Ev,@function
_ZN14CLocalProgressC1Ev = _ZN14CLocalProgressC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
