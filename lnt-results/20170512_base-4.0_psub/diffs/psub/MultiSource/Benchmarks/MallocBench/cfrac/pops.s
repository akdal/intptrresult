	.text
	.file	"pops.bc"
	.globl	pnorm
	.p2align	4, 0x90
	.type	pnorm,@function
pnorm:                                  # @pnorm
	.cfi_startproc
# BB#0:
	leaq	8(%rdi), %rax
	movzwl	4(%rdi), %ecx
	addq	%rcx, %rcx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movzwl	6(%rdi,%rcx), %esi
	leaq	-2(%rcx), %rdx
	testw	%si, %si
	jne	.LBB0_3
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	leaq	6(%rdi,%rcx), %rcx
	cmpq	%rax, %rcx
	movq	%rdx, %rcx
	ja	.LBB0_1
.LBB0_3:
	testw	%si, %si
	setne	%sil
	testq	%rdx, %rdx
	leaq	8(%rdi,%rdx), %rcx
	jne	.LBB0_6
# BB#4:
	testb	%sil, %sil
	jne	.LBB0_6
# BB#5:
	movb	$0, 6(%rdi)
.LBB0_6:
	subl	%eax, %ecx
	shrl	%ecx
	incl	%ecx
	movw	%cx, 4(%rdi)
	retq
.Lfunc_end0:
	.size	pnorm, .Lfunc_end0-pnorm
	.cfi_endproc

	.globl	palloc
	.p2align	4, 0x90
	.type	palloc,@function
palloc:                                 # @palloc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movzwl	%bx, %eax
	leal	8(%rax,%rax), %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB1_2
# BB#1:
	movw	$1, (%rax)
	movw	%bx, 2(%rax)
	movw	%bx, 4(%rax)
	popq	%rbx
	retq
.LBB1_2:
	movl	$1, %edi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	popq	%rbx
	jmp	errorp                  # TAILCALL
.Lfunc_end1:
	.size	palloc, .Lfunc_end1-palloc
	.cfi_endproc

	.globl	pfree
	.p2align	4, 0x90
	.type	pfree,@function
pfree:                                  # @pfree
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	callq	free
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	pfree, .Lfunc_end2-pfree
	.cfi_endproc

	.globl	psetv
	.p2align	4, 0x90
	.type	psetv,@function
psetv:                                  # @psetv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB3_2
# BB#1:
	movl	$4, %edi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	callq	errorp
.LBB3_2:
	movq	(%rbx), %r15
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.LBB3_4
# BB#3:
	incw	(%r14)
.LBB3_4:
	testq	%r15, %r15
	je	.LBB3_9
# BB#5:
	cmpb	$2, 6(%r15)
	jb	.LBB3_7
# BB#6:
	movl	$4, %edi
	movl	$.L.str.2, %esi
	movl	$.L.str.4, %edx
	callq	errorp
.LBB3_7:
	decw	(%r15)
	jne	.LBB3_9
# BB#8:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	free
.LBB3_9:
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	psetv, .Lfunc_end3-psetv
	.cfi_endproc

	.globl	pparmv
	.p2align	4, 0x90
	.type	pparmv,@function
pparmv:                                 # @pparmv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB4_2
# BB#1:
	movl	$4, %edi
	movl	$.L.str.5, %esi
	movl	$.L.str.6, %edx
	callq	errorp
.LBB4_2:
	cmpb	$2, 6(%rbx)
	jb	.LBB4_4
# BB#3:
	movl	$4, %edi
	movl	$.L.str.5, %esi
	movl	$.L.str.4, %edx
	callq	errorp
.LBB4_4:
	incw	(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	pparmv, .Lfunc_end4-pparmv
	.cfi_endproc

	.globl	pparmf
	.p2align	4, 0x90
	.type	pparmf,@function
pparmf:                                 # @pparmf
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	incw	(%rdi)
.LBB5_2:
	movq	%rdi, %rax
	retq
.Lfunc_end5:
	.size	pparmf, .Lfunc_end5-pparmf
	.cfi_endproc

	.globl	pdestroyf
	.p2align	4, 0x90
	.type	pdestroyf,@function
pdestroyf:                              # @pdestroyf
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB6_2
# BB#1:
	decw	(%rdi)
	je	.LBB6_3
.LBB6_2:
	retq
.LBB6_3:
	xorl	%eax, %eax
	jmp	free                    # TAILCALL
.Lfunc_end6:
	.size	pdestroyf, .Lfunc_end6-pdestroyf
	.cfi_endproc

	.globl	pnew
	.p2align	4, 0x90
	.type	pnew,@function
pnew:                                   # @pnew
	.cfi_startproc
# BB#0:
	incw	(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end7:
	.size	pnew, .Lfunc_end7-pnew
	.cfi_endproc

	.globl	presult
	.p2align	4, 0x90
	.type	presult,@function
presult:                                # @presult
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:
	decw	(%rdi)
.LBB8_2:
	movq	%rdi, %rax
	retq
.Lfunc_end8:
	.size	presult, .Lfunc_end8-presult
	.cfi_endproc

	.globl	psetq
	.p2align	4, 0x90
	.type	psetq,@function
psetq:                                  # @psetq
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	%rdi, %rax
	movq	(%rax), %rdi
	movq	%rbx, (%rax)
	testq	%rbx, %rbx
	je	.LBB9_2
# BB#1:
	incw	(%rbx)
.LBB9_2:
	testq	%rdi, %rdi
	je	.LBB9_5
# BB#3:
	decw	(%rdi)
	jne	.LBB9_5
# BB#4:
	xorl	%eax, %eax
	callq	free
.LBB9_5:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	psetq, .Lfunc_end9-psetq
	.cfi_endproc

	.type	pcache,@object          # @pcache
	.comm	pcache,512,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"palloc"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"out of memory"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"pset"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"lvalue is pNull"
	.size	.L.str.3, 16

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"invalid precision"
	.size	.L.str.4, 18

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"pparm"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"undefined function argument"
	.size	.L.str.6, 28


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
