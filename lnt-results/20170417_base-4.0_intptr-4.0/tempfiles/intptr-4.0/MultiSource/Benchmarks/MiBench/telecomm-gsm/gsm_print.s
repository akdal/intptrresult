	.text
	.file	"gsm_print.bc"
	.globl	gsm_print
	.p2align	4, 0x90
	.type	gsm_print,@function
gsm_print:                              # @gsm_print
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi6:
	.cfi_def_cfa_offset 384
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movzbl	(%r15), %ecx
	movl	%ecx, %edx
	andb	$-16, %dl
	movl	$-1, %eax
	cmpb	$-48, %dl
	jne	.LBB0_2
# BB#1:
	andl	$15, %ecx
	movzbl	1(%r15), %edx
	movl	%edx, 160(%rsp)         # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ecx
	movl	%ecx, 164(%rsp)         # 4-byte Spill
	movb	2(%r15), %cl
	movb	5(%r15), %dl
	movb	%cl, 4(%rsp)            # 1-byte Spill
	shlb	$2, %cl
	movzbl	3(%r15), %r14d
	movl	%r14d, %esi
	shrl	$6, %esi
	movzbl	%cl, %ecx
	orl	%esi, %ecx
	movl	%ecx, 156(%rsp)         # 4-byte Spill
	movb	%dl, 16(%rsp)           # 1-byte Spill
	addb	%dl, %dl
	movzbl	6(%r15), %r13d
	movl	%r13d, %ecx
	shrl	$7, %ecx
	movzbl	%dl, %edx
	orl	%ecx, %edx
	movl	%edx, 152(%rsp)         # 4-byte Spill
	movb	9(%r15), %dl
	movb	%dl, 15(%rsp)           # 1-byte Spill
	movb	%dl, 32(%rsp)           # 1-byte Spill
	addb	%dl, %dl
	movzbl	10(%r15), %ecx
	movl	%ecx, %esi
	shrl	$7, %esi
	movzbl	%dl, %edx
	orl	%esi, %edx
	movq	%rdx, 320(%rsp)         # 8-byte Spill
	movb	12(%r15), %dl
	movb	%dl, 14(%rsp)           # 1-byte Spill
	addb	%dl, %dl
	movzbl	13(%r15), %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%eax, %esi
	shrl	$7, %esi
	movzbl	%dl, %edx
	orl	%esi, %edx
	movl	%edx, 148(%rsp)         # 4-byte Spill
	movb	16(%r15), %dl
	movb	%dl, 12(%rsp)           # 1-byte Spill
	movb	%dl, 40(%rsp)           # 1-byte Spill
	addb	%dl, %dl
	movzbl	17(%r15), %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%eax, %esi
	shrl	$7, %esi
	movzbl	%dl, %edx
	orl	%esi, %edx
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	movb	19(%r15), %dl
	movb	%dl, 13(%rsp)           # 1-byte Spill
	addb	%dl, %dl
	movzbl	20(%r15), %r8d
	movl	%r8d, %esi
	movq	%r8, 80(%rsp)           # 8-byte Spill
	shrl	$7, %esi
	movzbl	%dl, %edx
	orl	%esi, %edx
	movl	%edx, 144(%rsp)         # 4-byte Spill
	movb	23(%r15), %dl
	movb	%dl, 10(%rsp)           # 1-byte Spill
	movb	%dl, 7(%rsp)            # 1-byte Spill
	addb	%dl, %dl
	movzbl	24(%r15), %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rdi, 272(%rsp)         # 8-byte Spill
	movl	%eax, %edi
	shrl	$7, %edi
	movzbl	%dl, %eax
	orl	%edi, %eax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movb	26(%r15), %dl
	movb	%dl, 11(%rsp)           # 1-byte Spill
	addb	%dl, %dl
	movzbl	27(%r15), %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	shrl	$7, %ebx
	movzbl	%dl, %edx
	orl	%ebx, %edx
	movl	%edx, 140(%rsp)         # 4-byte Spill
	movb	30(%r15), %bl
	movb	%bl, 9(%rsp)            # 1-byte Spill
	movb	%bl, 8(%rsp)            # 1-byte Spill
	addb	%bl, %bl
	movzbl	31(%r15), %r11d
	movl	%r11d, %ebp
	shrl	$7, %ebp
	movzbl	%bl, %eax
	orl	%ebp, %eax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movzbl	4(%r15), %r10d
	movl	%r10d, %ebp
	shrl	$6, %ebp
	leal	(%rbp,%r14,4), %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
                                        # kill: %R14B<def> %R14B<kill> %R14<kill>
	movzbl	7(%r15), %ebx
	movl	%ebx, %ebp
	shrl	$7, %ebp
	leal	(%rbp,%r13,2), %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movl	%r13d, %eax
	movb	%al, 5(%rsp)            # 1-byte Spill
	movzbl	8(%r15), %r12d
	movl	%r12d, %ebp
	movq	%r12, 216(%rsp)         # 8-byte Spill
	shrl	$6, %ebp
	leal	(%rbp,%rbx,4), %eax
	movl	%eax, 112(%rsp)         # 4-byte Spill
	movl	%ebx, %eax
	movb	%al, 48(%rsp)           # 1-byte Spill
	movb	%al, 56(%rsp)           # 1-byte Spill
	movzbl	11(%r15), %ebx
	movq	%rbx, 168(%rsp)         # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	shrl	$6, %ebx
	leal	(%rbx,%rcx,4), %eax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movb	4(%rsp), %bl            # 1-byte Reload
	shrb	$3, %bl
	movl	%ecx, %eax
	movb	%al, 6(%rsp)            # 1-byte Spill
	movzbl	%bl, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movl	%ecx, %eax
	movb	%al, 4(%rsp)            # 1-byte Spill
	movzbl	14(%r15), %ebp
	movl	%ebp, %ecx
	shrl	$7, %ecx
	movq	96(%rsp), %rdx          # 8-byte Reload
	leal	(%rcx,%rdx,2), %eax
	movl	%eax, 116(%rsp)         # 4-byte Spill
	movzbl	15(%r15), %esi
	movl	%esi, %ecx
	movq	%rsi, 240(%rsp)         # 8-byte Spill
	shrl	$6, %ecx
	leal	(%rcx,%rbp,4), %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movzbl	18(%r15), %r9d
	movl	%r9d, %ecx
	movq	%r9, 232(%rsp)          # 8-byte Spill
	shrl	$6, %ecx
	movq	88(%rsp), %rdi          # 8-byte Reload
	leal	(%rcx,%rdi,4), %eax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movzbl	21(%r15), %r13d
	movl	%r13d, %ecx
	shrl	$7, %ecx
	leal	(%rcx,%r8,2), %eax
	movl	%eax, 124(%rsp)         # 4-byte Spill
	movzbl	22(%r15), %r8d
	movl	%r8d, %ecx
	movq	%r8, 256(%rsp)          # 8-byte Spill
	shrl	$6, %ecx
	leal	(%rcx,%r13,4), %eax
	movl	%eax, 128(%rsp)         # 4-byte Spill
	movzbl	25(%r15), %ecx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	shrl	$6, %ecx
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	(%rcx,%rax,4), %eax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movzbl	28(%r15), %ecx
	movl	%ecx, %ebx
	shrl	$7, %ebx
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	(%rbx,%rax,2), %eax
	movl	%eax, 132(%rsp)         # 4-byte Spill
	movzbl	29(%r15), %ebx
	movq	%rbx, 184(%rsp)         # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	shrl	$6, %ebx
	leal	(%rbx,%rcx,4), %eax
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movzbl	32(%r15), %eax
	movq	%rax, 192(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$6, %eax
	leal	(%rax,%r11,4), %eax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	shrb	$2, %r14b
	andb	$15, %r14b
	movzbl	%r14b, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movl	%r10d, %eax
	shrb	$3, %al
	andb	$7, %al
	shrb	16(%rsp)                # 1-byte Folded Spill
	movb	5(%rsp), %bl            # 1-byte Reload
	shrb	$5, %bl
	andb	$3, %bl
	movb	%bl, 5(%rsp)            # 1-byte Spill
	movb	48(%rsp), %bl           # 1-byte Reload
	shrb	$4, %bl
	andb	$7, %bl
	movb	%bl, 48(%rsp)           # 1-byte Spill
	movb	56(%rsp), %bl           # 1-byte Reload
	shrb	%bl
	andb	$7, %bl
	movb	%bl, 56(%rsp)           # 1-byte Spill
	movzbl	%al, %eax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movl	%r12d, %r14d
	shrb	$3, %r14b
	andb	$7, %r14b
	shrb	$5, 15(%rsp)            # 1-byte Folded Spill
	movb	32(%rsp), %al           # 1-byte Reload
	shrb	$2, %al
	andb	$7, %al
	movb	%al, 32(%rsp)           # 1-byte Spill
	movb	6(%rsp), %al            # 1-byte Reload
	shrb	$4, %al
	andb	$7, %al
	movb	%al, 6(%rsp)            # 1-byte Spill
	movb	4(%rsp), %al            # 1-byte Reload
	shrb	%al
	andb	$7, %al
	movb	%al, 4(%rsp)            # 1-byte Spill
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ebx
	shrb	$3, %bl
	andb	$7, %bl
	shrb	14(%rsp)                # 1-byte Folded Spill
	movl	%edx, %r15d
	shrb	$5, %r15b
	andb	$3, %r15b
	movl	%ebp, %eax
	shrb	$4, %al
	andb	$7, %al
	movb	%al, 21(%rsp)           # 1-byte Spill
	movl	%ebp, %eax
	shrb	%al
	andb	$7, %al
	movb	%al, 19(%rsp)           # 1-byte Spill
	movl	%esi, %eax
	shrb	$3, %al
	andb	$7, %al
	movb	%al, 18(%rsp)           # 1-byte Spill
	shrb	$5, 12(%rsp)            # 1-byte Folded Spill
	movb	40(%rsp), %al           # 1-byte Reload
	shrb	$2, %al
	andb	$7, %al
	movb	%al, 40(%rsp)           # 1-byte Spill
	movl	%edi, %edx
	shrb	$4, %dl
	andb	$7, %dl
	movb	%dl, 20(%rsp)           # 1-byte Spill
	movl	%edi, %eax
	shrb	%al
	andb	$7, %al
	movb	%al, 17(%rsp)           # 1-byte Spill
	movl	%r9d, %eax
	shrb	$3, %al
	andb	$7, %al
	movb	%al, 22(%rsp)           # 1-byte Spill
	shrb	13(%rsp)                # 1-byte Folded Spill
	movq	80(%rsp), %rax          # 8-byte Reload
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	shrb	$5, %al
	andb	$3, %al
	movb	%al, 23(%rsp)           # 1-byte Spill
	movl	%r13d, %eax
	shrb	$4, %al
	andb	$7, %al
	movb	%al, 28(%rsp)           # 1-byte Spill
	movl	%r13d, %eax
	shrb	%al
	andb	$7, %al
	movb	%al, 24(%rsp)           # 1-byte Spill
	movl	%r8d, %eax
	shrb	$3, %al
	andb	$7, %al
	movb	%al, 27(%rsp)           # 1-byte Spill
	shrb	$5, 10(%rsp)            # 1-byte Folded Spill
	movb	7(%rsp), %al            # 1-byte Reload
	shrb	$2, %al
	andb	$7, %al
	movb	%al, 7(%rsp)            # 1-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edx
	shrb	$4, %dl
	andb	$7, %dl
	movb	%dl, 29(%rsp)           # 1-byte Spill
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	shrb	%al
	andb	$7, %al
	movb	%al, 25(%rsp)           # 1-byte Spill
	movq	176(%rsp), %rax         # 8-byte Reload
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	shrb	$3, %al
	andb	$7, %al
	movb	%al, 26(%rsp)           # 1-byte Spill
	shrb	11(%rsp)                # 1-byte Folded Spill
	movq	64(%rsp), %rax          # 8-byte Reload
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	shrb	$5, %al
	andb	$3, %al
	movb	%al, 30(%rsp)           # 1-byte Spill
	movl	%ecx, %eax
	shrb	$4, %al
	andb	$7, %al
	movb	%al, 88(%rsp)           # 1-byte Spill
	movl	%ecx, %eax
	shrb	%al
	andb	$7, %al
	movb	%al, 31(%rsp)           # 1-byte Spill
	movq	184(%rsp), %rax         # 8-byte Reload
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	shrb	$3, %al
	andb	$7, %al
	movb	%al, 80(%rsp)           # 1-byte Spill
	shrb	$5, 9(%rsp)             # 1-byte Folded Spill
	movb	8(%rsp), %al            # 1-byte Reload
	shrb	$2, %al
	andb	$7, %al
	movb	%al, 8(%rsp)            # 1-byte Spill
	movl	%r11d, %eax
	shrb	$4, %al
	andb	$7, %al
	movb	%al, 64(%rsp)           # 1-byte Spill
	movl	%r11d, %eax
	shrb	%al
	andb	$7, %al
	movb	%al, 96(%rsp)           # 1-byte Spill
	movq	192(%rsp), %rax         # 8-byte Reload
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	shrb	$3, %al
	andb	$7, %al
	movb	%al, 72(%rsp)           # 1-byte Spill
	movl	160(%rsp), %ecx         # 4-byte Reload
	andl	$63, %ecx
	movl	156(%rsp), %r9d         # 4-byte Reload
	andl	$31, %r9d
	movq	224(%rsp), %rbp         # 8-byte Reload
	andl	$15, %ebp
	andl	$7, %r10d
	movl	$.L.str, %esi
	movl	$0, %eax
	movl	104(%rsp), %r8d         # 4-byte Reload
	movq	272(%rsp), %r12         # 8-byte Reload
	movq	%r12, %rdi
	movl	164(%rsp), %edx         # 4-byte Reload
	pushq	%r10
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	208(%rsp)               # 8-byte Folded Reload
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)               # 8-byte Folded Reload
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	movzbl	16(%rsp), %edx          # 1-byte Folded Reload
	movl	152(%rsp), %ecx         # 4-byte Reload
	andl	$3, %ecx
	movzbl	5(%rsp), %r8d           # 1-byte Folded Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	andl	$63, %r9d
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r12, %rbp
	callq	fprintf
	movzbl	48(%rsp), %edx          # 1-byte Folded Reload
	movzbl	56(%rsp), %ecx          # 1-byte Folded Reload
	movl	112(%rsp), %r8d         # 4-byte Reload
	andl	$7, %r8d
	movzbl	%r14b, %r9d
	movq	216(%rsp), %r12         # 8-byte Reload
	andl	$7, %r12d
	movzbl	15(%rsp), %eax          # 1-byte Folded Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movzbl	32(%rsp), %eax          # 1-byte Folded Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	320(%rsp), %r13         # 8-byte Reload
	andl	$7, %r13d
	movzbl	6(%rsp), %eax           # 1-byte Folded Reload
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movzbl	4(%rsp), %r14d          # 1-byte Folded Reload
	movq	248(%rsp), %r10         # 8-byte Reload
	andl	$7, %r10d
	movzbl	%bl, %ebx
	movq	168(%rsp), %r11         # 8-byte Reload
	andl	$7, %r11d
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.2, %esi
	movl	$0, %eax
	movq	%rbp, %rdi
	pushq	%r11
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)               # 8-byte Folded Reload
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$80, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -80
	movzbl	14(%rsp), %edx          # 1-byte Folded Reload
	movl	148(%rsp), %ecx         # 4-byte Reload
	andl	$3, %ecx
	movzbl	%r15b, %r8d
	movl	116(%rsp), %r9d         # 4-byte Reload
	andl	$63, %r9d
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	fprintf
	movzbl	21(%rsp), %edx          # 1-byte Folded Reload
	movzbl	19(%rsp), %ecx          # 1-byte Folded Reload
	movl	120(%rsp), %r8d         # 4-byte Reload
	andl	$7, %r8d
	movzbl	18(%rsp), %r9d          # 1-byte Folded Reload
	movq	240(%rsp), %r13         # 8-byte Reload
	andl	$7, %r13d
	movzbl	12(%rsp), %eax          # 1-byte Folded Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movzbl	40(%rsp), %eax          # 1-byte Folded Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	312(%rsp), %rbx         # 8-byte Reload
	andl	$7, %ebx
	movzbl	20(%rsp), %r14d         # 1-byte Folded Reload
	movzbl	17(%rsp), %r15d         # 1-byte Folded Reload
	movq	264(%rsp), %r10         # 8-byte Reload
	andl	$7, %r10d
	movzbl	22(%rsp), %r12d         # 1-byte Folded Reload
	movq	232(%rsp), %r11         # 8-byte Reload
	andl	$7, %r11d
	subq	$8, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.2, %esi
	movl	$0, %eax
	movq	%rbp, %rdi
	pushq	%r11
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$80, %rsp
.Lcfi39:
	.cfi_adjust_cfa_offset -80
	movzbl	13(%rsp), %edx          # 1-byte Folded Reload
	movl	144(%rsp), %ecx         # 4-byte Reload
	andl	$3, %ecx
	movzbl	23(%rsp), %r8d          # 1-byte Folded Reload
	movl	124(%rsp), %r9d         # 4-byte Reload
	andl	$63, %r9d
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbp, %r14
	callq	fprintf
	movzbl	28(%rsp), %edx          # 1-byte Folded Reload
	movzbl	24(%rsp), %ecx          # 1-byte Folded Reload
	movl	128(%rsp), %r8d         # 4-byte Reload
	andl	$7, %r8d
	movzbl	27(%rsp), %r9d          # 1-byte Folded Reload
	movq	256(%rsp), %r13         # 8-byte Reload
	andl	$7, %r13d
	movzbl	10(%rsp), %eax          # 1-byte Folded Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movzbl	7(%rsp), %eax           # 1-byte Folded Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	304(%rsp), %rbx         # 8-byte Reload
	andl	$7, %ebx
	movzbl	29(%rsp), %r11d         # 1-byte Folded Reload
	movzbl	25(%rsp), %r15d         # 1-byte Folded Reload
	movq	280(%rsp), %rbp         # 8-byte Reload
	andl	$7, %ebp
	movzbl	26(%rsp), %r12d         # 1-byte Folded Reload
	movq	176(%rsp), %r10         # 8-byte Reload
	andl	$7, %r10d
	subq	$8, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.2, %esi
	movl	$0, %eax
	movq	%r14, %rdi
	pushq	%r10
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$80, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset -80
	movzbl	11(%rsp), %edx          # 1-byte Folded Reload
	movl	140(%rsp), %ecx         # 4-byte Reload
	andl	$3, %ecx
	movzbl	30(%rsp), %r8d          # 1-byte Folded Reload
	movl	132(%rsp), %r9d         # 4-byte Reload
	andl	$63, %r9d
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movzbl	88(%rsp), %edx          # 1-byte Folded Reload
	movzbl	31(%rsp), %ecx          # 1-byte Folded Reload
	movl	136(%rsp), %r8d         # 4-byte Reload
	andl	$7, %r8d
	movzbl	80(%rsp), %r9d          # 1-byte Folded Reload
	movq	184(%rsp), %r13         # 8-byte Reload
	andl	$7, %r13d
	movzbl	9(%rsp), %eax           # 1-byte Folded Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movzbl	8(%rsp), %eax           # 1-byte Folded Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	296(%rsp), %rbx         # 8-byte Reload
	andl	$7, %ebx
	movzbl	64(%rsp), %r11d         # 1-byte Folded Reload
	movzbl	96(%rsp), %r15d         # 1-byte Folded Reload
	movq	288(%rsp), %rbp         # 8-byte Reload
	andl	$7, %ebp
	movzbl	72(%rsp), %r12d         # 1-byte Folded Reload
	movq	192(%rsp), %r10         # 8-byte Reload
	andl	$7, %r10d
	subq	$8, %rsp
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.2, %esi
	movl	$0, %eax
	movq	%r14, %rdi
	pushq	%r10
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	xorl	%eax, %eax
	addq	$80, %rsp
.Lcfi61:
	.cfi_adjust_cfa_offset -80
.LBB0_2:
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	gsm_print, .Lfunc_end0-gsm_print
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"LARc:\t%2.2d  %2.2d  %2.2d  %2.2d  %2.2d  %2.2d  %2.2d  %2.2d\n"
	.size	.L.str, 62

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"#1: \tNc %4.4d    bc %d    Mc %d    xmaxc %d\n"
	.size	.L.str.1, 45

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\t%.2d %.2d %.2d %.2d %.2d %.2d %.2d %.2d %.2d %.2d %.2d %.2d %.2d\n"
	.size	.L.str.2, 67

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"#2: \tNc %4.4d    bc %d    Mc %d    xmaxc %d\n"
	.size	.L.str.3, 45

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"#3: \tNc %4.4d    bc %d    Mc %d    xmaxc %d\n"
	.size	.L.str.4, 45

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"#4: \tNc %4.4d    bc %d    Mc %d    xmaxc %d\n"
	.size	.L.str.5, 45


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
