	.text
	.file	"GridInit.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4746794007244308480     # double 2147483647
	.text
	.globl	generate_grids
	.p2align	4, 0x90
	.type	generate_grids,@function
generate_grids:                         # @generate_grids
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	testq	%r15, %r15
	jle	.LBB0_6
# BB#1:
	testq	%r14, %r14
	jle	.LBB0_6
# BB#2:                                 # %.preheader.us.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	xorl	%ebx, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI0_0(%rip), %xmm0
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, (%rax,%rbx)
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI0_0(%rip), %xmm0
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, 8(%rax,%rbx)
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI0_0(%rip), %xmm0
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, 16(%rax,%rbx)
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI0_0(%rip), %xmm0
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, 24(%rax,%rbx)
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI0_0(%rip), %xmm0
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, 32(%rax,%rbx)
	callq	rand
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm1, %xmm0
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, 40(%rax,%rbx)
	addq	$48, %rbx
	decq	%rbp
	jne	.LBB0_4
# BB#5:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB0_3 Depth=1
	incq	%r13
	cmpq	%r15, %r13
	jne	.LBB0_3
.LBB0_6:                                # %._crit_edge29
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	generate_grids, .Lfunc_end0-generate_grids
	.cfi_endproc

	.globl	generate_grids_v
	.p2align	4, 0x90
	.type	generate_grids_v,@function
generate_grids_v:                       # @generate_grids_v
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	testq	%r15, %r15
	jle	.LBB1_6
# BB#1:
	testq	%r14, %r14
	jle	.LBB1_6
# BB#2:                                 # %.preheader.us.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
	xorl	%ebx, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	rn_v
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, (%rax,%rbx)
	callq	rn_v
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, 8(%rax,%rbx)
	callq	rn_v
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, 16(%rax,%rbx)
	callq	rn_v
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, 24(%rax,%rbx)
	callq	rn_v
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, 32(%rax,%rbx)
	callq	rn_v
	movq	(%r12,%r13,8), %rax
	movsd	%xmm0, 40(%rax,%rbx)
	addq	$48, %rbx
	decq	%rbp
	jne	.LBB1_4
# BB#5:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB1_3 Depth=1
	incq	%r13
	cmpq	%r15, %r13
	jne	.LBB1_3
.LBB1_6:                                # %._crit_edge29
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	generate_grids_v, .Lfunc_end1-generate_grids_v
	.cfi_endproc

	.globl	sort_nuclide_grids
	.p2align	4, 0x90
	.type	sort_nuclide_grids,@function
sort_nuclide_grids:                     # @sort_nuclide_grids
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testq	%r15, %r15
	jle	.LBB2_2
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movl	$48, %edx
	movl	$NGP_compare, %ecx
	movq	%r14, %rsi
	callq	qsort
	addq	$8, %rbx
	decq	%r15
	jne	.LBB2_1
.LBB2_2:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	sort_nuclide_grids, .Lfunc_end2-sort_nuclide_grids
	.cfi_endproc

	.globl	generate_energy_grid
	.p2align	4, 0x90
	.type	generate_energy_grid,@function
generate_energy_grid:                   # @generate_energy_grid
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	$.Lstr, %edi
	callq	puts
	movq	%r12, %rbx
	imulq	%r15, %rbx
	movq	%rbx, %rbp
	shlq	$4, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %r14
	movl	$.Lstr.1, %edi
	callq	puts
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	gpmatrix
	movq	%rax, %r12
	movq	(%r12), %rdi
	movq	(%r13), %rsi
	leaq	(%rbp,%rbp,2), %rdx
	callq	memcpy
	movq	(%r12), %rdi
	movl	$48, %edx
	movl	$NGP_compare, %ecx
	movq	%rbx, %rsi
	callq	qsort
	movl	$.Lstr.2, %edi
	callq	puts
	testq	%rbx, %rbx
	jle	.LBB3_7
# BB#1:                                 # %.lr.ph55
	movq	(%r12), %rax
	leaq	-1(%rbx), %r8
	movq	%rbx, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB3_4
# BB#2:                                 # %.prol.preheader
	movq	%rax, %rdi
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdx
	movq	%rdx, (%rbp)
	incq	%rcx
	addq	$16, %rbp
	addq	$48, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB3_3
.LBB3_4:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_7
# BB#5:                                 # %.lr.ph55.new
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	%rcx, %rsi
	shlq	$4, %rsi
	leaq	48(%r14,%rsi), %rsi
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	addq	%rcx, %rax
	.p2align	4, 0x90
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	%rcx, -48(%rsi)
	movq	48(%rax), %rcx
	movq	%rcx, -32(%rsi)
	movq	96(%rax), %rcx
	movq	%rcx, -16(%rsi)
	movq	144(%rax), %rcx
	movq	%rcx, (%rsi)
	addq	$64, %rsi
	addq	$192, %rax
	addq	$-4, %rdx
	jne	.LBB3_6
.LBB3_7:                                # %._crit_edge56
	movq	%r12, %rdi
	callq	gpmatrix_free
	movq	%r15, %rdi
	imulq	%rbx, %rdi
	shlq	$2, %rdi
	callq	malloc
	testq	%rax, %rax
	je	.LBB3_17
# BB#8:                                 # %.preheader
	testq	%rbx, %rbx
	jle	.LBB3_16
# BB#9:                                 # %.lr.ph.preheader
	leaq	-1(%rbx), %r8
	movq	%rbx, %rsi
	andq	$3, %rsi
	je	.LBB3_10
# BB#11:                                # %.lr.ph.prol.preheader
	leaq	8(%r14), %rdi
	leaq	(,%r15,4), %rbp
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, (%rdi)
	incq	%rcx
	addq	$16, %rdi
	addq	%rbp, %rdx
	cmpq	%rcx, %rsi
	jne	.LBB3_12
	jmp	.LBB3_13
.LBB3_10:
	xorl	%ecx, %ecx
.LBB3_13:                               # %.lr.ph.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_16
# BB#14:                                # %.lr.ph.preheader.new
	subq	%rcx, %rbx
	movq	%rcx, %rdx
	shlq	$4, %rdx
	leaq	56(%r14,%rdx), %rdx
	leaq	3(%rcx), %r8
	imulq	%r15, %r8
	shlq	$2, %r8
	movq	%r15, %r9
	shlq	$4, %r9
	leaq	2(%rcx), %rbp
	imulq	%r15, %rbp
	shlq	$2, %rbp
	leaq	1(%rcx), %rsi
	imulq	%r15, %rsi
	shlq	$2, %rsi
	imulq	%r15, %rcx
	shlq	$2, %rcx
	.p2align	4, 0x90
.LBB3_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rax,%rcx), %rdi
	movq	%rdi, -48(%rdx)
	leaq	(%rax,%rsi), %rdi
	movq	%rdi, -32(%rdx)
	leaq	(%rax,%rbp), %rdi
	movq	%rdi, -16(%rdx)
	leaq	(%rax,%r8), %rdi
	movq	%rdi, (%rdx)
	addq	$64, %rdx
	addq	%r9, %rax
	addq	$-4, %rbx
	jne	.LBB3_15
.LBB3_16:                               # %._crit_edge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_17:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	generate_energy_grid, .Lfunc_end3-generate_energy_grid
	.cfi_endproc

	.globl	set_grid_ptrs
	.p2align	4, 0x90
	.type	set_grid_ptrs,@function
set_grid_ptrs:                          # @set_grid_ptrs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 80
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	$.Lstr.3, %edi
	callq	puts
	movq	%r14, %rax
	imulq	%r15, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB4_6
# BB#1:                                 # %.lr.ph32
	testq	%r15, %r15
	jle	.LBB4_6
# BB#2:                                 # %.lr.ph32.split.us.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph32.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	movq	%rbp, %rax
	shlq	$4, %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movsd	(%rcx,%rax), %xmm0      # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	leaq	8(%rcx,%rax), %r13
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r12,8), %rdi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movl	%r14d, %esi
	callq	binary_search
	movq	(%r13), %rcx
	movl	%eax, (%rcx,%r12,4)
	incq	%r12
	cmpq	%r12, %r15
	jne	.LBB4_4
# BB#5:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB4_3 Depth=1
	incq	%rbp
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jne	.LBB4_3
.LBB4_6:                                # %._crit_edge33
	movl	$10, %edi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	putchar                 # TAILCALL
.Lfunc_end4:
	.size	set_grid_ptrs, .Lfunc_end4-set_grid_ptrs
	.cfi_endproc

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"ERROR - Out Of Memory!\n"
	.size	.L.str.3, 24

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Generating Unionized Energy Grid..."
	.size	.Lstr, 36

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Copying and Sorting all nuclide grids..."
	.size	.Lstr.1, 41

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Assigning energies to unionized grid..."
	.size	.Lstr.2, 40

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"Assigning pointers to Unionized Energy Grid..."
	.size	.Lstr.3, 47


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
