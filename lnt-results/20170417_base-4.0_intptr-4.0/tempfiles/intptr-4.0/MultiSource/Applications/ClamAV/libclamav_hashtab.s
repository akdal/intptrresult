	.text
	.file	"libclamav_hashtab.bc"
	.globl	hashtab_init
	.p2align	4, 0x90
	.type	hashtab_init,@function
hashtab_init:                           # @hashtab_init
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movq	%rsi, %rdi
	callq	get_nearest_capacity
	movq	%rax, %r14
	movl	$16, %esi
	movq	%r14, %rdi
	callq	cli_calloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB0_3
# BB#4:
	movq	%r14, 8(%rbx)
	movq	$0, 16(%rbx)
	shlq	$3, %r14
	movabsq	$-3689348814741910323, %rcx # imm = 0xCCCCCCCCCCCCCCCD
	movq	%r14, %rax
	mulq	%rcx
	shrq	$3, %rdx
	movq	%rdx, 24(%rbx)
	xorl	%eax, %eax
	jmp	.LBB0_5
.LBB0_1:
	movl	$-111, %eax
	jmp	.LBB0_5
.LBB0_3:
	movl	$-114, %eax
.LBB0_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	hashtab_init, .Lfunc_end0-hashtab_init
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_nearest_capacity,@function
get_nearest_capacity:                   # @get_nearest_capacity
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	cmpq	$53, %rdi
	jae	.LBB1_2
# BB#1:
	movl	$53, %ebx
	jmp	.LBB1_4
.LBB1_2:
	cmpq	$96, %rdi
	ja	.LBB1_5
# BB#3:
	movl	$97, %ebx
	jmp	.LBB1_4
.LBB1_5:
	cmpq	$193, %rdi
	jae	.LBB1_7
# BB#6:
	movl	$193, %ebx
	jmp	.LBB1_4
.LBB1_7:
	cmpq	$389, %rdi              # imm = 0x185
	jae	.LBB1_9
# BB#8:
	movl	$389, %ebx              # imm = 0x185
	jmp	.LBB1_4
.LBB1_9:
	cmpq	$769, %rdi              # imm = 0x301
	jae	.LBB1_11
# BB#10:
	movl	$769, %ebx              # imm = 0x301
	jmp	.LBB1_4
.LBB1_11:
	cmpq	$1543, %rdi             # imm = 0x607
	jae	.LBB1_13
# BB#12:
	movl	$1543, %ebx             # imm = 0x607
	jmp	.LBB1_4
.LBB1_13:
	cmpq	$3079, %rdi             # imm = 0xC07
	jae	.LBB1_15
# BB#14:
	movl	$3079, %ebx             # imm = 0xC07
	jmp	.LBB1_4
.LBB1_15:
	cmpq	$6151, %rdi             # imm = 0x1807
	jae	.LBB1_17
# BB#16:
	movl	$6151, %ebx             # imm = 0x1807
	jmp	.LBB1_4
.LBB1_17:
	cmpq	$12289, %rdi            # imm = 0x3001
	jae	.LBB1_19
# BB#18:
	movl	$12289, %ebx            # imm = 0x3001
	jmp	.LBB1_4
.LBB1_19:
	cmpq	$24593, %rdi            # imm = 0x6011
	jae	.LBB1_21
# BB#20:
	movl	$24593, %ebx            # imm = 0x6011
	jmp	.LBB1_4
.LBB1_21:
	cmpq	$49157, %rdi            # imm = 0xC005
	jae	.LBB1_23
# BB#22:
	movl	$49157, %ebx            # imm = 0xC005
	jmp	.LBB1_4
.LBB1_23:
	cmpq	$98317, %rdi            # imm = 0x1800D
	jae	.LBB1_25
# BB#24:
	movl	$98317, %ebx            # imm = 0x1800D
	jmp	.LBB1_4
.LBB1_25:
	cmpq	$196613, %rdi           # imm = 0x30005
	jae	.LBB1_27
# BB#26:
	movl	$196613, %ebx           # imm = 0x30005
	jmp	.LBB1_4
.LBB1_27:
	cmpq	$393241, %rdi           # imm = 0x60019
	jae	.LBB1_29
# BB#28:
	movl	$393241, %ebx           # imm = 0x60019
	jmp	.LBB1_4
.LBB1_29:
	cmpq	$786433, %rdi           # imm = 0xC0001
	jae	.LBB1_31
# BB#30:
	movl	$786433, %ebx           # imm = 0xC0001
	jmp	.LBB1_4
.LBB1_31:
	cmpq	$1572869, %rdi          # imm = 0x180005
	jae	.LBB1_33
# BB#32:
	movl	$1572869, %ebx          # imm = 0x180005
	jmp	.LBB1_4
.LBB1_33:
	cmpq	$3145739, %rdi          # imm = 0x30000B
	jae	.LBB1_35
# BB#34:
	movl	$3145739, %ebx          # imm = 0x30000B
	jmp	.LBB1_4
.LBB1_35:
	cmpq	$6291469, %rdi          # imm = 0x60000D
	jae	.LBB1_37
# BB#36:
	movl	$6291469, %ebx          # imm = 0x60000D
	jmp	.LBB1_4
.LBB1_37:
	cmpq	$12582917, %rdi         # imm = 0xC00005
	jae	.LBB1_39
# BB#38:
	movl	$12582917, %ebx         # imm = 0xC00005
	jmp	.LBB1_4
.LBB1_39:
	cmpq	$25165843, %rdi         # imm = 0x1800013
	jae	.LBB1_41
# BB#40:
	movl	$25165843, %ebx         # imm = 0x1800013
	jmp	.LBB1_4
.LBB1_41:
	cmpq	$50331653, %rdi         # imm = 0x3000005
	jae	.LBB1_43
# BB#42:
	movl	$50331653, %ebx         # imm = 0x3000005
	jmp	.LBB1_4
.LBB1_43:
	cmpq	$100663319, %rdi        # imm = 0x6000017
	jae	.LBB1_45
# BB#44:
	movl	$100663319, %ebx        # imm = 0x6000017
	jmp	.LBB1_4
.LBB1_45:
	cmpq	$201326611, %rdi        # imm = 0xC000013
	jae	.LBB1_47
# BB#46:
	movl	$201326611, %ebx        # imm = 0xC000013
	jmp	.LBB1_4
.LBB1_47:
	cmpq	$402653189, %rdi        # imm = 0x18000005
	jae	.LBB1_49
# BB#48:
	movl	$402653189, %ebx        # imm = 0x18000005
	jmp	.LBB1_4
.LBB1_49:
	cmpq	$805306457, %rdi        # imm = 0x30000059
	jae	.LBB1_51
# BB#50:
	movl	$805306457, %ebx        # imm = 0x30000059
	jmp	.LBB1_4
.LBB1_51:
	cmpq	$1610612741, %rdi       # imm = 0x60000005
	jae	.LBB1_53
# BB#52:
	movl	$1610612741, %ebx       # imm = 0x60000005
	jmp	.LBB1_4
.LBB1_53:
	movl	$3221225473, %ebx       # imm = 0xC0000001
	cmpq	%rbx, %rdi
	jb	.LBB1_4
# BB#54:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB1_4:                                # %.loopexit
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	get_nearest_capacity, .Lfunc_end1-get_nearest_capacity
	.cfi_endproc

	.globl	hashtab_find
	.p2align	4, 0x90
	.type	hashtab_find,@function
hashtab_find:                           # @hashtab_find
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 64
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r15
	testq	%rdi, %rdi
	je	.LBB2_14
# BB#1:
	movq	8(%rdi), %r13
	testq	%rbp, %rbp
	je	.LBB2_2
# BB#3:                                 # %.lr.ph.i.preheader
	testb	$1, %bpl
	jne	.LBB2_5
# BB#4:
	xorl	%r14d, %r14d
	movq	%rbp, %rcx
	cmpq	$1, %rbp
	jne	.LBB2_7
	jmp	.LBB2_8
.LBB2_2:
	xorl	%r14d, %r14d
	jmp	.LBB2_8
.LBB2_5:                                # %.lr.ph.i.prol
	leaq	-1(%rbp), %rcx
	movzbl	-1(%r15,%rbp), %eax
	xorl	%edx, %edx
	divq	%r13
	movq	%rdx, %r14
	cmpq	$1, %rbp
	je	.LBB2_8
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	shlq	$8, %r14
	movzbl	-1(%r15,%rcx), %eax
	orq	%r14, %rax
	xorl	%edx, %edx
	divq	%r13
	shlq	$8, %rdx
	movzbl	-2(%r15,%rcx), %eax
	orq	%rdx, %rax
	xorl	%edx, %edx
	divq	%r13
	addq	$-2, %rcx
	movq	%rdx, %r14
	jne	.LBB2_7
.LBB2_8:                                # %hash.exit
	movq	(%rdi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rbx
	shlq	$4, %rbx
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax,%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_14
# BB#10:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$DELETED_KEY, %eax
	cmpq	%rax, %rsi
	je	.LBB2_13
# BB#11:                                #   in Loop: Header=BB2_9 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_12
.LBB2_13:                               #   in Loop: Header=BB2_9 Depth=1
	addq	%r12, %r14
	incq	%r12
	xorl	%edx, %edx
	movq	%r14, %rax
	divq	%r13
	cmpq	%r13, %r12
	movq	%rdx, %r14
	jbe	.LBB2_9
.LBB2_14:
	xorl	%eax, %eax
.LBB2_15:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_12:
	movq	(%rsp), %rax            # 8-byte Reload
	addq	%rbx, %rax
	jmp	.LBB2_15
.Lfunc_end2:
	.size	hashtab_find, .Lfunc_end2-hashtab_find
	.cfi_endproc

	.globl	hashtab_insert
	.p2align	4, 0x90
	.type	hashtab_insert,@function
hashtab_insert:                         # @hashtab_insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 112
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rdx, %r8
	movq	%rsi, %r13
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB3_1
# BB#2:                                 # %.preheader
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testq	%r8, %r8
	je	.LBB3_6
# BB#3:                                 # %.preheader.split.preheader
	movl	%r8d, %ecx
	andl	$1, %ecx
	leaq	-1(%r8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$1, %r15d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
.LBB3_4:                                # %.preheader.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_14 Depth 2
                                        #     Child Loop BB3_16 Depth 2
	testq	%rcx, %rcx
	movq	8(%rbp), %rbx
	jne	.LBB3_12
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	movq	%r8, %rcx
	xorl	%ebp, %ebp
	cmpq	$1, %r8
	jne	.LBB3_14
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph.i.prol
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movzbl	(%r13,%rcx), %eax
	xorl	%edx, %edx
	divq	%rbx
	movq	%rdx, %rbp
	cmpq	$1, %r8
	je	.LBB3_15
	.p2align	4, 0x90
.LBB3_14:                               # %.lr.ph.i
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shlq	$8, %rbp
	movzbl	-1(%r13,%rcx), %eax
	orq	%rbp, %rax
	xorl	%edx, %edx
	divq	%rbx
	shlq	$8, %rdx
	movzbl	-2(%r13,%rcx), %eax
	orq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rbx
	addq	$-2, %rcx
	movq	%rdx, %rbp
	jne	.LBB3_14
.LBB3_15:                               # %hash.exit
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movq	%rbp, %r14
	shlq	$4, %r14
	movq	%rax, 48(%rsp)          # 8-byte Spill
	addq	%rax, %r14
	.p2align	4, 0x90
.LBB3_16:                               #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB3_17
# BB#21:                                #   in Loop: Header=BB3_16 Depth=2
	movl	$DELETED_KEY, %eax
	cmpq	%rax, %rsi
	je	.LBB3_22
# BB#23:                                #   in Loop: Header=BB3_16 Depth=2
	movq	%r13, %rdi
	movq	%r8, %r12
	movq	%r8, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_24
# BB#25:                                #   in Loop: Header=BB3_16 Depth=2
	addq	%r15, %rbp
	incq	%r15
	xorl	%edx, %edx
	movq	%rbp, %rax
	divq	%rbx
	movq	%rdx, %rbp
	movq	%rbp, %r14
	shlq	$4, %r14
	addq	48(%rsp), %r14          # 8-byte Folded Reload
	movq	%r12, %r8
	cmpq	%rbx, %r15
	jbe	.LBB3_16
	jmp	.LBB3_27
	.p2align	4, 0x90
.LBB3_22:                               #   in Loop: Header=BB3_16 Depth=2
	movq	%r14, 24(%rsp)          # 8-byte Spill
	cmpq	%rbx, %r15
	jbe	.LBB3_16
.LBB3_27:                               #   in Loop: Header=BB3_4 Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r8, %rbx
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	hashtab_grow
	movq	%rbx, %r8
	testl	%eax, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	jns	.LBB3_4
	jmp	.LBB3_28
.LBB3_1:
	movl	$-111, %ebx
	jmp	.LBB3_30
.LBB3_17:
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB3_18
.LBB3_6:                                # %.preheader.split.us.preheader
	movl	$DELETED_KEY, %ebx
	xorl	%r14d, %r14d
.LBB3_7:                                # %.preheader.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_8 Depth 2
	movq	%r14, %rcx
	movq	(%rbp), %r14
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB3_8:                                #   Parent Loop BB3_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB3_18
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=2
	cmpq	%rbx, %rax
	jne	.LBB3_24
# BB#10:                                #   in Loop: Header=BB3_8 Depth=2
	movq	8(%rbp), %rdx
	testq	%rdx, %rdx
	movq	%r14, %rcx
	jne	.LBB3_8
# BB#11:                                #   in Loop: Header=BB3_7 Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%r8, %r15
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	hashtab_grow
	movq	%r15, %r8
	testl	%eax, %eax
	jns	.LBB3_7
.LBB3_28:                               # %.us-lcssa69.us
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB3_29
.LBB3_18:                               # %.us-lcssa.us
	testq	%rcx, %rcx
	cmovneq	%rcx, %r14
	incq	%r8
	movq	%r8, %rdi
	movq	%r8, %r15
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_29
# BB#19:
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	strncpy
	movq	%rbx, (%r14)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%r14)
	movq	16(%rbp), %rax
	incq	%rax
	movq	%rax, 16(%rbp)
	xorl	%ebx, %ebx
	cmpq	24(%rbp), %rax
	jbe	.LBB3_30
# BB#20:
	movq	8(%rbp), %rdx
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	hashtab_grow
	jmp	.LBB3_30
.LBB3_24:                               # %.us-lcssa68.us
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%r14)
	xorl	%ebx, %ebx
	jmp	.LBB3_30
.LBB3_29:
	movl	$-114, %ebx
.LBB3_30:
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	hashtab_insert, .Lfunc_end3-hashtab_insert
	.cfi_endproc

	.p2align	4, 0x90
	.type	hashtab_grow,@function
hashtab_grow:                           # @hashtab_grow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 80
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	8(%rbp), %rdi
	callq	get_nearest_capacity
	movq	%rax, %rbx
	movl	$16, %esi
	movq	%rbx, %rdi
	callq	cli_calloc
	movq	%rax, %r12
	movl	$-114, %eax
	testq	%r12, %r12
	je	.LBB4_35
# BB#1:
	cmpq	8(%rbp), %rbx
	je	.LBB4_35
# BB#2:
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%rbp), %r13
	movq	8(%rbp), %rax
	testq	%rax, %rax
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB4_3
# BB#4:                                 # %.lr.ph79
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB4_10
# BB#5:                                 # %.lr.ph79.split.preheader
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph79.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_26 Depth 2
                                        #     Child Loop BB4_29 Depth 2
	movq	%r15, %r14
	shlq	$4, %r14
	movq	(%r13,%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB4_33
# BB#7:                                 # %.lr.ph79.split
                                        #   in Loop: Header=BB4_6 Depth=1
	movl	$DELETED_KEY, %ecx
	cmpq	%rcx, %rbp
	je	.LBB4_33
# BB#8:                                 #   in Loop: Header=BB4_6 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB4_9
# BB#22:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_6 Depth=1
	testb	$1, %cl
	jne	.LBB4_24
# BB#23:                                #   in Loop: Header=BB4_6 Depth=1
	movq	%rcx, %rsi
	xorl	%edx, %edx
	cmpq	$1, %rcx
	jne	.LBB4_26
	jmp	.LBB4_27
.LBB4_9:                                #   in Loop: Header=BB4_6 Depth=1
	xorl	%edx, %edx
	jmp	.LBB4_27
.LBB4_24:                               # %.lr.ph.i.prol
                                        #   in Loop: Header=BB4_6 Depth=1
	leaq	-1(%rcx), %rsi
	movzbl	-1(%rbp,%rcx), %eax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	$1, %rcx
	je	.LBB4_27
	.p2align	4, 0x90
.LBB4_26:                               # %.lr.ph.i
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shlq	$8, %rdx
	movzbl	-1(%rbp,%rsi), %eax
	orq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rbx
	shlq	$8, %rdx
	movzbl	-2(%rbp,%rsi), %eax
	orq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rbx
	addq	$-2, %rsi
	jne	.LBB4_26
.LBB4_27:                               # %hash.exit.preheader
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	%rdx, %rax
	shlq	$4, %rax
	cmpq	$0, (%r12,%rax)
	je	.LBB4_32
# BB#28:                                # %hash.exit.preheader106
                                        #   in Loop: Header=BB4_6 Depth=1
	movl	$2, %ecx
	.p2align	4, 0x90
.LBB4_29:                               # %hash.exit
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx,%rcx), %rax
	xorl	%edx, %edx
	divq	%rbx
	movq	%rdx, %rax
	shlq	$4, %rax
	movq	(%r12,%rax), %rsi
	cmpq	%rbx, %rcx
	ja	.LBB4_31
# BB#30:                                # %hash.exit
                                        #   in Loop: Header=BB4_29 Depth=2
	incq	%rcx
	testq	%rsi, %rsi
	jne	.LBB4_29
.LBB4_31:                               # %.critedge
                                        #   in Loop: Header=BB4_6 Depth=1
	testq	%rsi, %rsi
	jne	.LBB4_36
.LBB4_32:                               #   in Loop: Header=BB4_6 Depth=1
	addq	%r12, %rax
	movups	(%r13,%r14), %xmm0
	movups	%xmm0, (%rax)
	incq	8(%rsp)                 # 8-byte Folded Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	movq	8(%rax), %rax
.LBB4_33:                               #   in Loop: Header=BB4_6 Depth=1
	incq	%r15
	cmpq	%rax, %r15
	jb	.LBB4_6
	jmp	.LBB4_34
.LBB4_3:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB4_34
.LBB4_10:                               # %.lr.ph79.split.us.preheader
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$DELETED_KEY, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph79.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_16 Depth 2
                                        #     Child Loop BB4_18 Depth 2
	movq	%rbp, %r15
	shlq	$4, %r15
	movq	(%r13,%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_21
# BB#12:                                # %.lr.ph79.split.us
                                        #   in Loop: Header=BB4_11 Depth=1
	cmpq	%r14, %rdi
	je	.LBB4_21
# BB#13:                                #   in Loop: Header=BB4_11 Depth=1
	callq	strlen
	testq	%rax, %rax
	je	.LBB4_19
# BB#14:                                # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB4_11 Depth=1
	leaq	-1(%rax), %rcx
	movq	%rax, %rdx
	andq	$7, %rdx
	je	.LBB4_17
# BB#15:                                # %.lr.ph.i.us.prol.preheader
                                        #   in Loop: Header=BB4_11 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB4_16:                               # %.lr.ph.i.us.prol
                                        #   Parent Loop BB4_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decq	%rax
	incq	%rdx
	jne	.LBB4_16
.LBB4_17:                               # %.lr.ph.i.us.prol.loopexit
                                        #   in Loop: Header=BB4_11 Depth=1
	cmpq	$7, %rcx
	jb	.LBB4_19
	.p2align	4, 0x90
.LBB4_18:                               # %.lr.ph.i.us
                                        #   Parent Loop BB4_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-8, %rax
	jne	.LBB4_18
.LBB4_19:                               # %hash.exit.preheader.us
                                        #   in Loop: Header=BB4_11 Depth=1
	cmpq	$0, (%r12)
	jne	.LBB4_36
# BB#20:                                # %.thread.us
                                        #   in Loop: Header=BB4_11 Depth=1
	movups	(%r13,%r15), %xmm0
	movups	%xmm0, (%r12)
	incq	8(%rsp)                 # 8-byte Folded Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	movq	8(%rax), %rax
.LBB4_21:                               #   in Loop: Header=BB4_11 Depth=1
	incq	%rbp
	cmpq	%rax, %rbp
	jb	.LBB4_11
.LBB4_34:                               # %._crit_edge
	movq	%r13, %rdi
	callq	free
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, (%rsi)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 16(%rsi)
	movq	%rbx, 8(%rsi)
	movq	%rbx, %rax
	shlq	$3, %rax
	movabsq	$-3689348814741910323, %rcx # imm = 0xCCCCCCCCCCCCCCCD
	mulq	%rcx
	shrq	$3, %rdx
	movq	%rdx, 24(%rsi)
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	cli_dbgmsg
	xorl	%eax, %eax
.LBB4_35:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_36:                               # %.us-lcssa.us
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %eax
	jmp	.LBB4_35
.Lfunc_end4:
	.size	hashtab_grow, .Lfunc_end4-hashtab_grow
	.cfi_endproc

	.globl	hashtab_delete
	.p2align	4, 0x90
	.type	hashtab_delete,@function
hashtab_delete:                         # @hashtab_delete
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 80
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movq	%rdi, (%rsp)            # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB5_15
# BB#1:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax), %rdi
	testq	%r12, %r12
	je	.LBB5_2
# BB#3:                                 # %.lr.ph.i.i.preheader
	testb	$1, %r12b
	jne	.LBB5_5
# BB#4:
	xorl	%r14d, %r14d
	movq	%r12, %rcx
	cmpq	$1, %r12
	jne	.LBB5_7
	jmp	.LBB5_8
.LBB5_2:
	xorl	%r14d, %r14d
	jmp	.LBB5_8
.LBB5_5:                                # %.lr.ph.i.i.prol
	leaq	-1(%r12), %rcx
	movzbl	-1(%rbp,%r12), %eax
	xorl	%edx, %edx
	divq	%rdi
	movq	%rdx, %r14
	cmpq	$1, %r12
	je	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	shlq	$8, %r14
	movzbl	-1(%rbp,%rcx), %eax
	orq	%r14, %rax
	xorl	%edx, %edx
	divq	%rdi
	shlq	$8, %rdx
	movzbl	-2(%rbp,%rcx), %eax
	orq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rdi
	addq	$-2, %rcx
	movq	%rdx, %r14
	jne	.LBB5_7
.LBB5_8:                                # %hash.exit.i
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %r15d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_9:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rbx
	shlq	$4, %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbx), %r13
	testq	%r13, %r13
	je	.LBB5_15
# BB#10:                                #   in Loop: Header=BB5_9 Depth=1
	movl	$DELETED_KEY, %eax
	cmpq	%rax, %r13
	je	.LBB5_12
# BB#11:                                #   in Loop: Header=BB5_9 Depth=1
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	callq	strncmp
	movq	16(%rsp), %rdi          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB5_13
.LBB5_12:                               #   in Loop: Header=BB5_9 Depth=1
	addq	%r15, %r14
	incq	%r15
	xorl	%edx, %edx
	movq	%r14, %rax
	divq	%rdi
	cmpq	%rdi, %r15
	movq	%rdx, %r14
	jbe	.LBB5_9
	jmp	.LBB5_15
.LBB5_13:                               # %hashtab_find.exit
	addq	%rbx, 8(%rsp)           # 8-byte Folded Spill
	je	.LBB5_15
# BB#14:
	movq	%r13, %rdi
	callq	free
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$DELETED_KEY, (%rax)
	movq	(%rsp), %rax            # 8-byte Reload
	decq	16(%rax)
.LBB5_15:                               # %hashtab_find.exit.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	hashtab_delete, .Lfunc_end5-hashtab_delete
	.cfi_endproc

	.globl	hashtab_clear
	.p2align	4, 0x90
	.type	hashtab_clear,@function
hashtab_clear:                          # @hashtab_clear
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 48
.Lcfi64:
	.cfi_offset %rbx, -40
.Lcfi65:
	.cfi_offset %r12, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	movq	8(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB6_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	movl	$DELETED_KEY, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%r12), %rax
	testq	%rax, %rax
	je	.LBB6_6
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_3 Depth=1
	cmpq	%r15, %rax
	je	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	%rax, %rdi
	callq	free
	movq	(%r14), %rdi
	movq	8(%r14), %rdx
.LBB6_6:                                #   in Loop: Header=BB6_3 Depth=1
	incq	%rbx
	addq	$16, %r12
	cmpq	%rdx, %rbx
	jb	.LBB6_3
	jmp	.LBB6_7
.LBB6_1:
	xorl	%edx, %edx
.LBB6_7:                                # %._crit_edge
	xorl	%esi, %esi
	callq	memset
	movq	$0, 16(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	hashtab_clear, .Lfunc_end6-hashtab_clear
	.cfi_endproc

	.globl	hashtab_store
	.p2align	4, 0x90
	.type	hashtab_store,@function
hashtab_store:                          # @hashtab_store
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 48
.Lcfi73:
	.cfi_offset %rbx, -48
.Lcfi74:
	.cfi_offset %r12, -40
.Lcfi75:
	.cfi_offset %r13, -32
.Lcfi76:
	.cfi_offset %r14, -24
.Lcfi77:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	8(%r15), %rax
	testq	%rax, %rax
	je	.LBB7_6
# BB#1:                                 # %.lr.ph
	movl	$8, %r13d
	movl	$DELETED_KEY, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rdx
	movq	-8(%rdx,%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB7_5
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpq	%r12, %rcx
	je	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	(%rdx,%r13), %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	8(%r15), %rax
.LBB7_5:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%rbx
	addq	$16, %r13
	cmpq	%rax, %rbx
	jb	.LBB7_2
.LBB7_6:                                # %._crit_edge
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	hashtab_store, .Lfunc_end7-hashtab_store
	.cfi_endproc

	.globl	hashtab_generate_c
	.p2align	4, 0x90
	.type	hashtab_generate_c,@function
hashtab_generate_c:                     # @hashtab_generate_c
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 48
.Lcfi83:
	.cfi_offset %rbx, -48
.Lcfi84:
	.cfi_offset %r12, -40
.Lcfi85:
	.cfi_offset %r13, -32
.Lcfi86:
	.cfi_offset %r14, -24
.Lcfi87:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	cmpq	$0, 8(%r15)
	je	.LBB8_9
# BB#1:                                 # %.lr.ph
	movl	$8, %r13d
	movl	$DELETED_KEY, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	-8(%rax,%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB8_5
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpq	%r12, %rsi
	je	.LBB8_6
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	(%rax,%r13), %rdx
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB8_8
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	movl	$.Lstr.4, %edi
	jmp	.LBB8_7
	.p2align	4, 0x90
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=1
	movl	$.Lstr.5, %edi
.LBB8_7:                                #   in Loop: Header=BB8_2 Depth=1
	callq	puts
.LBB8_8:                                #   in Loop: Header=BB8_2 Depth=1
	incq	%rbx
	addq	$16, %r13
	cmpq	8(%r15), %rbx
	jb	.LBB8_2
.LBB8_9:                                # %._crit_edge
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movq	8(%r15), %rdx
	movq	16(%r15), %rcx
	movq	24(%r15), %r8
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movl	$.Lstr.3, %edi
	callq	puts
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	hashtab_generate_c, .Lfunc_end8-hashtab_generate_c
	.cfi_endproc

	.globl	hashtab_load
	.p2align	4, 0x90
	.type	hashtab_load,@function
hashtab_load:                           # @hashtab_load
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 48
	subq	$2064, %rsp             # imm = 0x810
.Lcfi93:
	.cfi_def_cfa_offset 2112
.Lcfi94:
	.cfi_offset %rbx, -48
.Lcfi95:
	.cfi_offset %r12, -40
.Lcfi96:
	.cfi_offset %r13, -32
.Lcfi97:
	.cfi_offset %r14, -24
.Lcfi98:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	16(%rsp), %rdi
	movl	$1024, %esi             # imm = 0x400
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB9_3
# BB#1:                                 # %.lr.ph
	leaq	16(%rsp), %r13
	leaq	12(%rsp), %r12
	leaq	1040(%rsp), %rbx
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	sscanf
	movq	%rbx, %rdi
	callq	strlen
	movslq	12(%rsp), %rcx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	hashtab_insert
	movl	$1024, %esi             # imm = 0x400
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB9_2
.LBB9_3:                                # %._crit_edge
	xorl	%eax, %eax
	addq	$2064, %rsp             # imm = 0x810
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	hashtab_load, .Lfunc_end9-hashtab_load
	.cfi_endproc

	.type	DELETED_KEY,@object     # @DELETED_KEY
	.local	DELETED_KEY
	.comm	DELETED_KEY,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"hashtab.c:Growing hashtable %p, because it has exceeded maxfill, old size:%ld\n"
	.size	.L.str, 79

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"hashtab.c: Growing hashtable %p, because its full, old size:%ld.\n"
	.size	.L.str.1, 66

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"hashtab.c: Unable to grow hashtable\n"
	.size	.L.str.2, 37

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%ld %s\n"
	.size	.L.str.3, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"static struct element %s_elements[] = {\n"
	.size	.L.str.6, 41

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\t{(const unsigned char*)\"%s\", %ld},\n"
	.size	.L.str.9, 37

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"const struct hashtable %s = {\n"
	.size	.L.str.11, 31

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\t%s_elements, %ld, %ld, %ld"
	.size	.L.str.12, 28

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%d %1023s"
	.size	.L.str.14, 10

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Requested hashtable size is too big!"
	.size	.L.str.15, 37

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"hashtab.c: Warning: growing open-addressing hashtables is slow. Either allocate more storage when initializing, or use other hashtable types!\n"
	.size	.L.str.16, 143

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"hashtab.c: Impossible - unable to rehash table"
	.size	.L.str.17, 47

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Table %p size after grow:%ld\n"
	.size	.L.str.18, 30

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"/* TODO: include GPL headers */"
	.size	.Lstr, 32

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"#include <hashtab.h>"
	.size	.Lstr.1, 21

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.2:
	.asciz	"};"
	.size	.Lstr.2, 3

	.type	.Lstr.3,@object         # @str.3
.Lstr.3:
	.asciz	"\n};"
	.size	.Lstr.3, 4

	.type	.Lstr.4,@object         # @str.4
.Lstr.4:
	.asciz	"\t{NULL, 0},"
	.size	.Lstr.4, 12

	.type	.Lstr.5,@object         # @str.5
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.5:
	.asciz	"\t{DELETED_KEY,0},"
	.size	.Lstr.5, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
