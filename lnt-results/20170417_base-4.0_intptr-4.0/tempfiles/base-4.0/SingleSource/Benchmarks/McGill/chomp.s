	.text
	.file	"chomp.bc"
	.globl	copy_data
	.p2align	4, 0x90
	.type	copy_data,@function
copy_data:                              # @copy_data
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	ncol(%rip), %rbx
	leaq	(,%rbx,4), %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:                                 # %.lr.ph.preheader
	addq	$-4, %r12
	leal	-1(%rbx), %eax
	leaq	(,%rax,4), %rcx
	subq	%rcx, %r12
	movq	%r15, %rdi
	addq	%r12, %rdi
	decq	%rbx
	subq	%rax, %rbx
	leaq	(%r14,%rbx,4), %rsi
	leaq	4(,%rax,4), %rdx
	callq	memcpy
.LBB0_2:                                # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	copy_data, .Lfunc_end0-copy_data
	.cfi_endproc

	.globl	next_data
	.p2align	4, 0x90
	.type	next_data,@function
next_data:                              # @next_data
	.cfi_startproc
# BB#0:
	cmpl	$0, ncol(%rip)
	je	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax,4), %ecx
	cmpl	nrow(%rip), %ecx
	jne	.LBB1_6
# BB#4:                                 # %.outer
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	$0, (%rdi,%rax,4)
	incq	%rax
	cmpl	ncol(%rip), %eax
	jne	.LBB1_3
# BB#5:
	xorl	%eax, %eax
	retq
.LBB1_1:
	xorl	%eax, %eax
	retq
.LBB1_6:                                # %.critedge.loopexit17
	incl	%ecx
	movl	%ecx, (%rdi,%rax,4)
	movl	$1, %eax
	retq
.Lfunc_end1:
	.size	next_data, .Lfunc_end1-next_data
	.cfi_endproc

	.globl	melt_data
	.p2align	4, 0x90
	.type	melt_data,@function
melt_data:                              # @melt_data
	.cfi_startproc
# BB#0:
	movslq	ncol(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_37
# BB#1:                                 # %.lr.ph.preheader
	leal	-1(%rax), %r9d
	incq	%r9
	cmpq	$8, %r9
	jae	.LBB2_3
# BB#2:
	movq	%rax, %r10
	jmp	.LBB2_27
.LBB2_3:                                # %min.iters.checked
	movabsq	$8589934584, %r8        # imm = 0x1FFFFFFF8
	andq	%r9, %r8
	je	.LBB2_7
# BB#4:                                 # %vector.memcheck
	leaq	-1(%rax), %rcx
	leal	-1(%rax), %edx
	subq	%rdx, %rcx
	leaq	(%rdi,%rcx,4), %r10
	leaq	(%rsi,%rax,4), %rdx
	cmpq	%rdx, %r10
	jae	.LBB2_8
# BB#5:                                 # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	leaq	(%rsi,%rcx,4), %rcx
	cmpq	%rdx, %rcx
	jae	.LBB2_8
.LBB2_7:
	movq	%rax, %r10
	jmp	.LBB2_27
.LBB2_8:                                # %vector.body.preheader
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	movq	%rax, %r10
	subq	%r8, %r10
	leaq	-32(%rdi,%rax,4), %rdx
	leaq	-16(%rsi,%rax,4), %rax
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB2_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rdx), %xmm2
	movdqu	16(%rdx), %xmm0
	pshufd	$27, %xmm0, %xmm3       # xmm3 = xmm0[3,2,1,0]
	movdqu	-16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	pshufd	$27, %xmm1, %xmm4       # xmm4 = xmm1[3,2,1,0]
	pcmpgtd	%xmm4, %xmm3
	movdqa	%xmm3, -16(%rsp)
	testb	$1, -16(%rsp)
	je	.LBB2_11
# BB#10:                                # %pred.store.if
                                        #   in Loop: Header=BB2_9 Depth=1
	pshufd	$231, %xmm1, %xmm4      # xmm4 = xmm1[3,1,2,3]
	movd	%xmm4, 28(%rdx)
.LBB2_11:                               # %pred.store.continue
                                        #   in Loop: Header=BB2_9 Depth=1
	movdqa	%xmm3, -32(%rsp)
	testb	$1, -28(%rsp)
	je	.LBB2_13
# BB#12:                                # %pred.store.if23
                                        #   in Loop: Header=BB2_9 Depth=1
	pshufd	$78, %xmm1, %xmm4       # xmm4 = xmm1[2,3,0,1]
	movd	%xmm4, 24(%rdx)
.LBB2_13:                               # %pred.store.continue24
                                        #   in Loop: Header=BB2_9 Depth=1
	movdqa	%xmm3, -48(%rsp)
	testb	$1, -40(%rsp)
	je	.LBB2_15
# BB#14:                                # %pred.store.if25
                                        #   in Loop: Header=BB2_9 Depth=1
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	movd	%xmm4, 20(%rdx)
.LBB2_15:                               # %pred.store.continue26
                                        #   in Loop: Header=BB2_9 Depth=1
	pshufd	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	pshufd	$27, %xmm0, %xmm4       # xmm4 = xmm0[3,2,1,0]
	movdqa	%xmm3, -64(%rsp)
	testb	$1, -52(%rsp)
	je	.LBB2_17
# BB#16:                                # %pred.store.if27
                                        #   in Loop: Header=BB2_9 Depth=1
	movd	%xmm1, 16(%rdx)
.LBB2_17:                               # %pred.store.continue28
                                        #   in Loop: Header=BB2_9 Depth=1
	pcmpgtd	%xmm4, %xmm2
	movdqa	%xmm2, -80(%rsp)
	testb	$1, -80(%rsp)
	je	.LBB2_19
# BB#18:                                # %pred.store.if29
                                        #   in Loop: Header=BB2_9 Depth=1
	pshufd	$231, %xmm0, %xmm1      # xmm1 = xmm0[3,1,2,3]
	movd	%xmm1, 12(%rdx)
.LBB2_19:                               # %pred.store.continue30
                                        #   in Loop: Header=BB2_9 Depth=1
	movdqa	%xmm2, -96(%rsp)
	testb	$1, -92(%rsp)
	je	.LBB2_21
# BB#20:                                # %pred.store.if31
                                        #   in Loop: Header=BB2_9 Depth=1
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movd	%xmm1, 8(%rdx)
.LBB2_21:                               # %pred.store.continue32
                                        #   in Loop: Header=BB2_9 Depth=1
	movdqa	%xmm2, -112(%rsp)
	testb	$1, -104(%rsp)
	je	.LBB2_23
# BB#22:                                # %pred.store.if33
                                        #   in Loop: Header=BB2_9 Depth=1
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	movd	%xmm1, 4(%rdx)
.LBB2_23:                               # %pred.store.continue34
                                        #   in Loop: Header=BB2_9 Depth=1
	movdqa	%xmm2, -128(%rsp)
	testb	$1, -116(%rsp)
	je	.LBB2_25
# BB#24:                                # %pred.store.if35
                                        #   in Loop: Header=BB2_9 Depth=1
	movd	%xmm0, (%rdx)
.LBB2_25:                               # %pred.store.continue36
                                        #   in Loop: Header=BB2_9 Depth=1
	addq	$-32, %rdx
	addq	$-32, %rax
	addq	$-8, %rcx
	jne	.LBB2_9
# BB#26:                                # %middle.block
	cmpq	%r8, %r9
	leaq	8(%rsp), %rsp
	je	.LBB2_37
.LBB2_27:                               # %.lr.ph.preheader37
	leal	-1(%r10), %ecx
	testb	$1, %r10b
	jne	.LBB2_29
# BB#28:
	movq	%r10, %rax
	testl	%ecx, %ecx
	jne	.LBB2_32
	jmp	.LBB2_37
.LBB2_29:                               # %.lr.ph.prol
	leaq	-1(%r10), %rax
	movl	-4(%rsi,%r10,4), %edx
	cmpl	%edx, -4(%rdi,%r10,4)
	jle	.LBB2_31
# BB#30:
	movl	%edx, -4(%rdi,%r10,4)
.LBB2_31:                               # %.lr.ph.prol.loopexit
	testl	%ecx, %ecx
	je	.LBB2_37
	.p2align	4, 0x90
.LBB2_32:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi,%rax,4), %ecx
	cmpl	%ecx, -4(%rdi,%rax,4)
	jle	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_32 Depth=1
	movl	%ecx, -4(%rdi,%rax,4)
.LBB2_34:                               # %.backedge
                                        #   in Loop: Header=BB2_32 Depth=1
	leaq	-2(%rax), %rcx
	movl	-8(%rsi,%rax,4), %edx
	cmpl	%edx, -8(%rdi,%rax,4)
	jle	.LBB2_36
# BB#35:                                #   in Loop: Header=BB2_32 Depth=1
	movl	%edx, -8(%rdi,%rax,4)
.LBB2_36:                               # %.backedge.1
                                        #   in Loop: Header=BB2_32 Depth=1
	testl	%ecx, %ecx
	movq	%rcx, %rax
	jne	.LBB2_32
.LBB2_37:                               # %._crit_edge
	retq
.Lfunc_end2:
	.size	melt_data, .Lfunc_end2-melt_data
	.cfi_endproc

	.globl	equal_data
	.p2align	4, 0x90
	.type	equal_data,@function
equal_data:                             # @equal_data
	.cfi_startproc
# BB#0:
	movslq	ncol(%rip), %rdx
	leal	1(%rdx), %ecx
	leaq	-4(%rdi,%rdx,4), %rax
	leaq	-4(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	$1, %ecx
	je	.LBB3_2
# BB#3:                                 #   in Loop: Header=BB3_1 Depth=1
	movl	(%rax), %esi
	decl	%ecx
	addq	$-4, %rax
	cmpl	(%rdx), %esi
	leaq	-4(%rdx), %rdx
	je	.LBB3_1
	jmp	.LBB3_4
.LBB3_2:
	xorl	%ecx, %ecx
.LBB3_4:                                # %.critedge
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setle	%al
	retq
.Lfunc_end3:
	.size	equal_data, .Lfunc_end3-equal_data
	.cfi_endproc

	.globl	valid_data
	.p2align	4, 0x90
	.type	valid_data,@function
valid_data:                             # @valid_data
	.cfi_startproc
# BB#0:
	movl	ncol(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB4_1
# BB#2:                                 # %.lr.ph
	movl	nrow(%rip), %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movl	%eax, %esi
	movl	(%rdi,%rdx,4), %eax
	cmpl	%esi, %eax
	jg	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	incq	%rdx
	cmpl	%edx, %ecx
	jne	.LBB4_3
	jmp	.LBB4_5
.LBB4_1:
	xorl	%edx, %edx
.LBB4_5:                                # %._crit_edge
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	sete	%al
	retq
.Lfunc_end4:
	.size	valid_data, .Lfunc_end4-valid_data
	.cfi_endproc

	.globl	dump_list
	.p2align	4, 0x90
	.type	dump_list,@function
dump_list:                              # @dump_list
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB5_1
# BB#2:
	movq	8(%rbx), %rdi
	callq	dump_list
	movq	(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB5_1:
	popq	%rbx
	retq
.Lfunc_end5:
	.size	dump_list, .Lfunc_end5-dump_list
	.cfi_endproc

	.globl	dump_play
	.p2align	4, 0x90
	.type	dump_play,@function
dump_play:                              # @dump_play
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB6_1
# BB#2:
	movq	24(%rbx), %rdi
	callq	dump_play
	movq	16(%rbx), %rdi
	callq	dump_list
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB6_1:
	popq	%rbx
	retq
.Lfunc_end6:
	.size	dump_play, .Lfunc_end6-dump_play
	.cfi_endproc

	.globl	get_value
	.p2align	4, 0x90
	.type	get_value,@function
get_value:                              # @get_value
	.cfi_startproc
# BB#0:
	movslq	ncol(%rip), %r10
	leal	1(%r10), %r8d
	leaq	-4(%rdi,%r10,4), %r9
	decq	%r10
	movl	$game_tree, %esi
	shlq	$2, %r10
	jmp	.LBB7_1
	.p2align	4, 0x90
.LBB7_4:                                # %equal_data.exit
                                        #   in Loop: Header=BB7_1 Depth=1
	testl	%ecx, %ecx
	jle	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_1 Depth=1
	addq	$24, %rsi
.LBB7_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
	movq	(%rsi), %rsi
	movq	8(%rsi), %rdi
	addq	%r10, %rdi
	movq	%r9, %rdx
	movl	%r8d, %ecx
	.p2align	4, 0x90
.LBB7_2:                                #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$1, %ecx
	je	.LBB7_6
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=2
	movl	(%rdi), %eax
	decl	%ecx
	addq	$-4, %rdi
	cmpl	(%rdx), %eax
	leaq	-4(%rdx), %rdx
	je	.LBB7_2
	jmp	.LBB7_4
.LBB7_6:                                # %equal_data.exit.thread
	movl	(%rsi), %eax
	retq
.Lfunc_end7:
	.size	get_value, .Lfunc_end7-get_value
	.cfi_endproc

	.globl	show_data
	.p2align	4, 0x90
	.type	show_data,@function
show_data:                              # @show_data
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, ncol(%rip)
	je	.LBB8_5
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rbx,4), %esi
	incq	%rbx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	ncol(%rip), %eax
	cmpl	%eax, %ebx
	je	.LBB8_4
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movl	ncol(%rip), %eax
.LBB8_4:                                # %.backedge
                                        #   in Loop: Header=BB8_2 Depth=1
	cmpl	%eax, %ebx
	jne	.LBB8_2
.LBB8_5:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	show_data, .Lfunc_end8-show_data
	.cfi_endproc

	.globl	show_move
	.p2align	4, 0x90
	.type	show_move,@function
show_move:                              # @show_move
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	cmpl	$0, ncol(%rip)
	je	.LBB9_5
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rbx,4), %esi
	incq	%rbx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	ncol(%rip), %eax
	cmpl	%eax, %ebx
	je	.LBB9_4
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movl	ncol(%rip), %eax
.LBB9_4:                                # %.backedge.i
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpl	%eax, %ebx
	jne	.LBB9_2
.LBB9_5:                                # %show_data.exit
	movl	$.Lstr, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	puts                    # TAILCALL
.Lfunc_end9:
	.size	show_move, .Lfunc_end9-show_move
	.cfi_endproc

	.globl	show_list
	.p2align	4, 0x90
	.type	show_list,@function
show_list:                              # @show_list
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	jne	.LBB10_2
	jmp	.LBB10_8
	.p2align	4, 0x90
.LBB10_7:                               # %show_move.exit
                                        #   in Loop: Header=BB10_2 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	movq	8(%r14), %r14
	testq	%r14, %r14
	je	.LBB10_8
.LBB10_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_4 Depth 2
	movq	(%r14), %r15
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	cmpl	$0, ncol(%rip)
	je	.LBB10_7
# BB#3:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_4:                               # %.lr.ph.i.i
                                        #   Parent Loop BB10_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r15,%rbx,4), %esi
	incq	%rbx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	ncol(%rip), %eax
	cmpl	%eax, %ebx
	je	.LBB10_6
# BB#5:                                 #   in Loop: Header=BB10_4 Depth=2
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movl	ncol(%rip), %eax
.LBB10_6:                               # %.backedge.i.i
                                        #   in Loop: Header=BB10_4 Depth=2
	cmpl	%eax, %ebx
	jne	.LBB10_4
	jmp	.LBB10_7
.LBB10_8:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	show_list, .Lfunc_end10-show_list
	.cfi_endproc

	.globl	show_play
	.p2align	4, 0x90
	.type	show_play,@function
show_play:                              # @show_play
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 48
.Lcfi35:
	.cfi_offset %rbx, -40
.Lcfi36:
	.cfi_offset %r12, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	jne	.LBB11_2
	jmp	.LBB11_16
	.p2align	4, 0x90
.LBB11_15:                              # %show_list.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	24(%r14), %r14
	testq	%r14, %r14
	je	.LBB11_16
.LBB11_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_4 Depth 2
                                        #     Child Loop BB11_9 Depth 2
                                        #       Child Loop BB11_11 Depth 3
	movl	$.Lstr.1, %edi
	callq	puts
	cmpl	$0, ncol(%rip)
	je	.LBB11_7
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	8(%r14), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_4:                               # %.lr.ph.i
                                        #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r15,%rbx,4), %esi
	incq	%rbx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	ncol(%rip), %eax
	cmpl	%eax, %ebx
	je	.LBB11_6
# BB#5:                                 #   in Loop: Header=BB11_4 Depth=2
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movl	ncol(%rip), %eax
.LBB11_6:                               # %.backedge.i
                                        #   in Loop: Header=BB11_4 Depth=2
	cmpl	%eax, %ebx
	jne	.LBB11_4
.LBB11_7:                               # %show_data.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	movl	(%r14), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.2, %edi
	callq	puts
	movq	16(%r14), %r15
	testq	%r15, %r15
	jne	.LBB11_9
	jmp	.LBB11_15
	.p2align	4, 0x90
.LBB11_14:                              # %show_move.exit.i
                                        #   in Loop: Header=BB11_9 Depth=2
	movl	$.Lstr, %edi
	callq	puts
	movq	8(%r15), %r15
	testq	%r15, %r15
	je	.LBB11_15
.LBB11_9:                               # %.lr.ph.i6
                                        #   Parent Loop BB11_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_11 Depth 3
	movq	(%r15), %r12
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	cmpl	$0, ncol(%rip)
	je	.LBB11_14
# BB#10:                                # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB11_9 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_11:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB11_2 Depth=1
                                        #     Parent Loop BB11_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r12,%rbx,4), %esi
	incq	%rbx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	ncol(%rip), %eax
	cmpl	%eax, %ebx
	je	.LBB11_13
# BB#12:                                #   in Loop: Header=BB11_11 Depth=3
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movl	ncol(%rip), %eax
.LBB11_13:                              # %.backedge.i.i.i
                                        #   in Loop: Header=BB11_11 Depth=3
	cmpl	%eax, %ebx
	jne	.LBB11_11
	jmp	.LBB11_14
.LBB11_16:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	show_play, .Lfunc_end11-show_play
	.cfi_endproc

	.globl	in_wanted
	.p2align	4, 0x90
	.type	in_wanted,@function
in_wanted:                              # @in_wanted
	.cfi_startproc
# BB#0:
	movq	wanted(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB12_7
# BB#1:                                 # %.lr.ph
	movslq	ncol(%rip), %r10
	leal	1(%r10), %r8d
	leaq	-4(%rdi,%r10,4), %r9
	decq	%r10
	shlq	$2, %r10
.LBB12_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_3 Depth 2
	movq	(%rcx), %rdi
	addq	%r10, %rdi
	movq	%r9, %rsi
	movl	%r8d, %edx
	.p2align	4, 0x90
.LBB12_3:                               #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$1, %edx
	je	.LBB12_8
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=2
	movl	(%rdi), %eax
	decl	%edx
	addq	$-4, %rdi
	cmpl	(%rsi), %eax
	leaq	-4(%rsi), %rsi
	je	.LBB12_3
# BB#5:                                 # %equal_data.exit
                                        #   in Loop: Header=BB12_2 Depth=1
	testl	%edx, %edx
	jle	.LBB12_8
# BB#6:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB12_2
.LBB12_7:                               # %._crit_edge
	xorl	%eax, %eax
	retq
.LBB12_8:                               # %equal_data.exit.thread
	xorl	%eax, %eax
	testq	%rcx, %rcx
	setne	%al
	retq
.Lfunc_end12:
	.size	in_wanted, .Lfunc_end12-in_wanted
	.cfi_endproc

	.globl	make_data
	.p2align	4, 0x90
	.type	make_data,@function
make_data:                              # @make_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	%edi, %ebx
	movslq	ncol(%rip), %rbp
	movq	%rbp, %rdi
	shlq	$2, %rdi
	callq	malloc
	testl	%r14d, %r14d
	je	.LBB13_1
# BB#2:                                 # %.lr.ph16
	movl	nrow(%rip), %ecx
	movl	%r14d, %edx
	cmpl	$7, %r14d
	jbe	.LBB13_3
# BB#6:                                 # %min.iters.checked
	movl	%r14d, %r8d
	andl	$7, %r8d
	movq	%rdx, %r9
	subq	%r8, %r9
	je	.LBB13_3
# BB#7:                                 # %vector.ph
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rax), %rdi
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB13_8:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$32, %rdi
	addq	$-8, %rsi
	jne	.LBB13_8
# BB#9:                                 # %middle.block
	testl	%r8d, %r8d
	jne	.LBB13_4
	jmp	.LBB13_10
.LBB13_3:
	xorl	%r9d, %r9d
.LBB13_4:                               # %scalar.ph.preheader
	leaq	(%rax,%r9,4), %rdi
	subq	%r9, %rdx
	.p2align	4, 0x90
.LBB13_5:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, (%rdi)
	addq	$4, %rdi
	decq	%rdx
	jne	.LBB13_5
	jmp	.LBB13_10
.LBB13_1:
	xorl	%r14d, %r14d
.LBB13_10:                              # %.preheader
	cmpl	%ebp, %r14d
	je	.LBB13_14
# BB#11:                                # %.lr.ph
	movslq	%r14d, %r10
	leal	-1(%rbp), %edx
	subl	%r14d, %edx
	incq	%rdx
	cmpq	$7, %rdx
	jbe	.LBB13_12
# BB#15:                                # %min.iters.checked26
	movabsq	$8589934584, %r11       # imm = 0x1FFFFFFF8
	movq	%rdx, %r9
	andq	%r11, %r9
	andq	%rdx, %r11
	je	.LBB13_12
# BB#16:                                # %vector.ph30
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	-8(%r11), %r8
	movl	%r8d, %ecx
	shrl	$3, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB13_17
# BB#18:                                # %vector.body22.prol.preheader
	leaq	16(%rax,%r10,4), %rdi
	negq	%rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB13_19:                              # %vector.body22.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rdi,%rsi,4)
	movdqu	%xmm0, (%rdi,%rsi,4)
	addq	$8, %rsi
	incq	%rcx
	jne	.LBB13_19
	jmp	.LBB13_20
.LBB13_17:
	xorl	%esi, %esi
.LBB13_20:                              # %vector.body22.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB13_23
# BB#21:                                # %vector.ph30.new
	movq	%r11, %rcx
	subq	%rsi, %rcx
	addq	%r10, %rsi
	leaq	112(%rax,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB13_22:                              # %vector.body22
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB13_22
.LBB13_23:                              # %middle.block23
	cmpq	%r11, %rdx
	je	.LBB13_14
# BB#24:
	addq	%r9, %r10
.LBB13_12:                              # %scalar.ph24.preheader
	leaq	(%rax,%r10,4), %rcx
	subl	%r10d, %ebp
	.p2align	4, 0x90
.LBB13_13:                              # %scalar.ph24
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, (%rcx)
	addq	$4, %rcx
	decl	%ebp
	jne	.LBB13_13
.LBB13_14:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end13:
	.size	make_data, .Lfunc_end13-make_data
	.cfi_endproc

	.globl	make_list
	.p2align	4, 0x90
	.type	make_list,@function
make_list:                              # @make_list
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 256
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	$1, (%rsi)
	movl	$16, %edi
	callq	malloc
	movq	$0, 8(%rax)
	movl	nrow(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB14_93
# BB#1:                                 # %.preheader.preheader
	movl	ncol(%rip), %r14d
	leaq	-16(%r13), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	leaq	-4(%r13), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movl	%r14d, %ebp
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB14_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_13 Depth 2
                                        #       Child Loop BB14_17 Depth 3
                                        #       Child Loop BB14_21 Depth 3
                                        #       Child Loop BB14_28 Depth 3
                                        #       Child Loop BB14_32 Depth 3
                                        #       Child Loop BB14_36 Depth 3
                                        #       Child Loop BB14_41 Depth 3
                                        #       Child Loop BB14_65 Depth 3
                                        #       Child Loop BB14_71 Depth 3
                                        #       Child Loop BB14_81 Depth 3
                                        #         Child Loop BB14_82 Depth 4
                                        #       Child Loop BB14_6 Depth 3
                                        #         Child Loop BB14_7 Depth 4
	testl	%ebp, %ebp
	je	.LBB14_89
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB14_2 Depth=1
	xorl	%r12d, %r12d
	jmp	.LBB14_13
.LBB14_4:                               #   in Loop: Header=BB14_13 Depth=2
	movslq	ncol(%rip), %rax
	leal	-1(%rax), %r12d
	movl	nrow(%rip), %r15d
	decl	%r15d
	movq	wanted(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB14_87
# BB#5:                                 # %.lr.ph.i48
                                        #   in Loop: Header=BB14_13 Depth=2
	leal	1(%rax), %r8d
	leaq	-4(%rbx,%rax,4), %r9
.LBB14_6:                               #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB14_7 Depth 4
	movq	(%rcx), %rdx
	leaq	-4(%rdx,%rax,4), %rdi
	movq	%r9, %rsi
	movl	%r8d, %edx
	.p2align	4, 0x90
.LBB14_7:                               #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        #       Parent Loop BB14_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$1, %edx
	je	.LBB14_11
# BB#8:                                 #   in Loop: Header=BB14_7 Depth=4
	movl	(%rdi), %ebp
	decl	%edx
	addq	$-4, %rdi
	cmpl	(%rsi), %ebp
	leaq	-4(%rsi), %rsi
	je	.LBB14_7
# BB#9:                                 # %equal_data.exit.i
                                        #   in Loop: Header=BB14_6 Depth=3
	testl	%edx, %edx
	jle	.LBB14_11
# BB#10:                                #   in Loop: Header=BB14_6 Depth=3
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB14_6
	jmp	.LBB14_87
.LBB14_11:                              # %in_wanted.exit
                                        #   in Loop: Header=BB14_13 Depth=2
	testq	%rcx, %rcx
	je	.LBB14_87
# BB#12:                                #   in Loop: Header=BB14_13 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	$2, (%rax)
	jmp	.LBB14_87
	.p2align	4, 0x90
.LBB14_13:                              # %.lr.ph
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_17 Depth 3
                                        #       Child Loop BB14_21 Depth 3
                                        #       Child Loop BB14_28 Depth 3
                                        #       Child Loop BB14_32 Depth 3
                                        #       Child Loop BB14_36 Depth 3
                                        #       Child Loop BB14_41 Depth 3
                                        #       Child Loop BB14_65 Depth 3
                                        #       Child Loop BB14_71 Depth 3
                                        #       Child Loop BB14_81 Depth 3
                                        #         Child Loop BB14_82 Depth 4
                                        #       Child Loop BB14_6 Depth 3
                                        #         Child Loop BB14_7 Depth 4
	movslq	%ebp, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rbx
	testl	%r12d, %r12d
	movl	$0, %esi
	je	.LBB14_23
# BB#14:                                # %.lr.ph16.i
                                        #   in Loop: Header=BB14_13 Depth=2
	movl	nrow(%rip), %eax
	movl	%r12d, %ecx
	cmpl	$7, %r12d
	jbe	.LBB14_19
# BB#15:                                # %min.iters.checked158
                                        #   in Loop: Header=BB14_13 Depth=2
	movl	%r12d, %esi
	andl	$7, %esi
	movq	%rcx, %r8
	subq	%rsi, %r8
	je	.LBB14_19
# BB#16:                                # %vector.ph162
                                        #   in Loop: Header=BB14_13 Depth=2
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rbx), %rdi
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB14_17:                              # %vector.body154
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB14_17
# BB#18:                                # %middle.block155
                                        #   in Loop: Header=BB14_13 Depth=2
	testl	%esi, %esi
	movl	%r12d, %esi
	jne	.LBB14_20
	jmp	.LBB14_23
	.p2align	4, 0x90
.LBB14_19:                              #   in Loop: Header=BB14_13 Depth=2
	xorl	%r8d, %r8d
.LBB14_20:                              # %scalar.ph156.preheader
                                        #   in Loop: Header=BB14_13 Depth=2
	subq	%r8, %rcx
	leaq	(%rbx,%r8,4), %rdx
	.p2align	4, 0x90
.LBB14_21:                              # %scalar.ph156
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, (%rdx)
	addq	$4, %rdx
	decq	%rcx
	jne	.LBB14_21
# BB#22:                                #   in Loop: Header=BB14_13 Depth=2
	movl	%r12d, %esi
.LBB14_23:                              # %.preheader.i
                                        #   in Loop: Header=BB14_13 Depth=2
	cmpl	%ebp, %esi
	je	.LBB14_37
# BB#24:                                # %.lr.ph.i
                                        #   in Loop: Header=BB14_13 Depth=2
	movslq	%esi, %rax
	leal	-1(%rbp), %ecx
	subl	%esi, %ecx
	incq	%rcx
	cmpq	$7, %rcx
	jbe	.LBB14_35
# BB#25:                                # %min.iters.checked137
                                        #   in Loop: Header=BB14_13 Depth=2
	movq	%rcx, %r9
	movabsq	$8589934584, %rdx       # imm = 0x1FFFFFFF8
	andq	%rdx, %r9
	movq	%rcx, %r10
	andq	%rdx, %r10
	je	.LBB14_35
# BB#26:                                # %vector.ph141
                                        #   in Loop: Header=BB14_13 Depth=2
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	-8(%r10), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB14_29
# BB#27:                                # %vector.body133.prol.preheader
                                        #   in Loop: Header=BB14_13 Depth=2
	leaq	16(%rbx,%rax,4), %rsi
	negq	%rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_28:                              # %vector.body133.prol
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm0, -16(%rsi,%rdx,4)
	movdqu	%xmm0, (%rsi,%rdx,4)
	addq	$8, %rdx
	incq	%rdi
	jne	.LBB14_28
	jmp	.LBB14_30
.LBB14_29:                              #   in Loop: Header=BB14_13 Depth=2
	xorl	%edx, %edx
.LBB14_30:                              # %vector.body133.prol.loopexit
                                        #   in Loop: Header=BB14_13 Depth=2
	cmpq	$24, %r8
	jb	.LBB14_33
# BB#31:                                # %vector.ph141.new
                                        #   in Loop: Header=BB14_13 Depth=2
	movq	%r10, %rdi
	subq	%rdx, %rdi
	addq	%rax, %rdx
	leaq	112(%rbx,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB14_32:                              # %vector.body133
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	subq	$-128, %rdx
	addq	$-32, %rdi
	jne	.LBB14_32
.LBB14_33:                              # %middle.block134
                                        #   in Loop: Header=BB14_13 Depth=2
	cmpq	%r10, %rcx
	je	.LBB14_37
# BB#34:                                #   in Loop: Header=BB14_13 Depth=2
	addq	%r9, %rax
	.p2align	4, 0x90
.LBB14_35:                              # %scalar.ph135.preheader
                                        #   in Loop: Header=BB14_13 Depth=2
	subl	%eax, %ebp
	leaq	(%rbx,%rax,4), %rax
	.p2align	4, 0x90
.LBB14_36:                              # %scalar.ph135
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r15d, (%rax)
	addq	$4, %rax
	decl	%ebp
	jne	.LBB14_36
.LBB14_37:                              # %make_data.exit
                                        #   in Loop: Header=BB14_13 Depth=2
	testl	%r14d, %r14d
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	je	.LBB14_61
# BB#38:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB14_13 Depth=2
	movslq	%r14d, %r12
	leal	-1(%r14), %ecx
	incq	%rcx
	cmpq	$8, %rcx
	movq	%r12, %rax
	jb	.LBB14_59
# BB#39:                                # %min.iters.checked
                                        #   in Loop: Header=BB14_13 Depth=2
	movq	%rcx, %rdx
	movabsq	$8589934584, %rax       # imm = 0x1FFFFFFF8
	andq	%rax, %rdx
	movq	%rcx, %rbp
	andq	%rax, %rbp
	movq	%r12, %rax
	je	.LBB14_59
# BB#40:                                # %vector.body.preheader
                                        #   in Loop: Header=BB14_13 Depth=2
	movq	%r12, %rax
	subq	%rdx, %rax
	leaq	-32(%rbx,%r12,4), %rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r12,4), %rdi
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB14_41:                              # %vector.body
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%rsi), %xmm2
	movdqu	16(%rsi), %xmm0
	pshufd	$27, %xmm0, %xmm3       # xmm3 = xmm0[3,2,1,0]
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	pshufd	$27, %xmm1, %xmm4       # xmm4 = xmm1[3,2,1,0]
	pcmpgtd	%xmm4, %xmm3
	movdqa	%xmm3, 176(%rsp)
	testb	$1, 176(%rsp)
	je	.LBB14_43
# BB#42:                                # %pred.store.if
                                        #   in Loop: Header=BB14_41 Depth=3
	pshufd	$231, %xmm1, %xmm4      # xmm4 = xmm1[3,1,2,3]
	movd	%xmm4, 28(%rsi)
.LBB14_43:                              # %pred.store.continue
                                        #   in Loop: Header=BB14_41 Depth=3
	movdqa	%xmm3, 160(%rsp)
	testb	$1, 164(%rsp)
	je	.LBB14_45
# BB#44:                                # %pred.store.if119
                                        #   in Loop: Header=BB14_41 Depth=3
	pshufd	$78, %xmm1, %xmm4       # xmm4 = xmm1[2,3,0,1]
	movd	%xmm4, 24(%rsi)
.LBB14_45:                              # %pred.store.continue120
                                        #   in Loop: Header=BB14_41 Depth=3
	movdqa	%xmm3, 144(%rsp)
	testb	$1, 152(%rsp)
	je	.LBB14_47
# BB#46:                                # %pred.store.if121
                                        #   in Loop: Header=BB14_41 Depth=3
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	movd	%xmm4, 20(%rsi)
.LBB14_47:                              # %pred.store.continue122
                                        #   in Loop: Header=BB14_41 Depth=3
	pshufd	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	pshufd	$27, %xmm0, %xmm4       # xmm4 = xmm0[3,2,1,0]
	movdqa	%xmm3, 128(%rsp)
	testb	$1, 140(%rsp)
	je	.LBB14_49
# BB#48:                                # %pred.store.if123
                                        #   in Loop: Header=BB14_41 Depth=3
	movd	%xmm1, 16(%rsi)
.LBB14_49:                              # %pred.store.continue124
                                        #   in Loop: Header=BB14_41 Depth=3
	pcmpgtd	%xmm4, %xmm2
	movdqa	%xmm2, 112(%rsp)
	testb	$1, 112(%rsp)
	je	.LBB14_51
# BB#50:                                # %pred.store.if125
                                        #   in Loop: Header=BB14_41 Depth=3
	pshufd	$231, %xmm0, %xmm1      # xmm1 = xmm0[3,1,2,3]
	movd	%xmm1, 12(%rsi)
.LBB14_51:                              # %pred.store.continue126
                                        #   in Loop: Header=BB14_41 Depth=3
	movdqa	%xmm2, 96(%rsp)
	testb	$1, 100(%rsp)
	je	.LBB14_53
# BB#52:                                # %pred.store.if127
                                        #   in Loop: Header=BB14_41 Depth=3
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movd	%xmm1, 8(%rsi)
.LBB14_53:                              # %pred.store.continue128
                                        #   in Loop: Header=BB14_41 Depth=3
	movdqa	%xmm2, 80(%rsp)
	testb	$1, 88(%rsp)
	je	.LBB14_55
# BB#54:                                # %pred.store.if129
                                        #   in Loop: Header=BB14_41 Depth=3
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	movd	%xmm1, 4(%rsi)
.LBB14_55:                              # %pred.store.continue130
                                        #   in Loop: Header=BB14_41 Depth=3
	movdqa	%xmm2, 64(%rsp)
	testb	$1, 76(%rsp)
	je	.LBB14_57
# BB#56:                                # %pred.store.if131
                                        #   in Loop: Header=BB14_41 Depth=3
	movd	%xmm0, (%rsi)
.LBB14_57:                              # %pred.store.continue132
                                        #   in Loop: Header=BB14_41 Depth=3
	addq	$-32, %rsi
	addq	$-32, %rdi
	addq	$-8, %rdx
	jne	.LBB14_41
# BB#58:                                # %middle.block
                                        #   in Loop: Header=BB14_13 Depth=2
	cmpq	%rbp, %rcx
	je	.LBB14_70
	.p2align	4, 0x90
.LBB14_59:                              # %.lr.ph.i47.preheader
                                        #   in Loop: Header=BB14_13 Depth=2
	leal	-1(%rax), %edx
	testb	$1, %al
	jne	.LBB14_62
# BB#60:                                #   in Loop: Header=BB14_13 Depth=2
	movq	%rax, %rcx
	testl	%edx, %edx
	jne	.LBB14_65
	jmp	.LBB14_70
	.p2align	4, 0x90
.LBB14_61:                              #   in Loop: Header=BB14_13 Depth=2
	xorl	%r12d, %r12d
	jmp	.LBB14_70
	.p2align	4, 0x90
.LBB14_62:                              # %.lr.ph.i47.prol
                                        #   in Loop: Header=BB14_13 Depth=2
	leaq	-1(%rax), %rcx
	movl	-4(%r13,%rax,4), %esi
	cmpl	%esi, -4(%rbx,%rax,4)
	jle	.LBB14_64
# BB#63:                                #   in Loop: Header=BB14_13 Depth=2
	movl	%esi, -4(%rbx,%rax,4)
.LBB14_64:                              # %.lr.ph.i47.prol.loopexit
                                        #   in Loop: Header=BB14_13 Depth=2
	testl	%edx, %edx
	je	.LBB14_70
	.p2align	4, 0x90
.LBB14_65:                              # %.lr.ph.i47
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%r13,%rcx,4), %eax
	cmpl	%eax, -4(%rbx,%rcx,4)
	jle	.LBB14_67
# BB#66:                                #   in Loop: Header=BB14_65 Depth=3
	movl	%eax, -4(%rbx,%rcx,4)
.LBB14_67:                              # %.backedge.i
                                        #   in Loop: Header=BB14_65 Depth=3
	leaq	-2(%rcx), %rax
	movl	-8(%r13,%rcx,4), %edx
	cmpl	%edx, -8(%rbx,%rcx,4)
	jle	.LBB14_69
# BB#68:                                #   in Loop: Header=BB14_65 Depth=3
	movl	%edx, -8(%rbx,%rcx,4)
.LBB14_69:                              # %.backedge.i.1
                                        #   in Loop: Header=BB14_65 Depth=3
	testl	%eax, %eax
	movq	%rax, %rcx
	jne	.LBB14_65
.LBB14_70:                              # %melt_data.exit
                                        #   in Loop: Header=BB14_13 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r12,4), %rcx
	leaq	-4(%rbx,%r12,4), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB14_71:                              #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r12d, %esi
	addl	%eax, %esi
	je	.LBB14_78
# BB#72:                                #   in Loop: Header=BB14_71 Depth=3
	movl	(%rdx,%rax,4), %esi
	cmpl	(%rcx,%rax,4), %esi
	leaq	-1(%rax), %rax
	je	.LBB14_71
# BB#73:                                # %equal_data.exit
                                        #   in Loop: Header=BB14_13 Depth=2
	leal	1(%r12,%rax), %eax
	testl	%eax, %eax
	jle	.LBB14_78
# BB#74:                                #   in Loop: Header=BB14_13 Depth=2
	movl	$16, %edi
	callq	malloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx)
	leaq	(,%r12,4), %rbp
	movq	%rbp, %rdi
	callq	malloc
	testl	%r14d, %r14d
	je	.LBB14_76
# BB#75:                                # %.lr.ph.preheader.i54
                                        #   in Loop: Header=BB14_13 Depth=2
	leaq	-4(%rbp), %rdx
	decl	%r14d
	leaq	(,%r14,4), %rcx
	subq	%rcx, %rdx
	movq	%rax, %rdi
	addq	%rdx, %rdi
	leaq	-1(%r12), %rcx
	subq	%r14, %rcx
	leaq	(%rbx,%rcx,4), %rsi
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	4(,%r14,4), %rdx
	callq	memcpy
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB14_76:                              # %copy_data.exit
                                        #   in Loop: Header=BB14_13 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	8(%rcx), %rdx
	movq	%rax, (%rdx)
	movq	$0, 8(%rdx)
	movq	8(%rcx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpl	$1, %eax
	jne	.LBB14_85
# BB#77:                                # %.preheader92.preheader
                                        #   in Loop: Header=BB14_13 Depth=2
	leal	1(%r12), %r8d
	leaq	-4(%rbx,%rbp), %rbp
	movl	$game_tree, %edx
	jmp	.LBB14_81
	.p2align	4, 0x90
.LBB14_78:                              # %equal_data.exit.thread
                                        #   in Loop: Header=BB14_13 Depth=2
	movl	nrow(%rip), %eax
	decl	%eax
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	cmovel	%eax, %r15d
	decl	%r14d
	movl	%r14d, %r12d
	jmp	.LBB14_87
	.p2align	4, 0x90
.LBB14_79:                              # %equal_data.exit.i52
                                        #   in Loop: Header=BB14_81 Depth=3
	testl	%edi, %edi
	jle	.LBB14_84
# BB#80:                                #   in Loop: Header=BB14_81 Depth=3
	addq	$24, %rdx
.LBB14_81:                              # %.preheader92
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB14_82 Depth 4
	movq	(%rdx), %rdx
	movq	8(%rdx), %rcx
	leaq	-4(%rcx,%r12,4), %rsi
	movq	%rbp, %rcx
	movl	%r8d, %edi
	.p2align	4, 0x90
.LBB14_82:                              #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_13 Depth=2
                                        #       Parent Loop BB14_81 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$1, %edi
	je	.LBB14_84
# BB#83:                                #   in Loop: Header=BB14_82 Depth=4
	movl	(%rsi), %eax
	decl	%edi
	addq	$-4, %rsi
	cmpl	(%rcx), %eax
	leaq	-4(%rcx), %rcx
	je	.LBB14_82
	jmp	.LBB14_79
.LBB14_84:                              # %get_value.exit
                                        #   in Loop: Header=BB14_13 Depth=2
	movl	(%rdx), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, (%rcx)
.LBB14_85:                              #   in Loop: Header=BB14_13 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	orl	(%rcx), %eax
	je	.LBB14_4
# BB#86:                                #   in Loop: Header=BB14_13 Depth=2
	movl	4(%rsp), %r12d          # 4-byte Reload
	.p2align	4, 0x90
.LBB14_87:                              # %in_wanted.exit.thread
                                        #   in Loop: Header=BB14_13 Depth=2
	movq	%rbx, %rdi
	callq	free
	incl	%r12d
	movl	ncol(%rip), %r14d
	cmpl	%r14d, %r12d
	movl	%r14d, %ebp
	jne	.LBB14_13
# BB#88:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB14_2 Depth=1
	movl	nrow(%rip), %ecx
	movl	%r12d, %ebp
	jmp	.LBB14_90
	.p2align	4, 0x90
.LBB14_89:                              #   in Loop: Header=BB14_2 Depth=1
	movl	%r14d, %r12d
	xorl	%ebp, %ebp
.LBB14_90:                              # %._crit_edge
                                        #   in Loop: Header=BB14_2 Depth=1
	incl	%r15d
	cmpl	%ecx, %r15d
	movl	%r12d, %r14d
	jne	.LBB14_2
# BB#91:                                # %._crit_edge77
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	je	.LBB14_94
# BB#92:
	movl	$1, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	subl	(%rcx), %eax
	movl	%eax, (%rcx)
	jmp	.LBB14_95
.LBB14_93:                              # %._crit_edge77.thread
	movq	%rax, %rdi
	callq	free
.LBB14_94:
	xorl	%ebx, %ebx
.LBB14_95:
	movq	%rbx, %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	make_list, .Lfunc_end14-make_list
	.cfi_endproc

	.globl	make_play
	.p2align	4, 0x90
	.type	make_play,@function
make_play:                              # @make_play
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 64
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movl	%edi, (%rsp)
	movl	$32, %edi
	callq	malloc
	movq	%rax, %r14
	movq	$0, game_tree(%rip)
	movslq	ncol(%rip), %rbp
	leaq	(,%rbp,4), %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbp, %rbp
	je	.LBB15_2
# BB#1:                                 # %.lr.ph.i.preheader
	leal	-1(%rbp), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
	movl	(%rbx), %eax
	decl	%eax
	jmp	.LBB15_3
.LBB15_2:
                                        # implicit-def: %EAX
.LBB15_3:                               # %make_data.exit
	movl	%eax, (%rbx)
	testl	%ebp, %ebp
	je	.LBB15_29
# BB#4:                                 # %make_data.exit32.preheader.lr.ph.preheader
	movq	%rsp, %r12
	movq	%r14, %r15
.LBB15_16:                              # %make_data.exit32.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_17 Depth 2
                                        #       Child Loop BB15_18 Depth 3
                                        #       Child Loop BB15_21 Depth 3
                                        #     Child Loop BB15_11 Depth 2
                                        #     Child Loop BB15_15 Depth 2
	movl	%ebp, %ebp
	movl	nrow(%rip), %eax
.LBB15_17:                              # %make_data.exit32
                                        #   Parent Loop BB15_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_18 Depth 3
                                        #       Child Loop BB15_21 Depth 3
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_18:                              # %.lr.ph.i24
                                        #   Parent Loop BB15_16 Depth=1
                                        #     Parent Loop BB15_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx,%rcx,4), %edx
	cmpl	%eax, %edx
	jne	.LBB15_20
# BB#19:                                # %.outer.i
                                        #   in Loop: Header=BB15_18 Depth=3
	movl	$0, (%rbx,%rcx,4)
	incq	%rcx
	cmpl	%ecx, %ebp
	jne	.LBB15_18
	jmp	.LBB15_29
	.p2align	4, 0x90
.LBB15_20:                              # %.lr.ph.i33
                                        #   in Loop: Header=BB15_17 Depth=2
	incl	%edx
	movl	%edx, (%rbx,%rcx,4)
	xorl	%ecx, %ecx
	movl	%eax, %edx
	.p2align	4, 0x90
.LBB15_21:                              #   Parent Loop BB15_16 Depth=1
                                        #     Parent Loop BB15_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %esi
	movl	(%rbx,%rcx,4), %edx
	cmpl	%esi, %edx
	jg	.LBB15_23
# BB#22:                                #   in Loop: Header=BB15_21 Depth=3
	incq	%rcx
	cmpl	%ecx, %ebp
	jne	.LBB15_21
.LBB15_23:                              # %valid_data.exit
                                        #   in Loop: Header=BB15_17 Depth=2
	cmpl	%ebp, %ecx
	jne	.LBB15_17
# BB#24:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB15_16 Depth=1
	movl	$32, %edi
	callq	malloc
	movq	%rax, 24(%r15)
	cmpq	$0, game_tree(%rip)
	jne	.LBB15_26
# BB#25:                                #   in Loop: Header=BB15_16 Depth=1
	movq	%rax, game_tree(%rip)
.LBB15_26:                              # %copy_data.exit
                                        #   in Loop: Header=BB15_16 Depth=1
	movslq	%ebp, %rbp
	leaq	(,%rbp,4), %rdi
	callq	malloc
	movq	%rax, %r13
	leaq	-4(,%rbp,4), %rdi
	leal	-1(%rbp), %eax
	leaq	(,%rax,4), %rcx
	subq	%rcx, %rdi
	addq	%r13, %rdi
	decq	%rbp
	subq	%rax, %rbp
	leaq	(%rbx,%rbp,4), %rsi
	leaq	4(,%rax,4), %rdx
	callq	memcpy
	movq	24(%r15), %rax
	movq	%r13, 8(%rax)
	movq	%rbx, %rdi
	leaq	4(%rsp), %rsi
	movq	%r12, %rdx
	callq	make_list
	movq	24(%r15), %rcx
	movq	%rax, 16(%rcx)
	movl	4(%rsp), %eax
	movl	%eax, (%rcx)
	movq	$0, 24(%rcx)
	movq	24(%r15), %r15
	cmpl	$2, (%rsp)
	je	.LBB15_5
# BB#27:                                # %copy_data.exit.make_data.exit32.outer39_crit_edge
                                        #   in Loop: Header=BB15_16 Depth=1
	movl	ncol(%rip), %ebp
.LBB15_28:                              # %make_data.exit32.outer.backedge
                                        #   in Loop: Header=BB15_16 Depth=1
	testl	%ebp, %ebp
	jne	.LBB15_16
	jmp	.LBB15_29
.LBB15_5:                               #   in Loop: Header=BB15_16 Depth=1
	movq	%rbx, %rdi
	callq	free
	movslq	ncol(%rip), %rbp
	leaq	(,%rbp,4), %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbp, %rbp
	je	.LBB15_8
# BB#6:                                 # %.lr.ph16.i
                                        #   in Loop: Header=BB15_16 Depth=1
	movl	%ebp, %eax
	movl	nrow(%rip), %ecx
	cmpl	$8, %ebp
	jb	.LBB15_13
# BB#9:                                 # %min.iters.checked
                                        #   in Loop: Header=BB15_16 Depth=1
	movl	%ebp, %r8d
	andl	$7, %r8d
	movq	%rax, %rsi
	subq	%r8, %rsi
	je	.LBB15_13
# BB#10:                                # %vector.ph
                                        #   in Loop: Header=BB15_16 Depth=1
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rbx), %rdi
	movq	%rsi, %rdx
.LBB15_11:                              # %vector.body
                                        #   Parent Loop BB15_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB15_11
# BB#12:                                # %middle.block
                                        #   in Loop: Header=BB15_16 Depth=1
	testl	%r8d, %r8d
	jne	.LBB15_14
	jmp	.LBB15_28
.LBB15_13:                              #   in Loop: Header=BB15_16 Depth=1
	xorl	%esi, %esi
.LBB15_14:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB15_16 Depth=1
	subq	%rsi, %rax
	leaq	(%rbx,%rsi,4), %rdx
.LBB15_15:                              # %scalar.ph
                                        #   Parent Loop BB15_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, (%rdx)
	addq	$4, %rdx
	decq	%rax
	jne	.LBB15_15
	jmp	.LBB15_28
.LBB15_8:                               #   in Loop: Header=BB15_16 Depth=1
	xorl	%ebp, %ebp
	testl	%ebp, %ebp
	jne	.LBB15_16
.LBB15_29:                              # %.loopexit
	movq	24(%r14), %rbx
	movq	%r14, %rdi
	callq	free
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	make_play, .Lfunc_end15-make_play
	.cfi_endproc

	.globl	make_wanted
	.p2align	4, 0x90
	.type	make_wanted,@function
make_wanted:                            # @make_wanted
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi77:
	.cfi_def_cfa_offset 240
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$16, %edi
	callq	malloc
	movq	$0, 8(%rax)
	movl	nrow(%rip), %ecx
	xorl	%r14d, %r14d
	testl	%ecx, %ecx
	je	.LBB16_72
# BB#1:                                 # %.preheader.preheader
	movl	ncol(%rip), %esi
	leaq	-16(%r15), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	leaq	-4(%r15), %r13
	movl	%esi, %ebp
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB16_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_3 Depth 2
                                        #       Child Loop BB16_11 Depth 3
                                        #       Child Loop BB16_7 Depth 3
                                        #       Child Loop BB16_30 Depth 3
                                        #       Child Loop BB16_33 Depth 3
                                        #       Child Loop BB16_16 Depth 3
                                        #       Child Loop BB16_39 Depth 3
                                        #       Child Loop BB16_62 Depth 3
                                        #       Child Loop BB16_20 Depth 3
	xorl	%r12d, %r12d
	testl	%ebp, %ebp
	je	.LBB16_70
	.p2align	4, 0x90
.LBB16_3:                               # %.lr.ph
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_11 Depth 3
                                        #       Child Loop BB16_7 Depth 3
                                        #       Child Loop BB16_30 Depth 3
                                        #       Child Loop BB16_33 Depth 3
                                        #       Child Loop BB16_16 Depth 3
                                        #       Child Loop BB16_39 Depth 3
                                        #       Child Loop BB16_62 Depth 3
                                        #       Child Loop BB16_20 Depth 3
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movslq	%ebp, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rbx
	testl	%r12d, %r12d
	movl	$0, %esi
	je	.LBB16_13
# BB#4:                                 # %.lr.ph16.i
                                        #   in Loop: Header=BB16_3 Depth=2
	movl	nrow(%rip), %eax
	movl	%r12d, %ecx
	cmpl	$7, %r12d
	jbe	.LBB16_5
# BB#9:                                 # %min.iters.checked106
                                        #   in Loop: Header=BB16_3 Depth=2
	movl	%r12d, %esi
	andl	$7, %esi
	movq	%rcx, %r8
	subq	%rsi, %r8
	je	.LBB16_5
# BB#10:                                # %vector.ph110
                                        #   in Loop: Header=BB16_3 Depth=2
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rbx), %rdi
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB16_11:                              # %vector.body102
                                        #   Parent Loop BB16_2 Depth=1
                                        #     Parent Loop BB16_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB16_11
# BB#12:                                # %middle.block103
                                        #   in Loop: Header=BB16_3 Depth=2
	testl	%esi, %esi
	movl	%r12d, %esi
	jne	.LBB16_6
	jmp	.LBB16_13
	.p2align	4, 0x90
.LBB16_5:                               #   in Loop: Header=BB16_3 Depth=2
	xorl	%r8d, %r8d
.LBB16_6:                               # %scalar.ph104.preheader
                                        #   in Loop: Header=BB16_3 Depth=2
	subq	%r8, %rcx
	leaq	(%rbx,%r8,4), %rdx
	.p2align	4, 0x90
.LBB16_7:                               # %scalar.ph104
                                        #   Parent Loop BB16_2 Depth=1
                                        #     Parent Loop BB16_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, (%rdx)
	addq	$4, %rdx
	decq	%rcx
	jne	.LBB16_7
# BB#8:                                 #   in Loop: Header=BB16_3 Depth=2
	movl	%r12d, %esi
.LBB16_13:                              # %.preheader.i
                                        #   in Loop: Header=BB16_3 Depth=2
	cmpl	%ebp, %esi
	je	.LBB16_17
# BB#14:                                # %.lr.ph.i
                                        #   in Loop: Header=BB16_3 Depth=2
	movslq	%esi, %rax
	leal	-1(%rbp), %ecx
	subl	%esi, %ecx
	incq	%rcx
	cmpq	$7, %rcx
	jbe	.LBB16_15
# BB#26:                                # %min.iters.checked85
                                        #   in Loop: Header=BB16_3 Depth=2
	movq	%rcx, %r9
	movabsq	$8589934584, %rdx       # imm = 0x1FFFFFFF8
	andq	%rdx, %r9
	movq	%rcx, %r10
	andq	%rdx, %r10
	je	.LBB16_15
# BB#27:                                # %vector.ph89
                                        #   in Loop: Header=BB16_3 Depth=2
	movd	%r14d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	-8(%r10), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB16_28
# BB#29:                                # %vector.body81.prol.preheader
                                        #   in Loop: Header=BB16_3 Depth=2
	leaq	16(%rbx,%rax,4), %rsi
	negq	%rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB16_30:                              # %vector.body81.prol
                                        #   Parent Loop BB16_2 Depth=1
                                        #     Parent Loop BB16_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm0, -16(%rsi,%rdx,4)
	movdqu	%xmm0, (%rsi,%rdx,4)
	addq	$8, %rdx
	incq	%rdi
	jne	.LBB16_30
	jmp	.LBB16_31
.LBB16_28:                              #   in Loop: Header=BB16_3 Depth=2
	xorl	%edx, %edx
.LBB16_31:                              # %vector.body81.prol.loopexit
                                        #   in Loop: Header=BB16_3 Depth=2
	cmpq	$24, %r8
	jb	.LBB16_34
# BB#32:                                # %vector.ph89.new
                                        #   in Loop: Header=BB16_3 Depth=2
	movq	%r10, %rdi
	subq	%rdx, %rdi
	addq	%rax, %rdx
	leaq	112(%rbx,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB16_33:                              # %vector.body81
                                        #   Parent Loop BB16_2 Depth=1
                                        #     Parent Loop BB16_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	subq	$-128, %rdx
	addq	$-32, %rdi
	jne	.LBB16_33
.LBB16_34:                              # %middle.block82
                                        #   in Loop: Header=BB16_3 Depth=2
	cmpq	%r10, %rcx
	je	.LBB16_17
# BB#35:                                #   in Loop: Header=BB16_3 Depth=2
	addq	%r9, %rax
	.p2align	4, 0x90
.LBB16_15:                              # %scalar.ph83.preheader
                                        #   in Loop: Header=BB16_3 Depth=2
	subl	%eax, %ebp
	leaq	(%rbx,%rax,4), %rax
	.p2align	4, 0x90
.LBB16_16:                              # %scalar.ph83
                                        #   Parent Loop BB16_2 Depth=1
                                        #     Parent Loop BB16_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r14d, (%rax)
	addq	$4, %rax
	decl	%ebp
	jne	.LBB16_16
.LBB16_17:                              # %make_data.exit
                                        #   in Loop: Header=BB16_3 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	testl	%ebp, %ebp
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	je	.LBB16_18
# BB#36:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB16_3 Depth=2
	movslq	%ebp, %r12
	leal	-1(%rbp), %ecx
	incq	%rcx
	cmpq	$8, %rcx
	movq	%r12, %rax
	jb	.LBB16_57
# BB#37:                                # %min.iters.checked
                                        #   in Loop: Header=BB16_3 Depth=2
	movq	%rcx, %rdx
	movabsq	$8589934584, %rax       # imm = 0x1FFFFFFF8
	andq	%rax, %rdx
	movq	%rcx, %r8
	andq	%rax, %r8
	movq	%r12, %rax
	je	.LBB16_57
# BB#38:                                # %vector.body.preheader
                                        #   in Loop: Header=BB16_3 Depth=2
	movq	%r12, %rax
	subq	%rdx, %rax
	leaq	-32(%rbx,%r12,4), %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r12,4), %rdi
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB16_39:                              # %vector.body
                                        #   Parent Loop BB16_2 Depth=1
                                        #     Parent Loop BB16_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%rsi), %xmm2
	movdqu	16(%rsi), %xmm0
	pshufd	$27, %xmm0, %xmm3       # xmm3 = xmm0[3,2,1,0]
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	pshufd	$27, %xmm1, %xmm4       # xmm4 = xmm1[3,2,1,0]
	pcmpgtd	%xmm4, %xmm3
	movdqa	%xmm3, 160(%rsp)
	testb	$1, 160(%rsp)
	je	.LBB16_41
# BB#40:                                # %pred.store.if
                                        #   in Loop: Header=BB16_39 Depth=3
	pshufd	$231, %xmm1, %xmm4      # xmm4 = xmm1[3,1,2,3]
	movd	%xmm4, 28(%rsi)
.LBB16_41:                              # %pred.store.continue
                                        #   in Loop: Header=BB16_39 Depth=3
	movdqa	%xmm3, 144(%rsp)
	testb	$1, 148(%rsp)
	je	.LBB16_43
# BB#42:                                # %pred.store.if67
                                        #   in Loop: Header=BB16_39 Depth=3
	pshufd	$78, %xmm1, %xmm4       # xmm4 = xmm1[2,3,0,1]
	movd	%xmm4, 24(%rsi)
.LBB16_43:                              # %pred.store.continue68
                                        #   in Loop: Header=BB16_39 Depth=3
	movdqa	%xmm3, 128(%rsp)
	testb	$1, 136(%rsp)
	je	.LBB16_45
# BB#44:                                # %pred.store.if69
                                        #   in Loop: Header=BB16_39 Depth=3
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	movd	%xmm4, 20(%rsi)
.LBB16_45:                              # %pred.store.continue70
                                        #   in Loop: Header=BB16_39 Depth=3
	pshufd	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	pshufd	$27, %xmm0, %xmm4       # xmm4 = xmm0[3,2,1,0]
	movdqa	%xmm3, 112(%rsp)
	testb	$1, 124(%rsp)
	je	.LBB16_47
# BB#46:                                # %pred.store.if71
                                        #   in Loop: Header=BB16_39 Depth=3
	movd	%xmm1, 16(%rsi)
.LBB16_47:                              # %pred.store.continue72
                                        #   in Loop: Header=BB16_39 Depth=3
	pcmpgtd	%xmm4, %xmm2
	movdqa	%xmm2, 96(%rsp)
	testb	$1, 96(%rsp)
	je	.LBB16_49
# BB#48:                                # %pred.store.if73
                                        #   in Loop: Header=BB16_39 Depth=3
	pshufd	$231, %xmm0, %xmm1      # xmm1 = xmm0[3,1,2,3]
	movd	%xmm1, 12(%rsi)
.LBB16_49:                              # %pred.store.continue74
                                        #   in Loop: Header=BB16_39 Depth=3
	movdqa	%xmm2, 80(%rsp)
	testb	$1, 84(%rsp)
	je	.LBB16_51
# BB#50:                                # %pred.store.if75
                                        #   in Loop: Header=BB16_39 Depth=3
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movd	%xmm1, 8(%rsi)
.LBB16_51:                              # %pred.store.continue76
                                        #   in Loop: Header=BB16_39 Depth=3
	movdqa	%xmm2, 64(%rsp)
	testb	$1, 72(%rsp)
	je	.LBB16_53
# BB#52:                                # %pred.store.if77
                                        #   in Loop: Header=BB16_39 Depth=3
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	movd	%xmm1, 4(%rsi)
.LBB16_53:                              # %pred.store.continue78
                                        #   in Loop: Header=BB16_39 Depth=3
	movdqa	%xmm2, 48(%rsp)
	testb	$1, 60(%rsp)
	je	.LBB16_55
# BB#54:                                # %pred.store.if79
                                        #   in Loop: Header=BB16_39 Depth=3
	movd	%xmm0, (%rsi)
.LBB16_55:                              # %pred.store.continue80
                                        #   in Loop: Header=BB16_39 Depth=3
	addq	$-32, %rsi
	addq	$-32, %rdi
	addq	$-8, %rdx
	jne	.LBB16_39
# BB#56:                                # %middle.block
                                        #   in Loop: Header=BB16_3 Depth=2
	cmpq	%r8, %rcx
	je	.LBB16_19
	.p2align	4, 0x90
.LBB16_57:                              # %.lr.ph.i36.preheader
                                        #   in Loop: Header=BB16_3 Depth=2
	leal	-1(%rax), %edx
	testb	$1, %al
	jne	.LBB16_59
# BB#58:                                #   in Loop: Header=BB16_3 Depth=2
	movq	%rax, %rcx
	testl	%edx, %edx
	jne	.LBB16_62
	jmp	.LBB16_19
	.p2align	4, 0x90
.LBB16_18:                              #   in Loop: Header=BB16_3 Depth=2
	xorl	%r12d, %r12d
	jmp	.LBB16_19
	.p2align	4, 0x90
.LBB16_59:                              # %.lr.ph.i36.prol
                                        #   in Loop: Header=BB16_3 Depth=2
	leaq	-1(%rax), %rcx
	movl	-4(%r15,%rax,4), %esi
	cmpl	%esi, -4(%rbx,%rax,4)
	jle	.LBB16_61
# BB#60:                                #   in Loop: Header=BB16_3 Depth=2
	movl	%esi, -4(%rbx,%rax,4)
.LBB16_61:                              # %.lr.ph.i36.prol.loopexit
                                        #   in Loop: Header=BB16_3 Depth=2
	testl	%edx, %edx
	je	.LBB16_19
	.p2align	4, 0x90
.LBB16_62:                              # %.lr.ph.i36
                                        #   Parent Loop BB16_2 Depth=1
                                        #     Parent Loop BB16_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%r15,%rcx,4), %eax
	cmpl	%eax, -4(%rbx,%rcx,4)
	jle	.LBB16_64
# BB#63:                                #   in Loop: Header=BB16_62 Depth=3
	movl	%eax, -4(%rbx,%rcx,4)
.LBB16_64:                              # %.backedge.i
                                        #   in Loop: Header=BB16_62 Depth=3
	leaq	-2(%rcx), %rax
	movl	-8(%r15,%rcx,4), %edx
	cmpl	%edx, -8(%rbx,%rcx,4)
	jle	.LBB16_66
# BB#65:                                #   in Loop: Header=BB16_62 Depth=3
	movl	%edx, -8(%rbx,%rcx,4)
.LBB16_66:                              # %.backedge.i.1
                                        #   in Loop: Header=BB16_62 Depth=3
	testl	%eax, %eax
	movq	%rax, %rcx
	jne	.LBB16_62
.LBB16_19:                              # %melt_data.exit
                                        #   in Loop: Header=BB16_3 Depth=2
	leaq	(%r13,%r12,4), %rcx
	leaq	-4(%rbx,%r12,4), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_20:                              #   Parent Loop BB16_2 Depth=1
                                        #     Parent Loop BB16_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r12d, %esi
	addl	%eax, %esi
	je	.LBB16_67
# BB#21:                                #   in Loop: Header=BB16_20 Depth=3
	movl	(%rdx,%rax,4), %esi
	cmpl	(%rcx,%rax,4), %esi
	leaq	-1(%rax), %rax
	je	.LBB16_20
# BB#22:                                # %equal_data.exit
                                        #   in Loop: Header=BB16_3 Depth=2
	leal	1(%r12,%rax), %eax
	testl	%eax, %eax
	jle	.LBB16_67
# BB#23:                                #   in Loop: Header=BB16_3 Depth=2
	movl	$16, %edi
	callq	malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, 8(%rcx)
	leaq	(,%r12,4), %r13
	movq	%r13, %rdi
	callq	malloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	je	.LBB16_25
# BB#24:                                # %.lr.ph.preheader.i37
                                        #   in Loop: Header=BB16_3 Depth=2
	addq	$-4, %r13
	decl	%ecx
	leaq	(,%rcx,4), %rdx
	subq	%rdx, %r13
	movq	%rax, %rdi
	addq	%r13, %rdi
	decq	%r12
	subq	%rcx, %r12
	leaq	(%rbx,%r12,4), %rsi
	movq	%rax, %rbp
	leaq	4(,%rcx,4), %rdx
	callq	memcpy
	movq	%rbp, %rax
.LBB16_25:                              # %copy_data.exit
                                        #   in Loop: Header=BB16_3 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	8(%rcx), %rdx
	movq	%rax, (%rdx)
	movq	$0, 8(%rdx)
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	40(%rsp), %r13          # 8-byte Reload
	movl	4(%rsp), %r12d          # 4-byte Reload
	jmp	.LBB16_68
	.p2align	4, 0x90
.LBB16_67:                              # %equal_data.exit.thread
                                        #   in Loop: Header=BB16_3 Depth=2
	movl	nrow(%rip), %eax
	decl	%eax
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	cmovel	%eax, %r14d
	decl	%ebp
	movl	%ebp, %r12d
.LBB16_68:                              #   in Loop: Header=BB16_3 Depth=2
	movq	%rbx, %rdi
	callq	free
	incl	%r12d
	movl	ncol(%rip), %esi
	cmpl	%esi, %r12d
	movl	%esi, %ebp
	jne	.LBB16_3
# BB#69:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB16_2 Depth=1
	movl	nrow(%rip), %ecx
	movl	%r12d, %esi
.LBB16_70:                              # %._crit_edge
                                        #   in Loop: Header=BB16_2 Depth=1
	incl	%r14d
	cmpl	%ecx, %r14d
	movl	%r12d, %ebp
	jne	.LBB16_2
# BB#71:                                # %._crit_edge51.loopexit
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r14
.LBB16_72:                              # %._crit_edge51
	movq	%rax, %rdi
	callq	free
	movq	%r14, wanted(%rip)
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	make_wanted, .Lfunc_end16-make_wanted
	.cfi_endproc

	.globl	get_good_move
	.p2align	4, 0x90
	.type	get_good_move,@function
get_good_move:                          # @get_good_move
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 48
.Lcfi89:
	.cfi_offset %rbx, -40
.Lcfi90:
	.cfi_offset %r12, -32
.Lcfi91:
	.cfi_offset %r14, -24
.Lcfi92:
	.cfi_offset %r15, -16
	testq	%rdi, %rdi
	je	.LBB17_1
# BB#2:                                 # %.preheader
	movq	8(%rdi), %rax
	testq	%rax, %rax
	movslq	ncol(%rip), %r15
	je	.LBB17_12
# BB#3:                                 # %.lr.ph
	leal	1(%r15), %r8d
	leaq	-1(%r15), %r9
	shlq	$2, %r9
	.p2align	4, 0x90
.LBB17_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_5 Depth 2
                                        #       Child Loop BB17_6 Depth 3
	movq	%rax, %r10
	movq	(%rdi), %r11
	addq	%r9, %r11
	movl	$game_tree, %ebx
	jmp	.LBB17_5
	.p2align	4, 0x90
.LBB17_8:                               # %equal_data.exit.i
                                        #   in Loop: Header=BB17_5 Depth=2
	testl	%ecx, %ecx
	jle	.LBB17_10
# BB#9:                                 #   in Loop: Header=BB17_5 Depth=2
	addq	$24, %rbx
.LBB17_5:                               #   Parent Loop BB17_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_6 Depth 3
	movq	(%rbx), %rbx
	movq	8(%rbx), %rax
	addq	%r9, %rax
	movq	%r11, %rsi
	movl	%r8d, %ecx
	.p2align	4, 0x90
.LBB17_6:                               #   Parent Loop BB17_4 Depth=1
                                        #     Parent Loop BB17_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$1, %ecx
	je	.LBB17_10
# BB#7:                                 #   in Loop: Header=BB17_6 Depth=3
	movl	(%rax), %edx
	decl	%ecx
	addq	$-4, %rax
	cmpl	(%rsi), %edx
	leaq	-4(%rsi), %rsi
	je	.LBB17_6
	jmp	.LBB17_8
	.p2align	4, 0x90
.LBB17_10:                              # %get_value.exit
                                        #   in Loop: Header=BB17_4 Depth=1
	cmpl	$0, (%rbx)
	je	.LBB17_12
# BB#11:                                #   in Loop: Header=BB17_4 Depth=1
	movq	8(%r10), %rax
	testq	%rax, %rax
	movq	%r10, %rdi
	jne	.LBB17_4
.LBB17_12:                              # %.critedge
	movq	(%rdi), %r12
	leaq	(,%r15,4), %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r14
	testl	%r15d, %r15d
	je	.LBB17_14
# BB#13:                                # %.lr.ph.preheader.i
	addq	$-4, %rbx
	leal	-1(%r15), %eax
	leaq	(,%rax,4), %rcx
	subq	%rcx, %rbx
	addq	%r14, %rbx
	decq	%r15
	subq	%rax, %r15
	leaq	(%r12,%r15,4), %rsi
	leaq	4(,%rax,4), %rdx
	movq	%rbx, %rdi
	callq	memcpy
	jmp	.LBB17_14
.LBB17_1:
	xorl	%r14d, %r14d
.LBB17_14:                              # %copy_data.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	get_good_move, .Lfunc_end17-get_good_move
	.cfi_endproc

	.globl	get_winning_move
	.p2align	4, 0x90
	.type	get_winning_move,@function
get_winning_move:                       # @get_winning_move
	.cfi_startproc
# BB#0:
	.p2align	4, 0x90
.LBB18_1:                               # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB18_1
# BB#2:
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 48
.Lcfi98:
	.cfi_offset %rbx, -40
.Lcfi99:
	.cfi_offset %r12, -32
.Lcfi100:
	.cfi_offset %r14, -24
.Lcfi101:
	.cfi_offset %r15, -16
	movq	16(%rax), %r11
	testq	%r11, %r11
	je	.LBB18_3
# BB#4:                                 # %.preheader.i
	movq	8(%r11), %rcx
	testq	%rcx, %rcx
	movslq	ncol(%rip), %r15
	je	.LBB18_14
# BB#5:                                 # %.lr.ph.i.preheader
	leal	1(%r15), %r8d
	leaq	-1(%r15), %r9
	shlq	$2, %r9
	.p2align	4, 0x90
.LBB18_6:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_7 Depth 2
                                        #       Child Loop BB18_8 Depth 3
	movq	%rcx, %r10
	movq	(%r11), %rsi
	addq	%r9, %rsi
	movl	$game_tree, %ebx
	jmp	.LBB18_7
	.p2align	4, 0x90
.LBB18_10:                              # %equal_data.exit.i.i
                                        #   in Loop: Header=BB18_7 Depth=2
	testl	%edx, %edx
	jle	.LBB18_12
# BB#11:                                #   in Loop: Header=BB18_7 Depth=2
	addq	$24, %rbx
.LBB18_7:                               #   Parent Loop BB18_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_8 Depth 3
	movq	(%rbx), %rbx
	movq	8(%rbx), %rcx
	addq	%r9, %rcx
	movq	%rsi, %rdi
	movl	%r8d, %edx
	.p2align	4, 0x90
.LBB18_8:                               #   Parent Loop BB18_6 Depth=1
                                        #     Parent Loop BB18_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$1, %edx
	je	.LBB18_12
# BB#9:                                 #   in Loop: Header=BB18_8 Depth=3
	movl	(%rcx), %eax
	decl	%edx
	addq	$-4, %rcx
	cmpl	(%rdi), %eax
	leaq	-4(%rdi), %rdi
	je	.LBB18_8
	jmp	.LBB18_10
	.p2align	4, 0x90
.LBB18_12:                              # %get_value.exit.i
                                        #   in Loop: Header=BB18_6 Depth=1
	cmpl	$0, (%rbx)
	je	.LBB18_14
# BB#13:                                #   in Loop: Header=BB18_6 Depth=1
	movq	8(%r10), %rcx
	testq	%rcx, %rcx
	movq	%r10, %r11
	jne	.LBB18_6
.LBB18_14:                              # %.critedge.i
	movq	(%r11), %r12
	leaq	(,%r15,4), %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r14
	testl	%r15d, %r15d
	je	.LBB18_16
# BB#15:                                # %.lr.ph.preheader.i.i
	addq	$-4, %rbx
	leal	-1(%r15), %eax
	leaq	(,%rax,4), %rcx
	subq	%rcx, %rbx
	addq	%r14, %rbx
	decq	%r15
	subq	%rax, %r15
	leaq	(%r12,%r15,4), %rsi
	leaq	4(,%rax,4), %rdx
	movq	%rbx, %rdi
	callq	memcpy
	jmp	.LBB18_16
.LBB18_3:
	xorl	%r14d, %r14d
.LBB18_16:                              # %get_good_move.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	get_winning_move, .Lfunc_end18-get_winning_move
	.cfi_endproc

	.globl	where
	.p2align	4, 0x90
	.type	where,@function
where:                                  # @where
	.cfi_startproc
# BB#0:
	movslq	ncol(%rip), %r10
	leal	1(%r10), %r8d
	leaq	-4(%rdi,%r10,4), %r9
	decq	%r10
	shlq	$2, %r10
	jmp	.LBB19_1
	.p2align	4, 0x90
.LBB19_4:                               # %equal_data.exit
                                        #   in Loop: Header=BB19_1 Depth=1
	testl	%ecx, %ecx
	jle	.LBB19_6
# BB#5:                                 #   in Loop: Header=BB19_1 Depth=1
	movq	24(%rsi), %rsi
.LBB19_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_2 Depth 2
	movq	8(%rsi), %rdi
	addq	%r10, %rdi
	movq	%r9, %rdx
	movl	%r8d, %ecx
	.p2align	4, 0x90
.LBB19_2:                               #   Parent Loop BB19_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$1, %ecx
	je	.LBB19_6
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=2
	movl	(%rdi), %eax
	decl	%ecx
	addq	$-4, %rdi
	cmpl	(%rdx), %eax
	leaq	-4(%rdx), %rdx
	je	.LBB19_2
	jmp	.LBB19_4
.LBB19_6:                               # %equal_data.exit.thread
	movq	16(%rsi), %rax
	retq
.Lfunc_end19:
	.size	where, .Lfunc_end19-where
	.cfi_endproc

	.globl	get_real_move
	.p2align	4, 0x90
	.type	get_real_move,@function
get_real_move:                          # @get_real_move
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, (%rcx)
	movl	(%rdi,%rax,4), %r8d
	cmpl	(%rsi,%rax,4), %r8d
	leaq	1(%rax), %rax
	je	.LBB20_1
# BB#2:
	movl	%r8d, (%rdx)
	retq
.Lfunc_end20:
	.size	get_real_move, .Lfunc_end20-get_real_move
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi106:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi108:
	.cfi_def_cfa_offset 80
.Lcfi109:
	.cfi_offset %rbx, -56
.Lcfi110:
	.cfi_offset %r12, -48
.Lcfi111:
	.cfi_offset %r13, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	xorl	%ebx, %ebx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$7, ncol(%rip)
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$8, nrow(%rip)
	movl	$1, %edi
	callq	make_play
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	ncol(%rip), %rbp
	leaq	(,%rbp,4), %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%rbp, %rbp
	je	.LBB21_11
# BB#1:                                 # %.lr.ph16.i
	movl	%ebp, %eax
	movl	nrow(%rip), %ecx
	cmpl	$8, %ebp
	jb	.LBB21_2
# BB#6:                                 # %min.iters.checked
	movl	%ebp, %esi
	andl	$7, %esi
	movq	%rax, %rbx
	subq	%rsi, %rbx
	je	.LBB21_2
# BB#7:                                 # %vector.ph
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r12), %rdi
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB21_8:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB21_8
# BB#9:                                 # %middle.block
	testl	%esi, %esi
	jne	.LBB21_3
# BB#10:
	movl	%ebp, %ebx
	jmp	.LBB21_12
.LBB21_2:
	xorl	%ebx, %ebx
.LBB21_3:                               # %scalar.ph.preheader
	subq	%rbx, %rax
	leaq	(%r12,%rbx,4), %rdx
	.p2align	4, 0x90
.LBB21_4:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, (%rdx)
	addq	$4, %rdx
	decq	%rax
	jne	.LBB21_4
# BB#5:
	movl	%ebp, %ebx
	jmp	.LBB21_12
.LBB21_11:                              # %make_data.exit.preheader
	testq	%r12, %r12
	je	.LBB21_34
.LBB21_12:                              # %.lr.ph.preheader
	movl	%ebx, %eax
	xorl	%ebx, %ebx
	jmp	.LBB21_13
	.p2align	4, 0x90
.LBB21_37:                              # %get_real_move.exit..lr.ph_crit_edge
                                        #   in Loop: Header=BB21_13 Depth=1
	decl	%ecx
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movl	12(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	printf
	movq	%r12, %rdi
	callq	free
	movl	$1, %ecx
	subl	%ebx, %ecx
	movl	ncol(%rip), %eax
	movl	%ecx, %ebx
	movq	%rbp, %r12
.LBB21_13:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_14 Depth 2
                                        #       Child Loop BB21_15 Depth 3
                                        #     Child Loop BB21_22 Depth 2
                                        #       Child Loop BB21_23 Depth 3
                                        #         Child Loop BB21_24 Depth 4
                                        #     Child Loop BB21_36 Depth 2
	movl	%eax, %r14d
	movslq	%eax, %r15
	leaq	-4(%r12,%r15,4), %rcx
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB21_14
	.p2align	4, 0x90
.LBB21_17:                              # %equal_data.exit.i
                                        #   in Loop: Header=BB21_14 Depth=2
	leal	1(%r14,%rdx), %edx
	testl	%edx, %edx
	jle	.LBB21_19
# BB#18:                                #   in Loop: Header=BB21_14 Depth=2
	movq	24(%rax), %rax
.LBB21_14:                              #   Parent Loop BB21_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB21_15 Depth 3
	movq	8(%rax), %rdx
	leaq	-4(%rdx,%r15,4), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB21_15:                              #   Parent Loop BB21_13 Depth=1
                                        #     Parent Loop BB21_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r14d, %edi
	addl	%edx, %edi
	je	.LBB21_19
# BB#16:                                #   in Loop: Header=BB21_15 Depth=3
	movl	(%rsi,%rdx,4), %edi
	cmpl	(%rcx,%rdx,4), %edi
	leaq	-1(%rdx), %rdx
	je	.LBB21_15
	jmp	.LBB21_17
	.p2align	4, 0x90
.LBB21_19:                              # %where.exit
                                        #   in Loop: Header=BB21_13 Depth=1
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB21_34
# BB#20:                                # %.preheader.i35
                                        #   in Loop: Header=BB21_13 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	je	.LBB21_30
# BB#21:                                # %.lr.ph.i36.preheader
                                        #   in Loop: Header=BB21_13 Depth=1
	leal	1(%r14), %r8d
	.p2align	4, 0x90
.LBB21_22:                              # %.lr.ph.i36
                                        #   Parent Loop BB21_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB21_23 Depth 3
                                        #         Child Loop BB21_24 Depth 4
	movq	%rcx, %r9
	movq	(%rax), %rcx
	leaq	-4(%rcx,%r15,4), %rbx
	movl	$game_tree, %edi
	jmp	.LBB21_23
	.p2align	4, 0x90
.LBB21_26:                              # %equal_data.exit.i.i
                                        #   in Loop: Header=BB21_23 Depth=3
	testl	%ecx, %ecx
	jle	.LBB21_28
# BB#27:                                #   in Loop: Header=BB21_23 Depth=3
	addq	$24, %rdi
.LBB21_23:                              #   Parent Loop BB21_13 Depth=1
                                        #     Parent Loop BB21_22 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB21_24 Depth 4
	movq	(%rdi), %rdi
	movq	8(%rdi), %rcx
	leaq	-4(%rcx,%r15,4), %rbp
	movq	%rbx, %rsi
	movl	%r8d, %ecx
	.p2align	4, 0x90
.LBB21_24:                              #   Parent Loop BB21_13 Depth=1
                                        #     Parent Loop BB21_22 Depth=2
                                        #       Parent Loop BB21_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$1, %ecx
	je	.LBB21_28
# BB#25:                                #   in Loop: Header=BB21_24 Depth=4
	movl	(%rbp), %edx
	decl	%ecx
	addq	$-4, %rbp
	cmpl	(%rsi), %edx
	leaq	-4(%rsi), %rsi
	je	.LBB21_24
	jmp	.LBB21_26
	.p2align	4, 0x90
.LBB21_28:                              # %get_value.exit.i
                                        #   in Loop: Header=BB21_22 Depth=2
	cmpl	$0, (%rdi)
	je	.LBB21_30
# BB#29:                                #   in Loop: Header=BB21_22 Depth=2
	movq	8(%r9), %rcx
	testq	%rcx, %rcx
	movq	%r9, %rax
	jne	.LBB21_22
.LBB21_30:                              # %.critedge.i
                                        #   in Loop: Header=BB21_13 Depth=1
	movq	(%rax), %rbx
	leaq	(,%r15,4), %r13
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, %rbp
	testl	%r14d, %r14d
	je	.LBB21_32
# BB#31:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB21_13 Depth=1
	addq	$-4, %r13
	leal	-1(%r15), %eax
	leaq	(,%rax,4), %rcx
	subq	%rcx, %r13
	addq	%rbp, %r13
	decq	%r15
	subq	%rax, %r15
	leaq	(%rbx,%r15,4), %rsi
	leaq	4(,%rax,4), %rdx
	movq	%r13, %rdi
	callq	memcpy
.LBB21_32:                              # %get_good_move.exit
                                        #   in Loop: Header=BB21_13 Depth=1
	testq	%rbp, %rbp
	je	.LBB21_33
# BB#35:                                # %.preheader
                                        #   in Loop: Header=BB21_13 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB21_36:                              #   Parent Loop BB21_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rcx,4), %edx
	cmpl	(%r12,%rcx,4), %edx
	leaq	1(%rcx), %rcx
	je	.LBB21_36
	jmp	.LBB21_37
.LBB21_33:
	movl	12(%rsp), %ebx          # 4-byte Reload
.LBB21_34:                              # %make_data.exit.outer._crit_edge
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	dump_play
	movl	$1, %esi
	subl	%ebx, %esi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	main, .Lfunc_end21-main
	.cfi_endproc

	.type	ncol,@object            # @ncol
	.comm	ncol,4,4
	.type	nrow,@object            # @nrow
	.comm	nrow,4,4
	.type	game_tree,@object       # @game_tree
	.comm	game_tree,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d"
	.size	.L.str, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"  value = %d\n"
	.size	.L.str.3, 14

	.type	wanted,@object          # @wanted
	.comm	wanted,8,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" Selection : "
	.size	.L.str.8, 14

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Enter number of Columns : "
	.size	.L.str.13, 27

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Enter number of Rows    : "
	.size	.L.str.14, 27

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"player %d plays at (%d,%d)\n"
	.size	.L.str.15, 28

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"player %d loses\n"
	.size	.L.str.16, 17

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	")"
	.size	.Lstr, 2

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"For state :"
	.size	.Lstr.1, 12

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.2:
	.asciz	"We get, in order :"
	.size	.Lstr.2, 19

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"Mode : 1 -> multiple first moves"
	.size	.Lstr.3, 33

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"       2 -> report game"
	.size	.Lstr.4, 24

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"       3 -> good positions"
	.size	.Lstr.5, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
