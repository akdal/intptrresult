	.text
	.file	"rdbmp.bc"
	.globl	jinit_read_bmp
	.p2align	4, 0x90
	.type	jinit_read_bmp,@function
jinit_read_bmp:                         # @jinit_read_bmp
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$88, %edx
	callq	*(%rax)
	movq	%rbx, 48(%rax)
	movq	$start_input_bmp, (%rax)
	movq	$finish_input_bmp, 16(%rax)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	jinit_read_bmp, .Lfunc_end0-jinit_read_bmp
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_input_bmp,@function
start_input_bmp:                        # @start_input_bmp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 256
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	24(%r14), %rcx
	leaq	114(%rsp), %rdi
	movl	$1, %esi
	movl	$14, %edx
	callq	fread
	cmpq	$14, %rax
	je	.LBB1_2
# BB#1:
	movq	(%rbx), %rax
	movl	$42, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB1_2:
	movzbl	114(%rsp), %eax
	movzbl	115(%rsp), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	cmpl	$19778, %ecx            # imm = 0x4D42
	je	.LBB1_4
# BB#3:
	movq	(%rbx), %rax
	movl	$1007, 40(%rax)         # imm = 0x3EF
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB1_4:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movzbl	124(%rsp), %r12d
	movzbl	125(%rsp), %r15d
	shlq	$8, %r15
	movzbl	126(%rsp), %ebp
	movzbl	127(%rsp), %r13d
	movq	24(%r14), %rcx
	leaq	32(%rsp), %rdi
	movl	$1, %esi
	movl	$4, %edx
	callq	fread
	cmpq	$4, %rax
	je	.LBB1_6
# BB#5:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_6:
	orq	%r12, %r15
	shlq	$16, %rbp
	movzbl	32(%rsp), %eax
	movzbl	33(%rsp), %ecx
	shlq	$8, %rcx
	orq	%rax, %rcx
	movzbl	34(%rsp), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	movzbl	35(%rsp), %ecx
	shlq	$24, %rcx
	leaq	(%rax,%rcx), %rbx
	leaq	-12(%rcx,%rax), %rax
	cmpq	$53, %rax
	jb	.LBB1_8
# BB#7:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movl	$1003, 40(%rax)         # imm = 0x3EB
	callq	*(%rax)
.LBB1_8:
	orq	%rbp, %r15
	shlq	$24, %r13
	leaq	36(%rsp), %rdi
	leaq	-4(%rbx), %rbp
	movq	24(%r14), %rcx
	movl	$1, %esi
	movq	%rbp, %rdx
	callq	fread
	cmpq	%rbp, %rax
	je	.LBB1_10
# BB#9:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_10:
	orq	%r13, %r15
	cmpl	$64, %ebx
	je	.LBB1_16
# BB#11:
	cmpl	$40, %ebx
	je	.LBB1_16
# BB#12:
	cmpl	$12, %ebx
	movq	8(%rsp), %r13           # 8-byte Reload
	jne	.LBB1_19
# BB#13:
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movzbl	36(%rsp), %eax
	movzbl	37(%rsp), %ebp
	shll	$8, %ebp
	orl	%eax, %ebp
	movzbl	38(%rsp), %eax
	movzbl	39(%rsp), %ebx
	shll	$8, %ebx
	orl	%eax, %ebx
	movzbl	40(%rsp), %eax
	movzbl	41(%rsp), %r12d
	shll	$8, %r12d
	orl	%eax, %r12d
	movzbl	42(%rsp), %ecx
	movzbl	43(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movl	%eax, 80(%r14)
	cmpl	$24, %eax
	je	.LBB1_32
# BB#14:
	movzwl	%ax, %eax
	cmpl	$8, %eax
	jne	.LBB1_33
# BB#15:
	movq	(%r13), %rax
	movl	$1011, 40(%rax)         # imm = 0x3F3
	movl	%ebp, 44(%rax)
	movl	%ebx, 48(%rax)
	movl	$1, %esi
	movq	%r13, %rdi
	callq	*8(%rax)
	movl	$3, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_35
.LBB1_16:
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movzbl	36(%rsp), %eax
	movzbl	37(%rsp), %ecx
	shlq	$8, %rcx
	orq	%rax, %rcx
	movzbl	38(%rsp), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	movzbl	39(%rsp), %ecx
	shlq	$24, %rcx
	orq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movzbl	40(%rsp), %eax
	movzbl	41(%rsp), %ecx
	shlq	$8, %rcx
	orq	%rax, %rcx
	movzbl	42(%rsp), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	movzbl	43(%rsp), %ecx
	shlq	$24, %rcx
	orq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movzbl	44(%rsp), %eax
	movzbl	45(%rsp), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movl	%ecx, 108(%rsp)         # 4-byte Spill
	movzbl	46(%rsp), %ecx
	movzbl	47(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movl	%eax, 80(%r14)
	movzbl	48(%rsp), %ecx
	movzbl	49(%rsp), %edx
	shlq	$8, %rdx
	orq	%rcx, %rdx
	movzbl	50(%rsp), %ebp
	shlq	$16, %rbp
	orq	%rdx, %rbp
	movzbl	51(%rsp), %ecx
	shlq	$24, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movzbl	56(%rsp), %ecx
	movzbl	57(%rsp), %r13d
	shlq	$8, %r13
	orq	%rcx, %r13
	movzbl	58(%rsp), %ecx
	shlq	$16, %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movzbl	59(%rsp), %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movzbl	60(%rsp), %ecx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movzbl	61(%rsp), %ecx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movzbl	62(%rsp), %ecx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movzbl	63(%rsp), %ecx
	movzbl	64(%rsp), %edx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movzbl	65(%rsp), %r12d
	shlq	$8, %r12
	cmpl	$24, %eax
	movzbl	66(%rsp), %ebx
	movzbl	67(%rsp), %edx
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	je	.LBB1_20
# BB#17:
	movzwl	%ax, %eax
	cmpl	$8, %eax
	jne	.LBB1_21
# BB#18:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movl	$1009, 40(%rax)         # imm = 0x3F1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 48(%rax)
	movl	$1, %esi
	callq	*8(%rax)
	movl	$4, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_23
.LBB1_19:                               # %.thread
	movq	(%r13), %rax
	movl	$1003, 40(%rax)         # imm = 0x3EB
	movq	%r13, %rdi
	callq	*(%rax)
	addq	$-14, %r15
	subq	%rbx, %r15
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	testq	%r15, %r15
	jns	.LBB1_68
	jmp	.LBB1_73
.LBB1_20:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movl	$1008, 40(%rax)         # imm = 0x3F0
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 48(%rax)
	movl	$1, %esi
	callq	*8(%rax)
	jmp	.LBB1_22
.LBB1_21:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movl	$1002, 40(%rax)         # imm = 0x3EA
	callq	*(%rax)
.LBB1_22:
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
.LBB1_23:
	addq	168(%rsp), %r12         # 8-byte Folded Reload
	shlq	$16, %rbx
	addq	176(%rsp), %r13         # 8-byte Folded Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	shlq	$24, %rax
	addq	192(%rsp), %rbp         # 8-byte Folded Reload
	cmpl	$1, 108(%rsp)           # 4-byte Folded Reload
	je	.LBB1_25
# BB#24:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	(%rdi), %rax
	movl	$1004, 40(%rax)         # imm = 0x3EC
	callq	*(%rax)
	movq	96(%rsp), %rax          # 8-byte Reload
.LBB1_25:
	orq	%rbx, %r12
	movq	184(%rsp), %rbx         # 8-byte Reload
	shlq	$24, %rbx
	orq	%rax, %r13
	testq	%rbp, %rbp
	je	.LBB1_27
# BB#26:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movl	$1006, 40(%rax)         # imm = 0x3EE
	callq	*(%rax)
.LBB1_27:
	orq	%rbx, %r12
	testq	%r13, %r13
	movl	4(%rsp), %ebx           # 4-byte Reload
	je	.LBB1_31
# BB#28:
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	shlq	$8, %rcx
	addq	136(%rsp), %rcx         # 8-byte Folded Reload
	movq	152(%rsp), %rax         # 8-byte Reload
	shlq	$16, %rax
	orq	%rcx, %rax
	movq	160(%rsp), %rsi         # 8-byte Reload
	shlq	$24, %rsi
	orq	%rax, %rsi
	testq	%rsi, %rsi
	je	.LBB1_31
# BB#29:
	shrq	$2, %r13
	movabsq	$2951479051793528259, %rcx # imm = 0x28F5C28F5C28F5C3
	movq	%r13, %rax
	mulq	%rcx
	shrq	$2, %rdx
	movq	8(%rsp), %r13           # 8-byte Reload
	movw	%dx, 286(%r13)
	shrq	$2, %rsi
	movq	%rsi, %rax
	mulq	%rcx
	shrq	$2, %rdx
	movw	%dx, 288(%r13)
	movb	$2, 284(%r13)
	jmp	.LBB1_38
.LBB1_31:
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB1_38
.LBB1_32:
	movq	(%r13), %rax
	movl	$1010, 40(%rax)         # imm = 0x3F2
	movl	%ebp, 44(%rax)
	movl	%ebx, 48(%rax)
	movl	$1, %esi
	movq	%r13, %rdi
	callq	*8(%rax)
	jmp	.LBB1_34
.LBB1_33:
	movq	(%r13), %rax
	movl	$1002, 40(%rax)         # imm = 0x3EA
	movq	%r13, %rdi
	callq	*(%rax)
.LBB1_34:
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
.LBB1_35:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	cmpl	$1, %r12d
	je	.LBB1_37
# BB#36:
	movq	(%r13), %rax
	movl	$1004, 40(%rax)         # imm = 0x3EC
	movq	%r13, %rdi
	callq	*(%rax)
.LBB1_37:
	xorl	%r12d, %r12d
	movl	4(%rsp), %ebx           # 4-byte Reload
.LBB1_38:
	addq	$-14, %r15
	subq	128(%rsp), %r15         # 8-byte Folded Reload
	testl	%ebx, %ebx
	jle	.LBB1_67
# BB#39:
	testq	%r12, %r12
	jle	.LBB1_42
# BB#40:
	cmpq	$257, %r12              # imm = 0x101
	jl	.LBB1_43
# BB#41:
	movq	(%r13), %rax
	movl	$1001, 40(%rax)         # imm = 0x3E9
	movq	%r13, %rdi
	callq	*(%rax)
	jmp	.LBB1_43
.LBB1_42:
	movl	$256, %r12d             # imm = 0x100
.LBB1_43:
	movq	8(%r13), %rax
	movl	$1, %esi
	movl	$3, %ecx
	movq	%r13, %rdi
	movl	%r12d, %edx
	callq	*16(%rax)
	movq	%rax, 56(%r14)
	cmpl	$3, %ebx
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	je	.LBB1_56
# BB#44:
	cmpl	$4, %ebx
	jne	.LBB1_65
# BB#45:                                # %.preheader35.i
	testl	%r12d, %r12d
	jle	.LBB1_66
# BB#46:                                # %.lr.ph39.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_47:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_49
# BB#48:                                #   in Loop: Header=BB1_47 Depth=1
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_49:                               # %read_byte.exit31.i
                                        #   in Loop: Header=BB1_47 Depth=1
	movq	56(%r14), %rax
	movq	16(%rax), %rax
	movb	%bpl, (%rax,%rbx)
	movq	24(%r14), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_51
# BB#50:                                #   in Loop: Header=BB1_47 Depth=1
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_51:                               # %read_byte.exit32.i
                                        #   in Loop: Header=BB1_47 Depth=1
	movq	56(%r14), %rax
	movq	8(%rax), %rax
	movb	%bpl, (%rax,%rbx)
	movq	24(%r14), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_53
# BB#52:                                #   in Loop: Header=BB1_47 Depth=1
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_53:                               # %read_byte.exit33.i
                                        #   in Loop: Header=BB1_47 Depth=1
	movq	56(%r14), %rax
	movq	(%rax), %rax
	movb	%bpl, (%rax,%rbx)
	movq	24(%r14), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB1_55
# BB#54:                                #   in Loop: Header=BB1_47 Depth=1
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_55:                               # %read_byte.exit34.i
                                        #   in Loop: Header=BB1_47 Depth=1
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB1_47
	jmp	.LBB1_66
.LBB1_56:                               # %.preheader.i
	testl	%r12d, %r12d
	jle	.LBB1_66
# BB#57:                                # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_58:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_60
# BB#59:                                #   in Loop: Header=BB1_58 Depth=1
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_60:                               # %read_byte.exit.i
                                        #   in Loop: Header=BB1_58 Depth=1
	movq	56(%r14), %rax
	movq	16(%rax), %rax
	movb	%bpl, (%rax,%rbx)
	movq	24(%r14), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_62
# BB#61:                                #   in Loop: Header=BB1_58 Depth=1
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_62:                               # %read_byte.exit29.i
                                        #   in Loop: Header=BB1_58 Depth=1
	movq	56(%r14), %rax
	movq	8(%rax), %rax
	movb	%bpl, (%rax,%rbx)
	movq	24(%r14), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_64
# BB#63:                                #   in Loop: Header=BB1_58 Depth=1
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_64:                               # %read_byte.exit30.i
                                        #   in Loop: Header=BB1_58 Depth=1
	movq	56(%r14), %rax
	movq	(%rax), %rax
	movb	%bpl, (%rax,%rbx)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB1_58
	jmp	.LBB1_66
.LBB1_65:
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	movl	$1001, 40(%rax)         # imm = 0x3E9
	callq	*(%rax)
.LBB1_66:                               # %read_colormap.exit
	movslq	4(%rsp), %rax           # 4-byte Folded Reload
	imulq	%rax, %r12
	subq	%r12, %r15
.LBB1_67:
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	testq	%r15, %r15
	js	.LBB1_73
.LBB1_68:                               # %read_byte.exit.preheader
	je	.LBB1_74
# BB#69:                                # %.lr.ph
	incq	%r15
	.p2align	4, 0x90
.LBB1_70:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB1_72
# BB#71:                                #   in Loop: Header=BB1_70 Depth=1
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_72:                               # %read_byte.exit.backedge
                                        #   in Loop: Header=BB1_70 Depth=1
	decq	%r15
	cmpq	$1, %r15
	jg	.LBB1_70
	jmp	.LBB1_74
.LBB1_73:                               # %read_byte.exit.preheader.thread
	movq	(%r13), %rax
	movl	$1003, 40(%rax)         # imm = 0x3EB
	movq	%r13, %rdi
	callq	*(%rax)
.LBB1_74:                               # %read_byte.exit._crit_edge
	cmpl	$24, 80(%r14)
	leaq	(%rbx,%rbx,2), %rbp
	movl	%ebp, %ecx
	cmovnel	%ebx, %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB1_75:                               # =>This Inner Loop Header: Depth=1
	incl	%ecx
	testb	$3, %cl
	jne	.LBB1_75
# BB#76:
	movl	%ecx, 76(%r14)
	movq	8(%r13), %rax
	movl	$1, %esi
	xorl	%edx, %edx
	movl	$1, %r9d
	movq	%r13, %rdi
	movl	%r12d, %r8d
	callq	*32(%rax)
	movq	%rax, 64(%r14)
	movq	$preload_image, 8(%r14)
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.LBB1_78
# BB#77:
	incl	36(%rax)
.LBB1_78:
	movq	8(%r13), %rax
	movl	$1, %esi
	movl	$1, %ecx
	movq	%r13, %rdi
	movl	%ebp, %edx
	callq	*16(%rax)
	movq	%rax, 32(%r14)
	movl	$1, 40(%r14)
	movl	$2, 52(%r13)
	movl	$3, 48(%r13)
	movl	$8, 64(%r13)
	movl	%ebx, 40(%r13)
	movl	%r12d, 44(%r13)
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_input_bmp, .Lfunc_end1-start_input_bmp
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_input_bmp,@function
finish_input_bmp:                       # @finish_input_bmp
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	finish_input_bmp, .Lfunc_end2-finish_input_bmp
	.cfi_endproc

	.p2align	4, 0x90
	.type	preload_image,@function
preload_image:                          # @preload_image
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 80
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	16(%r15), %rcx
	movl	44(%r15), %eax
	testl	%eax, %eax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	je	.LBB3_1
# BB#2:                                 # %.lr.ph60
	movq	24(%rbx), %r13
	testq	%rcx, %rcx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB3_3
# BB#10:                                # %.lr.ph60.split.us.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph60.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_13 Depth 2
	movl	%r12d, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rcx, 8(%rdx)
	movl	%eax, %eax
	movq	%rax, 16(%rdx)
	movq	%r15, %rdi
	callq	*(%rdx)
	movq	8(%r15), %rax
	movq	64(%rbx), %rsi
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	movl	%r12d, %edx
	callq	*56(%rax)
	movl	76(%rbx), %r14d
	testl	%r14d, %r14d
	je	.LBB3_16
# BB#12:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB3_11 Depth=1
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph.us
                                        #   Parent Loop BB3_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_13 Depth=2
	movq	(%r15), %rax
	movl	$42, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB3_15:                               #   in Loop: Header=BB3_13 Depth=2
	movb	%bpl, (%rbx)
	incq	%rbx
	decl	%r14d
	jne	.LBB3_13
.LBB3_16:                               # %._crit_edge.us
                                        #   in Loop: Header=BB3_11 Depth=1
	incl	%r12d
	movl	44(%r15), %eax
	cmpl	%eax, %r12d
	movq	16(%rsp), %rbx          # 8-byte Reload
	jb	.LBB3_11
	jmp	.LBB3_17
.LBB3_1:
	xorl	%eax, %eax
	jmp	.LBB3_17
.LBB3_3:                                # %.lr.ph60.split.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph60.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_6 Depth 2
	movq	8(%r15), %rax
	movq	64(%rbx), %rsi
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	movl	%r12d, %edx
	callq	*56(%rax)
	movl	76(%rbx), %ebx
	testl	%ebx, %ebx
	je	.LBB3_9
# BB#5:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	(%rax), %rbp
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$-1, %r14d
	jne	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_6 Depth=2
	movq	(%r15), %rax
	movl	$42, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB3_8:                                #   in Loop: Header=BB3_6 Depth=2
	movb	%r14b, (%rbp)
	incq	%rbp
	decl	%ebx
	jne	.LBB3_6
.LBB3_9:                                # %._crit_edge
                                        #   in Loop: Header=BB3_4 Depth=1
	incl	%r12d
	movl	44(%r15), %eax
	cmpl	%eax, %r12d
	movq	16(%rsp), %rbx          # 8-byte Reload
	jb	.LBB3_4
.LBB3_17:                               # %._crit_edge61
	movq	8(%rsp), %rcx           # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB3_19
# BB#18:
	incl	32(%rcx)
.LBB3_19:
	movl	80(%rbx), %ecx
	cmpl	$24, %ecx
	je	.LBB3_22
# BB#20:
	cmpl	$8, %ecx
	jne	.LBB3_23
# BB#21:
	movq	$get_8bit_row, 8(%rbx)
	movl	$get_8bit_row, %ecx
	jmp	.LBB3_24
.LBB3_22:
	movq	$get_24bit_row, 8(%rbx)
	movl	$get_24bit_row, %ecx
	jmp	.LBB3_24
.LBB3_23:
	movq	(%r15), %rax
	movl	$1002, 40(%rax)         # imm = 0x3EA
	movq	%r15, %rdi
	callq	*(%rax)
	movl	44(%r15), %eax
	movq	8(%rbx), %rcx
.LBB3_24:
	movl	%eax, 72(%rbx)
	movq	%r15, %rdi
	movq	%rbx, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rcx                   # TAILCALL
.Lfunc_end3:
	.size	preload_image, .Lfunc_end3-preload_image
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_8bit_row,@function
get_8bit_row:                           # @get_8bit_row
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -40
.Lcfi34:
	.cfi_offset %r12, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	56(%r15), %r12
	movl	72(%r15), %edx
	decl	%edx
	movl	%edx, 72(%r15)
	movq	8(%r14), %rax
	movq	64(%r15), %rsi
	movl	$1, %ecx
	xorl	%r8d, %r8d
	callq	*56(%rax)
	movl	40(%r14), %esi
	testl	%esi, %esi
	je	.LBB4_6
# BB#1:                                 # %.lr.ph
	movq	32(%r15), %rcx
	movq	(%rcx), %rcx
	movq	(%rax), %rax
	testb	$1, %sil
	jne	.LBB4_3
# BB#2:
	movl	%esi, %edx
	cmpl	$1, %esi
	jne	.LBB4_5
	jmp	.LBB4_6
.LBB4_3:
	movq	(%r12), %rdx
	movzbl	(%rax), %edi
	incq	%rax
	movb	(%rdx,%rdi), %dl
	movb	%dl, (%rcx)
	movq	8(%r12), %rdx
	movb	(%rdx,%rdi), %dl
	movb	%dl, 1(%rcx)
	movq	16(%r12), %rdx
	movb	(%rdx,%rdi), %dl
	movb	%dl, 2(%rcx)
	addq	$3, %rcx
	leal	-1(%rsi), %edx
	cmpl	$1, %esi
	je	.LBB4_6
	.p2align	4, 0x90
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rsi
	movzbl	(%rax), %edi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, (%rcx)
	movq	8(%r12), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 1(%rcx)
	movq	16(%r12), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 2(%rcx)
	movq	(%r12), %rsi
	movzbl	1(%rax), %edi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 3(%rcx)
	movq	8(%r12), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 4(%rcx)
	movq	16(%r12), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, 5(%rcx)
	addq	$2, %rax
	addq	$6, %rcx
	addl	$-2, %edx
	jne	.LBB4_5
.LBB4_6:                                # %._crit_edge
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	get_8bit_row, .Lfunc_end4-get_8bit_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_24bit_row,@function
get_24bit_row:                          # @get_24bit_row
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	72(%r14), %edx
	decl	%edx
	movl	%edx, 72(%r14)
	movq	8(%rbx), %rax
	movq	64(%r14), %rsi
	movl	$1, %ecx
	xorl	%r8d, %r8d
	callq	*56(%rax)
	movl	40(%rbx), %esi
	testl	%esi, %esi
	je	.LBB5_7
# BB#1:                                 # %.lr.ph.preheader
	movq	32(%r14), %rcx
	movq	(%rcx), %rcx
	movq	(%rax), %rax
	testb	$1, %sil
	jne	.LBB5_3
# BB#2:
	movl	%esi, %edx
	cmpl	$1, %esi
	jne	.LBB5_5
	jmp	.LBB5_7
.LBB5_3:                                # %.lr.ph.prol
	movb	(%rax), %dl
	movb	%dl, 2(%rcx)
	movb	1(%rax), %dl
	movb	%dl, 1(%rcx)
	movb	2(%rax), %dl
	addq	$3, %rax
	movb	%dl, (%rcx)
	addq	$3, %rcx
	leal	-1(%rsi), %edx
	cmpl	$1, %esi
	je	.LBB5_7
.LBB5_5:                                # %.lr.ph.preheader.new
	addq	$5, %rcx
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ebx
	movb	%bl, -3(%rcx)
	movzbl	1(%rax), %ebx
	movb	%bl, -4(%rcx)
	movzbl	2(%rax), %ebx
	movb	%bl, -5(%rcx)
	movzbl	3(%rax), %ebx
	movb	%bl, (%rcx)
	movzbl	4(%rax), %ebx
	movb	%bl, -1(%rcx)
	movzbl	5(%rax), %ebx
	movb	%bl, -2(%rcx)
	addq	$6, %rcx
	addq	$6, %rax
	addl	$-2, %edx
	jne	.LBB5_6
.LBB5_7:                                # %._crit_edge
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	get_24bit_row, .Lfunc_end5-get_24bit_row
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
