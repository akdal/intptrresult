	.text
	.file	"jcmarker.bc"
	.globl	jinit_marker_writer
	.p2align	4, 0x90
	.type	jinit_marker_writer,@function
jinit_marker_writer:                    # @jinit_marker_writer
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$48, %edx
	callq	*(%rax)
	movq	%rax, 456(%rbx)
	movq	$write_any_marker, (%rax)
	movl	$write_frame_header, %ecx
	movd	%rcx, %xmm0
	movl	$write_file_header, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 8(%rax)
	movl	$write_file_trailer, %ecx
	movd	%rcx, %xmm0
	movl	$write_scan_header, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 24(%rax)
	movq	$write_tables_only, 40(%rax)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	jinit_marker_writer, .Lfunc_end0-jinit_marker_writer
	.cfi_endproc

	.p2align	4, 0x90
	.type	write_any_marker,@function
write_any_marker:                       # @write_any_marker
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movq	%rdi, %r15
	cmpl	$65533, %r14d           # imm = 0xFFFD
	ja	.LBB1_18
# BB#1:
	movq	32(%r15), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB1_4
# BB#2:
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB1_4
# BB#3:
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB1_4:                                # %emit_byte.exit.i
	movq	32(%r15), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bpl, (%rcx)
	decq	8(%rax)
	jne	.LBB1_7
# BB#5:
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB1_7
# BB#6:
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB1_7:                                # %emit_marker.exit
	leal	2(%r14), %esi
	movq	32(%r15), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	%esi, %edx
	movl	%edx, %ebp
	movb	%dh, (%rcx)  # NOREX
	decq	8(%rax)
	jne	.LBB1_10
# BB#8:
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB1_10
# BB#9:
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB1_10:                               # %emit_byte.exit.i9
	movq	32(%r15), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bpl, (%rcx)
	decq	8(%rax)
	jne	.LBB1_13
# BB#11:
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB1_13
# BB#12:
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	testl	%r14d, %r14d
	jne	.LBB1_14
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_17:                               # %emit_byte.exit
	incq	%rbx
.LBB1_13:                               # %emit_2bytes.exit.preheader
	testl	%r14d, %r14d
	je	.LBB1_18
.LBB1_14:
	decl	%r14d
	movb	(%rbx), %cl
	movq	32(%r15), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB1_17
# BB#15:
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB1_17
# BB#16:
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	jmp	.LBB1_17
.LBB1_18:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	write_any_marker, .Lfunc_end1-write_any_marker
	.cfi_endproc

	.p2align	4, 0x90
	.type	write_file_header,@function
write_file_header:                      # @write_file_header
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB2_3
# BB#1:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_3
# BB#2:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_3:                                # %emit_byte.exit.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-40, (%rcx)
	decq	8(%rax)
	jne	.LBB2_6
# BB#4:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_6
# BB#5:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_6:                                # %emit_marker.exit
	cmpl	$0, 280(%rbp)
	je	.LBB2_61
# BB#7:
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB2_10
# BB#8:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_10
# BB#9:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_10:                               # %emit_byte.exit.i.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-32, (%rcx)
	decq	8(%rax)
	jne	.LBB2_13
# BB#11:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_13
# BB#12:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_13:                               # %emit_marker.exit.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_16
# BB#14:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_16
# BB#15:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_16:                               # %emit_byte.exit.i17.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$16, (%rcx)
	decq	8(%rax)
	jne	.LBB2_19
# BB#17:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_19
# BB#18:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_19:                               # %emit_2bytes.exit.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$74, (%rcx)
	decq	8(%rax)
	jne	.LBB2_22
# BB#20:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_22
# BB#21:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_22:                               # %emit_byte.exit.i5
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$70, (%rcx)
	decq	8(%rax)
	jne	.LBB2_25
# BB#23:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_25
# BB#24:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_25:                               # %emit_byte.exit18.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$73, (%rcx)
	decq	8(%rax)
	jne	.LBB2_28
# BB#26:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_28
# BB#27:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_28:                               # %emit_byte.exit19.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$70, (%rcx)
	decq	8(%rax)
	jne	.LBB2_31
# BB#29:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_31
# BB#30:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_31:                               # %emit_byte.exit20.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_34
# BB#32:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_34
# BB#33:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_34:                               # %emit_byte.exit21.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$1, (%rcx)
	decq	8(%rax)
	jne	.LBB2_37
# BB#35:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_37
# BB#36:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_37:                               # %emit_byte.exit22.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$1, (%rcx)
	decq	8(%rax)
	jne	.LBB2_40
# BB#38:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_40
# BB#39:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_40:                               # %emit_byte.exit23.i
	movb	284(%rbp), %cl
	movq	32(%rbp), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB2_43
# BB#41:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_43
# BB#42:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_43:                               # %emit_byte.exit24.i
	movzwl	286(%rbp), %ebx
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	%ebx, %r14d
	movb	%bh, (%rcx)  # NOREX
	decq	8(%rax)
	jne	.LBB2_46
# BB#44:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_46
# BB#45:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_46:                               # %emit_byte.exit.i25.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%r14b, (%rcx)
	decq	8(%rax)
	jne	.LBB2_49
# BB#47:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_49
# BB#48:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_49:                               # %emit_2bytes.exit26.i
	movzwl	288(%rbp), %ebx
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bh, (%rcx)  # NOREX
	decq	8(%rax)
	jne	.LBB2_52
# BB#50:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_52
# BB#51:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_52:                               # %emit_byte.exit.i27.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bl, (%rcx)
	decq	8(%rax)
	jne	.LBB2_55
# BB#53:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_55
# BB#54:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_55:                               # %emit_2bytes.exit28.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_58
# BB#56:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_58
# BB#57:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_58:                               # %emit_byte.exit29.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_61
# BB#59:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_61
# BB#60:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_61:                               # %emit_jfif_app0.exit
	cmpl	$0, 292(%rbp)
	je	.LBB2_115
# BB#62:
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB2_65
# BB#63:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_65
# BB#64:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_65:                               # %emit_byte.exit.i.i6
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-18, (%rcx)
	decq	8(%rax)
	jne	.LBB2_68
# BB#66:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_68
# BB#67:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_68:                               # %emit_marker.exit.i7
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_71
# BB#69:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_71
# BB#70:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_71:                               # %emit_byte.exit.i14.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$14, (%rcx)
	decq	8(%rax)
	jne	.LBB2_74
# BB#72:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_74
# BB#73:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_74:                               # %emit_2bytes.exit.i8
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$65, (%rcx)
	decq	8(%rax)
	jne	.LBB2_77
# BB#75:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_77
# BB#76:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_77:                               # %emit_byte.exit.i9
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$100, (%rcx)
	decq	8(%rax)
	jne	.LBB2_80
# BB#78:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_80
# BB#79:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_80:                               # %emit_byte.exit15.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$111, (%rcx)
	decq	8(%rax)
	jne	.LBB2_83
# BB#81:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_83
# BB#82:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_83:                               # %emit_byte.exit16.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$98, (%rcx)
	decq	8(%rax)
	jne	.LBB2_86
# BB#84:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_86
# BB#85:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_86:                               # %emit_byte.exit17.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$101, (%rcx)
	decq	8(%rax)
	jne	.LBB2_89
# BB#87:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_89
# BB#88:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_89:                               # %emit_byte.exit18.i10
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_92
# BB#90:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_92
# BB#91:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_92:                               # %emit_byte.exit.i19.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$100, (%rcx)
	decq	8(%rax)
	jne	.LBB2_95
# BB#93:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_95
# BB#94:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_95:                               # %emit_2bytes.exit20.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_98
# BB#96:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_98
# BB#97:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_98:                               # %emit_byte.exit.i21.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_101
# BB#99:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_101
# BB#100:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_101:                              # %emit_2bytes.exit22.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_104
# BB#102:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_104
# BB#103:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_104:                              # %emit_byte.exit.i23.i
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_107
# BB#105:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB2_107
# BB#106:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB2_107:                              # %emit_2bytes.exit24.i
	movl	72(%rbp), %eax
	cmpl	$5, %eax
	je	.LBB2_113
# BB#108:                               # %emit_2bytes.exit24.i
	cmpl	$3, %eax
	jne	.LBB2_114
# BB#109:
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$1, (%rcx)
	decq	8(%rax)
	jne	.LBB2_115
	jmp	.LBB2_111
.LBB2_113:
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$2, (%rcx)
	decq	8(%rax)
	jne	.LBB2_115
	jmp	.LBB2_111
.LBB2_114:
	movq	32(%rbp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB2_115
.LBB2_111:
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB2_112
.LBB2_115:                              # %emit_adobe_app14.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB2_112:
	movq	(%rbp), %rax
	movl	$22, 40(%rax)
	movq	%rbp, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end2:
	.size	write_file_header, .Lfunc_end2-write_file_header
	.cfi_endproc

	.p2align	4, 0x90
	.type	write_frame_header,@function
write_frame_header:                     # @write_frame_header
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 48
.Lcfi22:
	.cfi_offset %rbx, -48
.Lcfi23:
	.cfi_offset %r12, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	68(%r15), %eax
	testl	%eax, %eax
	jle	.LBB3_1
# BB#2:                                 # %.lr.ph46.preheader
	movq	80(%r15), %rbx
	addq	$16, %rbx
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph46
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %esi
	movq	%r15, %rdi
	callq	emit_dqt
	addl	%eax, %r12d
	incl	%ebp
	movl	68(%r15), %eax
	addq	$96, %rbx
	cmpl	%eax, %ebp
	jl	.LBB3_3
	jmp	.LBB3_4
.LBB3_1:
	xorl	%r12d, %r12d
.LBB3_4:                                # %._crit_edge47
	cmpl	$0, 252(%r15)
	je	.LBB3_5
.LBB3_12:                               # %.thread
	movl	$201, %esi
	jmp	.LBB3_13
.LBB3_5:
	cmpl	$0, 300(%r15)
	jne	.LBB3_30
# BB#6:
	xorl	%r14d, %r14d
	cmpl	$8, 64(%r15)
	jne	.LBB3_29
# BB#7:
	testl	%eax, %eax
	jle	.LBB3_8
# BB#14:                                # %.lr.ph
	movq	80(%r15), %rcx
	testb	$1, %al
	jne	.LBB3_16
# BB#15:
	xorl	%edx, %edx
	movl	$1, %r14d
	cmpl	$1, %eax
	jne	.LBB3_21
	jmp	.LBB3_9
.LBB3_8:
	movl	$1, %r14d
	testl	%r12d, %r12d
	jne	.LBB3_10
	jmp	.LBB3_29
.LBB3_16:
	cmpl	$1, 20(%rcx)
	jg	.LBB3_18
# BB#17:
	movl	$1, %r14d
	cmpl	$2, 24(%rcx)
	jl	.LBB3_19
.LBB3_18:
	xorl	%r14d, %r14d
.LBB3_19:
	addq	$96, %rcx
	movl	$1, %edx
	cmpl	$1, %eax
	je	.LBB3_9
.LBB3_21:                               # %.lr.ph.new
	addq	$120, %rcx
	.p2align	4, 0x90
.LBB3_22:                               # =>This Inner Loop Header: Depth=1
	cmpl	$1, -100(%rcx)
	jg	.LBB3_24
# BB#23:                                #   in Loop: Header=BB3_22 Depth=1
	cmpl	$2, -96(%rcx)
	jl	.LBB3_25
.LBB3_24:                               #   in Loop: Header=BB3_22 Depth=1
	xorl	%r14d, %r14d
.LBB3_25:                               #   in Loop: Header=BB3_22 Depth=1
	cmpl	$1, -4(%rcx)
	jg	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_22 Depth=1
	cmpl	$2, (%rcx)
	jl	.LBB3_28
.LBB3_27:                               #   in Loop: Header=BB3_22 Depth=1
	xorl	%r14d, %r14d
.LBB3_28:                               #   in Loop: Header=BB3_22 Depth=1
	addl	$2, %edx
	addq	$192, %rcx
	cmpl	%eax, %edx
	jl	.LBB3_22
.LBB3_9:                                # %._crit_edge
	testl	%r12d, %r12d
	je	.LBB3_29
.LBB3_10:                               # %._crit_edge
	testl	%r14d, %r14d
	je	.LBB3_29
# BB#11:
	movq	(%r15), %rax
	movl	$74, 40(%rax)
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	cmpl	$0, 252(%r15)
	jne	.LBB3_12
.LBB3_29:                               # %.thread51
	cmpl	$0, 300(%r15)
	je	.LBB3_31
.LBB3_30:                               # %.thread51.thread
	movl	$194, %esi
.LBB3_13:                               # %.thread
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	emit_sof                # TAILCALL
.LBB3_31:
	testl	%r14d, %r14d
	je	.LBB3_33
# BB#32:
	movl	$192, %esi
	jmp	.LBB3_13
.LBB3_33:
	movl	$193, %esi
	jmp	.LBB3_13
.Lfunc_end3:
	.size	write_frame_header, .Lfunc_end3-write_frame_header
	.cfi_endproc

	.p2align	4, 0x90
	.type	write_scan_header,@function
write_scan_header:                      # @write_scan_header
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpl	$0, 252(%r14)
	jne	.LBB4_11
# BB#1:                                 # %.preheader
	cmpl	$0, 316(%r14)
	jle	.LBB4_11
# BB#2:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movq	320(%r14,%rbx,8), %r15
	cmpl	$0, 300(%r14)
	je	.LBB4_7
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, 404(%r14)
	jne	.LBB4_8
# BB#5:                                 #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, 412(%r14)
	jne	.LBB4_10
# BB#6:                                 #   in Loop: Header=BB4_3 Depth=1
	movl	20(%r15), %esi
	xorl	%edx, %edx
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_3 Depth=1
	movl	20(%r15), %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	emit_dht
.LBB4_8:                                #   in Loop: Header=BB4_3 Depth=1
	movl	24(%r15), %esi
	movl	$1, %edx
.LBB4_9:                                #   in Loop: Header=BB4_3 Depth=1
	movq	%r14, %rdi
	callq	emit_dht
.LBB4_10:                               #   in Loop: Header=BB4_3 Depth=1
	incq	%rbx
	movslq	316(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB4_3
.LBB4_11:                               # %.loopexit
	cmpl	$0, 272(%r14)
	je	.LBB4_30
# BB#12:
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB4_15
# BB#13:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_15
# BB#14:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_15:                               # %emit_byte.exit.i.i21
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-35, (%rcx)
	decq	8(%rax)
	jne	.LBB4_18
# BB#16:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_18
# BB#17:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_18:                               # %emit_marker.exit.i22
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB4_21
# BB#19:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_21
# BB#20:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_21:                               # %emit_byte.exit.i4.i
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$4, (%rcx)
	decq	8(%rax)
	jne	.LBB4_24
# BB#22:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_24
# BB#23:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_24:                               # %emit_2bytes.exit.i23
	movl	272(%r14), %ebx
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bh, (%rcx)  # NOREX
	decq	8(%rax)
	jne	.LBB4_27
# BB#25:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_27
# BB#26:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_27:                               # %emit_byte.exit.i5.i
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bl, (%rcx)
	decq	8(%rax)
	jne	.LBB4_30
# BB#28:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_30
# BB#29:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_30:                               # %emit_dri.exit
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB4_33
# BB#31:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_33
# BB#32:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_33:                               # %emit_byte.exit.i.i
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-38, (%rcx)
	decq	8(%rax)
	jne	.LBB4_36
# BB#34:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_36
# BB#35:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_36:                               # %emit_marker.exit.i
	movl	316(%r14), %eax
	leal	6(%rax,%rax), %ebx
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bh, (%rcx)  # NOREX
	decq	8(%rax)
	jne	.LBB4_39
# BB#37:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_39
# BB#38:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_39:                               # %emit_byte.exit.i35.i
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bl, (%rcx)
	decq	8(%rax)
	jne	.LBB4_42
# BB#40:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_42
# BB#41:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_42:                               # %emit_2bytes.exit.i
	movb	316(%r14), %cl
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB4_45
# BB#43:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_45
# BB#44:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_45:                               # %emit_byte.exit.preheader.i
	cmpl	$0, 316(%r14)
	jle	.LBB4_59
# BB#46:                                # %.lr.ph.i
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_47:                               # =>This Inner Loop Header: Depth=1
	movq	320(%r14,%r15,8), %rbx
	movzbl	(%rbx), %ecx
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB4_50
# BB#48:                                #   in Loop: Header=BB4_47 Depth=1
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_50
# BB#49:                                #   in Loop: Header=BB4_47 Depth=1
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB4_50:                               # %emit_byte.exit36.i
                                        #   in Loop: Header=BB4_47 Depth=1
	movl	20(%rbx), %edx
	movl	24(%rbx), %eax
	cmpl	$0, 300(%r14)
	je	.LBB4_54
# BB#51:                                #   in Loop: Header=BB4_47 Depth=1
	xorl	%ecx, %ecx
	cmpl	$0, 404(%r14)
	jne	.LBB4_55
# BB#52:                                #   in Loop: Header=BB4_47 Depth=1
	xorl	%eax, %eax
	cmpl	$0, 412(%r14)
	je	.LBB4_54
# BB#53:                                #   in Loop: Header=BB4_47 Depth=1
	movl	252(%r14), %ecx
	testl	%ecx, %ecx
	cmovel	%ecx, %edx
.LBB4_54:                               #   in Loop: Header=BB4_47 Depth=1
	movl	%edx, %ecx
.LBB4_55:                               #   in Loop: Header=BB4_47 Depth=1
	shll	$4, %ecx
	addl	%eax, %ecx
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB4_58
# BB#56:                                #   in Loop: Header=BB4_47 Depth=1
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_58
# BB#57:                                #   in Loop: Header=BB4_47 Depth=1
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB4_58:                               # %emit_byte.exit37.i
                                        #   in Loop: Header=BB4_47 Depth=1
	incq	%r15
	movslq	316(%r14), %rax
	cmpq	%rax, %r15
	jl	.LBB4_47
.LBB4_59:                               # %emit_byte.exit._crit_edge.i
	movb	404(%r14), %cl
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB4_62
# BB#60:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_62
# BB#61:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_62:                               # %emit_byte.exit38.i
	movb	408(%r14), %cl
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB4_65
# BB#63:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_65
# BB#64:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB4_65:                               # %emit_byte.exit39.i
	movl	412(%r14), %ecx
	shll	$4, %ecx
	addl	416(%r14), %ecx
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB4_67
# BB#66:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB4_68
.LBB4_67:                               # %emit_sos.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_68:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end4:
	.size	write_scan_header, .Lfunc_end4-write_scan_header
	.cfi_endproc

	.p2align	4, 0x90
	.type	write_file_trailer,@function
write_file_trailer:                     # @write_file_trailer
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
.Lcfi34:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB5_3
# BB#1:
	movq	%rbx, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB5_3
# BB#2:
	movq	(%rbx), %rax
	movl	$22, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB5_3:                                # %emit_byte.exit.i
	movq	32(%rbx), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-39, (%rcx)
	decq	8(%rax)
	jne	.LBB5_5
# BB#4:
	movq	%rbx, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_6
.LBB5_5:                                # %emit_marker.exit
	popq	%rbx
	retq
.LBB5_6:
	movq	(%rbx), %rax
	movl	$22, 40(%rax)
	movq	%rbx, %rdi
	popq	%rbx
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end5:
	.size	write_file_trailer, .Lfunc_end5-write_file_trailer
	.cfi_endproc

	.p2align	4, 0x90
	.type	write_tables_only,@function
write_tables_only:                      # @write_tables_only
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
.Lcfi36:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB6_3
# BB#1:
	movq	%rbx, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB6_3
# BB#2:
	movq	(%rbx), %rax
	movl	$22, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB6_3:                                # %emit_byte.exit.i
	movq	32(%rbx), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-40, (%rcx)
	decq	8(%rax)
	jne	.LBB6_6
# BB#4:
	movq	%rbx, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB6_6
# BB#5:
	movq	(%rbx), %rax
	movl	$22, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB6_6:                                # %emit_marker.exit.preheader.preheader
	cmpq	$0, 88(%rbx)
	je	.LBB6_8
# BB#7:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	emit_dqt
.LBB6_8:                                # %emit_marker.exit
	cmpq	$0, 96(%rbx)
	je	.LBB6_10
# BB#9:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	emit_dqt
.LBB6_10:                               # %emit_marker.exit.1
	cmpq	$0, 104(%rbx)
	je	.LBB6_12
# BB#11:
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	emit_dqt
.LBB6_12:                               # %emit_marker.exit.2
	cmpq	$0, 112(%rbx)
	je	.LBB6_14
# BB#13:
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	emit_dqt
.LBB6_14:                               # %emit_marker.exit.3
	cmpl	$0, 252(%rbx)
	jne	.LBB6_30
# BB#15:                                # %.preheader.preheader
	cmpq	$0, 120(%rbx)
	je	.LBB6_17
# BB#16:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	emit_dht
.LBB6_17:
	cmpq	$0, 152(%rbx)
	je	.LBB6_19
# BB#18:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	emit_dht
.LBB6_19:                               # %.preheader.122
	cmpq	$0, 128(%rbx)
	je	.LBB6_21
# BB#20:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	emit_dht
.LBB6_21:
	cmpq	$0, 160(%rbx)
	je	.LBB6_23
# BB#22:
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	emit_dht
.LBB6_23:                               # %.preheader.223
	cmpq	$0, 136(%rbx)
	je	.LBB6_25
# BB#24:
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	emit_dht
.LBB6_25:
	cmpq	$0, 168(%rbx)
	je	.LBB6_27
# BB#26:
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	emit_dht
.LBB6_27:                               # %.preheader.324
	cmpq	$0, 144(%rbx)
	je	.LBB6_29
# BB#28:
	movl	$3, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	emit_dht
.LBB6_29:
	cmpq	$0, 176(%rbx)
	je	.LBB6_30
# BB#36:
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	emit_dht
.LBB6_30:                               # %.loopexit
	movq	32(%rbx), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB6_33
# BB#31:
	movq	%rbx, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB6_33
# BB#32:
	movq	(%rbx), %rax
	movl	$22, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB6_33:                               # %emit_byte.exit.i18
	movq	32(%rbx), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-39, (%rcx)
	decq	8(%rax)
	jne	.LBB6_35
# BB#34:
	movq	%rbx, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB6_37
.LBB6_35:                               # %emit_marker.exit19
	popq	%rbx
	retq
.LBB6_37:
	movq	(%rbx), %rax
	movl	$22, 40(%rax)
	movq	%rbx, %rdi
	popq	%rbx
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end6:
	.size	write_tables_only, .Lfunc_end6-write_tables_only
	.cfi_endproc

	.p2align	4, 0x90
	.type	emit_dqt,@function
emit_dqt:                               # @emit_dqt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 64
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movslq	%r14d, %rax
	movq	88(%r15,%rax,8), %r12
	testq	%r12, %r12
	jne	.LBB7_2
# BB#1:
	movq	(%r15), %rax
	movl	$51, 40(%rax)
	movl	%r14d, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB7_2:                                # %.preheader.preheader
	xorl	%eax, %eax
	movl	$1, %ecx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%r12,%rax,2), %edx
	orw	(%r12,%rax,2), %dx
	orw	4(%r12,%rax,2), %dx
	orw	6(%r12,%rax,2), %dx
	movzwl	%dx, %edx
	cmpl	$255, %edx
	cmoval	%ecx, %ebp
	addq	$4, %rax
	cmpq	$64, %rax
	jne	.LBB7_3
# BB#4:
	cmpl	$0, 128(%r12)
	jne	.LBB7_37
# BB#5:
	movq	32(%r15), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB7_8
# BB#6:
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB7_8
# BB#7:
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB7_8:                                # %emit_byte.exit.i
	movq	32(%r15), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-37, (%rcx)
	decq	8(%rax)
	jne	.LBB7_11
# BB#9:
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB7_11
# BB#10:
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB7_11:                               # %emit_marker.exit
	movq	32(%r15), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx)
	decq	8(%rax)
	jne	.LBB7_14
# BB#12:
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB7_14
# BB#13:
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB7_14:                               # %emit_byte.exit.i37
	testl	%ebp, %ebp
	movq	32(%r15), %rax
	movb	$-125, %cl
	jne	.LBB7_16
# BB#15:                                # %emit_byte.exit.i37
	movb	$67, %cl
.LBB7_16:                               # %emit_byte.exit.i37
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB7_19
# BB#17:
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB7_19
# BB#18:
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB7_19:                               # %emit_2bytes.exit
	movl	%ebp, %ecx
	shll	$4, %ecx
	addl	%r14d, %ecx
	movq	32(%r15), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB7_22
# BB#20:
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB7_22
# BB#21:
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB7_22:                               # %emit_byte.exit.preheader
	testl	%ebp, %ebp
	je	.LBB7_23
# BB#28:                                # %emit_byte.exit.preheader.split.us.preheader
	movq	$-256, %r13
	.p2align	4, 0x90
.LBB7_29:                               # %emit_byte.exit.preheader.split.us
                                        # =>This Inner Loop Header: Depth=1
	movslq	jpeg_natural_order+256(%r13), %rax
	movzwl	(%r12,%rax,2), %ebx
	movq	32(%r15), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	%ebx, %r14d
	movb	%bh, (%rcx)  # NOREX
	decq	8(%rax)
	jne	.LBB7_32
# BB#30:                                #   in Loop: Header=BB7_29 Depth=1
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB7_32
# BB#31:                                #   in Loop: Header=BB7_29 Depth=1
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB7_32:                               # %emit_byte.exit38.us
                                        #   in Loop: Header=BB7_29 Depth=1
	movq	32(%r15), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%r14b, (%rcx)
	decq	8(%rax)
	jne	.LBB7_35
# BB#33:                                #   in Loop: Header=BB7_29 Depth=1
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB7_35
# BB#34:                                #   in Loop: Header=BB7_29 Depth=1
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB7_35:                               # %emit_byte.exit39.us
                                        #   in Loop: Header=BB7_29 Depth=1
	addq	$4, %r13
	jne	.LBB7_29
	jmp	.LBB7_36
.LBB7_23:                               # %emit_byte.exit.preheader.split.preheader
	movq	$-256, %rbx
	.p2align	4, 0x90
.LBB7_24:                               # %emit_byte.exit.preheader.split
                                        # =>This Inner Loop Header: Depth=1
	movslq	jpeg_natural_order+256(%rbx), %rax
	movzbl	(%r12,%rax,2), %ecx
	movq	32(%r15), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB7_27
# BB#25:                                #   in Loop: Header=BB7_24 Depth=1
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB7_27
# BB#26:                                #   in Loop: Header=BB7_24 Depth=1
	movq	(%r15), %rax
	movl	$22, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB7_27:                               # %emit_byte.exit39
                                        #   in Loop: Header=BB7_24 Depth=1
	addq	$4, %rbx
	jne	.LBB7_24
.LBB7_36:                               # %.us-lcssa.us
	movl	$1, 128(%r12)
.LBB7_37:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	emit_dqt, .Lfunc_end7-emit_dqt
	.cfi_endproc

	.p2align	4, 0x90
	.type	emit_sof,@function
emit_sof:                               # @emit_sof
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB8_3
# BB#1:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_3
# BB#2:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_3:                                # %emit_byte.exit.i
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bpl, (%rcx)
	decq	8(%rax)
	jne	.LBB8_6
# BB#4:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_6
# BB#5:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_6:                                # %emit_marker.exit
	movl	68(%r14), %eax
	leal	8(%rax,%rax,2), %ebx
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bh, (%rcx)  # NOREX
	decq	8(%rax)
	jne	.LBB8_9
# BB#7:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_9
# BB#8:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_9:                                # %emit_byte.exit.i33
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bl, (%rcx)
	decq	8(%rax)
	jne	.LBB8_12
# BB#10:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_12
# BB#11:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_12:                               # %emit_2bytes.exit
	cmpl	$65535, 44(%r14)        # imm = 0xFFFF
	ja	.LBB8_14
# BB#13:
	cmpl	$65536, 40(%r14)        # imm = 0x10000
	jb	.LBB8_15
.LBB8_14:
	movq	(%r14), %rax
	movl	$40, 40(%rax)
	movl	$65535, 44(%rax)        # imm = 0xFFFF
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_15:
	movb	64(%r14), %cl
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB8_18
# BB#16:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_18
# BB#17:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_18:                               # %emit_byte.exit
	movl	44(%r14), %ebx
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bh, (%rcx)  # NOREX
	decq	8(%rax)
	jne	.LBB8_21
# BB#19:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_21
# BB#20:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_21:                               # %emit_byte.exit.i34
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bl, (%rcx)
	decq	8(%rax)
	jne	.LBB8_24
# BB#22:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_24
# BB#23:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_24:                               # %emit_2bytes.exit35
	movl	40(%r14), %ebx
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bh, (%rcx)  # NOREX
	decq	8(%rax)
	jne	.LBB8_27
# BB#25:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_27
# BB#26:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_27:                               # %emit_byte.exit.i36
	movq	32(%r14), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bl, (%rcx)
	decq	8(%rax)
	jne	.LBB8_30
# BB#28:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_30
# BB#29:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_30:                               # %emit_2bytes.exit37
	movb	68(%r14), %cl
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB8_33
# BB#31:
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_33
# BB#32:
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_33:                               # %emit_byte.exit38
	cmpl	$0, 68(%r14)
	jle	.LBB8_45
# BB#34:                                # %.lr.ph
	movq	80(%r14), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_35:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %ecx
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB8_38
# BB#36:                                #   in Loop: Header=BB8_35 Depth=1
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_38
# BB#37:                                #   in Loop: Header=BB8_35 Depth=1
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB8_38:                               # %emit_byte.exit39
                                        #   in Loop: Header=BB8_35 Depth=1
	movl	8(%rbp), %ecx
	shll	$4, %ecx
	addl	12(%rbp), %ecx
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB8_41
# BB#39:                                #   in Loop: Header=BB8_35 Depth=1
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_41
# BB#40:                                #   in Loop: Header=BB8_35 Depth=1
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB8_41:                               # %emit_byte.exit40
                                        #   in Loop: Header=BB8_35 Depth=1
	movzbl	16(%rbp), %ecx
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB8_44
# BB#42:                                #   in Loop: Header=BB8_35 Depth=1
	movq	%r14, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB8_44
# BB#43:                                #   in Loop: Header=BB8_35 Depth=1
	movq	(%r14), %rax
	movl	$22, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB8_44:                               # %emit_byte.exit41
                                        #   in Loop: Header=BB8_35 Depth=1
	incl	%ebx
	addq	$96, %rbp
	cmpl	68(%r14), %ebx
	jl	.LBB8_35
.LBB8_45:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	emit_sof, .Lfunc_end8-emit_sof
	.cfi_endproc

	.p2align	4, 0x90
	.type	emit_dht,@function
emit_dht:                               # @emit_dht
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 64
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	16(%rsi), %eax
	testl	%edx, %edx
	movslq	%esi, %r15
	leaq	152(%r12,%r15,8), %rcx
	leaq	120(%r12,%r15,8), %rdx
	cmovnel	%eax, %r15d
	cmovneq	%rcx, %rdx
	movq	(%rdx), %r14
	testq	%r14, %r14
	jne	.LBB9_2
# BB#1:
	movq	(%r12), %rax
	movl	$49, 40(%rax)
	movl	%r15d, 44(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB9_2:
	cmpl	$0, 276(%r14)
	jne	.LBB9_30
# BB#3:
	movq	32(%r12), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-1, (%rcx)
	decq	8(%rax)
	jne	.LBB9_6
# BB#4:
	movq	%r12, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB9_6
# BB#5:
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB9_6:                                # %emit_byte.exit.i
	movq	32(%r12), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	$-60, (%rcx)
	decq	8(%rax)
	jne	.LBB9_9
# BB#7:
	movq	%r12, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB9_9
# BB#8:
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB9_9:                                # %emit_marker.exit.preheader
	movzbl	1(%r14), %eax
	movzbl	2(%r14), %ecx
	addl	%eax, %ecx
	movzbl	3(%r14), %eax
	addl	%ecx, %eax
	movzbl	4(%r14), %ecx
	addl	%eax, %ecx
	movzbl	5(%r14), %eax
	addl	%ecx, %eax
	movzbl	6(%r14), %ecx
	addl	%eax, %ecx
	movzbl	7(%r14), %eax
	addl	%ecx, %eax
	movzbl	8(%r14), %ecx
	addl	%eax, %ecx
	movzbl	9(%r14), %eax
	addl	%ecx, %eax
	movzbl	10(%r14), %ecx
	addl	%eax, %ecx
	movzbl	11(%r14), %eax
	addl	%ecx, %eax
	movzbl	12(%r14), %ecx
	addl	%eax, %ecx
	movzbl	13(%r14), %eax
	addl	%ecx, %eax
	movzbl	14(%r14), %ecx
	addl	%eax, %ecx
	movzbl	15(%r14), %r13d
	addl	%ecx, %r13d
	movzbl	16(%r14), %eax
	movq	%rax, %rbp
	leal	19(%rax,%r13), %ebx
	movq	32(%r12), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bh, (%rcx)  # NOREX
	decq	8(%rax)
	jne	.LBB9_12
# BB#10:
	movq	%r12, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB9_12
# BB#11:
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB9_12:                               # %emit_byte.exit.i41
	movq	32(%r12), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%bl, (%rcx)
	decq	8(%rax)
	jne	.LBB9_15
# BB#13:
	movq	%r12, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB9_15
# BB#14:
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB9_15:                               # %emit_2bytes.exit
	movq	32(%r12), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	movb	%r15b, (%rcx)
	decq	8(%rax)
	movq	%rbp, %rbx
	jne	.LBB9_18
# BB#16:
	movq	%r12, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB9_18
# BB#17:
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB9_18:                               # %emit_byte.exit.preheader
	addl	%r13d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_19:                               # =>This Inner Loop Header: Depth=1
	movzbl	1(%r14,%rbp), %ecx
	movq	32(%r12), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB9_22
# BB#20:                                #   in Loop: Header=BB9_19 Depth=1
	movq	%r12, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB9_22
# BB#21:                                #   in Loop: Header=BB9_19 Depth=1
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB9_22:                               # %emit_byte.exit42
                                        #   in Loop: Header=BB9_19 Depth=1
	incq	%rbp
	cmpq	$16, %rbp
	jne	.LBB9_19
# BB#23:                                # %.preheader
	testl	%ebx, %ebx
	jle	.LBB9_29
# BB#24:                                # %.lr.ph.preheader
	movl	%ebx, %ebx
	leaq	17(%r14), %rbp
	.p2align	4, 0x90
.LBB9_25:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %ecx
	movq	32(%r12), %rax
	movq	(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rax)
	movb	%cl, (%rdx)
	decq	8(%rax)
	jne	.LBB9_28
# BB#26:                                #   in Loop: Header=BB9_25 Depth=1
	movq	%r12, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB9_28
# BB#27:                                #   in Loop: Header=BB9_25 Depth=1
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB9_28:                               # %emit_byte.exit43
                                        #   in Loop: Header=BB9_25 Depth=1
	incq	%rbp
	decq	%rbx
	jne	.LBB9_25
.LBB9_29:                               # %._crit_edge
	movl	$1, 276(%r14)
.LBB9_30:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	emit_dht, .Lfunc_end9-emit_dht
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
