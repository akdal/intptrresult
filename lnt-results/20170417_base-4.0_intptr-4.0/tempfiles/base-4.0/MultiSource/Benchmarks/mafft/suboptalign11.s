	.text
	.file	"suboptalign11.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_1:
	.quad	4608533498688228557     # double 1.3
.LCPI0_4:
	.quad	4599075939470750515     # double 0.29999999999999999
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_2:
	.long	3296328090              # float -999.900024
.LCPI0_3:
	.long	3379831806              # float -999999.875
	.text
	.globl	suboptalign11
	.p2align	4, 0x90
	.type	suboptalign11,@function
suboptalign11:                          # @suboptalign11
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi6:
	.cfi_def_cfa_offset 400
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 232(%rsp)          # 8-byte Spill
	movq	%r8, 208(%rsp)          # 8-byte Spill
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movl	%edx, 228(%rsp)         # 4-byte Spill
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 224(%rsp)        # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	penalty_OP(%rip), %xmm0
	movss	%xmm0, 220(%rsp)        # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	penalty_ex(%rip), %xmm0
	movss	%xmm0, 216(%rsp)        # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	offset(%rip), %xmm0
	xorpd	.LCPI0_0(%rip), %xmm0
	movapd	%xmm0, 320(%rsp)        # 16-byte Spill
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	suboptalign11.shuryo(%rip), %rax
	testq	%rax, %rax
	jne	.LBB0_2
# BB#1:
	movl	$100, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, suboptalign11.shuryo(%rip)
.LBB0_2:                                # %.preheader447
	movl	$40, %ecx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movl	$-1, -40(%rax,%rcx)
	movl	$-1, -36(%rax,%rcx)
	movl	$0, -32(%rax,%rcx)
	movq	$-1, -8(%rax,%rcx)
	movl	$0, (%rax,%rcx)
	addq	$64, %rcx
	cmpq	$3240, %rcx             # imm = 0xCA8
	jne	.LBB0_3
# BB#4:
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	cmpl	suboptalign11.orlgth1(%rip), %ebx
	jg	.LBB0_6
# BB#5:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	suboptalign11.orlgth2(%rip), %eax
	jle	.LBB0_10
.LBB0_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movl	suboptalign11.orlgth1(%rip), %r14d
	movl	suboptalign11.orlgth2(%rip), %r15d
	testl	%r14d, %r14d
	jle	.LBB0_9
# BB#7:
	testl	%r15d, %r15d
	jle	.LBB0_9
# BB#8:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	suboptalign11.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	suboptalign11.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	suboptalign11.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	suboptalign11.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	suboptalign11.m(%rip), %rdi
	callq	FreeFloatVec
	movq	suboptalign11.mp(%rip), %rdi
	callq	FreeIntVec
	movq	suboptalign11.largeM(%rip), %rdi
	callq	FreeFloatVec
	movq	suboptalign11.Mp(%rip), %rdi
	callq	FreeIntVec
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	suboptalign11.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	suboptalign11.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	suboptalign11.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	suboptalign11.intwork(%rip), %rdi
	callq	FreeIntMtx
	movl	suboptalign11.orlgth1(%rip), %r14d
	movl	suboptalign11.orlgth2(%rip), %r15d
.LBB0_9:
	movq	144(%rsp), %rax         # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	leal	100(%r14), %r13d
	movq	8(%rsp), %rax           # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	leal	100(%r15), %ebp
	leal	102(%r15), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, suboptalign11.w1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, suboptalign11.w2(%rip)
	leal	102(%r14), %r12d
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, suboptalign11.initverticalw(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, suboptalign11.lastverticalw(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, suboptalign11.m(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, suboptalign11.mp(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, suboptalign11.largeM(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, suboptalign11.Mp(%rip)
	movl	$26, %edi
	movl	%r12d, %esi
	callq	AllocateFloatMtx
	movq	%rax, suboptalign11.cpmx1(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, suboptalign11.cpmx2(%rip)
	cmpl	%ebp, %r13d
	cmovgel	%r13d, %ebp
	addl	$2, %ebp
	movl	$26, %edi
	movl	%ebp, %esi
	callq	AllocateFloatMtx
	movq	%rax, suboptalign11.floatwork(%rip)
	movl	$26, %edi
	movl	%ebp, %esi
	callq	AllocateIntMtx
	movq	%rax, suboptalign11.intwork(%rip)
	movl	njob(%rip), %edi
	leal	200(%r15,%r14), %ebx
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, suboptalign11.mseq1(%rip)
	movl	njob(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, suboptalign11.mseq2(%rip)
	movl	%r14d, suboptalign11.orlgth1(%rip)
	movl	%r15d, suboptalign11.orlgth2(%rip)
.LBB0_10:
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movl	suboptalign11.orlgth1(%rip), %eax
	movl	commonAlloc1(%rip), %ebx
	cmpl	%ebx, %eax
	jle	.LBB0_16
# BB#11:                                # %._crit_edge
	movl	commonAlloc2(%rip), %r15d
	testl	%ebx, %ebx
	jne	.LBB0_13
	jmp	.LBB0_15
.LBB0_16:
	movl	commonAlloc2(%rip), %r15d
	cmpl	%r15d, suboptalign11.orlgth2(%rip)
	jle	.LBB0_17
# BB#12:
	testl	%ebx, %ebx
	je	.LBB0_15
.LBB0_13:
	testl	%r15d, %r15d
	je	.LBB0_15
# BB#14:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movq	commonJP(%rip), %rdi
	callq	FreeIntMtx
	movq	suboptalign11.used(%rip), %rdi
	callq	FreeIntMtx
	movl	suboptalign11.orlgth1(%rip), %eax
	movl	commonAlloc1(%rip), %ebx
	movl	commonAlloc2(%rip), %r15d
.LBB0_15:
	cmpl	%ebx, %eax
	cmovgel	%eax, %ebx
	movl	suboptalign11.orlgth2(%rip), %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	leal	10(%rbx), %r14d
	leal	10(%r15), %ebp
	movl	%r14d, %edi
	movl	%ebp, %esi
	callq	AllocateIntMtx
	movq	%rax, suboptalign11.used(%rip)
	movl	%r14d, %edi
	movl	%ebp, %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%r14d, %edi
	movl	%ebp, %esi
	callq	AllocateIntMtx
	movq	%rax, commonJP(%rip)
	movl	%ebx, commonAlloc1(%rip)
	movl	%r15d, commonAlloc2(%rip)
	jmp	.LBB0_18
.LBB0_17:                               # %._crit_edge582
	movq	commonJP(%rip), %rax
.LBB0_18:
	movq	commonIP(%rip), %rcx
	movq	%rcx, suboptalign11.ijpi(%rip)
	movq	%rax, suboptalign11.ijpj(%rip)
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movq	suboptalign11.w1(%rip), %rcx
	movq	suboptalign11.w2(%rip), %r9
	movq	suboptalign11.initverticalw(%rip), %r14
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movss	224(%rsp), %xmm8        # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	220(%rsp), %xmm9        # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	216(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	320(%rsp), %xmm11       # 16-byte Reload
	jle	.LBB0_25
# BB#19:                                # %.lr.ph.i
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rdi
	movq	144(%rsp), %rsi         # 8-byte Reload
	movl	%esi, %edx
	testb	$1, %sil
	jne	.LBB0_21
# BB#20:
	xorl	%ebp, %ebp
	cmpq	$1, %rdx
	jne	.LBB0_23
	jmp	.LBB0_25
.LBB0_21:
	movsbq	(%rax), %rsi
	movsbq	(%rdi), %rbp
	shlq	$9, %rsi
	cvtsi2ssl	amino_dis(%rsi,%rbp,4), %xmm0
	movss	%xmm0, (%r14)
	movl	$1, %ebp
	cmpq	$1, %rdx
	je	.LBB0_25
.LBB0_23:                               # %.lr.ph.i.new
	subq	%rbp, %rdx
	leaq	4(%r14,%rbp,4), %rsi
	leaq	1(%rdi,%rbp), %rdi
	.p2align	4, 0x90
.LBB0_24:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%rax), %rbp
	movsbq	-1(%rdi), %rbx
	shlq	$9, %rbp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rbx,4), %xmm0
	movss	%xmm0, -4(%rsi)
	movsbq	(%rax), %rbp
	movsbq	(%rdi), %rbx
	shlq	$9, %rbp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rbx,4), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rsi
	addq	$2, %rdi
	addq	$-2, %rdx
	jne	.LBB0_24
.LBB0_25:                               # %match_calc.exit
	movb	$1, %r15b
	movq	%r10, %rsi
	testl	%esi, %esi
	movq	%r14, 160(%rsp)         # 8-byte Spill
	jle	.LBB0_26
# BB#27:                                # %.lr.ph.i423
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	120(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx), %rdi
	movl	%esi, %edx
	testb	$1, %sil
	jne	.LBB0_29
# BB#28:
	xorl	%ebp, %ebp
	cmpq	$1, %rdx
	jne	.LBB0_31
	jmp	.LBB0_33
.LBB0_26:
	movq	%rsi, %rbp
	jmp	.LBB0_51
.LBB0_29:
	movsbq	(%rax), %rsi
	movsbq	(%rdi), %rbp
	shlq	$9, %rsi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rsi,%rbp,4), %xmm0
	movss	%xmm0, (%rcx)
	movl	$1, %ebp
	cmpq	$1, %rdx
	je	.LBB0_33
.LBB0_31:                               # %.lr.ph.i423.new
	subq	%rbp, %rdx
	leaq	4(%rcx,%rbp,4), %rsi
	leaq	1(%rdi,%rbp), %rdi
	.p2align	4, 0x90
.LBB0_32:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%rax), %rbp
	movsbq	-1(%rdi), %rbx
	shlq	$9, %rbp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rbx,4), %xmm0
	movss	%xmm0, -4(%rsi)
	movsbq	(%rax), %rbp
	movsbq	(%rdi), %rbx
	shlq	$9, %rbp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rbx,4), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rsi
	addq	$2, %rdi
	addq	$-2, %rdx
	jne	.LBB0_32
.LBB0_33:                               # %match_calc.exit428.preheader
	movq	%r10, %rbp
	testl	%ebp, %ebp
	setle	%r11b
	jle	.LBB0_51
# BB#34:                                # %.lr.ph521
	movq	suboptalign11.m(%rip), %rdx
	movq	suboptalign11.mp(%rip), %r13
	movq	suboptalign11.largeM(%rip), %rdi
	movq	%rbp, %rax
	movq	suboptalign11.Mp(%rip), %rbp
	leaq	1(%rax), %r10
	movl	%r10d, %r8d
	leaq	-1(%r8), %rbx
	cmpq	$8, %rbx
	jb	.LBB0_35
# BB#41:                                # %min.iters.checked
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$7, %eax
	movq	%rbx, %rsi
	subq	%rax, %rsi
	je	.LBB0_35
# BB#42:                                # %vector.memcheck
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movb	%r11b, 24(%rsp)         # 1-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	leaq	4(%rdx), %rax
	leaq	(%rdx,%r8,4), %r9
	leaq	4(%rdi), %r15
	leaq	(%rdi,%r8,4), %r12
	cmpq	%r12, %rax
	sbbb	%bl, %bl
	cmpq	%r9, %r15
	sbbb	%r11b, %r11b
	andb	%bl, %r11b
	leaq	-4(%rcx,%r8,4), %r14
	cmpq	%r14, %rax
	sbbb	%al, %al
	cmpq	%r9, %rcx
	sbbb	%bl, %bl
	movb	%bl, 48(%rsp)           # 1-byte Spill
	cmpq	%r14, %r15
	leaq	4(%r13), %r15
	leaq	(%rbp,%r8,4), %r9
	sbbb	%bl, %bl
	cmpq	%r12, %rcx
	sbbb	%r14b, %r14b
	cmpq	%r9, %r15
	leaq	(%r13,%r8,4), %r9
	leaq	4(%rbp), %r15
	sbbb	%r12b, %r12b
	cmpq	%r9, %r15
	sbbb	%r9b, %r9b
	testb	$1, %r11b
	movl	$1, %r15d
	jne	.LBB0_43
# BB#44:                                # %vector.memcheck
	andb	48(%rsp), %al           # 1-byte Folded Reload
	andb	$1, %al
	jne	.LBB0_43
# BB#45:                                # %vector.memcheck
	andb	%r14b, %bl
	andb	$1, %bl
	jne	.LBB0_43
# BB#46:                                # %vector.memcheck
	andb	%r9b, %r12b
	andb	$1, %r12b
	movq	160(%rsp), %r14         # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movb	24(%rsp), %r11b         # 1-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_36
# BB#47:                                # %vector.body.preheader
	leaq	1(%rsi), %r15
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_48:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rax,4), %xmm1
	movups	16(%rcx,%rax,4), %xmm2
	movups	%xmm1, 4(%rdx,%rax,4)
	movups	%xmm2, 20(%rdx,%rax,4)
	movupd	%xmm0, 4(%r13,%rax,4)
	movupd	%xmm0, 20(%r13,%rax,4)
	movupd	(%rcx,%rax,4), %xmm1
	movdqu	16(%rcx,%rax,4), %xmm2
	movupd	%xmm1, 4(%rdi,%rax,4)
	movdqu	%xmm2, 20(%rdi,%rax,4)
	movupd	%xmm0, 4(%rbp,%rax,4)
	movupd	%xmm0, 20(%rbp,%rax,4)
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.LBB0_48
# BB#49:                                # %middle.block
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_36
# BB#50:
	movl	%r11d, %r15d
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB0_51
.LBB0_35:
	movl	$1, %r15d
.LBB0_36:                               # %match_calc.exit428.preheader678
	subl	%r15d, %r10d
	testb	$1, %r10b
	movq	%r15, %rax
	je	.LBB0_38
# BB#37:                                # %match_calc.exit428.prol
	movl	-4(%rcx,%r15,4), %eax
	movl	%eax, (%rdx,%r15,4)
	movl	$0, (%r13,%r15,4)
	movl	-4(%rcx,%r15,4), %eax
	movl	%eax, (%rdi,%r15,4)
	movl	$0, (%rbp,%r15,4)
	leaq	1(%r15), %rax
.LBB0_38:                               # %match_calc.exit428.prol.loopexit
	cmpq	%r15, %rbx
	movq	8(%rsp), %r10           # 8-byte Reload
	je	.LBB0_40
	.p2align	4, 0x90
.LBB0_39:                               # %match_calc.exit428
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx,%rax,4), %ebx
	movl	%ebx, (%rdx,%rax,4)
	movl	$0, (%r13,%rax,4)
	movl	-4(%rcx,%rax,4), %ebx
	movl	%ebx, (%rdi,%rax,4)
	movl	$0, (%rbp,%rax,4)
	movl	(%rcx,%rax,4), %ebx
	movl	%ebx, 4(%rdx,%rax,4)
	movl	$0, 4(%r13,%rax,4)
	movl	(%rcx,%rax,4), %ebx
	movl	%ebx, 4(%rdi,%rax,4)
	movl	$0, 4(%rbp,%rax,4)
	addq	$2, %rax
	cmpq	%rax, %r8
	jne	.LBB0_39
.LBB0_40:
	movl	%r11d, %r15d
	movq	%r10, %rbp
.LBB0_51:                               # %match_calc.exit428._crit_edge
	movq	%rbp, %rax
	shlq	$32, %rax
	movabsq	$-4294967296, %r8       # imm = 0xFFFFFFFF00000000
	addq	%rax, %r8
	movq	%r8, %rax
	sarq	$30, %rax
	movl	(%rcx,%rax), %eax
	movq	suboptalign11.lastverticalw(%rip), %r11
	movl	%eax, (%r11)
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	1(%rax), %edx
	leal	1(%rax,%rbp), %edi
	movl	%edi, 108(%rsp)         # 4-byte Spill
	movl	%edi, localstop(%rip)
	testl	%eax, %eax
	movl	%edx, 180(%rsp)         # 4-byte Spill
	jle	.LBB0_52
# BB#73:                                # %.lr.ph512
	sarq	$32, %r8
	movl	%ebp, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	suboptalign11.ijpi(%rip), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	suboptalign11.ijpj(%rip), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	suboptalign11.m(%rip), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	suboptalign11.largeM(%rip), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	suboptalign11.mp(%rip), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	suboptalign11.Mp(%rip), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	suboptalign11.shuryo(%rip), %rax
	movl	%edx, %r13d
	movq	%rbp, %rsi
	movl	%ebp, %edx
	andl	$1, %edx
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	movq	%rax, 192(%rsp)         # 8-byte Spill
	leaq	136(%rax), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movss	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorpd	%xmm1, %xmm1
	xorl	%ebp, %ebp
	movl	$1, %ebx
	movss	.LCPI0_3(%rip), %xmm12  # xmm12 = mem[0],zero,zero,zero
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	xorps	%xmm7, %xmm7
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	movl	$0, 136(%rsp)           # 4-byte Folded Spill
	movb	%r15b, 23(%rsp)         # 1-byte Spill
	movq	%r8, 304(%rsp)          # 8-byte Spill
	movq	%r11, 296(%rsp)         # 8-byte Spill
	movq	%r13, 240(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_74:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_80 Depth 2
                                        #     Child Loop BB0_84 Depth 2
                                        #       Child Loop BB0_105 Depth 3
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	-4(%r14,%rbx,4), %eax
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movl	%eax, (%rcx)
	movq	%rsi, %r10
	testl	%esi, %esi
	jle	.LBB0_81
# BB#75:                                # %.lr.ph.i432
                                        #   in Loop: Header=BB0_74 Depth=1
	cmpq	$0, 312(%rsp)           # 8-byte Folded Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rsi
	jne	.LBB0_77
# BB#76:                                #   in Loop: Header=BB0_74 Depth=1
	xorl	%edi, %edi
	cmpq	$1, 80(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_79
	jmp	.LBB0_81
	.p2align	4, 0x90
.LBB0_77:                               #   in Loop: Header=BB0_74 Depth=1
	movsbq	(%rax,%rbx), %rcx
	movsbq	(%rsi), %rdx
	shlq	$9, %rcx
	cvtsi2ssl	amino_dis(%rcx,%rdx,4), %xmm2
	movd	%xmm2, (%r9)
	movl	$1, %edi
	cmpq	$1, 80(%rsp)            # 8-byte Folded Reload
	je	.LBB0_81
.LBB0_79:                               # %.lr.ph.i432.new
                                        #   in Loop: Header=BB0_74 Depth=1
	movq	80(%rsp), %rcx          # 8-byte Reload
	subq	%rdi, %rcx
	leaq	4(%r9,%rdi,4), %rdx
	leaq	1(%rsi,%rdi), %rsi
	.p2align	4, 0x90
.LBB0_80:                               #   Parent Loop BB0_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rax,%rbx), %rdi
	movsbq	-1(%rsi), %rbp
	shlq	$9, %rdi
	xorps	%xmm2, %xmm2
	cvtsi2ssl	amino_dis(%rdi,%rbp,4), %xmm2
	movss	%xmm2, -4(%rdx)
	movsbq	(%rax,%rbx), %rdi
	movsbq	(%rsi), %rbp
	shlq	$9, %rdi
	xorps	%xmm2, %xmm2
	cvtsi2ssl	amino_dis(%rdi,%rbp,4), %xmm2
	movd	%xmm2, (%rdx)
	addq	$8, %rdx
	addq	$2, %rsi
	addq	$-2, %rcx
	jne	.LBB0_80
.LBB0_81:                               # %match_calc.exit437
                                        #   in Loop: Header=BB0_74 Depth=1
	movl	(%r14,%rbx,4), %eax
	movl	%eax, (%r9)
	movq	168(%rsp), %rdi         # 8-byte Reload
	movl	(%rdi), %eax
	movl	%eax, suboptalign11.mi(%rip)
	movl	%eax, suboptalign11.Mi(%rip)
	testb	%r15b, %r15b
	je	.LBB0_83
# BB#82:                                #   in Loop: Header=BB0_74 Depth=1
	xorl	%edx, %edx
	movl	$0, 112(%rsp)           # 4-byte Folded Spill
	movq	%r10, %rsi
	jmp	.LBB0_115
	.p2align	4, 0x90
.LBB0_83:                               # %.lr.ph492.preheader
                                        #   in Loop: Header=BB0_74 Depth=1
	leaq	-1(%rbx), %rcx
	movd	%eax, %xmm2
	movq	280(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %r14
	movq	288(%rsp), %rax         # 8-byte Reload
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	(%rax,%rbx,8), %r12
	movl	$1, %ebx
	xorl	%esi, %esi
	movdqa	%xmm2, %xmm3
	movdqa	%xmm2, %xmm6
	movl	$0, 112(%rsp)           # 4-byte Folded Spill
	movq	%rdi, %rax
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	264(%rsp), %r11         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	248(%rsp), %r15         # 8-byte Reload
	movq	%r9, 88(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%r8d, %r8d
	movaps	%xmm12, %xmm4
	movq	%rax, %rbp
	movq	%rcx, %r10
	movq	%r10, 96(%rsp)          # 8-byte Spill
	jmp	.LBB0_84
.LBB0_189:                              #   in Loop: Header=BB0_84 Depth=2
	movaps	%xmm5, %xmm1
	jmp	.LBB0_110
	.p2align	4, 0x90
.LBB0_102:                              #   in Loop: Header=BB0_84 Depth=2
	movl	%r15d, %esi
	movq	%rdx, %r15
	movq	%r8, %r11
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB0_103:                              # %.loopexit446
                                        #   in Loop: Header=BB0_84 Depth=2
	movq	%r13, %rdi
	movl	152(%rsp), %r8d         # 4-byte Reload
	movq	%rbx, %rbp
	movl	36(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB0_111
.LBB0_107:                              #   in Loop: Header=BB0_84 Depth=2
	incl	%eax
	jmp	.LBB0_109
.LBB0_184:                              #   in Loop: Header=BB0_84 Depth=2
	addl	$2, %eax
	jmp	.LBB0_109
.LBB0_186:                              #   in Loop: Header=BB0_84 Depth=2
	addl	$3, %eax
	jmp	.LBB0_109
.LBB0_108:                              # %..loopexit446.loopexit_crit_edge
                                        #   in Loop: Header=BB0_84 Depth=2
	addl	$4, %eax
	.p2align	4, 0x90
.LBB0_109:                              # %.loopexit446
                                        #   in Loop: Header=BB0_84 Depth=2
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB0_110:                              # %.loopexit446
                                        #   in Loop: Header=BB0_84 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
.LBB0_111:                              # %.loopexit446
                                        #   in Loop: Header=BB0_84 Depth=2
	addq	$4, %r9
	ucomiss	%xmm5, %xmm11
	movaps	%xmm5, %xmm7
	jbe	.LBB0_113
# BB#112:                               #   in Loop: Header=BB0_84 Depth=2
	movl	%esi, %edx
	movq	%rax, %rsi
	movl	108(%rsp), %eax         # 4-byte Reload
	movl	%eax, (%r12)
	movq	%rsi, %rax
	movl	%edx, %esi
	movaps	%xmm11, %xmm7
.LBB0_113:                              #   in Loop: Header=BB0_84 Depth=2
	maxss	%xmm0, %xmm5
	movss	(%r9), %xmm6            # xmm6 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm6
	movss	%xmm6, (%r9)
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpl	%eax, %ebx
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	je	.LBB0_114
# BB#190:                               # %..lr.ph492_crit_edge
                                        #   in Loop: Header=BB0_84 Depth=2
	addq	$4, %rbp
	incl	%ebx
	movss	(%rbp), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
.LBB0_84:                               # %.lr.ph492
                                        #   Parent Loop BB0_74 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_105 Depth 3
	addq	$4, %rdi
	movl	%r10d, 4(%r12)
	leal	-1(%rbx), %ecx
	movaps	%xmm8, %xmm5
	addss	%xmm3, %xmm5
	ucomiss	%xmm6, %xmm5
	movl	%ecx, %eax
	cmoval	112(%rsp), %eax         # 4-byte Folded Reload
	maxss	%xmm6, %xmm5
	movl	%eax, 4(%r14)
	ucomiss	%xmm3, %xmm6
	jbe	.LBB0_86
# BB#85:                                #   in Loop: Header=BB0_84 Depth=2
	movss	%xmm6, suboptalign11.mi(%rip)
	movaps	%xmm6, %xmm3
	movl	%ecx, 112(%rsp)         # 4-byte Spill
.LBB0_86:                               #   in Loop: Header=BB0_84 Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	addq	$4, %rax
	addq	$4, %r12
	addss	%xmm10, %xmm3
	movss	%xmm3, suboptalign11.mi(%rip)
	movss	(%rdi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm7
	addss	%xmm6, %xmm7
	ucomiss	%xmm5, %xmm7
	jbe	.LBB0_88
# BB#87:                                #   in Loop: Header=BB0_84 Depth=2
	movq	%rax, %rdx
	movl	(%rdx), %eax
	movl	%eax, (%r12)
	movq	%rdx, %rax
	movaps	%xmm7, %xmm5
.LBB0_88:                               #   in Loop: Header=BB0_84 Depth=2
	movss	(%rbp), %xmm7           # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm7
	jbe	.LBB0_90
# BB#89:                                #   in Loop: Header=BB0_84 Depth=2
	movss	%xmm7, (%rdi)
	movl	%r10d, (%rax)
	movaps	%xmm7, %xmm6
.LBB0_90:                               #   in Loop: Header=BB0_84 Depth=2
	addq	$4, %r11
	addq	$4, %r14
	addss	%xmm10, %xmm6
	movss	%xmm6, (%rdi)
	movaps	%xmm9, %xmm6
	addss	%xmm4, %xmm6
	ucomiss	%xmm5, %xmm6
	jbe	.LBB0_92
# BB#91:                                #   in Loop: Header=BB0_84 Depth=2
	movl	%r8d, (%r12)
	movl	%r13d, (%r14)
	movaps	%xmm6, %xmm5
.LBB0_92:                               #   in Loop: Header=BB0_84 Depth=2
	addq	$4, %r15
	ucomiss	%xmm4, %xmm2
	movdqa	%xmm2, %xmm6
	maxss	%xmm4, %xmm6
	cmoval	%r10d, %r8d
	cmoval	%esi, %r13d
	movss	(%r11), %xmm7           # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm7
	movaps	%xmm6, %xmm4
	jbe	.LBB0_94
# BB#93:                                #   in Loop: Header=BB0_84 Depth=2
	movl	(%r15), %r8d
	movaps	%xmm7, %xmm4
	movl	%ecx, %r13d
.LBB0_94:                               #   in Loop: Header=BB0_84 Depth=2
	movd	(%rbp), %xmm6           # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm7, %xmm6
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	%r8d, 152(%rsp)         # 4-byte Spill
	movl	%r13d, 36(%rsp)         # 4-byte Spill
	jbe	.LBB0_95
# BB#96:                                #   in Loop: Header=BB0_84 Depth=2
	movq	%rdi, %r13
	movq	%r11, %r8
	movd	%xmm6, (%r11)
	movq	%r15, %rdx
	movl	%r10d, (%r15)
	movq	%rbp, %rbx
	movd	(%rbp), %xmm6           # xmm6 = mem[0],zero,zero,zero
	jmp	.LBB0_97
	.p2align	4, 0x90
.LBB0_95:                               #   in Loop: Header=BB0_84 Depth=2
	movq	%rbp, %rbx
	movq	%rdi, %r13
	movq	%r11, %r8
	movq	%r15, %rdx
.LBB0_97:                               #   in Loop: Header=BB0_84 Depth=2
	movq	%rax, %r11
	ucomiss	%xmm2, %xmm6
	jbe	.LBB0_99
# BB#98:                                #   in Loop: Header=BB0_84 Depth=2
	movd	%xmm6, suboptalign11.Mi(%rip)
	movl	%ecx, %esi
	movdqa	%xmm6, %xmm2
.LBB0_99:                               #   in Loop: Header=BB0_84 Depth=2
	movl	%esi, %r15d
	ucomiss	%xmm0, %xmm5
	movl	136(%rsp), %eax         # 4-byte Reload
	movq	128(%rsp), %rbp         # 8-byte Reload
	cmoval	%ebp, %eax
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movl	64(%rsp), %eax          # 4-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	cmoval	%edi, %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movl	48(%rsp), %ecx          # 4-byte Reload
	cmpl	$99, %ecx
	movq	%r11, 72(%rsp)          # 8-byte Spill
	jg	.LBB0_101
# BB#100:                               #   in Loop: Header=BB0_84 Depth=2
	movslq	%ecx, %rax
	shlq	$5, %rax
	movq	192(%rsp), %r10         # 8-byte Reload
	movl	%ebp, (%r10,%rax)
	movl	%edi, 4(%r10,%rax)
	movss	%xmm5, 8(%r10,%rax)
	movq	96(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	ucomiss	%xmm5, %xmm1
	movaps	%xmm5, %xmm6
	minss	%xmm1, %xmm6
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmoval	%ecx, %esi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	incl	%ecx
	movaps	%xmm6, %xmm1
	movl	%r15d, %esi
	movq	%rdx, %r15
	movq	%r8, %r11
	jmp	.LBB0_103
	.p2align	4, 0x90
.LBB0_101:                              #   in Loop: Header=BB0_84 Depth=2
	ucomiss	%xmm1, %xmm5
	jbe	.LBB0_102
# BB#104:                               #   in Loop: Header=BB0_84 Depth=2
	movslq	40(%rsp), %rax          # 4-byte Folded Reload
	shlq	$5, %rax
	movq	192(%rsp), %rbp         # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movl	%esi, (%rbp,%rax)
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%esi, 4(%rbp,%rax)
	movss	%xmm5, 8(%rbp,%rax)
	movq	184(%rsp), %r10         # 8-byte Reload
	xorl	%eax, %eax
	movl	%r15d, %esi
	movq	%rdx, %r15
	movq	%r8, %r11
	movq	%r13, %rdi
	movl	152(%rsp), %r8d         # 4-byte Reload
	movq	%rbx, %rbp
	movl	36(%rsp), %r13d         # 4-byte Reload
	.p2align	4, 0x90
.LBB0_105:                              #   Parent Loop BB0_74 Depth=1
                                        #     Parent Loop BB0_84 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	-128(%r10), %xmm1       # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm5
	ja	.LBB0_109
# BB#106:                               #   in Loop: Header=BB0_105 Depth=3
	movss	-96(%r10), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm5
	ja	.LBB0_107
# BB#183:                               #   in Loop: Header=BB0_105 Depth=3
	movss	-64(%r10), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm5
	ja	.LBB0_184
# BB#185:                               #   in Loop: Header=BB0_105 Depth=3
	movss	-32(%r10), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm5
	ja	.LBB0_186
# BB#187:                               #   in Loop: Header=BB0_105 Depth=3
	movss	(%r10), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm5
	ja	.LBB0_108
# BB#188:                               #   in Loop: Header=BB0_105 Depth=3
	addq	$5, %rax
	addq	$160, %r10
	cmpq	$100, %rax
	jl	.LBB0_105
	jmp	.LBB0_189
	.p2align	4, 0x90
.LBB0_114:                              #   in Loop: Header=BB0_74 Depth=1
	movl	%esi, %edx
	movq	%rax, %rsi
	movaps	%xmm5, %xmm0
	movq	160(%rsp), %r14         # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movb	23(%rsp), %r15b         # 1-byte Reload
	movq	304(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r11         # 8-byte Reload
	movq	240(%rsp), %r13         # 8-byte Reload
	movq	128(%rsp), %rbx         # 8-byte Reload
	movq	168(%rsp), %rdi         # 8-byte Reload
.LBB0_115:                              # %._crit_edge493
                                        #   in Loop: Header=BB0_74 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	(%r9,%r8,4), %eax
	movl	%eax, (%r11,%rbx,4)
	incq	%rbx
	cmpq	%r13, %rbx
	movq	%r9, %rcx
	movq	%rdi, %r9
	jne	.LBB0_74
# BB#53:                                # %..preheader445_crit_edge
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movss	%xmm7, 36(%rsp)         # 4-byte Spill
	movl	%edx, suboptalign11.Mpi(%rip)
	movl	112(%rsp), %eax         # 4-byte Reload
	movl	%eax, suboptalign11.mpi(%rip)
	movl	64(%rsp), %r14d         # 4-byte Reload
	movl	136(%rsp), %r15d        # 4-byte Reload
.LBB0_54:                               # %.preheader445
	movq	stderr(%rip), %rdi
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB0_55:                               # =>This Inner Loop Header: Depth=1
	movq	suboptalign11.shuryo(%rip), %rax
	movl	-8(%rax,%rbp), %ecx
	movl	-4(%rax,%rbp), %r8d
	movss	(%rax,%rbp), %xmm0      # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.9, %esi
	movb	$1, %al
	movl	%ebx, %edx
	callq	fprintf
	incq	%rbx
	movq	stderr(%rip), %rdi
	addq	$32, %rbp
	cmpq	$100, %rbx
	jne	.LBB0_55
# BB#56:
	movss	24(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.10, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	xorl	%ebp, %ebp
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movq	suboptalign11.shuryo(%rip), %rdi
	movl	$100, %esi
	movl	$32, %edx
	movl	$compshuryo, %ecx
	callq	qsort
	movl	$8, %ebx
	.p2align	4, 0x90
.LBB0_57:                               # =>This Inner Loop Header: Depth=1
	movq	stderr(%rip), %rdi
	movq	suboptalign11.shuryo(%rip), %rax
	movl	-8(%rax,%rbx), %ecx
	movl	-4(%rax,%rbx), %r8d
	movss	(%rax,%rbx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.9, %esi
	movb	$1, %al
	movl	%ebp, %edx
	callq	fprintf
	incq	%rbp
	addq	$32, %rbx
	cmpq	$100, %rbp
	jne	.LBB0_57
# BB#58:                                # %.preheader444
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	js	.LBB0_64
# BB#59:                                # %.lr.ph463
	movl	localstop(%rip), %eax
	movq	suboptalign11.ijpi(%rip), %rdx
	movq	suboptalign11.ijpj(%rip), %rsi
	movl	180(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %ecx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rbx
	xorl	%edi, %edi
	andq	$3, %rbx
	je	.LBB0_61
	.p2align	4, 0x90
.LBB0_60:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rdi,8), %rbp
	movl	%eax, (%rbp)
	movq	(%rsi,%rdi,8), %rbp
	movl	%eax, (%rbp)
	incq	%rdi
	cmpq	%rdi, %rbx
	jne	.LBB0_60
.LBB0_61:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB0_64
# BB#62:                                # %.lr.ph463.new
	subq	%rdi, %rcx
	leaq	24(%rsi,%rdi,8), %rsi
	leaq	24(%rdx,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB0_63:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rdi
	movl	%eax, (%rdi)
	movq	-24(%rsi), %rdi
	movl	%eax, (%rdi)
	movq	-16(%rdx), %rdi
	movl	%eax, (%rdi)
	movq	-16(%rsi), %rdi
	movl	%eax, (%rdi)
	movq	-8(%rdx), %rdi
	movl	%eax, (%rdi)
	movq	-8(%rsi), %rdi
	movl	%eax, (%rdi)
	movq	(%rdx), %rdi
	movl	%eax, (%rdi)
	movq	(%rsi), %rdi
	movl	%eax, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB0_63
.LBB0_64:                               # %.preheader443
	testl	%r9d, %r9d
	js	.LBB0_122
# BB#65:                                # %.lr.ph461
	movl	localstop(%rip), %eax
	movq	suboptalign11.ijpi(%rip), %rcx
	movq	(%rcx), %r9
	movq	suboptalign11.ijpj(%rip), %rcx
	movq	(%rcx), %r10
	movq	8(%rsp), %rdi           # 8-byte Reload
	incq	%rdi
	movl	%edi, %ebp
	cmpq	$7, %rbp
	jbe	.LBB0_66
# BB#116:                               # %min.iters.checked653
	movl	%edi, %r8d
	andl	$7, %r8d
	movq	%rbp, %rcx
	subq	%r8, %rcx
	je	.LBB0_66
# BB#117:                               # %vector.memcheck666
	leaq	(%r10,%rbp,4), %rdx
	cmpq	%rdx, %r9
	jae	.LBB0_119
# BB#118:                               # %vector.memcheck666
	leaq	(%r9,%rbp,4), %rdx
	cmpq	%rdx, %r10
	jae	.LBB0_119
.LBB0_66:
	xorl	%ecx, %ecx
.LBB0_67:                               # %scalar.ph651.preheader
	subl	%ecx, %edi
	leaq	-1(%rbp), %rbx
	subq	%rcx, %rbx
	andq	$7, %rdi
	je	.LBB0_70
# BB#68:                                # %scalar.ph651.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB0_69:                               # %scalar.ph651.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, (%r9,%rcx,4)
	movl	%eax, (%r10,%rcx,4)
	incq	%rcx
	incq	%rdi
	jne	.LBB0_69
.LBB0_70:                               # %scalar.ph651.prol.loopexit
	cmpq	$7, %rbx
	jb	.LBB0_122
# BB#71:                                # %scalar.ph651.preheader.new
	subq	%rcx, %rbp
	leaq	28(%r10,%rcx,4), %rsi
	leaq	28(%r9,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_72:                               # %scalar.ph651
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, -28(%rcx)
	movl	%eax, -28(%rsi)
	movl	%eax, -24(%rcx)
	movl	%eax, -24(%rsi)
	movl	%eax, -20(%rcx)
	movl	%eax, -20(%rsi)
	movl	%eax, -16(%rcx)
	movl	%eax, -16(%rsi)
	movl	%eax, -12(%rcx)
	movl	%eax, -12(%rsi)
	movl	%eax, -8(%rcx)
	movl	%eax, -8(%rsi)
	movl	%eax, -4(%rcx)
	movl	%eax, -4(%rsi)
	movl	%eax, (%rcx)
	movl	%eax, (%rsi)
	addq	$32, %rsi
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB0_72
.LBB0_122:                              # %.preheader442
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	js	.LBB0_129
# BB#123:                               # %.preheader441.lr.ph
	testl	%eax, %eax
	js	.LBB0_129
# BB#124:                               # %.preheader441.preheader
	movq	suboptalign11.used(%rip), %r12
	movl	%eax, %eax
	leaq	4(,%rax,4), %rbx
	movl	180(%rsp), %r14d        # 4-byte Reload
	leaq	-1(%r14), %r15
	movq	%r14, %r13
	xorl	%ebp, %ebp
	andq	$7, %r13
	je	.LBB0_126
	.p2align	4, 0x90
.LBB0_125:                              # %.preheader441.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB0_125
.LBB0_126:                              # %.preheader441.prol.loopexit
	cmpq	$7, %r15
	jb	.LBB0_129
# BB#127:                               # %.preheader441.preheader.new
	subq	%rbp, %r14
	leaq	56(%r12,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB0_128:                              # %.preheader441
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-48(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-40(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-32(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-24(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-16(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-8(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	addq	$64, %rbp
	addq	$-8, %r14
	jne	.LBB0_128
.LBB0_129:                              # %.preheader440
	movl	48(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB0_175
# BB#130:                               # %.lr.ph.preheader
	movslq	%eax, %r14
	movsd	.LCPI0_4(%rip), %xmm2   # xmm2 = mem[0],zero
	xorl	%ebx, %ebx
.LBB0_131:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_139 Depth 2
                                        #       Child Loop BB0_144 Depth 3
                                        #       Child Loop BB0_148 Depth 3
                                        #       Child Loop BB0_154 Depth 3
                                        #       Child Loop BB0_158 Depth 3
                                        #     Child Loop BB0_168 Depth 2
	movq	suboptalign11.shuryo(%rip), %rax
	movq	%rbx, %rbp
	shlq	$5, %rbp
	movss	8(%rax,%rbp), %xmm0     # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm1
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_175
# BB#132:                               #   in Loop: Header=BB0_131 Depth=1
	movq	stderr(%rip), %rdi
	movl	(%rax,%rbp), %ecx
	movl	4(%rax,%rbp), %r8d
	movl	$.L.str.13, %esi
	movb	$1, %al
	movl	%ebx, %edx
	callq	fprintf
	movq	suboptalign11.used(%rip), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	suboptalign11.mseq1(%rip), %rdx
	movq	suboptalign11.mseq2(%rip), %rcx
	movq	suboptalign11.ijpi(%rip), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	suboptalign11.ijpj(%rip), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	suboptalign11.shuryo(%rip), %rax
	movl	(%rax,%rbp), %esi
	movl	4(%rax,%rbp), %eax
	movslq	%esi, %rdi
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movslq	%eax, %rdi
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	gentracking.res1(%rip), %r15
	testq	%r15, %r15
	movq	%r14, 96(%rsp)          # 8-byte Spill
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%esi, 108(%rsp)         # 4-byte Spill
	movl	%eax, 184(%rsp)         # 4-byte Spill
	je	.LBB0_134
# BB#133:                               # %._crit_edge75.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movq	gentracking.res2(%rip), %r13
	jmp	.LBB0_135
.LBB0_134:                              #   in Loop: Header=BB0_131 Depth=1
	movl	$5000000, %edi          # imm = 0x4C4B40
	movl	$1, %esi
	callq	calloc
	movq	%rax, %r15
	movq	%r15, gentracking.res1(%rip)
	movl	$5000000, %edi          # imm = 0x4C4B40
	movl	$1, %esi
	callq	calloc
	movq	%rax, %r13
	movq	%r13, gentracking.res2(%rip)
.LBB0_135:                              #   in Loop: Header=BB0_131 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %r12
	movq	120(%rsp), %rbx         # 8-byte Reload
	movq	(%rbx), %rdi
	callq	strlen
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movslq	%r12d, %rcx
	addq	%rcx, %r15
	cltq
	movb	$0, (%rax,%r15)
	addq	%rcx, %r13
	movq	%rax, %rdx
	movb	$0, (%rax,%r13)
	movq	136(%rsp), %r10         # 8-byte Reload
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%r10,%rax,8), %rax
	movq	128(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, (%rax,%rcx,4)
	movq	.LCPI0_4(%rip), %xmm2   # xmm2 = mem[0],zero
	jne	.LBB0_174
# BB#136:                               # %.preheader.i
                                        #   in Loop: Header=BB0_131 Depth=1
	addq	%rdx, %r15
	addq	%rdx, %r13
	addl	%r12d, %edx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	js	.LBB0_137
# BB#138:                               # %.lr.ph31.preheader.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	128(%rsp), %rbp         # 8-byte Reload
	movl	(%rax,%rbp,4), %edx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movl	(%rax,%rbp,4), %eax
	xorl	%r11d, %r11d
	movl	108(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %ebp
	movl	184(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %ebx
	.p2align	4, 0x90
.LBB0_139:                              # %.lr.ph31.i
                                        #   Parent Loop BB0_131 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_144 Depth 3
                                        #       Child Loop BB0_148 Depth 3
                                        #       Child Loop BB0_154 Depth 3
                                        #       Child Loop BB0_158 Depth 3
	movl	%edx, %ecx
	movl	%eax, %r12d
	movslq	%ecx, %r14
	movq	(%r10,%r14,8), %rax
	movslq	%r12d, %r9
	cmpl	$0, (%rax,%r9,4)
	jne	.LBB0_174
# BB#140:                               #   in Loop: Header=BB0_139 Depth=2
	movl	%ebp, %eax
	subl	%ecx, %eax
	decl	%eax
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	je	.LBB0_150
# BB#141:                               # %.lr.ph13.preheader.i
                                        #   in Loop: Header=BB0_139 Depth=2
	movslq	%eax, %rdx
	movl	$-2, %r10d
	subl	%ecx, %r10d
	addl	%ebp, %r10d
	leal	-2(%rbp), %r8d
	subl	%ecx, %r8d
	movl	%edx, %eax
	andl	$3, %eax
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	je	.LBB0_142
# BB#143:                               # %.lr.ph13.i.prol.preheader
                                        #   in Loop: Header=BB0_139 Depth=2
	movl	%r12d, 8(%rsp)          # 4-byte Spill
	leaq	(%r14,%rdx), %rdi
	negl	%eax
	xorl	%ebx, %ebx
	movq	56(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_144:                              # %.lr.ph13.i.prol
                                        #   Parent Loop BB0_131 Depth=1
                                        #     Parent Loop BB0_139 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12), %rbp
	addq	%rdi, %rbp
	movzbl	(%rbx,%rbp), %ecx
	movb	%cl, -1(%r15,%rbx)
	movb	$45, -1(%r13,%rbx)
	decq	%rbx
	cmpl	%ebx, %eax
	jne	.LBB0_144
# BB#145:                               # %.lr.ph13.i.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB0_139 Depth=2
	addq	%rbx, %rdx
	leaq	(%r13,%rbx), %rbp
	addq	%r15, %rbx
	movl	8(%rsp), %r12d          # 4-byte Reload
	jmp	.LBB0_146
	.p2align	4, 0x90
.LBB0_142:                              #   in Loop: Header=BB0_139 Depth=2
	movq	%r13, %rbp
	movq	%r15, %rbx
.LBB0_146:                              # %.lr.ph13.i.prol.loopexit
                                        #   in Loop: Header=BB0_139 Depth=2
	negq	%r10
	cmpl	$3, %r8d
	movq	56(%rsp), %rdi          # 8-byte Reload
	jb	.LBB0_149
# BB#147:                               # %.lr.ph13.preheader.i.new
                                        #   in Loop: Header=BB0_139 Depth=2
	movl	%edx, %r8d
	negl	%r8d
	addq	%r14, %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_148:                              # %.lr.ph13.i
                                        #   Parent Loop BB0_131 Depth=1
                                        #     Parent Loop BB0_139 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdi), %rcx
	addq	%rax, %rcx
	movzbl	(%rdx,%rcx), %ecx
	movb	%cl, -1(%rbx,%rax)
	movb	$45, -1(%rbp,%rax)
	movq	(%rdi), %rcx
	addq	%rax, %rcx
	movzbl	-1(%rdx,%rcx), %ecx
	movb	%cl, -2(%rbx,%rax)
	movb	$45, -2(%rbp,%rax)
	movq	(%rdi), %rcx
	addq	%rax, %rcx
	movzbl	-2(%rdx,%rcx), %ecx
	movb	%cl, -3(%rbx,%rax)
	movb	$45, -3(%rbp,%rax)
	movq	(%rdi), %rcx
	addq	%rax, %rcx
	movzbl	-3(%rdx,%rcx), %ecx
	movb	%cl, -4(%rbx,%rax)
	movb	$45, -4(%rbp,%rax)
	addq	$-4, %rax
	cmpl	%eax, %r8d
	jne	.LBB0_148
.LBB0_149:                              # %._crit_edge14.loopexit.i
                                        #   in Loop: Header=BB0_139 Depth=2
	leaq	-1(%r15,%r10), %r15
	leaq	-1(%r13,%r10), %r13
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	notl	%eax
	addl	%eax, %r11d
	movq	72(%rsp), %rbp          # 8-byte Reload
	addl	%ebp, %r11d
	movq	136(%rsp), %r10         # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB0_150:                              # %._crit_edge14.i
                                        #   in Loop: Header=BB0_139 Depth=2
	movl	%ebx, %eax
	subl	%r12d, %eax
	decl	%eax
	je	.LBB0_160
# BB#151:                               # %.lr.ph21.preheader.i
                                        #   in Loop: Header=BB0_139 Depth=2
	movslq	%eax, %rdx
	movl	$-2, %r8d
	subl	%r12d, %r8d
	addl	%ebx, %r8d
	leal	-2(%rbx), %edi
	subl	%r12d, %edi
	movl	%edx, %eax
	movl	%ecx, %r10d
	andl	$3, %eax
	je	.LBB0_152
# BB#153:                               # %.lr.ph21.i.prol.preheader
                                        #   in Loop: Header=BB0_139 Depth=2
	leaq	(%r9,%rdx), %rbp
	negl	%eax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_154:                              # %.lr.ph21.i.prol
                                        #   Parent Loop BB0_131 Depth=1
                                        #     Parent Loop BB0_139 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movb	$45, -1(%r15,%rbx)
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rsi
	addq	%rbp, %rsi
	movzbl	(%rbx,%rsi), %ecx
	movq	120(%rsp), %rsi         # 8-byte Reload
	movb	%cl, -1(%r13,%rbx)
	decq	%rbx
	cmpl	%ebx, %eax
	jne	.LBB0_154
# BB#155:                               # %.lr.ph21.i.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB0_139 Depth=2
	addq	%rbx, %rdx
	leaq	(%r13,%rbx), %rbp
	addq	%r15, %rbx
	jmp	.LBB0_156
	.p2align	4, 0x90
.LBB0_152:                              #   in Loop: Header=BB0_139 Depth=2
	movq	%r13, %rbp
	movq	%r15, %rbx
.LBB0_156:                              # %.lr.ph21.i.prol.loopexit
                                        #   in Loop: Header=BB0_139 Depth=2
	negq	%r8
	cmpl	$3, %edi
	jb	.LBB0_159
# BB#157:                               # %.lr.ph21.preheader.i.new
                                        #   in Loop: Header=BB0_139 Depth=2
	movl	%edx, %edi
	negl	%edi
	addq	%r9, %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_158:                              # %.lr.ph21.i
                                        #   Parent Loop BB0_131 Depth=1
                                        #     Parent Loop BB0_139 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movb	$45, -1(%rbx,%rax)
	movq	(%rsi), %rcx
	addq	%rax, %rcx
	movzbl	(%rdx,%rcx), %ecx
	movb	%cl, -1(%rbp,%rax)
	movb	$45, -2(%rbx,%rax)
	movq	(%rsi), %rcx
	addq	%rax, %rcx
	movzbl	-1(%rdx,%rcx), %ecx
	movb	%cl, -2(%rbp,%rax)
	movb	$45, -3(%rbx,%rax)
	movq	(%rsi), %rcx
	addq	%rax, %rcx
	movzbl	-2(%rdx,%rcx), %ecx
	movb	%cl, -3(%rbp,%rax)
	movb	$45, -4(%rbx,%rax)
	movq	(%rsi), %rcx
	addq	%rax, %rcx
	movzbl	-3(%rdx,%rcx), %ecx
	movb	%cl, -4(%rbp,%rax)
	addq	$-4, %rax
	cmpl	%eax, %edi
	jne	.LBB0_158
.LBB0_159:                              # %._crit_edge22.loopexit.i
                                        #   in Loop: Header=BB0_139 Depth=2
	leaq	-1(%r15,%r8), %r15
	leaq	-1(%r13,%r8), %r13
	movl	%r12d, %eax
	notl	%eax
	movq	24(%rsp), %rbx          # 8-byte Reload
	addl	%ebx, %eax
	addl	%eax, %r11d
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	%r10d, %ecx
	movq	136(%rsp), %r10         # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
.LBB0_160:                              # %._crit_edge22.i
                                        #   in Loop: Header=BB0_139 Depth=2
	testl	%ebp, %ebp
	jle	.LBB0_162
# BB#161:                               # %._crit_edge22.i
                                        #   in Loop: Header=BB0_139 Depth=2
	testl	%ebx, %ebx
	jle	.LBB0_162
# BB#163:                               #   in Loop: Header=BB0_139 Depth=2
	movq	(%rdi), %rax
	movb	(%rax,%r14), %al
	movb	%al, -1(%r15)
	decq	%r15
	movq	(%rsi), %rax
	movb	(%rax,%r9), %al
	movb	%al, -1(%r13)
	decq	%r13
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	movl	(%rax,%r9,4), %edx
	movl	localstop(%rip), %r8d
	cmpl	%r8d, %edx
	je	.LBB0_164
# BB#165:                               #   in Loop: Header=BB0_139 Depth=2
	addl	$2, %r11d
	cmpl	152(%rsp), %r11d        # 4-byte Folded Reload
	jg	.LBB0_167
# BB#166:                               #   in Loop: Header=BB0_139 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	movl	(%rax,%r9,4), %eax
	cmpl	%r8d, %eax
	movl	%ecx, %ebp
	movl	%r12d, %ebx
	jne	.LBB0_139
	jmp	.LBB0_167
.LBB0_162:                              # %._crit_edge22.i..lr.ph.i419_crit_edge
                                        #   in Loop: Header=BB0_131 Depth=1
	movl	localstop(%rip), %r8d
	jmp	.LBB0_167
.LBB0_164:                              #   in Loop: Header=BB0_131 Depth=1
	movl	%edx, %r8d
.LBB0_167:                              # %.lr.ph.i419
                                        #   in Loop: Header=BB0_131 Depth=1
	cmpl	$-1, %ecx
	movl	$0, %edx
	cmovel	%edx, %ecx
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	%ecx, (%rax)
	cmpl	$-1, %r12d
	cmovel	%edx, %r12d
	movq	208(%rsp), %rax         # 8-byte Reload
	movl	%r12d, (%rax)
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	128(%rsp), %rsi         # 8-byte Reload
	movl	(%rax,%rsi,4), %edx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movl	(%rax,%rsi,4), %esi
	movl	$2, %eax
	movq	168(%rsp), %r14         # 8-byte Reload
	movq	88(%rsp), %r12          # 8-byte Reload
	movl	108(%rsp), %ecx         # 4-byte Reload
	movl	184(%rsp), %ebx         # 4-byte Reload
	.p2align	4, 0x90
.LBB0_168:                              #   Parent Loop BB0_131 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %edi
	testl	%edi, %edi
	movl	%edx, %ecx
	movl	%esi, %edi
	movslq	%ecx, %rsi
	movq	(%r10,%rsi,8), %rdx
	movslq	%edi, %rbp
	movl	$1, (%rdx,%rbp,4)
	jle	.LBB0_173
# BB#169:                               #   in Loop: Header=BB0_168 Depth=2
	testl	%ebx, %ebx
	jle	.LBB0_173
# BB#170:                               #   in Loop: Header=BB0_168 Depth=2
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%rsi,8), %rdx
	movl	(%rdx,%rbp,4), %edx
	cmpl	%r8d, %edx
	je	.LBB0_173
# BB#171:                               #   in Loop: Header=BB0_168 Depth=2
	cmpl	152(%rsp), %eax         # 4-byte Folded Reload
	jg	.LBB0_173
# BB#172:                               #   in Loop: Header=BB0_168 Depth=2
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rsi,8), %rsi
	movl	(%rsi,%rbp,4), %esi
	addl	$2, %eax
	cmpl	%r8d, %esi
	movl	%edi, %ebx
	jne	.LBB0_168
	jmp	.LBB0_173
.LBB0_137:                              # %._crit_edge32.thread.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	movq	208(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	movq	168(%rsp), %r14         # 8-byte Reload
	movq	88(%rsp), %r12          # 8-byte Reload
.LBB0_173:                              #   in Loop: Header=BB0_131 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	(%r12), %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	stderr(%rip), %rdi
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	movq	%r13, %rcx
	callq	fprintf
	movq	suboptalign11.mseq1(%rip), %rax
	movq	(%rax), %rbx
	movq	suboptalign11.mseq2(%rip), %rax
	movq	(%rax), %rbp
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r15d
	movq	208(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r13d
	movq	suboptalign11.shuryo(%rip), %rax
	cvttss2si	8(%rax,%r14), %r12d
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, (%rsp)
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	232(%rsp), %rdx         # 8-byte Reload
	movl	%r15d, %ecx
	movl	%r13d, %r8d
	movl	%r12d, %r9d
	callq	putlocalhom3
	movq	.LCPI0_4(%rip), %xmm2   # xmm2 = mem[0],zero
.LBB0_174:                              # %gentracking.exit.thread
                                        #   in Loop: Header=BB0_131 Depth=1
	movq	192(%rsp), %rbx         # 8-byte Reload
	incq	%rbx
	movq	96(%rsp), %r14          # 8-byte Reload
	cmpq	%r14, %rbx
	jl	.LBB0_131
.LBB0_175:                              # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_176:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_177 Depth 2
	movq	stderr(%rip), %rcx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_177:                              #   Parent Loop BB0_176 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	suboptalign11.used(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	movl	(%rax,%rbp,4), %edx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	incq	%rbp
	movq	stderr(%rip), %rcx
	cmpq	$20, %rbp
	jne	.LBB0_177
# BB#178:                               #   in Loop: Header=BB0_176 Depth=1
	movl	$10, %edi
	movq	%rcx, %rsi
	callq	fputc
	incq	%rbx
	cmpq	$20, %rbx
	jne	.LBB0_176
# BB#179:
	movq	suboptalign11.mseq1(%rip), %rax
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	228(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %ecx
	jg	.LBB0_181
# BB#180:
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB0_181
.LBB0_182:
	movss	36(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_119:                              # %vector.ph667
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r9), %rbx
	leaq	16(%r10), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB0_120:                              # %vector.body649
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$32, %rbx
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB0_120
# BB#121:                               # %middle.block650
	testq	%r8, %r8
	jne	.LBB0_67
	jmp	.LBB0_122
.LBB0_52:
	xorpd	%xmm0, %xmm0
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
	xorl	%r15d, %r15d
	movss	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	xorl	%r14d, %r14d
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_54
.LBB0_181:
	movq	stderr(%rip), %rdi
	movl	$.L.str.16, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.17, %edi
	callq	ErrorExit
	jmp	.LBB0_182
.LBB0_43:
	movq	160(%rsp), %r14         # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movb	24(%rsp), %r11b         # 1-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_36
.Lfunc_end0:
	.size	suboptalign11, .Lfunc_end0-suboptalign11
	.cfi_endproc

	.p2align	4, 0x90
	.type	compshuryo,@function
compshuryo:                             # @compshuryo
	.cfi_startproc
# BB#0:
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	xorl	%ecx, %ecx
	ucomiss	%xmm0, %xmm1
	seta	%cl
	ucomiss	%xmm1, %xmm0
	movl	$-1, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end1:
	.size	compshuryo, .Lfunc_end1-compshuryo
	.cfi_endproc

	.type	suboptalign11.used,@object # @suboptalign11.used
	.local	suboptalign11.used
	.comm	suboptalign11.used,8,8
	.type	suboptalign11.mi,@object # @suboptalign11.mi
	.local	suboptalign11.mi
	.comm	suboptalign11.mi,4,4
	.type	suboptalign11.m,@object # @suboptalign11.m
	.local	suboptalign11.m
	.comm	suboptalign11.m,8,8
	.type	suboptalign11.Mi,@object # @suboptalign11.Mi
	.local	suboptalign11.Mi
	.comm	suboptalign11.Mi,4,4
	.type	suboptalign11.largeM,@object # @suboptalign11.largeM
	.local	suboptalign11.largeM
	.comm	suboptalign11.largeM,8,8
	.type	suboptalign11.ijpi,@object # @suboptalign11.ijpi
	.local	suboptalign11.ijpi
	.comm	suboptalign11.ijpi,8,8
	.type	suboptalign11.ijpj,@object # @suboptalign11.ijpj
	.local	suboptalign11.ijpj
	.comm	suboptalign11.ijpj,8,8
	.type	suboptalign11.mpi,@object # @suboptalign11.mpi
	.local	suboptalign11.mpi
	.comm	suboptalign11.mpi,4,4
	.type	suboptalign11.mp,@object # @suboptalign11.mp
	.local	suboptalign11.mp
	.comm	suboptalign11.mp,8,8
	.type	suboptalign11.Mpi,@object # @suboptalign11.Mpi
	.local	suboptalign11.Mpi
	.comm	suboptalign11.Mpi,4,4
	.type	suboptalign11.Mp,@object # @suboptalign11.Mp
	.local	suboptalign11.Mp
	.comm	suboptalign11.Mp,8,8
	.type	suboptalign11.w1,@object # @suboptalign11.w1
	.local	suboptalign11.w1
	.comm	suboptalign11.w1,8,8
	.type	suboptalign11.w2,@object # @suboptalign11.w2
	.local	suboptalign11.w2
	.comm	suboptalign11.w2,8,8
	.type	suboptalign11.initverticalw,@object # @suboptalign11.initverticalw
	.local	suboptalign11.initverticalw
	.comm	suboptalign11.initverticalw,8,8
	.type	suboptalign11.lastverticalw,@object # @suboptalign11.lastverticalw
	.local	suboptalign11.lastverticalw
	.comm	suboptalign11.lastverticalw,8,8
	.type	suboptalign11.mseq1,@object # @suboptalign11.mseq1
	.local	suboptalign11.mseq1
	.comm	suboptalign11.mseq1,8,8
	.type	suboptalign11.mseq2,@object # @suboptalign11.mseq2
	.local	suboptalign11.mseq2
	.comm	suboptalign11.mseq2,8,8
	.type	suboptalign11.cpmx1,@object # @suboptalign11.cpmx1
	.local	suboptalign11.cpmx1
	.comm	suboptalign11.cpmx1,8,8
	.type	suboptalign11.cpmx2,@object # @suboptalign11.cpmx2
	.local	suboptalign11.cpmx2
	.comm	suboptalign11.cpmx2,8,8
	.type	suboptalign11.intwork,@object # @suboptalign11.intwork
	.local	suboptalign11.intwork
	.comm	suboptalign11.intwork,8,8
	.type	suboptalign11.floatwork,@object # @suboptalign11.floatwork
	.local	suboptalign11.floatwork
	.comm	suboptalign11.floatwork,8,8
	.type	suboptalign11.orlgth1,@object # @suboptalign11.orlgth1
	.local	suboptalign11.orlgth1
	.comm	suboptalign11.orlgth1,4,4
	.type	suboptalign11.orlgth2,@object # @suboptalign11.orlgth2
	.local	suboptalign11.orlgth2
	.comm	suboptalign11.orlgth2,4,4
	.type	suboptalign11.shuryo,@object # @suboptalign11.shuryo
	.local	suboptalign11.shuryo
	.comm	suboptalign11.shuryo,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in suboptalign11\n"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"in suboptalign11 step 1\n"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"in suboptalign11 step 1.3\n"
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"in suboptalign11 step 1.4\n"
	.size	.L.str.3, 27

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"in suboptalign11 step 1.5\n"
	.size	.L.str.4, 27

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"in suboptalign11 step 1.6\n"
	.size	.L.str.5, 27

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"in suboptalign11 step 1.7\n"
	.size	.L.str.6, 27

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"in suboptalign11 step 2\n"
	.size	.L.str.7, 25

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"in suboptalign11 step 3\n"
	.size	.L.str.8, 25

	.type	localstop,@object       # @localstop
	.local	localstop
	.comm	localstop,4,4
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"shuryo[%d].i,j,wm = %d,%d,%f\n"
	.size	.L.str.9, 30

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"maxwm = %f\n"
	.size	.L.str.10, 12

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"endali = %d\n"
	.size	.L.str.11, 13

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"endalj = %d\n"
	.size	.L.str.12, 13

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"k=%d, shuryo[k].i,j,wm=%d,%d,%f go\n"
	.size	.L.str.13, 36

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%2d "
	.size	.L.str.14, 5

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str.16, 33

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.17, 14

	.type	gentracking.res1,@object # @gentracking.res1
	.local	gentracking.res1
	.comm	gentracking.res1,8,8
	.type	gentracking.res2,@object # @gentracking.res2
	.local	gentracking.res2
	.comm	gentracking.res2,8,8
	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"mseq1=%s\nmseq2=%s\n"
	.size	.L.str.18, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
