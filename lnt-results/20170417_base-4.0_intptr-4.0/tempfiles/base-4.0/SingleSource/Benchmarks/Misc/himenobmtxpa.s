	.text
	.file	"himenobmtxpa.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	64                      # 0x40
	.long	64                      # 0x40
	.long	128                     # 0x80
.LCPI0_1:
	.long	4                       # 0x4
	.long	64                      # 0x40
	.long	64                      # 0x40
	.long	128                     # 0x80
.LCPI0_2:
	.long	3                       # 0x3
	.long	64                      # 0x40
	.long	64                      # 0x40
	.long	128                     # 0x80
.LCPI0_4:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
.LCPI0_5:
	.long	1042983595              # float 0.166666672
	.long	1042983595              # float 0.166666672
	.long	1042983595              # float 0.166666672
	.long	1042983595              # float 0.166666672
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_3:
	.long	1165496320              # float 3969
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %.preheader33.us.i.us.preheader
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	xorl	%ebp, %ebp
	movl	$.L.str, %edi
	movl	$64, %esi
	movl	$64, %edx
	movl	$128, %ecx
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$63, %esi
	movl	$63, %edx
	movl	$127, %ecx
	xorl	%eax, %eax
	callq	printf
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,64,64,128]
	movups	%xmm0, p+8(%rip)
	movl	$2097152, %edi          # imm = 0x200000
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, p(%rip)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,64,64,128]
	movups	%xmm0, bnd+8(%rip)
	movl	$2097152, %edi          # imm = 0x200000
	callq	malloc
	movq	%rax, bnd(%rip)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,64,64,128]
	movups	%xmm0, wrk1+8(%rip)
	movl	$2097152, %edi          # imm = 0x200000
	callq	malloc
	movq	%rax, wrk1(%rip)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,64,64,128]
	movups	%xmm0, wrk2+8(%rip)
	movl	$2097152, %edi          # imm = 0x200000
	callq	malloc
	movq	%rax, wrk2(%rip)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [4,64,64,128]
	movups	%xmm0, a+8(%rip)
	movl	$8388608, %edi          # imm = 0x800000
	callq	malloc
	movq	%rax, a(%rip)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [3,64,64,128]
	movups	%xmm0, b+8(%rip)
	movl	$6291456, %edi          # imm = 0x600000
	callq	malloc
	movq	%rax, b(%rip)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [3,64,64,128]
	movups	%xmm0, c+8(%rip)
	movl	$6291456, %edi          # imm = 0x600000
	callq	malloc
	movq	%rax, c(%rip)
	addq	$496, %rbx              # imm = 0x1F0
	movss	.LCPI0_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_1:                                # %.preheader33.us.i.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
	movl	%ebp, %eax
	imull	%eax, %eax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	divss	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movl	$64, %eax
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB0_2:                                # %vector.body
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm1, -496(%rcx)
	movups	%xmm1, -480(%rcx)
	movups	%xmm1, -464(%rcx)
	movups	%xmm1, -448(%rcx)
	movups	%xmm1, -432(%rcx)
	movups	%xmm1, -416(%rcx)
	movups	%xmm1, -400(%rcx)
	movups	%xmm1, -384(%rcx)
	movups	%xmm1, -368(%rcx)
	movups	%xmm1, -352(%rcx)
	movups	%xmm1, -336(%rcx)
	movups	%xmm1, -320(%rcx)
	movups	%xmm1, -304(%rcx)
	movups	%xmm1, -288(%rcx)
	movups	%xmm1, -272(%rcx)
	movups	%xmm1, -256(%rcx)
	movups	%xmm1, -240(%rcx)
	movups	%xmm1, -224(%rcx)
	movups	%xmm1, -208(%rcx)
	movups	%xmm1, -192(%rcx)
	movups	%xmm1, -176(%rcx)
	movups	%xmm1, -160(%rcx)
	movups	%xmm1, -144(%rcx)
	movups	%xmm1, -128(%rcx)
	movups	%xmm1, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	%xmm1, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	%xmm1, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	%xmm1, -16(%rcx)
	movups	%xmm1, (%rcx)
	addq	$512, %rcx              # imm = 0x200
	decq	%rax
	jne	.LBB0_2
# BB#3:                                 # %._crit_edge36.us.i.loopexit.us
                                        #   in Loop: Header=BB0_1 Depth=1
	incl	%ebp
	addq	$32768, %rbx            # imm = 0x8000
	cmpl	$64, %ebp
	jne	.LBB0_1
# BB#4:                                 # %.preheader27.us.i.us.preheader
	movl	$496, %eax              # imm = 0x1F0
	addq	bnd(%rip), %rax
	xorl	%ecx, %ecx
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader27.us.i.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
	movl	$64, %edx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB0_6:                                # %vector.body417
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm0, -496(%rsi)
	movups	%xmm0, -480(%rsi)
	movups	%xmm0, -464(%rsi)
	movups	%xmm0, -448(%rsi)
	movups	%xmm0, -432(%rsi)
	movups	%xmm0, -416(%rsi)
	movups	%xmm0, -400(%rsi)
	movups	%xmm0, -384(%rsi)
	movups	%xmm0, -368(%rsi)
	movups	%xmm0, -352(%rsi)
	movups	%xmm0, -336(%rsi)
	movups	%xmm0, -320(%rsi)
	movups	%xmm0, -304(%rsi)
	movups	%xmm0, -288(%rsi)
	movups	%xmm0, -272(%rsi)
	movups	%xmm0, -256(%rsi)
	movups	%xmm0, -240(%rsi)
	movups	%xmm0, -224(%rsi)
	movups	%xmm0, -208(%rsi)
	movups	%xmm0, -192(%rsi)
	movups	%xmm0, -176(%rsi)
	movups	%xmm0, -160(%rsi)
	movups	%xmm0, -144(%rsi)
	movups	%xmm0, -128(%rsi)
	movups	%xmm0, -112(%rsi)
	movups	%xmm0, -96(%rsi)
	movups	%xmm0, -80(%rsi)
	movups	%xmm0, -64(%rsi)
	movups	%xmm0, -48(%rsi)
	movups	%xmm0, -32(%rsi)
	movups	%xmm0, -16(%rsi)
	movups	%xmm0, (%rsi)
	addq	$512, %rsi              # imm = 0x200
	decq	%rdx
	jne	.LBB0_6
# BB#7:                                 # %._crit_edge30.us.i.loopexit.us
                                        #   in Loop: Header=BB0_5 Depth=1
	incl	%ecx
	addq	$32768, %rax            # imm = 0x8000
	cmpl	$64, %ecx
	jne	.LBB0_5
# BB#8:                                 # %.preheader27.us.i73.us.preheader
	movl	$1536, %r14d            # imm = 0x600
	addq	wrk1(%rip), %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_9:                                # %.preheader27.us.i73.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_10 Depth 2
	movl	$64, %ebp
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB0_10:                               # %.preheader.us.us.i78.us
                                        #   Parent Loop BB0_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1536(%rbx), %rdi
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	callq	memset
	leaq	-1024(%rbx), %rdi
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	callq	memset
	leaq	-512(%rbx), %rdi
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	callq	memset
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	movq	%rbx, %rdi
	callq	memset
	addq	$2048, %rbx             # imm = 0x800
	addq	$-4, %rbp
	jne	.LBB0_10
# BB#11:                                # %._crit_edge30.us.i76.loopexit.us
                                        #   in Loop: Header=BB0_9 Depth=1
	incl	%r15d
	addq	$32768, %r14            # imm = 0x8000
	cmpl	$64, %r15d
	jne	.LBB0_9
# BB#12:                                # %mat_set.exit81
	movl	wrk2+12(%rip), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB0_24
# BB#13:                                # %.preheader27.lr.ph.i83
	movl	wrk2+16(%rip), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB0_24
# BB#14:                                # %.preheader27.us.preheader.i85
	movl	wrk2+20(%rip), %r14d
	testl	%r14d, %r14d
	jle	.LBB0_24
# BB#15:                                # %.preheader27.us.i87.us.preheader
	movq	wrk2(%rip), %r12
	leal	-1(%r14), %eax
	leaq	4(,%rax,4), %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	-1(%rax), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%eax, %r15d
	andl	$3, %r15d
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r14d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	leal	(,%r14,4), %ebp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	%ebp, (%rsp)            # 4-byte Spill
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader27.us.i87.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_18 Depth 2
                                        #     Child Loop BB0_22 Depth 2
	movl	%edx, 64(%rsp)          # 4-byte Spill
	testq	%r15, %r15
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	je	.LBB0_19
# BB#17:                                # %.preheader.us.us.i92.us.prol.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	movl	%ecx, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_18:                               # %.preheader.us.us.i92.us.prol
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	leaq	(%r12,%rbp,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	incq	%r13
	addl	%r14d, %ebp
	cmpq	%r13, %r15
	jne	.LBB0_18
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_19:                               #   in Loop: Header=BB0_16 Depth=1
	xorl	%r13d, %r13d
.LBB0_20:                               # %.preheader.us.us.i92.us.prol.loopexit
                                        #   in Loop: Header=BB0_16 Depth=1
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	movl	(%rsp), %ebp            # 4-byte Reload
	jb	.LBB0_23
# BB#21:                                # %.preheader27.us.i87.us.new
                                        #   in Loop: Header=BB0_16 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	subq	%r13, %r15
	leal	3(%r13), %edx
	movq	32(%rsp), %rax          # 8-byte Reload
	imull	%eax, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	addl	%ecx, %edx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	leal	2(%r13), %edx
	imull	%eax, %edx
	addl	%ecx, %edx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%eax, %edx
	imull	%r13d, %edx
	addl	%ecx, %edx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	incl	%r13d
	imull	%eax, %r13d
	addl	%ecx, %r13d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_22:                               # %.preheader.us.us.i92.us
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r14), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	leal	(%r13,%r14), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r14), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r14), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	addl	%ebp, %r14d
	addq	$-4, %r15
	jne	.LBB0_22
.LBB0_23:                               # %._crit_edge30.us.i90.loopexit.us
                                        #   in Loop: Header=BB0_16 Depth=1
	movl	64(%rsp), %edx          # 4-byte Reload
	incl	%edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	addl	4(%rsp), %ecx           # 4-byte Folded Reload
	cmpl	24(%rsp), %edx          # 4-byte Folded Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	jne	.LBB0_16
.LBB0_24:                               # %mat_set.exit95
	movl	a+12(%rip), %r8d
	testl	%r8d, %r8d
	jle	.LBB0_85
# BB#25:                                # %.preheader27.lr.ph.i97
	movl	a+16(%rip), %r12d
	testl	%r12d, %r12d
	jle	.LBB0_85
# BB#26:                                # %.preheader27.us.preheader.i99
	movl	a+20(%rip), %edx
	testl	%edx, %edx
	jle	.LBB0_85
# BB#27:                                # %.preheader27.us.i101.us.preheader
	movq	a(%rip), %r13
	movl	%r12d, %r9d
	imull	%edx, %r9d
	movl	%edx, %r14d
	andl	$7, %r14d
	movq	%rdx, %rsi
	subq	%r14, %rsi
	leaq	16(%r13), %r15
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB0_28:                               # %.preheader27.us.i101.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_29 Depth 2
                                        #       Child Loop BB0_33 Depth 3
                                        #       Child Loop BB0_37 Depth 3
	movl	%r10d, %edi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_29:                               # %.preheader.us.us.i106.us
                                        #   Parent Loop BB0_28 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_33 Depth 3
                                        #       Child Loop BB0_37 Depth 3
	cmpl	$8, %edx
	movslq	%edi, %rbx
	jb	.LBB0_35
# BB#31:                                # %min.iters.checked433
                                        #   in Loop: Header=BB0_29 Depth=2
	testq	%rsi, %rsi
	je	.LBB0_35
# BB#32:                                # %vector.body430.preheader
                                        #   in Loop: Header=BB0_29 Depth=2
	leaq	(%r15,%rbx,4), %rcx
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB0_33:                               # %vector.body430
                                        #   Parent Loop BB0_28 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	%xmm0, -16(%rcx)
	movups	%xmm0, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB0_33
# BB#34:                                # %middle.block431
                                        #   in Loop: Header=BB0_29 Depth=2
	testl	%r14d, %r14d
	movq	%rsi, %rbp
	jne	.LBB0_36
	jmp	.LBB0_38
	.p2align	4, 0x90
.LBB0_35:                               #   in Loop: Header=BB0_29 Depth=2
	xorl	%ebp, %ebp
.LBB0_36:                               # %scalar.ph432.preheader
                                        #   in Loop: Header=BB0_29 Depth=2
	addq	%rbp, %rbx
	leaq	(%r13,%rbx,4), %rbx
	movq	%rdx, %rcx
	subq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_37:                               # %scalar.ph432
                                        #   Parent Loop BB0_28 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	addq	$4, %rbx
	decq	%rcx
	jne	.LBB0_37
.LBB0_38:                               # %._crit_edge.us.us.i107.us
                                        #   in Loop: Header=BB0_29 Depth=2
	incq	%rax
	addl	%edx, %edi
	cmpq	%r12, %rax
	jne	.LBB0_29
# BB#39:                                # %._crit_edge30.us.i104.loopexit.us
                                        #   in Loop: Header=BB0_28 Depth=1
	incl	%r11d
	addl	%r9d, %r10d
	cmpl	%r8d, %r11d
	jne	.LBB0_28
# BB#40:                                # %.preheader27.lr.ph.i111
	testl	%r12d, %r12d
	jle	.LBB0_85
# BB#41:                                # %.preheader27.us.preheader.i113
	movl	a+20(%rip), %edx
	testl	%edx, %edx
	jle	.LBB0_85
# BB#42:                                # %.preheader27.us.i115.us.preheader
	movq	a(%rip), %r13
	movl	%edx, %r14d
	andl	$7, %r14d
	movq	%rdx, %rsi
	subq	%r14, %rsi
	leaq	16(%r13), %r15
	movl	%r8d, %r10d
	imull	%r12d, %r10d
	imull	%edx, %r10d
	movl	%r12d, %r9d
	imull	%edx, %r9d
	xorl	%r11d, %r11d
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB0_43:                               # %.preheader27.us.i115.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_44 Depth 2
                                        #       Child Loop BB0_48 Depth 3
                                        #       Child Loop BB0_52 Depth 3
	movl	%r10d, %ebx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_44:                               # %.preheader.us.us.i120.us
                                        #   Parent Loop BB0_43 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_48 Depth 3
                                        #       Child Loop BB0_52 Depth 3
	cmpl	$8, %edx
	movslq	%ebx, %rax
	jb	.LBB0_50
# BB#46:                                # %min.iters.checked447
                                        #   in Loop: Header=BB0_44 Depth=2
	testq	%rsi, %rsi
	je	.LBB0_50
# BB#47:                                # %vector.body443.preheader
                                        #   in Loop: Header=BB0_44 Depth=2
	leaq	(%r15,%rax,4), %rcx
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB0_48:                               # %vector.body443
                                        #   Parent Loop BB0_43 Depth=1
                                        #     Parent Loop BB0_44 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	%xmm0, -16(%rcx)
	movups	%xmm0, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB0_48
# BB#49:                                # %middle.block444
                                        #   in Loop: Header=BB0_44 Depth=2
	testl	%r14d, %r14d
	movq	%rsi, %rbp
	jne	.LBB0_51
	jmp	.LBB0_53
	.p2align	4, 0x90
.LBB0_50:                               #   in Loop: Header=BB0_44 Depth=2
	xorl	%ebp, %ebp
.LBB0_51:                               # %scalar.ph445.preheader
                                        #   in Loop: Header=BB0_44 Depth=2
	addq	%rbp, %rax
	leaq	(%r13,%rax,4), %rax
	movq	%rdx, %rcx
	subq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_52:                               # %scalar.ph445
                                        #   Parent Loop BB0_43 Depth=1
                                        #     Parent Loop BB0_44 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$1065353216, (%rax)     # imm = 0x3F800000
	addq	$4, %rax
	decq	%rcx
	jne	.LBB0_52
.LBB0_53:                               # %._crit_edge.us.us.i121.us
                                        #   in Loop: Header=BB0_44 Depth=2
	incq	%rdi
	addl	%edx, %ebx
	cmpq	%r12, %rdi
	jne	.LBB0_44
# BB#54:                                # %._crit_edge30.us.i118.loopexit.us
                                        #   in Loop: Header=BB0_43 Depth=1
	incl	%r11d
	addl	%r9d, %r10d
	cmpl	%r8d, %r11d
	jne	.LBB0_43
# BB#55:                                # %.preheader27.lr.ph.i125
	testl	%r12d, %r12d
	jle	.LBB0_85
# BB#56:                                # %.preheader27.us.preheader.i127
	movl	a+20(%rip), %edx
	testl	%edx, %edx
	jle	.LBB0_85
# BB#57:                                # %.preheader27.us.i129.us.preheader
	movq	a(%rip), %r13
	movl	%edx, %r14d
	andl	$7, %r14d
	movq	%rdx, %rsi
	subq	%r14, %rsi
	leaq	16(%r13), %r15
	movl	%r8d, %r10d
	imull	%r12d, %r10d
	imull	%edx, %r10d
	addl	%r10d, %r10d
	movl	%r12d, %r9d
	imull	%edx, %r9d
	xorl	%r11d, %r11d
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB0_58:                               # %.preheader27.us.i129.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_59 Depth 2
                                        #       Child Loop BB0_63 Depth 3
                                        #       Child Loop BB0_67 Depth 3
	movl	%r10d, %eax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_59:                               # %.preheader.us.us.i134.us
                                        #   Parent Loop BB0_58 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_63 Depth 3
                                        #       Child Loop BB0_67 Depth 3
	cmpl	$8, %edx
	movslq	%eax, %rbx
	jb	.LBB0_65
# BB#61:                                # %min.iters.checked464
                                        #   in Loop: Header=BB0_59 Depth=2
	testq	%rsi, %rsi
	je	.LBB0_65
# BB#62:                                # %vector.body460.preheader
                                        #   in Loop: Header=BB0_59 Depth=2
	leaq	(%r15,%rbx,4), %rcx
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB0_63:                               # %vector.body460
                                        #   Parent Loop BB0_58 Depth=1
                                        #     Parent Loop BB0_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	%xmm0, -16(%rcx)
	movups	%xmm0, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB0_63
# BB#64:                                # %middle.block461
                                        #   in Loop: Header=BB0_59 Depth=2
	testl	%r14d, %r14d
	movq	%rsi, %rbp
	jne	.LBB0_66
	jmp	.LBB0_68
	.p2align	4, 0x90
.LBB0_65:                               #   in Loop: Header=BB0_59 Depth=2
	xorl	%ebp, %ebp
.LBB0_66:                               # %scalar.ph462.preheader
                                        #   in Loop: Header=BB0_59 Depth=2
	addq	%rbp, %rbx
	leaq	(%r13,%rbx,4), %rbx
	movq	%rdx, %rcx
	subq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_67:                               # %scalar.ph462
                                        #   Parent Loop BB0_58 Depth=1
                                        #     Parent Loop BB0_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	addq	$4, %rbx
	decq	%rcx
	jne	.LBB0_67
.LBB0_68:                               # %._crit_edge.us.us.i135.us
                                        #   in Loop: Header=BB0_59 Depth=2
	incq	%rdi
	addl	%edx, %eax
	cmpq	%r12, %rdi
	jne	.LBB0_59
# BB#69:                                # %._crit_edge30.us.i132.loopexit.us
                                        #   in Loop: Header=BB0_58 Depth=1
	incl	%r11d
	addl	%r9d, %r10d
	cmpl	%r8d, %r11d
	jne	.LBB0_58
# BB#70:                                # %.preheader27.lr.ph.i139
	testl	%r12d, %r12d
	jle	.LBB0_85
# BB#71:                                # %.preheader27.us.preheader.i141
	movl	a+20(%rip), %edx
	testl	%edx, %edx
	jle	.LBB0_85
# BB#72:                                # %.preheader27.us.i143.us.preheader
	movq	a(%rip), %r13
	movl	%edx, %r14d
	andl	$7, %r14d
	movq	%rdx, %rsi
	subq	%r14, %rsi
	leaq	16(%r13), %r15
	movl	%r8d, %eax
	imull	%r12d, %eax
	imull	%edx, %eax
	leal	(%rax,%rax,2), %r10d
	movl	%r12d, %r9d
	imull	%edx, %r9d
	xorl	%r11d, %r11d
	movaps	.LCPI0_5(%rip), %xmm0   # xmm0 = [1.666667e-01,1.666667e-01,1.666667e-01,1.666667e-01]
	.p2align	4, 0x90
.LBB0_73:                               # %.preheader27.us.i143.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_74 Depth 2
                                        #       Child Loop BB0_78 Depth 3
                                        #       Child Loop BB0_82 Depth 3
	movl	%r10d, %eax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_74:                               # %.preheader.us.us.i148.us
                                        #   Parent Loop BB0_73 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_78 Depth 3
                                        #       Child Loop BB0_82 Depth 3
	cmpl	$8, %edx
	movslq	%eax, %rbx
	jb	.LBB0_80
# BB#76:                                # %min.iters.checked481
                                        #   in Loop: Header=BB0_74 Depth=2
	testq	%rsi, %rsi
	je	.LBB0_80
# BB#77:                                # %vector.body477.preheader
                                        #   in Loop: Header=BB0_74 Depth=2
	leaq	(%r15,%rbx,4), %rcx
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB0_78:                               # %vector.body477
                                        #   Parent Loop BB0_73 Depth=1
                                        #     Parent Loop BB0_74 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	%xmm0, -16(%rcx)
	movups	%xmm0, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB0_78
# BB#79:                                # %middle.block478
                                        #   in Loop: Header=BB0_74 Depth=2
	testl	%r14d, %r14d
	movq	%rsi, %rbp
	jne	.LBB0_81
	jmp	.LBB0_83
	.p2align	4, 0x90
.LBB0_80:                               #   in Loop: Header=BB0_74 Depth=2
	xorl	%ebp, %ebp
.LBB0_81:                               # %scalar.ph479.preheader
                                        #   in Loop: Header=BB0_74 Depth=2
	addq	%rbp, %rbx
	leaq	(%r13,%rbx,4), %rbx
	movq	%rdx, %rcx
	subq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_82:                               # %scalar.ph479
                                        #   Parent Loop BB0_73 Depth=1
                                        #     Parent Loop BB0_74 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$1042983595, (%rbx)     # imm = 0x3E2AAAAB
	addq	$4, %rbx
	decq	%rcx
	jne	.LBB0_82
.LBB0_83:                               # %._crit_edge.us.us.i149.us
                                        #   in Loop: Header=BB0_74 Depth=2
	incq	%rdi
	addl	%edx, %eax
	cmpq	%r12, %rdi
	jne	.LBB0_74
# BB#84:                                # %._crit_edge30.us.i146.loopexit.us
                                        #   in Loop: Header=BB0_73 Depth=1
	incl	%r11d
	addl	%r9d, %r10d
	cmpl	%r8d, %r11d
	jne	.LBB0_73
.LBB0_85:                               # %mat_set.exit151
	movl	b+12(%rip), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB0_121
# BB#86:                                # %.preheader27.lr.ph.i152
	movl	b+16(%rip), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB0_121
# BB#87:                                # %.preheader27.us.preheader.i154
	movl	b+20(%rip), %r14d
	testl	%r14d, %r14d
	jle	.LBB0_98
# BB#88:                                # %.preheader27.us.i156.us.preheader
	movq	b(%rip), %r12
	leal	-1(%r14), %eax
	leaq	4(,%rax,4), %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	-1(%rax), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%eax, %r13d
	andl	$3, %r13d
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r14d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	leal	(,%r14,4), %ebp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movl	%ebp, (%rsp)            # 4-byte Spill
	.p2align	4, 0x90
.LBB0_89:                               # %.preheader27.us.i156.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_91 Depth 2
                                        #     Child Loop BB0_95 Depth 2
	movl	%edx, 64(%rsp)          # 4-byte Spill
	testq	%r13, %r13
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	je	.LBB0_92
# BB#90:                                # %.preheader.us.us.i161.us.prol.preheader
                                        #   in Loop: Header=BB0_89 Depth=1
	movl	%ecx, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_91:                               # %.preheader.us.us.i161.us.prol
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	leaq	(%r12,%rbp,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	incq	%r15
	addl	%r14d, %ebp
	cmpq	%r15, %r13
	jne	.LBB0_91
	jmp	.LBB0_93
	.p2align	4, 0x90
.LBB0_92:                               #   in Loop: Header=BB0_89 Depth=1
	xorl	%r15d, %r15d
.LBB0_93:                               # %.preheader.us.us.i161.us.prol.loopexit
                                        #   in Loop: Header=BB0_89 Depth=1
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	movl	(%rsp), %ebp            # 4-byte Reload
	jb	.LBB0_96
# BB#94:                                # %.preheader27.us.i156.us.new
                                        #   in Loop: Header=BB0_89 Depth=1
	movq	8(%rsp), %r13           # 8-byte Reload
	subq	%r15, %r13
	leal	3(%r15), %edx
	movq	24(%rsp), %rax          # 8-byte Reload
	imull	%eax, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	addl	%ecx, %edx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	leal	2(%r15), %edx
	imull	%eax, %edx
	addl	%ecx, %edx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%eax, %edx
	imull	%r15d, %edx
	addl	%ecx, %edx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	incl	%r15d
	imull	%eax, %r15d
	addl	%ecx, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_95:                               # %.preheader.us.us.i161.us
                                        #   Parent Loop BB0_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r14), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	leal	(%r15,%r14), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r14), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r14), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	addl	%ebp, %r14d
	addq	$-4, %r13
	jne	.LBB0_95
.LBB0_96:                               # %._crit_edge30.us.i159.loopexit.us
                                        #   in Loop: Header=BB0_89 Depth=1
	movl	64(%rsp), %edx          # 4-byte Reload
	incl	%edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	addl	4(%rsp), %ecx           # 4-byte Folded Reload
	cmpl	32(%rsp), %edx          # 4-byte Folded Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
	jne	.LBB0_89
# BB#97:                                # %mat_set.exit164
	movl	b+12(%rip), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB0_121
.LBB0_98:                               # %.preheader27.lr.ph.i217
	movl	b+16(%rip), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB0_121
# BB#99:                                # %.preheader27.us.preheader.i219
	movl	b+20(%rip), %r14d
	testl	%r14d, %r14d
	jle	.LBB0_110
# BB#100:                               # %.preheader27.us.i221.us.preheader
	movq	b(%rip), %r12
	movl	32(%rsp), %edx          # 4-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	imull	%ecx, %edx
	imull	%r14d, %edx
	leal	-1(%r14), %eax
	leaq	4(,%rax,4), %rbx
	leaq	-1(%rcx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ecx, %r15d
	andl	$3, %r15d
	movl	%ecx, %eax
	imull	%r14d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	leal	(,%r14,4), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	xorl	%eax, %eax
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_101:                              # %.preheader27.us.i221.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_103 Depth 2
                                        #     Child Loop BB0_107 Depth 2
	testq	%r15, %r15
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	je	.LBB0_104
# BB#102:                               # %.preheader.us.us.i226.us.prol.preheader
                                        #   in Loop: Header=BB0_101 Depth=1
	movl	%edx, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_103:                              # %.preheader.us.us.i226.us.prol
                                        #   Parent Loop BB0_101 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	leaq	(%r12,%rbp,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	incq	%r13
	addl	%r14d, %ebp
	cmpq	%r13, %r15
	jne	.LBB0_103
	jmp	.LBB0_105
	.p2align	4, 0x90
.LBB0_104:                              #   in Loop: Header=BB0_101 Depth=1
	xorl	%r13d, %r13d
.LBB0_105:                              # %.preheader.us.us.i226.us.prol.loopexit
                                        #   in Loop: Header=BB0_101 Depth=1
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	movl	(%rsp), %ebp            # 4-byte Reload
	jb	.LBB0_108
# BB#106:                               # %.preheader27.us.i221.us.new
                                        #   in Loop: Header=BB0_101 Depth=1
	movq	8(%rsp), %r14           # 8-byte Reload
	subq	%r13, %r14
	leal	3(%r13), %esi
	movq	24(%rsp), %rax          # 8-byte Reload
	imull	%eax, %esi
	movl	16(%rsp), %ecx          # 4-byte Reload
	addl	%ecx, %esi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	leal	2(%r13), %esi
	imull	%eax, %esi
	addl	%ecx, %esi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movl	%eax, %esi
	imull	%r13d, %esi
	addl	%ecx, %esi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	incl	%r13d
	imull	%eax, %r13d
	addl	%ecx, %r13d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_107:                              # %.preheader.us.us.i226.us
                                        #   Parent Loop BB0_101 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	leal	(%r13,%r15), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	addl	%ebp, %r15d
	addq	$-4, %r14
	jne	.LBB0_107
.LBB0_108:                              # %._crit_edge30.us.i224.loopexit.us
                                        #   in Loop: Header=BB0_101 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movl	16(%rsp), %edx          # 4-byte Reload
	addl	4(%rsp), %edx           # 4-byte Folded Reload
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	jne	.LBB0_101
# BB#109:                               # %mat_set.exit229
	movl	b+12(%rip), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB0_121
.LBB0_110:                              # %.preheader27.lr.ph.i204
	movl	b+16(%rip), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB0_121
# BB#111:                               # %.preheader27.us.preheader.i206
	movl	b+20(%rip), %r14d
	testl	%r14d, %r14d
	jle	.LBB0_121
# BB#112:                               # %.preheader27.us.i208.us.preheader
	movq	b(%rip), %r12
	movl	32(%rsp), %edx          # 4-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	imull	%ecx, %edx
	imull	%r14d, %edx
	addl	%edx, %edx
	leal	-1(%r14), %eax
	leaq	4(,%rax,4), %rbx
	leaq	-1(%rcx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ecx, %r15d
	andl	$3, %r15d
	movl	%ecx, %eax
	imull	%r14d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	leal	(,%r14,4), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	xorl	%eax, %eax
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_113:                              # %.preheader27.us.i208.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_115 Depth 2
                                        #     Child Loop BB0_119 Depth 2
	testq	%r15, %r15
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	je	.LBB0_116
# BB#114:                               # %.preheader.us.us.i213.us.prol.preheader
                                        #   in Loop: Header=BB0_113 Depth=1
	movl	%edx, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_115:                              # %.preheader.us.us.i213.us.prol
                                        #   Parent Loop BB0_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	leaq	(%r12,%rbp,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	incq	%r13
	addl	%r14d, %ebp
	cmpq	%r13, %r15
	jne	.LBB0_115
	jmp	.LBB0_117
	.p2align	4, 0x90
.LBB0_116:                              #   in Loop: Header=BB0_113 Depth=1
	xorl	%r13d, %r13d
.LBB0_117:                              # %.preheader.us.us.i213.us.prol.loopexit
                                        #   in Loop: Header=BB0_113 Depth=1
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	movl	(%rsp), %ebp            # 4-byte Reload
	jb	.LBB0_120
# BB#118:                               # %.preheader27.us.i208.us.new
                                        #   in Loop: Header=BB0_113 Depth=1
	movq	8(%rsp), %r14           # 8-byte Reload
	subq	%r13, %r14
	leal	3(%r13), %esi
	movq	24(%rsp), %rax          # 8-byte Reload
	imull	%eax, %esi
	movl	16(%rsp), %ecx          # 4-byte Reload
	addl	%ecx, %esi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	leal	2(%r13), %esi
	imull	%eax, %esi
	addl	%ecx, %esi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movl	%eax, %esi
	imull	%r13d, %esi
	addl	%ecx, %esi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	incl	%r13d
	imull	%eax, %r13d
	addl	%ecx, %r13d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_119:                              # %.preheader.us.us.i213.us
                                        #   Parent Loop BB0_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	leal	(%r13,%r15), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %eax
	cltq
	leaq	(%r12,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	addl	%ebp, %r15d
	addq	$-4, %r14
	jne	.LBB0_119
.LBB0_120:                              # %._crit_edge30.us.i211.loopexit.us
                                        #   in Loop: Header=BB0_113 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movl	16(%rsp), %edx          # 4-byte Reload
	addl	4(%rsp), %edx           # 4-byte Folded Reload
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	jne	.LBB0_113
.LBB0_121:                              # %mat_set.exit216
	movl	c+12(%rip), %r8d
	testl	%r8d, %r8d
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	jle	.LBB0_167
# BB#122:                               # %.preheader27.lr.ph.i191
	movl	c+16(%rip), %r12d
	testl	%r12d, %r12d
	jle	.LBB0_167
# BB#123:                               # %.preheader27.us.preheader.i193
	movl	c+20(%rip), %edx
	testl	%edx, %edx
	jle	.LBB0_167
# BB#124:                               # %.preheader27.us.i195.us.preheader
	movq	c(%rip), %r13
	movl	%r12d, %r9d
	imull	%edx, %r9d
	movl	%edx, %r14d
	andl	$7, %r14d
	movq	%rdx, %rsi
	subq	%r14, %rsi
	leaq	16(%r13), %r15
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_125:                              # %.preheader27.us.i195.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_126 Depth 2
                                        #       Child Loop BB0_130 Depth 3
                                        #       Child Loop BB0_134 Depth 3
	movl	%r10d, %edi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_126:                              # %.preheader.us.us.i200.us
                                        #   Parent Loop BB0_125 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_130 Depth 3
                                        #       Child Loop BB0_134 Depth 3
	cmpl	$8, %edx
	movslq	%edi, %rbx
	jb	.LBB0_132
# BB#128:                               # %min.iters.checked498
                                        #   in Loop: Header=BB0_126 Depth=2
	testq	%rsi, %rsi
	je	.LBB0_132
# BB#129:                               # %vector.body494.preheader
                                        #   in Loop: Header=BB0_126 Depth=2
	leaq	(%r15,%rbx,4), %rcx
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB0_130:                              # %vector.body494
                                        #   Parent Loop BB0_125 Depth=1
                                        #     Parent Loop BB0_126 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	%xmm0, -16(%rcx)
	movups	%xmm0, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB0_130
# BB#131:                               # %middle.block495
                                        #   in Loop: Header=BB0_126 Depth=2
	testl	%r14d, %r14d
	movq	%rsi, %rbp
	jne	.LBB0_133
	jmp	.LBB0_135
	.p2align	4, 0x90
.LBB0_132:                              #   in Loop: Header=BB0_126 Depth=2
	xorl	%ebp, %ebp
.LBB0_133:                              # %scalar.ph496.preheader
                                        #   in Loop: Header=BB0_126 Depth=2
	addq	%rbp, %rbx
	leaq	(%r13,%rbx,4), %rbx
	movq	%rdx, %rcx
	subq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_134:                              # %scalar.ph496
                                        #   Parent Loop BB0_125 Depth=1
                                        #     Parent Loop BB0_126 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	addq	$4, %rbx
	decq	%rcx
	jne	.LBB0_134
.LBB0_135:                              # %._crit_edge.us.us.i201.us
                                        #   in Loop: Header=BB0_126 Depth=2
	incq	%rax
	addl	%edx, %edi
	cmpq	%r12, %rax
	jne	.LBB0_126
# BB#136:                               # %._crit_edge30.us.i198.loopexit.us
                                        #   in Loop: Header=BB0_125 Depth=1
	incl	%r11d
	addl	%r9d, %r10d
	cmpl	%r8d, %r11d
	jne	.LBB0_125
# BB#137:                               # %.preheader27.lr.ph.i178
	testl	%r12d, %r12d
	jle	.LBB0_167
# BB#138:                               # %.preheader27.us.preheader.i180
	movl	c+20(%rip), %edx
	testl	%edx, %edx
	jle	.LBB0_167
# BB#139:                               # %.preheader27.us.i182.us.preheader
	movq	c(%rip), %r13
	movl	%edx, %r14d
	andl	$7, %r14d
	movq	%rdx, %rsi
	subq	%r14, %rsi
	leaq	16(%r13), %r15
	movl	%r8d, %r10d
	imull	%r12d, %r10d
	imull	%edx, %r10d
	movl	%r12d, %r9d
	imull	%edx, %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_140:                              # %.preheader27.us.i182.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_141 Depth 2
                                        #       Child Loop BB0_145 Depth 3
                                        #       Child Loop BB0_149 Depth 3
	movl	%r10d, %ebx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_141:                              # %.preheader.us.us.i187.us
                                        #   Parent Loop BB0_140 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_145 Depth 3
                                        #       Child Loop BB0_149 Depth 3
	cmpl	$8, %edx
	movslq	%ebx, %rax
	jb	.LBB0_147
# BB#143:                               # %min.iters.checked515
                                        #   in Loop: Header=BB0_141 Depth=2
	testq	%rsi, %rsi
	je	.LBB0_147
# BB#144:                               # %vector.body511.preheader
                                        #   in Loop: Header=BB0_141 Depth=2
	leaq	(%r15,%rax,4), %rcx
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB0_145:                              # %vector.body511
                                        #   Parent Loop BB0_140 Depth=1
                                        #     Parent Loop BB0_141 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	%xmm0, -16(%rcx)
	movups	%xmm0, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB0_145
# BB#146:                               # %middle.block512
                                        #   in Loop: Header=BB0_141 Depth=2
	testl	%r14d, %r14d
	movq	%rsi, %rbp
	jne	.LBB0_148
	jmp	.LBB0_150
	.p2align	4, 0x90
.LBB0_147:                              #   in Loop: Header=BB0_141 Depth=2
	xorl	%ebp, %ebp
.LBB0_148:                              # %scalar.ph513.preheader
                                        #   in Loop: Header=BB0_141 Depth=2
	addq	%rbp, %rax
	leaq	(%r13,%rax,4), %rax
	movq	%rdx, %rcx
	subq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_149:                              # %scalar.ph513
                                        #   Parent Loop BB0_140 Depth=1
                                        #     Parent Loop BB0_141 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$1065353216, (%rax)     # imm = 0x3F800000
	addq	$4, %rax
	decq	%rcx
	jne	.LBB0_149
.LBB0_150:                              # %._crit_edge.us.us.i188.us
                                        #   in Loop: Header=BB0_141 Depth=2
	incq	%rdi
	addl	%edx, %ebx
	cmpq	%r12, %rdi
	jne	.LBB0_141
# BB#151:                               # %._crit_edge30.us.i185.loopexit.us
                                        #   in Loop: Header=BB0_140 Depth=1
	incl	%r11d
	addl	%r9d, %r10d
	cmpl	%r8d, %r11d
	jne	.LBB0_140
# BB#152:                               # %.preheader27.lr.ph.i165
	testl	%r12d, %r12d
	jle	.LBB0_167
# BB#153:                               # %.preheader27.us.preheader.i167
	movl	c+20(%rip), %edx
	testl	%edx, %edx
	jle	.LBB0_167
# BB#154:                               # %.preheader27.us.i169.us.preheader
	movq	c(%rip), %r13
	movl	%edx, %r14d
	andl	$7, %r14d
	movq	%rdx, %rsi
	subq	%r14, %rsi
	leaq	16(%r13), %r15
	movl	%r8d, %r10d
	imull	%r12d, %r10d
	imull	%edx, %r10d
	addl	%r10d, %r10d
	movl	%r12d, %r9d
	imull	%edx, %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_155:                              # %.preheader27.us.i169.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_156 Depth 2
                                        #       Child Loop BB0_160 Depth 3
                                        #       Child Loop BB0_164 Depth 3
	movl	%r10d, %eax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_156:                              # %.preheader.us.us.i174.us
                                        #   Parent Loop BB0_155 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_160 Depth 3
                                        #       Child Loop BB0_164 Depth 3
	cmpl	$8, %edx
	movslq	%eax, %rbx
	jb	.LBB0_162
# BB#158:                               # %min.iters.checked532
                                        #   in Loop: Header=BB0_156 Depth=2
	testq	%rsi, %rsi
	je	.LBB0_162
# BB#159:                               # %vector.body528.preheader
                                        #   in Loop: Header=BB0_156 Depth=2
	leaq	(%r15,%rbx,4), %rcx
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB0_160:                              # %vector.body528
                                        #   Parent Loop BB0_155 Depth=1
                                        #     Parent Loop BB0_156 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	%xmm0, -16(%rcx)
	movups	%xmm0, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB0_160
# BB#161:                               # %middle.block529
                                        #   in Loop: Header=BB0_156 Depth=2
	testl	%r14d, %r14d
	movq	%rsi, %rbp
	jne	.LBB0_163
	jmp	.LBB0_165
	.p2align	4, 0x90
.LBB0_162:                              #   in Loop: Header=BB0_156 Depth=2
	xorl	%ebp, %ebp
.LBB0_163:                              # %scalar.ph530.preheader
                                        #   in Loop: Header=BB0_156 Depth=2
	addq	%rbp, %rbx
	leaq	(%r13,%rbx,4), %rbx
	movq	%rdx, %rcx
	subq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_164:                              # %scalar.ph530
                                        #   Parent Loop BB0_155 Depth=1
                                        #     Parent Loop BB0_156 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	addq	$4, %rbx
	decq	%rcx
	jne	.LBB0_164
.LBB0_165:                              # %._crit_edge.us.us.i175.us
                                        #   in Loop: Header=BB0_156 Depth=2
	incq	%rdi
	addl	%edx, %eax
	cmpq	%r12, %rdi
	jne	.LBB0_156
# BB#166:                               # %._crit_edge30.us.i172.loopexit.us
                                        #   in Loop: Header=BB0_155 Depth=1
	incl	%r11d
	addl	%r9d, %r10d
	cmpl	%r8d, %r11d
	jne	.LBB0_155
.LBB0_167:                              # %mat_set.exit177
	movl	$64, %edi
	movl	$a, %esi
	movl	$b, %edx
	movl	$c, %ecx
	movl	$p, %r8d
	movl	$bnd, %r9d
	pushq	$wrk2
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$wrk1
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	jacobi
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	movl	$.L.str.2, %edi
	movl	$64, %esi
	xorl	%eax, %eax
	callq	printf
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.3, %edi
	movb	$1, %al
	callq	printf
	movq	p(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_169
# BB#168:
	callq	free
.LBB0_169:                              # %clearMat.exit138
	xorps	%xmm0, %xmm0
	movups	%xmm0, p(%rip)
	movq	$0, p+16(%rip)
	movq	bnd(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_171
# BB#170:
	callq	free
	xorps	%xmm0, %xmm0
.LBB0_171:                              # %clearMat.exit124
	movups	%xmm0, bnd(%rip)
	movq	$0, bnd+16(%rip)
	movq	wrk1(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_173
# BB#172:
	callq	free
.LBB0_173:                              # %clearMat.exit110
	xorps	%xmm0, %xmm0
	movups	%xmm0, wrk1(%rip)
	movq	$0, wrk1+16(%rip)
	movq	wrk2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_175
# BB#174:
	callq	free
	xorps	%xmm0, %xmm0
.LBB0_175:                              # %clearMat.exit96
	movups	%xmm0, wrk2(%rip)
	movq	$0, wrk2+16(%rip)
	movq	a(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_177
# BB#176:
	callq	free
.LBB0_177:                              # %clearMat.exit82
	xorps	%xmm0, %xmm0
	movups	%xmm0, a(%rip)
	movq	$0, a+16(%rip)
	movq	b(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_179
# BB#178:
	callq	free
	xorps	%xmm0, %xmm0
.LBB0_179:                              # %clearMat.exit68
	movups	%xmm0, b(%rip)
	movq	$0, b+16(%rip)
	movq	c(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_181
# BB#180:
	callq	free
.LBB0_181:                              # %clearMat.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, c(%rip)
	movq	$0, c+16(%rip)
	xorl	%eax, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	newMat
	.p2align	4, 0x90
	.type	newMat,@function
newMat:                                 # @newMat
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	%esi, 8(%rbx)
	movl	%edx, 12(%rbx)
	movl	%ecx, 16(%rbx)
	movl	%r8d, 20(%rbx)
	imull	%esi, %edx
	imull	%r8d, %ecx
	imull	%edx, %ecx
	movslq	%ecx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, (%rbx)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	setne	%cl
	movl	%ecx, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	newMat, .Lfunc_end1-newMat
	.cfi_endproc

	.globl	mat_set_init
	.p2align	4, 0x90
	.type	mat_set_init,@function
mat_set_init:                           # @mat_set_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movl	12(%rdi), %eax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB2_17
# BB#1:                                 # %.preheader33.lr.ph
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	16(%rax), %r13d
	testl	%r13d, %r13d
	jle	.LBB2_17
# BB#2:                                 # %.preheader33.us.preheader
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	20(%rax), %edx
	movq	-8(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	imull	%eax, %eax
	cvtsi2ssl	%eax, %xmm0
	leal	-8(%rdx), %r9d
	movl	%r9d, %r14d
	shrl	$3, %r14d
	incl	%r14d
	movl	%edx, %r10d
	andl	$-8, %r10d
	andl	$3, %r14d
	movl	%r13d, %eax
	imull	%edx, %eax
	movl	%eax, -12(%rsp)         # 4-byte Spill
	movl	%r14d, %r8d
	negl	%r8d
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader33.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
                                        #       Child Loop BB2_11 Depth 3
                                        #       Child Loop BB2_13 Depth 3
                                        #       Child Loop BB2_7 Depth 3
	testl	%edx, %edx
	jle	.LBB2_16
# BB#4:                                 # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	%r12d, %ecx
	imull	%ecx, %ecx
	cvtsi2ssl	%ecx, %xmm1
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rsi
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm2
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	xorl	%ebx, %ebx
	movl	%r15d, %edi
	.p2align	4, 0x90
.LBB2_5:                                # %.preheader.us.us
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_11 Depth 3
                                        #       Child Loop BB2_13 Depth 3
                                        #       Child Loop BB2_7 Depth 3
	cmpl	$7, %edx
	movl	$0, %ecx
	jbe	.LBB2_6
# BB#8:                                 # %min.iters.checked
                                        #   in Loop: Header=BB2_5 Depth=2
	testl	%r10d, %r10d
	movl	$0, %ecx
	je	.LBB2_6
# BB#9:                                 # %vector.ph
                                        #   in Loop: Header=BB2_5 Depth=2
	xorl	%ecx, %ecx
	testl	%r14d, %r14d
	je	.LBB2_12
# BB#10:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB2_5 Depth=2
	movl	%r8d, %r11d
	movl	%edi, %ebp
	.p2align	4, 0x90
.LBB2_11:                               # %vector.body.prol
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebp, %rbp
	movups	%xmm2, (%rsi,%rbp,4)
	movups	%xmm2, 16(%rsi,%rbp,4)
	addl	$8, %ecx
	addl	$8, %ebp
	incl	%r11d
	jne	.LBB2_11
.LBB2_12:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB2_5 Depth=2
	cmpl	$24, %r9d
	jb	.LBB2_14
	.p2align	4, 0x90
.LBB2_13:                               # %vector.body
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rdi,%rcx), %eax
	cltq
	movups	%xmm2, (%rsi,%rax,4)
	movups	%xmm2, 16(%rsi,%rax,4)
	leal	8(%rdi,%rcx), %eax
	cltq
	movups	%xmm2, (%rsi,%rax,4)
	movups	%xmm2, 16(%rsi,%rax,4)
	leal	16(%rdi,%rcx), %eax
	cltq
	movups	%xmm2, (%rsi,%rax,4)
	movups	%xmm2, 16(%rsi,%rax,4)
	leal	24(%rdi,%rcx), %eax
	cltq
	movups	%xmm2, (%rsi,%rax,4)
	movups	%xmm2, 16(%rsi,%rax,4)
	addl	$32, %ecx
	cmpl	%ecx, %r10d
	jne	.LBB2_13
.LBB2_14:                               # %middle.block
                                        #   in Loop: Header=BB2_5 Depth=2
	cmpl	%r10d, %edx
	movl	%r10d, %ecx
	je	.LBB2_15
	.p2align	4, 0x90
.LBB2_6:                                # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_5 Depth=2
	movslq	%edi, %rbp
	movslq	%ecx, %rax
	addq	%rbp, %rax
	leaq	(%rsi,%rax,4), %rbp
	.p2align	4, 0x90
.LBB2_7:                                # %scalar.ph
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	%xmm1, (%rbp)
	incl	%ecx
	addq	$4, %rbp
	cmpl	%edx, %ecx
	jl	.LBB2_7
.LBB2_15:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB2_5 Depth=2
	incl	%ebx
	addl	%edx, %edi
	cmpl	%r13d, %ebx
	jl	.LBB2_5
.LBB2_16:                               # %._crit_edge36.us
                                        #   in Loop: Header=BB2_3 Depth=1
	incl	%r12d
	addl	-12(%rsp), %r15d        # 4-byte Folded Reload
	cmpl	-8(%rsp), %r12d         # 4-byte Folded Reload
	jl	.LBB2_3
.LBB2_17:                               # %._crit_edge40
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	mat_set_init, .Lfunc_end2-mat_set_init
	.cfi_endproc

	.globl	mat_set
	.p2align	4, 0x90
	.type	mat_set,@function
mat_set:                                # @mat_set
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rdi, -16(%rsp)         # 8-byte Spill
	movl	12(%rdi), %eax
	movl	%eax, -20(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB3_17
# BB#1:                                 # %.preheader27.lr.ph
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	16(%rax), %r13d
	testl	%r13d, %r13d
	jle	.LBB3_17
# BB#2:                                 # %.preheader27.us.preheader
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	20(%rax), %edx
	leal	-8(%rdx), %r9d
	movl	%r9d, %r10d
	shrl	$3, %r10d
	incl	%r10d
	movl	%edx, %r14d
	andl	$-8, %r14d
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	andl	$3, %r10d
	movl	-20(%rsp), %r11d        # 4-byte Reload
	imull	%r13d, %r11d
	imull	%edx, %r11d
	imull	%esi, %r11d
	movl	%r13d, %eax
	imull	%edx, %eax
	movl	%eax, -4(%rsp)          # 4-byte Spill
	movl	%r10d, %r8d
	negl	%r8d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader27.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_5 Depth 2
                                        #       Child Loop BB3_11 Depth 3
                                        #       Child Loop BB3_13 Depth 3
                                        #       Child Loop BB3_7 Depth 3
	testl	%edx, %edx
	jle	.LBB3_16
# BB#4:                                 # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rsi
	xorl	%ebx, %ebx
	movl	%r11d, %edi
	.p2align	4, 0x90
.LBB3_5:                                # %.preheader.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_11 Depth 3
                                        #       Child Loop BB3_13 Depth 3
                                        #       Child Loop BB3_7 Depth 3
	cmpl	$7, %edx
	movl	$0, %ecx
	jbe	.LBB3_6
# BB#8:                                 # %min.iters.checked
                                        #   in Loop: Header=BB3_5 Depth=2
	testl	%r14d, %r14d
	movl	$0, %ecx
	je	.LBB3_6
# BB#9:                                 # %vector.ph
                                        #   in Loop: Header=BB3_5 Depth=2
	xorl	%ecx, %ecx
	testl	%r10d, %r10d
	je	.LBB3_12
# BB#10:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB3_5 Depth=2
	movl	%r8d, %r15d
	movl	%edi, %ebp
	.p2align	4, 0x90
.LBB3_11:                               # %vector.body.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebp, %rbp
	movups	%xmm1, (%rsi,%rbp,4)
	movups	%xmm1, 16(%rsi,%rbp,4)
	addl	$8, %ecx
	addl	$8, %ebp
	incl	%r15d
	jne	.LBB3_11
.LBB3_12:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB3_5 Depth=2
	cmpl	$24, %r9d
	jb	.LBB3_14
	.p2align	4, 0x90
.LBB3_13:                               # %vector.body
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rdi,%rcx), %eax
	cltq
	movups	%xmm1, (%rsi,%rax,4)
	movups	%xmm1, 16(%rsi,%rax,4)
	leal	8(%rdi,%rcx), %eax
	cltq
	movups	%xmm1, (%rsi,%rax,4)
	movups	%xmm1, 16(%rsi,%rax,4)
	leal	16(%rdi,%rcx), %eax
	cltq
	movups	%xmm1, (%rsi,%rax,4)
	movups	%xmm1, 16(%rsi,%rax,4)
	leal	24(%rdi,%rcx), %eax
	cltq
	movups	%xmm1, (%rsi,%rax,4)
	movups	%xmm1, 16(%rsi,%rax,4)
	addl	$32, %ecx
	cmpl	%ecx, %r14d
	jne	.LBB3_13
.LBB3_14:                               # %middle.block
                                        #   in Loop: Header=BB3_5 Depth=2
	cmpl	%r14d, %edx
	movl	%r14d, %ecx
	je	.LBB3_15
	.p2align	4, 0x90
.LBB3_6:                                # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_5 Depth=2
	movslq	%edi, %rbp
	movslq	%ecx, %rax
	addq	%rbp, %rax
	leaq	(%rsi,%rax,4), %rbp
	.p2align	4, 0x90
.LBB3_7:                                # %scalar.ph
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	%xmm0, (%rbp)
	incl	%ecx
	addq	$4, %rbp
	cmpl	%edx, %ecx
	jl	.LBB3_7
.LBB3_15:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB3_5 Depth=2
	incl	%ebx
	addl	%edx, %edi
	cmpl	%r13d, %ebx
	jl	.LBB3_5
.LBB3_16:                               # %._crit_edge30.us
                                        #   in Loop: Header=BB3_3 Depth=1
	incl	%r12d
	addl	-4(%rsp), %r11d         # 4-byte Folded Reload
	cmpl	-20(%rsp), %r12d        # 4-byte Folded Reload
	jl	.LBB3_3
.LBB3_17:                               # %._crit_edge34
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	mat_set, .Lfunc_end3-mat_set
	.cfi_endproc

	.globl	jacobi
	.p2align	4, 0x90
	.type	jacobi,@function
jacobi:                                 # @jacobi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$128, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 184
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movq	%r8, %r9
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	testl	%edi, %edi
	jle	.LBB4_1
# BB#2:                                 # %.preheader409.lr.ph
	movl	12(%r9), %eax
	movl	16(%r9), %ecx
	decl	%eax
	decl	%ecx
	movl	20(%r9), %edx
	decl	%edx
	movl	%eax, -104(%rsp)        # 4-byte Spill
	cmpl	$1, %eax
	setg	%al
	movl	%ecx, -100(%rsp)        # 4-byte Spill
	cmpl	$1, %ecx
	setg	%cl
	movl	%edx, -20(%rsp)         # 4-byte Spill
	movl	%edx, %edx
	leaq	-1(%rdx), %rsi
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	leaq	-9(%rdx), %rdx
	shrq	$3, %rdx
	andb	%al, %cl
	movb	%cl, -121(%rsp)         # 1-byte Spill
	movq	%rsi, %rax
	andq	$-8, %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	incq	%rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rdx, 104(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$1, %edx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%r9, (%rsp)             # 8-byte Spill
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movl	%edi, -32(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader409
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #       Child Loop BB4_7 Depth 3
                                        #         Child Loop BB4_38 Depth 4
                                        #     Child Loop BB4_14 Depth 2
                                        #       Child Loop BB4_16 Depth 3
                                        #         Child Loop BB4_34 Depth 4
                                        #         Child Loop BB4_20 Depth 4
                                        #         Child Loop BB4_23 Depth 4
	xorps	%xmm0, %xmm0
	cmpl	$2, -104(%rsp)          # 4-byte Folded Reload
	jl	.LBB4_11
# BB#4:                                 # %.preheader407.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	movl	$1, %r12d
	movl	$0, -24(%rsp)           # 4-byte Folded Spill
	movl	$2, -28(%rsp)           # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB4_5:                                # %.preheader407
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_7 Depth 3
                                        #         Child Loop BB4_38 Depth 4
	leal	1(%r12), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	cmpl	$2, -100(%rsp)          # 4-byte Folded Reload
	jl	.LBB4_41
# BB#6:                                 # %.preheader405.lr.ph
                                        #   in Loop: Header=BB4_5 Depth=2
	movl	$1, %r11d
	movq	%r12, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_7:                                # %.preheader405
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_38 Depth 4
	cmpl	$1, -20(%rsp)           # 4-byte Folded Reload
	jle	.LBB4_8
# BB#37:                                # %.lr.ph
                                        #   in Loop: Header=BB4_7 Depth=3
	movl	16(%r9), %ecx
	movl	20(%r9), %eax
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsi), %r14d
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	12(%rdx), %r10d
	movl	16(%rdx), %ebx
	movl	20(%rdx), %r8d
	movl	%r12d, %edi
	imull	%ecx, %edi
	leal	(%r11,%rdi), %r13d
	imull	%eax, %r13d
	movl	-24(%rsp), %edx         # 4-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	imull	%ecx, %edx
	leal	(%r11,%rdx), %ebp
	imull	%eax, %ebp
	movl	%ebp, -80(%rsp)         # 4-byte Spill
	imull	-28(%rsp), %ecx         # 4-byte Folded Reload
	leal	(%r11,%rcx), %ebp
	imull	%eax, %ebp
	movl	%ebp, -88(%rsp)         # 4-byte Spill
	leal	-1(%r11,%rdi), %ebp
	imull	%eax, %ebp
	movl	%ebp, -112(%rsp)        # 4-byte Spill
	leal	1(%r11,%rdi), %edi
	imull	%eax, %edi
	movl	%edi, -120(%rsp)        # 4-byte Spill
	leal	(%r12,%r10,2), %edi
	imull	%ebx, %edi
	addl	%r11d, %edi
	imull	%r8d, %edi
	movq	%rdi, -96(%rsp)         # 8-byte Spill
	addl	%r12d, %r10d
	imull	%ebx, %r10d
	addl	%r11d, %r10d
	imull	%r8d, %r10d
	imull	%r12d, %ebx
	addl	%r11d, %ebx
	imull	%r8d, %ebx
	movq	%rbx, -40(%rsp)         # 8-byte Spill
	leal	-1(%r11,%rdx), %edi
	imull	%eax, %edi
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	leal	-1(%r11,%rcx), %edi
	imull	%eax, %edi
	movq	%rdi, -56(%rsp)         # 8-byte Spill
	leal	1(%r11,%rdx), %edx
	imull	%eax, %edx
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	leal	1(%r11,%rcx), %ecx
	imull	%eax, %ecx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	movl	16(%rsi), %ecx
	leal	(%r12,%r14,2), %r8d
	imull	%ecx, %r8d
	addl	%r11d, %r8d
	movl	20(%rsi), %eax
	imull	%eax, %r8d
	addl	%r12d, %r14d
	imull	%ecx, %r14d
	addl	%r11d, %r14d
	imull	%eax, %r14d
	imull	%r12d, %ecx
	addl	%r11d, %ecx
	imull	%eax, %ecx
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	12(%rdx), %r15d
	leal	(%r15,%r15,2), %esi
	addl	%r12d, %esi
	movl	16(%rdx), %eax
	imull	%eax, %esi
	addl	%r11d, %esi
	movl	20(%rdx), %edi
	imull	%edi, %esi
	leal	(%r12,%r15,2), %ebx
	imull	%eax, %ebx
	addl	%r11d, %ebx
	imull	%edi, %ebx
	addl	%r12d, %r15d
	imull	%eax, %r15d
	addl	%r11d, %r15d
	imull	%edi, %r15d
	imull	%r12d, %eax
	addl	%r11d, %eax
	imull	%edi, %eax
	movq	184(%rsp), %rdx
	movl	16(%rdx), %edi
	imull	%r12d, %edi
	addl	%r11d, %edi
	imull	20(%rdx), %edi
	movslq	%edi, %rdi
	movq	(%rdx), %rbp
	leaq	4(%rbp,%rdi,4), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rdx         # 8-byte Reload
	movl	16(%rdx), %edi
	imull	%r12d, %edi
	addl	%r11d, %edi
	imull	20(%rdx), %edi
	movslq	%edi, %rdi
	movq	(%rdx), %rbp
	leaq	4(%rbp,%rdi,4), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	192(%rsp), %rdx
	movl	16(%rdx), %edi
	imull	%r12d, %edi
	addl	%r11d, %edi
	imull	20(%rdx), %edi
	movslq	%edi, %rdi
	movq	(%rdx), %rbp
	leaq	4(%rbp,%rdi,4), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	leal	1(%r11), %edx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movslq	%r13d, %rdi
	movq	(%r9), %r11
	leaq	8(%r11,%rdi,4), %r12
	movslq	-80(%rsp), %rdi         # 4-byte Folded Reload
	leaq	8(%r11,%rdi,4), %rbp
	movslq	-88(%rsp), %rdi         # 4-byte Folded Reload
	leaq	8(%r11,%rdi,4), %r13
	movslq	-112(%rsp), %rdi        # 4-byte Folded Reload
	leaq	8(%r11,%rdi,4), %r9
	movslq	-120(%rsp), %rdi        # 4-byte Folded Reload
	leaq	8(%r11,%rdi,4), %rdi
	movq	-96(%rsp), %rdx         # 8-byte Reload
	incl	%edx
	movq	%rdx, -96(%rsp)         # 8-byte Spill
	incl	%r10d
	movq	%r10, -80(%rsp)         # 8-byte Spill
	movq	-40(%rsp), %rdx         # 8-byte Reload
	incl	%edx
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	incl	%r8d
	movq	%r8, -112(%rsp)         # 8-byte Spill
	incl	%r14d
	movq	%r14, -88(%rsp)         # 8-byte Spill
	movq	-48(%rsp), %rdx         # 8-byte Reload
	incl	%edx
	movq	%rdx, -48(%rsp)         # 8-byte Spill
	movq	-56(%rsp), %rdx         # 8-byte Reload
	incl	%edx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	movq	-64(%rsp), %rdx         # 8-byte Reload
	incl	%edx
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	movq	-72(%rsp), %rdx         # 8-byte Reload
	incl	%edx
	movq	%rdx, -72(%rsp)         # 8-byte Spill
	incl	%ecx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	incl	%esi
	movq	%rsi, %rcx
	incl	%ebx
	movq	%rbx, %rsi
	incl	%r15d
	incl	%eax
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %r8
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rbx
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %r10
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_38:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_5 Depth=2
                                        #       Parent Loop BB4_7 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rax,%r14), %edx
	movslq	%edx, %rdx
	movss	(%r8,%rdx,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%r13,%r14,4), %xmm1
	leal	(%r15,%r14), %edx
	movslq	%edx, %rdx
	movss	(%r8,%rdx,4), %xmm3     # xmm3 = mem[0],zero,zero,zero
	mulss	-4(%rdi,%r14,4), %xmm3
	addss	%xmm1, %xmm3
	leal	(%rsi,%r14), %edx
	movslq	%edx, %rdx
	movss	(%r8,%rdx,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	(%r12,%r14,4), %xmm2
	addss	%xmm3, %xmm2
	movq	-72(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r14), %edx
	movslq	%edx, %rdx
	movss	(%r11,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movq	-56(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r14), %edx
	movslq	%edx, %rdx
	subss	(%r11,%rdx,4), %xmm1
	movq	-64(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r14), %edx
	movslq	%edx, %rdx
	subss	(%r11,%rdx,4), %xmm1
	movq	-48(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r14), %edx
	movslq	%edx, %rdx
	addss	(%r11,%rdx,4), %xmm1
	movq	-120(%rsp), %rdx        # 8-byte Reload
	leal	(%rdx,%r14), %edx
	movslq	%edx, %rdx
	mulss	(%rbx,%rdx,4), %xmm1
	addss	%xmm2, %xmm1
	movq	-88(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r14), %edx
	movslq	%edx, %rdx
	movss	(%rdi,%r14,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	subss	(%r9,%r14,4), %xmm2
	subss	-8(%rdi,%r14,4), %xmm2
	addss	-8(%r9,%r14,4), %xmm2
	mulss	(%rbx,%rdx,4), %xmm2
	addss	%xmm1, %xmm2
	movq	-112(%rsp), %rdx        # 8-byte Reload
	leal	(%rdx,%r14), %edx
	movslq	%edx, %rdx
	movss	(%r13,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	(%rbp,%r14,4), %xmm1
	subss	-8(%r13,%r14,4), %xmm1
	addss	-8(%rbp,%r14,4), %xmm1
	mulss	(%rbx,%rdx,4), %xmm1
	addss	%xmm2, %xmm1
	movq	-40(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r14), %edx
	movslq	%edx, %rdx
	movss	(%r10,%rdx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	-4(%rbp,%r14,4), %xmm2
	addss	%xmm1, %xmm2
	movq	-80(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r14), %edx
	movslq	%edx, %rdx
	movss	(%r10,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%r9,%r14,4), %xmm1
	addss	%xmm2, %xmm1
	movq	-96(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r14), %edx
	movslq	%edx, %rdx
	movss	(%r10,%rdx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	-8(%r12,%r14,4), %xmm2
	addss	%xmm1, %xmm2
	movq	40(%rsp), %rdx          # 8-byte Reload
	addss	(%rdx,%r14,4), %xmm2
	leal	(%rcx,%r14), %edx
	movslq	%edx, %rdx
	mulss	(%r8,%rdx,4), %xmm2
	movss	-4(%r12,%r14,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movq	32(%rsp), %rdx          # 8-byte Reload
	mulss	(%rdx,%r14,4), %xmm2
	movaps	%xmm2, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm3, %xmm0
	mulss	omega(%rip), %xmm2
	addss	%xmm1, %xmm2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movss	%xmm2, (%rdx,%r14,4)
	incq	%r14
	cmpq	%r14, -16(%rsp)         # 8-byte Folded Reload
	jne	.LBB4_38
# BB#39:                                #   in Loop: Header=BB4_7 Depth=3
	movq	(%rsp), %r9             # 8-byte Reload
	movq	120(%rsp), %r12         # 8-byte Reload
	movl	16(%rsp), %eax          # 4-byte Reload
	jmp	.LBB4_40
	.p2align	4, 0x90
.LBB4_8:                                # %.preheader405.._crit_edge_crit_edge
                                        #   in Loop: Header=BB4_7 Depth=3
	incl	%r11d
	movl	%r11d, %eax
.LBB4_40:                               # %._crit_edge
                                        #   in Loop: Header=BB4_7 Depth=3
	cmpl	-100(%rsp), %eax        # 4-byte Folded Reload
	movl	%eax, %r11d
	jne	.LBB4_7
.LBB4_41:                               # %._crit_edge414
                                        #   in Loop: Header=BB4_5 Depth=2
	incl	-24(%rsp)               # 4-byte Folded Spill
	incl	-28(%rsp)               # 4-byte Folded Spill
	movl	52(%rsp), %eax          # 4-byte Reload
	cmpl	-104(%rsp), %eax        # 4-byte Folded Reload
	movl	%eax, %r12d
	jne	.LBB4_5
# BB#9:                                 # %.preheader408
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpb	$0, -121(%rsp)          # 1-byte Folded Reload
	je	.LBB4_10
# BB#13:                                # %.preheader406.us.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	$1, -112(%rsp)          # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	.p2align	4, 0x90
.LBB4_14:                               # %.preheader406.us
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_16 Depth 3
                                        #         Child Loop BB4_34 Depth 4
                                        #         Child Loop BB4_20 Depth 4
                                        #         Child Loop BB4_23 Depth 4
	cmpl	$2, -20(%rsp)           # 4-byte Folded Reload
	jl	.LBB4_24
# BB#15:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB4_14 Depth=2
	movq	-120(%rsp), %rax        # 8-byte Reload
	leal	1(%rax), %eax
	movq	192(%rsp), %rcx
	movq	(%rcx), %rsi
	movl	16(%rcx), %edi
	movl	20(%rcx), %r9d
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx), %r14
	movl	20(%rcx), %r11d
	movl	%edi, %ebp
	movl	-112(%rsp), %edx        # 4-byte Reload
	imull	%edx, %ebp
	movl	16(%rcx), %ecx
	imull	%edx, %ecx
	leaq	4(%r14), %rdx
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leal	1(%rcx), %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%r14,%rcx,4), %rbx
	movq	%rbx, -80(%rsp)         # 8-byte Spill
	leaq	4(%rsi), %rbx
	movq	%rbx, -88(%rsp)         # 8-byte Spill
	imull	%eax, %edi
	incl	%edi
	movq	%rdi, -56(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rcx,4), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	leaq	52(%rsi), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	leal	1(%rbp), %ecx
	imull	%r9d, %ecx
	movl	%r11d, %eax
	movq	%rdx, %r15
	imull	%edx, %eax
	leaq	52(%r14), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	leaq	12(%r14), %rdx
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	movq	%rsi, -96(%rsp)         # 8-byte Spill
	leaq	12(%rsi), %rdx
	movq	%rdx, -48(%rsp)         # 8-byte Spill
	movl	$1, %r13d
	xorl	%r12d, %r12d
	jmp	.LBB4_16
.LBB4_29:                               # %vector.body.preheader
                                        #   in Loop: Header=BB4_16 Depth=3
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	jne	.LBB4_30
# BB#31:                                # %vector.body.prol
                                        #   in Loop: Header=BB4_16 Depth=3
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%r13), %ecx
	imull	%r9d, %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%r13), %edx
	imull	%r11d, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	movq	-96(%rsp), %rsi         # 8-byte Reload
	movups	4(%rsi,%rcx,4), %xmm1
	movups	20(%rsi,%rcx,4), %xmm2
	movups	%xmm1, 4(%r14,%rdx,4)
	movups	%xmm2, 20(%r14,%rdx,4)
	movl	$8, %edx
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_33
	jmp	.LBB4_35
.LBB4_30:                               #   in Loop: Header=BB4_16 Depth=3
	xorl	%edx, %edx
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB4_35
.LBB4_33:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB4_16 Depth=3
	movq	-8(%rsp), %rcx          # 8-byte Reload
	subq	%rdx, %rcx
	leaq	(%rdx,%r8), %rsi
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rsi,4), %rbx
	addq	%rax, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,4), %rsi
	.p2align	4, 0x90
.LBB4_34:                               # %vector.body
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_16 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-48(%rbx), %xmm1
	movups	-32(%rbx), %xmm2
	movups	%xmm1, -48(%rsi)
	movups	%xmm2, -32(%rsi)
	movups	-16(%rbx), %xmm1
	movups	(%rbx), %xmm2
	movups	%xmm1, -16(%rsi)
	movups	%xmm2, (%rsi)
	addq	$64, %rbx
	addq	$64, %rsi
	addq	$-16, %rcx
	jne	.LBB4_34
.LBB4_35:                               # %middle.block
                                        #   in Loop: Header=BB4_16 Depth=3
	movq	-16(%rsp), %rcx         # 8-byte Reload
	cmpq	-8(%rsp), %rcx          # 8-byte Folded Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	jne	.LBB4_18
	jmp	.LBB4_36
	.p2align	4, 0x90
.LBB4_16:                               # %.preheader.us.us
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_34 Depth 4
                                        #         Child Loop BB4_20 Depth 4
                                        #         Child Loop BB4_23 Depth 4
	cltq
	movslq	%ecx, %r8
	cmpq	$7, -16(%rsp)           # 8-byte Folded Reload
	jbe	.LBB4_17
# BB#26:                                # %min.iters.checked
                                        #   in Loop: Header=BB4_16 Depth=3
	cmpq	$0, -8(%rsp)            # 8-byte Folded Reload
	je	.LBB4_17
# BB#27:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_16 Depth=3
	leal	(%r15,%r12), %ecx
	imull	%r11d, %ecx
	movslq	%ecx, %rcx
	movq	-64(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rsi
	movq	-56(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r12), %edx
	imull	%r9d, %edx
	movslq	%edx, %rdx
	movq	-72(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rdx,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB4_29
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_16 Depth=3
	movq	-80(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rcx
	movq	-88(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,4), %rdx
	cmpq	%rcx, %rdx
	jae	.LBB4_29
	.p2align	4, 0x90
.LBB4_17:                               #   in Loop: Header=BB4_16 Depth=3
	movl	$1, %ecx
.LBB4_18:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB4_16 Depth=3
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	subl	%ecx, %esi
	movq	-16(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	andq	$3, %rsi
	je	.LBB4_21
# BB#19:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB4_16 Depth=3
	leaq	(%r14,%rax,4), %rdx
	movq	-96(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r8,4), %rdi
	negq	%rsi
	.p2align	4, 0x90
.LBB4_20:                               # %scalar.ph.prol
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_16 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%rdi,%rcx,4), %ebp
	movl	%ebp, (%rdx,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB4_20
.LBB4_21:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB4_16 Depth=3
	cmpq	$3, %rbx
	jb	.LBB4_36
# BB#22:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB4_16 Depth=3
	movq	8(%rsp), %r10           # 8-byte Reload
	subq	%rcx, %r10
	movq	%rax, %rdx
	addq	%rcx, %rdx
	movq	-40(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,4), %rbx
	movq	%r8, %rdx
	addq	%rcx, %rdx
	movq	-48(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB4_23:                               # %scalar.ph
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_16 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	-12(%rcx), %edx
	movl	%edx, -12(%rbx)
	movl	-8(%rcx), %edx
	movl	%edx, -8(%rbx)
	movl	-4(%rcx), %edx
	movl	%edx, -4(%rbx)
	movl	(%rcx), %edx
	movl	%edx, (%rbx)
	addq	$16, %rbx
	addq	$16, %rcx
	addq	$-4, %r10
	jne	.LBB4_23
.LBB4_36:                               # %._crit_edge422.us.us
                                        #   in Loop: Header=BB4_16 Depth=3
	incl	%r13d
	incl	%r12d
	leal	(%r8,%r9), %ecx
	leal	(%rax,%r11), %eax
	cmpl	-100(%rsp), %r13d       # 4-byte Folded Reload
	jne	.LBB4_16
.LBB4_24:                               # %._crit_edge424.us
                                        #   in Loop: Header=BB4_14 Depth=2
	movl	-112(%rsp), %eax        # 4-byte Reload
	incl	%eax
	movq	-120(%rsp), %rcx        # 8-byte Reload
	incl	%ecx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	movl	%eax, -112(%rsp)        # 4-byte Spill
	cmpl	-104(%rsp), %eax        # 4-byte Folded Reload
	jne	.LBB4_14
# BB#25:                                #   in Loop: Header=BB4_3 Depth=1
	movq	(%rsp), %r9             # 8-byte Reload
.LBB4_10:                               #   in Loop: Header=BB4_3 Depth=1
	movl	-32(%rsp), %edi         # 4-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
.LBB4_11:                               # %._crit_edge428
                                        #   in Loop: Header=BB4_3 Depth=1
	incl	%eax
	cmpl	%edi, %eax
	jne	.LBB4_3
	jmp	.LBB4_12
.LBB4_1:
                                        # implicit-def: %XMM0
.LBB4_12:                               # %._crit_edge437
	addq	$128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	jacobi, .Lfunc_end4-jacobi
	.cfi_endproc

	.globl	clearMat
	.p2align	4, 0x90
	.type	clearMat,@function
clearMat:                               # @clearMat
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 16
.Lcfi56:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	callq	free
.LBB5_2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end5:
	.size	clearMat, .Lfunc_end5-clearMat
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4629981891913580544     # double 34
	.text
	.globl	fflop
	.p2align	4, 0x90
	.type	fflop,@function
fflop:                                  # @fflop
	.cfi_startproc
# BB#0:
	addl	$-2, %edx
	cvtsi2sdl	%edx, %xmm0
	addl	$-2, %esi
	cvtsi2sdl	%esi, %xmm1
	mulsd	%xmm0, %xmm1
	addl	$-2, %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	.LCPI6_0(%rip), %xmm0
	retq
.Lfunc_end6:
	.size	fflop, .Lfunc_end6-fflop
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	mflops
	.p2align	4, 0x90
	.type	mflops,@function
mflops:                                 # @mflops
	.cfi_startproc
# BB#0:
	divsd	%xmm0, %xmm1
	mulsd	.LCPI7_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm1, %xmm0
	retq
.Lfunc_end7:
	.size	mflops, .Lfunc_end7-mflops
	.cfi_endproc

	.globl	set_param
	.p2align	4, 0x90
	.type	set_param,@function
set_param:                              # @set_param
	.cfi_startproc
# BB#0:
	movb	(%rsi), %al
	movl	%eax, %ecx
	addb	$-76, %cl
	cmpb	$44, %cl
	ja	.LBB8_33
# BB#1:
	movzbl	%cl, %edx
	jmpq	*.LJTI8_0(,%rdx,8)
.LBB8_34:
	cmpb	$0, 1(%rsi)
	jne	.LBB8_33
# BB#35:
	movl	$512, %eax              # imm = 0x200
	movl	$256, %ecx              # imm = 0x100
	jmp	.LBB8_5
.LBB8_2:
	cmpb	$83, 1(%rsi)
	jne	.LBB8_6
# BB#3:
	cmpb	$0, 2(%rsi)
	je	.LBB8_4
.LBB8_6:                                # %.thread337
	cmpb	$44, %cl
	ja	.LBB8_33
# BB#7:                                 # %.thread337
	jmpq	*.LJTI8_1(,%rdx,8)
.LBB8_8:
	cmpb	$115, 1(%rsi)
	jne	.LBB8_10
# BB#9:
	cmpb	$0, 2(%rsi)
	je	.LBB8_4
.LBB8_10:                               # %.thread339
	cmpb	$44, %cl
	ja	.LBB8_33
# BB#11:                                # %.thread339
	jmpq	*.LJTI8_2(,%rdx,8)
.LBB8_12:
	cmpb	$0, 1(%rsi)
	je	.LBB8_13
# BB#14:                                # %.thread340
	cmpb	$44, %cl
	ja	.LBB8_33
# BB#15:                                # %.thread340
	jmpq	*.LJTI8_3(,%rdx,8)
.LBB8_16:
	cmpb	$0, 1(%rsi)
	je	.LBB8_13
# BB#17:                                # %.thread341
	cmpb	$44, %cl
	ja	.LBB8_33
# BB#18:                                # %.thread341
	jmpq	*.LJTI8_4(,%rdx,8)
.LBB8_19:
	cmpb	$0, 1(%rsi)
	je	.LBB8_20
# BB#21:                                # %.thread342
	cmpb	$44, %cl
	ja	.LBB8_33
# BB#22:                                # %.thread342
	jmpq	*.LJTI8_5(,%rdx,8)
.LBB8_23:
	cmpb	$0, 1(%rsi)
	je	.LBB8_20
# BB#24:                                # %.thread343
	cmpb	$107, %al
	jg	.LBB8_30
# BB#25:                                # %.thread343
	cmpb	$76, %al
	je	.LBB8_34
# BB#26:                                # %.thread343
	cmpb	$88, %al
	jne	.LBB8_33
.LBB8_27:
	cmpb	$76, 1(%rsi)
	je	.LBB8_28
	jmp	.LBB8_33
.LBB8_20:
	movl	$256, %eax              # imm = 0x100
	movl	$128, %ecx
	jmp	.LBB8_5
.LBB8_13:
	movl	$128, %eax
	movl	$64, %ecx
	jmp	.LBB8_5
.LBB8_30:                               # %.thread343
	cmpb	$108, %al
	je	.LBB8_34
# BB#31:                                # %.thread343
	cmpb	$120, %al
	jne	.LBB8_33
.LBB8_32:
	cmpb	$108, 1(%rsi)
	jne	.LBB8_33
.LBB8_28:
	cmpb	$0, 2(%rsi)
	jne	.LBB8_33
# BB#29:
	movl	$1024, %eax             # imm = 0x400
	movl	$512, %ecx              # imm = 0x200
.LBB8_5:
	movl	%ecx, (%rdi)
	movl	%ecx, 4(%rdi)
	movl	%eax, 8(%rdi)
	retq
.LBB8_4:
	movl	$64, %eax
	movl	$32, %ecx
	jmp	.LBB8_5
.LBB8_33:                               # %.thread335
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 16
	movl	$.Lstr, %edi
	callq	puts
	movl	$6, %edi
	callq	exit
.Lfunc_end8:
	.size	set_param, .Lfunc_end8-set_param
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_34
	.quad	.LBB8_19
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_12
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_2
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_34
	.quad	.LBB8_23
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_16
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_8
.LJTI8_1:
	.quad	.LBB8_34
	.quad	.LBB8_19
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_12
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_27
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_34
	.quad	.LBB8_23
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_16
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_8
.LJTI8_2:
	.quad	.LBB8_34
	.quad	.LBB8_19
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_12
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_27
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_34
	.quad	.LBB8_23
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_16
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_32
.LJTI8_3:
	.quad	.LBB8_34
	.quad	.LBB8_19
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_27
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_34
	.quad	.LBB8_23
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_16
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_32
.LJTI8_4:
	.quad	.LBB8_34
	.quad	.LBB8_19
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_27
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_34
	.quad	.LBB8_23
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_32
.LJTI8_5:
	.quad	.LBB8_34
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_27
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_34
	.quad	.LBB8_23
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_33
	.quad	.LBB8_32

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_0:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.globl	second
	.p2align	4, 0x90
	.type	second,@function
second:                                 # @second
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 32
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movslq	second.base_sec(%rip), %rdx
	movslq	second.base_usec(%rip), %rcx
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rax
	movl	%ecx, %edi
	orl	%edx, %edi
	je	.LBB9_1
# BB#2:
	subq	%rdx, %rsi
	cvtsi2sdq	%rsi, %xmm1
	subq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI9_0(%rip), %xmm0
	addsd	%xmm1, %xmm0
	addq	$24, %rsp
	retq
.LBB9_1:
	movl	%esi, second.base_sec(%rip)
	movl	%eax, second.base_usec(%rip)
	xorps	%xmm0, %xmm0
	addq	$24, %rsp
	retq
.Lfunc_end9:
	.size	second, .Lfunc_end9-second
	.cfi_endproc

	.type	omega,@object           # @omega
	.data
	.globl	omega
	.p2align	2
omega:
	.long	1061997773              # float 0.800000011
	.size	omega, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"mimax = %d mjmax = %d mkmax = %d\n"
	.size	.L.str, 34

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"imax = %d jmax = %d kmax =%d\n"
	.size	.L.str.1, 30

	.type	p,@object               # @p
	.comm	p,24,8
	.type	bnd,@object             # @bnd
	.comm	bnd,24,8
	.type	wrk1,@object            # @wrk1
	.comm	wrk1,24,8
	.type	wrk2,@object            # @wrk2
	.comm	wrk2,24,8
	.type	a,@object               # @a
	.comm	a,24,8
	.type	b,@object               # @b
	.comm	b,24,8
	.type	c,@object               # @c
	.comm	c,24,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" Loop executed for %d times\n"
	.size	.L.str.2, 29

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" Gosa : %e \n"
	.size	.L.str.3, 13

	.type	second.base_sec,@object # @second.base_sec
	.local	second.base_sec
	.comm	second.base_sec,4,4
	.type	second.base_usec,@object # @second.base_usec
	.local	second.base_usec
	.comm	second.base_usec,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Invalid input character !!"
	.size	.Lstr, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
