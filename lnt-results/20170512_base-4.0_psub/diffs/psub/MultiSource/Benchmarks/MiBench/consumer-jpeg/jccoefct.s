	.text
	.file	"jccoefct.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	128                     # 0x80
	.quad	256                     # 0x100
.LCPI0_1:
	.quad	384                     # 0x180
	.quad	512                     # 0x200
.LCPI0_2:
	.quad	640                     # 0x280
	.quad	768                     # 0x300
.LCPI0_3:
	.quad	896                     # 0x380
	.quad	1024                    # 0x400
	.text
	.globl	jinit_c_coef_controller
	.p2align	4, 0x90
	.type	jinit_c_coef_controller,@function
jinit_c_coef_controller:                # @jinit_c_coef_controller
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r15
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$192, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 448(%r15)
	movq	$start_pass_coef, (%r14)
	testl	%ebp, %ebp
	je	.LBB0_4
# BB#1:
	cmpl	$0, 68(%r15)
	jle	.LBB0_5
# BB#2:                                 # %.lr.ph
	movq	80(%r15), %rbx
	addq	$8, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rax
	movq	40(%rax), %r13
	movl	20(%rbx), %edi
	movslq	(%rbx), %rsi
	callq	jround_up
	movq	%rax, %r12
	movl	24(%rbx), %edi
	movslq	4(%rbx), %rsi
	callq	jround_up
	movl	4(%rbx), %r9d
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r12d, %ecx
	movl	%eax, %r8d
	callq	*%r13
	movq	%rax, 112(%r14,%rbp,8)
	incq	%rbp
	movslq	68(%r15), %rax
	addq	$96, %rbx
	cmpq	%rax, %rbp
	jl	.LBB0_3
	jmp	.LBB0_5
.LBB0_4:
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$1280, %edx             # imm = 0x500
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	%rax, 32(%r14)
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [128,256]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 40(%r14)
	movdqa	.LCPI0_1(%rip), %xmm1   # xmm1 = [384,512]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 56(%r14)
	movdqa	.LCPI0_2(%rip), %xmm1   # xmm1 = [640,768]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 72(%r14)
	paddq	.LCPI0_3(%rip), %xmm0
	movdqu	%xmm0, 88(%r14)
	addq	$1152, %rax             # imm = 0x480
	movq	%rax, 104(%r14)
	movq	$0, 112(%r14)
.LBB0_5:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jinit_c_coef_controller, .Lfunc_end0-jinit_c_coef_controller
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_coef,@function
start_pass_coef:                        # @start_pass_coef
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	448(%rdi), %rbx
	movl	$0, 16(%rbx)
	movl	$1, %eax
	cmpl	$1, 316(%rdi)
	jg	.LBB1_4
# BB#1:
	cmpl	$1, 312(%rdi)
	movq	320(%rdi), %rax
	jne	.LBB1_2
# BB#3:
	movl	72(%rax), %eax
	jmp	.LBB1_4
.LBB1_2:
	movl	12(%rax), %eax
.LBB1_4:                                # %start_iMCU_row.exit
	movl	%eax, 28(%rbx)
	movq	$0, 20(%rbx)
	cmpl	$3, %esi
	je	.LBB1_10
# BB#5:                                 # %start_iMCU_row.exit
	cmpl	$2, %esi
	je	.LBB1_13
# BB#6:                                 # %start_iMCU_row.exit
	testl	%esi, %esi
	jne	.LBB1_17
# BB#7:
	cmpq	$0, 112(%rbx)
	je	.LBB1_9
# BB#8:
	movq	(%rdi), %rax
	movl	$4, 40(%rax)
	callq	*(%rax)
.LBB1_9:
	movq	$compress_data, 8(%rbx)
	popq	%rbx
	retq
.LBB1_10:
	cmpq	$0, 112(%rbx)
	jne	.LBB1_12
# BB#11:
	movq	(%rdi), %rax
	movl	$4, 40(%rax)
	callq	*(%rax)
.LBB1_12:
	movq	$compress_first_pass, 8(%rbx)
	popq	%rbx
	retq
.LBB1_13:
	cmpq	$0, 112(%rbx)
	jne	.LBB1_15
# BB#14:
	movq	(%rdi), %rax
	movl	$4, 40(%rax)
	callq	*(%rax)
.LBB1_15:
	movq	$compress_output, 8(%rbx)
	popq	%rbx
	retq
.LBB1_17:
	movq	(%rdi), %rax
	movl	$4, 40(%rax)
	popq	%rbx
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end1:
	.size	start_pass_coef, .Lfunc_end1-start_pass_coef
	.cfi_endproc

	.p2align	4, 0x90
	.type	compress_data,@function
compress_data:                          # @compress_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 208
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movq	448(%rdi), %r9
	movl	24(%r9), %edx
	movl	28(%r9), %eax
	movq	%rdx, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	cmpl	%eax, %edx
	jge	.LBB2_1
# BB#2:                                 # %.lr.ph135
	movl	312(%rdi), %ecx
	movl	352(%rdi), %edx
	decl	%edx
	movl	%edx, 44(%rsp)          # 4-byte Spill
	decl	%ecx
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	leaq	316(%rdi), %r12
	leaq	32(%r9), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movl	20(%r9), %ebx
	leaq	56(%r9), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	leaq	40(%r9), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%r12, 112(%rsp)         # 8-byte Spill
.LBB2_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
                                        #       Child Loop BB2_8 Depth 3
                                        #         Child Loop BB2_10 Depth 4
                                        #           Child Loop BB2_50 Depth 5
                                        #           Child Loop BB2_53 Depth 5
                                        #           Child Loop BB2_44 Depth 5
                                        #       Child Loop BB2_15 Depth 3
                                        #         Child Loop BB2_17 Depth 4
                                        #           Child Loop BB2_28 Depth 5
                                        #           Child Loop BB2_31 Depth 5
                                        #           Child Loop BB2_36 Depth 5
	cmpl	44(%rsp), %ebx          # 4-byte Folded Reload
	ja	.LBB2_62
# BB#4:                                 # %.preheader.lr.ph
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	(,%rax,8), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB2_5:                                # %.preheader
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_8 Depth 3
                                        #         Child Loop BB2_10 Depth 4
                                        #           Child Loop BB2_50 Depth 5
                                        #           Child Loop BB2_53 Depth 5
                                        #           Child Loop BB2_44 Depth 5
                                        #       Child Loop BB2_15 Depth 3
                                        #         Child Loop BB2_17 Depth 4
                                        #           Child Loop BB2_28 Depth 5
                                        #           Child Loop BB2_31 Depth 5
                                        #           Child Loop BB2_36 Depth 5
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.LBB2_58
# BB#6:                                 # %.lr.ph126
                                        #   in Loop: Header=BB2_5 Depth=2
	cmpl	44(%rsp), %ebx          # 4-byte Folded Reload
	movl	%ebx, 80(%rsp)          # 4-byte Spill
	jae	.LBB2_7
# BB#14:                                # %.lr.ph126.split.us.preheader
                                        #   in Loop: Header=BB2_5 Depth=2
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_15:                               # %.lr.ph126.split.us
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_17 Depth 4
                                        #           Child Loop BB2_28 Depth 5
                                        #           Child Loop BB2_31 Depth 5
                                        #           Child Loop BB2_36 Depth 5
	movq	320(%rbp,%rdx,8), %r14
	cmpl	$0, 56(%r14)
	jle	.LBB2_23
# BB#16:                                # %.lr.ph123.us
                                        #   in Loop: Header=BB2_15 Depth=3
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movl	52(%r14), %eax
	movslq	%eax, %rcx
	movl	64(%r14), %edx
	imull	%ebx, %edx
	movl	%edx, 104(%rsp)         # 4-byte Spill
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	incq	%rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movq	%rax, 24(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	76(%rsp), %r15d         # 4-byte Reload
	.p2align	4, 0x90
.LBB2_17:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_28 Depth 5
                                        #           Child Loop BB2_31 Depth 5
                                        #           Child Loop BB2_36 Depth 5
	movl	84(%rsp), %ecx          # 4-byte Reload
	cmpl	%ecx, 16(%r9)
	jb	.LBB2_24
# BB#18:                                #   in Loop: Header=BB2_17 Depth=4
	movq	56(%rsp), %rcx          # 8-byte Reload
	leal	(%r12,%rcx), %ecx
	cmpl	72(%r14), %ecx
	jge	.LBB2_19
.LBB2_24:                               #   in Loop: Header=BB2_17 Depth=4
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	480(%rdi), %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rdx
	movslq	%r13d, %rbx
	movq	32(%r9,%rbx,8), %rcx
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, (%rsp)
	movq	%r14, %rsi
	movl	%r15d, %r8d
	movl	104(%rsp), %r9d         # 4-byte Reload
	callq	*8(%rax)
	movl	52(%r14), %eax
	subl	%ebp, %eax
	jle	.LBB2_26
# BB#25:                                #   in Loop: Header=BB2_17 Depth=4
	movq	24(%rsp), %rbp          # 8-byte Reload
	leal	(%r13,%rbp), %ecx
	movslq	%ecx, %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	32(%rdx,%rcx,8), %rdi
	movslq	%eax, %rsi
	shlq	$7, %rsi
	callq	jzero_far
	movslq	52(%r14), %rax
	cmpl	%eax, %ebp
	jge	.LBB2_26
# BB#32:                                # %.lr.ph117.us
                                        #   in Loop: Header=BB2_17 Depth=4
	movl	%eax, %ecx
	subl	24(%rsp), %ecx          # 4-byte Folded Reload
	testb	$1, %cl
	movq	88(%rsp), %rcx          # 8-byte Reload
	je	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_17 Depth=4
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsi,%rcx,8), %rdx
	movzwl	(%rdx), %edx
	movq	32(%rsi,%rcx,8), %rcx
	movw	%dx, (%rcx)
	movq	48(%rsp), %rcx          # 8-byte Reload
.LBB2_34:                               # %.prol.loopexit188
                                        #   in Loop: Header=BB2_17 Depth=4
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	je	.LBB2_26
# BB#35:                                # %.lr.ph117.us.new
                                        #   in Loop: Header=BB2_17 Depth=4
	movq	128(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rbx,8), %rdx
	.p2align	4, 0x90
.LBB2_36:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        #         Parent Loop BB2_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	-16(%rdx,%rcx,8), %rsi
	movzwl	(%rsi), %esi
	movq	-8(%rdx,%rcx,8), %rdi
	movw	%si, (%rdi)
	movq	-8(%rdx,%rcx,8), %rsi
	movzwl	(%rsi), %esi
	movq	(%rdx,%rcx,8), %rdi
	movw	%si, (%rdi)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jl	.LBB2_36
	jmp	.LBB2_26
	.p2align	4, 0x90
.LBB2_19:                               #   in Loop: Header=BB2_17 Depth=4
	movslq	%r13d, %rbx
	movq	32(%r9,%rbx,8), %rdi
	movslq	%eax, %rsi
	shlq	$7, %rsi
	callq	jzero_far
	movslq	52(%r14), %rax
	testq	%rax, %rax
	jle	.LBB2_26
# BB#20:                                # %.lr.ph.us
                                        #   in Loop: Header=BB2_17 Depth=4
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	24(%rcx,%rbx,8), %rcx
	leaq	-1(%rax), %r8
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB2_21
# BB#27:                                # %.prol.preheader182
                                        #   in Loop: Header=BB2_17 Depth=4
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %r9
	leaq	(%rdx,%rbx,8), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_28:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        #         Parent Loop BB2_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rcx), %ebx
	movq	(%rsi,%rdx,8), %rbp
	movw	%bx, (%rbp)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB2_28
	jmp	.LBB2_29
.LBB2_21:                               #   in Loop: Header=BB2_17 Depth=4
	movq	%rbx, %r9
	xorl	%edx, %edx
.LBB2_29:                               # %.prol.loopexit183
                                        #   in Loop: Header=BB2_17 Depth=4
	cmpq	$3, %r8
	jb	.LBB2_26
# BB#30:                                # %.lr.ph.us.new
                                        #   in Loop: Header=BB2_17 Depth=4
	movq	120(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r9,8), %rsi
	.p2align	4, 0x90
.LBB2_31:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        #         Parent Loop BB2_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rcx), %edi
	movq	-24(%rsi,%rdx,8), %rbx
	movw	%di, (%rbx)
	movzwl	(%rcx), %edi
	movq	-16(%rsi,%rdx,8), %rbx
	movw	%di, (%rbx)
	movzwl	(%rcx), %edi
	movq	-8(%rsi,%rdx,8), %rbx
	movw	%di, (%rbx)
	movzwl	(%rcx), %edi
	movq	(%rsi,%rdx,8), %rbx
	movw	%di, (%rbx)
	addq	$4, %rdx
	cmpq	%rax, %rdx
	jl	.LBB2_31
	.p2align	4, 0x90
.LBB2_26:                               # %.loopexit.us
                                        #   in Loop: Header=BB2_17 Depth=4
	movl	52(%r14), %eax
	addl	%eax, %r13d
	addl	$8, %r15d
	incl	%r12d
	cmpl	56(%r14), %r12d
	movq	16(%rsp), %r9           # 8-byte Reload
	jl	.LBB2_17
# BB#22:                                # %._crit_edge.us.loopexit
                                        #   in Loop: Header=BB2_15 Depth=3
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	112(%rsp), %r12         # 8-byte Reload
	movl	(%r12), %ecx
	movl	80(%rsp), %ebx          # 4-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
.LBB2_23:                               # %._crit_edge.us
                                        #   in Loop: Header=BB2_15 Depth=3
	incq	%rdx
	movslq	%ecx, %rax
	cmpq	%rax, %rdx
	jl	.LBB2_15
	jmp	.LBB2_58
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph126.split.preheader
                                        #   in Loop: Header=BB2_5 Depth=2
	xorl	%esi, %esi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph126.split
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_10 Depth 4
                                        #           Child Loop BB2_50 Depth 5
                                        #           Child Loop BB2_53 Depth 5
                                        #           Child Loop BB2_44 Depth 5
	movq	320(%rbp,%rsi,8), %r10
	cmpl	$0, 56(%r10)
	jle	.LBB2_57
# BB#9:                                 # %.lr.ph123
                                        #   in Loop: Header=BB2_8 Depth=3
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movslq	68(%r10), %rcx
	movl	64(%r10), %edx
	imull	%ebx, %edx
	movl	%edx, 88(%rsp)          # 4-byte Spill
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	incq	%rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	movl	76(%rsp), %r13d         # 4-byte Reload
	movq	%r10, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_10:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_8 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_50 Depth 5
                                        #           Child Loop BB2_53 Depth 5
                                        #           Child Loop BB2_44 Depth 5
	movl	%eax, %r15d
	movl	84(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, 16(%r9)
	jb	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_10 Depth=4
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	(%r14,%rax), %eax
	cmpl	72(%r10), %eax
	jge	.LBB2_45
.LBB2_12:                               #   in Loop: Header=BB2_10 Depth=4
	movq	480(%rbp), %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rdx
	movslq	%r15d, %rbx
	movq	32(%r9,%rbx,8), %rcx
	movq	%r15, %r12
	movq	104(%rsp), %r15         # 8-byte Reload
	movl	%r15d, (%rsp)
	movq	%rbp, %rdi
	movq	%r10, %rsi
	movl	%r13d, %r8d
	movq	%r9, %rbp
	movl	88(%rsp), %r9d          # 4-byte Reload
	callq	*8(%rax)
	movq	24(%rsp), %r10          # 8-byte Reload
	movl	52(%r10), %eax
	cmpl	%r15d, %eax
	jle	.LBB2_13
# BB#37:                                #   in Loop: Header=BB2_10 Depth=4
	movq	%r12, 48(%rsp)          # 8-byte Spill
	leal	(%r12,%r15), %ecx
	movslq	%ecx, %rcx
	movq	32(%rbp,%rcx,8), %rdi
	subl	%r15d, %eax
	movslq	%eax, %rsi
	shlq	$7, %rsi
	callq	jzero_far
	movq	24(%rsp), %r10          # 8-byte Reload
	movl	52(%r10), %eax
	cmpl	%eax, %r15d
	jge	.LBB2_38
# BB#39:                                # %.lr.ph117
                                        #   in Loop: Header=BB2_10 Depth=4
	movslq	%eax, %rcx
	movl	%eax, %edx
	subl	%r15d, %edx
	testb	$1, %dl
	movq	%r15, %rdx
	movq	16(%rsp), %r9           # 8-byte Reload
	je	.LBB2_41
# BB#40:                                #   in Loop: Header=BB2_10 Depth=4
	leaq	(%r15,%rbx), %rdx
	movq	24(%r9,%rdx,8), %rsi
	movzwl	(%rsi), %esi
	movq	32(%r9,%rdx,8), %rdx
	movw	%si, (%rdx)
	movq	136(%rsp), %rdx         # 8-byte Reload
.LBB2_41:                               # %.prol.loopexit179
                                        #   in Loop: Header=BB2_10 Depth=4
	cmpq	136(%rsp), %rcx         # 8-byte Folded Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	jne	.LBB2_43
# BB#42:                                #   in Loop: Header=BB2_10 Depth=4
	movq	48(%rsp), %r15          # 8-byte Reload
	jmp	.LBB2_55
	.p2align	4, 0x90
.LBB2_13:                               #   in Loop: Header=BB2_10 Depth=4
	movq	%rbp, %r9
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%r12, %r15
	jmp	.LBB2_55
	.p2align	4, 0x90
.LBB2_45:                               #   in Loop: Header=BB2_10 Depth=4
	movslq	%r15d, %r12
	movq	32(%r9,%r12,8), %rdi
	movslq	52(%r10), %rsi
	shlq	$7, %rsi
	movq	%r9, %rbx
	callq	jzero_far
	movq	24(%rsp), %r10          # 8-byte Reload
	movslq	52(%r10), %rax
	testq	%rax, %rax
	jle	.LBB2_46
# BB#47:                                # %.lr.ph
                                        #   in Loop: Header=BB2_10 Depth=4
	movq	24(%rbx,%r12,8), %rcx
	leaq	-1(%rax), %r8
	movq	%rax, %rdi
	andq	$3, %rdi
	movq	%rbx, %r9
	je	.LBB2_48
# BB#49:                                # %.prol.preheader
                                        #   in Loop: Header=BB2_10 Depth=4
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r12,8), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_50:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_8 Depth=3
                                        #         Parent Loop BB2_10 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rcx), %ebx
	movq	(%rsi,%rdx,8), %rbp
	movw	%bx, (%rbp)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB2_50
	jmp	.LBB2_51
.LBB2_38:                               #   in Loop: Header=BB2_10 Depth=4
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	jmp	.LBB2_55
.LBB2_43:                               # %.lr.ph117.new
                                        #   in Loop: Header=BB2_10 Depth=4
	movq	128(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rbx,8), %rsi
	movq	48(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_44:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_8 Depth=3
                                        #         Parent Loop BB2_10 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	-16(%rsi,%rdx,8), %rdi
	movzwl	(%rdi), %edi
	movq	-8(%rsi,%rdx,8), %rbx
	movw	%di, (%rbx)
	movq	-8(%rsi,%rdx,8), %rdi
	movzwl	(%rdi), %edi
	movq	(%rsi,%rdx,8), %rbx
	movw	%di, (%rbx)
	addq	$2, %rdx
	cmpq	%rcx, %rdx
	jl	.LBB2_44
	jmp	.LBB2_55
.LBB2_46:                               #   in Loop: Header=BB2_10 Depth=4
	movq	%rbx, %r9
	jmp	.LBB2_55
.LBB2_48:                               #   in Loop: Header=BB2_10 Depth=4
	xorl	%edx, %edx
.LBB2_51:                               # %.prol.loopexit
                                        #   in Loop: Header=BB2_10 Depth=4
	cmpq	$3, %r8
	jb	.LBB2_54
# BB#52:                                # %.lr.ph.new
                                        #   in Loop: Header=BB2_10 Depth=4
	movq	120(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r12,8), %rsi
	.p2align	4, 0x90
.LBB2_53:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_8 Depth=3
                                        #         Parent Loop BB2_10 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rcx), %edi
	movq	-24(%rsi,%rdx,8), %rbp
	movw	%di, (%rbp)
	movzwl	(%rcx), %edi
	movq	-16(%rsi,%rdx,8), %rbp
	movw	%di, (%rbp)
	movzwl	(%rcx), %edi
	movq	-8(%rsi,%rdx,8), %rbp
	movw	%di, (%rbp)
	movzwl	(%rcx), %edi
	movq	(%rsi,%rdx,8), %rbp
	movw	%di, (%rbp)
	addq	$4, %rdx
	cmpq	%rax, %rdx
	jl	.LBB2_53
.LBB2_54:                               #   in Loop: Header=BB2_10 Depth=4
	movq	32(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_55:                               # %.loopexit
                                        #   in Loop: Header=BB2_10 Depth=4
	addl	%r15d, %eax
	addl	$8, %r13d
	incl	%r14d
	cmpl	56(%r10), %r14d
	jl	.LBB2_10
# BB#56:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_8 Depth=3
	movq	112(%rsp), %r12         # 8-byte Reload
	movl	(%r12), %ecx
	movl	80(%rsp), %ebx          # 4-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
.LBB2_57:                               # %._crit_edge
                                        #   in Loop: Header=BB2_8 Depth=3
	incq	%rsi
	movslq	%ecx, %rdx
	cmpq	%rdx, %rsi
	jl	.LBB2_8
.LBB2_58:                               # %._crit_edge127
                                        #   in Loop: Header=BB2_5 Depth=2
	movq	%rbp, %rdi
	movq	488(%rdi), %rax
	movq	96(%rsp), %rsi          # 8-byte Reload
	callq	*8(%rax)
	testl	%eax, %eax
	je	.LBB2_59
# BB#60:                                #   in Loop: Header=BB2_5 Depth=2
	incl	%ebx
	cmpl	44(%rsp), %ebx          # 4-byte Folded Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	jbe	.LBB2_5
# BB#61:                                # %._crit_edge131.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	28(%r9), %eax
.LBB2_62:                               # %._crit_edge131
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$0, 20(%r9)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rdx
	incl	%edx
	xorl	%ebx, %ebx
	movq	%rdx, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	cmpl	%eax, %edx
	jl	.LBB2_3
# BB#63:                                # %._crit_edge136.loopexit
	movq	%rbp, %rdi
	movq	448(%rdi), %rcx
	jmp	.LBB2_64
.LBB2_1:                                # %.._crit_edge136_crit_edge
	leaq	316(%rdi), %r12
	movq	%r9, %rcx
.LBB2_64:                               # %._crit_edge136
	incl	16(%r9)
	movl	$1, %eax
	cmpl	$1, (%r12)
	movl	$1, %edx
	jg	.LBB2_68
# BB#65:
	movl	312(%rdi), %esi
	decl	%esi
	movq	320(%rdi), %rdx
	cmpl	%esi, 16(%rcx)
	jae	.LBB2_67
# BB#66:
	movl	12(%rdx), %edx
	jmp	.LBB2_68
.LBB2_59:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 24(%rax)
	movl	%ebx, 20(%rax)
	xorl	%eax, %eax
	jmp	.LBB2_69
.LBB2_67:
	movl	72(%rdx), %edx
.LBB2_68:                               # %start_iMCU_row.exit
	movl	%edx, 28(%rcx)
	movq	$0, 20(%rcx)
.LBB2_69:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	compress_data, .Lfunc_end2-compress_data
	.cfi_endproc

	.p2align	4, 0x90
	.type	compress_first_pass,@function
compress_first_pass:                    # @compress_first_pass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 176
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	cmpl	$0, 68(%r13)
	jle	.LBB3_36
# BB#1:                                 # %.lr.ph157
	movq	448(%r13), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	312(%r13), %eax
	decl	%eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	80(%r13), %r12
	xorl	%ebx, %ebx
	movq	%r13, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_8 Depth 2
                                        #     Child Loop BB3_10 Depth 2
                                        #       Child Loop BB3_13 Depth 3
                                        #       Child Loop BB3_16 Depth 3
                                        #     Child Loop BB3_24 Depth 2
                                        #       Child Loop BB3_26 Depth 3
                                        #         Child Loop BB3_29 Depth 4
                                        #         Child Loop BB3_32 Depth 4
                                        #     Child Loop BB3_22 Depth 2
	movq	8(%r13), %rax
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	112(%rbp,%rbx,8), %rsi
	movl	12(%r12), %ecx
	movl	16(%rbp), %edx
	imull	%ecx, %edx
	movl	$1, %r8d
	movq	%r13, %rdi
	callq	*64(%rax)
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	16(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, 16(%rbp)
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	jae	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	12(%r12), %esi
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movl	12(%r12), %ecx
	movl	32(%r12), %eax
	xorl	%edx, %edx
	divl	%ecx
	movl	%edx, %esi
	testl	%esi, %esi
	cmovel	%ecx, %esi
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movl	28(%r12), %r14d
	movslq	8(%r12), %rcx
	xorl	%edx, %edx
	movl	%r14d, %eax
	divl	%ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	%ecx, %eax
	subl	%edx, %eax
	testl	%edx, %edx
	cmovlel	%edx, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	testl	%esi, %esi
	movq	%r12, 72(%rsp)          # 8-byte Spill
	jle	.LBB3_18
# BB#6:                                 # %.lr.ph127
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB3_7
# BB#9:                                 # %.lr.ph127.split.us.split.us.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	movslq	%eax, %rcx
	shlq	$7, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	-1(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	andl	$7, %ebx
	movq	%r14, %rax
	shlq	$6, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_10:                               # %.lr.ph127.split.us.split.us
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_13 Depth 3
                                        #       Child Loop BB3_16 Depth 3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r15,8), %rbp
	movq	480(%r13), %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rdx
	leal	(,%r15,8), %r8d
	movl	%r14d, (%rsp)
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rcx
	callq	*8(%rax)
	movq	%r14, %r13
	shlq	$7, %r13
	leaq	(%rbp,%r13), %r12
	movq	%r12, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	jzero_far
	testq	%rbx, %rbx
	movzwl	-128(%rbp,%r13), %eax
	je	.LBB3_11
# BB#12:                                # %.prol.preheader
                                        #   in Loop: Header=BB3_10 Depth=2
	xorl	%ecx, %ecx
	movq	64(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_13:                               #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movw	%ax, (%r12)
	incq	%rcx
	subq	$-128, %r12
	cmpq	%rcx, %rbx
	jne	.LBB3_13
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_10 Depth=2
	xorl	%ecx, %ecx
	movq	64(%rsp), %r13          # 8-byte Reload
.LBB3_14:                               # %.prol.loopexit
                                        #   in Loop: Header=BB3_10 Depth=2
	cmpq	$7, 48(%rsp)            # 8-byte Folded Reload
	jb	.LBB3_17
# BB#15:                                # %.lr.ph127.split.us.split.us.new
                                        #   in Loop: Header=BB3_10 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	subq	%rcx, %rdx
	movq	112(%rsp), %rsi         # 8-byte Reload
	leaq	(%rbp,%rsi,2), %rsi
	shlq	$7, %rcx
	leaq	896(%rcx,%rsi), %rcx
	.p2align	4, 0x90
.LBB3_16:                               #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movw	%ax, -896(%rcx)
	movw	%ax, -768(%rcx)
	movw	%ax, -640(%rcx)
	movw	%ax, -512(%rcx)
	movw	%ax, -384(%rcx)
	movw	%ax, -256(%rcx)
	movw	%ax, -128(%rcx)
	movw	%ax, (%rcx)
	addq	$1024, %rcx             # imm = 0x400
	addq	$-8, %rdx
	jne	.LBB3_16
.LBB3_17:                               # %..loopexit_crit_edge.us.us
                                        #   in Loop: Header=BB3_10 Depth=2
	incq	%r15
	cmpq	56(%rsp), %r15          # 8-byte Folded Reload
	movq	72(%rsp), %r12          # 8-byte Reload
	jne	.LBB3_10
	jmp	.LBB3_18
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph127.split.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	12(%rsp), %ebx          # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_8:                                # %.lr.ph127.split
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp), %rcx
	movq	480(%r13), %rax
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	(%rdx,%rsi,8), %rdx
	movl	%r14d, (%rsp)
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movq	%r12, %rsi
	movl	%ebp, %r8d
	callq	*8(%rax)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB3_8
.LBB3_18:                               # %._crit_edge
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%r14, %rdx
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	16(%rsp), %ecx          # 4-byte Reload
	cmpl	%ecx, 16(%rax)
	movq	%r12, %r14
	movq	104(%rsp), %r12         # 8-byte Reload
	movl	12(%rsp), %ebx          # 4-byte Reload
	movl	20(%rsp), %edi          # 4-byte Reload
	jne	.LBB3_35
# BB#19:                                #   in Loop: Header=BB3_2 Depth=1
	addl	%edx, %edi
	xorl	%edx, %edx
	movl	%edi, %eax
	divl	%r12d
	cmpl	12(%r14), %ebx
	jge	.LBB3_35
# BB#20:                                # %.lr.ph141
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	%edi, %esi
	shlq	$7, %rsi
	movslq	%ebx, %rbp
	cmpl	%edi, %r12d
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	jbe	.LBB3_23
# BB#21:                                #   in Loop: Header=BB3_2 Depth=1
	movq	72(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_22:                               # %.lr.ph141.split
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	callq	jzero_far
	movq	24(%rsp), %rsi          # 8-byte Reload
	incq	%rbp
	movslq	12(%r14), %rax
	cmpq	%rax, %rbp
	jl	.LBB3_22
	jmp	.LBB3_35
.LBB3_23:                               # %.lr.ph141.split.us.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	-1(%r12), %r8
	movl	%r12d, %r9d
	leaq	-1(%r9), %r15
	movl	%r12d, %r13d
	andl	$7, %r13d
	shlq	$7, %r12
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%r9, 40(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_24:                               # %.lr.ph141.split.us
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_26 Depth 3
                                        #         Child Loop BB3_29 Depth 4
                                        #         Child Loop BB3_32 Depth 4
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rbp,8), %r14
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	-8(%rcx,%rbp,8), %rbp
	movq	%r14, %rdi
	movl	%eax, %ebx
	callq	jzero_far
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movl	%ebx, %eax
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	jle	.LBB3_34
# BB#25:                                # %.lr.ph130.us.us.preheader
                                        #   in Loop: Header=BB3_24 Depth=2
	leaq	512(%r14), %rbx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_26:                               # %.lr.ph130.us.us
                                        #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_24 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_29 Depth 4
                                        #         Child Loop BB3_32 Depth 4
	movq	%r8, %rdx
	shlq	$7, %rdx
	testq	%r13, %r13
	movzwl	(%rbp,%rdx), %edx
	je	.LBB3_27
# BB#28:                                # %.prol.preheader186
                                        #   in Loop: Header=BB3_26 Depth=3
	movq	%r14, %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_29:                               #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_24 Depth=2
                                        #       Parent Loop BB3_26 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movw	%dx, (%rdi)
	incq	%rsi
	subq	$-128, %rdi
	cmpq	%rsi, %r13
	jne	.LBB3_29
	jmp	.LBB3_30
	.p2align	4, 0x90
.LBB3_27:                               #   in Loop: Header=BB3_26 Depth=3
	xorl	%esi, %esi
.LBB3_30:                               # %.prol.loopexit187
                                        #   in Loop: Header=BB3_26 Depth=3
	cmpq	$7, %r15
	jb	.LBB3_33
# BB#31:                                # %.lr.ph130.us.us.new
                                        #   in Loop: Header=BB3_26 Depth=3
	movq	%r9, %rdi
	subq	%rsi, %rdi
	shlq	$7, %rsi
	addq	%rbx, %rsi
	.p2align	4, 0x90
.LBB3_32:                               #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_24 Depth=2
                                        #       Parent Loop BB3_26 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movw	%dx, -512(%rsi)
	movw	%dx, -384(%rsi)
	movw	%dx, -256(%rsi)
	movw	%dx, -128(%rsi)
	movw	%dx, (%rsi)
	movw	%dx, 128(%rsi)
	movw	%dx, 256(%rsi)
	movw	%dx, 384(%rsi)
	addq	$1024, %rsi             # imm = 0x400
	addq	$-8, %rdi
	jne	.LBB3_32
.LBB3_33:                               # %._crit_edge131.us.us
                                        #   in Loop: Header=BB3_26 Depth=3
	addq	%r12, %rbp
	incl	%ecx
	addq	%r12, %r14
	addq	%r12, %rbx
	cmpl	%eax, %ecx
	jb	.LBB3_26
.LBB3_34:                               # %._crit_edge138.us
                                        #   in Loop: Header=BB3_24 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
	incq	%rbp
	movq	72(%rsp), %r14          # 8-byte Reload
	movslq	12(%r14), %rcx
	cmpq	%rcx, %rbp
	movq	24(%rsp), %rsi          # 8-byte Reload
	jl	.LBB3_24
	.p2align	4, 0x90
.LBB3_35:                               # %.loopexit124
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	88(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	addq	$96, %r14
	movq	64(%rsp), %r13          # 8-byte Reload
	movslq	68(%r13), %rax
	cmpq	%rax, %rbx
	movq	%r14, %r12
	jl	.LBB3_2
.LBB3_36:                               # %._crit_edge158
	movq	%r13, %rdi
	movq	80(%rsp), %rsi          # 8-byte Reload
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	compress_output         # TAILCALL
.Lfunc_end3:
	.size	compress_first_pass, .Lfunc_end3-compress_first_pass
	.cfi_endproc

	.p2align	4, 0x90
	.type	compress_output,@function
compress_output:                        # @compress_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi47:
	.cfi_def_cfa_offset 176
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	448(%rbx), %r12
	movl	316(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph101
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	320(%rbx,%rbp,8), %rax
	movq	8(%rbx), %r9
	movslq	4(%rax), %rcx
	movq	112(%r12,%rcx,8), %rsi
	movl	12(%rax), %ecx
	movl	16(%r12), %edx
	imull	%ecx, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	callq	*64(%r9)
	movq	%rax, 80(%rsp,%rbp,8)
	incq	%rbp
	movslq	316(%rbx), %rcx
	cmpq	%rcx, %rbp
	jl	.LBB4_2
.LBB4_3:                                # %._crit_edge102
	movslq	24(%r12), %rsi
	movl	28(%r12), %eax
	cmpl	%eax, %esi
	jge	.LBB4_26
# BB#4:                                 # %.lr.ph97
	leaq	32(%r12), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	20(%r12), %edi
	movl	352(%rbx), %ecx
	leaq	48(%r12), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
.LBB4_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_7 Depth 2
                                        #       Child Loop BB4_9 Depth 3
                                        #         Child Loop BB4_12 Depth 4
                                        #           Child Loop BB4_15 Depth 5
                                        #           Child Loop BB4_17 Depth 5
	cmpl	%ecx, %edi
	jae	.LBB4_24
# BB#6:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_7:                                # %.preheader
                                        #   Parent Loop BB4_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_9 Depth 3
                                        #         Child Loop BB4_12 Depth 4
                                        #           Child Loop BB4_15 Depth 5
                                        #           Child Loop BB4_17 Depth 5
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movslq	316(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB4_20
# BB#8:                                 # %.lr.ph90.preheader
                                        #   in Loop: Header=BB4_7 Depth=2
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph90
                                        #   Parent Loop BB4_5 Depth=1
                                        #     Parent Loop BB4_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_12 Depth 4
                                        #           Child Loop BB4_15 Depth 5
                                        #           Child Loop BB4_17 Depth 5
	movq	320(%rbx,%rcx,8), %rax
	movslq	56(%rax), %r10
	testq	%r10, %r10
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	jle	.LBB4_19
# BB#10:                                # %.lr.ph85
                                        #   in Loop: Header=BB4_9 Depth=3
	movl	52(%rax), %r15d
	testl	%r15d, %r15d
	jle	.LBB4_19
# BB#11:                                # %.lr.ph85.split.us.preheader
                                        #   in Loop: Header=BB4_9 Depth=3
	movl	%r15d, %ebx
	imull	12(%rsp), %ebx          # 4-byte Folded Reload
	movq	80(%rsp,%rcx,8), %r13
	leal	-1(%r15), %r11d
	incq	%r11
	movq	%r11, %r9
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %r9
	movq	%rbx, %rax
	shlq	$6, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_12:                               # %.lr.ph85.split.us
                                        #   Parent Loop BB4_5 Depth=1
                                        #     Parent Loop BB4_7 Depth=2
                                        #       Parent Loop BB4_9 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_15 Depth 5
                                        #           Child Loop BB4_17 Depth 5
	leaq	(%r14,%rsi), %rax
	movq	(%r13,%rax,8), %rax
	movq	%rbx, %rbp
	shlq	$7, %rbp
	addq	%rax, %rbp
	xorl	%r8d, %r8d
	cmpq	$4, %r11
	movslq	%edx, %rdx
	jb	.LBB4_17
# BB#13:                                # %min.iters.checked
                                        #   in Loop: Header=BB4_12 Depth=4
	testq	%r9, %r9
	je	.LBB4_17
# BB#14:                                # %vector.body.preheader
                                        #   in Loop: Header=BB4_12 Depth=4
	movq	%r9, %rcx
	shlq	$7, %rcx
	addq	%rcx, %rbp
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	384(%rax,%rcx,2), %rsi
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rcx
	addq	%r9, %rdx
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB4_15:                               # %vector.body
                                        #   Parent Loop BB4_5 Depth=1
                                        #     Parent Loop BB4_7 Depth=2
                                        #       Parent Loop BB4_9 Depth=3
                                        #         Parent Loop BB4_12 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leaq	-384(%rsi), %rbx
	leaq	-256(%rsi), %rdi
	leaq	-128(%rsi), %r8
	movd	%rdi, %xmm0
	movd	%rbx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%rsi, %xmm0
	movd	%r8, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, -16(%rcx)
	movdqu	%xmm2, (%rcx)
	addq	$512, %rsi              # imm = 0x200
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB4_15
# BB#16:                                # %middle.block
                                        #   in Loop: Header=BB4_12 Depth=4
	cmpq	%r9, %r11
	movl	%r9d, %r8d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	je	.LBB4_18
	.p2align	4, 0x90
.LBB4_17:                               # %scalar.ph
                                        #   Parent Loop BB4_5 Depth=1
                                        #     Parent Loop BB4_7 Depth=2
                                        #       Parent Loop BB4_9 Depth=3
                                        #         Parent Loop BB4_12 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rbp, 32(%r12,%rdx,8)
	incq	%rdx
	subq	$-128, %rbp
	incl	%r8d
	cmpl	%r15d, %r8d
	jl	.LBB4_17
.LBB4_18:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_12 Depth=4
	incq	%r14
	cmpq	%r10, %r14
	jl	.LBB4_12
.LBB4_19:                               # %._crit_edge86
                                        #   in Loop: Header=BB4_9 Depth=3
	movq	48(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	40(%rsp), %rcx          # 8-byte Folded Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	jl	.LBB4_9
.LBB4_20:                               # %._crit_edge91
                                        #   in Loop: Header=BB4_7 Depth=2
	movq	488(%rbx), %rax
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	*8(%rax)
	testl	%eax, %eax
	je	.LBB4_21
# BB#22:                                #   in Loop: Header=BB4_7 Depth=2
	movl	12(%rsp), %edi          # 4-byte Reload
	incl	%edi
	movl	352(%rbx), %ecx
	cmpl	%ecx, %edi
	movq	16(%rsp), %rsi          # 8-byte Reload
	jb	.LBB4_7
# BB#23:                                # %._crit_edge93.loopexit
                                        #   in Loop: Header=BB4_5 Depth=1
	movl	28(%r12), %eax
.LBB4_24:                               # %._crit_edge93
                                        #   in Loop: Header=BB4_5 Depth=1
	movl	$0, 20(%r12)
	incq	%rsi
	movslq	%eax, %rdx
	xorl	%edi, %edi
	cmpq	%rdx, %rsi
	jl	.LBB4_5
# BB#25:                                # %._crit_edge98.loopexit
	movl	316(%rbx), %ecx
.LBB4_26:                               # %._crit_edge98
	incl	16(%r12)
	movq	448(%rbx), %rdx
	movl	$1, %eax
	cmpl	$1, %ecx
	movl	$1, %ecx
	jg	.LBB4_30
# BB#27:
	movl	312(%rbx), %esi
	decl	%esi
	movq	320(%rbx), %rcx
	cmpl	%esi, 16(%rdx)
	jae	.LBB4_29
# BB#28:
	movl	12(%rcx), %ecx
	jmp	.LBB4_30
.LBB4_21:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 24(%r12)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 20(%r12)
	xorl	%eax, %eax
	jmp	.LBB4_31
.LBB4_29:
	movl	72(%rcx), %ecx
.LBB4_30:                               # %start_iMCU_row.exit
	movl	%ecx, 28(%rdx)
	movq	$0, 20(%rdx)
.LBB4_31:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	compress_output, .Lfunc_end4-compress_output
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
