	.text
	.file	"cvrout.bc"
	.globl	fprint_pla
	.p2align	4, 0x90
	.type	fprint_pla,@function
fprint_pla:                             # @fprint_pla
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r15
	movq	%rdi, %r14
	testb	$1, %bh
	je	.LBB0_2
# BB#1:
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	output_symbolic_constraints
	andl	$-257, %ebx             # imm = 0xFEFF
	je	.LBB0_15
.LBB0_2:
	testb	$2, %bh
	je	.LBB0_4
# BB#3:
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	output_symbolic_constraints
	andl	$-513, %ebx             # imm = 0xFDFF
	je	.LBB0_15
.LBB0_4:
	cmpl	$128, %ebx
	je	.LBB0_9
# BB#5:
	cmpl	$16, %ebx
	je	.LBB0_8
# BB#6:
	cmpl	$8, %ebx
	jne	.LBB0_16
# BB#7:
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	pls_output              # TAILCALL
.LBB0_9:
	movq	(%r15), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB0_12
# BB#10:                                # %.lr.ph28.i.preheader
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph28.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.3, %ecx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	kiss_print_cube
	movq	(%r15), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB0_11
.LBB0_12:                               # %._crit_edge29.i
	movq	8(%r15), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB0_15
# BB#13:                                # %.lr.ph.i.preheader
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.4, %ecx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	kiss_print_cube
	movq	8(%r15), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB0_14
.LBB0_15:                               # %kiss_output.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_8:
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	eqn_output              # TAILCALL
.LBB0_16:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	fpr_header
	xorl	%edx, %edx
	movl	%ebx, %ecx
	andl	$1, %ebx
	je	.LBB0_18
# BB#17:
	movq	(%r15), %rax
	movl	12(%rax), %edx
.LBB0_18:
	movl	%ecx, %ebp
	movl	%ecx, %r13d
	andl	$2, %r13d
	je	.LBB0_20
# BB#19:
	movq	8(%r15), %rax
	addl	12(%rax), %edx
.LBB0_20:
	movl	%ebp, %eax
	movl	%eax, %r12d
	andl	$4, %r12d
	je	.LBB0_22
# BB#21:
	movq	16(%r15), %rax
	addl	12(%rax), %edx
.LBB0_22:
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	cmpl	$1, %ebp
	jne	.LBB0_28
# BB#23:
	movq	(%r15), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB0_26
# BB#24:                                # %.lr.ph.preheader
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.1, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	print_cube
	movq	(%r15), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB0_25
.LBB0_26:                               # %._crit_edge
	movl	$.L.str.2, %edi
	movl	$3, %esi
	jmp	.LBB0_27
.LBB0_28:
	testl	%ebx, %ebx
	je	.LBB0_32
# BB#29:
	movq	(%r15), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB0_32
# BB#30:                                # %.lr.ph94.preheader
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB0_31:                               # %.lr.ph94
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.3, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	print_cube
	movq	(%r15), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB0_31
.LBB0_32:                               # %.loopexit84
	testl	%r13d, %r13d
	je	.LBB0_36
# BB#33:
	movq	8(%r15), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB0_36
# BB#34:                                # %.lr.ph91.preheader
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB0_35:                               # %.lr.ph91
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.4, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	print_cube
	movq	8(%r15), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB0_35
.LBB0_36:                               # %.loopexit83
	testl	%r12d, %r12d
	je	.LBB0_40
# BB#37:
	movq	16(%r15), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB0_40
# BB#38:                                # %.lr.ph88.preheader
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB0_39:                               # %.lr.ph88
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.5, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	print_cube
	movq	16(%r15), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB0_39
.LBB0_40:                               # %.loopexit
	movl	$.L.str.6, %edi
	movl	$5, %esi
.LBB0_27:                               # %._crit_edge
	movl	$1, %edx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end0:
	.size	fprint_pla, .Lfunc_end0-fprint_pla
	.cfi_endproc

	.globl	fpr_header
	.p2align	4, 0x90
	.type	fpr_header,@function
fpr_header:                             # @fpr_header
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpl	$1, %ebx
	je	.LBB1_8
# BB#1:
	movl	$.L.str.7, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	testb	$1, %bl
	je	.LBB1_3
# BB#2:
	movl	$102, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB1_3:
	testb	$2, %bl
	je	.LBB1_5
# BB#4:
	movl	$100, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB1_5:
	testb	$4, %bl
	je	.LBB1_7
# BB#6:
	movl	$114, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB1_7:
	movl	$10, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB1_8:
	cmpl	$1, cube+120(%rip)
	jg	.LBB1_11
# BB#9:
	movl	cube+8(%rip), %edx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movslq	cube+124(%rip), %rax
	cmpq	$-1, %rax
	je	.LBB1_14
# BB#10:
	movq	cube+32(%rip), %rcx
	movl	(%rcx,%rax,4), %edx
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	jmp	.LBB1_14
.LBB1_11:
	movl	cube+4(%rip), %edx
	movl	cube+8(%rip), %ecx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movslq	cube+8(%rip), %rbx
	cmpl	cube+4(%rip), %ebx
	jge	.LBB1_13
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph91
                                        # =>This Inner Loop Header: Depth=1
	movq	cube+32(%rip), %rax
	movl	(%rax,%rbx,4), %edx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	incq	%rbx
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_12
.LBB1_13:                               # %._crit_edge92
	movl	$10, %edi
	movq	%r15, %rsi
	callq	fputc
.LBB1_14:
	movq	56(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_27
# BB#15:
	cmpq	$0, 8(%rax)
	je	.LBB1_21
# BB#16:
	movl	cube+8(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB1_21
# BB#17:
	movl	$.L.str.13, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	cmpl	$0, cube+8(%rip)
	jle	.LBB1_20
# BB#18:                                # %.lr.ph87.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph87
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	cube+16(%rip), %rcx
	movslq	(%rcx,%rbx,4), %rcx
	movq	8(%rax,%rcx,8), %rdx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	incq	%rbx
	movslq	cube+8(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_19
.LBB1_20:                               # %.loopexit
	movl	$10, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	56(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_27
.LBB1_21:                               # %.thread100
	movslq	cube+124(%rip), %rcx
	cmpq	$-1, %rcx
	je	.LBB1_27
# BB#22:                                # %.thread100
	movq	cube+16(%rip), %rdx
	movslq	(%rdx,%rcx,4), %rcx
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB1_27
# BB#23:
	movl	$.L.str.15, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	cube+32(%rip), %rcx
	movslq	cube+124(%rip), %rax
	cmpl	$0, (%rcx,%rax,4)
	jle	.LBB1_26
# BB#24:                                # %.lr.ph83.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_25:                               # %.lr.ph83
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rcx
	movq	cube+16(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	addl	%ebx, %eax
	cltq
	movq	(%rcx,%rax,8), %rdx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	incl	%ebx
	movq	cube+32(%rip), %rcx
	movslq	cube+124(%rip), %rax
	cmpl	(%rcx,%rax,4), %ebx
	jl	.LBB1_25
.LBB1_26:                               # %._crit_edge84
	movl	$10, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB1_27:                               # %.thread
	movslq	cube+8(%rip), %r12
	movl	cube+4(%rip), %eax
	leal	-1(%rax), %ecx
	cmpl	%ecx, %r12d
	jge	.LBB1_35
	.p2align	4, 0x90
.LBB1_28:                               # %.lr.ph79
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_32 Depth 2
	movq	56(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB1_34
# BB#29:                                #   in Loop: Header=BB1_28 Depth=1
	movq	cube+16(%rip), %rdx
	movslq	(%rdx,%r12,4), %rbx
	cmpq	$0, (%rcx,%rbx,8)
	je	.LBB1_34
# BB#30:                                #   in Loop: Header=BB1_28 Depth=1
	movq	cube+24(%rip), %rax
	movslq	(%rax,%r12,4), %rbp
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%r12d, %edx
	callq	fprintf
	cmpl	%ebp, %ebx
	jg	.LBB1_33
# BB#31:                                # %.lr.ph76.preheader
                                        #   in Loop: Header=BB1_28 Depth=1
	decq	%rbx
	.p2align	4, 0x90
.LBB1_32:                               # %.lr.ph76
                                        #   Parent Loop BB1_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rax
	movq	8(%rax,%rbx,8), %rdx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB1_32
.LBB1_33:                               # %._crit_edge77
                                        #   in Loop: Header=BB1_28 Depth=1
	movl	$10, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movl	cube+4(%rip), %eax
.LBB1_34:                               #   in Loop: Header=BB1_28 Depth=1
	incq	%r12
	leal	-1(%rax), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %r12
	jl	.LBB1_28
.LBB1_35:                               # %._crit_edge80
	cmpq	$0, 40(%r14)
	je	.LBB1_39
# BB#36:
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %ebx
	movq	cube+24(%rip), %rax
	movl	(%rax,%rcx,4), %ebp
	movl	$.L.str.17, %edi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	cmpl	%ebp, %ebx
	jg	.LBB1_38
	.p2align	4, 0x90
.LBB1_37:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%r14), %rax
	movl	%ebx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rax,%rcx,4), %eax
	btl	%ebx, %eax
	sbbl	%edi, %edi
	andl	$1, %edi
	orl	$48, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	cmpl	%ebp, %ebx
	leal	1(%rbx), %eax
	movl	%eax, %ebx
	jl	.LBB1_37
.LBB1_38:                               # %._crit_edge
	movl	$10, %edi
	movq	%r15, %rsi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fputc                   # TAILCALL
.LBB1_39:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	fpr_header, .Lfunc_end1-fpr_header
	.cfi_endproc

	.globl	pls_output
	.p2align	4, 0x90
	.type	pls_output,@function
pls_output:                             # @pls_output
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$.Lstr, %edi
	callq	puts
	movq	%r14, %rdi
	callq	makeup_labels
	movq	stdout(%rip), %rsi
	movq	%r14, %rdi
	callq	pls_label
	movq	stdout(%rip), %rsi
	movq	%r14, %rdi
	callq	pls_group
	movq	(%r14), %rax
	movl	12(%rax), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%r14), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %r15
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rdi
	movq	40(%r14), %rdx
	movq	%rbx, %rsi
	callq	print_expanded_cube
	movq	(%r14), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r15, %rbx
	jb	.LBB2_2
.LBB2_3:                                # %._crit_edge
	movl	$.Lstr.1, %edi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	puts                    # TAILCALL
.Lfunc_end2:
	.size	pls_output, .Lfunc_end2-pls_output
	.cfi_endproc

	.globl	pls_group
	.p2align	4, 0x90
	.type	pls_group,@function
pls_group:                              # @pls_group
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 80
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r15
	movl	$.L.str.19, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	cmpl	$2, cube+4(%rip)
	jl	.LBB3_11
# BB#1:                                 # %.lr.ph40
	movl	$6, %ebx
	xorl	%r14d, %r14d
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	movl	$.L.str.20, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	addl	$2, %ebx
	movq	cube+16(%rip), %rax
	movl	(%rax,%r14,4), %ebp
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%r14,4), %ebp
	jg	.LBB3_10
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	movslq	%ebp, %r12
	decq	%r12
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r15), %rax
	movq	8(%rax,%r12,8), %rdi
	callq	strlen
	movq	%rax, %r13
	leal	(%r13,%rbx), %eax
	cmpl	$76, %eax
	jl	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=2
	movl	$.L.str.21, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	%r15, %rcx
	callq	fwrite
	xorl	%ebx, %ebx
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=2
	testl	%ebp, %ebp
	je	.LBB3_7
# BB#8:                                 #   in Loop: Header=BB3_4 Depth=2
	movl	$32, %edi
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	%r15, %rsi
	callq	_IO_putc
	incl	%ebx
	jmp	.LBB3_9
.LBB3_7:                                #   in Loop: Header=BB3_4 Depth=2
	movq	8(%rsp), %r15           # 8-byte Reload
	.p2align	4, 0x90
.LBB3_9:                                #   in Loop: Header=BB3_4 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	56(%rcx), %rax
	movq	8(%rax,%r12,8), %rdi
	movq	%r15, %rsi
	movq	%rcx, %r15
	callq	fputs
	addl	%r13d, %ebx
	movq	cube+24(%rip), %rax
	movslq	(%rax,%r14,4), %rax
	incq	%r12
	incl	%ebp
	cmpq	%rax, %r12
	jl	.LBB3_4
.LBB3_10:                               # %._crit_edge
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$41, %edi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rsi
	callq	fputc
	incl	%ebx
	incq	%r14
	movslq	cube+4(%rip), %rax
	decq	%rax
	cmpq	%rax, %r14
	jl	.LBB3_2
.LBB3_11:                               # %._crit_edge41
	movl	$10, %edi
	movq	%rbp, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fputc                   # TAILCALL
.Lfunc_end3:
	.size	pls_group, .Lfunc_end3-pls_group
	.cfi_endproc

	.globl	pls_label
	.p2align	4, 0x90
	.type	pls_label,@function
pls_label:                              # @pls_label
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 64
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$.L.str.24, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	cube+4(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB4_10
# BB#1:                                 # %.lr.ph31
	movl	$6, %ebx
	movq	cube+24(%rip), %rax
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	movq	cube+16(%rip), %rdx
	movslq	(%rdx,%r12,4), %rbp
	cmpl	(%rax,%r12,4), %ebp
	jg	.LBB4_9
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	decq	%rbp
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r15), %rax
	movq	8(%rax,%rbp,8), %rdi
	callq	strlen
	movq	%rax, %r13
	leal	(%r13,%rbx), %eax
	cmpl	$76, %eax
	jl	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=2
	movl	$.L.str.21, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	xorl	%ebx, %ebx
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_6:                                #   in Loop: Header=BB4_4 Depth=2
	movl	$32, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	incl	%ebx
.LBB4_7:                                #   in Loop: Header=BB4_4 Depth=2
	movq	56(%r15), %rax
	movq	8(%rax,%rbp,8), %rdi
	movq	%r14, %rsi
	callq	fputs
	addl	%r13d, %ebx
	movq	cube+24(%rip), %rax
	movslq	(%rax,%r12,4), %rcx
	incq	%rbp
	cmpq	%rcx, %rbp
	jl	.LBB4_4
# BB#8:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	cube+4(%rip), %ecx
.LBB4_9:                                # %._crit_edge
                                        #   in Loop: Header=BB4_2 Depth=1
	incq	%r12
	movslq	%ecx, %rdx
	cmpq	%rdx, %r12
	jl	.LBB4_2
.LBB4_10:                               # %._crit_edge32
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	pls_label, .Lfunc_end4-pls_label
	.cfi_endproc

	.globl	eqn_output
	.p2align	4, 0x90
	.type	eqn_output,@function
eqn_output:                             # @eqn_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 96
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	cmpl	$-1, cube+124(%rip)
	jne	.LBB5_2
# BB#1:
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB5_2:
	cmpl	$1, cube+120(%rip)
	je	.LBB5_4
# BB#3:
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB5_4:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	makeup_labels
	movq	cube+32(%rip), %rcx
	movslq	cube+124(%rip), %rax
	cmpl	$0, (%rcx,%rax,4)
	jle	.LBB5_27
# BB#5:                                 # %.lr.ph77
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_8 Depth 2
                                        #       Child Loop BB5_14 Depth 3
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	56(%rbx), %rcx
	movq	cube+16(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	movslq	%r14d, %rdx
	addq	%rax, %rdx
	movq	(%rcx,%rdx,8), %rsi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rbp
	movslq	(%rbp), %rcx
	movslq	12(%rbp), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB5_26
# BB#7:                                 # %.lr.ph72.preheader
                                        #   in Loop: Header=BB5_6 Depth=1
	movq	cube+16(%rip), %rcx
	movslq	cube+124(%rip), %rdx
	movl	(%rcx,%rdx,4), %ebx
	movq	24(%rbp), %r15
	leaq	(%r15,%rax,4), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	leal	(%rbx,%r14), %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rdi
	callq	strlen
	movq	%r15, %rsi
	movq	%rax, %r15
	addl	$3, %r15d
	movl	$1, %eax
	movq	%r14, 24(%rsp)          # 8-byte Spill
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_23:                               # %._crit_edge
                                        #   in Loop: Header=BB5_8 Depth=2
	movl	$41, %edi
	callq	putchar
	movq	16(%rsp), %rsi          # 8-byte Reload
	incl	%r14d
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rbp
	xorl	%eax, %eax
	movl	%r14d, %r15d
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB5_24:                               #   in Loop: Header=BB5_8 Depth=2
	movslq	(%rbp), %rcx
	leaq	(%rsi,%rcx,4), %rsi
	cmpq	32(%rsp), %rsi          # 8-byte Folded Reload
	jae	.LBB5_26
# BB#25:                                # %..lr.ph72_crit_edge
                                        #   in Loop: Header=BB5_8 Depth=2
	movq	cube+16(%rip), %rcx
	movslq	cube+124(%rip), %rdx
	movl	(%rcx,%rdx,4), %ebx
.LBB5_8:                                # %.lr.ph72
                                        #   Parent Loop BB5_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_14 Depth 3
	addl	%r14d, %ebx
	movl	%ebx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rsi,%rcx,4), %ecx
	btl	%ebx, %ecx
	jae	.LBB5_24
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=2
	testl	%eax, %eax
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	je	.LBB5_11
# BB#10:                                #   in Loop: Header=BB5_8 Depth=2
	movl	$40, %edi
	callq	putchar
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	$1, %r14d
	jmp	.LBB5_12
	.p2align	4, 0x90
.LBB5_11:                               #   in Loop: Header=BB5_8 Depth=2
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	$4, %r14d
.LBB5_12:                               #   in Loop: Header=BB5_8 Depth=2
	addl	%r15d, %r14d
	movl	cube+8(%rip), %eax
	testl	%eax, %eax
	jle	.LBB5_23
# BB#13:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_8 Depth=2
	movl	$1, %r15d
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_14:                               # %.lr.ph
                                        #   Parent Loop BB5_6 Depth=1
                                        #     Parent Loop BB5_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r12d, %ecx
	sarl	$4, %ecx
	movslq	%ecx, %rcx
	movl	4(%rdx,%rcx,4), %r13d
	movl	%ebp, %ecx
	andb	$30, %cl
	shrl	%cl, %r13d
	andl	$3, %r13d
	cmpl	$3, %r13d
	je	.LBB5_22
# BB#15:                                #   in Loop: Header=BB5_14 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	movq	cube+16(%rip), %rcx
	movslq	(%rcx,%r12,4), %rcx
	movq	8(%rax,%rcx,8), %rdi
	callq	strlen
	movq	%rax, %rbx
	leal	(%rbx,%r14), %eax
	cmpl	$73, %eax
	jl	.LBB5_17
# BB#16:                                #   in Loop: Header=BB5_14 Depth=3
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$4, %r14d
.LBB5_17:                               #   in Loop: Header=BB5_14 Depth=3
	testl	%r15d, %r15d
	jne	.LBB5_19
# BB#18:                                #   in Loop: Header=BB5_14 Depth=3
	movl	$38, %edi
	callq	putchar
	incl	%r14d
.LBB5_19:                               #   in Loop: Header=BB5_14 Depth=3
	cmpl	$1, %r13d
	jne	.LBB5_21
# BB#20:                                #   in Loop: Header=BB5_14 Depth=3
	movl	$33, %edi
	callq	putchar
	incl	%r14d
.LBB5_21:                               #   in Loop: Header=BB5_14 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	movq	cube+16(%rip), %rcx
	movslq	(%rcx,%r12,4), %rcx
	movq	8(%rax,%rcx,8), %rsi
	xorl	%r15d, %r15d
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	printf
	addl	%ebx, %r14d
	movl	cube+8(%rip), %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB5_22:                               #   in Loop: Header=BB5_14 Depth=3
	incq	%r12
	movslq	%eax, %rcx
	addl	$2, %ebp
	cmpq	%rcx, %r12
	jl	.LBB5_14
	jmp	.LBB5_23
	.p2align	4, 0x90
.LBB5_26:                               # %._crit_edge73
                                        #   in Loop: Header=BB5_6 Depth=1
	movl	$.Lstr.2, %edi
	callq	puts
	incl	%r14d
	movq	cube+32(%rip), %rcx
	movslq	cube+124(%rip), %rax
	cmpl	(%rcx,%rax,4), %r14d
	jl	.LBB5_6
.LBB5_27:                               # %._crit_edge78
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	eqn_output, .Lfunc_end5-eqn_output
	.cfi_endproc

	.globl	fmt_cube
	.p2align	4, 0x90
	.type	fmt_cube,@function
fmt_cube:                               # @fmt_cube
	.cfi_startproc
# BB#0:
	movl	cube+8(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB6_1
# BB#13:                                # %.lr.ph60.preheader
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB6_14:                               # %.lr.ph60
                                        # =>This Inner Loop Header: Depth=1
	movl	%r9d, %ecx
	sarl	$4, %ecx
	movslq	%ecx, %rcx
	movl	4(%rdi,%rcx,4), %eax
	movl	%r8d, %ecx
	andb	$30, %cl
	shrl	%cl, %eax
	andl	$3, %eax
	movzbl	.L.str.34(%rax), %eax
	movb	%al, (%rdx,%r9)
	incq	%r9
	movl	cube+8(%rip), %ecx
	addl	$2, %r8d
	cmpl	%ecx, %r9d
	jl	.LBB6_14
	jmp	.LBB6_2
.LBB6_1:
	xorl	%r9d, %r9d
.LBB6_2:                                # %.preheader
	movl	cube+4(%rip), %eax
	decl	%eax
	cmpl	%eax, %ecx
	jge	.LBB6_8
# BB#3:                                 # %.lr.ph54.preheader
	movslq	%ecx, %r8
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph54
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_6 Depth 2
	movslq	%r9d, %r9
	movb	$32, (%rdx,%r9)
	movq	cube+16(%rip), %rax
	movl	(%rax,%r8,4), %r10d
	incl	%r9d
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%r8,4), %r10d
	jg	.LBB6_7
# BB#5:                                 # %.lr.ph50.preheader
                                        #   in Loop: Header=BB6_4 Depth=1
	movslq	%r9d, %rax
	addq	%rdx, %rax
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph50
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r10d, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rdi,%rcx,4), %ecx
	btl	%r10d, %ecx
	sbbq	%rcx, %rcx
	andl	$1, %ecx
	movzbl	.L.str.1(%rcx), %ecx
	movb	%cl, (%rax)
	movq	cube+24(%rip), %rcx
	incq	%rax
	incl	%r9d
	cmpl	(%rcx,%r8,4), %r10d
	leal	1(%r10), %ecx
	movl	%ecx, %r10d
	jl	.LBB6_6
.LBB6_7:                                # %._crit_edge
                                        #   in Loop: Header=BB6_4 Depth=1
	incq	%r8
	movslq	cube+4(%rip), %rax
	decq	%rax
	cmpq	%rax, %r8
	jl	.LBB6_4
.LBB6_8:                                # %._crit_edge55
	movslq	cube+124(%rip), %rax
	cmpq	$-1, %rax
	je	.LBB6_12
# BB#9:
	movq	cube+24(%rip), %rcx
	movl	(%rcx,%rax,4), %r8d
	movslq	%r9d, %r9
	movb	$32, (%rdx,%r9)
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %r10d
	incl	%r9d
	cmpl	%r8d, %r10d
	jg	.LBB6_12
# BB#10:                                # %.lr.ph.preheader
	movslq	%r9d, %rax
	addq	%rdx, %rax
	.p2align	4, 0x90
.LBB6_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r10d, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rdi,%rcx,4), %ecx
	btl	%r10d, %ecx
	movq	%rsi, %rcx
	adcq	$0, %rcx
	movzbl	(%rcx), %ecx
	movb	%cl, (%rax)
	incq	%rax
	incl	%r9d
	cmpl	%r8d, %r10d
	leal	1(%r10), %ecx
	movl	%ecx, %r10d
	jl	.LBB6_11
.LBB6_12:                               # %.loopexit
	movslq	%r9d, %rax
	movb	$0, (%rdx,%rax)
	movq	%rdx, %rax
	retq
.Lfunc_end6:
	.size	fmt_cube, .Lfunc_end6-fmt_cube
	.cfi_endproc

	.globl	print_cube
	.p2align	4, 0x90
	.type	print_cube,@function
print_cube:                             # @print_cube
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 48
.Lcfi73:
	.cfi_offset %rbx, -48
.Lcfi74:
	.cfi_offset %r12, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	cube+8(%rip), %eax
	testl	%eax, %eax
	jle	.LBB7_3
# BB#1:                                 # %.lr.ph46.preheader
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph46
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	sarl	$4, %eax
	cltq
	movl	4(%r15,%rax,4), %eax
	movl	%ebx, %ecx
	andb	$30, %cl
	shrl	%cl, %eax
	andl	$3, %eax
	movsbl	.L.str.34(%rax), %edi
	movq	%r12, %rsi
	callq	_IO_putc
	incl	%ebp
	movl	cube+8(%rip), %eax
	addl	$2, %ebx
	cmpl	%eax, %ebp
	jl	.LBB7_2
.LBB7_3:                                # %.preheader
	movl	cube+4(%rip), %ecx
	decl	%ecx
	cmpl	%ecx, %eax
	jge	.LBB7_8
# BB#4:                                 # %.lr.ph42.preheader
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph42
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_6 Depth 2
	movl	$32, %edi
	movq	%r12, %rsi
	callq	_IO_putc
	movq	cube+16(%rip), %rax
	movl	(%rax,%rbx,4), %ebp
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%rbx,4), %ebp
	jg	.LBB7_7
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph40
                                        #   Parent Loop BB7_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r15,%rax,4), %eax
	btl	%ebp, %eax
	sbbq	%rax, %rax
	andl	$1, %eax
	movsbl	.L.str.1(%rax), %edi
	movq	%r12, %rsi
	callq	_IO_putc
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%rbx,4), %ebp
	leal	1(%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB7_6
.LBB7_7:                                # %._crit_edge
                                        #   in Loop: Header=BB7_5 Depth=1
	incq	%rbx
	movslq	cube+4(%rip), %rax
	decq	%rax
	cmpq	%rax, %rbx
	jl	.LBB7_5
.LBB7_8:                                # %._crit_edge43
	movslq	cube+124(%rip), %rax
	cmpq	$-1, %rax
	je	.LBB7_11
# BB#9:
	movq	cube+24(%rip), %rcx
	movl	(%rcx,%rax,4), %ebx
	movl	$32, %edi
	movq	%r12, %rsi
	callq	_IO_putc
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %ebp
	cmpl	%ebx, %ebp
	jg	.LBB7_11
	.p2align	4, 0x90
.LBB7_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r15,%rax,4), %eax
	btl	%ebp, %eax
	movq	%r14, %rax
	adcq	$0, %rax
	movsbl	(%rax), %edi
	movq	%r12, %rsi
	callq	_IO_putc
	cmpl	%ebx, %ebp
	leal	1(%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB7_10
.LBB7_11:                               # %.loopexit
	movl	$10, %edi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.Lfunc_end7:
	.size	print_cube, .Lfunc_end7-print_cube
	.cfi_endproc

	.globl	print_expanded_cube
	.p2align	4, 0x90
	.type	print_expanded_cube,@function
print_expanded_cube:                    # @print_expanded_cube
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 64
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	cube+8(%rip), %eax
	movq	%rdx, %r12
	testl	%eax, %eax
	jle	.LBB8_1
# BB#12:                                # %.lr.ph60.preheader
	movq	cube+24(%rip), %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_13:                               # %.lr.ph60
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_14 Depth 2
	movq	cube+16(%rip), %rdx
	movl	(%rdx,%rbx,4), %ebp
	cmpl	(%rcx,%rbx,4), %ebp
	jg	.LBB8_16
	.p2align	4, 0x90
.LBB8_14:                               # %.lr.ph56
                                        #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r15,%rax,4), %eax
	btl	%ebp, %eax
	sbbq	%rax, %rax
	andl	$1, %eax
	movsbl	.L.str.3(%rax), %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	cube+24(%rip), %rcx
	cmpl	(%rcx,%rbx,4), %ebp
	leal	1(%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB8_14
# BB#15:                                # %._crit_edge57.loopexit
                                        #   in Loop: Header=BB8_13 Depth=1
	movl	cube+8(%rip), %eax
.LBB8_16:                               # %._crit_edge57
                                        #   in Loop: Header=BB8_13 Depth=1
	incq	%rbx
	movslq	%eax, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB8_13
.LBB8_1:                                # %.preheader
	movl	cube+4(%rip), %ecx
	leal	-1(%rcx), %ebp
	cmpl	%ebp, %eax
	jge	.LBB8_7
# BB#2:                                 # %.lr.ph52.preheader
	movslq	%eax, %rbx
	movq	cube+24(%rip), %rax
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph52
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_4 Depth 2
	movq	cube+16(%rip), %rdx
	movl	(%rdx,%rbx,4), %ebp
	cmpl	(%rax,%rbx,4), %ebp
	jg	.LBB8_6
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph50
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r15,%rax,4), %eax
	btl	%ebp, %eax
	sbbq	%rax, %rax
	andl	$1, %eax
	movsbl	.L.str.35(%rax), %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%rbx,4), %ebp
	leal	1(%rbp), %ecx
	movl	%ecx, %ebp
	jl	.LBB8_4
# BB#5:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB8_3 Depth=1
	movl	cube+4(%rip), %ecx
.LBB8_6:                                # %._crit_edge
                                        #   in Loop: Header=BB8_3 Depth=1
	incq	%rbx
	leal	-1(%rcx), %ebp
	movslq	%ebp, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB8_3
.LBB8_7:                                # %._crit_edge53
	cmpl	$-1, cube+124(%rip)
	movq	%r12, %rbx
	je	.LBB8_18
# BB#8:
	movl	$32, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	cube+16(%rip), %rax
	movslq	%ebp, %r12
	movl	(%rax,%r12,4), %r13d
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%r12,4), %r13d
	jg	.LBB8_18
# BB#9:                                 # %.lr.ph
	testq	%rbx, %rbx
	movq	%rbx, %rax
	je	.LBB8_17
# BB#10:
	movl	$.L.str.5, %ebx
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB8_11:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %eax
	sarl	$5, %eax
	cltq
	movl	4(%rbp,%rax,4), %ecx
	movl	%r13d, %edx
	andb	$31, %dl
	movzbl	%dl, %edx
	btl	%edx, %ecx
	movl	$.L.str.3, %ecx
	cmovaeq	%rbx, %rcx
	movl	4(%r15,%rax,4), %eax
	btl	%edx, %eax
	adcq	$0, %rcx
	movsbl	(%rcx), %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%r12,4), %r13d
	leal	1(%r13), %eax
	movl	%eax, %r13d
	jl	.LBB8_11
	jmp	.LBB8_18
	.p2align	4, 0x90
.LBB8_17:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r15,%rax,4), %eax
	btl	%r13d, %eax
	sbbq	%rax, %rax
	andl	$1, %eax
	movsbl	.L.str.3(%rax), %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%r12,4), %r13d
	leal	1(%r13), %eax
	movl	%eax, %r13d
	jl	.LBB8_17
.LBB8_18:                               # %.loopexit
	movl	$10, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.Lfunc_end8:
	.size	print_expanded_cube, .Lfunc_end8-print_expanded_cube
	.cfi_endproc

	.globl	pc1
	.p2align	4, 0x90
	.type	pc1,@function
pc1:                                    # @pc1
	.cfi_startproc
# BB#0:
	movl	$.L.str.1, %esi
	movl	$pc1.s1, %edx
	jmp	fmt_cube                # TAILCALL
.Lfunc_end9:
	.size	pc1, .Lfunc_end9-pc1
	.cfi_endproc

	.globl	pc2
	.p2align	4, 0x90
	.type	pc2,@function
pc2:                                    # @pc2
	.cfi_startproc
# BB#0:
	movl	$.L.str.1, %esi
	movl	$pc2.s2, %edx
	jmp	fmt_cube                # TAILCALL
.Lfunc_end10:
	.size	pc2, .Lfunc_end10-pc2
	.cfi_endproc

	.globl	debug_print
	.p2align	4, 0x90
	.type	debug_print,@function
debug_print:                            # @debug_print
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 64
.Lcfi98:
	.cfi_offset %rbx, -56
.Lcfi99:
	.cfi_offset %r12, -48
.Lcfi100:
	.cfi_offset %r13, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	movq	8(%r13), %rbp
	subq	%r13, %rbp
	shrq	$3, %rbp
	movl	cube(%rip), %r14d
	cmpl	$33, %r14d
	jge	.LBB11_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB11_3
.LBB11_2:
	leal	-1(%r14), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB11_3:
	addl	$-3, %ebp
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r14d, %esi
	callq	set_clear
	movq	%rax, %r14
	testl	%r12d, %r12d
	jne	.LBB11_6
# BB#4:
	movl	verbose_debug(%rip), %eax
	testl	%eax, %eax
	je	.LBB11_6
# BB#5:
	movl	$10, %edi
	callq	putchar
.LBB11_6:
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movl	%r12d, %edx
	movl	%ebp, %ecx
	callq	printf
	cmpl	$0, verbose_debug(%rip)
	je	.LBB11_10
# BB#7:
	movq	(%r13), %rdi
	movl	$.L.str.1, %esi
	movl	$pc1.s1, %edx
	callq	fmt_cube
	movq	%rax, %rcx
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB11_10
# BB#8:                                 # %.lr.ph.preheader
	movl	$3, %ebx
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rbx), %ebp
	movq	(%r13), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	set_or
	movl	$.L.str.1, %esi
	movl	$pc1.s1, %edx
	movq	%rax, %rdi
	callq	fmt_cube
	movq	%rax, %rcx
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	movq	%rcx, %rdx
	callq	printf
	movq	(%r13,%rbx,8), %rsi
	incq	%rbx
	testq	%rsi, %rsi
	jne	.LBB11_9
.LBB11_10:                              # %.loopexit
	testq	%r14, %r14
	je	.LBB11_11
# BB#12:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB11_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	debug_print, .Lfunc_end11-debug_print
	.cfi_endproc

	.globl	debug1_print
	.p2align	4, 0x90
	.type	debug1_print,@function
debug1_print:                           # @debug1_print
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 48
.Lcfi109:
	.cfi_offset %rbx, -48
.Lcfi110:
	.cfi_offset %r12, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r15
	movq	%rdi, %r14
	testl	%ebx, %ebx
	jne	.LBB12_3
# BB#1:
	movl	verbose_debug(%rip), %eax
	testl	%eax, %eax
	je	.LBB12_3
# BB#2:
	movl	$10, %edi
	callq	putchar
.LBB12_3:
	movl	12(%r14), %ecx
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	printf
	cmpl	$0, verbose_debug(%rip)
	je	.LBB12_7
# BB#4:
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB12_7
# BB#5:                                 # %.lr.ph.preheader
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %r12
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB12_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%r15), %ebp
	movl	$.L.str.1, %esi
	movl	$pc1.s1, %edx
	movq	%rbx, %rdi
	callq	fmt_cube
	movq	%rax, %rcx
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movq	%rcx, %rdx
	callq	printf
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r12, %rbx
	movl	%ebp, %r15d
	jb	.LBB12_6
.LBB12_7:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	debug1_print, .Lfunc_end12-debug1_print
	.cfi_endproc

	.globl	cprint
	.p2align	4, 0x90
	.type	cprint,@function
cprint:                                 # @cprint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 32
.Lcfi117:
	.cfi_offset %rbx, -32
.Lcfi118:
	.cfi_offset %r14, -24
.Lcfi119:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB13_3
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %r15
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.1, %esi
	movl	$pc1.s1, %edx
	movq	%rbx, %rdi
	callq	fmt_cube
	movq	%rax, %rdi
	callq	puts
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r15, %rbx
	jb	.LBB13_2
.LBB13_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	cprint, .Lfunc_end13-cprint
	.cfi_endproc

	.globl	makeup_labels
	.p2align	4, 0x90
	.type	makeup_labels,@function
makeup_labels:                          # @makeup_labels
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 48
.Lcfi125:
	.cfi_offset %rbx, -48
.Lcfi126:
	.cfi_offset %r12, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpq	$0, 56(%r14)
	jne	.LBB14_2
# BB#1:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	PLA_labels
.LBB14_2:                               # %.preheader28
	cmpl	$0, cube+4(%rip)
	jle	.LBB14_15
# BB#3:                                 # %.preheader.preheader
	movq	cube+32(%rip), %rax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB14_4:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_6 Depth 2
	cmpl	$0, (%rax,%r15,4)
	jle	.LBB14_14
# BB#5:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB14_4 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_6:                               # %.lr.ph
                                        #   Parent Loop BB14_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cube+16(%rip), %rax
	movl	(%rax,%r15,4), %eax
	addl	%ebp, %eax
	movq	56(%r14), %rbx
	movslq	%eax, %r12
	cmpq	$0, (%rbx,%r12,8)
	jne	.LBB14_13
# BB#7:                                 #   in Loop: Header=BB14_6 Depth=2
	movl	$15, %edi
	callq	malloc
	movq	%rax, (%rbx,%r12,8)
	movslq	cube+8(%rip), %rax
	cmpq	%rax, %r15
	jge	.LBB14_12
# BB#8:                                 #   in Loop: Header=BB14_6 Depth=2
	movq	56(%r14), %rax
	movq	(%rax,%r12,8), %rdi
	testb	$1, %bpl
	jne	.LBB14_11
# BB#9:                                 #   in Loop: Header=BB14_6 Depth=2
	movl	$.L.str.40, %esi
	jmp	.LBB14_10
.LBB14_12:                              #   in Loop: Header=BB14_6 Depth=2
	movq	56(%r14), %rax
	movq	(%rax,%r12,8), %rdi
	movl	$.L.str.42, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%ebp, %ecx
	callq	sprintf
	jmp	.LBB14_13
.LBB14_11:                              #   in Loop: Header=BB14_6 Depth=2
	movl	$.L.str.41, %esi
.LBB14_10:                              #   in Loop: Header=BB14_6 Depth=2
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	sprintf
	.p2align	4, 0x90
.LBB14_13:                              #   in Loop: Header=BB14_6 Depth=2
	incl	%ebp
	movq	cube+32(%rip), %rax
	cmpl	(%rax,%r15,4), %ebp
	jl	.LBB14_6
.LBB14_14:                              # %._crit_edge
                                        #   in Loop: Header=BB14_4 Depth=1
	incq	%r15
	movslq	cube+4(%rip), %rcx
	cmpq	%rcx, %r15
	jl	.LBB14_4
.LBB14_15:                              # %._crit_edge31
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	makeup_labels, .Lfunc_end14-makeup_labels
	.cfi_endproc

	.globl	kiss_output
	.p2align	4, 0x90
	.type	kiss_output,@function
kiss_output:                            # @kiss_output
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 48
.Lcfi135:
	.cfi_offset %rbx, -40
.Lcfi136:
	.cfi_offset %r12, -32
.Lcfi137:
	.cfi_offset %r14, -24
.Lcfi138:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	(%r15), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB15_3
# BB#1:                                 # %.lr.ph28.preheader
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %r12
	.p2align	4, 0x90
.LBB15_2:                               # %.lr.ph28
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.3, %ecx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	kiss_print_cube
	movq	(%r15), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r12, %rbx
	jb	.LBB15_2
.LBB15_3:                               # %._crit_edge29
	movq	8(%r15), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB15_6
# BB#4:                                 # %.lr.ph.preheader
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %r12
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.4, %ecx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	kiss_print_cube
	movq	8(%r15), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r12, %rbx
	jb	.LBB15_5
.LBB15_6:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	kiss_output, .Lfunc_end15-kiss_output
	.cfi_endproc

	.globl	kiss_print_cube
	.p2align	4, 0x90
	.type	kiss_print_cube,@function
kiss_print_cube:                        # @kiss_print_cube
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi145:
	.cfi_def_cfa_offset 64
.Lcfi146:
	.cfi_offset %rbx, -56
.Lcfi147:
	.cfi_offset %r12, -48
.Lcfi148:
	.cfi_offset %r13, -40
.Lcfi149:
	.cfi_offset %r14, -32
.Lcfi150:
	.cfi_offset %r15, -24
.Lcfi151:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	cube+8(%rip), %eax
	testl	%eax, %eax
	jle	.LBB16_3
# BB#1:                                 # %.lr.ph59.preheader
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_2:                               # %.lr.ph59
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	sarl	$4, %eax
	cltq
	movl	4(%r13,%rax,4), %eax
	movl	%ebx, %ecx
	andb	$30, %cl
	shrl	%cl, %eax
	andl	$3, %eax
	movsbl	.L.str.34(%rax), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incl	%ebp
	movl	cube+8(%rip), %eax
	addl	$2, %ebx
	cmpl	%eax, %ebp
	jl	.LBB16_2
.LBB16_3:                               # %.preheader
	movl	cube+4(%rip), %ecx
	decl	%ecx
	cmpl	%ecx, %eax
	jge	.LBB16_18
# BB#4:                                 # %.lr.ph55
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB16_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_10 Depth 2
	movl	$32, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	cube+72(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	setp_implies
	testl	%eax, %eax
	je	.LBB16_8
# BB#6:                                 #   in Loop: Header=BB16_5 Depth=1
	movl	$45, %edi
	jmp	.LBB16_7
	.p2align	4, 0x90
.LBB16_8:                               #   in Loop: Header=BB16_5 Depth=1
	movq	cube+16(%rip), %rax
	movl	(%rax,%rbx,4), %ebp
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%rbx,4), %ebp
	jg	.LBB16_15
# BB#9:                                 # %.lr.ph53.preheader
                                        #   in Loop: Header=BB16_5 Depth=1
	movl	$-1, %ecx
	.p2align	4, 0x90
.LBB16_10:                              # %.lr.ph53
                                        #   Parent Loop BB16_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	movl	4(%r13,%rdx,4), %edx
	btl	%ebp, %edx
	jae	.LBB16_13
# BB#11:                                #   in Loop: Header=BB16_10 Depth=2
	cmpl	$-1, %ecx
	movl	%ebp, %ecx
	je	.LBB16_13
# BB#12:                                #   in Loop: Header=BB16_10 Depth=2
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	fatal
	movq	cube+24(%rip), %rax
	movl	%ebp, %ecx
.LBB16_13:                              #   in Loop: Header=BB16_10 Depth=2
	cmpl	(%rax,%rbx,4), %ebp
	leal	1(%rbp), %edx
	movl	%edx, %ebp
	jl	.LBB16_10
# BB#14:                                # %._crit_edge
                                        #   in Loop: Header=BB16_5 Depth=1
	cmpl	$-1, %ecx
	je	.LBB16_15
# BB#16:                                #   in Loop: Header=BB16_5 Depth=1
	movq	56(%r12), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rdi
	movq	%r15, %rsi
	callq	fputs
	jmp	.LBB16_17
	.p2align	4, 0x90
.LBB16_15:                              # %._crit_edge.thread
                                        #   in Loop: Header=BB16_5 Depth=1
	movl	$126, %edi
.LBB16_7:                               #   in Loop: Header=BB16_5 Depth=1
	movq	%r15, %rsi
	callq	_IO_putc
.LBB16_17:                              #   in Loop: Header=BB16_5 Depth=1
	incq	%rbx
	movslq	cube+4(%rip), %rax
	decq	%rax
	cmpq	%rax, %rbx
	jl	.LBB16_5
.LBB16_18:                              # %._crit_edge56
	movslq	cube+124(%rip), %rbx
	cmpq	$-1, %rbx
	je	.LBB16_21
# BB#19:
	movl	$32, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	cube+16(%rip), %rax
	movl	(%rax,%rbx,4), %ebp
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%rbx,4), %ebp
	jg	.LBB16_21
	.p2align	4, 0x90
.LBB16_20:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r13,%rax,4), %eax
	btl	%ebp, %eax
	movq	%r14, %rax
	adcq	$0, %rax
	movsbl	(%rax), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%rbx,4), %ebp
	leal	1(%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB16_20
.LBB16_21:                              # %.loopexit
	movl	$10, %edi
	movq	%r15, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.Lfunc_end16:
	.size	kiss_print_cube, .Lfunc_end16-kiss_print_cube
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI17_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI17_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	output_symbolic_constraints
	.p2align	4, 0x90
	.type	output_symbolic_constraints,@function
output_symbolic_constraints:            # @output_symbolic_constraints
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi156:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi158:
	.cfi_def_cfa_offset 96
.Lcfi159:
	.cfi_offset %rbx, -56
.Lcfi160:
	.cfi_offset %r12, -48
.Lcfi161:
	.cfi_offset %r13, -40
.Lcfi162:
	.cfi_offset %r14, -32
.Lcfi163:
	.cfi_offset %r15, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r13
	movl	cube+4(%rip), %eax
	subl	cube+8(%rip), %eax
	cmpl	$2, %eax
	jl	.LBB17_67
# BB#1:
	movq	%r12, %rdi
	callq	makeup_labels
	movslq	cube+8(%rip), %rcx
	movl	cube+4(%rip), %eax
	decl	%eax
	cmpl	%eax, %ecx
	jge	.LBB17_67
# BB#2:                                 # %.lr.ph175
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_12 Depth 2
                                        #     Child Loop BB17_16 Depth 2
                                        #     Child Loop BB17_19 Depth 2
                                        #     Child Loop BB17_24 Depth 2
                                        #     Child Loop BB17_30 Depth 2
                                        #     Child Loop BB17_33 Depth 2
                                        #       Child Loop BB17_37 Depth 3
                                        #     Child Loop BB17_46 Depth 2
                                        #       Child Loop BB17_49 Depth 3
                                        #     Child Loop BB17_58 Depth 2
                                        #       Child Loop BB17_61 Depth 3
	movq	cube+32(%rip), %rax
	movq	%rcx, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movslq	(%rax,%rcx,4), %rbx
	leaq	(,%rbx,4), %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%rbx, %rbx
	movl	%ebx, %r14d
	jle	.LBB17_20
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(,%rax,4), %rax
	addq	cube+16(%rip), %rax
	cmpl	$8, %r14d
	jae	.LBB17_6
# BB#5:                                 #   in Loop: Header=BB17_3 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB17_14
	.p2align	4, 0x90
.LBB17_6:                               # %min.iters.checked
                                        #   in Loop: Header=BB17_3 Depth=1
	movl	%r14d, %edx
	andl	$7, %edx
	movq	%r14, %rcx
	subq	%rdx, %rcx
	je	.LBB17_10
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB17_3 Depth=1
	cmpq	%rax, %r15
	jae	.LBB17_11
# BB#8:                                 # %vector.memcheck
                                        #   in Loop: Header=BB17_3 Depth=1
	leaq	(%r15,%r14,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB17_11
.LBB17_10:                              #   in Loop: Header=BB17_3 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB17_14
.LBB17_11:                              # %vector.body.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r15), %rsi
	movq	%rcx, %rdi
	movdqa	.LCPI17_0(%rip), %xmm1  # xmm1 = [0,1,2,3]
	movdqa	.LCPI17_1(%rip), %xmm3  # xmm3 = [4,4,4,4]
	movdqa	.LCPI17_2(%rip), %xmm4  # xmm4 = [8,8,8,8]
	.p2align	4, 0x90
.LBB17_12:                              # %vector.body
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	%xmm0, %xmm2
	paddd	%xmm1, %xmm2
	movdqu	%xmm2, -16(%rsi)
	paddd	%xmm3, %xmm2
	movdqu	%xmm2, (%rsi)
	paddd	%xmm4, %xmm1
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB17_12
# BB#13:                                # %middle.block
                                        #   in Loop: Header=BB17_3 Depth=1
	testl	%edx, %edx
	je	.LBB17_20
	.p2align	4, 0x90
.LBB17_14:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	movl	%r14d, %esi
	subl	%ecx, %esi
	leaq	-1(%r14), %rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB17_17
# BB#15:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB17_16:                              # %scalar.ph.prol
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %edi
	addl	%ecx, %edi
	movl	%edi, (%r15,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB17_16
.LBB17_17:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB17_3 Depth=1
	cmpq	$3, %rdx
	jb	.LBB17_20
# BB#18:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	%r14, %rdx
	subq	%rcx, %rdx
	leaq	12(%r15,%rcx,4), %rsi
	incq	%rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB17_19:                              # %scalar.ph
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ebx
	leal	(%rcx,%rdi), %ebp
	leal	-1(%rbx,%rbp), %ebx
	movl	%ebx, -12(%rsi,%rdi,4)
	movl	%ebp, %ebx
	addl	(%rax), %ebx
	movl	%ebx, -8(%rsi,%rdi,4)
	movl	(%rax), %ebx
	leal	1(%rbx,%rbp), %ebx
	movl	%ebx, -4(%rsi,%rdi,4)
	movl	(%rax), %ebx
	leal	2(%rbx,%rbp), %ebp
	movl	%ebp, (%rsi,%rdi,4)
	addq	$4, %rdi
	cmpq	%rdi, %rdx
	jne	.LBB17_19
.LBB17_20:                              # %._crit_edge
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	(%r12), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r15, %rsi
	movl	%r14d, %edx
	callq	sf_permute
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.LBB17_22
# BB#21:                                #   in Loop: Header=BB17_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB17_22:                              # %.preheader132
                                        #   in Loop: Header=BB17_3 Depth=1
	movl	12(%rbx), %eax
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB17_28
# BB#23:                                # %.lr.ph138
                                        #   in Loop: Header=BB17_3 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_24:                              #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rax
	movslq	(%rbx), %rcx
	movslq	%ebp, %rdx
	imulq	%rcx, %rdx
	leaq	(%rax,%rdx,4), %rdi
	xorl	%eax, %eax
	callq	set_ord
	cmpl	$1, %eax
	je	.LBB17_26
# BB#25:                                #   in Loop: Header=BB17_24 Depth=2
	cmpl	4(%rbx), %eax
	jne	.LBB17_27
.LBB17_26:                              #   in Loop: Header=BB17_24 Depth=2
	leal	-1(%rbp), %r14d
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	sf_delset
	incl	%r15d
	movl	%r14d, %ebp
.LBB17_27:                              #   in Loop: Header=BB17_24 Depth=2
	incl	%ebp
	movl	12(%rbx), %eax
	cmpl	%eax, %ebp
	jl	.LBB17_24
.LBB17_28:                              # %._crit_edge139
                                        #   in Loop: Header=BB17_3 Depth=1
	movslq	%eax, %rbp
	leaq	(,%rbp,4), %rdi
	callq	malloc
	movq	%rax, %r14
	testl	%ebp, %ebp
	jle	.LBB17_43
# BB#29:                                # %.lr.ph144
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	24(%rbx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB17_30:                              #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rbx), %rax
	movslq	%edx, %rdx
	imulq	%rdx, %rax
	andl	$-2049, (%rcx,%rax,4)   # imm = 0xF7FF
	incl	%edx
	movl	12(%rbx), %eax
	cmpl	%eax, %edx
	jl	.LBB17_30
# BB#31:                                # %.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	testl	%eax, %eax
	jle	.LBB17_43
# BB#32:                                # %.lr.ph152
                                        #   in Loop: Header=BB17_3 Depth=1
	xorl	%ebp, %ebp
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB17_33:                              #   Parent Loop BB17_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_37 Depth 3
	movl	$0, (%r14,%rbp,4)
	movq	24(%rbx), %rcx
	movl	(%rbx), %edx
	movl	%edx, %esi
	imull	%ebp, %esi
	movslq	%esi, %rsi
	testb	$8, 1(%rcx,%rsi,4)
	jne	.LBB17_41
# BB#34:                                #   in Loop: Header=BB17_33 Depth=2
	movl	$1, (%r14,%rbp,4)
	leaq	1(%rbp), %rdi
	movslq	%eax, %rsi
	cmpq	%rsi, %rdi
	jge	.LBB17_42
# BB#35:                                # %.lr.ph148.preheader
                                        #   in Loop: Header=BB17_33 Depth=2
	movq	%r14, %r15
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leal	1(%rdi), %r12d
	movl	$1, %r13d
	jmp	.LBB17_37
	.p2align	4, 0x90
.LBB17_36:                              # %.backedge..lr.ph148_crit_edge
                                        #   in Loop: Header=BB17_37 Depth=3
	movq	24(%rbx), %rcx
	movl	(%rbx), %edx
	incl	%r12d
.LBB17_37:                              # %.lr.ph148
                                        #   Parent Loop BB17_3 Depth=1
                                        #     Parent Loop BB17_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%r12), %r14d
	movl	%edx, %eax
	imull	%ebp, %eax
	cltq
	leaq	(%rcx,%rax,4), %rdi
	imull	%r14d, %edx
	movslq	%edx, %rax
	leaq	(%rcx,%rax,4), %rsi
	xorl	%eax, %eax
	callq	setp_equal
	testl	%eax, %eax
	je	.LBB17_39
# BB#38:                                #   in Loop: Header=BB17_37 Depth=3
	incl	%r13d
	movl	%r13d, (%r15,%rbp,4)
	movq	24(%rbx), %rax
	movslq	(%rbx), %rcx
	movslq	%r14d, %rdx
	imulq	%rcx, %rdx
	orl	$2048, (%rax,%rdx,4)    # imm = 0x800
.LBB17_39:                              # %.backedge
                                        #   in Loop: Header=BB17_37 Depth=3
	movl	12(%rbx), %eax
	cmpl	%eax, %r12d
	jl	.LBB17_36
# BB#40:                                #   in Loop: Header=BB17_33 Depth=2
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	%r15, %r14
	movl	12(%rsp), %r15d         # 4-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB17_42
	.p2align	4, 0x90
.LBB17_41:                              # %..loopexit_crit_edge
                                        #   in Loop: Header=BB17_33 Depth=2
	incq	%rbp
	movq	%rbp, %rdi
.LBB17_42:                              # %.loopexit
                                        #   in Loop: Header=BB17_33 Depth=2
	movslq	%eax, %rcx
	cmpq	%rcx, %rdi
	movq	%rdi, %rbp
	jl	.LBB17_33
.LBB17_43:                              # %._crit_edge153
                                        #   in Loop: Header=BB17_3 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB17_56
# BB#44:                                #   in Loop: Header=BB17_3 Depth=1
	movl	$.L.str.48, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	(%rsp), %rdx            # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB17_54
# BB#45:                                # %.lr.ph162
                                        #   in Loop: Header=BB17_3 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB17_46:                              #   Parent Loop BB17_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_49 Depth 3
	movl	(%r14,%r15,4), %edx
	testl	%edx, %edx
	jle	.LBB17_53
# BB#47:                                #   in Loop: Header=BB17_46 Depth=2
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB17_52
# BB#48:                                # %.lr.ph156.preheader
                                        #   in Loop: Header=BB17_46 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_49:                              # %.lr.ph156
                                        #   Parent Loop BB17_3 Depth=1
                                        #     Parent Loop BB17_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	(%rbx), %rcx
	movslq	%r15d, %rdx
	imulq	%rcx, %rdx
	shlq	$2, %rdx
	addq	24(%rbx), %rdx
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rdx,%rcx,4), %ecx
	btl	%ebp, %ecx
	jae	.LBB17_51
# BB#50:                                #   in Loop: Header=BB17_49 Depth=3
	movq	56(%r12), %rax
	movq	cube+16(%rip), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	(%rcx,%rdx,4), %ecx
	addl	%ebp, %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rdx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	4(%rbx), %eax
.LBB17_51:                              #   in Loop: Header=BB17_49 Depth=3
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB17_49
.LBB17_52:                              # %._crit_edge157
                                        #   in Loop: Header=BB17_46 Depth=2
	movl	$.L.str.50, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	movl	12(%rbx), %eax
.LBB17_53:                              #   in Loop: Header=BB17_46 Depth=2
	incq	%r15
	movslq	%eax, %rcx
	cmpq	%rcx, %r15
	jl	.LBB17_46
.LBB17_54:                              # %._crit_edge163
                                        #   in Loop: Header=BB17_3 Depth=1
	testq	%r14, %r14
	je	.LBB17_66
# BB#55:                                #   in Loop: Header=BB17_3 Depth=1
	movq	%r14, %rdi
	callq	free
	jmp	.LBB17_66
	.p2align	4, 0x90
.LBB17_56:                              #   in Loop: Header=BB17_3 Depth=1
	movl	$.L.str.44, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	%ebp, %edx
	callq	fprintf
	movl	$.L.str.45, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%r15d, %edx
	callq	fprintf
	movq	cube+32(%rip), %rax
	movl	(%rax,%rbp,4), %edx
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB17_66
# BB#57:                                # %.lr.ph172
                                        #   in Loop: Header=BB17_3 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB17_58:                              #   Parent Loop BB17_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_61 Depth 3
	movl	(%r14,%r15,4), %edx
	testl	%edx, %edx
	jle	.LBB17_65
# BB#59:                                #   in Loop: Header=BB17_58 Depth=2
	movl	$.L.str.47, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB17_64
# BB#60:                                # %.lr.ph166.preheader
                                        #   in Loop: Header=BB17_58 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_61:                              # %.lr.ph166
                                        #   Parent Loop BB17_3 Depth=1
                                        #     Parent Loop BB17_58 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	(%rbx), %rcx
	movslq	%r15d, %rdx
	imulq	%rcx, %rdx
	shlq	$2, %rdx
	addq	24(%rbx), %rdx
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rdx,%rcx,4), %ecx
	btl	%ebp, %ecx
	jae	.LBB17_63
# BB#62:                                #   in Loop: Header=BB17_61 Depth=3
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebp, %edx
	callq	fprintf
	movl	4(%rbx), %eax
.LBB17_63:                              #   in Loop: Header=BB17_61 Depth=3
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB17_61
.LBB17_64:                              # %._crit_edge167
                                        #   in Loop: Header=BB17_58 Depth=2
	movl	$10, %edi
	movq	%r13, %rsi
	callq	fputc
	movl	12(%rbx), %eax
.LBB17_65:                              #   in Loop: Header=BB17_58 Depth=2
	incq	%r15
	movslq	%eax, %rcx
	cmpq	%rcx, %r15
	jl	.LBB17_58
.LBB17_66:                              # %.loopexit131
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	incq	%rcx
	movslq	cube+4(%rip), %rax
	decq	%rax
	cmpq	%rax, %rcx
	jl	.LBB17_3
.LBB17_67:                              # %.loopexit133
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	output_symbolic_constraints, .Lfunc_end17-output_symbolic_constraints
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	".p %d\n"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"01"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	".e\n"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"~1"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"~2"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"~0"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	".end\n"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	".type "
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	".i %d\n"
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	".o %d\n"
	.size	.L.str.9, 7

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	".mv %d %d"
	.size	.L.str.10, 10

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	" %d"
	.size	.L.str.11, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	".ilb"
	.size	.L.str.13, 5

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	" %s"
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	".ob"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	".label var=%d"
	.size	.L.str.16, 14

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"#.phase "
	.size	.L.str.17, 9

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"\n.group"
	.size	.L.str.19, 8

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	" ("
	.size	.L.str.20, 3

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	" \\\n"
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%s"
	.size	.L.str.22, 3

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	".label"
	.size	.L.str.24, 7

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Cannot have no-output function for EQNTOTT output mode"
	.size	.L.str.25, 55

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Must have binary-valued function for EQNTOTT output mode"
	.size	.L.str.26, 57

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"%s = "
	.size	.L.str.27, 6

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	" | ("
	.size	.L.str.29, 5

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"\n    "
	.size	.L.str.30, 6

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"?01-"
	.size	.L.str.34, 5

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"1~"
	.size	.L.str.35, 3

	.type	pc1.s1,@object          # @pc1.s1
	.local	pc1.s1
	.comm	pc1.s1,256,16
	.type	pc2.s2,@object          # @pc2.s2
	.local	pc2.s2
	.comm	pc2.s2,256,16
	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"%s[%d]: ord(T)=%d\n"
	.size	.L.str.36, 19

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"cofactor=%s\n"
	.size	.L.str.37, 13

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%4d. %s\n"
	.size	.L.str.38, 9

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"v%d.bar"
	.size	.L.str.40, 8

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"v%d"
	.size	.L.str.41, 4

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"v%d.%d"
	.size	.L.str.42, 7

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"more than 1 part in a symbolic variable\n"
	.size	.L.str.43, 41

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"# Symbolic constraints for variable %d (Numeric form)\n"
	.size	.L.str.44, 55

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"# unconstrained weight = %d\n"
	.size	.L.str.45, 29

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"num_codes=%d\n"
	.size	.L.str.46, 14

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"weight=%d: "
	.size	.L.str.47, 12

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"# Symbolic constraints for variable %d (Symbolic form)\n"
	.size	.L.str.48, 56

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"#   w=%d: ("
	.size	.L.str.49, 12

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	" )\n"
	.size	.L.str.50, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	".option unmerged"
	.size	.Lstr, 17

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	".end"
	.size	.Lstr.1, 5

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	";\n"
	.size	.Lstr.2, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
