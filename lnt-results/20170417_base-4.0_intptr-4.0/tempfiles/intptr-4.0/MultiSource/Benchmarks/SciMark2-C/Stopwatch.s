	.text
	.file	"Stopwatch.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	seconds
	.p2align	4, 0x90
	.type	seconds,@function
seconds:                                # @seconds
	.cfi_startproc
# BB#0:
	movsd	seconds.t(%rip), %xmm0  # xmm0 = mem[0],zero
	addsd	.LCPI0_0(%rip), %xmm0
	movsd	%xmm0, seconds.t(%rip)
	retq
.Lfunc_end0:
	.size	seconds, .Lfunc_end0-seconds
	.cfi_endproc

	.globl	Stopwtach_reset
	.p2align	4, 0x90
	.type	Stopwtach_reset,@function
Stopwtach_reset:                        # @Stopwtach_reset
	.cfi_startproc
# BB#0:
	movl	$0, (%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
	retq
.Lfunc_end1:
	.size	Stopwtach_reset, .Lfunc_end1-Stopwtach_reset
	.cfi_endproc

	.globl	new_Stopwatch
	.p2align	4, 0x90
	.type	new_Stopwatch,@function
new_Stopwatch:                          # @new_Stopwatch
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$24, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB2_1
# BB#2:
	movl	$0, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	popq	%rcx
	retq
.LBB2_1:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	new_Stopwatch, .Lfunc_end2-new_Stopwatch
	.cfi_endproc

	.globl	Stopwatch_delete
	.p2align	4, 0x90
	.type	Stopwatch_delete,@function
Stopwatch_delete:                       # @Stopwatch_delete
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB3_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB3_1:
	retq
.Lfunc_end3:
	.size	Stopwatch_delete, .Lfunc_end3-Stopwatch_delete
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	Stopwatch_start
	.p2align	4, 0x90
	.type	Stopwatch_start,@function
Stopwatch_start:                        # @Stopwatch_start
	.cfi_startproc
# BB#0:
	cmpl	$0, (%rdi)
	jne	.LBB4_2
# BB#1:
	movl	$1, (%rdi)
	movq	$0, 16(%rdi)
	movsd	seconds.t(%rip), %xmm0  # xmm0 = mem[0],zero
	addsd	.LCPI4_0(%rip), %xmm0
	movsd	%xmm0, seconds.t(%rip)
	movsd	%xmm0, 8(%rdi)
.LBB4_2:
	retq
.Lfunc_end4:
	.size	Stopwatch_start, .Lfunc_end4-Stopwatch_start
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	Stopwatch_resume
	.p2align	4, 0x90
	.type	Stopwatch_resume,@function
Stopwatch_resume:                       # @Stopwatch_resume
	.cfi_startproc
# BB#0:
	cmpl	$0, (%rdi)
	jne	.LBB5_2
# BB#1:
	movsd	seconds.t(%rip), %xmm0  # xmm0 = mem[0],zero
	addsd	.LCPI5_0(%rip), %xmm0
	movsd	%xmm0, seconds.t(%rip)
	movsd	%xmm0, 8(%rdi)
	movl	$1, (%rdi)
.LBB5_2:
	retq
.Lfunc_end5:
	.size	Stopwatch_resume, .Lfunc_end5-Stopwatch_resume
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	Stopwatch_stop
	.p2align	4, 0x90
	.type	Stopwatch_stop,@function
Stopwatch_stop:                         # @Stopwatch_stop
	.cfi_startproc
# BB#0:
	cmpl	$0, (%rdi)
	je	.LBB6_2
# BB#1:
	movsd	seconds.t(%rip), %xmm0  # xmm0 = mem[0],zero
	addsd	.LCPI6_0(%rip), %xmm0
	movsd	%xmm0, seconds.t(%rip)
	subsd	8(%rdi), %xmm0
	addsd	16(%rdi), %xmm0
	movsd	%xmm0, 16(%rdi)
	movl	$0, (%rdi)
.LBB6_2:
	retq
.Lfunc_end6:
	.size	Stopwatch_stop, .Lfunc_end6-Stopwatch_stop
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	Stopwatch_read
	.p2align	4, 0x90
	.type	Stopwatch_read,@function
Stopwatch_read:                         # @Stopwatch_read
	.cfi_startproc
# BB#0:
	cmpl	$0, (%rdi)
	je	.LBB7_1
# BB#2:
	movsd	seconds.t(%rip), %xmm1  # xmm1 = mem[0],zero
	addsd	.LCPI7_0(%rip), %xmm1
	movsd	%xmm1, seconds.t(%rip)
	movapd	%xmm1, %xmm0
	subsd	8(%rdi), %xmm0
	addsd	16(%rdi), %xmm0
	movsd	%xmm0, 16(%rdi)
	movsd	%xmm1, 8(%rdi)
	retq
.LBB7_1:                                # %._crit_edge
	movsd	16(%rdi), %xmm0         # xmm0 = mem[0],zero
	retq
.Lfunc_end7:
	.size	Stopwatch_read, .Lfunc_end7-Stopwatch_read
	.cfi_endproc

	.type	seconds.t,@object       # @seconds.t
	.local	seconds.t
	.comm	seconds.t,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
