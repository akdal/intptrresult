	.text
	.file	"textloc.bc"
	.globl	ConvertToFloat
	.p2align	4, 0x90
	.type	ConvertToFloat,@function
ConvertToFloat:                         # @ConvertToFloat
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movslq	32(%r14), %rax
	movslq	36(%r14), %rbx
	imulq	%rax, %rbx
	leaq	(,%rbx,4), %rdi
	callq	malloc
	movq	%rax, 64(%r14)
	testl	%ebx, %ebx
	jle	.LBB0_16
# BB#1:                                 # %.lr.ph
	movq	48(%r14), %r8
	cmpl	$8, %ebx
	jae	.LBB0_3
# BB#2:
	xorl	%edx, %edx
	jmp	.LBB0_15
.LBB0_3:                                # %min.iters.checked
	movq	%rbx, %rdx
	andq	$-8, %rdx
	je	.LBB0_4
# BB#5:                                 # %vector.memcheck
	leaq	(%r8,%rbx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB0_8
# BB#6:                                 # %vector.memcheck
	leaq	(%rax,%rbx,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB0_8
# BB#7:
	xorl	%edx, %edx
	jmp	.LBB0_15
.LBB0_4:
	xorl	%edx, %edx
	jmp	.LBB0_15
.LBB0_8:                                # %vector.body.preheader
	leaq	-8(%rdx), %rcx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	btl	$3, %ecx
	jb	.LBB0_9
# BB#10:                                # %vector.body.prol
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	movd	4(%r8), %xmm1           # xmm1 = mem[0],zero,zero,zero
	pxor	%xmm2, %xmm2
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	cvtdq2ps	%xmm0, %xmm0
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	cvtdq2ps	%xmm1, %xmm1
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movl	$8, %ecx
	testq	%rsi, %rsi
	jne	.LBB0_12
	jmp	.LBB0_14
.LBB0_9:
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	je	.LBB0_14
.LBB0_12:                               # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rcx, %rsi
	leaq	12(%r8,%rcx), %rdi
	leaq	48(%rax,%rcx,4), %rcx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-12(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movd	-8(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	cvtdq2ps	%xmm1, %xmm1
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	cvtdq2ps	%xmm2, %xmm2
	movups	%xmm1, -48(%rcx)
	movups	%xmm2, -32(%rcx)
	movd	-4(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movd	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	cvtdq2ps	%xmm1, %xmm1
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	cvtdq2ps	%xmm2, %xmm2
	movups	%xmm1, -16(%rcx)
	movups	%xmm2, (%rcx)
	addq	$16, %rdi
	addq	$64, %rcx
	addq	$-16, %rsi
	jne	.LBB0_13
.LBB0_14:                               # %middle.block
	cmpq	%rdx, %rbx
	je	.LBB0_16
	.p2align	4, 0x90
.LBB0_15:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r8,%rdx), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	movss	%xmm0, (%rax,%rdx,4)
	incq	%rdx
	cmpq	%rbx, %rdx
	jl	.LBB0_15
.LBB0_16:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	ConvertToFloat, .Lfunc_end0-ConvertToFloat
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1132396544              # float 255
	.text
	.globl	HorzVariance
	.p2align	4, 0x90
	.type	HorzVariance,@function
HorzVariance:                           # @HorzVariance
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 128
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	movl	32(%rbx), %ebp
	movl	36(%rbx), %ecx
	movl	%ebp, %eax
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	imull	%ecx, %eax
	movslq	%eax, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rax, 72(%rbx)
	movl	%ebp, 68(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	jle	.LBB1_41
# BB#1:                                 # %.preheader.lr.ph
	movl	12(%rsp), %ecx          # 4-byte Reload
	cvtsi2ssl	%ecx, %xmm0
	movq	40(%rsp), %rcx          # 8-byte Reload
	leal	-10(%rcx), %r8d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$10, 4(%rsp)            # 4-byte Folded Spill
	movss	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #       Child Loop BB1_27 Depth 3
                                        #       Child Loop BB1_30 Depth 3
                                        #       Child Loop BB1_38 Depth 3
                                        #     Child Loop BB1_9 Depth 2
                                        #       Child Loop BB1_18 Depth 3
                                        #       Child Loop BB1_12 Depth 3
                                        #       Child Loop BB1_15 Depth 3
	cmpl	$11, %r8d
	jl	.LBB1_40
# BB#3:                                 # %.lr.ph59
                                        #   in Loop: Header=BB1_2 Depth=1
	movslq	HVAR_WINDOW(%rip), %rdi
	movq	40(%rsp), %r13          # 8-byte Reload
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	imull	8(%rsp), %r13d          # 4-byte Folded Reload
	movq	%rdi, %r10
	negq	%r10
	cmpl	$-1, 12(%rsp)           # 4-byte Folded Reload
	leal	1(%rdi,%rdi), %ecx
	cvtsi2ssl	%ecx, %xmm2
	movslq	%r10d, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	je	.LBB1_8
# BB#4:                                 # %.lr.ph59.split.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	leaq	1(%rdi), %rsi
	subq	%r10, %rsi
	movq	%rdi, %rdx
	subq	%r10, %rdx
	movl	%esi, %r11d
	andl	$3, %r11d
	andl	$1, %esi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	incq	%rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r11, %rcx
	negq	%rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	$10, %r12d
	movl	4(%rsp), %ecx           # 4-byte Reload
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_33:                               # %.lr.ph55
                                        #   in Loop: Header=BB1_5 Depth=2
	divss	%xmm2, %xmm3
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movslq	%r9d, %rbx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	48(%rcx), %rcx
	movss	(%rax,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	jne	.LBB1_35
# BB#34:                                #   in Loop: Header=BB1_5 Depth=2
	movq	16(%rsp), %rsi          # 8-byte Reload
	testq	%rdx, %rdx
	jne	.LBB1_37
	jmp	.LBB1_39
.LBB1_35:                               #   in Loop: Header=BB1_5 Depth=2
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rbx), %rsi
	movzbl	(%rcx,%rsi), %esi
	cvtsi2ssl	%esi, %xmm5
	subss	%xmm3, %xmm5
	mulss	%xmm5, %xmm5
	addss	%xmm5, %xmm4
	movss	%xmm4, (%rax,%rbx,4)
	movq	32(%rsp), %rsi          # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB1_39
.LBB1_37:                               # %.lr.ph55.new
                                        #   in Loop: Header=BB1_5 Depth=2
	leaq	1(%rcx,%r14), %rcx
	decq	%rsi
	.p2align	4, 0x90
.LBB1_38:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rcx,%rsi), %ebp
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%ebp, %xmm5
	subss	%xmm3, %xmm5
	mulss	%xmm5, %xmm5
	addss	%xmm4, %xmm5
	movss	%xmm5, (%rax,%rbx,4)
	movzbl	1(%rcx,%rsi), %ebp
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebp, %xmm4
	subss	%xmm3, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm5, %xmm4
	movss	%xmm4, (%rax,%rbx,4)
	addq	$2, %rsi
	cmpq	%rdi, %rsi
	jl	.LBB1_38
	jmp	.LBB1_39
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph59.split
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_27 Depth 3
                                        #       Child Loop BB1_30 Depth 3
                                        #       Child Loop BB1_38 Depth 3
	leal	(%r13,%r12), %r9d
	cmpl	%r10d, %edi
	movslq	%ecx, %r14
	jl	.LBB1_32
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_5 Depth=2
	testq	%r11, %r11
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	48(%rcx), %rbx
	je	.LBB1_7
# BB#26:                                # %.prol.preheader
                                        #   in Loop: Header=BB1_5 Depth=2
	leaq	(%rbx,%r14), %rcx
	xorps	%xmm3, %xmm3
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	%r10, %rsi
	.p2align	4, 0x90
.LBB1_27:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rcx,%rsi), %ebp
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebp, %xmm4
	addss	%xmm4, %xmm3
	incq	%rsi
	incq	%r15
	jne	.LBB1_27
	jmp	.LBB1_28
.LBB1_7:                                #   in Loop: Header=BB1_5 Depth=2
	xorps	%xmm3, %xmm3
	movq	%r10, %rsi
.LBB1_28:                               # %.prol.loopexit
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpq	$3, %rdx
	jb	.LBB1_31
# BB#29:                                # %.lr.ph.new
                                        #   in Loop: Header=BB1_5 Depth=2
	decq	%rsi
	leaq	3(%rbx,%r14), %rcx
	.p2align	4, 0x90
.LBB1_30:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-2(%rcx,%rsi), %ebx
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebx, %xmm4
	addss	%xmm3, %xmm4
	movzbl	-1(%rcx,%rsi), %ebx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebx, %xmm3
	addss	%xmm4, %xmm3
	movzbl	(%rcx,%rsi), %ebx
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebx, %xmm4
	addss	%xmm3, %xmm4
	movzbl	1(%rcx,%rsi), %ebx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebx, %xmm3
	addss	%xmm4, %xmm3
	addq	$4, %rsi
	cmpq	%rdi, %rsi
	jl	.LBB1_30
.LBB1_31:                               # %._crit_edge
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpl	%r10d, %edi
	jge	.LBB1_33
.LBB1_32:                               # %._crit_edge.._crit_edge56_crit_edge
                                        #   in Loop: Header=BB1_5 Depth=2
	movslq	%r9d, %rbx
.LBB1_39:                               # %._crit_edge56
                                        #   in Loop: Header=BB1_5 Depth=2
	movss	(%rax,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm3
	cmpless	%xmm0, %xmm3
	andnps	%xmm1, %xmm3
	movss	%xmm3, (%rax,%rbx,4)
	incl	%r12d
	leal	1(%r14), %ecx
	cmpl	%r8d, %r12d
	jl	.LBB1_5
	jmp	.LBB1_40
.LBB1_8:                                # %.lr.ph59.split.us.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	leaq	1(%rdi), %rdx
	subq	%r10, %rdx
	movq	%rdi, %r9
	subq	%r10, %r9
	movl	%edx, %r12d
	andl	$3, %r12d
	andl	$1, %edx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	incq	%rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r12, %rcx
	negq	%rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	$10, %edx
	movl	4(%rsp), %ecx           # 4-byte Reload
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_21:                               # %.lr.ph55.us
                                        #   in Loop: Header=BB1_9 Depth=2
	divss	%xmm2, %xmm3
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movslq	%r15d, %rbx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	48(%rcx), %rcx
	movss	(%rax,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	jne	.LBB1_23
# BB#22:                                #   in Loop: Header=BB1_9 Depth=2
	movq	16(%rsp), %rsi          # 8-byte Reload
	testq	%r9, %r9
	jne	.LBB1_25
	jmp	.LBB1_16
.LBB1_23:                               #   in Loop: Header=BB1_9 Depth=2
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rbx), %rsi
	movzbl	(%rcx,%rsi), %esi
	cvtsi2ssl	%esi, %xmm5
	subss	%xmm3, %xmm5
	mulss	%xmm5, %xmm5
	addss	%xmm5, %xmm4
	movss	%xmm4, (%rax,%rbx,4)
	movq	32(%rsp), %rsi          # 8-byte Reload
	testq	%r9, %r9
	je	.LBB1_16
.LBB1_25:                               # %.lr.ph55.us.new
                                        #   in Loop: Header=BB1_9 Depth=2
	leaq	1(%rcx,%r14), %rcx
	decq	%rsi
	.p2align	4, 0x90
.LBB1_15:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rcx,%rsi), %ebp
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%ebp, %xmm5
	subss	%xmm3, %xmm5
	mulss	%xmm5, %xmm5
	addss	%xmm4, %xmm5
	movss	%xmm5, (%rax,%rbx,4)
	movzbl	1(%rcx,%rsi), %ebp
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebp, %xmm4
	subss	%xmm3, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm5, %xmm4
	movss	%xmm4, (%rax,%rbx,4)
	addq	$2, %rsi
	cmpq	%rdi, %rsi
	jl	.LBB1_15
	jmp	.LBB1_16
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph59.split.us
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_18 Depth 3
                                        #       Child Loop BB1_12 Depth 3
                                        #       Child Loop BB1_15 Depth 3
	leal	(%r13,%rdx), %r15d
	cmpl	%r10d, %edi
	movslq	%ecx, %r14
	jl	.LBB1_14
# BB#10:                                # %.lr.ph.us
                                        #   in Loop: Header=BB1_9 Depth=2
	testq	%r12, %r12
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	48(%rcx), %rbx
	je	.LBB1_11
# BB#17:                                # %.prol.preheader95
                                        #   in Loop: Header=BB1_9 Depth=2
	leaq	(%rbx,%r14), %rcx
	xorps	%xmm3, %xmm3
	movq	56(%rsp), %r11          # 8-byte Reload
	movq	%r10, %rsi
	.p2align	4, 0x90
.LBB1_18:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rcx,%rsi), %ebp
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebp, %xmm4
	addss	%xmm4, %xmm3
	incq	%rsi
	incq	%r11
	jne	.LBB1_18
	jmp	.LBB1_19
.LBB1_11:                               #   in Loop: Header=BB1_9 Depth=2
	xorps	%xmm3, %xmm3
	movq	%r10, %rsi
.LBB1_19:                               # %.prol.loopexit96
                                        #   in Loop: Header=BB1_9 Depth=2
	cmpq	$3, %r9
	jb	.LBB1_13
# BB#20:                                # %.lr.ph.us.new
                                        #   in Loop: Header=BB1_9 Depth=2
	decq	%rsi
	leaq	3(%rbx,%r14), %rcx
	.p2align	4, 0x90
.LBB1_12:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-2(%rcx,%rsi), %ebx
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebx, %xmm4
	addss	%xmm3, %xmm4
	movzbl	-1(%rcx,%rsi), %ebx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebx, %xmm3
	addss	%xmm4, %xmm3
	movzbl	(%rcx,%rsi), %ebx
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebx, %xmm4
	addss	%xmm3, %xmm4
	movzbl	1(%rcx,%rsi), %ebx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebx, %xmm3
	addss	%xmm4, %xmm3
	addq	$4, %rsi
	cmpq	%rdi, %rsi
	jl	.LBB1_12
.LBB1_13:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_9 Depth=2
	cmpl	%r10d, %edi
	jge	.LBB1_21
.LBB1_14:                               # %._crit_edge.us.._crit_edge56.us_crit_edge
                                        #   in Loop: Header=BB1_9 Depth=2
	movslq	%r15d, %rbx
.LBB1_16:                               # %._crit_edge56.us
                                        #   in Loop: Header=BB1_9 Depth=2
	movss	(%rax,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm3
	movss	%xmm3, (%rax,%rbx,4)
	incl	%edx
	leal	1(%r14), %ecx
	cmpl	%r8d, %edx
	jl	.LBB1_9
	.p2align	4, 0x90
.LBB1_40:                               # %._crit_edge60
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	8(%rsp), %ecx           # 4-byte Reload
	incl	%ecx
	movl	4(%rsp), %edx           # 4-byte Reload
	addl	40(%rsp), %edx          # 4-byte Folded Reload
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	cmpl	68(%rsp), %ecx          # 4-byte Folded Reload
	jl	.LBB1_2
.LBB1_41:                               # %._crit_edge64
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	HorzVariance, .Lfunc_end1-HorzVariance
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.zero	16
	.text
	.globl	BuildConnectedComponents
	.p2align	4, 0x90
	.type	BuildConnectedComponents,@function
BuildConnectedComponents:               # @BuildConnectedComponents
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 128
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r12
	movl	32(%r12), %ebx
	movl	36(%r12), %r14d
	movl	%ebx, %eax
	imull	%r14d, %eax
	movslq	%eax, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, 88(%r12)
	testl	%ebx, %ebx
	jle	.LBB2_1
# BB#2:                                 # %.preheader166.lr.ph
	cvtsi2ssl	%ebp, %xmm0
	movss	%xmm0, 56(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader166
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_9 Depth 2
                                        #       Child Loop BB2_18 Depth 3
                                        #       Child Loop BB2_28 Depth 3
                                        #         Child Loop BB2_31 Depth 4
                                        #         Child Loop BB2_36 Depth 4
                                        #         Child Loop BB2_41 Depth 4
	leal	-1(%r14), %eax
	cmpl	$1, %eax
	jle	.LBB2_4
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	-1(%rax), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	1(%rax), %rcx
	movl	$1, %esi
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB2_9
.LBB2_19:                               #   in Loop: Header=BB2_9 Depth=2
	movl	$80, %edi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbp, (%rsp)
	testq	%r13, %r13
	je	.LBB2_20
# BB#21:                                #   in Loop: Header=BB2_9 Depth=2
	movl	(%r13), %eax
	incl	%eax
	jmp	.LBB2_22
.LBB2_20:                               #   in Loop: Header=BB2_9 Depth=2
	movl	$1, %eax
.LBB2_22:                               #   in Loop: Header=BB2_9 Depth=2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%eax, (%rbp)
	movl	$0, 4(%rbp)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, 8(%rbp)
	movl	%edx, 12(%rbp)
	movl	%ecx, 16(%rbp)
	movl	%edx, 20(%rbp)
	movl	%ecx, 24(%rbp)
	movl	%ecx, 28(%rbp)
	movl	%ecx, 32(%rbp)
	movq	$0, 36(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbp)
	movb	$0, 64(%rbp)
	movq	%r13, 72(%rbp)
	movq	%rbp, %r13
	jmp	.LBB2_23
	.p2align	4, 0x90
.LBB2_9:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_18 Depth 3
                                        #       Child Loop BB2_28 Depth 3
                                        #         Child Loop BB2_31 Depth 4
                                        #         Child Loop BB2_36 Depth 4
                                        #         Child Loop BB2_41 Depth 4
	movl	%r14d, %r15d
	imull	8(%rsp), %r15d          # 4-byte Folded Reload
	addl	%esi, %r15d
	testl	%ebp, %ebp
	js	.LBB2_14
# BB#10:                                #   in Loop: Header=BB2_9 Depth=2
	movq	80(%r12), %rax
	movslq	%r15d, %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	56(%rsp), %xmm0         # 4-byte Folded Reload
	jne	.LBB2_14
	jp	.LBB2_14
# BB#11:                                # %..loopexit_crit_edge
                                        #   in Loop: Header=BB2_9 Depth=2
	incq	%rsi
	movq	%rsi, %rdx
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_9 Depth=2
	movq	88(%r12), %rbx
	movslq	%r14d, %rcx
	movq	%rcx, %rdx
	imulq	64(%rsp), %rdx          # 8-byte Folded Reload
	leaq	(%rsi,%rdx), %rax
	cmpl	$0, -4(%rbx,%rax,4)
	je	.LBB2_49
# BB#15:                                #   in Loop: Header=BB2_9 Depth=2
	leaq	-1(%rsi,%rdx), %rax
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_49:                               #   in Loop: Header=BB2_9 Depth=2
	cmpl	$0, (%rbx,%rax,4)
	jne	.LBB2_16
# BB#50:                                #   in Loop: Header=BB2_9 Depth=2
	leaq	1(%rsi,%rdx), %rax
	.p2align	4, 0x90
.LBB2_16:                               #   in Loop: Header=BB2_9 Depth=2
	cltq
	movl	(%rbx,%rax,4), %eax
	testl	%eax, %eax
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	jne	.LBB2_17
# BB#45:                                #   in Loop: Header=BB2_9 Depth=2
	imulq	8(%rsp), %rcx           # 8-byte Folded Reload
	leaq	(%rsi,%rcx), %rax
	cmpl	$0, -4(%rbx,%rax,4)
	je	.LBB2_51
# BB#46:                                #   in Loop: Header=BB2_9 Depth=2
	leaq	-1(%rsi,%rcx), %rax
	jmp	.LBB2_47
.LBB2_51:                               #   in Loop: Header=BB2_9 Depth=2
	cmpl	$0, (%rbx,%rax,4)
	jne	.LBB2_47
# BB#52:                                #   in Loop: Header=BB2_9 Depth=2
	leaq	1(%rsi,%rcx), %rax
	.p2align	4, 0x90
.LBB2_47:                               #   in Loop: Header=BB2_9 Depth=2
	cltq
	movl	(%rbx,%rax,4), %eax
	testl	%eax, %eax
	je	.LBB2_19
.LBB2_17:                               # %.preheader163
                                        #   in Loop: Header=BB2_9 Depth=2
	movq	%rsp, %rcx
	.p2align	4, 0x90
.LBB2_18:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %rbp
	leaq	72(%rbp), %rcx
	cmpl	%eax, (%rbp)
	jne	.LBB2_18
.LBB2_23:                               # %.loopexit164
                                        #   in Loop: Header=BB2_9 Depth=2
	movslq	%r15d, %rcx
	movl	%eax, (%rbx,%rcx,4)
	movl	$16, %edi
	callq	malloc
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%edi, (%rax)
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%esi, 4(%rax)
	movq	48(%rbp), %rcx
	movq	%rcx, 8(%rax)
	movq	%rax, 48(%rbp)
	movl	4(%rbp), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 4(%rbp)
	imull	8(%rbp), %eax
	addl	%esi, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	%ecx
	movl	%eax, 8(%rbp)
	movslq	12(%rbp), %rax
	cmpq	%rax, %rdi
	cmovgl	%edi, %eax
	movl	%eax, 12(%rbp)
	movslq	16(%rbp), %rcx
	cmpq	%rcx, %rsi
	cmovgl	%esi, %ecx
	movl	%ecx, 16(%rbp)
	movslq	20(%rbp), %rcx
	cmpq	%rcx, %rdi
	movl	%ecx, %edx
	cmovll	%edi, %edx
	movl	%edx, 20(%rbp)
	movslq	24(%rbp), %rdx
	cmpq	%rdx, %rsi
	cmovll	%esi, %edx
	cmpq	%rcx, %rdi
	movl	%edx, 24(%rbp)
	jg	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_9 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, 28(%rbp)
.LBB2_25:                               #   in Loop: Header=BB2_9 Depth=2
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	cmpq	%rdi, %rax
	movq	32(%rsp), %rdx          # 8-byte Reload
	jne	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_9 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 32(%rbp)
.LBB2_27:                               #   in Loop: Header=BB2_9 Depth=2
	movq	72(%r12), %rax
	movl	36(%r12), %r14d
	movl	%r14d, %ecx
	imull	64(%rsp), %ecx          # 4-byte Folded Reload
	addl	%edi, %ecx
	movslq	%ecx, %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	36(%rbp), %xmm0
	movss	%xmm0, 36(%rbp)
	movl	%r14d, %ecx
	imull	%edx, %ecx
	addl	%edi, %ecx
	movslq	%ecx, %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	40(%rbp), %xmm0
	movss	%xmm0, 40(%rbp)
	leal	-1(%rdi), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movq	%rdi, %rdx
	incq	%rdx
	movl	$-1, %r13d
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	jmp	.LBB2_28
	.p2align	4, 0x90
.LBB2_48:                               # %..preheader161_crit_edge
                                        #   in Loop: Header=BB2_28 Depth=3
	movq	88(%r12), %rbx
.LBB2_28:                               # %.preheader161
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_31 Depth 4
                                        #         Child Loop BB2_36 Depth 4
                                        #         Child Loop BB2_41 Depth 4
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%r13,%rax), %r15d
	movl	%r14d, %eax
	imull	%r15d, %eax
	addl	60(%rsp), %eax          # 4-byte Folded Reload
	cltq
	movl	(%rbx,%rax,4), %eax
	testl	%eax, %eax
	je	.LBB2_33
# BB#29:                                #   in Loop: Header=BB2_28 Depth=3
	cmpl	(%rbp), %eax
	je	.LBB2_33
# BB#30:                                # %.preheader.preheader
                                        #   in Loop: Header=BB2_28 Depth=3
	movq	%rsp, %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_31:                               # %.preheader
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        #       Parent Loop BB2_28 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rsi, %rbx
	movq	(%rcx), %rsi
	leaq	72(%rsi), %rcx
	cmpl	%eax, (%rsi)
	jne	.LBB2_31
# BB#32:                                #   in Loop: Header=BB2_28 Depth=3
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movq	%rsp, %rcx
	movq	%r12, %r8
	callq	MergeComponents
	testq	%rbx, %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	cmoveq	(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	36(%r12), %r14d
	movq	88(%r12), %rbx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
.LBB2_33:                               #   in Loop: Header=BB2_28 Depth=3
	movl	%r14d, %eax
	imull	%r15d, %eax
	addl	%edi, %eax
	cltq
	movl	(%rbx,%rax,4), %eax
	testl	%eax, %eax
	je	.LBB2_38
# BB#34:                                #   in Loop: Header=BB2_28 Depth=3
	cmpl	(%rbp), %eax
	je	.LBB2_38
# BB#35:                                # %.preheader.1.preheader
                                        #   in Loop: Header=BB2_28 Depth=3
	movq	%rsp, %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_36:                               # %.preheader.1
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        #       Parent Loop BB2_28 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rsi, %rbx
	movq	(%rcx), %rsi
	leaq	72(%rsi), %rcx
	cmpl	%eax, (%rsi)
	jne	.LBB2_36
# BB#37:                                #   in Loop: Header=BB2_28 Depth=3
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movq	%rsp, %rcx
	movq	%r12, %r8
	callq	MergeComponents
	testq	%rbx, %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	cmoveq	(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	36(%r12), %r14d
	movq	88(%r12), %rbx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
.LBB2_38:                               #   in Loop: Header=BB2_28 Depth=3
	imull	%r15d, %r14d
	addl	%edx, %r14d
	movslq	%r14d, %rax
	movl	(%rbx,%rax,4), %eax
	testl	%eax, %eax
	je	.LBB2_43
# BB#39:                                #   in Loop: Header=BB2_28 Depth=3
	cmpl	(%rbp), %eax
	je	.LBB2_43
# BB#40:                                # %.preheader.2.preheader
                                        #   in Loop: Header=BB2_28 Depth=3
	movq	%rsp, %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_41:                               # %.preheader.2
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        #       Parent Loop BB2_28 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rsi, %rbx
	movq	(%rcx), %rsi
	leaq	72(%rsi), %rcx
	cmpl	%eax, (%rsi)
	jne	.LBB2_41
# BB#42:                                #   in Loop: Header=BB2_28 Depth=3
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movq	%rsp, %rcx
	movq	%r12, %r8
	callq	MergeComponents
	testq	%rbx, %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	cmoveq	(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
.LBB2_43:                               #   in Loop: Header=BB2_28 Depth=3
	incl	%r13d
	cmpl	$2, %r13d
	movl	36(%r12), %r14d
	jne	.LBB2_48
# BB#44:                                #   in Loop: Header=BB2_9 Depth=2
	movl	52(%rsp), %ebp          # 4-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB2_12:                               # %.loopexit
                                        #   in Loop: Header=BB2_9 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	-1(%r14), %eax
	cltq
	cmpq	%rax, %rdx
	movq	%rdx, %rsi
	jl	.LBB2_9
# BB#13:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	32(%r12), %ebx
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_4:                                # %.preheader166.._crit_edge_crit_edge
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	incq	%rax
	movq	%rax, %rcx
.LBB2_5:                                # %._crit_edge
                                        #   in Loop: Header=BB2_3 Depth=1
	movslq	%ebx, %rax
	cmpq	%rax, %rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jl	.LBB2_3
# BB#6:                                 # %._crit_edge182.loopexit
	movq	(%rsp), %rax
	jmp	.LBB2_7
.LBB2_1:
                                        # implicit-def: %RAX
.LBB2_7:                                # %._crit_edge182
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	BuildConnectedComponents, .Lfunc_end2-BuildConnectedComponents
	.cfi_endproc

	.globl	MergeComponents
	.p2align	4, 0x90
	.type	MergeComponents,@function
MergeComponents:                        # @MergeComponents
	.cfi_startproc
# BB#0:
	movq	%rdx, %r9
	movq	48(%rsi), %r10
	testq	%r10, %r10
	je	.LBB3_7
# BB#1:                                 # %.lr.ph
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
.Lcfi32:
	.cfi_offset %rbx, -16
	movq	88(%r8), %r11
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdx
	movslq	36(%r8), %rax
	movslq	4(%rdx), %rbx
	imulq	%rax, %rbx
	movslq	(%rdx), %rax
	addq	%rbx, %rax
	movl	(%rdi), %ebx
	movl	%ebx, (%r11,%rax,4)
	movq	8(%rdx), %rax
	testq	%rax, %rax
	jne	.LBB3_2
# BB#3:                                 # %._crit_edge
	movq	48(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%r10, 48(%rdi)
	movl	4(%rdi), %ebx
	addl	4(%rsi), %ebx
	movl	%ebx, 4(%rdi)
	movl	4(%rsi), %eax
	movl	%ebx, %edx
	subl	%eax, %edx
	imull	8(%rdi), %edx
	imull	8(%rsi), %eax
	addl	%edx, %eax
	cltd
	idivl	%ebx
	movl	%eax, 8(%rdi)
	movl	12(%rsi), %eax
	cmpl	12(%rdi), %eax
	movq	%rdi, %rax
	cmovgq	%rsi, %rax
	movl	12(%rax), %eax
	movl	%eax, 12(%rdi)
	movl	16(%rsi), %edx
	cmpl	16(%rdi), %edx
	movq	%rdi, %rdx
	cmovgq	%rsi, %rdx
	movl	16(%rdx), %edx
	movl	%edx, 16(%rdi)
	movl	20(%rsi), %edx
	cmpl	20(%rdi), %edx
	movq	%rdi, %rdx
	cmovlq	%rsi, %rdx
	movl	20(%rdx), %edx
	movl	%edx, 20(%rdi)
	movl	24(%rsi), %ebx
	cmpl	24(%rdi), %ebx
	movq	%rdi, %rbx
	cmovlq	%rsi, %rbx
	movl	24(%rbx), %ebx
	movl	%ebx, 24(%rdi)
	cmpl	20(%rsi), %edx
	popq	%rbx
	jne	.LBB3_5
# BB#4:
	movl	28(%rsi), %edx
	movl	%edx, 28(%rdi)
.LBB3_5:
	cmpl	12(%rsi), %eax
	jne	.LBB3_7
# BB#6:
	movl	32(%rsi), %eax
	movl	%eax, 32(%rdi)
.LBB3_7:                                # %._crit_edge.thread
	testq	%r9, %r9
	leaq	72(%r9), %rax
	movq	72(%rsi), %rdx
	cmovneq	%rax, %rcx
	movq	%rdx, (%rcx)
	movq	%rsi, %rdi
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	MergeComponents, .Lfunc_end3-MergeComponents
	.cfi_endproc

	.globl	EliminateLargeSpreadComponents
	.p2align	4, 0x90
	.type	EliminateLargeSpreadComponents,@function
EliminateLargeSpreadComponents:         # @EliminateLargeSpreadComponents
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 32
.Lcfi36:
	.cfi_offset %rbx, -24
.Lcfi37:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB4_12
.LBB4_1:                                # %.lr.ph34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_2 Depth 2
                                        #       Child Loop BB4_10 Depth 3
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB4_2:                                #   Parent Loop BB4_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_10 Depth 3
	movl	KILL_SMALL_COMP(%rip), %eax
	movl	16(%rdi), %ecx
	subl	24(%rdi), %ecx
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	32(%rbx), %xmm1
	cvtss2sd	%xmm1, %xmm1
	mulsd	VSPREAD_THRESHOLD(%rip), %xmm1
	je	.LBB4_6
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=2
	ucomisd	%xmm1, %xmm0
	jae	.LBB4_6
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=2
	movl	12(%rdi), %eax
	subl	20(%rdi), %eax
	cmpl	SMALL_THRESHOLD(%rip), %eax
	jl	.LBB4_8
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_6:                                #   in Loop: Header=BB4_2 Depth=2
	testl	%eax, %eax
	setne	%al
	ucomisd	%xmm1, %xmm0
	jae	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_2 Depth=2
	testb	%al, %al
	je	.LBB4_5
.LBB4_8:                                #   in Loop: Header=BB4_2 Depth=2
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.LBB4_11
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_2 Depth=2
	movq	88(%rbx), %rcx
	.p2align	4, 0x90
.LBB4_10:                               #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	36(%rbx), %rdx
	movslq	4(%rax), %rsi
	imulq	%rdx, %rsi
	movslq	(%rax), %rdx
	addq	%rsi, %rdx
	movl	$0, (%rcx,%rdx,4)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB4_10
.LBB4_11:                               # %._crit_edge
                                        #   in Loop: Header=BB4_2 Depth=2
	movq	72(%rdi), %rax
	movq	%rax, (%r14)
	callq	free
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	jne	.LBB4_2
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_5:                                # %.outer
                                        #   in Loop: Header=BB4_1 Depth=1
	movss	36(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	40(%rdi), %xmm0
	setb	%al
	movb	%al, 44(%rdi)
	movq	72(%rdi), %rax
	addq	$72, %rdi
	testq	%rax, %rax
	movq	%rdi, %r14
	jne	.LBB4_1
.LBB4_12:                               # %.outer._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	EliminateLargeSpreadComponents, .Lfunc_end4-EliminateLargeSpreadComponents
	.cfi_endproc

	.globl	PrintConnectedComponents
	.p2align	4, 0x90
	.type	PrintConnectedComponents,@function
PrintConnectedComponents:               # @PrintConnectedComponents
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB5_4
# BB#1:                                 # %.lr.ph.preheader
	movl	$.L.str.9, %r15d
	xorl	%ebp, %ebp
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.Lstr.1, %edi
	callq	puts
	movl	(%rbx), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	4(%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	8(%rbx), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movl	12(%rbx), %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	movl	16(%rbx), %esi
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	movl	20(%rbx), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movl	24(%rbx), %esi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	cmpb	$0, 44(%rbx)
	movl	$.L.str.10, %esi
	cmoveq	%r15, %rsi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	addl	4(%rbx), %ebp
	movq	72(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_2
# BB#3:                                 # %._crit_edge
	testq	%r14, %r14
	je	.LBB5_4
# BB#5:
	movl	$.Lstr, %edi
	callq	puts
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB5_4:                                # %._crit_edge.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	PrintConnectedComponents, .Lfunc_end5-PrintConnectedComponents
	.cfi_endproc

	.globl	WriteConnectedComponentsToPGM
	.p2align	4, 0x90
	.type	WriteConnectedComponentsToPGM,@function
WriteConnectedComponentsToPGM:          # @WriteConnectedComponentsToPGM
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 64
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB6_14
# BB#1:
	cmpb	$0, (%r14)
	je	.LBB6_14
# BB#2:
	movl	36(%r14), %ebp
	testl	%ebp, %ebp
	je	.LBB6_14
# BB#3:
	movl	32(%r14), %eax
	testl	%eax, %eax
	je	.LBB6_14
# BB#4:
	movl	WriteConnectedComponentsToPGM.index(%rip), %r12d
	cmpl	$99, %r12d
	jg	.LBB6_14
# BB#5:                                 # %.lr.ph55.preheader
	imull	%ebp, %eax
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph55
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_7 Depth 2
	movq	48(%r15), %rax
	testq	%rax, %rax
	je	.LBB6_9
	.p2align	4, 0x90
.LBB6_7:                                # %.lr.ph51
                                        #   Parent Loop BB6_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rax), %ecx
	imull	%ebp, %ecx
	movslq	(%rax), %rdx
	movslq	%ecx, %rcx
	addq	%rdx, %rcx
	movb	$-1, (%rbx,%rcx)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB6_7
.LBB6_9:                                # %._crit_edge52
                                        #   in Loop: Header=BB6_6 Depth=1
	movq	72(%r15), %r15
	testq	%r15, %r15
	jne	.LBB6_6
# BB#10:                                # %._crit_edge56
	movq	16(%r14), %r13
	movq	%r13, %rdi
	callq	strlen
	leaq	12(%rax), %rdi
	callq	malloc
	movq	%rax, %r15
	leal	1(%r12), %eax
	movl	%eax, WriteConnectedComponentsToPGM.index(%rip)
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r13, %rdx
	movl	%r12d, %ecx
	callq	sprintf
	movq	stdout(%rip), %r12
	movl	$.L.str.14, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	32(%r14), %ecx
	movl	36(%r14), %edx
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movl	$.L.str.16, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	32(%r14), %eax
	imull	36(%r14), %eax
	testl	%eax, %eax
	jle	.LBB6_13
# BB#11:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_12:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rbx,%rbp), %edi
	movq	%r12, %rsi
	callq	fputc
	incq	%rbp
	movslq	32(%r14), %rax
	movslq	36(%r14), %rcx
	imulq	%rax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB6_12
.LBB6_13:                               # %._crit_edge
	movq	%r15, %rdi
	callq	free
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB6_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	WriteConnectedComponentsToPGM, .Lfunc_end6-WriteConnectedComponentsToPGM
	.cfi_endproc

	.globl	FreeConnectedComponents
	.p2align	4, 0x90
	.type	FreeConnectedComponents,@function
FreeConnectedComponents:                # @FreeConnectedComponents
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 32
.Lcfi63:
	.cfi_offset %rbx, -32
.Lcfi64:
	.cfi_offset %r14, -24
.Lcfi65:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB7_4
	.p2align	4, 0x90
.LBB7_1:                                # %.lr.ph22
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
	movq	48(%r15), %rdi
	movq	72(%r15), %r14
	testq	%rdi, %rdi
	je	.LBB7_3
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB7_2
.LBB7_3:                                # %._crit_edge
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	%r15, %rdi
	callq	free
	testq	%r14, %r14
	movq	%r14, %r15
	jne	.LBB7_1
.LBB7_4:                                # %._crit_edge23
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	FreeConnectedComponents, .Lfunc_end7-FreeConnectedComponents
	.cfi_endproc

	.globl	MergeRowComponents
	.p2align	4, 0x90
	.type	MergeRowComponents,@function
MergeRowComponents:                     # @MergeRowComponents
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -32
.Lcfi70:
	.cfi_offset %r14, -24
.Lcfi71:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB8_3
	.p2align	4, 0x90
.LBB8_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	MergeToLeft
	movq	72(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_1
.LBB8_3:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	MergeRowComponents, .Lfunc_end8-MergeRowComponents
	.cfi_endproc

	.globl	MergeToLeft
	.p2align	4, 0x90
	.type	MergeToLeft,@function
MergeToLeft:                            # @MergeToLeft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 80
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %rcx
	movl	SAME_ROW_V(%rip), %eax
	movl	%eax, %r13d
	negl	%r13d
	cmpl	%r13d, %eax
	jl	.LBB9_22
# BB#1:                                 # %.lr.ph90
	movl	20(%rdi), %ebp
	movl	28(%rdi), %r9d
	xorl	%r8d, %r8d
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_5 Depth 2
                                        #       Child Loop BB9_15 Depth 3
	movl	SAME_ROW_H(%rip), %r14d
	testl	%r14d, %r14d
	js	.LBB9_21
# BB#3:                                 # %.lr.ph85
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	%r13d, %r15d
	addl	%r9d, %r15d
	js	.LBB9_21
# BB#4:                                 # %.lr.ph85.split.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	negl	%r14d
	.p2align	4, 0x90
.LBB9_5:                                # %.lr.ph85.split
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_15 Depth 3
	movl	32(%r12), %ebx
	cmpl	%ebx, %r15d
	jge	.LBB9_20
# BB#6:                                 #   in Loop: Header=BB9_5 Depth=2
	movl	%r14d, %esi
	addl	%ebp, %esi
	js	.LBB9_20
# BB#7:                                 #   in Loop: Header=BB9_5 Depth=2
	movl	36(%r12), %eax
	cmpl	%eax, %esi
	jge	.LBB9_20
# BB#8:                                 #   in Loop: Header=BB9_5 Depth=2
	movl	%eax, %edx
	imull	%r15d, %edx
	addl	%esi, %edx
	imull	%eax, %ebx
	cmpl	%ebx, %edx
	jg	.LBB9_20
# BB#9:                                 #   in Loop: Header=BB9_5 Depth=2
	testl	%edx, %edx
	js	.LBB9_20
# BB#10:                                #   in Loop: Header=BB9_5 Depth=2
	movq	88(%r12), %rsi
	movslq	%edx, %rdx
	movl	(%rsi,%rdx,4), %esi
	testl	%esi, %esi
	je	.LBB9_20
# BB#11:                                #   in Loop: Header=BB9_5 Depth=2
	cmpl	%r8d, %esi
	je	.LBB9_20
# BB#12:                                #   in Loop: Header=BB9_5 Depth=2
	cmpl	(%rdi), %esi
	je	.LBB9_20
# BB#13:                                # %.preheader
                                        #   in Loop: Header=BB9_5 Depth=2
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB9_17
# BB#14:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_5 Depth=2
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_15:                               # %.lr.ph
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, %rbx
	cmpl	%esi, (%rbx)
	je	.LBB9_18
# BB#16:                                #   in Loop: Header=BB9_15 Depth=3
	movq	72(%rbx), %rax
	testq	%rax, %rax
	movq	%rbx, %rdx
	jne	.LBB9_15
	jmp	.LBB9_17
.LBB9_18:                               # %.critedge
                                        #   in Loop: Header=BB9_5 Depth=2
	movb	44(%rdi), %al
	cmpb	44(%rbx), %al
	movl	%esi, %r8d
	jne	.LBB9_20
# BB#19:                                #   in Loop: Header=BB9_5 Depth=2
	movq	%rbx, %rsi
	movq	%r12, %r8
	callq	MergeComponents
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	MergeToLeft
	movl	4(%rsp), %r9d           # 4-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	(%rbx), %r8d
	.p2align	4, 0x90
.LBB9_20:                               #   in Loop: Header=BB9_5 Depth=2
	testl	%r14d, %r14d
	leal	1(%r14), %edx
	movl	%edx, %r14d
	js	.LBB9_5
.LBB9_21:                               # %._crit_edge86
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpl	SAME_ROW_V(%rip), %r13d
	leal	1(%r13), %edx
	movl	%edx, %r13d
	jl	.LBB9_2
.LBB9_22:                               # %._crit_edge91
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_17:                               # %.preheader._crit_edge
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end9:
	.size	MergeToLeft, .Lfunc_end9-MergeToLeft
	.cfi_endproc

	.globl	PairComponents
	.p2align	4, 0x90
	.type	PairComponents,@function
PairComponents:                         # @PairComponents
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 96
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.LBB10_49
# BB#1:                                 # %.lr.ph179.lr.ph
	xorl	%r12d, %r12d
.LBB10_2:                               # %.lr.ph179
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_3 Depth 2
                                        #       Child Loop BB10_11 Depth 3
                                        #         Child Loop BB10_14 Depth 4
                                        #           Child Loop BB10_28 Depth 5
                                        #       Child Loop BB10_8 Depth 3
	testq	%r12, %r12
	leaq	72(%r12), %r14
	cmoveq	24(%rsp), %r14          # 8-byte Folded Reload
	movq	%rax, %r12
	movq	%r14, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_3:                               #   Parent Loop BB10_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_11 Depth 3
                                        #         Child Loop BB10_14 Depth 4
                                        #           Child Loop BB10_28 Depth 5
                                        #       Child Loop BB10_8 Depth 3
	cmpq	$0, 56(%r12)
	je	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=2
	movb	$1, %r9b
.LBB10_44:                              #   in Loop: Header=BB10_3 Depth=2
	xorl	%r13d, %r13d
	testb	%r13b, %r13b
	je	.LBB10_46
	jmp	.LBB10_47
	.p2align	4, 0x90
.LBB10_5:                               # %.preheader135
                                        #   in Loop: Header=BB10_3 Depth=2
	xorl	%r8d, %r8d
	cmpl	$0, MAX_CHAR_SIZE(%rip)
	jle	.LBB10_6
# BB#10:                                # %.lr.ph170
                                        #   in Loop: Header=BB10_3 Depth=2
	movl	12(%r12), %ecx
	movl	$1, %r15d
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB10_11:                              #   Parent Loop BB10_2 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_14 Depth 4
                                        #           Child Loop BB10_28 Depth 5
	movl	%r9d, %eax
	orb	%r10b, %al
	jne	.LBB10_40
# BB#12:                                #   in Loop: Header=BB10_11 Depth=3
	movl	20(%r12), %r14d
	cmpl	%ecx, %r14d
	jg	.LBB10_40
# BB#13:                                # %.lr.ph154
                                        #   in Loop: Header=BB10_11 Depth=3
	movl	%r15d, %r13d
	negl	%r13d
	.p2align	4, 0x90
.LBB10_14:                              #   Parent Loop BB10_2 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        #       Parent Loop BB10_11 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB10_28 Depth 5
	testb	%r11b, %r11b
	je	.LBB10_17
# BB#15:                                #   in Loop: Header=BB10_14 Depth=4
	cmpl	20(%r12), %r14d
	je	.LBB10_16
.LBB10_17:                              #   in Loop: Header=BB10_14 Depth=4
	movb	44(%r12), %dl
	testb	%dl, %dl
	movl	%r15d, %ebp
	cmovel	%r13d, %ebp
	movl	8(%r12), %edi
	addl	%ebp, %edi
	js	.LBB10_38
# BB#18:                                #   in Loop: Header=BB10_14 Depth=4
	movl	32(%rsi), %ebx
	cmpl	%ebx, %edi
	jge	.LBB10_38
# BB#19:                                #   in Loop: Header=BB10_14 Depth=4
	movl	36(%rsi), %eax
	imull	%eax, %edi
	addl	%r14d, %edi
	js	.LBB10_38
# BB#20:                                #   in Loop: Header=BB10_14 Depth=4
	imull	%eax, %ebx
	cmpl	%ebx, %edi
	jg	.LBB10_38
# BB#21:                                #   in Loop: Header=BB10_14 Depth=4
	movq	88(%rsi), %rax
	movslq	%edi, %rdi
	movl	(%rax,%rdi,4), %ecx
	testl	%ecx, %ecx
	je	.LBB10_37
# BB#22:                                #   in Loop: Header=BB10_14 Depth=4
	cmpl	%r8d, %ecx
	je	.LBB10_37
# BB#23:                                #   in Loop: Header=BB10_14 Depth=4
	cmpl	(%r12), %ecx
	je	.LBB10_37
# BB#24:                                #   in Loop: Header=BB10_14 Depth=4
	movl	%ebp, %eax
	negl	%eax
	cmovll	%ebp, %eax
	cmpl	MIN_CHAR_SIZE(%rip), %eax
	jge	.LBB10_26
# BB#25:                                #   in Loop: Header=BB10_14 Depth=4
	movb	$1, %r10b
	jmp	.LBB10_37
.LBB10_26:                              # %.preheader
                                        #   in Loop: Header=BB10_14 Depth=4
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB10_28
	jmp	.LBB10_30
	.p2align	4, 0x90
.LBB10_29:                              #   in Loop: Header=BB10_28 Depth=5
	movq	72(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB10_30
.LBB10_28:                              # %.lr.ph
                                        #   Parent Loop BB10_2 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        #       Parent Loop BB10_11 Depth=3
                                        #         Parent Loop BB10_14 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpl	%ecx, (%rbx)
	jne	.LBB10_29
# BB#31:                                # %.critedge128
                                        #   in Loop: Header=BB10_14 Depth=4
	cmpb	44(%rbx), %dl
	jne	.LBB10_33
# BB#32:                                #   in Loop: Header=BB10_14 Depth=4
	movb	$1, %r10b
	jmp	.LBB10_37
.LBB10_33:                              #   in Loop: Header=BB10_14 Depth=4
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	%r11d, 12(%rsp)         # 4-byte Spill
	movl	%r10d, 4(%rsp)          # 4-byte Spill
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movq	%rsi, %rbp
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	Overlap
	testb	%al, %al
	je	.LBB10_34
# BB#35:                                #   in Loop: Header=BB10_14 Depth=4
	movq	%rbx, 56(%r12)
	movq	%r12, 56(%rbx)
	movb	$1, %r9b
	movq	%rbp, %rsi
	movl	4(%rsp), %r10d          # 4-byte Reload
	movl	12(%rsp), %r11d         # 4-byte Reload
	jmp	.LBB10_36
.LBB10_34:                              #   in Loop: Header=BB10_14 Depth=4
	movb	$1, %r11b
	movq	%rbp, %rsi
	movl	8(%rsp), %r9d           # 4-byte Reload
	movl	4(%rsp), %r10d          # 4-byte Reload
.LBB10_36:                              #   in Loop: Header=BB10_14 Depth=4
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB10_37:                              #   in Loop: Header=BB10_14 Depth=4
	movq	88(%rsi), %rax
	movl	(%rax,%rdi,4), %r8d
	movl	12(%r12), %ecx
	.p2align	4, 0x90
.LBB10_38:                              #   in Loop: Header=BB10_14 Depth=4
	movl	%r9d, %eax
	orb	%r10b, %al
	jne	.LBB10_40
# BB#39:                                #   in Loop: Header=BB10_14 Depth=4
	cmpl	%ecx, %r14d
	leal	1(%r14), %eax
	movl	%eax, %r14d
	jl	.LBB10_14
	jmp	.LBB10_40
.LBB10_16:                              #   in Loop: Header=BB10_11 Depth=3
	movb	$1, %r10b
	.p2align	4, 0x90
.LBB10_40:                              # %.critedge1
                                        #   in Loop: Header=BB10_11 Depth=3
	movl	%r9d, %eax
	orb	%r10b, %al
	jne	.LBB10_42
# BB#41:                                # %.critedge1
                                        #   in Loop: Header=BB10_11 Depth=3
	cmpl	MAX_CHAR_SIZE(%rip), %r15d
	leal	1(%r15), %eax
	movl	%eax, %r15d
	jl	.LBB10_11
.LBB10_42:                              # %.critedge
                                        #   in Loop: Header=BB10_3 Depth=2
	testb	%r10b, %r10b
	movq	32(%rsp), %r14          # 8-byte Reload
	jne	.LBB10_7
# BB#43:                                # %.critedge
                                        #   in Loop: Header=BB10_3 Depth=2
	testb	%r9b, %r9b
	jne	.LBB10_44
	jmp	.LBB10_7
.LBB10_6:                               #   in Loop: Header=BB10_3 Depth=2
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
.LBB10_7:                               # %.critedge.thread
                                        #   in Loop: Header=BB10_3 Depth=2
	movl	%r10d, %r13d
	movl	%r9d, %r15d
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	movq	%rsi, %rbp
	je	.LBB10_9
	.p2align	4, 0x90
.LBB10_8:                               # %.lr.ph175
                                        #   Parent Loop BB10_2 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	88(%rbp), %rax
	movslq	36(%rbp), %rcx
	movslq	4(%rdi), %rdx
	imulq	%rcx, %rdx
	movslq	(%rdi), %rcx
	addq	%rdx, %rcx
	movl	$0, (%rax,%rcx,4)
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB10_8
.LBB10_9:                               # %._crit_edge176
                                        #   in Loop: Header=BB10_3 Depth=2
	movq	72(%r12), %rax
	movq	%rax, (%r14)
	movq	%r12, %rdi
	callq	free
	movq	%rbp, %rsi
	movl	%r15d, %r9d
	testb	%r13b, %r13b
	jne	.LBB10_47
.LBB10_46:                              #   in Loop: Header=BB10_3 Depth=2
	testb	%r9b, %r9b
	jne	.LBB10_48
.LBB10_47:                              #   in Loop: Header=BB10_3 Depth=2
	movq	(%r14), %r12
	testq	%r12, %r12
	jne	.LBB10_3
	jmp	.LBB10_49
	.p2align	4, 0x90
.LBB10_48:                              # %.outer
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	72(%r12), %rax
	testq	%rax, %rax
	jne	.LBB10_2
.LBB10_49:                              # %.outer._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_30:                              # %.preheader._crit_edge
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end10:
	.size	PairComponents, .Lfunc_end10-PairComponents
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1056964608              # float 0.5
	.text
	.globl	Overlap
	.p2align	4, 0x90
	.type	Overlap,@function
Overlap:                                # @Overlap
	.cfi_startproc
# BB#0:
	movl	12(%rsi), %eax
	movl	20(%rsi), %ecx
	movl	12(%rdi), %esi
	movl	20(%rdi), %edx
	movl	%esi, %edi
	subl	%ecx, %edi
	movl	%eax, %r9d
	subl	%esi, %r9d
	subl	%edx, %esi
	movl	%eax, %r8d
	subl	%ecx, %r8d
	subl	%edx, %ecx
	subl	%edx, %eax
	jle	.LBB11_6
# BB#1:
	movl	%ecx, %edx
	orl	%edi, %edx
	orl	%r9d, %edx
	js	.LBB11_6
# BB#2:
	cvtsi2ssl	%edi, %xmm0
	jmp	.LBB11_3
.LBB11_6:
	testl	%edi, %edi
	setg	%r10b
	testl	%ecx, %ecx
	setle	%dl
	andb	%r10b, %dl
	testl	%eax, %eax
	js	.LBB11_10
# BB#7:
	testb	%dl, %dl
	je	.LBB11_10
# BB#8:
	testl	%r9d, %r9d
	jg	.LBB11_10
# BB#9:
	cvtsi2ssl	%eax, %xmm0
.LBB11_3:
	cvtsi2ssl	%esi, %xmm1
	movaps	%xmm0, %xmm2
	divss	%xmm1, %xmm2
	ucomiss	.LCPI11_0(%rip), %xmm2
	jae	.LBB11_4
.LBB11_24:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB11_10:
	testl	%r9d, %r9d
	setns	%r10b
	testl	%eax, %eax
	jle	.LBB11_13
# BB#11:
	andb	%r10b, %dl
	je	.LBB11_13
# BB#12:
	cvtsi2ssl	%esi, %xmm0
.LBB11_4:
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r8d, %xmm1
.LBB11_5:
	divss	%xmm1, %xmm0
	ucomiss	.LCPI11_0(%rip), %xmm0
	setae	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB11_13:
	testl	%eax, %eax
	jle	.LBB11_18
# BB#14:
	testl	%edi, %edi
	jle	.LBB11_18
# BB#15:
	testl	%ecx, %ecx
	js	.LBB11_18
# BB#16:
	testl	%r9d, %r9d
	jg	.LBB11_18
# BB#17:
	cvtsi2ssl	%r8d, %xmm0
	cvtsi2ssl	%esi, %xmm1
	jmp	.LBB11_5
.LBB11_18:
	testl	%eax, %eax
	jle	.LBB11_22
# BB#19:
	testl	%edi, %edi
	jns	.LBB11_22
# BB#20:
	testl	%ecx, %ecx
	jle	.LBB11_22
# BB#21:
	testl	%r9d, %r9d
	jg	.LBB11_24
.LBB11_22:
	testl	%edi, %edi
	jle	.LBB11_26
# BB#23:
	andl	%ecx, %r9d
	andl	%eax, %r9d
	js	.LBB11_24
.LBB11_26:
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 16
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end11:
	.size	Overlap, .Lfunc_end11-Overlap
	.cfi_endproc

	.globl	ComputeBoundingBoxes
	.p2align	4, 0x90
	.type	ComputeBoundingBoxes,@function
ComputeBoundingBoxes:                   # @ComputeBoundingBoxes
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 48
.Lcfi104:
	.cfi_offset %rbx, -40
.Lcfi105:
	.cfi_offset %r12, -32
.Lcfi106:
	.cfi_offset %r14, -24
.Lcfi107:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.LBB12_1
	.p2align	4, 0x90
.LBB12_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_7 Depth 2
                                        #     Child Loop BB12_9 Depth 2
	cmpb	$0, 44(%rdi)
	jne	.LBB12_2
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=1
	movl	20(%rdi), %eax
	movq	56(%rdi), %rsi
	cmpl	20(%rsi), %eax
	movq	%rsi, %rax
	cmovlq	%rdi, %rax
	movl	20(%rax), %r8d
	movl	12(%rdi), %ecx
	movl	16(%rdi), %eax
	cmpl	12(%rsi), %ecx
	movq	%rsi, %rcx
	cmovgq	%rdi, %rcx
	movl	12(%rcx), %r11d
	movl	24(%rdi), %edx
	cmpl	24(%rsi), %edx
	movq	%rsi, %rdx
	cmovlq	%rdi, %rdx
	movl	24(%rdx), %edx
	cmpl	16(%rsi), %eax
	cmovgq	%rdi, %rsi
	movl	16(%rsi), %r10d
	cmpl	%r11d, %r8d
	jg	.LBB12_8
# BB#5:                                 # %.lr.ph93.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	imull	36(%rbx), %eax
	addl	%r8d, %eax
	movq	48(%rbx), %r9
	cltq
	movb	$-1, (%r9,%rax)
	movq	56(%rdi), %r9
	movl	36(%rbx), %eax
	imull	24(%r9), %eax
	addl	%r8d, %eax
	movq	48(%rbx), %r9
	cltq
	movb	$-1, (%r9,%rax)
	cmpl	%r11d, %r8d
	jge	.LBB12_8
# BB#6:                                 # %.lr.ph93..lr.ph93_crit_edge.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	leal	1(%r8), %eax
	.p2align	4, 0x90
.LBB12_7:                               # %.lr.ph93..lr.ph93_crit_edge
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	36(%rbx), %esi
	imull	16(%rdi), %esi
	addl	%eax, %esi
	movq	48(%rbx), %rcx
	movslq	%esi, %rsi
	movb	$-1, (%rcx,%rsi)
	movq	56(%rdi), %rcx
	movl	36(%rbx), %esi
	imull	24(%rcx), %esi
	addl	%eax, %esi
	movq	48(%rbx), %rcx
	movslq	%esi, %rsi
	movb	$-1, (%rcx,%rsi)
	cmpl	%r11d, %eax
	leal	1(%rax), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jl	.LBB12_7
.LBB12_8:                               # %.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	cmpl	%r10d, %edx
	jg	.LBB12_2
	.p2align	4, 0x90
.LBB12_9:                               # %.lr.ph95
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	36(%rbx), %eax
	imull	%edx, %eax
	addl	%r8d, %eax
	movq	48(%rbx), %rcx
	cltq
	movb	$-1, (%rcx,%rax)
	movl	36(%rbx), %eax
	imull	%edx, %eax
	addl	%r11d, %eax
	movq	48(%rbx), %rcx
	cltq
	movb	$-1, (%rcx,%rax)
	cmpl	%r10d, %edx
	leal	1(%rdx), %eax
	movl	%eax, %edx
	jl	.LBB12_9
	.p2align	4, 0x90
.LBB12_2:                               # %.sink.split
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB12_3
# BB#10:                                # %._crit_edge98
	movq	16(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	leaq	9(%rax), %rdi
	callq	malloc
	movq	%rax, %r14
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	sprintf
	movq	stdout(%rip), %r15
	movl	$.L.str.14, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	32(%rbx), %ecx
	movl	36(%rbx), %edx
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	$.L.str.16, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	32(%rbx), %eax
	imull	36(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB12_13
# BB#11:                                # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB12_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rax
	movsbl	(%rax,%r12), %edi
	movq	%r15, %rsi
	callq	fputc
	incq	%r12
	movslq	32(%rbx), %rax
	movslq	36(%rbx), %rcx
	imulq	%rax, %rcx
	cmpq	%rcx, %r12
	jl	.LBB12_12
.LBB12_13:                              # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB12_1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	ComputeBoundingBoxes, .Lfunc_end12-ComputeBoundingBoxes
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"component:\t %d\n"
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"# points:\t %d\n"
	.size	.L.str.2, 15

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"average row:\t %d\n"
	.size	.L.str.3, 18

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"x max:\t\t %d\n"
	.size	.L.str.4, 13

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"y max:\t\t %d\n"
	.size	.L.str.5, 13

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"x min:\t\t %d\n"
	.size	.L.str.6, 13

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"y min:\t\t %d\n"
	.size	.L.str.7, 13

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"type:\t\t %s\n"
	.size	.L.str.8, 12

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"falling"
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"rising"
	.size	.L.str.10, 7

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Total points: %d\n"
	.size	.L.str.12, 18

	.type	WriteConnectedComponentsToPGM.index,@object # @WriteConnectedComponentsToPGM.index
	.local	WriteConnectedComponentsToPGM.index
	.comm	WriteConnectedComponentsToPGM.index,4,4
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%s.comp%d.pgm"
	.size	.L.str.13, 14

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"P5\n"
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"%d %d\n"
	.size	.L.str.15, 7

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"255\n"
	.size	.L.str.16, 5

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Unexpected error in PairComponents(). "
	.size	.L.str.18, 39

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Unexpected case or mathematical absurdity reached in Overlap(). "
	.size	.L.str.20, 65

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%s.out.pgm"
	.size	.L.str.22, 11

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"------------------------\n"
	.size	.Lstr, 26

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"------------------------"
	.size	.Lstr.1, 25

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Unexpected error in MergeToLeft(), aborting."
	.size	.Lstr.2, 45

	.type	.Lstr.3,@object         # @str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.3:
	.asciz	"Exiting."
	.size	.Lstr.3, 9

	.type	.Lstr.4,@object         # @str.4
.Lstr.4:
	.asciz	"Twink!"
	.size	.Lstr.4, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
