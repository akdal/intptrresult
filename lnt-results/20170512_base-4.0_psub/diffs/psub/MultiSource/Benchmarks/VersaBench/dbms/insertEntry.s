	.text
	.file	"insertEntry.bc"
	.globl	insertEntry
	.p2align	4, 0x90
	.type	insertEntry,@function
insertEntry:                            # @insertEntry
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %r13
	cmpq	%rbp, (%r13)
	jle	.LBB0_5
# BB#1:
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	chooseEntry
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_8
# BB#2:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	insertEntry
	testq	%rax, %rax
	je	.LBB0_17
# BB#3:
	movq	%rax, %rcx
	decq	%rcx
	cmpq	$2, %rcx
	ja	.LBB0_30
	jmp	.LBB0_31
.LBB0_5:
	movq	8(%r13), %rdx
	testq	%rdx, %rdx
	je	.LBB0_9
# BB#6:                                 # %.lr.ph88.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph88
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rax
	incq	%rcx
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_7
	jmp	.LBB0_10
.LBB0_8:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$insertEntry.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$1, %eax
	jmp	.LBB0_31
.LBB0_9:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB0_10:                               # %._crit_edge89
	cmpq	%r15, %rcx
	jge	.LBB0_12
# BB#11:
	addq	$8, %r13
	testq	%rax, %rax
	leaq	40(%rax), %rax
	cmoveq	%r13, %rax
	movq	%r12, (%rax)
.LBB0_26:                               # %.thread81
	movq	$0, (%r14)
	jmp	.LBB0_30
.LBB0_12:
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	splitNode
	cmpq	$1, %rax
	jne	.LBB0_30
# BB#13:
	cmpq	$0, (%r13)
	je	.LBB0_22
# BB#14:
	movl	$.L.str.2, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$insertEntry.name, %edi
	movl	$1, %esi
	callq	errorMessage
.LBB0_15:                               # %.thread81
	movl	$2, %eax
	jmp	.LBB0_31
.LBB0_17:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$8, %rbx
	movq	%rbx, %rsi
	callq	keysUnion
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB0_30
# BB#18:
	movq	8(%r13), %rdx
	movq	%rdx, (%rsp)
	testq	%rdx, %rdx
	je	.LBB0_24
# BB#19:                                # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rax
	incq	%rcx
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_20
# BB#21:                                # %._crit_edge
	movq	$0, (%rsp)
	cmpq	%r15, %rcx
	jl	.LBB0_25
	jmp	.LBB0_27
.LBB0_22:
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$insertEntry.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$3, %eax
	jmp	.LBB0_31
.LBB0_24:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	cmpq	%r15, %rcx
	jge	.LBB0_27
.LBB0_25:
	movq	%rsi, 40(%rax)
	jmp	.LBB0_26
.LBB0_27:
	movq	%rsp, %rcx
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	splitNode
	cmpq	$1, %rax
	je	.LBB0_15
# BB#28:
	testq	%rax, %rax
	jne	.LBB0_30
# BB#29:
	movq	(%rsp), %rax
	movq	%rax, (%r14)
.LBB0_30:                               # %.thread
	xorl	%eax, %eax
.LBB0_31:                               # %.thread81
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	insertEntry, .Lfunc_end0-insertEntry
	.cfi_endproc

	.type	insertEntry.name,@object # @insertEntry.name
	.data
insertEntry.name:
	.asciz	"insertEntry"
	.size	insertEntry.name, 12

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"can't choose entry on node"
	.size	.L.str, 27

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"can't split LEAF node"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"can't split non-LEAF node"
	.size	.L.str.2, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
