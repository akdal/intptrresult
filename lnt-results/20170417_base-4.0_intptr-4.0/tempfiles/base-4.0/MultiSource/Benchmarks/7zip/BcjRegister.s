	.text
	.file	"BcjRegister.bc"
	.p2align	4, 0x90
	.type	_ZL11CreateCodecv,@function
_ZL11CreateCodecv:                      # @_ZL11CreateCodecv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$24, %edi
	callq	_Znwm
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rax)
	movq	$0, 16(%rax)
	movq	$_ZTV16CBCJ_x86_Decoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end0:
	.size	_ZL11CreateCodecv, .Lfunc_end0-_ZL11CreateCodecv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL14CreateCodecOutv,@function
_ZL14CreateCodecOutv:                   # @_ZL14CreateCodecOutv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movl	$24, %edi
	callq	_Znwm
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rax)
	movq	$0, 16(%rax)
	movq	$_ZTV16CBCJ_x86_Encoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end1:
	.size	_ZL14CreateCodecOutv, .Lfunc_end1-_ZL14CreateCodecOutv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_BcjRegister.ii,@function
_GLOBAL__sub_I_BcjRegister.ii:          # @_GLOBAL__sub_I_BcjRegister.ii
	.cfi_startproc
# BB#0:
	movl	$_ZL11g_CodecInfo, %edi
	jmp	_Z13RegisterCodecPK10CCodecInfo # TAILCALL
.Lfunc_end2:
	.size	_GLOBAL__sub_I_BcjRegister.ii, .Lfunc_end2-_GLOBAL__sub_I_BcjRegister.ii
	.cfi_endproc

	.type	_ZL11g_CodecInfo,@object # @_ZL11g_CodecInfo
	.data
	.p2align	3
_ZL11g_CodecInfo:
	.quad	_ZL11CreateCodecv
	.quad	_ZL14CreateCodecOutv
	.quad	50528515                # 0x3030103
	.quad	.L.str
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.size	_ZL11g_CodecInfo, 40

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	66                      # 0x42
	.long	67                      # 0x43
	.long	74                      # 0x4a
	.long	0                       # 0x0
	.size	.L.str, 16

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_BcjRegister.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
