	.text
	.file	"btSolve2LinearConstraint.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	3212836864              # float -1
.LCPI0_2:
	.long	872415232               # float 1.1920929E-7
.LCPI0_3:
	.long	1065353216              # float 1
	.text
	.globl	_ZN24btSolve2LinearConstraint31resolveUnilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_
	.p2align	4, 0x90
	.type	_ZN24btSolve2LinearConstraint31resolveUnilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_,@function
_ZN24btSolve2LinearConstraint31resolveUnilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_: # @_ZN24btSolve2LinearConstraint31resolveUnilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 272
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movss	%xmm3, 28(%rsp)         # 4-byte Spill
	movss	%xmm2, 24(%rsp)         # 4-byte Spill
	movaps	%xmm0, %xmm3
	movq	%r9, %rbx
	movq	%r8, %r12
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %r15
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	368(%rsp), %rcx
	movq	360(%rsp), %rax
	movq	328(%rsp), %rbp
	movl	$0, (%rax)
	movl	$0, (%rcx)
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movss	8(%rbp), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm4
	addss	%xmm2, %xmm4
	xorps	%xmm0, %xmm0
	sqrtss	%xmm4, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB0_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm4, %xmm0
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB0_2:                                # %.split
	movaps	.LCPI0_0(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm0
	addss	.LCPI0_1(%rip), %xmm0
	andps	%xmm2, %xmm0
	ucomiss	.LCPI0_2(%rip), %xmm0
	jae	.LBB0_4
# BB#3:
	movq	320(%rsp), %r8
	movq	288(%rsp), %rcx
	leaq	128(%rsp), %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%rbp, %r9
	movaps	%xmm3, %xmm0
	pushq	296(%rsp)
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	movss	%xmm3, 24(%rsp)         # 4-byte Spill
	movss	%xmm1, 20(%rsp)         # 4-byte Spill
	callq	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	leaq	40(%rsp), %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	336(%rsp), %rcx
	movq	344(%rsp), %r8
	movq	352(%rsp), %r9
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	pushq	296(%rsp)
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	movss	348(%r15), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movq	288(%rsp), %rax
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm7
	mulss	%xmm4, %xmm7
	movss	352(%r15), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	(%rax), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm11, %xmm0
	subss	%xmm0, %xmm7
	movaps	%xmm5, %xmm2
	mulss	%xmm12, %xmm2
	movss	344(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm6
	mulss	%xmm3, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm11, %xmm8
	mulss	%xmm3, %xmm8
	movaps	%xmm9, %xmm6
	mulss	%xmm12, %xmm6
	subss	%xmm6, %xmm8
	movss	348(%r13), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm6
	mulss	%xmm10, %xmm6
	movss	352(%r13), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm1
	mulss	%xmm13, %xmm1
	subss	%xmm1, %xmm6
	movaps	%xmm12, %xmm1
	mulss	%xmm13, %xmm1
	movss	344(%r13), %xmm14       # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm4
	subss	%xmm4, %xmm1
	mulss	%xmm14, %xmm11
	mulss	%xmm10, %xmm12
	subss	%xmm12, %xmm11
	movss	328(%r15), %xmm15       # xmm15 = mem[0],zero,zero,zero
	addss	%xmm15, %xmm7
	movss	328(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	addss	%xmm0, %xmm6
	subss	%xmm6, %xmm7
	movss	332(%r15), %xmm12       # xmm12 = mem[0],zero,zero,zero
	addss	%xmm12, %xmm2
	movss	332(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	addss	%xmm0, %xmm1
	subss	%xmm1, %xmm2
	movss	336(%r15), %xmm6        # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm8
	movss	336(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	addss	%xmm0, %xmm11
	subss	%xmm11, %xmm8
	mulss	(%rbp), %xmm7
	mulss	4(%rbp), %xmm2
	addss	%xmm7, %xmm2
	mulss	8(%rbp), %xmm8
	addss	%xmm2, %xmm8
	movq	336(%rsp), %rax
	movss	8(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm2, %xmm1
	movss	4(%rax), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm7, %xmm4
	subss	%xmm4, %xmm1
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	movaps	%xmm3, %xmm0
	mulss	%xmm2, %xmm0
	subss	%xmm0, %xmm5
	mulss	%xmm7, %xmm3
	mulss	%xmm4, %xmm9
	subss	%xmm9, %xmm3
	addss	%xmm15, %xmm1
	addss	%xmm12, %xmm5
	addss	%xmm6, %xmm3
	movaps	%xmm10, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm13, %xmm6
	mulss	%xmm7, %xmm6
	subss	%xmm6, %xmm0
	mulss	%xmm4, %xmm13
	mulss	%xmm14, %xmm2
	subss	%xmm2, %xmm13
	mulss	%xmm7, %xmm14
	mulss	%xmm4, %xmm10
	subss	%xmm10, %xmm14
	addss	20(%rsp), %xmm0         # 4-byte Folded Reload
	addss	16(%rsp), %xmm13        # 4-byte Folded Reload
	addss	12(%rsp), %xmm14        # 4-byte Folded Reload
	subss	%xmm0, %xmm1
	subss	%xmm13, %xmm5
	subss	%xmm14, %xmm3
	movq	352(%rsp), %rax
	mulss	(%rax), %xmm1
	mulss	4(%rax), %xmm5
	addss	%xmm1, %xmm5
	mulss	8(%rax), %xmm3
	addss	%xmm5, %xmm3
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm0
	movss	.LCPI0_3(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	divss	%xmm0, %xmm1
	movq	32(%rsp), %rax          # 8-byte Reload
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm12
	mulss	%xmm1, %xmm12
	mulss	%xmm4, %xmm8
	subss	%xmm8, %xmm12
	movss	28(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	mulss	%xmm1, %xmm7
	mulss	%xmm4, %xmm3
	subss	%xmm3, %xmm7
	movss	128(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	132(%rsp), %xmm11       # xmm11 = mem[0],zero,zero,zero
	mulss	40(%rsp), %xmm1
	mulss	44(%rsp), %xmm11
	movss	136(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	48(%rsp), %xmm4
	movss	176(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	56(%rsp), %xmm3
	movss	180(%rsp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	mulss	60(%rsp), %xmm8
	movss	184(%rsp), %xmm10       # xmm10 = mem[0],zero,zero,zero
	mulss	64(%rsp), %xmm10
	movaps	%xmm1, %xmm2
	mulss	%xmm5, %xmm2
	movaps	%xmm11, %xmm0
	mulss	%xmm5, %xmm0
	mulss	%xmm4, %xmm5
	movaps	%xmm5, %xmm13
	movaps	%xmm6, %xmm5
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm11
	mulss	%xmm5, %xmm4
	movss	192(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	72(%rsp), %xmm5
	addss	%xmm3, %xmm5
	movss	196(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	76(%rsp), %xmm3
	addss	%xmm8, %xmm3
	movss	200(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	80(%rsp), %xmm6
	addss	%xmm10, %xmm6
	addss	%xmm2, %xmm5
	addss	%xmm0, %xmm3
	addss	%xmm13, %xmm6
	addss	%xmm1, %xmm5
	addss	%xmm11, %xmm3
	addss	%xmm4, %xmm6
	addss	%xmm5, %xmm3
	addss	%xmm6, %xmm3
	movss	208(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	120(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	mulss	%xmm0, %xmm2
	mulss	%xmm1, %xmm0
	movaps	%xmm3, %xmm4
	mulss	%xmm4, %xmm4
	subss	%xmm4, %xmm0
	divss	%xmm0, %xmm9
	mulss	%xmm9, %xmm2
	movaps	%xmm7, %xmm0
	mulss	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	subss	%xmm0, %xmm2
	movq	360(%rsp), %rax
	movss	%xmm2, (%rax)
	mulss	%xmm1, %xmm7
	mulss	%xmm12, %xmm3
	mulss	%xmm9, %xmm7
	mulss	%xmm9, %xmm3
	subss	%xmm3, %xmm7
	movq	368(%rsp), %rax
	movss	%xmm7, (%rax)
.LBB0_4:
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN24btSolve2LinearConstraint31resolveUnilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_, .Lfunc_end0-_ZN24btSolve2LinearConstraint31resolveUnilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_
	.cfi_endproc

	.section	.text._ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,"axG",@progbits,_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,comdat
	.weak	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.p2align	4, 0x90
	.type	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,@function
_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f: # @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_startproc
# BB#0:
	movq	16(%rsp), %r10
	movq	8(%rsp), %rax
	movups	(%r9), %xmm2
	movups	%xmm2, (%rdi)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm11, %xmm7
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm9, %xmm5
	movaps	%xmm11, %xmm4
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm5
	mulss	%xmm10, %xmm2
	mulss	%xmm9, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1]
	mulps	%xmm4, %xmm6
	movss	20(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm6, %xmm4
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm12   # xmm12 = xmm12[0],xmm8[0],xmm12[1],xmm8[1]
	mulps	%xmm3, %xmm12
	addps	%xmm4, %xmm12
	mulss	32(%rsi), %xmm7
	mulss	36(%rsi), %xmm5
	addss	%xmm7, %xmm5
	mulss	40(%rsi), %xmm2
	addss	%xmm5, %xmm2
	xorps	%xmm8, %xmm8
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm12, 16(%rdi)
	movlps	%xmm3, 24(%rdi)
	movss	(%r8), %xmm3            # xmm3 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm11, %xmm5
	movss	8(%r8), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm6
	mulss	%xmm10, %xmm6
	subss	%xmm5, %xmm6
	mulss	%xmm9, %xmm7
	mulss	%xmm3, %xmm11
	subss	%xmm7, %xmm11
	mulss	%xmm10, %xmm3
	mulss	%xmm9, %xmm4
	subss	%xmm3, %xmm4
	movss	16(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	movss	20(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0],xmm9[1],xmm5[1]
	movaps	%xmm11, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm9, %xmm7
	addps	%xmm3, %xmm7
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rdx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1]
	mulps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	mulss	32(%rdx), %xmm6
	mulss	36(%rdx), %xmm11
	addss	%xmm6, %xmm11
	mulss	40(%rdx), %xmm4
	addss	%xmm11, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm5, 32(%rdi)
	movlps	%xmm3, 40(%rdi)
	movsd	(%rax), %xmm9           # xmm9 = mem[0],zero
	mulps	%xmm12, %xmm9
	mulss	8(%rax), %xmm2
	xorps	%xmm6, %xmm6
	movss	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1,2,3]
	movlps	%xmm9, 48(%rdi)
	movlps	%xmm6, 56(%rdi)
	movsd	(%r10), %xmm6           # xmm6 = mem[0],zero
	mulps	%xmm5, %xmm6
	mulss	8(%r10), %xmm4
	movaps	%xmm6, %xmm7
	movlps	%xmm6, 64(%rdi)
	mulss	%xmm5, %xmm6
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movss	%xmm4, %xmm8            # xmm8 = xmm4[0],xmm8[1,2,3]
	movlps	%xmm8, 72(%rdi)
	movss	16(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	20(%rdi), %xmm9
	addss	%xmm3, %xmm9
	mulss	24(%rdi), %xmm2
	addss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm5, %xmm7
	addss	%xmm6, %xmm7
	mulss	40(%rdi), %xmm4
	addss	%xmm7, %xmm4
	addss	%xmm2, %xmm4
	movss	%xmm4, 80(%rdi)
	retq
.Lfunc_end1:
	.size	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f, .Lfunc_end1-_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI2_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_1:
	.long	3212836864              # float -1
.LCPI2_2:
	.long	872415232               # float 1.1920929E-7
.LCPI2_3:
	.long	1065353216              # float 1
	.text
	.globl	_ZN24btSolve2LinearConstraint30resolveBilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_
	.p2align	4, 0x90
	.type	_ZN24btSolve2LinearConstraint30resolveBilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_,@function
_ZN24btSolve2LinearConstraint30resolveBilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_: # @_ZN24btSolve2LinearConstraint30resolveBilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 288
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movaps	%xmm3, 208(%rsp)        # 16-byte Spill
	movaps	%xmm2, 192(%rsp)        # 16-byte Spill
	movaps	%xmm0, %xmm3
	movq	%r9, %r15
	movq	%r8, %r12
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	384(%rsp), %rcx
	movq	376(%rsp), %rdx
	movq	344(%rsp), %rax
	movl	$0, (%rdx)
	movl	$0, (%rcx)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm4
	addss	%xmm2, %xmm4
	xorps	%xmm0, %xmm0
	sqrtss	%xmm4, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB2_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm4, %xmm0
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	movss	%xmm1, (%rsp)           # 4-byte Spill
	callq	sqrtf
	movq	344(%rsp), %rax
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB2_2:                                # %.split
	movaps	.LCPI2_0(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm0
	addss	.LCPI2_1(%rip), %xmm0
	andps	%xmm2, %xmm0
	ucomiss	.LCPI2_2(%rip), %xmm0
	jae	.LBB2_8
# BB#3:
	movq	336(%rsp), %r8
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	304(%rsp), %rbp
	leaq	104(%rsp), %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%rbp, %rcx
	movq	%rax, %r9
	movaps	%xmm3, %xmm0
	pushq	312(%rsp)
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	movss	%xmm3, 20(%rsp)         # 4-byte Spill
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
	callq	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	addq	$16, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset -16
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	352(%rsp), %rcx
	movq	360(%rsp), %r8
	movq	368(%rsp), %r9
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	pushq	312(%rsp)
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	addq	$16, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -16
	movss	348(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	8(%rbp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movq	352(%rsp), %rdx
	movss	8(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movaps	%xmm0, %xmm6
	mulps	%xmm3, %xmm6
	movss	352(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	(%rbp), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movq	384(%rsp), %rcx
	movq	376(%rsp), %rax
	movss	(%rdx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm9    # xmm9 = xmm9[0],xmm7[0],xmm9[1],xmm7[1]
	movaps	%xmm1, %xmm7
	mulps	%xmm9, %xmm7
	subps	%xmm7, %xmm6
	unpcklps	%xmm2, %xmm8    # xmm8 = xmm8[0],xmm2[0],xmm8[1],xmm2[1]
	mulps	%xmm8, %xmm1
	movss	344(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movaps	%xmm7, %xmm2
	mulps	%xmm3, %xmm2
	subps	%xmm2, %xmm1
	mulps	%xmm9, %xmm7
	mulps	%xmm8, %xmm0
	subps	%xmm0, %xmm7
	movss	328(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	332(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	addps	%xmm6, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	addps	%xmm1, %xmm0
	movss	336(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	addps	%xmm7, %xmm1
	movss	348(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movaps	%xmm6, %xmm7
	mulps	%xmm3, %xmm7
	movss	352(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movaps	%xmm5, %xmm4
	mulps	%xmm9, %xmm4
	subps	%xmm4, %xmm7
	mulps	%xmm8, %xmm5
	movss	344(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm4, %xmm3
	subps	%xmm3, %xmm5
	mulps	%xmm9, %xmm4
	mulps	%xmm8, %xmm6
	subps	%xmm6, %xmm4
	movss	328(%r13), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	332(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	addps	%xmm7, %xmm3
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	addps	%xmm5, %xmm6
	movss	336(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	addps	%xmm4, %xmm5
	subps	%xmm3, %xmm2
	subps	%xmm6, %xmm0
	subps	%xmm5, %xmm1
	movq	344(%rsp), %rdx
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movq	%rdx, %rsi
	movq	368(%rsp), %rdx
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulps	%xmm2, %xmm5
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm0, %xmm6
	addps	%xmm5, %xmm6
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	mulps	%xmm1, %xmm2
	addps	%xmm6, %xmm2
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	unpcklps	192(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movq	8(%rsp), %rdx           # 8-byte Reload
	movss	(%rdx), %xmm11          # xmm11 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm0, %xmm11
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	subps	%xmm1, %xmm11
	movss	104(%rsp), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movss	108(%rsp), %xmm12       # xmm12 = mem[0],zero,zero,zero
	mulss	16(%rsp), %xmm13
	mulss	20(%rsp), %xmm12
	movss	112(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	24(%rsp), %xmm4
	movss	152(%rsp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	mulss	32(%rsp), %xmm8
	movss	156(%rsp), %xmm9        # xmm9 = mem[0],zero,zero,zero
	mulss	36(%rsp), %xmm9
	movss	160(%rsp), %xmm10       # xmm10 = mem[0],zero,zero,zero
	mulss	40(%rsp), %xmm10
	movss	168(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	48(%rsp), %xmm5
	movss	172(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	52(%rsp), %xmm1
	movss	176(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	56(%rsp), %xmm6
	movaps	%xmm13, %xmm3
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movaps	%xmm12, %xmm2
	mulss	%xmm0, %xmm2
	mulss	%xmm4, %xmm0
	movss	(%rsp), %xmm7           # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm13
	mulss	%xmm7, %xmm12
	mulss	%xmm7, %xmm4
	addss	%xmm8, %xmm5
	addss	%xmm9, %xmm1
	addss	%xmm10, %xmm6
	addss	%xmm3, %xmm5
	addss	%xmm2, %xmm1
	addss	%xmm0, %xmm6
	addss	%xmm13, %xmm5
	addss	%xmm12, %xmm1
	addss	%xmm4, %xmm6
	addss	%xmm5, %xmm1
	addss	%xmm6, %xmm1
	movss	184(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	mulss	%xmm4, %xmm0
	movaps	%xmm1, %xmm3
	mulss	%xmm3, %xmm3
	subss	%xmm3, %xmm0
	movss	.LCPI2_3(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm6
	movaps	%xmm11, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm3, %xmm0
	mulss	%xmm2, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm11, %xmm5
	mulss	%xmm1, %xmm5
	xorps	.LCPI2_4(%rip), %xmm1
	mulss	%xmm6, %xmm5
	subss	%xmm5, %xmm0
	movss	%xmm0, (%rax)
	movaps	%xmm4, %xmm0
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	mulps	%xmm11, %xmm0
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm0, %xmm6
	movaps	%xmm6, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	addss	%xmm6, %xmm5
	movss	%xmm5, (%rcx)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB2_5
# BB#4:
	ucomiss	%xmm1, %xmm5
	jbe	.LBB2_6
	jmp	.LBB2_8
.LBB2_5:
	movl	$0, (%rax)
	divss	%xmm4, %xmm11
	movss	%xmm11, (%rcx)
	ucomiss	%xmm11, %xmm1
	jb	.LBB2_8
.LBB2_6:
	movl	$0, (%rcx)
	divss	%xmm2, %xmm3
	movss	%xmm3, (%rax)
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm3
	ja	.LBB2_8
# BB#7:
	movl	$0, (%rax)
.LBB2_8:
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN24btSolve2LinearConstraint30resolveBilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_, .Lfunc_end2-_ZN24btSolve2LinearConstraint30resolveBilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
