	.text
	.file	"btConeShape.bc"
	.globl	_ZN11btConeShapeC2Eff
	.p2align	4, 0x90
	.type	_ZN11btConeShapeC2Eff,@function
_ZN11btConeShapeC2Eff:                  # @_ZN11btConeShapeC2Eff
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movq	$_ZTV11btConeShape+16, (%rbx)
	movss	%xmm1, 68(%rbx)
	movss	%xmm2, 72(%rbx)
	movl	$11, 8(%rbx)
	movl	$0, 76(%rbx)
	movl	$1, 80(%rbx)
	movl	$2, 84(%rbx)
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB0_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm2, %xmm0
	callq	sqrtf
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
.LBB0_2:                                # %.split
	divss	%xmm0, %xmm1
	movss	%xmm1, 64(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN11btConeShapeC2Eff, .Lfunc_end0-_ZN11btConeShapeC2Eff
	.cfi_endproc

	.globl	_ZN11btConeShape14setConeUpIndexEi
	.p2align	4, 0x90
	.type	_ZN11btConeShape14setConeUpIndexEi,@function
_ZN11btConeShape14setConeUpIndexEi:     # @_ZN11btConeShape14setConeUpIndexEi
	.cfi_startproc
# BB#0:
	cmpl	$2, %esi
	ja	.LBB1_2
# BB#1:                                 # %switch.lookup
	movslq	%esi, %rax
	movl	.Lswitch.table(,%rax,4), %ecx
	movl	.Lswitch.table.1(,%rax,4), %eax
	movl	%ecx, 76(%rdi)
	movl	%esi, 80(%rdi)
	movl	%eax, 84(%rdi)
.LBB1_2:
	retq
.Lfunc_end1:
	.size	_ZN11btConeShape14setConeUpIndexEi, .Lfunc_end1-_ZN11btConeShape14setConeUpIndexEi
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN12btConeShapeZC2Eff
	.p2align	4, 0x90
	.type	_ZN12btConeShapeZC2Eff,@function
_ZN12btConeShapeZC2Eff:                 # @_ZN12btConeShapeZC2Eff
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -16
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movq	$_ZTV11btConeShape+16, (%rbx)
	movss	%xmm1, 68(%rbx)
	movss	%xmm2, 72(%rbx)
	movl	$11, 8(%rbx)
	movl	$0, 76(%rbx)
	movl	$1, 80(%rbx)
	movl	$2, 84(%rbx)
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm2, %xmm0
	callq	sqrtf
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
.LBB3_2:                                # %.split
	divss	%xmm0, %xmm1
	movss	%xmm1, 64(%rbx)
	movq	$_ZTV12btConeShapeZ+16, (%rbx)
	movabsq	$8589934592, %rax       # imm = 0x200000000
	movq	%rax, 76(%rbx)
	movl	$1, 84(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZN12btConeShapeZC2Eff, .Lfunc_end3-_ZN12btConeShapeZC2Eff
	.cfi_endproc

	.globl	_ZN12btConeShapeXC2Eff
	.p2align	4, 0x90
	.type	_ZN12btConeShapeXC2Eff,@function
_ZN12btConeShapeXC2Eff:                 # @_ZN12btConeShapeXC2Eff
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -16
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movq	$_ZTV11btConeShape+16, (%rbx)
	movss	%xmm1, 68(%rbx)
	movss	%xmm2, 72(%rbx)
	movl	$11, 8(%rbx)
	movl	$0, 76(%rbx)
	movl	$1, 80(%rbx)
	movl	$2, 84(%rbx)
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB4_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm2, %xmm0
	callq	sqrtf
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
.LBB4_2:                                # %.split
	divss	%xmm0, %xmm1
	movss	%xmm1, 64(%rbx)
	movq	$_ZTV12btConeShapeX+16, (%rbx)
	movq	$1, 76(%rbx)
	movl	$2, 84(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN12btConeShapeXC2Eff, .Lfunc_end4-_ZN12btConeShapeXC2Eff
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1056964608              # float 0.5
.LCPI5_1:
	.long	872415232               # float 1.1920929E-7
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK11btConeShape16coneLocalSupportERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK11btConeShape16coneLocalSupportERK9btVector3,@function
_ZNK11btConeShape16coneLocalSupportERK9btVector3: # @_ZNK11btConeShape16coneLocalSupportERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 80
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movss	72(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movslq	80(%rbx), %rax
	movss	(%r14,%rax,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm3, 32(%rsp)         # 16-byte Spill
	movss	%xmm4, 28(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	28(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm3         # 16-byte Reload
.LBB5_2:                                # %.split
	mulss	.LCPI5_0(%rip), %xmm3
	mulss	64(%rbx), %xmm0
	movslq	76(%rbx), %rax
	ucomiss	%xmm0, %xmm4
	jbe	.LBB5_4
# BB#3:
	movl	$0, 8(%rsp,%rax,4)
	movslq	80(%rbx), %rax
	movss	%xmm3, 8(%rsp,%rax,4)
	addq	$84, %rbx
	xorps	%xmm1, %xmm1
	movq	%rbx, %rax
	jmp	.LBB5_9
.LBB5_4:
	movss	(%r14,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movslq	84(%rbx), %rax
	movss	(%r14,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_6
# BB#5:                                 # %call.sqrt30
	movaps	%xmm1, %xmm0
	movaps	%xmm3, 32(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	32(%rsp), %xmm3         # 16-byte Reload
.LBB5_6:                                # %.split29
	leaq	84(%rbx), %rax
	ucomiss	.LCPI5_1(%rip), %xmm0
	jbe	.LBB5_8
# BB#7:
	movss	68(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movslq	76(%rbx), %rcx
	movss	(%r14,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	%xmm0, 8(%rsp,%rcx,4)
	xorps	.LCPI5_2(%rip), %xmm3
	movslq	80(%rbx), %rcx
	movss	%xmm3, 8(%rsp,%rcx,4)
	movslq	84(%rbx), %rcx
	mulss	(%r14,%rcx,4), %xmm1
	jmp	.LBB5_9
.LBB5_8:
	movslq	76(%rbx), %rcx
	movl	$0, 8(%rsp,%rcx,4)
	xorps	.LCPI5_2(%rip), %xmm3
	movslq	80(%rbx), %rcx
	movss	%xmm3, 8(%rsp,%rcx,4)
	xorps	%xmm1, %xmm1
.LBB5_9:
	movslq	(%rax), %rax
	movss	%xmm1, 8(%rsp,%rax,4)
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%rsp), %xmm1         # xmm1 = mem[0],zero
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZNK11btConeShape16coneLocalSupportERK9btVector3, .Lfunc_end5-_ZNK11btConeShape16coneLocalSupportERK9btVector3
	.cfi_endproc

	.globl	_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	jmp	_ZNK11btConeShape16coneLocalSupportERK9btVector3 # TAILCALL
.Lfunc_end6:
	.size	_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end6-_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.globl	_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -40
.Lcfi20:
	.cfi_offset %r12, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	testl	%ecx, %ecx
	jle	.LBB7_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%ecx, %r12d
	addq	$8, %rbx
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZNK11btConeShape16coneLocalSupportERK9btVector3
	movlps	%xmm0, -8(%rbx)
	movlps	%xmm1, (%rbx)
	addq	$16, %r14
	addq	$16, %rbx
	decq	%r12
	jne	.LBB7_2
.LBB7_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end7-_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	679477248               # float 1.42108547E-14
.LCPI8_2:
	.long	3212836864              # float -1
.LCPI8_3:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_1:
	.long	3212836864              # float -1
	.long	3212836864              # float -1
	.zero	4
	.zero	4
	.text
	.globl	_ZNK11btConeShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK11btConeShape24localGetSupportingVertexERK9btVector3,@function
_ZNK11btConeShape24localGetSupportingVertexERK9btVector3: # @_ZNK11btConeShape24localGetSupportingVertexERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 96
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	_ZNK11btConeShape16coneLocalSupportERK9btVector3
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB8_2
	jnp	.LBB8_1
.LBB8_2:
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI8_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm2
	seta	%al
	movd	%eax, %xmm1
	pshufd	$80, %xmm1, %xmm2       # xmm2 = xmm1[0,0,1,1]
	pslld	$31, %xmm2
	psrad	$31, %xmm2
	movdqa	%xmm2, %xmm1
	pandn	%xmm0, %xmm1
	pand	.LCPI8_1(%rip), %xmm2
	por	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movdqa	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	jbe	.LBB8_4
# BB#3:
	movss	.LCPI8_2(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
.LBB8_4:
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_6
# BB#5:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movdqa	%xmm2, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movdqa	(%rsp), %xmm2           # 16-byte Reload
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB8_6:                                # %.split
	movss	.LCPI8_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	movaps	(%rsp), %xmm2           # 16-byte Reload
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	addps	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm2, %xmm1
	jmp	.LBB8_7
.LBB8_1:
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	32(%rsp), %xmm1         # 16-byte Reload
.LBB8_7:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	_ZNK11btConeShape24localGetSupportingVertexERK9btVector3, .Lfunc_end8-_ZNK11btConeShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc

	.section	.text._ZN11btConeShapeD0Ev,"axG",@progbits,_ZN11btConeShapeD0Ev,comdat
	.weak	_ZN11btConeShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN11btConeShapeD0Ev,@function
_ZN11btConeShapeD0Ev:                   # @_ZN11btConeShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB9_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN11btConeShapeD0Ev, .Lfunc_end9-_ZN11btConeShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end10:
	.size	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end10-_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end11:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end11-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_1:
	.long	1056964608              # float 0.5
.LCPI12_2:
	.long	1034594986              # float 0.0833333284
	.section	.text._ZNK11btConeShape21calculateLocalInertiaEfR9btVector3,"axG",@progbits,_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3,comdat
	.weak	_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3: # @_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 24
	subq	$152, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 176
.Lcfi36:
	.cfi_offset %rbx, -24
.Lcfi37:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movq	%rdi, %rbx
	movl	$1065353216, 88(%rsp)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 92(%rsp)
	movl	$1065353216, 108(%rsp)  # imm = 0x3F800000
	movups	%xmm0, 112(%rsp)
	movl	$1065353216, 128(%rsp)  # imm = 0x3F800000
	movups	%xmm0, 132(%rsp)
	movl	$0, 148(%rsp)
	movq	(%rbx), %rax
	leaq	88(%rsp), %rsi
	leaq	16(%rsp), %rdx
	movq	%rsp, %rcx
	callq	*16(%rax)
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	subps	%xmm1, %xmm2
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	24(%rsp), %xmm0
	mulps	.LCPI12_0(%rip), %xmm2
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	mulss	.LCPI12_1(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	addps	48(%rsp), %xmm0         # 16-byte Folded Reload
	addps	%xmm0, %xmm0
	addss	%xmm1, %xmm1
	mulps	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	mulss	.LCPI12_2(%rip), %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	addps	%xmm0, %xmm1
	movaps	%xmm1, %xmm3
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	%xmm0, %xmm1
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	mulss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, (%r14)
	movlps	%xmm2, 8(%r14)
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end12-_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZNK11btConeShape7getNameEv,"axG",@progbits,_ZNK11btConeShape7getNameEv,comdat
	.weak	_ZNK11btConeShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK11btConeShape7getNameEv,@function
_ZNK11btConeShape7getNameEv:            # @_ZNK11btConeShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end13:
	.size	_ZNK11btConeShape7getNameEv, .Lfunc_end13-_ZNK11btConeShape7getNameEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end14-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end15:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end15-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end16-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end17:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end17-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZN12btConeShapeZD0Ev,"axG",@progbits,_ZN12btConeShapeZD0Ev,comdat
	.weak	_ZN12btConeShapeZD0Ev
	.p2align	4, 0x90
	.type	_ZN12btConeShapeZD0Ev,@function
_ZN12btConeShapeZD0Ev:                  # @_ZN12btConeShapeZD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -24
.Lcfi42:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp6:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp7:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB18_2:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp10:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_4:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN12btConeShapeZD0Ev, .Lfunc_end18-_ZN12btConeShapeZD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end18-.Ltmp10    #   Call between .Ltmp10 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12btConeShapeXD0Ev,"axG",@progbits,_ZN12btConeShapeXD0Ev,comdat
	.weak	_ZN12btConeShapeXD0Ev
	.p2align	4, 0x90
	.type	_ZN12btConeShapeXD0Ev,@function
_ZN12btConeShapeXD0Ev:                  # @_ZN12btConeShapeXD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -24
.Lcfi47:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp12:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp13:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB19_2:
.Ltmp14:
	movq	%rax, %r14
.Ltmp15:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB19_4:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end19:
	.size	_ZN12btConeShapeXD0Ev, .Lfunc_end19-_ZN12btConeShapeXD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end19-.Ltmp16    #   Call between .Ltmp16 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	_ZTV11btConeShape,@object # @_ZTV11btConeShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV11btConeShape
	.p2align	3
_ZTV11btConeShape:
	.quad	0
	.quad	_ZTI11btConeShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN11btConeShapeD0Ev
	.quad	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK11btConeShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK11btConeShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV11btConeShape, 160

	.type	_ZTV12btConeShapeZ,@object # @_ZTV12btConeShapeZ
	.section	.rodata._ZTV12btConeShapeZ,"aG",@progbits,_ZTV12btConeShapeZ,comdat
	.weak	_ZTV12btConeShapeZ
	.p2align	3
_ZTV12btConeShapeZ:
	.quad	0
	.quad	_ZTI12btConeShapeZ
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN12btConeShapeZD0Ev
	.quad	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK11btConeShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK11btConeShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV12btConeShapeZ, 160

	.type	_ZTV12btConeShapeX,@object # @_ZTV12btConeShapeX
	.section	.rodata._ZTV12btConeShapeX,"aG",@progbits,_ZTV12btConeShapeX,comdat
	.weak	_ZTV12btConeShapeX
	.p2align	3
_ZTV12btConeShapeX:
	.quad	0
	.quad	_ZTI12btConeShapeX
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN12btConeShapeXD0Ev
	.quad	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK11btConeShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK11btConeShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV12btConeShapeX, 160

	.type	_ZTS11btConeShape,@object # @_ZTS11btConeShape
	.section	.rodata,"a",@progbits
	.globl	_ZTS11btConeShape
_ZTS11btConeShape:
	.asciz	"11btConeShape"
	.size	_ZTS11btConeShape, 14

	.type	_ZTI11btConeShape,@object # @_ZTI11btConeShape
	.globl	_ZTI11btConeShape
	.p2align	4
_ZTI11btConeShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11btConeShape
	.quad	_ZTI21btConvexInternalShape
	.size	_ZTI11btConeShape, 24

	.type	_ZTS12btConeShapeZ,@object # @_ZTS12btConeShapeZ
	.section	.rodata._ZTS12btConeShapeZ,"aG",@progbits,_ZTS12btConeShapeZ,comdat
	.weak	_ZTS12btConeShapeZ
_ZTS12btConeShapeZ:
	.asciz	"12btConeShapeZ"
	.size	_ZTS12btConeShapeZ, 15

	.type	_ZTI12btConeShapeZ,@object # @_ZTI12btConeShapeZ
	.section	.rodata._ZTI12btConeShapeZ,"aG",@progbits,_ZTI12btConeShapeZ,comdat
	.weak	_ZTI12btConeShapeZ
	.p2align	4
_ZTI12btConeShapeZ:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS12btConeShapeZ
	.quad	_ZTI11btConeShape
	.size	_ZTI12btConeShapeZ, 24

	.type	_ZTS12btConeShapeX,@object # @_ZTS12btConeShapeX
	.section	.rodata._ZTS12btConeShapeX,"aG",@progbits,_ZTS12btConeShapeX,comdat
	.weak	_ZTS12btConeShapeX
_ZTS12btConeShapeX:
	.asciz	"12btConeShapeX"
	.size	_ZTS12btConeShapeX, 15

	.type	_ZTI12btConeShapeX,@object # @_ZTI12btConeShapeX
	.section	.rodata._ZTI12btConeShapeX,"aG",@progbits,_ZTI12btConeShapeX,comdat
	.weak	_ZTI12btConeShapeX
	.p2align	4
_ZTI12btConeShapeX:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS12btConeShapeX
	.quad	_ZTI11btConeShape
	.size	_ZTI12btConeShapeX, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Cone"
	.size	.L.str, 5

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	2
.Lswitch.table:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lswitch.table, 12

	.type	.Lswitch.table.1,@object # @switch.table.1
	.p2align	2
.Lswitch.table.1:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.size	.Lswitch.table.1, 12


	.globl	_ZN11btConeShapeC1Eff
	.type	_ZN11btConeShapeC1Eff,@function
_ZN11btConeShapeC1Eff = _ZN11btConeShapeC2Eff
	.globl	_ZN12btConeShapeZC1Eff
	.type	_ZN12btConeShapeZC1Eff,@function
_ZN12btConeShapeZC1Eff = _ZN12btConeShapeZC2Eff
	.globl	_ZN12btConeShapeXC1Eff
	.type	_ZN12btConeShapeXC1Eff,@function
_ZN12btConeShapeXC1Eff = _ZN12btConeShapeXC2Eff
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
