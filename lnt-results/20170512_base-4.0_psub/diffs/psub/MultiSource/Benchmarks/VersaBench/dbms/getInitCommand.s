	.text
	.file	"getInitCommand.bc"
	.globl	getInitCommand
	.p2align	4, 0x90
	.type	getInitCommand,@function
getInitCommand:                         # @getInitCommand
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	callq	getInt
	cmpq	$3, %rax
	ja	.LBB0_7
# BB#1:
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_2:
	cmpq	$1, (%rbx)
	jg	.LBB0_3
.LBB0_5:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInitCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$3, %eax
	popq	%rbx
	retq
.LBB0_4:
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInitCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$2, %eax
	popq	%rbx
	retq
.LBB0_6:
	movl	$.L.str.2, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInitCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$1, %eax
.LBB0_7:
	popq	%rbx
	retq
.LBB0_3:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	getInitCommand, .Lfunc_end0-getInitCommand
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_2
	.quad	.LBB0_4
	.quad	.LBB0_5
	.quad	.LBB0_6

	.type	getInitCommand.name,@object # @getInitCommand.name
	.data
getInitCommand.name:
	.asciz	"getInitCommand"
	.size	getInitCommand.name, 15

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"invalid fan specified"
	.size	.L.str, 22

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"improper format - early EOI"
	.size	.L.str.1, 28

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"improper format - fan must be an integer"
	.size	.L.str.2, 41


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
