	.text
	.file	"reg_detect.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_1:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi7:
	.cfi_def_cfa_offset 496
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 40(%rsp)
	leaq	40(%rsp), %rdi
	movl	$32, %esi
	movl	$144, %edx
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_268
# BB#1:
	movq	40(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_268
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 40(%rsp)
	leaq	40(%rsp), %rdi
	movl	$32, %esi
	movl	$144, %edx
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_268
# BB#3:                                 # %polybench_alloc_data.exit
	movq	40(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_268
# BB#4:                                 # %polybench_alloc_data.exit53
	movq	$0, 40(%rsp)
	leaq	40(%rsp), %rdi
	movl	$32, %esi
	movl	$144, %edx
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_268
# BB#5:                                 # %polybench_alloc_data.exit53
	movq	40(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_268
# BB#6:                                 # %polybench_alloc_data.exit55
	movq	$0, 40(%rsp)
	leaq	40(%rsp), %rdi
	movl	$32, %esi
	movl	$144, %edx
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_268
# BB#7:                                 # %polybench_alloc_data.exit55
	movq	40(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB6_268
# BB#8:                                 # %polybench_alloc_data.exit57
	movq	$0, 40(%rsp)
	leaq	40(%rsp), %rdi
	movl	$32, %esi
	movl	$9216, %edx             # imm = 0x2400
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_268
# BB#9:                                 # %polybench_alloc_data.exit57
	movq	40(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_268
# BB#10:                                # %polybench_alloc_data.exit59
	movq	%rbp, 192(%rsp)         # 8-byte Spill
	movq	$0, 40(%rsp)
	leaq	40(%rsp), %rdi
	movl	$32, %esi
	movl	$9216, %edx             # imm = 0x2400
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_268
# BB#11:                                # %polybench_alloc_data.exit59
	movq	40(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_268
# BB#12:                                # %polybench_alloc_data.exit61
	movq	%r14, %rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rsi
	movq	%rbx, 184(%rsp)         # 8-byte Spill
	movq	%rbx, %rdx
	callq	init_array
	movq	%rbp, %rsi
	leaq	4(%r14), %r9
	leaq	8(%r14), %r8
	leaq	12(%r14), %r13
	leaq	16(%r14), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leaq	20(%r14), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leaq	1280(%r12), %rbp
	leaq	1536(%r12), %r11
	leaq	1024(%r12), %rbx
	leaq	768(%r12), %rdi
	leaq	512(%r12), %rdx
	leaq	256(%r12), %rax
	cmpq	%r14, %r12
	sbbb	%cl, %cl
	cmpq	%rax, %r14
	sbbb	%r10b, %r10b
	andb	%cl, %r10b
	andb	$1, %r10b
	movb	%r10b, 39(%rsp)         # 1-byte Spill
	movq	%rax, 392(%rsp)         # 8-byte Spill
	cmpq	%r9, %rax
	sbbb	%al, %al
	movq	%r9, 216(%rsp)          # 8-byte Spill
	cmpq	%rdx, %r9
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 38(%rsp)           # 1-byte Spill
	movq	%rdx, 400(%rsp)         # 8-byte Spill
	cmpq	%r8, %rdx
	sbbb	%al, %al
	cmpq	%rdi, %r8
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 37(%rsp)           # 1-byte Spill
	movq	%rdi, 408(%rsp)         # 8-byte Spill
	cmpq	%r13, %rdi
	sbbb	%al, %al
	movq	%r13, 160(%rsp)         # 8-byte Spill
	cmpq	%rbx, %r13
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 36(%rsp)           # 1-byte Spill
	movq	%rbx, 416(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rbx
	sbbb	%al, %al
	cmpq	%rbp, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 35(%rsp)           # 1-byte Spill
	movq	%rbp, 424(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rbp
	sbbb	%al, %al
	movq	%r11, 432(%rsp)         # 8-byte Spill
	cmpq	%r11, %rcx
	sbbb	%dl, %dl
	andb	%al, %dl
	leaq	28(%r14), %rcx
	andb	$1, %dl
	movb	%dl, 34(%rsp)           # 1-byte Spill
	leaq	1792(%r12), %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rax
	sbbb	%al, %al
	leaq	2048(%r12), %rdx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 33(%rsp)           # 1-byte Spill
	leaq	32(%r14), %rcx
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	sbbb	%al, %al
	leaq	2304(%r12), %rdx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 32(%rsp)           # 1-byte Spill
	leaq	36(%r14), %rcx
	movq	%rdx, 320(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	sbbb	%al, %al
	leaq	2560(%r12), %rdx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 31(%rsp)           # 1-byte Spill
	leaq	40(%r14), %rcx
	movq	%rdx, 328(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	sbbb	%al, %al
	leaq	2816(%r12), %rdx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 30(%rsp)           # 1-byte Spill
	leaq	44(%r14), %rcx
	movq	%rdx, 336(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	sbbb	%al, %al
	leaq	3072(%r12), %rdx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	%rdx, 240(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 29(%rsp)           # 1-byte Spill
	leaq	56(%r14), %rcx
	leaq	3584(%r12), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rax
	sbbb	%al, %al
	leaq	3840(%r12), %rdx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 28(%rsp)           # 1-byte Spill
	leaq	60(%r14), %rcx
	movq	%rdx, 344(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	sbbb	%al, %al
	leaq	4096(%r12), %rdx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 27(%rsp)           # 1-byte Spill
	leaq	64(%r14), %rcx
	movq	%rdx, 352(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	sbbb	%al, %al
	leaq	4352(%r12), %rdx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 26(%rsp)           # 1-byte Spill
	leaq	68(%r14), %rcx
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	sbbb	%al, %al
	leaq	4608(%r12), %rdx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 25(%rsp)           # 1-byte Spill
	leaq	84(%r14), %rcx
	leaq	5376(%r12), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rax
	sbbb	%al, %al
	leaq	5632(%r12), %rdx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 24(%rsp)           # 1-byte Spill
	leaq	88(%r14), %rcx
	movq	%rdx, 368(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	sbbb	%al, %al
	leaq	5888(%r12), %rdx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 23(%rsp)           # 1-byte Spill
	leaq	92(%r14), %rcx
	movq	%rdx, 376(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	sbbb	%al, %al
	leaq	6144(%r12), %rdx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%rdx, 272(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 22(%rsp)           # 1-byte Spill
	leaq	112(%r14), %rcx
	leaq	7168(%r12), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rax
	leaq	7424(%r12), %rdx
	sbbb	%al, %al
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 21(%rsp)           # 1-byte Spill
	leaq	116(%r14), %rcx
	movq	%rdx, 384(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	leaq	7680(%r12), %rdx
	sbbb	%al, %al
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 296(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 20(%rsp)           # 1-byte Spill
	leaq	140(%r14), %r13
	leaq	8960(%r12), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	cmpq	%r13, %rax
	leaq	9216(%r12), %rcx
	sbbb	%al, %al
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	cmpq	%rcx, %r13
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 55(%rsp)           # 1-byte Spill
	xorl	%r9d, %r9d
	movq	%r8, 208(%rsp)          # 8-byte Spill
	movq	%r14, 224(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_13:                               # %.preheader.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_15 Depth 2
                                        #     Child Loop BB6_18 Depth 2
                                        #     Child Loop BB6_22 Depth 2
                                        #     Child Loop BB6_235 Depth 2
                                        #     Child Loop BB6_238 Depth 2
                                        #     Child Loop BB6_241 Depth 2
                                        #     Child Loop BB6_64 Depth 2
                                        #     Child Loop BB6_67 Depth 2
                                        #     Child Loop BB6_71 Depth 2
                                        #     Child Loop BB6_248 Depth 2
                                        #     Child Loop BB6_251 Depth 2
                                        #     Child Loop BB6_72 Depth 2
                                        #     Child Loop BB6_75 Depth 2
                                        #     Child Loop BB6_79 Depth 2
                                        #     Child Loop BB6_258 Depth 2
                                        #     Child Loop BB6_80 Depth 2
                                        #     Child Loop BB6_83 Depth 2
                                        #     Child Loop BB6_87 Depth 2
                                        #     Child Loop BB6_88 Depth 2
                                        #     Child Loop BB6_91 Depth 2
                                        #     Child Loop BB6_95 Depth 2
                                        #     Child Loop BB6_23 Depth 2
                                        #       Child Loop BB6_24 Depth 3
                                        #     Child Loop BB6_27 Depth 2
                                        #     Child Loop BB6_29 Depth 2
                                        #     Child Loop BB6_31 Depth 2
                                        #     Child Loop BB6_33 Depth 2
                                        #     Child Loop BB6_35 Depth 2
                                        #     Child Loop BB6_37 Depth 2
                                        #     Child Loop BB6_39 Depth 2
                                        #     Child Loop BB6_41 Depth 2
                                        #     Child Loop BB6_43 Depth 2
                                        #     Child Loop BB6_45 Depth 2
                                        #     Child Loop BB6_47 Depth 2
                                        #     Child Loop BB6_49 Depth 2
                                        #     Child Loop BB6_51 Depth 2
                                        #     Child Loop BB6_53 Depth 2
                                        #     Child Loop BB6_55 Depth 2
	cmpb	$0, 39(%rsp)            # 1-byte Folded Reload
	movq	216(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rbx         # 8-byte Reload
	movq	168(%rsp), %rbp         # 8-byte Reload
	movq	200(%rsp), %rdi         # 8-byte Reload
	je	.LBB6_269
# BB#14:                                # %scalar.ph644.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_15:                               # %scalar.ph644
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14), %ecx
	movl	%ecx, (%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 4(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 8(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 12(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 16(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 20(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 24(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 28(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_15
	jmp	.LBB6_16
	.p2align	4, 0x90
.LBB6_269:                              # %vector.body642
                                        #   in Loop: Header=BB6_13 Depth=1
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 16(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 32(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 48(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 64(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 80(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 96(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 112(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 128(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 144(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 160(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 176(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 192(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 208(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 224(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 240(%r12)
.LBB6_16:                               # %vector.memcheck633
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 38(%rsp)            # 1-byte Folded Reload
	je	.LBB6_19
# BB#17:                                # %.preheader.i.1168.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_18:                               # %.preheader.i.1168
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %ecx
	movl	%ecx, 256(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 260(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 264(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 268(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 272(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 276(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 280(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 284(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_18
	jmp	.LBB6_20
	.p2align	4, 0x90
.LBB6_19:                               # %vector.body620
                                        #   in Loop: Header=BB6_13 Depth=1
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 256(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 272(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 288(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 304(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 320(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 336(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 352(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 368(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 384(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 400(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 416(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 432(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 448(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 464(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 480(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 496(%r12)
.LBB6_20:                               # %vector.memcheck611
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 37(%rsp)            # 1-byte Folded Reload
	je	.LBB6_232
# BB#21:                                # %.preheader.i.2169.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_22:                               # %.preheader.i.2169
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r8), %ecx
	movl	%ecx, 512(%r12,%rax,4)
	movl	(%r8), %ecx
	movl	%ecx, 516(%r12,%rax,4)
	movl	(%r8), %ecx
	movl	%ecx, 520(%r12,%rax,4)
	movl	(%r8), %ecx
	movl	%ecx, 524(%r12,%rax,4)
	movl	(%r8), %ecx
	movl	%ecx, 528(%r12,%rax,4)
	movl	(%r8), %ecx
	movl	%ecx, 532(%r12,%rax,4)
	movl	(%r8), %ecx
	movl	%ecx, 536(%r12,%rax,4)
	movl	(%r8), %ecx
	movl	%ecx, 540(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_22
	jmp	.LBB6_233
	.p2align	4, 0x90
.LBB6_232:                              # %vector.body598
                                        #   in Loop: Header=BB6_13 Depth=1
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 512(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 528(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 544(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 560(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 576(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 592(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 608(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 624(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 640(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 656(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 672(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 688(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 704(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 720(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 736(%r12)
	movd	(%r8), %xmm0            # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 752(%r12)
.LBB6_233:                              # %vector.memcheck589
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 36(%rsp)            # 1-byte Folded Reload
	je	.LBB6_277
# BB#234:                               # %.preheader.i.3170.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	160(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_235:                              # %.preheader.i.3170
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %ecx
	movl	%ecx, 768(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 772(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 776(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 780(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 784(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 788(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 792(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 796(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_235
	jmp	.LBB6_236
	.p2align	4, 0x90
.LBB6_277:                              # %vector.body576
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	160(%rsp), %rax         # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 768(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 784(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 800(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 816(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 832(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 848(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 864(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 880(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 896(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 912(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 928(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 944(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 960(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 976(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 992(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1008(%r12)
.LBB6_236:                              # %vector.memcheck567
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 35(%rsp)            # 1-byte Folded Reload
	je	.LBB6_278
# BB#237:                               # %.preheader.i.4171.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_238:                              # %.preheader.i.4171
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %ecx
	movl	%ecx, 1024(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1028(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1032(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1036(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1040(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1044(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1048(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1052(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_238
	jmp	.LBB6_239
	.p2align	4, 0x90
.LBB6_278:                              # %vector.body554
                                        #   in Loop: Header=BB6_13 Depth=1
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1024(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1040(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1056(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1072(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1088(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1104(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1120(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1136(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1152(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1168(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1184(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1200(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1216(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1232(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1248(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1264(%r12)
.LBB6_239:                              # %vector.memcheck545
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 34(%rsp)            # 1-byte Folded Reload
	je	.LBB6_279
# BB#240:                               # %.preheader.i.5172.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	184(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_241:                              # %.preheader.i.5172
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %ecx
	movl	%ecx, 1280(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1284(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1288(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1292(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1296(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1300(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1304(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1308(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_241
	jmp	.LBB6_242
	.p2align	4, 0x90
.LBB6_279:                              # %vector.body532
                                        #   in Loop: Header=BB6_13 Depth=1
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1280(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1296(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1312(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1328(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1344(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1360(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1376(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1392(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1408(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1424(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1440(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1456(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1472(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1488(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1504(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1520(%r12)
	movq	184(%rsp), %rdx         # 8-byte Reload
.LBB6_242:                              # %vector.memcheck523
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 33(%rsp)            # 1-byte Folded Reload
	je	.LBB6_244
# BB#243:                               # %.preheader.1.i.preheader173.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_64:                               # %.preheader.1.i.preheader173
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 1792(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 1796(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 1800(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 1804(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 1808(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 1812(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 1816(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 1820(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_64
	jmp	.LBB6_65
	.p2align	4, 0x90
.LBB6_244:                              # %vector.body510
                                        #   in Loop: Header=BB6_13 Depth=1
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1792(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1808(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1824(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1840(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1856(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1872(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1888(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1904(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1920(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1936(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1952(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1968(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1984(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2000(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2016(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2032(%r12)
.LBB6_65:                               # %vector.memcheck501
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 32(%rsp)            # 1-byte Folded Reload
	je	.LBB6_68
# BB#66:                                # %.preheader.1.i.1174.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	152(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_67:                               # %.preheader.1.i.1174
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 2048(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2052(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2056(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2060(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2064(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2068(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2072(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2076(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_67
	jmp	.LBB6_69
	.p2align	4, 0x90
.LBB6_68:                               # %vector.body488
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	152(%rsp), %rax         # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2048(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2064(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2080(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2096(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2112(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2128(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2144(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2160(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2176(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2192(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2208(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2224(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2240(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2256(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2272(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2288(%r12)
.LBB6_69:                               # %vector.memcheck479
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	je	.LBB6_245
# BB#70:                                # %.preheader.1.i.2175.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	144(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_71:                               # %.preheader.1.i.2175
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 2304(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2308(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2312(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2316(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2320(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2324(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2328(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2332(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_71
	jmp	.LBB6_246
	.p2align	4, 0x90
.LBB6_245:                              # %vector.body466
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2304(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2320(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2336(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2352(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2368(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2384(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2400(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2416(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2432(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2448(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2464(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2480(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2496(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2512(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2528(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2544(%r12)
.LBB6_246:                              # %vector.memcheck457
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 30(%rsp)            # 1-byte Folded Reload
	je	.LBB6_280
# BB#247:                               # %.preheader.1.i.3176.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	136(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_248:                              # %.preheader.1.i.3176
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 2560(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2564(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2568(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2572(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2576(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2580(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2584(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2588(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_248
	jmp	.LBB6_249
	.p2align	4, 0x90
.LBB6_280:                              # %vector.body444
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	136(%rsp), %rax         # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2560(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2576(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2592(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2608(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2624(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2640(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2656(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2672(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2688(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2704(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2720(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2736(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2752(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2768(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2784(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2800(%r12)
.LBB6_249:                              # %vector.memcheck435
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 29(%rsp)            # 1-byte Folded Reload
	je	.LBB6_281
# BB#250:                               # %.preheader.1.i.4177.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	128(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_251:                              # %.preheader.1.i.4177
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 2816(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2820(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2824(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2828(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2832(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2836(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2840(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2844(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_251
	jmp	.LBB6_252
	.p2align	4, 0x90
.LBB6_281:                              # %vector.body422
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2816(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2832(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2848(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2864(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2880(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2896(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2912(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2928(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2944(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2960(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2976(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2992(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3008(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3024(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3040(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3056(%r12)
.LBB6_252:                              # %vector.memcheck413
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 28(%rsp)            # 1-byte Folded Reload
	je	.LBB6_254
# BB#253:                               # %.preheader.2.i.preheader178.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	120(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_72:                               # %.preheader.2.i.preheader178
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 3584(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3588(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3592(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3596(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3600(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3604(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3608(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3612(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_72
	jmp	.LBB6_73
	.p2align	4, 0x90
.LBB6_254:                              # %vector.body400
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3584(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3600(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3616(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3632(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3648(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3664(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3680(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3696(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3712(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3728(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3744(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3760(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3776(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3792(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3808(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3824(%r12)
.LBB6_73:                               # %vector.memcheck391
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 27(%rsp)            # 1-byte Folded Reload
	je	.LBB6_76
# BB#74:                                # %.preheader.2.i.1179.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_75:                               # %.preheader.2.i.1179
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 3840(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3844(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3848(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3852(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3856(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3860(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3864(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3868(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_75
	jmp	.LBB6_77
	.p2align	4, 0x90
.LBB6_76:                               # %vector.body378
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3840(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3856(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3872(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3888(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3904(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3920(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3936(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3952(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3968(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3984(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4000(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4016(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4032(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4048(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4064(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4080(%r12)
.LBB6_77:                               # %vector.memcheck369
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 26(%rsp)            # 1-byte Folded Reload
	je	.LBB6_255
# BB#78:                                # %.preheader.2.i.2180.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	104(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_79:                               # %.preheader.2.i.2180
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 4096(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4100(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4104(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4108(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4112(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4116(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4120(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4124(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_79
	jmp	.LBB6_256
	.p2align	4, 0x90
.LBB6_255:                              # %vector.body356
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4096(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4112(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4128(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4144(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4160(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4176(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4192(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4208(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4224(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4240(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4256(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4272(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4288(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4304(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4320(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4336(%r12)
.LBB6_256:                              # %vector.memcheck347
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 25(%rsp)            # 1-byte Folded Reload
	je	.LBB6_282
# BB#257:                               # %.preheader.2.i.3181.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	96(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_258:                              # %.preheader.2.i.3181
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 4352(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4356(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4360(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4364(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4368(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4372(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4376(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 4380(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_258
	jmp	.LBB6_259
	.p2align	4, 0x90
.LBB6_282:                              # %vector.body334
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4352(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4368(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4384(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4400(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4416(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4432(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4448(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4464(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4480(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4496(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4512(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4528(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4544(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4560(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4576(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4592(%r12)
.LBB6_259:                              # %vector.memcheck325
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 24(%rsp)            # 1-byte Folded Reload
	je	.LBB6_261
# BB#260:                               # %.preheader.3.i.preheader182.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	88(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_80:                               # %.preheader.3.i.preheader182
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 5376(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5380(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5384(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5388(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5392(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5396(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5400(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5404(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_80
	jmp	.LBB6_81
	.p2align	4, 0x90
.LBB6_261:                              # %vector.body312
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5376(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5392(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5408(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5424(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5440(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5456(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5472(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5488(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5504(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5520(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5536(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5552(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5568(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5584(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5600(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5616(%r12)
.LBB6_81:                               # %vector.memcheck303
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 23(%rsp)            # 1-byte Folded Reload
	je	.LBB6_84
# BB#82:                                # %.preheader.3.i.1183.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	80(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_83:                               # %.preheader.3.i.1183
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 5632(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5636(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5640(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5644(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5648(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5652(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5656(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5660(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_83
	jmp	.LBB6_85
	.p2align	4, 0x90
.LBB6_84:                               # %vector.body290
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5632(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5648(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5664(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5680(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5696(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5712(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5728(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5744(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5760(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5776(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5792(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5808(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5824(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5840(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5856(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5872(%r12)
.LBB6_85:                               # %vector.memcheck281
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 22(%rsp)            # 1-byte Folded Reload
	je	.LBB6_262
# BB#86:                                # %.preheader.3.i.2184.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	72(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_87:                               # %.preheader.3.i.2184
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 5888(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5892(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5896(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5900(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5904(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5908(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5912(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5916(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_87
	jmp	.LBB6_263
	.p2align	4, 0x90
.LBB6_262:                              # %vector.body268
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5888(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5904(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5920(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5936(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5952(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5968(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5984(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6000(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6016(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6032(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6048(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6064(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6080(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6096(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6112(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6128(%r12)
.LBB6_263:                              # %vector.memcheck259
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 21(%rsp)            # 1-byte Folded Reload
	je	.LBB6_265
# BB#264:                               # %.preheader.4.i.preheader185.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_88:                               # %.preheader.4.i.preheader185
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 7168(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7172(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7176(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7180(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7184(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7188(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7192(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7196(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_88
	jmp	.LBB6_89
	.p2align	4, 0x90
.LBB6_265:                              # %vector.body246
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7168(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7184(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7200(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7216(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7232(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7248(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7264(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7280(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7296(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7312(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7328(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7344(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7360(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7376(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7392(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7408(%r12)
.LBB6_89:                               # %vector.memcheck237
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpb	$0, 20(%rsp)            # 1-byte Folded Reload
	je	.LBB6_92
# BB#90:                                # %.preheader.4.i.1186.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_91:                               # %.preheader.4.i.1186
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 7424(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7428(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7432(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7436(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7440(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7444(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7448(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7452(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_91
	jmp	.LBB6_93
	.p2align	4, 0x90
.LBB6_92:                               # %vector.body224
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7424(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7440(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7456(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7472(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7488(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7504(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7520(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7536(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7552(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7568(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7584(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7600(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7616(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7632(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7648(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7664(%r12)
.LBB6_93:                               # %vector.memcheck
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	%rdx, %r10
	cmpb	$0, 55(%rsp)            # 1-byte Folded Reload
	je	.LBB6_266
# BB#94:                                # %.preheader.5.i.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_95:                               # %.preheader.5.i
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13), %ecx
	movl	%ecx, 8960(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8964(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8968(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8972(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8976(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8980(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8984(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8988(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_95
	jmp	.LBB6_267
	.p2align	4, 0x90
.LBB6_266:                              # %vector.body
                                        #   in Loop: Header=BB6_13 Depth=1
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 8960(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 8976(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 8992(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9008(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9024(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9040(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9056(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9072(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9088(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9104(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9120(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9136(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9152(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9168(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9184(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9200(%r12)
.LBB6_267:                              # %.lr.ph.i.preheader.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	$12, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_23:                               # %.lr.ph.i.preheader
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_24 Depth 3
	movq	%rcx, %rdx
	shlq	$8, %rdx
	movl	(%r12,%rdx), %ebx
	movl	%ebx, (%r15,%rdx)
	movq	%rdi, %rax
	movl	$63, %ebp
	.p2align	4, 0x90
.LBB6_24:                               #   Parent Loop BB6_13 Depth=1
                                        #     Parent Loop BB6_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addl	-8(%r12,%rax), %ebx
	movl	%ebx, -8(%r15,%rax)
	addl	-4(%r12,%rax), %ebx
	movl	%ebx, -4(%r15,%rax)
	addl	(%r12,%rax), %ebx
	movl	%ebx, (%r15,%rax)
	addq	$12, %rax
	addq	$-3, %rbp
	jne	.LBB6_24
# BB#25:                                #   in Loop: Header=BB6_23 Depth=2
	movl	252(%r15,%rdx), %eax
	movl	%eax, (%rsi,%rcx,4)
	incq	%rcx
	addq	$256, %rdi              # imm = 0x100
	cmpq	$6, %rcx
	jne	.LBB6_23
# BB#26:                                # %._crit_edge13.i
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	1792(%r12), %eax
	movl	%eax, 1792(%r15)
	movl	$451, %ecx              # imm = 0x1C3
	.p2align	4, 0x90
.LBB6_27:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$514, %rcx              # imm = 0x202
	jne	.LBB6_27
# BB#28:                                # %._crit_edge13.i.112941300
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	2044(%r15), %eax
	movl	%eax, 28(%rsi)
	movl	2048(%r12), %eax
	movl	%eax, 2048(%r15)
	movl	$515, %ecx              # imm = 0x203
	movq	224(%rsp), %r14         # 8-byte Reload
	movq	%r10, %rbx
	movq	208(%rsp), %r8          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_29:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$578, %rcx              # imm = 0x242
	jne	.LBB6_29
# BB#30:                                # %._crit_edge13.i.212961301
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	2300(%r15), %eax
	movl	%eax, 32(%rsi)
	movl	2304(%r12), %eax
	movl	%eax, 2304(%r15)
	movl	$579, %ecx              # imm = 0x243
	.p2align	4, 0x90
.LBB6_31:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$642, %rcx              # imm = 0x282
	jne	.LBB6_31
# BB#32:                                # %._crit_edge13.i.312981302
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	2556(%r15), %eax
	movl	%eax, 36(%rsi)
	movl	2560(%r12), %eax
	movl	%eax, 2560(%r15)
	movl	$643, %ecx              # imm = 0x283
	.p2align	4, 0x90
.LBB6_33:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$706, %rcx              # imm = 0x2C2
	jne	.LBB6_33
# BB#34:                                # %._crit_edge13.i.412991303
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	2812(%r15), %eax
	movl	%eax, 40(%rsi)
	movl	2816(%r12), %eax
	movl	%eax, 2816(%r15)
	movl	$707, %ecx              # imm = 0x2C3
	.p2align	4, 0x90
.LBB6_35:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$770, %rcx              # imm = 0x302
	jne	.LBB6_35
# BB#36:                                # %._crit_edge13.i.1
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	3068(%r15), %eax
	movl	%eax, 44(%rsi)
	movl	3584(%r12), %eax
	movl	%eax, 3584(%r15)
	movl	$899, %ecx              # imm = 0x383
	.p2align	4, 0x90
.LBB6_37:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$962, %rcx              # imm = 0x3C2
	jne	.LBB6_37
# BB#38:                                # %._crit_edge13.i.1.11307
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	3836(%r15), %eax
	movl	%eax, 56(%rsi)
	movl	3840(%r12), %eax
	movl	%eax, 3840(%r15)
	movl	$963, %ecx              # imm = 0x3C3
	.p2align	4, 0x90
.LBB6_39:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1026, %rcx             # imm = 0x402
	jne	.LBB6_39
# BB#40:                                # %._crit_edge13.i.1.21308
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	4092(%r15), %eax
	movl	%eax, 60(%rsi)
	movl	4096(%r12), %eax
	movl	%eax, 4096(%r15)
	movl	$1027, %ecx             # imm = 0x403
	.p2align	4, 0x90
.LBB6_41:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1090, %rcx             # imm = 0x442
	jne	.LBB6_41
# BB#42:                                # %._crit_edge13.i.1.31309
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	4348(%r15), %eax
	movl	%eax, 64(%rsi)
	movl	4352(%r12), %eax
	movl	%eax, 4352(%r15)
	movl	$1091, %ecx             # imm = 0x443
	.p2align	4, 0x90
.LBB6_43:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1154, %rcx             # imm = 0x482
	jne	.LBB6_43
# BB#44:                                # %._crit_edge13.i.2
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	4604(%r15), %eax
	movl	%eax, 68(%rsi)
	movl	5376(%r12), %eax
	movl	%eax, 5376(%r15)
	movl	$1347, %ecx             # imm = 0x543
	.p2align	4, 0x90
.LBB6_45:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1410, %rcx             # imm = 0x582
	jne	.LBB6_45
# BB#46:                                # %._crit_edge13.i.2.11313
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	5628(%r15), %eax
	movl	%eax, 84(%rsi)
	movl	5632(%r12), %eax
	movl	%eax, 5632(%r15)
	movl	$1411, %ecx             # imm = 0x583
	.p2align	4, 0x90
.LBB6_47:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1474, %rcx             # imm = 0x5C2
	jne	.LBB6_47
# BB#48:                                # %._crit_edge13.i.2.21314
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	5884(%r15), %eax
	movl	%eax, 88(%rsi)
	movl	5888(%r12), %eax
	movl	%eax, 5888(%r15)
	movl	$1475, %ecx             # imm = 0x5C3
	.p2align	4, 0x90
.LBB6_49:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1538, %rcx             # imm = 0x602
	jne	.LBB6_49
# BB#50:                                # %._crit_edge13.i.3
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	6140(%r15), %eax
	movl	%eax, 92(%rsi)
	movl	7168(%r12), %eax
	movl	%eax, 7168(%r15)
	movl	$1795, %ecx             # imm = 0x703
	.p2align	4, 0x90
.LBB6_51:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1858, %rcx             # imm = 0x742
	jne	.LBB6_51
# BB#52:                                # %._crit_edge13.i.3.11317
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	7420(%r15), %eax
	movl	%eax, 112(%rsi)
	movl	7424(%r12), %eax
	movl	%eax, 7424(%r15)
	movl	$1859, %ecx             # imm = 0x743
	.p2align	4, 0x90
.LBB6_53:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1922, %rcx             # imm = 0x782
	jne	.LBB6_53
# BB#54:                                # %._crit_edge13.i.41318
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	7676(%r15), %eax
	movl	%eax, 116(%rsi)
	movl	8960(%r12), %eax
	movl	%eax, 8960(%r15)
	movl	$2243, %ecx             # imm = 0x8C3
	.p2align	4, 0x90
.LBB6_55:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$2306, %rcx             # imm = 0x902
	jne	.LBB6_55
# BB#56:                                # %._crit_edge13.i.5
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	9212(%r15), %eax
	movl	%eax, 140(%rsi)
	movl	(%rsi), %r10d
	movl	%r10d, (%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	4(%rax), %r11d
	movl	%r11d, 4(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	8(%rax), %eax
	movl	%eax, 8(%rbx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	12(%rcx), %ecx
	movl	%ecx, 12(%rbx)
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	16(%rdx), %edx
	movl	%edx, 16(%rbx)
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	20(%rsi), %esi
	movl	%esi, 20(%rbx)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	28(%rsi), %r10d
	movl	%r10d, 28(%rbx)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	32(%rsi), %r11d
	movl	%r11d, 32(%rbx)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	36(%rsi), %eax
	movl	%eax, 36(%rbx)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	40(%rsi), %ecx
	movl	%ecx, 40(%rbx)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	44(%rsi), %edx
	movl	%edx, 44(%rbx)
	movq	8(%rsp), %rdx           # 8-byte Reload
	addl	56(%rdx), %r10d
	movl	%r10d, 56(%rbx)
	movq	8(%rsp), %rdx           # 8-byte Reload
	addl	60(%rdx), %r11d
	movl	%r11d, 60(%rbx)
	movq	8(%rsp), %rdx           # 8-byte Reload
	addl	64(%rdx), %eax
	movl	%eax, 64(%rbx)
	movq	8(%rsp), %rdx           # 8-byte Reload
	addl	68(%rdx), %ecx
	movl	%ecx, 68(%rbx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	addl	84(%rcx), %r10d
	movl	%r10d, 84(%rbx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	addl	88(%rcx), %r11d
	movl	%r11d, 88(%rbx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	addl	92(%rcx), %eax
	movl	%eax, 92(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	addl	112(%rax), %r10d
	movl	%r10d, 112(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	addl	116(%rax), %r11d
	movl	%r11d, 116(%rbx)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	140(%rsi), %r10d
	movl	%r10d, 140(%rbx)
	incl	%r9d
	cmpl	$10000, %r9d            # imm = 0x2710
	jne	.LBB6_13
# BB#57:                                # %kernel_reg_detect.exit
	movq	%r14, %rdi
	movq	192(%rsp), %rdx         # 8-byte Reload
	callq	init_array
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	8(%rsp), %r11           # 8-byte Reload
	cmpq	%r14, %r12
	sbbb	%al, %al
	movq	392(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %r14
	sbbb	%dl, %dl
	andb	%al, %dl
	andb	$1, %dl
	movb	%dl, 39(%rsp)           # 1-byte Spill
	movq	216(%rsp), %rdx         # 8-byte Reload
	cmpq	%rdx, %rcx
	sbbb	%al, %al
	movq	400(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rdx
	sbbb	%bl, %bl
	andb	%al, %bl
	andb	$1, %bl
	movb	%bl, 38(%rsp)           # 1-byte Spill
	cmpq	%rsi, %rcx
	sbbb	%al, %al
	movq	408(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rsi
	sbbb	%bl, %bl
	andb	%al, %bl
	andb	$1, %bl
	movb	%bl, 37(%rsp)           # 1-byte Spill
	movq	160(%rsp), %rdi         # 8-byte Reload
	cmpq	%rdi, %rcx
	sbbb	%al, %al
	movq	416(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rdi
	sbbb	%bl, %bl
	andb	%al, %bl
	andb	$1, %bl
	movb	%bl, 36(%rsp)           # 1-byte Spill
	movq	176(%rsp), %rbx         # 8-byte Reload
	cmpq	%rbx, %rcx
	sbbb	%r8b, %r8b
	movq	424(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rbx
	sbbb	%al, %al
	andb	%r8b, %al
	andb	$1, %al
	movb	%al, 35(%rsp)           # 1-byte Spill
	movq	168(%rsp), %rbp         # 8-byte Reload
	cmpq	%rbp, %rcx
	sbbb	%al, %al
	cmpq	432(%rsp), %rbp         # 8-byte Folded Reload
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 34(%rsp)           # 1-byte Spill
	movq	200(%rsp), %r10         # 8-byte Reload
	cmpq	%r10, 232(%rsp)         # 8-byte Folded Reload
	sbbb	%r8b, %r8b
	movq	312(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %r10
	sbbb	%cl, %cl
	andb	%r8b, %cl
	andb	$1, %cl
	movb	%cl, 33(%rsp)           # 1-byte Spill
	movq	152(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rax
	sbbb	%r8b, %r8b
	movq	320(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	sbbb	%cl, %cl
	andb	%r8b, %cl
	andb	$1, %cl
	movb	%cl, 32(%rsp)           # 1-byte Spill
	movq	144(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rax
	sbbb	%r8b, %r8b
	movq	328(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	sbbb	%cl, %cl
	andb	%r8b, %cl
	andb	$1, %cl
	movb	%cl, 31(%rsp)           # 1-byte Spill
	movq	136(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rax
	sbbb	%r8b, %r8b
	movq	336(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	sbbb	%cl, %cl
	andb	%r8b, %cl
	andb	$1, %cl
	movb	%cl, 30(%rsp)           # 1-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rax
	sbbb	%al, %al
	cmpq	240(%rsp), %rcx         # 8-byte Folded Reload
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 29(%rsp)           # 1-byte Spill
	movq	120(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, 248(%rsp)         # 8-byte Folded Reload
	sbbb	%r8b, %r8b
	movq	344(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	sbbb	%cl, %cl
	andb	%r8b, %cl
	andb	$1, %cl
	movb	%cl, 28(%rsp)           # 1-byte Spill
	movq	112(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rax
	sbbb	%r8b, %r8b
	movq	352(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	sbbb	%cl, %cl
	andb	%r8b, %cl
	andb	$1, %cl
	movb	%cl, 27(%rsp)           # 1-byte Spill
	movq	104(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %rax
	sbbb	%r8b, %r8b
	movq	360(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	sbbb	%cl, %cl
	andb	%r8b, %cl
	andb	$1, %cl
	movb	%cl, 26(%rsp)           # 1-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rax
	sbbb	%al, %al
	cmpq	256(%rsp), %rcx         # 8-byte Folded Reload
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 25(%rsp)           # 1-byte Spill
	movq	88(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, 264(%rsp)         # 8-byte Folded Reload
	sbbb	%r8b, %r8b
	movq	368(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	sbbb	%cl, %cl
	andb	%r8b, %cl
	andb	$1, %cl
	movb	%cl, 24(%rsp)           # 1-byte Spill
	movq	80(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rax
	sbbb	%r8b, %r8b
	movq	376(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	sbbb	%cl, %cl
	andb	%r8b, %cl
	andb	$1, %cl
	movb	%cl, 23(%rsp)           # 1-byte Spill
	movq	72(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rax
	sbbb	%al, %al
	cmpq	272(%rsp), %rcx         # 8-byte Folded Reload
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 22(%rsp)           # 1-byte Spill
	movq	64(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, 280(%rsp)         # 8-byte Folded Reload
	sbbb	%r8b, %r8b
	movq	384(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	sbbb	%cl, %cl
	andb	%r8b, %cl
	andb	$1, %cl
	movb	%cl, 21(%rsp)           # 1-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rax
	sbbb	%al, %al
	cmpq	296(%rsp), %rcx         # 8-byte Folded Reload
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 20(%rsp)           # 1-byte Spill
	cmpq	%r13, 288(%rsp)         # 8-byte Folded Reload
	sbbb	%al, %al
	cmpq	304(%rsp), %r13         # 8-byte Folded Reload
	sbbb	%r8b, %r8b
	andb	%al, %r8b
	andb	$1, %r8b
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB6_58:                               # %.preheader.lr.ph.i63
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_60 Depth 2
                                        #     Child Loop BB6_63 Depth 2
                                        #     Child Loop BB6_99 Depth 2
                                        #     Child Loop BB6_199 Depth 2
                                        #     Child Loop BB6_202 Depth 2
                                        #     Child Loop BB6_205 Depth 2
                                        #     Child Loop BB6_139 Depth 2
                                        #     Child Loop BB6_142 Depth 2
                                        #     Child Loop BB6_146 Depth 2
                                        #     Child Loop BB6_212 Depth 2
                                        #     Child Loop BB6_215 Depth 2
                                        #     Child Loop BB6_147 Depth 2
                                        #     Child Loop BB6_150 Depth 2
                                        #     Child Loop BB6_154 Depth 2
                                        #     Child Loop BB6_222 Depth 2
                                        #     Child Loop BB6_155 Depth 2
                                        #     Child Loop BB6_158 Depth 2
                                        #     Child Loop BB6_162 Depth 2
                                        #     Child Loop BB6_163 Depth 2
                                        #     Child Loop BB6_166 Depth 2
                                        #     Child Loop BB6_170 Depth 2
                                        #     Child Loop BB6_100 Depth 2
                                        #       Child Loop BB6_101 Depth 3
                                        #     Child Loop BB6_104 Depth 2
                                        #     Child Loop BB6_106 Depth 2
                                        #     Child Loop BB6_108 Depth 2
                                        #     Child Loop BB6_110 Depth 2
                                        #     Child Loop BB6_112 Depth 2
                                        #     Child Loop BB6_114 Depth 2
                                        #     Child Loop BB6_116 Depth 2
                                        #     Child Loop BB6_118 Depth 2
                                        #     Child Loop BB6_120 Depth 2
                                        #     Child Loop BB6_122 Depth 2
                                        #     Child Loop BB6_124 Depth 2
                                        #     Child Loop BB6_126 Depth 2
                                        #     Child Loop BB6_128 Depth 2
                                        #     Child Loop BB6_130 Depth 2
                                        #     Child Loop BB6_132 Depth 2
	cmpb	$0, 39(%rsp)            # 1-byte Folded Reload
	je	.LBB6_270
# BB#59:                                # %scalar.ph1105.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_60:                               # %scalar.ph1105
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14), %ecx
	movl	%ecx, (%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 4(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 8(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 12(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 16(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 20(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 24(%r12,%rax,4)
	movl	(%r14), %ecx
	movl	%ecx, 28(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_60
	jmp	.LBB6_61
	.p2align	4, 0x90
.LBB6_270:                              # %vector.body1103
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 16(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 32(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 48(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 64(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 80(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 96(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 112(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 128(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 144(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 160(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 176(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 192(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 208(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 224(%r12)
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 240(%r12)
.LBB6_61:                               # %vector.memcheck1094
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 38(%rsp)            # 1-byte Folded Reload
	je	.LBB6_96
# BB#62:                                # %.preheader.i65.1148.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_63:                               # %.preheader.i65.1148
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %ecx
	movl	%ecx, 256(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 260(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 264(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 268(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 272(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 276(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 280(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 284(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_63
	jmp	.LBB6_97
	.p2align	4, 0x90
.LBB6_96:                               # %vector.body1081
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 256(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 272(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 288(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 304(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 320(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 336(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 352(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 368(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 384(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 400(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 416(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 432(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 448(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 464(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 480(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 496(%r12)
.LBB6_97:                               # %vector.memcheck1072
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 37(%rsp)            # 1-byte Folded Reload
	je	.LBB6_196
# BB#98:                                # %.preheader.i65.2149.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_99:                               # %.preheader.i65.2149
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %ecx
	movl	%ecx, 512(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 516(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 520(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 524(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 528(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 532(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 536(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 540(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_99
	jmp	.LBB6_197
	.p2align	4, 0x90
.LBB6_196:                              # %vector.body1059
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 512(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 528(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 544(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 560(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 576(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 592(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 608(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 624(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 640(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 656(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 672(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 688(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 704(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 720(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 736(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 752(%r12)
.LBB6_197:                              # %vector.memcheck1050
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 36(%rsp)            # 1-byte Folded Reload
	je	.LBB6_271
# BB#198:                               # %.preheader.i65.3150.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_199:                              # %.preheader.i65.3150
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 768(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 772(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 776(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 780(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 784(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 788(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 792(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 796(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_199
	jmp	.LBB6_200
	.p2align	4, 0x90
.LBB6_271:                              # %vector.body1037
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 768(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 784(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 800(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 816(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 832(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 848(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 864(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 880(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 896(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 912(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 928(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 944(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 960(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 976(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 992(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1008(%r12)
.LBB6_200:                              # %vector.memcheck1028
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 35(%rsp)            # 1-byte Folded Reload
	je	.LBB6_272
# BB#201:                               # %.preheader.i65.4151.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	movq	152(%rsp), %rdx         # 8-byte Reload
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	136(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_202:                              # %.preheader.i65.4151
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %ecx
	movl	%ecx, 1024(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1028(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1032(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1036(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1040(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1044(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1048(%r12,%rax,4)
	movl	(%rbx), %ecx
	movl	%ecx, 1052(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_202
	jmp	.LBB6_203
	.p2align	4, 0x90
.LBB6_272:                              # %vector.body1015
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1024(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1040(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1056(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1072(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1088(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1104(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1120(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1136(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1152(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1168(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1184(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1200(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1216(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1232(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1248(%r12)
	movd	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1264(%r12)
	movq	152(%rsp), %rdx         # 8-byte Reload
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	136(%rsp), %rdi         # 8-byte Reload
.LBB6_203:                              # %vector.memcheck1006
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 34(%rsp)            # 1-byte Folded Reload
	je	.LBB6_273
# BB#204:                               # %.preheader.i65.5152.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_205:                              # %.preheader.i65.5152
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %ecx
	movl	%ecx, 1280(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1284(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1288(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1292(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1296(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1300(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1304(%r12,%rax,4)
	movl	(%rbp), %ecx
	movl	%ecx, 1308(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_205
	jmp	.LBB6_206
	.p2align	4, 0x90
.LBB6_273:                              # %vector.body993
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1280(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1296(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1312(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1328(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1344(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1360(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1376(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1392(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1408(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1424(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1440(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1456(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1472(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1488(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1504(%r12)
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1520(%r12)
.LBB6_206:                              # %vector.memcheck984
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 33(%rsp)            # 1-byte Folded Reload
	je	.LBB6_208
# BB#207:                               # %.preheader.1.i90.preheader153.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_139:                              # %.preheader.1.i90.preheader153
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r10), %ecx
	movl	%ecx, 1792(%r12,%rax,4)
	movl	(%r10), %ecx
	movl	%ecx, 1796(%r12,%rax,4)
	movl	(%r10), %ecx
	movl	%ecx, 1800(%r12,%rax,4)
	movl	(%r10), %ecx
	movl	%ecx, 1804(%r12,%rax,4)
	movl	(%r10), %ecx
	movl	%ecx, 1808(%r12,%rax,4)
	movl	(%r10), %ecx
	movl	%ecx, 1812(%r12,%rax,4)
	movl	(%r10), %ecx
	movl	%ecx, 1816(%r12,%rax,4)
	movl	(%r10), %ecx
	movl	%ecx, 1820(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_139
	jmp	.LBB6_140
	.p2align	4, 0x90
.LBB6_208:                              # %vector.body971
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1792(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1808(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1824(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1840(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1856(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1872(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1888(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1904(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1920(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1936(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1952(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1968(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 1984(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2000(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2016(%r12)
	movd	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2032(%r12)
.LBB6_140:                              # %vector.memcheck962
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 32(%rsp)            # 1-byte Folded Reload
	je	.LBB6_143
# BB#141:                               # %.preheader.1.i90.1154.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_142:                              # %.preheader.1.i90.1154
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %ecx
	movl	%ecx, 2048(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2052(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2056(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2060(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2064(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2068(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2072(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2076(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_142
	jmp	.LBB6_144
	.p2align	4, 0x90
.LBB6_143:                              # %vector.body949
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2048(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2064(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2080(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2096(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2112(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2128(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2144(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2160(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2176(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2192(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2208(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2224(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2240(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2256(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2272(%r12)
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2288(%r12)
.LBB6_144:                              # %vector.memcheck940
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	je	.LBB6_209
# BB#145:                               # %.preheader.1.i90.2155.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_146:                              # %.preheader.1.i90.2155
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %ecx
	movl	%ecx, 2304(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 2308(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 2312(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 2316(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 2320(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 2324(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 2328(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 2332(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_146
	jmp	.LBB6_210
	.p2align	4, 0x90
.LBB6_209:                              # %vector.body927
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2304(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2320(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2336(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2352(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2368(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2384(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2400(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2416(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2432(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2448(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2464(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2480(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2496(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2512(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2528(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2544(%r12)
.LBB6_210:                              # %vector.memcheck918
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 30(%rsp)            # 1-byte Folded Reload
	je	.LBB6_274
# BB#211:                               # %.preheader.1.i90.3156.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_212:                              # %.preheader.1.i90.3156
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 2560(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2564(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2568(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2572(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2576(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2580(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2584(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 2588(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_212
	jmp	.LBB6_213
	.p2align	4, 0x90
.LBB6_274:                              # %vector.body905
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2560(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2576(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2592(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2608(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2624(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2640(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2656(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2672(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2688(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2704(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2720(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2736(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2752(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2768(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2784(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2800(%r12)
.LBB6_213:                              # %vector.memcheck896
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 29(%rsp)            # 1-byte Folded Reload
	je	.LBB6_275
# BB#214:                               # %.preheader.1.i90.4157.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	movq	128(%rsp), %rdx         # 8-byte Reload
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	112(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_215:                              # %.preheader.1.i90.4157
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %ecx
	movl	%ecx, 2816(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2820(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2824(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2828(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2832(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2836(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2840(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 2844(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_215
	jmp	.LBB6_216
	.p2align	4, 0x90
.LBB6_275:                              # %vector.body883
                                        #   in Loop: Header=BB6_58 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2816(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2832(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2848(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2864(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2880(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2896(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2912(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2928(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2944(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2960(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2976(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 2992(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3008(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3024(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3040(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3056(%r12)
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	112(%rsp), %rdi         # 8-byte Reload
.LBB6_216:                              # %vector.memcheck874
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 28(%rsp)            # 1-byte Folded Reload
	je	.LBB6_218
# BB#217:                               # %.preheader.2.i97.preheader158.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_147:                              # %.preheader.2.i97.preheader158
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %ecx
	movl	%ecx, 3584(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 3588(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 3592(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 3596(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 3600(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 3604(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 3608(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 3612(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_147
	jmp	.LBB6_148
	.p2align	4, 0x90
.LBB6_218:                              # %vector.body861
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3584(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3600(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3616(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3632(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3648(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3664(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3680(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3696(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3712(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3728(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3744(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3760(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3776(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3792(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3808(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3824(%r12)
.LBB6_148:                              # %vector.memcheck852
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 27(%rsp)            # 1-byte Folded Reload
	je	.LBB6_151
# BB#149:                               # %.preheader.2.i97.1159.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_150:                              # %.preheader.2.i97.1159
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 3840(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3844(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3848(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3852(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3856(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3860(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3864(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 3868(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_150
	jmp	.LBB6_152
	.p2align	4, 0x90
.LBB6_151:                              # %vector.body839
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3840(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3856(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3872(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3888(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3904(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3920(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3936(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3952(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3968(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 3984(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4000(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4016(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4032(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4048(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4064(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4080(%r12)
.LBB6_152:                              # %vector.memcheck830
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 26(%rsp)            # 1-byte Folded Reload
	je	.LBB6_219
# BB#153:                               # %.preheader.2.i97.2160.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_154:                              # %.preheader.2.i97.2160
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %ecx
	movl	%ecx, 4096(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 4100(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 4104(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 4108(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 4112(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 4116(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 4120(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 4124(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_154
	jmp	.LBB6_220
	.p2align	4, 0x90
.LBB6_219:                              # %vector.body817
                                        #   in Loop: Header=BB6_58 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4096(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4112(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4128(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4144(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4160(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4176(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4192(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4208(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4224(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4240(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4256(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4272(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4288(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4304(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4320(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4336(%r12)
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
.LBB6_220:                              # %vector.memcheck808
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 25(%rsp)            # 1-byte Folded Reload
	je	.LBB6_276
# BB#221:                               # %.preheader.2.i97.3161.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_222:                              # %.preheader.2.i97.3161
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %ecx
	movl	%ecx, 4352(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 4356(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 4360(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 4364(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 4368(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 4372(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 4376(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 4380(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_222
	jmp	.LBB6_223
	.p2align	4, 0x90
.LBB6_276:                              # %vector.body795
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4352(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4368(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4384(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4400(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4416(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4432(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4448(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4464(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4480(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4496(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4512(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4528(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4544(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4560(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4576(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4592(%r12)
.LBB6_223:                              # %vector.memcheck786
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 24(%rsp)            # 1-byte Folded Reload
	je	.LBB6_225
# BB#224:                               # %.preheader.3.i104.preheader162.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_155:                              # %.preheader.3.i104.preheader162
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 5376(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5380(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5384(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5388(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5392(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5396(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5400(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 5404(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_155
	jmp	.LBB6_156
	.p2align	4, 0x90
.LBB6_225:                              # %vector.body773
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5376(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5392(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5408(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5424(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5440(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5456(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5472(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5488(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5504(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5520(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5536(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5552(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5568(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5584(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5600(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5616(%r12)
.LBB6_156:                              # %vector.memcheck764
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 23(%rsp)            # 1-byte Folded Reload
	je	.LBB6_159
# BB#157:                               # %.preheader.3.i104.1163.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_158:                              # %.preheader.3.i104.1163
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %ecx
	movl	%ecx, 5632(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 5636(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 5640(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 5644(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 5648(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 5652(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 5656(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 5660(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_158
	jmp	.LBB6_160
	.p2align	4, 0x90
.LBB6_159:                              # %vector.body751
                                        #   in Loop: Header=BB6_58 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5632(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5648(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5664(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5680(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5696(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5712(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5728(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5744(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5760(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5776(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5792(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5808(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5824(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5840(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5856(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5872(%r12)
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
.LBB6_160:                              # %vector.memcheck742
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 22(%rsp)            # 1-byte Folded Reload
	je	.LBB6_226
# BB#161:                               # %.preheader.3.i104.2164.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_162:                              # %.preheader.3.i104.2164
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %ecx
	movl	%ecx, 5888(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 5892(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 5896(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 5900(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 5904(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 5908(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 5912(%r12,%rax,4)
	movl	(%rsi), %ecx
	movl	%ecx, 5916(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_162
	jmp	.LBB6_227
	.p2align	4, 0x90
.LBB6_226:                              # %vector.body729
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5888(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5904(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5920(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5936(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5952(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5968(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 5984(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6000(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6016(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6032(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6048(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6064(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6080(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6096(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6112(%r12)
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 6128(%r12)
.LBB6_227:                              # %vector.memcheck720
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 21(%rsp)            # 1-byte Folded Reload
	je	.LBB6_229
# BB#228:                               # %.preheader.4.i111.preheader165.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_163:                              # %.preheader.4.i111.preheader165
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ecx
	movl	%ecx, 7168(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7172(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7176(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7180(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7184(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7188(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7192(%r12,%rax,4)
	movl	(%rdi), %ecx
	movl	%ecx, 7196(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_163
	jmp	.LBB6_164
	.p2align	4, 0x90
.LBB6_229:                              # %vector.body707
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7168(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7184(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7200(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7216(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7232(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7248(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7264(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7280(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7296(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7312(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7328(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7344(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7360(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7376(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7392(%r12)
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7408(%r12)
.LBB6_164:                              # %vector.memcheck698
                                        #   in Loop: Header=BB6_58 Depth=1
	cmpb	$0, 20(%rsp)            # 1-byte Folded Reload
	je	.LBB6_167
# BB#165:                               # %.preheader.4.i111.1166.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	movq	56(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_166:                              # %.preheader.4.i111.1166
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %ecx
	movl	%ecx, 7424(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 7428(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 7432(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 7436(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 7440(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 7444(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 7448(%r12,%rax,4)
	movl	(%rdx), %ecx
	movl	%ecx, 7452(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_166
	jmp	.LBB6_168
	.p2align	4, 0x90
.LBB6_167:                              # %vector.body685
                                        #   in Loop: Header=BB6_58 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7424(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7440(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7456(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7472(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7488(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7504(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7520(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7536(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7552(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7568(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7584(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7600(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7616(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7632(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7648(%r12)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 7664(%r12)
.LBB6_168:                              # %vector.memcheck676
                                        #   in Loop: Header=BB6_58 Depth=1
	testb	%r8b, %r8b
	je	.LBB6_230
# BB#169:                               # %.preheader.5.i120.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_170:                              # %.preheader.5.i120
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13), %ecx
	movl	%ecx, 8960(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8964(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8968(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8972(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8976(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8980(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8984(%r12,%rax,4)
	movl	(%r13), %ecx
	movl	%ecx, 8988(%r12,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB6_170
	jmp	.LBB6_231
	.p2align	4, 0x90
.LBB6_230:                              # %vector.body663
                                        #   in Loop: Header=BB6_58 Depth=1
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 8960(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 8976(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 8992(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9008(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9024(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9040(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9056(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9072(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9088(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9104(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9120(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9136(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9152(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9168(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9184(%r12)
	movd	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 9200(%r12)
.LBB6_231:                              # %.lr.ph.i72.preheader.preheader
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	$12, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_100:                              # %.lr.ph.i72.preheader
                                        #   Parent Loop BB6_58 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_101 Depth 3
	movq	%rcx, %rdx
	shlq	$8, %rdx
	movl	(%r12,%rdx), %esi
	movl	%esi, (%r15,%rdx)
	movq	%rdi, %rax
	movl	$63, %ebp
	.p2align	4, 0x90
.LBB6_101:                              #   Parent Loop BB6_58 Depth=1
                                        #     Parent Loop BB6_100 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addl	-8(%r12,%rax), %esi
	movl	%esi, -8(%r15,%rax)
	addl	-4(%r12,%rax), %esi
	movl	%esi, -4(%r15,%rax)
	addl	(%r12,%rax), %esi
	movl	%esi, (%r15,%rax)
	addq	$12, %rax
	addq	$-3, %rbp
	jne	.LBB6_101
# BB#102:                               #   in Loop: Header=BB6_100 Depth=2
	movl	252(%r15,%rdx), %eax
	movl	%eax, (%r11,%rcx,4)
	incq	%rcx
	addq	$256, %rdi              # imm = 0x100
	cmpq	$6, %rcx
	jne	.LBB6_100
# BB#103:                               # %._crit_edge13.i81
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	1792(%r12), %eax
	movl	%eax, 1792(%r15)
	movl	$451, %ecx              # imm = 0x1C3
	.p2align	4, 0x90
.LBB6_104:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$514, %rcx              # imm = 0x202
	jne	.LBB6_104
# BB#105:                               # %._crit_edge13.i81.112181224
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	2044(%r15), %eax
	movl	%eax, 28(%r11)
	movl	2048(%r12), %eax
	movl	%eax, 2048(%r15)
	movl	$515, %ecx              # imm = 0x203
	movq	224(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_106:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$578, %rcx              # imm = 0x242
	jne	.LBB6_106
# BB#107:                               # %._crit_edge13.i81.212201225
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	2300(%r15), %eax
	movl	%eax, 32(%r11)
	movl	2304(%r12), %eax
	movl	%eax, 2304(%r15)
	movl	$579, %ecx              # imm = 0x243
	.p2align	4, 0x90
.LBB6_108:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$642, %rcx              # imm = 0x282
	jne	.LBB6_108
# BB#109:                               # %._crit_edge13.i81.312221226
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	2556(%r15), %eax
	movl	%eax, 36(%r11)
	movl	2560(%r12), %eax
	movl	%eax, 2560(%r15)
	movl	$643, %ecx              # imm = 0x283
	.p2align	4, 0x90
.LBB6_110:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$706, %rcx              # imm = 0x2C2
	jne	.LBB6_110
# BB#111:                               # %._crit_edge13.i81.412231227
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	2812(%r15), %eax
	movl	%eax, 40(%r11)
	movl	2816(%r12), %eax
	movl	%eax, 2816(%r15)
	movl	$707, %ecx              # imm = 0x2C3
	.p2align	4, 0x90
.LBB6_112:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$770, %rcx              # imm = 0x302
	jne	.LBB6_112
# BB#113:                               # %._crit_edge13.i81.1
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	3068(%r15), %eax
	movl	%eax, 44(%r11)
	movl	3584(%r12), %eax
	movl	%eax, 3584(%r15)
	movl	$899, %ecx              # imm = 0x383
	.p2align	4, 0x90
.LBB6_114:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$962, %rcx              # imm = 0x3C2
	jne	.LBB6_114
# BB#115:                               # %._crit_edge13.i81.1.11231
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	3836(%r15), %eax
	movl	%eax, 56(%r11)
	movl	3840(%r12), %eax
	movl	%eax, 3840(%r15)
	movl	$963, %ecx              # imm = 0x3C3
	.p2align	4, 0x90
.LBB6_116:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1026, %rcx             # imm = 0x402
	jne	.LBB6_116
# BB#117:                               # %._crit_edge13.i81.1.21232
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	4092(%r15), %eax
	movl	%eax, 60(%r11)
	movl	4096(%r12), %eax
	movl	%eax, 4096(%r15)
	movl	$1027, %ecx             # imm = 0x403
	.p2align	4, 0x90
.LBB6_118:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1090, %rcx             # imm = 0x442
	jne	.LBB6_118
# BB#119:                               # %._crit_edge13.i81.1.31233
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	4348(%r15), %eax
	movl	%eax, 64(%r11)
	movl	4352(%r12), %eax
	movl	%eax, 4352(%r15)
	movl	$1091, %ecx             # imm = 0x443
	.p2align	4, 0x90
.LBB6_120:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1154, %rcx             # imm = 0x482
	jne	.LBB6_120
# BB#121:                               # %._crit_edge13.i81.2
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	4604(%r15), %eax
	movl	%eax, 68(%r11)
	movl	5376(%r12), %eax
	movl	%eax, 5376(%r15)
	movl	$1347, %ecx             # imm = 0x543
	.p2align	4, 0x90
.LBB6_122:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1410, %rcx             # imm = 0x582
	jne	.LBB6_122
# BB#123:                               # %._crit_edge13.i81.2.11237
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	5628(%r15), %eax
	movl	%eax, 84(%r11)
	movl	5632(%r12), %eax
	movl	%eax, 5632(%r15)
	movl	$1411, %ecx             # imm = 0x583
	.p2align	4, 0x90
.LBB6_124:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1474, %rcx             # imm = 0x5C2
	jne	.LBB6_124
# BB#125:                               # %._crit_edge13.i81.2.21238
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	5884(%r15), %eax
	movl	%eax, 88(%r11)
	movl	5888(%r12), %eax
	movl	%eax, 5888(%r15)
	movl	$1475, %ecx             # imm = 0x5C3
	.p2align	4, 0x90
.LBB6_126:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1538, %rcx             # imm = 0x602
	jne	.LBB6_126
# BB#127:                               # %._crit_edge13.i81.3
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	6140(%r15), %eax
	movl	%eax, 92(%r11)
	movl	7168(%r12), %eax
	movl	%eax, 7168(%r15)
	movl	$1795, %ecx             # imm = 0x703
	.p2align	4, 0x90
.LBB6_128:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1858, %rcx             # imm = 0x742
	jne	.LBB6_128
# BB#129:                               # %._crit_edge13.i81.3.11241
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	7420(%r15), %eax
	movl	%eax, 112(%r11)
	movl	7424(%r12), %eax
	movl	%eax, 7424(%r15)
	movl	$1859, %ecx             # imm = 0x743
	.p2align	4, 0x90
.LBB6_130:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$1922, %rcx             # imm = 0x782
	jne	.LBB6_130
# BB#131:                               # %._crit_edge13.i81.41242
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	7676(%r15), %eax
	movl	%eax, 116(%r11)
	movl	8960(%r12), %eax
	movl	%eax, 8960(%r15)
	movl	$2243, %ecx             # imm = 0x8C3
	.p2align	4, 0x90
.LBB6_132:                              #   Parent Loop BB6_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	-8(%r12,%rcx,4), %eax
	movl	%eax, -8(%r15,%rcx,4)
	addl	-4(%r12,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	addl	(%r12,%rcx,4), %eax
	movl	%eax, (%r15,%rcx,4)
	addq	$3, %rcx
	cmpq	$2306, %rcx             # imm = 0x902
	jne	.LBB6_132
# BB#133:                               # %._crit_edge13.i81.5
                                        #   in Loop: Header=BB6_58 Depth=1
	movl	9212(%r15), %eax
	movl	%eax, 140(%r11)
	movl	(%r11), %r10d
	movq	192(%rsp), %rdi         # 8-byte Reload
	movl	%r10d, (%rdi)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	4(%rax), %r11d
	movl	%r11d, 4(%rdi)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	8(%rax), %eax
	movl	%eax, 8(%rdi)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	12(%rcx), %ecx
	movl	%ecx, 12(%rdi)
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	16(%rdx), %edx
	movl	%edx, 16(%rdi)
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	20(%rsi), %esi
	movl	%esi, 20(%rdi)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	28(%rsi), %r10d
	movl	%r10d, 28(%rdi)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	32(%rsi), %r11d
	movl	%r11d, 32(%rdi)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	36(%rsi), %eax
	movl	%eax, 36(%rdi)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	40(%rsi), %ecx
	movl	%ecx, 40(%rdi)
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	44(%rsi), %edx
	movl	%edx, 44(%rdi)
	movq	8(%rsp), %rdx           # 8-byte Reload
	addl	56(%rdx), %r10d
	movl	%r10d, 56(%rdi)
	movq	8(%rsp), %rdx           # 8-byte Reload
	addl	60(%rdx), %r11d
	movl	%r11d, 60(%rdi)
	movq	8(%rsp), %rdx           # 8-byte Reload
	addl	64(%rdx), %eax
	movl	%eax, 64(%rdi)
	movq	8(%rsp), %rdx           # 8-byte Reload
	addl	68(%rdx), %ecx
	movl	%ecx, 68(%rdi)
	movq	8(%rsp), %rcx           # 8-byte Reload
	addl	84(%rcx), %r10d
	movl	%r10d, 84(%rdi)
	movq	8(%rsp), %rcx           # 8-byte Reload
	addl	88(%rcx), %r11d
	movl	%r11d, 88(%rdi)
	movq	8(%rsp), %rcx           # 8-byte Reload
	addl	92(%rcx), %eax
	movl	%eax, 92(%rdi)
	movq	8(%rsp), %rax           # 8-byte Reload
	addl	112(%rax), %r10d
	movl	%r10d, 112(%rdi)
	movq	8(%rsp), %rax           # 8-byte Reload
	addl	116(%rax), %r11d
	movl	%r11d, 116(%rdi)
	movq	8(%rsp), %r11           # 8-byte Reload
	addl	140(%r11), %r10d
	movl	%r10d, 140(%rdi)
	incl	%r9d
	cmpl	$10000, %r9d            # imm = 0x2710
	movq	216(%rsp), %rdx         # 8-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	176(%rsp), %rbx         # 8-byte Reload
	movq	168(%rsp), %rbp         # 8-byte Reload
	movq	200(%rsp), %r10         # 8-byte Reload
	jne	.LBB6_58
# BB#134:                               # %.preheader.i136.preheader
	xorl	%ecx, %ecx
	movl	$20, %eax
	movapd	.LCPI6_0(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	xorl	%edx, %edx
	movq	192(%rsp), %r13         # 8-byte Reload
	movq	184(%rsp), %rbx         # 8-byte Reload
.LBB6_135:                              # %.preheader.i136
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	-20(%rbx,%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	-20(%r13,%rax), %xmm1
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_138
# BB#136:                               #   in Loop: Header=BB6_135 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	-16(%rbx,%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	-16(%r13,%rax), %xmm1
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_137
# BB#171:                               #   in Loop: Header=BB6_135 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	-12(%rbx,%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	-12(%r13,%rax), %xmm1
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_172
# BB#173:                               #   in Loop: Header=BB6_135 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	-8(%rbx,%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	-8(%r13,%rax), %xmm1
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_174
# BB#175:                               #   in Loop: Header=BB6_135 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	-4(%rbx,%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	-4(%r13,%rax), %xmm1
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_176
# BB#177:                               #   in Loop: Header=BB6_135 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	(%rbx,%rax), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	(%r13,%rax), %xmm1
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_178
# BB#179:                               #   in Loop: Header=BB6_135 Depth=1
	incq	%rdx
	addq	$24, %rax
	cmpq	$6, %rdx
	jl	.LBB6_135
# BB#180:                               # %.preheader.i139.preheader
	movq	$-36, %rbp
	.p2align	4, 0x90
.LBB6_181:                              # %.preheader.i139
                                        # =>This Inner Loop Header: Depth=1
	movq	stderr(%rip), %rdi
	movl	144(%r13,%rbp,4), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	leaq	36(%rbp), %rax
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.LBB6_183
# BB#182:                               #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB6_183:                              #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rdi
	movl	148(%r13,%rbp,4), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	leaq	37(%rbp), %rax
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.LBB6_185
# BB#184:                               #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB6_185:                              #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rdi
	movl	152(%r13,%rbp,4), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	leaq	38(%rbp), %rax
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.LBB6_187
# BB#186:                               #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB6_187:                              #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rdi
	movl	156(%r13,%rbp,4), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	leaq	39(%rbp), %rax
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.LBB6_189
# BB#188:                               #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB6_189:                              #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rdi
	movl	160(%r13,%rbp,4), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	leaq	40(%rbp), %rax
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.LBB6_191
# BB#190:                               #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB6_191:                              #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rdi
	movl	164(%r13,%rbp,4), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	leaq	41(%rbp), %rax
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB6_192
# BB#193:                               #   in Loop: Header=BB6_181 Depth=1
	addq	$6, %rbp
	jne	.LBB6_181
	jmp	.LBB6_194
.LBB6_192:                              #   in Loop: Header=BB6_181 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	addq	$6, %rbp
	jne	.LBB6_181
.LBB6_194:                              # %print_array.exit
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	%r14, %rdi
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_195
.LBB6_137:
	movl	$1, %ecx
	jmp	.LBB6_138
.LBB6_172:
	movl	$2, %ecx
	jmp	.LBB6_138
.LBB6_174:
	movl	$3, %ecx
	jmp	.LBB6_138
.LBB6_176:
	movl	$4, %ecx
	jmp	.LBB6_138
.LBB6_178:
	movl	$5, %ecx
.LBB6_138:                              # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_195:
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_268:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	init_array,@function
init_array:                             # @init_array
	.cfi_startproc
# BB#0:                                 # %.preheader
	movl	$1, (%rdi)
	movl	$0, (%rsi)
	movl	$0, (%rdx)
	movl	$2, 4(%rdi)
	movl	$0, 4(%rsi)
	movl	$0, 4(%rdx)
	movl	$3, 8(%rdi)
	movl	$0, 8(%rsi)
	movl	$0, 8(%rdx)
	movl	$4, 12(%rdi)
	movl	$0, 12(%rsi)
	movl	$0, 12(%rdx)
	movl	$5, 16(%rdi)
	movl	$0, 16(%rsi)
	movl	$0, 16(%rdx)
	movl	$6, 20(%rdi)
	movl	$0, 20(%rsi)
	movl	$0, 20(%rdx)
	movl	$2, 24(%rdi)
	movl	$0, 24(%rsi)
	movl	$0, 24(%rdx)
	movl	$4, 28(%rdi)
	movl	$0, 28(%rsi)
	movl	$0, 28(%rdx)
	movl	$6, 32(%rdi)
	movl	$0, 32(%rsi)
	movl	$0, 32(%rdx)
	movl	$8, 36(%rdi)
	movl	$0, 36(%rsi)
	movl	$0, 36(%rdx)
	movl	$10, 40(%rdi)
	movl	$0, 40(%rsi)
	movl	$0, 40(%rdx)
	movl	$12, 44(%rdi)
	movl	$0, 44(%rsi)
	movl	$0, 44(%rdx)
	movl	$3, 48(%rdi)
	movl	$0, 48(%rsi)
	movl	$0, 48(%rdx)
	movl	$6, 52(%rdi)
	movl	$0, 52(%rsi)
	movl	$0, 52(%rdx)
	movl	$9, 56(%rdi)
	movl	$0, 56(%rsi)
	movl	$0, 56(%rdx)
	movl	$12, 60(%rdi)
	movl	$0, 60(%rsi)
	movl	$0, 60(%rdx)
	movl	$15, 64(%rdi)
	movl	$0, 64(%rsi)
	movl	$1, 64(%rdx)
	movl	$18, 68(%rdi)
	movl	$0, 68(%rsi)
	movl	$1, 68(%rdx)
	movl	$4, 72(%rdi)
	movl	$0, 72(%rsi)
	movl	$0, 72(%rdx)
	movl	$8, 76(%rdi)
	movl	$0, 76(%rsi)
	movl	$0, 76(%rdx)
	movl	$12, 80(%rdi)
	movl	$0, 80(%rsi)
	movl	$0, 80(%rdx)
	movl	$16, 84(%rdi)
	movl	$0, 84(%rsi)
	movl	$1, 84(%rdx)
	movl	$20, 88(%rdi)
	movl	$0, 88(%rsi)
	movl	$1, 88(%rdx)
	movl	$24, 92(%rdi)
	movl	$0, 92(%rsi)
	movl	$2, 92(%rdx)
	movl	$5, 96(%rdi)
	movl	$0, 96(%rsi)
	movl	$0, 96(%rdx)
	movl	$10, 100(%rdi)
	movl	$0, 100(%rsi)
	movl	$0, 100(%rdx)
	movl	$15, 104(%rdi)
	movl	$0, 104(%rsi)
	movl	$0, 104(%rdx)
	movl	$20, 108(%rdi)
	movl	$0, 108(%rsi)
	movl	$1, 108(%rdx)
	movl	$25, 112(%rdi)
	movl	$0, 112(%rsi)
	movl	$2, 112(%rdx)
	movl	$30, 116(%rdi)
	movl	$0, 116(%rsi)
	movl	$2, 116(%rdx)
	movl	$6, 120(%rdi)
	movl	$0, 120(%rsi)
	movl	$0, 120(%rdx)
	movl	$12, 124(%rdi)
	movl	$0, 124(%rsi)
	movl	$0, 124(%rdx)
	movl	$18, 128(%rdi)
	movl	$0, 128(%rsi)
	movl	$0, 128(%rdx)
	movl	$24, 132(%rdi)
	movl	$0, 132(%rsi)
	movl	$1, 132(%rdx)
	movl	$30, 136(%rdi)
	movl	$0, 136(%rsi)
	movl	$2, 136(%rdx)
	movl	$36, 140(%rdi)
	movl	$0, 140(%rsi)
	movl	$3, 140(%rdx)
	retq
.Lfunc_end7:
	.size	init_array, .Lfunc_end7-init_array
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d "
	.size	.L.str.3, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
