	.text
	.file	"mySplitCommandLine.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_Z18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEE
	.p2align	4, 0x90
	.type	_Z18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEE,@function
_Z18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEE: # @_Z18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movl	%edi, %r12d
	movq	$0, 32(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbp
	movq	%rbp, 24(%rsp)
	movb	$0, (%rbp)
	movl	$4, 36(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp0:
	movl	$4, %edi
	callq	_Znam
.Ltmp1:
# BB#1:
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	$4, 12(%rsp)
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	(%rbx), %rbp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movl	$-1, %ebx
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_2
# BB#3:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	1(%rbx), %r14d
	movslq	%r14d, %rax
	cmpl	$-1, %ebx
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp3:
	callq	_Znam
.Ltmp4:
# BB#4:                                 # %.noexc
	movq	%rax, 48(%rsp)
	movb	$0, (%rax)
	movl	%r14d, 60(%rsp)
	.p2align	4, 0x90
.LBB0_5:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %ecx
	incq	%rbp
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB0_5
# BB#6:
	movl	%ebx, 56(%rsp)
.Ltmp6:
	leaq	48(%rsp), %rdi
	leaq	24(%rsp), %rsi
	movq	%rsp, %rdx
	callq	_Z21my_windows_split_pathRK11CStringBaseIcERS0_S3_
.Ltmp7:
# BB#7:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#8:
	callq	_ZdaPv
.LBB0_9:                                # %_ZN11CStringBaseIcED2Ev.exit
	movl	%r12d, 44(%rsp)         # 4-byte Spill
	movq	24(%rsp), %rcx
	movl	$_ZZ18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEEE14p7zip_home_dir, %edi
	movl	$4096, %esi             # imm = 0x1000
	movl	$.L.str, %edx
	xorl	%eax, %eax
	callq	snprintf
	movb	$0, _ZZ18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEEE14p7zip_home_dir+4095(%rip)
	movl	$_ZZ18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEEE14p7zip_home_dir, %edi
	callq	putenv
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_11
# BB#10:
	callq	_ZdaPv
.LBB0_11:                               # %_ZN11CStringBaseIcED2Ev.exit49
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
# BB#12:
	callq	_ZdaPv
.LBB0_13:                               # %_ZN11CStringBaseIcED2Ev.exit50
	movl	$6, %edi
	movl	$.L.str.1, %esi
	callq	setlocale
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	setlocale
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_25
# BB#14:
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %r14
	leaq	1(%r14), %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_25
# BB#15:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	strcpy
	testq	%r14, %r14
	je	.LBB0_20
# BB#16:
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph79
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ebp
	movl	%ebp, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_17 Depth=1
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movl	(%rax,%rbp,4), %ebp
.LBB0_19:                               # %toupper.exit
                                        #   in Loop: Header=BB0_17 Depth=1
	movb	%bpl, (%rbx)
	incq	%rbx
	decq	%r14
	jne	.LBB0_17
.LBB0_20:                               # %._crit_edge80
	cmpb	$0, (%r12)
	je	.LBB0_24
# BB#21:
	movl	$.L.str.2, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_24
# BB#22:
	movl	$.L.str.3, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_24
# BB#23:
	movl	$1, global_use_utf16_conversion(%rip)
.LBB0_24:
	movq	%r12, %rdi
	callq	free
.LBB0_25:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	44(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	64(%rsp), %rbx          # 8-byte Reload
	jle	.LBB0_61
# BB#26:                                # %.lr.ph
	movslq	%eax, %r13
	xorl	%r12d, %r12d
	jmp	.LBB0_27
.LBB0_39:                               #   in Loop: Header=BB0_27 Depth=1
	movl	$1, global_use_utf16_conversion(%rip)
	jmp	.LBB0_60
	.p2align	4, 0x90
.LBB0_27:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_41 Depth 2
                                        #     Child Loop BB0_43 Depth 2
                                        #     Child Loop BB0_54 Depth 2
	movq	(%rbx,%r12,8), %rbp
	cmpq	$2, %r12
	jg	.LBB0_40
# BB#28:                                #   in Loop: Header=BB0_27 Depth=1
	movl	$.L.str.4, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_29
# BB#38:                                #   in Loop: Header=BB0_27 Depth=1
	movl	$.L.str.5, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_39
.LBB0_40:                               # %.critedge
                                        #   in Loop: Header=BB0_27 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebx
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB0_41:                               #   Parent Loop BB0_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_41
# BB#42:                                # %_Z11MyStringLenIcEiPKT_.exit.i56
                                        #   in Loop: Header=BB0_27 Depth=1
	leal	1(%rbx), %eax
	movslq	%eax, %r14
	cmpl	$-1, %ebx
	movq	%r14, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	%r14d, 12(%rsp)
	.p2align	4, 0x90
.LBB0_43:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i59
                                        #   Parent Loop BB0_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %ecx
	incq	%rbp
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB0_43
# BB#44:                                # %_ZN11CStringBaseIcEC2EPKc.exit60
                                        #   in Loop: Header=BB0_27 Depth=1
	movl	%ebx, 8(%rsp)
.Ltmp9:
	xorl	%edx, %edx
	leaq	24(%rsp), %rdi
	movq	%rsp, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp10:
# BB#45:                                #   in Loop: Header=BB0_27 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_47
# BB#46:                                #   in Loop: Header=BB0_27 Depth=1
	callq	_ZdaPv
.LBB0_47:                               # %_ZN11CStringBaseIcED2Ev.exit61
                                        #   in Loop: Header=BB0_27 Depth=1
	movslq	32(%rsp), %r14
	testq	%r14, %r14
	je	.LBB0_57
# BB#48:                                #   in Loop: Header=BB0_27 Depth=1
.Ltmp12:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp13:
# BB#49:                                # %.noexc63
                                        #   in Loop: Header=BB0_27 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	leaq	1(%r14), %rbx
	testl	%ebx, %ebx
	je	.LBB0_50
# BB#51:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp14:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp15:
# BB#52:                                # %.noexc.i
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	%rax, (%rbp)
	movl	$0, (%rax)
	movl	%ebx, 12(%rbp)
	jmp	.LBB0_53
.LBB0_29:                               #   in Loop: Header=BB0_27 Depth=1
	movl	$0, global_use_utf16_conversion(%rip)
	jmp	.LBB0_60
.LBB0_50:                               #   in Loop: Header=BB0_27 Depth=1
	xorl	%eax, %eax
.LBB0_53:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	24(%rsp), %rcx
	.p2align	4, 0x90
.LBB0_54:                               #   Parent Loop BB0_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB0_54
# BB#55:                                #   in Loop: Header=BB0_27 Depth=1
	movl	%r14d, 8(%rbp)
.Ltmp17:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp18:
# BB#56:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
.LBB0_57:                               #   in Loop: Header=BB0_27 Depth=1
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_59
# BB#58:                                #   in Loop: Header=BB0_27 Depth=1
	callq	_ZdaPv
.LBB0_59:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	64(%rsp), %rbx          # 8-byte Reload
.LBB0_60:                               #   in Loop: Header=BB0_27 Depth=1
	incq	%r12
	cmpq	%r13, %r12
	jl	.LBB0_27
.LBB0_61:                               # %._crit_edge
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_32:
.Ltmp8:
	movq	%rax, %rbx
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_34
# BB#33:
	callq	_ZdaPv
	jmp	.LBB0_34
.LBB0_31:
.Ltmp5:
	movq	%rax, %rbx
.LBB0_34:                               # %_ZN11CStringBaseIcED2Ev.exit51
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_36
# BB#35:
	callq	_ZdaPv
.LBB0_36:
	movq	24(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB0_67
# BB#37:
	movq	%rbp, %rdi
	jmp	.LBB0_66
.LBB0_30:                               # %.thread
.Ltmp2:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	jmp	.LBB0_66
.LBB0_68:
.Ltmp16:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB0_64
.LBB0_62:
.Ltmp11:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_66
	jmp	.LBB0_67
.LBB0_63:
.Ltmp19:
	movq	%rax, %rbx
.LBB0_64:                               # %.body
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_67
.LBB0_66:
	callq	_ZdaPv
.LBB0_67:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEE, .Lfunc_end0-_Z18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\205\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Lfunc_end0-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z12my_getlocalev
	.p2align	4, 0x90
	.type	_Z12my_getlocalev,@function
_Z12my_getlocalev:                      # @_Z12my_getlocalev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	setlocale
	testq	%rax, %rax
	movl	$.L.str.2, %ecx
	cmoveq	%rcx, %rax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	_Z12my_getlocalev, .Lfunc_end1-_Z12my_getlocalev
	.cfi_endproc

	.type	_ZZ18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEEE14p7zip_home_dir,@object # @_ZZ18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEEE14p7zip_home_dir
	.local	_ZZ18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEEE14p7zip_home_dir
	.comm	_ZZ18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEEE14p7zip_home_dir,4096,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"P7ZIP_HOME_DIR=%s/"
	.size	.L.str, 19

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.zero	1
	.size	.L.str.1, 1

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"C"
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"POSIX"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"-no-utf16"
	.size	.L.str.4, 10

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"-utf16"
	.size	.L.str.5, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
