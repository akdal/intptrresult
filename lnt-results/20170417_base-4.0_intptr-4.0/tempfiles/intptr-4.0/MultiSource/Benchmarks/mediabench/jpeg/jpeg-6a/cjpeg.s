	.text
	.file	"cjpeg.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$664, %rsp              # imm = 0x298
.Lcfi6:
	.cfi_def_cfa_offset 720
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r15d
	movq	(%r14), %rax
	movq	%rax, progname(%rip)
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	cmpb	$0, (%rax)
	jne	.LBB0_3
.LBB0_2:
	movq	$.L.str, progname(%rip)
.LBB0_3:
	leaq	496(%rsp), %rdi
	callq	jpeg_std_error
	movq	%rax, (%rsp)
	movq	%rsp, %rbx
	movl	$61, %esi
	movl	$496, %edx              # imm = 0x1F0
	movq	%rbx, %rdi
	callq	jpeg_CreateCompress
	movq	$cdjpeg_message_table, 648(%rsp)
	movl	$1000, 656(%rsp)        # imm = 0x3E8
	movl	$1043, 660(%rsp)        # imm = 0x413
	movl	$2, 52(%rsp)
	movq	%rbx, %rdi
	callq	jpeg_set_defaults
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r14, %rdx
	callq	parse_switches
	leal	-1(%r15), %ecx
	cmpl	%ecx, %eax
	jl	.LBB0_37
# BB#4:
	cmpl	%r15d, %eax
	jge	.LBB0_7
# BB#5:
	movslq	%eax, %rbx
	movq	(%r14,%rbx,8), %rdi
	movl	$.L.str.2, %esi
	callq	fopen
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB0_8
# BB#6:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	(%r14,%rbx,8), %rcx
	jmp	.LBB0_11
.LBB0_7:
	callq	read_stdin
	movq	%rax, %r13
.LBB0_8:
	movq	outfilename(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_12
# BB#9:
	movl	$.L.str.4, %esi
	callq	fopen
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB0_13
# BB#10:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	outfilename(%rip), %rcx
.LBB0_11:
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_12:
	callq	write_stdout
	movq	%rax, %r12
.LBB0_13:
	cmpb	$1, is_targa(%rip)
	jne	.LBB0_24
.LBB0_14:
	movq	%rsp, %rdi
	callq	jinit_read_targa
.LBB0_15:                               # %select_file_type.exit
	movq	%rax, %rbx
.LBB0_16:                               # %select_file_type.exit
	movq	%r13, 24(%rbx)
	movq	%rsp, %rbp
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*(%rbx)
	movq	%rbp, %rdi
	callq	jpeg_default_colorspace
	movl	$1, %ecx
	movq	%rbp, %rdi
	movl	%r15d, %esi
	movq	%r14, %rdx
	callq	parse_switches
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	jpeg_stdio_dest
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	jpeg_start_compress
	movl	296(%rsp), %eax
	cmpl	44(%rsp), %eax
	jae	.LBB0_19
# BB#17:                                # %.lr.ph
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB0_18:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*8(%rbx)
	movq	32(%rbx), %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	callq	jpeg_write_scanlines
	movl	296(%rsp), %eax
	cmpl	44(%rsp), %eax
	jb	.LBB0_18
.LBB0_19:                               # %._crit_edge
	movq	%rsp, %rbp
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*16(%rbx)
	movq	%rbp, %rdi
	callq	jpeg_finish_compress
	movq	%rbp, %rdi
	callq	jpeg_destroy_compress
	cmpq	stdin(%rip), %r13
	je	.LBB0_21
# BB#20:
	movq	%r13, %rdi
	callq	fclose
.LBB0_21:
	cmpq	stdout(%rip), %r12
	je	.LBB0_23
# BB#22:
	movq	%r12, %rdi
	callq	fclose
.LBB0_23:
	xorl	%edi, %edi
	cmpq	$0, 624(%rsp)
	setne	%dil
	addl	%edi, %edi
	callq	exit
.LBB0_24:
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	jne	.LBB0_26
# BB#25:
	movq	(%rsp), %rax
	movl	$41, 40(%rax)
	movq	%rsp, %rdi
	callq	*(%rax)
.LBB0_26:
	movl	%ebx, %edi
	movq	%r13, %rsi
	callq	ungetc
	cmpl	$-1, %eax
	jne	.LBB0_28
# BB#27:
	movq	(%rsp), %rax
	movl	$1040, 40(%rax)         # imm = 0x410
	movq	%rsp, %rdi
	callq	*(%rax)
.LBB0_28:
	cmpl	$70, %ebx
	jg	.LBB0_32
# BB#29:
	testl	%ebx, %ebx
	je	.LBB0_14
# BB#30:
	cmpl	$66, %ebx
	jne	.LBB0_36
# BB#31:
	movq	%rsp, %rdi
	callq	jinit_read_bmp
	jmp	.LBB0_15
.LBB0_32:
	cmpl	$71, %ebx
	je	.LBB0_35
# BB#33:
	cmpl	$80, %ebx
	jne	.LBB0_36
# BB#34:
	movq	%rsp, %rdi
	callq	jinit_read_ppm
	jmp	.LBB0_15
.LBB0_35:
	movq	%rsp, %rdi
	callq	jinit_read_gif
	jmp	.LBB0_15
.LBB0_36:
	movq	(%rsp), %rax
	movl	$1041, 40(%rax)         # imm = 0x411
	movq	%rsp, %rdi
	callq	*(%rax)
	xorl	%ebx, %ebx
	jmp	.LBB0_16
.LBB0_37:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	callq	usage
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	parse_switches,@function
parse_switches:                         # @parse_switches
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 144
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	movq	%rdx, %r14
	movl	%esi, %r15d
	movq	%rdi, %r13
	movl	$75, 36(%rsp)
	movb	$0, is_targa(%rip)
	movq	$0, outfilename(%rip)
	movq	(%r13), %rax
	movl	$0, 124(%rax)
	cmpl	$2, %r15d
	jl	.LBB1_1
# BB#2:                                 # %.lr.ph
	movl	$1, %ebp
	movl	$100, 28(%rsp)          # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jmp	.LBB1_3
.LBB1_9:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.50, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_18
# BB#10:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB1_85
# BB#11:                                #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebp, %rbx
	movq	(%r14,%rbx,8), %rdi
	movl	$.L.str.51, %esi
	movl	$1, %edx
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$0, 268(%r13)
	jmp	.LBB1_71
.LBB1_18:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.54, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	jne	.LBB1_20
# BB#19:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.55, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_23
.LBB1_20:                               #   in Loop: Header=BB1_3 Depth=1
	movzbl	parse_switches.printed_version(%rip), %eax
	testb	%al, %al
	je	.LBB1_21
.LBB1_22:                               #   in Loop: Header=BB1_3 Depth=1
	movq	(%r13), %rax
	incl	124(%rax)
	jmp	.LBB1_71
.LBB1_13:                               #   in Loop: Header=BB1_3 Depth=1
	movq	(%r14,%rbx,8), %rdi
	movl	$.L.str.52, %esi
	movl	$2, %edx
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$1, 268(%r13)
	jmp	.LBB1_71
.LBB1_15:                               #   in Loop: Header=BB1_3 Depth=1
	movq	(%r14,%rbx,8), %rdi
	movl	$.L.str.53, %esi
	movl	$2, %edx
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$2, 268(%r13)
	jmp	.LBB1_71
.LBB1_23:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.59, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	jne	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.60, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_26
.LBB1_25:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$1, %esi
	movq	%r13, %rdi
	callq	jpeg_set_colorspace
	jmp	.LBB1_71
.LBB1_21:                               #   in Loop: Header=BB1_3 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.56, %esi
	movl	$.L.str.57, %edx
	movl	$.L.str.58, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movb	$1, parse_switches.printed_version(%rip)
	jmp	.LBB1_22
.LBB1_26:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.61, %esi
	movl	$3, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_33
# BB#27:                                #   in Loop: Header=BB1_3 Depth=1
	movb	$120, 15(%rsp)
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB1_85
# BB#28:                                #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rdi
	movl	$.L.str.62, %esi
	xorl	%eax, %eax
	leaq	16(%rsp), %rdx
	leaq	15(%rsp), %rcx
	callq	sscanf
	testl	%eax, %eax
	jle	.LBB1_85
# BB#29:                                #   in Loop: Header=BB1_3 Depth=1
	movzbl	15(%rsp), %eax
	orb	$32, %al
	cmpb	$109, %al
	jne	.LBB1_30
# BB#31:                                #   in Loop: Header=BB1_3 Depth=1
	imulq	$1000, 16(%rsp), %rax   # imm = 0x3E8
	movq	%rax, 16(%rsp)
	jmp	.LBB1_32
.LBB1_33:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.63, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	jne	.LBB1_35
# BB#34:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.64, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_36
.LBB1_35:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$1, 256(%r13)
	jmp	.LBB1_71
.LBB1_30:                               # %._crit_edge177
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	16(%rsp), %rax
.LBB1_32:                               #   in Loop: Header=BB1_3 Depth=1
	imulq	$1000, %rax, %rax       # imm = 0x3E8
	movq	8(%r13), %rcx
	movq	%rax, 88(%rcx)
	jmp	.LBB1_71
.LBB1_36:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.65, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_39
# BB#37:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB1_85
# BB#38:                                #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, outfilename(%rip)
	jmp	.LBB1_71
.LBB1_39:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.66, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_41
# BB#40:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$1, 32(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_71
.LBB1_41:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.67, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_45
# BB#42:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB1_85
# BB#43:                                #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rdi
	movl	$.L.str.68, %esi
	xorl	%eax, %eax
	leaq	36(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB1_85
# BB#44:                                #   in Loop: Header=BB1_3 Depth=1
	movl	36(%rsp), %edi
	callq	jpeg_quality_scaling
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jmp	.LBB1_71
.LBB1_45:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.69, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_48
# BB#46:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB1_85
# BB#47:                                #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jmp	.LBB1_71
.LBB1_48:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.70, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_51
# BB#49:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB1_85
# BB#50:                                #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jmp	.LBB1_71
.LBB1_51:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.71, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_58
# BB#52:                                #   in Loop: Header=BB1_3 Depth=1
	movb	$120, 15(%rsp)
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB1_85
# BB#53:                                #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rdi
	movl	$.L.str.62, %esi
	xorl	%eax, %eax
	leaq	16(%rsp), %rdx
	leaq	15(%rsp), %rcx
	callq	sscanf
	testl	%eax, %eax
	jle	.LBB1_85
# BB#54:                                #   in Loop: Header=BB1_3 Depth=1
	movq	16(%rsp), %rax
	cmpq	$65536, %rax            # imm = 0x10000
	jae	.LBB1_85
# BB#55:                                #   in Loop: Header=BB1_3 Depth=1
	movzbl	15(%rsp), %ecx
	orb	$32, %cl
	cmpb	$98, %cl
	jne	.LBB1_57
# BB#56:                                #   in Loop: Header=BB1_3 Depth=1
	movl	%eax, 272(%r13)
	xorl	%eax, %eax
.LBB1_57:                               #   in Loop: Header=BB1_3 Depth=1
	movl	%eax, 276(%r13)
	jmp	.LBB1_71
.LBB1_58:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.72, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_61
# BB#59:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB1_85
# BB#60:                                #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jmp	.LBB1_71
.LBB1_61:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.73, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_64
# BB#62:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB1_85
# BB#63:                                #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	jmp	.LBB1_71
.LBB1_64:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.74, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_69
# BB#65:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB1_85
# BB#66:                                #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rdi
	movl	$.L.str.68, %esi
	xorl	%eax, %eax
	leaq	16(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB1_85
# BB#67:                                #   in Loop: Header=BB1_3 Depth=1
	movl	16(%rsp), %eax
	cmpl	$101, %eax
	jae	.LBB1_85
# BB#68:                                #   in Loop: Header=BB1_3 Depth=1
	movl	%eax, 264(%r13)
	jmp	.LBB1_71
.LBB1_69:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.75, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_85
# BB#70:                                #   in Loop: Header=BB1_3 Depth=1
	movb	$1, is_targa(%rip)
	jmp	.LBB1_71
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %r12
	cmpb	$45, (%r12)
	jne	.LBB1_4
# BB#6:                                 #   in Loop: Header=BB1_3 Depth=1
	incq	%r12
	movl	$.L.str.47, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	jne	.LBB1_84
# BB#7:                                 #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str.49, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_3 Depth=1
	movl	$1, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jmp	.LBB1_71
	.p2align	4, 0x90
.LBB1_4:                                #   in Loop: Header=BB1_3 Depth=1
	testl	%ebp, %ebp
	jg	.LBB1_72
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=1
	movq	$0, outfilename(%rip)
.LBB1_71:                               #   in Loop: Header=BB1_3 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jl	.LBB1_3
	jmp	.LBB1_72
.LBB1_1:
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$100, 28(%rsp)          # 4-byte Folded Spill
	movl	$1, %ebp
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
.LBB1_72:                               # %._crit_edge
	cmpl	$0, 84(%rsp)            # 4-byte Folded Reload
	movq	64(%rsp), %r14          # 8-byte Reload
	je	.LBB1_83
# BB#73:
	movl	36(%rsp), %esi
	movq	%r13, %rdi
	movq	72(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edx
	callq	jpeg_set_quality
	movq	48(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB1_75
# BB#74:
	movq	%r13, %rdi
	movl	28(%rsp), %edx          # 4-byte Reload
	movl	%ebx, %ecx
	callq	read_quant_tables
	testl	%eax, %eax
	je	.LBB1_85
.LBB1_75:
	movq	56(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB1_77
# BB#76:
	movq	%r13, %rdi
	callq	set_quant_slots
	testl	%eax, %eax
	je	.LBB1_85
.LBB1_77:
	movq	40(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB1_79
# BB#78:
	movq	%r13, %rdi
	callq	set_sample_factors
	testl	%eax, %eax
	je	.LBB1_85
.LBB1_79:
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	je	.LBB1_81
# BB#80:
	movq	%r13, %rdi
	callq	jpeg_simple_progression
.LBB1_81:
	testq	%r14, %r14
	je	.LBB1_83
# BB#82:
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	read_scan_script
	testl	%eax, %eax
	je	.LBB1_85
.LBB1_83:
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_84:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.48, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB1_85:
	callq	usage
.LBB1_17:
	callq	usage
.Lfunc_end1:
	.size	parse_switches, .Lfunc_end1-parse_switches
	.cfi_endproc

	.p2align	4, 0x90
	.type	usage,@function
usage:                                  # @usage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.76, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.77, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.78, %edi
	movl	$37, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.79, %edi
	movl	$68, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.80, %edi
	movl	$45, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.81, %edi
	movl	$77, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.82, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.83, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.84, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movl	$.L.str.85, %esi
	movl	$.L.str.86, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.87, %esi
	movl	$.L.str.88, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.89, %esi
	movl	$.L.str.88, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.90, %edi
	movl	$67, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.91, %edi
	movl	$62, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.92, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.93, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.94, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.95, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.96, %edi
	movl	$39, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.97, %edi
	movl	$55, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.98, %edi
	movl	$55, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.99, %edi
	movl	$52, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.100, %edi
	movl	$56, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	usage, .Lfunc_end2-usage
	.cfi_endproc

	.type	progname,@object        # @progname
	.local	progname
	.comm	progname,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cjpeg"
	.size	.L.str, 6

	.type	cdjpeg_message_table,@object # @cdjpeg_message_table
	.section	.rodata,"a",@progbits
	.p2align	4
cdjpeg_message_table:
	.quad	0
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.11
	.quad	.L.str.12
	.quad	.L.str.13
	.quad	.L.str.14
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.29
	.quad	.L.str.30
	.quad	.L.str.31
	.quad	.L.str.32
	.quad	.L.str.33
	.quad	.L.str.34
	.quad	.L.str.35
	.quad	.L.str.36
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	.L.str.43
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.46
	.quad	0
	.size	cdjpeg_message_table, 352

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%s: only one input file\n"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"rb"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s: can't open %s\n"
	.size	.L.str.3, 19

	.type	outfilename,@object     # @outfilename
	.local	outfilename
	.comm	outfilename,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"wb"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Unsupported BMP colormap format"
	.size	.L.str.5, 32

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Only 8- and 24-bit BMP files are supported"
	.size	.L.str.6, 43

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Invalid BMP file: bad header length"
	.size	.L.str.7, 36

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Invalid BMP file: biPlanes not equal to 1"
	.size	.L.str.8, 42

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"BMP output must be grayscale or RGB"
	.size	.L.str.9, 36

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Sorry, compressed BMPs not yet supported"
	.size	.L.str.10, 41

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Not a BMP file - does not start with BM"
	.size	.L.str.11, 40

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%ux%u 24-bit BMP image"
	.size	.L.str.12, 23

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%ux%u 8-bit colormapped BMP image"
	.size	.L.str.13, 34

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%ux%u 24-bit OS2 BMP image"
	.size	.L.str.14, 27

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"%ux%u 8-bit colormapped OS2 BMP image"
	.size	.L.str.15, 38

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"GIF output got confused"
	.size	.L.str.16, 24

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Bogus GIF codesize %d"
	.size	.L.str.17, 22

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"GIF output must be grayscale or RGB"
	.size	.L.str.18, 36

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Too few images in GIF file"
	.size	.L.str.19, 27

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Not a GIF file"
	.size	.L.str.20, 15

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%ux%ux%d GIF image"
	.size	.L.str.21, 19

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Warning: unexpected GIF version number '%c%c%c'"
	.size	.L.str.22, 48

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Ignoring GIF extension block of type 0x%02x"
	.size	.L.str.23, 44

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Caution: nonsquare pixels in input"
	.size	.L.str.24, 35

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Corrupt data in GIF file"
	.size	.L.str.25, 25

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Bogus char 0x%02x in GIF file, ignoring"
	.size	.L.str.26, 40

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Premature end of GIF image"
	.size	.L.str.27, 27

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Ran out of GIF bits"
	.size	.L.str.28, 20

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"PPM output must be grayscale or RGB"
	.size	.L.str.29, 36

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Nonnumeric data in PPM file"
	.size	.L.str.30, 28

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Not a PPM file"
	.size	.L.str.31, 15

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"%ux%u PGM image"
	.size	.L.str.32, 16

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%ux%u text PGM image"
	.size	.L.str.33, 21

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"%ux%u PPM image"
	.size	.L.str.34, 16

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"%ux%u text PPM image"
	.size	.L.str.35, 21

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Unsupported Targa colormap format"
	.size	.L.str.36, 34

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Invalid or unsupported Targa file"
	.size	.L.str.37, 34

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Targa output must be grayscale or RGB"
	.size	.L.str.38, 38

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"%ux%u RGB Targa image"
	.size	.L.str.39, 22

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"%ux%u grayscale Targa image"
	.size	.L.str.40, 28

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"%ux%u colormapped Targa image"
	.size	.L.str.41, 30

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"Color map file is invalid or of unsupported format"
	.size	.L.str.42, 51

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"Output file format cannot handle %d colormap entries"
	.size	.L.str.43, 53

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"ungetc failed"
	.size	.L.str.44, 14

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"Unrecognized input file format --- perhaps you need -targa"
	.size	.L.str.45, 59

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"Unsupported output file format"
	.size	.L.str.46, 31

	.type	is_targa,@object        # @is_targa
	.local	is_targa
	.comm	is_targa,1,4
	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"arithmetic"
	.size	.L.str.47, 11

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"%s: sorry, arithmetic coding not supported\n"
	.size	.L.str.48, 44

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"baseline"
	.size	.L.str.49, 9

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"dct"
	.size	.L.str.50, 4

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"int"
	.size	.L.str.51, 4

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"fast"
	.size	.L.str.52, 5

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"float"
	.size	.L.str.53, 6

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"debug"
	.size	.L.str.54, 6

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"verbose"
	.size	.L.str.55, 8

	.type	parse_switches.printed_version,@object # @parse_switches.printed_version
	.local	parse_switches.printed_version
	.comm	parse_switches.printed_version,1,4
	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"Independent JPEG Group's CJPEG, version %s\n%s\n"
	.size	.L.str.56, 47

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"6a  7-Feb-96"
	.size	.L.str.57, 13

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"Copyright (C) 1996, Thomas G. Lane"
	.size	.L.str.58, 35

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"grayscale"
	.size	.L.str.59, 10

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"greyscale"
	.size	.L.str.60, 10

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"maxmemory"
	.size	.L.str.61, 10

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"%ld%c"
	.size	.L.str.62, 6

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"optimize"
	.size	.L.str.63, 9

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"optimise"
	.size	.L.str.64, 9

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"outfile"
	.size	.L.str.65, 8

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"progressive"
	.size	.L.str.66, 12

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"quality"
	.size	.L.str.67, 8

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"%d"
	.size	.L.str.68, 3

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"qslots"
	.size	.L.str.69, 7

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"qtables"
	.size	.L.str.70, 8

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"restart"
	.size	.L.str.71, 8

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"sample"
	.size	.L.str.72, 7

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"scans"
	.size	.L.str.73, 6

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"smooth"
	.size	.L.str.74, 7

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"targa"
	.size	.L.str.75, 6

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"usage: %s [switches] "
	.size	.L.str.76, 22

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"[inputfile]\n"
	.size	.L.str.77, 13

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"Switches (names may be abbreviated):\n"
	.size	.L.str.78, 38

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"  -quality N     Compression quality (0..100; 5-95 is useful range)\n"
	.size	.L.str.79, 69

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"  -grayscale     Create monochrome JPEG file\n"
	.size	.L.str.80, 46

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"  -optimize      Optimize Huffman table (smaller file, but slow compression)\n"
	.size	.L.str.81, 78

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"  -progressive   Create progressive JPEG file\n"
	.size	.L.str.82, 47

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"  -targa         Input file is Targa format (usually not needed)\n"
	.size	.L.str.83, 66

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"Switches for advanced users:\n"
	.size	.L.str.84, 30

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"  -dct int       Use integer DCT method%s\n"
	.size	.L.str.85, 43

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	" (default)"
	.size	.L.str.86, 11

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"  -dct fast      Use fast integer DCT (less accurate)%s\n"
	.size	.L.str.87, 57

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.zero	1
	.size	.L.str.88, 1

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"  -dct float     Use floating-point DCT method%s\n"
	.size	.L.str.89, 50

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"  -restart N     Set restart interval in rows, or in blocks with B\n"
	.size	.L.str.90, 68

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"  -smooth N      Smooth dithered input (N=1..100 is strength)\n"
	.size	.L.str.91, 63

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"  -maxmemory N   Maximum memory to use (in kbytes)\n"
	.size	.L.str.92, 52

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"  -outfile name  Specify name for output file\n"
	.size	.L.str.93, 47

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"  -verbose  or  -debug   Emit debug output\n"
	.size	.L.str.94, 44

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"Switches for wizards:\n"
	.size	.L.str.95, 23

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"  -baseline      Force baseline output\n"
	.size	.L.str.96, 40

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"  -qtables file  Use quantization tables given in file\n"
	.size	.L.str.97, 56

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"  -qslots N[,...]    Set component quantization tables\n"
	.size	.L.str.98, 56

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"  -sample HxV[,...]  Set component sampling factors\n"
	.size	.L.str.99, 53

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"  -scans file    Create multi-scan JPEG per script file\n"
	.size	.L.str.100, 57


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
