	.text
	.file	"btGImpactShape.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_ZNK22btGImpactCompoundShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK22btGImpactCompoundShape21calculateLocalInertiaEfR9btVector3: # @_ZNK22btGImpactCompoundShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*192(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*144(%rax)
	movl	%eax, %r13d
	testl	%r13d, %r13d
	je	.LBB0_12
# BB#1:                                 # %.lr.ph
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r13d, %xmm0
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	movslq	%r13d, %rbx
	leaq	-8(,%rbx,8), %rbp
	shlq	$6, %rbx
	addq	$-64, %rbx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	240(%r14), %rax
	movq	(%rax,%rbp), %rdi
	movq	(%rdi), %rax
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	%r15, %rsi
	callq	*64(%rax)
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*152(%rax)
	testb	%al, %al
	je	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	208(%r14), %rax
	addq	%rbx, %rax
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	movb	_ZGVZN11btTransform11getIdentityEvE17identityTransform(%rip), %al
	testb	%al, %al
	jne	.LBB0_10
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_10
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	movb	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %al
	testb	%al, %al
	jne	.LBB0_9
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip) # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+4(%rip)
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+20(%rip) # imm = 0x3F800000
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+24(%rip)
	movq	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+40(%rip) # imm = 0x3F800000
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_release
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+16(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+16(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+32(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+32(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+48(%rip)
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_release
.LBB0_10:                               # %_ZN11btTransform11getIdentityEv.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$_ZZN11btTransform11getIdentityEvE17identityTransform, %eax
.LBB0_11:                               # %_ZN11btTransform11getIdentityEv.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm1, %xmm5
	movaps	%xmm3, %xmm4
	mulss	%xmm0, %xmm4
	movss	8(%rax), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	movss	16(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	movaps	%xmm6, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm5, %xmm4
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	movaps	%xmm1, %xmm6
	mulss	%xmm5, %xmm6
	addss	%xmm4, %xmm3
	movss	20(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm6
	movaps	%xmm0, %xmm5
	mulss	%xmm4, %xmm5
	mulss	%xmm4, %xmm5
	movss	24(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm5
	movaps	%xmm2, %xmm6
	mulss	%xmm4, %xmm6
	mulss	%xmm4, %xmm6
	addss	%xmm5, %xmm6
	movss	32(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	mulss	%xmm4, %xmm1
	movss	36(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	mulss	%xmm4, %xmm0
	movss	40(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm1, %xmm0
	mulss	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	movss	52(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movss	56(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movaps	%xmm0, %xmm4
	addss	%xmm1, %xmm4
	mulss	%xmm3, %xmm4
	movss	48(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm3
	addss	%xmm3, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm3, %xmm0
	mulss	%xmm2, %xmm0
	addss	(%r12), %xmm4
	addss	4(%r12), %xmm1
	addss	8(%r12), %xmm0
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm4, (%r12)
	movlps	%xmm1, 8(%r12)
	addq	$-8, %rbp
	addq	$-64, %rbx
	decl	%r13d
	jne	.LBB0_2
.LBB0_12:                               # %._crit_edge
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*200(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZNK22btGImpactCompoundShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end0-_ZNK22btGImpactCompoundShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.globl	_ZNK22btGImpactMeshShapePart21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart21calculateLocalInertiaEfR9btVector3,@function
_ZNK22btGImpactMeshShapePart21calculateLocalInertiaEfR9btVector3: # @_ZNK22btGImpactMeshShapePart21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*192(%rax)
	movaps	(%rsp), %xmm7           # 16-byte Reload
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	232(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_6
# BB#1:                                 # %.lr.ph
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	%xmm0, %xmm7
	movq	%rax, %rdx
	decq	%rdx
	cmpl	$1, 236(%r14)
	movq	224(%r14), %rsi
	movslq	240(%r14), %rcx
	jne	.LBB1_2
# BB#4:                                 # %.lr.ph.split.us.preheader
	movaps	%xmm7, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	imulq	%rcx, %rdx
	leaq	8(%rsi,%rdx), %rdx
	negq	%rcx
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movss	200(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	mulsd	-8(%rdx), %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movupd	(%rdx), %xmm4
	cvtps2pd	204(%r14), %xmm5
	mulpd	%xmm4, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	mulss	%xmm3, %xmm3
	mulps	%xmm4, %xmm4
	movaps	%xmm4, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	addss	%xmm4, %xmm5
	mulss	%xmm7, %xmm5
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	addps	%xmm4, %xmm3
	mulps	%xmm0, %xmm3
	addss	%xmm5, %xmm2
	movss	%xmm2, (%rbx)
	addps	%xmm3, %xmm1
	movaps	%xmm1, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movss	%xmm3, 4(%rbx)
	movss	%xmm1, 8(%rbx)
	addq	%rcx, %rdx
	decl	%eax
	jne	.LBB1_5
	jmp	.LBB1_6
.LBB1_2:                                # %.lr.ph.split.preheader
	imulq	%rcx, %rdx
	leaq	8(%rsi,%rdx), %rdx
	negq	%rcx
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movss	200(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	-8(%rdx), %xmm3
	movss	-4(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	mulss	204(%r14), %xmm4
	mulss	208(%r14), %xmm5
	mulss	%xmm3, %xmm3
	mulss	%xmm4, %xmm4
	mulss	%xmm5, %xmm5
	movaps	%xmm4, %xmm6
	addss	%xmm5, %xmm6
	mulss	%xmm7, %xmm6
	addss	%xmm3, %xmm5
	mulss	%xmm7, %xmm5
	addss	%xmm3, %xmm4
	mulss	%xmm7, %xmm4
	addss	%xmm6, %xmm2
	movss	%xmm2, (%rbx)
	addss	%xmm5, %xmm1
	movss	%xmm1, 4(%rbx)
	addss	%xmm4, %xmm0
	movss	%xmm0, 8(%rbx)
	addq	%rcx, %rdx
	decl	%eax
	jne	.LBB1_3
.LBB1_6:                                # %._crit_edge
	movq	(%r14), %rax
	movq	%r14, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*200(%rax)              # TAILCALL
.Lfunc_end1:
	.size	_ZNK22btGImpactMeshShapePart21calculateLocalInertiaEfR9btVector3, .Lfunc_end1-_ZNK22btGImpactMeshShapePart21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.globl	_ZNK18btGImpactMeshShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK18btGImpactMeshShape21calculateLocalInertiaEfR9btVector3: # @_ZNK18btGImpactMeshShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 80
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r13, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movslq	188(%r14), %r13
	testq	%r13, %r13
	je	.LBB2_3
# BB#1:                                 # %.lr.ph
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r13d, %xmm0
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	leaq	-8(,%r13,8), %rbx
	leaq	16(%rsp), %r15
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	200(%r14), %rax
	movq	(%rax,%rbx), %rdi
	movq	(%rdi), %rax
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	%r15, %rsi
	callq	*64(%rax)
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	(%r12), %xmm0
	movss	%xmm0, (%r12)
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	4(%r12), %xmm0
	movss	%xmm0, 4(%r12)
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	8(%r12), %xmm0
	movss	%xmm0, 8(%r12)
	addq	$-8, %rbx
	decl	%r13d
	jne	.LBB2_2
.LBB2_3:                                # %._crit_edge
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_ZNK18btGImpactMeshShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end2-_ZNK18btGImpactMeshShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.globl	_ZNK18btGImpactMeshShape7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE,@function
_ZNK18btGImpactMeshShape7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE: # @_ZNK18btGImpactMeshShape7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end3:
	.size	_ZNK18btGImpactMeshShape7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE, .Lfunc_end3-_ZNK18btGImpactMeshShape7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.cfi_endproc

	.globl	_ZNK22btGImpactMeshShapePart19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,@function
_ZNK22btGImpactMeshShapePart19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_: # @_ZNK22btGImpactMeshShapePart19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 208
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	(%r13), %rax
	callq	*192(%rax)
	movups	(%rbp), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	(%r15), %xmm0
	movaps	%xmm0, 64(%rsp)
	movb	$1, 40(%rsp)
	movq	$0, 32(%rsp)
	movq	$0, 20(%rsp)
	leaq	80(%r13), %rdi
.Ltmp0:
	leaq	48(%rsp), %rsi
	leaq	16(%rsp), %rdx
	callq	_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE
.Ltmp1:
# BB#1:
	movslq	20(%rsp), %r15
	testq	%r15, %r15
	je	.LBB4_2
# BB#3:
	movl	216(%r13), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	$1008981770, 144(%rsp)  # imm = 0x3C23D70A
	movq	%r15, %rbx
	decq	%rbx
	leaq	80(%rsp), %r12
	.p2align	4, 0x90
.LBB4_4:                                # =>This Inner Loop Header: Depth=1
	testl	%r15d, %r15d
	je	.LBB4_10
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=1
	movq	32(%rsp), %rax
	movl	(%rax,%rbx,4), %ebp
	movq	(%r13), %rax
.Ltmp2:
	movq	%r13, %rdi
	callq	*136(%rax)
.Ltmp3:
# BB#6:                                 # %.noexc19
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	(%rax), %rcx
.Ltmp4:
	movq	%rax, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	*40(%rcx)
.Ltmp5:
# BB#7:                                 # %_ZNK23btGImpactShapeInterface20getPrimitiveTriangleEiR19btPrimitiveTriangle.exit
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	(%r14), %rax
	movq	32(%rsp), %rcx
	movl	(%rcx,%rbx,4), %ecx
	decl	%r15d
	decq	%rbx
.Ltmp6:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*16(%rax)
.Ltmp7:
	jmp	.LBB4_4
.LBB4_10:
	movq	(%r13), %rax
.Ltmp9:
	movq	%r13, %rdi
	callq	*200(%rax)
.Ltmp10:
	jmp	.LBB4_11
.LBB4_2:
	movq	(%r13), %rax
.Ltmp12:
	movq	%r13, %rdi
	callq	*200(%rax)
.Ltmp13:
.LBB4_11:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_15
# BB#12:
	cmpb	$0, 40(%rsp)
	je	.LBB4_14
# BB#13:
	callq	_Z21btAlignedFreeInternalPv
.LBB4_14:
	movq	$0, 32(%rsp)
.LBB4_15:                               # %_ZN20btAlignedObjectArrayIiED2Ev.exit18
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_9:                                # %.loopexit.split-lp
.Ltmp11:
	jmp	.LBB4_17
.LBB4_16:
.Ltmp14:
	jmp	.LBB4_17
.LBB4_8:                                # %.loopexit
.Ltmp8:
.LBB4_17:
	movq	%rax, %rbx
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_21
# BB#18:
	cmpb	$0, 40(%rsp)
	je	.LBB4_20
# BB#19:
.Ltmp15:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
.LBB4_20:                               # %.noexc
	movq	$0, 32(%rsp)
.LBB4_21:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_22:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZNK22btGImpactMeshShapePart19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_, .Lfunc_end4-_ZNK22btGImpactMeshShapePart19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp2           #   Call between .Ltmp2 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Lfunc_end4-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end5:
	.size	__clang_call_terminate, .Lfunc_end5-__clang_call_terminate

	.text
	.globl	_ZNK18btGImpactMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,@function
_ZNK18btGImpactMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_: # @_ZNK18btGImpactMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 64
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	movslq	188(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB6_3
# BB#1:                                 # %.lr.ph
	leaq	-8(,%rbx,8), %rbp
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	200(%r13), %rax
	movq	(%rax,%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	*96(%rax)
	addq	$-8, %rbp
	decl	%ebx
	jne	.LBB6_2
.LBB6_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZNK18btGImpactMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_, .Lfunc_end6-_ZNK18btGImpactMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_endproc

	.section	.text._ZN22btGImpactCompoundShapeD2Ev,"axG",@progbits,_ZN22btGImpactCompoundShapeD2Ev,comdat
	.weak	_ZN22btGImpactCompoundShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN22btGImpactCompoundShapeD2Ev,@function
_ZN22btGImpactCompoundShapeD2Ev:        # @_ZN22btGImpactCompoundShapeD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -24
.Lcfi59:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV22btGImpactCompoundShape+16, (%rbx)
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_4
# BB#1:
	cmpb	$0, 248(%rbx)
	je	.LBB7_3
# BB#2:
.Ltmp18:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp19:
.LBB7_3:                                # %.noexc
	movq	$0, 240(%rbx)
.LBB7_4:
	movb	$1, 248(%rbx)
	movq	$0, 240(%rbx)
	movq	$0, 228(%rbx)
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_8
# BB#5:
	cmpb	$0, 216(%rbx)
	je	.LBB7_7
# BB#6:
.Ltmp23:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp24:
.LBB7_7:                                # %.noexc5
	movq	$0, 208(%rbx)
.LBB7_8:
	movb	$1, 216(%rbx)
	movq	$0, 208(%rbx)
	movl	$0, 196(%rbx)
	movl	$0, 200(%rbx)
	movq	$_ZTV23btGImpactShapeInterface+16, (%rbx)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_12
# BB#9:
	cmpb	$0, 112(%rbx)
	je	.LBB7_11
# BB#10:
.Ltmp35:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp36:
.LBB7_11:                               # %.noexc.i
	movq	$0, 104(%rbx)
.LBB7_12:                               # %_ZN23btGImpactShapeInterfaceD2Ev.exit
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN14btConcaveShapeD2Ev # TAILCALL
.LBB7_13:
.Ltmp37:
	movq	%rax, %r14
.Ltmp38:
	movq	%rbx, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp39:
	jmp	.LBB7_14
.LBB7_15:
.Ltmp40:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_21:
.Ltmp25:
	movq	%rax, %r14
	jmp	.LBB7_22
.LBB7_16:
.Ltmp20:
	movq	%rax, %r14
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_20
# BB#17:
	cmpb	$0, 216(%rbx)
	je	.LBB7_19
# BB#18:
.Ltmp21:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp22:
.LBB7_19:                               # %.noexc7
	movq	$0, 208(%rbx)
.LBB7_20:                               # %_ZN20btAlignedObjectArrayI11btTransformED2Ev.exit8
	movb	$1, 216(%rbx)
	movq	$0, 208(%rbx)
	movq	$0, 196(%rbx)
.LBB7_22:
	movq	$_ZTV23btGImpactShapeInterface+16, (%rbx)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_26
# BB#23:
	cmpb	$0, 112(%rbx)
	je	.LBB7_25
# BB#24:
.Ltmp26:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp27:
.LBB7_25:                               # %.noexc.i9
	movq	$0, 104(%rbx)
.LBB7_26:
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
.Ltmp32:
	movq	%rbx, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp33:
.LBB7_14:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_27:
.Ltmp28:
	movq	%rax, %r14
.Ltmp29:
	movq	%rbx, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp30:
	jmp	.LBB7_30
.LBB7_28:
.Ltmp31:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_29:
.Ltmp34:
	movq	%rax, %r14
.LBB7_30:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN22btGImpactCompoundShapeD2Ev, .Lfunc_end7-_ZN22btGImpactCompoundShapeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin1   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin1   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp38-.Ltmp36         #   Call between .Ltmp36 and .Ltmp38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin1   #     jumps to .Ltmp40
	.byte	1                       #   On action: 1
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	1                       #   On action: 1
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	1                       #   On action: 1
	.long	.Ltmp33-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp29-.Ltmp33         #   Call between .Ltmp33 and .Ltmp29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin1   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN22btGImpactCompoundShapeD0Ev,"axG",@progbits,_ZN22btGImpactCompoundShapeD0Ev,comdat
	.weak	_ZN22btGImpactCompoundShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN22btGImpactCompoundShapeD0Ev,@function
_ZN22btGImpactCompoundShapeD0Ev:        # @_ZN22btGImpactCompoundShapeD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 32
.Lcfi63:
	.cfi_offset %rbx, -24
.Lcfi64:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp41:
	callq	_ZN22btGImpactCompoundShapeD2Ev
.Ltmp42:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_2:
.Ltmp43:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN22btGImpactCompoundShapeD0Ev, .Lfunc_end8-_ZN22btGImpactCompoundShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp41-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin2   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end8-.Ltmp42     #   Call between .Ltmp42 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.text._ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_: # @_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movq	28(%rdi), %xmm2         # xmm2 = mem[0],zero
	movq	44(%rdi), %xmm11        # xmm11 = mem[0],zero
	pshufd	$229, %xmm11, %xmm8     # xmm8 = xmm11[1,1,2,3]
	pshufd	$229, %xmm2, %xmm6      # xmm6 = xmm2[1,1,2,3]
	addss	%xmm11, %xmm2
	addss	%xmm8, %xmm6
	movss	52(%rdi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	36(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm9, %xmm0
	movss	.LCPI9_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	mulss	%xmm1, %xmm6
	mulss	%xmm1, %xmm0
	subss	%xmm2, %xmm11
	subss	%xmm6, %xmm8
	subss	%xmm0, %xmm9
	movss	16(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm5, %xmm1
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	20(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm12   # xmm12 = xmm12[0],xmm7[0],xmm12[1],xmm7[1]
	mulps	%xmm12, %xmm3
	addps	%xmm1, %xmm3
	movaps	%xmm0, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	mulps	%xmm1, %xmm7
	addps	%xmm3, %xmm7
	movsd	48(%rsi), %xmm10        # xmm10 = mem[0],zero
	addps	%xmm7, %xmm10
	movss	32(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	movss	36(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm2, %xmm6
	movss	40(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm6, %xmm0
	addss	56(%rsi), %xmm0
	movaps	.LCPI9_1(%rip), %xmm6   # xmm6 = [nan,nan,nan,nan]
	andps	%xmm6, %xmm5
	andps	%xmm6, %xmm12
	andps	%xmm6, %xmm1
	andps	%xmm6, %xmm7
	mulss	%xmm11, %xmm7
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm5, %xmm11
	andps	%xmm6, %xmm3
	mulss	%xmm8, %xmm3
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm12, %xmm8
	addps	%xmm11, %xmm8
	andps	%xmm6, %xmm2
	mulss	%xmm9, %xmm2
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm1, %xmm9
	addps	%xmm8, %xmm9
	addss	%xmm7, %xmm3
	addss	%xmm3, %xmm2
	movaps	%xmm10, %xmm1
	subps	%xmm9, %xmm1
	movaps	%xmm0, %xmm3
	subss	%xmm2, %xmm3
	xorps	%xmm4, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1,2,3]
	addps	%xmm10, %xmm9
	addss	%xmm0, %xmm2
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm1, (%rdx)
	movlps	%xmm5, 8(%rdx)
	movlps	%xmm9, (%rcx)
	movlps	%xmm4, 8(%rcx)
	retq
.Lfunc_end9:
	.size	_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end9-_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3,"axG",@progbits,_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3,comdat
	.weak	_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3,@function
_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3: # @_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 64(%rdi)
	movq	(%rdi), %rax
	jmpq	*112(%rax)              # TAILCALL
.Lfunc_end10:
	.size	_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3, .Lfunc_end10-_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3
	.cfi_endproc

	.section	.text._ZNK23btGImpactShapeInterface15getLocalScalingEv,"axG",@progbits,_ZNK23btGImpactShapeInterface15getLocalScalingEv,comdat
	.weak	_ZNK23btGImpactShapeInterface15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK23btGImpactShapeInterface15getLocalScalingEv,@function
_ZNK23btGImpactShapeInterface15getLocalScalingEv: # @_ZNK23btGImpactShapeInterface15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	64(%rdi), %rax
	retq
.Lfunc_end11:
	.size	_ZNK23btGImpactShapeInterface15getLocalScalingEv, .Lfunc_end11-_ZNK23btGImpactShapeInterface15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape7getNameEv,"axG",@progbits,_ZNK22btGImpactCompoundShape7getNameEv,comdat
	.weak	_ZNK22btGImpactCompoundShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape7getNameEv,@function
_ZNK22btGImpactCompoundShape7getNameEv: # @_ZNK22btGImpactCompoundShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end12:
	.size	_ZNK22btGImpactCompoundShape7getNameEv, .Lfunc_end12-_ZNK22btGImpactCompoundShape7getNameEv
	.cfi_endproc

	.section	.text._ZN23btGImpactShapeInterface9setMarginEf,"axG",@progbits,_ZN23btGImpactShapeInterface9setMarginEf,comdat
	.weak	_ZN23btGImpactShapeInterface9setMarginEf
	.p2align	4, 0x90
	.type	_ZN23btGImpactShapeInterface9setMarginEf,@function
_ZN23btGImpactShapeInterface9setMarginEf: # @_ZN23btGImpactShapeInterface9setMarginEf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 32
.Lcfi68:
	.cfi_offset %rbx, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movss	%xmm0, 24(%rbx)
	movq	(%rbx), %rax
	callq	*144(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB13_3
# BB#1:                                 # %.lr.ph
	decl	%ebp
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	*216(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*80(%rcx)
	decl	%ebp
	cmpl	$-1, %ebp
	jne	.LBB13_2
.LBB13_3:                               # %._crit_edge
	movb	$1, 60(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN23btGImpactShapeInterface9setMarginEf, .Lfunc_end13-_ZN23btGImpactShapeInterface9setMarginEf
	.cfi_endproc

	.section	.text._ZNK14btConcaveShape9getMarginEv,"axG",@progbits,_ZNK14btConcaveShape9getMarginEv,comdat
	.weak	_ZNK14btConcaveShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK14btConcaveShape9getMarginEv,@function
_ZNK14btConcaveShape9getMarginEv:       # @_ZNK14btConcaveShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end14:
	.size	_ZNK14btConcaveShape9getMarginEv, .Lfunc_end14-_ZNK14btConcaveShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,"axG",@progbits,_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,comdat
	.weak	_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,@function
_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_: # @_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end15:
	.size	_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_, .Lfunc_end15-_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_endproc

	.section	.text._ZN23btGImpactShapeInterface13calcLocalAABBEv,"axG",@progbits,_ZN23btGImpactShapeInterface13calcLocalAABBEv,comdat
	.weak	_ZN23btGImpactShapeInterface13calcLocalAABBEv
	.p2align	4, 0x90
	.type	_ZN23btGImpactShapeInterface13calcLocalAABBEv,@function
_ZN23btGImpactShapeInterface13calcLocalAABBEv: # @_ZN23btGImpactShapeInterface13calcLocalAABBEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 16
.Lcfi71:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*192(%rax)
	leaq	80(%rbx), %rdi
	cmpl	$0, 80(%rbx)
	je	.LBB16_1
# BB#2:
	callq	_ZN21btGImpactQuantizedBvh5refitEv
	jmp	.LBB16_3
.LBB16_1:
	callq	_ZN21btGImpactQuantizedBvh8buildSetEv
.LBB16_3:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*200(%rax)
	movq	104(%rbx), %rax
	movzwl	(%rax), %ecx
	cvtsi2ssl	%ecx, %xmm0
	movss	152(%rbx), %xmm8        # xmm8 = mem[0],zero,zero,zero
	divss	%xmm8, %xmm0
	movzwl	2(%rax), %ecx
	cvtsi2ssl	%ecx, %xmm2
	movss	156(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	divss	%xmm9, %xmm2
	movzwl	4(%rax), %ecx
	cvtsi2ssl	%ecx, %xmm4
	movss	160(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	divss	%xmm10, %xmm4
	movss	120(%rbx), %xmm11       # xmm11 = mem[0],zero,zero,zero
	addss	%xmm11, %xmm0
	movss	124(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm2
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movss	128(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm4
	xorps	%xmm1, %xmm1
	xorps	%xmm3, %xmm3
	shufps	$1, %xmm4, %xmm3        # xmm3 = xmm3[1,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	movzwl	6(%rax), %ecx
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ecx, %xmm4
	divss	%xmm8, %xmm4
	movzwl	8(%rax), %ecx
	cvtsi2ssl	%ecx, %xmm5
	divss	%xmm9, %xmm5
	movzwl	10(%rax), %eax
	cvtsi2ssl	%eax, %xmm6
	divss	%xmm10, %xmm6
	addss	%xmm11, %xmm4
	addss	%xmm7, %xmm5
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	addss	%xmm2, %xmm6
	shufps	$1, %xmm6, %xmm1        # xmm1 = xmm1[1,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm1      # xmm1 = xmm1[2,0],xmm6[2,3]
	movlps	%xmm0, 28(%rbx)
	movlps	%xmm3, 36(%rbx)
	movlps	%xmm4, 44(%rbx)
	movlps	%xmm1, 52(%rbx)
	popq	%rbx
	retq
.Lfunc_end16:
	.size	_ZN23btGImpactShapeInterface13calcLocalAABBEv, .Lfunc_end16-_ZN23btGImpactShapeInterface13calcLocalAABBEv
	.cfi_endproc

	.section	.text._ZN23btGImpactShapeInterface10postUpdateEv,"axG",@progbits,_ZN23btGImpactShapeInterface10postUpdateEv,comdat
	.weak	_ZN23btGImpactShapeInterface10postUpdateEv
	.p2align	4, 0x90
	.type	_ZN23btGImpactShapeInterface10postUpdateEv,@function
_ZN23btGImpactShapeInterface10postUpdateEv: # @_ZN23btGImpactShapeInterface10postUpdateEv
	.cfi_startproc
# BB#0:
	movb	$1, 60(%rdi)
	retq
.Lfunc_end17:
	.size	_ZN23btGImpactShapeInterface10postUpdateEv, .Lfunc_end17-_ZN23btGImpactShapeInterface10postUpdateEv
	.cfi_endproc

	.section	.text._ZNK23btGImpactShapeInterface12getShapeTypeEv,"axG",@progbits,_ZNK23btGImpactShapeInterface12getShapeTypeEv,comdat
	.weak	_ZNK23btGImpactShapeInterface12getShapeTypeEv
	.p2align	4, 0x90
	.type	_ZNK23btGImpactShapeInterface12getShapeTypeEv,@function
_ZNK23btGImpactShapeInterface12getShapeTypeEv: # @_ZNK23btGImpactShapeInterface12getShapeTypeEv
	.cfi_startproc
# BB#0:
	movl	$25, %eax
	retq
.Lfunc_end18:
	.size	_ZNK23btGImpactShapeInterface12getShapeTypeEv, .Lfunc_end18-_ZNK23btGImpactShapeInterface12getShapeTypeEv
	.cfi_endproc

	.section	.text._ZN22btGImpactCompoundShape19getGImpactShapeTypeEv,"axG",@progbits,_ZN22btGImpactCompoundShape19getGImpactShapeTypeEv,comdat
	.weak	_ZN22btGImpactCompoundShape19getGImpactShapeTypeEv
	.p2align	4, 0x90
	.type	_ZN22btGImpactCompoundShape19getGImpactShapeTypeEv,@function
_ZN22btGImpactCompoundShape19getGImpactShapeTypeEv: # @_ZN22btGImpactCompoundShape19getGImpactShapeTypeEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end19:
	.size	_ZN22btGImpactCompoundShape19getGImpactShapeTypeEv, .Lfunc_end19-_ZN22btGImpactCompoundShape19getGImpactShapeTypeEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape19getPrimitiveManagerEv,"axG",@progbits,_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv,comdat
	.weak	_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv,@function
_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv: # @_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv
	.cfi_startproc
# BB#0:
	leaq	176(%rdi), %rax
	retq
.Lfunc_end20:
	.size	_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv, .Lfunc_end20-_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape17getNumChildShapesEv,"axG",@progbits,_ZNK22btGImpactCompoundShape17getNumChildShapesEv,comdat
	.weak	_ZNK22btGImpactCompoundShape17getNumChildShapesEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape17getNumChildShapesEv,@function
_ZNK22btGImpactCompoundShape17getNumChildShapesEv: # @_ZNK22btGImpactCompoundShape17getNumChildShapesEv
	.cfi_startproc
# BB#0:
	movl	228(%rdi), %eax
	retq
.Lfunc_end21:
	.size	_ZNK22btGImpactCompoundShape17getNumChildShapesEv, .Lfunc_end21-_ZNK22btGImpactCompoundShape17getNumChildShapesEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape20childrenHasTransformEv,"axG",@progbits,_ZNK22btGImpactCompoundShape20childrenHasTransformEv,comdat
	.weak	_ZNK22btGImpactCompoundShape20childrenHasTransformEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape20childrenHasTransformEv,@function
_ZNK22btGImpactCompoundShape20childrenHasTransformEv: # @_ZNK22btGImpactCompoundShape20childrenHasTransformEv
	.cfi_startproc
# BB#0:
	cmpl	$0, 196(%rdi)
	setne	%al
	retq
.Lfunc_end22:
	.size	_ZNK22btGImpactCompoundShape20childrenHasTransformEv, .Lfunc_end22-_ZNK22btGImpactCompoundShape20childrenHasTransformEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv,"axG",@progbits,_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv,comdat
	.weak	_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv,@function
_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv: # @_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end23:
	.size	_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv, .Lfunc_end23-_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv,"axG",@progbits,_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv,comdat
	.weak	_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv,@function
_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv: # @_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end24:
	.size	_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv, .Lfunc_end24-_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx,"axG",@progbits,_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx,comdat
	.weak	_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx,@function
_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx: # @_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end25:
	.size	_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx, .Lfunc_end25-_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx,"axG",@progbits,_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx,comdat
	.weak	_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx,@function
_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx: # @_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end26:
	.size	_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx, .Lfunc_end26-_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.cfi_endproc

	.section	.text._ZNK23btGImpactShapeInterface15lockChildShapesEv,"axG",@progbits,_ZNK23btGImpactShapeInterface15lockChildShapesEv,comdat
	.weak	_ZNK23btGImpactShapeInterface15lockChildShapesEv
	.p2align	4, 0x90
	.type	_ZNK23btGImpactShapeInterface15lockChildShapesEv,@function
_ZNK23btGImpactShapeInterface15lockChildShapesEv: # @_ZNK23btGImpactShapeInterface15lockChildShapesEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end27:
	.size	_ZNK23btGImpactShapeInterface15lockChildShapesEv, .Lfunc_end27-_ZNK23btGImpactShapeInterface15lockChildShapesEv
	.cfi_endproc

	.section	.text._ZNK23btGImpactShapeInterface17unlockChildShapesEv,"axG",@progbits,_ZNK23btGImpactShapeInterface17unlockChildShapesEv,comdat
	.weak	_ZNK23btGImpactShapeInterface17unlockChildShapesEv
	.p2align	4, 0x90
	.type	_ZNK23btGImpactShapeInterface17unlockChildShapesEv,@function
_ZNK23btGImpactShapeInterface17unlockChildShapesEv: # @_ZNK23btGImpactShapeInterface17unlockChildShapesEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end28:
	.size	_ZNK23btGImpactShapeInterface17unlockChildShapesEv, .Lfunc_end28-_ZNK23btGImpactShapeInterface17unlockChildShapesEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_,@function
_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_: # @_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 48
	subq	$192, %rsp
.Lcfi77:
	.cfi_def_cfa_offset 240
.Lcfi78:
	.cfi_offset %rbx, -48
.Lcfi79:
	.cfi_offset %r12, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*152(%rax)
	movq	240(%rbx), %rdx
	movslq	%ebp, %rcx
	movq	(%rdx,%rcx,8), %rdi
	movq	(%rdi), %rdx
	movq	16(%rdx), %r8
	testb	%al, %al
	je	.LBB29_2
# BB#1:
	movq	208(%rbx), %rax
	shlq	$6, %rcx
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	(%rax,%rcx), %xmm6      # xmm6 = mem[0],zero,zero,zero
	movss	4(%rax,%rcx), %xmm10    # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	movss	16(%rax,%rcx), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm9
	movaps	%xmm9, 144(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	32(%rax,%rcx), %xmm14   # xmm14 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm7
	movaps	%xmm7, 128(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm10, %xmm0
	movss	20(%rax,%rcx), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	36(%rax,%rcx), %xmm12   # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movss	8(%rax,%rcx), %xmm8     # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm8, %xmm0
	movss	24(%rax,%rcx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	40(%rax,%rcx), %xmm9    # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm9, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movss	16(%r12), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movss	20(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm7, %xmm3
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	24(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm10, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm8, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm9, %xmm7
	mulss	%xmm1, %xmm7
	addss	%xmm3, %xmm7
	movss	32(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	movss	36(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm6, %xmm5
	movss	40(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm10
	mulss	%xmm3, %xmm4
	addss	%xmm10, %xmm4
	mulss	%xmm6, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm8
	mulss	%xmm3, %xmm2
	addss	%xmm8, %xmm2
	mulss	%xmm6, %xmm9
	addss	%xmm2, %xmm9
	movaps	112(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	48(%rax,%rcx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	144(%rsp), %xmm4        # 16-byte Reload
	unpcklps	96(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	52(%rax,%rcx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	128(%rsp), %xmm4        # 16-byte Reload
	unpcklps	80(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	56(%rax,%rcx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	48(%r12), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	addss	56(%r12), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 20(%rsp)
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 24(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm15, 32(%rsp)
	movss	%xmm13, 36(%rsp)
	movss	%xmm7, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	%xmm14, 48(%rsp)
	movss	%xmm12, 52(%rsp)
	movss	%xmm9, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm2, 64(%rsp)
	movlps	%xmm1, 72(%rsp)
	leaq	16(%rsp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	*%r8
	addq	$192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_2:
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	addq	$192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%r8                    # TAILCALL
.Lfunc_end29:
	.size	_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_, .Lfunc_end29-_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZN22btGImpactCompoundShape13getChildShapeEi,"axG",@progbits,_ZN22btGImpactCompoundShape13getChildShapeEi,comdat
	.weak	_ZN22btGImpactCompoundShape13getChildShapeEi
	.p2align	4, 0x90
	.type	_ZN22btGImpactCompoundShape13getChildShapeEi,@function
_ZN22btGImpactCompoundShape13getChildShapeEi: # @_ZN22btGImpactCompoundShape13getChildShapeEi
	.cfi_startproc
# BB#0:
	movq	240(%rdi), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	retq
.Lfunc_end30:
	.size	_ZN22btGImpactCompoundShape13getChildShapeEi, .Lfunc_end30-_ZN22btGImpactCompoundShape13getChildShapeEi
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape13getChildShapeEi,"axG",@progbits,_ZNK22btGImpactCompoundShape13getChildShapeEi,comdat
	.weak	_ZNK22btGImpactCompoundShape13getChildShapeEi
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape13getChildShapeEi,@function
_ZNK22btGImpactCompoundShape13getChildShapeEi: # @_ZNK22btGImpactCompoundShape13getChildShapeEi
	.cfi_startproc
# BB#0:
	movq	240(%rdi), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	retq
.Lfunc_end31:
	.size	_ZNK22btGImpactCompoundShape13getChildShapeEi, .Lfunc_end31-_ZNK22btGImpactCompoundShape13getChildShapeEi
	.cfi_endproc

	.section	.text._ZNK22btGImpactCompoundShape17getChildTransformEi,"axG",@progbits,_ZNK22btGImpactCompoundShape17getChildTransformEi,comdat
	.weak	_ZNK22btGImpactCompoundShape17getChildTransformEi
	.p2align	4, 0x90
	.type	_ZNK22btGImpactCompoundShape17getChildTransformEi,@function
_ZNK22btGImpactCompoundShape17getChildTransformEi: # @_ZNK22btGImpactCompoundShape17getChildTransformEi
	.cfi_startproc
# BB#0:
	movq	208(%rsi), %rax
	movslq	%edx, %rcx
	shlq	$6, %rcx
	movups	(%rax,%rcx), %xmm0
	movups	%xmm0, (%rdi)
	movups	16(%rax,%rcx), %xmm0
	movups	%xmm0, 16(%rdi)
	movups	32(%rax,%rcx), %xmm0
	movups	%xmm0, 32(%rdi)
	movups	48(%rax,%rcx), %xmm0
	movups	%xmm0, 48(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end32:
	.size	_ZNK22btGImpactCompoundShape17getChildTransformEi, .Lfunc_end32-_ZNK22btGImpactCompoundShape17getChildTransformEi
	.cfi_endproc

	.section	.text._ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform,"axG",@progbits,_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform,comdat
	.weak	_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform
	.p2align	4, 0x90
	.type	_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform,@function
_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform: # @_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform
	.cfi_startproc
# BB#0:
	movq	208(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$6, %rcx
	movups	(%rdx), %xmm0
	movups	%xmm0, (%rax,%rcx)
	movups	16(%rdx), %xmm0
	movups	%xmm0, 16(%rax,%rcx)
	movups	32(%rdx), %xmm0
	movups	%xmm0, 32(%rax,%rcx)
	movups	48(%rdx), %xmm0
	movups	%xmm0, 48(%rax,%rcx)
	movq	(%rdi), %rax
	jmpq	*112(%rax)              # TAILCALL
.Lfunc_end33:
	.size	_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform, .Lfunc_end33-_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform
	.cfi_endproc

	.section	.text._ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE,"axG",@progbits,_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE,comdat
	.weak	_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.p2align	4, 0x90
	.type	_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE,@function
_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE: # @_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end34:
	.size	_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE, .Lfunc_end34-_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.cfi_endproc

	.section	.text._ZN22btGImpactMeshShapePartD2Ev,"axG",@progbits,_ZN22btGImpactMeshShapePartD2Ev,comdat
	.weak	_ZN22btGImpactMeshShapePartD2Ev
	.p2align	4, 0x90
	.type	_ZN22btGImpactMeshShapePartD2Ev,@function
_ZN22btGImpactMeshShapePartD2Ev:        # @_ZN22btGImpactMeshShapePartD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -24
.Lcfi87:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV23btGImpactShapeInterface+16, (%rbx)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_4
# BB#1:
	cmpb	$0, 112(%rbx)
	je	.LBB35_3
# BB#2:
.Ltmp44:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp45:
.LBB35_3:                               # %.noexc.i
	movq	$0, 104(%rbx)
.LBB35_4:                               # %_ZN23btGImpactShapeInterfaceD2Ev.exit
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN14btConcaveShapeD2Ev # TAILCALL
.LBB35_5:
.Ltmp46:
	movq	%rax, %r14
.Ltmp47:
	movq	%rbx, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp48:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB35_7:
.Ltmp49:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN22btGImpactMeshShapePartD2Ev, .Lfunc_end35-_ZN22btGImpactMeshShapePartD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp44-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin3   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp47-.Ltmp45         #   Call between .Ltmp45 and .Ltmp47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin3   #     jumps to .Ltmp49
	.byte	1                       #   On action: 1
	.long	.Ltmp48-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end35-.Ltmp48    #   Call between .Ltmp48 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN22btGImpactMeshShapePartD0Ev,"axG",@progbits,_ZN22btGImpactMeshShapePartD0Ev,comdat
	.weak	_ZN22btGImpactMeshShapePartD0Ev
	.p2align	4, 0x90
	.type	_ZN22btGImpactMeshShapePartD0Ev,@function
_ZN22btGImpactMeshShapePartD0Ev:        # @_ZN22btGImpactMeshShapePartD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 32
.Lcfi91:
	.cfi_offset %rbx, -24
.Lcfi92:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV23btGImpactShapeInterface+16, (%rbx)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB36_4
# BB#1:
	cmpb	$0, 112(%rbx)
	je	.LBB36_3
# BB#2:
.Ltmp50:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp51:
.LBB36_3:                               # %.noexc.i.i
	movq	$0, 104(%rbx)
.LBB36_4:                               # %_ZN23btGImpactShapeInterfaceD2Ev.exit.i
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
.Ltmp56:
	movq	%rbx, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp57:
# BB#5:                                 # %_ZN22btGImpactMeshShapePartD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB36_6:
.Ltmp52:
	movq	%rax, %r14
.Ltmp53:
	movq	%rbx, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp54:
	jmp	.LBB36_9
.LBB36_7:
.Ltmp55:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB36_8:
.Ltmp58:
	movq	%rax, %r14
.LBB36_9:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end36:
	.size	_ZN22btGImpactMeshShapePartD0Ev, .Lfunc_end36-_ZN22btGImpactMeshShapePartD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp50-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin4   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin4   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin4   #     jumps to .Ltmp55
	.byte	1                       #   On action: 1
	.long	.Ltmp54-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end36-.Ltmp54    #   Call between .Ltmp54 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3,"axG",@progbits,_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3,comdat
	.weak	_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3,@function
_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3: # @_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 200(%rdi)
	movq	(%rdi), %rax
	jmpq	*112(%rax)              # TAILCALL
.Lfunc_end37:
	.size	_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3, .Lfunc_end37-_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart15getLocalScalingEv,"axG",@progbits,_ZNK22btGImpactMeshShapePart15getLocalScalingEv,comdat
	.weak	_ZNK22btGImpactMeshShapePart15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart15getLocalScalingEv,@function
_ZNK22btGImpactMeshShapePart15getLocalScalingEv: # @_ZNK22btGImpactMeshShapePart15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	200(%rdi), %rax
	retq
.Lfunc_end38:
	.size	_ZNK22btGImpactMeshShapePart15getLocalScalingEv, .Lfunc_end38-_ZNK22btGImpactMeshShapePart15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart7getNameEv,"axG",@progbits,_ZNK22btGImpactMeshShapePart7getNameEv,comdat
	.weak	_ZNK22btGImpactMeshShapePart7getNameEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart7getNameEv,@function
_ZNK22btGImpactMeshShapePart7getNameEv: # @_ZNK22btGImpactMeshShapePart7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.1, %eax
	retq
.Lfunc_end39:
	.size	_ZNK22btGImpactMeshShapePart7getNameEv, .Lfunc_end39-_ZNK22btGImpactMeshShapePart7getNameEv
	.cfi_endproc

	.section	.text._ZN22btGImpactMeshShapePart9setMarginEf,"axG",@progbits,_ZN22btGImpactMeshShapePart9setMarginEf,comdat
	.weak	_ZN22btGImpactMeshShapePart9setMarginEf
	.p2align	4, 0x90
	.type	_ZN22btGImpactMeshShapePart9setMarginEf,@function
_ZN22btGImpactMeshShapePart9setMarginEf: # @_ZN22btGImpactMeshShapePart9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 184(%rdi)
	movq	(%rdi), %rax
	jmpq	*112(%rax)              # TAILCALL
.Lfunc_end40:
	.size	_ZN22btGImpactMeshShapePart9setMarginEf, .Lfunc_end40-_ZN22btGImpactMeshShapePart9setMarginEf
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart9getMarginEv,"axG",@progbits,_ZNK22btGImpactMeshShapePart9getMarginEv,comdat
	.weak	_ZNK22btGImpactMeshShapePart9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart9getMarginEv,@function
_ZNK22btGImpactMeshShapePart9getMarginEv: # @_ZNK22btGImpactMeshShapePart9getMarginEv
	.cfi_startproc
# BB#0:
	movss	184(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end41:
	.size	_ZNK22btGImpactMeshShapePart9getMarginEv, .Lfunc_end41-_ZNK22btGImpactMeshShapePart9getMarginEv
	.cfi_endproc

	.section	.text._ZN22btGImpactMeshShapePart19getGImpactShapeTypeEv,"axG",@progbits,_ZN22btGImpactMeshShapePart19getGImpactShapeTypeEv,comdat
	.weak	_ZN22btGImpactMeshShapePart19getGImpactShapeTypeEv
	.p2align	4, 0x90
	.type	_ZN22btGImpactMeshShapePart19getGImpactShapeTypeEv,@function
_ZN22btGImpactMeshShapePart19getGImpactShapeTypeEv: # @_ZN22btGImpactMeshShapePart19getGImpactShapeTypeEv
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	retq
.Lfunc_end42:
	.size	_ZN22btGImpactMeshShapePart19getGImpactShapeTypeEv, .Lfunc_end42-_ZN22btGImpactMeshShapePart19getGImpactShapeTypeEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv,"axG",@progbits,_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv,comdat
	.weak	_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv,@function
_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv: # @_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv
	.cfi_startproc
# BB#0:
	leaq	176(%rdi), %rax
	retq
.Lfunc_end43:
	.size	_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv, .Lfunc_end43-_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart17getNumChildShapesEv,"axG",@progbits,_ZNK22btGImpactMeshShapePart17getNumChildShapesEv,comdat
	.weak	_ZNK22btGImpactMeshShapePart17getNumChildShapesEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart17getNumChildShapesEv,@function
_ZNK22btGImpactMeshShapePart17getNumChildShapesEv: # @_ZNK22btGImpactMeshShapePart17getNumChildShapesEv
	.cfi_startproc
# BB#0:
	movl	260(%rdi), %eax
	retq
.Lfunc_end44:
	.size	_ZNK22btGImpactMeshShapePart17getNumChildShapesEv, .Lfunc_end44-_ZNK22btGImpactMeshShapePart17getNumChildShapesEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart20childrenHasTransformEv,"axG",@progbits,_ZNK22btGImpactMeshShapePart20childrenHasTransformEv,comdat
	.weak	_ZNK22btGImpactMeshShapePart20childrenHasTransformEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart20childrenHasTransformEv,@function
_ZNK22btGImpactMeshShapePart20childrenHasTransformEv: # @_ZNK22btGImpactMeshShapePart20childrenHasTransformEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end45:
	.size	_ZNK22btGImpactMeshShapePart20childrenHasTransformEv, .Lfunc_end45-_ZNK22btGImpactMeshShapePart20childrenHasTransformEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv,"axG",@progbits,_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv,comdat
	.weak	_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv,@function
_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv: # @_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end46:
	.size	_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv, .Lfunc_end46-_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv,"axG",@progbits,_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv,comdat
	.weak	_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv,@function
_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv: # @_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end47:
	.size	_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv, .Lfunc_end47-_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx,"axG",@progbits,_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx,comdat
	.weak	_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx,@function
_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx: # @_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx
	.cfi_startproc
# BB#0:
	addq	$176, %rdi
	jmp	_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx # TAILCALL
.Lfunc_end48:
	.size	_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx, .Lfunc_end48-_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx,"axG",@progbits,_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx,comdat
	.weak	_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx,@function
_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx: # @_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end49:
	.size	_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx, .Lfunc_end49-_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart15lockChildShapesEv,"axG",@progbits,_ZNK22btGImpactMeshShapePart15lockChildShapesEv,comdat
	.weak	_ZNK22btGImpactMeshShapePart15lockChildShapesEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart15lockChildShapesEv,@function
_ZNK22btGImpactMeshShapePart15lockChildShapesEv: # @_ZNK22btGImpactMeshShapePart15lockChildShapesEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 32
.Lcfi96:
	.cfi_offset %rbx, -32
.Lcfi97:
	.cfi_offset %r14, -24
.Lcfi98:
	.cfi_offset %r15, -16
	movq	168(%rdi), %rbx
	movl	44(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB50_2
# BB#1:
	incl	%eax
	jmp	.LBB50_3
.LBB50_2:
	movq	16(%rbx), %rdi
	movq	(%rdi), %r10
	leaq	48(%rbx), %rsi
	leaq	56(%rbx), %rdx
	leaq	60(%rbx), %rcx
	leaq	64(%rbx), %r8
	leaq	72(%rbx), %r9
	leaq	80(%rbx), %r11
	leaq	84(%rbx), %r14
	leaq	88(%rbx), %r15
	movl	40(%rbx), %eax
	pushq	%rax
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi102:
	.cfi_adjust_cfa_offset 8
	callq	*32(%r10)
	addq	$32, %rsp
.Lcfi103:
	.cfi_adjust_cfa_offset -32
	movl	$1, %eax
.LBB50_3:                               # %_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManager4lockEv.exit
	movl	%eax, 44(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end50:
	.size	_ZNK22btGImpactMeshShapePart15lockChildShapesEv, .Lfunc_end50-_ZNK22btGImpactMeshShapePart15lockChildShapesEv
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart17unlockChildShapesEv,"axG",@progbits,_ZNK22btGImpactMeshShapePart17unlockChildShapesEv,comdat
	.weak	_ZNK22btGImpactMeshShapePart17unlockChildShapesEv
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart17unlockChildShapesEv,@function
_ZNK22btGImpactMeshShapePart17unlockChildShapesEv: # @_ZNK22btGImpactMeshShapePart17unlockChildShapesEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 16
.Lcfi105:
	.cfi_offset %rbx, -16
	movq	168(%rdi), %rbx
	movl	44(%rbx), %eax
	testl	%eax, %eax
	je	.LBB51_5
# BB#1:
	cmpl	$2, %eax
	jl	.LBB51_3
# BB#2:
	decl	%eax
	jmp	.LBB51_4
.LBB51_3:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movl	40(%rbx), %esi
	callq	*48(%rax)
	movq	$0, 48(%rbx)
	xorl	%eax, %eax
.LBB51_4:                               # %.sink.split.i
	movl	%eax, 44(%rbx)
.LBB51_5:                               # %_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManager6unlockEv.exit
	popq	%rbx
	retq
.Lfunc_end51:
	.size	_ZNK22btGImpactMeshShapePart17unlockChildShapesEv, .Lfunc_end51-_ZNK22btGImpactMeshShapePart17unlockChildShapesEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI52_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI52_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.text._ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_,@function
_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_: # @_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi110:
	.cfi_def_cfa_offset 80
.Lcfi111:
	.cfi_offset %rbx, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movq	(%rdi), %rax
	callq	*136(%rax)
	movq	(%rax), %rcx
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movl	%ebp, %esi
	callq	*32(%rcx)
	movss	24(%rsp), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	addss	%xmm9, %xmm2
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm8, %xmm1
	movss	32(%rsp), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm10, %xmm0
	movss	.LCPI52_0(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm0
	subss	%xmm2, %xmm9
	subss	%xmm1, %xmm8
	subss	%xmm0, %xmm10
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm12         # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	mulps	%xmm5, %xmm3
	movaps	%xmm1, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	20(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm12   # xmm12 = xmm12[0],xmm7[0],xmm12[1],xmm7[1]
	mulps	%xmm12, %xmm6
	addps	%xmm3, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	mulps	%xmm7, %xmm3
	addps	%xmm6, %xmm3
	movsd	48(%rbx), %xmm11        # xmm11 = mem[0],zero
	addps	%xmm3, %xmm11
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	movss	36(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	addss	%xmm2, %xmm1
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	addss	56(%rbx), %xmm0
	movaps	.LCPI52_1(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm5
	andps	%xmm1, %xmm12
	andps	%xmm1, %xmm7
	andps	%xmm1, %xmm3
	mulss	%xmm9, %xmm3
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm5, %xmm9
	andps	%xmm1, %xmm6
	mulss	%xmm8, %xmm6
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm12, %xmm8
	addps	%xmm9, %xmm8
	andps	%xmm1, %xmm2
	mulss	%xmm10, %xmm2
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm7, %xmm10
	addps	%xmm8, %xmm10
	addss	%xmm3, %xmm6
	addss	%xmm6, %xmm2
	movaps	%xmm11, %xmm1
	subps	%xmm10, %xmm1
	movaps	%xmm0, %xmm3
	subss	%xmm2, %xmm3
	xorps	%xmm4, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1,2,3]
	movlps	%xmm1, 8(%rsp)
	movlps	%xmm5, 16(%rsp)
	addps	%xmm11, %xmm10
	addss	%xmm0, %xmm2
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm10, 24(%rsp)
	movlps	%xmm4, 32(%rsp)
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%r15)
	movups	24(%rsp), %xmm0
	movups	%xmm0, (%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end52:
	.size	_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_, .Lfunc_end52-_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZN22btGImpactMeshShapePart13getChildShapeEi,"axG",@progbits,_ZN22btGImpactMeshShapePart13getChildShapeEi,comdat
	.weak	_ZN22btGImpactMeshShapePart13getChildShapeEi
	.p2align	4, 0x90
	.type	_ZN22btGImpactMeshShapePart13getChildShapeEi,@function
_ZN22btGImpactMeshShapePart13getChildShapeEi: # @_ZN22btGImpactMeshShapePart13getChildShapeEi
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end53:
	.size	_ZN22btGImpactMeshShapePart13getChildShapeEi, .Lfunc_end53-_ZN22btGImpactMeshShapePart13getChildShapeEi
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart13getChildShapeEi,"axG",@progbits,_ZNK22btGImpactMeshShapePart13getChildShapeEi,comdat
	.weak	_ZNK22btGImpactMeshShapePart13getChildShapeEi
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart13getChildShapeEi,@function
_ZNK22btGImpactMeshShapePart13getChildShapeEi: # @_ZNK22btGImpactMeshShapePart13getChildShapeEi
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end54:
	.size	_ZNK22btGImpactMeshShapePart13getChildShapeEi, .Lfunc_end54-_ZNK22btGImpactMeshShapePart13getChildShapeEi
	.cfi_endproc

	.section	.text._ZNK22btGImpactMeshShapePart17getChildTransformEi,"axG",@progbits,_ZNK22btGImpactMeshShapePart17getChildTransformEi,comdat
	.weak	_ZNK22btGImpactMeshShapePart17getChildTransformEi
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart17getChildTransformEi,@function
_ZNK22btGImpactMeshShapePart17getChildTransformEi: # @_ZNK22btGImpactMeshShapePart17getChildTransformEi
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end55:
	.size	_ZNK22btGImpactMeshShapePart17getChildTransformEi, .Lfunc_end55-_ZNK22btGImpactMeshShapePart17getChildTransformEi
	.cfi_endproc

	.section	.text._ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform,"axG",@progbits,_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform,comdat
	.weak	_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform
	.p2align	4, 0x90
	.type	_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform,@function
_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform: # @_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end56:
	.size	_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform, .Lfunc_end56-_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform
	.cfi_endproc

	.section	.text._ZN18btGImpactMeshShapeD2Ev,"axG",@progbits,_ZN18btGImpactMeshShapeD2Ev,comdat
	.weak	_ZN18btGImpactMeshShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN18btGImpactMeshShapeD2Ev,@function
_ZN18btGImpactMeshShapeD2Ev:            # @_ZN18btGImpactMeshShapeD2Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r15
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 32
.Lcfi118:
	.cfi_offset %rbx, -32
.Lcfi119:
	.cfi_offset %r14, -24
.Lcfi120:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV18btGImpactMeshShape+16, (%r15)
	movslq	188(%r15), %r14
	testq	%r14, %r14
	movq	200(%r15), %rdi
	je	.LBB57_6
# BB#1:                                 # %.lr.ph.preheader
	leaq	-8(,%r14,8), %rbx
	.p2align	4, 0x90
.LBB57_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx), %rax
	testq	%rax, %rax
	je	.LBB57_5
# BB#3:                                 #   in Loop: Header=BB57_2 Depth=1
	movq	(%rax), %rcx
.Ltmp59:
	movq	%rax, %rdi
	callq	*8(%rcx)
.Ltmp60:
# BB#4:                                 # %..backedge_crit_edge
                                        #   in Loop: Header=BB57_2 Depth=1
	movq	200(%r15), %rdi
.LBB57_5:                               # %.backedge
                                        #   in Loop: Header=BB57_2 Depth=1
	addq	$-8, %rbx
	decl	%r14d
	jne	.LBB57_2
.LBB57_6:                               # %._crit_edge
	testq	%rdi, %rdi
	je	.LBB57_10
# BB#7:
	leaq	208(%r15), %rbx
	cmpb	$0, (%rbx)
	je	.LBB57_9
# BB#8:
.Ltmp62:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp63:
.LBB57_9:                               # %.noexc
	movq	$0, 200(%r15)
.LBB57_10:
	movb	$1, 208(%r15)
	movq	$0, 200(%r15)
	movl	$0, 188(%r15)
	movl	$0, 192(%r15)
	movq	$_ZTV23btGImpactShapeInterface+16, (%r15)
	movq	104(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB57_14
# BB#11:
	cmpb	$0, 112(%r15)
	je	.LBB57_13
# BB#12:
.Ltmp76:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp77:
.LBB57_13:                              # %.noexc.i15
	movq	$0, 104(%r15)
.LBB57_14:                              # %_ZN23btGImpactShapeInterfaceD2Ev.exit16
	movb	$1, 112(%r15)
	movq	$0, 104(%r15)
	movq	$0, 92(%r15)
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN14btConcaveShapeD2Ev # TAILCALL
.LBB57_15:
.Ltmp78:
	movq	%rax, %r14
.Ltmp79:
	movq	%r15, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp80:
	jmp	.LBB57_16
.LBB57_17:
.Ltmp81:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB57_18:
.Ltmp64:
	movq	%rax, %r14
	jmp	.LBB57_19
.LBB57_32:
.Ltmp61:
	movq	%rax, %r14
	leaq	208(%r15), %rbx
.LBB57_19:
	movq	200(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB57_23
# BB#20:
	cmpb	$0, (%rbx)
	je	.LBB57_22
# BB#21:
.Ltmp65:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp66:
.LBB57_22:                              # %.noexc18
	movq	$0, 200(%r15)
.LBB57_23:                              # %_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartED2Ev.exit19
	movb	$1, 208(%r15)
	movq	$0, 200(%r15)
	movl	$0, 188(%r15)
	movl	$0, 192(%r15)
	movq	$_ZTV23btGImpactShapeInterface+16, (%r15)
	movq	104(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB57_27
# BB#24:
	cmpb	$0, 112(%r15)
	je	.LBB57_26
# BB#25:
.Ltmp67:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp68:
.LBB57_26:                              # %.noexc.i
	movq	$0, 104(%r15)
.LBB57_27:
	movb	$1, 112(%r15)
	movq	$0, 104(%r15)
	movq	$0, 92(%r15)
.Ltmp73:
	movq	%r15, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp74:
.LBB57_16:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB57_28:
.Ltmp69:
	movq	%rax, %r14
.Ltmp70:
	movq	%r15, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp71:
	jmp	.LBB57_31
.LBB57_29:
.Ltmp72:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB57_30:
.Ltmp75:
	movq	%rax, %r14
.LBB57_31:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end57:
	.size	_ZN18btGImpactMeshShapeD2Ev, .Lfunc_end57-_ZN18btGImpactMeshShapeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table57:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp59-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin5   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin5   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin5   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp79-.Ltmp77         #   Call between .Ltmp77 and .Ltmp79
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin5   #     jumps to .Ltmp81
	.byte	1                       #   On action: 1
	.long	.Ltmp65-.Lfunc_begin5   # >> Call Site 6 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp75-.Lfunc_begin5   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp67-.Lfunc_begin5   # >> Call Site 7 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin5   #     jumps to .Ltmp69
	.byte	1                       #   On action: 1
	.long	.Ltmp73-.Lfunc_begin5   # >> Call Site 8 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin5   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp74-.Lfunc_begin5   # >> Call Site 9 <<
	.long	.Ltmp70-.Ltmp74         #   Call between .Ltmp74 and .Ltmp70
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin5   # >> Call Site 10 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin5   #     jumps to .Ltmp72
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN18btGImpactMeshShapeD0Ev,"axG",@progbits,_ZN18btGImpactMeshShapeD0Ev,comdat
	.weak	_ZN18btGImpactMeshShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN18btGImpactMeshShapeD0Ev,@function
_ZN18btGImpactMeshShapeD0Ev:            # @_ZN18btGImpactMeshShapeD0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 32
.Lcfi124:
	.cfi_offset %rbx, -24
.Lcfi125:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp82:
	callq	_ZN18btGImpactMeshShapeD2Ev
.Ltmp83:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB58_2:
.Ltmp84:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end58:
	.size	_ZN18btGImpactMeshShapeD0Ev, .Lfunc_end58-_ZN18btGImpactMeshShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table58:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp82-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin6   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Lfunc_end58-.Ltmp83    #   Call between .Ltmp83 and .Lfunc_end58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN18btGImpactMeshShape15setLocalScalingERK9btVector3,"axG",@progbits,_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3,comdat
	.weak	_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3,@function
_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3: # @_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi128:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 48
.Lcfi131:
	.cfi_offset %rbx, -40
.Lcfi132:
	.cfi_offset %r12, -32
.Lcfi133:
	.cfi_offset %r14, -24
.Lcfi134:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movups	(%r14), %xmm0
	movups	%xmm0, 64(%r15)
	movslq	188(%r15), %r12
	testq	%r12, %r12
	je	.LBB59_3
# BB#1:                                 # %.lr.ph
	leaq	-8(,%r12,8), %rbx
	.p2align	4, 0x90
.LBB59_2:                               # =>This Inner Loop Header: Depth=1
	movq	200(%r15), %rax
	movq	(%rax,%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*48(%rax)
	addq	$-8, %rbx
	decl	%r12d
	jne	.LBB59_2
.LBB59_3:                               # %._crit_edge
	movb	$1, 60(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end59:
	.size	_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3, .Lfunc_end59-_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape7getNameEv,"axG",@progbits,_ZNK18btGImpactMeshShape7getNameEv,comdat
	.weak	_ZNK18btGImpactMeshShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape7getNameEv,@function
_ZNK18btGImpactMeshShape7getNameEv:     # @_ZNK18btGImpactMeshShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.2, %eax
	retq
.Lfunc_end60:
	.size	_ZNK18btGImpactMeshShape7getNameEv, .Lfunc_end60-_ZNK18btGImpactMeshShape7getNameEv
	.cfi_endproc

	.section	.text._ZN18btGImpactMeshShape9setMarginEf,"axG",@progbits,_ZN18btGImpactMeshShape9setMarginEf,comdat
	.weak	_ZN18btGImpactMeshShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN18btGImpactMeshShape9setMarginEf,@function
_ZN18btGImpactMeshShape9setMarginEf:    # @_ZN18btGImpactMeshShape9setMarginEf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi138:
	.cfi_def_cfa_offset 48
.Lcfi139:
	.cfi_offset %rbx, -32
.Lcfi140:
	.cfi_offset %r14, -24
.Lcfi141:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movss	%xmm0, 24(%r14)
	movslq	188(%r14), %r15
	testq	%r15, %r15
	je	.LBB61_3
# BB#1:                                 # %.lr.ph
	leaq	-8(,%r15,8), %rbx
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB61_2:                               # =>This Inner Loop Header: Depth=1
	movq	200(%r14), %rax
	movq	(%rax,%rbx), %rdi
	movq	(%rdi), %rax
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*80(%rax)
	addq	$-8, %rbx
	decl	%r15d
	jne	.LBB61_2
.LBB61_3:                               # %._crit_edge
	movb	$1, 60(%r14)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end61:
	.size	_ZN18btGImpactMeshShape9setMarginEf, .Lfunc_end61-_ZN18btGImpactMeshShape9setMarginEf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI62_0:
	.long	2139095039              # float 3.40282347E+38
.LCPI62_1:
	.long	4286578687              # float -3.40282347E+38
	.section	.text._ZN18btGImpactMeshShape13calcLocalAABBEv,"axG",@progbits,_ZN18btGImpactMeshShape13calcLocalAABBEv,comdat
	.weak	_ZN18btGImpactMeshShape13calcLocalAABBEv
	.p2align	4, 0x90
	.type	_ZN18btGImpactMeshShape13calcLocalAABBEv,@function
_ZN18btGImpactMeshShape13calcLocalAABBEv: # @_ZN18btGImpactMeshShape13calcLocalAABBEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi146:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi148:
	.cfi_def_cfa_offset 64
.Lcfi149:
	.cfi_offset %rbx, -56
.Lcfi150:
	.cfi_offset %r12, -48
.Lcfi151:
	.cfi_offset %r13, -40
.Lcfi152:
	.cfi_offset %r14, -32
.Lcfi153:
	.cfi_offset %r15, -24
.Lcfi154:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movabsq	$9187343237679939583, %rax # imm = 0x7F7FFFFF7F7FFFFF
	movq	%rax, 28(%r14)
	movl	$2139095039, 36(%r14)   # imm = 0x7F7FFFFF
	movl	$-8388609, 44(%r14)     # imm = 0xFF7FFFFF
	movabsq	$-36028797027352577, %rax # imm = 0xFF7FFFFFFF7FFFFF
	movq	%rax, 48(%r14)
	movslq	188(%r14), %r12
	testq	%r12, %r12
	je	.LBB62_5
# BB#1:                                 # %.lr.ph
	leaq	28(%r14), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	44(%r14), %r13
	movq	200(%r14), %r8
	movq	%r12, %r15
	decq	%r15
	movd	.LCPI62_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movd	.LCPI62_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm3
	movdqa	%xmm2, %xmm4
	movdqa	%xmm2, %xmm5
	.p2align	4, 0x90
.LBB62_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%r8,%r15,8), %rbx
	cmpb	$0, 60(%rbx)
	je	.LBB62_4
# BB#3:                                 #   in Loop: Header=BB62_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*104(%rax)
	movb	$0, 60(%rbx)
	movq	200(%r14), %r8
	movq	(%r8,%r15,8), %rbx
	movd	28(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movd	32(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movd	36(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movd	44(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movd	48(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movd	52(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
.LBB62_4:                               # %_ZN23btGImpactShapeInterface11updateBoundEv.exit
                                        #   in Loop: Header=BB62_2 Depth=1
	leaq	28(%rbx), %rcx
	ucomiss	28(%rbx), %xmm5
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rdx
	cmovaq	%rcx, %rdx
	movl	(%rdx), %edx
	movl	%edx, 28(%r14)
	ucomiss	32(%rbx), %xmm4
	movq	%rax, %rsi
	cmovaq	%rcx, %rsi
	movl	4(%rsi), %esi
	movl	%esi, 32(%r14)
	ucomiss	36(%rbx), %xmm2
	cmovbeq	%rax, %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 36(%r14)
	leaq	44(%rbx), %rdi
	movss	44(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	movq	%r13, %rax
	cmovaq	%rdi, %rax
	movl	(%rax), %eax
	movl	%eax, 44(%r14)
	movss	48(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	movq	%r13, %rbp
	cmovaq	%rdi, %rbp
	movl	4(%rbp), %ebp
	movl	%ebp, 48(%r14)
	movss	52(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	cmovbeq	%r13, %rdi
	movl	8(%rdi), %edi
	movl	%edi, 52(%r14)
	movd	%edx, %xmm5
	movd	%esi, %xmm4
	movd	%ecx, %xmm2
	movd	%eax, %xmm3
	movd	%ebp, %xmm1
	movd	%edi, %xmm0
	decq	%r15
	decl	%r12d
	jne	.LBB62_2
.LBB62_5:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end62:
	.size	_ZN18btGImpactMeshShape13calcLocalAABBEv, .Lfunc_end62-_ZN18btGImpactMeshShape13calcLocalAABBEv
	.cfi_endproc

	.section	.text._ZN18btGImpactMeshShape10postUpdateEv,"axG",@progbits,_ZN18btGImpactMeshShape10postUpdateEv,comdat
	.weak	_ZN18btGImpactMeshShape10postUpdateEv
	.p2align	4, 0x90
	.type	_ZN18btGImpactMeshShape10postUpdateEv,@function
_ZN18btGImpactMeshShape10postUpdateEv:  # @_ZN18btGImpactMeshShape10postUpdateEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi155:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 32
.Lcfi158:
	.cfi_offset %rbx, -32
.Lcfi159:
	.cfi_offset %r14, -24
.Lcfi160:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	188(%r14), %r15
	testq	%r15, %r15
	je	.LBB63_3
# BB#1:                                 # %.lr.ph
	leaq	-8(,%r15,8), %rbx
	.p2align	4, 0x90
.LBB63_2:                               # =>This Inner Loop Header: Depth=1
	movq	200(%r14), %rax
	movq	(%rax,%rbx), %rdi
	movq	(%rdi), %rax
	callq	*112(%rax)
	addq	$-8, %rbx
	decl	%r15d
	jne	.LBB63_2
.LBB63_3:                               # %._crit_edge
	movb	$1, 60(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end63:
	.size	_ZN18btGImpactMeshShape10postUpdateEv, .Lfunc_end63-_ZN18btGImpactMeshShape10postUpdateEv
	.cfi_endproc

	.section	.text._ZN18btGImpactMeshShape19getGImpactShapeTypeEv,"axG",@progbits,_ZN18btGImpactMeshShape19getGImpactShapeTypeEv,comdat
	.weak	_ZN18btGImpactMeshShape19getGImpactShapeTypeEv
	.p2align	4, 0x90
	.type	_ZN18btGImpactMeshShape19getGImpactShapeTypeEv,@function
_ZN18btGImpactMeshShape19getGImpactShapeTypeEv: # @_ZN18btGImpactMeshShape19getGImpactShapeTypeEv
	.cfi_startproc
# BB#0:
	movl	$2, %eax
	retq
.Lfunc_end64:
	.size	_ZN18btGImpactMeshShape19getGImpactShapeTypeEv, .Lfunc_end64-_ZN18btGImpactMeshShape19getGImpactShapeTypeEv
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape19getPrimitiveManagerEv,"axG",@progbits,_ZNK18btGImpactMeshShape19getPrimitiveManagerEv,comdat
	.weak	_ZNK18btGImpactMeshShape19getPrimitiveManagerEv
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape19getPrimitiveManagerEv,@function
_ZNK18btGImpactMeshShape19getPrimitiveManagerEv: # @_ZNK18btGImpactMeshShape19getPrimitiveManagerEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end65:
	.size	_ZNK18btGImpactMeshShape19getPrimitiveManagerEv, .Lfunc_end65-_ZNK18btGImpactMeshShape19getPrimitiveManagerEv
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape17getNumChildShapesEv,"axG",@progbits,_ZNK18btGImpactMeshShape17getNumChildShapesEv,comdat
	.weak	_ZNK18btGImpactMeshShape17getNumChildShapesEv
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape17getNumChildShapesEv,@function
_ZNK18btGImpactMeshShape17getNumChildShapesEv: # @_ZNK18btGImpactMeshShape17getNumChildShapesEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end66:
	.size	_ZNK18btGImpactMeshShape17getNumChildShapesEv, .Lfunc_end66-_ZNK18btGImpactMeshShape17getNumChildShapesEv
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape20childrenHasTransformEv,"axG",@progbits,_ZNK18btGImpactMeshShape20childrenHasTransformEv,comdat
	.weak	_ZNK18btGImpactMeshShape20childrenHasTransformEv
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape20childrenHasTransformEv,@function
_ZNK18btGImpactMeshShape20childrenHasTransformEv: # @_ZNK18btGImpactMeshShape20childrenHasTransformEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end67:
	.size	_ZNK18btGImpactMeshShape20childrenHasTransformEv, .Lfunc_end67-_ZNK18btGImpactMeshShape20childrenHasTransformEv
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv,"axG",@progbits,_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv,comdat
	.weak	_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv,@function
_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv: # @_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end68:
	.size	_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv, .Lfunc_end68-_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv,"axG",@progbits,_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv,comdat
	.weak	_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv,@function
_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv: # @_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end69:
	.size	_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv, .Lfunc_end69-_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx,"axG",@progbits,_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx,comdat
	.weak	_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx,@function
_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx: # @_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end70:
	.size	_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx, .Lfunc_end70-_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx,"axG",@progbits,_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx,comdat
	.weak	_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx,@function
_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx: # @_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end71:
	.size	_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx, .Lfunc_end71-_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape15lockChildShapesEv,"axG",@progbits,_ZNK18btGImpactMeshShape15lockChildShapesEv,comdat
	.weak	_ZNK18btGImpactMeshShape15lockChildShapesEv
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape15lockChildShapesEv,@function
_ZNK18btGImpactMeshShape15lockChildShapesEv: # @_ZNK18btGImpactMeshShape15lockChildShapesEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end72:
	.size	_ZNK18btGImpactMeshShape15lockChildShapesEv, .Lfunc_end72-_ZNK18btGImpactMeshShape15lockChildShapesEv
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape17unlockChildShapesEv,"axG",@progbits,_ZNK18btGImpactMeshShape17unlockChildShapesEv,comdat
	.weak	_ZNK18btGImpactMeshShape17unlockChildShapesEv
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape17unlockChildShapesEv,@function
_ZNK18btGImpactMeshShape17unlockChildShapesEv: # @_ZNK18btGImpactMeshShape17unlockChildShapesEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end73:
	.size	_ZNK18btGImpactMeshShape17unlockChildShapesEv, .Lfunc_end73-_ZNK18btGImpactMeshShape17unlockChildShapesEv
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_,@function
_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_: # @_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end74:
	.size	_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_, .Lfunc_end74-_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZN18btGImpactMeshShape13getChildShapeEi,"axG",@progbits,_ZN18btGImpactMeshShape13getChildShapeEi,comdat
	.weak	_ZN18btGImpactMeshShape13getChildShapeEi
	.p2align	4, 0x90
	.type	_ZN18btGImpactMeshShape13getChildShapeEi,@function
_ZN18btGImpactMeshShape13getChildShapeEi: # @_ZN18btGImpactMeshShape13getChildShapeEi
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end75:
	.size	_ZN18btGImpactMeshShape13getChildShapeEi, .Lfunc_end75-_ZN18btGImpactMeshShape13getChildShapeEi
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape13getChildShapeEi,"axG",@progbits,_ZNK18btGImpactMeshShape13getChildShapeEi,comdat
	.weak	_ZNK18btGImpactMeshShape13getChildShapeEi
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape13getChildShapeEi,@function
_ZNK18btGImpactMeshShape13getChildShapeEi: # @_ZNK18btGImpactMeshShape13getChildShapeEi
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end76:
	.size	_ZNK18btGImpactMeshShape13getChildShapeEi, .Lfunc_end76-_ZNK18btGImpactMeshShape13getChildShapeEi
	.cfi_endproc

	.section	.text._ZNK18btGImpactMeshShape17getChildTransformEi,"axG",@progbits,_ZNK18btGImpactMeshShape17getChildTransformEi,comdat
	.weak	_ZNK18btGImpactMeshShape17getChildTransformEi
	.p2align	4, 0x90
	.type	_ZNK18btGImpactMeshShape17getChildTransformEi,@function
_ZNK18btGImpactMeshShape17getChildTransformEi: # @_ZNK18btGImpactMeshShape17getChildTransformEi
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end77:
	.size	_ZNK18btGImpactMeshShape17getChildTransformEi, .Lfunc_end77-_ZNK18btGImpactMeshShape17getChildTransformEi
	.cfi_endproc

	.section	.text._ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform,"axG",@progbits,_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform,comdat
	.weak	_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform
	.p2align	4, 0x90
	.type	_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform,@function
_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform: # @_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end78:
	.size	_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform, .Lfunc_end78-_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform
	.cfi_endproc

	.section	.text._ZN23btGImpactShapeInterfaceD2Ev,"axG",@progbits,_ZN23btGImpactShapeInterfaceD2Ev,comdat
	.weak	_ZN23btGImpactShapeInterfaceD2Ev
	.p2align	4, 0x90
	.type	_ZN23btGImpactShapeInterfaceD2Ev,@function
_ZN23btGImpactShapeInterfaceD2Ev:       # @_ZN23btGImpactShapeInterfaceD2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 32
.Lcfi164:
	.cfi_offset %rbx, -24
.Lcfi165:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV23btGImpactShapeInterface+16, (%rbx)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB79_4
# BB#1:
	cmpb	$0, 112(%rbx)
	je	.LBB79_3
# BB#2:
.Ltmp85:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp86:
.LBB79_3:                               # %.noexc
	movq	$0, 104(%rbx)
.LBB79_4:
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN14btConcaveShapeD2Ev # TAILCALL
.LBB79_5:
.Ltmp87:
	movq	%rax, %r14
.Ltmp88:
	movq	%rbx, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp89:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB79_7:
.Ltmp90:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end79:
	.size	_ZN23btGImpactShapeInterfaceD2Ev, .Lfunc_end79-_ZN23btGImpactShapeInterfaceD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table79:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp85-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin7   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp88-.Ltmp86         #   Call between .Ltmp86 and .Ltmp88
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin7   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.long	.Ltmp89-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Lfunc_end79-.Ltmp89    #   Call between .Ltmp89 and .Lfunc_end79
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN23btGImpactShapeInterfaceD0Ev,"axG",@progbits,_ZN23btGImpactShapeInterfaceD0Ev,comdat
	.weak	_ZN23btGImpactShapeInterfaceD0Ev
	.p2align	4, 0x90
	.type	_ZN23btGImpactShapeInterfaceD0Ev,@function
_ZN23btGImpactShapeInterfaceD0Ev:       # @_ZN23btGImpactShapeInterfaceD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi166:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi168:
	.cfi_def_cfa_offset 32
.Lcfi169:
	.cfi_offset %rbx, -24
.Lcfi170:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV23btGImpactShapeInterface+16, (%rbx)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB80_4
# BB#1:
	cmpb	$0, 112(%rbx)
	je	.LBB80_3
# BB#2:
.Ltmp91:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp92:
.LBB80_3:                               # %.noexc.i
	movq	$0, 104(%rbx)
.LBB80_4:
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
.Ltmp97:
	movq	%rbx, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp98:
# BB#5:                                 # %_ZN23btGImpactShapeInterfaceD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB80_6:
.Ltmp93:
	movq	%rax, %r14
.Ltmp94:
	movq	%rbx, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp95:
	jmp	.LBB80_9
.LBB80_7:
.Ltmp96:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB80_8:
.Ltmp99:
	movq	%rax, %r14
.LBB80_9:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end80:
	.size	_ZN23btGImpactShapeInterfaceD0Ev, .Lfunc_end80-_ZN23btGImpactShapeInterfaceD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table80:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp91-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin8   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin8   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin8   # >> Call Site 3 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin8   #     jumps to .Ltmp96
	.byte	1                       #   On action: 1
	.long	.Ltmp95-.Lfunc_begin8   # >> Call Site 4 <<
	.long	.Lfunc_end80-.Ltmp95    #   Call between .Ltmp95 and .Lfunc_end80
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx,"axG",@progbits,_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx,comdat
	.weak	_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx
	.p2align	4, 0x90
	.type	_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx,@function
_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx: # @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx
	.cfi_startproc
# BB#0:
	movq	72(%rdi), %rax
	movslq	80(%rdi), %r8
	movslq	%esi, %rcx
	imulq	%r8, %rcx
	cmpl	$3, 88(%rdi)
	jne	.LBB81_2
# BB#1:
	movswl	(%rax,%rcx), %esi
	movswl	2(%rax,%rcx), %r10d
	movswl	4(%rax,%rcx), %r8d
	jmp	.LBB81_3
.LBB81_2:
	movl	(%rax,%rcx), %esi
	movl	4(%rax,%rcx), %r10d
	movl	8(%rax,%rcx), %r8d
.LBB81_3:                               # %_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager11get_indicesEiRiS1_S1_.exit
	movl	60(%rdi), %r9d
	movq	48(%rdi), %rax
	movl	64(%rdi), %ecx
	imull	%ecx, %esi
	movslq	%esi, %rsi
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cmpl	$1, %r9d
	jne	.LBB81_5
# BB#4:
	cvtss2sd	%xmm0, %xmm0
	mulsd	(%rax,%rsi), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 64(%rdx)
	movss	28(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	8(%rax,%rsi), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 68(%rdx)
	movss	32(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	16(%rax,%rsi), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	jmp	.LBB81_6
.LBB81_5:
	mulss	(%rax,%rsi), %xmm0
	movss	%xmm0, 64(%rdx)
	movss	4(%rax,%rsi), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	28(%rdi), %xmm0
	movss	%xmm0, 68(%rdx)
	movss	8(%rax,%rsi), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	32(%rdi), %xmm0
.LBB81_6:                               # %_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEiR9btVector3.exit
	movss	%xmm0, 72(%rdx)
	imull	%ecx, %r10d
	movslq	%r10d, %rsi
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cmpl	$1, %r9d
	jne	.LBB81_8
# BB#7:
	cvtss2sd	%xmm0, %xmm0
	mulsd	(%rax,%rsi), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 80(%rdx)
	movss	28(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	8(%rax,%rsi), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 84(%rdx)
	movss	32(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	16(%rax,%rsi), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	jmp	.LBB81_9
.LBB81_8:
	mulss	(%rax,%rsi), %xmm0
	movss	%xmm0, 80(%rdx)
	movss	4(%rax,%rsi), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	28(%rdi), %xmm0
	movss	%xmm0, 84(%rdx)
	movss	8(%rax,%rsi), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	32(%rdi), %xmm0
.LBB81_9:                               # %_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEiR9btVector3.exit8
	movss	%xmm0, 88(%rdx)
	imull	%r8d, %ecx
	movslq	%ecx, %rcx
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cmpl	$1, %r9d
	jne	.LBB81_11
# BB#10:
	cvtss2sd	%xmm0, %xmm0
	mulsd	(%rax,%rcx), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 96(%rdx)
	movss	28(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	8(%rax,%rcx), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 100(%rdx)
	movss	32(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	16(%rax,%rcx), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	jmp	.LBB81_12
.LBB81_11:
	mulss	(%rax,%rcx), %xmm0
	movss	%xmm0, 96(%rdx)
	movss	4(%rax,%rcx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	28(%rdi), %xmm0
	movss	%xmm0, 100(%rdx)
	movss	8(%rax,%rcx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	32(%rdi), %xmm0
.LBB81_12:                              # %_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEiR9btVector3.exit6
	movss	%xmm0, 104(%rdx)
	movq	(%rdx), %rax
	movq	80(%rax), %rax
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %rdi
	jmpq	*%rax                   # TAILCALL
.Lfunc_end81:
	.size	_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx, .Lfunc_end81-_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx
	.cfi_endproc

	.type	_ZTV22btGImpactCompoundShape,@object # @_ZTV22btGImpactCompoundShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV22btGImpactCompoundShape
	.p2align	3
_ZTV22btGImpactCompoundShape:
	.quad	0
	.quad	_ZTI22btGImpactCompoundShape
	.quad	_ZN22btGImpactCompoundShapeD2Ev
	.quad	_ZN22btGImpactCompoundShapeD0Ev
	.quad	_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3
	.quad	_ZNK23btGImpactShapeInterface15getLocalScalingEv
	.quad	_ZNK22btGImpactCompoundShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK22btGImpactCompoundShape7getNameEv
	.quad	_ZN23btGImpactShapeInterface9setMarginEf
	.quad	_ZNK14btConcaveShape9getMarginEv
	.quad	_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.quad	_ZN23btGImpactShapeInterface13calcLocalAABBEv
	.quad	_ZN23btGImpactShapeInterface10postUpdateEv
	.quad	_ZNK23btGImpactShapeInterface12getShapeTypeEv
	.quad	_ZN22btGImpactCompoundShape19getGImpactShapeTypeEv
	.quad	_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv
	.quad	_ZNK22btGImpactCompoundShape17getNumChildShapesEv
	.quad	_ZNK22btGImpactCompoundShape20childrenHasTransformEv
	.quad	_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv
	.quad	_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv
	.quad	_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx
	.quad	_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.quad	_ZNK23btGImpactShapeInterface15lockChildShapesEv
	.quad	_ZNK23btGImpactShapeInterface17unlockChildShapesEv
	.quad	_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_
	.quad	_ZN22btGImpactCompoundShape13getChildShapeEi
	.quad	_ZNK22btGImpactCompoundShape13getChildShapeEi
	.quad	_ZNK22btGImpactCompoundShape17getChildTransformEi
	.quad	_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform
	.quad	_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.size	_ZTV22btGImpactCompoundShape, 272

	.type	_ZTS22btGImpactCompoundShape,@object # @_ZTS22btGImpactCompoundShape
	.globl	_ZTS22btGImpactCompoundShape
	.p2align	4
_ZTS22btGImpactCompoundShape:
	.asciz	"22btGImpactCompoundShape"
	.size	_ZTS22btGImpactCompoundShape, 25

	.type	_ZTS23btGImpactShapeInterface,@object # @_ZTS23btGImpactShapeInterface
	.section	.rodata._ZTS23btGImpactShapeInterface,"aG",@progbits,_ZTS23btGImpactShapeInterface,comdat
	.weak	_ZTS23btGImpactShapeInterface
	.p2align	4
_ZTS23btGImpactShapeInterface:
	.asciz	"23btGImpactShapeInterface"
	.size	_ZTS23btGImpactShapeInterface, 26

	.type	_ZTI23btGImpactShapeInterface,@object # @_ZTI23btGImpactShapeInterface
	.section	.rodata._ZTI23btGImpactShapeInterface,"aG",@progbits,_ZTI23btGImpactShapeInterface,comdat
	.weak	_ZTI23btGImpactShapeInterface
	.p2align	4
_ZTI23btGImpactShapeInterface:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23btGImpactShapeInterface
	.quad	_ZTI14btConcaveShape
	.size	_ZTI23btGImpactShapeInterface, 24

	.type	_ZTI22btGImpactCompoundShape,@object # @_ZTI22btGImpactCompoundShape
	.section	.rodata,"a",@progbits
	.globl	_ZTI22btGImpactCompoundShape
	.p2align	4
_ZTI22btGImpactCompoundShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22btGImpactCompoundShape
	.quad	_ZTI23btGImpactShapeInterface
	.size	_ZTI22btGImpactCompoundShape, 24

	.type	_ZTV22btGImpactMeshShapePart,@object # @_ZTV22btGImpactMeshShapePart
	.globl	_ZTV22btGImpactMeshShapePart
	.p2align	3
_ZTV22btGImpactMeshShapePart:
	.quad	0
	.quad	_ZTI22btGImpactMeshShapePart
	.quad	_ZN22btGImpactMeshShapePartD2Ev
	.quad	_ZN22btGImpactMeshShapePartD0Ev
	.quad	_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3
	.quad	_ZNK22btGImpactMeshShapePart15getLocalScalingEv
	.quad	_ZNK22btGImpactMeshShapePart21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK22btGImpactMeshShapePart7getNameEv
	.quad	_ZN22btGImpactMeshShapePart9setMarginEf
	.quad	_ZNK22btGImpactMeshShapePart9getMarginEv
	.quad	_ZNK22btGImpactMeshShapePart19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.quad	_ZN23btGImpactShapeInterface13calcLocalAABBEv
	.quad	_ZN23btGImpactShapeInterface10postUpdateEv
	.quad	_ZNK23btGImpactShapeInterface12getShapeTypeEv
	.quad	_ZN22btGImpactMeshShapePart19getGImpactShapeTypeEv
	.quad	_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv
	.quad	_ZNK22btGImpactMeshShapePart17getNumChildShapesEv
	.quad	_ZNK22btGImpactMeshShapePart20childrenHasTransformEv
	.quad	_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv
	.quad	_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv
	.quad	_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx
	.quad	_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.quad	_ZNK22btGImpactMeshShapePart15lockChildShapesEv
	.quad	_ZNK22btGImpactMeshShapePart17unlockChildShapesEv
	.quad	_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_
	.quad	_ZN22btGImpactMeshShapePart13getChildShapeEi
	.quad	_ZNK22btGImpactMeshShapePart13getChildShapeEi
	.quad	_ZNK22btGImpactMeshShapePart17getChildTransformEi
	.quad	_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform
	.quad	_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.size	_ZTV22btGImpactMeshShapePart, 272

	.type	_ZTS22btGImpactMeshShapePart,@object # @_ZTS22btGImpactMeshShapePart
	.globl	_ZTS22btGImpactMeshShapePart
	.p2align	4
_ZTS22btGImpactMeshShapePart:
	.asciz	"22btGImpactMeshShapePart"
	.size	_ZTS22btGImpactMeshShapePart, 25

	.type	_ZTI22btGImpactMeshShapePart,@object # @_ZTI22btGImpactMeshShapePart
	.globl	_ZTI22btGImpactMeshShapePart
	.p2align	4
_ZTI22btGImpactMeshShapePart:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22btGImpactMeshShapePart
	.quad	_ZTI23btGImpactShapeInterface
	.size	_ZTI22btGImpactMeshShapePart, 24

	.type	_ZTV18btGImpactMeshShape,@object # @_ZTV18btGImpactMeshShape
	.globl	_ZTV18btGImpactMeshShape
	.p2align	3
_ZTV18btGImpactMeshShape:
	.quad	0
	.quad	_ZTI18btGImpactMeshShape
	.quad	_ZN18btGImpactMeshShapeD2Ev
	.quad	_ZN18btGImpactMeshShapeD0Ev
	.quad	_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3
	.quad	_ZNK23btGImpactShapeInterface15getLocalScalingEv
	.quad	_ZNK18btGImpactMeshShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK18btGImpactMeshShape7getNameEv
	.quad	_ZN18btGImpactMeshShape9setMarginEf
	.quad	_ZNK14btConcaveShape9getMarginEv
	.quad	_ZNK18btGImpactMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.quad	_ZN18btGImpactMeshShape13calcLocalAABBEv
	.quad	_ZN18btGImpactMeshShape10postUpdateEv
	.quad	_ZNK23btGImpactShapeInterface12getShapeTypeEv
	.quad	_ZN18btGImpactMeshShape19getGImpactShapeTypeEv
	.quad	_ZNK18btGImpactMeshShape19getPrimitiveManagerEv
	.quad	_ZNK18btGImpactMeshShape17getNumChildShapesEv
	.quad	_ZNK18btGImpactMeshShape20childrenHasTransformEv
	.quad	_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv
	.quad	_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv
	.quad	_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx
	.quad	_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx
	.quad	_ZNK18btGImpactMeshShape15lockChildShapesEv
	.quad	_ZNK18btGImpactMeshShape17unlockChildShapesEv
	.quad	_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_
	.quad	_ZN18btGImpactMeshShape13getChildShapeEi
	.quad	_ZNK18btGImpactMeshShape13getChildShapeEi
	.quad	_ZNK18btGImpactMeshShape17getChildTransformEi
	.quad	_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform
	.quad	_ZNK18btGImpactMeshShape7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.size	_ZTV18btGImpactMeshShape, 272

	.type	_ZTS18btGImpactMeshShape,@object # @_ZTS18btGImpactMeshShape
	.globl	_ZTS18btGImpactMeshShape
	.p2align	4
_ZTS18btGImpactMeshShape:
	.asciz	"18btGImpactMeshShape"
	.size	_ZTS18btGImpactMeshShape, 21

	.type	_ZTI18btGImpactMeshShape,@object # @_ZTI18btGImpactMeshShape
	.globl	_ZTI18btGImpactMeshShape
	.p2align	4
_ZTI18btGImpactMeshShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18btGImpactMeshShape
	.quad	_ZTI23btGImpactShapeInterface
	.size	_ZTI18btGImpactMeshShape, 24

	.type	_ZZN11btTransform11getIdentityEvE17identityTransform,@object # @_ZZN11btTransform11getIdentityEvE17identityTransform
	.section	.bss._ZZN11btTransform11getIdentityEvE17identityTransform,"aGw",@nobits,_ZZN11btTransform11getIdentityEvE17identityTransform,comdat
	.weak	_ZZN11btTransform11getIdentityEvE17identityTransform
	.p2align	2
_ZZN11btTransform11getIdentityEvE17identityTransform:
	.zero	64
	.size	_ZZN11btTransform11getIdentityEvE17identityTransform, 64

	.type	_ZGVZN11btTransform11getIdentityEvE17identityTransform,@object # @_ZGVZN11btTransform11getIdentityEvE17identityTransform
	.section	.bss._ZGVZN11btTransform11getIdentityEvE17identityTransform,"aGw",@nobits,_ZGVZN11btTransform11getIdentityEvE17identityTransform,comdat
	.weak	_ZGVZN11btTransform11getIdentityEvE17identityTransform
	.p2align	3
_ZGVZN11btTransform11getIdentityEvE17identityTransform:
	.quad	0                       # 0x0
	.size	_ZGVZN11btTransform11getIdentityEvE17identityTransform, 8

	.type	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix,@object # @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
	.section	.bss._ZZN11btMatrix3x311getIdentityEvE14identityMatrix,"aGw",@nobits,_ZZN11btMatrix3x311getIdentityEvE14identityMatrix,comdat
	.weak	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
	.p2align	2
_ZZN11btMatrix3x311getIdentityEvE14identityMatrix:
	.zero	48
	.size	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix, 48

	.type	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,@object # @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix
	.section	.bss._ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,"aGw",@nobits,_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,comdat
	.weak	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix
	.p2align	3
_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix:
	.quad	0                       # 0x0
	.size	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, 8

	.type	_ZTV23btGImpactShapeInterface,@object # @_ZTV23btGImpactShapeInterface
	.section	.rodata._ZTV23btGImpactShapeInterface,"aG",@progbits,_ZTV23btGImpactShapeInterface,comdat
	.weak	_ZTV23btGImpactShapeInterface
	.p2align	3
_ZTV23btGImpactShapeInterface:
	.quad	0
	.quad	_ZTI23btGImpactShapeInterface
	.quad	_ZN23btGImpactShapeInterfaceD2Ev
	.quad	_ZN23btGImpactShapeInterfaceD0Ev
	.quad	_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3
	.quad	_ZNK23btGImpactShapeInterface15getLocalScalingEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN23btGImpactShapeInterface9setMarginEf
	.quad	_ZNK14btConcaveShape9getMarginEv
	.quad	_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.quad	_ZN23btGImpactShapeInterface13calcLocalAABBEv
	.quad	_ZN23btGImpactShapeInterface10postUpdateEv
	.quad	_ZNK23btGImpactShapeInterface12getShapeTypeEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK23btGImpactShapeInterface15lockChildShapesEv
	.quad	_ZNK23btGImpactShapeInterface17unlockChildShapesEv
	.quad	_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.size	_ZTV23btGImpactShapeInterface, 272

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"GImpactCompound"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"GImpactMeshShapePart"
	.size	.L.str.1, 21

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"GImpactMesh"
	.size	.L.str.2, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
