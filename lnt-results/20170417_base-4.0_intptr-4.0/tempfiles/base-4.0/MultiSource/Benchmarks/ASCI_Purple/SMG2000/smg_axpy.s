	.text
	.file	"smg_axpy.bc"
	.globl	hypre_SMGAxpy
	.p2align	4, 0x90
	.type	hypre_SMGAxpy,@function
hypre_SMGAxpy:                          # @hypre_SMGAxpy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi6:
	.cfi_def_cfa_offset 320
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	callq	hypre_BoxCreate
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	movq	8(%rbx), %rax
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB0_26
# BB#1:                                 # %.lr.ph340
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	xorl	%ebp, %ebp
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_20 Depth 4
                                        #         Child Loop BB0_15 Depth 4
	movq	(%rcx), %rax
	leaq	(,%rbp,8), %rcx
	leaq	(%rcx,%rcx,2), %r15
	movl	(%rax,%r15), %ecx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	%ecx, (%rbx)
	movl	4(%rax,%r15), %ecx
	movl	%ecx, 4(%rbx)
	movl	8(%rax,%r15), %ecx
	movl	%ecx, 8(%rbx)
	movl	12(%rax,%r15), %ecx
	movl	%ecx, 12(%rbx)
	movl	16(%rax,%r15), %ecx
	movl	%ecx, 16(%rbx)
	movl	20(%rax,%r15), %eax
	movl	%eax, 20(%rbx)
	movq	%rbx, %rdi
	movq	176(%rsp), %rsi         # 8-byte Reload
	movq	120(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdx
	callq	hypre_ProjectBox
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	(%rax), %r13
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rax), %r12
	movq	40(%rdx), %rax
	movslq	(%rax,%rbp,4), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	40(%rcx), %rax
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	movslq	(%rax,%rbp,4), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	140(%rsp), %rdx
	callq	hypre_BoxGetStrideSize
	movl	(%r12,%r15), %edx
	movl	4(%r12,%r15), %r10d
	movl	12(%r12,%r15), %ecx
	movl	%ecx, %r8d
	subl	%edx, %r8d
	incl	%r8d
	movl	%edx, 12(%rsp)          # 4-byte Spill
	cmpl	%edx, %ecx
	movl	16(%r12,%r15), %ecx
	movl	$0, %edx
	cmovsl	%edx, %r8d
	movl	%ecx, %edi
	subl	%r10d, %edi
	incl	%edi
	cmpl	%r10d, %ecx
	movl	(%r13,%r15), %esi
	movl	4(%r13,%r15), %eax
	movl	12(%r13,%r15), %ecx
	cmovsl	%edx, %edi
	movl	%ecx, %ebx
	subl	%esi, %ebx
	incl	%ebx
	cmpl	%esi, %ecx
	movl	16(%r13,%r15), %ecx
	cmovsl	%edx, %ebx
	movl	%ecx, %r14d
	subl	%eax, %r14d
	incl	%r14d
	cmpl	%eax, %ecx
	cmovsl	%edx, %r14d
	movl	140(%rsp), %ecx
	movl	144(%rsp), %edx
	movl	148(%rsp), %ebp
	cmpl	%ecx, %edx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	cmovgel	%edx, %ecx
	cmpl	%ecx, %ebp
	movl	%ebp, 36(%rsp)          # 4-byte Spill
	cmovgel	%ebp, %ecx
	testl	%ecx, %ecx
	jle	.LBB0_25
# BB#3:                                 # %.lr.ph336
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_25
# BB#4:                                 # %.lr.ph336
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_25
# BB#5:                                 # %.preheader302.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	8(%rcx), %ecx
	movl	%esi, 72(%rsp)          # 4-byte Spill
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movl	%ecx, %r9d
	subl	8(%r12,%r15), %r9d
	subl	8(%r13,%r15), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%r8d, %r15d
	imull	%edi, %r15d
	movl	%r10d, 4(%rsp)          # 4-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	8(%rax), %ecx
	imull	%ecx, %r15d
	movl	%ebx, %edx
	imull	%r14d, %edx
	imull	%ecx, %edx
	movl	4(%rax), %r10d
	movl	%r8d, %r12d
	imull	%r10d, %r12d
	imull	%ebx, %r10d
	movslq	(%rax), %r11
	movl	%r11d, %eax
	negl	%eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	imull	%ebp, %eax
	leal	(%rax,%r10), %esi
	addl	%r12d, %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %r13d
	imull	%r13d, %esi
	imull	%r13d, %eax
	addl	%r10d, %esi
	movl	%ebp, %ecx
	imull	%r11d, %ecx
	subl	%ecx, %esi
	movl	%esi, 104(%rsp)         # 4-byte Spill
	addl	%r12d, %eax
	subl	%ecx, %eax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	movl	%ecx, %r13d
	subl	12(%rsp), %r13d         # 4-byte Folded Reload
	movl	4(%rax), %eax
	movl	%eax, %ebp
	subl	4(%rsp), %ebp           # 4-byte Folded Reload
	imull	%edi, %r9d
	addl	%ebp, %r9d
	imull	%r8d, %r9d
	addl	%r13d, %r9d
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	subl	72(%rsp), %ecx          # 4-byte Folded Reload
	subl	80(%rsp), %eax          # 4-byte Folded Reload
	movl	8(%rsp), %ebp           # 4-byte Reload
	imull	%r14d, %ebp
	addl	%eax, %ebp
	imull	%ebx, %ebp
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	(%rsi,%rdi,8), %r13
	addl	%ecx, %ebp
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	addq	%rax, %rdi
	leaq	8(%rsi,%rdi,8), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	(%rsi,%rdi,8), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	addq	%rax, %rdi
	leaq	8(%rsi,%rdi,8), %rcx
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %ecx
	movq	%r10, 64(%rsp)          # 8-byte Spill
	imull	%r10d, %ecx
	movl	%ecx, 100(%rsp)         # 4-byte Spill
	subl	%ecx, %edx
	movl	%edx, 108(%rsp)         # 4-byte Spill
	movl	%esi, %ecx
	movl	%r12d, %r8d
	imull	%r12d, %ecx
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	subl	%ecx, %r15d
	movl	%r15d, 112(%rsp)        # 4-byte Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	incq	%rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %rcx
	setne	%al
	cmpl	$1, %r11d
	sete	%dl
	andb	%al, %dl
	movb	%dl, 12(%rsp)           # 1-byte Spill
	movq	%r11, %rax
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	imulq	%rcx, %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	%r11, %r10
	shlq	$5, %r10
	movq	%r11, %r14
	shlq	$4, %r14
	movq	%r11, 80(%rsp)          # 8-byte Spill
	leaq	(,%r11,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader302.us
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_20 Depth 4
                                        #         Child Loop BB0_15 Depth 4
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	192(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	104(%rsp), %ecx         # 4-byte Reload
	jle	.LBB0_24
# BB#7:                                 # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	%edx, 116(%rsp)         # 4-byte Spill
	xorl	%r12d, %r12d
	movl	4(%rsp), %edi           # 4-byte Reload
	movl	8(%rsp), %r11d          # 4-byte Reload
	movapd	224(%rsp), %xmm4        # 16-byte Reload
	movapd	240(%rsp), %xmm5        # 16-byte Reload
	jmp	.LBB0_8
.LBB0_19:                               # %vector.ph
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	leaq	(%r13,%rsi,8), %rsi
	movq	200(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %r9
	addq	%rdx, %rax
	movq	128(%rsp), %rdx         # 8-byte Reload
	movl	$16, %ebp
	.p2align	4, 0x90
.LBB0_20:                               # %vector.body
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	-16(%rcx,%rbp), %xmm0
	movupd	(%rcx,%rbp), %xmm1
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm1
	movupd	-16(%rsi,%rbp), %xmm2
	movupd	(%rsi,%rbp), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rsi,%rbp)
	movupd	%xmm3, (%rsi,%rbp)
	addq	%r10, %rbp
	addq	$-4, %rdx
	jne	.LBB0_20
# BB#21:                                # %middle.block
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	128(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, 56(%rsp)          # 8-byte Folded Reload
	movl	%ecx, %r15d
	jne	.LBB0_10
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader.us.us
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_20 Depth 4
                                        #         Child Loop BB0_15 Depth 4
	movslq	%r11d, %rcx
	movslq	%edi, %rsi
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	jbe	.LBB0_9
# BB#16:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_8 Depth=3
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB0_9
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	64(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r12d, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	cltq
	movl	%r8d, %edx
	imull	%r12d, %edx
	addl	4(%rsp), %edx           # 4-byte Folded Reload
	movslq	%edx, %r9
	leaq	(%r13,%r9,8), %rdx
	movq	216(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rax,8), %rbx
	cmpq	%rbx, %rdx
	jae	.LBB0_19
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	208(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r9,8), %rdx
	movq	16(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rax,8), %rbx
	cmpq	%rdx, %rbx
	jae	.LBB0_19
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_8 Depth=3
	movq	%rsi, %r9
	movq	%rcx, %rax
	xorl	%r15d, %r15d
.LBB0_10:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r15d, %ecx
	testb	$1, %cl
	jne	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_8 Depth=3
	movl	%r15d, %ebx
	cmpl	%r15d, 88(%rsp)         # 4-byte Folded Reload
	jne	.LBB0_14
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_12:                               # %scalar.ph.prol
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	(%r13,%r9,8), %xmm0
	movsd	%xmm0, (%r13,%r9,8)
	movq	80(%rsp), %rcx          # 8-byte Reload
	addq	%rcx, %rax
	addq	%rcx, %r9
	leal	1(%r15), %ebx
	cmpl	%r15d, 88(%rsp)         # 4-byte Folded Reload
	je	.LBB0_22
.LBB0_14:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_8 Depth=3
	leaq	(%r13,%r9,8), %rcx
	movq	72(%rsp), %rbp          # 8-byte Reload
	leaq	(%rcx,%rbp), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %rax
	leaq	(%rax,%rbp), %rdx
	movq	24(%rsp), %rbp          # 8-byte Reload
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill>
	subl	%ebx, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_15:                               # %scalar.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rax,%rbx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	(%rcx,%rbx), %xmm0
	movsd	%xmm0, (%rcx,%rbx)
	movsd	(%rdx,%rbx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	(%rsi,%rbx), %xmm0
	movsd	%xmm0, (%rsi,%rbx)
	addq	%r14, %rbx
	addl	$-2, %ebp
	jne	.LBB0_15
.LBB0_22:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB0_8 Depth=3
	addl	64(%rsp), %r11d         # 4-byte Folded Reload
	addl	%r8d, %edi
	incl	%r12d
	cmpl	48(%rsp), %r12d         # 4-byte Folded Reload
	jne	.LBB0_8
# BB#23:                                #   in Loop: Header=BB0_6 Depth=2
	movl	96(%rsp), %eax          # 4-byte Reload
	movl	100(%rsp), %ecx         # 4-byte Reload
	movl	116(%rsp), %edx         # 4-byte Reload
.LBB0_24:                               # %._crit_edge310.us
                                        #   in Loop: Header=BB0_6 Depth=2
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	addl	108(%rsp), %ecx         # 4-byte Folded Reload
	addl	112(%rsp), %eax         # 4-byte Folded Reload
	incl	%edx
	cmpl	36(%rsp), %edx          # 4-byte Folded Reload
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jne	.LBB0_6
.LBB0_25:                               # %._crit_edge337
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	184(%rsp), %rbp         # 8-byte Reload
	incq	%rbp
	movq	152(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rbp
	jl	.LBB0_2
.LBB0_26:                               # %._crit_edge341
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	hypre_BoxDestroy
	xorl	%eax, %eax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_SMGAxpy, .Lfunc_end0-hypre_SMGAxpy
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
