	.text
	.file	"Crystal_div.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4596373779694328218     # double 0.20000000000000001
.LCPI0_1:
	.quad	4622945017495814144     # double 12
.LCPI0_2:
	.quad	4606281698874543309     # double 0.90000000000000002
.LCPI0_5:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
.LCPI0_6:
	.quad	4607182418800017408     # double 1
.LCPI0_7:
	.quad	4608083138725491507     # double 1.2
.LCPI0_8:
	.quad	4629137466983448576     # double 30
.LCPI0_9:
	.quad	4576918229304087675     # double 0.01
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_3:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.quad	4517329193108106637     # double 9.9999999999999995E-7
.LCPI0_4:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.text
	.globl	Crystal_div
	.p2align	4, 0x90
	.type	Crystal_div,@function
Crystal_div:                            # @Crystal_div
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 256
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, -104(%rsp)         # 8-byte Spill
	movq	%r8, -112(%rsp)         # 8-byte Spill
	movq	%rdx, -128(%rsp)        # 8-byte Spill
	testl	%edi, %edi
	jle	.LBB0_45
# BB#1:                                 # %.lr.ph112.preheader
	movl	%edi, %r10d
	testb	$1, %r10b
	jne	.LBB0_3
# BB#2:
	xorl	%eax, %eax
	cmpl	$1, %edi
	jne	.LBB0_4
	jmp	.LBB0_6
.LBB0_3:                                # %.lr.ph112.prol
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, -96(%rsp)
	movabsq	$4606281698874543309, %rax # imm = 0x3FECCCCCCCCCCCCD
	movq	%rax, 96(%rsp)
	movl	$1, %eax
	cmpl	$1, %edi
	je	.LBB0_6
.LBB0_4:
	movabsq	$4607182418800017408, %rdx # imm = 0x3FF0000000000000
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI0_2(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph112
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, -96(%rsp,%rax,8)
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%eax, %xmm4
	mulsd	%xmm1, %xmm4
	divsd	%xmm2, %xmm4
	addsd	%xmm3, %xmm4
	movsd	%xmm4, 96(%rsp,%rax,8)
	leal	1(%rax), %ebp
	movq	%rdx, -88(%rsp,%rax,8)
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ebp, %xmm4
	mulsd	%xmm1, %xmm4
	divsd	%xmm2, %xmm4
	addsd	%xmm3, %xmm4
	movsd	%xmm4, 104(%rsp,%rax,8)
	addq	$2, %rax
	cmpq	%r10, %rax
	jne	.LBB0_5
.LBB0_6:                                # %.preheader95
	testl	%edi, %edi
	jle	.LBB0_45
# BB#7:                                 # %.lr.ph109.preheader
	cmpl	$1, %edi
	je	.LBB0_12
# BB#8:                                 # %min.iters.checked
	movl	%edi, %r9d
	andl	$1, %r9d
	movq	%r10, %r8
	subq	%r9, %r8
	je	.LBB0_12
# BB#9:                                 # %vector.body.preheader
	leaq	-96(%rsp), %rdx
	movq	%rsp, %rbx
	movapd	.LCPI0_3(%rip), %xmm1   # xmm1 = [1.000000e-06,1.000000e-06]
	movapd	.LCPI0_4(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movq	%r8, %rax
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB0_10:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbp), %xmm3
	mulpd	(%rdx), %xmm3
	addpd	%xmm1, %xmm3
	movapd	%xmm2, %xmm4
	divpd	%xmm3, %xmm4
	movapd	%xmm4, (%rbx)
	addq	$16, %rbp
	addq	$16, %rdx
	addq	$16, %rbx
	addq	$-2, %rax
	jne	.LBB0_10
# BB#11:                                # %middle.block
	testl	%r9d, %r9d
	jne	.LBB0_13
	jmp	.LBB0_15
.LBB0_12:
	xorl	%r8d, %r8d
.LBB0_13:                               # %.lr.ph109.preheader174
	leaq	(%rsi,%r8,8), %rdx
	leaq	-96(%rsp,%r8,8), %rsi
	leaq	(%rsp,%r8,8), %rbp
	movq	%r10, %rbx
	subq	%r8, %rbx
	movsd	.LCPI0_5(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_6(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph109
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm3           # xmm3 = mem[0],zero
	mulsd	(%rsi), %xmm3
	addsd	%xmm1, %xmm3
	movapd	%xmm2, %xmm4
	divsd	%xmm3, %xmm4
	movsd	%xmm4, (%rbp)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB0_14
.LBB0_15:                               # %.preheader94
	testl	%edi, %edi
	jle	.LBB0_45
# BB#16:                                # %.lr.ph107
	movq	256(%rsp), %r11
	movsd	.LCPI0_7(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movl	%edi, %r15d
	andl	$3, %r15d
	movq	%r10, %r8
	subq	%r15, %r8
	leaq	16(%r11), %r9
	xorl	%ebp, %ebp
	movsd	.LCPI0_8(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI0_9(%rip), %xmm3   # xmm3 = mem[0],zero
	movq	%r11, %rax
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph104.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_21 Depth 2
                                        #     Child Loop BB0_25 Depth 2
	cmpl	$4, %edi
	movsd	96(%rsp,%rbp,8), %xmm6  # xmm6 = mem[0],zero
	movapd	%xmm1, %xmm5
	mulsd	%xmm6, %xmm5
	mulsd	%xmm2, %xmm6
	movsd	-96(%rsp,%rbp,8), %xmm4 # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm6
	movsd	%xmm6, (%rcx,%rbp,8)
	jb	.LBB0_23
# BB#19:                                # %min.iters.checked142
                                        #   in Loop: Header=BB0_17 Depth=1
	testq	%r8, %r8
	je	.LBB0_23
# BB#20:                                # %vector.ph146
                                        #   in Loop: Header=BB0_17 Depth=1
	movapd	%xmm5, %xmm6
	movlhps	%xmm6, %xmm6            # xmm6 = xmm6[0,0]
	movq	%r8, %rdx
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB0_21:                               # %vector.body138
                                        #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm6, -16(%rsi)
	movups	%xmm6, (%rsi)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB0_21
# BB#22:                                # %middle.block139
                                        #   in Loop: Header=BB0_17 Depth=1
	testl	%r15d, %r15d
	movq	%r8, %rbx
	jne	.LBB0_24
	jmp	.LBB0_26
	.p2align	4, 0x90
.LBB0_23:                               #   in Loop: Header=BB0_17 Depth=1
	xorl	%ebx, %ebx
.LBB0_24:                               # %scalar.ph140.preheader
                                        #   in Loop: Header=BB0_17 Depth=1
	leaq	(%rax,%rbx,8), %rdx
	movq	%r10, %rsi
	subq	%rbx, %rsi
	.p2align	4, 0x90
.LBB0_25:                               # %scalar.ph140
                                        #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm5, (%rdx)
	addq	$8, %rdx
	decq	%rsi
	jne	.LBB0_25
.LBB0_26:                               # %._crit_edge105.us
                                        #   in Loop: Header=BB0_17 Depth=1
	movsd	(%rcx,%rbp,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm3, %xmm5
	mulsd	%xmm4, %xmm5
	mulsd	(%rsp,%rbp,8), %xmm5
	leaq	(%rbp,%rbp,2), %rdx
	shlq	$5, %rdx
	addq	%r11, %rdx
	addsd	(%rdx,%rbp,8), %xmm5
	movsd	%xmm5, (%rdx,%rbp,8)
	incq	%rbp
	addq	$96, %r9
	addq	$96, %rax
	cmpq	%r10, %rbp
	jne	.LBB0_17
# BB#27:                                # %.preheader93
	testl	%edi, %edi
	jle	.LBB0_45
# BB#28:                                # %.lr.ph101.preheader
	cmpl	$1, %edi
	jne	.LBB0_30
# BB#29:
	xorl	%ebp, %ebp
	movq	256(%rsp), %r8
	jmp	.LBB0_35
.LBB0_30:                               # %min.iters.checked161
	movl	%edi, %edx
	andl	$1, %edx
	movq	%r10, %rbp
	subq	%rdx, %rbp
	movq	256(%rsp), %r8
	je	.LBB0_34
# BB#31:                                # %vector.body157.preheader
	movq	%rsp, %rsi
	movapd	.LCPI0_4(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00]
	movq	%rbp, %rax
	movq	%r8, %rbx
	.p2align	4, 0x90
.LBB0_32:                               # %vector.body157
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	movhpd	104(%rbx), %xmm2        # xmm2 = xmm2[0],mem[0]
	movapd	%xmm1, %xmm3
	divpd	%xmm2, %xmm3
	movapd	%xmm3, (%rsi)
	addq	$208, %rbx
	addq	$16, %rsi
	addq	$-2, %rax
	jne	.LBB0_32
# BB#33:                                # %middle.block158
	testl	%edx, %edx
	jne	.LBB0_35
	jmp	.LBB0_37
.LBB0_34:
	xorl	%ebp, %ebp
.LBB0_35:                               # %.lr.ph101.preheader173
	imulq	$104, %rbp, %rdx
	addq	%r8, %rdx
	leaq	(%rsp,%rbp,8), %rsi
	movq	%r10, %rax
	subq	%rbp, %rax
	movsd	.LCPI0_6(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph101
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm1, %xmm2
	divsd	(%rdx), %xmm2
	movsd	%xmm2, (%rsi)
	addq	$104, %rdx
	addq	$8, %rsi
	decq	%rax
	jne	.LBB0_36
.LBB0_37:                               # %.preheader
	testl	%edi, %edi
	jle	.LBB0_45
# BB#38:                                # %.lr.ph98.split.us.preheader
	movl	%r10d, %r12d
	andl	$1, %r12d
	movq	264(%rsp), %rax
	leaq	8(%rax), %r14
	movq	-128(%rsp), %rax        # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	256(%rsp), %rax
	leaq	8(%rax), %rbx
	movq	272(%rsp), %rax
	leaq	8(%rax), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_39:                               # %.lr.ph98.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_43 Depth 2
	testq	%r12, %r12
	movsd	(%rcx,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	movsd	(%rsp,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	jne	.LBB0_41
# BB#40:                                #   in Loop: Header=BB0_39 Depth=1
	xorl	%edx, %edx
	cmpl	$1, %edi
	jne	.LBB0_42
	jmp	.LBB0_44
	.p2align	4, 0x90
.LBB0_41:                               #   in Loop: Header=BB0_39 Depth=1
	movq	%rbp, %rdx
	shlq	$5, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movq	264(%rsp), %rsi
	movsd	(%rsi,%rdx), %xmm3      # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movsd	(%rsi), %xmm4           # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm2
	movq	256(%rsp), %rsi
	movsd	(%rsi,%rdx), %xmm4      # xmm4 = mem[0],zero
	subsd	%xmm3, %xmm4
	mulsd	%xmm1, %xmm4
	movq	272(%rsp), %rsi
	movsd	%xmm4, (%rsi,%rdx)
	movl	$1, %edx
	cmpl	$1, %edi
	je	.LBB0_44
.LBB0_42:                               # %.lr.ph98.split.us.new
                                        #   in Loop: Header=BB0_39 Depth=1
	movq	%r10, %r13
	subq	%rdx, %r13
	leaq	(%r14,%rdx,8), %r11
	movq	-120(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rdx,8), %r15
	leaq	(%rbx,%rdx,8), %r9
	leaq	(%rax,%rdx,8), %r8
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_43:                               #   Parent Loop BB0_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r11,%rdx,8), %xmm3  # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	movsd	-8(%r15,%rdx,8), %xmm4  # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	addsd	%xmm2, %xmm4
	movsd	-8(%r9,%rdx,8), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm3, %xmm2
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, -8(%r8,%rdx,8)
	movsd	(%r11,%rdx,8), %xmm3    # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	movsd	(%r15,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm3, %xmm2
	addsd	%xmm4, %xmm2
	movsd	(%r9,%rdx,8), %xmm4     # xmm4 = mem[0],zero
	subsd	%xmm3, %xmm4
	mulsd	%xmm1, %xmm4
	movsd	%xmm4, (%r8,%rdx,8)
	addq	$2, %rdx
	cmpq	%rdx, %r13
	jne	.LBB0_43
.LBB0_44:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_39 Depth=1
	movq	-112(%rsp), %rdx        # 8-byte Reload
	subsd	(%rdx,%rbp,8), %xmm2
	mulsd	%xmm1, %xmm2
	movq	-104(%rsp), %rdx        # 8-byte Reload
	movsd	%xmm2, (%rdx,%rbp,8)
	incq	%rbp
	addq	$96, %r14
	addq	$96, %rbx
	addq	$96, %rax
	cmpq	%r10, %rbp
	jne	.LBB0_39
.LBB0_45:                               # %._crit_edge99
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Crystal_div, .Lfunc_end0-Crystal_div
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
