	.text
	.file	"btEmptyShape.bc"
	.globl	_ZN12btEmptyShapeC2Ev
	.p2align	4, 0x90
	.type	_ZN12btEmptyShapeC2Ev,@function
_ZN12btEmptyShapeC2Ev:                  # @_ZN12btEmptyShapeC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN14btConcaveShapeC2Ev
	movq	$_ZTV12btEmptyShape+16, (%rbx)
	movl	$27, 8(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN12btEmptyShapeC2Ev, .Lfunc_end0-_ZN12btEmptyShapeC2Ev
	.cfi_endproc

	.globl	_ZN12btEmptyShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN12btEmptyShapeD2Ev,@function
_ZN12btEmptyShapeD2Ev:                  # @_ZN12btEmptyShapeD2Ev
	.cfi_startproc
# BB#0:
	jmp	_ZN14btConcaveShapeD2Ev # TAILCALL
.Lfunc_end1:
	.size	_ZN12btEmptyShapeD2Ev, .Lfunc_end1-_ZN12btEmptyShapeD2Ev
	.cfi_endproc

	.globl	_ZN12btEmptyShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN12btEmptyShapeD0Ev,@function
_ZN12btEmptyShapeD0Ev:                  # @_ZN12btEmptyShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp1:
# BB#1:                                 # %_ZN12btEmptyShapeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN12btEmptyShapeD0Ev, .Lfunc_end2-_ZN12btEmptyShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK12btEmptyShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK12btEmptyShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK12btEmptyShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK12btEmptyShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 80
.Lcfi12:
	.cfi_offset %rbx, -40
.Lcfi13:
	.cfi_offset %r12, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movsd	48(%r12), %xmm1         # xmm1 = mem[0],zero
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	unpcklps	(%rsp), %xmm2   # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	subps	%xmm2, %xmm1
	movaps	%xmm2, %xmm5
	movss	56(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm2
	xorps	%xmm3, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm1, (%r15)
	movlps	%xmm4, 8(%r15)
	movsd	48(%r12), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm5, %xmm1
	addss	56(%r12), %xmm0
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movlps	%xmm1, (%r14)
	movlps	%xmm3, 8(%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZNK12btEmptyShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end3-_ZNK12btEmptyShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.globl	_ZNK12btEmptyShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK12btEmptyShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK12btEmptyShape21calculateLocalInertiaEfR9btVector3: # @_ZNK12btEmptyShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end4:
	.size	_ZNK12btEmptyShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end4-_ZNK12btEmptyShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZN12btEmptyShape15setLocalScalingERK9btVector3,"axG",@progbits,_ZN12btEmptyShape15setLocalScalingERK9btVector3,comdat
	.weak	_ZN12btEmptyShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN12btEmptyShape15setLocalScalingERK9btVector3,@function
_ZN12btEmptyShape15setLocalScalingERK9btVector3: # @_ZN12btEmptyShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 28(%rdi)
	retq
.Lfunc_end5:
	.size	_ZN12btEmptyShape15setLocalScalingERK9btVector3, .Lfunc_end5-_ZN12btEmptyShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.section	.text._ZNK12btEmptyShape15getLocalScalingEv,"axG",@progbits,_ZNK12btEmptyShape15getLocalScalingEv,comdat
	.weak	_ZNK12btEmptyShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK12btEmptyShape15getLocalScalingEv,@function
_ZNK12btEmptyShape15getLocalScalingEv:  # @_ZNK12btEmptyShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	28(%rdi), %rax
	retq
.Lfunc_end6:
	.size	_ZNK12btEmptyShape15getLocalScalingEv, .Lfunc_end6-_ZNK12btEmptyShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK12btEmptyShape7getNameEv,"axG",@progbits,_ZNK12btEmptyShape7getNameEv,comdat
	.weak	_ZNK12btEmptyShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK12btEmptyShape7getNameEv,@function
_ZNK12btEmptyShape7getNameEv:           # @_ZNK12btEmptyShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end7:
	.size	_ZNK12btEmptyShape7getNameEv, .Lfunc_end7-_ZNK12btEmptyShape7getNameEv
	.cfi_endproc

	.section	.text._ZN14btConcaveShape9setMarginEf,"axG",@progbits,_ZN14btConcaveShape9setMarginEf,comdat
	.weak	_ZN14btConcaveShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN14btConcaveShape9setMarginEf,@function
_ZN14btConcaveShape9setMarginEf:        # @_ZN14btConcaveShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 24(%rdi)
	retq
.Lfunc_end8:
	.size	_ZN14btConcaveShape9setMarginEf, .Lfunc_end8-_ZN14btConcaveShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK14btConcaveShape9getMarginEv,"axG",@progbits,_ZNK14btConcaveShape9getMarginEv,comdat
	.weak	_ZNK14btConcaveShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK14btConcaveShape9getMarginEv,@function
_ZNK14btConcaveShape9getMarginEv:       # @_ZNK14btConcaveShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end9:
	.size	_ZNK14btConcaveShape9getMarginEv, .Lfunc_end9-_ZNK14btConcaveShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,"axG",@progbits,_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,comdat
	.weak	_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,@function
_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_: # @_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_, .Lfunc_end10-_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_endproc

	.type	_ZTV12btEmptyShape,@object # @_ZTV12btEmptyShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV12btEmptyShape
	.p2align	3
_ZTV12btEmptyShape:
	.quad	0
	.quad	_ZTI12btEmptyShape
	.quad	_ZN12btEmptyShapeD2Ev
	.quad	_ZN12btEmptyShapeD0Ev
	.quad	_ZNK12btEmptyShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN12btEmptyShape15setLocalScalingERK9btVector3
	.quad	_ZNK12btEmptyShape15getLocalScalingEv
	.quad	_ZNK12btEmptyShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK12btEmptyShape7getNameEv
	.quad	_ZN14btConcaveShape9setMarginEf
	.quad	_ZNK14btConcaveShape9getMarginEv
	.quad	_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.size	_ZTV12btEmptyShape, 120

	.type	_ZTS12btEmptyShape,@object # @_ZTS12btEmptyShape
	.globl	_ZTS12btEmptyShape
_ZTS12btEmptyShape:
	.asciz	"12btEmptyShape"
	.size	_ZTS12btEmptyShape, 15

	.type	_ZTI12btEmptyShape,@object # @_ZTI12btEmptyShape
	.globl	_ZTI12btEmptyShape
	.p2align	4
_ZTI12btEmptyShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS12btEmptyShape
	.quad	_ZTI14btConcaveShape
	.size	_ZTI12btEmptyShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Empty"
	.size	.L.str, 6


	.globl	_ZN12btEmptyShapeC1Ev
	.type	_ZN12btEmptyShapeC1Ev,@function
_ZN12btEmptyShapeC1Ev = _ZN12btEmptyShapeC2Ev
	.globl	_ZN12btEmptyShapeD1Ev
	.type	_ZN12btEmptyShapeD1Ev,@function
_ZN12btEmptyShapeD1Ev = _ZN12btEmptyShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
