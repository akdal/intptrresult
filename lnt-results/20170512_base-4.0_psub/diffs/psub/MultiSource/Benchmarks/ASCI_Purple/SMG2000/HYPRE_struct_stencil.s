	.text
	.file	"HYPRE_struct_stencil.bc"
	.globl	HYPRE_StructStencilCreate
	.p2align	4, 0x90
	.type	HYPRE_StructStencilCreate,@function
HYPRE_StructStencilCreate:              # @HYPRE_StructStencilCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movl	%edi, %ebx
	movl	$12, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movl	%ebx, %edi
	movl	%ebp, %esi
	movq	%rax, %rdx
	callq	hypre_StructStencilCreate
	movq	%rax, (%r14)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	HYPRE_StructStencilCreate, .Lfunc_end0-HYPRE_StructStencilCreate
	.cfi_endproc

	.globl	HYPRE_StructStencilSetElement
	.p2align	4, 0x90
	.type	HYPRE_StructStencilSetElement,@function
HYPRE_StructStencilSetElement:          # @HYPRE_StructStencilSetElement
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	$0, (%rax,%rcx,4)
	movl	$0, 4(%rax,%rcx,4)
	movl	$0, 8(%rax,%rcx,4)
	cmpl	$0, 16(%rdi)
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	(%rax,%rcx,4), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rcx,4), %esi
	movl	%esi, (%rax,%rcx,4)
	incq	%rcx
	movslq	16(%rdi), %rsi
	cmpq	%rsi, %rcx
	jl	.LBB1_2
.LBB1_3:                                # %._crit_edge
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	HYPRE_StructStencilSetElement, .Lfunc_end1-HYPRE_StructStencilSetElement
	.cfi_endproc

	.globl	HYPRE_StructStencilDestroy
	.p2align	4, 0x90
	.type	HYPRE_StructStencilDestroy,@function
HYPRE_StructStencilDestroy:             # @HYPRE_StructStencilDestroy
	.cfi_startproc
# BB#0:
	jmp	hypre_StructStencilDestroy # TAILCALL
.Lfunc_end2:
	.size	HYPRE_StructStencilDestroy, .Lfunc_end2-HYPRE_StructStencilDestroy
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
