	.text
	.file	"fourierf.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	4618760256179416344     # double 6.2831853071795862
	.quad	-4604611780675359464    # double -6.2831853071795862
.LCPI0_2:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_1:
	.quad	-4611686018427387904    # double -2
	.text
	.globl	fft_float
	.p2align	4, 0x90
	.type	fft_float,@function
fft_float:                              # @fft_float
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %rbx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	movl	%esi, %r13d
	movl	%edi, %r15d
	callq	IsPowerOfTwo
	testl	%eax, %eax
	je	.LBB0_35
# BB#1:
	testl	%r13d, %r13d
	setne	%r12b
	testq	%rbp, %rbp
	je	.LBB0_36
# BB#2:                                 # %CheckPointer.exit
	testq	%rbx, %rbx
	je	.LBB0_37
# BB#3:                                 # %CheckPointer.exit139
	testq	%r14, %r14
	je	.LBB0_38
# BB#4:                                 # %CheckPointer.exit140
	movl	%r15d, %edi
	callq	NumberOfBitsNeeded
	testl	%r15d, %r15d
	je	.LBB0_20
# BB#5:                                 # %.lr.ph159
	movb	%r12b, (%rsp)           # 1-byte Spill
	movq	%rbp, %rcx
	movl	%r13d, 12(%rsp)         # 4-byte Spill
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movl	%r15d, %r12d
	movl	%eax, 32(%rsp)          # 4-byte Spill
	je	.LBB0_8
# BB#6:                                 # %.lr.ph159.split.preheader
	xorl	%r13d, %r13d
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph159.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edi
	movl	32(%rsp), %esi          # 4-byte Reload
	callq	ReverseBits
	movq	%r12, %rdx
	movl	(%rbp,%r13,4), %ecx
	movl	%eax, %eax
	movl	%ecx, (%rbx,%rax,4)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r13,4), %ecx
	movl	%ecx, (%r14,%rax,4)
	incq	%r13
	cmpq	%r13, %rdx
	jne	.LBB0_7
	jmp	.LBB0_10
.LBB0_8:                                # %.lr.ph159.split.us.preheader
	xorl	%ebp, %ebp
	movq	%rcx, %r13
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph159.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edi
	movl	32(%rsp), %esi          # 4-byte Reload
	callq	ReverseBits
	movq	%r12, %rdx
	movl	(%r13,%rbp,4), %ecx
	movl	%eax, %eax
	movl	%ecx, (%rbx,%rax,4)
	movl	$0, (%r14,%rax,4)
	incq	%rbp
	cmpq	%rbp, %rdx
	jne	.LBB0_9
.LBB0_10:                               # %.preheader141
	cmpl	$2, %r15d
	movl	12(%rsp), %r13d         # 4-byte Reload
	movl	$0, %eax
	movb	(%rsp), %cl             # 1-byte Reload
	jb	.LBB0_20
# BB#11:                                # %.preheader.lr.ph.preheader
	movb	%cl, %al
	movsd	.LCPI0_0(,%rax,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movl	$1, %ebp
	movl	$2, %eax
	.p2align	4, 0x90
.LBB0_12:                               # %.preheader.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
                                        #       Child Loop BB0_15 Depth 3
                                        #     Child Loop BB0_18 Depth 2
	movl	%eax, %r12d
	movl	%r12d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movsd	56(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm1, 32(%rsp)         # 16-byte Spill
	movapd	%xmm1, %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	callq	sin
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	xorpd	.LCPI0_2(%rip), %xmm0
	callq	sin
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	callq	cos
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	callq	cos
	testl	%ebp, %ebp
	je	.LBB0_17
# BB#13:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB0_12 Depth=1
	movapd	%xmm0, %xmm8
	addsd	%xmm8, %xmm8
	xorl	%eax, %eax
	movsd	(%rsp), %xmm10          # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movsd	64(%rsp), %xmm11        # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movsd	16(%rsp), %xmm12        # 8-byte Reload
                                        # xmm12 = mem[0],zero
	.p2align	4, 0x90
.LBB0_14:                               # %.preheader.us
                                        #   Parent Loop BB0_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_15 Depth 3
	leal	(%rbp,%rax), %ecx
	xorl	%edx, %edx
	movapd	%xmm11, %xmm3
	movapd	%xmm10, %xmm6
	movapd	%xmm0, %xmm4
	movapd	%xmm12, %xmm7
	.p2align	4, 0x90
.LBB0_15:                               #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movapd	%xmm4, %xmm9
	movapd	%xmm3, %xmm5
	leal	(%rax,%rdx), %esi
	movapd	%xmm8, %xmm4
	mulsd	%xmm9, %xmm4
	subsd	%xmm7, %xmm4
	movapd	%xmm8, %xmm3
	mulsd	%xmm5, %xmm3
	leal	(%rcx,%rdx), %edi
	subsd	%xmm6, %xmm3
	movss	(%rbx,%rdi,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	cvtss2sd	%xmm6, %xmm6
	movapd	%xmm4, %xmm7
	movss	(%r14,%rdi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulsd	%xmm6, %xmm7
	cvtss2sd	%xmm1, %xmm1
	movapd	%xmm3, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm1
	subsd	%xmm2, %xmm7
	mulsd	%xmm3, %xmm6
	movss	(%rbx,%rsi,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	addsd	%xmm1, %xmm6
	subsd	%xmm7, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rbx,%rdi,4)
	movss	(%r14,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	subsd	%xmm6, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%r14,%rdi,4)
	movss	(%rbx,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm7, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rbx,%rsi,4)
	movss	(%r14,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm6, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	incl	%edx
	cmpl	%edx, %ebp
	movapd	%xmm5, %xmm6
	movapd	%xmm9, %xmm7
	movss	%xmm1, (%r14,%rsi,4)
	jne	.LBB0_15
# BB#16:                                # %._crit_edge.us
                                        #   in Loop: Header=BB0_14 Depth=2
	addl	%r12d, %eax
	cmpl	%r15d, %eax
	jb	.LBB0_14
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_17:                               # %.preheader.preheader
                                        #   in Loop: Header=BB0_12 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_18:                               # %.preheader
                                        #   Parent Loop BB0_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	%r12d, %eax
	cmpl	%r15d, %eax
	jb	.LBB0_18
.LBB0_19:                               # %._crit_edge151
                                        #   in Loop: Header=BB0_12 Depth=1
	leal	(%r12,%r12), %eax
	cmpl	%r15d, %eax
	movl	%r12d, %ebp
	jbe	.LBB0_12
.LBB0_20:                               # %._crit_edge157
	testl	%r13d, %r13d
	je	.LBB0_34
# BB#21:
	testl	%r15d, %r15d
	je	.LBB0_34
# BB#22:                                # %.lr.ph.preheader
	movl	%r15d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	cmpl	$4, %r15d
	jae	.LBB0_24
# BB#23:
	xorl	%ecx, %ecx
	jmp	.LBB0_32
.LBB0_24:                               # %min.iters.checked
	andl	$3, %r15d
	movq	%rax, %rcx
	subq	%r15, %rcx
	je	.LBB0_28
# BB#25:                                # %vector.memcheck
	leaq	(%r14,%rax,4), %rdx
	cmpq	%rbx, %rdx
	jbe	.LBB0_29
# BB#26:                                # %vector.memcheck
	leaq	(%rbx,%rax,4), %rdx
	cmpq	%r14, %rdx
	jbe	.LBB0_29
.LBB0_28:
	xorl	%ecx, %ecx
	jmp	.LBB0_32
.LBB0_29:                               # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movq	%rcx, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB0_30:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rdi), %xmm2
	cvtps2pd	(%rdi), %xmm3
	divpd	%xmm1, %xmm3
	divpd	%xmm1, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movupd	%xmm3, (%rdi)
	cvtps2pd	8(%rsi), %xmm2
	cvtps2pd	(%rsi), %xmm3
	divpd	%xmm1, %xmm3
	divpd	%xmm1, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movupd	%xmm3, (%rsi)
	addq	$16, %rdi
	addq	$16, %rsi
	addq	$-4, %rdx
	jne	.LBB0_30
# BB#31:                                # %middle.block
	testl	%r15d, %r15d
	je	.LBB0_34
.LBB0_32:                               # %.lr.ph.preheader183
	leaq	(%rbx,%rcx,4), %rdx
	leaq	(%r14,%rcx,4), %rsi
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_33:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	divsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rdx)
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	divsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rsi)
	addq	$4, %rdx
	addq	$4, %rsi
	decq	%rax
	jne	.LBB0_33
.LBB0_34:                               # %.loopexit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_35:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_36:
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	movl	$.L.str.1, %edx
	jmp	.LBB0_39
.LBB0_37:
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	movl	$.L.str.2, %edx
	jmp	.LBB0_39
.LBB0_38:
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	movl	$.L.str.3, %edx
.LBB0_39:
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	fft_float, .Lfunc_end0-fft_float
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	4618760256179416344     # double 6.2831853071795862
	.quad	-4604611780675359464    # double -6.2831853071795862
.LCPI1_2:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_1:
	.quad	-4611686018427387904    # double -2
	.text
	.globl	fft_float_StrictFP
	.p2align	4, 0x90
	.type	fft_float_StrictFP,@function
fft_float_StrictFP:                     # @fft_float_StrictFP
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 128
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %rbx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	movl	%esi, %r13d
	movl	%edi, %r15d
	callq	IsPowerOfTwo
	testl	%eax, %eax
	je	.LBB1_35
# BB#1:
	testl	%r13d, %r13d
	setne	%r12b
	testq	%rbp, %rbp
	je	.LBB1_36
# BB#2:                                 # %CheckPointer.exit
	testq	%rbx, %rbx
	je	.LBB1_37
# BB#3:                                 # %CheckPointer.exit139
	testq	%r14, %r14
	je	.LBB1_38
# BB#4:                                 # %CheckPointer.exit140
	movl	%r15d, %edi
	callq	NumberOfBitsNeeded
	testl	%r15d, %r15d
	je	.LBB1_20
# BB#5:                                 # %.lr.ph159
	movb	%r12b, (%rsp)           # 1-byte Spill
	movq	%rbp, %rcx
	movl	%r13d, 12(%rsp)         # 4-byte Spill
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movl	%r15d, %r12d
	movl	%eax, 32(%rsp)          # 4-byte Spill
	je	.LBB1_8
# BB#6:                                 # %.lr.ph159.split.preheader
	xorl	%r13d, %r13d
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph159.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edi
	movl	32(%rsp), %esi          # 4-byte Reload
	callq	ReverseBits
	movq	%r12, %rdx
	movl	(%rbp,%r13,4), %ecx
	movl	%eax, %eax
	movl	%ecx, (%rbx,%rax,4)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r13,4), %ecx
	movl	%ecx, (%r14,%rax,4)
	incq	%r13
	cmpq	%r13, %rdx
	jne	.LBB1_7
	jmp	.LBB1_10
.LBB1_8:                                # %.lr.ph159.split.us.preheader
	xorl	%ebp, %ebp
	movq	%rcx, %r13
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph159.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edi
	movl	32(%rsp), %esi          # 4-byte Reload
	callq	ReverseBits
	movq	%r12, %rdx
	movl	(%r13,%rbp,4), %ecx
	movl	%eax, %eax
	movl	%ecx, (%rbx,%rax,4)
	movl	$0, (%r14,%rax,4)
	incq	%rbp
	cmpq	%rbp, %rdx
	jne	.LBB1_9
.LBB1_10:                               # %.preheader141
	cmpl	$2, %r15d
	movl	12(%rsp), %r13d         # 4-byte Reload
	movl	$0, %eax
	movb	(%rsp), %cl             # 1-byte Reload
	jb	.LBB1_20
# BB#11:                                # %.preheader.lr.ph.preheader
	movb	%cl, %al
	movsd	.LCPI1_0(,%rax,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movl	$1, %ebp
	movl	$2, %eax
	.p2align	4, 0x90
.LBB1_12:                               # %.preheader.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
                                        #       Child Loop BB1_15 Depth 3
                                        #     Child Loop BB1_18 Depth 2
	movl	%eax, %r12d
	movl	%r12d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movsd	56(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm1, 32(%rsp)         # 16-byte Spill
	movapd	%xmm1, %xmm0
	mulsd	.LCPI1_1(%rip), %xmm0
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	callq	sin
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	xorpd	.LCPI1_2(%rip), %xmm0
	callq	sin
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	callq	cos
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	callq	cos
	testl	%ebp, %ebp
	je	.LBB1_17
# BB#13:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB1_12 Depth=1
	movapd	%xmm0, %xmm8
	addsd	%xmm8, %xmm8
	xorl	%eax, %eax
	movsd	(%rsp), %xmm10          # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movsd	64(%rsp), %xmm11        # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movsd	16(%rsp), %xmm12        # 8-byte Reload
                                        # xmm12 = mem[0],zero
	.p2align	4, 0x90
.LBB1_14:                               # %.preheader.us
                                        #   Parent Loop BB1_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_15 Depth 3
	leal	(%rbp,%rax), %ecx
	xorl	%edx, %edx
	movapd	%xmm11, %xmm3
	movapd	%xmm10, %xmm6
	movapd	%xmm0, %xmm4
	movapd	%xmm12, %xmm7
	.p2align	4, 0x90
.LBB1_15:                               #   Parent Loop BB1_12 Depth=1
                                        #     Parent Loop BB1_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movapd	%xmm4, %xmm9
	movapd	%xmm3, %xmm5
	leal	(%rax,%rdx), %esi
	movapd	%xmm8, %xmm4
	mulsd	%xmm9, %xmm4
	subsd	%xmm7, %xmm4
	movapd	%xmm8, %xmm3
	mulsd	%xmm5, %xmm3
	leal	(%rcx,%rdx), %edi
	subsd	%xmm6, %xmm3
	movss	(%rbx,%rdi,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	cvtss2sd	%xmm6, %xmm6
	movapd	%xmm4, %xmm7
	movss	(%r14,%rdi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulsd	%xmm6, %xmm7
	cvtss2sd	%xmm1, %xmm1
	movapd	%xmm3, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm1
	subsd	%xmm2, %xmm7
	mulsd	%xmm3, %xmm6
	movss	(%rbx,%rsi,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	addsd	%xmm1, %xmm6
	subsd	%xmm7, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rbx,%rdi,4)
	movss	(%r14,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	subsd	%xmm6, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%r14,%rdi,4)
	movss	(%rbx,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm7, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rbx,%rsi,4)
	movss	(%r14,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm6, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	incl	%edx
	cmpl	%edx, %ebp
	movapd	%xmm5, %xmm6
	movapd	%xmm9, %xmm7
	movss	%xmm1, (%r14,%rsi,4)
	jne	.LBB1_15
# BB#16:                                # %._crit_edge.us
                                        #   in Loop: Header=BB1_14 Depth=2
	addl	%r12d, %eax
	cmpl	%r15d, %eax
	jb	.LBB1_14
	jmp	.LBB1_19
	.p2align	4, 0x90
.LBB1_17:                               # %.preheader.preheader
                                        #   in Loop: Header=BB1_12 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_18:                               # %.preheader
                                        #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	%r12d, %eax
	cmpl	%r15d, %eax
	jb	.LBB1_18
.LBB1_19:                               # %._crit_edge151
                                        #   in Loop: Header=BB1_12 Depth=1
	leal	(%r12,%r12), %eax
	cmpl	%r15d, %eax
	movl	%r12d, %ebp
	jbe	.LBB1_12
.LBB1_20:                               # %._crit_edge157
	testl	%r13d, %r13d
	je	.LBB1_34
# BB#21:
	testl	%r15d, %r15d
	je	.LBB1_34
# BB#22:                                # %.lr.ph.preheader
	movl	%r15d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	cmpl	$4, %r15d
	jae	.LBB1_24
# BB#23:
	xorl	%ecx, %ecx
	jmp	.LBB1_32
.LBB1_24:                               # %min.iters.checked
	andl	$3, %r15d
	movq	%rax, %rcx
	subq	%r15, %rcx
	je	.LBB1_28
# BB#25:                                # %vector.memcheck
	leaq	(%r14,%rax,4), %rdx
	cmpq	%rbx, %rdx
	jbe	.LBB1_29
# BB#26:                                # %vector.memcheck
	leaq	(%rbx,%rax,4), %rdx
	cmpq	%r14, %rdx
	jbe	.LBB1_29
.LBB1_28:
	xorl	%ecx, %ecx
	jmp	.LBB1_32
.LBB1_29:                               # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movq	%rcx, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB1_30:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rdi), %xmm2
	cvtps2pd	(%rdi), %xmm3
	divpd	%xmm1, %xmm3
	divpd	%xmm1, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movupd	%xmm3, (%rdi)
	cvtps2pd	8(%rsi), %xmm2
	cvtps2pd	(%rsi), %xmm3
	divpd	%xmm1, %xmm3
	divpd	%xmm1, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movupd	%xmm3, (%rsi)
	addq	$16, %rdi
	addq	$16, %rsi
	addq	$-4, %rdx
	jne	.LBB1_30
# BB#31:                                # %middle.block
	testl	%r15d, %r15d
	je	.LBB1_34
.LBB1_32:                               # %.lr.ph.preheader183
	leaq	(%rbx,%rcx,4), %rdx
	leaq	(%r14,%rcx,4), %rsi
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB1_33:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	divsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rdx)
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	divsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rsi)
	addq	$4, %rdx
	addq	$4, %rsi
	decq	%rax
	jne	.LBB1_33
.LBB1_34:                               # %.loopexit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_35:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB1_36:
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	movl	$.L.str.1, %edx
	jmp	.LBB1_39
.LBB1_37:
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	movl	$.L.str.2, %edx
	jmp	.LBB1_39
.LBB1_38:
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	movl	$.L.str.3, %edx
.LBB1_39:
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	fft_float_StrictFP, .Lfunc_end1-fft_float_StrictFP
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Error in fft():  NumSamples=%u is not power of two\n"
	.size	.L.str, 52

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"RealIn"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"RealOut"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"ImagOut"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Error in fft_float():  %s == NULL\n"
	.size	.L.str.4, 35


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
