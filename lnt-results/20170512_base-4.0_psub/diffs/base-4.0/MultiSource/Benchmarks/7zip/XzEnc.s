	.text
	.file	"XzEnc.bc"
	.globl	Xz_WriteHeader
	.p2align	4, 0x90
	.type	Xz_WriteHeader,@function
Xz_WriteHeader:                         # @Xz_WriteHeader
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	%edi, %eax
	movzwl	XZ_SIG+4(%rip), %ecx
	movw	%cx, 8(%rsp)
	movl	XZ_SIG(%rip), %ecx
	movl	%ecx, 4(%rsp)
	leaq	10(%rsp), %rdi
	movb	%ah, 10(%rsp)  # NOREX
	movb	%al, 11(%rsp)
	movl	$2, %esi
	callq	CrcCalc
	movl	%eax, 12(%rsp)
	leaq	4(%rsp), %rsi
	movl	$12, %edx
	movq	%rbx, %rdi
	callq	*(%rbx)
	xorl	%ecx, %ecx
	cmpq	$12, %rax
	setne	%cl
	leal	(%rcx,%rcx,8), %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	Xz_WriteHeader, .Lfunc_end0-Xz_WriteHeader
	.cfi_endproc

	.globl	XzBlock_WriteHeader
	.p2align	4, 0x90
	.type	XzBlock_WriteHeader,@function
XzBlock_WriteHeader:                    # @XzBlock_WriteHeader
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 56
	subq	$1048, %rsp             # imm = 0x418
.Lcfi9:
	.cfi_def_cfa_offset 1104
.Lcfi10:
	.cfi_offset %rbx, -56
.Lcfi11:
	.cfi_offset %r12, -48
.Lcfi12:
	.cfi_offset %r13, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movb	16(%rbp), %cl
	movb	%cl, 17(%rsp)
	movl	$2, %ebx
	testb	$64, %cl
	je	.LBB1_2
# BB#1:
	leaq	18(%rsp), %rdi
	movq	(%rbp), %rsi
	callq	Xz_WriteVarInt
	movl	%eax, %ebx
	addl	$2, %ebx
	movb	16(%rbp), %cl
.LBB1_2:
	testb	%cl, %cl
	movq	%r14, (%rsp)            # 8-byte Spill
	jns	.LBB1_4
# BB#3:
	movl	%ebx, %eax
	leaq	16(%rsp,%rax), %rdi
	movq	8(%rbp), %rsi
	callq	Xz_WriteVarInt
	addl	%ebx, %eax
	movb	16(%rbp), %cl
	movl	%eax, %ebx
.LBB1_4:
	andb	$3, %cl
	incb	%cl
	movzbl	%cl, %r15d
	addq	$36, %rbp
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	movl	%ebx, %r13d
	movl	%r13d, %eax
	leaq	16(%rsp,%rax), %rdi
	movq	-12(%rbp), %rsi
	callq	Xz_WriteVarInt
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leal	(%rax,%r13), %ebx
	leaq	16(%rsp,%rbx), %rdi
	movl	-4(%rbp), %esi
	callq	Xz_WriteVarInt
	movl	%eax, %r12d
	addl	%r12d, %ebx
	leaq	16(%rsp,%rbx), %rdi
	movl	-4(%rbp), %r14d
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	memcpy
	movq	%r14, %rdx
	addl	%edx, %ebx
	addq	$32, %rbp
	decq	%r15
	jne	.LBB1_5
# BB#6:                                 # %.preheader
	movq	8(%rsp), %r14           # 8-byte Reload
	testb	$3, %bl
	je	.LBB1_7
# BB#8:                                 # %.lr.ph.preheader
	leal	1(%rbx), %ecx
	movl	%ecx, %esi
	andl	$3, %esi
	movl	$1, %r9d
	subl	%esi, %r9d
	cmpl	$31, %r9d
	jbe	.LBB1_9
# BB#10:                                # %min.iters.checked
	movl	%r9d, %r8d
	andl	$-32, %r8d
	je	.LBB1_9
# BB#11:                                # %vector.scevcheck
	andb	$3, %cl
	movl	%esi, %edi
	negl	%edi
	movl	%edi, %eax
	andb	$3, %al
	addb	%cl, %al
	movl	%eax, %ecx
	andb	$3, %cl
	cmpb	%al, %cl
	jne	.LBB1_9
# BB#12:                                # %vector.scevcheck
	cmpl	$3, %edi
	ja	.LBB1_9
# BB#13:                                # %vector.scevcheck
	movl	%ebx, %eax
	subl	%esi, %eax
	cmpl	%ebx, %eax
	jb	.LBB1_9
# BB#14:                                # %vector.body.preheader
	leal	-32(%r8), %r10d
	movl	%r10d, %edi
	shrl	$5, %edi
	incl	%edi
	andl	$3, %edi
	je	.LBB1_15
# BB#16:                                # %vector.body.prol.preheader
	leal	(%r12,%r14), %ebp
	addl	%r13d, %ebp
	addl	%edx, %ebp
	negl	%edi
	xorl	%esi, %esi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_17:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%rsi), %eax
	movups	%xmm0, 16(%rsp,%rax)
	movups	%xmm0, 32(%rsp,%rax)
	addl	$32, %esi
	incl	%edi
	jne	.LBB1_17
	jmp	.LBB1_18
.LBB1_9:
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	incl	%ebx
	movb	$0, 16(%rsp,%rax)
	testb	$3, %bl
	jne	.LBB1_22
	jmp	.LBB1_23
.LBB1_7:
	movq	(%rsp), %r14            # 8-byte Reload
.LBB1_23:                               # %._crit_edge
	movl	%ebx, %eax
	shrl	$2, %eax
	movb	%al, 16(%rsp)
	movl	%ebx, %ebp
	leaq	16(%rsp), %r15
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	CrcCalc
	movl	%eax, 16(%rsp,%rbp)
	addl	$4, %ebx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	*(%r14)
	xorl	%ecx, %ecx
	cmpq	%rbx, %rax
	setne	%cl
	leal	(%rcx,%rcx,8), %eax
	addq	$1048, %rsp             # imm = 0x418
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_15:
	xorl	%esi, %esi
.LBB1_18:                               # %vector.body.prol.loopexit
	cmpl	$96, %r10d
	jb	.LBB1_21
# BB#19:                                # %vector.body.preheader.new
	movl	%esi, %edi
	subl	%r8d, %edi
	addl	%r14d, %r12d
	addl	%r13d, %r12d
	addl	%edx, %r12d
	addl	%esi, %r12d
	xorl	%esi, %esi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_20:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r12,%rsi), %eax
	movups	%xmm0, 16(%rsp,%rax)
	movups	%xmm0, 32(%rsp,%rax)
	leal	32(%r12,%rsi), %eax
	movups	%xmm0, 16(%rsp,%rax)
	movups	%xmm0, 32(%rsp,%rax)
	leal	64(%r12,%rsi), %eax
	movups	%xmm0, 16(%rsp,%rax)
	movups	%xmm0, 32(%rsp,%rax)
	leal	96(%r12,%rsi), %eax
	movups	%xmm0, 16(%rsp,%rax)
	movups	%xmm0, 32(%rsp,%rax)
	subl	$-128, %esi
	movl	%edi, %eax
	addl	%esi, %eax
	jne	.LBB1_20
.LBB1_21:                               # %middle.block
	addl	%r8d, %ebx
	cmpl	%r8d, %r9d
	movq	(%rsp), %r14            # 8-byte Reload
	jne	.LBB1_22
	jmp	.LBB1_23
.Lfunc_end1:
	.size	XzBlock_WriteHeader, .Lfunc_end1-XzBlock_WriteHeader
	.cfi_endproc

	.globl	Xz_WriteFooter
	.p2align	4, 0x90
	.type	Xz_WriteFooter,@function
Xz_WriteFooter:                         # @Xz_WriteFooter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 128
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	17(%rsp), %rdi
	movq	8(%rbx), %rsi
	callq	Xz_WriteVarInt
	movl	%eax, %ebp
	incl	%ebp
	movb	$0, 16(%rsp)
	leaq	16(%rsp), %r12
	movl	$-1, %edi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	CrcUpdate
	movl	%eax, %r15d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	*(%r14)
	movl	$9, %r12d
	cmpq	%rbp, %rax
	jne	.LBB2_12
# BB#1:                                 # %.preheader
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	cmpq	$0, 8(%rbx)
	je	.LBB2_6
# BB#2:                                 # %.lr.ph
	xorl	%r15d, %r15d
	movl	$8, %r13d
	movq	%r14, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	24(%rbx), %rbp
	movq	(%rbp,%r13), %rsi
	leaq	16(%rsp), %rax
	movq	%rax, %rdi
	callq	Xz_WriteVarInt
	movl	%eax, %r14d
	movl	%r14d, %eax
	leaq	16(%rsp,%rax), %rdi
	movq	-8(%rbp,%r13), %rsi
	callq	Xz_WriteVarInt
	movl	%eax, %r12d
	addl	%r14d, %r12d
	movq	56(%rsp), %r14          # 8-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	leaq	16(%rsp), %rbp
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	CrcUpdate
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	*(%r14)
	cmpq	%r12, %rax
	jne	.LBB2_5
# BB#3:                                 #   in Loop: Header=BB2_4 Depth=1
	movq	64(%rsp), %rbp          # 8-byte Reload
	addq	%r12, %rbp
	incq	%r15
	addq	$16, %r13
	cmpq	8(%rbx), %r15
	jb	.LBB2_4
.LBB2_6:                                # %._crit_edge
	movl	%ebp, %eax
	andl	$3, %eax
	je	.LBB2_7
# BB#8:
	movq	%rbp, %r15
	movb	$0, 18(%rsp)
	movw	$0, 16(%rsp)
	movl	$4, %r13d
	subl	%eax, %r13d
	movq	%r14, %rbp
	leaq	16(%rsp), %r14
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	CrcUpdate
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%rbp, %r14
	movq	%r13, %rdx
	callq	*(%r14)
	cmpq	%r13, %rax
	movl	$9, %r12d
	jne	.LBB2_12
# BB#9:
	movq	%r15, %rbp
	addq	%r13, %rbp
	jmp	.LBB2_10
.LBB2_5:
	movl	$9, %r12d
	jmp	.LBB2_12
.LBB2_7:
	movl	$9, %r12d
.LBB2_10:
	movl	12(%rsp), %eax          # 4-byte Reload
	notl	%eax
	movl	%eax, 16(%rsp)
	leaq	16(%rsp), %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	callq	*(%r14)
	cmpq	$4, %rax
	jne	.LBB2_12
# BB#11:                                # %.thread
	addq	$4, %rbp
	shrq	$2, %rbp
	decl	%ebp
	leaq	20(%rsp), %rdi
	movl	%ebp, 20(%rsp)
	movzwl	(%rbx), %eax
	movb	%ah, 24(%rsp)  # NOREX
	movb	%al, 25(%rsp)
	movl	$6, %esi
	callq	CrcCalc
	movl	%eax, 16(%rsp)
	movzwl	XZ_FOOTER_SIG(%rip), %eax
	movw	%ax, 26(%rsp)
	leaq	16(%rsp), %rsi
	movl	$12, %edx
	movq	%r14, %rdi
	callq	*(%r14)
	xorl	%ecx, %ecx
	cmpq	$12, %rax
	setne	%cl
	leal	(%rcx,%rcx,8), %r12d
.LBB2_12:                               # %.loopexit
	movl	%r12d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	Xz_WriteFooter, .Lfunc_end2-Xz_WriteFooter
	.cfi_endproc

	.globl	Xz_AddIndexRecord
	.p2align	4, 0x90
	.type	Xz_AddIndexRecord,@function
Xz_AddIndexRecord:                      # @Xz_AddIndexRecord
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 80
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdi, %rbx
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.LBB3_1
# BB#2:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rcx
	leaq	8(%rbx), %r14
	cmpq	%rax, %rcx
	je	.LBB3_3
	jmp	.LBB3_9
.LBB3_1:                                # %._crit_edge
	leaq	8(%rbx), %r14
	movq	8(%rbx), %rcx
.LBB3_3:
	leaq	2(%rcx,%rcx), %r15
	movabsq	$1152921504606846974, %rax # imm = 0xFFFFFFFFFFFFFFE
	andq	%r15, %rax
	movl	$2, %ebp
	cmpq	%r15, %rax
	jne	.LBB3_10
# BB#4:
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%r15, %rsi
	shlq	$4, %rsi
	movq	%r12, %rdi
	callq	*(%r12)
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB3_10
# BB#5:
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB3_6
# BB#7:
	movq	24(%rbx), %rsi
	shlq	$4, %rdx
	movq	%r13, %rdi
	callq	memcpy
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	Xz_Free
	movq	8(%rbx), %rax
	jmp	.LBB3_8
.LBB3_6:
	xorl	%eax, %eax
.LBB3_8:                                # %.critedge
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r13, 24(%rbx)
	movq	%r15, 16(%rbx)
.LBB3_9:
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	shlq	$4, %rax
	movq	%rdx, 8(%r13,%rax)
	movq	%rsi, (%r13,%rax)
	xorl	%ebp, %ebp
.LBB3_10:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	Xz_AddIndexRecord, .Lfunc_end3-Xz_AddIndexRecord
	.cfi_endproc

	.globl	SeqCheckInStream_Init
	.p2align	4, 0x90
	.type	SeqCheckInStream_Init,@function
SeqCheckInStream_Init:                  # @SeqCheckInStream_Init
	.cfi_startproc
# BB#0:
	movq	$0, 16(%rdi)
	addq	$24, %rdi
	jmp	XzCheck_Init            # TAILCALL
.Lfunc_end4:
	.size	SeqCheckInStream_Init, .Lfunc_end4-SeqCheckInStream_Init
	.cfi_endproc

	.globl	SeqCheckInStream_GetDigest
	.p2align	4, 0x90
	.type	SeqCheckInStream_GetDigest,@function
SeqCheckInStream_GetDigest:             # @SeqCheckInStream_GetDigest
	.cfi_startproc
# BB#0:
	addq	$24, %rdi
	jmp	XzCheck_Final           # TAILCALL
.Lfunc_end5:
	.size	SeqCheckInStream_GetDigest, .Lfunc_end5-SeqCheckInStream_GetDigest
	.cfi_endproc

	.globl	Xz_Encode
	.p2align	4, 0x90
	.type	Xz_Encode,@function
Xz_Encode:                              # @Xz_Encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$520, %rsp              # imm = 0x208
.Lcfi48:
	.cfi_def_cfa_offset 576
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movl	%ecx, %r14d
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %rbx
	leaq	16(%rsp), %rdi
	callq	Xz_Construct
	movl	$g_Alloc, %edi
	movl	$g_BigAlloc, %esi
	callq	Lzma2Enc_Create
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB6_1
# BB#2:
	movq	%rbx, (%rsp)            # 8-byte Spill
	movw	$1, 16(%rsp)
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	Lzma2Enc_SetProps
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB6_40
# BB#3:
	movzwl	16(%rsp), %eax
	movzwl	XZ_SIG+4(%rip), %ecx
	movw	%cx, 228(%rsp)
	movl	XZ_SIG(%rip), %ecx
	movl	%ecx, 224(%rsp)
	leaq	230(%rsp), %rdi
	movb	%ah, 230(%rsp)  # NOREX
	movb	%al, 231(%rsp)
	movl	$2, %esi
	callq	CrcCalc
	movl	%eax, 232(%rsp)
	leaq	224(%rsp), %rsi
	movl	$12, %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	*(%rdi)
	cmpq	$12, %rax
	setne	%al
	movl	$9, %ebp
	jne	.LBB6_40
# BB#4:
	testl	%r14d, %r14d
	setne	240(%rsp)
	movl	%eax, %r14d
	je	.LBB6_5
# BB#6:
	movq	$1, 248(%rsp)
	movl	$0, 256(%rsp)
	movl	$1, %ebx
	jmp	.LBB6_7
.LBB6_1:
	movl	$2, %ebp
	jmp	.LBB6_41
.LBB6_5:
	xorl	%ebx, %ebx
.LBB6_7:
	shlq	$5, %rbx
	movq	$33, 248(%rsp,%rbx)
	movl	$1, 256(%rsp,%rbx)
	movq	%r13, %rdi
	callq	Lzma2Enc_WriteProperties
	movb	%al, 260(%rsp,%rbx)
	movq	$MyWrite, 56(%rsp)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 64(%rsp)
	movq	$0, 72(%rsp)
	leaq	224(%rsp), %rdi
	leaq	56(%rsp), %rsi
	callq	XzBlock_WriteHeader
	movl	%eax, %ebp
	movl	$1, %ebx
	testl	%ebp, %ebp
	jne	.LBB6_37
# BB#8:
	xorl	%eax, %eax
	movb	%r14b, %al
	leal	(%rax,%rax,8), %r14d
	movq	$SeqCheckInStream_Read, 376(%rsp)
	movq	%r15, 384(%rsp)
	movzwl	16(%rsp), %esi
	andl	$15, %esi
	movq	$0, 392(%rsp)
	leaq	400(%rsp), %rdi
	callq	XzCheck_Init
	movq	72(%rsp), %r15
	leaq	56(%rsp), %rsi
	leaq	376(%rsp), %rdx
	movq	%r13, %rdi
	movq	%r12, %rcx
	callq	Lzma2Enc_Encode
	movl	%eax, %ebp
	testl	%ebp, %ebp
	cmovnel	%ebp, %r14d
	jne	.LBB6_37
# BB#9:                                 # %.thread.i
	leaq	400(%rsp), %rsi
	movq	392(%rsp), %rcx
	movq	%rcx, 232(%rsp)
	movq	72(%rsp), %rax
	subq	%r15, %rax
	movq	%rax, 224(%rsp)
	testb	$3, %al
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	je	.LBB6_10
# BB#11:                                # %.lr.ph.i
	leal	1(%rax), %edx
	andl	$3, %edx
	movl	$1, %r8d
	subl	%edx, %r8d
	xorl	%edx, %edx
	cmpl	$32, %r8d
	movq	%rsi, %rdi
	jb	.LBB6_23
# BB#12:                                # %min.iters.checked
	movl	%r8d, %ebp
	andl	$-32, %ebp
	je	.LBB6_23
# BB#13:                                # %vector.scevcheck
	movl	%eax, %ecx
	incb	%cl
	movzbl	%cl, %esi
	andb	$3, %cl
	andl	$3, %esi
	negl	%esi
	movl	%esi, %ebx
	andb	$3, %bl
	addb	%cl, %bl
	movl	%ebx, %ecx
	andb	$3, %cl
	cmpb	%bl, %cl
	jne	.LBB6_23
# BB#14:                                # %vector.scevcheck
	cmpl	$3, %esi
	ja	.LBB6_23
# BB#15:                                # %vector.body.preheader
	leal	-32(%rbp), %esi
	movl	%esi, %ecx
	shrl	$5, %ecx
	incl	%ecx
	andl	$7, %ecx
	je	.LBB6_16
# BB#17:                                # %vector.body.prol.preheader
	negl	%ecx
	xorl	%edx, %edx
	xorps	%xmm0, %xmm0
.LBB6_18:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, 96(%rsp,%rdx)
	movaps	%xmm0, 112(%rsp,%rdx)
	addq	$32, %rdx
	incl	%ecx
	jne	.LBB6_18
	jmp	.LBB6_19
.LBB6_10:
	xorl	%ebp, %ebp
	movq	%rsi, %rdi
	jmp	.LBB6_25
.LBB6_16:
	xorl	%edx, %edx
.LBB6_19:                               # %vector.body.prol.loopexit
	cmpl	$224, %esi
	jb	.LBB6_22
# BB#20:                                # %vector.body.preheader.new
	movl	%r8d, %esi
	andl	$-32, %esi
	negl	%esi
	addl	$224, %edx
	xorps	%xmm0, %xmm0
.LBB6_21:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leal	-224(%rdx), %ecx
	movaps	%xmm0, 96(%rsp,%rcx)
	movaps	%xmm0, 112(%rsp,%rcx)
	leal	-192(%rdx), %ecx
	movaps	%xmm0, 96(%rsp,%rcx)
	movaps	%xmm0, 112(%rsp,%rcx)
	leal	-160(%rdx), %ecx
	movaps	%xmm0, 96(%rsp,%rcx)
	movaps	%xmm0, 112(%rsp,%rcx)
	leal	-128(%rdx), %ecx
	movaps	%xmm0, 96(%rsp,%rcx)
	movaps	%xmm0, 112(%rsp,%rcx)
	leal	-96(%rdx), %ecx
	movaps	%xmm0, 96(%rsp,%rcx)
	movaps	%xmm0, 112(%rsp,%rcx)
	leal	-64(%rdx), %ecx
	movaps	%xmm0, 96(%rsp,%rcx)
	movaps	%xmm0, 112(%rsp,%rcx)
	leal	-32(%rdx), %ecx
	movaps	%xmm0, 96(%rsp,%rcx)
	movaps	%xmm0, 112(%rsp,%rcx)
	movl	%edx, %ecx
	movaps	%xmm0, 96(%rsp,%rcx)
	movaps	%xmm0, 112(%rsp,%rcx)
	leal	256(%rsi,%rdx), %ecx
	leal	256(%rdx), %edx
	cmpl	$224, %ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	jne	.LBB6_21
.LBB6_22:                               # %middle.block
	cmpl	%ebp, %r8d
	movl	%ebp, %edx
	je	.LBB6_25
.LBB6_23:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rdx), %ebp
	movl	%edx, %ecx
	movb	$0, 96(%rsp,%rcx)
	leal	1(%rax,%rdx), %ecx
	testb	$3, %cl
	movl	%ebp, %edx
	jne	.LBB6_23
.LBB6_25:                               # %._crit_edge.i
	movl	%ebp, %r12d
	leaq	96(%rsp,%r12), %rsi
	callq	XzCheck_Final
	movzwl	16(%rsp), %edi
	callq	XzFlags_GetCheckSize
	movl	%eax, %ebx
	addl	%ebp, %ebx
	leaq	56(%rsp), %rdi
	leaq	96(%rsp), %rsi
	movq	%rbx, %rdx
	callq	*56(%rsp)
	cmpq	%rbx, %rax
	movl	$9, %ebp
	cmovnel	%ebp, %r14d
	movl	$1, %ebx
	jne	.LBB6_37
# BB#26:
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movq	72(%rsp), %r14
	movq	40(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_27
# BB#28:
	movq	24(%rsp), %rax
	movq	32(%rsp), %rcx
	leaq	24(%rsp), %rdx
	cmpq	%rax, %rcx
	je	.LBB6_29
	jmp	.LBB6_35
.LBB6_27:                               # %._crit_edge.i.i
	leaq	24(%rsp), %rdx
	movq	24(%rsp), %rcx
.LBB6_29:
	leaq	2(%rcx,%rcx), %r15
	movabsq	$1152921504606846974, %rax # imm = 0xFFFFFFFFFFFFFFE
	andq	%r15, %rax
	movl	$2, %ebp
	cmpq	%r15, %rax
	jne	.LBB6_36
# BB#30:
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%r15, %rsi
	shlq	$4, %rsi
	movl	$g_Alloc, %edi
	callq	*g_Alloc(%rip)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB6_36
# BB#31:
	movq	24(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB6_32
# BB#33:
	movq	40(%rsp), %rsi
	shlq	$4, %rdx
	movq	%rbx, %rdi
	callq	memcpy
	leaq	16(%rsp), %rdi
	movl	$g_Alloc, %esi
	callq	Xz_Free
	movq	24(%rsp), %rax
	jmp	.LBB6_34
.LBB6_32:
	xorl	%eax, %eax
.LBB6_34:                               # %.critedge.i.i
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, 40(%rsp)
	movq	%r15, 32(%rsp)
.LBB6_35:
	subq	%r12, %r14
	leaq	1(%rax), %rcx
	movq	%rcx, (%rdx)
	shlq	$4, %rax
	movq	%r14, 8(%rbx,%rax)
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rbx,%rax)
	xorl	%ebp, %ebp
.LBB6_36:                               # %Xz_AddIndexRecord.exit.i
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	setne	%bl
	movl	12(%rsp), %eax          # 4-byte Reload
	cmovnel	%ebp, %eax
	movl	%eax, %ebp
.LBB6_37:
	testl	%ebx, %ebx
	jne	.LBB6_39
# BB#38:
	leaq	16(%rsp), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	Xz_WriteFooter
	movl	%eax, %ebp
.LBB6_39:                               # %Xz_Compress.exit
	testq	%r13, %r13
	je	.LBB6_41
.LBB6_40:                               # %Xz_Compress.exit.thread
	movq	%r13, %rdi
	callq	Lzma2Enc_Destroy
.LBB6_41:                               # %Lzma2WithFilters_Free.exit
	leaq	16(%rsp), %rdi
	movl	$g_Alloc, %esi
	callq	Xz_Free
	movl	%ebp, %eax
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	Xz_Encode, .Lfunc_end6-Xz_Encode
	.cfi_endproc

	.globl	Xz_EncodeEmpty
	.p2align	4, 0x90
	.type	Xz_EncodeEmpty,@function
Xz_EncodeEmpty:                         # @Xz_EncodeEmpty
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 80
.Lcfi58:
	.cfi_offset %rbx, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	16(%rsp), %rdi
	callq	Xz_Construct
	movzwl	16(%rsp), %eax
	movzwl	XZ_SIG+4(%rip), %ecx
	movw	%cx, 8(%rsp)
	movl	XZ_SIG(%rip), %ecx
	movl	%ecx, 4(%rsp)
	leaq	10(%rsp), %rdi
	movb	%ah, 10(%rsp)  # NOREX
	movb	%al, 11(%rsp)
	movl	$2, %esi
	callq	CrcCalc
	movl	%eax, 12(%rsp)
	leaq	4(%rsp), %rsi
	movl	$12, %edx
	movq	%rbx, %rdi
	callq	*(%rbx)
	movl	$9, %ebp
	cmpq	$12, %rax
	jne	.LBB7_2
# BB#1:
	leaq	16(%rsp), %rdi
	movq	%rbx, %rsi
	callq	Xz_WriteFooter
	movl	%eax, %ebp
.LBB7_2:
	leaq	16(%rsp), %rdi
	movl	$g_Alloc, %esi
	callq	Xz_Free
	movl	%ebp, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end7:
	.size	Xz_EncodeEmpty, .Lfunc_end7-Xz_EncodeEmpty
	.cfi_endproc

	.p2align	4, 0x90
	.type	SzAlloc,@function
SzAlloc:                                # @SzAlloc
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyAlloc                 # TAILCALL
.Lfunc_end8:
	.size	SzAlloc, .Lfunc_end8-SzAlloc
	.cfi_endproc

	.p2align	4, 0x90
	.type	SzFree,@function
SzFree:                                 # @SzFree
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyFree                  # TAILCALL
.Lfunc_end9:
	.size	SzFree, .Lfunc_end9-SzFree
	.cfi_endproc

	.p2align	4, 0x90
	.type	SzBigAlloc,@function
SzBigAlloc:                             # @SzBigAlloc
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigAlloc                # TAILCALL
.Lfunc_end10:
	.size	SzBigAlloc, .Lfunc_end10-SzBigAlloc
	.cfi_endproc

	.p2align	4, 0x90
	.type	SzBigFree,@function
SzBigFree:                              # @SzBigFree
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigFree                 # TAILCALL
.Lfunc_end11:
	.size	SzBigFree, .Lfunc_end11-SzBigFree
	.cfi_endproc

	.p2align	4, 0x90
	.type	MyWrite,@function
MyWrite:                                # @MyWrite
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 16
.Lcfi61:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	callq	*(%rdi)
	addq	%rax, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end12:
	.size	MyWrite, .Lfunc_end12-MyWrite
	.cfi_endproc

	.p2align	4, 0x90
	.type	SeqCheckInStream_Read,@function
SeqCheckInStream_Read:                  # @SeqCheckInStream_Read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 48
.Lcfi67:
	.cfi_offset %rbx, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	callq	*(%rdi)
	movl	%eax, %ebp
	leaq	24(%rbx), %rdi
	movq	(%r15), %rdx
	movq	%r14, %rsi
	callq	XzCheck_Update
	movq	(%r15), %rax
	addq	%rax, 16(%rbx)
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	SeqCheckInStream_Read, .Lfunc_end13-SeqCheckInStream_Read
	.cfi_endproc

	.type	g_Alloc,@object         # @g_Alloc
	.data
	.p2align	3
g_Alloc:
	.quad	SzAlloc
	.quad	SzFree
	.size	g_Alloc, 16

	.type	g_BigAlloc,@object      # @g_BigAlloc
	.p2align	3
g_BigAlloc:
	.quad	SzBigAlloc
	.quad	SzBigFree
	.size	g_BigAlloc, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
