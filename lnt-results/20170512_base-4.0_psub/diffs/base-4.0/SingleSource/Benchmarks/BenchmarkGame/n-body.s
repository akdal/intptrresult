	.text
	.file	"n-body.bc"
	.globl	advance
	.p2align	4, 0x90
	.type	advance,@function
advance:                                # @advance
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movapd	%xmm0, %xmm5
	movq	%rsi, %rbx
	testl	%edi, %edi
	jle	.LBB0_7
# BB#1:                                 # %.lr.ph90.preheader
	movslq	%edi, %rdx
	movl	%edi, 4(%rsp)           # 4-byte Spill
	movl	%edi, %ecx
	leaq	104(%rbx), %r13
	leaq	-1(%rcx), %r14
	xorl	%edi, %edi
	movapd	%xmm5, 48(%rsp)         # 16-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph90
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
	movq	%rdi, %rax
	leaq	1(%rax), %rdi
	cmpq	%rdx, %rdi
	jge	.LBB0_3
# BB#8:                                 # %.lr.ph87
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	imulq	$56, %rax, %rax
	leaq	(%rbx,%rax), %r15
	leaq	16(%rbx,%rax), %r12
	movq	%r14, %rbp
	movq	%r13, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_9:                                #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%r15), %xmm7
	movupd	-48(%r13), %xmm0
	subpd	%xmm0, %xmm7
	movsd	(%r12), %xmm6           # xmm6 = mem[0],zero
	subsd	-32(%r13), %xmm6
	movapd	%xmm7, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm7, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm2
	movapd	%xmm6, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_11
# BB#10:                                # %call.sqrt
                                        #   in Loop: Header=BB0_9 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm6, 40(%rsp)         # 8-byte Spill
	movapd	%xmm7, 64(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	64(%rsp), %xmm7         # 16-byte Reload
	movsd	40(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movapd	48(%rsp), %xmm5         # 16-byte Reload
.LBB0_11:                               # %.split
                                        #   in Loop: Header=BB0_9 Depth=2
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	movapd	%xmm5, %xmm0
	divsd	%xmm1, %xmm0
	movsd	(%r13), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm6, %xmm2
	mulsd	%xmm1, %xmm2
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm7, %xmm1
	movapd	%xmm0, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm3, %xmm1
	movupd	8(%r12), %xmm4
	subpd	%xmm1, %xmm4
	movupd	%xmm4, 8(%r12)
	mulsd	%xmm0, %xmm2
	movsd	24(%r12), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 24(%r12)
	movsd	32(%r12), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm6
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm7, %xmm1
	mulpd	%xmm3, %xmm1
	movupd	-24(%r13), %xmm2
	addpd	%xmm1, %xmm2
	movupd	%xmm2, -24(%r13)
	mulsd	%xmm0, %xmm6
	addsd	-8(%r13), %xmm6
	movsd	%xmm6, -8(%r13)
	addq	$56, %r13
	decq	%rbp
	jne	.LBB0_9
# BB#12:                                #   in Loop: Header=BB0_2 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB0_3:                                # %.loopexit
                                        #   in Loop: Header=BB0_2 Depth=1
	addq	$56, %r13
	decq	%r14
	cmpq	%rcx, %rdi
	jne	.LBB0_2
# BB#4:                                 # %.preheader
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB0_7
# BB#5:                                 # %.lr.ph.preheader
	movapd	%xmm5, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbx), %xmm1
	movupd	24(%rbx), %xmm2
	mulpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	movupd	%xmm2, (%rbx)
	movsd	40(%rbx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm5, %xmm1
	addsd	16(%rbx), %xmm1
	movsd	%xmm1, 16(%rbx)
	addq	$56, %rbx
	decq	%rcx
	jne	.LBB0_6
.LBB0_7:                                # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	advance, .Lfunc_end0-advance
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	energy
	.p2align	4, 0x90
	.type	energy,@function
energy:                                 # @energy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	testl	%edi, %edi
	jle	.LBB1_1
# BB#3:                                 # %.lr.ph58.preheader
	movslq	%edi, %rax
	movl	%edi, %ecx
	leaq	56(%rsi), %rbp
	leaq	-1(%rcx), %r12
	xorpd	%xmm4, %xmm4
	xorl	%edx, %edx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph58
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
	movapd	%xmm4, %xmm0
	imulq	$56, %rdx, %r14
	movsd	48(%rsi,%r14), %xmm1    # xmm1 = mem[0],zero
	mulsd	.LCPI1_0(%rip), %xmm1
	movsd	24(%rsi,%r14), %xmm2    # xmm2 = mem[0],zero
	movsd	32(%rsi,%r14), %xmm3    # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm2
	mulsd	%xmm3, %xmm3
	addsd	%xmm2, %xmm3
	movsd	40(%rsi,%r14), %xmm4    # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	mulsd	%xmm1, %xmm4
	addsd	%xmm0, %xmm4
	incq	%rdx
	cmpq	%rax, %rdx
	jge	.LBB1_4
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	48(%rsi,%r14), %r15
	addq	%rsi, %r14
	movq	%r12, %rbx
	movq	%rbp, %r13
	.p2align	4, 0x90
.LBB1_7:                                #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	subsd	(%r13), %xmm1
	movsd	-40(%r15), %xmm2        # xmm2 = mem[0],zero
	subsd	8(%r13), %xmm2
	movsd	-32(%r15), %xmm0        # xmm0 = mem[0],zero
	subsd	16(%r13), %xmm0
	mulsd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB1_9
# BB#8:                                 # %call.sqrt
                                        #   in Loop: Header=BB1_7 Depth=2
	movsd	%xmm4, 32(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	32(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB1_9:                                # %.split
                                        #   in Loop: Header=BB1_7 Depth=2
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	mulsd	48(%r13), %xmm0
	divsd	%xmm1, %xmm0
	subsd	%xmm0, %xmm4
	addq	$56, %r13
	decq	%rbx
	jne	.LBB1_7
# BB#10:                                #   in Loop: Header=BB1_5 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rax            # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB1_4:                                # %.loopexit
                                        #   in Loop: Header=BB1_5 Depth=1
	addq	$56, %rbp
	decq	%r12
	cmpq	%rcx, %rdx
	jne	.LBB1_5
	jmp	.LBB1_2
.LBB1_1:
	xorpd	%xmm4, %xmm4
.LBB1_2:                                # %._crit_edge
	movapd	%xmm4, %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	energy, .Lfunc_end1-energy
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI2_1:
	.quad	4630752910647379422     # double 39.478417604357432
	.quad	4630752910647379422     # double 39.478417604357432
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_2:
	.quad	4630752910647379422     # double 39.478417604357432
	.text
	.globl	offset_momentum
	.p2align	4, 0x90
	.type	offset_momentum,@function
offset_momentum:                        # @offset_momentum
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph.preheader
	movl	%edi, %eax
	testb	$1, %al
	jne	.LBB2_4
# BB#3:
	xorpd	%xmm1, %xmm1
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	cmpl	$1, %edi
	jne	.LBB2_6
	jmp	.LBB2_8
.LBB2_1:
	xorpd	%xmm1, %xmm1
	xorpd	%xmm0, %xmm0
	jmp	.LBB2_8
.LBB2_4:                                # %.lr.ph.prol
	movsd	48(%rsi), %xmm2         # xmm2 = mem[0],zero
	movupd	24(%rsi), %xmm0
	movaps	%xmm2, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm0, %xmm3
	xorpd	%xmm1, %xmm1
	addpd	%xmm3, %xmm1
	mulsd	40(%rsi), %xmm2
	xorpd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	movl	$1, %ecx
	cmpl	$1, %edi
	je	.LBB2_8
.LBB2_6:                                # %.lr.ph.preheader.new
	subq	%rcx, %rax
	imulq	$56, %rcx, %rcx
	leaq	104(%rsi,%rcx), %rcx
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	-56(%rcx), %xmm2        # xmm2 = mem[0],zero
	movupd	-80(%rcx), %xmm3
	movaps	%xmm2, %xmm4
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm3, %xmm4
	addpd	%xmm1, %xmm4
	mulsd	-64(%rcx), %xmm2
	addsd	%xmm0, %xmm2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movupd	-24(%rcx), %xmm3
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm3, %xmm1
	addpd	%xmm4, %xmm1
	mulsd	-8(%rcx), %xmm0
	addsd	%xmm2, %xmm0
	addq	$112, %rcx
	addq	$-2, %rax
	jne	.LBB2_7
.LBB2_8:                                # %._crit_edge
	movapd	.LCPI2_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm2, %xmm1
	divpd	.LCPI2_1(%rip), %xmm1
	movupd	%xmm1, 24(%rsi)
	xorpd	%xmm2, %xmm0
	divsd	.LCPI2_2(%rip), %xmm0
	movsd	%xmm0, 40(%rsi)
	retq
.Lfunc_end2:
	.size	offset_momentum, .Lfunc_end2-offset_momentum
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI3_2:
	.quad	4630752910647379422     # double 39.478417604357432
	.quad	4630752910647379422     # double 39.478417604357432
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_1:
	.quad	4630752910647379422     # double 39.478417604357432
.LCPI3_3:
	.quad	4602678819172646912     # double 0.5
.LCPI3_4:
	.quad	4576918229304087675     # double 0.01
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 80
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movsd	bodies+48(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	bodies+24(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	xorpd	%xmm5, %xmm5
	addsd	%xmm5, %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movsd	bodies+40(%rip), %xmm3  # xmm3 = mem[0],zero
	movhpd	bodies+32(%rip), %xmm3  # xmm3 = xmm3[0],mem[0]
	mulpd	%xmm2, %xmm3
	xorpd	%xmm2, %xmm2
	addpd	%xmm3, %xmm2
	movsd	bodies+104(%rip), %xmm3 # xmm3 = mem[0],zero
	movsd	bodies+80(%rip), %xmm4  # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	addsd	%xmm1, %xmm4
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	movsd	bodies+96(%rip), %xmm1  # xmm1 = mem[0],zero
	movhpd	bodies+88(%rip), %xmm1  # xmm1 = xmm1[0],mem[0]
	mulpd	%xmm3, %xmm1
	addpd	%xmm2, %xmm1
	movsd	bodies+160(%rip), %xmm2 # xmm2 = mem[0],zero
	movsd	bodies+136(%rip), %xmm3 # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	addsd	%xmm4, %xmm3
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movsd	bodies+152(%rip), %xmm4 # xmm4 = mem[0],zero
	movhpd	bodies+144(%rip), %xmm4 # xmm4 = xmm4[0],mem[0]
	mulpd	%xmm2, %xmm4
	addpd	%xmm1, %xmm4
	movsd	bodies+216(%rip), %xmm1 # xmm1 = mem[0],zero
	movsd	bodies+192(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	%xmm3, %xmm2
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movsd	bodies+208(%rip), %xmm3 # xmm3 = mem[0],zero
	movhpd	bodies+200(%rip), %xmm3 # xmm3 = xmm3[0],mem[0]
	mulpd	%xmm1, %xmm3
	addpd	%xmm4, %xmm3
	movsd	bodies+272(%rip), %xmm4 # xmm4 = mem[0],zero
	movsd	bodies+248(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm4, %xmm1
	addsd	%xmm2, %xmm1
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	movsd	bodies+264(%rip), %xmm2 # xmm2 = mem[0],zero
	movhpd	bodies+256(%rip), %xmm2 # xmm2 = xmm2[0],mem[0]
	mulpd	%xmm4, %xmm2
	addpd	%xmm3, %xmm2
	movapd	.LCPI3_0(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm3, %xmm1
	divsd	.LCPI3_1(%rip), %xmm1
	movsd	%xmm1, bodies+24(%rip)
	xorpd	%xmm3, %xmm2
	divpd	.LCPI3_2(%rip), %xmm2
	movhpd	%xmm2, bodies+32(%rip)
	movlpd	%xmm2, bodies+40(%rip)
	xorl	%r12d, %r12d
	movl	$bodies+56, %r14d
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_1:                                # %.loopexit.i13
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	$5, %r15
	je	.LBB3_8
# BB#2:                                 # %.loopexit.i13..lr.ph58.i18_crit_edge
                                        #   in Loop: Header=BB3_3 Depth=1
	imulq	$56, %r15, %rax
	movsd	bodies+48(%rax), %xmm0  # xmm0 = mem[0],zero
	movsd	bodies+24(%rax), %xmm1  # xmm1 = mem[0],zero
	movsd	bodies+40(%rax), %xmm2  # xmm2 = mem[0],zero
	movhpd	bodies+32(%rax), %xmm2  # xmm2 = xmm2[0],mem[0]
	addq	$56, %r14
	movq	%r15, %r12
.LBB3_3:                                # %.lr.ph58.i18
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_5 Depth 2
	movapd	%xmm5, %xmm3
	mulsd	.LCPI3_3(%rip), %xmm0
	mulsd	%xmm1, %xmm1
	movapd	%xmm2, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	mulsd	%xmm4, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	%xmm2, %xmm2
	addsd	%xmm4, %xmm2
	mulsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm5
	addsd	%xmm3, %xmm5
	leaq	1(%r12), %r15
	cmpq	$4, %r15
	jg	.LBB3_1
# BB#4:                                 # %.lr.ph.i19
                                        #   in Loop: Header=BB3_3 Depth=1
	imulq	$56, %r12, %rax
	leaq	bodies+48(%rax), %r13
	movl	$4, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-48(%r13), %xmm1        # xmm1 = mem[0],zero
	subsd	(%rbp), %xmm1
	movsd	-40(%r13), %xmm2        # xmm2 = mem[0],zero
	subsd	8(%rbp), %xmm2
	movsd	-32(%r13), %xmm0        # xmm0 = mem[0],zero
	subsd	16(%rbp), %xmm0
	mulsd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB3_7
# BB#6:                                 # %call.sqrt
                                        #   in Loop: Header=BB3_5 Depth=2
	movapd	%xmm5, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm5           # 16-byte Reload
	movapd	%xmm0, %xmm1
.LBB3_7:                                # %.split
                                        #   in Loop: Header=BB3_5 Depth=2
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	mulsd	48(%rbp), %xmm0
	divsd	%xmm1, %xmm0
	subsd	%xmm0, %xmm5
	addq	$56, %rbp
	decq	%rbx
	cmpq	%r12, %rbx
	jne	.LBB3_5
	jmp	.LBB3_1
.LBB3_8:                                # %energy.exit24
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm5, %xmm0
	callq	printf
	movl	$5000000, %ebx          # imm = 0x4C4B40
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	movl	$5, %edi
	movl	$bodies, %esi
	movsd	.LCPI3_4(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	advance
	decl	%ebx
	jne	.LBB3_9
# BB#10:                                # %.lr.ph58.i.preheader
	xorpd	%xmm4, %xmm4
	xorl	%r14d, %r14d
	movl	$bodies+56, %r15d
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph58.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_14 Depth 2
	movapd	%xmm4, %xmm0
	movq	%r14, %r12
	imulq	$56, %r12, %rax
	movsd	bodies+48(%rax), %xmm1  # xmm1 = mem[0],zero
	mulsd	.LCPI3_3(%rip), %xmm1
	movsd	bodies+24(%rax), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm2
	movsd	bodies+32(%rax), %xmm3  # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm3
	addsd	%xmm2, %xmm3
	movsd	bodies+40(%rax), %xmm4  # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	mulsd	%xmm1, %xmm4
	addsd	%xmm0, %xmm4
	leaq	1(%r12), %r14
	cmpq	$4, %r14
	jg	.LBB3_11
# BB#13:                                # %.lr.ph.i8
                                        #   in Loop: Header=BB3_12 Depth=1
	leaq	bodies+48(%rax), %r13
	movl	$4, %ebx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB3_14:                               #   Parent Loop BB3_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-48(%r13), %xmm1        # xmm1 = mem[0],zero
	subsd	(%rbp), %xmm1
	movsd	-40(%r13), %xmm2        # xmm2 = mem[0],zero
	subsd	8(%rbp), %xmm2
	movsd	-32(%r13), %xmm0        # xmm0 = mem[0],zero
	subsd	16(%rbp), %xmm0
	mulsd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB3_16
# BB#15:                                # %call.sqrt64
                                        #   in Loop: Header=BB3_14 Depth=2
	movsd	%xmm4, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm4           # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB3_16:                               # %.split63
                                        #   in Loop: Header=BB3_14 Depth=2
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	mulsd	48(%rbp), %xmm0
	divsd	%xmm1, %xmm0
	subsd	%xmm0, %xmm4
	addq	$56, %rbp
	decq	%rbx
	cmpq	%r12, %rbx
	jne	.LBB3_14
.LBB3_11:                               # %.loopexit.i
                                        #   in Loop: Header=BB3_12 Depth=1
	addq	$56, %r15
	cmpq	$5, %r14
	jne	.LBB3_12
# BB#17:                                # %energy.exit
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm4, %xmm0
	callq	printf
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc

	.type	bodies,@object          # @bodies
	.data
	.globl	bodies
	.p2align	4
bodies:
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	4630752910647379422     # double 39.478417604357432
	.quad	4617136985637443884     # double 4.8414314424647209
	.quad	-4615467600764216452    # double -1.1603200440274284
	.quad	-4631240860977730576    # double -0.10362204447112311
	.quad	4603636522180398268     # double 0.60632639299583202
	.quad	4613514450253485211     # double 2.8119868449162602
	.quad	-4640446117579192555    # double -0.025218361659887629
	.quad	4585593052079010776     # double 0.037693674870389493
	.quad	4620886515960171111     # double 8.3433667182445799
	.quad	4616330128746480048     # double 4.1247985641243048
	.quad	-4622431185293064580    # double -0.40352341711432138
	.quad	-4616141094713322430    # double -1.0107743461787924
	.quad	4610900871547424531     # double 1.8256623712304119
	.quad	4576004977915405236     # double 0.0084157613765841535
	.quad	4577659745833829943     # double 0.011286326131968767
	.quad	4623448502799161807     # double 12.894369562139131
	.quad	-4598675596822288770    # double -15.111151401698631
	.quad	-4626158513131520608    # double -0.22330757889265573
	.quad	4607555276345777135     # double 1.0827910064415354
	.quad	4605999890795117509     # double 0.86871301816960822
	.quad	-4645973824767902084    # double -0.010832637401363636
	.quad	4565592097032511155     # double 0.0017237240570597112
	.quad	4624847617829197610     # double 15.379697114850917
	.quad	-4595383180696444384    # double -25.919314609987964
	.quad	4595626498235032896     # double 0.17925877295037118
	.quad	4606994084859067466     # double 0.97909073224389798
	.quad	4603531791922690979     # double 0.59469899864767617
	.quad	-4638202354754755082    # double -0.034755955504078104
	.quad	4566835785178257836     # double 0.0020336868699246304
	.size	bodies, 280

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%.9f\n"
	.size	.L.str, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
