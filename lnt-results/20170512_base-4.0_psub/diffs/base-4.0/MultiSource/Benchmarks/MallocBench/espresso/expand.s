	.text
	.file	"expand.bc"
	.globl	expand
	.p2align	4, 0x90
	.type	expand,@function
expand:                                 # @expand
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	cmpl	$0, use_random_order(%rip)
	je	.LBB0_2
# BB#1:
	xorl	%eax, %eax
	callq	random_order
	jmp	.LBB0_3
.LBB0_2:
	movl	$ascend, %esi
	xorl	%eax, %eax
	callq	mini_sort
.LBB0_3:
	movq	%rax, %rbx
	movl	cube(%rip), %ebp
	movl	$2, %r15d
	cmpl	$33, %ebp
	movl	$2, %eax
	jl	.LBB0_5
# BB#4:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	addl	$2, %eax
.LBB0_5:
	movslq	%eax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jl	.LBB0_7
# BB#6:
	leal	-1(%rbp), %r15d
	sarl	$5, %r15d
	addl	$2, %r15d
.LBB0_7:
	movslq	%r15d, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r15
	movl	cube(%rip), %ebp
	movl	$2, %eax
	cmpl	$33, %ebp
	jl	.LBB0_9
# BB#8:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	addl	$2, %eax
.LBB0_9:
	movslq	%eax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r13
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB0_11
# BB#10:
	movl	$8, %edi
	jmp	.LBB0_12
.LBB0_11:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_12:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB0_14
# BB#13:
	movl	$8, %edi
	jmp	.LBB0_15
.LBB0_14:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_15:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r14
	testl	%r12d, %r12d
	je	.LBB0_21
# BB#16:
	movl	cube+4(%rip), %eax
	testl	%eax, %eax
	jle	.LBB0_21
# BB#17:                                # %.lr.ph103.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph103
                                        # =>This Inner Loop Header: Depth=1
	movq	cube+112(%rip), %rcx
	cmpl	$0, (%rcx,%rbp,4)
	je	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_18 Depth=1
	movq	cube+72(%rip), %rax
	movq	(%rax,%rbp,8), %rdx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r13, %rsi
	callq	set_or
	movl	cube+4(%rip), %eax
.LBB0_20:                               #   in Loop: Header=BB0_18 Depth=1
	incq	%rbp
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB0_18
.LBB0_21:                               # %.loopexit
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	24(%rbx), %r12
	movl	(%rbx), %eax
	movl	12(%rbx), %ecx
	movl	%eax, %edx
	imull	%ecx, %edx
	testl	%edx, %edx
	jle	.LBB0_25
# BB#22:                                # %.lr.ph100.preheader
	movslq	%edx, %rax
	leaq	(%r12,%rax,4), %rcx
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph100
                                        # =>This Inner Loop Header: Depth=1
	andb	$-73, 1(%rdx)
	movslq	(%rbx), %rax
	leaq	(%rdx,%rax,4), %rdx
	cmpq	%rcx, %rdx
	jb	.LBB0_23
# BB#24:                                # %._crit_edge101.loopexit
	movl	12(%rbx), %ecx
.LBB0_25:                               # %._crit_edge101
	movl	%eax, %edx
	imull	%ecx, %edx
	testl	%edx, %edx
	jle	.LBB0_36
# BB#26:                                # %.lr.ph96.preheader
	movslq	%edx, %rax
	leaq	(%r12,%rax,4), %rbp
	movq	8(%rsp), %r15           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_27:                               # %.lr.ph96
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12), %eax
	testw	%ax, %ax
	js	.LBB0_34
# BB#28:                                # %.lr.ph96
                                        #   in Loop: Header=BB0_27 Depth=1
	andl	$2048, %eax             # imm = 0x800
	jne	.LBB0_34
# BB#29:                                #   in Loop: Header=BB0_27 Depth=1
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %r8
	movq	24(%rsp), %r9           # 8-byte Reload
	pushq	%r12
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	leaq	20(%rsp), %rax
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	expand1
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	testb	$4, debug(%rip)
	je	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_27 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	pc1
	movq	%rax, %rcx
	movl	4(%rsp), %edx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
.LBB0_31:                               #   in Loop: Header=BB0_27 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	set_copy
	movl	(%r12), %eax
	movl	$-34817, %ecx           # imm = 0xFFFF77FF
	andl	%ecx, %eax
	orl	$32768, %eax            # imm = 0x8000
	movl	%eax, (%r12)
	cmpl	$0, 4(%rsp)
	jne	.LBB0_34
# BB#32:                                #   in Loop: Header=BB0_27 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	setp_equal
	testl	%eax, %eax
	jne	.LBB0_34
# BB#33:                                #   in Loop: Header=BB0_27 Depth=1
	orb	$64, 1(%r12)
	.p2align	4, 0x90
.LBB0_34:                               #   in Loop: Header=BB0_27 Depth=1
	movslq	(%rbx), %rax
	leaq	(%r12,%rax,4), %r12
	cmpq	%rbp, %r12
	jb	.LBB0_27
# BB#35:                                # %._crit_edge97.loopexit
	movq	24(%rbx), %r12
	movl	12(%rbx), %ecx
.LBB0_36:                               # %._crit_edge97
	movl	$0, 16(%rbx)
	imull	%ecx, %eax
	testl	%eax, %eax
	jle	.LBB0_37
# BB#38:                                # %.lr.ph.preheader
	cltq
	leaq	(%r12,%rax,4), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_39:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12), %edx
	testb	$8, %dh
	jne	.LBB0_40
# BB#41:                                #   in Loop: Header=BB0_39 Depth=1
	orl	$8192, %edx             # imm = 0x2000
	movl	%edx, (%r12)
	incl	16(%rbx)
	jmp	.LBB0_42
	.p2align	4, 0x90
.LBB0_40:                               #   in Loop: Header=BB0_39 Depth=1
	andl	$-8193, %edx            # imm = 0xDFFF
	movl	%edx, (%r12)
	movl	$1, %ecx
.LBB0_42:                               #   in Loop: Header=BB0_39 Depth=1
	movslq	(%rbx), %rdx
	leaq	(%r12,%rdx,4), %r12
	cmpq	%rax, %r12
	jb	.LBB0_39
# BB#43:                                # %._crit_edge
	testl	%ecx, %ecx
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_45
# BB#44:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_inactive
	movq	%rax, %rbx
	jmp	.LBB0_45
.LBB0_37:
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB0_45:                               # %._crit_edge.thread
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_47
# BB#46:
	callq	free
.LBB0_47:
	testq	%rbp, %rbp
	je	.LBB0_49
# BB#48:
	movq	%rbp, %rdi
	callq	free
.LBB0_49:
	testq	%r13, %r13
	je	.LBB0_51
# BB#50:
	movq	%r13, %rdi
	callq	free
.LBB0_51:
	testq	%r15, %r15
	je	.LBB0_53
# BB#52:
	movq	%r15, %rdi
	callq	free
.LBB0_53:
	testq	%r14, %r14
	je	.LBB0_55
# BB#54:
	movq	%r14, %rdi
	callq	free
.LBB0_55:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	expand, .Lfunc_end0-expand
	.cfi_endproc

	.globl	expand1
	.p2align	4, 0x90
	.type	expand1,@function
expand1:                                # @expand1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 80
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	96(%rsp), %rbp
	movq	88(%rsp), %rdi
	movq	80(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testb	$8, debug(%rip)
	movq	%rdi, (%rsp)            # 8-byte Spill
	je	.LBB1_2
# BB#1:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r9, %r13
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r13, %r9
.LBB1_2:
	orb	$-128, 1(%rbp)
	movslq	12(%rbx), %rax
	movl	%eax, 16(%rbx)
	movslq	(%rbx), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB1_5
# BB#3:                                 # %.lr.ph33.i.preheader
	movq	24(%rbx), %rax
	leaq	(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph33.i
                                        # =>This Inner Loop Header: Depth=1
	orb	$32, 1(%rax)
	movslq	(%rbx), %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB1_4
.LBB1_5:                                # %._crit_edge.i
	testq	%r12, %r12
	je	.LBB1_13
# BB#6:
	movslq	12(%r12), %rax
	movl	%eax, 16(%r12)
	movslq	(%r12), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB1_13
# BB#7:                                 # %.lr.ph.i.preheader
	movq	24(%r12), %rax
	leaq	(%rax,%rcx,4), %rcx
	movl	$-8193, %esi            # imm = 0xDFFF
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	testb	$8, %dh
	jne	.LBB1_10
# BB#9:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB1_8 Depth=1
	testw	%dx, %dx
	js	.LBB1_10
# BB#11:                                #   in Loop: Header=BB1_8 Depth=1
	orl	$8192, %edx             # imm = 0x2000
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_10:                               #   in Loop: Header=BB1_8 Depth=1
	decl	16(%r12)
	movl	(%rax), %edx
	andl	%esi, %edx
.LBB1_12:                               #   in Loop: Header=BB1_8 Depth=1
	movl	%edx, (%rax)
	movslq	(%r12), %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB1_8
.LBB1_13:                               # %setup_BB_CC.exit
	movl	$0, (%rdi)
	xorl	%eax, %eax
	movq	%r9, %r13
	movq	%r9, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	set_diff
	xorl	%eax, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	callq	setp_empty
	testl	%eax, %eax
	jne	.LBB1_15
# BB#14:
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	set_diff
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	elim_lowering
.LBB1_15:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	essen_parts
	xorl	%eax, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	set_or
	cmpl	$0, 16(%r12)
	jle	.LBB1_20
# BB#16:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r13, %r8
	movq	(%rsp), %r9             # 8-byte Reload
	callq	select_feasible
	cmpl	$0, 16(%r12)
	jle	.LBB1_20
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph55
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	most_frequent
	movl	%eax, %ecx
	andl	$31, %ecx
	movl	$1, %edx
	shll	%cl, %edx
	sarl	$5, %eax
	cltq
	orl	%edx, 4(%r14,%rax,4)
	movl	$-2, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	roll	%cl, %edx
	andl	%edx, 4(%r15,%rax,4)
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	essen_parts
	cmpl	$0, 16(%r12)
	jg	.LBB1_18
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph
                                        #   in Loop: Header=BB1_20 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	mincov
.LBB1_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 16(%rbx)
	jg	.LBB1_19
# BB#21:                                # %._crit_edge
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	set_or                  # TAILCALL
.Lfunc_end1:
	.size	expand1, .Lfunc_end1-expand1
	.cfi_endproc

	.globl	essen_parts
	.p2align	4, 0x90
	.type	essen_parts,@function
essen_parts:                            # @essen_parts
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 80
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r14
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	movq	cube+80(%rip), %rax
	movq	(%rax), %r13
	movq	cube+96(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	set_copy
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB2_27
# BB#1:                                 # %.lr.ph118
	movq	24(%r12), %rbp
	leaq	(%rbp,%rax,4), %r15
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_10 Depth 2
                                        #     Child Loop BB2_16 Depth 2
                                        #       Child Loop BB2_19 Depth 3
	testb	$32, 1(%rbp)
	je	.LBB2_26
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movslq	cube+108(%rip), %r8
	cmpq	$-1, %r8
	je	.LBB2_4
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	(%r14,%r8,4), %eax
	andl	(%rbp,%r8,4), %eax
	movl	%eax, %edx
	shrl	%edx
	orl	%eax, %edx
	notl	%edx
	andl	cube+104(%rip), %edx
	je	.LBB2_6
# BB#7:                                 #   in Loop: Header=BB2_2 Depth=1
	movzbl	%dl, %esi
	movzbl	%dh, %eax  # NOREX
	movl	bit_count(,%rax,4), %eax
	addl	bit_count(,%rsi,4), %eax
	movl	%edx, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	addl	bit_count(,%rsi,4), %eax
	shrl	$24, %edx
	addl	bit_count(,%rdx,4), %eax
	cmpl	$1, %eax
	jle	.LBB2_8
	jmp	.LBB2_26
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB2_14
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%eax, %eax
.LBB2_8:                                # %.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$2, %r8d
	jl	.LBB2_14
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	$1, %edx
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14,%rdx,4), %edi
	andl	(%rbp,%rdx,4), %edi
	movl	%edi, %esi
	shrl	%esi
	orl	%edi, %esi
	andl	$1431655765, %esi       # imm = 0x55555555
	cmpl	$1431655765, %esi       # imm = 0x55555555
	je	.LBB2_13
# BB#11:                                #   in Loop: Header=BB2_10 Depth=2
	cmpl	$1, %eax
	je	.LBB2_26
# BB#12:                                #   in Loop: Header=BB2_10 Depth=2
	xorl	$1431655765, %esi       # imm = 0x55555555
	movl	%esi, %edi
	andl	$85, %edi
	movl	%esi, %ecx
	shrl	$8, %ecx
	andl	$85, %ecx
	movl	%esi, %ebx
	shrl	$16, %ebx
	andl	$85, %ebx
	shrl	$24, %esi
	addl	bit_count(,%rdi,4), %eax
	addl	bit_count(,%rcx,4), %eax
	addl	bit_count(,%rbx,4), %eax
	addl	bit_count(,%rsi,4), %eax
	cmpl	$1, %eax
	jg	.LBB2_26
.LBB2_13:                               #   in Loop: Header=BB2_10 Depth=2
	incq	%rdx
	cmpq	%r8, %rdx
	jl	.LBB2_10
	.p2align	4, 0x90
.LBB2_14:                               # %.loopexit100
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	cube+8(%rip), %rcx
	movslq	cube+4(%rip), %r11
	cmpl	%r11d, %ecx
	jge	.LBB2_23
# BB#15:                                # %.lr.ph112
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	cube+72(%rip), %r8
	movq	cube+48(%rip), %r9
	movq	cube+40(%rip), %r10
	.p2align	4, 0x90
.LBB2_16:                               #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_19 Depth 3
	movslq	(%r9,%rcx,4), %rdi
	movslq	(%r10,%rcx,4), %rsi
	cmpl	%edi, %esi
	jg	.LBB2_20
# BB#17:                                # %.lr.ph107.preheader
                                        #   in Loop: Header=BB2_16 Depth=2
	movq	(%r8,%rcx,8), %rdx
	decq	%rsi
	.p2align	4, 0x90
.LBB2_19:                               # %.lr.ph107
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	4(%r14,%rsi,4), %ebx
	andl	4(%rbp,%rsi,4), %ebx
	testl	4(%rdx,%rsi,4), %ebx
	jne	.LBB2_22
# BB#18:                                #   in Loop: Header=BB2_19 Depth=3
	incq	%rsi
	cmpq	%rdi, %rsi
	jl	.LBB2_19
.LBB2_20:                               # %._crit_edge
                                        #   in Loop: Header=BB2_16 Depth=2
	testl	%eax, %eax
	jg	.LBB2_26
# BB#21:                                #   in Loop: Header=BB2_16 Depth=2
	incl	%eax
.LBB2_22:                               # %.loopexit
                                        #   in Loop: Header=BB2_16 Depth=2
	incq	%rcx
	cmpq	%r11, %rcx
	jl	.LBB2_16
.LBB2_23:                               # %._crit_edge113
                                        #   in Loop: Header=BB2_2 Depth=1
	testl	%eax, %eax
	je	.LBB2_24
# BB#25:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	force_lower
	decl	16(%r12)
	andb	$-33, 1(%rbp)
	jmp	.LBB2_26
.LBB2_24:                               #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	fatal
	.p2align	4, 0x90
.LBB2_26:                               # %.thread
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	(%r12), %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%r15, %rbp
	jb	.LBB2_2
.LBB2_27:                               # %._crit_edge119
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	setp_empty
	testl	%eax, %eax
	jne	.LBB2_29
# BB#28:
	xorl	%eax, %eax
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	set_diff
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	elim_lowering
.LBB2_29:
	testb	$8, debug(%rip)
	jne	.LBB2_31
# BB#30:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_31:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pc1
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	pc2
	movq	%rax, %rcx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end2:
	.size	essen_parts, .Lfunc_end2-essen_parts
	.cfi_endproc

	.globl	essen_raising
	.p2align	4, 0x90
	.type	essen_raising,@function
essen_raising:                          # @essen_raising
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 80
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r12
	movq	cube+80(%rip), %rax
	movq	(%rax), %rbx
	movq	cube+96(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_copy
	movl	(%r12), %edx
	movl	12(%r12), %ecx
	imull	%edx, %ecx
	testl	%ecx, %ecx
	jle	.LBB3_19
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%r12), %rax
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %r10
	leaq	-4(%rbx), %r11
	leaq	4(%rbx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	-12(%rbx), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	$1, %r13d
	movl	$2, %r9d
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_12 Depth 2
                                        #     Child Loop BB3_16 Depth 2
	testb	$32, 1(%rax)
	je	.LBB3_18
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	(%rbx), %edx
	movl	%edx, %esi
	andl	$1023, %esi             # imm = 0x3FF
	testw	$1023, %dx              # imm = 0x3FF
	movq	%rsi, %r8
	cmoveq	%r13, %r8
	cmpq	$8, %r8
	jb	.LBB3_15
# BB#4:                                 # %min.iters.checked
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%r8, %rdi
	andq	$1016, %rdi             # imm = 0x3F8
	je	.LBB3_15
# BB#5:                                 # %vector.memcheck
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	1(%rsi), %rdx
	testl	%esi, %esi
	cmovneq	%r9, %rdx
	leaq	(%r11,%rdx,4), %rbp
	leaq	4(%rax,%rsi,4), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB3_7
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rcx
	leaq	-4(%rax,%rdx,4), %rdx
	cmpq	%rcx, %rdx
	jb	.LBB3_15
.LBB3_7:                                # %vector.body.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	-8(%rdi), %rcx
	movq	%rcx, %rdx
	shrq	$3, %rdx
	btl	$3, %ecx
	jb	.LBB3_8
# BB#9:                                 # %vector.body.prol
                                        #   in Loop: Header=BB3_2 Depth=1
	movups	-12(%rbx,%rsi,4), %xmm0
	movups	-28(%rbx,%rsi,4), %xmm1
	movups	-12(%rax,%rsi,4), %xmm2
	movups	-28(%rax,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rsi,4)
	movups	%xmm3, -28(%rbx,%rsi,4)
	movl	$8, %ebp
	testq	%rdx, %rdx
	jne	.LBB3_11
	jmp	.LBB3_13
.LBB3_8:                                #   in Loop: Header=BB3_2 Depth=1
	xorl	%ebp, %ebp
	testq	%rdx, %rdx
	je	.LBB3_13
.LBB3_11:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%r8, %rdx
	andq	$-8, %rdx
	negq	%rdx
	negq	%rbp
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%rsi,4), %r9
	leaq	-12(%rax,%rsi,4), %r14
	.p2align	4, 0x90
.LBB3_12:                               # %vector.body
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r9,%rbp,4), %xmm0
	movups	-16(%r9,%rbp,4), %xmm1
	movups	(%r14,%rbp,4), %xmm2
	movups	-16(%r14,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%r9,%rbp,4)
	movups	%xmm3, -16(%r9,%rbp,4)
	movups	-32(%r9,%rbp,4), %xmm0
	movups	-48(%r9,%rbp,4), %xmm1
	movups	-32(%r14,%rbp,4), %xmm2
	movups	-48(%r14,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%r9,%rbp,4)
	movups	%xmm3, -48(%r9,%rbp,4)
	addq	$-16, %rbp
	cmpq	%rbp, %rdx
	jne	.LBB3_12
.LBB3_13:                               # %middle.block
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	%rdi, %r8
	movl	$2, %r9d
	je	.LBB3_17
# BB#14:                                #   in Loop: Header=BB3_2 Depth=1
	subq	%rdi, %rsi
	.p2align	4, 0x90
.LBB3_15:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB3_16:                               # %scalar.ph
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %ecx
	orl	%ecx, -4(%rbx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB3_16
.LBB3_17:                               # %.loopexit.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	(%r12), %edx
.LBB3_18:                               # %.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	movslq	%edx, %rcx
	leaq	(%rax,%rcx,4), %rax
	cmpq	%r10, %rax
	jb	.LBB3_2
.LBB3_19:                               # %._crit_edge
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	set_diff
	xorl	%eax, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	set_or
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	set_diff
	testb	$8, debug(%rip)
	jne	.LBB3_21
# BB#20:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_21:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	pc1
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	pc2
	movq	%rax, %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end3:
	.size	essen_raising, .Lfunc_end3-essen_raising
	.cfi_endproc

	.globl	elim_lowering
	.p2align	4, 0x90
	.type	elim_lowering,@function
elim_lowering:                          # @elim_lowering
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 64
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	cube+80(%rip), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	callq	set_or
	movl	(%r15), %r12d
	movl	12(%r15), %ecx
	imull	%r12d, %ecx
	testl	%ecx, %ecx
	jle	.LBB4_17
# BB#1:                                 # %.lr.ph110
	movq	24(%r15), %rdx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,4), %r11
	movq	cube+72(%rip), %r8
	movq	cube+48(%rip), %r9
	movq	cube+40(%rip), %r10
	.p2align	4, 0x90
.LBB4_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
                                        #     Child Loop BB4_11 Depth 2
                                        #       Child Loop BB4_14 Depth 3
	testb	$32, 1(%rdx)
	je	.LBB4_16
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movslq	cube+108(%rip), %rdi
	cmpq	$-1, %rdi
	je	.LBB4_10
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	movl	(%rax,%rdi,4), %ecx
	andl	(%rdx,%rdi,4), %ecx
	movl	%ecx, %esi
	shrl	%esi
	orl	%ecx, %esi
	notl	%esi
	testl	cube+104(%rip), %esi
	jne	.LBB4_9
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpl	$2, %edi
	jl	.LBB4_10
# BB#6:                                 # %.lr.ph101.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph101
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rcx,4), %esi
	andl	(%rdx,%rcx,4), %esi
	movl	%esi, %ebx
	shrl	%ebx
	orl	%esi, %ebx
	andl	$1431655765, %ebx       # imm = 0x55555555
	cmpl	$1431655765, %ebx       # imm = 0x55555555
	jne	.LBB4_9
# BB#7:                                 #   in Loop: Header=BB4_8 Depth=2
	incq	%rcx
	cmpq	%rdi, %rcx
	jl	.LBB4_8
	.p2align	4, 0x90
.LBB4_10:                               # %.loopexit96
                                        #   in Loop: Header=BB4_2 Depth=1
	movslq	cube+8(%rip), %rdi
	movslq	cube+4(%rip), %r13
	cmpl	%r13d, %edi
	jge	.LBB4_16
.LBB4_11:                               #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_14 Depth 3
	movslq	(%r9,%rdi,4), %rsi
	movslq	(%r10,%rdi,4), %rcx
	cmpl	%esi, %ecx
	jg	.LBB4_9
# BB#12:                                # %.lr.ph103.preheader
                                        #   in Loop: Header=BB4_11 Depth=2
	movq	(%r8,%rdi,8), %rbp
	decq	%rcx
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph103
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	4(%rax,%rcx,4), %ebx
	andl	4(%rdx,%rcx,4), %ebx
	testl	4(%rbp,%rcx,4), %ebx
	jne	.LBB4_15
# BB#13:                                #   in Loop: Header=BB4_14 Depth=3
	incq	%rcx
	cmpq	%rsi, %rcx
	jl	.LBB4_14
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_15:                               #   in Loop: Header=BB4_11 Depth=2
	incq	%rdi
	cmpq	%r13, %rdi
	jl	.LBB4_11
	jmp	.LBB4_16
	.p2align	4, 0x90
.LBB4_9:                                # %.critedge
                                        #   in Loop: Header=BB4_2 Depth=1
	decl	16(%r15)
	andb	$-33, 1(%rdx)
	movl	(%r15), %r12d
.LBB4_16:                               # %.loopexit95
                                        #   in Loop: Header=BB4_2 Depth=1
	movslq	%r12d, %rcx
	leaq	(%rdx,%rcx,4), %rdx
	cmpq	%r11, %rdx
	jb	.LBB4_2
.LBB4_17:                               # %._crit_edge
	testq	%r14, %r14
	je	.LBB4_28
# BB#18:
	movl	(%r14), %ecx
	movl	12(%r14), %esi
	imull	%ecx, %esi
	testl	%esi, %esi
	jle	.LBB4_28
# BB#19:                                # %.lr.ph
	movq	24(%r14), %rdx
	movslq	%esi, %rsi
	leaq	(%rdx,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB4_20:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_22 Depth 2
	movl	(%rdx), %ebx
	testb	$32, %bh
	je	.LBB4_27
# BB#21:                                #   in Loop: Header=BB4_20 Depth=1
	andl	$1023, %ebx             # imm = 0x3FF
	.p2align	4, 0x90
.LBB4_22:                               #   Parent Loop BB4_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rbx,4), %edi
	notl	%edi
	testl	(%rdx,%rbx,4), %edi
	jne	.LBB4_24
# BB#23:                                #   in Loop: Header=BB4_22 Depth=2
	leaq	-1(%rbx), %rdi
	cmpq	$1, %rbx
	movq	%rdi, %rbx
	jg	.LBB4_22
	jmp	.LBB4_25
	.p2align	4, 0x90
.LBB4_24:                               # %._crit_edge126
                                        #   in Loop: Header=BB4_20 Depth=1
	movl	%ebx, %edi
.LBB4_25:                               #   in Loop: Header=BB4_20 Depth=1
	testl	%edi, %edi
	je	.LBB4_27
# BB#26:                                #   in Loop: Header=BB4_20 Depth=1
	decl	16(%r14)
	andb	$-33, 1(%rdx)
	movl	(%r14), %ecx
.LBB4_27:                               #   in Loop: Header=BB4_20 Depth=1
	movslq	%ecx, %rdi
	leaq	(%rdx,%rdi,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB4_20
.LBB4_28:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	elim_lowering, .Lfunc_end4-elim_lowering
	.cfi_endproc

	.globl	most_frequent
	.p2align	4, 0x90
	.type	most_frequent,@function
most_frequent:                          # @most_frequent
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 48
.Lcfi75:
	.cfi_offset %rbx, -48
.Lcfi76:
	.cfi_offset %r12, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	cube(%rip), %rbp
	leaq	(,%rbp,4), %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%rbp, %rbp
	jle	.LBB5_2
# BB#1:                                 # %.lr.ph55
	leal	-1(%rbp), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB5_2:                                # %._crit_edge56
	testq	%r15, %r15
	je	.LBB5_9
# BB#3:
	movl	(%r15), %eax
	movl	12(%r15), %ecx
	imull	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB5_9
# BB#4:
	movq	24(%r15), %rbp
	movslq	%ecx, %rcx
	leaq	(%rbp,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph52
                                        # =>This Inner Loop Header: Depth=1
	testb	$32, 1(%rbp)
	je	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	set_adjcnt
	movl	(%r15), %eax
.LBB5_7:                                #   in Loop: Header=BB5_5 Depth=1
	movslq	%eax, %rcx
	leaq	(%rbp,%rcx,4), %rbp
	cmpq	%rbx, %rbp
	jb	.LBB5_5
# BB#8:                                 # %.preheader.loopexit
	movl	cube(%rip), %ebp
.LBB5_9:                                # %.preheader
	testl	%ebp, %ebp
	jle	.LBB5_10
# BB#11:                                # %.lr.ph
	movslq	%ebp, %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB5_12:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	sarl	$5, %esi
	movslq	%esi, %rsi
	movl	4(%r14,%rsi,4), %esi
	btl	%edx, %esi
	jae	.LBB5_14
# BB#13:                                #   in Loop: Header=BB5_12 Depth=1
	movl	(%r12,%rdx,4), %esi
	cmpl	%ecx, %esi
	cmovgl	%edx, %ebp
	cmovgel	%esi, %ecx
.LBB5_14:                               #   in Loop: Header=BB5_12 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB5_12
	jmp	.LBB5_15
.LBB5_10:
	movl	$-1, %ebp
.LBB5_15:                               # %._crit_edge
	testq	%r12, %r12
	je	.LBB5_17
# BB#16:
	movq	%r12, %rdi
	callq	free
.LBB5_17:
	testb	$8, debug(%rip)
	je	.LBB5_19
# BB#18:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pc2
	movq	%rax, %rcx
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	movq	%rcx, %rdx
	callq	printf
.LBB5_19:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	most_frequent, .Lfunc_end5-most_frequent
	.cfi_endproc

	.globl	setup_BB_CC
	.p2align	4, 0x90
	.type	setup_BB_CC,@function
setup_BB_CC:                            # @setup_BB_CC
	.cfi_startproc
# BB#0:
	movslq	12(%rdi), %rax
	movl	%eax, 16(%rdi)
	movslq	(%rdi), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB6_3
# BB#1:                                 # %.lr.ph33.preheader
	movq	24(%rdi), %rax
	leaq	(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph33
                                        # =>This Inner Loop Header: Depth=1
	orb	$32, 1(%rax)
	movslq	(%rdi), %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB6_2
.LBB6_3:                                # %._crit_edge
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 16
.Lcfi81:
	.cfi_offset %rbx, -16
	testq	%rsi, %rsi
	je	.LBB6_11
# BB#4:
	movslq	12(%rsi), %rax
	movl	%eax, 16(%rsi)
	movslq	(%rsi), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB6_11
# BB#5:                                 # %.lr.ph.preheader
	movq	24(%rsi), %rax
	leaq	(%rax,%rcx,4), %rcx
	movl	$-8193, %edx            # imm = 0xDFFF
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ebx
	testb	$8, %bh
	jne	.LBB6_8
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_6 Depth=1
	testw	%bx, %bx
	js	.LBB6_8
# BB#9:                                 #   in Loop: Header=BB6_6 Depth=1
	orl	$8192, %ebx             # imm = 0x2000
	jmp	.LBB6_10
	.p2align	4, 0x90
.LBB6_8:                                #   in Loop: Header=BB6_6 Depth=1
	decl	16(%rsi)
	movl	(%rax), %ebx
	andl	%edx, %ebx
.LBB6_10:                               #   in Loop: Header=BB6_6 Depth=1
	movl	%ebx, (%rax)
	movslq	(%rsi), %rdi
	leaq	(%rax,%rdi,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB6_6
.LBB6_11:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end6:
	.size	setup_BB_CC, .Lfunc_end6-setup_BB_CC
	.cfi_endproc

	.globl	select_feasible
	.p2align	4, 0x90
	.type	select_feasible,@function
select_feasible:                        # @select_feasible
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 160
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%r8, 80(%rsp)           # 8-byte Spill
	movq	%rcx, %rbx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movslq	16(%r15), %r14
	shlq	$3, %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, %rbp
	movslq	(%r15), %rax
	movslq	12(%r15), %rdx
	imulq	%rax, %rdx
	testl	%edx, %edx
	jle	.LBB7_1
# BB#2:                                 # %.lr.ph159
	movq	24(%r15), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	shlq	$2, %rax
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	testb	$32, 1(%rcx)
	je	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movslq	%r12d, %rsi
	incl	%r12d
	movq	%rcx, (%rbp,%rsi,8)
.LBB7_5:                                #   in Loop: Header=BB7_3 Depth=1
	addq	%rax, %rcx
	cmpq	%rdx, %rcx
	jb	.LBB7_3
	jmp	.LBB7_6
.LBB7_1:
	xorl	%r12d, %r12d
.LBB7_6:                                # %._crit_edge160
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	movl	%r12d, %r14d
	movl	%r14d, %edi
	callq	sf_new
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	movq	32(%rsp), %r14          # 8-byte Reload
	jle	.LBB7_14
# BB#7:                                 # %.lr.ph155
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %r8
	movslq	(%rax), %rax
	movl	%r12d, %ecx
	leaq	-1(%rcx), %r9
	movq	%rcx, %r11
	andq	$7, %r11
	je	.LBB7_8
# BB#9:                                 # %.prol.preheader
	leaq	(,%rax,4), %r10
	xorl	%edi, %edi
	movq	%r8, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_10:                               # =>This Inner Loop Header: Depth=1
	movq	%rdx, (%rsi,%rdi,8)
	incq	%rdi
	addq	%r10, %rdx
	cmpq	%rdi, %r11
	jne	.LBB7_10
	jmp	.LBB7_11
.LBB7_8:
	xorl	%edi, %edi
.LBB7_11:                               # %.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB7_14
# BB#12:                                # %.lr.ph155.new
	subq	%rdi, %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	56(%rdx,%rdi,8), %rdx
	imulq	%rax, %rdi
	leaq	(%r8,%rdi,4), %rsi
	movq	%rax, %r8
	shlq	$5, %r8
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB7_13:                               # =>This Inner Loop Header: Depth=1
	movq	%rsi, -56(%rdx)
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -48(%rdx)
	addq	%rax, %rdi
	movq	%rdi, -40(%rdx)
	addq	%rax, %rdi
	movq	%rdi, -32(%rdx)
	addq	%rax, %rdi
	movq	%rdi, -24(%rdx)
	addq	%rax, %rdi
	movq	%rdi, -16(%rdx)
	addq	%rax, %rdi
	movq	%rdi, -8(%rdx)
	addq	%rax, %rdi
	movq	%rdi, (%rdx)
	addq	$64, %rdx
	addq	%r8, %rsi
	addq	$-8, %rcx
	jne	.LBB7_13
.LBB7_14:                               # %.preheader137.preheader
                                        # implicit-def: %RAX
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r12d, %r15d
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	jmp	.LBB7_15
	.p2align	4, 0x90
.LBB7_36:                               # %._crit_edge151
                                        #   in Loop: Header=BB7_15 Depth=1
	xorl	%eax, %eax
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%r14, %rsi
	movq	72(%rsp), %rdx          # 8-byte Reload
	callq	set_or
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	set_diff
	testb	$8, debug(%rip)
	je	.LBB7_38
# BB#37:                                #   in Loop: Header=BB7_15 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pc1
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pc2
	movq	%rax, %rcx
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%rcx, %rdx
	callq	printf
.LBB7_38:                               #   in Loop: Header=BB7_15 Depth=1
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	essen_parts
	movq	%r15, %rdi
	movl	4(%rsp), %r15d          # 4-byte Reload
.LBB7_15:                               # %.preheader137
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_17 Depth 2
                                        #     Child Loop BB7_28 Depth 2
                                        #       Child Loop BB7_29 Depth 3
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	essen_raising
	movl	%r15d, 8(%rsp)          # 4-byte Spill
	testl	%r15d, %r15d
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	jle	.LBB7_23
# BB#16:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_15 Depth=1
	movl	8(%rsp), %r15d          # 4-byte Reload
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB7_17:                               # %.lr.ph
                                        #   Parent Loop BB7_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %r14
	testb	$32, 1(%r14)
	je	.LBB7_22
# BB#18:                                #   in Loop: Header=BB7_17 Depth=2
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rsi
	callq	setp_implies
	testl	%eax, %eax
	je	.LBB7_20
# BB#19:                                #   in Loop: Header=BB7_17 Depth=2
	movq	88(%rsp), %rax          # 8-byte Reload
	incl	(%rax)
	xorl	%eax, %eax
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rsi
	movq	%r14, %rdx
	callq	set_or
	movq	64(%rsp), %rax          # 8-byte Reload
	decl	16(%rax)
	movl	(%r14), %eax
	movl	$-10241, %ecx           # imm = 0xD7FF
	andl	%ecx, %eax
	orl	$2048, %eax             # imm = 0x800
	movl	%eax, (%r14)
	jmp	.LBB7_22
.LBB7_20:                               #   in Loop: Header=BB7_17 Depth=2
	movslq	4(%rsp), %r12           # 4-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rcx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	feasibly_covered
	testl	%eax, %eax
	je	.LBB7_22
# BB#21:                                #   in Loop: Header=BB7_17 Depth=2
	movq	%r14, (%rbp,%r12,8)
	incl	%r12d
	movl	%r12d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB7_22:                               #   in Loop: Header=BB7_17 Depth=2
	addq	$8, %rbx
	decq	%r15
	jne	.LBB7_17
.LBB7_23:                               # %._crit_edge
                                        #   in Loop: Header=BB7_15 Depth=1
	testb	$8, debug(%rip)
	je	.LBB7_25
# BB#24:                                #   in Loop: Header=BB7_15 Depth=1
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	printf
.LBB7_25:                               #   in Loop: Header=BB7_15 Depth=1
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB7_40
# BB#26:                                # %.preheader
                                        #   in Loop: Header=BB7_15 Depth=1
	movq	40(%rsp), %rbx          # 8-byte Reload
	jle	.LBB7_36
# BB#27:                                # %.lr.ph150.split.us.preheader
                                        #   in Loop: Header=BB7_15 Depth=1
	movl	4(%rsp), %r13d          # 4-byte Reload
	movl	$9999, 28(%rsp)         # 4-byte Folded Spill
                                        # imm = 0x270F
	xorl	%r14d, %r14d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB7_28
.LBB7_32:                               #   in Loop: Header=BB7_28 Depth=2
	movq	40(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB7_35
	.p2align	4, 0x90
.LBB7_28:                               # %.lr.ph150.split.us
                                        #   Parent Loop BB7_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_29 Depth 3
	movq	(%rbp,%r14,8), %rdi
	xorl	%eax, %eax
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%rbx, %rsi
	callq	set_dist
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r14,8), %rbx
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB7_29:                               #   Parent Loop BB7_15 Depth=1
                                        #     Parent Loop BB7_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp,%r12,8), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	setp_disjoint
	cmpl	$1, %eax
	sbbl	$-1, %r15d
	incq	%r12
	cmpq	%r12, %r13
	jne	.LBB7_29
# BB#30:                                # %._crit_edge144.us
                                        #   in Loop: Header=BB7_28 Depth=2
	cmpl	12(%rsp), %r15d         # 4-byte Folded Reload
	jle	.LBB7_31
# BB#39:                                #   in Loop: Header=BB7_28 Depth=2
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	8(%rsp), %eax           # 4-byte Reload
	jmp	.LBB7_34
	.p2align	4, 0x90
.LBB7_31:                               #   in Loop: Header=BB7_28 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	jge	.LBB7_32
# BB#33:                                #   in Loop: Header=BB7_28 Depth=2
	cmpl	12(%rsp), %r15d         # 4-byte Folded Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	jne	.LBB7_35
.LBB7_34:                               # %.sink.split.us
                                        #   in Loop: Header=BB7_28 Depth=2
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, 72(%rsp)          # 8-byte Spill
.LBB7_35:                               #   in Loop: Header=BB7_28 Depth=2
	incq	%r14
	cmpq	%r12, %r14
	jne	.LBB7_28
	jmp	.LBB7_36
.LBB7_40:
	testq	%rbp, %rbp
	je	.LBB7_42
# BB#41:
	movq	%rbp, %rdi
	callq	free
.LBB7_42:
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB7_44
# BB#43:
	callq	free
.LBB7_44:
	xorl	%eax, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_free                 # TAILCALL
.Lfunc_end7:
	.size	select_feasible, .Lfunc_end7-select_feasible
	.cfi_endproc

	.globl	feasibly_covered
	.p2align	4, 0x90
	.type	feasibly_covered,@function
feasibly_covered:                       # @feasibly_covered
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 64
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rsi, %rcx
	movq	%rdi, %r15
	movq	cube+80(%rip), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	callq	set_or
	movq	%rax, %rbx
	movq	cube+96(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rdi
	callq	set_copy
	movl	(%r15), %r8d
	movl	12(%r15), %ecx
	imull	%r8d, %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	jle	.LBB8_28
# BB#1:                                 # %.lr.ph108.preheader
	movq	24(%r15), %rbp
	movslq	%ecx, %rax
	leaq	(%rbp,%rax,4), %r13
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph108
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_10 Depth 2
                                        #     Child Loop BB8_16 Depth 2
                                        #       Child Loop BB8_19 Depth 3
	testb	$32, 1(%rbp)
	je	.LBB8_26
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movslq	cube+108(%rip), %r9
	cmpq	$-1, %r9
	je	.LBB8_4
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	(%rbx,%r9,4), %ecx
	andl	(%rbp,%r9,4), %ecx
	movl	%ecx, %eax
	shrl	%eax
	orl	%ecx, %eax
	notl	%eax
	andl	cube+104(%rip), %eax
	je	.LBB8_6
# BB#7:                                 #   in Loop: Header=BB8_2 Depth=1
	movzbl	%al, %esi
	movzbl	%ah, %ecx  # NOREX
	movl	bit_count(,%rcx,4), %ecx
	addl	bit_count(,%rsi,4), %ecx
	movl	%eax, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	addl	bit_count(,%rsi,4), %ecx
	shrl	$24, %eax
	addl	bit_count(,%rax,4), %ecx
	cmpl	$1, %ecx
	jle	.LBB8_8
	jmp	.LBB8_26
.LBB8_4:                                #   in Loop: Header=BB8_2 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB8_14
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=1
	xorl	%ecx, %ecx
.LBB8_8:                                # %.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	cmpl	$2, %r9d
	jl	.LBB8_14
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	$1, %esi
	.p2align	4, 0x90
.LBB8_10:                               # %.lr.ph
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rsi,4), %edi
	andl	(%rbp,%rsi,4), %edi
	movl	%edi, %eax
	shrl	%eax
	orl	%edi, %eax
	andl	$1431655765, %eax       # imm = 0x55555555
	cmpl	$1431655765, %eax       # imm = 0x55555555
	je	.LBB8_13
# BB#11:                                #   in Loop: Header=BB8_10 Depth=2
	cmpl	$1, %ecx
	je	.LBB8_26
# BB#12:                                #   in Loop: Header=BB8_10 Depth=2
	xorl	$1431655765, %eax       # imm = 0x55555555
	movl	%eax, %r10d
	andl	$85, %r10d
	movl	%eax, %edx
	shrl	$8, %edx
	andl	$85, %edx
	movl	%eax, %edi
	shrl	$16, %edi
	andl	$85, %edi
	shrl	$24, %eax
	addl	bit_count(,%r10,4), %ecx
	addl	bit_count(,%rdx,4), %ecx
	addl	bit_count(,%rdi,4), %ecx
	addl	bit_count(,%rax,4), %ecx
	cmpl	$1, %ecx
	jg	.LBB8_26
.LBB8_13:                               #   in Loop: Header=BB8_10 Depth=2
	incq	%rsi
	cmpq	%r9, %rsi
	jl	.LBB8_10
	.p2align	4, 0x90
.LBB8_14:                               # %.loopexit88
                                        #   in Loop: Header=BB8_2 Depth=1
	movslq	cube+8(%rip), %rdx
	movslq	cube+4(%rip), %rsi
	cmpl	%esi, %edx
	jge	.LBB8_23
# BB#15:                                # %.lr.ph100
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	cube+72(%rip), %r9
	movq	cube+48(%rip), %r10
	movq	cube+40(%rip), %r11
	.p2align	4, 0x90
.LBB8_16:                               #   Parent Loop BB8_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_19 Depth 3
	movslq	(%r10,%rdx,4), %rax
	movslq	(%r11,%rdx,4), %rdi
	cmpl	%eax, %edi
	jg	.LBB8_20
# BB#17:                                # %.lr.ph95.preheader
                                        #   in Loop: Header=BB8_16 Depth=2
	movq	(%r9,%rdx,8), %r12
	decq	%rdi
	.p2align	4, 0x90
.LBB8_19:                               # %.lr.ph95
                                        #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	4(%rbx,%rdi,4), %r14d
	andl	4(%rbp,%rdi,4), %r14d
	testl	4(%r12,%rdi,4), %r14d
	jne	.LBB8_22
# BB#18:                                #   in Loop: Header=BB8_19 Depth=3
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB8_19
.LBB8_20:                               # %._crit_edge
                                        #   in Loop: Header=BB8_16 Depth=2
	testl	%ecx, %ecx
	jg	.LBB8_26
# BB#21:                                #   in Loop: Header=BB8_16 Depth=2
	incl	%ecx
.LBB8_22:                               # %.loopexit
                                        #   in Loop: Header=BB8_16 Depth=2
	incq	%rdx
	cmpq	%rsi, %rdx
	jl	.LBB8_16
.LBB8_23:                               # %._crit_edge101
                                        #   in Loop: Header=BB8_2 Depth=1
	testl	%ecx, %ecx
	je	.LBB8_24
# BB#25:                                #   in Loop: Header=BB8_2 Depth=1
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	force_lower
	movl	(%r15), %r8d
.LBB8_26:                               # %.thread
                                        #   in Loop: Header=BB8_2 Depth=1
	movslq	%r8d, %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%r13, %rbp
	jb	.LBB8_2
# BB#27:
	movl	$1, %eax
	jmp	.LBB8_28
.LBB8_24:
	xorl	%eax, %eax
.LBB8_28:                               # %._crit_edge109
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	feasibly_covered, .Lfunc_end8-feasibly_covered
	.cfi_endproc

	.globl	mincov
	.p2align	4, 0x90
	.type	mincov,@function
mincov:                                 # @mincov
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi114:
	.cfi_def_cfa_offset 96
.Lcfi115:
	.cfi_offset %rbx, -56
.Lcfi116:
	.cfi_offset %r12, -48
.Lcfi117:
	.cfi_offset %r13, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	cube+80(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	16(%r13), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	movl	(%r13), %eax
	movl	12(%r13), %ecx
	imull	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB9_5
# BB#1:                                 # %.lr.ph92
	movq	24(%r13), %rbp
	movslq	%ecx, %rcx
	leaq	(%rbp,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	testb	$32, 1(%rbp)
	je	.LBB9_4
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	24(%r15), %rax
	movl	12(%r15), %ecx
	leal	1(%rcx), %edx
	imull	(%r15), %ecx
	movl	%edx, 12(%r15)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	movq	cube+96(%rip), %rsi
	xorl	%eax, %eax
	callq	set_copy
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	force_lower
	movl	(%r13), %eax
.LBB9_4:                                #   in Loop: Header=BB9_2 Depth=1
	movslq	%eax, %rcx
	leaq	(%rbp,%rcx,4), %rbp
	cmpq	%rbx, %rbp
	jb	.LBB9_2
.LBB9_5:                                # %._crit_edge93
	movslq	(%r15), %rcx
	movslq	12(%r15), %rax
	imulq	%rcx, %rax
	movl	cube+8(%rip), %esi
	testl	%eax, %eax
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r12, 8(%rsp)           # 8-byte Spill
	jle	.LBB9_14
# BB#6:                                 # %.preheader.preheader
	movq	24(%r15), %rbp
	leaq	(%rbp,%rax,4), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	cube+4(%rip), %eax
	xorl	%r14d, %r14d
.LBB9_7:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_9 Depth 2
	movl	$1, %r12d
	cmpl	%eax, %esi
	jge	.LBB9_12
# BB#8:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_7 Depth=1
	movslq	%esi, %rbx
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB9_9:                                # %.lr.ph
                                        #   Parent Loop BB9_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cube+72(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	set_dist
	cmpl	$2, %eax
	jl	.LBB9_11
# BB#10:                                #   in Loop: Header=BB9_9 Depth=2
	imull	%eax, %r12d
	cmpl	$500, %r12d             # imm = 0x1F4
	jg	.LBB9_18
.LBB9_11:                               #   in Loop: Header=BB9_9 Depth=2
	incq	%rbx
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB9_9
.LBB9_12:                               # %._crit_edge
                                        #   in Loop: Header=BB9_7 Depth=1
	addl	%r12d, %r14d
	cmpl	$500, %r14d             # imm = 0x1F4
	jg	.LBB9_18
# BB#13:                                #   in Loop: Header=BB9_7 Depth=1
	movslq	(%r15), %rcx
	leaq	(%rbp,%rcx,4), %rbp
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	movl	cube+8(%rip), %esi
	jb	.LBB9_7
.LBB9_14:                               # %._crit_edge88
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	unravel
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	do_sm_minimum_cover
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	set_diff
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	callq	set_or
	movq	cube+96(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	set_copy
	movl	$0, 16(%r13)
	testb	$8, debug(%rip)
	je	.LBB9_16
# BB#15:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pc1
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	pc2
	movq	%rax, %rcx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%rcx, %rdx
	callq	printf
.LBB9_16:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	testq	%r12, %r12
	je	.LBB9_19
# BB#17:
	movq	%r12, %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB9_18:                               # %.loopexit
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	xorl	%edi, %edi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	callq	most_frequent
	movl	$1, %r14d
	movl	%eax, %ecx
	shll	%cl, %r14d
	xorl	%edi, %edi
	movq	%rbp, %rsi
	callq	most_frequent
	sarl	$5, %eax
	cltq
	movq	8(%rsp), %rbx           # 8-byte Reload
	orl	%r14d, 4(%rbx,%rax,4)
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	set_diff
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	essen_parts             # TAILCALL
.LBB9_19:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	mincov, .Lfunc_end9-mincov
	.cfi_endproc

	.globl	find_all_primes
	.p2align	4, 0x90
	.type	find_all_primes,@function
find_all_primes:                        # @find_all_primes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi124:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi127:
	.cfi_def_cfa_offset 112
.Lcfi128:
	.cfi_offset %rbx, -56
.Lcfi129:
	.cfi_offset %r12, -48
.Lcfi130:
	.cfi_offset %r13, -40
.Lcfi131:
	.cfi_offset %r14, -32
.Lcfi132:
	.cfi_offset %r15, -24
.Lcfi133:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	16(%r15), %edi
	movl	cube(%rip), %esi
	testl	%edi, %edi
	je	.LBB10_1
# BB#2:
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r12
	movl	(%r15), %eax
	movl	12(%r15), %ecx
	imull	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB10_7
# BB#3:                                 # %.lr.ph76
	movq	24(%r15), %rbx
	movslq	%ecx, %rcx
	leaq	(%rbx,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB10_4:                               # =>This Inner Loop Header: Depth=1
	testb	$32, 1(%rbx)
	je	.LBB10_6
# BB#5:                                 #   in Loop: Header=BB10_4 Depth=1
	movq	24(%r12), %rax
	movl	12(%r12), %ecx
	leal	1(%rcx), %edx
	imull	(%r12), %ecx
	movl	%edx, 12(%r12)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	movq	cube+96(%rip), %rsi
	xorl	%eax, %eax
	callq	set_copy
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	force_lower
	movl	(%r15), %eax
.LBB10_6:                               #   in Loop: Header=BB10_4 Depth=1
	movslq	%eax, %rcx
	leaq	(%rbx,%rcx,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB10_4
.LBB10_7:                               # %._crit_edge77
	movl	cube+8(%rip), %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	unravel
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_rev_contain
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rcx, %rdi
	callq	exact_minimum_cover
	movq	%rax, %r15
	movslq	(%r15), %rax
	movslq	12(%r15), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB10_38
# BB#8:                                 # %.lr.ph.preheader
	movq	24(%r15), %rax
	leaq	(%rax,%rcx,4), %r11
	leaq	-4(%r14), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	4(%r14), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	-4(%r13), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	4(%r13), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	-12(%r13), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	-12(%r14), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$-1024, %r9d            # imm = 0xFC00
	movl	$1, %r10d
	movl	$2, %edi
	.p2align	4, 0x90
.LBB10_9:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_18 Depth 2
                                        #     Child Loop BB10_22 Depth 2
                                        #     Child Loop BB10_32 Depth 2
                                        #     Child Loop BB10_36 Depth 2
	movl	(%r13), %r8d
	movl	(%rax), %ecx
	andl	%r9d, %ecx
	andl	$1023, %r8d             # imm = 0x3FF
	movq	%r8, %rbp
	cmoveq	%r10, %rbp
	movl	%r8d, %edx
	orl	%ecx, %edx
	movl	%edx, (%rax)
	cmpq	$8, %rbp
	jb	.LBB10_21
# BB#10:                                # %min.iters.checked106
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	%rbp, %r12
	andq	$1016, %r12             # imm = 0x3F8
	je	.LBB10_21
# BB#11:                                # %vector.memcheck128
                                        #   in Loop: Header=BB10_9 Depth=1
	leaq	1(%r8), %rcx
	testl	%r8d, %r8d
	cmovneq	%rdi, %rcx
	leaq	-4(%rax,%rcx,4), %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r8,4), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB10_13
# BB#12:                                # %vector.memcheck128
                                        #   in Loop: Header=BB10_9 Depth=1
	leaq	4(%rax,%r8,4), %rdx
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB10_21
.LBB10_13:                              # %vector.body101.preheader
                                        #   in Loop: Header=BB10_9 Depth=1
	leaq	-8(%r12), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB10_14
# BB#15:                                # %vector.body101.prol
                                        #   in Loop: Header=BB10_9 Depth=1
	movups	-12(%r13,%r8,4), %xmm0
	movups	-28(%r13,%r8,4), %xmm1
	movups	-12(%rax,%r8,4), %xmm2
	movups	-28(%rax,%r8,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -12(%rax,%r8,4)
	movups	%xmm3, -28(%rax,%r8,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB10_17
	jmp	.LBB10_19
.LBB10_14:                              #   in Loop: Header=BB10_9 Depth=1
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB10_19
.LBB10_17:                              # %vector.body101.preheader.new
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	%rbp, %rdx
	andq	$-8, %rdx
	negq	%rdx
	negq	%rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r8,4), %rdi
	leaq	-12(%rax,%r8,4), %rcx
	.p2align	4, 0x90
.LBB10_18:                              # %vector.body101
                                        #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rsi,4), %xmm0
	movups	-16(%rdi,%rsi,4), %xmm1
	movups	(%rcx,%rsi,4), %xmm2
	movups	-16(%rcx,%rsi,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%rsi,4)
	movups	%xmm3, -16(%rcx,%rsi,4)
	movups	-32(%rdi,%rsi,4), %xmm0
	movups	-48(%rdi,%rsi,4), %xmm1
	movups	-32(%rcx,%rsi,4), %xmm2
	movups	-48(%rcx,%rsi,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -32(%rcx,%rsi,4)
	movups	%xmm3, -48(%rcx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB10_18
.LBB10_19:                              # %middle.block102
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpq	%r12, %rbp
	movl	$2, %edi
	je	.LBB10_23
# BB#20:                                #   in Loop: Header=BB10_9 Depth=1
	subq	%r12, %r8
	.p2align	4, 0x90
.LBB10_21:                              # %scalar.ph103.preheader
                                        #   in Loop: Header=BB10_9 Depth=1
	incq	%r8
	.p2align	4, 0x90
.LBB10_22:                              # %scalar.ph103
                                        #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%r8,4), %ecx
	notl	%ecx
	andl	-4(%r13,%r8,4), %ecx
	movl	%ecx, -4(%rax,%r8,4)
	decq	%r8
	cmpq	$1, %r8
	jg	.LBB10_22
.LBB10_23:                              # %.loopexit150
                                        #   in Loop: Header=BB10_9 Depth=1
	movl	(%rax), %ecx
	movl	%ecx, %ebp
	andl	$1023, %ebp             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movl	$1, %ebx
	cmovneq	%rbp, %rbx
	cmpq	$8, %rbx
	jb	.LBB10_35
# BB#24:                                # %min.iters.checked
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	%rbx, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB10_35
# BB#25:                                # %vector.memcheck
                                        #   in Loop: Header=BB10_9 Depth=1
	leaq	1(%rbp), %rcx
	testl	%ebp, %ebp
	cmovneq	%rdi, %rcx
	leaq	-4(%rax,%rcx,4), %rdx
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rbp,4), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB10_27
# BB#26:                                # %vector.memcheck
                                        #   in Loop: Header=BB10_9 Depth=1
	leaq	4(%rax,%rbp,4), %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB10_35
.LBB10_27:                              # %vector.body.preheader
                                        #   in Loop: Header=BB10_9 Depth=1
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB10_28
# BB#29:                                # %vector.body.prol
                                        #   in Loop: Header=BB10_9 Depth=1
	movups	-12(%rax,%rbp,4), %xmm0
	movups	-28(%rax,%rbp,4), %xmm1
	movups	-12(%r14,%rbp,4), %xmm2
	movups	-28(%r14,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rax,%rbp,4)
	movups	%xmm3, -28(%rax,%rbp,4)
	movl	$8, %edx
	testq	%rcx, %rcx
	jne	.LBB10_31
	jmp	.LBB10_33
.LBB10_28:                              #   in Loop: Header=BB10_9 Depth=1
	xorl	%edx, %edx
	testq	%rcx, %rcx
	je	.LBB10_33
.LBB10_31:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	%rbx, %rsi
	andq	$-8, %rsi
	negq	%rsi
	negq	%rdx
	leaq	-12(%rax,%rbp,4), %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(%rdi,%rbp,4), %rdi
	.p2align	4, 0x90
.LBB10_32:                              # %vector.body
                                        #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rcx,%rdx,4), %xmm0
	movups	-16(%rcx,%rdx,4), %xmm1
	movups	(%rdi,%rdx,4), %xmm2
	movups	-16(%rdi,%rdx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%rdx,4)
	movups	%xmm3, -16(%rcx,%rdx,4)
	movups	-32(%rcx,%rdx,4), %xmm0
	movups	-48(%rcx,%rdx,4), %xmm1
	movups	-32(%rdi,%rdx,4), %xmm2
	movups	-48(%rdi,%rdx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rcx,%rdx,4)
	movups	%xmm3, -48(%rcx,%rdx,4)
	addq	$-16, %rdx
	cmpq	%rdx, %rsi
	jne	.LBB10_32
.LBB10_33:                              # %middle.block
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpq	%r8, %rbx
	movl	$2, %edi
	je	.LBB10_37
# BB#34:                                #   in Loop: Header=BB10_9 Depth=1
	subq	%r8, %rbp
	.p2align	4, 0x90
.LBB10_35:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB10_9 Depth=1
	incq	%rbp
	.p2align	4, 0x90
.LBB10_36:                              # %scalar.ph
                                        #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r14,%rbp,4), %ecx
	orl	%ecx, -4(%rax,%rbp,4)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB10_36
.LBB10_37:                              # %.loopexit
                                        #   in Loop: Header=BB10_9 Depth=1
	orb	$-128, 1(%rax)
	movslq	(%r15), %rcx
	leaq	(%rax,%rcx,4), %rax
	cmpq	%r11, %rax
	jb	.LBB10_9
.LBB10_38:                              # %._crit_edge
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	sf_free
	jmp	.LBB10_39
.LBB10_1:
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	movq	24(%r15), %rbx
	movl	12(%r15), %eax
	leal	1(%rax), %ecx
	imull	(%r15), %eax
	movl	%ecx, 12(%r15)
	movslq	%eax, %rbp
	leaq	(%rbx,%rbp,4), %rdi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	set_copy
	orb	$-128, 1(%rbx,%rbp,4)
.LBB10_39:
	movq	%r15, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	find_all_primes, .Lfunc_end10-find_all_primes
	.cfi_endproc

	.globl	all_primes
	.p2align	4, 0x90
	.type	all_primes,@function
all_primes:                             # @all_primes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi137:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi138:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi140:
	.cfi_def_cfa_offset 64
.Lcfi141:
	.cfi_offset %rbx, -56
.Lcfi142:
	.cfi_offset %r12, -48
.Lcfi143:
	.cfi_offset %r13, -40
.Lcfi144:
	.cfi_offset %r14, -32
.Lcfi145:
	.cfi_offset %r15, -24
.Lcfi146:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB11_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB11_3
.LBB11_2:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB11_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r15
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB11_5
# BB#4:
	movl	$8, %edi
	jmp	.LBB11_6
.LBB11_5:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB11_6:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r12
	movl	12(%r14), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r13
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB11_15
# BB#7:                                 # %.lr.ph
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	24(%r14), %rbp
	leaq	(%rbp,%rax,4), %r14
	.p2align	4, 0x90
.LBB11_8:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_12 Depth 2
	cmpw	$0, (%rbp)
	js	.LBB11_9
# BB#10:                                #   in Loop: Header=BB11_8 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	set_diff
	movslq	12(%rbx), %rax
	movl	%eax, 16(%rbx)
	movslq	(%rbx), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB11_13
# BB#11:                                # %.lr.ph33.i.preheader
                                        #   in Loop: Header=BB11_8 Depth=1
	movq	24(%rbx), %rax
	leaq	(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB11_12:                              # %.lr.ph33.i
                                        #   Parent Loop BB11_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	orb	$32, 1(%rax)
	movslq	(%rbx), %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB11_12
.LBB11_13:                              # %setup_BB_CC.exit
                                        #   in Loop: Header=BB11_8 Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	essen_parts
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	find_all_primes
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rcx, %rsi
	callq	sf_append
	jmp	.LBB11_14
	.p2align	4, 0x90
.LBB11_9:                               #   in Loop: Header=BB11_8 Depth=1
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	sf_addset
.LBB11_14:                              #   in Loop: Header=BB11_8 Depth=1
	movq	%rax, %r13
	movq	(%rsp), %rax            # 8-byte Reload
	movslq	(%rax), %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%r14, %rbp
	jb	.LBB11_8
.LBB11_15:                              # %._crit_edge
	testq	%r12, %r12
	je	.LBB11_17
# BB#16:
	movq	%r12, %rdi
	callq	free
.LBB11_17:
	testq	%r15, %r15
	je	.LBB11_19
# BB#18:
	movq	%r15, %rdi
	callq	free
.LBB11_19:
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	all_primes, .Lfunc_end11-all_primes
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"EXPAND: %s (covered %d)\n"
	.size	.L.str, 25

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\nEXPAND1:    \t%s\n"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ON-set and OFF-set are not orthogonal"
	.size	.L.str.2, 38

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"ESSEN_PARTS:\tRAISE=%s FREESET=%s\n"
	.size	.L.str.3, 34

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"ESSEN_RAISING:\tRAISE=%s FREESET=%s\n"
	.size	.L.str.4, 36

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"MOST_FREQUENT:\tbest=%d FREESET=%s\n"
	.size	.L.str.5, 35

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"SELECT_FEASIBLE: started with %d pfcc, ended with %d fcc\n"
	.size	.L.str.6, 58

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"FEASIBLE:  \tRAISE=%s FREESET=%s\n"
	.size	.L.str.7, 33

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"MINCOV:    \tRAISE=%s FREESET=%s\n"
	.size	.L.str.8, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
