	.text
	.file	"btContactConstraint.bc"
	.globl	_ZN19btContactConstraintC2Ev
	.p2align	4, 0x90
	.type	_ZN19btContactConstraintC2Ev,@function
_ZN19btContactConstraintC2Ev:           # @_ZN19btContactConstraintC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$8, %esi
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintType
	movq	$_ZTV19btContactConstraint+16, (%rbx)
	addq	$96, %rbx
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZN20btPersistentManifoldC1Ev # TAILCALL
.Lfunc_end0:
	.size	_ZN19btContactConstraintC2Ev, .Lfunc_end0-_ZN19btContactConstraintC2Ev
	.cfi_endproc

	.globl	_ZN19btContactConstraintC2EP20btPersistentManifoldR11btRigidBodyS3_
	.p2align	4, 0x90
	.type	_ZN19btContactConstraintC2EP20btPersistentManifoldR11btRigidBodyS3_,@function
_ZN19btContactConstraintC2EP20btPersistentManifoldR11btRigidBodyS3_: # @_ZN19btContactConstraintC2EP20btPersistentManifoldR11btRigidBodyS3_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$8, %esi
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	movq	$_ZTV19btContactConstraint+16, (%rbx)
	addq	$96, %rbx
	movl	$744, %edx              # imm = 0x2E8
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	memcpy                  # TAILCALL
.Lfunc_end1:
	.size	_ZN19btContactConstraintC2EP20btPersistentManifoldR11btRigidBodyS3_, .Lfunc_end1-_ZN19btContactConstraintC2EP20btPersistentManifoldR11btRigidBodyS3_
	.cfi_endproc

	.globl	_ZN19btContactConstraintD2Ev
	.p2align	4, 0x90
	.type	_ZN19btContactConstraintD2Ev,@function
_ZN19btContactConstraintD2Ev:           # @_ZN19btContactConstraintD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	_ZN19btContactConstraintD2Ev, .Lfunc_end2-_ZN19btContactConstraintD2Ev
	.cfi_endproc

	.globl	_ZN19btContactConstraintD0Ev
	.p2align	4, 0x90
	.type	_ZN19btContactConstraintD0Ev,@function
_ZN19btContactConstraintD0Ev:           # @_ZN19btContactConstraintD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end3:
	.size	_ZN19btContactConstraintD0Ev, .Lfunc_end3-_ZN19btContactConstraintD0Ev
	.cfi_endproc

	.globl	_ZN19btContactConstraint18setContactManifoldEP20btPersistentManifold
	.p2align	4, 0x90
	.type	_ZN19btContactConstraint18setContactManifoldEP20btPersistentManifold,@function
_ZN19btContactConstraint18setContactManifoldEP20btPersistentManifold: # @_ZN19btContactConstraint18setContactManifoldEP20btPersistentManifold
	.cfi_startproc
# BB#0:
	addq	$96, %rdi
	movl	$744, %edx              # imm = 0x2E8
	jmp	memcpy                  # TAILCALL
.Lfunc_end4:
	.size	_ZN19btContactConstraint18setContactManifoldEP20btPersistentManifold, .Lfunc_end4-_ZN19btContactConstraint18setContactManifoldEP20btPersistentManifold
	.cfi_endproc

	.globl	_ZN19btContactConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN19btContactConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN19btContactConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E: # @_ZN19btContactConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZN19btContactConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end5-_ZN19btContactConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.globl	_ZN19btContactConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.p2align	4, 0x90
	.type	_ZN19btContactConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E,@function
_ZN19btContactConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E: # @_ZN19btContactConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end6:
	.size	_ZN19btContactConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E, .Lfunc_end6-_ZN19btContactConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_endproc

	.globl	_ZN19btContactConstraint13buildJacobianEv
	.p2align	4, 0x90
	.type	_ZN19btContactConstraint13buildJacobianEv,@function
_ZN19btContactConstraint13buildJacobianEv: # @_ZN19btContactConstraint13buildJacobianEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	_ZN19btContactConstraint13buildJacobianEv, .Lfunc_end7-_ZN19btContactConstraint13buildJacobianEv
	.cfi_endproc

	.globl	_ZN19btContactConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.p2align	4, 0x90
	.type	_ZN19btContactConstraint23solveConstraintObsoleteER12btSolverBodyS1_f,@function
_ZN19btContactConstraint23solveConstraintObsoleteER12btSolverBodyS1_f: # @_ZN19btContactConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	_ZN19btContactConstraint23solveConstraintObsoleteER12btSolverBodyS1_f, .Lfunc_end8-_ZN19btContactConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1066192077              # float 1.10000002
.LCPI9_1:
	.long	1065353216              # float 1
.LCPI9_2:
	.long	3192704205              # float -0.200000003
	.text
	.globl	_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff
	.p2align	4, 0x90
	.type	_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff,@function
_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff: # @_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 32
	subq	$256, %rsp              # imm = 0x100
.Lcfi10:
	.cfi_def_cfa_offset 288
.Lcfi11:
	.cfi_offset %rbx, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rdx, %rbx
	movq	%rdi, %rax
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI9_0(%rip), %xmm0
	jbe	.LBB9_2
# BB#1:
	movl	$0, (%r14)
	jmp	.LBB9_3
.LBB9_2:
	movsd	(%rsi), %xmm3           # xmm3 = mem[0],zero
	movsd	56(%rax), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm0, %xmm3
	movss	8(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	subss	64(%rax), %xmm4
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, %xmm7
	xorps	%xmm5, %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm3, 128(%rsp)
	movlps	%xmm0, 136(%rsp)
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	movsd	56(%rbx), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm0, %xmm1
	movss	8(%rcx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	64(%rbx), %xmm2
	movaps	%xmm1, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1,2,3]
	movlps	%xmm1, 112(%rsp)
	movlps	%xmm5, 120(%rsp)
	movss	348(%rax), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movss	352(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm5
	mulss	%xmm3, %xmm6
	mulss	%xmm8, %xmm3
	mulss	%xmm4, %xmm8
	mulss	%xmm7, %xmm5
	subss	%xmm5, %xmm8
	movss	344(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm4
	subss	%xmm4, %xmm6
	mulss	%xmm5, %xmm7
	subss	%xmm3, %xmm7
	addss	328(%rax), %xmm8
	addss	332(%rax), %xmm6
	addss	336(%rax), %xmm7
	movss	348(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	352(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm1, %xmm4
	mulss	%xmm3, %xmm1
	mulss	%xmm2, %xmm3
	mulss	%xmm0, %xmm5
	subss	%xmm5, %xmm3
	movss	344(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	subss	%xmm2, %xmm4
	mulss	%xmm5, %xmm0
	subss	%xmm1, %xmm0
	addss	328(%rbx), %xmm3
	addss	332(%rbx), %xmm4
	addss	336(%rbx), %xmm0
	subss	%xmm3, %xmm8
	movss	%xmm8, 8(%rsp)          # 4-byte Spill
	subss	%xmm4, %xmm6
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	subss	%xmm0, %xmm7
	movaps	%xmm7, 144(%rsp)        # 16-byte Spill
	movl	8(%rax), %ecx
	movl	%ecx, 64(%rsp)
	movl	24(%rax), %ecx
	movl	%ecx, 68(%rsp)
	movl	40(%rax), %ecx
	movl	%ecx, 72(%rsp)
	movl	$0, 76(%rsp)
	movl	12(%rax), %ecx
	movl	%ecx, 80(%rsp)
	movl	28(%rax), %ecx
	movl	%ecx, 84(%rsp)
	movl	44(%rax), %ecx
	movl	%ecx, 88(%rsp)
	movl	$0, 92(%rsp)
	movl	16(%rax), %ecx
	movl	%ecx, 96(%rsp)
	movl	32(%rax), %ecx
	movl	%ecx, 100(%rsp)
	movl	48(%rax), %ecx
	movl	%ecx, 104(%rsp)
	movl	$0, 108(%rsp)
	movl	8(%rbx), %ecx
	movl	%ecx, 16(%rsp)
	movl	24(%rbx), %ecx
	movl	%ecx, 20(%rsp)
	movl	40(%rbx), %ecx
	movl	%ecx, 24(%rsp)
	movl	$0, 28(%rsp)
	movl	12(%rbx), %ecx
	movl	%ecx, 32(%rsp)
	movl	28(%rbx), %ecx
	movl	%ecx, 36(%rsp)
	movl	44(%rbx), %ecx
	movl	%ecx, 40(%rsp)
	movl	$0, 44(%rsp)
	movl	16(%rbx), %ecx
	movl	%ecx, 48(%rsp)
	movl	32(%rbx), %ecx
	movl	%ecx, 52(%rsp)
	movl	48(%rbx), %ecx
	movl	%ecx, 56(%rsp)
	movl	$0, 60(%rsp)
	movss	360(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addq	$428, %rax              # imm = 0x1AC
	movss	360(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addq	$428, %rbx              # imm = 0x1AC
	leaq	168(%rsp), %rdi
	leaq	64(%rsp), %rsi
	leaq	16(%rsp), %rdx
	leaq	128(%rsp), %rcx
	leaq	112(%rsp), %r8
	movq	%r15, %r9
	pushq	%rbx
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	callq	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	addq	$16, %rsp
.Lcfi16:
	.cfi_adjust_cfa_offset -16
	movss	.LCPI9_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	248(%rsp), %xmm0
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	(%r15), %xmm2
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	4(%r15), %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm1, %xmm2
	movaps	144(%rsp), %xmm1        # 16-byte Reload
	mulss	8(%r15), %xmm1
	addss	%xmm2, %xmm1
	mulss	.LCPI9_2(%rip), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%r14)
.LBB9_3:
	addq	$256, %rsp              # imm = 0x100
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff, .Lfunc_end9-_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff
	.cfi_endproc

	.section	.text._ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,"axG",@progbits,_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,comdat
	.weak	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.p2align	4, 0x90
	.type	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,@function
_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f: # @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_startproc
# BB#0:
	movq	16(%rsp), %r10
	movq	8(%rsp), %rax
	movups	(%r9), %xmm2
	movups	%xmm2, (%rdi)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm11, %xmm7
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm9, %xmm5
	movaps	%xmm11, %xmm4
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm5
	mulss	%xmm10, %xmm2
	mulss	%xmm9, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1]
	mulps	%xmm4, %xmm6
	movss	20(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm6, %xmm4
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm12   # xmm12 = xmm12[0],xmm8[0],xmm12[1],xmm8[1]
	mulps	%xmm3, %xmm12
	addps	%xmm4, %xmm12
	mulss	32(%rsi), %xmm7
	mulss	36(%rsi), %xmm5
	addss	%xmm7, %xmm5
	mulss	40(%rsi), %xmm2
	addss	%xmm5, %xmm2
	xorps	%xmm8, %xmm8
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm12, 16(%rdi)
	movlps	%xmm3, 24(%rdi)
	movss	(%r8), %xmm3            # xmm3 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm11, %xmm5
	movss	8(%r8), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm6
	mulss	%xmm10, %xmm6
	subss	%xmm5, %xmm6
	mulss	%xmm9, %xmm7
	mulss	%xmm3, %xmm11
	subss	%xmm7, %xmm11
	mulss	%xmm10, %xmm3
	mulss	%xmm9, %xmm4
	subss	%xmm3, %xmm4
	movss	16(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	movss	20(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0],xmm9[1],xmm5[1]
	movaps	%xmm11, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm9, %xmm7
	addps	%xmm3, %xmm7
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rdx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1]
	mulps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	mulss	32(%rdx), %xmm6
	mulss	36(%rdx), %xmm11
	addss	%xmm6, %xmm11
	mulss	40(%rdx), %xmm4
	addss	%xmm11, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm5, 32(%rdi)
	movlps	%xmm3, 40(%rdi)
	movsd	(%rax), %xmm9           # xmm9 = mem[0],zero
	mulps	%xmm12, %xmm9
	mulss	8(%rax), %xmm2
	xorps	%xmm6, %xmm6
	movss	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1,2,3]
	movlps	%xmm9, 48(%rdi)
	movlps	%xmm6, 56(%rdi)
	movsd	(%r10), %xmm6           # xmm6 = mem[0],zero
	mulps	%xmm5, %xmm6
	mulss	8(%r10), %xmm4
	movaps	%xmm6, %xmm7
	movlps	%xmm6, 64(%rdi)
	mulss	%xmm5, %xmm6
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movss	%xmm4, %xmm8            # xmm8 = xmm4[0],xmm8[1,2,3]
	movlps	%xmm8, 72(%rdi)
	movss	16(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	20(%rdi), %xmm9
	addss	%xmm3, %xmm9
	mulss	24(%rdi), %xmm2
	addss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm5, %xmm7
	addss	%xmm6, %xmm7
	mulss	40(%rdi), %xmm4
	addss	%xmm7, %xmm4
	addss	%xmm2, %xmm4
	movss	%xmm4, 80(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f, .Lfunc_end10-_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_endproc

	.section	.text._ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,"axG",@progbits,_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,comdat
	.weak	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,@function
_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif: # @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif, .Lfunc_end11-_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_endproc

	.type	_ZTV19btContactConstraint,@object # @_ZTV19btContactConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTV19btContactConstraint
	.p2align	3
_ZTV19btContactConstraint:
	.quad	0
	.quad	_ZTI19btContactConstraint
	.quad	_ZN19btContactConstraintD2Ev
	.quad	_ZN19btContactConstraintD0Ev
	.quad	_ZN19btContactConstraint13buildJacobianEv
	.quad	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.quad	_ZN19btContactConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.quad	_ZN19btContactConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.quad	_ZN19btContactConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.size	_ZTV19btContactConstraint, 72

	.type	_ZTS19btContactConstraint,@object # @_ZTS19btContactConstraint
	.globl	_ZTS19btContactConstraint
	.p2align	4
_ZTS19btContactConstraint:
	.asciz	"19btContactConstraint"
	.size	_ZTS19btContactConstraint, 22

	.type	_ZTS17btTypedConstraint,@object # @_ZTS17btTypedConstraint
	.section	.rodata._ZTS17btTypedConstraint,"aG",@progbits,_ZTS17btTypedConstraint,comdat
	.weak	_ZTS17btTypedConstraint
	.p2align	4
_ZTS17btTypedConstraint:
	.asciz	"17btTypedConstraint"
	.size	_ZTS17btTypedConstraint, 20

	.type	_ZTS13btTypedObject,@object # @_ZTS13btTypedObject
	.section	.rodata._ZTS13btTypedObject,"aG",@progbits,_ZTS13btTypedObject,comdat
	.weak	_ZTS13btTypedObject
_ZTS13btTypedObject:
	.asciz	"13btTypedObject"
	.size	_ZTS13btTypedObject, 16

	.type	_ZTI13btTypedObject,@object # @_ZTI13btTypedObject
	.section	.rodata._ZTI13btTypedObject,"aG",@progbits,_ZTI13btTypedObject,comdat
	.weak	_ZTI13btTypedObject
	.p2align	3
_ZTI13btTypedObject:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13btTypedObject
	.size	_ZTI13btTypedObject, 16

	.type	_ZTI17btTypedConstraint,@object # @_ZTI17btTypedConstraint
	.section	.rodata._ZTI17btTypedConstraint,"aG",@progbits,_ZTI17btTypedConstraint,comdat
	.weak	_ZTI17btTypedConstraint
	.p2align	4
_ZTI17btTypedConstraint:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS17btTypedConstraint
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	_ZTI13btTypedObject
	.quad	2050                    # 0x802
	.size	_ZTI17btTypedConstraint, 40

	.type	_ZTI19btContactConstraint,@object # @_ZTI19btContactConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTI19btContactConstraint
	.p2align	4
_ZTI19btContactConstraint:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19btContactConstraint
	.quad	_ZTI17btTypedConstraint
	.size	_ZTI19btContactConstraint, 24


	.globl	_ZN19btContactConstraintC1Ev
	.type	_ZN19btContactConstraintC1Ev,@function
_ZN19btContactConstraintC1Ev = _ZN19btContactConstraintC2Ev
	.globl	_ZN19btContactConstraintC1EP20btPersistentManifoldR11btRigidBodyS3_
	.type	_ZN19btContactConstraintC1EP20btPersistentManifoldR11btRigidBodyS3_,@function
_ZN19btContactConstraintC1EP20btPersistentManifoldR11btRigidBodyS3_ = _ZN19btContactConstraintC2EP20btPersistentManifoldR11btRigidBodyS3_
	.globl	_ZN19btContactConstraintD1Ev
	.type	_ZN19btContactConstraintD1Ev,@function
_ZN19btContactConstraintD1Ev = _ZN19btContactConstraintD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
