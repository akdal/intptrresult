	.text
	.file	"make_dparser.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 64
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$arg_state, %edi
	callq	process_args
	cmpl	$1, arg_state+8(%rip)
	jne	.LBB0_1
.LBB0_2:
	movq	arg_state(%rip), %rax
	movq	(%rax), %r14
	movq	%r14, %rdi
	callq	new_D_Grammar
	movq	%rax, %rbx
	movl	set_op_priority_from_rule(%rip), %eax
	movl	%eax, 248(%rbx)
	movl	right_recursive_BNF(%rip), %eax
	movl	%eax, 252(%rbx)
	movl	states_for_whitespace(%rip), %eax
	movl	%eax, 256(%rbx)
	movl	states_for_all_nterms(%rip), %eax
	movl	%eax, 260(%rbx)
	movl	tokenizer(%rip), %eax
	movl	%eax, 264(%rbx)
	movl	longest_match(%rip), %eax
	movl	%eax, 268(%rbx)
	leaq	276(%rbx), %rdi
	movl	$grammar_ident, %esi
	callq	strcpy
	movl	scanner_blocks(%rip), %eax
	movl	%eax, 532(%rbx)
	movl	$0, 536(%rbx)
	movl	write_line_directives(%rip), %eax
	movl	%eax, 540(%rbx)
	movl	write_header(%rip), %eax
	movl	%eax, 544(%rbx)
	movl	token_type(%rip), %eax
	movl	%eax, 548(%rbx)
	movl	$parser_tables_dparser_gram, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	parse_grammar
	testl	%eax, %eax
	jns	.LBB0_4
# BB#3:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	d_fail
.LBB0_4:
	movq	%rbx, %rdi
	callq	build_grammar
	testl	%eax, %eax
	jns	.LBB0_6
# BB#5:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	d_fail
.LBB0_6:
	movq	%rbx, %rdi
	callq	write_ctables
	testl	%eax, %eax
	jns	.LBB0_8
# BB#7:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	d_fail
.LBB0_8:
	movq	%rbx, %rdi
	callq	free_D_Grammar
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_1:
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	callq	d_version
	movq	stderr(%rip), %rdi
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.50, %edi
	movl	$37, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$arg_state, %edi
	xorl	%esi, %esi
	callq	usage
	jmp	.LBB0_2
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	help,@function
help:                                   # @help
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 64
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	callq	d_version
	movq	stderr(%rip), %rdi
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.50, %edi
	movl	$37, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	usage
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	help, .Lfunc_end1-help
	.cfi_endproc

	.type	arg_state,@object       # @arg_state
	.data
	.p2align	3
arg_state:
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.3
	.quad	arg_desc
	.size	arg_state, 32

	.type	set_op_priority_from_rule,@object # @set_op_priority_from_rule
	.local	set_op_priority_from_rule
	.comm	set_op_priority_from_rule,4,4
	.type	right_recursive_BNF,@object # @right_recursive_BNF
	.local	right_recursive_BNF
	.comm	right_recursive_BNF,4,4
	.type	states_for_whitespace,@object # @states_for_whitespace
	.p2align	2
states_for_whitespace:
	.long	1                       # 0x1
	.size	states_for_whitespace, 4

	.type	states_for_all_nterms,@object # @states_for_all_nterms
	.local	states_for_all_nterms
	.comm	states_for_all_nterms,4,4
	.type	tokenizer,@object       # @tokenizer
	.local	tokenizer
	.comm	tokenizer,4,4
	.type	longest_match,@object   # @longest_match
	.local	longest_match
	.comm	longest_match,4,4
	.type	grammar_ident,@object   # @grammar_ident
	.p2align	4
grammar_ident:
	.asciz	"gram\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	grammar_ident, 256

	.type	scanner_blocks,@object  # @scanner_blocks
	.p2align	2
scanner_blocks:
	.long	4                       # 0x4
	.size	scanner_blocks, 4

	.type	write_line_directives,@object # @write_line_directives
	.p2align	2
write_line_directives:
	.long	1                       # 0x1
	.size	write_line_directives, 4

	.type	write_header,@object    # @write_header
	.p2align	2
write_header:
	.long	4294967295              # 0xffffffff
	.size	write_header, 4

	.type	token_type,@object      # @token_type
	.local	token_type
	.comm	token_type,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"unable to parse grammar '%s'"
	.size	.L.str, 29

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"unable to load grammar '%s'"
	.size	.L.str.1, 28

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"unable to write C tables '%s'"
	.size	.L.str.2, 30

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"program"
	.size	.L.str.3, 8

	.type	arg_desc,@object        # @arg_desc
	.data
	.p2align	4
arg_desc:
	.quad	.L.str.4
	.byte	108                     # 0x6c
	.zero	7
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	longest_match
	.quad	.L.str.7
	.quad	0
	.quad	.L.str.8
	.byte	84                      # 0x54
	.zero	7
	.quad	.L.str.9
	.quad	.L.str.6
	.quad	tokenizer
	.quad	.L.str.10
	.quad	0
	.quad	.L.str.11
	.byte	72                      # 0x48
	.zero	7
	.quad	.L.str.12
	.quad	.L.str.13
	.quad	write_header
	.quad	.L.str.14
	.quad	0
	.quad	.L.str.15
	.byte	116                     # 0x74
	.zero	7
	.quad	.L.str.16
	.quad	.L.str.13
	.quad	token_type
	.quad	.L.str.17
	.quad	0
	.quad	.L.str.18
	.byte	67                      # 0x43
	.zero	7
	.quad	.L.str.19
	.quad	.L.str.6
	.quad	states_for_whitespace
	.quad	.L.str.20
	.quad	0
	.quad	.L.str.21
	.byte	65                      # 0x41
	.zero	7
	.quad	.L.str.22
	.quad	.L.str.6
	.quad	states_for_all_nterms
	.quad	.L.str.23
	.quad	0
	.quad	.L.str.24
	.byte	105                     # 0x69
	.zero	7
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	grammar_ident
	.quad	.L.str.27
	.quad	0
	.quad	.L.str.28
	.byte	98                      # 0x62
	.zero	7
	.quad	.L.str.29
	.quad	.L.str.13
	.quad	scanner_blocks
	.quad	.L.str.30
	.quad	0
	.quad	.L.str.31
	.byte	112                     # 0x70
	.zero	7
	.quad	.L.str.32
	.quad	.L.str.6
	.quad	set_op_priority_from_rule
	.quad	.L.str.33
	.quad	0
	.quad	.L.str.34
	.byte	114                     # 0x72
	.zero	7
	.quad	.L.str.35
	.quad	.L.str.6
	.quad	right_recursive_BNF
	.quad	.L.str.36
	.quad	0
	.quad	.L.str.37
	.byte	76                      # 0x4c
	.zero	7
	.quad	.L.str.38
	.quad	.L.str.6
	.quad	write_line_directives
	.quad	.L.str.39
	.quad	0
	.quad	.L.str.40
	.byte	118                     # 0x76
	.zero	7
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	verbose_level
	.quad	.L.str.43
	.quad	0
	.quad	.L.str.44
	.byte	100                     # 0x64
	.zero	7
	.quad	.L.str.45
	.quad	.L.str.42
	.quad	debug_level
	.quad	.L.str.46
	.quad	0
	.quad	.L.str.47
	.byte	104                     # 0x68
	.zero	7
	.quad	.L.str.48
	.quad	0
	.quad	0
	.quad	0
	.quad	help
	.zero	56
	.size	arg_desc, 840

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"longest_match"
	.size	.L.str.4, 14

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Use Longest Match Rule for Tokens"
	.size	.L.str.5, 34

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"T"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"D_MAKE_LONGEST_MATCH"
	.size	.L.str.7, 21

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"tokenizer"
	.size	.L.str.8, 10

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Tokenizer for START"
	.size	.L.str.9, 20

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"D_MAKE_PARSER_TOKENIZER"
	.size	.L.str.10, 24

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"write_header"
	.size	.L.str.11, 13

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Write Header (-1:if not empty)"
	.size	.L.str.12, 31

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"I"
	.size	.L.str.13, 2

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"D_MAKE_PARSER_HEADER"
	.size	.L.str.14, 21

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"token_type"
	.size	.L.str.15, 11

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Token Type (0:define, 1:enum)"
	.size	.L.str.16, 30

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"D_MAKE_PARSER_TOKEN"
	.size	.L.str.17, 20

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"whitespace_states"
	.size	.L.str.18, 18

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Compute Whitespace States"
	.size	.L.str.19, 26

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"D_MAKE_PARSER_WHITESPACE"
	.size	.L.str.20, 25

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"all_states"
	.size	.L.str.21, 11

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Compute States For All NTERMs"
	.size	.L.str.22, 30

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"D_MAKE_PARSER_ALL_NTERMS"
	.size	.L.str.23, 25

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"grammar_ident"
	.size	.L.str.24, 14

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Grammar Identifier"
	.size	.L.str.25, 19

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"S255"
	.size	.L.str.26, 5

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"D_MAKE_PARSER_GRAMMAR_IDENT"
	.size	.L.str.27, 28

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"scanner_blocks"
	.size	.L.str.28, 15

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Scanner Blocks"
	.size	.L.str.29, 15

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"D_MAKE_PARSER_SCANNER_BLOCKS"
	.size	.L.str.30, 29

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"op_pri_from_rule"
	.size	.L.str.31, 17

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Set Operator Priority From Rule"
	.size	.L.str.32, 32

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"D_MAKE_PARSER_SET_PRIORITY"
	.size	.L.str.33, 27

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"right_recurse_BNF"
	.size	.L.str.34, 18

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Use Right Recursion For */+"
	.size	.L.str.35, 28

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"D_MAKE_PARSER_RIGHT_RECURSIVE_BNF"
	.size	.L.str.36, 34

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"write_lines"
	.size	.L.str.37, 12

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Write #line(s)"
	.size	.L.str.38, 15

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"D_MAKE_PARSER_WRITE_LINE_DIRECTIVES"
	.size	.L.str.39, 36

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"verbose"
	.size	.L.str.40, 8

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"Verbose"
	.size	.L.str.41, 8

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"+"
	.size	.L.str.42, 2

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"D_MAKE_PARSER_VERBOSE"
	.size	.L.str.43, 22

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"debug"
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"Debug"
	.size	.L.str.45, 6

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"D_MAKE_PARSER_DEBUG"
	.size	.L.str.46, 20

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"help"
	.size	.L.str.47, 5

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Help"
	.size	.L.str.48, 5

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"D Make Parser Version %s "
	.size	.L.str.49, 26

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"Copyright (c) 2002-2003 John Plevyak\n"
	.size	.L.str.50, 38


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
