	.text
	.file	"btGImpactCollisionAlgorithm.bc"
	.globl	_ZN27btGImpactCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN27btGImpactCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN27btGImpactCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV27btGImpactCollisionAlgorithm+16, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN27btGImpactCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end0-_ZN27btGImpactCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.globl	_ZN27btGImpactCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithmD2Ev,@function
_ZN27btGImpactCollisionAlgorithmD2Ev:   # @_ZN27btGImpactCollisionAlgorithmD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV27btGImpactCollisionAlgorithm+16, (%rbx)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB1_3
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	callq	*32(%rax)
.Ltmp1:
# BB#2:                                 # %.noexc
	movq	$0, 24(%rbx)
.LBB1_3:                                # %_ZN27btGImpactCollisionAlgorithm23destroyContactManifoldsEv.exit.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_7
# BB#4:
	movq	(%rdi), %rax
.Ltmp2:
	callq	*(%rax)
.Ltmp3:
# BB#5:                                 # %.noexc2
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
.Ltmp4:
	callq	*104(%rax)
.Ltmp5:
# BB#6:                                 # %.noexc3
	movq	$0, 16(%rbx)
.LBB1_7:
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 48(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN30btActivatingCollisionAlgorithmD2Ev # TAILCALL
.LBB1_8:
.Ltmp6:
	movq	%rax, %r14
.Ltmp7:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp8:
# BB#9:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB1_10:
.Ltmp9:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN27btGImpactCollisionAlgorithmD2Ev, .Lfunc_end1-_ZN27btGImpactCollisionAlgorithmD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp5           #   Call between .Ltmp5 and .Ltmp7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	1                       #   On action: 1
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end1-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN27btGImpactCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithmD0Ev,@function
_ZN27btGImpactCollisionAlgorithmD0Ev:   # @_ZN27btGImpactCollisionAlgorithmD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV27btGImpactCollisionAlgorithm+16, (%rbx)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB3_3
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp10:
	callq	*32(%rax)
.Ltmp11:
# BB#2:                                 # %.noexc.i
	movq	$0, 24(%rbx)
.LBB3_3:                                # %_ZN27btGImpactCollisionAlgorithm23destroyContactManifoldsEv.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_7
# BB#4:
	movq	(%rdi), %rax
.Ltmp12:
	callq	*(%rax)
.Ltmp13:
# BB#5:                                 # %.noexc2.i
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
.Ltmp14:
	callq	*104(%rax)
.Ltmp15:
# BB#6:                                 # %.noexc3.i
	movq	$0, 16(%rbx)
.LBB3_7:
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 48(%rbx)
.Ltmp20:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp21:
# BB#8:                                 # %_ZN27btGImpactCollisionAlgorithmD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_11:
.Ltmp22:
	movq	%rax, %r14
	jmp	.LBB3_12
.LBB3_9:
.Ltmp16:
	movq	%rax, %r14
.Ltmp17:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp18:
.LBB3_12:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_10:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN27btGImpactCollisionAlgorithmD0Ev, .Lfunc_end3-_ZN27btGImpactCollisionAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp15-.Ltmp10         #   Call between .Ltmp10 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin1   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin1   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin1   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN27btGImpactCollisionAlgorithm15addContactPointEP17btCollisionObjectS1_RK9btVector3S4_f
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm15addContactPointEP17btCollisionObjectS1_RK9btVector3S4_f,@function
_ZN27btGImpactCollisionAlgorithm15addContactPointEP17btCollisionObjectS1_RK9btVector3S4_f: # @_ZN27btGImpactCollisionAlgorithm15addContactPointEP17btCollisionObjectS1_RK9btVector3S4_f
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r13, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movl	52(%rbx), %esi
	movl	48(%rbx), %edx
	callq	*16(%rax)
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movl	60(%rbx), %esi
	movl	56(%rbx), %edx
	callq	*24(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB4_2
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	movq	%r12, %rdx
	callq	*24(%rax)
	movq	%rax, 24(%rbx)
.LBB4_2:                                # %_ZN27btGImpactCollisionAlgorithm13checkManifoldEP17btCollisionObjectS1_.exit
	movq	32(%rbx), %rdi
	movq	%rax, 8(%rdi)
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movq	%r14, %rsi
	movq	%r15, %rdx
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.Lfunc_end4:
	.size	_ZN27btGImpactCollisionAlgorithm15addContactPointEP17btCollisionObjectS1_RK9btVector3S4_f, .Lfunc_end4-_ZN27btGImpactCollisionAlgorithm15addContactPointEP17btCollisionObjectS1_RK9btVector3S4_f
	.cfi_endproc

	.globl	_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_,@function
_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_: # @_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 64
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	200(%r15), %r13
	movq	200(%r14), %rbp
	movq	%rcx, 200(%r15)
	movq	%r8, 200(%r14)
	movq	24(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_2
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*24(%rax)
	movq	%rax, %rcx
	movq	%rcx, 24(%rbx)
.LBB5_2:                                # %_ZN27btGImpactCollisionAlgorithm12newAlgorithmEP17btCollisionObjectS1_.exit
	movq	32(%rbx), %rax
	movq	%rcx, 8(%rax)
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*16(%rax)
	movq	%rax, %r12
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movl	52(%rbx), %esi
	movl	48(%rbx), %edx
	callq	*16(%rax)
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movl	60(%rbx), %esi
	movl	56(%rbx), %edx
	callq	*24(%rax)
	movq	(%r12), %rax
	movq	32(%rbx), %r8
	movq	40(%rbx), %rcx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*16(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*104(%rax)
	movq	%r13, 200(%r15)
	movq	%rbp, 200(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_, .Lfunc_end5-_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_
	.cfi_endproc

	.globl	_ZN27btGImpactCollisionAlgorithm26convex_vs_convex_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm26convex_vs_convex_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_,@function
_ZN27btGImpactCollisionAlgorithm26convex_vs_convex_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_: # @_ZN27btGImpactCollisionAlgorithm26convex_vs_convex_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 48
.Lcfi41:
	.cfi_offset %rbx, -48
.Lcfi42:
	.cfi_offset %r12, -40
.Lcfi43:
	.cfi_offset %r13, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	200(%r15), %r12
	movq	200(%r14), %r13
	movq	%rcx, 200(%r15)
	movq	%r8, 200(%r14)
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movl	52(%rbx), %esi
	movl	48(%rbx), %edx
	callq	*16(%rax)
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movl	60(%rbx), %esi
	movl	56(%rbx), %edx
	callq	*24(%rax)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB6_4
# BB#1:
	movq	24(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*24(%rax)
	movq	%rax, %rcx
	movq	%rcx, 24(%rbx)
.LBB6_3:                                # %_ZN27btGImpactCollisionAlgorithm12newAlgorithmEP17btCollisionObjectS1_.exit.i
	movq	32(%rbx), %rax
	movq	%rcx, 8(%rax)
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*16(%rax)
	movq	%rax, %rdi
	movq	%rdi, 16(%rbx)
.LBB6_4:                                # %_ZN27btGImpactCollisionAlgorithm20checkConvexAlgorithmEP17btCollisionObjectS1_.exit
	movq	(%rdi), %rax
	movq	32(%rbx), %r8
	movq	40(%rbx), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*16(%rax)
	movq	%r12, 200(%r15)
	movq	%r13, 200(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZN27btGImpactCollisionAlgorithm26convex_vs_convex_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_, .Lfunc_end6-_ZN27btGImpactCollisionAlgorithm26convex_vs_convex_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_
	.cfi_endproc

	.globl	_ZN27btGImpactCollisionAlgorithm29gimpact_vs_gimpact_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceS4_R9btPairSet
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm29gimpact_vs_gimpact_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceS4_R9btPairSet,@function
_ZN27btGImpactCollisionAlgorithm29gimpact_vs_gimpact_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceS4_R9btPairSet: # @_ZN27btGImpactCollisionAlgorithm29gimpact_vs_gimpact_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceS4_R9btPairSet
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 160
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movq	%rdx, %r15
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpl	$0, 80(%rcx)
	je	.LBB7_2
# BB#1:
	cmpl	$0, 80(%rbx)
	je	.LBB7_2
# BB#31:
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	$80, %rdi
	addq	$80, %rbx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%r9, %r8
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN21btGImpactQuantizedBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet # TAILCALL
.LBB7_2:
	movq	%r9, (%rsp)             # 8-byte Spill
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	callq	*144(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB7_30
# BB#3:                                 # %.lr.ph25
	leaq	56(%rsp), %r13
	leaq	40(%rsp), %r14
	.p2align	4, 0x90
.LBB7_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_29 Depth 2
                                        #       Child Loop BB7_19 Depth 3
                                        #       Child Loop BB7_22 Depth 3
	decl	%ebp
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movl	%ebp, %esi
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	72(%rsp), %rcx
	leaq	88(%rsp), %r8
	callq	*208(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*144(%rax)
	movl	%eax, %r12d
	jmp	.LBB7_29
.LBB7_28:                               # %_ZN9btPairSet9push_pairEii.exit
                                        #   in Loop: Header=BB7_29 Depth=2
	movq	16(%r10), %rax
	movslq	%r9d, %rcx
	movl	%ebp, (%rax,%rcx,8)
	movl	%r12d, 4(%rax,%rcx,8)
	incl	%ecx
	movl	%ecx, 4(%r10)
	jmp	.LBB7_29
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph
                                        #   in Loop: Header=BB7_29 Depth=2
	decl	%r12d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	*208(%rax)
	movss	40(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	88(%rsp), %xmm0
	ja	.LBB7_29
# BB#7:                                 #   in Loop: Header=BB7_29 Depth=2
	movss	72(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	56(%rsp), %xmm0
	ja	.LBB7_29
# BB#8:                                 #   in Loop: Header=BB7_29 Depth=2
	movss	44(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	92(%rsp), %xmm0
	ja	.LBB7_29
# BB#9:                                 #   in Loop: Header=BB7_29 Depth=2
	movss	76(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	60(%rsp), %xmm0
	ja	.LBB7_29
# BB#10:                                #   in Loop: Header=BB7_29 Depth=2
	movss	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	96(%rsp), %xmm0
	ja	.LBB7_29
# BB#11:                                #   in Loop: Header=BB7_29 Depth=2
	movss	80(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	64(%rsp), %xmm0
	jbe	.LBB7_12
	.p2align	4, 0x90
.LBB7_29:                               # %_ZNK6btAABB13has_collisionERKS_.exit.thread.backedge
                                        #   Parent Loop BB7_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_19 Depth 3
                                        #       Child Loop BB7_22 Depth 3
	testl	%r12d, %r12d
	jne	.LBB7_6
	jmp	.LBB7_4
.LBB7_12:                               # %_ZNK6btAABB13has_collisionERKS_.exit
                                        #   in Loop: Header=BB7_29 Depth=2
	movq	(%rsp), %r10            # 8-byte Reload
	movl	4(%r10), %r9d
	cmpl	8(%r10), %r9d
	jne	.LBB7_28
# BB#13:                                #   in Loop: Header=BB7_29 Depth=2
	leal	(%r9,%r9), %ecx
	testl	%r9d, %r9d
	movl	$1, %eax
	cmovel	%eax, %ecx
	cmpl	%ecx, %r9d
	jge	.LBB7_28
# BB#14:                                #   in Loop: Header=BB7_29 Depth=2
	testl	%ecx, %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	je	.LBB7_15
# BB#16:                                #   in Loop: Header=BB7_29 Depth=2
	movslq	%ecx, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	(%rsp), %r10            # 8-byte Reload
	movq	%rax, %r11
	movl	4(%r10), %r9d
	jmp	.LBB7_17
.LBB7_15:                               #   in Loop: Header=BB7_29 Depth=2
	xorl	%r11d, %r11d
.LBB7_17:                               # %_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi.exit.i.i.i
                                        #   in Loop: Header=BB7_29 Depth=2
	movq	16(%r10), %rdi
	testl	%r9d, %r9d
	jle	.LBB7_23
# BB#18:                                # %.lr.ph.i.i.i.i
                                        #   in Loop: Header=BB7_29 Depth=2
	movslq	%r9d, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdx
	xorl	%esi, %esi
	andq	$3, %rdx
	je	.LBB7_20
	.p2align	4, 0x90
.LBB7_19:                               #   Parent Loop BB7_5 Depth=1
                                        #     Parent Loop BB7_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rsi,8), %eax
	movl	%eax, (%r11,%rsi,8)
	movl	4(%rdi,%rsi,8), %eax
	movl	%eax, 4(%r11,%rsi,8)
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB7_19
.LBB7_20:                               # %.prol.loopexit
                                        #   in Loop: Header=BB7_29 Depth=2
	cmpq	$3, %r8
	jb	.LBB7_24
# BB#21:                                # %.lr.ph.i.i.i.i.new
                                        #   in Loop: Header=BB7_29 Depth=2
	subq	%rsi, %rcx
	leaq	28(%r11,%rsi,8), %rdx
	leaq	28(%rdi,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB7_22:                               #   Parent Loop BB7_5 Depth=1
                                        #     Parent Loop BB7_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-28(%rsi), %eax
	movl	%eax, -28(%rdx)
	movl	-24(%rsi), %eax
	movl	%eax, -24(%rdx)
	movl	-20(%rsi), %eax
	movl	%eax, -20(%rdx)
	movl	-16(%rsi), %eax
	movl	%eax, -16(%rdx)
	movl	-12(%rsi), %eax
	movl	%eax, -12(%rdx)
	movl	-8(%rsi), %eax
	movl	%eax, -8(%rdx)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdx)
	movl	(%rsi), %eax
	movl	%eax, (%rdx)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB7_22
	jmp	.LBB7_24
.LBB7_23:                               # %_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_.exit.i.i.i
                                        #   in Loop: Header=BB7_29 Depth=2
	testq	%rdi, %rdi
	je	.LBB7_27
.LBB7_24:                               # %_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_.exit.thread.i.i.i
                                        #   in Loop: Header=BB7_29 Depth=2
	cmpb	$0, 24(%r10)
	je	.LBB7_26
# BB#25:                                #   in Loop: Header=BB7_29 Depth=2
	movq	%r11, 32(%rsp)          # 8-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	(%rsp), %r10            # 8-byte Reload
	movl	4(%r10), %r9d
.LBB7_26:                               #   in Loop: Header=BB7_29 Depth=2
	movq	$0, 16(%r10)
.LBB7_27:                               # %_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv.exit.i.i.i
                                        #   in Loop: Header=BB7_29 Depth=2
	movb	$1, 24(%r10)
	movq	%r11, 16(%r10)
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%r10)
	jmp	.LBB7_28
	.p2align	4, 0x90
.LBB7_4:                                # %.loopexit
                                        #   in Loop: Header=BB7_5 Depth=1
	testl	%ebp, %ebp
	jne	.LBB7_5
.LBB7_30:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN27btGImpactCollisionAlgorithm29gimpact_vs_gimpact_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceS4_R9btPairSet, .Lfunc_end7-_ZN27btGImpactCollisionAlgorithm29gimpact_vs_gimpact_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceS4_R9btPairSet
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN27btGImpactCollisionAlgorithm27gimpact_vs_shape_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceP16btCollisionShapeR20btAlignedObjectArrayIiE
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm27gimpact_vs_shape_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceP16btCollisionShapeR20btAlignedObjectArrayIiE,@function
_ZN27btGImpactCollisionAlgorithm27gimpact_vs_shape_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceP16btCollisionShapeR20btAlignedObjectArrayIiE: # @_ZN27btGImpactCollisionAlgorithm27gimpact_vs_shape_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceP16btCollisionShapeR20btAlignedObjectArrayIiE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi65:
	.cfi_def_cfa_offset 176
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rcx, %r15
	movq	%rsi, %rbx
	cmpl	$0, 80(%r15)
	je	.LBB8_2
# BB#1:
	movl	16(%rbx), %edi
	movl	32(%rbx), %ebp
	movl	(%rbx), %eax
	movl	4(%rbx), %r12d
	movl	20(%rbx), %ecx
	movl	36(%rbx), %esi
	movl	8(%rbx), %r9d
	movl	24(%rbx), %r10d
	movl	40(%rbx), %r11d
	movss	48(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	xorps	.LCPI8_0(%rip), %xmm3
	movss	52(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	56(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movd	%eax, %xmm15
	movdqa	%xmm15, %xmm13
	mulss	%xmm3, %xmm13
	movd	%edi, %xmm1
	movdqa	%xmm1, %xmm0
	mulss	%xmm7, %xmm0
	subss	%xmm0, %xmm13
	movd	%ebp, %xmm9
	movdqa	%xmm9, %xmm0
	mulss	%xmm2, %xmm0
	subss	%xmm0, %xmm13
	movd	%r12d, %xmm12
	movdqa	%xmm12, %xmm14
	mulss	%xmm3, %xmm14
	movd	%ecx, %xmm6
	movdqa	%xmm6, %xmm5
	mulss	%xmm7, %xmm5
	subss	%xmm5, %xmm14
	movd	%esi, %xmm4
	movdqa	%xmm4, %xmm5
	mulss	%xmm2, %xmm5
	subss	%xmm5, %xmm14
	movd	%r9d, %xmm0
	movd	%xmm0, 12(%rsp)         # 4-byte Folded Spill
	mulss	%xmm0, %xmm3
	movd	%r10d, %xmm10
	mulss	%xmm10, %xmm7
	subss	%xmm7, %xmm3
	movd	%r11d, %xmm11
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	movl	%eax, 16(%rsp)
	movl	%edi, 20(%rsp)
	movl	%ebp, 24(%rsp)
	movl	$0, 28(%rsp)
	movl	%r12d, 32(%rsp)
	movl	%ecx, 36(%rsp)
	movl	%esi, 40(%rsp)
	movl	$0, 44(%rsp)
	movl	%r9d, 48(%rsp)
	movl	%r10d, 52(%rsp)
	movl	%r11d, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm2, 72(%rsp)
	movss	48(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	52(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movdqa	%xmm15, %xmm2
	mulss	%xmm5, %xmm2
	movdqa	%xmm1, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm2, %xmm0
	movss	56(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movdqa	%xmm9, %xmm8
	mulss	%xmm2, %xmm8
	addss	%xmm0, %xmm8
	mulss	%xmm5, %xmm12
	mulss	%xmm7, %xmm6
	addss	%xmm12, %xmm6
	mulss	%xmm2, %xmm4
	addss	%xmm6, %xmm4
	mulss	12(%rsp), %xmm5         # 4-byte Folded Reload
	mulss	%xmm10, %xmm7
	addss	%xmm5, %xmm7
	mulss	%xmm11, %xmm2
	addss	%xmm7, %xmm2
	addss	%xmm13, %xmm8
	movss	%xmm8, 64(%rsp)
	addss	%xmm14, %xmm4
	movss	%xmm4, 68(%rsp)
	addss	%xmm3, %xmm2
	movss	%xmm2, 72(%rsp)
	movss	(%rdx), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	mulss	%xmm15, %xmm2
	movss	16(%rdx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	32(%rdx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	movdqa	%xmm9, %xmm4
	mulss	%xmm4, %xmm0
	addss	%xmm3, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movaps	%xmm15, %xmm2
	mulss	%xmm10, %xmm2
	movss	20(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	36(%rdx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm11
	mulss	%xmm9, %xmm11
	addss	%xmm3, %xmm11
	movss	8(%rdx), %xmm13         # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm15
	movss	24(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	addss	%xmm15, %xmm1
	movss	40(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm1, %xmm3
	movss	%xmm3, 80(%rsp)         # 4-byte Spill
	movss	32(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	36(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	mulss	%xmm4, %xmm1
	movaps	%xmm8, %xmm5
	mulss	%xmm3, %xmm5
	addss	%xmm1, %xmm5
	movss	40(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	addss	%xmm5, %xmm15
	movaps	%xmm10, %xmm5
	mulss	%xmm4, %xmm5
	movaps	%xmm0, %xmm6
	mulss	%xmm3, %xmm6
	addss	%xmm5, %xmm6
	movaps	%xmm9, %xmm5
	mulss	%xmm1, %xmm5
	addss	%xmm6, %xmm5
	mulss	%xmm13, %xmm4
	mulss	%xmm7, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm2, %xmm1
	addss	%xmm3, %xmm1
	movss	48(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm12
	movss	52(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm8
	addss	%xmm12, %xmm8
	movss	56(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm14
	addss	%xmm8, %xmm14
	mulss	%xmm3, %xmm10
	mulss	%xmm4, %xmm0
	addss	%xmm10, %xmm0
	mulss	%xmm6, %xmm9
	addss	%xmm0, %xmm9
	mulss	%xmm3, %xmm13
	mulss	%xmm4, %xmm7
	addss	%xmm13, %xmm7
	mulss	%xmm6, %xmm2
	addss	%xmm7, %xmm2
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)
	movss	%xmm11, 20(%rsp)
	movss	80(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm15, 32(%rsp)
	movss	%xmm5, 36(%rsp)
	movss	%xmm1, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	%xmm14, 48(%rsp)
	movss	%xmm9, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	movq	(%r8), %rax
	leaq	104(%rsp), %rcx
	leaq	16(%rsp), %rsi
	leaq	88(%rsp), %rbx
	movq	%r8, %rdi
	movq	%rbx, %rdx
	callq	*16(%rax)
	addq	$80, %r15
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE
	jmp	.LBB8_44
.LBB8_2:
	movq	(%r8), %rbp
	leaq	104(%rsp), %rcx
	leaq	88(%rsp), %rax
	movq	%r8, %rdi
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	*16(%rbp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*144(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB8_44
# BB#3:                                 # %.lr.ph
	leaq	32(%rsp), %r12
	leaq	16(%rsp), %r13
	jmp	.LBB8_4
.LBB8_24:                               # %vector.body.preheader
                                        #   in Loop: Header=BB8_4 Depth=1
	leaq	-8(%rdx), %r8
	movl	%r8d, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_27
# BB#25:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB8_4 Depth=1
	negq	%rsi
	xorl	%ecx, %ecx
.LBB8_26:                               # %vector.body.prol
                                        #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rcx,4), %xmm0
	movups	16(%rdi,%rcx,4), %xmm1
	movups	%xmm0, (%rax,%rcx,4)
	movups	%xmm1, 16(%rax,%rcx,4)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB8_26
	jmp	.LBB8_28
.LBB8_27:                               #   in Loop: Header=BB8_4 Depth=1
	xorl	%ecx, %ecx
.LBB8_28:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB8_4 Depth=1
	cmpq	$24, %r8
	jb	.LBB8_31
# BB#29:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	%rdx, %r8
	subq	%rcx, %r8
	leaq	112(%rdi,%rcx,4), %rsi
	leaq	112(%rax,%rcx,4), %rcx
.LBB8_30:                               # %vector.body
                                        #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rsi
	subq	$-128, %rcx
	addq	$-32, %r8
	jne	.LBB8_30
.LBB8_31:                               # %middle.block
                                        #   in Loop: Header=BB8_4 Depth=1
	cmpq	%rdx, %r9
	je	.LBB8_38
	jmp	.LBB8_32
	.p2align	4, 0x90
.LBB8_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_26 Depth 2
                                        #     Child Loop BB8_30 Depth 2
                                        #     Child Loop BB8_34 Depth 2
                                        #     Child Loop BB8_37 Depth 2
	decl	%ebp
	movq	(%r15), %rax
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	callq	*208(%rax)
	movss	88(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rsp), %xmm0
	ja	.LBB8_43
# BB#5:                                 #   in Loop: Header=BB8_4 Depth=1
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	104(%rsp), %xmm0
	ja	.LBB8_43
# BB#6:                                 #   in Loop: Header=BB8_4 Depth=1
	movss	92(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	36(%rsp), %xmm0
	ja	.LBB8_43
# BB#7:                                 #   in Loop: Header=BB8_4 Depth=1
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	108(%rsp), %xmm0
	ja	.LBB8_43
# BB#8:                                 #   in Loop: Header=BB8_4 Depth=1
	movss	96(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	40(%rsp), %xmm0
	ja	.LBB8_43
# BB#9:                                 #   in Loop: Header=BB8_4 Depth=1
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	112(%rsp), %xmm0
	ja	.LBB8_43
# BB#10:                                # %_ZNK6btAABB13has_collisionERKS_.exit
                                        #   in Loop: Header=BB8_4 Depth=1
	movl	4(%r14), %r10d
	cmpl	8(%r14), %r10d
	jne	.LBB8_42
# BB#11:                                #   in Loop: Header=BB8_4 Depth=1
	leal	(%r10,%r10), %edx
	testl	%r10d, %r10d
	movl	$1, %ecx
	cmovel	%ecx, %edx
	cmpl	%edx, %r10d
	jge	.LBB8_42
# BB#12:                                #   in Loop: Header=BB8_4 Depth=1
	testl	%edx, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	je	.LBB8_14
# BB#13:                                #   in Loop: Header=BB8_4 Depth=1
	movslq	%edx, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movl	4(%r14), %r10d
	jmp	.LBB8_15
.LBB8_14:                               #   in Loop: Header=BB8_4 Depth=1
	xorl	%eax, %eax
.LBB8_15:                               # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	16(%r14), %rdi
	testl	%r10d, %r10d
	jle	.LBB8_18
# BB#16:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB8_4 Depth=1
	movslq	%r10d, %r9
	cmpl	$8, %r10d
	jae	.LBB8_19
# BB#17:                                #   in Loop: Header=BB8_4 Depth=1
	xorl	%edx, %edx
	jmp	.LBB8_32
.LBB8_18:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
                                        #   in Loop: Header=BB8_4 Depth=1
	testq	%rdi, %rdi
	jne	.LBB8_38
	jmp	.LBB8_41
.LBB8_19:                               # %min.iters.checked
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	%r9, %rdx
	andq	$-8, %rdx
	je	.LBB8_23
# BB#20:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_4 Depth=1
	leaq	(%rdi,%r9,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB8_24
# BB#21:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_4 Depth=1
	leaq	(%rax,%r9,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB8_24
.LBB8_23:                               #   in Loop: Header=BB8_4 Depth=1
	xorl	%edx, %edx
.LBB8_32:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB8_4 Depth=1
	subl	%edx, %r10d
	leaq	-1(%r9), %rcx
	subq	%rdx, %rcx
	andq	$7, %r10
	je	.LBB8_35
# BB#33:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB8_4 Depth=1
	negq	%r10
	.p2align	4, 0x90
.LBB8_34:                               # %scalar.ph.prol
                                        #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rdx,4), %esi
	movl	%esi, (%rax,%rdx,4)
	incq	%rdx
	incq	%r10
	jne	.LBB8_34
.LBB8_35:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB8_4 Depth=1
	cmpq	$7, %rcx
	jb	.LBB8_38
# BB#36:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB8_4 Depth=1
	subq	%rdx, %r9
	leaq	28(%rdi,%rdx,4), %rsi
	leaq	28(%rax,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB8_37:                               # %scalar.ph
                                        #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rsi), %edx
	movl	%edx, -28(%rcx)
	movl	-24(%rsi), %edx
	movl	%edx, -24(%rcx)
	movl	-20(%rsi), %edx
	movl	%edx, -20(%rcx)
	movl	-16(%rsi), %edx
	movl	%edx, -16(%rcx)
	movl	-12(%rsi), %edx
	movl	%edx, -12(%rcx)
	movl	-8(%rsi), %edx
	movl	%edx, -8(%rcx)
	movl	-4(%rsi), %edx
	movl	%edx, -4(%rcx)
	movl	(%rsi), %edx
	movl	%edx, (%rcx)
	addq	$32, %rsi
	addq	$32, %rcx
	addq	$-8, %r9
	jne	.LBB8_37
.LBB8_38:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
                                        #   in Loop: Header=BB8_4 Depth=1
	cmpb	$0, 24(%r14)
	je	.LBB8_40
# BB#39:                                #   in Loop: Header=BB8_4 Depth=1
	movq	%rax, 80(%rsp)          # 8-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movq	80(%rsp), %rax          # 8-byte Reload
.LBB8_40:                               #   in Loop: Header=BB8_4 Depth=1
	movq	$0, 16(%r14)
	movl	4(%r14), %r10d
.LBB8_41:                               # %_ZN20btAlignedObjectArrayIiE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB8_4 Depth=1
	movb	$1, 24(%r14)
	movq	%rax, 16(%r14)
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 8(%r14)
.LBB8_42:                               # %_ZN20btAlignedObjectArrayIiE9push_backERKi.exit
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	16(%r14), %rcx
	movslq	%r10d, %rax
	movl	%ebp, (%rcx,%rax,4)
	incl	4(%r14)
	.p2align	4, 0x90
.LBB8_43:                               # %_ZNK6btAABB13has_collisionERKS_.exit.thread.backedge
                                        #   in Loop: Header=BB8_4 Depth=1
	testl	%ebp, %ebp
	jne	.LBB8_4
.LBB8_44:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN27btGImpactCollisionAlgorithm27gimpact_vs_shape_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceP16btCollisionShapeR20btAlignedObjectArrayIiE, .Lfunc_end8-_ZN27btGImpactCollisionAlgorithm27gimpact_vs_shape_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceP16btCollisionShapeR20btAlignedObjectArrayIiE
	.cfi_endproc

	.globl	_ZN27btGImpactCollisionAlgorithm21collide_gjk_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm21collide_gjk_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii,@function
_ZN27btGImpactCollisionAlgorithm21collide_gjk_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii: # @_ZN27btGImpactCollisionAlgorithm21collide_gjk_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 304
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, %rbx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %rbp
	leaq	136(%rsp), %rdi
	callq	_ZN23btPolyhedralConvexShapeC2Ev
	movl	$1, 144(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 232(%rsp)
	movups	%xmm0, 216(%rsp)
	movups	%xmm0, 200(%rsp)
	movq	$_ZTV17btTriangleShapeEx+16, 136(%rsp)
.Ltmp23:
	leaq	24(%rsp), %rdi
	callq	_ZN23btPolyhedralConvexShapeC2Ev
.Ltmp24:
# BB#1:
	movl	$1, 32(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 120(%rsp)
	movups	%xmm0, 104(%rsp)
	movups	%xmm0, 88(%rsp)
	movq	$_ZTV17btTriangleShapeEx+16, 24(%rsp)
	movq	(%rbx), %rax
.Ltmp26:
	movq	%rbx, %rdi
	callq	*192(%rax)
.Ltmp27:
# BB#2:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp28:
	callq	*192(%rax)
.Ltmp29:
# BB#3:                                 # %.preheader
	movl	304(%rsp), %r14d
	testl	%r14d, %r14d
	je	.LBB9_9
	.p2align	4, 0x90
.LBB9_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%r13), %esi
	movl	%esi, 48(%rbp)
	movl	4(%r13), %eax
	movl	%eax, 56(%rbp)
	movq	(%rbx), %rax
.Ltmp31:
	movq	%rbx, %r15
	movq	%rbx, %rdi
	leaq	136(%rsp), %rdx
	callq	*176(%rax)
.Ltmp32:
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	56(%rbp), %esi
.Ltmp33:
	leaq	24(%rsp), %rdx
	callq	*176(%rax)
.Ltmp34:
# BB#6:                                 #   in Loop: Header=BB9_4 Depth=1
.Ltmp35:
	leaq	136(%rsp), %rdi
	leaq	24(%rsp), %rsi
	callq	_ZN17btTriangleShapeEx25overlap_test_conservativeERKS_
.Ltmp36:
# BB#7:                                 #   in Loop: Header=BB9_4 Depth=1
	testb	%al, %al
	je	.LBB9_8
# BB#13:                                #   in Loop: Header=BB9_4 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	200(%rcx), %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	200(%rax), %r12
	leaq	136(%rsp), %rdx
	movq	%rdx, 200(%rcx)
	leaq	24(%rsp), %rcx
	movq	%rcx, 200(%rax)
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movl	52(%rbp), %esi
	movl	48(%rbp), %edx
.Ltmp37:
	callq	*16(%rax)
.Ltmp38:
# BB#14:                                # %.noexc
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movl	60(%rbp), %esi
	movl	56(%rbp), %edx
.Ltmp39:
	callq	*24(%rax)
.Ltmp40:
# BB#15:                                # %.noexc24
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	16(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB9_21
# BB#16:                                #   in Loop: Header=BB9_4 Depth=1
	movq	24(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB9_19
# BB#17:                                #   in Loop: Header=BB9_4 Depth=1
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
.Ltmp41:
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	*24(%rax)
.Ltmp42:
# BB#18:                                # %.noexc25
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	%rax, 24(%rbp)
.LBB9_19:                               # %_ZN27btGImpactCollisionAlgorithm12newAlgorithmEP17btCollisionObjectS1_.exit.i.i
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	32(%rbp), %rcx
	movq	%rax, 8(%rcx)
	movq	8(%rbp), %rdi
	movq	(%rdi), %r8
.Ltmp43:
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rax, %rcx
	callq	*16(%r8)
.Ltmp44:
# BB#20:                                # %.noexc26
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	%rax, 16(%rbp)
.LBB9_21:                               # %_ZN27btGImpactCollisionAlgorithm20checkConvexAlgorithmEP17btCollisionObjectS1_.exit.i
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	(%rax), %r9
	movq	32(%rbp), %r8
	movq	40(%rbp), %rcx
.Ltmp45:
	movq	%rax, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	*16(%r9)
.Ltmp46:
# BB#22:                                # %_ZN27btGImpactCollisionAlgorithm26convex_vs_convex_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_.exit
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rbx, 200(%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%r12, 200(%rax)
.LBB9_8:                                # %.backedge
                                        #   in Loop: Header=BB9_4 Depth=1
	decl	%r14d
	addq	$8, %r13
	testl	%r14d, %r14d
	movq	%r15, %rbx
	jne	.LBB9_4
.LBB9_9:                                # %._crit_edge
	movq	(%rbx), %rax
.Ltmp48:
	movq	%rbx, %rdi
	callq	*200(%rax)
.Ltmp49:
# BB#10:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp50:
	callq	*200(%rax)
.Ltmp51:
# BB#11:
.Ltmp55:
	leaq	24(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp56:
# BB#12:
	leaq	136(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_26:
.Ltmp57:
	movq	%rax, %rbx
	jmp	.LBB9_29
.LBB9_23:
.Ltmp25:
	movq	%rax, %rbx
	jmp	.LBB9_29
.LBB9_25:                               # %.loopexit.split-lp
.Ltmp52:
	jmp	.LBB9_28
.LBB9_27:
.Ltmp30:
	jmp	.LBB9_28
.LBB9_24:                               # %.loopexit
.Ltmp47:
.LBB9_28:
	movq	%rax, %rbx
.Ltmp53:
	leaq	24(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp54:
.LBB9_29:
.Ltmp58:
	leaq	136(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp59:
# BB#30:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_31:
.Ltmp60:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN27btGImpactCollisionAlgorithm21collide_gjk_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii, .Lfunc_end9-_ZN27btGImpactCollisionAlgorithm21collide_gjk_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp23-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp29-.Ltmp26         #   Call between .Ltmp26 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin2   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp46-.Ltmp31         #   Call between .Ltmp31 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin2   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp51-.Ltmp48         #   Call between .Ltmp48 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin2   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin2   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp53-.Ltmp56         #   Call between .Ltmp56 and .Ltmp53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp59-.Ltmp53         #   Call between .Ltmp53 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	1                       #   On action: 1
	.long	.Ltmp59-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Lfunc_end9-.Ltmp59     #   Call between .Ltmp59 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN27btGImpactCollisionAlgorithm21collide_sat_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm21collide_sat_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii,@function
_ZN27btGImpactCollisionAlgorithm21collide_sat_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii: # @_ZN27btGImpactCollisionAlgorithm21collide_sat_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$712, %rsp              # imm = 0x2C8
.Lcfi91:
	.cfi_def_cfa_offset 768
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, %rbx
	movq	%rdi, %rbp
	movl	768(%rsp), %r13d
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 320(%rsp)        # 16-byte Spill
	movss	12(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 304(%rsp)        # 16-byte Spill
	movss	16(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 288(%rsp)        # 16-byte Spill
	movss	24(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	movss	28(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 208(%rsp)        # 16-byte Spill
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movss	40(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movss	44(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	movss	48(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
	movsd	56(%rsi), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 416(%rsp)        # 16-byte Spill
	movq	%rsi, 336(%rsp)         # 8-byte Spill
	movss	64(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 272(%rsp)        # 16-byte Spill
	movss	12(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 256(%rsp)        # 16-byte Spill
	movss	16(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	movss	24(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 352(%rsp)        # 16-byte Spill
	movss	28(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 368(%rsp)        # 16-byte Spill
	movss	32(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 384(%rsp)        # 16-byte Spill
	movss	40(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	movss	44(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movss	48(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	movsd	56(%rdx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 400(%rsp)        # 16-byte Spill
	movq	%rdx, 344(%rsp)         # 8-byte Spill
	movss	64(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movl	$1008981770, 184(%rsp)  # imm = 0x3C23D70A
	movl	$1008981770, 112(%rsp)  # imm = 0x3C23D70A
	movq	(%rcx), %rax
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	callq	*192(%rax)
	movq	(%rbx), %rax
	movq	%rbx, 232(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	callq	*192(%rax)
	testl	%r13d, %r13d
	je	.LBB10_9
# BB#1:                                 # %.lr.ph164
	leaq	440(%rsp), %r14
	movaps	320(%rsp), %xmm0        # 16-byte Reload
	unpcklps	192(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movaps	%xmm0, 320(%rsp)        # 16-byte Spill
	movaps	304(%rsp), %xmm0        # 16-byte Reload
	unpcklps	208(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movaps	%xmm0, 304(%rsp)        # 16-byte Spill
	movaps	288(%rsp), %xmm0        # 16-byte Reload
	unpcklps	(%rsp), %xmm0   # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movaps	%xmm0, 288(%rsp)        # 16-byte Spill
	movaps	272(%rsp), %xmm0        # 16-byte Reload
	unpcklps	352(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movaps	%xmm0, 272(%rsp)        # 16-byte Spill
	movaps	256(%rsp), %xmm0        # 16-byte Reload
	unpcklps	368(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movaps	%xmm0, 256(%rsp)        # 16-byte Spill
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	unpcklps	384(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	.p2align	4, 0x90
.LBB10_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_12 Depth 2
	movl	(%r12), %ebx
	movl	%ebx, 48(%rbp)
	movl	4(%r12), %eax
	movl	%eax, 56(%rbp)
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	callq	*136(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movl	%ebx, %esi
	leaq	120(%rsp), %rdx
	callq	*40(%rcx)
	movl	56(%rbp), %ebx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	callq	*136(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movl	%ebx, %esi
	leaq	48(%rsp), %rdx
	callq	*40(%rcx)
	movss	120(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	124(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	128(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	44(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	320(%rsp), %xmm5        # 16-byte Reload
	mulps	%xmm5, %xmm1
	movss	40(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	mulss	%xmm2, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	304(%rsp), %xmm6        # 16-byte Reload
	mulps	%xmm6, %xmm2
	addps	%xmm1, %xmm2
	movss	36(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm1
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	288(%rsp), %xmm7        # 16-byte Reload
	mulps	%xmm7, %xmm0
	addps	%xmm2, %xmm0
	movaps	416(%rsp), %xmm11       # 16-byte Reload
	addps	%xmm11, %xmm0
	addss	%xmm3, %xmm4
	addss	%xmm1, %xmm4
	movss	32(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	addss	%xmm12, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm0, 120(%rsp)
	movlps	%xmm1, 128(%rsp)
	movss	136(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	140(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	144(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm5, %xmm1
	movaps	%xmm9, %xmm4
	mulss	%xmm2, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm6, %xmm2
	addps	%xmm1, %xmm2
	movaps	%xmm10, %xmm1
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm7, %xmm0
	addps	%xmm2, %xmm0
	addps	%xmm11, %xmm0
	addss	%xmm3, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm12, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm0, 136(%rsp)
	movlps	%xmm1, 144(%rsp)
	movss	152(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	156(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	160(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm5, %xmm1
	movaps	%xmm9, %xmm4
	mulss	%xmm2, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm6, %xmm2
	addps	%xmm1, %xmm2
	movaps	%xmm10, %xmm1
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm7, %xmm0
	addps	%xmm2, %xmm0
	addps	%xmm11, %xmm0
	addss	%xmm3, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm12, %xmm4
	movaps	%xmm0, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm0, 152(%rsp)
	movlps	%xmm1, 160(%rsp)
	movss	48(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	56(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	mulss	%xmm2, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	272(%rsp), %xmm6        # 16-byte Reload
	mulps	%xmm6, %xmm2
	movss	24(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm5
	mulss	%xmm3, %xmm5
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movaps	256(%rsp), %xmm14       # 16-byte Reload
	mulps	%xmm14, %xmm3
	addps	%xmm2, %xmm3
	movss	20(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	240(%rsp), %xmm8        # 16-byte Reload
	mulps	%xmm8, %xmm1
	addps	%xmm3, %xmm1
	movaps	400(%rsp), %xmm12       # 16-byte Reload
	addps	%xmm12, %xmm1
	addss	%xmm4, %xmm5
	addss	%xmm2, %xmm5
	movss	16(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	addss	%xmm13, %xmm5
	xorps	%xmm2, %xmm2
	movss	%xmm5, %xmm2            # xmm2 = xmm5[0],xmm2[1,2,3]
	movlps	%xmm1, 48(%rsp)
	movlps	%xmm2, 56(%rsp)
	movss	64(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	68(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	72(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	mulss	%xmm2, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm6, %xmm2
	movaps	%xmm10, %xmm5
	mulss	%xmm3, %xmm5
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm14, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm11, %xmm2
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm8, %xmm1
	addps	%xmm3, %xmm1
	addps	%xmm12, %xmm1
	addss	%xmm4, %xmm5
	addss	%xmm2, %xmm5
	addss	%xmm13, %xmm5
	xorps	%xmm2, %xmm2
	movss	%xmm5, %xmm2            # xmm2 = xmm5[0],xmm2[1,2,3]
	movlps	%xmm1, 64(%rsp)
	movlps	%xmm2, 72(%rsp)
	movss	80(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	84(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	mulss	%xmm2, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm6, %xmm2
	movaps	%xmm10, %xmm5
	mulss	%xmm3, %xmm5
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm14, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm11, %xmm2
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm8, %xmm1
	addps	%xmm3, %xmm1
	addps	%xmm12, %xmm1
	addss	%xmm4, %xmm5
	addss	%xmm2, %xmm5
	addss	%xmm13, %xmm5
	xorps	%xmm2, %xmm2
	movss	%xmm5, %xmm2            # xmm2 = xmm5[0],xmm2[1,2,3]
	movlps	%xmm1, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	movss	136(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	120(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	124(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	movss	140(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm4
	movss	144(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	movss	128(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm5
	subss	%xmm3, %xmm7
	movss	160(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movaps	%xmm5, %xmm2
	mulss	%xmm0, %xmm5
	mulss	%xmm4, %xmm0
	mulss	%xmm3, %xmm4
	mulss	%xmm7, %xmm2
	subss	%xmm2, %xmm4
	mulss	%xmm1, %xmm3
	subss	%xmm3, %xmm5
	mulss	%xmm1, %xmm7
	subss	%xmm0, %xmm7
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB10_4
# BB#3:                                 # %call.sqrt
                                        #   in Loop: Header=BB10_2 Depth=1
	movaps	%xmm7, (%rsp)           # 16-byte Spill
	movss	%xmm5, 208(%rsp)        # 4-byte Spill
	movss	%xmm4, 192(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	192(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	208(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	(%rsp), %xmm7           # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB10_4:                               # %.split
                                        #   in Loop: Header=BB10_2 Depth=1
	movss	.LCPI10_0(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm8
	movaps	%xmm8, %xmm0
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm5
	mulss	%xmm7, %xmm0
	movss	120(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	124(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	movss	128(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	%xmm4, 168(%rsp)
	movss	%xmm5, 172(%rsp)
	movss	%xmm0, 176(%rsp)
	movss	%xmm1, 180(%rsp)
	movss	64(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	68(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movss	72(%rsp), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	56(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm7
	movss	80(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm5
	movss	84(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm4
	movss	88(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm1
	movaps	%xmm7, %xmm2
	mulss	%xmm5, %xmm7
	mulss	%xmm3, %xmm5
	mulss	%xmm1, %xmm3
	mulss	%xmm4, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm7
	mulss	%xmm0, %xmm4
	subss	%xmm5, %xmm4
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm7, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB10_6
# BB#5:                                 # %call.sqrt171
                                        #   in Loop: Header=BB10_2 Depth=1
	movaps	%xmm1, %xmm0
	movss	%xmm7, (%rsp)           # 4-byte Spill
	movss	%xmm4, 208(%rsp)        # 4-byte Spill
	movss	%xmm3, 192(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	192(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	208(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	(%rsp), %xmm7           # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	.LCPI10_0(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm8
.LBB10_6:                               # %.split.split
                                        #   in Loop: Header=BB10_2 Depth=1
	decl	%r13d
	movaps	%xmm8, %xmm1
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm7
	mulss	%xmm4, %xmm1
	movss	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	52(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm0, %xmm2
	movss	56(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm3, 96(%rsp)
	movss	%xmm7, 100(%rsp)
	movss	%xmm1, 104(%rsp)
	movss	%xmm0, 108(%rsp)
	leaq	120(%rsp), %rdi
	leaq	48(%rsp), %rsi
	callq	_ZN19btPrimitiveTriangle25overlap_test_conservativeERKS_
	testb	%al, %al
	je	.LBB10_8
# BB#7:                                 #   in Loop: Header=BB10_2 Depth=1
	leaq	120(%rsp), %rdi
	leaq	48(%rsp), %rsi
	leaq	432(%rsp), %rdx
	callq	_ZN19btPrimitiveTriangle35find_triangle_collision_clip_methodERS_R20GIM_TRIANGLE_CONTACT
	testb	%al, %al
	je	.LBB10_8
# BB#10:                                #   in Loop: Header=BB10_2 Depth=1
	movslq	436(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB10_8
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	%rbx, %r15
	shlq	$4, %r15
	addq	%r14, %r15
	.p2align	4, 0x90
.LBB10_12:                              # %.lr.ph
                                        #   Parent Loop BB10_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	432(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI10_1(%rip), %xmm0
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movl	52(%rbp), %esi
	movl	48(%rbp), %edx
	callq	*16(%rax)
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movl	60(%rbp), %esi
	movl	56(%rbp), %edx
	callq	*24(%rax)
	movq	24(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB10_14
# BB#13:                                #   in Loop: Header=BB10_12 Depth=2
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	movq	336(%rsp), %rsi         # 8-byte Reload
	movq	344(%rsp), %rdx         # 8-byte Reload
	callq	*24(%rax)
	movq	%rax, 24(%rbp)
.LBB10_14:                              # %_ZN27btGImpactCollisionAlgorithm15addContactPointEP17btCollisionObjectS1_RK9btVector3S4_f.exit
                                        #   in Loop: Header=BB10_12 Depth=2
	movq	32(%rbp), %rdi
	movq	%rax, 8(%rdi)
	movq	(%rdi), %rax
	movq	%r14, %rsi
	movq	%r15, %rdx
	movaps	(%rsp), %xmm0           # 16-byte Reload
	callq	*32(%rax)
	addq	$-16, %r15
	decl	%ebx
	jne	.LBB10_12
	.p2align	4, 0x90
.LBB10_8:                               # %.backedge
                                        #   in Loop: Header=BB10_2 Depth=1
	addq	$8, %r12
	testl	%r13d, %r13d
	jne	.LBB10_2
.LBB10_9:                               # %._crit_edge
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	callq	*200(%rax)
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	callq	*200(%rax)
	addq	$712, %rsp              # imm = 0x2C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN27btGImpactCollisionAlgorithm21collide_sat_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii, .Lfunc_end10-_ZN27btGImpactCollisionAlgorithm21collide_sat_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii
	.cfi_endproc

	.globl	_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEP17btCollisionObjectS1_P23btGImpactShapeInterfaceS3_
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEP17btCollisionObjectS1_P23btGImpactShapeInterfaceS3_,@function
_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEP17btCollisionObjectS1_P23btGImpactShapeInterfaceS3_: # @_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEP17btCollisionObjectS1_P23btGImpactShapeInterfaceS3_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 56
	subq	$1144, %rsp             # imm = 0x478
.Lcfi104:
	.cfi_def_cfa_offset 1200
.Lcfi105:
	.cfi_offset %rbx, -56
.Lcfi106:
	.cfi_offset %r12, -48
.Lcfi107:
	.cfi_offset %r13, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*128(%rax)
	cmpl	$2, %eax
	jne	.LBB11_4
# BB#1:
	movl	188(%r15), %ecx
	jmp	.LBB11_2
	.p2align	4, 0x90
.LBB11_3:                               #   in Loop: Header=BB11_2 Depth=1
	movq	200(%r15), %rcx
	cltq
	movq	(%rcx,%rax,8), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%r14, %r8
	callq	_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEP17btCollisionObjectS1_P23btGImpactShapeInterfaceS3_
	movl	52(%rbx), %ecx
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	leal	-1(%rcx), %eax
	movl	%eax, 52(%rbx)
	testl	%ecx, %ecx
	jne	.LBB11_3
	jmp	.LBB11_8
.LBB11_4:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*128(%rax)
	cmpl	$2, %eax
	jne	.LBB11_9
# BB#5:
	movl	188(%r14), %ecx
	jmp	.LBB11_7
	.p2align	4, 0x90
.LBB11_6:                               #   in Loop: Header=BB11_7 Depth=1
	movq	200(%r14), %rcx
	cltq
	movq	(%rcx,%rax,8), %r8
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	callq	_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEP17btCollisionObjectS1_P23btGImpactShapeInterfaceS3_
	movl	60(%rbx), %ecx
.LBB11_7:                               # =>This Inner Loop Header: Depth=1
	leal	-1(%rcx), %eax
	movl	%eax, 60(%rbx)
	testl	%ecx, %ecx
	jne	.LBB11_6
	jmp	.LBB11_8
.LBB11_9:
	movups	8(%r12), %xmm0
	movaps	%xmm0, 224(%rsp)
	movups	24(%r12), %xmm0
	movaps	%xmm0, 240(%rsp)
	movups	40(%r12), %xmm0
	movaps	%xmm0, 256(%rsp)
	movups	56(%r12), %xmm0
	movaps	%xmm0, 272(%rsp)
	movups	8(%rbp), %xmm0
	movaps	%xmm0, 160(%rsp)
	movups	24(%rbp), %xmm0
	movaps	%xmm0, 176(%rsp)
	movups	40(%rbp), %xmm0
	movaps	%xmm0, 192(%rsp)
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movups	56(%rbp), %xmm0
	movaps	%xmm0, 208(%rsp)
	movb	$1, 56(%rsp)
	movq	$0, 48(%rsp)
	movq	$0, 36(%rsp)
.Ltmp61:
	movl	$256, %edi              # imm = 0x100
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp62:
# BB#10:                                # %.noexc90
	movslq	36(%rsp), %rax
	testq	%rax, %rax
	movq	48(%rsp), %rdi
	jle	.LBB11_16
# BB#11:                                # %.lr.ph.i.i
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	xorl	%edx, %edx
	andq	$3, %rsi
	je	.LBB11_13
	.p2align	4, 0x90
.LBB11_12:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,8), %ecx
	movl	%ecx, (%rbp,%rdx,8)
	movl	4(%rdi,%rdx,8), %ecx
	movl	%ecx, 4(%rbp,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB11_12
.LBB11_13:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB11_17
# BB#14:                                # %.lr.ph.i.i.new
	subq	%rdx, %rax
	leaq	28(%rbp,%rdx,8), %rcx
	leaq	28(%rdi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB11_15:                              # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rcx)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rcx)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	movl	(%rdx), %esi
	movl	%esi, (%rcx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB11_15
	jmp	.LBB11_17
.LBB11_16:                              # %_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_.exit.i
	testq	%rdi, %rdi
	je	.LBB11_20
.LBB11_17:                              # %_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_.exit.thread.i
	cmpb	$0, 56(%rsp)
	je	.LBB11_19
# BB#18:
.Ltmp63:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp64:
.LBB11_19:                              # %.noexc91
	movq	$0, 48(%rsp)
.LBB11_20:                              # %_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv.exit.i
	movb	$1, 56(%rsp)
	movq	%rbp, 48(%rsp)
	movl	$32, 40(%rsp)
.Ltmp69:
	leaq	224(%rsp), %rsi
	leaq	160(%rsp), %rdx
	leaq	32(%rsp), %r9
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	_ZN27btGImpactCollisionAlgorithm29gimpact_vs_gimpact_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceS4_R9btPairSet
.Ltmp70:
	movq	16(%rsp), %rbp          # 8-byte Reload
# BB#21:
	cmpl	$0, 36(%rsp)
	je	.LBB11_27
# BB#22:
	movq	(%r15), %rax
.Ltmp71:
	movq	%r15, %rdi
	callq	*128(%rax)
.Ltmp72:
# BB#23:
	cmpl	$1, %eax
	jne	.LBB11_43
# BB#24:
	movq	(%r14), %rax
.Ltmp73:
	movq	%r14, %rdi
	callq	*128(%rax)
.Ltmp74:
# BB#25:
	cmpl	$1, %eax
	jne	.LBB11_43
# BB#26:
	movq	48(%rsp), %r9
	movl	36(%rsp), %eax
.Ltmp153:
	movl	%eax, (%rsp)
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	_ZN27btGImpactCollisionAlgorithm21collide_sat_trianglesEP17btCollisionObjectS1_P22btGImpactMeshShapePartS3_PKii
.Ltmp154:
	jmp	.LBB11_27
.LBB11_43:
	movq	(%r15), %rax
.Ltmp75:
	movq	%r15, %rdi
	callq	*192(%rax)
.Ltmp76:
# BB#44:
	movq	(%r14), %rax
.Ltmp77:
	movq	%r14, %rdi
	callq	*192(%rax)
.Ltmp78:
# BB#45:
.Ltmp79:
	leaq	800(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface
.Ltmp80:
# BB#46:
.Ltmp82:
	leaq	456(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface
.Ltmp83:
# BB#47:
	movq	(%r15), %rax
.Ltmp85:
	movq	%r15, %rdi
	callq	*152(%rax)
	movb	%al, 15(%rsp)           # 1-byte Spill
.Ltmp86:
# BB#48:
	movq	(%r14), %rax
.Ltmp88:
	movq	%r14, %rdi
	callq	*152(%rax)
	movb	%al, 14(%rsp)           # 1-byte Spill
.Ltmp89:
# BB#49:
	movslq	36(%rsp), %r13
	testq	%r13, %r13
	je	.LBB11_72
# BB#50:                                # %.lr.ph224
	leaq	8(%r12), %rax
	movq	%rax, 440(%rsp)         # 8-byte Spill
	leaq	24(%r12), %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	leaq	40(%r12), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	leaq	56(%r12), %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rcx
	movq	%rcx, 408(%rsp)         # 8-byte Spill
	leaq	24(%rax), %rcx
	movq	%rcx, 400(%rsp)         # 8-byte Spill
	leaq	40(%rax), %rcx
	movq	%rcx, 392(%rsp)         # 8-byte Spill
	leaq	56(%rax), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	leaq	-4(,%r13,8), %rbp
	movq	%r12, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB11_51:                              # =>This Inner Loop Header: Depth=1
	movq	48(%rsp), %rax
	movl	-4(%rax,%rbp), %esi
	movl	%esi, 48(%rbx)
	movl	(%rax,%rbp), %eax
	movl	%eax, 56(%rbx)
	movq	1136(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp91:
	callq	*(%rax)
	movq	%rax, 448(%rsp)         # 8-byte Spill
.Ltmp92:
# BB#52:                                # %_ZN18GIM_ShapeRetriever13getChildShapeEi.exit
                                        #   in Loop: Header=BB11_51 Depth=1
	movl	56(%rbx), %esi
	movq	792(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp94:
	callq	*(%rax)
	movq	%rax, %rdx
.Ltmp95:
# BB#53:                                # %_ZN18GIM_ShapeRetriever13getChildShapeEi.exit94
                                        #   in Loop: Header=BB11_51 Depth=1
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	je	.LBB11_56
# BB#54:                                #   in Loop: Header=BB11_51 Depth=1
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	(%r15), %rax
	movl	48(%rbx), %edx
.Ltmp97:
	leaq	80(%rsp), %rdi
	movq	%r15, %rsi
	callq	*232(%rax)
.Ltmp98:
# BB#55:                                #   in Loop: Header=BB11_51 Depth=1
	movss	224(%rsp), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	228(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movss	84(%rsp), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm0
	mulss	%xmm14, %xmm0
	movss	96(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm6
	movaps	%xmm6, 304(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	112(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	232(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	movaps	%xmm0, %xmm11
	movss	%xmm11, 76(%rsp)        # 4-byte Spill
	mulss	%xmm3, %xmm2
	movaps	%xmm3, %xmm7
	movaps	%xmm7, 320(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm2
	movss	%xmm2, 144(%rsp)        # 4-byte Spill
	movaps	%xmm14, %xmm0
	mulss	%xmm9, %xmm0
	movss	100(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movss	116(%rsp), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 368(%rsp)        # 16-byte Spill
	movss	88(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm4, %xmm0
	movss	104(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	mulss	%xmm1, %xmm3
	addss	%xmm0, %xmm3
	movss	120(%rsp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm8, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 352(%rsp)        # 16-byte Spill
	movss	240(%rsp), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm0
	mulss	%xmm10, %xmm0
	movss	244(%rsp), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm0, %xmm3
	movss	248(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm6, 288(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm0
	movss	%xmm0, 336(%rsp)        # 4-byte Spill
	movaps	%xmm9, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm11
	mulss	%xmm6, %xmm11
	addss	%xmm3, %xmm11
	movaps	%xmm4, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm1, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm8, %xmm7
	mulss	%xmm6, %xmm7
	addss	%xmm3, %xmm7
	movss	256(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm15
	movss	260(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm15, %xmm5
	movss	264(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	76(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm3, %xmm9
	mulss	%xmm0, %xmm2
	addss	%xmm9, %xmm2
	mulss	%xmm6, %xmm12
	addss	%xmm2, %xmm12
	mulss	%xmm3, %xmm4
	mulss	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	mulss	%xmm6, %xmm8
	addss	%xmm1, %xmm8
	unpcklps	%xmm10, %xmm14  # xmm14 = xmm14[0],xmm10[0],xmm14[1],xmm10[1]
	movss	128(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm14, %xmm1
	movaps	304(%rsp), %xmm4        # 16-byte Reload
	unpcklps	%xmm13, %xmm4   # xmm4 = xmm4[0],xmm13[0],xmm4[1],xmm13[1]
	movss	132(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	320(%rsp), %xmm4        # 16-byte Reload
	unpcklps	288(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	136(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	addss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	addss	280(%rsp), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	addps	272(%rsp), %xmm1
	movss	144(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%r12)
	movaps	368(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 12(%r12)
	movaps	352(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 16(%r12)
	movl	$0, 20(%r12)
	movss	336(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%r12)
	movss	%xmm11, 28(%r12)
	movss	%xmm7, 32(%r12)
	movl	$0, 36(%r12)
	movss	%xmm15, 40(%r12)
	movss	%xmm12, 44(%r12)
	movss	%xmm8, 48(%r12)
	movl	$0, 52(%r12)
	movlps	%xmm1, 56(%r12)
	movlps	%xmm2, 64(%r12)
	movq	64(%rsp), %rdx          # 8-byte Reload
.LBB11_56:                              #   in Loop: Header=BB11_51 Depth=1
	cmpb	$0, 14(%rsp)            # 1-byte Folded Reload
	je	.LBB11_59
# BB#57:                                #   in Loop: Header=BB11_51 Depth=1
	movq	%rdx, %r12
	movq	(%r14), %rax
	movl	56(%rbx), %edx
.Ltmp100:
	leaq	80(%rsp), %rdi
	movq	%r14, %rsi
	callq	*232(%rax)
.Ltmp101:
# BB#58:                                #   in Loop: Header=BB11_51 Depth=1
	movss	160(%rsp), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	164(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movss	84(%rsp), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm0
	mulss	%xmm14, %xmm0
	movss	96(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm6
	movaps	%xmm6, 320(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	112(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	168(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	movaps	%xmm0, %xmm11
	movss	%xmm11, 288(%rsp)       # 4-byte Spill
	mulss	%xmm3, %xmm2
	movaps	%xmm3, %xmm7
	movaps	%xmm7, 336(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm2
	movss	%xmm2, 64(%rsp)         # 4-byte Spill
	movaps	%xmm14, %xmm0
	mulss	%xmm9, %xmm0
	movss	100(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movss	116(%rsp), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movss	88(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm4, %xmm0
	movss	104(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	mulss	%xmm1, %xmm3
	addss	%xmm0, %xmm3
	movss	120(%rsp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm8, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 368(%rsp)        # 16-byte Spill
	movss	176(%rsp), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm0
	mulss	%xmm10, %xmm0
	movss	180(%rsp), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm0, %xmm3
	movss	184(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm6, 304(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm0
	movss	%xmm0, 352(%rsp)        # 4-byte Spill
	movaps	%xmm9, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm11
	mulss	%xmm6, %xmm11
	addss	%xmm3, %xmm11
	movaps	%xmm4, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm1, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm8, %xmm7
	mulss	%xmm6, %xmm7
	addss	%xmm3, %xmm7
	movss	192(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm15
	movss	196(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm15, %xmm5
	movss	200(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	288(%rsp), %xmm15       # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm3, %xmm9
	mulss	%xmm0, %xmm2
	addss	%xmm9, %xmm2
	mulss	%xmm6, %xmm12
	addss	%xmm2, %xmm12
	mulss	%xmm3, %xmm4
	mulss	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	mulss	%xmm6, %xmm8
	addss	%xmm1, %xmm8
	unpcklps	%xmm10, %xmm14  # xmm14 = xmm14[0],xmm10[0],xmm14[1],xmm10[1]
	movss	128(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm14, %xmm1
	movaps	320(%rsp), %xmm4        # 16-byte Reload
	unpcklps	%xmm13, %xmm4   # xmm4 = xmm4[0],xmm13[0],xmm4[1],xmm13[1]
	movss	132(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	336(%rsp), %xmm4        # 16-byte Reload
	unpcklps	304(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	136(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	addss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	addss	216(%rsp), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	addps	208(%rsp), %xmm1
	movq	16(%rsp), %rax          # 8-byte Reload
	movss	64(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rax)
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 12(%rax)
	movaps	368(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 16(%rax)
	movl	$0, 20(%rax)
	movss	352(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%rax)
	movss	%xmm11, 28(%rax)
	movss	%xmm7, 32(%rax)
	movl	$0, 36(%rax)
	movss	%xmm15, 40(%rax)
	movss	%xmm12, 44(%rax)
	movss	%xmm8, 48(%rax)
	movl	$0, 52(%rax)
	movlps	%xmm1, 56(%rax)
	movlps	%xmm2, 64(%rax)
	movq	%r12, %rdx
.LBB11_59:                              #   in Loop: Header=BB11_51 Depth=1
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movq	%r15, %r13
	movq	%r14, %r12
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	200(%rax), %r15
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	200(%rbp), %r14
	movq	448(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 200(%rax)
	movq	%rdx, 200(%rbp)
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movl	52(%rbx), %esi
	movl	48(%rbx), %edx
.Ltmp103:
	callq	*16(%rax)
.Ltmp104:
# BB#60:                                # %.noexc121
                                        #   in Loop: Header=BB11_51 Depth=1
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movl	60(%rbx), %esi
	movl	56(%rbx), %edx
.Ltmp105:
	callq	*24(%rax)
.Ltmp106:
# BB#61:                                # %.noexc122
                                        #   in Loop: Header=BB11_51 Depth=1
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB11_67
# BB#62:                                #   in Loop: Header=BB11_51 Depth=1
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB11_65
# BB#63:                                #   in Loop: Header=BB11_51 Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp107:
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	callq	*24(%rax)
.Ltmp108:
# BB#64:                                # %.noexc123
                                        #   in Loop: Header=BB11_51 Depth=1
	movq	%rax, 24(%rbx)
.LBB11_65:                              # %_ZN27btGImpactCollisionAlgorithm12newAlgorithmEP17btCollisionObjectS1_.exit.i.i
                                        #   in Loop: Header=BB11_51 Depth=1
	movq	32(%rbx), %rcx
	movq	%rax, 8(%rcx)
	movq	8(%rbx), %rdi
	movq	(%rdi), %r8
.Ltmp109:
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%rax, %rcx
	callq	*16(%r8)
.Ltmp110:
# BB#66:                                # %.noexc124
                                        #   in Loop: Header=BB11_51 Depth=1
	movq	%rax, 16(%rbx)
.LBB11_67:                              # %_ZN27btGImpactCollisionAlgorithm20checkConvexAlgorithmEP17btCollisionObjectS1_.exit.i
                                        #   in Loop: Header=BB11_51 Depth=1
	movq	(%rax), %rbp
	movq	32(%rbx), %r8
	movq	40(%rbx), %rcx
.Ltmp111:
	movq	%rax, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	*16(%rbp)
	movq	16(%rsp), %rcx          # 8-byte Reload
.Ltmp112:
# BB#68:                                #   in Loop: Header=BB11_51 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%r15, 200(%rax)
	movq	%r14, 200(%rcx)
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	je	.LBB11_70
# BB#69:                                #   in Loop: Header=BB11_51 Depth=1
	movaps	224(%rsp), %xmm0
	movq	440(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	leaq	240(%rsp), %rax
	movq	%rax, %rdx
	movups	(%rdx), %xmm0
	movq	432(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	movups	16(%rdx), %xmm0
	movq	424(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	movups	32(%rdx), %xmm0
	movq	416(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
.LBB11_70:                              #   in Loop: Header=BB11_51 Depth=1
	cmpb	$0, 14(%rsp)            # 1-byte Folded Reload
	movq	%r12, %r14
	movq	%r13, %r15
	je	.LBB11_71
# BB#85:                                #   in Loop: Header=BB11_51 Depth=1
	movaps	160(%rsp), %xmm0
	movq	408(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	leaq	176(%rsp), %rax
	movq	%rax, %rcx
	movups	(%rcx), %xmm0
	movq	400(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	movups	16(%rcx), %xmm0
	movq	392(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	movups	32(%rcx), %xmm0
	movq	384(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
.LBB11_71:                              # %.backedge
                                        #   in Loop: Header=BB11_51 Depth=1
	movq	144(%rsp), %rbp         # 8-byte Reload
	addq	$-8, %rbp
	movq	64(%rsp), %r13          # 8-byte Reload
	decl	%r13d
	movq	24(%rsp), %r12          # 8-byte Reload
	jne	.LBB11_51
.LBB11_72:                              # %._crit_edge
	movq	(%r15), %rax
.Ltmp114:
	movq	%r15, %rdi
	callq	*200(%rax)
.Ltmp115:
# BB#73:
	movq	(%r14), %rax
.Ltmp116:
	movq	%r14, %rdi
	callq	*200(%rax)
.Ltmp117:
# BB#74:
	leaq	576(%rsp), %rdi
.Ltmp127:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp128:
# BB#75:
	leaq	464(%rsp), %rdi
.Ltmp133:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp134:
# BB#76:                                # %_ZN18GIM_ShapeRetrieverD2Ev.exit129
	leaq	920(%rsp), %rdi
.Ltmp144:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp145:
# BB#77:
	leaq	808(%rsp), %rdi
.Ltmp150:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp151:
.LBB11_27:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_8
# BB#28:
	cmpb	$0, 56(%rsp)
	je	.LBB11_30
# BB#29:
	callq	_Z21btAlignedFreeInternalPv
.LBB11_30:
	movq	$0, 48(%rsp)
.LBB11_8:                               # %.loopexit
	addq	$1144, %rsp             # imm = 0x478
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_42:
.Ltmp155:
	jmp	.LBB11_38
.LBB11_96:
.Ltmp152:
	jmp	.LBB11_38
.LBB11_88:
.Ltmp146:
	movq	%rax, %rbx
	leaq	808(%rsp), %rdi
.Ltmp147:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp148:
	jmp	.LBB11_39
.LBB11_89:
.Ltmp149:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_90:
.Ltmp135:
	jmp	.LBB11_98
.LBB11_86:
.Ltmp129:
	movq	%rax, %rbx
	leaq	464(%rsp), %rdi
.Ltmp130:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp131:
	jmp	.LBB11_99
.LBB11_87:
.Ltmp132:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_78:
.Ltmp90:
	jmp	.LBB11_92
.LBB11_91:
.Ltmp87:
	jmp	.LBB11_92
.LBB11_97:
.Ltmp84:
.LBB11_98:
	movq	%rax, %rbx
	jmp	.LBB11_99
.LBB11_79:
.Ltmp118:
	jmp	.LBB11_92
.LBB11_31:
.Ltmp65:
	movq	%rax, %rbx
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_35
# BB#32:
	cmpb	$0, 56(%rsp)
	je	.LBB11_34
# BB#33:
.Ltmp66:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp67:
	jmp	.LBB11_34
.LBB11_36:
.Ltmp68:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_37:
.Ltmp81:
.LBB11_38:
	movq	%rax, %rbx
	jmp	.LBB11_39
.LBB11_83:
.Ltmp102:
	jmp	.LBB11_92
.LBB11_82:
.Ltmp99:
	jmp	.LBB11_92
.LBB11_80:
.Ltmp93:
	jmp	.LBB11_92
.LBB11_81:
.Ltmp96:
	jmp	.LBB11_92
.LBB11_84:
.Ltmp113:
.LBB11_92:
	movq	%rax, %rbx
	leaq	576(%rsp), %rdi
.Ltmp119:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp120:
# BB#93:
	leaq	464(%rsp), %rdi
.Ltmp125:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp126:
.LBB11_99:
	leaq	920(%rsp), %rdi
.Ltmp136:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp137:
# BB#100:
	leaq	808(%rsp), %rdi
.Ltmp142:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp143:
.LBB11_39:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_35
# BB#40:
	cmpb	$0, 56(%rsp)
	je	.LBB11_34
# BB#41:
.Ltmp156:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp157:
.LBB11_34:                              # %.noexc.i
	movq	$0, 48(%rsp)
.LBB11_35:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB11_94:
.Ltmp121:
	movq	%rax, %rbx
	leaq	464(%rsp), %rdi
.Ltmp122:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp123:
	jmp	.LBB11_104
.LBB11_95:
.Ltmp124:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_101:
.Ltmp138:
	movq	%rax, %rbx
	leaq	808(%rsp), %rdi
.Ltmp139:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp140:
	jmp	.LBB11_104
.LBB11_102:
.Ltmp141:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_103:
.Ltmp158:
	movq	%rax, %rbx
.LBB11_104:                             # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEP17btCollisionObjectS1_P23btGImpactShapeInterfaceS3_, .Lfunc_end11-_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEP17btCollisionObjectS1_P23btGImpactShapeInterfaceS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\202\203\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\371\002"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp61-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp64-.Ltmp61         #   Call between .Ltmp61 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin3   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp74-.Ltmp69         #   Call between .Ltmp69 and .Ltmp74
	.long	.Ltmp81-.Lfunc_begin3   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp154-.Ltmp153       #   Call between .Ltmp153 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin3  #     jumps to .Ltmp155
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp80-.Ltmp75         #   Call between .Ltmp75 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin3   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin3   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin3   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin3   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin3   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin3   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin3   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin3  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp112-.Ltmp103       #   Call between .Ltmp103 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin3  #     jumps to .Ltmp113
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp117-.Ltmp114       #   Call between .Ltmp114 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin3  #     jumps to .Ltmp118
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin3  #     jumps to .Ltmp129
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin3  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin3  #     jumps to .Ltmp146
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp151-.Ltmp150       #   Call between .Ltmp150 and .Ltmp151
	.long	.Ltmp152-.Lfunc_begin3  #     jumps to .Ltmp152
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin3  # >> Call Site 19 <<
	.long	.Ltmp147-.Ltmp151       #   Call between .Ltmp151 and .Ltmp147
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp147-.Lfunc_begin3  # >> Call Site 20 <<
	.long	.Ltmp148-.Ltmp147       #   Call between .Ltmp147 and .Ltmp148
	.long	.Ltmp149-.Lfunc_begin3  #     jumps to .Ltmp149
	.byte	1                       #   On action: 1
	.long	.Ltmp130-.Lfunc_begin3  # >> Call Site 21 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin3  #     jumps to .Ltmp132
	.byte	1                       #   On action: 1
	.long	.Ltmp66-.Lfunc_begin3   # >> Call Site 22 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin3   #     jumps to .Ltmp68
	.byte	1                       #   On action: 1
	.long	.Ltmp119-.Lfunc_begin3  # >> Call Site 23 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin3  #     jumps to .Ltmp121
	.byte	1                       #   On action: 1
	.long	.Ltmp125-.Lfunc_begin3  # >> Call Site 24 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp158-.Lfunc_begin3  #     jumps to .Ltmp158
	.byte	1                       #   On action: 1
	.long	.Ltmp136-.Lfunc_begin3  # >> Call Site 25 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin3  #     jumps to .Ltmp138
	.byte	1                       #   On action: 1
	.long	.Ltmp142-.Lfunc_begin3  # >> Call Site 26 <<
	.long	.Ltmp157-.Ltmp142       #   Call between .Ltmp142 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin3  #     jumps to .Ltmp158
	.byte	1                       #   On action: 1
	.long	.Ltmp157-.Lfunc_begin3  # >> Call Site 27 <<
	.long	.Ltmp122-.Ltmp157       #   Call between .Ltmp157 and .Ltmp122
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp122-.Lfunc_begin3  # >> Call Site 28 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin3  #     jumps to .Ltmp124
	.byte	1                       #   On action: 1
	.long	.Ltmp139-.Lfunc_begin3  # >> Call Site 29 <<
	.long	.Ltmp140-.Ltmp139       #   Call between .Ltmp139 and .Ltmp140
	.long	.Ltmp141-.Lfunc_begin3  #     jumps to .Ltmp141
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface,"axG",@progbits,_ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface,comdat
	.weak	_ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface
	.p2align	4, 0x90
	.type	_ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface,@function
_ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface: # @_ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi115:
	.cfi_def_cfa_offset 48
.Lcfi116:
	.cfi_offset %rbx, -40
.Lcfi117:
	.cfi_offset %r12, -32
.Lcfi118:
	.cfi_offset %r14, -24
.Lcfi119:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %rbx
	leaq	8(%rbx), %r14
	movq	%r14, %rdi
	callq	_ZN23btPolyhedralConvexShapeC2Ev
	movl	$1, 16(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 88(%rbx)
	movups	%xmm0, 72(%rbx)
	movq	$_ZTV17btTriangleShapeEx+16, 8(%rbx)
	leaq	120(%rbx), %r15
.Ltmp159:
	movq	%r15, %rdi
	callq	_ZN16btBU_Simplex1to4C2Ev
.Ltmp160:
# BB#1:
	movq	$_ZTV20btTetrahedronShapeEx+16, 120(%rbx)
	movl	$4, 220(%rbx)
	movq	$_ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE+16, 288(%rbx)
	movq	$_ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE+16, 304(%rbx)
	movq	$_ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE+16, 320(%rbx)
	movq	%r12, (%rbx)
	movq	(%r12), %rax
.Ltmp162:
	movq	%r12, %rdi
	callq	*160(%rax)
.Ltmp163:
# BB#2:
	testb	%al, %al
	je	.LBB12_5
# BB#3:
	leaq	304(%rbx), %rcx
	jmp	.LBB12_7
.LBB12_5:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp164:
	callq	*168(%rax)
.Ltmp165:
# BB#6:
	leaq	288(%rbx), %rcx
	leaq	320(%rbx), %rdx
	testb	%al, %al
	cmovneq	%rdx, %rcx
.LBB12_7:
	movq	%rcx, 336(%rbx)
	movq	%rbx, 8(%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB12_8:
.Ltmp161:
	movq	%rax, %rbx
	jmp	.LBB12_9
.LBB12_4:
.Ltmp166:
	movq	%rax, %rbx
.Ltmp167:
	movq	%r15, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp168:
.LBB12_9:
.Ltmp169:
	movq	%r14, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp170:
# BB#10:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB12_11:
.Ltmp171:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface, .Lfunc_end12-_ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp159-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp159
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp160-.Ltmp159       #   Call between .Ltmp159 and .Ltmp160
	.long	.Ltmp161-.Lfunc_begin4  #     jumps to .Ltmp161
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp165-.Ltmp162       #   Call between .Ltmp162 and .Ltmp165
	.long	.Ltmp166-.Lfunc_begin4  #     jumps to .Ltmp166
	.byte	0                       #   On action: cleanup
	.long	.Ltmp167-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp170-.Ltmp167       #   Call between .Ltmp167 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin4  #     jumps to .Ltmp171
	.byte	1                       #   On action: 1
	.long	.Ltmp170-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Lfunc_end12-.Ltmp170   #   Call between .Ltmp170 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP16btCollisionShapeb
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP16btCollisionShapeb,@function
_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP16btCollisionShapeb: # @_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP16btCollisionShapeb
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi124:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 56
	subq	$760, %rsp              # imm = 0x2F8
.Lcfi126:
	.cfi_def_cfa_offset 816
.Lcfi127:
	.cfi_offset %rbx, -56
.Lcfi128:
	.cfi_offset %r12, -48
.Lcfi129:
	.cfi_offset %r13, -40
.Lcfi130:
	.cfi_offset %r14, -32
.Lcfi131:
	.cfi_offset %r15, -24
.Lcfi132:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%rcx, %rbx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r12
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*128(%rax)
	cmpl	$2, %eax
	jne	.LBB13_5
# BB#1:
	leaq	60(%r12), %rax
	leaq	52(%r12), %r15
	testb	%r13b, %r13b
	cmovneq	%rax, %r15
	movl	188(%rbx), %ecx
	leal	-1(%rcx), %eax
	movl	%eax, (%r15)
	testl	%ecx, %ecx
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	%r13d, %ecx
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB13_4
# BB#2:                                 # %.lr.ph
	movzbl	%cl, %ebp
	.p2align	4, 0x90
.LBB13_3:                               # =>This Inner Loop Header: Depth=1
	movq	200(%rbx), %rcx
	cltq
	movq	(%rcx,%rax,8), %rcx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	88(%rsp), %r8           # 8-byte Reload
	movl	%ebp, %r9d
	callq	_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP16btCollisionShapeb
	movl	(%r15), %ecx
	leal	-1(%rcx), %eax
	movl	%eax, (%r15)
	testl	%ecx, %ecx
	jne	.LBB13_3
.LBB13_4:                               # %.loopexit
	addq	$760, %rsp              # imm = 0x2F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_5:
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	*128(%rax)
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r14
	movl	8(%rcx), %ecx
	cmpl	$1, %eax
	jne	.LBB13_7
# BB#6:
	cmpl	$28, %ecx
	jne	.LBB13_7
# BB#45:
	movzbl	%r13b, %r9d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %r8
	addq	$760, %rsp              # imm = 0x2F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN27btGImpactCollisionAlgorithm37gimpacttrimeshpart_vs_plane_collisionEP17btCollisionObjectS1_P22btGImpactMeshShapePartP18btStaticPlaneShapeb # TAILCALL
.LBB13_7:                               # %._crit_edge139
	cmpl	$31, %ecx
	jne	.LBB13_8
# BB#46:
	movzbl	%r13b, %r9d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %r8
	addq	$760, %rsp              # imm = 0x2F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN27btGImpactCollisionAlgorithm24gimpact_vs_compoundshapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP15btCompoundShapeb # TAILCALL
.LBB13_8:
	addl	$-21, %ecx
	cmpl	$8, %ecx
	ja	.LBB13_9
# BB#47:
	movzbl	%r13b, %r9d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %r8
	addq	$760, %rsp              # imm = 0x2F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN27btGImpactCollisionAlgorithm18gimpact_vs_concaveEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP14btConcaveShapeb # TAILCALL
.LBB13_9:
	movq	8(%rsp), %rax           # 8-byte Reload
	movups	8(%rax), %xmm0
	movaps	%xmm0, 96(%rsp)
	movups	24(%rax), %xmm0
	movaps	%xmm0, 112(%rsp)
	movups	40(%rax), %xmm0
	movaps	%xmm0, 128(%rsp)
	movups	56(%rax), %xmm0
	movaps	%xmm0, 144(%rsp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movups	8(%rax), %xmm0
	movaps	%xmm0, 352(%rsp)
	movups	24(%rax), %xmm0
	movaps	%xmm0, 368(%rsp)
	movups	40(%rax), %xmm0
	movaps	%xmm0, 384(%rsp)
	movups	56(%rax), %xmm0
	movaps	%xmm0, 400(%rsp)
	movb	$1, 80(%rsp)
	movq	$0, 72(%rsp)
	movq	$0, 60(%rsp)
.Ltmp172:
	leaq	96(%rsp), %rsi
	leaq	352(%rsp), %rdx
	leaq	56(%rsp), %r9
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %r8
	callq	_ZN27btGImpactCollisionAlgorithm27gimpact_vs_shape_find_pairsERK11btTransformS2_P23btGImpactShapeInterfaceP16btCollisionShapeR20btAlignedObjectArrayIiE
.Ltmp173:
# BB#10:
	cmpl	$0, 60(%rsp)
	je	.LBB13_31
# BB#11:
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rax
.Ltmp174:
	movq	%rbx, %rdi
	callq	*192(%rax)
.Ltmp175:
# BB#12:
.Ltmp176:
	leaq	416(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN18GIM_ShapeRetrieverC2EP23btGImpactShapeInterface
.Ltmp177:
# BB#13:
	movq	(%rbx), %rax
.Ltmp179:
	movq	%rbx, %rdi
	callq	*152(%rax)
	movb	%al, 7(%rsp)            # 1-byte Spill
.Ltmp180:
# BB#14:
	movslq	60(%rsp), %r12
	testq	%r12, %r12
	je	.LBB13_28
# BB#15:                                # %.lr.ph138
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	8(%rax), %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	leaq	24(%rax), %rcx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	leaq	40(%rax), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	leaq	56(%rax), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	48(%rcx), %rax
	leaq	56(%rcx), %rcx
	testb	%r13b, %r13b
	cmoveq	%rax, %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	leaq	-4(,%r12,4), %rcx
	.p2align	4, 0x90
.LBB13_16:                              # =>This Inner Loop Header: Depth=1
	movq	72(%rsp), %rax
	movq	%rcx, %r15
	movl	(%rax,%rcx), %ebp
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	%ebp, (%rax)
	movq	752(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp182:
	movl	%ebp, %esi
	callq	*(%rax)
	movq	%rax, %rbx
.Ltmp183:
# BB#17:                                # %_ZN18GIM_ShapeRetriever13getChildShapeEi.exit
                                        #   in Loop: Header=BB13_16 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB13_20
# BB#18:                                #   in Loop: Header=BB13_16 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi), %rax
.Ltmp185:
	leaq	208(%rsp), %rdi
	movl	%ebp, %edx
	callq	*232(%rax)
.Ltmp186:
# BB#19:                                #   in Loop: Header=BB13_16 Depth=1
	movss	96(%rsp), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	100(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	208(%rsp), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movss	212(%rsp), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm0
	mulss	%xmm14, %xmm0
	movss	224(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm6
	movaps	%xmm6, 288(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	240(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	movaps	%xmm0, %xmm11
	movss	%xmm11, 44(%rsp)        # 4-byte Spill
	mulss	%xmm3, %xmm2
	movaps	%xmm3, %xmm7
	movaps	%xmm7, 304(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm2
	movss	%xmm2, 52(%rsp)         # 4-byte Spill
	movaps	%xmm14, %xmm0
	mulss	%xmm9, %xmm0
	movss	228(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movss	244(%rsp), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 336(%rsp)        # 16-byte Spill
	movss	216(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm4, %xmm0
	movss	232(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	mulss	%xmm1, %xmm3
	addss	%xmm0, %xmm3
	movss	248(%rsp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm8, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 320(%rsp)        # 16-byte Spill
	movss	112(%rsp), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm0
	mulss	%xmm10, %xmm0
	movss	116(%rsp), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm0, %xmm3
	movss	120(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm6, 272(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm0
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	movaps	%xmm9, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm11
	mulss	%xmm6, %xmm11
	addss	%xmm3, %xmm11
	movaps	%xmm4, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm1, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm8, %xmm7
	mulss	%xmm6, %xmm7
	addss	%xmm3, %xmm7
	movss	128(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm15
	movss	132(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm15, %xmm5
	movss	136(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	44(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm3, %xmm9
	mulss	%xmm0, %xmm2
	addss	%xmm9, %xmm2
	mulss	%xmm6, %xmm12
	addss	%xmm2, %xmm12
	mulss	%xmm3, %xmm4
	mulss	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	mulss	%xmm6, %xmm8
	addss	%xmm1, %xmm8
	unpcklps	%xmm10, %xmm14  # xmm14 = xmm14[0],xmm10[0],xmm14[1],xmm10[1]
	movss	256(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm14, %xmm1
	movaps	288(%rsp), %xmm4        # 16-byte Reload
	unpcklps	%xmm13, %xmm4   # xmm4 = xmm4[0],xmm13[0],xmm4[1],xmm13[1]
	movss	260(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	304(%rsp), %xmm4        # 16-byte Reload
	unpcklps	272(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	264(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	addss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	addss	152(%rsp), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	addps	144(%rsp), %xmm1
	movq	8(%rsp), %rax           # 8-byte Reload
	movss	52(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rax)
	movaps	336(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 12(%rax)
	movaps	320(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 16(%rax)
	movl	$0, 20(%rax)
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%rax)
	movss	%xmm11, 28(%rax)
	movss	%xmm7, 32(%rax)
	movl	$0, 36(%rax)
	movss	%xmm15, 40(%rax)
	movss	%xmm12, 44(%rax)
	movss	%xmm8, 48(%rax)
	movl	$0, 52(%rax)
	movlps	%xmm1, 56(%rax)
	movlps	%xmm2, 64(%rax)
.LBB13_20:                              #   in Loop: Header=BB13_16 Depth=1
	testb	%r13b, %r13b
	je	.LBB13_25
# BB#21:                                #   in Loop: Header=BB13_16 Depth=1
.Ltmp190:
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r14, %rcx
	movq	%rbx, %r8
	callq	_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_
.Ltmp191:
	jmp	.LBB13_26
	.p2align	4, 0x90
.LBB13_25:                              #   in Loop: Header=BB13_16 Depth=1
.Ltmp188:
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	%r14, %r8
	callq	_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEP17btCollisionObjectS1_P16btCollisionShapeS3_
.Ltmp189:
.LBB13_26:                              #   in Loop: Header=BB13_16 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB13_27
# BB#56:                                #   in Loop: Header=BB13_16 Depth=1
	movaps	96(%rsp), %xmm0
	movq	192(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	leaq	112(%rsp), %rax
	movq	%rax, %rcx
	movups	(%rcx), %xmm0
	movq	184(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	movups	16(%rcx), %xmm0
	movq	176(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	movups	32(%rcx), %xmm0
	movq	168(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
.LBB13_27:                              # %.backedge
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	%r15, %rcx
	addq	$-4, %rcx
	decl	%r12d
	jne	.LBB13_16
.LBB13_28:                              # %._crit_edge
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp193:
	callq	*200(%rax)
.Ltmp194:
# BB#29:
	leaq	536(%rsp), %rdi
.Ltmp204:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp205:
# BB#30:
	leaq	424(%rsp), %rdi
.Ltmp210:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp211:
.LBB13_31:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_4
# BB#32:
	cmpb	$0, 80(%rsp)
	je	.LBB13_34
# BB#33:
	callq	_Z21btAlignedFreeInternalPv
.LBB13_34:
	movq	$0, 72(%rsp)
	jmp	.LBB13_4
.LBB13_37:
.Ltmp212:
	jmp	.LBB13_49
.LBB13_35:
.Ltmp206:
	movq	%rax, %rbx
	leaq	424(%rsp), %rdi
.Ltmp207:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp208:
	jmp	.LBB13_50
.LBB13_36:
.Ltmp209:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_55:
.Ltmp195:
	jmp	.LBB13_39
.LBB13_38:
.Ltmp181:
	jmp	.LBB13_39
.LBB13_48:
.Ltmp178:
.LBB13_49:
	movq	%rax, %rbx
	jmp	.LBB13_50
.LBB13_23:
.Ltmp187:
	jmp	.LBB13_39
.LBB13_22:
.Ltmp184:
	jmp	.LBB13_39
.LBB13_24:
.Ltmp192:
.LBB13_39:
	movq	%rax, %rbx
	leaq	536(%rsp), %rdi
.Ltmp196:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp197:
# BB#40:
	leaq	424(%rsp), %rdi
.Ltmp202:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp203:
.LBB13_50:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_54
# BB#51:
	cmpb	$0, 80(%rsp)
	je	.LBB13_53
# BB#52:
.Ltmp213:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp214:
.LBB13_53:                              # %.noexc
	movq	$0, 72(%rsp)
.LBB13_54:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB13_41:
.Ltmp198:
	movq	%rax, %rbx
	leaq	424(%rsp), %rdi
.Ltmp199:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp200:
	jmp	.LBB13_44
.LBB13_42:
.Ltmp201:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_43:
.Ltmp215:
	movq	%rax, %rbx
.LBB13_44:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP16btCollisionShapeb, .Lfunc_end13-_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP16btCollisionShapeb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\314\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp172-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp172
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp177-.Ltmp172       #   Call between .Ltmp172 and .Ltmp177
	.long	.Ltmp178-.Lfunc_begin5  #     jumps to .Ltmp178
	.byte	0                       #   On action: cleanup
	.long	.Ltmp179-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp180-.Ltmp179       #   Call between .Ltmp179 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin5  #     jumps to .Ltmp181
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp183-.Ltmp182       #   Call between .Ltmp182 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin5  #     jumps to .Ltmp184
	.byte	0                       #   On action: cleanup
	.long	.Ltmp185-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp186-.Ltmp185       #   Call between .Ltmp185 and .Ltmp186
	.long	.Ltmp187-.Lfunc_begin5  #     jumps to .Ltmp187
	.byte	0                       #   On action: cleanup
	.long	.Ltmp190-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp189-.Ltmp190       #   Call between .Ltmp190 and .Ltmp189
	.long	.Ltmp192-.Lfunc_begin5  #     jumps to .Ltmp192
	.byte	0                       #   On action: cleanup
	.long	.Ltmp193-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin5  #     jumps to .Ltmp195
	.byte	0                       #   On action: cleanup
	.long	.Ltmp204-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp205-.Ltmp204       #   Call between .Ltmp204 and .Ltmp205
	.long	.Ltmp206-.Lfunc_begin5  #     jumps to .Ltmp206
	.byte	0                       #   On action: cleanup
	.long	.Ltmp210-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp211-.Ltmp210       #   Call between .Ltmp210 and .Ltmp211
	.long	.Ltmp212-.Lfunc_begin5  #     jumps to .Ltmp212
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp207-.Ltmp211       #   Call between .Ltmp211 and .Ltmp207
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp207-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp208-.Ltmp207       #   Call between .Ltmp207 and .Ltmp208
	.long	.Ltmp209-.Lfunc_begin5  #     jumps to .Ltmp209
	.byte	1                       #   On action: 1
	.long	.Ltmp196-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp197-.Ltmp196       #   Call between .Ltmp196 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin5  #     jumps to .Ltmp198
	.byte	1                       #   On action: 1
	.long	.Ltmp202-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp214-.Ltmp202       #   Call between .Ltmp202 and .Ltmp214
	.long	.Ltmp215-.Lfunc_begin5  #     jumps to .Ltmp215
	.byte	1                       #   On action: 1
	.long	.Ltmp214-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp199-.Ltmp214       #   Call between .Ltmp214 and .Ltmp199
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp199-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp200-.Ltmp199       #   Call between .Ltmp199 and .Ltmp200
	.long	.Ltmp201-.Lfunc_begin5  #     jumps to .Ltmp201
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	1056964608              # float 0.5
.LCPI14_2:
	.long	897988541               # float 9.99999997E-7
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI14_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN27btGImpactCollisionAlgorithm37gimpacttrimeshpart_vs_plane_collisionEP17btCollisionObjectS1_P22btGImpactMeshShapePartP18btStaticPlaneShapeb
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm37gimpacttrimeshpart_vs_plane_collisionEP17btCollisionObjectS1_P22btGImpactMeshShapePartP18btStaticPlaneShapeb,@function
_ZN27btGImpactCollisionAlgorithm37gimpacttrimeshpart_vs_plane_collisionEP17btCollisionObjectS1_P22btGImpactMeshShapePartP18btStaticPlaneShapeb: # @_ZN27btGImpactCollisionAlgorithm37gimpacttrimeshpart_vs_plane_collisionEP17btCollisionObjectS1_P22btGImpactMeshShapePartP18btStaticPlaneShapeb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi136:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi137:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi139:
	.cfi_def_cfa_offset 240
.Lcfi140:
	.cfi_offset %rbx, -56
.Lcfi141:
	.cfi_offset %r12, -48
.Lcfi142:
	.cfi_offset %r13, -40
.Lcfi143:
	.cfi_offset %r14, -32
.Lcfi144:
	.cfi_offset %r15, -24
.Lcfi145:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%r8, %r13
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movups	8(%r15), %xmm0
	movaps	%xmm0, 96(%rsp)
	movups	24(%r15), %xmm0
	movaps	%xmm0, 112(%rsp)
	movups	40(%r15), %xmm0
	movaps	%xmm0, 128(%rsp)
	movups	56(%r15), %xmm0
	movaps	%xmm0, 144(%rsp)
	movss	44(%r12), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	12(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	64(%r12), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	28(%r12), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	60(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	64(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	68(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	16(%r12), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm12
	movss	24(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm5
	movss	32(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	movss	40(%r12), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm11
	mulss	48(%r12), %xmm2
	mulss	56(%r12), %xmm6
	movss	60(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm6, %xmm7
	unpcklps	%xmm9, %xmm10   # xmm10 = xmm10[0],xmm9[0],xmm10[1],xmm9[1]
	unpcklps	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1]
	unpcklps	%xmm10, %xmm3   # xmm3 = xmm3[0],xmm10[0],xmm3[1],xmm10[1]
	movsd	64(%r13), %xmm6         # xmm6 = mem[0],zero
	unpcklps	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1]
	unpcklps	%xmm6, %xmm1    # xmm1 = xmm1[0],xmm6[0],xmm1[1],xmm6[1]
	mulps	%xmm3, %xmm1
	unpcklps	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1]
	unpcklps	%xmm7, %xmm5    # xmm5 = xmm5[0],xmm7[0],xmm5[1],xmm7[1]
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	addps	%xmm1, %xmm0
	unpcklps	%xmm2, %xmm12   # xmm12 = xmm12[0],xmm2[0],xmm12[1],xmm2[1]
	movss	76(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	unpcklps	%xmm4, %xmm12   # xmm12 = xmm12[0],xmm4[0],xmm12[1],xmm4[1]
	addps	%xmm0, %xmm12
	movaps	%xmm12, 64(%rsp)        # 16-byte Spill
	movaps	%xmm12, 80(%rsp)
	movq	(%rbx), %rax
	leaq	48(%rsp), %rcx
	leaq	96(%rsp), %rsi
	leaq	32(%rsp), %rdx
	movq	%rbx, %rdi
	callq	*16(%rax)
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*88(%rax)
	movss	32(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm3
	movss	%xmm3, 32(%rsp)
	movss	36(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm2
	movss	%xmm2, 36(%rsp)
	movss	40(%rsp), %xmm11        # xmm11 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm11
	movss	%xmm11, 40(%rsp)
	movss	48(%rsp), %xmm9         # xmm9 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm9
	movss	%xmm9, 48(%rsp)
	movss	52(%rsp), %xmm8         # xmm8 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm8
	movss	%xmm8, 52(%rsp)
	addss	56(%rsp), %xmm0
	movss	%xmm0, 56(%rsp)
	addss	%xmm9, %xmm3
	addss	%xmm8, %xmm2
	addss	%xmm0, %xmm11
	movss	.LCPI14_0(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	mulss	%xmm4, %xmm2
	mulss	%xmm4, %xmm11
	subss	%xmm3, %xmm9
	subss	%xmm2, %xmm8
	subss	%xmm11, %xmm0
	movaps	.LCPI14_1(%rip), %xmm4  # xmm4 = [nan,nan,nan,nan]
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm5
	andps	%xmm4, %xmm5
	movaps	%xmm1, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm6, %xmm7
	andps	%xmm4, %xmm7
	movaps	%xmm1, %xmm10
	movhlps	%xmm10, %xmm10          # xmm10 = xmm10[1,1]
	andps	%xmm10, %xmm4
	unpcklps	%xmm9, %xmm3    # xmm3 = xmm3[0],xmm9[0],xmm3[1],xmm9[1]
	shufps	$0, %xmm1, %xmm5        # xmm5 = xmm5[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm5      # xmm5 = xmm5[2,0],xmm1[2,3]
	mulps	%xmm3, %xmm5
	unpcklps	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1]
	shufps	$0, %xmm6, %xmm7        # xmm7 = xmm7[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm7      # xmm7 = xmm7[2,0],xmm6[2,3]
	mulps	%xmm2, %xmm7
	addps	%xmm5, %xmm7
	movaps	%xmm1, %xmm2
	unpcklps	%xmm0, %xmm11   # xmm11 = xmm11[0],xmm0[0],xmm11[1],xmm0[1]
	shufps	$0, %xmm10, %xmm4       # xmm4 = xmm4[0,0],xmm10[0,0]
	shufps	$226, %xmm10, %xmm4     # xmm4 = xmm4[2,0],xmm10[2,3]
	mulps	%xmm11, %xmm4
	addps	%xmm7, %xmm4
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	addss	.LCPI14_2(%rip), %xmm1
	shufps	$231, %xmm2, %xmm2      # xmm2 = xmm2[3,1,2,3]
	ucomiss	%xmm1, %xmm2
	ja	.LBB14_15
# BB#1:
	subss	%xmm0, %xmm4
	addss	.LCPI14_2(%rip), %xmm2
	ucomiss	%xmm4, %xmm2
	jb	.LBB14_15
# BB#2:                                 # %_ZNK6btAABB14plane_classifyERK9btVector4.exit
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*192(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*88(%rax)
	movss	12(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movl	232(%rbx), %r13d
	testl	%r13d, %r13d
	je	.LBB14_14
# BB#3:                                 # %.lr.ph
	addss	%xmm0, %xmm8
	decl	%r13d
	xorps	%xmm7, %xmm7
	movss	%xmm8, 12(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB14_4:                               # =>This Inner Loop Header: Depth=1
	movq	224(%rbx), %rax
	movslq	240(%rbx), %rdx
	movslq	%r13d, %rcx
	imulq	%rdx, %rcx
	movss	200(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	cmpl	$1, 236(%rbx)
	jne	.LBB14_6
# BB#5:                                 #   in Loop: Header=BB14_4 Depth=1
	cvtss2sd	%xmm0, %xmm0
	mulsd	(%rax,%rcx), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 16(%rsp)
	movss	204(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	8(%rax,%rcx), %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 20(%rsp)
	movss	208(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	16(%rax,%rcx), %xmm2
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm2, %xmm4
	jmp	.LBB14_7
	.p2align	4, 0x90
.LBB14_6:                               #   in Loop: Header=BB14_4 Depth=1
	mulss	(%rax,%rcx), %xmm0
	movss	%xmm0, 16(%rsp)
	movss	4(%rax,%rcx), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	204(%rbx), %xmm1
	movss	%xmm1, 20(%rsp)
	movss	8(%rax,%rcx), %xmm4     # xmm4 = mem[0],zero,zero,zero
	mulss	208(%rbx), %xmm4
.LBB14_7:                               # %_ZNK22btGImpactMeshShapePart9getVertexEiR9btVector3.exit
                                        #   in Loop: Header=BB14_4 Depth=1
	movss	112(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	100(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	movss	116(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm4, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	120(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	mulps	%xmm6, %xmm2
	addps	%xmm3, %xmm2
	addps	144(%rsp), %xmm2
	mulss	128(%rsp), %xmm0
	mulss	132(%rsp), %xmm1
	addss	%xmm0, %xmm1
	mulss	136(%rsp), %xmm4
	addss	%xmm1, %xmm4
	addss	152(%rsp), %xmm4
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm2, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	movaps	80(%rsp), %xmm0
	movaps	%xmm0, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	mulss	%xmm0, %xmm2
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	88(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	subss	92(%rsp), %xmm4
	subss	%xmm8, %xmm4
	ucomiss	%xmm4, %xmm7
	jbe	.LBB14_13
# BB#8:                                 #   in Loop: Header=BB14_4 Depth=1
	testb	%r14b, %r14b
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	je	.LBB14_16
# BB#9:                                 #   in Loop: Header=BB14_4 Depth=1
	movaps	.LCPI14_3(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm0
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 168(%rsp)
	movlps	%xmm2, 176(%rsp)
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movl	52(%rbp), %esi
	movl	48(%rbp), %edx
	callq	*16(%rax)
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movl	60(%rbp), %esi
	movl	56(%rbp), %edx
	callq	*24(%rax)
	movq	24(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB14_11
# BB#10:                                #   in Loop: Header=BB14_4 Depth=1
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	*24(%rax)
	movq	%rax, 24(%rbp)
.LBB14_11:                              # %_ZN27btGImpactCollisionAlgorithm15addContactPointEP17btCollisionObjectS1_RK9btVector3S4_f.exit33
                                        #   in Loop: Header=BB14_4 Depth=1
	movq	32(%rbp), %rdi
	movq	%rax, 8(%rdi)
	movq	(%rdi), %rax
	leaq	168(%rsp), %rsi
	jmp	.LBB14_12
	.p2align	4, 0x90
.LBB14_16:                              #   in Loop: Header=BB14_4 Depth=1
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movl	52(%rbp), %esi
	movl	48(%rbp), %edx
	callq	*16(%rax)
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movl	60(%rbp), %esi
	movl	56(%rbp), %edx
	callq	*24(%rax)
	movq	24(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB14_18
# BB#17:                                #   in Loop: Header=BB14_4 Depth=1
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	*24(%rax)
	movq	%rax, 24(%rbp)
.LBB14_18:                              # %_ZN27btGImpactCollisionAlgorithm15addContactPointEP17btCollisionObjectS1_RK9btVector3S4_f.exit
                                        #   in Loop: Header=BB14_4 Depth=1
	movq	32(%rbp), %rdi
	movq	%rax, 8(%rdi)
	movq	(%rdi), %rax
	leaq	80(%rsp), %rsi
.LBB14_12:                              # %.backedge
                                        #   in Loop: Header=BB14_4 Depth=1
	leaq	16(%rsp), %rdx
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	callq	*32(%rax)
	movss	12(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	xorps	%xmm7, %xmm7
.LBB14_13:                              # %.backedge
                                        #   in Loop: Header=BB14_4 Depth=1
	decl	%r13d
	cmpl	$-1, %r13d
	jne	.LBB14_4
.LBB14_14:                              # %._crit_edge
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*200(%rax)
.LBB14_15:                              # %_ZNK6btAABB14plane_classifyERK9btVector4.exit.thread
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN27btGImpactCollisionAlgorithm37gimpacttrimeshpart_vs_plane_collisionEP17btCollisionObjectS1_P22btGImpactMeshShapePartP18btStaticPlaneShapeb, .Lfunc_end14-_ZN27btGImpactCollisionAlgorithm37gimpacttrimeshpart_vs_plane_collisionEP17btCollisionObjectS1_P22btGImpactMeshShapePartP18btStaticPlaneShapeb
	.cfi_endproc

	.globl	_ZN27btGImpactCollisionAlgorithm24gimpact_vs_compoundshapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP15btCompoundShapeb
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm24gimpact_vs_compoundshapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP15btCompoundShapeb,@function
_ZN27btGImpactCollisionAlgorithm24gimpact_vs_compoundshapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP15btCompoundShapeb: # @_ZN27btGImpactCollisionAlgorithm24gimpact_vs_compoundshapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP15btCompoundShapeb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi149:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi150:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi152:
	.cfi_def_cfa_offset 336
.Lcfi153:
	.cfi_offset %rbx, -56
.Lcfi154:
	.cfi_offset %r12, -48
.Lcfi155:
	.cfi_offset %r13, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movslq	28(%r12), %rax
	testq	%rax, %rax
	je	.LBB15_3
# BB#1:                                 # %.lr.ph
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movl	20(%rbx), %edx
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movss	24(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	28(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movl	36(%rbx), %edx
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movss	40(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 16(%rsp)         # 4-byte Spill
	movss	44(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movss	48(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 8(%rsp)          # 4-byte Spill
	movl	52(%rbx), %edx
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movsd	56(%rbx), %xmm6         # xmm6 = mem[0],zero
	movss	64(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	%xmm7, 4(%rsp)          # 4-byte Spill
	movl	68(%rbx), %edx
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movaps	%xmm3, 96(%rsp)         # 16-byte Spill
	unpcklps	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	movaps	%xmm0, 208(%rsp)        # 16-byte Spill
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	movaps	%xmm1, 192(%rsp)        # 16-byte Spill
	movaps	%xmm2, 112(%rsp)        # 16-byte Spill
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	movaps	%xmm2, 176(%rsp)        # 16-byte Spill
	movaps	%xmm6, 48(%rsp)         # 16-byte Spill
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm6, 160(%rsp)        # 16-byte Spill
	imulq	$88, %rax, %r13
	addq	$-24, %r13
	movzbl	%r9b, %edx
	movl	%edx, 20(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	40(%r12), %rax
	movss	-64(%rax,%r13), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movss	-60(%rax,%r13), %xmm14  # xmm14 = mem[0],zero,zero,zero
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm1
	movaps	%xmm0, %xmm4
	mulss	%xmm5, %xmm1
	movss	-48(%rax,%r13), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm3, %xmm2
	movaps	%xmm3, %xmm7
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	-32(%rax,%r13), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	112(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm1, %xmm3
	movaps	%xmm1, %xmm10
	mulss	%xmm11, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm3, 256(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm1
	mulss	%xmm14, %xmm1
	movss	-44(%rax,%r13), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm1, %xmm2
	movss	-28(%rax,%r13), %xmm13  # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm1
	mulss	%xmm13, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm1, 240(%rsp)        # 16-byte Spill
	movss	-56(%rax,%r13), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	mulss	%xmm1, %xmm3
	movss	-40(%rax,%r13), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	addss	%xmm3, %xmm7
	movss	-24(%rax,%r13), %xmm8   # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm10
	addss	%xmm7, %xmm10
	movaps	%xmm10, 224(%rsp)       # 16-byte Spill
	movaps	96(%rsp), %xmm15        # 16-byte Reload
	movaps	%xmm15, %xmm3
	mulss	%xmm5, %xmm3
	movaps	80(%rsp), %xmm12        # 16-byte Reload
	movaps	%xmm12, %xmm7
	mulss	%xmm0, %xmm7
	addss	%xmm3, %xmm7
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm10
	mulss	%xmm11, %xmm10
	addss	%xmm7, %xmm10
	movaps	%xmm15, %xmm3
	mulss	%xmm14, %xmm3
	movaps	%xmm12, %xmm7
	mulss	%xmm6, %xmm7
	addss	%xmm3, %xmm7
	movaps	%xmm4, %xmm9
	mulss	%xmm13, %xmm9
	addss	%xmm7, %xmm9
	mulss	%xmm1, %xmm15
	movaps	%xmm12, %xmm7
	mulss	%xmm2, %xmm7
	addss	%xmm15, %xmm7
	movaps	%xmm4, %xmm12
	mulss	%xmm8, %xmm12
	addss	%xmm7, %xmm12
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	addss	%xmm5, %xmm0
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm11
	addss	%xmm0, %xmm11
	mulss	%xmm4, %xmm14
	mulss	%xmm7, %xmm6
	addss	%xmm14, %xmm6
	mulss	%xmm5, %xmm13
	addss	%xmm6, %xmm13
	mulss	%xmm4, %xmm1
	mulss	%xmm7, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm5, %xmm8
	movaps	%xmm5, %xmm3
	addss	%xmm2, %xmm8
	movss	-16(%rax,%r13), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	-12(%rax,%r13), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	-8(%rax,%r13), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm1, %xmm5
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	208(%rsp), %xmm1        # 16-byte Folded Reload
	movaps	%xmm7, %xmm6
	mulss	%xmm2, %xmm6
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	192(%rsp), %xmm2        # 16-byte Folded Reload
	addps	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	176(%rsp), %xmm0        # 16-byte Folded Reload
	addps	%xmm2, %xmm0
	addss	%xmm5, %xmm6
	addss	%xmm1, %xmm6
	addss	4(%rsp), %xmm6          # 4-byte Folded Reload
	xorps	%xmm1, %xmm1
	movss	%xmm6, %xmm1            # xmm1 = xmm6[0],xmm1[1,2,3]
	movq	(%rax,%r13), %r8
	movaps	256(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 8(%rbx)
	movaps	240(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 12(%rbx)
	movaps	224(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 16(%rbx)
	movl	$0, 20(%rbx)
	movss	%xmm10, 24(%rbx)
	movss	%xmm9, 28(%rbx)
	movss	%xmm12, 32(%rbx)
	movl	$0, 36(%rbx)
	movss	%xmm11, 40(%rbx)
	movss	%xmm13, 44(%rbx)
	movss	%xmm8, 48(%rbx)
	addps	48(%rsp), %xmm0         # 16-byte Folded Reload
	movl	$0, 52(%rbx)
	movlps	%xmm0, 56(%rbx)
	movlps	%xmm1, 64(%rbx)
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movl	20(%rsp), %r9d          # 4-byte Reload
	callq	_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP16btCollisionShapeb
	movq	40(%rsp), %rax          # 8-byte Reload
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 8(%rbx)
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 12(%rbx)
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 16(%rbx)
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	%edx, 20(%rbx)
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 24(%rbx)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 28(%rbx)
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 32(%rbx)
	movl	32(%rsp), %edx          # 4-byte Reload
	movl	%edx, 36(%rbx)
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 40(%rbx)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 44(%rbx)
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rbx)
	movl	28(%rsp), %edx          # 4-byte Reload
	movl	%edx, 52(%rbx)
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 56(%rbx)
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 60(%rbx)
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 64(%rbx)
	movl	24(%rsp), %edx          # 4-byte Reload
	movl	%edx, 68(%rbx)
	addq	$-88, %r13
	decl	%eax
	jne	.LBB15_2
.LBB15_3:                               # %._crit_edge
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZN27btGImpactCollisionAlgorithm24gimpact_vs_compoundshapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP15btCompoundShapeb, .Lfunc_end15-_ZN27btGImpactCollisionAlgorithm24gimpact_vs_compoundshapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP15btCompoundShapeb
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN27btGImpactCollisionAlgorithm18gimpact_vs_concaveEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP14btConcaveShapeb
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm18gimpact_vs_concaveEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP14btConcaveShapeb,@function
_ZN27btGImpactCollisionAlgorithm18gimpact_vs_concaveEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP14btConcaveShapeb: # @_ZN27btGImpactCollisionAlgorithm18gimpact_vs_concaveEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP14btConcaveShapeb
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r15
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi161:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 40
	subq	$296, %rsp              # imm = 0x128
.Lcfi163:
	.cfi_def_cfa_offset 336
.Lcfi164:
	.cfi_offset %rbx, -40
.Lcfi165:
	.cfi_offset %r12, -32
.Lcfi166:
	.cfi_offset %r14, -24
.Lcfi167:
	.cfi_offset %r15, -16
	movq	%r8, %r15
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	$_ZTV25btGImpactTriangleCallback+16, 80(%rsp)
	movq	%rdi, 88(%rsp)
	movq	%r12, 96(%rsp)
	movq	%rbx, 104(%rsp)
	movq	%r14, 112(%rsp)
	movb	%r9b, 120(%rsp)
	movq	(%r15), %rax
.Ltmp216:
	movq	%r15, %rdi
	callq	*88(%rax)
.Ltmp217:
# BB#1:
	movss	%xmm0, 124(%rsp)
	movsd	8(%rbx), %xmm8          # xmm8 = mem[0],zero
	movsd	24(%rbx), %xmm6         # xmm6 = mem[0],zero
	movsd	40(%rbx), %xmm3         # xmm3 = mem[0],zero
	movss	16(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	movss	32(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	48(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	%xmm10, 12(%rsp)        # 4-byte Spill
	movd	56(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movdqa	.LCPI16_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm13
	movd	60(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movdqa	%xmm1, %xmm2
	pxor	%xmm0, %xmm2
	movd	64(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	pxor	%xmm4, %xmm0
	pshufd	$224, %xmm13, %xmm5     # xmm5 = xmm13[0,0,2,3]
	mulps	%xmm8, %xmm5
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm6, %xmm2
	addps	%xmm5, %xmm2
	pshufd	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm2, %xmm0
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	mulss	%xmm7, %xmm13
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm13
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm13
	movss	8(%r12), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	12(%r12), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	movaps	%xmm2, %xmm0
	mulss	%xmm12, %xmm0
	movss	24(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	40(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 4(%rsp)          # 4-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm0
	movaps	%xmm2, 128(%rsp)        # 16-byte Spill
	mulss	%xmm14, %xmm0
	movss	28(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm4
	movaps	%xmm6, %xmm10
	movaps	%xmm10, 144(%rsp)       # 16-byte Spill
	mulss	%xmm5, %xmm4
	addss	%xmm0, %xmm4
	movss	44(%r12), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm15, %xmm0
	addss	%xmm4, %xmm0
	movaps	%xmm0, 208(%rsp)        # 16-byte Spill
	movss	16(%r12), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	mulss	%xmm11, %xmm6
	movss	32(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm8
	mulss	%xmm4, %xmm8
	addss	%xmm6, %xmm8
	movss	48(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	movaps	%xmm1, 160(%rsp)        # 16-byte Spill
	mulss	%xmm6, %xmm0
	addss	%xmm8, %xmm0
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	pshufd	$229, %xmm2, %xmm3      # xmm3 = xmm2[1,1,2,3]
	movdqa	%xmm3, %xmm8
	mulss	%xmm12, %xmm8
	pshufd	$229, %xmm10, %xmm2     # xmm2 = xmm10[1,1,2,3]
	movdqa	%xmm2, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm8, %xmm0
	pshufd	$229, %xmm1, %xmm10     # xmm10 = xmm1[1,1,2,3]
	movdqa	%xmm10, %xmm1
	mulss	4(%rsp), %xmm1          # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	movdqa	%xmm3, %xmm0
	mulss	%xmm14, %xmm0
	movdqa	%xmm2, %xmm1
	mulss	%xmm5, %xmm1
	addss	%xmm0, %xmm1
	movdqa	%xmm10, %xmm8
	mulss	%xmm15, %xmm8
	addss	%xmm1, %xmm8
	mulss	%xmm11, %xmm3
	mulss	%xmm4, %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm6, %xmm10
	addss	%xmm2, %xmm10
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm12
	mulss	%xmm9, %xmm7
	addss	%xmm12, %xmm7
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm7, %xmm2
	movaps	%xmm2, %xmm7
	mulss	%xmm0, %xmm14
	mulss	%xmm9, %xmm5
	addss	%xmm14, %xmm5
	mulss	%xmm1, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm0, %xmm11
	movaps	%xmm0, %xmm2
	mulss	%xmm9, %xmm4
	addss	%xmm11, %xmm4
	mulss	%xmm1, %xmm6
	movaps	%xmm1, %xmm3
	addss	%xmm4, %xmm6
	movss	56(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	128(%rsp), %xmm0        # 16-byte Folded Reload
	movss	60(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm9
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	144(%rsp), %xmm1        # 16-byte Folded Reload
	addps	%xmm0, %xmm1
	movss	64(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	160(%rsp), %xmm0        # 16-byte Folded Reload
	addps	%xmm1, %xmm0
	addps	240(%rsp), %xmm0        # 16-byte Folded Reload
	addss	%xmm2, %xmm9
	addss	%xmm3, %xmm9
	addss	%xmm13, %xmm9
	xorps	%xmm1, %xmm1
	movss	%xmm9, %xmm1            # xmm1 = xmm9[0],xmm1[1,2,3]
	movaps	224(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 16(%rsp)
	movaps	208(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 20(%rsp)
	movaps	192(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 24(%rsp)
	movl	$0, 28(%rsp)
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 32(%rsp)
	movss	%xmm8, 36(%rsp)
	movss	%xmm10, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	%xmm7, 48(%rsp)
	movss	%xmm15, 52(%rsp)
	movss	%xmm6, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm0, 64(%rsp)
	movlps	%xmm1, 72(%rsp)
	movq	(%r14), %rax
.Ltmp219:
	leaq	16(%rsp), %rsi
	leaq	280(%rsp), %rdx
	leaq	264(%rsp), %rcx
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp220:
# BB#2:
	movq	(%r15), %rax
.Ltmp221:
	leaq	80(%rsp), %rsi
	leaq	280(%rsp), %rdx
	leaq	264(%rsp), %rcx
	movq	%r15, %rdi
	callq	*96(%rax)
.Ltmp222:
# BB#3:
	leaq	80(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB16_4:
.Ltmp218:
	jmp	.LBB16_6
.LBB16_5:
.Ltmp223:
.LBB16_6:
	movq	%rax, %rbx
.Ltmp224:
	leaq	80(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp225:
# BB#7:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB16_8:
.Ltmp226:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN27btGImpactCollisionAlgorithm18gimpact_vs_concaveEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP14btConcaveShapeb, .Lfunc_end16-_ZN27btGImpactCollisionAlgorithm18gimpact_vs_concaveEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP14btConcaveShapeb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp216-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp217-.Ltmp216       #   Call between .Ltmp216 and .Ltmp217
	.long	.Ltmp218-.Lfunc_begin6  #     jumps to .Ltmp218
	.byte	0                       #   On action: cleanup
	.long	.Ltmp219-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp222-.Ltmp219       #   Call between .Ltmp219 and .Ltmp222
	.long	.Ltmp223-.Lfunc_begin6  #     jumps to .Ltmp223
	.byte	0                       #   On action: cleanup
	.long	.Ltmp222-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp224-.Ltmp222       #   Call between .Ltmp222 and .Ltmp224
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin6  #     jumps to .Ltmp226
	.byte	1                       #   On action: 1
	.long	.Ltmp225-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Lfunc_end16-.Ltmp225   #   Call between .Ltmp225 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN27btGImpactCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN27btGImpactCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN27btGImpactCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi170:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi171:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 48
.Lcfi173:
	.cfi_offset %rbx, -48
.Lcfi174:
	.cfi_offset %r12, -40
.Lcfi175:
	.cfi_offset %r13, -32
.Lcfi176:
	.cfi_offset %r14, -24
.Lcfi177:
	.cfi_offset %r15, -16
	movq	%r8, %r13
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB17_2
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	movq	$0, 24(%rbx)
.LBB17_2:                               # %_ZN27btGImpactCollisionAlgorithm23destroyContactManifoldsEv.exit.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_4
# BB#3:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	callq	*104(%rax)
	movq	$0, 16(%rbx)
.LBB17_4:                               # %_ZN27btGImpactCollisionAlgorithm10clearCacheEv.exit
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 48(%rbx)
	movq	%r13, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	200(%r15), %rax
	cmpl	$25, 8(%rax)
	jne	.LBB17_8
# BB#5:
	movq	200(%r14), %r8
	cmpl	$25, 8(%r8)
	jne	.LBB17_6
# BB#11:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%rax, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEP17btCollisionObjectS1_P23btGImpactShapeInterfaceS3_ # TAILCALL
.LBB17_8:
	movq	200(%r14), %rcx
	cmpl	$25, 8(%rcx)
	jne	.LBB17_10
# BB#9:
	movl	$1, %r9d
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%rax, %r8
	jmp	.LBB17_7
.LBB17_6:
	xorl	%r9d, %r9d
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%rax, %rcx
.LBB17_7:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP16btCollisionShapeb # TAILCALL
.LBB17_10:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	_ZN27btGImpactCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end17-_ZN27btGImpactCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN27btGImpactCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN27btGImpactCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN27btGImpactCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	movss	.LCPI18_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end18:
	.size	_ZN27btGImpactCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end18-_ZN27btGImpactCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.text._ZN30btCollisionAlgorithmCreateFuncD2Ev,"axG",@progbits,_ZN30btCollisionAlgorithmCreateFuncD2Ev,comdat
	.weak	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.p2align	4, 0x90
	.type	_ZN30btCollisionAlgorithmCreateFuncD2Ev,@function
_ZN30btCollisionAlgorithmCreateFuncD2Ev: # @_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end19:
	.size	_ZN30btCollisionAlgorithmCreateFuncD2Ev, .Lfunc_end19-_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.cfi_endproc

	.text
	.globl	_ZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcher
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcher,@function
_ZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcher: # @_ZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi178:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi180:
	.cfi_def_cfa_offset 32
.Lcfi181:
	.cfi_offset %rbx, -24
.Lcfi182:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	movl	$25, %esi
	movl	$g_gimpact_cf, %ecx
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc
	incl	%ebp
	cmpl	$36, %ebp
	jne	.LBB20_1
# BB#2:                                 # %.preheader.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB20_3:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	$25, %edx
	movl	$g_gimpact_cf, %ecx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc
	incl	%ebp
	cmpl	$36, %ebp
	jne	.LBB20_3
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcher, .Lfunc_end20-_ZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcher
	.cfi_endproc

	.section	.text._ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi183:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi184:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi185:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi186:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi187:
	.cfi_def_cfa_offset 48
.Lcfi188:
	.cfi_offset %rbx, -40
.Lcfi189:
	.cfi_offset %r14, -32
.Lcfi190:
	.cfi_offset %r15, -24
.Lcfi191:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB21_17
# BB#1:
	movl	4(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB21_16
# BB#2:
	leal	(%rax,%rax), %edx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%edx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB21_16
# BB#3:
	testl	%ebp, %ebp
	je	.LBB21_4
# BB#5:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB21_7
	jmp	.LBB21_11
.LBB21_4:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB21_11
.LBB21_7:                               # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB21_9
	.p2align	4, 0x90
.LBB21_8:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB21_8
.LBB21_9:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB21_11
	.p2align	4, 0x90
.LBB21_10:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB21_10
.LBB21_11:                              # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_15
# BB#12:
	cmpb	$0, 24(%rbx)
	je	.LBB21_14
# BB#13:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%rbx), %eax
.LBB21_14:
	movq	$0, 16(%rbx)
.LBB21_15:                              # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
	movb	$1, 24(%rbx)
	movq	%r15, 16(%rbx)
	movl	%ebp, 8(%rbx)
	movq	24(%r14), %rcx
.LBB21_16:                              # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
	movq	16(%rbx), %rdx
	cltq
	movq	%rcx, (%rdx,%rax,8)
	incl	%eax
	movl	%eax, 4(%rbx)
.LBB21_17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end21-_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.section	.text._ZN17btTriangleShapeExD0Ev,"axG",@progbits,_ZN17btTriangleShapeExD0Ev,comdat
	.weak	_ZN17btTriangleShapeExD0Ev
	.p2align	4, 0x90
	.type	_ZN17btTriangleShapeExD0Ev,@function
_ZN17btTriangleShapeExD0Ev:             # @_ZN17btTriangleShapeExD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi193:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi194:
	.cfi_def_cfa_offset 32
.Lcfi195:
	.cfi_offset %rbx, -24
.Lcfi196:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp227:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp228:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB22_2:
.Ltmp229:
	movq	%rax, %r14
.Ltmp230:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp231:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_4:
.Ltmp232:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN17btTriangleShapeExD0Ev, .Lfunc_end22-_ZN17btTriangleShapeExD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp227-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp228-.Ltmp227       #   Call between .Ltmp227 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin7  #     jumps to .Ltmp229
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp230-.Ltmp228       #   Call between .Ltmp228 and .Ltmp230
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp230-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp231-.Ltmp230       #   Call between .Ltmp230 and .Ltmp231
	.long	.Ltmp232-.Lfunc_begin7  #     jumps to .Ltmp232
	.byte	1                       #   On action: 1
	.long	.Ltmp231-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end22-.Ltmp231   #   Call between .Ltmp231 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_: # @_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi197:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi198:
	.cfi_def_cfa_offset 24
.Lcfi199:
	.cfi_offset %rbx, -24
.Lcfi200:
	.cfi_offset %r14, -16
	movss	64(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	68(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	movaps	%xmm6, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	movss	20(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm13   # xmm13 = xmm13[0],xmm5[0],xmm13[1],xmm5[1]
	movaps	%xmm4, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm13, %xmm7
	addps	%xmm0, %xmm7
	movss	24(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm14         # xmm14 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm14   # xmm14 = xmm14[0],xmm0[0],xmm14[1],xmm0[1]
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm14, %xmm0
	addps	%xmm7, %xmm0
	movsd	48(%rsi), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm8, %xmm0
	movss	32(%rsi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm6
	movss	36(%rsi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm6, %xmm4
	movss	40(%rsi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	addss	%xmm4, %xmm1
	movss	56(%rsi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	addss	%xmm12, %xmm1
	xorps	%xmm4, %xmm4
	movss	%xmm1, %xmm4            # xmm4 = xmm1[0],xmm4[1,2,3]
	movlps	%xmm0, -48(%rsp)
	movlps	%xmm4, -40(%rsp)
	movss	80(%rdi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	84(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	88(%rdi), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movaps	%xmm2, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm13, %xmm6
	addps	%xmm4, %xmm6
	movaps	%xmm15, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm14, %xmm4
	addps	%xmm6, %xmm4
	addps	%xmm8, %xmm4
	mulss	%xmm9, %xmm7
	mulss	%xmm10, %xmm2
	addss	%xmm7, %xmm2
	mulss	%xmm11, %xmm15
	addss	%xmm2, %xmm15
	addss	%xmm12, %xmm15
	xorps	%xmm2, %xmm2
	movss	%xmm15, %xmm2           # xmm2 = xmm15[0],xmm2[1,2,3]
	movlps	%xmm4, -16(%rsp)
	movlps	%xmm2, -8(%rsp)
	movss	96(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	100(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm3, %xmm6
	movaps	%xmm1, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm13, %xmm7
	movss	104(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addps	%xmm6, %xmm7
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm14, %xmm3
	addps	%xmm7, %xmm3
	addps	%xmm8, %xmm3
	mulss	%xmm9, %xmm5
	mulss	%xmm10, %xmm1
	addss	%xmm5, %xmm1
	movaps	%xmm0, %xmm8
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	mulss	%xmm11, %xmm2
	xorps	%xmm5, %xmm5
	addss	%xmm1, %xmm2
	movaps	%xmm4, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	addss	%xmm12, %xmm2
	movaps	%xmm3, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movss	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1,2,3]
	movlps	%xmm3, -32(%rsp)
	movlps	%xmm5, -24(%rsp)
	ucomiss	%xmm3, %xmm4
	leaq	-32(%rsp), %r8
	leaq	-16(%rsp), %r11
	movq	%r11, %r10
	cmovaq	%r8, %r10
	ucomiss	(%r10), %xmm0
	leaq	-48(%rsp), %r9
	cmovbeq	%r9, %r10
	ucomiss	%xmm6, %xmm7
	movq	%r11, %rsi
	cmovaq	%r8, %rsi
	movq	%rsi, %rax
	orq	$4, %rax
	ucomiss	(%rax), %xmm8
	cmovbeq	%r9, %rsi
	orq	$4, %rsi
	movss	-40(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm15
	movq	%r11, %rax
	cmovaq	%r8, %rax
	ucomiss	8(%rax), %xmm1
	cmovbeq	%r9, %rax
	ucomiss	%xmm4, %xmm3
	movq	%r11, %r14
	cmovaq	%r8, %r14
	movss	(%r14), %xmm3           # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm3
	cmovbeq	%r9, %r14
	ucomiss	%xmm7, %xmm6
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movq	%r11, %rbx
	cmovaq	%r8, %rbx
	movq	%rbx, %rdi
	orq	$4, %rdi
	movss	(%rdi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm8, %xmm3
	movss	(%r10), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cmovbeq	%r9, %rbx
	orq	$4, %rbx
	ucomiss	%xmm15, %xmm2
	movss	8(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cmovaq	%r8, %r11
	movss	8(%r11), %xmm5          # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm5
	cmovbeq	%r9, %r11
	subss	%xmm0, %xmm3
	subss	%xmm0, %xmm4
	subss	%xmm0, %xmm2
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	(%rbx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm5
	addss	8(%r11), %xmm0
	movss	%xmm3, (%rdx)
	movss	%xmm4, 4(%rdx)
	movss	%xmm2, 8(%rdx)
	movss	%xmm1, (%rcx)
	movss	%xmm5, 4(%rcx)
	movss	%xmm0, 8(%rcx)
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end23:
	.size	_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end23-_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end24:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end24-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,"axG",@progbits,_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,comdat
	.weak	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3: # @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end25:
	.size	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end25-_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape7getNameEv,"axG",@progbits,_ZNK15btTriangleShape7getNameEv,comdat
	.weak	_ZNK15btTriangleShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getNameEv,@function
_ZNK15btTriangleShape7getNameEv:        # @_ZNK15btTriangleShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end26:
	.size	_ZNK15btTriangleShape7getNameEv, .Lfunc_end26-_ZNK15btTriangleShape7getNameEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end27:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end27-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end28:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end28-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,"axG",@progbits,_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,comdat
	.weak	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	80(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	84(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	88(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	mulss	96(%rdi), %xmm2
	mulss	100(%rdi), %xmm1
	addss	%xmm2, %xmm1
	mulss	104(%rdi), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm5, -16(%rsp)
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, -12(%rsp)
	movss	%xmm0, -8(%rsp)
	movl	$0, -4(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm5, %xmm1
	seta	%al
	ucomiss	-16(%rsp,%rax,4), %xmm0
	movl	$2, %ecx
	cmovbeq	%rax, %rcx
	shlq	$4, %rcx
	movsd	64(%rdi,%rcx), %xmm0    # xmm0 = mem[0],zero
	movsd	72(%rdi,%rcx), %xmm1    # xmm1 = mem[0],zero
	retq
.Lfunc_end29:
	.size	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end29-_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,"axG",@progbits,_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,comdat
	.weak	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	jle	.LBB30_3
# BB#1:                                 # %.lr.ph
	movl	%ecx, %eax
	addq	$8, %rsi
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB30_2:                               # =>This Inner Loop Header: Depth=1
	movss	-8(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	80(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	84(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	88(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	mulss	96(%rdi), %xmm2
	mulss	100(%rdi), %xmm1
	addss	%xmm2, %xmm1
	mulss	104(%rdi), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm5, -16(%rsp)
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, -12(%rsp)
	movss	%xmm0, -8(%rsp)
	movl	$0, -4(%rsp)
	xorl	%ecx, %ecx
	ucomiss	%xmm5, %xmm1
	seta	%cl
	ucomiss	-16(%rsp,%rcx,4), %xmm0
	cmovaq	%r8, %rcx
	shlq	$4, %rcx
	movups	64(%rdi,%rcx), %xmm0
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	addq	$16, %rsi
	decq	%rax
	jne	.LBB30_2
.LBB30_3:                               # %._crit_edge
	retq
.Lfunc_end30:
	.size	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end30-_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	movl	$2, %eax
	retq
.Lfunc_end31:
	.size	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end31-_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI32_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI32_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi201:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi202:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi203:
	.cfi_def_cfa_offset 32
.Lcfi204:
	.cfi_offset %rbx, -24
.Lcfi205:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movss	64(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	80(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movsd	84(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	68(%rdi), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm1
	movss	100(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	96(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	104(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm6
	movaps	%xmm1, %xmm3
	mulps	%xmm6, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm0
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm1, %xmm6
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	subss	%xmm6, %xmm0
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, (%rbx)
	movlps	%xmm2, 8(%rbx)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB32_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB32_2:                               # %.split
	movss	.LCPI32_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	%xmm2, 4(%rbx)
	mulss	8(%rbx), %xmm0
	movss	%xmm0, 8(%rbx)
	testl	%ebp, %ebp
	je	.LBB32_4
# BB#3:
	movaps	.LCPI32_1(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm3, %xmm1
	movss	%xmm1, (%rbx)
	xorps	%xmm3, %xmm2
	movss	%xmm2, 4(%rbx)
	xorps	%xmm3, %xmm0
	movss	%xmm0, 8(%rbx)
.LBB32_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end32:
	.size	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end32-_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape14getNumVerticesEv,"axG",@progbits,_ZNK15btTriangleShape14getNumVerticesEv,comdat
	.weak	_ZNK15btTriangleShape14getNumVerticesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape14getNumVerticesEv,@function
_ZNK15btTriangleShape14getNumVerticesEv: # @_ZNK15btTriangleShape14getNumVerticesEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end33:
	.size	_ZNK15btTriangleShape14getNumVerticesEv, .Lfunc_end33-_ZNK15btTriangleShape14getNumVerticesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape11getNumEdgesEv,"axG",@progbits,_ZNK15btTriangleShape11getNumEdgesEv,comdat
	.weak	_ZNK15btTriangleShape11getNumEdgesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape11getNumEdgesEv,@function
_ZNK15btTriangleShape11getNumEdgesEv:   # @_ZNK15btTriangleShape11getNumEdgesEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end34:
	.size	_ZNK15btTriangleShape11getNumEdgesEv, .Lfunc_end34-_ZNK15btTriangleShape11getNumEdgesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,"axG",@progbits,_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,comdat
	.weak	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,@function
_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_: # @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi206:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi207:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi208:
	.cfi_def_cfa_offset 32
.Lcfi209:
	.cfi_offset %rbx, -32
.Lcfi210:
	.cfi_offset %r14, -24
.Lcfi211:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*168(%rax)
	movq	(%rbx), %rax
	movq	168(%rax), %rax
	leal	1(%r15), %ecx
	movslq	%ecx, %rcx
	imulq	$1431655766, %rcx, %rcx # imm = 0x55555556
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,2), %ecx
	negl	%ecx
	leal	1(%r15,%rcx), %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.Lfunc_end35:
	.size	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_, .Lfunc_end35-_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape9getVertexEiR9btVector3,"axG",@progbits,_ZNK15btTriangleShape9getVertexEiR9btVector3,comdat
	.weak	_ZNK15btTriangleShape9getVertexEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape9getVertexEiR9btVector3,@function
_ZNK15btTriangleShape9getVertexEiR9btVector3: # @_ZNK15btTriangleShape9getVertexEiR9btVector3
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	shlq	$4, %rax
	movups	64(%rdi,%rax), %xmm0
	movups	%xmm0, (%rdx)
	retq
.Lfunc_end36:
	.size	_ZNK15btTriangleShape9getVertexEiR9btVector3, .Lfunc_end36-_ZNK15btTriangleShape9getVertexEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape12getNumPlanesEv,"axG",@progbits,_ZNK15btTriangleShape12getNumPlanesEv,comdat
	.weak	_ZNK15btTriangleShape12getNumPlanesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape12getNumPlanesEv,@function
_ZNK15btTriangleShape12getNumPlanesEv:  # @_ZNK15btTriangleShape12getNumPlanesEv
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	retq
.Lfunc_end37:
	.size	_ZNK15btTriangleShape12getNumPlanesEv, .Lfunc_end37-_ZNK15btTriangleShape12getNumPlanesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape8getPlaneER9btVector3S1_i,"axG",@progbits,_ZNK15btTriangleShape8getPlaneER9btVector3S1_i,comdat
	.weak	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i,@function
_ZNK15btTriangleShape8getPlaneER9btVector3S1_i: # @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movq	%rsi, %rdx
	movq	(%rdi), %rsi
	movq	200(%rsi), %rax
	movl	%ecx, %esi
	movq	%r8, %rcx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end38:
	.size	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i, .Lfunc_end38-_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI39_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI39_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK15btTriangleShape8isInsideERK9btVector3f,"axG",@progbits,_ZNK15btTriangleShape8isInsideERK9btVector3f,comdat
	.weak	_ZNK15btTriangleShape8isInsideERK9btVector3f
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape8isInsideERK9btVector3f,@function
_ZNK15btTriangleShape8isInsideERK9btVector3f: # @_ZNK15btTriangleShape8isInsideERK9btVector3f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi212:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi213:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi214:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi215:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi216:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi217:
	.cfi_def_cfa_offset 192
.Lcfi218:
	.cfi_offset %rbx, -48
.Lcfi219:
	.cfi_offset %r12, -40
.Lcfi220:
	.cfi_offset %r14, -32
.Lcfi221:
	.cfi_offset %r15, -24
.Lcfi222:
	.cfi_offset %rbp, -16
	movaps	%xmm0, %xmm8
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	84(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movss	88(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm6
	movss	96(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm5
	movss	100(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm7
	movss	104(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movaps	%xmm6, %xmm2
	mulss	%xmm5, %xmm6
	mulss	%xmm3, %xmm5
	mulss	%xmm1, %xmm3
	mulss	%xmm7, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm0, %xmm7
	subss	%xmm5, %xmm7
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm7, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB39_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	movss	%xmm7, 16(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
.LBB39_2:                               # %.split
	movss	.LCPI39_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	divss	%xmm0, %xmm5
	mulss	%xmm5, %xmm3
	mulss	%xmm5, %xmm6
	mulss	%xmm7, %xmm5
	movss	64(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	movss	4(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	%xmm4, 48(%rsp)         # 16-byte Spill
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm6, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm3, %xmm1
	addps	%xmm0, %xmm1
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	72(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	%xmm1, %xmm0
	movaps	.LCPI39_1(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm1
	xorl	%eax, %eax
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	ucomiss	%xmm1, %xmm0
	jb	.LBB39_11
# BB#3:                                 # %.split
	ucomiss	%xmm0, %xmm8
	jb	.LBB39_11
# BB#4:                                 # %.preheader
	xorl	%ebp, %ebp
	movq	%rsp, %r15
	leaq	80(%rsp), %r12
	.p2align	4, 0x90
.LBB39_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	*160(%rax)
	movss	80(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	84(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	(%rsp), %xmm5
	subss	4(%rsp), %xmm0
	movss	88(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	8(%rsp), %xmm6
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm7
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm1
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm7
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm3, %xmm6
	movaps	%xmm4, %xmm1
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm5
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB39_7
# BB#6:                                 # %call.sqrt86
                                        #   in Loop: Header=BB39_5 Depth=1
	movaps	%xmm5, 128(%rsp)        # 16-byte Spill
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	movaps	112(%rsp), %xmm6        # 16-byte Reload
	movaps	128(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB39_7:                               # %.split85
                                        #   in Loop: Header=BB39_5 Depth=1
	movss	.LCPI39_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm7
	mulss	%xmm0, %xmm6
	mulss	%xmm0, %xmm5
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm2, %xmm7
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm3, %xmm6
	addps	%xmm7, %xmm6
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm5, %xmm1
	addps	%xmm6, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	ucomiss	%xmm1, %xmm0
	ja	.LBB39_10
# BB#8:                                 #   in Loop: Header=BB39_5 Depth=1
	incl	%ebp
	cmpl	$2, %ebp
	jle	.LBB39_5
# BB#9:
	movb	$1, %al
	jmp	.LBB39_11
.LBB39_10:
	xorl	%eax, %eax
.LBB39_11:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end39:
	.size	_ZNK15btTriangleShape8isInsideERK9btVector3f, .Lfunc_end39-_ZNK15btTriangleShape8isInsideERK9btVector3f
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI40_0:
	.long	1065353216              # float 1
	.section	.text._ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,"axG",@progbits,_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,comdat
	.weak	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,@function
_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_: # @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi223:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi224:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi225:
	.cfi_def_cfa_offset 32
.Lcfi226:
	.cfi_offset %rbx, -32
.Lcfi227:
	.cfi_offset %r14, -24
.Lcfi228:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movss	64(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movsd	84(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	68(%rbx), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm1
	movss	100(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	96(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	104(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm6
	movaps	%xmm1, %xmm3
	mulps	%xmm6, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm0
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm1, %xmm6
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	subss	%xmm6, %xmm0
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, (%r15)
	movlps	%xmm2, 8(%r15)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB40_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB40_2:                               # %.split
	movss	.LCPI40_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%r15)
	mulss	8(%r15), %xmm0
	movss	%xmm0, 8(%r15)
	movups	64(%rbx), %xmm0
	movups	%xmm0, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end40:
	.size	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_, .Lfunc_end40-_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.cfi_endproc

	.section	.text._ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev,"axG",@progbits,_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev,comdat
	.weak	_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev
	.p2align	4, 0x90
	.type	_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev,@function
_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev: # @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end41:
	.size	_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev, .Lfunc_end41-_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev
	.cfi_endproc

	.section	.text._ZN20btTetrahedronShapeExD0Ev,"axG",@progbits,_ZN20btTetrahedronShapeExD0Ev,comdat
	.weak	_ZN20btTetrahedronShapeExD0Ev
	.p2align	4, 0x90
	.type	_ZN20btTetrahedronShapeExD0Ev,@function
_ZN20btTetrahedronShapeExD0Ev:          # @_ZN20btTetrahedronShapeExD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi229:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi230:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi231:
	.cfi_def_cfa_offset 32
.Lcfi232:
	.cfi_offset %rbx, -24
.Lcfi233:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp233:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp234:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB42_2:
.Ltmp235:
	movq	%rax, %r14
.Ltmp236:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp237:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB42_4:
.Ltmp238:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end42:
	.size	_ZN20btTetrahedronShapeExD0Ev, .Lfunc_end42-_ZN20btTetrahedronShapeExD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp233-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp234-.Ltmp233       #   Call between .Ltmp233 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin8  #     jumps to .Ltmp235
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp236-.Ltmp234       #   Call between .Ltmp234 and .Ltmp236
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp236-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp237-.Ltmp236       #   Call between .Ltmp236 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin8  #     jumps to .Ltmp238
	.byte	1                       #   On action: 1
	.long	.Ltmp237-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Lfunc_end42-.Ltmp237   #   Call between .Ltmp237 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK16btBU_Simplex1to47getNameEv,"axG",@progbits,_ZNK16btBU_Simplex1to47getNameEv,comdat
	.weak	_ZNK16btBU_Simplex1to47getNameEv
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to47getNameEv,@function
_ZNK16btBU_Simplex1to47getNameEv:       # @_ZNK16btBU_Simplex1to47getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.1, %eax
	retq
.Lfunc_end43:
	.size	_ZNK16btBU_Simplex1to47getNameEv, .Lfunc_end43-_ZNK16btBU_Simplex1to47getNameEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end44:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end44-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end45:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end45-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi,"axG",@progbits,_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi,comdat
	.weak	_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi
	.p2align	4, 0x90
	.type	_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi,@function
_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi: # @_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	movq	216(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end46:
	.size	_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi, .Lfunc_end46-_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi
	.cfi_endproc

	.section	.text._ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev,"axG",@progbits,_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev,comdat
	.weak	_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev
	.p2align	4, 0x90
	.type	_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev,@function
_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev: # @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end47:
	.size	_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev, .Lfunc_end47-_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev
	.cfi_endproc

	.section	.text._ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi,"axG",@progbits,_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi,comdat
	.weak	_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi
	.p2align	4, 0x90
	.type	_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi,@function
_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi: # @_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi234:
	.cfi_def_cfa_offset 16
.Lcfi235:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rdi), %rax
	addq	$8, %rdx
	callq	*176(%rax)
	movq	8(%rbx), %rax
	addq	$8, %rax
	popq	%rbx
	retq
.Lfunc_end48:
	.size	_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi, .Lfunc_end48-_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi
	.cfi_endproc

	.section	.text._ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev,"axG",@progbits,_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev,comdat
	.weak	_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev
	.p2align	4, 0x90
	.type	_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev,@function
_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev: # @_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end49:
	.size	_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev, .Lfunc_end49-_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev
	.cfi_endproc

	.section	.text._ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi,"axG",@progbits,_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi,comdat
	.weak	_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi
	.p2align	4, 0x90
	.type	_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi,@function
_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi: # @_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi236:
	.cfi_def_cfa_offset 16
.Lcfi237:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rdi), %rax
	addq	$120, %rdx
	callq	*184(%rax)
	movq	8(%rbx), %rax
	addq	$120, %rax
	popq	%rbx
	retq
.Lfunc_end50:
	.size	_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi, .Lfunc_end50-_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi
	.cfi_endproc

	.section	.text._ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev,"axG",@progbits,_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev,comdat
	.weak	_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev
	.p2align	4, 0x90
	.type	_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev,@function
_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev: # @_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end51:
	.size	_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev, .Lfunc_end51-_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev
	.cfi_endproc

	.section	.text._ZN25btGImpactTriangleCallbackD0Ev,"axG",@progbits,_ZN25btGImpactTriangleCallbackD0Ev,comdat
	.weak	_ZN25btGImpactTriangleCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN25btGImpactTriangleCallbackD0Ev,@function
_ZN25btGImpactTriangleCallbackD0Ev:     # @_ZN25btGImpactTriangleCallbackD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi238:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi239:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi240:
	.cfi_def_cfa_offset 32
.Lcfi241:
	.cfi_offset %rbx, -24
.Lcfi242:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp239:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp240:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB52_2:
.Ltmp241:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end52:
	.size	_ZN25btGImpactTriangleCallbackD0Ev, .Lfunc_end52-_ZN25btGImpactTriangleCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table52:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp239-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp240-.Ltmp239       #   Call between .Ltmp239 and .Ltmp240
	.long	.Ltmp241-.Lfunc_begin9  #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.long	.Ltmp240-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end52-.Ltmp240   #   Call between .Ltmp240 and .Lfunc_end52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii,"axG",@progbits,_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii,comdat
	.weak	_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii,@function
_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii: # @_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbp
.Lcfi243:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi244:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi245:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi246:
	.cfi_def_cfa_offset 40
	subq	$120, %rsp
.Lcfi247:
	.cfi_def_cfa_offset 160
.Lcfi248:
	.cfi_offset %rbx, -40
.Lcfi249:
	.cfi_offset %r14, -32
.Lcfi250:
	.cfi_offset %r15, -24
.Lcfi251:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	leaq	8(%rsp), %rdi
	callq	_ZN23btPolyhedralConvexShapeC2Ev
	movl	$1, 16(%rsp)
	movups	(%rbp), %xmm0
	movups	%xmm0, 72(%rsp)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 88(%rsp)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 104(%rsp)
	movq	$_ZTV17btTriangleShapeEx+16, 8(%rsp)
	movl	44(%rbx), %eax
	movl	%eax, 64(%rsp)
	movb	40(%rbx), %al
	testb	%al, %al
	movq	8(%rbx), %rdi
	je	.LBB53_2
# BB#1:
	movl	%r15d, 52(%rdi)
	leaq	48(%rdi), %rcx
	jmp	.LBB53_3
.LBB53_2:
	movl	%r15d, 60(%rdi)
	leaq	56(%rdi), %rcx
.LBB53_3:
	movl	%r14d, (%rcx)
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rdx
	movq	32(%rbx), %rcx
.Ltmp242:
	movzbl	%al, %r9d
	leaq	8(%rsp), %r8
	callq	_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEP17btCollisionObjectS1_P23btGImpactShapeInterfaceP16btCollisionShapeb
.Ltmp243:
# BB#4:
	leaq	8(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB53_5:
.Ltmp244:
	movq	%rax, %rbx
.Ltmp245:
	leaq	8(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp246:
# BB#6:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB53_7:
.Ltmp247:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end53:
	.size	_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii, .Lfunc_end53-_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table53:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin10-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp242-.Lfunc_begin10 #   Call between .Lfunc_begin10 and .Ltmp242
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp242-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp243-.Ltmp242       #   Call between .Ltmp242 and .Ltmp243
	.long	.Ltmp244-.Lfunc_begin10 #     jumps to .Ltmp244
	.byte	0                       #   On action: cleanup
	.long	.Ltmp243-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp245-.Ltmp243       #   Call between .Ltmp243 and .Ltmp245
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp245-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp246-.Ltmp245       #   Call between .Ltmp245 and .Ltmp246
	.long	.Ltmp247-.Lfunc_begin10 #     jumps to .Ltmp247
	.byte	1                       #   On action: 1
	.long	.Ltmp246-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Lfunc_end53-.Ltmp246   #   Call between .Ltmp246 and .Lfunc_end53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev,@function
_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev: # @_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end54:
	.size	_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev, .Lfunc_end54-_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi254:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi255:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi256:
	.cfi_def_cfa_offset 48
.Lcfi257:
	.cfi_offset %rbx, -40
.Lcfi258:
	.cfi_offset %r12, -32
.Lcfi259:
	.cfi_offset %r14, -24
.Lcfi260:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$64, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV27btGImpactCollisionAlgorithm+16, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end55:
	.size	_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end55-_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.type	_ZTV27btGImpactCollisionAlgorithm,@object # @_ZTV27btGImpactCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV27btGImpactCollisionAlgorithm
	.p2align	3
_ZTV27btGImpactCollisionAlgorithm:
	.quad	0
	.quad	_ZTI27btGImpactCollisionAlgorithm
	.quad	_ZN27btGImpactCollisionAlgorithmD2Ev
	.quad	_ZN27btGImpactCollisionAlgorithmD0Ev
	.quad	_ZN27btGImpactCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN27btGImpactCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV27btGImpactCollisionAlgorithm, 56

	.type	g_gimpact_cf,@object    # @g_gimpact_cf
	.data
	.globl	g_gimpact_cf
	.p2align	3
g_gimpact_cf:
	.quad	_ZTVN27btGImpactCollisionAlgorithm10CreateFuncE+16
	.byte	0                       # 0x0
	.zero	7
	.size	g_gimpact_cf, 16

	.type	_ZTS27btGImpactCollisionAlgorithm,@object # @_ZTS27btGImpactCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTS27btGImpactCollisionAlgorithm
	.p2align	4
_ZTS27btGImpactCollisionAlgorithm:
	.asciz	"27btGImpactCollisionAlgorithm"
	.size	_ZTS27btGImpactCollisionAlgorithm, 30

	.type	_ZTI27btGImpactCollisionAlgorithm,@object # @_ZTI27btGImpactCollisionAlgorithm
	.globl	_ZTI27btGImpactCollisionAlgorithm
	.p2align	4
_ZTI27btGImpactCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27btGImpactCollisionAlgorithm
	.quad	_ZTI30btActivatingCollisionAlgorithm
	.size	_ZTI27btGImpactCollisionAlgorithm, 24

	.type	_ZTV17btTriangleShapeEx,@object # @_ZTV17btTriangleShapeEx
	.section	.rodata._ZTV17btTriangleShapeEx,"aG",@progbits,_ZTV17btTriangleShapeEx,comdat
	.weak	_ZTV17btTriangleShapeEx
	.p2align	3
_ZTV17btTriangleShapeEx:
	.quad	0
	.quad	_ZTI17btTriangleShapeEx
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN17btTriangleShapeExD0Ev
	.quad	_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK15btTriangleShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK15btTriangleShape14getNumVerticesEv
	.quad	_ZNK15btTriangleShape11getNumEdgesEv
	.quad	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.quad	_ZNK15btTriangleShape9getVertexEiR9btVector3
	.quad	_ZNK15btTriangleShape12getNumPlanesEv
	.quad	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.quad	_ZNK15btTriangleShape8isInsideERK9btVector3f
	.quad	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.size	_ZTV17btTriangleShapeEx, 224

	.type	_ZTS17btTriangleShapeEx,@object # @_ZTS17btTriangleShapeEx
	.section	.rodata._ZTS17btTriangleShapeEx,"aG",@progbits,_ZTS17btTriangleShapeEx,comdat
	.weak	_ZTS17btTriangleShapeEx
	.p2align	4
_ZTS17btTriangleShapeEx:
	.asciz	"17btTriangleShapeEx"
	.size	_ZTS17btTriangleShapeEx, 20

	.type	_ZTS15btTriangleShape,@object # @_ZTS15btTriangleShape
	.section	.rodata._ZTS15btTriangleShape,"aG",@progbits,_ZTS15btTriangleShape,comdat
	.weak	_ZTS15btTriangleShape
	.p2align	4
_ZTS15btTriangleShape:
	.asciz	"15btTriangleShape"
	.size	_ZTS15btTriangleShape, 18

	.type	_ZTI15btTriangleShape,@object # @_ZTI15btTriangleShape
	.section	.rodata._ZTI15btTriangleShape,"aG",@progbits,_ZTI15btTriangleShape,comdat
	.weak	_ZTI15btTriangleShape
	.p2align	4
_ZTI15btTriangleShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btTriangleShape
	.quad	_ZTI23btPolyhedralConvexShape
	.size	_ZTI15btTriangleShape, 24

	.type	_ZTI17btTriangleShapeEx,@object # @_ZTI17btTriangleShapeEx
	.section	.rodata._ZTI17btTriangleShapeEx,"aG",@progbits,_ZTI17btTriangleShapeEx,comdat
	.weak	_ZTI17btTriangleShapeEx
	.p2align	4
_ZTI17btTriangleShapeEx:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS17btTriangleShapeEx
	.quad	_ZTI15btTriangleShape
	.size	_ZTI17btTriangleShapeEx, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Triangle"
	.size	.L.str, 9

	.type	_ZTV20btTetrahedronShapeEx,@object # @_ZTV20btTetrahedronShapeEx
	.section	.rodata._ZTV20btTetrahedronShapeEx,"aG",@progbits,_ZTV20btTetrahedronShapeEx,comdat
	.weak	_ZTV20btTetrahedronShapeEx
	.p2align	3
_ZTV20btTetrahedronShapeEx:
	.quad	0
	.quad	_ZTI20btTetrahedronShapeEx
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN20btTetrahedronShapeExD0Ev
	.quad	_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK16btBU_Simplex1to47getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK16btBU_Simplex1to414getNumVerticesEv
	.quad	_ZNK16btBU_Simplex1to411getNumEdgesEv
	.quad	_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_
	.quad	_ZNK16btBU_Simplex1to49getVertexEiR9btVector3
	.quad	_ZNK16btBU_Simplex1to412getNumPlanesEv
	.quad	_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i
	.quad	_ZNK16btBU_Simplex1to48isInsideERK9btVector3f
	.quad	_ZNK16btBU_Simplex1to48getIndexEi
	.size	_ZTV20btTetrahedronShapeEx, 224

	.type	_ZTS20btTetrahedronShapeEx,@object # @_ZTS20btTetrahedronShapeEx
	.section	.rodata._ZTS20btTetrahedronShapeEx,"aG",@progbits,_ZTS20btTetrahedronShapeEx,comdat
	.weak	_ZTS20btTetrahedronShapeEx
	.p2align	4
_ZTS20btTetrahedronShapeEx:
	.asciz	"20btTetrahedronShapeEx"
	.size	_ZTS20btTetrahedronShapeEx, 23

	.type	_ZTI20btTetrahedronShapeEx,@object # @_ZTI20btTetrahedronShapeEx
	.section	.rodata._ZTI20btTetrahedronShapeEx,"aG",@progbits,_ZTI20btTetrahedronShapeEx,comdat
	.weak	_ZTI20btTetrahedronShapeEx
	.p2align	4
_ZTI20btTetrahedronShapeEx:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20btTetrahedronShapeEx
	.quad	_ZTI16btBU_Simplex1to4
	.size	_ZTI20btTetrahedronShapeEx, 24

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"btBU_Simplex1to4"
	.size	.L.str.1, 17

	.type	_ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE,@object # @_ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE
	.section	.rodata._ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE,"aG",@progbits,_ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE,comdat
	.weak	_ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE
	.p2align	3
_ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE:
	.quad	0
	.quad	_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE
	.quad	_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi
	.quad	_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev
	.quad	_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev
	.size	_ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE, 40

	.type	_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE,@object # @_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE
	.section	.rodata._ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE,"aG",@progbits,_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE,comdat
	.weak	_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE
	.p2align	4
_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE:
	.asciz	"N18GIM_ShapeRetriever19ChildShapeRetrieverE"
	.size	_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE, 44

	.type	_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE,@object # @_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE
	.section	.rodata._ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE,"aG",@progbits,_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE,comdat
	.weak	_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE
	.p2align	3
_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE
	.size	_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE, 16

	.type	_ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE,@object # @_ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE
	.section	.rodata._ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE,"aG",@progbits,_ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE,comdat
	.weak	_ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE
	.p2align	3
_ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE:
	.quad	0
	.quad	_ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE
	.quad	_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi
	.quad	_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev
	.quad	_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev
	.size	_ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE, 40

	.type	_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE,@object # @_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE
	.section	.rodata._ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE,"aG",@progbits,_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE,comdat
	.weak	_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE
	.p2align	4
_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE:
	.asciz	"N18GIM_ShapeRetriever22TriangleShapeRetrieverE"
	.size	_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE, 47

	.type	_ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE,@object # @_ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE
	.section	.rodata._ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE,"aG",@progbits,_ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE,comdat
	.weak	_ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE
	.p2align	4
_ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE
	.quad	_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE
	.size	_ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE, 24

	.type	_ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE,@object # @_ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE
	.section	.rodata._ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE,"aG",@progbits,_ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE,comdat
	.weak	_ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE
	.p2align	3
_ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE:
	.quad	0
	.quad	_ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE
	.quad	_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi
	.quad	_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev
	.quad	_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev
	.size	_ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE, 40

	.type	_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE,@object # @_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE
	.section	.rodata._ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE,"aG",@progbits,_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE,comdat
	.weak	_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE
	.p2align	4
_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE:
	.asciz	"N18GIM_ShapeRetriever19TetraShapeRetrieverE"
	.size	_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE, 44

	.type	_ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE,@object # @_ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE
	.section	.rodata._ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE,"aG",@progbits,_ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE,comdat
	.weak	_ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE
	.p2align	4
_ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE
	.quad	_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE
	.size	_ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE, 24

	.type	_ZTV25btGImpactTriangleCallback,@object # @_ZTV25btGImpactTriangleCallback
	.section	.rodata._ZTV25btGImpactTriangleCallback,"aG",@progbits,_ZTV25btGImpactTriangleCallback,comdat
	.weak	_ZTV25btGImpactTriangleCallback
	.p2align	3
_ZTV25btGImpactTriangleCallback:
	.quad	0
	.quad	_ZTI25btGImpactTriangleCallback
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZN25btGImpactTriangleCallbackD0Ev
	.quad	_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii
	.size	_ZTV25btGImpactTriangleCallback, 40

	.type	_ZTS25btGImpactTriangleCallback,@object # @_ZTS25btGImpactTriangleCallback
	.section	.rodata._ZTS25btGImpactTriangleCallback,"aG",@progbits,_ZTS25btGImpactTriangleCallback,comdat
	.weak	_ZTS25btGImpactTriangleCallback
	.p2align	4
_ZTS25btGImpactTriangleCallback:
	.asciz	"25btGImpactTriangleCallback"
	.size	_ZTS25btGImpactTriangleCallback, 28

	.type	_ZTI25btGImpactTriangleCallback,@object # @_ZTI25btGImpactTriangleCallback
	.section	.rodata._ZTI25btGImpactTriangleCallback,"aG",@progbits,_ZTI25btGImpactTriangleCallback,comdat
	.weak	_ZTI25btGImpactTriangleCallback
	.p2align	4
_ZTI25btGImpactTriangleCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25btGImpactTriangleCallback
	.quad	_ZTI18btTriangleCallback
	.size	_ZTI25btGImpactTriangleCallback, 24

	.type	_ZTVN27btGImpactCollisionAlgorithm10CreateFuncE,@object # @_ZTVN27btGImpactCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTVN27btGImpactCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN27btGImpactCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN27btGImpactCollisionAlgorithm10CreateFuncE
	.p2align	3
_ZTVN27btGImpactCollisionAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN27btGImpactCollisionAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev
	.quad	_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN27btGImpactCollisionAlgorithm10CreateFuncE, 40

	.type	_ZTSN27btGImpactCollisionAlgorithm10CreateFuncE,@object # @_ZTSN27btGImpactCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTSN27btGImpactCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN27btGImpactCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN27btGImpactCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTSN27btGImpactCollisionAlgorithm10CreateFuncE:
	.asciz	"N27btGImpactCollisionAlgorithm10CreateFuncE"
	.size	_ZTSN27btGImpactCollisionAlgorithm10CreateFuncE, 44

	.type	_ZTS30btCollisionAlgorithmCreateFunc,@object # @_ZTS30btCollisionAlgorithmCreateFunc
	.section	.rodata._ZTS30btCollisionAlgorithmCreateFunc,"aG",@progbits,_ZTS30btCollisionAlgorithmCreateFunc,comdat
	.weak	_ZTS30btCollisionAlgorithmCreateFunc
	.p2align	4
_ZTS30btCollisionAlgorithmCreateFunc:
	.asciz	"30btCollisionAlgorithmCreateFunc"
	.size	_ZTS30btCollisionAlgorithmCreateFunc, 33

	.type	_ZTI30btCollisionAlgorithmCreateFunc,@object # @_ZTI30btCollisionAlgorithmCreateFunc
	.section	.rodata._ZTI30btCollisionAlgorithmCreateFunc,"aG",@progbits,_ZTI30btCollisionAlgorithmCreateFunc,comdat
	.weak	_ZTI30btCollisionAlgorithmCreateFunc
	.p2align	3
_ZTI30btCollisionAlgorithmCreateFunc:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS30btCollisionAlgorithmCreateFunc
	.size	_ZTI30btCollisionAlgorithmCreateFunc, 16

	.type	_ZTIN27btGImpactCollisionAlgorithm10CreateFuncE,@object # @_ZTIN27btGImpactCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTIN27btGImpactCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN27btGImpactCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN27btGImpactCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTIN27btGImpactCollisionAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN27btGImpactCollisionAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN27btGImpactCollisionAlgorithm10CreateFuncE, 24


	.globl	_ZN27btGImpactCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.type	_ZN27btGImpactCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN27btGImpactCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_ = _ZN27btGImpactCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.globl	_ZN27btGImpactCollisionAlgorithmD1Ev
	.type	_ZN27btGImpactCollisionAlgorithmD1Ev,@function
_ZN27btGImpactCollisionAlgorithmD1Ev = _ZN27btGImpactCollisionAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
