	.text
	.file	"db-meth.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	39                      # 0x27
.LCPI0_1:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	11                      # 0xb
	.long	5                       # 0x5
	.text
	.globl	_Z10invokefuncP9Classfile
	.p2align	4, 0x90
	.type	_Z10invokefuncP9Classfile,@function
_Z10invokefuncP9Classfile:              # @_Z10invokefuncP9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1080, %rsp             # imm = 0x438
.Lcfi6:
	.cfi_def_cfa_offset 1136
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %r13d
	leal	2(%r13), %eax
	movl	%eax, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %ecx
	shlq	$8, %rcx
	movzbl	1(%rax), %eax
	orq	%rcx, %rax
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	40(%rdi), %rcx
	shlq	$4, %rax
	movq	8(%rcx,%rax), %r14
	movzwl	2(%r14), %eax
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rax
	movzwl	(%r14), %edx
	shlq	$4, %rdx
	movq	8(%rcx,%rdx), %rdx
	shlq	$4, %rdx
	movq	8(%rcx,%rdx), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movzwl	2(%rax), %edx
	shlq	$4, %rdx
	movq	8(%rcx,%rdx), %rbp
	movzwl	(%rax), %eax
	shlq	$4, %rax
	movq	8(%rcx,%rax), %r12
	movq	%rbp, %rdi
	callq	strlen
	addq	$-2, %rax
	movl	$8, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp0:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp1:
# BB#1:                                 # %.noexc219
	decl	%r13d
	movq	%r12, (%rbx)
	movl	$0, 8(%rbx)
	movl	$0, 12(%rbx)
	movl	$0, 16(%rbx)
	movl	$1, 8(%r15)
	movl	%r13d, 16(%r15)
	movl	%r13d, 12(%r15)
.Ltmp2:
	movl	$24, %edi
	callq	_Znwm
.Ltmp3:
# BB#2:                                 # %_ZN3ExpC2EjPc4Type3Loci.exit221
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,0,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%rax, (%r15)
	cmpb	$41, 1(%rbp)
	movq	%r13, 24(%rsp)          # 8-byte Spill
	jne	.LBB0_3
# BB#75:                                # %._crit_edge287.thread
	addq	$2, %rbp
	movq	%rbp, %rdi
	callq	_Z8sig2typePc
	xorl	%r15d, %r15d
                                        # kill: %R13D<def> %R13D<kill> %R13<kill>
	jmp	.LBB0_16
.LBB0_3:                                # %.lr.ph286.preheader
	movq	%r14, (%rsp)            # 8-byte Spill
	leaq	1(%rbp), %rbx
	xorl	%r15d, %r15d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph286
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_76 Depth 2
	movl	%eax, %r14d
	movq	%rbx, %rdi
	callq	_Z8sig2typePc
	movl	%r15d, %ecx
	movl	%eax, 48(%rsp,%rcx,4)
	addq	$2, %rbp
	cmpb	$91, (%rbx)
	cmovneq	%rbx, %rbp
	cmpb	$76, (%rbp)
	jne	.LBB0_5
	.p2align	4, 0x90
.LBB0_76:                               # %.preheader
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$59, 1(%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB0_76
.LBB0_5:                                # %.backedge
                                        #   in Loop: Header=BB0_4 Depth=1
	incl	%r15d
	leaq	1(%rbp), %rbx
	leal	1(%r14), %eax
	cmpb	$41, 1(%rbp)
	jne	.LBB0_4
# BB#6:                                 # %._crit_edge287
	addq	$2, %rbp
	movq	%rbp, %rdi
	callq	_Z8sig2typePc
	testl	%r15d, %r15d
	je	.LBB0_7
# BB#8:                                 # %.lr.ph.preheader
	movq	stkptr(%rip), %rdi
	movslq	%r14d, %rcx
	leaq	44(%rsp,%rcx,4), %rbx
	xorl	%ecx, %ecx
	movl	$std_exps+48, %r9d
	movl	$std_exps+72, %r8d
                                        # kill: %R13D<def> %R13D<kill> %R13<kill>
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %r10d
	movq	%rdi, %rdx
	leaq	-8(%rdx), %rdi
	movq	%rdi, stkptr(%rip)
	movq	-8(%rdx), %rsi
	movq	(%rsi), %rdx
	cmpl	$4, 8(%rdx)
	jne	.LBB0_14
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	cmpl	$10, (%rbx)
	jne	.LBB0_14
# BB#11:                                #   in Loop: Header=BB0_9 Depth=1
	cmpq	%r9, %rdx
	je	.LBB0_13
# BB#12:                                #   in Loop: Header=BB0_9 Depth=1
	cmpq	%r8, %rdx
	jne	.LBB0_14
.LBB0_13:                               #   in Loop: Header=BB0_9 Depth=1
	addq	$312, %rdx              # imm = 0x138
	movq	%rdx, (%rsi)
	movq	stkptr(%rip), %rdi
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_9 Depth=1
	movq	(%rdi), %rdx
	movq	%rdx, (%rbp)
	addq	$8, %rbp
	movq	(%rdi), %rdx
	movl	16(%rdx), %r13d
	cmpl	%r13d, %r10d
	cmovbl	%r10d, %r13d
	addq	$-4, %rbx
	incl	%ecx
	cmpl	%r14d, %ecx
	jne	.LBB0_9
	jmp	.LBB0_15
.LBB0_7:
	xorl	%r15d, %r15d
                                        # kill: %R13D<def> %R13D<kill> %R13<kill>
.LBB0_15:
	movq	(%rsp), %r14            # 8-byte Reload
.LBB0_16:                               # %._crit_edge
	movl	ch(%rip), %ecx
	cmpl	$184, %ecx
	movl	%eax, (%rsp)            # 4-byte Spill
	je	.LBB0_59
# BB#17:                                # %._crit_edge
	cmpl	$185, %ecx
	jne	.LBB0_21
# BB#18:
	movl	currpc(%rip), %edi
	leal	1(%rdi), %ecx
	movl	%ecx, currpc(%rip)
	movl	bufflength(%rip), %ecx
	leal	-1(%rcx), %edx
	movl	%edx, bufflength(%rip)
	movq	inbuff(%rip), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, inbuff(%rip)
	movzbl	(%rdx), %esi
	decl	%esi
	cmpl	%esi, %r15d
	jne	.LBB0_19
# BB#20:                                # %.thread
	addl	$2, %edi
	movl	%edi, currpc(%rip)
	addl	$-2, %ecx
	movl	%ecx, bufflength(%rip)
	addq	$2, %rdx
	movq	%rdx, inbuff(%rip)
.LBB0_21:
	movq	stkptr(%rip), %r14
	movq	-8(%r14), %rbp
	addq	$-8, %r14
	movq	(%rbp), %rax
	cmpl	$1, 4(%rax)
	jne	.LBB0_48
# BB#22:
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movl	$.L.str.1, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_23
.LBB0_48:
	movl	$.L.str.2, %esi
	movq	%r12, %rdi
	callq	strcmp
	movl	%eax, %ebx
	movl	16(%rbp), %r12d
	cmpl	%r12d, %r13d
	cmovbl	%r13d, %r12d
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movq	(%r14), %r13
	movl	$1, 8(%rbp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 12(%rbp)
	movl	%r12d, 16(%rbp)
	testl	%ebx, %ebx
	je	.LBB0_49
# BB#54:
.Ltmp19:
	movl	$24, %edi
	callq	_Znwm
.Ltmp20:
# BB#55:
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,4,11,5]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbp)
	movq	%r13, 24(%rbp)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%rbp)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$1, 8(%rbx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 12(%rbx)
	movl	%r12d, 16(%rbx)
.Ltmp22:
	movl	$24, %edi
	callq	_Znwm
.Ltmp23:
# BB#56:
	movq	%rbp, %r13
	movl	(%rsp), %esi            # 4-byte Reload
	movl	%esi, %ecx
	movq	%rbx, %rbp
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB0_51
.LBB0_59:
	movzwl	(%r14), %eax
	shlq	$4, %rax
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	40(%rdx), %rcx
	movq	64(%rdx), %rsi
	movq	8(%rcx,%rax), %rax
	shlq	$4, %rax
	movq	8(%rcx,%rax), %r14
	movq	%r14, %rdi
	callq	strcmp
	movl	%eax, %ebx
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	testl	%ebx, %ebx
	je	.LBB0_68
# BB#60:
.Ltmp5:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp6:
	movq	24(%rsp), %r12          # 8-byte Reload
# BB#61:                                # %.noexc
	movq	%r14, (%rbx)
	movl	$0, 8(%rbx)
	movl	$0, 12(%rbx)
	movl	$0, 16(%rbx)
	movl	$1, 8(%rbp)
	movl	%r12d, 16(%rbp)
	movl	%r12d, 12(%rbp)
.Ltmp7:
	movl	$24, %edi
	callq	_Znwm
.Ltmp8:
# BB#62:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,0,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%rbp)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$1, 8(%rbx)
	movl	%r12d, 12(%rbx)
	movl	%r13d, 16(%rbx)
.Ltmp10:
	movl	$24, %edi
	callq	_Znwm
.Ltmp11:
# BB#63:
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,4,11,5]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%rbp, 24(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%rbx)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	$1, 8(%rbp)
	movl	%r12d, 12(%rbp)
	movl	%r13d, 16(%rbp)
.Ltmp13:
	movl	$24, %edi
	callq	_Znwm
.Ltmp14:
# BB#64:
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	jmp	.LBB0_70
.LBB0_49:
.Ltmp25:
	movl	$24, %edi
	callq	_Znwm
.Ltmp26:
# BB#50:
	movl	$8, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rsp), %esi            # 4-byte Reload
.LBB0_51:                               # %_ZN3ExpC2Ejj7Exptype4Type2OpPS_jPS3_.exit238
	movl	$0, (%rax)
	movl	$6, 4(%rax)
	movl	%ecx, 8(%rax)
	movl	$39, 12(%rax)
	movq	%rax, (%rbp)
	movq	%r13, 24(%rbp)
	movl	%r15d, 48(%rbp)
	movq	%rdx, 56(%rbp)
	testl	%esi, %esi
	jne	.LBB0_37
# BB#52:                                # %_ZN3ExpC2Ejj7Exptype4Type2OpPS_jPS3_.exit238
	movq	(%r13), %rax
	cmpl	$18, 12(%rax)
	je	.LBB0_37
.LBB0_36:
	movq	%r14, stkptr(%rip)
	movq	donestkptr(%rip), %r14
	leaq	8(%r14), %rax
	movq	%rax, donestkptr(%rip)
.LBB0_37:
	movq	%rbp, (%r14)
	jmp	.LBB0_71
.LBB0_68:
	movl	$1, 8(%rbp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 12(%rbp)
	movl	%r13d, 16(%rbp)
.Ltmp16:
	movl	$24, %edi
	callq	_Znwm
.Ltmp17:
# BB#69:
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB0_70:                               # %_ZN3ExpC2Ejj7Exptype4Type2OpPS_jPS3_.exit
	movl	$0, (%rax)
	movl	$6, 4(%rax)
	movl	%ecx, 8(%rax)
	movl	$39, 12(%rax)
	movq	%rax, (%rbp)
	movq	%rbx, 24(%rbp)
	movl	%r15d, 48(%rbp)
	movq	%rdx, 56(%rbp)
	testl	%ecx, %ecx
	movl	$stkptr, %eax
	movl	$donestkptr, %ecx
	cmovneq	%rax, %rcx
	movq	(%rcx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	%rbp, (%rax)
.LBB0_71:
	xorl	%eax, %eax
.LBB0_72:
	addq	$1080, %rsp             # imm = 0x438
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_23:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	64(%rax), %rsi
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	strcmp
	movl	%eax, %ebx
	movl	$.L.str.2, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%ebx, %ebx
	je	.LBB0_42
# BB#24:
	testl	%eax, %eax
	je	.LBB0_25
# BB#31:
	movl	16(%rbp), %eax
	cmpl	%eax, %r13d
	cmovbl	%r13d, %eax
	movl	%eax, %r13d
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp31:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp32:
	movq	24(%rsp), %r12          # 8-byte Reload
# BB#32:                                # %.noexc224
	movq	$.L.str.3, (%rbx)
	movl	$0, 8(%rbx)
	movl	$0, 12(%rbx)
	movl	$0, 16(%rbx)
	movl	$1, 8(%rbp)
	movl	%r12d, 16(%rbp)
	movl	%r12d, 12(%rbp)
.Ltmp33:
	movl	$24, %edi
	callq	_Znwm
.Ltmp34:
# BB#33:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,0,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%rbp)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$1, 8(%rbx)
	movl	%r12d, 12(%rbx)
	movl	%r13d, 16(%rbx)
.Ltmp36:
	movl	$24, %edi
	callq	_Znwm
.Ltmp37:
# BB#34:
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,4,11,5]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%rbp, 24(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%rbx)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	$1, 8(%rbp)
	movl	%r12d, 12(%rbp)
	movl	%r13d, 16(%rbp)
.Ltmp39:
	movl	$24, %edi
	callq	_Znwm
.Ltmp40:
# BB#35:
	movl	$0, (%rax)
	movl	$6, 4(%rax)
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ecx, 8(%rax)
	movl	$39, 12(%rax)
	movq	%rax, (%rbp)
	movq	%rbx, 24(%rbp)
	movl	%r15d, 48(%rbp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 56(%rbp)
	testl	%ecx, %ecx
	jne	.LBB0_37
	jmp	.LBB0_36
.LBB0_42:
	testl	%eax, %eax
	jne	.LBB0_44
# BB#43:
	movl	16(%rbp), %eax
	cmpl	%eax, %r13d
	cmovael	%eax, %r13d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movq	$.L.str.1, (%rax)
.LBB0_44:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$1, 8(%rbx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 12(%rbx)
	movl	%r13d, 16(%rbx)
.Ltmp42:
	movl	$24, %edi
	callq	_Znwm
.Ltmp43:
# BB#45:
	movl	$0, (%rax)
	movl	$6, 4(%rax)
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ecx, 8(%rax)
	movl	$39, 12(%rax)
	movq	%rax, (%rbx)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 24(%rbx)
	movl	%r15d, 48(%rbx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 56(%rbx)
	movq	stkptr(%rip), %rcx
	addq	$-8, %rcx
	cmpl	$0, 8(%rax)
	jne	.LBB0_47
# BB#46:
	movq	%rcx, stkptr(%rip)
	movq	donestkptr(%rip), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, donestkptr(%rip)
.LBB0_47:
	movq	%rbx, (%rcx)
	jmp	.LBB0_71
.LBB0_25:
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %rax
	movq	16(%rax), %rax
	movq	$.L.str.3, (%rax)
	movq	stkptr(%rip), %r14
	movq	-8(%r14), %rax
	movl	16(%rax), %ebp
	cmpl	%ebp, %r13d
	cmovbl	%r13d, %ebp
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$1, 8(%rbx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 12(%rbx)
	movl	%ebp, 16(%rbx)
.Ltmp28:
	movl	$24, %edi
	callq	_Znwm
.Ltmp29:
# BB#26:
	addq	$-8, %r14
	movl	$0, (%rax)
	movl	$6, 4(%rax)
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ecx, 8(%rax)
	movl	$39, 12(%rax)
	movq	%rax, (%rbx)
	movq	%r12, 24(%rbx)
	movl	%r15d, 48(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 56(%rbx)
	testl	%ecx, %ecx
	jne	.LBB0_28
# BB#27:
	movq	%r14, stkptr(%rip)
	movq	donestkptr(%rip), %r14
	leaq	8(%r14), %rax
	movq	%rax, donestkptr(%rip)
.LBB0_28:
	movq	%rbx, (%r14)
	jmp	.LBB0_71
.LBB0_19:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$60, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %eax
	jmp	.LBB0_72
.LBB0_29:
.Ltmp30:
	jmp	.LBB0_30
.LBB0_77:
.Ltmp44:
	jmp	.LBB0_30
.LBB0_41:
.Ltmp41:
	jmp	.LBB0_39
.LBB0_40:
.Ltmp38:
	jmp	.LBB0_30
.LBB0_38:
.Ltmp35:
	jmp	.LBB0_39
.LBB0_78:
.Ltmp18:
	jmp	.LBB0_39
.LBB0_53:
.Ltmp27:
	jmp	.LBB0_39
.LBB0_67:
.Ltmp15:
	jmp	.LBB0_39
.LBB0_66:
.Ltmp12:
	jmp	.LBB0_30
.LBB0_58:
.Ltmp24:
.LBB0_30:
	movq	%rax, %r14
	movq	%rbx, %rdi
	jmp	.LBB0_74
.LBB0_57:
.Ltmp21:
	jmp	.LBB0_39
.LBB0_65:
.Ltmp9:
.LBB0_39:
	movq	%rax, %r14
	movq	%rbp, %rdi
	jmp	.LBB0_74
.LBB0_73:
.Ltmp4:
	movq	%rax, %r14
	movq	%r15, %rdi
.LBB0_74:
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z10invokefuncP9Classfile, .Lfunc_end0-_Z10invokefuncP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\310\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\305\002"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp19-.Ltmp3          #   Call between .Ltmp3 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp22-.Ltmp20         #   Call between .Ltmp20 and .Ltmp22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp5-.Ltmp23          #   Call between .Ltmp23 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 8 <<
	.long	.Ltmp8-.Ltmp5           #   Call between .Ltmp5 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 9 <<
	.long	.Ltmp10-.Ltmp8          #   Call between .Ltmp8 and .Ltmp10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp13-.Ltmp11         #   Call between .Ltmp11 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp31-.Ltmp17         #   Call between .Ltmp17 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp34-.Ltmp31         #   Call between .Ltmp31 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin0   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp36-.Ltmp34         #   Call between .Ltmp34 and .Ltmp36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin0   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp39-.Ltmp37         #   Call between .Ltmp37 and .Ltmp39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin0   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp42-.Ltmp40         #   Call between .Ltmp40 and .Ltmp42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin0   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp28-.Ltmp43         #   Call between .Ltmp43 and .Ltmp28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Lfunc_end0-.Ltmp29     #   Call between .Ltmp29 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Error in interface method invocation - nargs doesn't match.\n"
	.size	.L.str, 61

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"this"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"<init>"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"super"
	.size	.L.str.3, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
