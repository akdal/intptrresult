	.text
	.file	"gim_box_set.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	1056964608              # float 0.5
.LCPI0_2:
	.long	1065353216              # float 1
.LCPI0_3:
	.long	3212836864              # float -1
	.text
	.globl	_ZN12GIM_BOX_TREE20_calc_splitting_axisER9gim_arrayI13GIM_AABB_DATAEjj
	.p2align	4, 0x90
	.type	_ZN12GIM_BOX_TREE20_calc_splitting_axisER9gim_arrayI13GIM_AABB_DATAEjj,@function
_ZN12GIM_BOX_TREE20_calc_splitting_axisER9gim_arrayI13GIM_AABB_DATAEjj: # @_ZN12GIM_BOX_TREE20_calc_splitting_axisER9gim_arrayI13GIM_AABB_DATAEjj
	.cfi_startproc
# BB#0:
	movl	%ecx, %r9d
	subl	%edx, %r9d
	xorps	%xmm3, %xmm3
	movaps	%xmm3, -24(%rsp)
	jbe	.LBB0_1
# BB#2:                                 # %.lr.ph130
	movq	(%rsi), %rax
	movl	%edx, %r8d
	movl	%ecx, %r10d
	leaq	(%r8,%r8,8), %rdi
	leaq	24(%rax,%rdi,4), %rdi
	movq	%r10, %rax
	subq	%r8, %rax
	xorps	%xmm4, %xmm4
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = <0.5,0.5,u,u>
	movss	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movsd	-8(%rdi), %xmm2         # xmm2 = mem[0],zero
	movsd	-24(%rdi), %xmm5        # xmm5 = mem[0],zero
	addps	%xmm2, %xmm5
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	addss	-16(%rdi), %xmm2
	mulps	%xmm0, %xmm5
	mulss	%xmm1, %xmm2
	addps	%xmm5, %xmm3
	addss	%xmm2, %xmm4
	addq	$36, %rdi
	decq	%rax
	jne	.LBB0_3
# BB#4:                                 # %._crit_edge131
	cmpl	%edx, %ecx
	movl	%r9d, %eax
	cvtsi2ssq	%rax, %xmm8
	jbe	.LBB0_5
# BB#6:                                 # %.lr.ph
	movss	.LCPI0_2(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	divss	%xmm8, %xmm5
	movaps	%xmm5, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm3, %xmm6
	mulss	%xmm4, %xmm5
	movq	(%rsi), %rax
	leaq	(%r8,%r8,8), %rcx
	leaq	24(%rax,%rcx,4), %rcx
	subq	%r8, %r10
	xorps	%xmm4, %xmm4
	xorps	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movsd	-8(%rcx), %xmm7         # xmm7 = mem[0],zero
	movsd	-24(%rcx), %xmm2        # xmm2 = mem[0],zero
	addps	%xmm7, %xmm2
	movss	(%rcx), %xmm7           # xmm7 = mem[0],zero,zero,zero
	addss	-16(%rcx), %xmm7
	mulps	%xmm0, %xmm2
	mulss	%xmm1, %xmm7
	subps	%xmm6, %xmm2
	subss	%xmm5, %xmm7
	mulps	%xmm2, %xmm2
	mulss	%xmm7, %xmm7
	addps	%xmm2, %xmm4
	addss	%xmm7, %xmm3
	addq	$36, %rcx
	decq	%r10
	jne	.LBB0_7
# BB#8:                                 # %._crit_edge
	movss	%xmm4, -24(%rsp)
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, -20(%rsp)
	movss	%xmm3, -16(%rsp)
	jmp	.LBB0_9
.LBB0_1:                                # %._crit_edge131.thread
	movl	%r9d, %eax
	cvtsi2ssq	%rax, %xmm8
.LBB0_5:                                # %._crit_edge131._crit_edge
	xorps	%xmm4, %xmm4
	xorps	%xmm3, %xmm3
.LBB0_9:
	addss	.LCPI0_3(%rip), %xmm8
	movss	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm8, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	movss	%xmm1, -24(%rsp)
	movaps	%xmm1, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movss	%xmm2, -20(%rsp)
	mulss	%xmm3, %xmm0
	movss	%xmm0, -16(%rsp)
	xorl	%ecx, %ecx
	ucomiss	%xmm1, %xmm2
	seta	%cl
	ucomiss	-24(%rsp,%rcx,4), %xmm0
	movl	$2, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end0:
	.size	_ZN12GIM_BOX_TREE20_calc_splitting_axisER9gim_arrayI13GIM_AABB_DATAEjj, .Lfunc_end0-_ZN12GIM_BOX_TREE20_calc_splitting_axisER9gim_arrayI13GIM_AABB_DATAEjj
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZN12GIM_BOX_TREE30_sort_and_calc_splitting_indexER9gim_arrayI13GIM_AABB_DATAEjjj
	.p2align	4, 0x90
	.type	_ZN12GIM_BOX_TREE30_sort_and_calc_splitting_indexER9gim_arrayI13GIM_AABB_DATAEjjj,@function
_ZN12GIM_BOX_TREE30_sort_and_calc_splitting_indexER9gim_arrayI13GIM_AABB_DATAEjjj: # @_ZN12GIM_BOX_TREE30_sort_and_calc_splitting_indexER9gim_arrayI13GIM_AABB_DATAEjjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
.Lcfi4:
	.cfi_offset %rbx, -40
.Lcfi5:
	.cfi_offset %r14, -32
.Lcfi6:
	.cfi_offset %r15, -24
.Lcfi7:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%ecx, %r9d
	subl	%edx, %r9d
	movl	%edx, %eax
	jbe	.LBB1_12
# BB#1:                                 # %.lr.ph63
	movq	(%rsi), %rax
	movl	%r8d, %r8d
	movl	%edx, %r10d
	movl	%ecx, %r14d
	leaq	-1(%r14), %rdi
	testb	$1, %r9b
	jne	.LBB1_3
# BB#2:
	xorps	%xmm1, %xmm1
                                        # implicit-def: %XMM0
	movq	%r10, %rbx
	cmpq	%r10, %rdi
	jne	.LBB1_5
	jmp	.LBB1_7
.LBB1_3:
	leaq	(%r10,%r10,8), %rbx
	leaq	(%rax,%rbx,4), %rbx
	movss	16(%rbx,%r8,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	(%rbx,%r8,4), %xmm1
	mulss	.LCPI1_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	leaq	1(%r10), %rbx
	movaps	%xmm0, %xmm1
	cmpq	%r10, %rdi
	je	.LBB1_7
.LBB1_5:                                # %.lr.ph63.new
	movq	%r14, %rdi
	subq	%rbx, %rdi
	leaq	(%rbx,%rbx,8), %rbx
	leaq	(%rax,%rbx,4), %rax
	leaq	52(%rax,%r8,4), %rax
	movss	.LCPI1_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB1_6:                                # =>This Inner Loop Header: Depth=1
	movss	-36(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-52(%rax), %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm1, %xmm3
	addss	-16(%rax), %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm3, %xmm0
	addq	$72, %rax
	addq	$-2, %rdi
	movaps	%xmm0, %xmm1
	jne	.LBB1_6
.LBB1_7:                                # %._crit_edge64
	cmpl	%edx, %ecx
	movl	%edx, %eax
	jbe	.LBB1_12
# BB#8:                                 # %.lr.ph
	movl	%r9d, %eax
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rax, %xmm1
	divss	%xmm1, %xmm0
	leaq	-24(%rsp), %r11
	leaq	(%r10,%r10,8), %rax
	leaq	16(,%rax,4), %rbx
	shlq	$2, %r8
	subq	%r10, %r14
	movss	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movl	%edx, %eax
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rdi
	leaq	(%rdi,%r8), %r10
	movss	(%rbx,%r10), %xmm2      # xmm2 = mem[0],zero,zero,zero
	addss	-16(%rbx,%r10), %xmm2
	mulss	%xmm1, %xmm2
	ucomiss	%xmm0, %xmm2
	jbe	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_9 Depth=1
	movl	%eax, %r10d
	movups	-16(%rdi,%rbx), %xmm2
	movaps	%xmm2, -40(%rsp)
	movups	(%rdi,%rbx), %xmm2
	movups	%xmm2, (%r11)
	movl	16(%rdi,%rbx), %r15d
	leaq	(%r10,%r10,8), %r10
	movl	32(%rdi,%r10,4), %ebp
	movl	%ebp, 16(%rdi,%rbx)
	movups	16(%rdi,%r10,4), %xmm2
	movups	%xmm2, (%rdi,%rbx)
	movups	(%rdi,%r10,4), %xmm2
	movups	%xmm2, -16(%rdi,%rbx)
	movaps	-40(%rsp), %xmm2
	movaps	-24(%rsp), %xmm3
	movups	%xmm3, 16(%rdi,%r10,4)
	movups	%xmm2, (%rdi,%r10,4)
	movl	%r15d, 32(%rdi,%r10,4)
	incl	%eax
.LBB1_11:                               #   in Loop: Header=BB1_9 Depth=1
	addq	$36, %rbx
	decq	%r14
	jne	.LBB1_9
.LBB1_12:                               # %._crit_edge
	movl	%r9d, %edi
	movl	$2863311531, %esi       # imm = 0xAAAAAAAB
	imulq	%rdi, %rsi
	shrq	$33, %rsi
	leal	(%rsi,%rdx), %edi
	cmpl	%edi, %eax
	jbe	.LBB1_14
# BB#13:
	decl	%ecx
	subl	%esi, %ecx
	cmpl	%ecx, %eax
	jb	.LBB1_15
.LBB1_14:                               # %.critedge
	shrl	%r9d
	addl	%edx, %r9d
	movl	%r9d, %eax
.LBB1_15:
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN12GIM_BOX_TREE30_sort_and_calc_splitting_indexER9gim_arrayI13GIM_AABB_DATAEjjj, .Lfunc_end1-_ZN12GIM_BOX_TREE30_sort_and_calc_splitting_indexER9gim_arrayI13GIM_AABB_DATAEjjj
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	2139095039              # float 3.40282347E+38
.LCPI2_1:
	.long	4286578687              # float -3.40282347E+38
.LCPI2_2:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj
	.p2align	4, 0x90
	.type	_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj,@function
_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj: # @_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 128
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r12
	movl	(%r12), %edi
	leal	1(%rdi), %eax
	movl	%eax, (%r12)
	movl	%ecx, %r15d
	subl	%edx, %r15d
	movq	8(%r12), %r8
	cmpl	$1, %r15d
	jne	.LBB2_2
# BB#1:
	shlq	$4, %rdi
	leaq	(%rdi,%rdi,2), %rcx
	movq	$0, 32(%r8,%rcx)
	movl	$0, 40(%r8,%rcx)
	movl	%edx, %edx
	movq	(%rsi), %rax
	shlq	$2, %rdx
	leaq	(%rdx,%rdx,8), %rdx
	movdqu	(%rax,%rdx), %xmm0
	movdqu	16(%rax,%rdx), %xmm1
	movdqu	%xmm1, 16(%r8,%rcx)
	movdqu	%xmm0, (%r8,%rcx)
	movq	(%rsi), %rax
	movl	32(%rax,%rdx), %eax
	movq	8(%r12), %rdx
	movl	%eax, 44(%rdx,%rcx)
	jmp	.LBB2_8
.LBB2_2:
	leaq	(%rdi,%rdi,2), %rbp
	shlq	$4, %rbp
	movabsq	$9187343237679939583, %rax # imm = 0x7F7FFFFF7F7FFFFF
	movq	%rax, (%r8,%rbp)
	movl	$2139095039, 8(%r8,%rbp) # imm = 0x7F7FFFFF
	movl	$-8388609, 16(%r8,%rbp) # imm = 0xFF7FFFFF
	movabsq	$-36028797027352577, %rax # imm = 0xFF7FFFFFFF7FFFFF
	movq	%rax, 20(%r8,%rbp)
	cmpl	%edx, %ecx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
	jbe	.LBB2_3
# BB#9:                                 # %.lr.ph
	leaq	4(%r8,%rbp), %r9
	leaq	16(%r8,%rbp), %r10
	addq	%rbp, %r8
	movq	(%rsi), %rsi
	movq	%rdx, %r14
	movl	%edx, %eax
	movl	%ecx, %r13d
	leaq	(%rax,%rax,8), %rdi
	leaq	16(%rsi,%rdi,4), %rsi
	movq	%r13, %rdi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	subq	%rax, %rdi
	movd	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movd	.LCPI2_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm3
	movdqa	%xmm2, %xmm4
	movdqa	%xmm2, %xmm5
	.p2align	4, 0x90
.LBB2_10:                               # =>This Inner Loop Header: Depth=1
	leaq	-16(%rsi), %rbp
	ucomiss	-16(%rsi), %xmm5
	movq	%r8, %rax
	cmovaq	%rbp, %rax
	movl	(%rax), %r11d
	movl	%r11d, (%r8)
	ucomiss	-12(%rsi), %xmm4
	movq	%r8, %rbx
	cmovaq	%rbp, %rbx
	movl	4(%rbx), %ebx
	movl	%ebx, (%r9)
	ucomiss	-8(%rsi), %xmm2
	cmovbeq	%r8, %rbp
	movl	8(%rbp), %ebp
	movl	%ebp, 4(%r9)
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	movq	%r10, %rdx
	cmovaq	%rsi, %rdx
	movl	(%rdx), %edx
	movl	%edx, (%r10)
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	movq	%r10, %rcx
	cmovaq	%rsi, %rcx
	movl	4(%rcx), %ecx
	movl	%ecx, 16(%r9)
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movq	%r10, %rax
	cmovaq	%rsi, %rax
	movl	8(%rax), %eax
	movl	%eax, 20(%r9)
	movd	%r11d, %xmm5
	movd	%ebx, %xmm4
	movd	%ebp, %xmm2
	movd	%edx, %xmm3
	movd	%ecx, %xmm1
	movd	%eax, %xmm0
	addq	$36, %rsi
	decq	%rdi
	jne	.LBB2_10
# BB#11:                                # %._crit_edge
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r14, %rbp
	movl	%ebp, %edx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebx, %ecx
	callq	_ZN12GIM_BOX_TREE20_calc_splitting_axisER9gim_arrayI13GIM_AABB_DATAEjj
	movq	%rbp, %rdx
	movl	%ebx, %esi
	subl	%edx, %esi
	movl	%edx, %r14d
	jbe	.LBB2_4
# BB#12:                                # %.lr.ph63.i
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx), %rbp
	movl	%eax, %r9d
	leaq	-1(%r13), %rcx
	testb	$1, %sil
	movq	16(%rsp), %rax          # 8-byte Reload
	jne	.LBB2_14
# BB#13:
	pxor	%xmm1, %xmm1
                                        # implicit-def: %XMM0
	movq	%rax, %rsi
	subq	%rax, %rcx
	jne	.LBB2_16
	jmp	.LBB2_18
.LBB2_3:                                # %._crit_edge.thread
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rdx, %rbx
	callq	_ZN12GIM_BOX_TREE20_calc_splitting_axisER9gim_arrayI13GIM_AABB_DATAEjj
	movq	%rbx, %rdx
	movl	%edx, %r14d
	jmp	.LBB2_4
.LBB2_14:
	leaq	(%rax,%rax,8), %rsi
	leaq	(%rbp,%rsi,4), %rsi
	movss	16(%rsi,%r9,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	(%rsi,%r9,4), %xmm1
	mulss	.LCPI2_2(%rip), %xmm1
	pxor	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	leaq	1(%rax), %rsi
	movaps	%xmm0, %xmm1
	subq	%rax, %rcx
	je	.LBB2_18
.LBB2_16:                               # %.lr.ph63.i.new
	subq	%rsi, %r13
	leaq	(%rsi,%rsi,8), %rsi
	leaq	(%rbp,%rsi,4), %rsi
	leaq	52(%rsi,%r9,4), %rsi
	movss	.LCPI2_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB2_17:                               # =>This Inner Loop Header: Depth=1
	movss	-36(%rsi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-52(%rsi), %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm1, %xmm3
	addss	-16(%rsi), %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm3, %xmm0
	addq	$72, %rsi
	addq	$-2, %r13
	movaps	%xmm0, %xmm1
	jne	.LBB2_17
.LBB2_18:                               # %.lr.ph.i
	movl	%r15d, %esi
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rsi, %xmm1
	divss	%xmm1, %xmm0
	leaq	48(%rsp), %r8
	leaq	(%rax,%rax,8), %rdi
	leaq	16(,%rdi,4), %rdi
	shlq	$2, %r9
	movss	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movl	%edx, %r14d
	movq	(%rsp), %r10            # 8-byte Reload
	jmp	.LBB2_19
	.p2align	4, 0x90
.LBB2_22:                               # %._crit_edge41
                                        #   in Loop: Header=BB2_19 Depth=1
	movq	(%r10), %rbp
	addq	$36, %rdi
	decq	%rcx
.LBB2_19:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rbp,%r9), %rbx
	movss	(%rdi,%rbx), %xmm2      # xmm2 = mem[0],zero,zero,zero
	addss	-16(%rdi,%rbx), %xmm2
	mulss	%xmm1, %xmm2
	ucomiss	%xmm0, %xmm2
	jbe	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_19 Depth=1
	movl	%r14d, %ebx
	movups	-16(%rbp,%rdi), %xmm2
	movaps	%xmm2, 32(%rsp)
	movups	(%rbp,%rdi), %xmm2
	movups	%xmm2, (%r8)
	movl	16(%rbp,%rdi), %esi
	leaq	(%rbx,%rbx,8), %rbx
	movl	32(%rbp,%rbx,4), %eax
	movl	%eax, 16(%rbp,%rdi)
	movups	16(%rbp,%rbx,4), %xmm2
	movups	%xmm2, (%rbp,%rdi)
	movups	(%rbp,%rbx,4), %xmm2
	movups	%xmm2, -16(%rbp,%rdi)
	movaps	32(%rsp), %xmm2
	movaps	48(%rsp), %xmm3
	movups	%xmm3, 16(%rbp,%rbx,4)
	movups	%xmm2, (%rbp,%rbx,4)
	movl	%esi, 32(%rbp,%rbx,4)
	incl	%r14d
.LBB2_21:                               #   in Loop: Header=BB2_19 Depth=1
	testq	%rcx, %rcx
	jne	.LBB2_22
.LBB2_4:                                # %._crit_edge.i
	movl	%r15d, %ecx
	movl	$2863311531, %eax       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rdx), %ecx
	cmpl	%ecx, %r14d
	movq	8(%rsp), %r13           # 8-byte Reload
	jbe	.LBB2_6
# BB#5:
	leal	-1(%r13), %ecx
	subl	%eax, %ecx
	cmpl	%ecx, %r14d
	jb	.LBB2_7
.LBB2_6:                                # %.critedge.i
	shrl	%r15d
	addl	%edx, %r15d
	movl	%r15d, %r14d
.LBB2_7:                                # %_ZN12GIM_BOX_TREE30_sort_and_calc_splitting_indexER9gim_arrayI13GIM_AABB_DATAEjjj.exit
	movl	(%r12), %eax
	movq	8(%r12), %rcx
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rbx
	movl	%eax, 32(%rcx,%rbx)
	movq	%r12, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r14d, %ecx
	callq	_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj
	movl	(%r12), %eax
	movq	8(%r12), %rcx
	movl	%eax, 36(%rcx,%rbx)
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movl	%r14d, %edx
	movl	%r13d, %ecx
	callq	_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj
	movl	(%r12), %eax
	subl	%r15d, %eax
	movq	8(%r12), %rcx
	movl	%eax, 40(%rcx,%rbx)
.LBB2_8:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj, .Lfunc_end2-_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj
	.cfi_endproc

	.globl	_ZN12GIM_BOX_TREE10build_treeER9gim_arrayI13GIM_AABB_DATAE
	.p2align	4, 0x90
	.type	_ZN12GIM_BOX_TREE10build_treeER9gim_arrayI13GIM_AABB_DATAE,@function
_ZN12GIM_BOX_TREE10build_treeER9gim_arrayI13GIM_AABB_DATAE: # @_ZN12GIM_BOX_TREE10build_treeER9gim_arrayI13GIM_AABB_DATAE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 80
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$0, (%rbx)
	movl	8(%r14), %ecx
	leal	(%rcx,%rcx), %ebp
	movl	16(%rbx), %eax
	cmpl	%ebp, %eax
	jae	.LBB3_10
# BB#1:
	cmpl	%ebp, 20(%rbx)
	jae	.LBB3_6
# BB#2:
	testl	%eax, %eax
	je	.LBB3_4
# BB#3:
	movq	8(%rbx), %rdi
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rsi
	movl	%ebp, %eax
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rdx
	callq	_Z11gim_reallocPvmm
	jmp	.LBB3_5
.LBB3_10:
	jbe	.LBB3_12
# BB#11:
	movl	%ebp, 16(%rbx)
	jmp	.LBB3_12
.LBB3_4:
	movl	%ebp, %eax
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rdi
	callq	_Z9gim_allocm
.LBB3_5:                                # %_ZN9gim_arrayI17GIM_BOX_TREE_NODEE10resizeDataEj.exit.i.i
	movq	%rax, 8(%rbx)
	movl	%ebp, 20(%rbx)
	movl	16(%rbx), %eax
.LBB3_6:                                # %_ZN9gim_arrayI17GIM_BOX_TREE_NODEE7reserveEj.exit.i
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rsp)
	cmpl	%ebp, %eax
	jae	.LBB3_9
	.p2align	4, 0x90
.LBB3_7:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rcx
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movups	(%rsp), %xmm0
	movups	16(%rsp), %xmm1
	movups	32(%rsp), %xmm2
	movups	%xmm2, 32(%rcx,%rax)
	movups	%xmm1, 16(%rcx,%rax)
	movups	%xmm0, (%rcx,%rax)
	movl	16(%rbx), %eax
	incl	%eax
	movl	%eax, 16(%rbx)
	cmpl	%ebp, %eax
	jb	.LBB3_7
.LBB3_9:                                # %._crit_edge.i
	movl	8(%r14), %ecx
.LBB3_12:                               # %_ZN9gim_arrayI17GIM_BOX_TREE_NODEE6resizeEjb.exit
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj # TAILCALL
.Lfunc_end3:
	.size	_ZN12GIM_BOX_TREE10build_treeER9gim_arrayI13GIM_AABB_DATAE, .Lfunc_end3-_ZN12GIM_BOX_TREE10build_treeER9gim_arrayI13GIM_AABB_DATAE
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
