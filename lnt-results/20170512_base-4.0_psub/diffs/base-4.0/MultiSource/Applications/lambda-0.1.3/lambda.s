	.text
	.file	"lambda.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$8792, %rsp             # imm = 0x2258
.Lcfi6:
	.cfi_def_cfa_offset 8848
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$.Lstr, %edi
	callq	puts
	leaq	32(%rsp), %rdi
	movl	$4096, %esi             # imm = 0x1000
	callq	getcwd
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rdi
	cmoveq	%rbx, %rdi
	callq	puts
.LBB0_2:
	leaq	32(%rsp), %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN12token_streamC1EPKc
.Ltmp0:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN12token_stream4openEPKc
.Ltmp1:
# BB#3:
.Ltmp2:
	leaq	16(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN24lambda_expression_parserC1EP12token_stream
.Ltmp3:
# BB#4:
	movq	$0, 8(%rsp)
	leaq	16(%rsp), %rbx
	leaq	8(%rsp), %r12
	jmp	.LBB0_5
.LBB0_71:                               #   in Loop: Header=BB0_5 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_59
.LBB0_72:                               # %.thread190
                                        #   in Loop: Header=BB0_5 Depth=1
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$3, %r14d
	jne	.LBB0_77
# BB#73:                                #   in Loop: Header=BB0_5 Depth=1
	movq	(%rbx), %rax
	movq	8(%rsp), %rsi
	andl	$-9, %r15d
.Ltmp45:
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	*112(%rax)
.Ltmp46:
	jmp	.LBB0_78
.LBB0_80:                               #   in Loop: Header=BB0_5 Depth=1
	movq	(%r13), %rax
	movq	8(%rsp), %rsi
.Ltmp50:
	movq	%r13, %rdi
	movl	%r15d, %edx
	leaq	4(%rsp), %rcx
	callq	*104(%rax)
	movq	%rax, %rbx
.Ltmp51:
# BB#81:                                #   in Loop: Header=BB0_5 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_88
# BB#82:                                #   in Loop: Header=BB0_5 Depth=1
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$3, %r14d
	jne	.LBB0_85
# BB#83:                                #   in Loop: Header=BB0_5 Depth=1
	movq	(%rbx), %rax
	movq	8(%rsp), %rsi
	andl	$-9, %r15d
.Ltmp54:
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	*112(%rax)
.Ltmp55:
	jmp	.LBB0_86
.LBB0_77:                               #   in Loop: Header=BB0_5 Depth=1
	movq	8(%rsp), %rsi
.Ltmp43:
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	_ZNK8exp_node14symbolic_printEPK9alst_nodei
.Ltmp44:
.LBB0_78:                               #   in Loop: Header=BB0_5 Depth=1
	movl	$10, %edi
	callq	putchar
	movq	(%rbx), %rax
.Ltmp47:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp48:
# BB#79:                                #   in Loop: Header=BB0_5 Depth=1
	cmpq	%r13, %rbx
	je	.LBB0_89
	.p2align	4, 0x90
.LBB0_59:                               # %.thread185
                                        #   in Loop: Header=BB0_5 Depth=1
	testq	%r13, %r13
	je	.LBB0_89
.LBB0_88:                               # %.thread197
                                        #   in Loop: Header=BB0_5 Depth=1
	movq	(%r13), %rax
.Ltmp59:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp60:
	jmp	.LBB0_89
.LBB0_85:                               #   in Loop: Header=BB0_5 Depth=1
	movq	8(%rsp), %rsi
.Ltmp52:
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	_ZNK8exp_node14symbolic_printEPK9alst_nodei
.Ltmp53:
.LBB0_86:                               #   in Loop: Header=BB0_5 Depth=1
	movl	$10, %edi
	callq	putchar
	movq	(%rbx), %rax
.Ltmp56:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp57:
# BB#87:                                #   in Loop: Header=BB0_5 Depth=1
	cmpq	%r13, %rbx
	jne	.LBB0_88
	.p2align	4, 0x90
.LBB0_89:                               # %.thread200
                                        #   in Loop: Header=BB0_5 Depth=1
	leaq	16(%rsp), %rbx
.LBB0_5:                                # %.thread203
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_42 Depth 2
                                        #       Child Loop BB0_56 Depth 3
                                        #     Child Loop BB0_24 Depth 2
                                        #       Child Loop BB0_37 Depth 3
                                        #     Child Loop BB0_62 Depth 2
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
.Ltmp5:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZN24lambda_expression_parser10expressionEPP11arglst_node
	movq	%rax, %r13
.Ltmp6:
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	testq	%r13, %r13
	je	.LBB0_5
# BB#7:                                 #   in Loop: Header=BB0_5 Depth=1
	movl	$0, 4(%rsp)
	movq	(%r13), %rax
.Ltmp8:
	movq	%r13, %rdi
	callq	*24(%rax)
.Ltmp9:
# BB#8:                                 #   in Loop: Header=BB0_5 Depth=1
	cmpl	$2, %eax
	je	.LBB0_9
# BB#17:                                #   in Loop: Header=BB0_5 Depth=1
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%r13), %rax
.Ltmp10:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	*112(%rax)
.Ltmp11:
# BB#18:                                #   in Loop: Header=BB0_5 Depth=1
	movl	$10, %edi
	callq	putchar
.Ltmp12:
	callq	_ZN4node5resetEv
.Ltmp13:
# BB#19:                                #   in Loop: Header=BB0_5 Depth=1
	xorl	%eax, %eax
	cmpl	$0, trace_lambda(%rip)
	setne	%al
	shll	$5, %eax
	leal	8(%rax), %ecx
	cmpl	$0, print_symbols(%rip)
	cmovel	%eax, %ecx
	movl	%ecx, %eax
	orl	$2, %eax
	cmpl	$0, applicative_order(%rip)
	cmovel	%ecx, %eax
	movl	%eax, %ecx
	orl	$4, %ecx
	cmpl	$0, reduce_body(%rip)
	cmovel	%eax, %ecx
	xorl	%eax, %eax
	cmpl	$0, step_lambda(%rip)
	setne	%al
	orl	%ecx, %eax
	movl	%eax, %ecx
	orl	$16, %ecx
	cmpl	$0, brief_print(%rip)
	cmovel	%eax, %ecx
	movl	%ecx, %r15d
	orl	$256, %r15d             # imm = 0x100
	cmpl	$0, reduce_fully(%rip)
	cmovel	%ecx, %r15d
	movq	(%r13), %rax
.Ltmp14:
	movq	%r13, %rdi
	callq	*24(%rax)
	movl	%eax, %r14d
.Ltmp15:
# BB#20:                                #   in Loop: Header=BB0_5 Depth=1
	cmpl	$0, step_lambda(%rip)
	je	.LBB0_60
# BB#21:                                #   in Loop: Header=BB0_5 Depth=1
	movq	(%r13), %rax
	movq	8(%rsp), %rsi
.Ltmp17:
	movq	%r13, %rdi
	movl	%r15d, %edx
	leaq	4(%rsp), %rcx
	callq	*104(%rax)
	movq	%rax, %rbx
.Ltmp18:
# BB#22:                                # %.preheader211
                                        #   in Loop: Header=BB0_5 Depth=1
	cmpl	$3, %r14d
	jne	.LBB0_42
# BB#23:                                # %.preheader211.split.us.preheader
                                        #   in Loop: Header=BB0_5 Depth=1
	movl	%r15d, %r14d
	andl	$-9, %r14d
	.p2align	4, 0x90
.LBB0_24:                               # %.preheader211.split.us
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_37 Depth 3
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#25:                                #   in Loop: Header=BB0_24 Depth=2
	movl	4(%rsp), %eax
	testb	$2, %al
	jne	.LBB0_27
# BB#26:                                #   in Loop: Header=BB0_24 Depth=2
	movl	$.L.str.5, %esi
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_27:                               #   in Loop: Header=BB0_24 Depth=2
	testb	$4, %al
	movl	$.L.str.6, %esi
	movl	$.L.str.7, %eax
	cmoveq	%rax, %rsi
.LBB0_28:                               #   in Loop: Header=BB0_24 Depth=2
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rax
	movq	8(%rsp), %rsi
.Ltmp28:
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	*112(%rax)
.Ltmp29:
# BB#29:                                #   in Loop: Header=BB0_24 Depth=2
	movl	$10, %edi
	callq	putchar
	movl	4(%rsp), %eax
	testb	$2, %al
	je	.LBB0_59
# BB#30:                                #   in Loop: Header=BB0_24 Depth=2
	andl	$-7, %eax
	movl	%eax, 4(%rsp)
	movq	(%rbx), %rax
	movq	8(%rsp), %rsi
.Ltmp31:
	movq	%rbx, %rdi
	movl	%r15d, %edx
	leaq	4(%rsp), %rcx
	callq	*104(%rax)
	movq	%rax, %rbp
.Ltmp32:
# BB#31:                                #   in Loop: Header=BB0_24 Depth=2
	testq	%rbp, %rbp
	je	.LBB0_35
# BB#32:                                #   in Loop: Header=BB0_24 Depth=2
	cmpq	%rbp, %rbx
	je	.LBB0_35
# BB#33:                                #   in Loop: Header=BB0_24 Depth=2
	movq	(%rbx), %rax
.Ltmp33:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp34:
# BB#34:                                #   in Loop: Header=BB0_24 Depth=2
	cmpq	%r13, %rbx
	movl	$0, %eax
	cmoveq	%rax, %r13
	movq	%rbp, %rbx
.LBB0_35:                               #   in Loop: Header=BB0_24 Depth=2
	testb	$2, 4(%rsp)
	je	.LBB0_59
# BB#36:                                #   in Loop: Header=BB0_24 Depth=2
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$10, %ebp
	je	.LBB0_38
	.p2align	4, 0x90
.LBB0_37:                               # %.preheader.us
                                        #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB0_37
.LBB0_38:                               # %.loopexit.us
                                        #   in Loop: Header=BB0_24 Depth=2
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#39:                                # %.loopexit.us
                                        #   in Loop: Header=BB0_24 Depth=2
	orl	$32, %ebp
	cmpl	$110, %ebp
	jne	.LBB0_24
	jmp	.LBB0_59
	.p2align	4, 0x90
.LBB0_42:                               # %.preheader211.split
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_56 Depth 3
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#43:                                #   in Loop: Header=BB0_42 Depth=2
	movl	4(%rsp), %eax
	testb	$2, %al
	jne	.LBB0_46
# BB#44:                                #   in Loop: Header=BB0_42 Depth=2
	movl	$.L.str.5, %esi
	jmp	.LBB0_47
	.p2align	4, 0x90
.LBB0_46:                               #   in Loop: Header=BB0_42 Depth=2
	testb	$4, %al
	movl	$.L.str.6, %esi
	movl	$.L.str.7, %eax
	cmoveq	%rax, %rsi
.LBB0_47:                               #   in Loop: Header=BB0_42 Depth=2
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%rsp), %rsi
.Ltmp20:
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	_ZNK8exp_node14symbolic_printEPK9alst_nodei
.Ltmp21:
# BB#48:                                #   in Loop: Header=BB0_42 Depth=2
	movl	$10, %edi
	callq	putchar
	movl	4(%rsp), %eax
	testb	$2, %al
	je	.LBB0_59
# BB#49:                                #   in Loop: Header=BB0_42 Depth=2
	andl	$-7, %eax
	movl	%eax, 4(%rsp)
	movq	(%rbx), %rax
	movq	8(%rsp), %rsi
.Ltmp23:
	movq	%rbx, %rdi
	movl	%r15d, %edx
	leaq	4(%rsp), %rcx
	callq	*104(%rax)
	movq	%rax, %rbp
.Ltmp24:
# BB#50:                                #   in Loop: Header=BB0_42 Depth=2
	testq	%rbp, %rbp
	je	.LBB0_54
# BB#51:                                #   in Loop: Header=BB0_42 Depth=2
	cmpq	%rbp, %rbx
	je	.LBB0_54
# BB#52:                                #   in Loop: Header=BB0_42 Depth=2
	movq	(%rbx), %rax
.Ltmp25:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp26:
# BB#53:                                #   in Loop: Header=BB0_42 Depth=2
	cmpq	%r13, %rbx
	movl	$0, %eax
	cmoveq	%rax, %r13
	movq	%rbp, %rbx
.LBB0_54:                               #   in Loop: Header=BB0_42 Depth=2
	testb	$2, 4(%rsp)
	je	.LBB0_59
# BB#55:                                #   in Loop: Header=BB0_42 Depth=2
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$10, %ebp
	je	.LBB0_57
	.p2align	4, 0x90
.LBB0_56:                               # %.preheader
                                        #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_42 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB0_56
.LBB0_57:                               # %.loopexit
                                        #   in Loop: Header=BB0_42 Depth=2
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#58:                                # %.loopexit
                                        #   in Loop: Header=BB0_42 Depth=2
	orl	$32, %ebp
	cmpl	$110, %ebp
	jne	.LBB0_42
	jmp	.LBB0_59
.LBB0_60:                               #   in Loop: Header=BB0_5 Depth=1
	cmpl	$0, step_thru(%rip)
	je	.LBB0_80
# BB#61:                                #   in Loop: Header=BB0_5 Depth=1
	orl	$1, %r15d
	movq	(%r13), %rax
	movq	8(%rsp), %rsi
.Ltmp36:
	movq	%r13, %rdi
	movl	%r15d, %edx
	leaq	4(%rsp), %rcx
	callq	*104(%rax)
	movq	%rax, %rbx
.Ltmp37:
	.p2align	4, 0x90
.LBB0_62:                               # %.preheader210
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#63:                                #   in Loop: Header=BB0_62 Depth=2
	movl	4(%rsp), %eax
	testb	$2, %al
	je	.LBB0_72
# BB#64:                                #   in Loop: Header=BB0_62 Depth=2
	andl	$-3, %eax
	movl	%eax, 4(%rsp)
	movq	(%rbx), %rax
	movq	8(%rsp), %rsi
.Ltmp38:
	movq	%rbx, %rdi
	movl	%r15d, %edx
	leaq	4(%rsp), %rcx
	callq	*104(%rax)
	movq	%rax, %rbp
.Ltmp39:
# BB#65:                                #   in Loop: Header=BB0_62 Depth=2
	testq	%rbp, %rbp
	je	.LBB0_69
# BB#66:                                #   in Loop: Header=BB0_62 Depth=2
	cmpq	%rbp, %rbx
	je	.LBB0_69
# BB#67:                                #   in Loop: Header=BB0_62 Depth=2
	movq	(%rbx), %rax
.Ltmp40:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp41:
# BB#68:                                #   in Loop: Header=BB0_62 Depth=2
	cmpq	%r13, %rbx
	movl	$0, %eax
	cmoveq	%rax, %r13
	movq	%rbp, %rbx
.LBB0_69:                               #   in Loop: Header=BB0_62 Depth=2
	testq	%rbx, %rbx
	je	.LBB0_71
# BB#70:                                #   in Loop: Header=BB0_62 Depth=2
	movl	4(%rsp), %eax
	andl	$2, %eax
	jne	.LBB0_62
	jmp	.LBB0_71
.LBB0_9:
	movq	(%r13), %rax
.Ltmp62:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp63:
# BB#10:
.Ltmp65:
	leaq	32(%rsp), %rdi
	callq	_ZN12token_stream5closeEv
.Ltmp66:
# BB#11:
.Ltmp70:
	leaq	16(%rsp), %rdi
	callq	_ZN24lambda_expression_parserD1Ev
.Ltmp71:
# BB#12:
	leaq	32(%rsp), %rdi
	callq	_ZN12token_streamD1Ev
	xorl	%eax, %eax
	addq	$8792, %rsp             # imm = 0x2258
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_92:
.Ltmp72:
	jmp	.LBB0_93
.LBB0_15:                               # %.loopexit.split-lp
.Ltmp67:
	jmp	.LBB0_91
.LBB0_16:                               # %.loopexit.split-lp215
.Ltmp64:
	jmp	.LBB0_91
.LBB0_13:
.Ltmp4:
.LBB0_93:
	movq	%rax, %rbx
	jmp	.LBB0_94
.LBB0_84:
.Ltmp58:
	jmp	.LBB0_91
.LBB0_76:
.Ltmp49:
	jmp	.LBB0_91
.LBB0_45:
.Ltmp19:
	jmp	.LBB0_91
.LBB0_74:
.Ltmp61:
	jmp	.LBB0_91
.LBB0_14:                               # %.loopexit213
.Ltmp7:
	jmp	.LBB0_91
.LBB0_97:                               # %.us-lcssa
.Ltmp22:
	jmp	.LBB0_91
.LBB0_40:                               # %.us-lcssa.us
.Ltmp30:
	jmp	.LBB0_91
.LBB0_75:
.Ltmp42:
	jmp	.LBB0_91
.LBB0_98:                               # %.us-lcssa242
.Ltmp27:
	jmp	.LBB0_91
.LBB0_41:                               # %.us-lcssa242.us
.Ltmp35:
	jmp	.LBB0_91
.LBB0_90:                               # %.loopexit214
.Ltmp16:
.LBB0_91:
	movq	%rax, %rbx
.Ltmp68:
	leaq	16(%rsp), %rdi
	callq	_ZN24lambda_expression_parserD1Ev
.Ltmp69:
.LBB0_94:
.Ltmp73:
	leaq	32(%rsp), %rdi
	callq	_ZN12token_streamD1Ev
.Ltmp74:
# BB#95:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_96:
.Ltmp75:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\234\003"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\223\003"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp61-.Lfunc_begin0   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp47-.Ltmp44         #   Call between .Ltmp44 and .Ltmp47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin0   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp56-.Ltmp53         #   Call between .Ltmp53 and .Ltmp56
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 13 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 14 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp15-.Ltmp12         #   Call between .Ltmp12 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp31-.Ltmp29         #   Call between .Ltmp29 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp34-.Ltmp31         #   Call between .Ltmp31 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin0   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp23-.Ltmp21         #   Call between .Ltmp21 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp26-.Ltmp23         #   Call between .Ltmp23 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp61-.Lfunc_begin0   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp41-.Ltmp38         #   Call between .Ltmp38 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin0   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin0   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin0   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin0   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin0   # >> Call Site 29 <<
	.long	.Ltmp68-.Ltmp71         #   Call between .Ltmp71 and .Ltmp68
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin0   # >> Call Site 30 <<
	.long	.Ltmp74-.Ltmp68         #   Call between .Ltmp68 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin0   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp74-.Lfunc_begin0   # >> Call Site 31 <<
	.long	.Lfunc_end0-.Ltmp74     #   Call between .Ltmp74 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"<< "
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"==> "
	.size	.L.str.3, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"="
	.size	.L.str.5, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"H"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"B"
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"=%1.1s==> "
	.size	.L.str.8, 11

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"*** Continue?[y/n]:"
	.size	.L.str.9, 20

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"====>"
	.size	.L.str.11, 6

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Copyright (c) 2000 John A. Maiorana. All rights reserved."
	.size	.Lstr, 58


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
