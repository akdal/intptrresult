	.text
	.file	"smg_setup.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	hypre_SMGSetup
	.p2align	4, 0x90
	.type	hypre_SMGSetup,@function
hypre_SMGSetup:                         # @hypre_SMGSetup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi6:
	.cfi_def_cfa_offset 352
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	%rdi, %r13
	movl	(%r13), %eax
	movl	%eax, 112(%rsp)         # 4-byte Spill
	movl	36(%r13), %eax
	movl	%eax, 152(%rsp)         # 4-byte Spill
	movl	40(%r13), %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 272(%rsp)
	movq	$0, 288(%rsp)
	movaps	%xmm0, 240(%rsp)
	movq	$0, 256(%rsp)
	movq	24(%rsi), %rax
	movslq	16(%rax), %r14
	leaq	-1(%r14), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movl	%eax, 44(%r13)
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	8(%rsi), %r15
	movq	40(%r15), %rdi
	callq	hypre_BoxDuplicate
	movq	%rax, %rbx
	movl	8(%rbx,%r14,4), %eax
	movl	-4(%rbx,%r14,4), %ecx
	movl	%eax, %edi
	subl	%ecx, %edi
	incl	%edi
	xorl	%ebp, %ebp
	cmpl	%ecx, %eax
	movq	%r14, 200(%rsp)         # 8-byte Spill
	movl	%r14d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cmovsl	%ebp, %edi
	movq	%rbx, %rax
	addq	$12, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	callq	hypre_Log2
	addl	$2, %eax
	movl	28(%r13), %ecx
	cmpl	%ecx, %eax
	movl	%ecx, %r14d
	cmovlel	%eax, %r14d
	testl	%ecx, %ecx
	cmovlel	%eax, %r14d
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movl	%r14d, 28(%r13)
	leal	(,%r14,8), %r12d
	movl	%r12d, %edi
	callq	hypre_MAlloc
	movq	%rax, %r13
	movl	%r12d, %edi
	callq	hypre_MAlloc
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	$0, (%rax)
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	hypre_StructGridRef
	decl	%r14d
	movq	%r14, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
	testq	%r15, %r15
	jg	.LBB0_2
	.p2align	4, 0x90
.LBB0_3:
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	48(%rcx), %eax
	movl	%eax, 52(%rsp)
	movl	52(%rcx), %eax
	movl	%eax, 56(%rsp)
	movl	56(%rcx), %eax
	movl	%eax, 60(%rsp)
	movl	60(%rcx), %eax
	movl	%eax, 40(%rsp)
	movl	64(%rcx), %eax
	movl	%eax, 44(%rsp)
	movl	68(%rcx), %eax
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_1:
	movq	%rbx, %rdi
	leaq	52(%rsp), %r12
	movq	%r12, %rsi
	leaq	40(%rsp), %r14
	movq	%r14, %rdx
	callq	hypre_ProjectBox
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	hypre_StructMapFineToCoarse
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%rdi, %rcx
	callq	hypre_StructMapFineToCoarse
	movq	(%r13,%r15,8), %rdi
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax,%rbp), %r8
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	hypre_StructCoarsen
	movq	(%r13,%r15,8), %rdi
	incq	%r15
	leaq	8(%r13,%rbp), %r8
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	hypre_StructCoarsen
	addq	$8, %rbp
	testq	%r15, %r15
	jle	.LBB0_3
.LBB0_2:
	movl	$0, 52(%rsp)
	movl	$0, 56(%rsp)
	movl	$0, 60(%rsp)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 40(%rsp)
	movl	$1, %eax
.LBB0_4:
	movl	%eax, 48(%rsp)
	movq	200(%rsp), %rcx         # 8-byte Reload
	shll	36(%rsp,%rcx,4)
	cmpq	%r15, (%rsp)            # 8-byte Folded Reload
	je	.LBB0_6
# BB#5:
	movl	8(%rbx,%rcx,4), %eax
	cmpl	%eax, -4(%rbx,%rcx,4)
	jne	.LBB0_1
.LBB0_6:
	movq	%r15, 136(%rsp)         # 8-byte Spill
	leal	1(%r15), %r14d
	movq	%rbx, %rdi
	callq	hypre_BoxDestroy
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%r14d, 32(%rax)
	movq	%rax, %rcx
	movq	%r13, 72(%rcx)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, 80(%rcx)
	leaq	8(%rbp), %r15
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, %r14
	movl	%ebp, %edi
	callq	hypre_MAlloc
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	hypre_MAlloc
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbp
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbx
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, %r12
	movq	%r15, 184(%rsp)         # 8-byte Spill
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	hypre_StructMatrixRef
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	%rax, (%r14)
	movq	128(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdi
	callq	hypre_StructVectorRef
	movq	%rbp, 176(%rsp)         # 8-byte Spill
	movq	%rax, (%rbp)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	callq	hypre_StructVectorRef
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	%rax, (%rbx)
	cmpl	$0, 200(%rsp)           # 4-byte Folded Reload
	movl	112(%rsp), %r15d        # 4-byte Reload
	movabsq	$4294967297, %rdi       # imm = 0x100000001
	movq	32(%rsp), %rbx          # 8-byte Reload
	jle	.LBB0_16
# BB#7:                                 # %.lr.ph584.preheader
	cmpl	$4, %ebx
	jb	.LBB0_13
# BB#9:                                 # %min.iters.checked
	movl	%ebx, %ecx
	andl	$3, %ecx
	movq	%rbx, %rax
	subq	%rcx, %rax
	je	.LBB0_13
# BB#10:                                # %vector.body.preheader
	leaq	240(%rsp), %rdx
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB0_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB0_11
# BB#12:                                # %middle.block
	testl	%ecx, %ecx
	jne	.LBB0_14
	jmp	.LBB0_16
.LBB0_13:
	xorl	%eax, %eax
.LBB0_14:                               # %.lr.ph584.preheader619
	leaq	244(%rsp,%rax,8), %rcx
	subq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph584
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdi, -4(%rcx)
	addq	$8, %rcx
	decq	%rbx
	jne	.LBB0_15
.LBB0_16:                               # %._crit_edge585
	movq	(%r13), %rsi
	movl	%r15d, %edi
	callq	hypre_StructVectorCreate
	movq	%rax, (%r12)
	addq	$48, %r14
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	hypre_StructVectorSetNumGhost
	movq	(%r12), %rdi
	callq	hypre_StructVectorInitializeShell
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	(%r12), %rax
	movl	36(%rax), %r12d
	movq	(%r13), %rsi
	movl	%r15d, %edi
	callq	hypre_StructVectorCreate
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rax, (%rbx)
	addq	$48, %rbp
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	hypre_StructVectorSetNumGhost
	movq	(%rbx), %rdi
	callq	hypre_StructVectorInitializeShell
	movq	(%rbx), %rax
	addl	36(%rax), %r12d
	movq	136(%rsp), %rax         # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB0_19
# BB#17:                                # %.lr.ph579
	movl	%eax, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r14, 128(%rsp)         # 8-byte Spill
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB0_18:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	leaq	1(%rbx), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	8(%rax,%rbx,8), %rsi
	movq	216(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	hypre_SMGCreateInterpOp
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	%rax, (%rbp,%rbx,8)
	movq	%rax, %rdi
	callq	hypre_StructMatrixInitializeShell
	movq	(%rbp,%rbx,8), %rdi
	addl	60(%rdi), %r12d
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	%rdi, (%rax,%rbx,8)
	movq	(%r14,%rbx,8), %rsi
	movq	(%rbp,%rbx,8), %rdx
	movq	8(%r13,%rbx,8), %rcx
	callq	hypre_SMGCreateRAPOp
	movq	%rax, 8(%r14,%rbx,8)
	movq	%rax, %rdi
	callq	hypre_StructMatrixInitializeShell
	movq	8(%r14,%rbx,8), %rax
	addl	60(%rax), %r12d
	movq	8(%r13,%rbx,8), %rsi
	movl	%r15d, %edi
	callq	hypre_StructVectorCreate
	movq	176(%rsp), %rbp         # 8-byte Reload
	movq	%rax, 8(%rbp,%rbx,8)
	movq	%rax, %rdi
	leaq	272(%rsp), %rsi
	callq	hypre_StructVectorSetNumGhost
	movq	8(%rbp,%rbx,8), %rdi
	callq	hypre_StructVectorInitializeShell
	movq	8(%rbp,%rbx,8), %rax
	addl	36(%rax), %r12d
	movq	8(%r13,%rbx,8), %rsi
	movl	%r15d, %edi
	callq	hypre_StructVectorCreate
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	%rax, 8(%rbp,%rbx,8)
	movq	%rax, %rdi
	leaq	240(%rsp), %rsi
	callq	hypre_StructVectorSetNumGhost
	movq	8(%rbp,%rbx,8), %rdi
	callq	hypre_StructVectorInitializeShell
	movq	8(%rbp,%rbx,8), %rax
	addl	36(%rax), %r12d
	movq	8(%r13,%rbx,8), %rsi
	movl	%r15d, %edi
	callq	hypre_StructVectorCreate
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rax, 8(%rbp,%rbx,8)
	movq	%rax, %rdi
	movq	128(%rsp), %rsi         # 8-byte Reload
	callq	hypre_StructVectorSetNumGhost
	movq	8(%rbp,%rbx,8), %rdi
	callq	hypre_StructVectorInitializeShell
	movq	8(%r13,%rbx,8), %rsi
	movl	%r15d, %edi
	callq	hypre_StructVectorCreate
	movl	%r15d, %ebp
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%rax, 8(%r15,%rbx,8)
	movq	%rax, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	hypre_StructVectorSetNumGhost
	movq	8(%r15,%rbx,8), %rdi
	movl	%ebp, %r15d
	callq	hypre_StructVectorInitializeShell
	movq	88(%rsp), %rbx          # 8-byte Reload
	cmpq	%rbx, 168(%rsp)         # 8-byte Folded Reload
	jne	.LBB0_18
.LBB0_19:                               # %._crit_edge580
	movl	$8, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbx, 88(%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rdi
	movq	%rbx, %rsi
	callq	hypre_StructVectorInitializeData
	movq	(%rbp), %rdi
	callq	hypre_StructVectorAssemble
	movq	(%rbp), %rax
	movslq	36(%rax), %rax
	leaq	(%rbx,%rax,8), %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rdi
	movq	%rbx, %rsi
	callq	hypre_StructVectorInitializeData
	movq	(%rbp), %rdi
	callq	hypre_StructVectorAssemble
	cmpl	$0, 136(%rsp)           # 4-byte Folded Reload
	jle	.LBB0_22
# BB#20:                                # %.lr.ph574.preheader
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %rax
	movslq	36(%rax), %rax
	leaq	(%rbx,%rax,8), %rbx
	movl	136(%rsp), %eax         # 4-byte Reload
	movq	(%rsp), %r14            # 8-byte Reload
	addq	$8, %r14
	movq	176(%rsp), %r13         # 8-byte Reload
	addq	$8, %r13
	movq	96(%rsp), %rbp          # 8-byte Reload
	addq	$8, %rbp
	movq	32(%rsp), %rdx          # 8-byte Reload
	addq	$8, %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	addq	$8, %r12
	movq	144(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_21:                               # %.lr.ph574
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	(%r15), %rdi
	movq	%rbx, %rsi
	callq	hypre_StructMatrixInitializeData
	movq	(%r15), %rax
	movslq	60(%rax), %rax
	leaq	(%rbx,%rax,8), %rbx
	movq	(%r14), %rdi
	movq	%rbx, %rsi
	callq	hypre_StructMatrixInitializeData
	movq	(%r14), %rax
	movslq	60(%rax), %rax
	leaq	(%rbx,%rax,8), %rbx
	movq	(%r13), %rdi
	movq	%rbx, %rsi
	callq	hypre_StructVectorInitializeData
	movq	(%r13), %rdi
	callq	hypre_StructVectorAssemble
	movq	(%r13), %rax
	movslq	36(%rax), %rax
	leaq	(%rbx,%rax,8), %rbx
	movq	(%rbp), %rdi
	movq	%rbx, %rsi
	callq	hypre_StructVectorInitializeData
	movq	(%rbp), %rdi
	callq	hypre_StructVectorAssemble
	movq	(%rbp), %rax
	movslq	36(%rax), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	24(%rax), %rsi
	callq	hypre_StructVectorInitializeData
	movq	(%rbx), %rdi
	callq	hypre_StructVectorAssemble
	movq	(%r12), %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	24(%rax), %rsi
	callq	hypre_StructVectorInitializeData
	movq	(%r12), %rdi
	callq	hypre_StructVectorAssemble
	movq	88(%rsp), %rax          # 8-byte Reload
	addq	$8, %r15
	addq	$8, %r14
	addq	$8, %r13
	addq	$8, %rbp
	addq	$8, %rbx
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rbx           # 8-byte Reload
	addq	$8, %r12
	decq	%rax
	jne	.LBB0_21
.LBB0_22:                               # %._crit_edge575
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 96(%rcx)
	movq	144(%rsp), %r13         # 8-byte Reload
	movq	%r13, 104(%rcx)
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	%rax, 112(%rcx)
	movq	176(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, 120(%rcx)
	movq	96(%rsp), %r12          # 8-byte Reload
	movq	%r12, 128(%rcx)
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, 136(%rcx)
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r15, 144(%rcx)
	movq	%r15, 152(%rcx)
	movq	%r15, 160(%rcx)
	movq	184(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %edi
	callq	hypre_MAlloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ebx, %edi
	callq	hypre_MAlloc
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%ebx, %edi
	callq	hypre_MAlloc
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	hypre_MAlloc
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	(%rbp), %rdi
	movq	24(%rdi), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movl	32(%rdi), %eax
	movl	%eax, 208(%rsp)         # 4-byte Spill
	movq	(%r12), %rax
	movq	24(%rax), %rcx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movl	32(%rax), %ebx
	movq	(%r14), %rax
	movq	24(%rax), %rsi
	callq	hypre_StructVectorInitializeData
	movq	(%r12), %rdi
	movq	(%r15), %rax
	movq	24(%rax), %rsi
	callq	hypre_StructVectorInitializeData
	movq	(%rbp), %rdi
	callq	hypre_StructVectorAssemble
	movq	(%r12), %rdi
	callq	hypre_StructVectorAssemble
	movq	136(%rsp), %rax         # 8-byte Reload
	testl	%eax, %eax
	movl	%ebx, 212(%rsp)         # 4-byte Spill
	jle	.LBB0_29
# BB#23:                                # %.lr.ph
	movl	%eax, %eax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	176(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_24:                               # =>This Inner Loop Header: Depth=1
	testb	$1, %al
	je	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_24 Depth=1
	movl	$0, 76(%rsp)
	movl	$0, 80(%rsp)
	movl	$0, 84(%rsp)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 116(%rsp)
	xorl	%eax, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movl	$1, %edi
	jmp	.LBB0_27
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_24 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	48(%rcx), %eax
	movl	%eax, 76(%rsp)
	movl	52(%rcx), %edx
	movl	%edx, 80(%rsp)
	movl	56(%rcx), %esi
	movl	%esi, 84(%rsp)
	movl	60(%rcx), %r8d
	movl	%r8d, 116(%rsp)
	movl	64(%rcx), %edi
	movl	%edi, 120(%rsp)
	movl	68(%rcx), %ecx
.LBB0_27:                               #   in Loop: Header=BB0_24 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%r13, %r14
	movl	%ecx, 124(%rsp)
	movl	%eax, 52(%rsp)
	movl	%edx, 56(%rsp)
	movl	%esi, 60(%rsp)
	movl	%eax, 156(%rsp)
	movl	%edx, 160(%rsp)
	movl	%esi, 164(%rsp)
	movq	200(%rsp), %r15         # 8-byte Reload
	incl	152(%rsp,%r15,4)
	movl	%r8d, 40(%rsp)
	movl	%edi, 44(%rsp)
	movl	%ecx, 48(%rsp)
	shll	36(%rsp,%r15,4)
	movl	112(%rsp), %edi         # 4-byte Reload
	callq	hypre_SMGRelaxCreate
	movq	%rax, (%rbp,%rbx,8)
	movq	%rax, %rdi
	leaq	76(%rsp), %rax
	movq	%rax, %rsi
	leaq	116(%rsp), %rax
	movq	%rax, %rdx
	callq	hypre_SMGRelaxSetBase
	movq	(%rbp,%rbx,8), %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	callq	hypre_SMGRelaxSetMemoryUse
	movq	(%rbp,%rbx,8), %rdi
	xorps	%xmm0, %xmm0
	callq	hypre_SMGRelaxSetTol
	movq	(%rbp,%rbx,8), %rdi
	movl	$2, %esi
	callq	hypre_SMGRelaxSetNumSpaces
	movq	(%rbp,%rbx,8), %rdi
	movl	48(%rsp,%r15,4), %edx
	movl	36(%rsp,%r15,4), %ecx
	xorl	%esi, %esi
	callq	hypre_SMGRelaxSetSpace
	movq	(%rbp,%rbx,8), %rdi
	movl	152(%rsp,%r15,4), %edx
	movl	36(%rsp,%r15,4), %ecx
	movl	$1, %esi
	callq	hypre_SMGRelaxSetSpace
	movq	(%rbp,%rbx,8), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	callq	hypre_SMGRelaxSetTempVec
	movq	(%rbp,%rbx,8), %rdi
	movl	152(%rsp), %esi         # 4-byte Reload
	callq	hypre_SMGRelaxSetNumPreRelax
	movq	(%rbp,%rbx,8), %rdi
	movl	108(%rsp), %esi         # 4-byte Reload
	callq	hypre_SMGRelaxSetNumPostRelax
	movq	(%rbp,%rbx,8), %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	movq	(%r12,%rbx,8), %rdx
	movq	96(%rsp), %r13          # 8-byte Reload
	movq	(%r13,%rbx,8), %rcx
	callq	hypre_SMGRelaxSetup
	movq	(%rbp,%rbx,8), %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	movq	(%r12,%rbx,8), %rdx
	movq	(%r13,%rbx,8), %rcx
	movq	(%r14,%rbx,8), %r8
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movq	224(%rsp), %r9          # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	leaq	48(%rsp), %rax
	pushq	%rax
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	leaq	172(%rsp), %rax
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	leaq	76(%rsp), %r14
	pushq	%r14
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	hypre_SMGSetupInterpOp
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	movq	(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	callq	hypre_SMGRelaxSetNumPreSpaces
	movq	(%rbp,%rbx,8), %rdi
	movl	$2, %esi
	callq	hypre_SMGRelaxSetNumRegSpaces
	movq	(%rbp,%rbx,8), %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	movq	(%r12,%rbx,8), %rdx
	movq	(%r13,%rbx,8), %rcx
	callq	hypre_SMGRelaxSetup
	callq	hypre_SMGResidualCreate
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	%rax, (%rbp,%rbx,8)
	movq	%rax, %rdi
	leaq	76(%rsp), %rsi
	leaq	116(%rsp), %rdx
	callq	hypre_SMGResidualSetBase
	movq	(%rbp,%rbx,8), %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	movq	(%r13,%rbx,8), %rdx
	movq	(%r12,%rbx,8), %rcx
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	(%r14,%rbx,8), %r8
	callq	hypre_SMGResidualSetup
	callq	hypre_SemiInterpCreate
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	%rax, (%rcx,%rbx,8)
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rbx,8), %rsi
	leaq	1(%rbx), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	8(%r13,%rbx,8), %rcx
	movq	(%rsp), %r15            # 8-byte Reload
	movq	(%r14,%rbx,8), %r8
	movl	$1, %edx
	movq	%rax, %rdi
	leaq	52(%rsp), %rbp
	movq	%rbp, %r9
	leaq	40(%rsp), %rax
	pushq	%rax
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	leaq	164(%rsp), %rax
	pushq	%rax
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	hypre_SemiInterpSetup
	addq	$16, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -16
	callq	hypre_SemiRestrictCreate
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	%rax, (%rcx,%rbx,8)
	movq	192(%rsp), %r13         # 8-byte Reload
	movq	(%r13,%rbx,8), %rsi
	movq	(%r14,%rbx,8), %rcx
	movq	144(%rsp), %r14         # 8-byte Reload
	movq	8(%r12,%rbx,8), %r8
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rbp, %r9
	leaq	40(%rsp), %rbp
	pushq	%rbp
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	leaq	164(%rsp), %rax
	pushq	%rax
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	callq	hypre_SemiRestrictSetup
	addq	$16, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset -16
	movq	(%r13,%rbx,8), %rdi
	movq	(%r15,%rbx,8), %rsi
	movq	%r14, %r13
	movq	(%r14,%rbx,8), %rdx
	movq	8(%r15,%rbx,8), %rcx
	leaq	52(%rsp), %r8
	movq	%rbp, %r9
	callq	hypre_SMGSetupRAPOp
	movb	$1, %al
	movq	64(%rsp), %rbx          # 8-byte Reload
	cmpq	%rbx, 184(%rsp)         # 8-byte Folded Reload
	jne	.LBB0_24
# BB#28:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %edi
	movl	$1, %eax
	movq	136(%rsp), %rbx         # 8-byte Reload
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	(%rsp), %r13            # 8-byte Reload
	movq	%r12, %r14
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	108(%rsp), %r15d        # 4-byte Reload
	jmp	.LBB0_30
.LBB0_29:                               # %._crit_edge
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	48(%rax), %ecx
	movl	52(%rax), %edx
	movl	56(%rax), %esi
	movl	60(%rax), %r8d
	movl	64(%rax), %edi
	movl	68(%rax), %eax
	xorl	%ebx, %ebx
	movq	%rbp, %r14
	movl	108(%rsp), %r15d        # 4-byte Reload
	movq	(%rsp), %r13            # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_30:
	movl	%ecx, 76(%rsp)
	movl	%edx, 80(%rsp)
	movl	%esi, 84(%rsp)
	movl	%r8d, 116(%rsp)
	movl	%edi, 120(%rsp)
	movl	%eax, 124(%rsp)
	movl	112(%rsp), %edi         # 4-byte Reload
	callq	hypre_SMGRelaxCreate
	movslq	%ebx, %rbx
	movq	%rax, (%rbp,%rbx,8)
	leaq	76(%rsp), %rsi
	leaq	116(%rsp), %rdx
	movq	%rax, %rdi
	callq	hypre_SMGRelaxSetBase
	movq	(%rbp,%rbx,8), %rdi
	xorps	%xmm0, %xmm0
	callq	hypre_SMGRelaxSetTol
	movq	(%rbp,%rbx,8), %rdi
	movl	$1, %esi
	callq	hypre_SMGRelaxSetMaxIter
	movq	(%rbp,%rbx,8), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	callq	hypre_SMGRelaxSetTempVec
	movq	(%rbp,%rbx,8), %rdi
	movl	152(%rsp), %esi         # 4-byte Reload
	callq	hypre_SMGRelaxSetNumPreRelax
	movq	(%rbp,%rbx,8), %rdi
	movl	%r15d, %esi
	callq	hypre_SMGRelaxSetNumPostRelax
	movq	(%rbp,%rbx,8), %rdi
	movq	(%r13,%rbx,8), %rsi
	movq	(%r14,%rbx,8), %rdx
	movq	(%r12,%rbx,8), %rcx
	callq	hypre_SMGRelaxSetup
	testl	%ebx, %ebx
	jne	.LBB0_32
# BB#31:
	callq	hypre_SMGResidualCreate
	movq	%r12, %r15
	movq	%r14, %r12
	movq	%r13, %rbp
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	%rax, (%r14,%rbx,8)
	leaq	76(%rsp), %rsi
	leaq	116(%rsp), %rdx
	movq	%rax, %rdi
	callq	hypre_SMGResidualSetBase
	movq	(%r14,%rbx,8), %rdi
	movq	(%rbp,%rbx,8), %rsi
	movq	%r12, %r14
	movq	%r15, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%r12,%rbx,8), %rdx
	movq	(%r14,%rbx,8), %rcx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %r8
	callq	hypre_SMGResidualSetup
.LBB0_32:
	movq	(%r14), %rdi
	movq	224(%rsp), %rsi         # 8-byte Reload
	callq	hypre_StructVectorInitializeData
	movq	(%r14), %rax
	movl	208(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, 32(%rax)
	movq	(%r12), %rdi
	movq	232(%rsp), %rsi         # 8-byte Reload
	callq	hypre_StructVectorInitializeData
	movq	(%r12), %rax
	movl	212(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, 32(%rax)
	movq	(%r14), %rdi
	callq	hypre_StructVectorAssemble
	movq	(%r12), %rdi
	callq	hypre_StructVectorAssemble
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, 168(%rcx)
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 176(%rcx)
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%rax, 184(%rcx)
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	%rax, 192(%rcx)
	cmpl	$0, 208(%rcx)
	jle	.LBB0_34
# BB#33:
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	16(%rbp), %ebx
	shll	$3, %ebx
	movl	%ebx, %edi
	callq	hypre_MAlloc
	movq	%rax, 216(%rbp)
	movl	%ebx, %edi
	callq	hypre_MAlloc
	movq	%rax, 224(%rbp)
.LBB0_34:
	xorl	%eax, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_SMGSetup, .Lfunc_end0-hypre_SMGSetup
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
