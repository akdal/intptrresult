	.text
	.file	"LzxDecoder.bc"
	.globl	_ZN9NCompress4NLzx8CDecoderC2Eb
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoderC2Eb,@function
_ZN9NCompress4NLzx8CDecoderC2Eb:        # @_ZN9NCompress4NLzx8CDecoderC2Eb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movl	%esi, %ebx
	movq	%rdi, %r13
	movl	$0, 8(%r13)
	movq	$_ZTVN9NCompress4NLzx8CDecoderE+16, (%r13)
	leaq	16(%r13), %r14
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZN9CInBufferC1Ev
.Ltmp1:
# BB#1:
	leaq	72(%r13), %r12
	movq	$0, 72(%r13)
	movl	$0, 80(%r13)
	movq	$0, 96(%r13)
	movq	$0, 112(%r13)
	movq	$0, 7392(%r13)
	movb	$0, 7404(%r13)
	movb	$0, 7412(%r13)
	movb	%bl, 7413(%r13)
.Ltmp3:
	movl	$32808, %edi            # imm = 0x8028
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp4:
# BB#2:
	movl	$0, 8(%rbx)
	movq	$_ZTVN9NCompress4NLzx20Cx86ConvertOutStreamE+16, (%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, 7384(%r13)
.Ltmp5:
	movq	%rbx, %rdi
	callq	*_ZTVN9NCompress4NLzx20Cx86ConvertOutStreamE+24(%rip)
.Ltmp6:
# BB#3:                                 # %.noexc7
	movq	7392(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp7:
	callq	*16(%rax)
.Ltmp8:
.LBB0_5:
	movq	%rbx, 7392(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_20:
.Ltmp2:
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_6:
.Ltmp9:
	movq	%rax, %r15
	movq	7392(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB0_8
# BB#7:
	movq	(%rdi), %rax
.Ltmp10:
	callq	*16(%rax)
.Ltmp11:
.LBB0_8:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
.Ltmp12:
	movq	%r12, %rdi
	callq	_ZN10COutBuffer4FreeEv
.Ltmp13:
# BB#9:
	movq	96(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB0_11
# BB#10:
	movq	(%rdi), %rax
.Ltmp18:
	callq	*16(%rax)
.Ltmp19:
.LBB0_11:                               # %_ZN10COutBufferD2Ev.exit
.Ltmp20:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp21:
# BB#12:
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB0_21
# BB#13:
	movq	(%rdi), %rax
.Ltmp26:
	callq	*16(%rax)
.Ltmp27:
.LBB0_21:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoderD2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_17:
.Ltmp22:
	movq	%rax, %r14
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB0_23
# BB#18:
	movq	(%rdi), %rax
.Ltmp23:
	callq	*16(%rax)
.Ltmp24:
	jmp	.LBB0_23
.LBB0_19:
.Ltmp25:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_14:
.Ltmp14:
	movq	%rax, %r14
	movq	96(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB0_23
# BB#15:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
	jmp	.LBB0_23
.LBB0_16:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_22:
.Ltmp28:
	movq	%rax, %r14
.LBB0_23:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN9NCompress4NLzx8CDecoderC2Eb, .Lfunc_end0-_ZN9NCompress4NLzx8CDecoderC2Eb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp8-.Ltmp3           #   Call between .Ltmp3 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp8          #   Call between .Ltmp8 and .Ltmp10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	1                       #   On action: 1
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp23-.Ltmp27         #   Call between .Ltmp27 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN9NCompress4NLzx8CDecoder14ReleaseStreamsEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder14ReleaseStreamsEv,@function
_ZN9NCompress4NLzx8CDecoder14ReleaseStreamsEv: # @_ZN9NCompress4NLzx8CDecoder14ReleaseStreamsEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 96(%rbx)
.LBB2_2:                                # %_ZN10COutBuffer13ReleaseStreamEv.exit
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 40(%rbx)
.LBB2_4:                                # %_ZN9NCompress4NLzx10NBitStream8CDecoder13ReleaseStreamEv.exit
	movq	7384(%rbx), %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_6
# BB#5:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%rbx)
.LBB2_6:                                # %_ZN9NCompress4NLzx20Cx86ConvertOutStream13ReleaseStreamEv.exit
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN9NCompress4NLzx8CDecoder14ReleaseStreamsEv, .Lfunc_end2-_ZN9NCompress4NLzx8CDecoder14ReleaseStreamsEv
	.cfi_endproc

	.globl	_ZN9NCompress4NLzx8CDecoder5FlushEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder5FlushEv,@function
_ZN9NCompress4NLzx8CDecoder5FlushEv:    # @_ZN9NCompress4NLzx8CDecoder5FlushEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	72(%rbx), %rdi
	callq	_ZN10COutBuffer5FlushEv
	testl	%eax, %eax
	je	.LBB3_2
# BB#1:
	popq	%rbx
	retq
.LBB3_2:
	movq	7384(%rbx), %rdi
	popq	%rbx
	jmp	_ZN9NCompress4NLzx20Cx86ConvertOutStream5FlushEv # TAILCALL
.Lfunc_end3:
	.size	_ZN9NCompress4NLzx8CDecoder5FlushEv, .Lfunc_end3-_ZN9NCompress4NLzx8CDecoder5FlushEv
	.cfi_endproc

	.globl	_ZN9NCompress4NLzx8CDecoder8ReadBitsEj
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder8ReadBitsEj,@function
_ZN9NCompress4NLzx8CDecoder8ReadBitsEj: # @_ZN9NCompress4NLzx8CDecoder8ReadBitsEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	64(%rbx), %r14d
	movl	68(%rbx), %eax
	movl	$15, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r14d
	andl	$131071, %r14d          # imm = 0x1FFFF
	movl	$17, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r14d
	addl	%esi, %eax
	movl	%eax, 68(%rbx)
	cmpl	$16, %eax
	jb	.LBB4_9
# BB#1:                                 # %.lr.ph.i.i.i
	leaq	16(%rbx), %rbp
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB4_3
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, (%rbp)
	movzbl	(%rdx), %r15d
	cmpq	%rcx, %rax
	jb	.LBB4_7
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_3:                                #   in Loop: Header=BB4_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %r15d
	movq	16(%rbx), %rax
	movq	24(%rbx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB4_6
.LBB4_7:                                #   in Loop: Header=BB4_2 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB4_8
	.p2align	4, 0x90
.LBB4_6:                                #   in Loop: Header=BB4_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB4_8:                                # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	64(%rbx), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%r15b, %ecx
	orl	%eax, %ecx
	movl	%ecx, 64(%rbx)
	movl	68(%rbx), %eax
	addl	$-16, %eax
	movl	%eax, 68(%rbx)
	cmpl	$15, %eax
	ja	.LBB4_2
.LBB4_9:                                # %_ZN9NCompress4NLzx10NBitStream8CDecoder8ReadBitsEj.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN9NCompress4NLzx8CDecoder8ReadBitsEj, .Lfunc_end4-_ZN9NCompress4NLzx8CDecoder8ReadBitsEj
	.cfi_endproc

	.globl	_ZN9NCompress4NLzx8CDecoder9ReadTableEPhS2_j
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder9ReadTableEPhS2_j,@function
_ZN9NCompress4NLzx8CDecoder9ReadTableEPhS2_j: # @_ZN9NCompress4NLzx8CDecoder9ReadTableEPhS2_j
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 112
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r13
	leaq	16(%r13), %r12
	movl	64(%r13), %eax
	movl	68(%r13), %edx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_2 Depth 2
	movl	$15, %ecx
	subl	%edx, %ecx
	movl	%eax, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	shrl	$13, %ebp
	addl	$4, %edx
	movl	%edx, 68(%r13)
	cmpl	$16, %edx
	jb	.LBB5_9
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rdx
	movq	24(%r13), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB5_3
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=2
	leaq	1(%rdx), %rax
	movq	%rax, (%r12)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB5_7
	jmp	.LBB5_6
	.p2align	4, 0x90
.LBB5_3:                                #   in Loop: Header=BB5_2 Depth=2
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%r13), %rax
	movq	24(%r13), %rcx
	cmpq	%rcx, %rax
	jae	.LBB5_6
.LBB5_7:                                #   in Loop: Header=BB5_2 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_2 Depth=2
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB5_8:                                # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i
                                        #   in Loop: Header=BB5_2 Depth=2
	movl	64(%r13), %ecx
	shll	$8, %ecx
	movzbl	%al, %edx
	orl	%ecx, %edx
	shll	$8, %edx
	movzbl	%bl, %eax
	orl	%edx, %eax
	movl	%eax, 64(%r13)
	movl	68(%r13), %edx
	addl	$-16, %edx
	movl	%edx, 68(%r13)
	cmpl	$15, %edx
	ja	.LBB5_2
.LBB5_9:                                # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit
                                        #   in Loop: Header=BB5_1 Depth=1
	andb	$15, %bpl
	movb	%bpl, 32(%rsp,%r14)
	incq	%r14
	cmpq	$20, %r14
	jne	.LBB5_1
# BB#10:
	leaq	5744(%r13), %rdi
	leaq	32(%rsp), %rsi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE14SetCodeLengthsEPKh
	testb	%al, %al
	je	.LBB5_52
# BB#11:                                # %.thread.preheader
	movb	$1, %al
	testl	%r15d, %r15d
	je	.LBB5_53
# BB#12:                                # %.lr.ph.lr.ph
	movl	%r15d, %ebp
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_42 Depth 2
                                        #     Child Loop BB5_19 Depth 2
                                        #     Child Loop BB5_29 Depth 2
	testl	%r15d, %r15d
	jne	.LBB5_14
# BB#16:                                # %.lr.ph91
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	callq	_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_
	cmpl	$18, %eax
	je	.LBB5_28
# BB#17:                                # %.lr.ph91
                                        #   in Loop: Header=BB5_13 Depth=1
	cmpl	$17, %eax
	jne	.LBB5_37
# BB#18:                                #   in Loop: Header=BB5_13 Depth=1
	movl	64(%r13), %r15d
	movl	68(%r13), %eax
	movl	$15, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r15d
	shrl	$13, %r15d
	andl	$15, %r15d
	addl	$4, %eax
	movl	%eax, 68(%r13)
	cmpl	$16, %eax
	jb	.LBB5_26
	.p2align	4, 0x90
.LBB5_19:                               # %.lr.ph.i.i.i.i41
                                        #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rdx
	movq	24(%r13), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB5_20
# BB#21:                                #   in Loop: Header=BB5_19 Depth=2
	leaq	1(%rdx), %rax
	movq	%rax, (%r12)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB5_24
	jmp	.LBB5_23
	.p2align	4, 0x90
.LBB5_20:                               #   in Loop: Header=BB5_19 Depth=2
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%r13), %rax
	movq	24(%r13), %rcx
	cmpq	%rcx, %rax
	jae	.LBB5_23
.LBB5_24:                               #   in Loop: Header=BB5_19 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB5_25
	.p2align	4, 0x90
.LBB5_23:                               #   in Loop: Header=BB5_19 Depth=2
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB5_25:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i47
                                        #   in Loop: Header=BB5_19 Depth=2
	movl	64(%r13), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bl, %ecx
	orl	%eax, %ecx
	movl	%ecx, 64(%r13)
	movl	68(%r13), %eax
	addl	$-16, %eax
	movl	%eax, 68(%r13)
	cmpl	$15, %eax
	ja	.LBB5_19
.LBB5_26:                               # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit48
                                        #   in Loop: Header=BB5_13 Depth=1
	addl	$4, %r15d
	jmp	.LBB5_27
.LBB5_28:                               #   in Loop: Header=BB5_13 Depth=1
	movl	64(%r13), %r15d
	movl	68(%r13), %eax
	movl	$15, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r15d
	shrl	$12, %r15d
	andl	$31, %r15d
	addl	$5, %eax
	movl	%eax, 68(%r13)
	cmpl	$16, %eax
	jb	.LBB5_36
	.p2align	4, 0x90
.LBB5_29:                               # %.lr.ph.i.i.i.i49
                                        #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rdx
	movq	24(%r13), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB5_30
# BB#31:                                #   in Loop: Header=BB5_29 Depth=2
	leaq	1(%rdx), %rax
	movq	%rax, (%r12)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB5_34
	jmp	.LBB5_33
	.p2align	4, 0x90
.LBB5_30:                               #   in Loop: Header=BB5_29 Depth=2
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%r13), %rax
	movq	24(%r13), %rcx
	cmpq	%rcx, %rax
	jae	.LBB5_33
.LBB5_34:                               #   in Loop: Header=BB5_29 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB5_35
	.p2align	4, 0x90
.LBB5_33:                               #   in Loop: Header=BB5_29 Depth=2
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB5_35:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i55
                                        #   in Loop: Header=BB5_29 Depth=2
	movl	64(%r13), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bl, %ecx
	orl	%eax, %ecx
	movl	%ecx, 64(%r13)
	movl	68(%r13), %eax
	addl	$-16, %eax
	movl	%eax, 68(%r13)
	cmpl	$15, %eax
	ja	.LBB5_29
.LBB5_36:                               # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit56
                                        #   in Loop: Header=BB5_13 Depth=1
	addl	$20, %r15d
.LBB5_27:                               # %.thread.outer
                                        #   in Loop: Header=BB5_13 Depth=1
	xorl	%ecx, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB5_14
.LBB5_37:                               #   in Loop: Header=BB5_13 Depth=1
	cmpl	$19, %eax
	je	.LBB5_39
# BB#38:                                #   in Loop: Header=BB5_13 Depth=1
	cmpl	$16, %eax
	ja	.LBB5_52
.LBB5_39:                               #   in Loop: Header=BB5_13 Depth=1
	cmpl	$17, %eax
	jae	.LBB5_41
# BB#40:                                #   in Loop: Header=BB5_13 Depth=1
	movl	$1, %r15d
	jmp	.LBB5_51
.LBB5_41:                               #   in Loop: Header=BB5_13 Depth=1
	movl	64(%r13), %r15d
	movl	68(%r13), %eax
	movl	$15, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r15d
	incl	%eax
	movl	%eax, 68(%r13)
	cmpl	$16, %eax
	jb	.LBB5_49
	.p2align	4, 0x90
.LBB5_42:                               # %.lr.ph.i.i.i.i57
                                        #   Parent Loop BB5_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rdx
	movq	24(%r13), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB5_43
# BB#44:                                #   in Loop: Header=BB5_42 Depth=2
	leaq	1(%rdx), %rax
	movq	%rax, (%r12)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB5_47
	jmp	.LBB5_46
	.p2align	4, 0x90
.LBB5_43:                               #   in Loop: Header=BB5_42 Depth=2
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%r13), %rax
	movq	24(%r13), %rcx
	cmpq	%rcx, %rax
	jae	.LBB5_46
.LBB5_47:                               #   in Loop: Header=BB5_42 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB5_48
	.p2align	4, 0x90
.LBB5_46:                               #   in Loop: Header=BB5_42 Depth=2
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB5_48:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i63
                                        #   in Loop: Header=BB5_42 Depth=2
	movl	64(%r13), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bl, %ecx
	orl	%eax, %ecx
	movl	%ecx, 64(%r13)
	movl	68(%r13), %eax
	addl	$-16, %eax
	movl	%eax, 68(%r13)
	cmpl	$15, %eax
	ja	.LBB5_42
.LBB5_49:                               # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit64
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	callq	_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_
	cmpl	$16, %eax
	ja	.LBB5_52
# BB#50:                                #   in Loop: Header=BB5_13 Depth=1
	shrl	$16, %r15d
	andl	$1, %r15d
	orl	$4, %r15d
.LBB5_51:                               #   in Loop: Header=BB5_13 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzbl	(%rcx,%r14), %edx
	movl	$17, %ecx
	subl	%eax, %ecx
	addl	%edx, %ecx
	movq	%rcx, %rax
	movl	$4042322161, %edx       # imm = 0xF0F0F0F1
	imulq	%rdx, %rax
	shrq	$36, %rax
	movl	%eax, %edx
	shll	$4, %edx
	addl	%eax, %edx
	subl	%edx, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_14:                               # %.thread.outer
                                        #   in Loop: Header=BB5_13 Depth=1
	movb	%cl, (%rax,%r14)
	movb	%cl, (%rdx,%r14)
	incq	%r14
	decl	%r15d
	cmpq	%rbp, %r14
	jb	.LBB5_13
# BB#15:
	movb	$1, %al
	jmp	.LBB5_53
.LBB5_52:
	xorl	%eax, %eax
.LBB5_53:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN9NCompress4NLzx8CDecoder9ReadTableEPhS2_j, .Lfunc_end5-_ZN9NCompress4NLzx8CDecoder9ReadTableEPhS2_j
	.cfi_endproc

	.section	.text._ZN9NCompress8NHuffman8CDecoderILi16ELj20EE14SetCodeLengthsEPKh,"axG",@progbits,_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE14SetCodeLengthsEPKh,comdat
	.weak	_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE14SetCodeLengthsEPKh
	.p2align	4, 0x90
	.type	_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE14SetCodeLengthsEPKh,@function
_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE14SetCodeLengthsEPKh: # @_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE14SetCodeLengthsEPKh
	.cfi_startproc
# BB#0:                                 # %.preheader66.preheader
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 208
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 52(%rsp)
	movups	%xmm0, 36(%rsp)
	movups	%xmm0, 20(%rsp)
	movups	%xmm0, 4(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_1:                                # %.preheader66
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	cmpq	$16, %rcx
	ja	.LBB6_19
# BB#2:                                 # %.critedge
                                        #   in Loop: Header=BB6_1 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 136(%r15,%rax,4)
	movzbl	1(%r14,%rax), %ecx
	cmpq	$16, %rcx
	ja	.LBB6_19
# BB#3:                                 # %.critedge.1
                                        #   in Loop: Header=BB6_1 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 140(%r15,%rax,4)
	addq	$2, %rax
	cmpl	$20, %eax
	jb	.LBB6_1
# BB#4:
	movl	$0, (%rsp)
	movl	$0, (%r15)
	movl	$0, 68(%r15)
	xorl	%eax, %eax
	movl	$15, %r13d
	movl	$1, %ebx
	movl	$65536, %esi            # imm = 0x10000
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movl	(%rsp,%rbx,4), %ebp
	movl	%r13d, %ecx
	shll	%cl, %ebp
	addl	%edx, %ebp
	cmpl	$65536, %ebp            # imm = 0x10000
	ja	.LBB6_19
# BB#6:                                 #   in Loop: Header=BB6_5 Depth=1
	cmpq	$16, %rbx
	movl	%ebp, %r12d
	cmovel	%esi, %r12d
	movl	%r12d, (%r15,%rbx,4)
	movl	-4(%rsp,%rbx,4), %ecx
	addl	64(%r15,%rbx,4), %ecx
	movl	%ecx, 68(%r15,%rbx,4)
	movl	%ecx, 80(%rsp,%rbx,4)
	cmpq	$9, %rbx
	jg	.LBB6_10
# BB#7:                                 #   in Loop: Header=BB6_5 Depth=1
	shrl	$7, %r12d
	cmpl	%r12d, %eax
	jae	.LBB6_10
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_5 Depth=1
	movl	%eax, %eax
	leaq	216(%r15,%rax), %rdi
	movl	%r12d, %edx
	subq	%rax, %rdx
	movl	%ebx, %esi
	callq	memset
	movl	$65536, %esi            # imm = 0x10000
	jmp	.LBB6_11
	.p2align	4, 0x90
.LBB6_10:                               #   in Loop: Header=BB6_5 Depth=1
	movl	%eax, %r12d
.LBB6_11:                               # %.loopexit64
                                        #   in Loop: Header=BB6_5 Depth=1
	incq	%rbx
	decl	%r13d
	cmpq	$17, %rbx
	movl	%r12d, %eax
	jl	.LBB6_5
# BB#12:                                # %.preheader.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_13:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_13 Depth=1
	movl	80(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 80(%rsp,%rcx,4)
	movl	%eax, 136(%r15,%rdx,4)
.LBB6_15:                               # %.preheader.194
                                        #   in Loop: Header=BB6_13 Depth=1
	movzbl	1(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB6_17
# BB#16:                                #   in Loop: Header=BB6_13 Depth=1
	movl	80(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 80(%rsp,%rcx,4)
	leal	1(%rax), %ecx
	movl	%ecx, 136(%r15,%rdx,4)
.LBB6_17:                               #   in Loop: Header=BB6_13 Depth=1
	addq	$2, %rax
	cmpq	$20, %rax
	jne	.LBB6_13
# BB#18:
	movb	$1, %al
	jmp	.LBB6_20
.LBB6_19:
	xorl	%eax, %eax
.LBB6_20:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE14SetCodeLengthsEPKh, .Lfunc_end6-_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE14SetCodeLengthsEPKh
	.cfi_endproc

	.section	.text._ZN9NCompress8NHuffman8CDecoderILi16ELj20EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_,"axG",@progbits,_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_,comdat
	.weak	_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_
	.p2align	4, 0x90
	.type	_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_,@function
_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_: # @_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 48
.Lcfi54:
	.cfi_offset %rbx, -48
.Lcfi55:
	.cfi_offset %r12, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	48(%rbx), %edx
	movl	52(%rbx), %eax
	movl	$15, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	shrl	%edx
	movzwl	%dx, %r15d
	cmpl	36(%r14), %r15d
	jae	.LBB7_1
# BB#15:
	movl	%r15d, %ecx
	shrl	$7, %ecx
	movzbl	216(%r14,%rcx), %ebp
	jmp	.LBB7_4
.LBB7_1:                                # %.preheader.preheader
	movl	$10, %ebp
	.p2align	4, 0x90
.LBB7_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpl	(%r14,%rbp,4), %r15d
	leaq	1(%rbp), %rbp
	jae	.LBB7_2
# BB#3:                                 # %.loopexit.loopexit
	decl	%ebp
.LBB7_4:                                # %.loopexit
	addl	%ebp, %eax
	movl	%eax, 52(%rbx)
	cmpl	$16, %eax
	jb	.LBB7_12
	.p2align	4, 0x90
.LBB7_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movq	8(%rbx), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB7_6
# BB#7:                                 #   in Loop: Header=BB7_5 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, (%rbx)
	movzbl	(%rdx), %r12d
	cmpq	%rcx, %rax
	jb	.LBB7_10
	jmp	.LBB7_9
	.p2align	4, 0x90
.LBB7_6:                                #   in Loop: Header=BB7_5 Depth=1
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %r12d
	movq	(%rbx), %rax
	movq	8(%rbx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB7_9
.LBB7_10:                               #   in Loop: Header=BB7_5 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	(%rax), %eax
	jmp	.LBB7_11
	.p2align	4, 0x90
.LBB7_9:                                #   in Loop: Header=BB7_5 Depth=1
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB7_11:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i.i
                                        #   in Loop: Header=BB7_5 Depth=1
	movl	48(%rbx), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%r12b, %ecx
	orl	%eax, %ecx
	movl	%ecx, 48(%rbx)
	movl	52(%rbx), %eax
	addl	$-16, %eax
	movl	%eax, 52(%rbx)
	cmpl	$15, %eax
	ja	.LBB7_5
.LBB7_12:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder7MovePosEj.exit
	movslq	%ebp, %rax
	subl	-4(%r14,%rax,4), %r15d
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r15d
	addl	68(%r14,%rax,4), %r15d
	movl	$-1, %eax
	cmpl	$19, %r15d
	ja	.LBB7_14
# BB#13:
	movl	%r15d, %eax
	movl	136(%r14,%rax,4), %eax
.LBB7_14:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_, .Lfunc_end7-_ZN9NCompress8NHuffman8CDecoderILi16ELj20EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_
	.cfi_endproc

	.text
	.globl	_ZN9NCompress4NLzx8CDecoder10ReadTablesEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder10ReadTablesEv,@function
_ZN9NCompress4NLzx8CDecoder10ReadTablesEv: # @_ZN9NCompress4NLzx8CDecoder10ReadTablesEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$664, %rsp              # imm = 0x298
.Lcfi65:
	.cfi_def_cfa_offset 720
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	leaq	16(%rbp), %r14
	cmpb	$0, 7412(%rbp)
	je	.LBB8_4
# BB#1:
	movq	16(%rbp), %rax
	cmpq	24(%rbp), %rax
	jae	.LBB8_3
# BB#2:
	incq	%rax
	movq	%rax, (%r14)
	jmp	.LBB8_4
.LBB8_3:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB8_4:                                # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit
	movl	68(%rbp), %eax
	cmpl	$15, %eax
	jbe	.LBB8_11
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB8_8
# BB#6:                                 #   in Loop: Header=BB8_5 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbp)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB8_9
	jmp	.LBB8_7
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_5 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%rbp), %rax
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB8_7
.LBB8_9:                                #   in Loop: Header=BB8_5 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, 16(%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB8_10
	.p2align	4, 0x90
.LBB8_7:                                #   in Loop: Header=BB8_5 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB8_10:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i
                                        #   in Loop: Header=BB8_5 Depth=1
	movl	64(%rbp), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bl, %ebx
	orl	%eax, %ebx
	movl	%ebx, 64(%rbp)
	movl	68(%rbp), %eax
	addl	$-16, %eax
	movl	%eax, 68(%rbp)
	cmpl	$15, %eax
	ja	.LBB8_5
	jmp	.LBB8_12
.LBB8_11:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit._ZN9NCompress4NLzx10NBitStream8CDecoder9NormalizeEv.exit_crit_edge
	movl	64(%rbp), %ebx
.LBB8_12:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder9NormalizeEv.exit
	movl	$15, %ecx
	subl	%eax, %ecx
	movl	%ebx, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r12d
	shrl	$14, %r12d
	andl	$7, %r12d
	addl	$3, %eax
	movl	%eax, 68(%rbp)
	cmpl	$16, %eax
	jb	.LBB8_19
	.p2align	4, 0x90
.LBB8_13:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB8_16
# BB#14:                                #   in Loop: Header=BB8_13 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbp)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB8_17
	jmp	.LBB8_15
	.p2align	4, 0x90
.LBB8_16:                               #   in Loop: Header=BB8_13 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%rbp), %rax
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB8_15
.LBB8_17:                               #   in Loop: Header=BB8_13 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, 16(%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB8_18
	.p2align	4, 0x90
.LBB8_15:                               #   in Loop: Header=BB8_13 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB8_18:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i
                                        #   in Loop: Header=BB8_13 Depth=1
	movl	64(%rbp), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bl, %ebx
	orl	%eax, %ebx
	movl	%ebx, 64(%rbp)
	movl	68(%rbp), %eax
	addl	$-16, %eax
	movl	%eax, 68(%rbp)
	cmpl	$15, %eax
	ja	.LBB8_13
.LBB8_19:                               # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit
	cmpl	$3, %r12d
	jbe	.LBB8_21
# BB#20:
	xorl	%eax, %eax
	jmp	.LBB8_93
.LBB8_21:
	cmpb	$0, 7413(%rbp)
	je	.LBB8_37
# BB#22:
	movl	$15, %r13d
	subl	%eax, %r13d
	incl	%eax
	movl	%eax, 68(%rbp)
	cmpl	$16, %eax
	movl	%ebx, %edx
	jb	.LBB8_29
	.p2align	4, 0x90
.LBB8_23:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB8_26
# BB#24:                                #   in Loop: Header=BB8_23 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbp)
	movzbl	(%rdx), %r15d
	cmpq	%rcx, %rax
	jb	.LBB8_27
	jmp	.LBB8_25
	.p2align	4, 0x90
.LBB8_26:                               #   in Loop: Header=BB8_23 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %r15d
	movq	16(%rbp), %rax
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB8_25
.LBB8_27:                               #   in Loop: Header=BB8_23 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, 16(%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB8_28
	.p2align	4, 0x90
.LBB8_25:                               #   in Loop: Header=BB8_23 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB8_28:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i46
                                        #   in Loop: Header=BB8_23 Depth=1
	movl	64(%rbp), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%r15b, %edx
	orl	%eax, %edx
	movl	%edx, 64(%rbp)
	movl	68(%rbp), %eax
	addl	$-16, %eax
	movl	%eax, 68(%rbp)
	cmpl	$15, %eax
	ja	.LBB8_23
.LBB8_29:                               # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit47
	movl	$65536, %esi            # imm = 0x10000
	movl	%r13d, %ecx
	shll	%cl, %esi
	movl	$32768, %r13d           # imm = 0x8000
	testl	%ebx, %esi
	jne	.LBB8_38
# BB#30:                                # %.lr.ph.i.i.i.i48
	movl	$15, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	shrl	%edx
	movzwl	%dx, %r13d
	addl	$16, %eax
	movl	%eax, 68(%rbp)
	.p2align	4, 0x90
.LBB8_31:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB8_34
# BB#32:                                #   in Loop: Header=BB8_31 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbp)
	movzbl	(%rdx), %r15d
	cmpq	%rcx, %rax
	jb	.LBB8_35
	jmp	.LBB8_33
	.p2align	4, 0x90
.LBB8_34:                               #   in Loop: Header=BB8_31 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %r15d
	movq	16(%rbp), %rax
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB8_33
.LBB8_35:                               #   in Loop: Header=BB8_31 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, 16(%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB8_36
	.p2align	4, 0x90
.LBB8_33:                               #   in Loop: Header=BB8_31 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB8_36:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i54
                                        #   in Loop: Header=BB8_31 Depth=1
	movl	64(%rbp), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%r15b, %ecx
	orl	%eax, %ecx
	movl	%ecx, 64(%rbp)
	movl	68(%rbp), %eax
	addl	$-16, %eax
	movl	%eax, 68(%rbp)
	cmpl	$15, %eax
	ja	.LBB8_31
	jmp	.LBB8_38
.LBB8_37:
	movl	$24, %esi
	movq	%r14, %rdi
	callq	_ZN9NCompress4NLzx10NBitStream8CDecoder11ReadBitsBigEj
	movl	%eax, %r13d
.LBB8_38:                               # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit55
	movl	%r13d, 7400(%rbp)
	cmpl	$3, %r12d
	sete	144(%rbp)
	jne	.LBB8_48
# BB#39:
	andb	$1, %r13b
	movb	%r13b, 7412(%rbp)
	movl	68(%rbp), %ecx
	movl	%ecx, %edx
	andl	$15, %edx
	movl	$16, %eax
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, 68(%rbp)
	cmpl	$16, %eax
	jb	.LBB8_46
	.p2align	4, 0x90
.LBB8_40:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB8_43
# BB#41:                                #   in Loop: Header=BB8_40 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbp)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB8_44
	jmp	.LBB8_42
	.p2align	4, 0x90
.LBB8_43:                               #   in Loop: Header=BB8_40 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%rbp), %rax
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB8_42
.LBB8_44:                               #   in Loop: Header=BB8_40 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, 16(%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB8_45
	.p2align	4, 0x90
.LBB8_42:                               #   in Loop: Header=BB8_40 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB8_45:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i62
                                        #   in Loop: Header=BB8_40 Depth=1
	movl	64(%rbp), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bl, %ecx
	orl	%eax, %ecx
	movl	%ecx, 64(%rbp)
	movl	68(%rbp), %eax
	addl	$-16, %eax
	movl	%eax, 68(%rbp)
	cmpl	$15, %eax
	ja	.LBB8_40
.LBB8_46:                               # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit63
	testl	%eax, %eax
	je	.LBB8_66
# BB#47:
	xorl	%eax, %eax
	jmp	.LBB8_93
.LBB8_48:
	movb	$0, 7412(%rbp)
	cmpl	$2, %r12d
	sete	145(%rbp)
	jne	.LBB8_59
# BB#49:                                # %.preheader
	movl	64(%rbp), %eax
	movl	68(%rbp), %edx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_50:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_51 Depth 2
	movl	$15, %ecx
	subl	%edx, %ecx
	movl	%eax, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r12d
	shrl	$14, %r12d
	addl	$3, %edx
	movl	%edx, 68(%rbp)
	cmpl	$16, %edx
	jb	.LBB8_57
	.p2align	4, 0x90
.LBB8_51:                               # %.lr.ph.i.i.i.i67
                                        #   Parent Loop BB8_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB8_54
# BB#52:                                #   in Loop: Header=BB8_51 Depth=2
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbp)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB8_55
	jmp	.LBB8_53
	.p2align	4, 0x90
.LBB8_54:                               #   in Loop: Header=BB8_51 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%rbp), %rax
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB8_53
.LBB8_55:                               #   in Loop: Header=BB8_51 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, 16(%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB8_56
	.p2align	4, 0x90
.LBB8_53:                               #   in Loop: Header=BB8_51 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB8_56:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i73
                                        #   in Loop: Header=BB8_51 Depth=2
	movl	64(%rbp), %ecx
	shll	$8, %ecx
	movzbl	%al, %edx
	orl	%ecx, %edx
	shll	$8, %edx
	movzbl	%bl, %eax
	orl	%edx, %eax
	movl	%eax, 64(%rbp)
	movl	68(%rbp), %edx
	addl	$-16, %edx
	movl	%edx, 68(%rbp)
	cmpl	$15, %edx
	ja	.LBB8_51
.LBB8_57:                               # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit74
                                        #   in Loop: Header=BB8_50 Depth=1
	andb	$7, %r12b
	movb	%r12b, (%rsp,%r15)
	incq	%r15
	cmpq	$8, %r15
	jne	.LBB8_50
# BB#58:
	leaq	5064(%rbp), %rdi
	movq	%rsp, %rsi
	callq	_ZN9NCompress8NHuffman8CDecoderILi16ELj8EE14SetCodeLengthsEPKh
	testb	%al, %al
	je	.LBB8_69
.LBB8_59:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder10ReadUInt32ERj.exit
	leaq	6472(%rbp), %rsi
	movq	%rsp, %rdx
	movl	$256, %ecx              # imm = 0x100
	movq	%rbp, %rdi
	callq	_ZN9NCompress4NLzx8CDecoder9ReadTableEPhS2_j
	testb	%al, %al
	je	.LBB8_69
# BB#60:
	leaq	6728(%rbp), %rsi
	leaq	256(%rsp), %rdx
	movl	140(%rbp), %ecx
	movq	%rbp, %rdi
	callq	_ZN9NCompress4NLzx8CDecoder9ReadTableEPhS2_j
	testb	%al, %al
	je	.LBB8_69
# BB#61:
	movl	140(%rbp), %eax
	leal	256(%rax), %ecx
	cmpl	$655, %ecx              # imm = 0x28F
	ja	.LBB8_63
# BB#62:                                # %.lr.ph.preheader
	movl	%ecx, %ecx
	leaq	(%rsp,%rcx), %rdi
	movl	$399, %edx              # imm = 0x18F
	subl	%eax, %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB8_63:                               # %._crit_edge
	leaq	148(%rbp), %rdi
	movq	%rsp, %rsi
	callq	_ZN9NCompress8NHuffman8CDecoderILi16ELj656EE14SetCodeLengthsEPKh
	testb	%al, %al
	je	.LBB8_94
# BB#64:
	leaq	7128(%rbp), %rsi
	movq	%rsp, %rdx
	movl	$249, %ecx
	movq	%rbp, %rdi
	callq	_ZN9NCompress4NLzx8CDecoder9ReadTableEPhS2_j
	testb	%al, %al
	je	.LBB8_94
# BB#65:
	addq	$3420, %rbp             # imm = 0xD5C
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	callq	_ZN9NCompress8NHuffman8CDecoderILi16ELj249EE14SetCodeLengthsEPKh
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	jmp	.LBB8_93
.LBB8_69:
	xorl	%eax, %eax
	jmp	.LBB8_93
.LBB8_66:                               # %.preheader75
	movl	64(%rbp), %eax
	roll	$16, %eax
	movl	$32, 68(%rbp)
	decl	%eax
	movl	%eax, 128(%rbp)
	movq	16(%rbp), %rax
	movq	24(%rbp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB8_70
# BB#67:
	leaq	1(%rax), %rsi
	movq	%rsi, 16(%rbp)
	movb	(%rax), %al
	jmp	.LBB8_71
.LBB8_70:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	16(%rbp), %rsi
	movq	24(%rbp), %rcx
.LBB8_71:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit66
	movzbl	%al, %r15d
	cmpq	%rcx, %rsi
	jae	.LBB8_73
# BB#72:
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rbp)
	movb	(%rsi), %al
	jmp	.LBB8_74
.LBB8_73:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rcx
.LBB8_74:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit66.1
	movzbl	%al, %ebx
	shll	$8, %ebx
	orl	%r15d, %ebx
	cmpq	%rcx, %rdx
	jae	.LBB8_76
# BB#75:
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%rbp)
	movb	(%rdx), %al
	jmp	.LBB8_77
.LBB8_76:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	16(%rbp), %rsi
	movq	24(%rbp), %rcx
.LBB8_77:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit66.2
	movzbl	%al, %r15d
	shll	$16, %r15d
	orl	%ebx, %r15d
	cmpq	%rcx, %rsi
	jae	.LBB8_79
# BB#78:
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rbp)
	movb	(%rsi), %al
	jmp	.LBB8_80
.LBB8_79:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rcx
.LBB8_80:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit66.3
	movzbl	%al, %eax
	shll	$24, %eax
	orl	%r15d, %eax
	decl	%eax
	movl	%eax, 132(%rbp)
	cmpq	%rcx, %rdx
	jae	.LBB8_82
# BB#81:
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%rbp)
	movb	(%rdx), %al
	jmp	.LBB8_83
.LBB8_82:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	16(%rbp), %rsi
	movq	24(%rbp), %rcx
.LBB8_83:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit66.186
	movzbl	%al, %r15d
	cmpq	%rcx, %rsi
	jae	.LBB8_85
# BB#84:
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rbp)
	movb	(%rsi), %al
	jmp	.LBB8_86
.LBB8_85:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rcx
.LBB8_86:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit66.1.1
	movzbl	%al, %r12d
	shll	$8, %r12d
	orl	%r15d, %r12d
	cmpq	%rcx, %rdx
	jae	.LBB8_88
# BB#87:
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%rbp)
	movb	(%rdx), %al
	jmp	.LBB8_89
.LBB8_88:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	16(%rbp), %rsi
	movq	24(%rbp), %rcx
.LBB8_89:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit66.2.1
	movzbl	%al, %ebx
	shll	$16, %ebx
	orl	%r12d, %ebx
	cmpq	%rcx, %rsi
	jae	.LBB8_91
# BB#90:
	leaq	1(%rsi), %rax
	movq	%rax, 16(%rbp)
	movb	(%rsi), %al
	jmp	.LBB8_92
.LBB8_91:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB8_92:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit66.3.1
	movzbl	%al, %eax
	shll	$24, %eax
	orl	%ebx, %eax
	decl	%eax
	movl	%eax, 136(%rbp)
	movb	$1, %al
	jmp	.LBB8_93
.LBB8_94:
	xorl	%eax, %eax
.LBB8_93:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder10ReadUInt32ERj.exit.thread
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$664, %rsp              # imm = 0x298
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN9NCompress4NLzx8CDecoder10ReadTablesEv, .Lfunc_end8-_ZN9NCompress4NLzx8CDecoder10ReadTablesEv
	.cfi_endproc

	.section	.text._ZN9NCompress4NLzx10NBitStream8CDecoder11ReadBitsBigEj,"axG",@progbits,_ZN9NCompress4NLzx10NBitStream8CDecoder11ReadBitsBigEj,comdat
	.weak	_ZN9NCompress4NLzx10NBitStream8CDecoder11ReadBitsBigEj
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx10NBitStream8CDecoder11ReadBitsBigEj,@function
_ZN9NCompress4NLzx10NBitStream8CDecoder11ReadBitsBigEj: # @_ZN9NCompress4NLzx10NBitStream8CDecoder11ReadBitsBigEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 64
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	%r14d, %eax
	shrl	%eax
	subl	%eax, %r14d
	movl	48(%rbx), %ebp
	movl	52(%rbx), %edx
	movl	$15, %r15d
	movl	$15, %ecx
	subl	%edx, %ecx
	movl	%ebp, %r13d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r13d
	andl	$131071, %r13d          # imm = 0x1FFFF
	movl	$17, %r12d
	movl	$17, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r13d
	addl	%edx, %eax
	movl	%eax, 52(%rbx)
	cmpl	$16, %eax
	jb	.LBB9_8
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movq	8(%rbx), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB9_2
# BB#3:                                 #   in Loop: Header=BB9_1 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, (%rbx)
	movzbl	(%rdx), %ebp
	cmpq	%rcx, %rax
	jb	.LBB9_6
	jmp	.LBB9_5
	.p2align	4, 0x90
.LBB9_2:                                #   in Loop: Header=BB9_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebp
	movq	(%rbx), %rax
	movq	8(%rbx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB9_5
.LBB9_6:                                #   in Loop: Header=BB9_1 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	(%rax), %eax
	jmp	.LBB9_7
	.p2align	4, 0x90
.LBB9_5:                                #   in Loop: Header=BB9_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB9_7:                                # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i
                                        #   in Loop: Header=BB9_1 Depth=1
	movl	48(%rbx), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bpl, %ebp
	orl	%eax, %ebp
	movl	%ebp, 48(%rbx)
	movl	52(%rbx), %eax
	addl	$-16, %eax
	movl	%eax, 52(%rbx)
	cmpl	$15, %eax
	ja	.LBB9_1
.LBB9_8:                                # %_ZN9NCompress4NLzx10NBitStream8CDecoder8ReadBitsEj.exit
	movl	%r14d, %ecx
	shll	%cl, %r13d
	subl	%eax, %r15d
	movl	%r15d, %ecx
	shrl	%cl, %ebp
	andl	$131071, %ebp           # imm = 0x1FFFF
	subl	%r14d, %r12d
	movl	%r12d, %ecx
	shrl	%cl, %ebp
	addl	%r14d, %eax
	movl	%eax, 52(%rbx)
	cmpl	$16, %eax
	jb	.LBB9_16
	.p2align	4, 0x90
.LBB9_9:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movq	8(%rbx), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB9_10
# BB#11:                                #   in Loop: Header=BB9_9 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, (%rbx)
	movzbl	(%rdx), %r14d
	cmpq	%rcx, %rax
	jb	.LBB9_14
	jmp	.LBB9_13
	.p2align	4, 0x90
.LBB9_10:                               #   in Loop: Header=BB9_9 Depth=1
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %r14d
	movq	(%rbx), %rax
	movq	8(%rbx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB9_13
.LBB9_14:                               #   in Loop: Header=BB9_9 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	(%rax), %eax
	jmp	.LBB9_15
	.p2align	4, 0x90
.LBB9_13:                               #   in Loop: Header=BB9_9 Depth=1
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB9_15:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i18
                                        #   in Loop: Header=BB9_9 Depth=1
	movl	48(%rbx), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%r14b, %ecx
	orl	%eax, %ecx
	movl	%ecx, 48(%rbx)
	movl	52(%rbx), %eax
	addl	$-16, %eax
	movl	%eax, 52(%rbx)
	cmpl	$15, %eax
	ja	.LBB9_9
.LBB9_16:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder8ReadBitsEj.exit19
	addl	%r13d, %ebp
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZN9NCompress4NLzx10NBitStream8CDecoder11ReadBitsBigEj, .Lfunc_end9-_ZN9NCompress4NLzx10NBitStream8CDecoder11ReadBitsBigEj
	.cfi_endproc

	.section	.text._ZN9NCompress8NHuffman8CDecoderILi16ELj8EE14SetCodeLengthsEPKh,"axG",@progbits,_ZN9NCompress8NHuffman8CDecoderILi16ELj8EE14SetCodeLengthsEPKh,comdat
	.weak	_ZN9NCompress8NHuffman8CDecoderILi16ELj8EE14SetCodeLengthsEPKh
	.p2align	4, 0x90
	.type	_ZN9NCompress8NHuffman8CDecoderILi16ELj8EE14SetCodeLengthsEPKh,@function
_ZN9NCompress8NHuffman8CDecoderILi16ELj8EE14SetCodeLengthsEPKh: # @_ZN9NCompress8NHuffman8CDecoderILi16ELj8EE14SetCodeLengthsEPKh
	.cfi_startproc
# BB#0:                                 # %.preheader66.preheader
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 208
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 132(%rsp)
	movups	%xmm0, 116(%rsp)
	movups	%xmm0, 100(%rsp)
	movups	%xmm0, 84(%rsp)
	movzbl	(%rbx), %eax
	cmpq	$16, %rax
	ja	.LBB10_39
# BB#2:                                 # %.critedge
	incl	80(%rsp,%rax,4)
	movl	$-1, 136(%r14)
	movzbl	1(%rbx), %eax
	cmpq	$16, %rax
	ja	.LBB10_39
# BB#4:                                 # %.critedge.1
	incl	80(%rsp,%rax,4)
	movl	$-1, 140(%r14)
	movzbl	2(%rbx), %eax
	cmpq	$16, %rax
	ja	.LBB10_39
# BB#6:                                 # %.critedge.2
	incl	80(%rsp,%rax,4)
	movl	$-1, 144(%r14)
	movzbl	3(%rbx), %eax
	cmpq	$16, %rax
	ja	.LBB10_39
# BB#8:                                 # %.critedge.3
	incl	80(%rsp,%rax,4)
	movl	$-1, 148(%r14)
	movzbl	4(%rbx), %eax
	cmpq	$16, %rax
	ja	.LBB10_39
# BB#10:                                # %.critedge.4
	incl	80(%rsp,%rax,4)
	movl	$-1, 152(%r14)
	movzbl	5(%rbx), %eax
	cmpq	$16, %rax
	ja	.LBB10_39
# BB#12:                                # %.critedge.5
	incl	80(%rsp,%rax,4)
	movl	$-1, 156(%r14)
	movzbl	6(%rbx), %eax
	cmpq	$16, %rax
	ja	.LBB10_39
# BB#14:                                # %.critedge.6
	incl	80(%rsp,%rax,4)
	movl	$-1, 160(%r14)
	movzbl	7(%rbx), %ecx
	xorl	%eax, %eax
	cmpq	$16, %rcx
	ja	.LBB10_40
# BB#15:                                # %.critedge.7
	incl	80(%rsp,%rcx,4)
	movl	$-1, 164(%r14)
	movl	$0, 80(%rsp)
	movl	$0, (%r14)
	movl	$0, 68(%r14)
	movl	$15, %r15d
	movl	$1, %r13d
	movl	$65536, %esi            # imm = 0x10000
	xorl	%ebp, %ebp
.LBB10_16:                              # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movl	80(%rsp,%r13,4), %ebp
	movl	%r15d, %ecx
	shll	%cl, %ebp
	addl	%edx, %ebp
	cmpl	$65536, %ebp            # imm = 0x10000
	ja	.LBB10_39
# BB#17:                                #   in Loop: Header=BB10_16 Depth=1
	cmpq	$16, %r13
	movl	%ebp, %r12d
	cmovel	%esi, %r12d
	movl	%r12d, (%r14,%r13,4)
	movl	76(%rsp,%r13,4), %ecx
	addl	64(%r14,%r13,4), %ecx
	movl	%ecx, 68(%r14,%r13,4)
	movl	%ecx, (%rsp,%r13,4)
	cmpq	$9, %r13
	jg	.LBB10_21
# BB#18:                                #   in Loop: Header=BB10_16 Depth=1
	shrl	$7, %r12d
	cmpl	%r12d, %eax
	jae	.LBB10_21
# BB#19:                                # %.lr.ph
                                        #   in Loop: Header=BB10_16 Depth=1
	movl	%eax, %eax
	leaq	168(%r14,%rax), %rdi
	movl	%r12d, %edx
	subq	%rax, %rdx
	movl	%r13d, %esi
	callq	memset
	movl	$65536, %esi            # imm = 0x10000
	jmp	.LBB10_22
.LBB10_21:                              #   in Loop: Header=BB10_16 Depth=1
	movl	%eax, %r12d
.LBB10_22:                              # %.loopexit64
                                        #   in Loop: Header=BB10_16 Depth=1
	incq	%r13
	decl	%r15d
	cmpq	$17, %r13
	movl	%r12d, %eax
	jl	.LBB10_16
# BB#23:                                # %.preheader.preheader
	movzbl	(%rbx), %eax
	testq	%rax, %rax
	je	.LBB10_25
# BB#24:
	movl	(%rsp,%rax,4), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%rsp,%rax,4)
	movl	$0, 136(%r14,%rcx,4)
.LBB10_25:                              # %.preheader.177
	movzbl	1(%rbx), %eax
	testq	%rax, %rax
	je	.LBB10_27
# BB#26:
	movl	(%rsp,%rax,4), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%rsp,%rax,4)
	movl	$1, 136(%r14,%rcx,4)
.LBB10_27:                              # %.preheader.278
	movzbl	2(%rbx), %eax
	testq	%rax, %rax
	je	.LBB10_29
# BB#28:
	movl	(%rsp,%rax,4), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%rsp,%rax,4)
	movl	$2, 136(%r14,%rcx,4)
.LBB10_29:                              # %.preheader.379
	movzbl	3(%rbx), %eax
	testq	%rax, %rax
	je	.LBB10_31
# BB#30:
	movl	(%rsp,%rax,4), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%rsp,%rax,4)
	movl	$3, 136(%r14,%rcx,4)
.LBB10_31:                              # %.preheader.480
	movzbl	4(%rbx), %eax
	testq	%rax, %rax
	je	.LBB10_33
# BB#32:
	movl	(%rsp,%rax,4), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%rsp,%rax,4)
	movl	$4, 136(%r14,%rcx,4)
.LBB10_33:                              # %.preheader.581
	movzbl	5(%rbx), %eax
	testq	%rax, %rax
	je	.LBB10_35
# BB#34:
	movl	(%rsp,%rax,4), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%rsp,%rax,4)
	movl	$5, 136(%r14,%rcx,4)
.LBB10_35:                              # %.preheader.682
	movzbl	6(%rbx), %eax
	testq	%rax, %rax
	je	.LBB10_37
# BB#36:
	movl	(%rsp,%rax,4), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%rsp,%rax,4)
	movl	$6, 136(%r14,%rcx,4)
.LBB10_37:                              # %.preheader.783
	movzbl	7(%rbx), %ecx
	testq	%rcx, %rcx
	movb	$1, %al
	je	.LBB10_40
# BB#38:
	movl	(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, (%rsp,%rcx,4)
	movl	$7, 136(%r14,%rdx,4)
	jmp	.LBB10_40
.LBB10_39:
	xorl	%eax, %eax
.LBB10_40:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN9NCompress8NHuffman8CDecoderILi16ELj8EE14SetCodeLengthsEPKh, .Lfunc_end10-_ZN9NCompress8NHuffman8CDecoderILi16ELj8EE14SetCodeLengthsEPKh
	.cfi_endproc

	.section	.text._ZN9NCompress8NHuffman8CDecoderILi16ELj656EE14SetCodeLengthsEPKh,"axG",@progbits,_ZN9NCompress8NHuffman8CDecoderILi16ELj656EE14SetCodeLengthsEPKh,comdat
	.weak	_ZN9NCompress8NHuffman8CDecoderILi16ELj656EE14SetCodeLengthsEPKh
	.p2align	4, 0x90
	.type	_ZN9NCompress8NHuffman8CDecoderILi16ELj656EE14SetCodeLengthsEPKh,@function
_ZN9NCompress8NHuffman8CDecoderILi16ELj656EE14SetCodeLengthsEPKh: # @_ZN9NCompress8NHuffman8CDecoderILi16ELj656EE14SetCodeLengthsEPKh
	.cfi_startproc
# BB#0:                                 # %.preheader66.preheader
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi104:
	.cfi_def_cfa_offset 208
.Lcfi105:
	.cfi_offset %rbx, -56
.Lcfi106:
	.cfi_offset %r12, -48
.Lcfi107:
	.cfi_offset %r13, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 52(%rsp)
	movups	%xmm0, 36(%rsp)
	movups	%xmm0, 20(%rsp)
	movups	%xmm0, 4(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_1:                               # %.preheader66
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	cmpq	$16, %rcx
	ja	.LBB11_19
# BB#2:                                 # %.critedge
                                        #   in Loop: Header=BB11_1 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 136(%r15,%rax,4)
	movzbl	1(%r14,%rax), %ecx
	cmpq	$16, %rcx
	ja	.LBB11_19
# BB#3:                                 # %.critedge.1
                                        #   in Loop: Header=BB11_1 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 140(%r15,%rax,4)
	addq	$2, %rax
	cmpl	$656, %eax              # imm = 0x290
	jb	.LBB11_1
# BB#4:
	movl	$0, (%rsp)
	movl	$0, (%r15)
	movl	$0, 68(%r15)
	xorl	%eax, %eax
	movl	$15, %r13d
	movl	$1, %ebx
	movl	$65536, %esi            # imm = 0x10000
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_5:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movl	(%rsp,%rbx,4), %ebp
	movl	%r13d, %ecx
	shll	%cl, %ebp
	addl	%edx, %ebp
	cmpl	$65536, %ebp            # imm = 0x10000
	ja	.LBB11_19
# BB#6:                                 #   in Loop: Header=BB11_5 Depth=1
	cmpq	$16, %rbx
	movl	%ebp, %r12d
	cmovel	%esi, %r12d
	movl	%r12d, (%r15,%rbx,4)
	movl	-4(%rsp,%rbx,4), %ecx
	addl	64(%r15,%rbx,4), %ecx
	movl	%ecx, 68(%r15,%rbx,4)
	movl	%ecx, 80(%rsp,%rbx,4)
	cmpq	$9, %rbx
	jg	.LBB11_10
# BB#7:                                 #   in Loop: Header=BB11_5 Depth=1
	shrl	$7, %r12d
	cmpl	%r12d, %eax
	jae	.LBB11_10
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB11_5 Depth=1
	movl	%eax, %eax
	leaq	2760(%r15,%rax), %rdi
	movl	%r12d, %edx
	subq	%rax, %rdx
	movl	%ebx, %esi
	callq	memset
	movl	$65536, %esi            # imm = 0x10000
	jmp	.LBB11_11
	.p2align	4, 0x90
.LBB11_10:                              #   in Loop: Header=BB11_5 Depth=1
	movl	%eax, %r12d
.LBB11_11:                              # %.loopexit64
                                        #   in Loop: Header=BB11_5 Depth=1
	incq	%rbx
	decl	%r13d
	cmpq	$17, %rbx
	movl	%r12d, %eax
	jl	.LBB11_5
# BB#12:                                # %.preheader.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_13:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB11_15
# BB#14:                                #   in Loop: Header=BB11_13 Depth=1
	movl	80(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 80(%rsp,%rcx,4)
	movl	%eax, 136(%r15,%rdx,4)
.LBB11_15:                              # %.preheader.194
                                        #   in Loop: Header=BB11_13 Depth=1
	movzbl	1(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB11_17
# BB#16:                                #   in Loop: Header=BB11_13 Depth=1
	movl	80(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 80(%rsp,%rcx,4)
	leal	1(%rax), %ecx
	movl	%ecx, 136(%r15,%rdx,4)
.LBB11_17:                              #   in Loop: Header=BB11_13 Depth=1
	addq	$2, %rax
	cmpq	$656, %rax              # imm = 0x290
	jne	.LBB11_13
# BB#18:
	movb	$1, %al
	jmp	.LBB11_20
.LBB11_19:
	xorl	%eax, %eax
.LBB11_20:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN9NCompress8NHuffman8CDecoderILi16ELj656EE14SetCodeLengthsEPKh, .Lfunc_end11-_ZN9NCompress8NHuffman8CDecoderILi16ELj656EE14SetCodeLengthsEPKh
	.cfi_endproc

	.section	.text._ZN9NCompress8NHuffman8CDecoderILi16ELj249EE14SetCodeLengthsEPKh,"axG",@progbits,_ZN9NCompress8NHuffman8CDecoderILi16ELj249EE14SetCodeLengthsEPKh,comdat
	.weak	_ZN9NCompress8NHuffman8CDecoderILi16ELj249EE14SetCodeLengthsEPKh
	.p2align	4, 0x90
	.type	_ZN9NCompress8NHuffman8CDecoderILi16ELj249EE14SetCodeLengthsEPKh,@function
_ZN9NCompress8NHuffman8CDecoderILi16ELj249EE14SetCodeLengthsEPKh: # @_ZN9NCompress8NHuffman8CDecoderILi16ELj249EE14SetCodeLengthsEPKh
	.cfi_startproc
# BB#0:                                 # %.preheader66.preheader
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi114:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi115:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi117:
	.cfi_def_cfa_offset 208
.Lcfi118:
	.cfi_offset %rbx, -56
.Lcfi119:
	.cfi_offset %r12, -48
.Lcfi120:
	.cfi_offset %r13, -40
.Lcfi121:
	.cfi_offset %r14, -32
.Lcfi122:
	.cfi_offset %r15, -24
.Lcfi123:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 52(%rsp)
	movups	%xmm0, 36(%rsp)
	movups	%xmm0, 20(%rsp)
	movups	%xmm0, 4(%rsp)
	xorl	%eax, %eax
	jmp	.LBB12_2
	.p2align	4, 0x90
.LBB12_1:                               # %.critedge.1
                                        #   in Loop: Header=BB12_2 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 140(%r15,%rax,4)
	addq	$2, %rax
.LBB12_2:                               # %.preheader66
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	cmpq	$16, %rcx
	ja	.LBB12_6
# BB#3:                                 # %.critedge
                                        #   in Loop: Header=BB12_2 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 136(%r15,%rax,4)
	leal	1(%rax), %ecx
	cmpl	$249, %ecx
	jae	.LBB12_7
# BB#4:                                 # %.preheader66.1
                                        #   in Loop: Header=BB12_2 Depth=1
	movzbl	1(%r14,%rax), %ecx
	cmpq	$16, %rcx
	jbe	.LBB12_1
.LBB12_6:
	xorl	%eax, %eax
	jmp	.LBB12_24
.LBB12_7:
	movl	$0, (%rsp)
	movl	$0, (%r15)
	movl	$0, 68(%r15)
	xorl	%eax, %eax
	movl	$15, %r13d
	movl	$1, %ebx
	movl	$65536, %esi            # imm = 0x10000
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_8:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movl	(%rsp,%rbx,4), %ebp
	movl	%r13d, %ecx
	shll	%cl, %ebp
	addl	%edx, %ebp
	cmpl	$65536, %ebp            # imm = 0x10000
	ja	.LBB12_22
# BB#9:                                 #   in Loop: Header=BB12_8 Depth=1
	cmpq	$16, %rbx
	movl	%ebp, %r12d
	cmovel	%esi, %r12d
	movl	%r12d, (%r15,%rbx,4)
	movl	-4(%rsp,%rbx,4), %ecx
	addl	64(%r15,%rbx,4), %ecx
	movl	%ecx, 68(%r15,%rbx,4)
	movl	%ecx, 80(%rsp,%rbx,4)
	cmpq	$9, %rbx
	jg	.LBB12_13
# BB#10:                                #   in Loop: Header=BB12_8 Depth=1
	shrl	$7, %r12d
	cmpl	%r12d, %eax
	jae	.LBB12_13
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB12_8 Depth=1
	movl	%eax, %eax
	leaq	1132(%r15,%rax), %rdi
	movl	%r12d, %edx
	subq	%rax, %rdx
	movl	%ebx, %esi
	callq	memset
	movl	$65536, %esi            # imm = 0x10000
	jmp	.LBB12_14
	.p2align	4, 0x90
.LBB12_13:                              #   in Loop: Header=BB12_8 Depth=1
	movl	%eax, %r12d
.LBB12_14:                              # %.loopexit64
                                        #   in Loop: Header=BB12_8 Depth=1
	incq	%rbx
	decl	%r13d
	cmpq	$17, %rbx
	movl	%r12d, %eax
	jl	.LBB12_8
# BB#15:                                # %.preheader.preheader
	xorl	%eax, %eax
	jmp	.LBB12_17
	.p2align	4, 0x90
.LBB12_16:                              #   in Loop: Header=BB12_17 Depth=1
	addq	$2, %rax
.LBB12_17:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB12_19
# BB#18:                                #   in Loop: Header=BB12_17 Depth=1
	movl	80(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 80(%rsp,%rcx,4)
	movl	%eax, 136(%r15,%rdx,4)
.LBB12_19:                              #   in Loop: Header=BB12_17 Depth=1
	cmpq	$248, %rax
	je	.LBB12_23
# BB#20:                                # %.preheader.1
                                        #   in Loop: Header=BB12_17 Depth=1
	movzbl	1(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB12_16
# BB#21:                                #   in Loop: Header=BB12_17 Depth=1
	movl	80(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 80(%rsp,%rcx,4)
	leal	1(%rax), %ecx
	movl	%ecx, 136(%r15,%rdx,4)
	jmp	.LBB12_16
.LBB12_22:
	xorl	%eax, %eax
	jmp	.LBB12_24
.LBB12_23:
	movb	$1, %al
.LBB12_24:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN9NCompress8NHuffman8CDecoderILi16ELj249EE14SetCodeLengthsEPKh, .Lfunc_end12-_ZN9NCompress8NHuffman8CDecoderILi16ELj249EE14SetCodeLengthsEPKh
	.cfi_endproc

	.text
	.globl	_ZN9NCompress4NLzx8CDecoder15ClearPrevLevelsEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder15ClearPrevLevelsEv,@function
_ZN9NCompress4NLzx8CDecoder15ClearPrevLevelsEv: # @_ZN9NCompress4NLzx8CDecoder15ClearPrevLevelsEv
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 16
	addq	$6472, %rdi             # imm = 0x1948
	xorl	%esi, %esi
	movl	$905, %edx              # imm = 0x389
	callq	memset
	popq	%rax
	retq
.Lfunc_end13:
	.size	_ZN9NCompress4NLzx8CDecoder15ClearPrevLevelsEv, .Lfunc_end13-_ZN9NCompress4NLzx8CDecoder15ClearPrevLevelsEv
	.cfi_endproc

	.globl	_ZN9NCompress4NLzx8CDecoder8CodeSpecEj
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder8CodeSpecEj,@function
_ZN9NCompress4NLzx8CDecoder8CodeSpecEj: # @_ZN9NCompress4NLzx8CDecoder8CodeSpecEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi128:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi129:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi131:
	.cfi_def_cfa_offset 96
.Lcfi132:
	.cfi_offset %rbx, -56
.Lcfi133:
	.cfi_offset %r12, -48
.Lcfi134:
	.cfi_offset %r13, -40
.Lcfi135:
	.cfi_offset %r14, -32
.Lcfi136:
	.cfi_offset %r15, -24
.Lcfi137:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r12
	cmpl	$-2, 7408(%r12)
	jne	.LBB14_36
# BB#1:
	movl	$0, 7408(%r12)
	leaq	16(%r12), %r14
	movq	%r14, %rdi
	callq	_ZN9CInBuffer4InitEv
	movl	$32, 68(%r12)
	cmpb	$0, 7404(%r12)
	je	.LBB14_3
# BB#2:
	cmpb	$0, 144(%r12)
	jne	.LBB14_36
	.p2align	4, 0x90
.LBB14_3:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rdx
	movq	24(%r12), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB14_6
# BB#4:                                 #   in Loop: Header=BB14_3 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, (%r14)
	movzbl	(%rdx), %r15d
	cmpq	%rcx, %rax
	jb	.LBB14_7
	jmp	.LBB14_5
	.p2align	4, 0x90
.LBB14_6:                               #   in Loop: Header=BB14_3 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %r15d
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB14_5
.LBB14_7:                               #   in Loop: Header=BB14_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB14_8
	.p2align	4, 0x90
.LBB14_5:                               #   in Loop: Header=BB14_3 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB14_8:                               # %_ZN9CInBuffer8ReadByteEv.exit3.i
                                        #   in Loop: Header=BB14_3 Depth=1
	movl	64(%r12), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%r15b, %ecx
	orl	%eax, %ecx
	movl	%ecx, 64(%r12)
	movl	68(%r12), %ebp
	leal	-16(%rbp), %eax
	movl	%eax, 68(%r12)
	cmpl	$15, %eax
	ja	.LBB14_3
# BB#9:                                 # %_ZN9NCompress4NLzx10NBitStream8CDecoder9NormalizeEv.exit
	cmpb	$0, 7404(%r12)
	jne	.LBB14_36
# BB#10:
	movb	$0, 7412(%r12)
	movl	$0, 7400(%r12)
	leaq	6472(%r12), %rdi
	xorl	%esi, %esi
	movl	$905, %edx              # imm = 0x389
	callq	memset
	movl	$12000000, %ecx         # imm = 0xB71B00
	movb	$1, %dl
	cmpb	$0, 7413(%r12)
	jne	.LBB14_35
# BB#11:
	movl	64(%r12), %r13d
	movl	$31, %ecx
	subl	%ebp, %ecx
	movl	%r13d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	andl	$65536, %eax            # imm = 0x10000
	movl	%eax, 16(%rsp)          # 4-byte Spill
	addl	$-15, %ebp
	movl	%ebp, 68(%r12)
	cmpl	$16, %ebp
	jb	.LBB14_18
	.p2align	4, 0x90
.LBB14_12:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rdx
	movq	24(%r12), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB14_15
# BB#13:                                #   in Loop: Header=BB14_12 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, (%r14)
	movzbl	(%rdx), %r15d
	cmpq	%rcx, %rax
	jb	.LBB14_16
	jmp	.LBB14_14
	.p2align	4, 0x90
.LBB14_15:                              #   in Loop: Header=BB14_12 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %r15d
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB14_14
.LBB14_16:                              #   in Loop: Header=BB14_12 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB14_17
	.p2align	4, 0x90
.LBB14_14:                              #   in Loop: Header=BB14_12 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB14_17:                              # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i
                                        #   in Loop: Header=BB14_12 Depth=1
	movl	64(%r12), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%r15b, %r13d
	orl	%eax, %r13d
	movl	%r13d, 64(%r12)
	movl	68(%r12), %ebp
	addl	$-16, %ebp
	movl	%ebp, 68(%r12)
	cmpl	$15, %ebp
	ja	.LBB14_12
.LBB14_18:                              # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, %edx
	shrl	$16, %edx
	testl	%eax, %eax
	jne	.LBB14_20
# BB#19:
	movl	$12000000, %ecx         # imm = 0xB71B00
	jmp	.LBB14_35
.LBB14_20:                              # %.lr.ph.i.i.i.i123
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movl	$15, %ecx
	subl	%ebp, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r13d
	shrl	%r13d
	addl	$16, %ebp
	movl	%ebp, 68(%r12)
	.p2align	4, 0x90
.LBB14_21:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rdx
	movq	24(%r12), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB14_24
# BB#22:                                #   in Loop: Header=BB14_21 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, (%r14)
	movzbl	(%rdx), %r15d
	cmpq	%rcx, %rax
	jb	.LBB14_25
	jmp	.LBB14_23
	.p2align	4, 0x90
.LBB14_24:                              #   in Loop: Header=BB14_21 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %r15d
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB14_23
.LBB14_25:                              #   in Loop: Header=BB14_21 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB14_26
	.p2align	4, 0x90
.LBB14_23:                              #   in Loop: Header=BB14_21 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB14_26:                              # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i129
                                        #   in Loop: Header=BB14_21 Depth=1
	movl	64(%r12), %ecx
	shll	$8, %ecx
	movzbl	%al, %edx
	orl	%ecx, %edx
	shll	$8, %edx
	movzbl	%r15b, %eax
	orl	%edx, %eax
	movl	%eax, 64(%r12)
	movl	68(%r12), %edx
	leal	-16(%rdx), %ecx
	movl	%ecx, 68(%r12)
	cmpl	$15, %ecx
	ja	.LBB14_21
# BB#27:                                # %.lr.ph.i.i.i.i131
	shll	$16, %r13d
	movl	$31, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	shrl	%eax
	movzwl	%ax, %ebp
	movl	%edx, 68(%r12)
	.p2align	4, 0x90
.LBB14_28:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rdx
	movq	24(%r12), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB14_31
# BB#29:                                #   in Loop: Header=BB14_28 Depth=1
	leaq	1(%rdx), %rax
	movq	%rax, (%r14)
	movzbl	(%rdx), %r15d
	cmpq	%rcx, %rax
	jb	.LBB14_32
	jmp	.LBB14_30
	.p2align	4, 0x90
.LBB14_31:                              #   in Loop: Header=BB14_28 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %r15d
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB14_30
.LBB14_32:                              #   in Loop: Header=BB14_28 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB14_33
	.p2align	4, 0x90
.LBB14_30:                              #   in Loop: Header=BB14_28 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB14_33:                              # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i.i137
                                        #   in Loop: Header=BB14_28 Depth=1
	movl	64(%r12), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%r15b, %ecx
	orl	%eax, %ecx
	movl	%ecx, 64(%r12)
	movl	68(%r12), %eax
	addl	$-16, %eax
	movl	%eax, 68(%r12)
	cmpl	$15, %eax
	ja	.LBB14_28
# BB#34:                                # %_ZN9NCompress4NLzx8CDecoder8ReadBitsEj.exit138
	movl	%ebp, %ecx
	orl	%r13d, %ecx
	movl	16(%rsp), %edx          # 4-byte Reload
.LBB14_35:                              # %.preheader206.loopexit
	movq	7384(%r12), %rax
	movb	%dl, 36(%rax)
	movl	%ecx, 32(%rax)
	movq	$0, 24(%rax)
	movl	$0, 136(%r12)
	movq	$0, 128(%r12)
.LBB14_36:                              # %.preheader206
	testl	%ebx, %ebx
	je	.LBB14_43
# BB#37:                                # %.preheader206
	movl	7408(%r12), %eax
	testl	%eax, %eax
	jle	.LBB14_43
# BB#38:                                # %.lr.ph228
	leaq	72(%r12), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_39:                              # =>This Inner Loop Header: Depth=1
	movl	80(%r12), %eax
	movl	%eax, %ecx
	subl	128(%r12), %ecx
	leal	-1(%rcx), %edx
	movl	92(%r12), %esi
	cmpl	%esi, %edx
	cmovbl	%ebp, %esi
	leal	-1(%rsi,%rcx), %ecx
	movq	72(%r12), %rdx
	movzbl	(%rdx,%rcx), %ecx
	leal	1(%rax), %esi
	movl	%esi, 80(%r12)
	movb	%cl, (%rdx,%rax)
	movl	80(%r12), %eax
	cmpl	84(%r12), %eax
	jne	.LBB14_41
# BB#40:                                #   in Loop: Header=BB14_39 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_41:                              # %_ZN12CLzOutWindow7PutByteEh.exit
                                        #   in Loop: Header=BB14_39 Depth=1
	movl	7408(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 7408(%r12)
	leal	-1(%rbx), %ecx
	cmpl	$1, %ebx
	je	.LBB14_44
# BB#42:                                # %_ZN12CLzOutWindow7PutByteEh.exit
                                        #   in Loop: Header=BB14_39 Depth=1
	cmpl	$1, %eax
	movl	%ecx, %ebx
	jg	.LBB14_39
	jmp	.LBB14_44
.LBB14_43:
	movl	%ebx, %ecx
.LBB14_44:                              # %_ZN12CLzOutWindow9CopyBlockEjj.exit.preheader
	testl	%ecx, %ecx
	je	.LBB14_149
# BB#45:                                # %.lr.ph217.lr.ph
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	leaq	72(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	16(%r12), %r15
	movl	28(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB14_49
.LBB14_47:                              # %.lr.ph217
                                        #   in Loop: Header=BB14_49 Depth=1
	movl	28(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB14_49
.LBB14_46:                              # %_ZN12CLzOutWindow9CopyBlockEjj.exit.loopexit
                                        #   in Loop: Header=BB14_49 Depth=1
	xorl	%eax, %eax
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jne	.LBB14_47
	jmp	.LBB14_171
	.p2align	4, 0x90
.LBB14_48:                              # %_ZN12CLzOutWindow9CopyBlockEjj.exit.loopexit203
                                        #   in Loop: Header=BB14_49 Depth=1
	testl	%r13d, %r13d
	je	.LBB14_149
.LBB14_49:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_55 Depth 2
                                        #     Child Loop BB14_169 Depth 2
                                        #       Child Loop BB14_62 Depth 3
                                        #         Child Loop BB14_65 Depth 4
                                        #         Child Loop BB14_68 Depth 4
                                        #       Child Loop BB14_85 Depth 3
                                        #       Child Loop BB14_88 Depth 3
                                        #       Child Loop BB14_102 Depth 3
                                        #       Child Loop BB14_108 Depth 3
                                        #       Child Loop BB14_121 Depth 3
                                        #       Child Loop BB14_124 Depth 3
                                        #       Child Loop BB14_144 Depth 3
                                        #       Child Loop BB14_156 Depth 3
                                        #       Child Loop BB14_160 Depth 3
                                        #       Child Loop BB14_165 Depth 3
                                        #       Child Loop BB14_167 Depth 3
	movl	%r13d, %ebx
	movl	7400(%r12), %eax
	testl	%eax, %eax
	jne	.LBB14_52
# BB#50:                                #   in Loop: Header=BB14_49 Depth=1
	movq	%r12, %rdi
	callq	_ZN9NCompress4NLzx8CDecoder10ReadTablesEv
	testb	%al, %al
	je	.LBB14_170
# BB#51:                                # %._crit_edge
                                        #   in Loop: Header=BB14_49 Depth=1
	movl	7400(%r12), %eax
.LBB14_52:                              #   in Loop: Header=BB14_49 Depth=1
	cmpl	%ebx, %eax
	movl	%ebx, %r14d
	cmovbl	%eax, %r14d
	movl	%ebx, %r13d
	subl	%r14d, %r13d
	movl	%eax, %ecx
	subl	%r14d, %ecx
	movl	%ecx, 7400(%r12)
	cmpb	$0, 144(%r12)
	je	.LBB14_168
# BB#53:                                # %.preheader
                                        #   in Loop: Header=BB14_49 Depth=1
	testl	%r14d, %r14d
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB14_48
# BB#54:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB14_49 Depth=1
	notl	%eax
	notl	%ebx
	cmpl	%ebx, %eax
	cmoval	%eax, %ebx
	incl	%ebx
	.p2align	4, 0x90
.LBB14_55:                              # %.lr.ph
                                        #   Parent Loop BB14_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jae	.LBB14_57
# BB#56:                                #   in Loop: Header=BB14_55 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB14_58
	.p2align	4, 0x90
.LBB14_57:                              #   in Loop: Header=BB14_55 Depth=2
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB14_58:                              # %_ZN9NCompress4NLzx10NBitStream8CDecoder14DirectReadByteEv.exit
                                        #   in Loop: Header=BB14_55 Depth=2
	movq	72(%r12), %rcx
	movl	80(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 80(%r12)
	movb	%al, (%rcx,%rdx)
	movl	80(%r12), %eax
	cmpl	84(%r12), %eax
	jne	.LBB14_60
# BB#59:                                #   in Loop: Header=BB14_55 Depth=2
	movq	%rbp, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_60:                              # %_ZN12CLzOutWindow7PutByteEh.exit141
                                        #   in Loop: Header=BB14_55 Depth=2
	incl	%ebx
	jne	.LBB14_55
	jmp	.LBB14_48
.LBB14_168:                             #   in Loop: Header=BB14_49 Depth=1
	movl	%r13d, 28(%rsp)         # 4-byte Spill
	jmp	.LBB14_169
	.p2align	4, 0x90
.LBB14_61:                              # %.lr.ph221.preheader
                                        #   in Loop: Header=BB14_169 Depth=2
	movl	%r14d, %eax
	notl	%eax
	.p2align	4, 0x90
.LBB14_62:                              # %.lr.ph221
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB14_65 Depth 4
                                        #         Child Loop BB14_68 Depth 4
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	64(%r12), %ebx
	movl	68(%r12), %eax
	movl	$15, %ecx
	subl	%eax, %ecx
	movl	%ebx, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	shrl	%edx
	movzwl	%dx, %r13d
	cmpl	184(%r12), %r13d
	jae	.LBB14_64
# BB#63:                                #   in Loop: Header=BB14_62 Depth=3
	movl	%r13d, %ecx
	shrl	$7, %ecx
	movzbl	2908(%r12,%rcx), %ebp
	jmp	.LBB14_67
	.p2align	4, 0x90
.LBB14_64:                              # %.preheader.i.preheader
                                        #   in Loop: Header=BB14_62 Depth=3
	movl	$47, %ebp
	.p2align	4, 0x90
.LBB14_65:                              # %.preheader.i
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        #       Parent Loop BB14_62 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	(%r12,%rbp,4), %r13d
	leaq	1(%rbp), %rbp
	jae	.LBB14_65
# BB#66:                                # %.loopexit.loopexit.i
                                        #   in Loop: Header=BB14_62 Depth=3
	addl	$-38, %ebp
.LBB14_67:                              # %.loopexit.i
                                        #   in Loop: Header=BB14_62 Depth=3
	addl	%ebp, %eax
	movl	%eax, 68(%r12)
	cmpl	$16, %eax
	jb	.LBB14_74
	.p2align	4, 0x90
.LBB14_68:                              # %.lr.ph.i.i
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        #       Parent Loop BB14_62 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%r12), %rdx
	movq	24(%r12), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB14_71
# BB#69:                                #   in Loop: Header=BB14_68 Depth=4
	leaq	1(%rdx), %rax
	movq	%rax, (%r15)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB14_72
	jmp	.LBB14_70
	.p2align	4, 0x90
.LBB14_71:                              #   in Loop: Header=BB14_68 Depth=4
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB14_70
.LBB14_72:                              #   in Loop: Header=BB14_68 Depth=4
	leaq	1(%rax), %rcx
	movq	%rcx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB14_73
	.p2align	4, 0x90
.LBB14_70:                              #   in Loop: Header=BB14_68 Depth=4
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB14_73:                              # %_ZN9CInBuffer8ReadByteEv.exit3.i.i
                                        #   in Loop: Header=BB14_68 Depth=4
	movl	64(%r12), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bl, %ebx
	orl	%eax, %ebx
	movl	%ebx, 64(%r12)
	movl	68(%r12), %eax
	addl	$-16, %eax
	movl	%eax, 68(%r12)
	cmpl	$15, %eax
	ja	.LBB14_68
.LBB14_74:                              # %_ZN9NCompress4NLzx10NBitStream8CDecoder7MovePosEj.exit
                                        #   in Loop: Header=BB14_62 Depth=3
	movslq	%ebp, %rdx
	subl	144(%r12,%rdx,4), %r13d
	movl	$16, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r13d
	addl	216(%r12,%rdx,4), %r13d
	cmpl	$655, %r13d             # imm = 0x28F
	ja	.LBB14_79
# BB#75:                                # %_ZN9NCompress8NHuffman8CDecoderILi16ELj656EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_.exit
                                        #   in Loop: Header=BB14_62 Depth=3
	movl	%r13d, %ecx
	movl	284(%r12,%rcx,4), %r13d
	cmpl	$255, %r13d
	ja	.LBB14_80
# BB#76:                                #   in Loop: Header=BB14_62 Depth=3
	movq	72(%r12), %rax
	movl	80(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 80(%r12)
	movb	%r13b, (%rax,%rcx)
	movl	80(%r12), %eax
	cmpl	84(%r12), %eax
	jne	.LBB14_78
# BB#77:                                #   in Loop: Header=BB14_62 Depth=3
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_78:                              # %_ZN12CLzOutWindow7PutByteEh.exit142
                                        #   in Loop: Header=BB14_62 Depth=3
	movl	4(%rsp), %eax           # 4-byte Reload
	incl	%eax
	decl	%r14d
	jne	.LBB14_62
	jmp	.LBB14_46
	.p2align	4, 0x90
.LBB14_79:                              #   in Loop: Header=BB14_169 Depth=2
	movl	$-1, %r13d
.LBB14_80:                              # %_ZN9NCompress8NHuffman8CDecoderILi16ELj656EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_.exit.thread
                                        #   in Loop: Header=BB14_169 Depth=2
	addl	$-256, %r13d
	cmpl	140(%r12), %r13d
	jae	.LBB14_170
# BB#81:                                #   in Loop: Header=BB14_169 Depth=2
	movl	%r13d, %ecx
	andl	$7, %ecx
	leal	2(%rcx), %ebp
	cmpl	$7, %ecx
	jne	.LBB14_97
# BB#82:                                #   in Loop: Header=BB14_169 Depth=2
	movl	$15, %ecx
	subl	%eax, %ecx
	movl	%ebx, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	shrl	%edx
	movzwl	%dx, %edx
	cmpl	3456(%r12), %edx
	movl	%ebp, (%rsp)            # 4-byte Spill
	jae	.LBB14_84
# BB#83:                                #   in Loop: Header=BB14_169 Depth=2
	movl	%edx, %ecx
	shrl	$7, %ecx
	movzbl	4552(%r12,%rcx), %ebp
	jmp	.LBB14_87
.LBB14_84:                              # %.preheader.i145.preheader
                                        #   in Loop: Header=BB14_169 Depth=2
	movl	$865, %ebp              # imm = 0x361
	.p2align	4, 0x90
.LBB14_85:                              # %.preheader.i145
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	(%r12,%rbp,4), %edx
	leaq	1(%rbp), %rbp
	jae	.LBB14_85
# BB#86:                                # %.loopexit.loopexit.i146
                                        #   in Loop: Header=BB14_169 Depth=2
	addl	$-856, %ebp             # imm = 0xFCA8
.LBB14_87:                              # %.loopexit.i148
                                        #   in Loop: Header=BB14_169 Depth=2
	movl	%edx, 8(%rsp)           # 4-byte Spill
	addl	%ebp, %eax
	movl	%eax, 68(%r12)
	cmpl	$16, %eax
	jb	.LBB14_94
	.p2align	4, 0x90
.LBB14_88:                              # %.lr.ph.i.i150
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r12), %rdx
	movq	24(%r12), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB14_91
# BB#89:                                #   in Loop: Header=BB14_88 Depth=3
	leaq	1(%rdx), %rax
	movq	%rax, (%r15)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB14_92
	jmp	.LBB14_90
	.p2align	4, 0x90
.LBB14_91:                              #   in Loop: Header=BB14_88 Depth=3
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB14_90
.LBB14_92:                              #   in Loop: Header=BB14_88 Depth=3
	leaq	1(%rax), %rcx
	movq	%rcx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB14_93
	.p2align	4, 0x90
.LBB14_90:                              #   in Loop: Header=BB14_88 Depth=3
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB14_93:                              # %_ZN9CInBuffer8ReadByteEv.exit3.i.i156
                                        #   in Loop: Header=BB14_88 Depth=3
	movl	64(%r12), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bl, %ebx
	orl	%eax, %ebx
	movl	%ebx, 64(%r12)
	movl	68(%r12), %eax
	addl	$-16, %eax
	movl	%eax, 68(%r12)
	cmpl	$15, %eax
	ja	.LBB14_88
.LBB14_94:                              # %_ZN9NCompress4NLzx10NBitStream8CDecoder7MovePosEj.exit157
                                        #   in Loop: Header=BB14_169 Depth=2
	movslq	%ebp, %rdx
	movl	8(%rsp), %esi           # 4-byte Reload
	subl	3416(%r12,%rdx,4), %esi
	movl	$16, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	addl	3488(%r12,%rdx,4), %esi
	movl	%esi, %ecx
	cmpl	$248, %ecx
	ja	.LBB14_170
# BB#95:                                # %_ZN9NCompress8NHuffman8CDecoderILi16ELj249EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_.exit
                                        #   in Loop: Header=BB14_169 Depth=2
	movl	%ecx, %ecx
	movl	3556(%r12,%rcx,4), %edx
	cmpl	$248, %edx
	movl	%edx, %ecx
	movl	$0, %esi
	cmoval	%esi, %ecx
	cmpl	$248, %edx
	ja	.LBB14_170
# BB#96:                                #   in Loop: Header=BB14_169 Depth=2
	movl	(%rsp), %ebp            # 4-byte Reload
	addl	%ecx, %ebp
.LBB14_97:                              #   in Loop: Header=BB14_169 Depth=2
	movl	%r13d, %ecx
	shrl	$3, %ecx
	cmpl	$23, %r13d
	movl	%ebp, (%rsp)            # 4-byte Spill
	ja	.LBB14_99
# BB#98:                                #   in Loop: Header=BB14_169 Depth=2
	movl	%ecx, %eax
	movl	128(%r12,%rax,4), %ebx
	movl	128(%r12), %ecx
	movl	%ecx, 128(%r12,%rax,4)
	jmp	.LBB14_134
	.p2align	4, 0x90
.LBB14_99:                              #   in Loop: Header=BB14_169 Depth=2
	cmpl	$303, %r13d             # imm = 0x12F
	ja	.LBB14_105
# BB#100:                               #   in Loop: Header=BB14_169 Depth=2
	shrl	$4, %r13d
	decl	%r13d
	andl	$1, %ecx
	orl	$2, %ecx
	movq	%rcx, %rdx
	movl	%r13d, %ecx
	shll	%cl, %edx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	cmpl	$3, %r13d
	jae	.LBB14_106
	jmp	.LBB14_101
.LBB14_105:                             #   in Loop: Header=BB14_169 Depth=2
	shll	$17, %ecx
	addl	$-4456448, %ecx         # imm = 0xFFBC0000
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$17, %r13d
	cmpl	$3, %r13d
	jb	.LBB14_101
.LBB14_106:                             #   in Loop: Header=BB14_169 Depth=2
	movb	145(%r12), %cl
	testb	%cl, %cl
	je	.LBB14_101
# BB#107:                               #   in Loop: Header=BB14_169 Depth=2
	movl	$15, %ecx
	subl	%eax, %ecx
	movl	%ebx, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$131071, %edx           # imm = 0x1FFFF
	movl	$20, %ecx
	subl	%r13d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	leal	-3(%r13,%rax), %eax
	movl	%eax, 68(%r12)
	cmpl	$16, %eax
	jb	.LBB14_114
	.p2align	4, 0x90
.LBB14_108:                             # %.lr.ph.i.i.i
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r12), %rdx
	movq	24(%r12), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB14_111
# BB#109:                               #   in Loop: Header=BB14_108 Depth=3
	leaq	1(%rdx), %rax
	movq	%rax, (%r15)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB14_112
	jmp	.LBB14_110
	.p2align	4, 0x90
.LBB14_111:                             #   in Loop: Header=BB14_108 Depth=3
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB14_110
.LBB14_112:                             #   in Loop: Header=BB14_108 Depth=3
	leaq	1(%rax), %rcx
	movq	%rcx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB14_113
	.p2align	4, 0x90
.LBB14_110:                             #   in Loop: Header=BB14_108 Depth=3
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB14_113:                             # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i
                                        #   in Loop: Header=BB14_108 Depth=3
	movl	64(%r12), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bl, %ebx
	orl	%eax, %ebx
	movl	%ebx, 64(%r12)
	movl	68(%r12), %eax
	addl	$-16, %eax
	movl	%eax, 68(%r12)
	cmpl	$15, %eax
	ja	.LBB14_108
.LBB14_114:                             # %_ZN9NCompress4NLzx10NBitStream8CDecoder8ReadBitsEj.exit
                                        #   in Loop: Header=BB14_169 Depth=2
	movl	$15, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebx
	shrl	%ebx
	movzwl	%bx, %r13d
	cmpl	5100(%r12), %r13d
	jae	.LBB14_120
# BB#115:                               #   in Loop: Header=BB14_169 Depth=2
	movl	%r13d, %ecx
	shrl	$7, %ecx
	movzbl	5232(%r12,%rcx), %ebp
	jmp	.LBB14_123
.LBB14_101:                             #   in Loop: Header=BB14_169 Depth=2
	movl	$15, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebx
	andl	$131071, %ebx           # imm = 0x1FFFF
	movl	$17, %ecx
	subl	%r13d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebx
	addl	%r13d, %eax
	movl	%eax, 68(%r12)
	cmpl	$16, %eax
	jb	.LBB14_119
	.p2align	4, 0x90
.LBB14_102:                             # %.lr.ph.i.i.i173
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r12), %rdx
	movq	24(%r12), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB14_116
# BB#103:                               #   in Loop: Header=BB14_102 Depth=3
	leaq	1(%rdx), %rax
	movq	%rax, (%r15)
	movzbl	(%rdx), %r13d
	cmpq	%rcx, %rax
	jb	.LBB14_117
	jmp	.LBB14_104
	.p2align	4, 0x90
.LBB14_116:                             #   in Loop: Header=BB14_102 Depth=3
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %r13d
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB14_104
.LBB14_117:                             #   in Loop: Header=BB14_102 Depth=3
	leaq	1(%rax), %rcx
	movq	%rcx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB14_118
	.p2align	4, 0x90
.LBB14_104:                             #   in Loop: Header=BB14_102 Depth=3
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB14_118:                             # %_ZN9CInBuffer8ReadByteEv.exit3.i.i.i179
                                        #   in Loop: Header=BB14_102 Depth=3
	movl	64(%r12), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%r13b, %ecx
	orl	%eax, %ecx
	movl	%ecx, 64(%r12)
	movl	68(%r12), %eax
	addl	$-16, %eax
	movl	%eax, 68(%r12)
	cmpl	$15, %eax
	ja	.LBB14_102
.LBB14_119:                             # %_ZN9NCompress4NLzx10NBitStream8CDecoder8ReadBitsEj.exit180
                                        #   in Loop: Header=BB14_169 Depth=2
	addl	8(%rsp), %ebx           # 4-byte Folded Reload
	jmp	.LBB14_133
.LBB14_120:                             # %.preheader.i160.preheader
                                        #   in Loop: Header=BB14_169 Depth=2
	movl	$1276, %ebp             # imm = 0x4FC
	.p2align	4, 0x90
.LBB14_121:                             # %.preheader.i160
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	(%r12,%rbp,4), %r13d
	leaq	1(%rbp), %rbp
	jae	.LBB14_121
# BB#122:                               # %.loopexit.loopexit.i161
                                        #   in Loop: Header=BB14_169 Depth=2
	addl	$-1267, %ebp            # imm = 0xFB0D
.LBB14_123:                             # %.loopexit.i163
                                        #   in Loop: Header=BB14_169 Depth=2
	addl	%ebp, %eax
	movl	%eax, 68(%r12)
	cmpl	$16, %eax
	jb	.LBB14_130
	.p2align	4, 0x90
.LBB14_124:                             # %.lr.ph.i.i165
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r12), %rdx
	movq	24(%r12), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB14_127
# BB#125:                               #   in Loop: Header=BB14_124 Depth=3
	leaq	1(%rdx), %rax
	movq	%rax, (%r15)
	movzbl	(%rdx), %ebx
	cmpq	%rcx, %rax
	jb	.LBB14_128
	jmp	.LBB14_126
	.p2align	4, 0x90
.LBB14_127:                             #   in Loop: Header=BB14_124 Depth=3
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	%eax, %ebx
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB14_126
.LBB14_128:                             #   in Loop: Header=BB14_124 Depth=3
	leaq	1(%rax), %rcx
	movq	%rcx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB14_129
	.p2align	4, 0x90
.LBB14_126:                             #   in Loop: Header=BB14_124 Depth=3
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB14_129:                             # %_ZN9CInBuffer8ReadByteEv.exit3.i.i171
                                        #   in Loop: Header=BB14_124 Depth=3
	movl	64(%r12), %ecx
	shll	$8, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$8, %eax
	movzbl	%bl, %ecx
	orl	%eax, %ecx
	movl	%ecx, 64(%r12)
	movl	68(%r12), %eax
	addl	$-16, %eax
	movl	%eax, 68(%r12)
	cmpl	$15, %eax
	ja	.LBB14_124
.LBB14_130:                             # %_ZN9NCompress4NLzx10NBitStream8CDecoder7MovePosEj.exit172
                                        #   in Loop: Header=BB14_169 Depth=2
	movslq	%ebp, %rax
	subl	5060(%r12,%rax,4), %r13d
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r13d
	addl	5132(%r12,%rax,4), %r13d
	cmpl	$7, %r13d
	ja	.LBB14_170
# BB#131:                               # %_ZN9NCompress8NHuffman8CDecoderILi16ELj8EE12DecodeSymbolINS_4NLzx10NBitStream8CDecoderEEEjPT_.exit
                                        #   in Loop: Header=BB14_169 Depth=2
	movl	%r13d, %eax
	movl	5200(%r12,%rax,4), %ecx
	cmpl	$7, %ecx
	movl	%ecx, %eax
	movl	$0, %edx
	cmoval	%edx, %eax
	cmpl	$7, %ecx
	ja	.LBB14_170
# BB#132:                               #   in Loop: Header=BB14_169 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	leal	(%rcx,%rdx,8), %ebx
	addl	%eax, %ebx
	movl	(%rsp), %ebp            # 4-byte Reload
.LBB14_133:                             #   in Loop: Header=BB14_169 Depth=2
	movl	132(%r12), %eax
	movl	%eax, 136(%r12)
	movl	128(%r12), %eax
	movl	%eax, 132(%r12)
	addl	$-3, %ebx
.LBB14_134:                             #   in Loop: Header=BB14_169 Depth=2
	movl	%ebx, 128(%r12)
	cmpl	%r14d, %ebp
	movl	%ebp, %r13d
	cmoval	%r14d, %r13d
	movl	80(%r12), %edx
	movl	%edx, %ebp
	subl	%ebx, %ebp
	decl	%ebp
	cmpl	%ebx, %edx
	ja	.LBB14_138
# BB#135:                               #   in Loop: Header=BB14_169 Depth=2
	cmpb	$0, 120(%r12)
	je	.LBB14_170
# BB#136:                               #   in Loop: Header=BB14_169 Depth=2
	movl	92(%r12), %ecx
	cmpl	%ebx, %ecx
	movl	$1, %eax
	jbe	.LBB14_171
# BB#137:                               #   in Loop: Header=BB14_169 Depth=2
	addl	%ebp, %ecx
	movl	%ecx, %ebp
.LBB14_138:                             #   in Loop: Header=BB14_169 Depth=2
	movl	84(%r12), %eax
	subl	%edx, %eax
	cmpl	%r13d, %eax
	jbe	.LBB14_142
# BB#139:                               #   in Loop: Header=BB14_169 Depth=2
	movl	92(%r12), %eax
	subl	%ebp, %eax
	cmpl	%r13d, %eax
	jbe	.LBB14_142
# BB#140:                               #   in Loop: Header=BB14_169 Depth=2
	movq	72(%r12), %r11
	movl	%ebp, %r10d
	leaq	(%r11,%r10), %rax
	leaq	(%r11,%rdx), %rcx
	movl	%edx, %esi
	addl	%r13d, %esi
	movl	%esi, 80(%r12)
	movl	(%rsp), %ebp            # 4-byte Reload
	cmpl	%r14d, %ebp
	movl	%ebp, %esi
	cmoval	%r14d, %esi
	decl	%esi
	incq	%rsi
	cmpq	$32, %rsi
	jae	.LBB14_150
# BB#141:                               #   in Loop: Header=BB14_169 Depth=2
	movl	%r13d, %edx
	jmp	.LBB14_163
	.p2align	4, 0x90
.LBB14_142:                             # %.preheader.i181.preheader
                                        #   in Loop: Header=BB14_169 Depth=2
	movl	(%rsp), %ebx            # 4-byte Reload
	notl	%ebx
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	%ebx, %eax
	cmoval	%eax, %ebx
	addl	$2, %ebx
	jmp	.LBB14_144
	.p2align	4, 0x90
.LBB14_143:                             # %._crit_edge.i
                                        #   in Loop: Header=BB14_144 Depth=3
	incl	%ebp
	movl	80(%r12), %edx
	incl	%ebx
.LBB14_144:                             # %.preheader.i181
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	92(%r12), %ebp
	movl	$0, %eax
	cmovel	%eax, %ebp
	movq	72(%r12), %rax
	movzbl	(%rax,%rbp), %ecx
	leal	1(%rdx), %esi
	movl	%esi, 80(%r12)
	movl	%edx, %edx
	movb	%cl, (%rax,%rdx)
	movl	80(%r12), %eax
	cmpl	84(%r12), %eax
	jne	.LBB14_146
# BB#145:                               #   in Loop: Header=BB14_144 Depth=3
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_146:                             #   in Loop: Header=BB14_144 Depth=3
	testl	%ebx, %ebx
	jne	.LBB14_143
.LBB14_147:                             # %.loopexit
                                        #   in Loop: Header=BB14_169 Depth=2
	subl	%r13d, %r14d
	movl	(%rsp), %eax            # 4-byte Reload
	subl	%r13d, %eax
	je	.LBB14_169
	jmp	.LBB14_148
.LBB14_150:                             # %min.iters.checked
                                        #   in Loop: Header=BB14_169 Depth=2
	movq	%rsi, %r9
	movabsq	$8589934560, %rdi       # imm = 0x1FFFFFFE0
	andq	%rdi, %r9
	je	.LBB14_153
# BB#151:                               # %vector.memcheck
                                        #   in Loop: Header=BB14_169 Depth=2
	cmpl	%r14d, %ebp
	movl	%ebp, %edi
	cmoval	%r14d, %edi
	decl	%edi
	leaq	(%r10,%rdi), %rbp
	leaq	1(%r11,%rbp), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB14_154
# BB#152:                               # %vector.memcheck
                                        #   in Loop: Header=BB14_169 Depth=2
	addq	%rdx, %rdi
	leaq	1(%r11,%rdi), %rdi
	cmpq	%rdi, %rax
	jae	.LBB14_154
.LBB14_153:                             #   in Loop: Header=BB14_169 Depth=2
	movl	%r13d, %edx
.LBB14_163:                             # %scalar.ph.preheader
                                        #   in Loop: Header=BB14_169 Depth=2
	leal	-1(%rdx), %esi
	movl	%edx, %edi
	andl	$7, %edi
	je	.LBB14_166
# BB#164:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB14_169 Depth=2
	negl	%edi
	.p2align	4, 0x90
.LBB14_165:                             # %scalar.ph.prol
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	incq	%rax
	movb	%bl, (%rcx)
	incq	%rcx
	decl	%edx
	incl	%edi
	jne	.LBB14_165
.LBB14_166:                             # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB14_169 Depth=2
	cmpl	$7, %esi
	jb	.LBB14_147
	.p2align	4, 0x90
.LBB14_167:                             # %scalar.ph
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	movb	%bl, (%rcx)
	movzbl	1(%rax), %ebx
	movb	%bl, 1(%rcx)
	movzbl	2(%rax), %ebx
	movb	%bl, 2(%rcx)
	movzbl	3(%rax), %ebx
	movb	%bl, 3(%rcx)
	movzbl	4(%rax), %ebx
	movb	%bl, 4(%rcx)
	movzbl	5(%rax), %ebx
	movb	%bl, 5(%rcx)
	movzbl	6(%rax), %ebx
	movb	%bl, 6(%rcx)
	movzbl	7(%rax), %ebx
	movb	%bl, 7(%rcx)
	addq	$8, %rax
	addq	$8, %rcx
	addl	$-8, %edx
	jne	.LBB14_167
	jmp	.LBB14_147
.LBB14_154:                             # %vector.body.preheader
                                        #   in Loop: Header=BB14_169 Depth=2
	leaq	-32(%r9), %rbp
	movl	%ebp, %edi
	shrl	$5, %edi
	incl	%edi
	testb	$3, %dil
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	je	.LBB14_157
# BB#155:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB14_169 Depth=2
	leaq	16(%r11,%rdx), %rbp
	leaq	16(%r11,%r10), %rbx
	movl	(%rsp), %edi            # 4-byte Reload
	movl	%edi, %r8d
	notl	%r8d
	movl	4(%rsp), %edi           # 4-byte Reload
	cmpl	%r8d, %edi
	cmoval	%edi, %r8d
	notl	%r8d
	andl	$96, %r8d
	addl	$-32, %r8d
	shrl	$5, %r8d
	incl	%r8d
	andl	$3, %r8d
	negq	%r8
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB14_156:                             # %vector.body.prol
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%rbx,%rdi), %xmm0
	movups	(%rbx,%rdi), %xmm1
	movups	%xmm0, -16(%rbp,%rdi)
	movups	%xmm1, (%rbp,%rdi)
	addq	$32, %rdi
	incq	%r8
	jne	.LBB14_156
	jmp	.LBB14_158
.LBB14_157:                             #   in Loop: Header=BB14_169 Depth=2
	xorl	%edi, %edi
.LBB14_158:                             # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB14_169 Depth=2
	cmpq	$96, 8(%rsp)            # 8-byte Folded Reload
	jb	.LBB14_161
# BB#159:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB14_169 Depth=2
	movl	(%rsp), %ebx            # 4-byte Reload
	notl	%ebx
	movl	4(%rsp), %ebp           # 4-byte Reload
	cmpl	%ebx, %ebp
	cmoval	%ebp, %ebx
	movl	$-2, %ebp
	subl	%ebx, %ebp
	incq	%rbp
	movabsq	$8589934560, %rbx       # imm = 0x1FFFFFFE0
	andq	%rbx, %rbp
	subq	%rdi, %rbp
	leaq	(%rdx,%rdi), %rdx
	leaq	112(%r11,%rdx), %rdx
	addq	%rdi, %r10
	leaq	112(%r11,%r10), %rdi
	.p2align	4, 0x90
.LBB14_160:                             # %vector.body
                                        #   Parent Loop BB14_49 Depth=1
                                        #     Parent Loop BB14_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rdi
	addq	$-128, %rbp
	jne	.LBB14_160
.LBB14_161:                             # %middle.block
                                        #   in Loop: Header=BB14_169 Depth=2
	cmpq	%r9, %rsi
	je	.LBB14_147
# BB#162:                               #   in Loop: Header=BB14_169 Depth=2
	movl	%r13d, %edx
	subl	%r9d, %edx
	addq	%r9, %rax
	addq	%r9, %rcx
	jmp	.LBB14_163
	.p2align	4, 0x90
.LBB14_169:                             # %_ZN12CLzOutWindow9CopyBlockEjj.exit.thread199.outer
                                        #   Parent Loop BB14_49 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_62 Depth 3
                                        #         Child Loop BB14_65 Depth 4
                                        #         Child Loop BB14_68 Depth 4
                                        #       Child Loop BB14_85 Depth 3
                                        #       Child Loop BB14_88 Depth 3
                                        #       Child Loop BB14_102 Depth 3
                                        #       Child Loop BB14_108 Depth 3
                                        #       Child Loop BB14_121 Depth 3
                                        #       Child Loop BB14_124 Depth 3
                                        #       Child Loop BB14_144 Depth 3
                                        #       Child Loop BB14_156 Depth 3
                                        #       Child Loop BB14_160 Depth 3
                                        #       Child Loop BB14_165 Depth 3
                                        #       Child Loop BB14_167 Depth 3
	testl	%r14d, %r14d
	jne	.LBB14_61
	jmp	.LBB14_46
.LBB14_170:
	movl	$1, %eax
	jmp	.LBB14_171
.LBB14_148:
	movl	%eax, 7408(%r12)
.LBB14_149:                             # %_ZN12CLzOutWindow9CopyBlockEjj.exit.thread
	xorl	%eax, %eax
.LBB14_171:                             # %_ZN12CLzOutWindow9CopyBlockEjj.exit.thread
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN9NCompress4NLzx8CDecoder8CodeSpecEj, .Lfunc_end14-_ZN9NCompress4NLzx8CDecoder8CodeSpecEj
	.cfi_endproc

	.globl	_ZN9NCompress4NLzx8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress4NLzx8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress4NLzx8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi141:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi142:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi144:
	.cfi_def_cfa_offset 80
.Lcfi145:
	.cfi_offset %rbx, -56
.Lcfi146:
	.cfi_offset %r12, -48
.Lcfi147:
	.cfi_offset %r13, -40
.Lcfi148:
	.cfi_offset %r14, -32
.Lcfi149:
	.cfi_offset %r15, -24
.Lcfi150:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %rbx
	movq	%rdx, %r15
	movq	%rdi, %r13
	testq	%rbx, %rbx
	je	.LBB15_1
# BB#2:
	movq	(%rbx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*56(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB15_35
# BB#3:
	movq	7384(%r13), %rbp
	testq	%r15, %r15
	je	.LBB15_5
# BB#4:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*8(%rax)
.LBB15_5:
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_7
# BB#6:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB15_7:                               # %_ZN9NCompress4NLzx20Cx86ConvertOutStream9SetStreamEP20ISequentialOutStream.exit
	movq	%r15, 16(%rbp)
	leaq	72(%r13), %r15
	movq	7392(%r13), %rsi
	movq	%r15, %rdi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	*72(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB15_35
# BB#8:
.Ltmp29:
	movq	%r15, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movq	%rax, %r12
.Ltmp30:
# BB#9:                                 # %.thread.preheader
	movl	$262144, %ebx           # imm = 0x40000
	testq	%r14, %r14
	je	.LBB15_17
	.p2align	4, 0x90
.LBB15_10:                              # %.thread.outer
                                        # =>This Inner Loop Header: Depth=1
.Ltmp32:
	movq	%r15, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp33:
# BB#11:                                #   in Loop: Header=BB15_10 Depth=1
	movq	%r12, %rsi
	subq	%rax, %rsi
	addq	(%rsp), %rsi            # 8-byte Folded Reload
	cmpq	$262144, %rsi           # imm = 0x40000
	cmovael	%ebx, %esi
	testl	%esi, %esi
	je	.LBB15_28
# BB#12:                                #   in Loop: Header=BB15_10 Depth=1
.Ltmp35:
	movq	%r13, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN9NCompress4NLzx8CDecoder8CodeSpecEj
	movl	%eax, %ebp
.Ltmp36:
# BB#13:                                #   in Loop: Header=BB15_10 Depth=1
	testl	%ebp, %ebp
	jne	.LBB15_21
# BB#14:                                # %.us-lcssa113
                                        #   in Loop: Header=BB15_10 Depth=1
	movq	16(%r13), %rax
	addq	48(%r13), %rax
	subq	32(%r13), %rax
	movl	$32, %ecx
	subl	68(%r13), %ecx
	shrl	$3, %ecx
	subq	%rcx, %rax
	movq	%rax, 16(%rsp)
.Ltmp38:
	movq	%r15, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp39:
# BB#15:                                #   in Loop: Header=BB15_10 Depth=1
	subq	%r12, %rax
	movq	%rax, 8(%rsp)
	movq	(%r14), %rax
.Ltmp41:
	movq	%r14, %rdi
	leaq	16(%rsp), %rsi
	leaq	8(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp42:
# BB#16:                                #   in Loop: Header=BB15_10 Depth=1
	testl	%ebp, %ebp
	je	.LBB15_10
	jmp	.LBB15_21
	.p2align	4, 0x90
.LBB15_17:                              # %.thread.us.us
                                        # =>This Inner Loop Header: Depth=1
.Ltmp44:
	movq	%r15, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp45:
# BB#18:                                #   in Loop: Header=BB15_17 Depth=1
	movq	%r12, %rsi
	subq	%rax, %rsi
	addq	(%rsp), %rsi            # 8-byte Folded Reload
	cmpq	$262144, %rsi           # imm = 0x40000
	cmovael	%ebx, %esi
	testl	%esi, %esi
	je	.LBB15_28
# BB#19:                                #   in Loop: Header=BB15_17 Depth=1
.Ltmp47:
	movq	%r13, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN9NCompress4NLzx8CDecoder8CodeSpecEj
	movl	%eax, %ebp
.Ltmp48:
# BB#20:                                #   in Loop: Header=BB15_17 Depth=1
	testl	%ebp, %ebp
	je	.LBB15_17
.LBB15_21:                              # %.us-lcssa119.us
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*48(%rax)
	jmp	.LBB15_29
.LBB15_1:
	movl	$-2147024809, %ebp      # imm = 0x80070057
.LBB15_35:                              # %_ZN9NCompress4NLzx15CDecoderFlusherD2Ev.exit84
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_28:                              # %.thread93
	movq	(%r13), %rax
.Ltmp52:
	movq	%r13, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp53:
.LBB15_29:                              # %._crit_edge.i81
	movq	96(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB15_31
# BB#30:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 96(%r13)
.LBB15_31:                              # %_ZN10COutBuffer13ReleaseStreamEv.exit.i.i82
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB15_33
# BB#32:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 40(%r13)
.LBB15_33:                              # %_ZN9NCompress4NLzx10NBitStream8CDecoder13ReleaseStreamEv.exit.i.i83
	movq	7384(%r13), %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_35
# BB#34:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%rbx)
	jmp	.LBB15_35
.LBB15_36:
.Ltmp54:
	movq	%rax, %rbp
	jmp	.LBB15_39
.LBB15_37:
.Ltmp31:
	jmp	.LBB15_38
.LBB15_23:                              # %.us-lcssa111.us.us-lcssa.us
.Ltmp49:
	jmp	.LBB15_38
.LBB15_22:                              # %.us-lcssa.us.us-lcssa.us
.Ltmp46:
	jmp	.LBB15_38
.LBB15_27:                              # %.us-lcssa117
.Ltmp43:
	jmp	.LBB15_38
.LBB15_26:                              # %.us-lcssa115
.Ltmp40:
	jmp	.LBB15_38
.LBB15_25:                              # %.us-lcssa111.us-lcssa
.Ltmp37:
	jmp	.LBB15_38
.LBB15_24:                              # %.us-lcssa.us-lcssa
.Ltmp34:
.LBB15_38:
	movq	%rax, %rbp
	movq	(%r13), %rax
.Ltmp50:
	movq	%r13, %rdi
	callq	*48(%rax)
.Ltmp51:
.LBB15_39:                              # %._crit_edge.i
	movq	96(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB15_42
# BB#40:
	movq	(%rdi), %rax
.Ltmp55:
	callq	*16(%rax)
.Ltmp56:
# BB#41:                                # %.noexc77
	movq	$0, 96(%r13)
.LBB15_42:                              # %_ZN10COutBuffer13ReleaseStreamEv.exit.i.i
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB15_45
# BB#43:
	movq	(%rdi), %rax
.Ltmp57:
	callq	*16(%rax)
.Ltmp58:
# BB#44:                                # %.noexc78
	movq	$0, 40(%r13)
.LBB15_45:                              # %_ZN9NCompress4NLzx10NBitStream8CDecoder13ReleaseStreamEv.exit.i.i
	movq	7384(%r13), %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_48
# BB#46:
	movq	(%rdi), %rax
.Ltmp59:
	callq	*16(%rax)
.Ltmp60:
# BB#47:                                # %.noexc79
	movq	$0, 16(%rbx)
.LBB15_48:                              # %_ZN9NCompress4NLzx15CDecoderFlusherD2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB15_49:
.Ltmp61:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN9NCompress4NLzx8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end15-_ZN9NCompress4NLzx8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\262\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp29-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin1   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin1   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin1   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin1   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin1   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin1   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp52-.Ltmp48         #   Call between .Ltmp48 and .Ltmp52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin1   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp50-.Ltmp53         #   Call between .Ltmp53 and .Ltmp50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp60-.Ltmp50         #   Call between .Ltmp50 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin1   #     jumps to .Ltmp61
	.byte	1                       #   On action: 1
	.long	.Ltmp60-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Lfunc_end15-.Ltmp60    #   Call between .Ltmp60 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress4NLzx8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress4NLzx8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress4NLzx8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 16
.Lcfi152:
	.cfi_offset %rbx, -16
.Ltmp62:
	callq	_ZN9NCompress4NLzx8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	movl	%eax, %ebx
.Ltmp63:
.LBB16_4:
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB16_1:
.Ltmp64:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB16_3
# BB#2:
	movl	(%rax), %ebx
	callq	__cxa_end_catch
	jmp	.LBB16_4
.LBB16_3:
	callq	__cxa_end_catch
	movl	$1, %ebx
	jmp	.LBB16_4
.Lfunc_end16:
	.size	_ZN9NCompress4NLzx8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end16-_ZN9NCompress4NLzx8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\250"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp62-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin2   #     jumps to .Ltmp64
	.byte	3                       #   On action: 2
	.long	.Ltmp63-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end16-.Ltmp63    #   Call between .Ltmp63 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress4NLzx8CDecoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder11SetInStreamEP19ISequentialInStream,@function
_ZN9NCompress4NLzx8CDecoder11SetInStreamEP19ISequentialInStream: # @_ZN9NCompress4NLzx8CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi153:
	.cfi_def_cfa_offset 16
	addq	$16, %rdi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end17:
	.size	_ZN9NCompress4NLzx8CDecoder11SetInStreamEP19ISequentialInStream, .Lfunc_end17-_ZN9NCompress4NLzx8CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN9NCompress4NLzx8CDecoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder15ReleaseInStreamEv,@function
_ZN9NCompress4NLzx8CDecoder15ReleaseInStreamEv: # @_ZN9NCompress4NLzx8CDecoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 16
.Lcfi155:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 40(%rbx)
.LBB18_2:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoder13ReleaseStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end18:
	.size	_ZN9NCompress4NLzx8CDecoder15ReleaseInStreamEv, .Lfunc_end18-_ZN9NCompress4NLzx8CDecoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZN9NCompress4NLzx8CDecoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder16SetOutStreamSizeEPKy,@function
_ZN9NCompress4NLzx8CDecoder16SetOutStreamSizeEPKy: # @_ZN9NCompress4NLzx8CDecoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi156:
	.cfi_def_cfa_offset 16
	testq	%rsi, %rsi
	je	.LBB19_1
# BB#2:
	movl	$-2, 7408(%rdi)
	movzbl	7404(%rdi), %esi
	addq	$72, %rdi
	callq	_ZN12CLzOutWindow4InitEb
	xorl	%eax, %eax
	popq	%rcx
	retq
.LBB19_1:
	movl	$-2147467259, %eax      # imm = 0x80004005
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZN9NCompress4NLzx8CDecoder16SetOutStreamSizeEPKy, .Lfunc_end19-_ZN9NCompress4NLzx8CDecoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.globl	_ZN9NCompress4NLzx8CDecoder9SetParamsEj
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder9SetParamsEj,@function
_ZN9NCompress4NLzx8CDecoder9SetParamsEj: # @_ZN9NCompress4NLzx8CDecoder9SetParamsEj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 16
.Lcfi158:
	.cfi_offset %rbx, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rbx
	leal	-15(%rsi), %ecx
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$6, %ecx
	ja	.LBB20_3
# BB#1:
	movl	%esi, %eax
	shll	$4, %eax
	cmpl	$20, %esi
	movl	$336, %ecx              # imm = 0x150
	movl	$400, %edx              # imm = 0x190
	cmovel	%ecx, %edx
	cmovbl	%eax, %edx
	movl	%edx, 140(%rbx)
	leaq	72(%rbx), %rdi
	movl	$2097152, %esi          # imm = 0x200000
	callq	_ZN10COutBuffer6CreateEj
	movl	%eax, %ecx
	movl	$-2147024882, %eax      # imm = 0x8007000E
	testb	%cl, %cl
	je	.LBB20_3
# BB#2:
	addq	$16, %rbx
	movl	$65536, %esi            # imm = 0x10000
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer6CreateEj
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147024882, %eax      # imm = 0x8007000E
	cmovnel	%ecx, %eax
.LBB20_3:
	popq	%rbx
	retq
.Lfunc_end20:
	.size	_ZN9NCompress4NLzx8CDecoder9SetParamsEj, .Lfunc_end20-_ZN9NCompress4NLzx8CDecoder9SetParamsEj
	.cfi_endproc

	.section	.text._ZN9NCompress4NLzx8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress4NLzx8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress4NLzx8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress4NLzx8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress4NLzx8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi159:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB21_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB21_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB21_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB21_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB21_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB21_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB21_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB21_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB21_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB21_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB21_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB21_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB21_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB21_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB21_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB21_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB21_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZN9NCompress4NLzx8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end21-_ZN9NCompress4NLzx8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress4NLzx8CDecoder6AddRefEv,"axG",@progbits,_ZN9NCompress4NLzx8CDecoder6AddRefEv,comdat
	.weak	_ZN9NCompress4NLzx8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder6AddRefEv,@function
_ZN9NCompress4NLzx8CDecoder6AddRefEv:   # @_ZN9NCompress4NLzx8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end22:
	.size	_ZN9NCompress4NLzx8CDecoder6AddRefEv, .Lfunc_end22-_ZN9NCompress4NLzx8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress4NLzx8CDecoder7ReleaseEv,"axG",@progbits,_ZN9NCompress4NLzx8CDecoder7ReleaseEv,comdat
	.weak	_ZN9NCompress4NLzx8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoder7ReleaseEv,@function
_ZN9NCompress4NLzx8CDecoder7ReleaseEv:  # @_ZN9NCompress4NLzx8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi160:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB23_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB23_2:
	popq	%rcx
	retq
.Lfunc_end23:
	.size	_ZN9NCompress4NLzx8CDecoder7ReleaseEv, .Lfunc_end23-_ZN9NCompress4NLzx8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress4NLzx8CDecoderD2Ev,"axG",@progbits,_ZN9NCompress4NLzx8CDecoderD2Ev,comdat
	.weak	_ZN9NCompress4NLzx8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoderD2Ev,@function
_ZN9NCompress4NLzx8CDecoderD2Ev:        # @_ZN9NCompress4NLzx8CDecoderD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 32
.Lcfi164:
	.cfi_offset %rbx, -24
.Lcfi165:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN9NCompress4NLzx8CDecoderE+16, (%rbx)
	movq	7392(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp65:
	callq	*16(%rax)
.Ltmp66:
.LBB24_2:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	leaq	72(%rbx), %rdi
.Ltmp76:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp77:
# BB#3:
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp82:
	callq	*16(%rax)
.Ltmp83:
.LBB24_5:                               # %_ZN10COutBufferD2Ev.exit
	leaq	16(%rbx), %rdi
.Ltmp94:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp95:
# BB#6:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_8
# BB#7:
	movq	(%rdi), %rax
.Ltmp100:
	callq	*16(%rax)
.Ltmp101:
.LBB24_8:                               # %_ZN9NCompress4NLzx10NBitStream8CDecoderD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB24_26:
.Ltmp102:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB24_25:
.Ltmp84:
	movq	%rax, %r14
	jmp	.LBB24_18
.LBB24_15:
.Ltmp67:
	movq	%rax, %r14
	leaq	72(%rbx), %rdi
.Ltmp68:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp69:
# BB#16:
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_18
# BB#17:
	movq	(%rdi), %rax
.Ltmp74:
	callq	*16(%rax)
.Ltmp75:
	jmp	.LBB24_18
.LBB24_22:
.Ltmp70:
	movq	%rax, %r14
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_31
# BB#23:
	movq	(%rdi), %rax
.Ltmp71:
	callq	*16(%rax)
.Ltmp72:
	jmp	.LBB24_31
.LBB24_24:
.Ltmp73:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_12:
.Ltmp96:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_21
# BB#13:
	movq	(%rdi), %rax
.Ltmp97:
	callq	*16(%rax)
.Ltmp98:
	jmp	.LBB24_21
.LBB24_14:
.Ltmp99:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_9:
.Ltmp78:
	movq	%rax, %r14
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_18
# BB#10:
	movq	(%rdi), %rax
.Ltmp79:
	callq	*16(%rax)
.Ltmp80:
.LBB24_18:                              # %_ZN10COutBufferD2Ev.exit12
	leaq	16(%rbx), %rdi
.Ltmp85:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp86:
# BB#19:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_21
# BB#20:
	movq	(%rdi), %rax
.Ltmp91:
	callq	*16(%rax)
.Ltmp92:
.LBB24_21:                              # %_ZN9NCompress4NLzx10NBitStream8CDecoderD2Ev.exit17
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB24_11:
.Ltmp81:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_30:
.Ltmp93:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB24_27:
.Ltmp87:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_31
# BB#28:
	movq	(%rdi), %rax
.Ltmp88:
	callq	*16(%rax)
.Ltmp89:
.LBB24_31:                              # %.body10
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB24_29:
.Ltmp90:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end24:
	.size	_ZN9NCompress4NLzx8CDecoderD2Ev, .Lfunc_end24-_ZN9NCompress4NLzx8CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\314\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Ltmp65-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin3   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin3   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin3   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin3   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin3  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp68-.Ltmp101        #   Call between .Ltmp101 and .Ltmp68
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin3   #     jumps to .Ltmp70
	.byte	1                       #   On action: 1
	.long	.Ltmp74-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp93-.Lfunc_begin3   #     jumps to .Ltmp93
	.byte	1                       #   On action: 1
	.long	.Ltmp71-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin3   #     jumps to .Ltmp73
	.byte	1                       #   On action: 1
	.long	.Ltmp97-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin3   #     jumps to .Ltmp99
	.byte	1                       #   On action: 1
	.long	.Ltmp79-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin3   #     jumps to .Ltmp81
	.byte	1                       #   On action: 1
	.long	.Ltmp85-.Lfunc_begin3   # >> Call Site 12 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin3   #     jumps to .Ltmp87
	.byte	1                       #   On action: 1
	.long	.Ltmp91-.Lfunc_begin3   # >> Call Site 13 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin3   #     jumps to .Ltmp93
	.byte	1                       #   On action: 1
	.long	.Ltmp92-.Lfunc_begin3   # >> Call Site 14 <<
	.long	.Ltmp88-.Ltmp92         #   Call between .Ltmp92 and .Ltmp88
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin3   # >> Call Site 15 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin3   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress4NLzx8CDecoderD0Ev,"axG",@progbits,_ZN9NCompress4NLzx8CDecoderD0Ev,comdat
	.weak	_ZN9NCompress4NLzx8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx8CDecoderD0Ev,@function
_ZN9NCompress4NLzx8CDecoderD0Ev:        # @_ZN9NCompress4NLzx8CDecoderD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi166:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi168:
	.cfi_def_cfa_offset 32
.Lcfi169:
	.cfi_offset %rbx, -24
.Lcfi170:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp103:
	callq	_ZN9NCompress4NLzx8CDecoderD2Ev
.Ltmp104:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB25_2:
.Ltmp105:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end25:
	.size	_ZN9NCompress4NLzx8CDecoderD0Ev, .Lfunc_end25-_ZN9NCompress4NLzx8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp103-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin4  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Lfunc_end25-.Ltmp104   #   Call between .Ltmp104 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTVN9NCompress4NLzx8CDecoderE,@object # @_ZTVN9NCompress4NLzx8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress4NLzx8CDecoderE
	.p2align	3
_ZTVN9NCompress4NLzx8CDecoderE:
	.quad	0
	.quad	_ZTIN9NCompress4NLzx8CDecoderE
	.quad	_ZN9NCompress4NLzx8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress4NLzx8CDecoder6AddRefEv
	.quad	_ZN9NCompress4NLzx8CDecoder7ReleaseEv
	.quad	_ZN9NCompress4NLzx8CDecoderD2Ev
	.quad	_ZN9NCompress4NLzx8CDecoderD0Ev
	.quad	_ZN9NCompress4NLzx8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.quad	_ZN9NCompress4NLzx8CDecoder5FlushEv
	.quad	_ZN9NCompress4NLzx8CDecoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN9NCompress4NLzx8CDecoder15ReleaseInStreamEv
	.quad	_ZN9NCompress4NLzx8CDecoder16SetOutStreamSizeEPKy
	.size	_ZTVN9NCompress4NLzx8CDecoderE, 96

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24

	.type	_ZTSN9NCompress4NLzx8CDecoderE,@object # @_ZTSN9NCompress4NLzx8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN9NCompress4NLzx8CDecoderE
	.p2align	4
_ZTSN9NCompress4NLzx8CDecoderE:
	.asciz	"N9NCompress4NLzx8CDecoderE"
	.size	_ZTSN9NCompress4NLzx8CDecoderE, 27

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress4NLzx8CDecoderE,@object # @_ZTIN9NCompress4NLzx8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress4NLzx8CDecoderE
	.p2align	4
_ZTIN9NCompress4NLzx8CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress4NLzx8CDecoderE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN9NCompress4NLzx8CDecoderE, 56


	.globl	_ZN9NCompress4NLzx8CDecoderC1Eb
	.type	_ZN9NCompress4NLzx8CDecoderC1Eb,@function
_ZN9NCompress4NLzx8CDecoderC1Eb = _ZN9NCompress4NLzx8CDecoderC2Eb
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
