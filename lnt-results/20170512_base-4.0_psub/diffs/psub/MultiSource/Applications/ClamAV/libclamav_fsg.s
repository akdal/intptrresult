	.text
	.file	"libclamav_fsg.bc"
	.globl	unfsg_200
	.p2align	4, 0x90
	.type	unfsg_200,@function
unfsg_200:                              # @unfsg_200
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 96
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, %r15d
	movl	%ecx, %ebx
	movq	%rsi, %r12
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	cli_unfsg
	movl	$-1, %ebp
	testl	%eax, %eax
	jne	.LBB0_3
# BB#1:
	movl	104(%rsp), %eax
	movl	96(%rsp), %r8d
	movl	$0, 16(%rsp)
	movl	%ebx, 20(%rsp)
	movl	%ebx, 12(%rsp)
	movl	%r15d, 8(%rsp)
	leaq	8(%rsp), %rsi
	movl	$1, %ebp
	movl	$1, %edx
	movl	$0, %r9d
	movq	%r12, %rdi
	movl	%r14d, %ecx
	pushq	%rax
.Lcfi11:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi12:
	.cfi_adjust_cfa_offset 8
	callq	cli_rebuildpe
	addq	$16, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB0_3
# BB#2:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%ebp, %ebp
.LBB0_3:
	movl	%ebp, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	unfsg_200, .Lfunc_end0-unfsg_200
	.cfi_endproc

	.globl	unfsg_133
	.p2align	4, 0x90
	.type	unfsg_133,@function
unfsg_133:                              # @unfsg_133
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 128
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%r8, %r12
	movl	%ecx, %ebp
	movl	%edx, %r15d
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rbx, 32(%rsp)
	movq	%r13, 24(%rsp)
	testl	%r14d, %r14d
	movq	%r12, 16(%rsp)          # 8-byte Spill
	js	.LBB1_1
# BB#2:                                 # %.lr.ph131
	leaq	32(%rsp), %r8
	leaq	24(%rsp), %r9
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%r15d, %edx
	movl	%ebp, %ecx
	callq	cli_unfsg
	movl	$-1, %ecx
	cmpl	$-1, %eax
	je	.LBB1_24
# BB#3:                                 # %.lr.ph160.preheader
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ebp, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movslq	%r14d, %r15
	leaq	12(%r12), %r14
	xorl	%eax, %eax
	movq	$-1, %r12
	movq	%r13, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph160
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rcx
	movl	%eax, -4(%r14)
	movq	24(%rsp), %rbx
	movq	%rbx, %r13
	subq	%rcx, %r13
	movl	%r13d, (%r14)
	incq	%r12
	cmpq	%r15, %r12
	jge	.LBB1_5
# BB#6:                                 # %._crit_edge142
                                        #   in Loop: Header=BB1_4 Depth=1
	addq	$36, %r14
	addl	%eax, %r13d
	movq	32(%rsp), %rdi
	movl	%edi, %eax
	subl	64(%rsp), %eax          # 4-byte Folded Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%ebx, %eax
	subl	8(%rsp), %eax           # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%eax, %ecx
	movq	%rbx, %rsi
	leaq	32(%rsp), %r8
	leaq	24(%rsp), %r9
	callq	cli_unfsg
	cmpl	$-1, %eax
	movl	%r13d, %eax
	jne	.LBB1_4
# BB#7:
	movl	$-1, %ecx
	jmp	.LBB1_24
.LBB1_1:                                # %..preheader117.preheader_crit_edge
	movslq	%r14d, %r15
	jmp	.LBB1_9
.LBB1_5:
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_9:                                # %.preheader117
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_15 Depth 2
                                        #       Child Loop BB1_12 Depth 3
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.LBB1_15
	.p2align	4, 0x90
.LBB1_14:                               #   in Loop: Header=BB1_15 Depth=2
	movl	-28(%rdx), %ecx
	movl	-24(%rdx), %ebx
	movl	%edi, -36(%rdx)
	movl	12(%rdx), %edi
	movl	%edi, -24(%rdx)
	movl	8(%rdx), %edi
	movl	%edi, -28(%rdx)
	movl	%esi, (%rdx)
	movl	%ecx, 8(%rdx)
	movl	%ebx, 12(%rdx)
	incq	%rax
	movl	$1, %ecx
.LBB1_15:                               # %.outer
                                        #   Parent Loop BB1_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_12 Depth 3
	cltq
	leaq	(%rax,%rax,8), %rdx
	leaq	(%r12,%rdx,4), %rdx
	decq	%rax
	.p2align	4, 0x90
.LBB1_12:                               #   Parent Loop BB1_9 Depth=1
                                        #     Parent Loop BB1_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%rax
	cmpq	%r15, %rax
	jge	.LBB1_8
# BB#13:                                #   in Loop: Header=BB1_12 Depth=3
	movl	(%rdx), %esi
	movl	36(%rdx), %edi
	leaq	36(%rdx), %rdx
	cmpl	%edi, %esi
	jbe	.LBB1_12
	jmp	.LBB1_14
	.p2align	4, 0x90
.LBB1_8:                                # %.loopexit
                                        #   in Loop: Header=BB1_9 Depth=1
	testl	%ecx, %ecx
	jne	.LBB1_9
# BB#10:                                # %.preheader
	testl	%r14d, %r14d
	js	.LBB1_11
# BB#16:                                # %.lr.ph.preheader
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movl	%r14d, %r15d
	incl	%r14d
	leaq	(%r15,%r15,8), %rax
	leaq	(%r12,%rax,4), %r13
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r15, %rsi
	jne	.LBB1_18
# BB#19:                                #   in Loop: Header=BB1_17 Depth=1
	movl	(%r13), %edx
	leaq	1(%rsi), %r12
	movl	%ebp, %ebx
	movq	16(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_17 Depth=1
	leaq	1(%rsi), %r12
	leaq	(%rsi,%rsi,8), %rax
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	36(%rdi,%rax,4), %ecx
	movl	(%rdi,%rax,4), %edx
	addl	%edx, %ebp
	movl	%ebp, %ebx
	subl	%ecx, %ebx
	subl	%edx, %ecx
	movl	%ecx, %ebp
.LBB1_20:                               #   in Loop: Header=BB1_17 Depth=1
	leaq	(%rsi,%rsi,8), %rax
	movl	%ebp, 4(%rdi,%rax,4)
	movl	8(%rdi,%rax,4), %r8d
	movl	12(%rdi,%rax,4), %r9d
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%ebp, %ecx
	callq	cli_dbgmsg
	cmpq	%r14, %r12
	movq	%r12, %rsi
	movl	%ebx, %ebp
	jne	.LBB1_17
# BB#21:
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB1_22
.LBB1_11:                               # %.preheader.._crit_edge_crit_edge
	incl	%r14d
.LBB1_22:                               # %._crit_edge
	movl	144(%rsp), %eax
	movl	128(%rsp), %ecx
	movl	$0, %r9d
	movq	%r13, %rdi
	movq	%r12, %rsi
	movl	%r14d, %edx
	movl	136(%rsp), %r8d
	pushq	%rax
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	callq	cli_rebuildpe
	addq	$16, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset -16
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB1_24
# BB#23:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%ecx, %ecx
.LBB1_24:                               # %.loopexit119
	movl	%ecx, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	unfsg_133, .Lfunc_end1-unfsg_133
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"FSG: Rebuilding failed\n"
	.size	.L.str, 24

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"FSG: .SECT%d RVA:%x VSize:%x ROffset: %x, RSize:%x\n"
	.size	.L.str.1, 52


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
