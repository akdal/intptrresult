	.text
	.file	"genmove.bc"
	.globl	genmove
	.p2align	4, 0x90
	.type	genmove,@function
genmove:                                # @genmove
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 64
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$-1, (%r14)
	movl	$-1, (%rbx)
	movl	umove(%rip), %edi
	callq	eval
	leaq	20(%rsp), %rdi
	leaq	16(%rsp), %rsi
	leaq	12(%rsp), %rdx
	callq	findwinner
	movl	$-1, %ebp
	testl	%eax, %eax
	je	.LBB0_3
# BB#1:
	movl	12(%rsp), %eax
	testl	%eax, %eax
	js	.LBB0_3
# BB#2:
	movl	20(%rsp), %ecx
	movl	%ecx, (%r14)
	movl	16(%rsp), %ecx
	movl	%ecx, (%rbx)
	movl	%eax, %ebp
.LBB0_3:
	leaq	20(%rsp), %rdi
	leaq	16(%rsp), %rsi
	leaq	12(%rsp), %rdx
	callq	findsaver
	testl	%eax, %eax
	je	.LBB0_6
# BB#4:
	movl	12(%rsp), %eax
	cmpl	%ebp, %eax
	jle	.LBB0_6
# BB#5:
	movl	20(%rsp), %ecx
	movl	%ecx, (%r14)
	movl	16(%rsp), %ecx
	movl	%ecx, (%rbx)
	movl	%eax, %ebp
.LBB0_6:
	leaq	20(%rsp), %rdi
	leaq	16(%rsp), %rsi
	leaq	12(%rsp), %rdx
	callq	findpatn
	testl	%eax, %eax
	je	.LBB0_9
# BB#7:
	movl	12(%rsp), %eax
	cmpl	%ebp, %eax
	jle	.LBB0_9
# BB#8:
	movl	20(%rsp), %ecx
	movl	%ecx, (%r14)
	movl	16(%rsp), %ecx
	movl	%ecx, (%rbx)
	movl	%eax, %ebp
.LBB0_9:
	testl	%ebp, %ebp
	js	.LBB0_10
.LBB0_25:                               # %.critedge.thread60
	movl	$0, pass(%rip)
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	(%rbx), %eax
	xorl	%ecx, %ecx
	cmpl	$7, %eax
	setg	%cl
	leal	65(%rcx,%rax), %eax
	movsbl	%al, %edi
	callq	putchar
	movl	$19, %esi
	subl	(%r14), %esi
	cmpl	$9, %esi
	jg	.LBB0_27
# BB#26:
	movl	$.L.str.3, %edi
	jmp	.LBB0_28
.LBB0_10:                               # %.critedge2.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_11:                               # %.critedge2
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %r15d
	movl	$rd, %edi
	callq	random_nasko
	movslq	rd(%rip), %rax
	imulq	$1808407283, %rax, %rcx # imm = 0x6BCA1AF3
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	imull	$19, %ecx, %ecx
	subl	%ecx, %eax
	movl	%eax, (%r14)
	leal	-2(%rax), %ecx
	cmpl	$14, %ecx
	ja	.LBB0_13
# BB#12:                                # %.critedge2
                                        #   in Loop: Header=BB0_11 Depth=1
	addl	$-6, %eax
	cmpl	$6, %eax
	ja	.LBB0_15
.LBB0_13:                               #   in Loop: Header=BB0_11 Depth=1
	movl	$rd, %edi
	callq	random_nasko
	movslq	rd(%rip), %rax
	imulq	$1808407283, %rax, %rcx # imm = 0x6BCA1AF3
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	imull	$19, %ecx, %ecx
	subl	%ecx, %eax
	movl	%eax, (%r14)
	addl	$-2, %eax
	cmpl	$15, %eax
	jb	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_11 Depth=1
	movl	$rd, %edi
	callq	random_nasko
	movslq	rd(%rip), %rax
	imulq	$1808407283, %rax, %rcx # imm = 0x6BCA1AF3
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	imull	$19, %ecx, %ecx
	subl	%ecx, %eax
	movl	%eax, (%r14)
.LBB0_15:                               #   in Loop: Header=BB0_11 Depth=1
	movl	$rd, %edi
	callq	random_nasko
	movslq	rd(%rip), %rax
	imulq	$1808407283, %rax, %rcx # imm = 0x6BCA1AF3
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	imull	$19, %ecx, %ecx
	subl	%ecx, %eax
	movl	%eax, (%rbx)
	leal	-2(%rax), %ecx
	cmpl	$14, %ecx
	ja	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_11 Depth=1
	addl	$-6, %eax
	cmpl	$6, %eax
	ja	.LBB0_19
.LBB0_17:                               #   in Loop: Header=BB0_11 Depth=1
	movl	$rd, %edi
	callq	random_nasko
	movslq	rd(%rip), %rax
	imulq	$1808407283, %rax, %rcx # imm = 0x6BCA1AF3
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	imull	$19, %ecx, %ecx
	subl	%ecx, %eax
	movl	%eax, (%rbx)
	addl	$-2, %eax
	cmpl	$15, %eax
	jb	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_11 Depth=1
	movl	$rd, %edi
	callq	random_nasko
	movslq	rd(%rip), %rax
	imulq	$1808407283, %rax, %rcx # imm = 0x6BCA1AF3
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	imull	$19, %ecx, %ecx
	subl	%ecx, %eax
	movl	%eax, (%rbx)
.LBB0_19:                               #   in Loop: Header=BB0_11 Depth=1
	movl	$0, lib(%rip)
	movl	(%r14), %edi
	movl	(%rbx), %esi
	movl	mymove(%rip), %edx
	callq	countlib
	leal	1(%r15), %ebp
	cmpl	$399, %ebp              # imm = 0x18F
	jg	.LBB0_24
# BB#20:                                #   in Loop: Header=BB0_11 Depth=1
	movslq	(%r14), %rdi
	movslq	(%rbx), %rsi
	imulq	$19, %rdi, %rax
	cmpb	$0, p(%rax,%rsi)
	jne	.LBB0_11
# BB#21:                                #   in Loop: Header=BB0_11 Depth=1
	cmpl	$2, lib(%rip)
	jl	.LBB0_11
# BB#22:                                #   in Loop: Header=BB0_11 Depth=1
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	fioe
	testl	%eax, %eax
	jne	.LBB0_11
# BB#23:                                # %.critedge
	cmpl	$399, %r15d             # imm = 0x18F
	jl	.LBB0_25
.LBB0_24:                               # %.critedge.thread
	incl	pass(%rip)
	movl	$.Lstr, %edi
	callq	puts
	movl	$-1, (%r14)
	jmp	.LBB0_29
.LBB0_27:
	movl	$.L.str.4, %edi
.LBB0_28:
	xorl	%eax, %eax
	callq	printf
.LBB0_29:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	genmove, .Lfunc_end0-genmove
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"my move: "
	.size	.L.str.1, 10

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%1d\n"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%2d\n"
	.size	.L.str.4, 5

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"I pass."
	.size	.Lstr, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
