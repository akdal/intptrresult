	.text
	.file	"CommandLineParser.bc"
	.globl	_ZN18NCommandLineParser7CParserC2Ei
	.p2align	4, 0x90
	.type	_ZN18NCommandLineParser7CParserC2Ei,@function
_ZN18NCommandLineParser7CParserC2Ei:    # @_ZN18NCommandLineParser7CParserC2Ei
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi2:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
.Lcfi3:
	.cfi_offset %rbx, -40
.Lcfi4:
	.cfi_offset %r14, -32
.Lcfi5:
	.cfi_offset %r15, -24
	movq	%rdi, %r14
	movslq	%esi, %rbx
	movl	$48, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	pushfq
	popq	%rcx
	addq	$8, %rax
	movq	$-1, %rdi
	cmovbq	%rdi, %rax
	movl	%ebx, (%r14)
	pushq	%rcx
	popfq
	cmovnoq	%rax, %rdi
	leaq	16(%r14), %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%r14)
	movq	$8, 40(%r14)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 16(%r14)
.Ltmp0:
	callq	_Znam
.Ltmp1:
# BB#1:
	movq	%rbx, (%rax)
	addq	$8, %rax
	testl	%ebx, %ebx
	je	.LBB0_4
# BB#2:
	leaq	(%rbx,%rbx,2), %rcx
	shlq	$4, %rcx
	addq	%rax, %rcx
	xorps	%xmm0, %xmm0
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movb	$0, (%rdx)
	movups	%xmm0, 16(%rdx)
	movq	$8, 32(%rdx)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 8(%rdx)
	addq	$48, %rdx
	cmpq	%rcx, %rdx
	jne	.LBB0_3
.LBB0_4:                                # %.loopexit
	movq	%rax, 8(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:
.Ltmp2:
	movq	%rax, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%r15)
.Ltmp3:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp4:
# BB#6:
.Ltmp9:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp10:
# BB#7:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_10:                               # %.body4
.Ltmp11:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_8:
.Ltmp5:
	movq	%rax, %rbx
.Ltmp6:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp7:
# BB#11:                                # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_9:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN18NCommandLineParser7CParserC2Ei, .Lfunc_end0-_ZN18NCommandLineParser7CParserC2Ei
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp6-.Ltmp10          #   Call between .Ltmp10 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp12:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp13:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB2_2:
.Ltmp14:
	movq	%rax, %r14
.Ltmp15:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp16:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end2-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN18NCommandLineParser7CParserD2Ev
	.p2align	4, 0x90
	.type	_ZN18NCommandLineParser7CParserD2Ev,@function
_ZN18NCommandLineParser7CParserD2Ev:    # @_ZN18NCommandLineParser7CParserD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.LBB3_7
# BB#1:
	leaq	-8(%r13), %r14
	movq	-8(%r13), %rax
	testq	%rax, %rax
	je	.LBB3_6
# BB#2:                                 # %.preheader10.preheader
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rbp
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader10
                                        # =>This Inner Loop Header: Depth=1
	leaq	-40(%r13,%rbp), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, -40(%r13,%rbp)
.Ltmp18:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp19:
# BB#4:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit.i
                                        #   in Loop: Header=BB3_3 Depth=1
.Ltmp24:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp25:
# BB#5:                                 # %_ZN18NCommandLineParser13CSwitchResultD2Ev.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	addq	$-48, %rbp
	jne	.LBB3_3
.LBB3_6:                                # %.loopexit11
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB3_7:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 16(%r12)
	addq	$16, %r12
.Ltmp45:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp46:
# BB#8:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB3_11:
.Ltmp47:
	movq	%rax, %r15
.Ltmp48:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp49:
	jmp	.LBB3_12
.LBB3_13:
.Ltmp50:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_14:
.Ltmp26:
	movq	%rax, %r15
	cmpq	$48, %rbp
	jne	.LBB3_16
	jmp	.LBB3_19
.LBB3_9:
.Ltmp20:
	movq	%rax, %r15
.Ltmp21:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp22:
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_18:                               # %_ZN18NCommandLineParser13CSwitchResultD2Ev.exit5
	addq	$-48, %rbp
.LBB3_15:                               # %.body
	cmpq	$48, %rbp
	je	.LBB3_19
.LBB3_16:                               # %.preheader
	leaq	-88(%r13,%rbp), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, -88(%r13,%rbp)
.Ltmp27:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp28:
# BB#17:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit.i2
.Ltmp33:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp34:
	jmp	.LBB3_18
.LBB3_19:                               # %.loopexit9
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 16(%r12)
	addq	$16, %r12
.Ltmp36:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp37:
# BB#20:
.Ltmp42:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp43:
.LBB3_12:                               # %unwind_resume
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB3_26:                               # %.loopexit.split-lp
.Ltmp44:
	jmp	.LBB3_27
.LBB3_23:
.Ltmp38:
	movq	%rax, %r14
.Ltmp39:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp40:
	jmp	.LBB3_28
.LBB3_24:
.Ltmp41:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_10:
.Ltmp23:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_25:                               # %.loopexit
.Ltmp35:
.LBB3_27:                               # %.body3
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB3_21:
.Ltmp29:
	movq	%rax, %r14
.Ltmp30:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp31:
.LBB3_28:                               # %.body3
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB3_22:
.Ltmp32:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN18NCommandLineParser7CParserD2Ev, .Lfunc_end3-_ZN18NCommandLineParser7CParserD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\262\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin2   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin2   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp48-.Ltmp46         #   Call between .Ltmp46 and .Ltmp48
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin2   #     jumps to .Ltmp50
	.byte	1                       #   On action: 1
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp27-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin2   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp33-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin2   #     jumps to .Ltmp35
	.byte	1                       #   On action: 1
	.long	.Ltmp36-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin2   #     jumps to .Ltmp38
	.byte	1                       #   On action: 1
	.long	.Ltmp42-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin2   #     jumps to .Ltmp44
	.byte	1                       #   On action: 1
	.long	.Ltmp43-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp39-.Ltmp43         #   Call between .Ltmp43 and .Ltmp39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin2   #     jumps to .Ltmp41
	.byte	1                       #   On action: 1
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 13 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin2   #     jumps to .Ltmp32
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.zero	16
	.text
	.globl	_ZN18NCommandLineParser7CParser12ParseStringsEPKNS_11CSwitchFormERK13CObjectVectorI11CStringBaseIwEE
	.p2align	4, 0x90
	.type	_ZN18NCommandLineParser7CParser12ParseStringsEPKNS_11CSwitchFormERK13CObjectVectorI11CStringBaseIwEE,@function
_ZN18NCommandLineParser7CParser12ParseStringsEPKNS_11CSwitchFormERK13CObjectVectorI11CStringBaseIwEE: # @_ZN18NCommandLineParser7CParser12ParseStringsEPKNS_11CSwitchFormERK13CObjectVectorI11CStringBaseIwEE
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 96
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movslq	12(%rdx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB4_22
# BB#1:                                 # %.lr.ph
	leaq	16(%r13), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB4_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
                                        #     Child Loop BB4_18 Depth 2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%r15,8), %rbx
	testb	$1, 8(%rsp)             # 1-byte Folded Reload
	jne	.LBB4_3
# BB#10:                                #   in Loop: Header=BB4_2 Depth=1
	movq	(%rbx), %rdi
	movl	$.L.str.3, %esi
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	je	.LBB4_11
# BB#12:                                #   in Loop: Header=BB4_2 Depth=1
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	_ZN18NCommandLineParser7CParser11ParseStringERK11CStringBaseIwEPKNS_11CSwitchFormE
	testb	%al, %al
	jne	.LBB4_21
# BB#13:                                #   in Loop: Header=BB4_2 Depth=1
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movslq	8(%rbx), %r12
	leaq	1(%r12), %rbp
	testl	%ebp, %ebp
	je	.LBB4_14
# BB#15:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp54:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rcx
.Ltmp55:
# BB#16:                                # %.noexc.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%rcx, (%r14)
	movl	$0, (%rcx)
	movl	%ebp, 12(%r14)
	jmp	.LBB4_17
	.p2align	4, 0x90
.LBB4_3:                                #   in Loop: Header=BB4_2 Depth=1
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movslq	8(%rbx), %r12
	leaq	1(%r12), %r14
	testl	%r14d, %r14d
	je	.LBB4_4
# BB#5:                                 # %._crit_edge16.i.i.i18
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp51:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rcx
.Ltmp52:
# BB#6:                                 # %.noexc.i19
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%rcx, (%rbp)
	movl	$0, (%rcx)
	movl	%r14d, 12(%rbp)
	jmp	.LBB4_7
.LBB4_11:                               #   in Loop: Header=BB4_2 Depth=1
	movb	$1, %al
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB4_21
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	xorl	%ecx, %ecx
.LBB4_7:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i20
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%rbx), %rsi
	.p2align	4, 0x90
.LBB4_8:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB4_8
# BB#9:                                 # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit23
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	%r12d, 8(%rbp)
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	32(%r13), %rax
	movslq	28(%r13), %rcx
	movq	%rbp, (%rax,%rcx,8)
	jmp	.LBB4_20
.LBB4_14:                               #   in Loop: Header=BB4_2 Depth=1
	xorl	%ecx, %ecx
.LBB4_17:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%rbx), %rsi
	.p2align	4, 0x90
.LBB4_18:                               #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB4_18
# BB#19:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	%r12d, 8(%r14)
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	32(%r13), %rax
	movslq	28(%r13), %rcx
	movq	%r14, (%rax,%rcx,8)
.LBB4_20:                               #   in Loop: Header=BB4_2 Depth=1
	leal	1(%rcx), %eax
	movl	%eax, 28(%r13)
.LBB4_21:                               #   in Loop: Header=BB4_2 Depth=1
	incq	%r15
	cmpq	24(%rsp), %r15          # 8-byte Folded Reload
	jl	.LBB4_2
.LBB4_22:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_25:
.Ltmp56:
	movq	%rax, %rbx
	movq	%r14, %rdi
	jmp	.LBB4_24
.LBB4_23:
.Ltmp53:
	movq	%rax, %rbx
	movq	%rbp, %rdi
.LBB4_24:                               # %unwind_resume
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN18NCommandLineParser7CParser12ParseStringsEPKNS_11CSwitchFormERK13CObjectVectorI11CStringBaseIwEE, .Lfunc_end4-_ZN18NCommandLineParser7CParser12ParseStringsEPKNS_11CSwitchFormERK13CObjectVectorI11CStringBaseIwEE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp54-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin3   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp51-.Ltmp55         #   Call between .Ltmp55 and .Ltmp51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin3   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp52     #   Call between .Ltmp52 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.zero	16
	.text
	.globl	_ZN18NCommandLineParser7CParser11ParseStringERK11CStringBaseIwEPKNS_11CSwitchFormE
	.p2align	4, 0x90
	.type	_ZN18NCommandLineParser7CParser11ParseStringERK11CStringBaseIwEPKNS_11CSwitchFormE,@function
_ZN18NCommandLineParser7CParser11ParseStringERK11CStringBaseIwEPKNS_11CSwitchFormE: # @_ZN18NCommandLineParser7CParser11ParseStringERK11CStringBaseIwEPKNS_11CSwitchFormE
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 160
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movslq	8(%r13), %rcx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rcx, %rcx
	je	.LBB5_108
# BB#1:
	movq	(%r13), %rax
	cmpl	$45, (%rax)
	jne	.LBB5_108
# BB#2:                                 # %.thread223.preheader
	movb	$1, %al
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB5_120
# BB#3:                                 # %.lr.ph281.lr.ph
	xorl	%ebp, %ebp
	movq	%r13, 72(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_6 Depth 2
                                        #       Child Loop BB5_7 Depth 3
                                        #       Child Loop BB5_11 Depth 3
                                        #       Child Loop BB5_13 Depth 3
                                        #       Child Loop BB5_24 Depth 3
                                        #     Child Loop BB5_40 Depth 2
                                        #     Child Loop BB5_42 Depth 2
                                        #     Child Loop BB5_46 Depth 2
                                        #     Child Loop BB5_66 Depth 2
                                        #       Child Loop BB5_60 Depth 3
                                        #       Child Loop BB5_64 Depth 3
                                        #       Child Loop BB5_81 Depth 3
                                        #       Child Loop BB5_84 Depth 3
                                        #     Child Loop BB5_96 Depth 2
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	cmpl	$45, (%rax,%rcx,4)
	sete	%cl
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.LBB5_121
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_4 Depth=1
	xorl	%esi, %esi
	movb	%cl, %sil
	addl	%ebp, %esi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movslq	%esi, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	$-1, %ebp
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_6:                                #   Parent Loop BB5_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_7 Depth 3
                                        #       Child Loop BB5_11 Depth 3
                                        #       Child Loop BB5_13 Depth 3
                                        #       Child Loop BB5_24 Depth 3
	movq	%r12, %rcx
	shlq	$5, %rcx
	leaq	(%rdx,%rcx), %rsi
	movq	(%rdx,%rcx), %rcx
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB5_7:                                #   Parent Loop BB5_4 Depth=1
                                        #     Parent Loop BB5_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%r15d
	cmpl	$0, (%rcx)
	leaq	4(%rcx), %rcx
	jne	.LBB5_7
# BB#8:                                 # %_Z11MyStringLenIwEiPKT_.exit
                                        #   in Loop: Header=BB5_6 Depth=2
	cmpl	%ebp, %r15d
	jle	.LBB5_31
# BB#9:                                 # %_Z11MyStringLenIwEiPKT_.exit
                                        #   in Loop: Header=BB5_6 Depth=2
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%r15), %ecx
	cmpl	16(%rsp), %ecx          # 4-byte Folded Reload
	jg	.LBB5_31
# BB#10:                                #   in Loop: Header=BB5_6 Depth=2
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movl	%ebx, 56(%rsp)          # 4-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	(%r13), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,4), %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB5_11:                               #   Parent Loop BB5_4 Depth=1
                                        #     Parent Loop BB5_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB5_11
# BB#12:                                # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB5_6 Depth=2
	leal	1(%rbp), %eax
	movslq	%eax, %r14
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	%r14d, 12(%rsp)
	.p2align	4, 0x90
.LBB5_13:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB5_4 Depth=1
                                        #     Parent Loop BB5_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_13
# BB#14:                                # %_ZN11CStringBaseIwEC2EPKw.exit
                                        #   in Loop: Header=BB5_6 Depth=2
	movl	%ebp, 8(%rsp)
.Ltmp57:
	xorl	%edx, %edx
	leaq	88(%rsp), %rdi
	movq	%rsp, %rsi
	movl	%r15d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp58:
# BB#15:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
                                        #   in Loop: Header=BB5_6 Depth=2
	movl	$0, 8(%rsp)
	movq	(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	96(%rsp), %r14
	incq	%r14
	movl	12(%rsp), %ebp
	cmpl	%ebp, %r14d
	jne	.LBB5_17
# BB#16:                                #   in Loop: Header=BB5_6 Depth=2
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB5_23
	.p2align	4, 0x90
.LBB5_17:                               #   in Loop: Header=BB5_6 Depth=2
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp60:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp61:
# BB#18:                                # %.noexc186
                                        #   in Loop: Header=BB5_6 Depth=2
	testq	%rbx, %rbx
	je	.LBB5_21
# BB#19:                                # %.noexc186
                                        #   in Loop: Header=BB5_6 Depth=2
	testl	%ebp, %ebp
	movl	$0, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	jle	.LBB5_22
# BB#20:                                # %._crit_edge.thread.i.i181
                                        #   in Loop: Header=BB5_6 Depth=2
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	jmp	.LBB5_22
.LBB5_21:                               #   in Loop: Header=BB5_6 Depth=2
	xorl	%eax, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB5_22:                               # %._crit_edge16.i.i182
                                        #   in Loop: Header=BB5_6 Depth=2
	movq	%r13, (%rsp)
	movl	$0, (%r13,%rax,4)
	movl	%r14d, 12(%rsp)
	movq	%r13, %rbx
	movq	72(%rsp), %r13          # 8-byte Reload
.LBB5_23:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i183
                                        #   in Loop: Header=BB5_6 Depth=2
	movq	88(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_24:                               #   Parent Loop BB5_4 Depth=1
                                        #     Parent Loop BB5_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_24
# BB#25:                                #   in Loop: Header=BB5_6 Depth=2
	movl	96(%rsp), %eax
	movl	%eax, 8(%rsp)
	testq	%rdi, %rdi
	je	.LBB5_27
# BB#26:                                #   in Loop: Header=BB5_6 Depth=2
	callq	_ZdaPv
	movq	(%rsp), %rbx
.LBB5_27:                               # %_ZN11CStringBaseIwED2Ev.exit187
                                        #   in Loop: Header=BB5_6 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
.Ltmp63:
	movq	%rbx, %rdi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp64:
# BB#28:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit
                                        #   in Loop: Header=BB5_6 Depth=2
	testl	%eax, %eax
	cmovel	%r15d, %ebp
	movl	56(%rsp), %ebx          # 4-byte Reload
	cmovel	%r12d, %ebx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_30
# BB#29:                                #   in Loop: Header=BB5_6 Depth=2
	callq	_ZdaPv
.LBB5_30:                               # %_ZN11CStringBaseIwED2Ev.exit189
                                        #   in Loop: Header=BB5_6 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB5_31:                               #   in Loop: Header=BB5_6 Depth=2
	incq	%r12
	movslq	%eax, %rcx
	cmpq	%rcx, %r12
	jl	.LBB5_6
# BB#32:                                # %._crit_edge
                                        #   in Loop: Header=BB5_4 Depth=1
	cmpl	$-1, %ebp
	je	.LBB5_121
# BB#33:                                #   in Loop: Header=BB5_4 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r15
	movslq	%ebx, %rax
	leaq	(%rax,%rax,2), %r12
	shlq	$5, %rax
	shlq	$4, %r12
	leaq	(%r15,%r12), %rcx
	cmpb	$0, 12(%rdx,%rax)
	jne	.LBB5_35
# BB#34:                                #   in Loop: Header=BB5_4 Depth=1
	cmpb	$0, (%rcx)
	jne	.LBB5_123
.LBB5_35:                               # %._crit_edge318
                                        #   in Loop: Header=BB5_4 Depth=1
	movb	$1, (%rcx)
	addl	80(%rsp), %ebp          # 4-byte Folded Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %ebx
	subl	%ebp, %ebx
	movl	8(%rdx,%rax), %ecx
	leal	-2(%rcx), %esi
	cmpl	$2, %esi
	jb	.LBB5_50
# BB#36:                                # %._crit_edge318
                                        #   in Loop: Header=BB5_4 Depth=1
	cmpl	$1, %ecx
	je	.LBB5_55
# BB#37:                                # %._crit_edge318
                                        #   in Loop: Header=BB5_4 Depth=1
	cmpl	$4, %ecx
	jne	.LBB5_101
# BB#38:                                #   in Loop: Header=BB5_4 Depth=1
	movq	%rbp, %r14
	cmpl	16(%rdx,%rax), %ebx
	jl	.LBB5_122
# BB#39:                                #   in Loop: Header=BB5_4 Depth=1
	movq	24(%rdx,%rax), %rbp
	movq	%rbp, %rcx
	xorl	%eax, %eax
	movabsq	$4294967296, %rdx       # imm = 0x100000000
	.p2align	4, 0x90
.LBB5_40:                               #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rdx, %rax
	cmpl	$0, (%rcx)
	leaq	4(%rcx), %rcx
	jne	.LBB5_40
# BB#41:                                # %_Z11MyStringLenIwEiPKT_.exit.i194
                                        #   in Loop: Header=BB5_4 Depth=1
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
	movq	%rax, %rdi
	callq	_Znam
	movl	$0, (%rax)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_42:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i197
                                        #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB5_42
# BB#43:                                # %_ZN11CStringBaseIwEC2EPKw.exit198
                                        #   in Loop: Header=BB5_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB5_102
# BB#44:                                #   in Loop: Header=BB5_4 Depth=1
	movq	(%r13), %rcx
	movq	%r14, %rbp
	movslq	%ebp, %rdx
	movl	(%rcx,%rdx,4), %edx
	movl	(%rax), %esi
	cmpl	%edx, %esi
	movq	%rax, %rcx
	je	.LBB5_48
# BB#45:                                # %.lr.ph.i.i200.preheader
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB5_46:                               # %.lr.ph.i.i200
                                        #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%esi, %esi
	je	.LBB5_104
# BB#47:                                #   in Loop: Header=BB5_46 Depth=2
	movl	4(%rcx), %esi
	addq	$4, %rcx
	cmpl	%edx, %esi
	jne	.LBB5_46
.LBB5_48:                               # %_ZNK11CStringBaseIwE4FindEw.exit
                                        #   in Loop: Header=BB5_4 Depth=1
	subq	%rax, %rcx
	shrq	$2, %rcx
	leaq	40(%r15,%r12), %rdx
	testl	%ecx, %ecx
	js	.LBB5_105
# BB#49:                                #   in Loop: Header=BB5_4 Depth=1
	movl	%ecx, (%rdx)
	incl	%ebp
	jmp	.LBB5_106
	.p2align	4, 0x90
.LBB5_50:                               #   in Loop: Header=BB5_4 Depth=1
	movl	16(%rdx,%rax), %r14d
	cmpl	%r14d, %ebx
	jl	.LBB5_122
# BB#51:                                #   in Loop: Header=BB5_4 Depth=1
	cmpl	$3, %ecx
	je	.LBB5_109
# BB#52:                                #   in Loop: Header=BB5_4 Depth=1
	movl	20(%rdx,%rax), %ebx
	movq	%rsp, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	movl	%r14d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
	addl	%r14d, %ebp
	cmpl	16(%rsp), %ebp          # 4-byte Folded Reload
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movq	%r12, 40(%rsp)          # 8-byte Spill
	jge	.LBB5_90
# BB#53:                                #   in Loop: Header=BB5_4 Depth=1
	cmpl	%ebx, %r14d
	movl	%ebx, %edi
	jge	.LBB5_90
# BB#54:                                # %.lr.ph276.preheader
                                        #   in Loop: Header=BB5_4 Depth=1
	movslq	%ebp, %rbp
	movl	%edi, 64(%rsp)          # 4-byte Spill
	jmp	.LBB5_66
	.p2align	4, 0x90
.LBB5_55:                               #   in Loop: Header=BB5_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB5_57
# BB#56:                                #   in Loop: Header=BB5_4 Depth=1
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	xorl	%esi, %esi
	cmpl	$45, (%rax,%rcx,4)
	sete	%sil
	sete	1(%r15,%r12)
	addl	%ebp, %esi
	movl	%esi, %ebp
	jmp	.LBB5_101
.LBB5_57:                               #   in Loop: Header=BB5_4 Depth=1
	movb	$0, 1(%r15,%r12)
	jmp	.LBB5_101
.LBB5_58:                               # %vector.body.preheader
                                        #   in Loop: Header=BB5_66 Depth=2
	movq	%rbp, %r8
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_61
# BB#59:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB5_66 Depth=2
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_60:                               # %vector.body.prol
                                        #   Parent Loop BB5_4 Depth=1
                                        #     Parent Loop BB5_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r13,%rbp,4)
	movups	%xmm1, 16(%r13,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB5_60
	jmp	.LBB5_62
.LBB5_61:                               #   in Loop: Header=BB5_66 Depth=2
	xorl	%ebp, %ebp
.LBB5_62:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB5_66 Depth=2
	cmpq	$24, %rdx
	jb	.LBB5_65
# BB#63:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB5_66 Depth=2
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r13,%rbp,4), %rsi
	leaq	112(%rdi,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB5_64:                               # %vector.body
                                        #   Parent Loop BB5_4 Depth=1
                                        #     Parent Loop BB5_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-32, %rdx
	jne	.LBB5_64
.LBB5_65:                               # %middle.block
                                        #   in Loop: Header=BB5_66 Depth=2
	cmpq	%rcx, %rax
	movq	%r8, %rbp
	je	.LBB5_86
	jmp	.LBB5_79
	.p2align	4, 0x90
.LBB5_66:                               # %.lr.ph276
                                        #   Parent Loop BB5_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_60 Depth 3
                                        #       Child Loop BB5_64 Depth 3
                                        #       Child Loop BB5_81 Depth 3
                                        #       Child Loop BB5_84 Depth 3
	movq	(%r13), %rax
	movl	(%rax,%rbp,4), %r12d
	cmpl	$45, %r12d
	je	.LBB5_90
# BB#67:                                #   in Loop: Header=BB5_66 Depth=2
	movl	8(%rsp), %ebx
	movl	12(%rsp), %r15d
	movl	%r15d, %eax
	subl	%ebx, %eax
	cmpl	$1, %eax
	jg	.LBB5_88
# BB#68:                                #   in Loop: Header=BB5_66 Depth=2
	leal	-1(%rax), %ecx
	cmpl	$8, %r15d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r15d
	jl	.LBB5_70
# BB#69:                                # %select.true.sink
                                        #   in Loop: Header=BB5_66 Depth=2
	movl	%r15d, %edx
	shrl	$31, %edx
	addl	%r15d, %edx
	sarl	%edx
.LBB5_70:                               # %select.end
                                        #   in Loop: Header=BB5_66 Depth=2
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r15,%rsi), %eax
	cmpl	%r15d, %eax
	je	.LBB5_88
# BB#71:                                #   in Loop: Header=BB5_66 Depth=2
	movl	%eax, 24(%rsp)          # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp66:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp67:
# BB#72:                                # %.noexc176
                                        #   in Loop: Header=BB5_66 Depth=2
	testl	%r15d, %r15d
	jle	.LBB5_87
# BB#73:                                # %.preheader.i.i
                                        #   in Loop: Header=BB5_66 Depth=2
	movq	(%rsp), %rdi
	testl	%ebx, %ebx
	jle	.LBB5_85
# BB#74:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB5_66 Depth=2
	movslq	%ebx, %rax
	cmpl	$7, %ebx
	jbe	.LBB5_78
# BB#75:                                # %min.iters.checked
                                        #   in Loop: Header=BB5_66 Depth=2
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB5_78
# BB#76:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_66 Depth=2
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r13
	jae	.LBB5_58
# BB#77:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_66 Depth=2
	leaq	(%r13,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB5_58
.LBB5_78:                               #   in Loop: Header=BB5_66 Depth=2
	xorl	%ecx, %ecx
.LBB5_79:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB5_66 Depth=2
	subl	%ecx, %ebx
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbx
	je	.LBB5_82
# BB#80:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB5_66 Depth=2
	negq	%rbx
	.p2align	4, 0x90
.LBB5_81:                               # %scalar.ph.prol
                                        #   Parent Loop BB5_4 Depth=1
                                        #     Parent Loop BB5_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r13,%rcx,4)
	incq	%rcx
	incq	%rbx
	jne	.LBB5_81
.LBB5_82:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB5_66 Depth=2
	cmpq	$7, %rdx
	jb	.LBB5_86
# BB#83:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB5_66 Depth=2
	subq	%rcx, %rax
	leaq	28(%r13,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB5_84:                               # %scalar.ph
                                        #   Parent Loop BB5_4 Depth=1
                                        #     Parent Loop BB5_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB5_84
	jmp	.LBB5_86
.LBB5_85:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB5_66 Depth=2
	testq	%rdi, %rdi
	je	.LBB5_87
.LBB5_86:                               # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB5_66 Depth=2
	callq	_ZdaPv
	movl	8(%rsp), %ebx
.LBB5_87:                               # %._crit_edge16.i.i
                                        #   in Loop: Header=BB5_66 Depth=2
	movq	%r13, (%rsp)
	movslq	%ebx, %rax
	movl	$0, (%r13,%rax,4)
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, 12(%rsp)
	movq	72(%rsp), %r13          # 8-byte Reload
	movl	64(%rsp), %edi          # 4-byte Reload
.LBB5_88:                               #   in Loop: Header=BB5_66 Depth=2
	movq	(%rsp), %rax
	movslq	%ebx, %rcx
	movl	%r12d, (%rax,%rcx,4)
	leal	1(%rcx), %edx
	movl	%edx, 8(%rsp)
	movl	$0, 4(%rax,%rcx,4)
	incq	%rbp
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	jge	.LBB5_90
# BB#89:                                #   in Loop: Header=BB5_66 Depth=2
	incl	%r14d
	cmpl	%edi, %r14d
	jl	.LBB5_66
	.p2align	4, 0x90
.LBB5_90:                               # %.critedge
                                        #   in Loop: Header=BB5_4 Depth=1
.Ltmp69:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp70:
	movq	56(%rsp), %rbx          # 8-byte Reload
# BB#91:                                # %.noexc
                                        #   in Loop: Header=BB5_4 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movslq	8(%rsp), %r14
	leaq	1(%r14), %r12
	testl	%r12d, %r12d
	je	.LBB5_94
# BB#92:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp71:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp72:
# BB#93:                                # %.noexc.i
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	%rax, (%r15)
	movl	$0, (%rax)
	movl	%r12d, 12(%r15)
	jmp	.LBB5_95
.LBB5_94:                               #   in Loop: Header=BB5_4 Depth=1
	xorl	%eax, %eax
.LBB5_95:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	(%rsp), %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_96:                               #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB5_96
# BB#97:                                #   in Loop: Header=BB5_4 Depth=1
	movl	%r14d, 8(%r15)
	movq	%rsi, %rbp
	leaq	8(%rbx,%rsi), %rdi
.Ltmp74:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp75:
# BB#98:                                #   in Loop: Header=BB5_4 Depth=1
	movq	24(%rbx,%rbp), %rax
	movslq	20(%rbx,%rbp), %rcx
	movq	%r15, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 20(%rbx,%rbp)
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_100
# BB#99:                                #   in Loop: Header=BB5_4 Depth=1
	callq	_ZdaPv
.LBB5_100:                              # %.thread
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB5_101:                              # %.thread223.backedge
                                        #   in Loop: Header=BB5_4 Depth=1
	cmpl	16(%rsp), %ebp          # 4-byte Folded Reload
	jl	.LBB5_4
	jmp	.LBB5_119
.LBB5_102:                              #   in Loop: Header=BB5_4 Depth=1
	movl	$-1, 40(%r15,%r12)
	movq	%r14, %rbp
	jmp	.LBB5_106
.LBB5_104:                              # %_ZNK11CStringBaseIwE4FindEw.exit.thread
                                        #   in Loop: Header=BB5_4 Depth=1
	leaq	40(%r15,%r12), %rdx
.LBB5_105:                              # %_ZN11CStringBaseIwED2Ev.exit202
                                        #   in Loop: Header=BB5_4 Depth=1
	movl	$-1, (%rdx)
.LBB5_106:                              # %_ZN11CStringBaseIwED2Ev.exit202
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	%rax, %rdi
	callq	_ZdaPv
	cmpl	16(%rsp), %ebp          # 4-byte Folded Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	jl	.LBB5_4
	jmp	.LBB5_119
.LBB5_108:
	xorl	%eax, %eax
	jmp	.LBB5_120
.LBB5_109:
	movl	8(%r13), %ecx
	subl	%ebp, %ecx
	movq	%rsp, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp77:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp78:
# BB#110:                               # %.noexc208
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movslq	8(%rsp), %r14
	leaq	1(%r14), %rbx
	testl	%ebx, %ebx
	je	.LBB5_113
# BB#111:                               # %._crit_edge16.i.i.i203
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp79:
	callq	_Znam
.Ltmp80:
# BB#112:                               # %.noexc.i204
	movq	%rax, (%rbp)
	movl	$0, (%rax)
	movl	%ebx, 12(%rbp)
	jmp	.LBB5_114
.LBB5_113:
	xorl	%eax, %eax
.LBB5_114:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i205
	movq	(%rsp), %rcx
	.p2align	4, 0x90
.LBB5_115:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB5_115
# BB#116:
	movl	%r14d, 8(%rbp)
	leaq	8(%r15,%r12), %rdi
.Ltmp82:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp83:
# BB#117:
	movq	24(%r15,%r12), %rax
	movslq	20(%r15,%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 20(%r15,%r12)
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_119
# BB#118:
	callq	_ZdaPv
.LBB5_119:
	movb	$1, %al
.LBB5_120:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_121:                              # %._crit_edge.thread
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	jmp	.LBB5_124
.LBB5_122:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.2, (%rax)
	jmp	.LBB5_124
.LBB5_123:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.1, (%rax)
.LBB5_124:                              # %._crit_edge.thread
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB5_125:
.Ltmp81:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB5_135
.LBB5_126:
.Ltmp84:
	jmp	.LBB5_134
.LBB5_127:
.Ltmp73:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB5_135
.LBB5_128:
.Ltmp68:
	jmp	.LBB5_134
.LBB5_129:
.Ltmp76:
	jmp	.LBB5_134
.LBB5_130:
.Ltmp62:
	movq	%rax, %rbx
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_135
# BB#131:
	callq	_ZdaPv
	jmp	.LBB5_135
.LBB5_132:
.Ltmp65:
	jmp	.LBB5_134
.LBB5_133:
.Ltmp59:
.LBB5_134:                              # %.body
	movq	%rax, %rbx
.LBB5_135:                              # %.body
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_137
# BB#136:
	callq	_ZdaPv
.LBB5_137:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN18NCommandLineParser7CParser11ParseStringERK11CStringBaseIwEPKNS_11CSwitchFormE, .Lfunc_end5-_ZN18NCommandLineParser7CParser11ParseStringERK11CStringBaseIwEPKNS_11CSwitchFormE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\271\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp57-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp57
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin4   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin4   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin4   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp66-.Ltmp64         #   Call between .Ltmp64 and .Ltmp66
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin4   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin4   # >> Call Site 7 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp76-.Lfunc_begin4   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin4   # >> Call Site 8 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin4   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin4   # >> Call Site 9 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin4   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin4   # >> Call Site 10 <<
	.long	.Ltmp77-.Ltmp75         #   Call between .Ltmp75 and .Ltmp77
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin4   # >> Call Site 11 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp84-.Lfunc_begin4   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin4   # >> Call Site 12 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin4   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin4   # >> Call Site 13 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin4   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin4   # >> Call Site 14 <<
	.long	.Lfunc_end5-.Ltmp83     #   Call between .Ltmp83 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 64
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB6_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB6_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB6_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB6_5
.LBB6_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB6_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp85:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp86:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB6_35
# BB#12:
	movq	%rbx, %r13
.LBB6_13:                               # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB6_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB6_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB6_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB6_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB6_15
.LBB6_14:
	xorl	%esi, %esi
.LBB6_15:                               # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB6_16:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB6_16
.LBB6_29:
	movq	%r13, %rbx
.LBB6_30:                               # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp87:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp88:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB6_32:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB6_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB6_8
.LBB6_3:
	xorl	%eax, %eax
.LBB6_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB6_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB6_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB6_30
.LBB6_21:                               # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB6_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_24:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB6_24
	jmp	.LBB6_25
.LBB6_22:
	xorl	%ecx, %ecx
.LBB6_25:                               # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB6_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB6_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB6_27
.LBB6_28:                               # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB6_15
	jmp	.LBB6_29
.LBB6_34:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp89:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end6-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp85-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp85
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp88-.Ltmp85         #   Call between .Ltmp85 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin5   #     jumps to .Ltmp89
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Lfunc_end6-.Ltmp88     #   Call between .Ltmp88 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK18NCommandLineParser7CParserixEm
	.p2align	4, 0x90
	.type	_ZNK18NCommandLineParser7CParserixEm,@function
_ZNK18NCommandLineParser7CParserixEm:   # @_ZNK18NCommandLineParser7CParserixEm
	.cfi_startproc
# BB#0:
	leaq	(%rsi,%rsi,2), %rax
	shlq	$4, %rax
	addq	8(%rdi), %rax
	retq
.Lfunc_end7:
	.size	_ZNK18NCommandLineParser7CParserixEm, .Lfunc_end7-_ZNK18NCommandLineParser7CParserixEm
	.cfi_endproc

	.globl	_ZN18NCommandLineParser12ParseCommandEiPKNS_12CCommandFormERK11CStringBaseIwERS4_
	.p2align	4, 0x90
	.type	_ZN18NCommandLineParser12ParseCommandEiPKNS_12CCommandFormERK11CStringBaseIwERS4_,@function
_ZN18NCommandLineParser12ParseCommandEiPKNS_12CCommandFormERK11CStringBaseIwERS4_: # @_ZN18NCommandLineParser12ParseCommandEiPKNS_12CCommandFormERK11CStringBaseIwERS4_
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi69:
	.cfi_def_cfa_offset 112
.Lcfi70:
	.cfi_offset %rbx, -56
.Lcfi71:
	.cfi_offset %r12, -48
.Lcfi72:
	.cfi_offset %r13, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	testl	%edi, %edi
	jle	.LBB8_22
# BB#1:                                 # %.lr.ph
	movq	%rcx, (%rsp)            # 8-byte Spill
	movslq	%edi, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movabsq	$-4294967296, %r12      # imm = 0xFFFFFFFF00000000
	movq	%r13, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_3 Depth 2
                                        #     Child Loop BB8_5 Depth 2
                                        #     Child Loop BB8_11 Depth 2
                                        #       Child Loop BB8_12 Depth 3
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	shlq	$4, %rbx
	movq	(%r13,%rbx), %r13
	movl	$-1, %ebp
	movq	%r13, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_3:                                #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r12, %rax
	incl	%ebp
	cmpl	$0, (%rcx)
	leaq	4(%rcx), %rcx
	jne	.LBB8_3
# BB#4:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	%r12, %r14
	subq	%rax, %r14
	negq	%rax
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	movl	$0, (%r15)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_5
# BB#6:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	48(%rsp), %r13          # 8-byte Reload
	cmpb	$0, 8(%r13,%rbx)
	je	.LBB8_17
# BB#7:                                 #   in Loop: Header=BB8_2 Depth=1
	testl	%ebp, %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rcx
	je	.LBB8_23
# BB#8:                                 # %.preheader22.i.i
                                        #   in Loop: Header=BB8_2 Depth=1
	testl	%ecx, %ecx
	jle	.LBB8_20
# BB#9:                                 # %.preheader22.i.i
                                        #   in Loop: Header=BB8_2 Depth=1
	testl	%ebp, %ebp
	jle	.LBB8_20
# BB#10:                                # %.preheader.us.preheader.i.i
                                        #   in Loop: Header=BB8_2 Depth=1
	sarq	$32, %r14
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_11:                               # %.preheader.us.i.i
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_12 Depth 3
	movq	%rax, %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB8_12:                               #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%rdx,%rsi), %rbx
	cmpq	%rcx, %rbx
	jge	.LBB8_15
# BB#13:                                #   in Loop: Header=BB8_12 Depth=3
	movl	(%rdi), %ebx
	cmpl	(%r15,%rsi,4), %ebx
	jne	.LBB8_15
# BB#14:                                #   in Loop: Header=BB8_12 Depth=3
	incq	%rsi
	addq	$4, %rdi
	cmpq	%r14, %rsi
	jl	.LBB8_12
.LBB8_15:                               # %.critedge.us.i.i
                                        #   in Loop: Header=BB8_11 Depth=2
	cmpl	%ebp, %esi
	je	.LBB8_19
# BB#16:                                #   in Loop: Header=BB8_11 Depth=2
	incq	%rdx
	addq	$4, %rax
	cmpq	%rcx, %rdx
	jl	.LBB8_11
	jmp	.LBB8_20
	.p2align	4, 0x90
.LBB8_17:                               #   in Loop: Header=BB8_2 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
.Ltmp96:
	movq	%r15, %rsi
	callq	_Z15MyStringComparePKwS0_
.Ltmp97:
# BB#18:                                #   in Loop: Header=BB8_2 Depth=1
	testl	%eax, %eax
	jne	.LBB8_20
	jmp	.LBB8_31
.LBB8_19:                               # %_ZNK11CStringBaseIwE4FindERKS0_.exit
                                        #   in Loop: Header=BB8_2 Depth=1
	testl	%edx, %edx
	je	.LBB8_24
	.p2align	4, 0x90
.LBB8_20:                               # %_ZNK11CStringBaseIwE4FindERKS0_.exit.thread
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	8(%rsp), %rbx           # 8-byte Reload
	incq	%rbx
	cmpq	40(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB8_2
.LBB8_22:
	movl	$-1, %ebx
	jmp	.LBB8_40
.LBB8_23:
	xorl	%ebp, %ebp
.LBB8_24:                               # %_ZNK11CStringBaseIwE4FindERKS0_.exit.thread45
	subl	%ebp, %ecx
.Ltmp90:
	leaq	24(%rsp), %r14
	movq	%r14, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%ebp, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp91:
# BB#25:                                # %_ZNK11CStringBaseIwE3MidEi.exit
	movq	(%rsp), %rax            # 8-byte Reload
	cmpq	%rax, %r14
	je	.LBB8_32
# BB#26:
	movl	$0, 8(%rax)
	movq	(%rax), %rbp
	movl	$0, (%rbp)
	movslq	32(%rsp), %r12
	incq	%r12
	movl	12(%rax), %ebx
	cmpl	%ebx, %r12d
	je	.LBB8_35
# BB#27:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp93:
	callq	_Znam
	movq	%rax, %r14
.Ltmp94:
# BB#28:                                # %.noexc
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB8_33
# BB#29:                                # %.noexc
	testl	%ebx, %ebx
	movq	(%rsp), %rcx            # 8-byte Reload
	jle	.LBB8_34
# BB#30:                                # %._crit_edge.thread.i.i
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	(%rsp), %rcx            # 8-byte Reload
	movslq	8(%rcx), %rax
	jmp	.LBB8_34
.LBB8_31:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$0, 8(%rax)
	movq	(%rax), %rax
	movl	$0, (%rax)
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB8_39
.LBB8_32:                               # %_ZNK11CStringBaseIwE3MidEi.exit._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	(%rax), %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	testq	%rdi, %rdi
	jne	.LBB8_38
	jmp	.LBB8_39
.LBB8_33:
	movq	(%rsp), %rcx            # 8-byte Reload
.LBB8_34:                               # %._crit_edge16.i.i
	movq	%r14, (%rcx)
	movl	$0, (%r14,%rax,4)
	movl	%r12d, 12(%rcx)
	movq	%r14, %rbp
.LBB8_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i29
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	24(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_36:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_36
# BB#37:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	32(%rsp), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%eax, 8(%rcx)
	testq	%rdi, %rdi
	je	.LBB8_39
.LBB8_38:
	callq	_ZdaPv
.LBB8_39:                               # %_ZN11CStringBaseIwED2Ev.exit28
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB8_40:                               # %.loopexit
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_41:
.Ltmp95:
	movq	%rax, %rbp
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_46
# BB#42:
	callq	_ZdaPv
	jmp	.LBB8_46
.LBB8_43:
.Ltmp92:
	jmp	.LBB8_45
.LBB8_44:
.Ltmp98:
.LBB8_45:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rax, %rbp
.LBB8_46:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN18NCommandLineParser12ParseCommandEiPKNS_12CCommandFormERK11CStringBaseIwERS4_, .Lfunc_end8-_ZN18NCommandLineParser12ParseCommandEiPKNS_12CCommandFormERK11CStringBaseIwERS4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp96-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp96
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin6   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin6   #     jumps to .Ltmp92
	.byte	0                       #   On action: cleanup
	.long	.Ltmp93-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin6   #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin6   # >> Call Site 5 <<
	.long	.Lfunc_end8-.Ltmp94     #   Call between .Ltmp94 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 32
.Lcfi79:
	.cfi_offset %rbx, -24
.Lcfi80:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp99:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp100:
# BB#1:
.Ltmp105:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp106:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB9_5:
.Ltmp107:
	movq	%rax, %r14
	jmp	.LBB9_6
.LBB9_3:
.Ltmp101:
	movq	%rax, %r14
.Ltmp102:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp103:
.LBB9_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_4:
.Ltmp104:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end9-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp99-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin7  #     jumps to .Ltmp101
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp107-.Lfunc_begin7  #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin7  #     jumps to .Ltmp104
	.byte	1                       #   On action: 1
	.long	.Ltmp103-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp103    #   Call between .Ltmp103 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 64
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB10_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB10_6
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	callq	_ZdaPv
.LBB10_5:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB10_6:                               #   in Loop: Header=BB10_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB10_2
.LBB10_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end10:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end10-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"maxLen == kNoLen"
	.size	.L.str, 17

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"switch must be single"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"switch is not full"
	.size	.L.str.2, 19

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.3:
	.long	45                      # 0x2d
	.long	45                      # 0x2d
	.long	0                       # 0x0
	.size	.L.str.3, 12

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24


	.globl	_ZN18NCommandLineParser7CParserC1Ei
	.type	_ZN18NCommandLineParser7CParserC1Ei,@function
_ZN18NCommandLineParser7CParserC1Ei = _ZN18NCommandLineParser7CParserC2Ei
	.globl	_ZN18NCommandLineParser7CParserD1Ev
	.type	_ZN18NCommandLineParser7CParserD1Ev,@function
_ZN18NCommandLineParser7CParserD1Ev = _ZN18NCommandLineParser7CParserD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
