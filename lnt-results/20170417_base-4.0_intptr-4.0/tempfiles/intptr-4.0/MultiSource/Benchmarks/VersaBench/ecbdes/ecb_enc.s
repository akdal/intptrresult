	.text
	.file	"ecb_enc.bc"
	.globl	des_options
	.p2align	4, 0x90
	.type	des_options,@function
des_options:                            # @des_options
	.cfi_startproc
# BB#0:
	movb	des_options.init(%rip), %al
	testb	%al, %al
	jne	.LBB0_2
# BB#1:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movb	$1, des_options.init(%rip)
	movl	$des_options.buf, %edi
	movl	$.L.str.6, %esi
	movl	$.L.str.2, %edx
	movl	$.L.str.3, %ecx
	movl	$.L.str.4, %r8d
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	sprintf
	addq	$8, %rsp
.LBB0_2:
	movl	$des_options.buf, %eax
	retq
.Lfunc_end0:
	.size	des_options, .Lfunc_end0-des_options
	.cfi_endproc

	.globl	des_ecb_encrypt
	.p2align	4, 0x90
	.type	des_ecb_encrypt,@function
des_ecb_encrypt:                        # @des_ecb_encrypt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movzbl	(%rdi), %eax
	movzbl	4(%rdi), %esi
	shll	$8, %esi
	orl	%eax, %esi
	pinsrw	$0, %esi, %xmm2
	pxor	%xmm0, %xmm0
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movzbl	1(%rdi), %eax
	movzbl	5(%rdi), %esi
	shll	$8, %esi
	orl	%eax, %esi
	pinsrw	$0, %esi, %xmm1
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	psllq	$8, %xmm1
	por	%xmm2, %xmm1
	movzbl	2(%rdi), %eax
	movzbl	6(%rdi), %esi
	shll	$8, %esi
	orl	%eax, %esi
	pinsrw	$0, %esi, %xmm2
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	psllq	$16, %xmm2
	movzbl	3(%rdi), %eax
	movzbl	7(%rdi), %esi
	shll	$8, %esi
	orl	%eax, %esi
	pinsrw	$0, %esi, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	psllq	$24, %xmm3
	por	%xmm2, %xmm3
	por	%xmm1, %xmm3
	movdqa	%xmm3, (%rsp)
	movq	%rsp, %rdi
	movq	%rdx, %rsi
	movl	%ecx, %edx
	callq	des_encrypt
	movq	(%rsp), %rax
	movb	%al, (%rbx)
	movb	%ah, 1(%rbx)  # NOREX
	movq	%rax, %rcx
	shrq	$16, %rcx
	movb	%cl, 2(%rbx)
	shrq	$24, %rax
	movb	%al, 3(%rbx)
	movq	8(%rsp), %rax
	movb	%al, 4(%rbx)
	movb	%ah, 5(%rbx)  # NOREX
	movq	%rax, %rcx
	shrq	$16, %rcx
	movb	%cl, 6(%rbx)
	shrq	$24, %rax
	movb	%al, 7(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	des_ecb_encrypt, .Lfunc_end1-des_ecb_encrypt
	.cfi_endproc

	.type	des_SPtrans,@object     # @des_SPtrans
	.section	.rodata,"a",@progbits
	.globl	des_SPtrans
	.p2align	4
des_SPtrans:
	.quad	34080768                # 0x2080800
	.quad	524288                  # 0x80000
	.quad	33554434                # 0x2000002
	.quad	34080770                # 0x2080802
	.quad	33554432                # 0x2000000
	.quad	526338                  # 0x80802
	.quad	524290                  # 0x80002
	.quad	33554434                # 0x2000002
	.quad	526338                  # 0x80802
	.quad	34080768                # 0x2080800
	.quad	34078720                # 0x2080000
	.quad	2050                    # 0x802
	.quad	33556482                # 0x2000802
	.quad	33554432                # 0x2000000
	.quad	0                       # 0x0
	.quad	524290                  # 0x80002
	.quad	524288                  # 0x80000
	.quad	2                       # 0x2
	.quad	33556480                # 0x2000800
	.quad	526336                  # 0x80800
	.quad	34080770                # 0x2080802
	.quad	34078720                # 0x2080000
	.quad	2050                    # 0x802
	.quad	33556480                # 0x2000800
	.quad	2                       # 0x2
	.quad	2048                    # 0x800
	.quad	526336                  # 0x80800
	.quad	34078722                # 0x2080002
	.quad	2048                    # 0x800
	.quad	33556482                # 0x2000802
	.quad	34078722                # 0x2080002
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	34080770                # 0x2080802
	.quad	33556480                # 0x2000800
	.quad	524290                  # 0x80002
	.quad	34080768                # 0x2080800
	.quad	524288                  # 0x80000
	.quad	2050                    # 0x802
	.quad	33556480                # 0x2000800
	.quad	34078722                # 0x2080002
	.quad	2048                    # 0x800
	.quad	526336                  # 0x80800
	.quad	33554434                # 0x2000002
	.quad	526338                  # 0x80802
	.quad	2                       # 0x2
	.quad	33554434                # 0x2000002
	.quad	34078720                # 0x2080000
	.quad	34080770                # 0x2080802
	.quad	526336                  # 0x80800
	.quad	34078720                # 0x2080000
	.quad	33556482                # 0x2000802
	.quad	33554432                # 0x2000000
	.quad	2050                    # 0x802
	.quad	524290                  # 0x80002
	.quad	0                       # 0x0
	.quad	524288                  # 0x80000
	.quad	33554432                # 0x2000000
	.quad	33556482                # 0x2000802
	.quad	34080768                # 0x2080800
	.quad	2                       # 0x2
	.quad	34078722                # 0x2080002
	.quad	2048                    # 0x800
	.quad	526338                  # 0x80802
	.quad	1074823184              # 0x40108010
	.quad	0                       # 0x0
	.quad	1081344                 # 0x108000
	.quad	1074790400              # 0x40100000
	.quad	1073741840              # 0x40000010
	.quad	32784                   # 0x8010
	.quad	1073774592              # 0x40008000
	.quad	1081344                 # 0x108000
	.quad	32768                   # 0x8000
	.quad	1074790416              # 0x40100010
	.quad	16                      # 0x10
	.quad	1073774592              # 0x40008000
	.quad	1048592                 # 0x100010
	.quad	1074823168              # 0x40108000
	.quad	1074790400              # 0x40100000
	.quad	16                      # 0x10
	.quad	1048576                 # 0x100000
	.quad	1073774608              # 0x40008010
	.quad	1074790416              # 0x40100010
	.quad	32768                   # 0x8000
	.quad	1081360                 # 0x108010
	.quad	1073741824              # 0x40000000
	.quad	0                       # 0x0
	.quad	1048592                 # 0x100010
	.quad	1073774608              # 0x40008010
	.quad	1081360                 # 0x108010
	.quad	1074823168              # 0x40108000
	.quad	1073741840              # 0x40000010
	.quad	1073741824              # 0x40000000
	.quad	1048576                 # 0x100000
	.quad	32784                   # 0x8010
	.quad	1074823184              # 0x40108010
	.quad	1048592                 # 0x100010
	.quad	1074823168              # 0x40108000
	.quad	1073774592              # 0x40008000
	.quad	1081360                 # 0x108010
	.quad	1074823184              # 0x40108010
	.quad	1048592                 # 0x100010
	.quad	1073741840              # 0x40000010
	.quad	0                       # 0x0
	.quad	1073741824              # 0x40000000
	.quad	32784                   # 0x8010
	.quad	1048576                 # 0x100000
	.quad	1074790416              # 0x40100010
	.quad	32768                   # 0x8000
	.quad	1073741824              # 0x40000000
	.quad	1081360                 # 0x108010
	.quad	1073774608              # 0x40008010
	.quad	1074823168              # 0x40108000
	.quad	32768                   # 0x8000
	.quad	0                       # 0x0
	.quad	1073741840              # 0x40000010
	.quad	16                      # 0x10
	.quad	1074823184              # 0x40108010
	.quad	1081344                 # 0x108000
	.quad	1074790400              # 0x40100000
	.quad	1074790416              # 0x40100010
	.quad	1048576                 # 0x100000
	.quad	32784                   # 0x8010
	.quad	1073774592              # 0x40008000
	.quad	1073774608              # 0x40008010
	.quad	16                      # 0x10
	.quad	1074790400              # 0x40100000
	.quad	1081344                 # 0x108000
	.quad	67108865                # 0x4000001
	.quad	67371264                # 0x4040100
	.quad	256                     # 0x100
	.quad	67109121                # 0x4000101
	.quad	262145                  # 0x40001
	.quad	67108864                # 0x4000000
	.quad	67109121                # 0x4000101
	.quad	262400                  # 0x40100
	.quad	67109120                # 0x4000100
	.quad	262144                  # 0x40000
	.quad	67371008                # 0x4040000
	.quad	1                       # 0x1
	.quad	67371265                # 0x4040101
	.quad	257                     # 0x101
	.quad	1                       # 0x1
	.quad	67371009                # 0x4040001
	.quad	0                       # 0x0
	.quad	262145                  # 0x40001
	.quad	67371264                # 0x4040100
	.quad	256                     # 0x100
	.quad	257                     # 0x101
	.quad	67371265                # 0x4040101
	.quad	262144                  # 0x40000
	.quad	67108865                # 0x4000001
	.quad	67371009                # 0x4040001
	.quad	67109120                # 0x4000100
	.quad	262401                  # 0x40101
	.quad	67371008                # 0x4040000
	.quad	262400                  # 0x40100
	.quad	0                       # 0x0
	.quad	67108864                # 0x4000000
	.quad	262401                  # 0x40101
	.quad	67371264                # 0x4040100
	.quad	256                     # 0x100
	.quad	1                       # 0x1
	.quad	262144                  # 0x40000
	.quad	257                     # 0x101
	.quad	262145                  # 0x40001
	.quad	67371008                # 0x4040000
	.quad	67109121                # 0x4000101
	.quad	0                       # 0x0
	.quad	67371264                # 0x4040100
	.quad	262400                  # 0x40100
	.quad	67371009                # 0x4040001
	.quad	262145                  # 0x40001
	.quad	67108864                # 0x4000000
	.quad	67371265                # 0x4040101
	.quad	1                       # 0x1
	.quad	262401                  # 0x40101
	.quad	67108865                # 0x4000001
	.quad	67108864                # 0x4000000
	.quad	67371265                # 0x4040101
	.quad	262144                  # 0x40000
	.quad	67109120                # 0x4000100
	.quad	67109121                # 0x4000101
	.quad	262400                  # 0x40100
	.quad	67109120                # 0x4000100
	.quad	0                       # 0x0
	.quad	67371009                # 0x4040001
	.quad	257                     # 0x101
	.quad	67108865                # 0x4000001
	.quad	262401                  # 0x40101
	.quad	256                     # 0x100
	.quad	67371008                # 0x4040000
	.quad	4198408                 # 0x401008
	.quad	268439552               # 0x10001000
	.quad	8                       # 0x8
	.quad	272633864               # 0x10401008
	.quad	0                       # 0x0
	.quad	272629760               # 0x10400000
	.quad	268439560               # 0x10001008
	.quad	4194312                 # 0x400008
	.quad	272633856               # 0x10401000
	.quad	268435464               # 0x10000008
	.quad	268435456               # 0x10000000
	.quad	4104                    # 0x1008
	.quad	268435464               # 0x10000008
	.quad	4198408                 # 0x401008
	.quad	4194304                 # 0x400000
	.quad	268435456               # 0x10000000
	.quad	272629768               # 0x10400008
	.quad	4198400                 # 0x401000
	.quad	4096                    # 0x1000
	.quad	8                       # 0x8
	.quad	4198400                 # 0x401000
	.quad	268439560               # 0x10001008
	.quad	272629760               # 0x10400000
	.quad	4096                    # 0x1000
	.quad	4104                    # 0x1008
	.quad	0                       # 0x0
	.quad	4194312                 # 0x400008
	.quad	272633856               # 0x10401000
	.quad	268439552               # 0x10001000
	.quad	272629768               # 0x10400008
	.quad	272633864               # 0x10401008
	.quad	4194304                 # 0x400000
	.quad	272629768               # 0x10400008
	.quad	4104                    # 0x1008
	.quad	4194304                 # 0x400000
	.quad	268435464               # 0x10000008
	.quad	4198400                 # 0x401000
	.quad	268439552               # 0x10001000
	.quad	8                       # 0x8
	.quad	272629760               # 0x10400000
	.quad	268439560               # 0x10001008
	.quad	0                       # 0x0
	.quad	4096                    # 0x1000
	.quad	4194312                 # 0x400008
	.quad	0                       # 0x0
	.quad	272629768               # 0x10400008
	.quad	272633856               # 0x10401000
	.quad	4096                    # 0x1000
	.quad	268435456               # 0x10000000
	.quad	272633864               # 0x10401008
	.quad	4198408                 # 0x401008
	.quad	4194304                 # 0x400000
	.quad	272633864               # 0x10401008
	.quad	8                       # 0x8
	.quad	268439552               # 0x10001000
	.quad	4198408                 # 0x401008
	.quad	4194312                 # 0x400008
	.quad	4198400                 # 0x401000
	.quad	272629760               # 0x10400000
	.quad	268439560               # 0x10001008
	.quad	4104                    # 0x1008
	.quad	268435456               # 0x10000000
	.quad	268435464               # 0x10000008
	.quad	272633856               # 0x10401000
	.quad	134217728               # 0x8000000
	.quad	65536                   # 0x10000
	.quad	1024                    # 0x400
	.quad	134284320               # 0x8010420
	.quad	134283296               # 0x8010020
	.quad	134218752               # 0x8000400
	.quad	66592                   # 0x10420
	.quad	134283264               # 0x8010000
	.quad	65536                   # 0x10000
	.quad	32                      # 0x20
	.quad	134217760               # 0x8000020
	.quad	66560                   # 0x10400
	.quad	134218784               # 0x8000420
	.quad	134283296               # 0x8010020
	.quad	134284288               # 0x8010400
	.quad	0                       # 0x0
	.quad	66560                   # 0x10400
	.quad	134217728               # 0x8000000
	.quad	65568                   # 0x10020
	.quad	1056                    # 0x420
	.quad	134218752               # 0x8000400
	.quad	66592                   # 0x10420
	.quad	0                       # 0x0
	.quad	134217760               # 0x8000020
	.quad	32                      # 0x20
	.quad	134218784               # 0x8000420
	.quad	134284320               # 0x8010420
	.quad	65568                   # 0x10020
	.quad	134283264               # 0x8010000
	.quad	1024                    # 0x400
	.quad	1056                    # 0x420
	.quad	134284288               # 0x8010400
	.quad	134284288               # 0x8010400
	.quad	134218784               # 0x8000420
	.quad	65568                   # 0x10020
	.quad	134283264               # 0x8010000
	.quad	65536                   # 0x10000
	.quad	32                      # 0x20
	.quad	134217760               # 0x8000020
	.quad	134218752               # 0x8000400
	.quad	134217728               # 0x8000000
	.quad	66560                   # 0x10400
	.quad	134284320               # 0x8010420
	.quad	0                       # 0x0
	.quad	66592                   # 0x10420
	.quad	134217728               # 0x8000000
	.quad	1024                    # 0x400
	.quad	65568                   # 0x10020
	.quad	134218784               # 0x8000420
	.quad	1024                    # 0x400
	.quad	0                       # 0x0
	.quad	134284320               # 0x8010420
	.quad	134283296               # 0x8010020
	.quad	134284288               # 0x8010400
	.quad	1056                    # 0x420
	.quad	65536                   # 0x10000
	.quad	66560                   # 0x10400
	.quad	134283296               # 0x8010020
	.quad	134218752               # 0x8000400
	.quad	1056                    # 0x420
	.quad	32                      # 0x20
	.quad	66592                   # 0x10420
	.quad	134283264               # 0x8010000
	.quad	134217760               # 0x8000020
	.quad	2147483712              # 0x80000040
	.quad	2097216                 # 0x200040
	.quad	0                       # 0x0
	.quad	2149588992              # 0x80202000
	.quad	2097216                 # 0x200040
	.quad	8192                    # 0x2000
	.quad	2147491904              # 0x80002040
	.quad	2097152                 # 0x200000
	.quad	8256                    # 0x2040
	.quad	2149589056              # 0x80202040
	.quad	2105344                 # 0x202000
	.quad	2147483648              # 0x80000000
	.quad	2147491840              # 0x80002000
	.quad	2147483712              # 0x80000040
	.quad	2149580800              # 0x80200000
	.quad	2105408                 # 0x202040
	.quad	2097152                 # 0x200000
	.quad	2147491904              # 0x80002040
	.quad	2149580864              # 0x80200040
	.quad	0                       # 0x0
	.quad	8192                    # 0x2000
	.quad	64                      # 0x40
	.quad	2149588992              # 0x80202000
	.quad	2149580864              # 0x80200040
	.quad	2149589056              # 0x80202040
	.quad	2149580800              # 0x80200000
	.quad	2147483648              # 0x80000000
	.quad	8256                    # 0x2040
	.quad	64                      # 0x40
	.quad	2105344                 # 0x202000
	.quad	2105408                 # 0x202040
	.quad	2147491840              # 0x80002000
	.quad	8256                    # 0x2040
	.quad	2147483648              # 0x80000000
	.quad	2147491840              # 0x80002000
	.quad	2105408                 # 0x202040
	.quad	2149588992              # 0x80202000
	.quad	2097216                 # 0x200040
	.quad	0                       # 0x0
	.quad	2147491840              # 0x80002000
	.quad	2147483648              # 0x80000000
	.quad	8192                    # 0x2000
	.quad	2149580864              # 0x80200040
	.quad	2097152                 # 0x200000
	.quad	2097216                 # 0x200040
	.quad	2149589056              # 0x80202040
	.quad	2105344                 # 0x202000
	.quad	64                      # 0x40
	.quad	2149589056              # 0x80202040
	.quad	2105344                 # 0x202000
	.quad	2097152                 # 0x200000
	.quad	2147491904              # 0x80002040
	.quad	2147483712              # 0x80000040
	.quad	2149580800              # 0x80200000
	.quad	2105408                 # 0x202040
	.quad	0                       # 0x0
	.quad	8192                    # 0x2000
	.quad	2147483712              # 0x80000040
	.quad	2147491904              # 0x80002040
	.quad	2149588992              # 0x80202000
	.quad	2149580800              # 0x80200000
	.quad	8256                    # 0x2040
	.quad	64                      # 0x40
	.quad	2149580864              # 0x80200040
	.quad	16384                   # 0x4000
	.quad	512                     # 0x200
	.quad	16777728                # 0x1000200
	.quad	16777220                # 0x1000004
	.quad	16794116                # 0x1004204
	.quad	16388                   # 0x4004
	.quad	16896                   # 0x4200
	.quad	0                       # 0x0
	.quad	16777216                # 0x1000000
	.quad	16777732                # 0x1000204
	.quad	516                     # 0x204
	.quad	16793600                # 0x1004000
	.quad	4                       # 0x4
	.quad	16794112                # 0x1004200
	.quad	16793600                # 0x1004000
	.quad	516                     # 0x204
	.quad	16777732                # 0x1000204
	.quad	16384                   # 0x4000
	.quad	16388                   # 0x4004
	.quad	16794116                # 0x1004204
	.quad	0                       # 0x0
	.quad	16777728                # 0x1000200
	.quad	16777220                # 0x1000004
	.quad	16896                   # 0x4200
	.quad	16793604                # 0x1004004
	.quad	16900                   # 0x4204
	.quad	16794112                # 0x1004200
	.quad	4                       # 0x4
	.quad	16900                   # 0x4204
	.quad	16793604                # 0x1004004
	.quad	512                     # 0x200
	.quad	16777216                # 0x1000000
	.quad	16900                   # 0x4204
	.quad	16793600                # 0x1004000
	.quad	16793604                # 0x1004004
	.quad	516                     # 0x204
	.quad	16384                   # 0x4000
	.quad	512                     # 0x200
	.quad	16777216                # 0x1000000
	.quad	16793604                # 0x1004004
	.quad	16777732                # 0x1000204
	.quad	16900                   # 0x4204
	.quad	16896                   # 0x4200
	.quad	0                       # 0x0
	.quad	512                     # 0x200
	.quad	16777220                # 0x1000004
	.quad	4                       # 0x4
	.quad	16777728                # 0x1000200
	.quad	0                       # 0x0
	.quad	16777732                # 0x1000204
	.quad	16777728                # 0x1000200
	.quad	16896                   # 0x4200
	.quad	516                     # 0x204
	.quad	16384                   # 0x4000
	.quad	16794116                # 0x1004204
	.quad	16777216                # 0x1000000
	.quad	16794112                # 0x1004200
	.quad	4                       # 0x4
	.quad	16388                   # 0x4004
	.quad	16794116                # 0x1004204
	.quad	16777220                # 0x1000004
	.quad	16794112                # 0x1004200
	.quad	16793600                # 0x1004000
	.quad	16388                   # 0x4004
	.quad	545259648               # 0x20800080
	.quad	545390592               # 0x20820000
	.quad	131200                  # 0x20080
	.quad	0                       # 0x0
	.quad	537001984               # 0x20020000
	.quad	8388736                 # 0x800080
	.quad	545259520               # 0x20800000
	.quad	545390720               # 0x20820080
	.quad	128                     # 0x80
	.quad	536870912               # 0x20000000
	.quad	8519680                 # 0x820000
	.quad	131200                  # 0x20080
	.quad	8519808                 # 0x820080
	.quad	537002112               # 0x20020080
	.quad	536871040               # 0x20000080
	.quad	545259520               # 0x20800000
	.quad	131072                  # 0x20000
	.quad	8519808                 # 0x820080
	.quad	8388736                 # 0x800080
	.quad	537001984               # 0x20020000
	.quad	545390720               # 0x20820080
	.quad	536871040               # 0x20000080
	.quad	0                       # 0x0
	.quad	8519680                 # 0x820000
	.quad	536870912               # 0x20000000
	.quad	8388608                 # 0x800000
	.quad	537002112               # 0x20020080
	.quad	545259648               # 0x20800080
	.quad	8388608                 # 0x800000
	.quad	131072                  # 0x20000
	.quad	545390592               # 0x20820000
	.quad	128                     # 0x80
	.quad	8388608                 # 0x800000
	.quad	131072                  # 0x20000
	.quad	536871040               # 0x20000080
	.quad	545390720               # 0x20820080
	.quad	131200                  # 0x20080
	.quad	536870912               # 0x20000000
	.quad	0                       # 0x0
	.quad	8519680                 # 0x820000
	.quad	545259648               # 0x20800080
	.quad	537002112               # 0x20020080
	.quad	537001984               # 0x20020000
	.quad	8388736                 # 0x800080
	.quad	545390592               # 0x20820000
	.quad	128                     # 0x80
	.quad	8388736                 # 0x800080
	.quad	537001984               # 0x20020000
	.quad	545390720               # 0x20820080
	.quad	8388608                 # 0x800000
	.quad	545259520               # 0x20800000
	.quad	536871040               # 0x20000080
	.quad	8519680                 # 0x820000
	.quad	131200                  # 0x20080
	.quad	537002112               # 0x20020080
	.quad	545259520               # 0x20800000
	.quad	128                     # 0x80
	.quad	545390592               # 0x20820000
	.quad	8519808                 # 0x820080
	.quad	0                       # 0x0
	.quad	536870912               # 0x20000000
	.quad	545259648               # 0x20800080
	.quad	131072                  # 0x20000
	.quad	8519808                 # 0x820080
	.size	des_SPtrans, 4096

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"libdes v 3.24 - 20-Apr-1996 - eay"
	.size	.L.str, 34

	.type	libdes_version,@object  # @libdes_version
	.data
	.globl	libdes_version
	.p2align	3
libdes_version:
	.quad	.L.str
	.size	libdes_version, 8

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"DES part of SSLeay 0.6.6 06-Dec-1996"
	.size	.L.str.1, 37

	.type	DES_version,@object     # @DES_version
	.data
	.globl	DES_version
	.p2align	3
DES_version:
	.quad	.L.str.1
	.size	DES_version, 8

	.type	des_options.init,@object # @des_options.init
	.local	des_options.init
	.comm	des_options.init,1,4
	.type	des_options.buf,@object # @des_options.buf
	.local	des_options.buf
	.comm	des_options.buf,32,16
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"idx"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"cisc"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"4"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"long"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"des(%s,%s,%s,%s)"
	.size	.L.str.6, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
