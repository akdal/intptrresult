	.text
	.file	"clamscan_treewalk.bc"
	.globl	treewalk
	.p2align	4, 0x90
	.type	treewalk,@function
treewalk:                               # @treewalk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 256
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movq	%r8, %r14
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	$.L.str, %esi
	movq	%r13, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_6
# BB#1:
	leaq	24(%rsp), %rdx
	movl	$.L.str, %esi
	movq	%r13, %rdi
	callq	opt_firstarg
	testq	%rax, %rax
	je	.LBB0_6
# BB#2:                                 # %.lr.ph136.preheader
	leaq	24(%rsp), %rbp
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph136
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	match_regex
	cmpl	$1, %eax
	je	.LBB0_4
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str, %esi
	movq	%rbp, %rdi
	callq	opt_nextarg
	testq	%rax, %rax
	jne	.LBB0_3
.LBB0_6:                                # %.loopexit
	movl	$.L.str.1, %esi
	movq	%r13, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_13
# BB#7:
	leaq	24(%rsp), %rdx
	movl	$.L.str.1, %esi
	movq	%r13, %rdi
	callq	opt_firstarg
	testq	%rax, %rax
	je	.LBB0_11
# BB#8:                                 # %.lr.ph133.preheader
	leaq	24(%rsp), %rbp
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph133
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	match_regex
	cmpl	$1, %eax
	je	.LBB0_13
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	movl	$.L.str.1, %esi
	movq	%rbp, %rdi
	callq	opt_nextarg
	testq	%rax, %rax
	jne	.LBB0_9
.LBB0_11:                               # %.critedge125
	xorl	%ebp, %ebp
	cmpw	$0, printinfected(%rip)
	jne	.LBB0_38
# BB#12:
	xorl	%ebp, %ebp
	movl	$.L.str.2, %edi
.LBB0_37:                               # %.loopexit127
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	logg
	jmp	.LBB0_38
.LBB0_13:                               # %.critedge
	movl	256(%rsp), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$.L.str.3, %esi
	movq	%r13, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_14
# BB#15:
	movl	$.L.str.3, %esi
	movq	%r13, %rdi
	callq	opt_arg
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	jmp	.LBB0_16
.LBB0_4:
	xorl	%ebp, %ebp
	jmp	.LBB0_38
.LBB0_14:
	movl	$15, %eax
.LBB0_16:
	movl	256(%rsp), %ecx
	xorl	%ebp, %ebp
	cmpl	%ecx, %eax
	jb	.LBB0_38
# BB#17:
	movq	%r14, 40(%rsp)          # 8-byte Spill
	incl	info+4(%rip)
	movq	%rbx, %rdi
	callq	opendir
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_35
# BB#18:                                # %.preheader
	movl	%r15d, 16(%rsp)         # 4-byte Spill
	movq	%r14, %rdi
	callq	readdir
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_19
# BB#21:                                # %.lr.ph.lr.ph
	incl	20(%rsp)                # 4-byte Folded Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	jmp	.LBB0_22
.LBB0_28:                               # %.thread
                                        #   in Loop: Header=BB0_22 Depth=1
	addq	$19, %r15
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	strlen
	leaq	2(%rbp,%rax), %rdi
	callq	malloc
	movq	%rax, %r12
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	sprintf
	movl	$1, %edi
	movq	%r12, %rsi
	leaq	56(%rsp), %rdx
	callq	__lxstat
	cmpl	$-1, %eax
	je	.LBB0_34
# BB#29:                                #   in Loop: Header=BB0_22 Depth=1
	movl	80(%rsp), %eax
	movl	$61440, %ecx            # imm = 0xF000
	andl	%ecx, %eax
	cmpl	$16384, %eax            # imm = 0x4000
	jne	.LBB0_32
# BB#30:                                #   in Loop: Header=BB0_22 Depth=1
	movzwl	recursion(%rip), %ecx
	testw	%cx, %cx
	je	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_22 Depth=1
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%r12, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r13, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	16(%rsp), %r9d          # 4-byte Reload
	callq	treewalk
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	sete	%cl
	addl	%ecx, 12(%rsp)          # 4-byte Folded Spill
	jmp	.LBB0_34
.LBB0_32:                               #   in Loop: Header=BB0_22 Depth=1
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB0_34
# BB#33:                                #   in Loop: Header=BB0_22 Depth=1
	movq	%r12, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r13, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	16(%rsp), %r9d          # 4-byte Reload
	callq	scanfile
	addl	%eax, 12(%rsp)          # 4-byte Folded Spill
.LBB0_34:                               # %.outer
                                        #   in Loop: Header=BB0_22 Depth=1
	movq	%r12, %rdi
	callq	free
	jmp	.LBB0_23
	.p2align	4, 0x90
.LBB0_22:                               # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%r15)
	je	.LBB0_23
# BB#24:                                #   in Loop: Header=BB0_22 Depth=1
	cmpb	$46, 19(%r15)
	jne	.LBB0_28
# BB#25:                                #   in Loop: Header=BB0_22 Depth=1
	cmpb	$0, 20(%r15)
	je	.LBB0_23
# BB#26:                                #   in Loop: Header=BB0_22 Depth=1
	cmpb	$46, 20(%r15)
	jne	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_22 Depth=1
	cmpb	$0, 21(%r15)
	jne	.LBB0_28
.LBB0_23:                               # %.backedge
                                        #   in Loop: Header=BB0_22 Depth=1
	movq	%r14, %rdi
	callq	readdir
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB0_22
	jmp	.LBB0_20
.LBB0_35:
	movl	$53, %ebp
	cmpw	$0, printinfected(%rip)
	jne	.LBB0_38
# BB#36:
	movl	$.L.str.7, %edi
	jmp	.LBB0_37
.LBB0_19:
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
.LBB0_20:                               # %.outer._crit_edge
	movq	%r14, %rdi
	callq	closedir
	xorl	%ebp, %ebp
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	setne	%bpl
.LBB0_38:                               # %.loopexit127
	movl	%ebp, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	treewalk, .Lfunc_end0-treewalk
	.cfi_endproc

	.globl	clamav_rmdirs
	.p2align	4, 0x90
	.type	clamav_rmdirs,@function
clamav_rmdirs:                          # @clamav_rmdirs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	fork
	cmpl	$-1, %eax
	je	.LBB1_1
# BB#2:
	testl	%eax, %eax
	jne	.LBB1_11
# BB#3:
	callq	geteuid
	testl	%eax, %eax
	jne	.LBB1_10
# BB#4:
	movl	$.L.str.8, %edi
	callq	getpwnam
	movq	%rax, %r15
	movl	$-3, %ebx
	testq	%r15, %r15
	je	.LBB1_12
# BB#5:
	movl	20(%r15), %edi
	callq	setgid
	testl	%eax, %eax
	jne	.LBB1_6
# BB#8:
	movl	16(%r15), %edi
	callq	setuid
	testl	%eax, %eax
	je	.LBB1_10
# BB#9:
	movq	stderr(%rip), %rdi
	movl	16(%r15), %edx
	movl	$.L.str.10, %esi
.LBB1_7:
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB1_12
.LBB1_1:
	movl	$-1, %ebx
	jmp	.LBB1_12
.LBB1_11:
	leaq	4(%rsp), %rsi
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	movl	%eax, %edi
	callq	waitpid
	testb	$127, 4(%rsp)
	movl	$-2, %ebx
	cmovel	%ebp, %ebx
.LBB1_12:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_6:
	movq	stderr(%rip), %rdi
	movl	20(%r15), %edx
	movl	$.L.str.9, %esi
	jmp	.LBB1_7
.LBB1_10:
	movq	%r14, %rdi
	callq	cli_rmdirs
	xorl	%edi, %edi
	callq	exit
.Lfunc_end1:
	.size	clamav_rmdirs, .Lfunc_end1-clamav_rmdirs
	.cfi_endproc

	.globl	fixperms
	.p2align	4, 0x90
	.type	fixperms,@function
fixperms:                               # @fixperms
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 192
.Lcfi28:
	.cfi_offset %rbx, -48
.Lcfi29:
	.cfi_offset %r12, -40
.Lcfi30:
	.cfi_offset %r13, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	callq	opendir
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB2_16
# BB#1:                                 # %.preheader
	movq	%r12, %rdi
	callq	readdir
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_15
# BB#2:                                 # %.lr.ph
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rbx)
	je	.LBB2_14
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	cmpb	$46, 19(%rbx)
	jne	.LBB2_8
# BB#5:                                 #   in Loop: Header=BB2_3 Depth=1
	cmpb	$0, 20(%rbx)
	je	.LBB2_14
# BB#6:                                 #   in Loop: Header=BB2_3 Depth=1
	cmpb	$46, 20(%rbx)
	jne	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_3 Depth=1
	cmpb	$0, 21(%rbx)
	je	.LBB2_14
	.p2align	4, 0x90
.LBB2_8:                                # %.thread
                                        #   in Loop: Header=BB2_3 Depth=1
	addq	$19, %rbx
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %r13
	movq	%rbx, %rdi
	callq	strlen
	leaq	2(%r13,%rax), %rdi
	callq	malloc
	movq	%rax, %r13
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	sprintf
	movl	$1, %edi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	__lxstat
	cmpl	$-1, %eax
	je	.LBB2_13
# BB#9:                                 #   in Loop: Header=BB2_3 Depth=1
	movzwl	24(%rsp), %eax
	andl	$61440, %eax            # imm = 0xF000
	cmpl	$32768, %eax            # imm = 0x8000
	je	.LBB2_12
# BB#10:                                #   in Loop: Header=BB2_3 Depth=1
	movzwl	%ax, %eax
	cmpl	$16384, %eax            # imm = 0x4000
	jne	.LBB2_13
# BB#11:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$448, %esi              # imm = 0x1C0
	movq	%r13, %rdi
	callq	chmod
	movq	%r13, %rdi
	callq	fixperms
	jmp	.LBB2_13
.LBB2_12:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$448, %esi              # imm = 0x1C0
	movq	%r13, %rdi
	callq	chmod
.LBB2_13:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%r13, %rdi
	callq	free
.LBB2_14:                               # %.backedge
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r12, %rdi
	callq	readdir
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_3
.LBB2_15:                               # %._crit_edge
	movq	%r12, %rdi
	callq	closedir
	xorl	%ebx, %ebx
	jmp	.LBB2_18
.LBB2_16:
	movl	$53, %ebx
	cmpw	$0, printinfected(%rip)
	jne	.LBB2_18
# BB#17:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	logg
.LBB2_18:
	movl	%ebx, %eax
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	fixperms, .Lfunc_end2-fixperms
	.cfi_endproc

	.globl	du
	.p2align	4, 0x90
	.type	du,@function
du:                                     # @du
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 208
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	opendir
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB3_15
# BB#1:                                 # %.preheader
	movq	%r13, %rdi
	callq	readdir
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_14
# BB#2:                                 # %.lr.ph
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rbx)
	je	.LBB3_13
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpb	$46, 19(%rbx)
	jne	.LBB3_8
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpb	$0, 20(%rbx)
	je	.LBB3_13
# BB#6:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpb	$46, 20(%rbx)
	jne	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpb	$0, 21(%rbx)
	je	.LBB3_13
	.p2align	4, 0x90
.LBB3_8:                                # %.thread
                                        #   in Loop: Header=BB3_3 Depth=1
	addq	$19, %rbx
	incl	(%r14)
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	strlen
	leaq	2(%rbp,%rax), %rdi
	callq	malloc
	movq	%rax, %rbp
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rcx
	callq	sprintf
	movl	$1, %edi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	__lxstat
	cmpl	$-1, %eax
	je	.LBB3_12
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	movl	32(%rsp), %eax
	movl	$61440, %ecx            # imm = 0xF000
	andl	%ecx, %eax
	cmpl	$16384, %eax            # imm = 0x4000
	jne	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_3 Depth=1
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	du
	jmp	.LBB3_12
.LBB3_11:                               #   in Loop: Header=BB3_3 Depth=1
	movq	56(%rsp), %rax
	movq	%rax, %rcx
	sarq	$63, %rcx
	shrq	$54, %rcx
	addq	%rax, %rcx
	sarq	$10, %rcx
	addq	%rcx, 8(%r14)
.LBB3_12:                               #   in Loop: Header=BB3_3 Depth=1
	movq	%rbp, %rdi
	callq	free
.LBB3_13:                               # %.backedge
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r13, %rdi
	callq	readdir
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB3_3
.LBB3_14:                               # %._crit_edge
	movq	%r13, %rdi
	callq	closedir
	xorl	%ebx, %ebx
	jmp	.LBB3_17
.LBB3_15:
	movl	$53, %ebx
	cmpw	$0, printinfected(%rip)
	jne	.LBB3_17
# BB#16:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
.LBB3_17:
	movl	%ebx, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	du, .Lfunc_end3-du
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"exclude-dir"
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"include-dir"
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s: Excluded\n"
	.size	.L.str.2, 14

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"max-dir-recursion"
	.size	.L.str.3, 18

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s/%s"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%s: Can't open directory.\n"
	.size	.L.str.7, 27

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"clamav"
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"ERROR: setgid(%d) failed.\n"
	.size	.L.str.9, 27

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"ERROR: setuid(%d) failed.\n"
	.size	.L.str.10, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
