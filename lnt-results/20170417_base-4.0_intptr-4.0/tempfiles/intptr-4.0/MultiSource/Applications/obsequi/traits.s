	.text
	.file	"traits.bc"
	.globl	write_node_info
	.p2align	4, 0x90
	.type	write_node_info,@function
write_node_info:                        # @write_node_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbp
	movq	trait_file(%rip), %r15
	testq	%r15, %r15
	jne	.LBB0_3
# BB#1:
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	callq	fopen
	movq	%rax, %r15
	movq	%r15, trait_file(%rip)
	testq	%r15, %r15
	je	.LBB0_2
.LBB0_3:
	cmpl	$1, %r14d
	movl	$86, %eax
	movl	$72, %ebx
	cmovel	%eax, %ebx
	movq	%rbp, %rdi
	callq	u64bit_to_string
	movq	%rax, %rcx
	xorl	%ebp, %ebp
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%ebx, %edx
	callq	fprintf
	movslq	%r14d, %r12
	movl	g_board_size(,%r12,4), %eax
	testl	%eax, %eax
	movl	$0, %edx
	jle	.LBB0_6
# BB#4:                                 # %.lr.ph.preheader.i
	movq	%r12, %rcx
	shlq	$7, %rcx
	movl	g_board(%rcx), %edi
	movl	g_board+4(%rcx), %r9d
	leaq	g_board+8(%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%r9d, %r8d
	movl	(%rcx), %r9d
	andl	%r9d, %edi
	movl	%edi, %ebx
	shrl	%ebx
	andl	%edi, %ebx
	movl	%r8d, %edi
	notl	%r8d
	notl	%ebx
	movl	%r8d, %esi
	shrl	%esi
	andl	%r8d, %esi
	andl	%ebx, %esi
	movzwl	%si, %ebx
	shrl	$16, %esi
	addl	countbits16(,%rbx,4), %edx
	addl	countbits16(,%rsi,4), %edx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB0_5
.LBB0_6:                                # %tr_total_non_safe_moves.exit
	movq	trait_file(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	%r14d, %r15d
	xorl	$1, %r15d
	movslq	%r15d, %rcx
	movl	g_board_size(,%rcx,4), %eax
	testl	%eax, %eax
	jle	.LBB0_9
# BB#7:                                 # %.lr.ph.preheader.i45
	shlq	$7, %rcx
	movl	g_board(%rcx), %edi
	movl	g_board+4(%rcx), %r8d
	leaq	g_board+8(%rcx), %rcx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph.i50
                                        # =>This Inner Loop Header: Depth=1
	movl	%r8d, %esi
	movl	(%rcx), %r8d
	andl	%r8d, %edi
	movl	%edi, %ebx
	shrl	%ebx
	andl	%edi, %ebx
	movl	%esi, %edi
	notl	%esi
	notl	%ebx
	movl	%esi, %edx
	shrl	%edx
	andl	%esi, %edx
	andl	%ebx, %edx
	movzwl	%dx, %esi
	shrl	$16, %edx
	addl	countbits16(,%rsi,4), %ebp
	addl	countbits16(,%rdx,4), %ebp
	addq	$4, %rcx
	decq	%rax
	jne	.LBB0_8
.LBB0_9:                                # %tr_total_non_safe_moves.exit52
	movq	trait_file(%rip), %rdi
	xorl	%ebx, %ebx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	movl	%r14d, %edi
	callq	tr_non_safe_moves_a_little_touchy
	movl	%eax, %ecx
	movq	trait_file(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	fprintf
	movl	%r15d, %edi
	callq	tr_non_safe_moves_a_little_touchy
	movl	%eax, %ecx
	movq	trait_file(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	fprintf
	movq	trait_file(%rip), %rdi
	movl	$.L.str.4, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	trait_file(%rip), %rdi
	movl	$.L.str.5, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	g_board_size(%rip), %eax
	testl	%eax, %eax
	movl	$0, %edx
	jle	.LBB0_15
# BB#10:                                # %.lr.ph.preheader.i35
	testb	$1, %al
	jne	.LBB0_12
# BB#11:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	cmpl	$1, %eax
	jne	.LBB0_14
	jmp	.LBB0_15
.LBB0_12:                               # %.lr.ph.i39.prol
	movl	g_board+4(%rip), %ecx
	notl	%ecx
	movzwl	%cx, %edx
	movl	countbits16(,%rdx,4), %edx
	shrl	$16, %ecx
	addl	countbits16(,%rcx,4), %edx
	movl	$1, %ecx
	cmpl	$1, %eax
	je	.LBB0_15
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph.i39
                                        # =>This Inner Loop Header: Depth=1
	movl	g_board+4(,%rcx,4), %esi
	notl	%esi
	movzwl	%si, %edi
	shrl	$16, %esi
	addl	countbits16(,%rdi,4), %edx
	addl	countbits16(,%rsi,4), %edx
	movl	g_board+8(,%rcx,4), %esi
	addq	$2, %rcx
	notl	%esi
	movzwl	%si, %edi
	shrl	$16, %esi
	addl	countbits16(,%rdi,4), %edx
	addl	countbits16(,%rsi,4), %edx
	cmpq	%rcx, %rax
	jne	.LBB0_14
.LBB0_15:                               # %tr_total_empty_squares.exit
	movq	trait_file(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	g_board_size(,%r12,4), %edx
	testl	%edx, %edx
	js	.LBB0_22
# BB#16:                                # %.lr.ph.preheader.i29
	leal	1(%rdx), %eax
	movq	%r12, %rsi
	shlq	$7, %rsi
	movl	g_board(%rsi), %ecx
	testb	$1, %al
	jne	.LBB0_18
# BB#17:
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	testl	%edx, %edx
	jne	.LBB0_20
	jmp	.LBB0_22
.LBB0_18:                               # %.lr.ph.i33.prol
	movl	g_board+4(%rsi), %ebp
	xorl	%ebp, %ecx
	movzwl	%cx, %edi
	movl	countbits16(,%rdi,4), %ebx
	shrl	$16, %ecx
	addl	countbits16(,%rcx,4), %ebx
	movl	$1, %edi
	movl	%ebp, %ecx
	testl	%edx, %edx
	je	.LBB0_22
.LBB0_20:                               # %.lr.ph.preheader.i29.new
	subq	%rdi, %rax
	leaq	g_board+8(%rsi,%rdi,4), %rdx
	.p2align	4, 0x90
.LBB0_21:                               # %.lr.ph.i33
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx), %esi
	movl	(%rdx), %edi
	xorl	%esi, %ecx
	movzwl	%cx, %ebp
	shrl	$16, %ecx
	addl	countbits16(,%rbp,4), %ebx
	addl	countbits16(,%rcx,4), %ebx
	xorl	%edi, %esi
	movzwl	%si, %ecx
	shrl	$16, %esi
	addl	countbits16(,%rcx,4), %ebx
	addl	countbits16(,%rsi,4), %ebx
	addq	$8, %rdx
	addq	$-2, %rax
	movl	%edi, %ecx
	jne	.LBB0_21
.LBB0_22:                               # %tr_border_length_col.exit
	movq	trait_file(%rip), %rdi
	xorl	%ebp, %ebp
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	g_board_size(,%r12,4), %ecx
	testl	%ecx, %ecx
	js	.LBB0_29
# BB#23:                                # %.lr.ph.preheader.i21
	leal	1(%rcx), %eax
	testb	$1, %al
	jne	.LBB0_25
# BB#24:
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	testl	%ecx, %ecx
	jne	.LBB0_27
	jmp	.LBB0_29
.LBB0_25:                               # %.lr.ph.i25.prol
	movq	%r12, %rdx
	shlq	$7, %rdx
	movl	g_board+4(%rdx), %edx
	movl	%edx, %esi
	shrl	%esi
	andl	$2147483647, %edx       # imm = 0x7FFFFFFF
	xorl	%esi, %edx
	movzwl	%dx, %esi
	movl	countbits16(,%rsi,4), %ebp
	shrl	$16, %edx
	addl	countbits16(,%rdx,4), %ebp
	movl	$1, %edx
	testl	%ecx, %ecx
	je	.LBB0_29
.LBB0_27:                               # %.lr.ph.preheader.i21.new
	subq	%rdx, %rax
	shlq	$7, %r12
	leaq	g_board+8(%r12,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph.i25
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx), %esi
	movl	(%rcx), %edx
	movl	%esi, %edi
	shrl	%edi
	andl	$2147483647, %esi       # imm = 0x7FFFFFFF
	xorl	%edi, %esi
	movzwl	%si, %edi
	shrl	$16, %esi
	addl	countbits16(,%rdi,4), %ebp
	addl	countbits16(,%rsi,4), %ebp
	movl	%edx, %esi
	shrl	%esi
	andl	$2147483647, %edx       # imm = 0x7FFFFFFF
	xorl	%esi, %edx
	movzwl	%dx, %esi
	shrl	$16, %edx
	addl	countbits16(,%rsi,4), %ebp
	addl	countbits16(,%rdx,4), %ebp
	addq	$8, %rcx
	addq	$-2, %rax
	jne	.LBB0_28
.LBB0_29:                               # %tr_border_length_row.exit
	movq	trait_file(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	movq	trait_file(%rip), %rsi
	movl	$10, %edi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fputc                   # TAILCALL
.LBB0_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movq	trait_file(%rip), %r15
	jmp	.LBB0_3
.Lfunc_end0:
	.size	write_node_info, .Lfunc_end0-write_node_info
	.cfi_endproc

	.p2align	4, 0x90
	.type	tr_non_safe_moves_a_little_touchy,@function
tr_non_safe_moves_a_little_touchy:      # @tr_non_safe_moves_a_little_touchy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movslq	%r14d, %rax
	movl	g_board_size(,%rax,4), %r10d
	testl	%r10d, %r10d
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	shlq	$7, %rax
	movl	g_board(%rax), %ebx
	movl	g_board+4(%rax), %r8d
	leaq	g_board+8(%rax), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r8d, %edi
	movl	(%rdx), %r8d
	orl	%r8d, %ebx
	movl	%edi, %ebp
	shrl	%ebp
	leal	(%rdi,%rdi), %esi
	leal	(,%rdi,4), %r9d
	movl	%edi, %ecx
	orl	%ebx, %ecx
	addl	%ebx, %ebx
	orl	%ecx, %ebx
	orl	%ebp, %esi
	orl	%r9d, %esi
	orl	%ebx, %esi
	notl	%esi
	movzwl	%si, %ecx
	shrl	$16, %esi
	addl	countbits16(,%rcx,4), %eax
	addl	countbits16(,%rsi,4), %eax
	addq	$4, %rdx
	decq	%r10
	movl	%edi, %ebx
	jne	.LBB1_3
# BB#4:                                 # %._crit_edge
	cmpl	$-1, %eax
	jne	.LBB1_5
# BB#6:
	movl	%r14d, %edi
	callq	print_board
	movl	$.L.str.7, %edi
	movl	$-1, %edx
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movl	$1, %edi
	callq	exit
.LBB1_1:
	xorl	%eax, %eax
.LBB1_5:                                # %._crit_edge.thread
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	tr_non_safe_moves_a_little_touchy, .Lfunc_end1-tr_non_safe_moves_a_little_touchy
	.cfi_endproc

	.type	trait_file,@object      # @trait_file
	.local	trait_file
	.comm	trait_file,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"trait_file"
	.size	.L.str, 11

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"w"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Couldn't open \"trait_file\".\n"
	.size	.L.str.2, 29

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%c %15s :"
	.size	.L.str.3, 10

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" %2d"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" %2d :"
	.size	.L.str.5, 7

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%d %d\n"
	.size	.L.str.7, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
