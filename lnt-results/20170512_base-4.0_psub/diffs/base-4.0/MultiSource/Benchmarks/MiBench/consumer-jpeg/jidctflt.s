	.text
	.file	"jidctflt.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1068827891              # float 1.41421354
.LCPI0_1:
	.long	1072464734              # float 1.84775901
.LCPI0_2:
	.long	1066044372              # float 1.08239222
.LCPI0_3:
	.long	3223797109              # float -2.61312604
	.text
	.globl	jpeg_idct_float
	.p2align	4, 0x90
	.type	jpeg_idct_float,@function
jpeg_idct_float:                        # @jpeg_idct_float
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, -116(%rsp)        # 4-byte Spill
	movq	408(%rdi), %r8
	leaq	-112(%rsp), %rdi
	movl	$224, %eax
	addq	88(%rsi), %rax
	addq	$112, %rdx
	movl	$9, %r12d
	movss	.LCPI0_0(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movss	.LCPI0_1(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI0_2(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movss	.LCPI0_3(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movzwl	-96(%rdx), %r10d
	movzwl	-80(%rdx), %ebx
	movl	%ebx, %esi
	orl	%r10d, %esi
	movzwl	-64(%rdx), %r11d
	movzwl	-48(%rdx), %r13d
	movl	%r11d, %ebp
	orl	%r13d, %ebp
	orl	%esi, %ebp
	movzwl	-32(%rdx), %r9d
	movzwl	-16(%rdx), %esi
	movl	%r9d, %r14d
	orl	%esi, %r14d
	orl	%ebp, %r14d
	movzwl	(%rdx), %r15d
	movswl	-112(%rdx), %ebp
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebp, %xmm4
	mulss	-224(%rax), %xmm4
	orw	%r15w, %r14w
	je	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movswl	%bx, %ebx
	cvtsi2ssl	%ebx, %xmm0
	mulss	-160(%rax), %xmm0
	movswl	%r13w, %ebx
	cvtsi2ssl	%ebx, %xmm1
	mulss	-96(%rax), %xmm1
	movswl	%si, %esi
	cvtsi2ssl	%esi, %xmm2
	mulss	-32(%rax), %xmm2
	movaps	%xmm4, %xmm5
	addss	%xmm1, %xmm5
	subss	%xmm1, %xmm4
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	subss	%xmm2, %xmm0
	mulss	%xmm11, %xmm0
	subss	%xmm1, %xmm0
	movaps	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	subss	%xmm1, %xmm5
	movaps	%xmm4, %xmm12
	addss	%xmm0, %xmm12
	subss	%xmm0, %xmm4
	movswl	%r10w, %esi
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%esi, %xmm1
	mulss	-192(%rax), %xmm1
	movswl	%r11w, %esi
	cvtsi2ssl	%esi, %xmm7
	mulss	-128(%rax), %xmm7
	movswl	%r9w, %esi
	cvtsi2ssl	%esi, %xmm3
	mulss	-64(%rax), %xmm3
	movswl	%r15w, %esi
	cvtsi2ssl	%esi, %xmm6
	mulss	(%rax), %xmm6
	movaps	%xmm7, %xmm0
	addss	%xmm3, %xmm0
	subss	%xmm7, %xmm3
	movaps	%xmm1, %xmm7
	addss	%xmm6, %xmm7
	subss	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	addss	%xmm7, %xmm6
	subss	%xmm0, %xmm7
	mulss	%xmm11, %xmm7
	movaps	%xmm3, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm8, %xmm0
	mulss	%xmm9, %xmm1
	subss	%xmm0, %xmm1
	mulss	%xmm10, %xmm3
	addss	%xmm0, %xmm3
	subss	%xmm6, %xmm3
	subss	%xmm3, %xmm7
	addss	%xmm7, %xmm1
	movaps	%xmm2, %xmm0
	addss	%xmm6, %xmm0
	movss	%xmm0, (%rdi)
	subss	%xmm6, %xmm2
	movss	%xmm2, 224(%rdi)
	movaps	%xmm4, %xmm0
	movaps	%xmm4, %xmm2
	movaps	%xmm12, %xmm4
	addss	%xmm3, %xmm4
	subss	%xmm3, %xmm12
	addss	%xmm7, %xmm0
	subss	%xmm7, %xmm2
	movaps	%xmm5, %xmm3
	addss	%xmm1, %xmm3
	subss	%xmm1, %xmm5
	movl	$24, %esi
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	movss	%xmm4, (%rdi)
	movss	%xmm4, 96(%rdi)
	movaps	%xmm4, %xmm12
	movaps	%xmm4, %xmm0
	movaps	%xmm4, %xmm2
	movaps	%xmm4, %xmm3
	movl	$56, %esi
	movaps	%xmm4, %xmm5
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movss	%xmm4, 32(%rdi)
	movss	%xmm12, 192(%rdi)
	movss	%xmm0, 64(%rdi)
	movss	%xmm2, 160(%rdi)
	movss	%xmm3, 128(%rdi)
	movss	%xmm5, (%rdi,%rsi,4)
	addq	$4, %rdi
	decl	%r12d
	addq	$4, %rax
	addq	$2, %rdx
	cmpl	$1, %r12d
	jg	.LBB0_1
# BB#5:                                 # %.preheader
	movl	-116(%rsp), %eax        # 4-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx), %rsi
	movss	-112(%rsp,%rdx,4), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movss	-96(%rsp,%rdx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm12
	addss	%xmm0, %xmm12
	subss	%xmm0, %xmm13
	movss	-104(%rsp,%rdx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	-88(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	subss	%xmm1, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm12, %xmm4
	movss	-92(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movss	-100(%rsp,%rdx,4), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	addss	%xmm2, %xmm4
	addss	%xmm3, %xmm5
	subss	%xmm3, %xmm1
	movss	-108(%rsp,%rdx,4), %xmm6 # xmm6 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm12
	movaps	%xmm13, %xmm14
	movss	-84(%rsp,%rdx,4), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	addss	%xmm3, %xmm7
	subss	%xmm3, %xmm6
	movaps	%xmm5, %xmm3
	subss	%xmm2, %xmm0
	addss	%xmm7, %xmm3
	subss	%xmm5, %xmm7
	mulss	%xmm11, %xmm7
	movaps	%xmm1, %xmm2
	addss	%xmm0, %xmm14
	addss	%xmm6, %xmm2
	mulss	%xmm8, %xmm2
	mulss	%xmm9, %xmm6
	subss	%xmm0, %xmm13
	subss	%xmm2, %xmm6
	mulss	%xmm10, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm4, %xmm0
	subss	%xmm3, %xmm1
	addss	%xmm3, %xmm0
	cvttss2si	%xmm0, %rdi
	addl	$4, %edi
	subss	%xmm1, %xmm7
	shrl	$3, %edi
	andl	$1023, %edi             # imm = 0x3FF
	movzbl	128(%r8,%rdi), %ebx
	movb	%bl, (%rsi,%rax)
	subss	%xmm3, %xmm4
	cvttss2si	%xmm4, %rdi
	addl	$4, %edi
	addss	%xmm7, %xmm6
	shrl	$3, %edi
	andl	$1023, %edi             # imm = 0x3FF
	movzbl	128(%r8,%rdi), %ebx
	movaps	%xmm14, %xmm0
	movb	%bl, 7(%rsi,%rax)
	addss	%xmm1, %xmm0
	cvttss2si	%xmm0, %rdi
	addl	$4, %edi
	shrl	$3, %edi
	andl	$1023, %edi             # imm = 0x3FF
	subss	%xmm1, %xmm14
	cvttss2si	%xmm14, %rbp
	movzbl	128(%r8,%rdi), %ebx
	addl	$4, %ebp
	shrl	$3, %ebp
	andl	$1023, %ebp             # imm = 0x3FF
	movb	%bl, 1(%rsi,%rax)
	movzbl	128(%r8,%rbp), %ebx
	movaps	%xmm13, %xmm0
	addss	%xmm7, %xmm0
	cvttss2si	%xmm0, %rdi
	movb	%bl, 6(%rsi,%rax)
	addl	$4, %edi
	shrl	$3, %edi
	andl	$1023, %edi             # imm = 0x3FF
	movzbl	128(%r8,%rdi), %ebx
	subss	%xmm7, %xmm13
	cvttss2si	%xmm13, %rdi
	addl	$4, %edi
	movb	%bl, 2(%rsi,%rax)
	shrl	$3, %edi
	andl	$1023, %edi             # imm = 0x3FF
	movaps	%xmm12, %xmm0
	addss	%xmm6, %xmm0
	movzbl	128(%r8,%rdi), %ebx
	cvttss2si	%xmm0, %rdi
	addl	$4, %edi
	shrl	$3, %edi
	movb	%bl, 5(%rsi,%rax)
	andl	$1023, %edi             # imm = 0x3FF
	subss	%xmm6, %xmm12
	cvttss2si	%xmm12, %rbp
	movzbl	128(%r8,%rdi), %ebx
	addl	$4, %ebp
	shrl	$3, %ebp
	andl	$1023, %ebp             # imm = 0x3FF
	movb	%bl, 4(%rsi,%rax)
	movzbl	128(%r8,%rbp), %ebx
	addq	$8, %rdx
	cmpq	$64, %rdx
	movb	%bl, 3(%rsi,%rax)
	jne	.LBB0_6
# BB#7:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_idct_float, .Lfunc_end0-jpeg_idct_float
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
