	.text
	.file	"laplace.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.text
	.globl	GenerateSeqLaplacian
	.p2align	4, 0x90
	.type	GenerateSeqLaplacian,@function
GenerateSeqLaplacian:                   # @GenerateSeqLaplacian
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r8, 120(%rsp)          # 8-byte Spill
	movq	%rcx, %r12
	movl	%edi, %r13d
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%esi, %r14d
	imull	%r13d, %r14d
	movq	%r14, 96(%rsp)          # 8-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	movl	%edx, 12(%rsp)          # 4-byte Spill
	imull	%edx, %r14d
	leal	1(%r14), %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$8, %esi
	movl	%r14d, %edi
	callq	hypre_CAlloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$8, %esi
	movl	%r14d, %edi
	callq	hypre_CAlloc
	movq	%rax, %r15
	movl	$8, %esi
	movl	%r14d, %edi
	callq	hypre_CAlloc
	testl	%r14d, %r14d
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
	jle	.LBB0_18
# BB#1:                                 # %.lr.ph250.preheader
	movl	%r14d, %ebp
	cmpl	$4, %r14d
	jae	.LBB0_3
# BB#2:
	xorl	%ecx, %ecx
	jmp	.LBB0_12
.LBB0_3:                                # %min.iters.checked
	movl	%r14d, %r9d
	andl	$3, %r9d
	movq	%rbp, %r8
	subq	%r9, %r8
	je	.LBB0_4
# BB#5:                                 # %vector.memcheck
	leaq	(%r15,%rbp,8), %rcx
	movq	%r15, %rdx
	leaq	(%rax,%rbp,8), %r15
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rbp,8), %rdi
	cmpq	%r15, %rdx
	sbbb	%r10b, %r10b
	cmpq	%rcx, %rax
	sbbb	%bl, %bl
	andb	%r10b, %bl
	cmpq	%rdi, %rdx
	sbbb	%dl, %dl
	cmpq	%rcx, %rsi
	sbbb	%r11b, %r11b
	cmpq	%rdi, %rax
	sbbb	%r10b, %r10b
	cmpq	%r15, %rsi
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %bl
	jne	.LBB0_6
# BB#7:                                 # %vector.memcheck
	andb	%r11b, %dl
	andb	$1, %dl
	jne	.LBB0_6
# BB#8:                                 # %vector.memcheck
	andb	%sil, %r10b
	andb	$1, %r10b
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	jne	.LBB0_12
# BB#9:                                 # %vector.body.preheader
	leaq	16(%r15), %rcx
	leaq	16(%rax), %rdi
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	16(%rdx), %rbx
	xorpd	%xmm0, %xmm0
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00]
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB0_10:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -16(%rcx)
	movupd	%xmm0, (%rcx)
	movupd	%xmm0, -16(%rdi)
	movupd	%xmm0, (%rdi)
	movups	%xmm1, -16(%rbx)
	movups	%xmm1, (%rbx)
	addq	$32, %rcx
	addq	$32, %rdi
	addq	$32, %rbx
	addq	$-4, %rsi
	jne	.LBB0_10
# BB#11:                                # %middle.block
	testl	%r9d, %r9d
	movq	%r8, %rcx
	jne	.LBB0_12
	jmp	.LBB0_18
.LBB0_4:
	xorl	%ecx, %ecx
	jmp	.LBB0_12
.LBB0_6:
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
.LBB0_12:                               # %.lr.ph250.preheader327
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB0_15
# BB#13:                                # %.lr.ph250.prol.preheader
	negq	%rsi
	movabsq	$4607182418800017408, %rdi # imm = 0x3FF0000000000000
	movq	48(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph250.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, (%r15,%rcx,8)
	movq	$0, (%rax,%rcx,8)
	movq	%rdi, (%rbx,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_14
.LBB0_15:                               # %.lr.ph250.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB0_18
# BB#16:                                # %.lr.ph250.preheader327.new
	subq	%rcx, %rbp
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	24(%rdx,%rcx,8), %rdx
	leaq	24(%rax,%rcx,8), %rsi
	leaq	24(%r15,%rcx,8), %rcx
	movabsq	$4607182418800017408, %rdi # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph250
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, -24(%rcx)
	movq	$0, -24(%rsi)
	movq	%rdi, -24(%rdx)
	movq	$0, -16(%rcx)
	movq	$0, -16(%rsi)
	movq	%rdi, -16(%rdx)
	movq	$0, -8(%rcx)
	movq	$0, -8(%rsi)
	movq	%rdi, -8(%rdx)
	movq	$0, (%rcx)
	movq	$0, (%rsi)
	movq	%rdi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$32, %rcx
	addq	$-4, %rbp
	jne	.LBB0_17
.LBB0_18:                               # %._crit_edge251
	movq	%r14, 72(%rsp)          # 8-byte Spill
	movq	24(%rsp), %r8           # 8-byte Reload
	movl	$0, (%r8)
	movl	12(%rsp), %ebp          # 4-byte Reload
	testl	%ebp, %ebp
	movl	8(%rsp), %ecx           # 4-byte Reload
	jle	.LBB0_42
# BB#19:                                # %.preheader212.lr.ph
	leal	-1(%r13), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	incq	%rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	andl	$1, %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	cmpl	$1, %r13d
	setg	%al
	movl	%eax, 116(%rsp)         # 4-byte Spill
	movl	%r13d, %r15d
	leaq	4(%r8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB0_20:                               # %.preheader212
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_23 Depth 2
                                        #       Child Loop BB0_31 Depth 3
                                        #       Child Loop BB0_37 Depth 3
                                        #       Child Loop BB0_27 Depth 3
	testl	%ecx, %ecx
	jle	.LBB0_21
# BB#22:                                # %.preheader211.lr.ph
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpl	$1, %r11d
	movl	$1, %eax
	sbbl	$-1, %eax
	incl	%r11d
	xorl	%r9d, %r9d
	cmpl	%ebp, %r11d
	setl	%r9b
	incl	%r9d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_23:                               # %.preheader211
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_31 Depth 3
                                        #       Child Loop BB0_37 Depth 3
                                        #       Child Loop BB0_27 Depth 3
	testl	%r13d, %r13d
	jle	.LBB0_24
# BB#25:                                # %.lr.ph235
                                        #   in Loop: Header=BB0_23 Depth=2
	leal	1(%rdx), %r10d
	movslq	%r14d, %rdi
	testl	%edx, %edx
	je	.LBB0_26
# BB#29:                                # %.lr.ph235.split
                                        #   in Loop: Header=BB0_23 Depth=2
	cmpl	%ecx, %r10d
	jge	.LBB0_30
# BB#33:                                # %.lr.ph235.split.split.us.preheader
                                        #   in Loop: Header=BB0_23 Depth=2
	xorl	%ebx, %ebx
	cmpl	$0, 88(%rsp)            # 4-byte Folded Reload
	movq	%r8, %rcx
	movl	-4(%rcx,%rdi,4), %r8d
	je	.LBB0_35
# BB#34:                                # %.lr.ph235.split.split.us.prol
                                        #   in Loop: Header=BB0_23 Depth=2
	addl	%eax, %r8d
	addl	116(%rsp), %r8d         # 4-byte Folded Reload
	leal	1(%r9,%r8), %r8d
	movl	%r8d, (%rcx,%rdi,4)
	incq	%rdi
	movl	$1, %ebx
.LBB0_35:                               # %.lr.ph235.split.split.us.prol.loopexit
                                        #   in Loop: Header=BB0_23 Depth=2
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	je	.LBB0_38
# BB#36:                                # %.lr.ph235.split.split.us.preheader.new
                                        #   in Loop: Header=BB0_23 Depth=2
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph235.split.split.us
                                        #   Parent Loop BB0_20 Depth=1
                                        #     Parent Loop BB0_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addl	%eax, %r8d
	xorl	%ecx, %ecx
	testl	%ebx, %ebx
	setne	%cl
	addl	%r8d, %ecx
	leal	1(%rbx), %edx
	xorl	%esi, %esi
	cmpl	%r13d, %edx
	setl	%sil
	addl	%ecx, %esi
	leal	(%rsi,%r9), %ecx
	leal	1(%rax,%rcx), %ecx
	addl	$2, %ebx
	xorl	%edx, %edx
	cmpl	%r13d, %ebx
	setl	%dl
	addl	%ecx, %edx
	cmpl	%r13d, %ebx
	leal	1(%r9,%rsi), %ecx
	movl	%ecx, -4(%rdi)
	leal	2(%r9,%rdx), %r8d
	movl	%r8d, (%rdi)
	leaq	8(%rdi), %rdi
	jne	.LBB0_37
.LBB0_38:                               # %._crit_edge236.loopexit253
                                        #   in Loop: Header=BB0_23 Depth=2
	addl	16(%rsp), %r14d         # 4-byte Folded Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	jmp	.LBB0_39
	.p2align	4, 0x90
.LBB0_24:                               # %.preheader211.._crit_edge236_crit_edge
                                        #   in Loop: Header=BB0_23 Depth=2
	incl	%edx
	movl	%edx, %r10d
	jmp	.LBB0_40
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph235.split.us.preheader
                                        #   in Loop: Header=BB0_23 Depth=2
	movl	%r14d, 104(%rsp)        # 4-byte Spill
	movl	-4(%r8,%rdi,4), %edx
	leaq	(%r8,%rdi,4), %rdi
	xorl	%ebx, %ebx
	movq	%r13, %r14
	movl	%ebp, %r13d
	movl	%ecx, %r8d
	.p2align	4, 0x90
.LBB0_27:                               # %.lr.ph235.split.us
                                        #   Parent Loop BB0_20 Depth=1
                                        #     Parent Loop BB0_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rdx,%rax), %ecx
	testl	%ebx, %ebx
	leal	1(%rdx,%rax), %edx
	cmovel	%ecx, %edx
	leaq	1(%rbx), %rcx
	leal	1(%rdx), %esi
	cmpl	%r14d, %ecx
	cmovgel	%edx, %esi
	leal	1(%rsi), %ebp
	cmpl	%r8d, %r10d
	cmovgel	%esi, %ebp
	leal	1(%rbp), %edx
	cmpl	%r13d, %r11d
	cmovgel	%ebp, %edx
	movl	%edx, (%rdi,%rbx,4)
	cmpl	%ecx, %r15d
	movq	%rcx, %rbx
	jne	.LBB0_27
# BB#28:                                # %._crit_edge236.loopexit
                                        #   in Loop: Header=BB0_23 Depth=2
	movl	104(%rsp), %edx         # 4-byte Reload
	addl	16(%rsp), %edx          # 4-byte Folded Reload
	movl	%r13d, %ebp
	movl	%r8d, %ecx
	movq	%r14, %r13
	movl	%edx, %r14d
	movq	24(%rsp), %r8           # 8-byte Reload
	jmp	.LBB0_40
.LBB0_30:                               # %.lr.ph235.split.split.preheader
                                        #   in Loop: Header=BB0_23 Depth=2
	movl	-4(%r8,%rdi,4), %edx
	leaq	(%r8,%rdi,4), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_31:                               # %.lr.ph235.split.split
                                        #   Parent Loop BB0_20 Depth=1
                                        #     Parent Loop BB0_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	1(%rdx,%rax), %ecx
	testl	%ebx, %ebx
	leal	2(%rdx,%rax), %edx
	cmovel	%ecx, %edx
	leaq	1(%rbx), %rcx
	leal	1(%rdx), %esi
	cmpl	%r13d, %ecx
	cmovgel	%edx, %esi
	leal	1(%rsi), %edx
	cmpl	%ebp, %r11d
	cmovgel	%esi, %edx
	movl	%edx, (%rdi,%rbx,4)
	cmpl	%ecx, %r15d
	movq	%rcx, %rbx
	jne	.LBB0_31
# BB#32:                                # %._crit_edge236.loopexit254
                                        #   in Loop: Header=BB0_23 Depth=2
	addl	16(%rsp), %r14d         # 4-byte Folded Reload
.LBB0_39:                               # %._crit_edge236
                                        #   in Loop: Header=BB0_23 Depth=2
	movl	8(%rsp), %ecx           # 4-byte Reload
.LBB0_40:                               # %._crit_edge236
                                        #   in Loop: Header=BB0_23 Depth=2
	cmpl	%ecx, %r10d
	movl	%r10d, %edx
	jne	.LBB0_23
	jmp	.LBB0_41
	.p2align	4, 0x90
.LBB0_21:                               # %.preheader212.._crit_edge243_crit_edge
                                        #   in Loop: Header=BB0_20 Depth=1
	incl	%r11d
.LBB0_41:                               # %._crit_edge243
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpl	%ebp, %r11d
	jne	.LBB0_20
.LBB0_42:                               # %._crit_edge247
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movslq	72(%rsp), %rbx          # 4-byte Folded Reload
	movl	(%r8,%rbx,4), %edi
	movl	$4, %esi
	movq	%r8, %r14
	callq	hypre_CAlloc
	movq	%rax, %r13
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movl	(%r14,%rbx,4), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movl	8(%rsp), %ecx           # 4-byte Reload
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB0_81
# BB#43:                                # %.preheader210.lr.ph
	movq	40(%rsp), %r10          # 8-byte Reload
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	negl	%r10d
	movq	96(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	negl	%eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_44:                               # %.preheader210
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_47 Depth 2
                                        #       Child Loop BB0_51 Depth 3
                                        #       Child Loop BB0_62 Depth 3
	testl	%ecx, %ecx
	jle	.LBB0_45
# BB#46:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB0_44 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	leal	1(%rsi), %r11d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_47:                               # %.preheader
                                        #   Parent Loop BB0_44 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_51 Depth 3
                                        #       Child Loop BB0_62 Depth 3
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_48
# BB#49:                                # %.lr.ph219
                                        #   in Loop: Header=BB0_47 Depth=2
	leal	1(%rsi), %r8d
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	je	.LBB0_61
# BB#50:                                # %.lr.ph219.split.preheader
                                        #   in Loop: Header=BB0_47 Depth=2
	movq	88(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rdx), %edi
	leal	(%r10,%rdx), %r9d
	movq	40(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rdx), %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rdx), %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB0_51:                               # %.lr.ph219.split
                                        #   Parent Loop BB0_44 Depth=1
                                        #     Parent Loop BB0_47 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	2(%rax), %r15d
	testl	%esi, %esi
	leal	-1(%rdx,%r14), %ebp
	movslq	%eax, %rcx
	movl	%ebp, (%r13,%rcx,4)
	movq	(%r12), %rbp
	movq	%rbp, (%rbx,%rcx,8)
	leal	-1(%rdi,%r14), %ebp
	movl	%ebp, 4(%r13,%rcx,4)
	movq	24(%r12), %rbp
	movq	%rbp, 8(%rbx,%rcx,8)
	je	.LBB0_53
# BB#52:                                #   in Loop: Header=BB0_51 Depth=3
	leal	-1(%r9,%r14), %ecx
	movslq	%r15d, %rbp
	movl	%ecx, (%r13,%rbp,4)
	movq	16(%r12), %rcx
	addl	$3, %eax
	movq	%rcx, (%rbx,%rbp,8)
	movl	%eax, %r15d
.LBB0_53:                               #   in Loop: Header=BB0_51 Depth=3
	cmpl	$1, %r14d
	movl	12(%rsp), %ebp          # 4-byte Reload
	je	.LBB0_55
# BB#54:                                #   in Loop: Header=BB0_51 Depth=3
	leal	-2(%rdx,%r14), %eax
	movslq	%r15d, %rcx
	movl	%eax, (%r13,%rcx,4)
	movq	8(%r12), %rax
	leal	1(%rcx), %r15d
	movq	%rax, (%rbx,%rcx,8)
.LBB0_55:                               #   in Loop: Header=BB0_51 Depth=3
	cmpl	40(%rsp), %r14d         # 4-byte Folded Reload
	jge	.LBB0_57
# BB#56:                                #   in Loop: Header=BB0_51 Depth=3
	leal	(%rdx,%r14), %eax
	movslq	%r15d, %rcx
	movl	%eax, (%r13,%rcx,4)
	movq	8(%r12), %rax
	leal	1(%rcx), %r15d
	movq	%rax, (%rbx,%rcx,8)
.LBB0_57:                               #   in Loop: Header=BB0_51 Depth=3
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	%eax, %r8d
	jge	.LBB0_59
# BB#58:                                #   in Loop: Header=BB0_51 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax,%r14), %eax
	movslq	%r15d, %rcx
	movl	%eax, (%r13,%rcx,4)
	movq	16(%r12), %rax
	leal	1(%rcx), %r15d
	movq	%rax, (%rbx,%rcx,8)
.LBB0_59:                               #   in Loop: Header=BB0_51 Depth=3
	cmpl	%ebp, %r11d
	jge	.LBB0_60
# BB#75:                                #   in Loop: Header=BB0_51 Depth=3
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax,%r14), %eax
	movslq	%r15d, %rcx
	movl	%eax, (%r13,%rcx,4)
	movq	24(%r12), %rbp
	leal	1(%rcx), %eax
	movq	%rbp, (%rbx,%rcx,8)
	jmp	.LBB0_76
	.p2align	4, 0x90
.LBB0_60:                               #   in Loop: Header=BB0_51 Depth=3
	movl	%r15d, %eax
.LBB0_76:                               #   in Loop: Header=BB0_51 Depth=3
	leal	1(%r10,%r14), %ecx
	leal	1(%r14), %ebp
	cmpl	$1, %ecx
	movl	%ebp, %r14d
	jne	.LBB0_51
# BB#77:                                # %._crit_edge220.loopexit252
                                        #   in Loop: Header=BB0_47 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	movl	12(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB0_78
	.p2align	4, 0x90
.LBB0_48:                               # %.preheader.._crit_edge220_crit_edge
                                        #   in Loop: Header=BB0_47 Depth=2
	incl	%esi
	movl	%esi, %r8d
	jmp	.LBB0_79
	.p2align	4, 0x90
.LBB0_61:                               # %.lr.ph219.split.us.preheader
                                        #   in Loop: Header=BB0_47 Depth=2
	movq	96(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rdx), %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rdx), %r9d
	leal	(%r10,%rdx), %r14d
	movl	$1, %edi
	.p2align	4, 0x90
.LBB0_62:                               # %.lr.ph219.split.us
                                        #   Parent Loop BB0_44 Depth=1
                                        #     Parent Loop BB0_47 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	1(%rax), %ecx
	testl	%esi, %esi
	leal	-1(%rdx,%rdi), %ebp
	movslq	%eax, %r15
	movl	%ebp, (%r13,%r15,4)
	movq	(%r12), %rbp
	movq	%rbp, (%rbx,%r15,8)
	je	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_62 Depth=3
	leal	-1(%r14,%rdi), %ebp
	movslq	%ecx, %rcx
	movl	%ebp, (%r13,%rcx,4)
	movq	16(%r12), %rbp
	addl	$2, %eax
	movq	%rbp, (%rbx,%rcx,8)
	movl	%eax, %ecx
.LBB0_64:                               #   in Loop: Header=BB0_62 Depth=3
	cmpl	$1, %edi
	je	.LBB0_66
# BB#65:                                #   in Loop: Header=BB0_62 Depth=3
	leal	-2(%rdx,%rdi), %eax
	movslq	%ecx, %rbp
	movl	%eax, (%r13,%rbp,4)
	movq	8(%r12), %rax
	leal	1(%rbp), %ecx
	movq	%rax, (%rbx,%rbp,8)
.LBB0_66:                               #   in Loop: Header=BB0_62 Depth=3
	cmpl	40(%rsp), %edi          # 4-byte Folded Reload
	jge	.LBB0_68
# BB#67:                                #   in Loop: Header=BB0_62 Depth=3
	leal	(%rdx,%rdi), %eax
	movslq	%ecx, %rbp
	movl	%eax, (%r13,%rbp,4)
	movq	8(%r12), %rax
	leal	1(%rbp), %ecx
	movq	%rax, (%rbx,%rbp,8)
.LBB0_68:                               #   in Loop: Header=BB0_62 Depth=3
	cmpl	8(%rsp), %r8d           # 4-byte Folded Reload
	jge	.LBB0_70
# BB#69:                                #   in Loop: Header=BB0_62 Depth=3
	leal	-1(%r9,%rdi), %eax
	movslq	%ecx, %rbp
	movl	%eax, (%r13,%rbp,4)
	movq	16(%r12), %rax
	leal	1(%rbp), %ecx
	movq	%rax, (%rbx,%rbp,8)
.LBB0_70:                               #   in Loop: Header=BB0_62 Depth=3
	movl	12(%rsp), %ebp          # 4-byte Reload
	cmpl	%ebp, %r11d
	jge	.LBB0_71
# BB#72:                                #   in Loop: Header=BB0_62 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax,%rdi), %eax
	movslq	%ecx, %rcx
	movl	%eax, (%r13,%rcx,4)
	movq	24(%r12), %rbp
	leal	1(%rcx), %eax
	movq	%rbp, (%rbx,%rcx,8)
	movl	12(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB0_73
	.p2align	4, 0x90
.LBB0_71:                               #   in Loop: Header=BB0_62 Depth=3
	movl	%ecx, %eax
.LBB0_73:                               #   in Loop: Header=BB0_62 Depth=3
	leal	1(%r10,%rdi), %ecx
	leal	1(%rdi), %edi
	cmpl	$1, %ecx
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	jne	.LBB0_62
# BB#74:                                # %._crit_edge220.loopexit
                                        #   in Loop: Header=BB0_47 Depth=2
	movl	%r9d, %edx
.LBB0_78:                               # %._crit_edge220
                                        #   in Loop: Header=BB0_47 Depth=2
	movl	8(%rsp), %ecx           # 4-byte Reload
.LBB0_79:                               # %._crit_edge220
                                        #   in Loop: Header=BB0_47 Depth=2
	cmpl	%ecx, %r8d
	movl	%r8d, %esi
	jne	.LBB0_47
	jmp	.LBB0_80
	.p2align	4, 0x90
.LBB0_45:                               # %.preheader210.._crit_edge226_crit_edge
                                        #   in Loop: Header=BB0_44 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	incl	%esi
	movl	%esi, %r11d
.LBB0_80:                               # %._crit_edge226
                                        #   in Loop: Header=BB0_44 Depth=1
	cmpl	%ebp, %r11d
	movl	%r11d, %esi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	jne	.LBB0_44
.LBB0_81:                               # %._crit_edge232
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	(%rax,%rcx,4), %edx
	movq	72(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %edi
	movl	%ebp, %esi
	callq	hypre_CSRMatrixCreate
	movq	%rax, %r12
	movl	%ebp, %edi
	callq	hypre_SeqVectorCreate
	movq	%rax, %r14
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r14)
	movl	%ebp, %edi
	callq	hypre_SeqVectorCreate
	movq	%rax, %r15
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	testl	%ebp, %ebp
	movq	64(%rsp), %r10          # 8-byte Reload
	jle	.LBB0_90
# BB#82:                                # %.lr.ph215.preheader
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	movl	72(%rsp), %r8d          # 4-byte Reload
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB0_84:                               # %.lr.ph215
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_87 Depth 2
                                        #     Child Loop BB0_89 Depth 2
	movq	%r9, %rsi
	movl	%ecx, %eax
	leaq	1(%rsi), %r9
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	4(%rcx,%rsi,4), %ecx
	cmpl	%ecx, %eax
	jge	.LBB0_83
# BB#85:                                # %.lr.ph
                                        #   in Loop: Header=BB0_84 Depth=1
	movslq	%ecx, %rdi
	movslq	%eax, %rbp
	movsd	(%r10,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movl	%ecx, %edx
	subl	%eax, %edx
	leaq	-1(%rdi), %rax
	subq	%rbp, %rax
	andq	$3, %rdx
	je	.LBB0_88
# BB#86:                                # %.prol.preheader
                                        #   in Loop: Header=BB0_84 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB0_87:                               #   Parent Loop BB0_84 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rbx,%rbp,8), %xmm0
	movsd	%xmm0, (%r10,%rsi,8)
	incq	%rbp
	incq	%rdx
	jne	.LBB0_87
.LBB0_88:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_84 Depth=1
	cmpq	$3, %rax
	jb	.LBB0_83
	.p2align	4, 0x90
.LBB0_89:                               #   Parent Loop BB0_84 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rbx,%rbp,8), %xmm0
	movsd	%xmm0, (%r10,%rsi,8)
	addsd	8(%rbx,%rbp,8), %xmm0
	movsd	%xmm0, (%r10,%rsi,8)
	addsd	16(%rbx,%rbp,8), %xmm0
	movsd	%xmm0, (%r10,%rsi,8)
	addsd	24(%rbx,%rbp,8), %xmm0
	movsd	%xmm0, (%r10,%rsi,8)
	addq	$4, %rbp
	cmpq	%rdi, %rbp
	jl	.LBB0_89
.LBB0_83:                               # %.loopexit
                                        #   in Loop: Header=BB0_84 Depth=1
	cmpq	%r8, %r9
	jne	.LBB0_84
.LBB0_90:                               # %._crit_edge
	movq	72(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r10, %rbp
	callq	hypre_SeqVectorCreate
	movq	%rbp, (%rax)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%r12)
	movq	%r13, 16(%r12)
	movq	%rbx, (%r12)
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	%r14, (%rcx)
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	%r15, (%rcx)
	movq	192(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	%r12, %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	GenerateSeqLaplacian, .Lfunc_end0-GenerateSeqLaplacian
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
