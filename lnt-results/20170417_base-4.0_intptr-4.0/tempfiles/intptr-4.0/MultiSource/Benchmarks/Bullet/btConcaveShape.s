	.text
	.file	"btConcaveShape.bc"
	.globl	_ZN14btConcaveShapeC2Ev
	.p2align	4, 0x90
	.type	_ZN14btConcaveShapeC2Ev,@function
_ZN14btConcaveShapeC2Ev:                # @_ZN14btConcaveShapeC2Ev
	.cfi_startproc
# BB#0:
	movl	$35, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$_ZTV14btConcaveShape+16, (%rdi)
	movl	$0, 24(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN14btConcaveShapeC2Ev, .Lfunc_end0-_ZN14btConcaveShapeC2Ev
	.cfi_endproc

	.globl	_ZN14btConcaveShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN14btConcaveShapeD2Ev,@function
_ZN14btConcaveShapeD2Ev:                # @_ZN14btConcaveShapeD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN14btConcaveShapeD2Ev, .Lfunc_end1-_ZN14btConcaveShapeD2Ev
	.cfi_endproc

	.globl	_ZN14btConcaveShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN14btConcaveShapeD0Ev,@function
_ZN14btConcaveShapeD0Ev:                # @_ZN14btConcaveShapeD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN14btConcaveShapeD0Ev, .Lfunc_end2-_ZN14btConcaveShapeD0Ev
	.cfi_endproc

	.section	.text._ZN14btConcaveShape9setMarginEf,"axG",@progbits,_ZN14btConcaveShape9setMarginEf,comdat
	.weak	_ZN14btConcaveShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN14btConcaveShape9setMarginEf,@function
_ZN14btConcaveShape9setMarginEf:        # @_ZN14btConcaveShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 24(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN14btConcaveShape9setMarginEf, .Lfunc_end3-_ZN14btConcaveShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK14btConcaveShape9getMarginEv,"axG",@progbits,_ZNK14btConcaveShape9getMarginEv,comdat
	.weak	_ZNK14btConcaveShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK14btConcaveShape9getMarginEv,@function
_ZNK14btConcaveShape9getMarginEv:       # @_ZNK14btConcaveShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end4:
	.size	_ZNK14btConcaveShape9getMarginEv, .Lfunc_end4-_ZNK14btConcaveShape9getMarginEv
	.cfi_endproc

	.type	_ZTV14btConcaveShape,@object # @_ZTV14btConcaveShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV14btConcaveShape
	.p2align	3
_ZTV14btConcaveShape:
	.quad	0
	.quad	_ZTI14btConcaveShape
	.quad	_ZN14btConcaveShapeD2Ev
	.quad	_ZN14btConcaveShapeD0Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN14btConcaveShape9setMarginEf
	.quad	_ZNK14btConcaveShape9getMarginEv
	.quad	__cxa_pure_virtual
	.size	_ZTV14btConcaveShape, 120

	.type	_ZTS14btConcaveShape,@object # @_ZTS14btConcaveShape
	.globl	_ZTS14btConcaveShape
	.p2align	4
_ZTS14btConcaveShape:
	.asciz	"14btConcaveShape"
	.size	_ZTS14btConcaveShape, 17

	.type	_ZTI14btConcaveShape,@object # @_ZTI14btConcaveShape
	.globl	_ZTI14btConcaveShape
	.p2align	4
_ZTI14btConcaveShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14btConcaveShape
	.quad	_ZTI16btCollisionShape
	.size	_ZTI14btConcaveShape, 24


	.globl	_ZN14btConcaveShapeD1Ev
	.type	_ZN14btConcaveShapeD1Ev,@function
_ZN14btConcaveShapeD1Ev = _ZN14btConcaveShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
