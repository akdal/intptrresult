	.text
	.file	"PropVariant.bc"
	.globl	_ZN8NWindows4NCOM12CPropVariantC2ERK14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantC2ERK14tagPROPVARIANT,@function
_ZN8NWindows4NCOM12CPropVariantC2ERK14tagPROPVARIANT: # @_ZN8NWindows4NCOM12CPropVariantC2ERK14tagPROPVARIANT
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movw	$0, (%rbx)
	callq	VariantClear
	movzwl	(%r14), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB0_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB0_3
# BB#2:                                 # %_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT.exit.thread.i
	movups	(%r14), %xmm0
	movups	%xmm0, (%rbx)
.LBB0_6:                                # %_ZN8NWindows4NCOM12CPropVariant12InternalCopyEPK14tagPROPVARIANT.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_3:                                # %_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT.exit.i
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	VariantCopy
	testl	%eax, %eax
	jns	.LBB0_6
# BB#4:
	cmpl	$-2147024882, %eax      # imm = 0x8007000E
	je	.LBB0_7
# BB#5:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB0_6
.LBB0_7:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end0:
	.size	_ZN8NWindows4NCOM12CPropVariantC2ERK14tagPROPVARIANT, .Lfunc_end0-_ZN8NWindows4NCOM12CPropVariantC2ERK14tagPROPVARIANT
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariant12InternalCopyEPK14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariant12InternalCopyEPK14tagPROPVARIANT,@function
_ZN8NWindows4NCOM12CPropVariant12InternalCopyEPK14tagPROPVARIANT: # @_ZN8NWindows4NCOM12CPropVariant12InternalCopyEPK14tagPROPVARIANT
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	VariantClear
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB1_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB1_3
# BB#2:                                 # %_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT.exit.thread
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
.LBB1_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_3:                                # %_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT.exit
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	VariantCopy
	testl	%eax, %eax
	jns	.LBB1_6
# BB#4:
	cmpl	$-2147024882, %eax      # imm = 0x8007000E
	je	.LBB1_7
# BB#5:
	movw	$10, (%r14)
	movl	%eax, 8(%r14)
	jmp	.LBB1_6
.LBB1_7:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end1:
	.size	_ZN8NWindows4NCOM12CPropVariant12InternalCopyEPK14tagPROPVARIANT, .Lfunc_end1-_ZN8NWindows4NCOM12CPropVariant12InternalCopyEPK14tagPROPVARIANT
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantC2ERKS1_,@function
_ZN8NWindows4NCOM12CPropVariantC2ERKS1_: # @_ZN8NWindows4NCOM12CPropVariantC2ERKS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movw	$0, (%rbx)
	callq	VariantClear
	movzwl	(%r14), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB2_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB2_3
# BB#2:                                 # %_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT.exit.thread.i
	movups	(%r14), %xmm0
	movups	%xmm0, (%rbx)
.LBB2_6:                                # %_ZN8NWindows4NCOM12CPropVariant12InternalCopyEPK14tagPROPVARIANT.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_3:                                # %_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT.exit.i
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	VariantCopy
	testl	%eax, %eax
	jns	.LBB2_6
# BB#4:
	cmpl	$-2147024882, %eax      # imm = 0x8007000E
	je	.LBB2_7
# BB#5:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB2_6
.LBB2_7:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end2:
	.size	_ZN8NWindows4NCOM12CPropVariantC2ERKS1_, .Lfunc_end2-_ZN8NWindows4NCOM12CPropVariantC2ERKS1_
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantC2EPw
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantC2EPw,@function
_ZN8NWindows4NCOM12CPropVariantC2EPw:   # @_ZN8NWindows4NCOM12CPropVariantC2EPw
	.cfi_startproc
# BB#0:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i.i.i
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movw	$0, (%rbx)
	callq	VariantClear
	testl	%eax, %eax
	js	.LBB3_2
# BB#1:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i.i.i._ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit.i.i_crit_edge
	leaq	8(%rbx), %r15
	jmp	.LBB3_3
.LBB3_2:
	movw	$10, (%rbx)
	leaq	8(%rbx), %r15
	movl	%eax, 8(%rbx)
.LBB3_3:                                # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit.i.i
	movl	$8, (%rbx)
	movq	%r14, %rdi
	callq	SysAllocString
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.LBB3_5
# BB#4:                                 # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit.i.i
	testq	%rax, %rax
	je	.LBB3_6
.LBB3_5:                                # %_ZN8NWindows4NCOM12CPropVariantaSEPw.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB3_6:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end3:
	.size	_ZN8NWindows4NCOM12CPropVariantC2EPw, .Lfunc_end3-_ZN8NWindows4NCOM12CPropVariantC2EPw
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSEPw
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSEPw,@function
_ZN8NWindows4NCOM12CPropVariantaSEPw:   # @_ZN8NWindows4NCOM12CPropVariantaSEPw
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB4_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB4_3
# BB#2:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread.i.i
	movl	$0, (%rbx)
.LBB4_5:                                # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit.i
	movl	$8, (%rbx)
	movq	%r14, %rdi
	callq	SysAllocString
	movq	%rax, 8(%rbx)
	testq	%r14, %r14
	je	.LBB4_7
# BB#6:                                 # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit.i
	testq	%rax, %rax
	je	.LBB4_8
.LBB4_7:                                # %_ZN8NWindows4NCOM12CPropVariantaSEPKw.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_3:                                # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i.i
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB4_5
# BB#4:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB4_5
.LBB4_8:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end4:
	.size	_ZN8NWindows4NCOM12CPropVariantaSEPw, .Lfunc_end4-_ZN8NWindows4NCOM12CPropVariantaSEPw
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantC2EPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantC2EPKw,@function
_ZN8NWindows4NCOM12CPropVariantC2EPKw:  # @_ZN8NWindows4NCOM12CPropVariantC2EPKw
	.cfi_startproc
# BB#0:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i.i
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movw	$0, (%rbx)
	callq	VariantClear
	testl	%eax, %eax
	js	.LBB5_2
# BB#1:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i.i._ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit.i_crit_edge
	leaq	8(%rbx), %r15
	jmp	.LBB5_3
.LBB5_2:
	movw	$10, (%rbx)
	leaq	8(%rbx), %r15
	movl	%eax, 8(%rbx)
.LBB5_3:                                # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit.i
	movl	$8, (%rbx)
	movq	%r14, %rdi
	callq	SysAllocString
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.LBB5_5
# BB#4:                                 # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit.i
	testq	%rax, %rax
	je	.LBB5_6
.LBB5_5:                                # %_ZN8NWindows4NCOM12CPropVariantaSEPKw.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB5_6:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end5:
	.size	_ZN8NWindows4NCOM12CPropVariantC2EPKw, .Lfunc_end5-_ZN8NWindows4NCOM12CPropVariantC2EPKw
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSEPKw
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSEPKw,@function
_ZN8NWindows4NCOM12CPropVariantaSEPKw:  # @_ZN8NWindows4NCOM12CPropVariantaSEPKw
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB6_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB6_3
# BB#2:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread.i
	movl	$0, (%rbx)
.LBB6_5:                                # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit
	movl	$8, (%rbx)
	movq	%r14, %rdi
	callq	SysAllocString
	movq	%rax, 8(%rbx)
	testq	%r14, %r14
	je	.LBB6_7
# BB#6:                                 # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit
	testq	%rax, %rax
	je	.LBB6_8
.LBB6_7:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_3:                                # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB6_5
# BB#4:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB6_5
.LBB6_8:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end6:
	.size	_ZN8NWindows4NCOM12CPropVariantaSEPKw, .Lfunc_end6-_ZN8NWindows4NCOM12CPropVariantaSEPKw
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSERKS1_,@function
_ZN8NWindows4NCOM12CPropVariantaSERKS1_: # @_ZN8NWindows4NCOM12CPropVariantaSERKS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	VariantClear
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB7_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB7_3
# BB#2:                                 # %_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT.exit.thread.i
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
.LBB7_6:                                # %_ZN8NWindows4NCOM12CPropVariant12InternalCopyEPK14tagPROPVARIANT.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB7_3:                                # %_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT.exit.i
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	VariantCopy
	testl	%eax, %eax
	jns	.LBB7_6
# BB#4:
	cmpl	$-2147024882, %eax      # imm = 0x8007000E
	je	.LBB7_7
# BB#5:
	movw	$10, (%r14)
	movl	%eax, 8(%r14)
	jmp	.LBB7_6
.LBB7_7:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end7:
	.size	_ZN8NWindows4NCOM12CPropVariantaSERKS1_, .Lfunc_end7-_ZN8NWindows4NCOM12CPropVariantaSERKS1_
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSERK14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSERK14tagPROPVARIANT,@function
_ZN8NWindows4NCOM12CPropVariantaSERK14tagPROPVARIANT: # @_ZN8NWindows4NCOM12CPropVariantaSERK14tagPROPVARIANT
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -24
.Lcfi46:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	VariantClear
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB8_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB8_3
# BB#2:                                 # %_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT.exit.thread.i
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
.LBB8_6:                                # %_ZN8NWindows4NCOM12CPropVariant12InternalCopyEPK14tagPROPVARIANT.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB8_3:                                # %_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT.exit.i
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	VariantCopy
	testl	%eax, %eax
	jns	.LBB8_6
# BB#4:
	cmpl	$-2147024882, %eax      # imm = 0x8007000E
	je	.LBB8_7
# BB#5:
	movw	$10, (%r14)
	movl	%eax, 8(%r14)
	jmp	.LBB8_6
.LBB8_7:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end8:
	.size	_ZN8NWindows4NCOM12CPropVariantaSERK14tagPROPVARIANT, .Lfunc_end8-_ZN8NWindows4NCOM12CPropVariantaSERK14tagPROPVARIANT
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariant13InternalClearEv
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariant13InternalClearEv,@function
_ZN8NWindows4NCOM12CPropVariant13InternalClearEv: # @_ZN8NWindows4NCOM12CPropVariant13InternalClearEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 16
.Lcfi48:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB9_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB9_3
# BB#2:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread
	movl	$0, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB9_3:                                # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB9_5
# BB#4:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
.LBB9_5:
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN8NWindows4NCOM12CPropVariant13InternalClearEv, .Lfunc_end9-_ZN8NWindows4NCOM12CPropVariant13InternalClearEv
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSEPKc
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSEPKc,@function
_ZN8NWindows4NCOM12CPropVariantaSEPKc:  # @_ZN8NWindows4NCOM12CPropVariantaSEPKc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 48
.Lcfi54:
	.cfi_offset %rbx, -40
.Lcfi55:
	.cfi_offset %r12, -32
.Lcfi56:
	.cfi_offset %r14, -24
.Lcfi57:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movzwl	(%r14), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB10_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB10_3
# BB#2:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread.i
	movl	$0, (%r14)
.LBB10_5:                               # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit
	movl	$8, (%r14)
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r12
	leal	(,%r12,4), %esi
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	callq	SysAllocStringByteLen
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.LBB10_8
	.p2align	4, 0x90
.LBB10_6:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %ecx
	movsbl	(%r15,%rcx), %edx
	movl	%edx, (%rax,%rcx,4)
	incl	%ebx
	cmpl	%r12d, %ebx
	jbe	.LBB10_6
# BB#7:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB10_3:                               # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i
	movq	%r14, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB10_5
# BB#4:
	movw	$10, (%r14)
	movl	%eax, 8(%r14)
	jmp	.LBB10_5
.LBB10_8:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end10:
	.size	_ZN8NWindows4NCOM12CPropVariantaSEPKc, .Lfunc_end10-_ZN8NWindows4NCOM12CPropVariantaSEPKc
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSEb
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSEb,@function
_ZN8NWindows4NCOM12CPropVariantaSEb:    # @_ZN8NWindows4NCOM12CPropVariantaSEb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 32
.Lcfi61:
	.cfi_offset %rbx, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB11_3
# BB#1:
	movabsq	$4611686018431303999, %rcx # imm = 0x40000000003BC13F
	btq	%rax, %rcx
	jae	.LBB11_2
# BB#7:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread.i
	movl	$0, (%rbx)
.LBB11_5:                               # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit
	movw	$11, (%rbx)
.LBB11_6:
	movzbl	%bpl, %eax
	negl	%eax
	movw	%ax, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB11_2:
	cmpq	$9, %rax
	je	.LBB11_6
.LBB11_3:                               # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB11_5
# BB#4:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB11_5
.Lfunc_end11:
	.size	_ZN8NWindows4NCOM12CPropVariantaSEb, .Lfunc_end11-_ZN8NWindows4NCOM12CPropVariantaSEb
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSEh
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSEh,@function
_ZN8NWindows4NCOM12CPropVariantaSEh:    # @_ZN8NWindows4NCOM12CPropVariantaSEh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 32
.Lcfi66:
	.cfi_offset %rbx, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB12_3
# BB#1:
	movabsq	$4611686018431271743, %rcx # imm = 0x40000000003B433F
	btq	%rax, %rcx
	jae	.LBB12_2
# BB#7:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread.i
	movl	$0, (%rbx)
.LBB12_5:                               # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit
	movw	$17, (%rbx)
.LBB12_6:
	movb	%bpl, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB12_2:
	cmpq	$15, %rax
	je	.LBB12_6
.LBB12_3:                               # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB12_5
# BB#4:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB12_5
.Lfunc_end12:
	.size	_ZN8NWindows4NCOM12CPropVariantaSEh, .Lfunc_end12-_ZN8NWindows4NCOM12CPropVariantaSEh
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSEs
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSEs,@function
_ZN8NWindows4NCOM12CPropVariantaSEs:    # @_ZN8NWindows4NCOM12CPropVariantaSEs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB13_3
# BB#1:
	movabsq	$4611686018431304510, %rcx # imm = 0x40000000003BC33E
	btq	%rax, %rcx
	jae	.LBB13_2
# BB#7:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread.i
	movl	$0, (%rbx)
.LBB13_5:                               # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit
	movw	$2, (%rbx)
.LBB13_6:
	movw	%bp, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB13_2:
	testq	%rax, %rax
	je	.LBB13_6
.LBB13_3:                               # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB13_5
# BB#4:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB13_5
.Lfunc_end13:
	.size	_ZN8NWindows4NCOM12CPropVariantaSEs, .Lfunc_end13-_ZN8NWindows4NCOM12CPropVariantaSEs
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSEi
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSEi,@function
_ZN8NWindows4NCOM12CPropVariantaSEi:    # @_ZN8NWindows4NCOM12CPropVariantaSEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 32
.Lcfi76:
	.cfi_offset %rbx, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB14_3
# BB#1:
	movabsq	$4611686018431304509, %rcx # imm = 0x40000000003BC33D
	btq	%rax, %rcx
	jae	.LBB14_2
# BB#7:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread.i
	movl	$0, (%rbx)
.LBB14_5:                               # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit
	movw	$3, (%rbx)
.LBB14_6:
	movl	%ebp, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB14_2:
	cmpq	$1, %rax
	je	.LBB14_6
.LBB14_3:                               # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB14_5
# BB#4:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB14_5
.Lfunc_end14:
	.size	_ZN8NWindows4NCOM12CPropVariantaSEi, .Lfunc_end14-_ZN8NWindows4NCOM12CPropVariantaSEi
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSEj
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSEj,@function
_ZN8NWindows4NCOM12CPropVariantaSEj:    # @_ZN8NWindows4NCOM12CPropVariantaSEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 32
.Lcfi81:
	.cfi_offset %rbx, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB15_3
# BB#1:
	movabsq	$4611686018431173439, %rcx # imm = 0x400000000039C33F
	btq	%rax, %rcx
	jae	.LBB15_2
# BB#7:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread.i
	movl	$0, (%rbx)
.LBB15_5:                               # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit
	movw	$19, (%rbx)
.LBB15_6:
	movl	%ebp, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB15_2:
	cmpq	$17, %rax
	je	.LBB15_6
.LBB15_3:                               # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB15_5
# BB#4:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB15_5
.Lfunc_end15:
	.size	_ZN8NWindows4NCOM12CPropVariantaSEj, .Lfunc_end15-_ZN8NWindows4NCOM12CPropVariantaSEj
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSEy
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSEy,@function
_ZN8NWindows4NCOM12CPropVariantaSEy:    # @_ZN8NWindows4NCOM12CPropVariantaSEy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -24
.Lcfi87:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB16_3
# BB#1:
	movabsq	$4611686018430780223, %rcx # imm = 0x400000000033C33F
	btq	%rax, %rcx
	jae	.LBB16_2
# BB#7:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread.i
	movl	$0, (%rbx)
.LBB16_5:                               # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit
	movw	$21, (%rbx)
.LBB16_6:
	movq	%r14, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB16_2:
	cmpq	$19, %rax
	je	.LBB16_6
.LBB16_3:                               # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB16_5
# BB#4:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB16_5
.Lfunc_end16:
	.size	_ZN8NWindows4NCOM12CPropVariantaSEy, .Lfunc_end16-_ZN8NWindows4NCOM12CPropVariantaSEy
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME,@function
_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME: # @_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 32
.Lcfi91:
	.cfi_offset %rbx, -24
.Lcfi92:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB17_3
# BB#1:
	movl	$3916607, %ecx          # imm = 0x3BC33F
	btq	%rax, %rcx
	jae	.LBB17_2
# BB#7:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread.i
	movl	$0, (%rbx)
.LBB17_5:                               # %_ZN8NWindows4NCOM12CPropVariant13InternalClearEv.exit
	movw	$64, (%rbx)
.LBB17_6:
	movq	(%r14), %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB17_2:
	cmpq	$62, %rax
	je	.LBB17_6
.LBB17_3:                               # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.i
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB17_5
# BB#4:
	movw	$10, (%rbx)
	movl	%eax, 8(%rbx)
	jmp	.LBB17_5
.Lfunc_end17:
	.size	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME, .Lfunc_end17-_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariant5ClearEv,@function
_ZN8NWindows4NCOM12CPropVariant5ClearEv: # @_ZN8NWindows4NCOM12CPropVariant5ClearEv
	.cfi_startproc
# BB#0:
	movzwl	(%rdi), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB18_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB18_3
# BB#2:                                 # %_ZN8NWindows4NCOML18MyPropVariantClearEP14tagPROPVARIANT.exit
	movl	$0, (%rdi)
	xorl	%eax, %eax
	retq
.LBB18_3:
	jmp	VariantClear            # TAILCALL
.Lfunc_end18:
	.size	_ZN8NWindows4NCOM12CPropVariant5ClearEv, .Lfunc_end18-_ZN8NWindows4NCOM12CPropVariant5ClearEv
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT,@function
_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT: # @_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 32
.Lcfi96:
	.cfi_offset %rbx, -24
.Lcfi97:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	VariantClear
	movzwl	(%r14), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB19_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB19_3
# BB#2:
	movups	(%r14), %xmm0
	movups	%xmm0, (%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB19_3:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	VariantCopy             # TAILCALL
.Lfunc_end19:
	.size	_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT, .Lfunc_end19-_ZN8NWindows4NCOM12CPropVariant4CopyEPK14tagPROPVARIANT
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariant6AttachEP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariant6AttachEP14tagPROPVARIANT,@function
_ZN8NWindows4NCOM12CPropVariant6AttachEP14tagPROPVARIANT: # @_ZN8NWindows4NCOM12CPropVariant6AttachEP14tagPROPVARIANT
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 32
.Lcfi101:
	.cfi_offset %rbx, -24
.Lcfi102:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB20_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB20_3
# BB#2:                                 # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit.thread
	movl	$0, (%rbx)
.LBB20_4:
	movups	(%r14), %xmm0
	movups	%xmm0, (%rbx)
	movw	$0, (%r14)
	xorl	%eax, %eax
.LBB20_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB20_3:                               # %_ZN8NWindows4NCOM12CPropVariant5ClearEv.exit
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB20_4
	jmp	.LBB20_5
.Lfunc_end20:
	.size	_ZN8NWindows4NCOM12CPropVariant6AttachEP14tagPROPVARIANT, .Lfunc_end20-_ZN8NWindows4NCOM12CPropVariant6AttachEP14tagPROPVARIANT
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT,@function
_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT: # @_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 32
.Lcfi106:
	.cfi_offset %rbx, -24
.Lcfi107:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movzwl	(%rbx), %eax
	addl	$-2, %eax
	movzwl	%ax, %eax
	cmpl	$62, %eax
	ja	.LBB21_3
# BB#1:
	movabsq	$4611686018431304511, %rcx # imm = 0x40000000003BC33F
	btq	%rax, %rcx
	jae	.LBB21_3
# BB#2:                                 # %_ZN8NWindows4NCOML18MyPropVariantClearEP14tagPROPVARIANT.exit.thread
	movl	$0, (%rbx)
.LBB21_4:
	movups	(%r14), %xmm0
	movups	%xmm0, (%rbx)
	movw	$0, (%r14)
	xorl	%eax, %eax
.LBB21_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB21_3:                               # %_ZN8NWindows4NCOML18MyPropVariantClearEP14tagPROPVARIANT.exit
	movq	%rbx, %rdi
	callq	VariantClear
	testl	%eax, %eax
	jns	.LBB21_4
	jmp	.LBB21_5
.Lfunc_end21:
	.size	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT, .Lfunc_end21-_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
	.cfi_endproc

	.globl	_ZN8NWindows4NCOM12CPropVariant7CompareERKS1_
	.p2align	4, 0x90
	.type	_ZN8NWindows4NCOM12CPropVariant7CompareERKS1_,@function
_ZN8NWindows4NCOM12CPropVariant7CompareERKS1_: # @_ZN8NWindows4NCOM12CPropVariant7CompareERKS1_
	.cfi_startproc
# BB#0:
	movw	(%rdi), %cx
	cmpw	(%rsi), %cx
	jne	.LBB22_1
# BB#2:
	xorl	%eax, %eax
	addl	$-2, %ecx
	movzwl	%cx, %ecx
	cmpl	$62, %ecx
	ja	.LBB22_15
# BB#3:
	jmpq	*.LJTI22_0(,%rcx,8)
.LBB22_6:
	movzwl	8(%rdi), %eax
	xorl	%ecx, %ecx
	cmpw	8(%rsi), %ax
	jmp	.LBB22_7
.LBB22_1:
	movl	$-1, %ecx
	movl	$1, %eax
	cmovbl	%ecx, %eax
	retq
.LBB22_9:
	movl	8(%rdi), %eax
	xorl	%ecx, %ecx
	cmpl	8(%rsi), %eax
	jmp	.LBB22_7
.LBB22_14:
	movzwl	8(%rdi), %eax
	xorl	%ecx, %ecx
	cmpw	8(%rsi), %ax
	movl	$-1, %edx
	cmovel	%ecx, %edx
	movl	$1, %eax
	cmovgel	%edx, %eax
.LBB22_15:
	retq
.LBB22_4:
	movb	8(%rdi), %al
	xorl	%ecx, %ecx
	cmpb	8(%rsi), %al
	jmp	.LBB22_5
.LBB22_8:
	movzwl	8(%rdi), %eax
	xorl	%ecx, %ecx
	cmpw	8(%rsi), %ax
	jmp	.LBB22_5
.LBB22_10:
	movl	8(%rdi), %eax
	xorl	%ecx, %ecx
	cmpl	8(%rsi), %eax
	jmp	.LBB22_5
.LBB22_11:
	movq	8(%rdi), %rax
	xorl	%ecx, %ecx
	cmpq	8(%rsi), %rax
.LBB22_7:
	setne	%cl
	movl	$-1, %eax
	cmovgel	%ecx, %eax
	retq
.LBB22_12:
	movq	8(%rdi), %rax
	xorl	%ecx, %ecx
	cmpq	8(%rsi), %rax
.LBB22_5:
	setne	%cl
	movl	$-1, %eax
	cmovael	%ecx, %eax
	retq
.LBB22_13:
	addq	$8, %rdi
	addq	$8, %rsi
	jmp	CompareFileTime         # TAILCALL
.Lfunc_end22:
	.size	_ZN8NWindows4NCOM12CPropVariant7CompareERKS1_, .Lfunc_end22-_ZN8NWindows4NCOM12CPropVariant7CompareERKS1_
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI22_0:
	.quad	.LBB22_6
	.quad	.LBB22_9
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_14
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_4
	.quad	.LBB22_8
	.quad	.LBB22_10
	.quad	.LBB22_11
	.quad	.LBB22_12
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_15
	.quad	.LBB22_13

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"out of memory"
	.size	.L.str, 14


	.globl	_ZN8NWindows4NCOM12CPropVariantC1ERK14tagPROPVARIANT
	.type	_ZN8NWindows4NCOM12CPropVariantC1ERK14tagPROPVARIANT,@function
_ZN8NWindows4NCOM12CPropVariantC1ERK14tagPROPVARIANT = _ZN8NWindows4NCOM12CPropVariantC2ERK14tagPROPVARIANT
	.globl	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
	.type	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_,@function
_ZN8NWindows4NCOM12CPropVariantC1ERKS1_ = _ZN8NWindows4NCOM12CPropVariantC2ERKS1_
	.globl	_ZN8NWindows4NCOM12CPropVariantC1EPw
	.type	_ZN8NWindows4NCOM12CPropVariantC1EPw,@function
_ZN8NWindows4NCOM12CPropVariantC1EPw = _ZN8NWindows4NCOM12CPropVariantC2EPw
	.globl	_ZN8NWindows4NCOM12CPropVariantC1EPKw
	.type	_ZN8NWindows4NCOM12CPropVariantC1EPKw,@function
_ZN8NWindows4NCOM12CPropVariantC1EPKw = _ZN8NWindows4NCOM12CPropVariantC2EPKw
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
