	.text
	.file	"shared_options.bc"
	.globl	opt_free
	.p2align	4, 0x90
	.type	opt_free,@function
opt_free:                               # @opt_free
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB0_10
# BB#1:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB0_7
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	callq	free
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	callq	free
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movq	24(%rbx), %r15
	movq	%rbx, %rdi
	callq	free
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB0_2
.LBB0_7:                                # %._crit_edge
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#8:
	callq	free
.LBB0_9:
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB0_10:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	opt_free, .Lfunc_end0-opt_free
	.cfi_endproc

	.globl	opt_parse
	.p2align	4, 0x90
	.type	opt_parse,@function
opt_parse:                              # @opt_parse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 80
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, %rbx
	movq	%rdx, %r15
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	%edi, %r13d
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_1
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader71
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 4(%rsp)
	movl	%r13d, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r15, %rdx
	movq	%rbx, %rcx
	leaq	4(%rsp), %r8
	callq	getopt_long
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB1_7
# BB#3:                                 # %.preheader71
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpl	$-1, %r14d
	je	.LBB1_32
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_26
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	movslq	4(%rsp), %rax
	testq	%rax, %rax
	je	.LBB1_6
# BB#18:                                #   in Loop: Header=BB1_2 Depth=1
	shlq	$5, %rax
	movq	(%rbx,%rax), %rsi
	jmp	.LBB1_19
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	movslq	4(%rsp), %rax
	shlq	$5, %rax
	movq	(%rbx,%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	register_option
	cmpl	$-1, %eax
	jne	.LBB1_2
	jmp	.LBB1_8
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	xorl	%esi, %esi
.LBB1_19:                               #   in Loop: Header=BB1_2 Depth=1
	movsbl	%r14b, %edx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	register_option
	cmpl	$-1, %eax
	jne	.LBB1_2
# BB#20:
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB1_14
	.p2align	4, 0x90
.LBB1_21:                               # %.lr.ph.i60
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_23
# BB#22:                                #   in Loop: Header=BB1_21 Depth=1
	callq	free
.LBB1_23:                               #   in Loop: Header=BB1_21 Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_21 Depth=1
	callq	free
.LBB1_25:                               #   in Loop: Header=BB1_21 Depth=1
	movq	24(%rbx), %r14
	movq	%rbx, %rdi
	callq	free
	testq	%r14, %r14
	movq	%r14, %rbx
	jne	.LBB1_21
	jmp	.LBB1_14
.LBB1_1:
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	mprintf
	jmp	.LBB1_48
.LBB1_32:
	movslq	optind(%rip), %r15
	cmpl	%r13d, %r15d
	movq	%r12, %rbx
	jge	.LBB1_48
# BB#33:                                # %.lr.ph75.preheader
	movslq	%r13d, %r14
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%r15,8), %rbp
	subq	%r15, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_34:                               # %.lr.ph75
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	callq	strlen
	movl	%ebx, %ebx
	addq	%rax, %rbx
	addq	$8, %rbp
	decq	%r14
	jne	.LBB1_34
# BB#35:                                # %._crit_edge
	leal	63(%r13,%rbx), %eax
	subl	%r15d, %eax
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %r14
	movq	%r12, %rbp
	movq	%r14, 8(%rbp)
	testq	%r14, %r14
	je	.LBB1_36
# BB#43:                                # %.lr.ph
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%r15,8), %rbx
	subl	%r15d, %r13d
	.p2align	4, 0x90
.LBB1_44:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	strncat
	cmpl	$1, %r13d
	je	.LBB1_46
# BB#45:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%r14, %rdi
	callq	strlen
	movw	$9, (%r14,%rax)
.LBB1_46:                               #   in Loop: Header=BB1_44 Depth=1
	addq	$8, %rbx
	decl	%r13d
	jne	.LBB1_44
# BB#47:
	movq	%r12, %rbx
	jmp	.LBB1_48
.LBB1_26:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	mprintf
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB1_14
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph.i64
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_27 Depth=1
	callq	free
.LBB1_29:                               #   in Loop: Header=BB1_27 Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_27 Depth=1
	callq	free
.LBB1_31:                               #   in Loop: Header=BB1_27 Depth=1
	movq	24(%rbx), %rbp
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB1_27
	jmp	.LBB1_14
.LBB1_8:
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB1_14
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_9 Depth=1
	callq	free
.LBB1_11:                               #   in Loop: Header=BB1_9 Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_9 Depth=1
	callq	free
.LBB1_13:                               #   in Loop: Header=BB1_9 Depth=1
	movq	24(%rbx), %r14
	movq	%rbx, %rdi
	callq	free
	testq	%r14, %r14
	movq	%r14, %rbx
	jne	.LBB1_9
.LBB1_14:                               # %._crit_edge.i
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB1_16
# BB#15:
	callq	free
.LBB1_16:
	movq	%r12, %rdi
.LBB1_17:                               # %opt_free.exit
	callq	free
	xorl	%ebx, %ebx
.LBB1_48:                               # %opt_free.exit
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_36:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	mprintf
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	movq	%rbp, %r14
	je	.LBB1_42
	.p2align	4, 0x90
.LBB1_37:                               # %.lr.ph.i68
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_39
# BB#38:                                #   in Loop: Header=BB1_37 Depth=1
	callq	free
.LBB1_39:                               #   in Loop: Header=BB1_37 Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_41
# BB#40:                                #   in Loop: Header=BB1_37 Depth=1
	callq	free
.LBB1_41:                               #   in Loop: Header=BB1_37 Depth=1
	movq	24(%rbx), %rbp
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB1_37
.LBB1_42:                               # %opt_free.exit70
	movq	%r14, %rdi
	jmp	.LBB1_17
.Lfunc_end1:
	.size	opt_parse, .Lfunc_end1-opt_parse
	.cfi_endproc

	.p2align	4, 0x90
	.type	register_option,@function
register_option:                        # @register_option
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 64
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movl	%edx, %r12d
	movq	%rsi, %r13
	movq	%rdi, %r14
	testb	%r12b, %r12b
	je	.LBB2_5
# BB#1:                                 # %.preheader61
	movq	(%rcx), %r13
	testq	%r13, %r13
	je	.LBB2_15
# BB#2:                                 # %.lr.ph66
	movsbl	%r12b, %eax
	addq	$32, %rcx
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	%eax, -8(%rcx)
	je	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	(%rcx), %r13
	addq	$32, %rcx
	testq	%r13, %r13
	jne	.LBB2_3
	jmp	.LBB2_15
.LBB2_5:
	testq	%r13, %r13
	je	.LBB2_15
.LBB2_6:                                # %.thread59
	testq	%rbp, %rbp
	je	.LBB2_11
# BB#7:                                 # %.preheader
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_18
# BB#8:                                 # %.lr.ph.preheader
	addq	$8, %rbp
	xorl	%ebx, %ebx
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB2_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, %rsi
	callq	strcmp
	testl	%eax, %eax
	cmovel	%r15d, %ebx
	movq	(%rbp), %rdi
	addq	$8, %rbp
	testq	%rdi, %rdi
	jne	.LBB2_9
# BB#10:                                # %._crit_edge
	testl	%ebx, %ebx
	je	.LBB2_18
.LBB2_11:
	movl	$32, %edi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_20
# BB#12:
	movb	%r12b, (%r15)
	movq	optarg(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB2_21
# BB#13:
	movq	%rbp, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, 16(%r15)
	testq	%rax, %rax
	je	.LBB2_26
# BB#14:
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcpy
	jmp	.LBB2_22
.LBB2_15:                               # %.thread
	movsbl	%r12b, %esi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	mprintf
.LBB2_16:
	movl	$-1, %ebp
.LBB2_17:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_18:                               # %._crit_edge.thread
	testb	%r12b, %r12b
	je	.LBB2_24
# BB#19:
	movsbl	%r12b, %edx
	xorl	%ebp, %ebp
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	mprintf
	jmp	.LBB2_17
.LBB2_20:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	mprintf
	jmp	.LBB2_16
.LBB2_21:
	movq	$0, 16(%r15)
.LBB2_22:
	movq	%r13, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.LBB2_25
# BB#23:
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	(%r14), %rax
	movq	%rax, 24(%r15)
	movq	%r15, (%r14)
	xorl	%ebp, %ebp
	jmp	.LBB2_17
.LBB2_24:
	xorl	%ebp, %ebp
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	mprintf
	jmp	.LBB2_17
.LBB2_25:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	mprintf
	movq	16(%r15), %rdi
	callq	free
	jmp	.LBB2_27
.LBB2_26:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	mprintf
.LBB2_27:
	movq	%r15, %rdi
	callq	free
	jmp	.LBB2_16
.Lfunc_end2:
	.size	register_option, .Lfunc_end2-register_option
	.cfi_endproc

	.globl	opt_check
	.p2align	4, 0x90
	.type	opt_check,@function
opt_check:                              # @opt_check
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.LBB3_1
# BB#2:
	movq	(%rdi), %rbp
	xorl	%r14d, %r14d
	testq	%rbp, %rbp
	jne	.LBB3_4
	jmp	.LBB3_7
	.p2align	4, 0x90
.LBB3_8:                                #   in Loop: Header=BB3_4 Depth=1
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB3_7
.LBB3_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_8
# BB#6:
	movl	$1, %r14d
	jmp	.LBB3_7
.LBB3_1:
	xorl	%r14d, %r14d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	mprintf
.LBB3_7:                                # %.loopexit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	opt_check, .Lfunc_end3-opt_check
	.cfi_endproc

	.globl	opt_arg
	.p2align	4, 0x90
	.type	opt_arg,@function
opt_arg:                                # @opt_arg
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	testq	%rdi, %rdi
	je	.LBB4_1
# BB#2:
	movq	(%rdi), %rbx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	jne	.LBB4_4
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_8:                                #   in Loop: Header=BB4_4 Depth=1
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB4_7
.LBB4_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_8
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=1
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB4_8
# BB#6:
	movq	16(%rbx), %r14
	jmp	.LBB4_7
.LBB4_1:
	xorl	%r14d, %r14d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	mprintf
.LBB4_7:                                # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	opt_arg, .Lfunc_end4-opt_arg
	.cfi_endproc

	.globl	opt_firstarg
	.p2align	4, 0x90
	.type	opt_firstarg,@function
opt_firstarg:                           # @opt_firstarg
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	testq	%rdi, %rdi
	je	.LBB5_1
# BB#2:
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_4
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                #   in Loop: Header=BB5_4 Depth=1
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB5_8
.LBB5_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_7
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB5_7
# BB#6:
	movq	%rbx, (%r14)
	movq	16(%rbx), %rbx
	jmp	.LBB5_9
.LBB5_8:                                # %._crit_edge
	movq	$0, (%r14)
	xorl	%ebx, %ebx
	jmp	.LBB5_9
.LBB5_1:
	xorl	%ebx, %ebx
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	mprintf
.LBB5_9:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	opt_firstarg, .Lfunc_end5-opt_firstarg
	.cfi_endproc

	.globl	opt_nextarg
	.p2align	4, 0x90
	.type	opt_nextarg,@function
opt_nextarg:                            # @opt_nextarg
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB6_9
# BB#1:
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB6_9
# BB#2:                                 # %.preheader
	movq	24(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_4
	jmp	.LBB6_7
	.p2align	4, 0x90
.LBB6_6:                                # %.backedge
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB6_7
.LBB6_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=1
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB6_6
# BB#10:
	movq	%rbx, (%r14)
	movq	16(%rbx), %rbx
	jmp	.LBB6_8
.LBB6_9:
	xorl	%ebx, %ebx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	mprintf
	jmp	.LBB6_8
.LBB6_7:                                # %._crit_edge
	movq	$0, (%r14)
	xorl	%ebx, %ebx
.LBB6_8:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	opt_nextarg, .Lfunc_end6-opt_nextarg
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"!opt_parse: calloc failed\n"
	.size	.L.str, 27

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"!Unknown option passed.\n"
	.size	.L.str.1, 25

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"!opt_check: opt == NULL\n"
	.size	.L.str.3, 25

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"!opt_arg: opt == NULL\n"
	.size	.L.str.4, 23

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"!opt_firstarg: opt == NULL\n"
	.size	.L.str.5, 28

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"!opt_nextarg: *optnode == NULL\n"
	.size	.L.str.6, 32

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"!register_option: No long option for -%c\n"
	.size	.L.str.7, 42

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"WARNING: Ignoring option --%s (-%c)\n"
	.size	.L.str.8, 37

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"WARNING: Ignoring option --%s\n"
	.size	.L.str.9, 31

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"!register_long_option: malloc failed\n"
	.size	.L.str.10, 38

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"ERROR: register_long_option: malloc failed\n"
	.size	.L.str.11, 44


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
