	.text
	.file	"kernel.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4665729213955833856     # double 8192
.LCPI0_1:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	kernel_measureFFT
	.p2align	4, 0x90
	.type	kernel_measureFFT,@function
kernel_measureFFT:                      # @kernel_measureFFT
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	%rdi, (%rsp)            # 8-byte Spill
	leal	(%rdi,%rdi), %ebp
	movl	%ebp, %edi
	callq	RandomVector
	movq	%rax, %r13
	movl	$1, %r12d
	callq	new_Stopwatch
	movq	%rax, %r14
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_5:                                # %.backedge.backedge
                                        #   in Loop: Header=BB0_1 Depth=1
	addq	%r12, %r12
.LBB0_1:                                # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
	movq	%r14, %rdi
	callq	Stopwatch_start
	testq	%r12, %r12
	jle	.LBB0_7
# BB#2:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	leaq	(,%r12,8), %rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	FFT_transform
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	FFT_inverse
	incq	%r15
	cmpq	%rbx, %r15
	jl	.LBB0_3
# BB#4:                                 # %._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	%r14, %rdi
	callq	Stopwatch_stop
	cmpq	$4096, %r12             # imm = 0x1000
	jle	.LBB0_5
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_7:                                # %._crit_edge.thread
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	%r14, %rdi
	callq	Stopwatch_stop
	addq	%r12, %r12
	jmp	.LBB0_1
.LBB0_6:
	movq	(%rsp), %rdi            # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	FFT_num_flops
	mulsd	.LCPI0_0(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%r14, %rdi
	callq	Stopwatch_read
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	mulsd	.LCPI0_1(%rip), %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movq	%r14, %rdi
	callq	Stopwatch_delete
	movq	%r13, %rdi
	callq	free
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	kernel_measureFFT, .Lfunc_end0-kernel_measureFFT
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4608308318706860032     # double 1.25
.LCPI1_1:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	kernel_measureSOR
	.p2align	4, 0x90
	.type	kernel_measureSOR,@function
kernel_measureSOR:                      # @kernel_measureSOR
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rsi, %rax
	movl	%edi, %r15d
	movl	%r15d, %esi
	movq	%rax, %rdx
	callq	RandomMatrix
	movq	%rax, %r14
	movl	$1, %ebp
	callq	new_Stopwatch
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	%ebp, %ecx
	shll	$4, %ecx
	movl	%r15d, %edi
	movl	%r15d, %esi
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r14, %rdx
	callq	SOR_execute
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	cmpl	$4097, %ebp             # imm = 0x1001
	leal	(%rbp,%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB1_1
# BB#2:
	movl	$8192, %edx             # imm = 0x2000
	movl	%r15d, %edi
	movl	%r15d, %esi
	callq	SOR_num_flops
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rbx, %rdi
	callq	Stopwatch_read
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	mulsd	.LCPI1_1(%rip), %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movq	%rbx, %rdi
	callq	Stopwatch_delete
	movl	%r15d, %edi
	movl	%r15d, %esi
	movq	%r14, %rdx
	callq	Array2D_double_delete
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	kernel_measureSOR, .Lfunc_end1-kernel_measureSOR
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	kernel_measureMonteCarlo
	.p2align	4, 0x90
	.type	kernel_measureMonteCarlo,@function
kernel_measureMonteCarlo:               # @kernel_measureMonteCarlo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -16
	callq	new_Stopwatch
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$65536, %edi            # imm = 0x10000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$131072, %edi           # imm = 0x20000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$262144, %edi           # imm = 0x40000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$524288, %edi           # imm = 0x80000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$1048576, %edi          # imm = 0x100000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$2097152, %edi          # imm = 0x200000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$4194304, %edi          # imm = 0x400000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$8388608, %edi          # imm = 0x800000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$16777216, %edi         # imm = 0x1000000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$33554432, %edi         # imm = 0x2000000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$67108864, %edi         # imm = 0x4000000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$134217728, %edi        # imm = 0x8000000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$268435456, %edi        # imm = 0x10000000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movq	%rbx, %rdi
	callq	Stopwatch_start
	movl	$536870912, %edi        # imm = 0x20000000
	callq	MonteCarlo_integrate
	movq	%rbx, %rdi
	callq	Stopwatch_stop
	movl	$8192, %edi             # imm = 0x2000
	callq	MonteCarlo_num_flops
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	Stopwatch_read
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	mulsd	.LCPI2_0(%rip), %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	Stopwatch_delete
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end2:
	.size	kernel_measureMonteCarlo, .Lfunc_end2-kernel_measureMonteCarlo
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI3_1:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_2:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	kernel_measureSparseMatMult
	.p2align	4, 0x90
	.type	kernel_measureSparseMatMult,@function
kernel_measureSparseMatMult:            # @kernel_measureSparseMatMult
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 160
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebx
	movl	%edi, %r12d
	movq	%r14, %rsi
	callq	RandomVector
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movslq	%r12d, %r15
	leaq	(,%r15,8), %rdi
	callq	malloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%ebx, %eax
	cltd
	idivl	%r15d
	movl	%eax, %ebp
	movl	%ebp, %edi
	imull	%r12d, %edi
	movq	%r14, %rsi
	callq	RandomVector
	movq	%rax, %r13
	movl	%ebx, 36(%rsp)          # 4-byte Spill
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	4(,%r15,4), %rdi
	callq	malloc
	movq	%rax, %rbx
	callq	new_Stopwatch
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	$0, (%rbx)
	testl	%r15d, %r15d
	jle	.LBB3_19
# BB#1:                                 # %.lr.ph85
	testl	%ebp, %ebp
	movl	%r12d, %r14d
	jle	.LBB3_2
# BB#10:                                # %.lr.ph85.split.us.preheader
	movl	%ebp, %ebx
	leaq	-1(%rbx), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%ebx, %edi
	andl	$3, %edi
	movslq	%ebp, %rcx
	shlq	$2, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	40(%rsp), %r11          # 8-byte Reload
	xorl	%ecx, %ecx
	movl	%r12d, 16(%rsp)         # 4-byte Spill
	movq	%r13, 96(%rsp)          # 8-byte Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph85.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_14 Depth 2
                                        #     Child Loop BB3_17 Depth 2
	movl	%ecx, %eax
	cltd
	idivl	%ebp
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	%ebp, %r15d
	testl	%eax, %eax
	movl	$1, %edx
	cmovlel	%edx, %eax
	testq	%rdi, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%r15d, 4(%rdx,%rcx,4)
	je	.LBB3_12
# BB#13:                                # %.prol.preheader
                                        #   in Loop: Header=BB3_11 Depth=1
	xorl	%ebp, %ebp
	movq	%r11, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_14:                               #   Parent Loop BB3_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, (%rsi)
	incq	%rdx
	addq	$4, %rsi
	leal	(%rax,%rbp), %ebp
	cmpq	%rdx, %rdi
	jne	.LBB3_14
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_12:                               #   in Loop: Header=BB3_11 Depth=1
	xorl	%edx, %edx
.LBB3_15:                               # %.prol.loopexit
                                        #   in Loop: Header=BB3_11 Depth=1
	incq	%rcx
	cmpq	$3, 72(%rsp)            # 8-byte Folded Reload
	jb	.LBB3_18
# BB#16:                                # %.lr.ph85.split.us.new
                                        #   in Loop: Header=BB3_11 Depth=1
	leal	1(%rdx), %r10d
	imull	%eax, %r10d
	leal	(,%rax,4), %r8d
	leal	3(%rdx), %r9d
	imull	%eax, %r9d
	movl	%edx, %r13d
	imull	%eax, %r13d
	leal	2(%rdx), %r12d
	imull	%eax, %r12d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_17:                               #   Parent Loop BB3_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r13,%rax), %esi
	movl	%esi, (%r11,%rdx,4)
	leal	(%r10,%rax), %esi
	movl	%esi, 4(%r11,%rdx,4)
	leal	(%r12,%rax), %esi
	movl	%esi, 8(%r11,%rdx,4)
	leal	(%r9,%rax), %esi
	movl	%esi, 12(%r11,%rdx,4)
	addq	$4, %rdx
	addl	%r8d, %eax
	cmpq	%rdx, %rbx
	jne	.LBB3_17
.LBB3_18:                               # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB3_11 Depth=1
	addq	64(%rsp), %r11          # 8-byte Folded Reload
	movq	%r14, %rax
	cmpq	%rax, %rcx
	movl	16(%rsp), %r12d         # 4-byte Reload
	movq	96(%rsp), %r13          # 8-byte Reload
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_11
	jmp	.LBB3_19
.LBB3_2:                                # %.lr.ph85.split.preheader
	cmpl	$7, %r12d
	jbe	.LBB3_3
# BB#6:                                 # %min.iters.checked
	movl	%r12d, %r8d
	andl	$7, %r8d
	movq	%r14, %rdx
	subq	%r8, %rdx
	movq	%r14, %r9
	subq	%r8, %r9
	je	.LBB3_3
# BB#7:                                 # %vector.ph
	imull	%ebp, %edx
	movd	%ebp, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	.LCPI3_0(%rip), %xmm1   # xmm1 = [0,1,2,3]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqa	.LCPI3_1(%rip), %xmm2   # xmm2 = [4,5,6,7]
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pmuludq	%xmm0, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	20(%rcx), %rsi
	leal	(,%rbp,8), %edi
	xorl	%ebx, %ebx
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB3_8:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	%ebx, %xmm3
	pshufd	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0]
	movdqa	%xmm3, %xmm4
	paddd	%xmm1, %xmm4
	paddd	%xmm2, %xmm3
	paddd	%xmm0, %xmm4
	paddd	%xmm0, %xmm3
	movdqu	%xmm4, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rsi
	addl	%edi, %ebx
	addq	$-8, %rcx
	jne	.LBB3_8
# BB#9:                                 # %middle.block
	testl	%r8d, %r8d
	jne	.LBB3_4
	jmp	.LBB3_19
.LBB3_3:
	xorl	%edx, %edx
	xorl	%r9d, %r9d
.LBB3_4:                                # %.lr.ph85.split.preheader107
	addl	%ebp, %edx
	subq	%r9, %r14
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rcx,%r9,4), %rcx
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph85.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	addl	%ebp, %edx
	addq	$4, %rcx
	decq	%r14
	jne	.LBB3_5
.LBB3_19:                               # %.preheader.preheader
	movl	$1, %ebx
	movl	%r12d, %r15d
	movq	%rax, %r12
	movq	%r13, %rbp
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_20:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	Stopwatch_start
	movl	%ebx, %eax
	shll	$6, %eax
	movl	%eax, (%rsp)
	movl	%r15d, %edi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%r13, %rcx
	movq	%r14, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	callq	SparseCompRow_matmult
	movq	%r12, %rdi
	callq	Stopwatch_stop
	cmpl	$4097, %ebx             # imm = 0x1001
	leal	(%rbx,%rbx), %eax
	movl	%eax, %ebx
	jl	.LBB3_20
# BB#21:
	movl	$8192, %edx             # imm = 0x2000
	movl	%r15d, %edi
	movl	36(%rsp), %esi          # 4-byte Reload
	callq	SparseCompRow_num_flops
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	callq	Stopwatch_read
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	mulsd	.LCPI3_2(%rip), %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	callq	Stopwatch_delete
	movq	%r13, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	kernel_measureSparseMatMult, .Lfunc_end3-kernel_measureSparseMatMult
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4665729213955833856     # double 8192
.LCPI4_1:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	kernel_measureLU
	.p2align	4, 0x90
	.type	kernel_measureLU,@function
kernel_measureLU:                       # @kernel_measureLU
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 80
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	callq	new_Stopwatch
	movq	%rax, %rbp
	movl	%ebx, %edi
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	RandomMatrix
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB4_10
# BB#1:
	movl	%ebx, %edi
	movl	%ebx, %esi
	callq	new_Array2D_double
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB4_10
# BB#2:
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB4_10
# BB#3:                                 # %.preheader.preheader
	movl	$1, %r15d
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jmp	.LBB4_4
	.p2align	4, 0x90
.LBB4_8:                                # %.preheader.backedge
                                        #   in Loop: Header=BB4_4 Depth=1
	addl	%r15d, %r15d
.LBB4_4:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_6 Depth 2
	movq	%rbp, %rdi
	callq	Stopwatch_start
	testl	%r15d, %r15d
	movl	%r15d, %ebp
	jle	.LBB4_5
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph
                                        #   Parent Loop BB4_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %edi
	movl	%ebx, %esi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	Array2D_double_copy
	movl	%ebx, %edi
	movl	%ebx, %esi
	movq	%r12, %rdx
	movq	%r13, %rcx
	callq	LU_factor
	decl	%ebp
	jne	.LBB4_6
# BB#7:                                 # %._crit_edge
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	Stopwatch_stop
	cmpl	$4096, %r15d            # imm = 0x1000
	jle	.LBB4_8
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_5:                                # %._crit_edge.thread
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	Stopwatch_stop
	addl	%r15d, %r15d
	jmp	.LBB4_4
.LBB4_9:
	movl	%ebx, %edi
	callq	LU_num_flops
	mulsd	.LCPI4_0(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	Stopwatch_read
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	mulsd	.LCPI4_1(%rip), %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	Stopwatch_delete
	movq	%r13, %rdi
	callq	free
	movl	%ebx, %edi
	movl	%ebx, %esi
	movq	%r12, %rdx
	callq	Array2D_double_delete
	movl	%ebx, %edi
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	Array2D_double_delete
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_10:
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	kernel_measureLU, .Lfunc_end4-kernel_measureLU
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
