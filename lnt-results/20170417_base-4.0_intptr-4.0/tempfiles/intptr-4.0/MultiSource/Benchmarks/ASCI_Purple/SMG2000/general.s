	.text
	.file	"general.bc"
	.globl	hypre_Log2
	.p2align	4, 0x90
	.type	hypre_Log2,@function
hypre_Log2:                             # @hypre_Log2
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB0_1
# BB#2:                                 # %.preheader
	xorl	%eax, %eax
	cmpl	$1, %edi
	je	.LBB0_5
# BB#3:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	movl	%edi, %ecx
	shrl	%ecx
	cmpl	$3, %edi
	movl	%ecx, %edi
	ja	.LBB0_4
.LBB0_5:                                # %.loopexit
	retq
.LBB0_1:
	movl	$-1, %eax
	retq
.Lfunc_end0:
	.size	hypre_Log2, .Lfunc_end0-hypre_Log2
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
