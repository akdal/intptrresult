	.text
	.file	"version.bc"
	.globl	lame_print_version
	.p2align	4, 0x90
	.type	lame_print_version,@function
lame_print_version:                     # @lame_print_version
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$lpszVersion, %edi
	movl	$.L.str.2, %esi
	movl	$3, %edx
	movl	$70, %ecx
	xorl	%eax, %eax
	callq	sprintf
	movl	$.L.str, %esi
	movl	$lpszVersion, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$lpszVersion, %edi
	movl	$.L.str.2, %esi
	xorl	%edx, %edx
	movl	$77, %ecx
	xorl	%eax, %eax
	callq	sprintf
	movl	$.L.str.1, %esi
	movl	$lpszVersion, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	popq	%rbx
	jmp	fprintf                 # TAILCALL
.Lfunc_end0:
	.size	lame_print_version, .Lfunc_end0-lame_print_version
	.cfi_endproc

	.globl	get_lame_version
	.p2align	4, 0x90
	.type	get_lame_version,@function
get_lame_version:                       # @get_lame_version
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movl	$lpszVersion, %edi
	movl	$.L.str.2, %esi
	movl	$3, %edx
	movl	$70, %ecx
	xorl	%eax, %eax
	callq	sprintf
	movl	$lpszVersion, %eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	get_lame_version, .Lfunc_end1-get_lame_version
	.cfi_endproc

	.globl	get_psy_version
	.p2align	4, 0x90
	.type	get_psy_version,@function
get_psy_version:                        # @get_psy_version
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movl	$lpszVersion, %edi
	movl	$.L.str.2, %esi
	xorl	%edx, %edx
	movl	$77, %ecx
	xorl	%eax, %eax
	callq	sprintf
	movl	$lpszVersion, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	get_psy_version, .Lfunc_end2-get_psy_version
	.cfi_endproc

	.globl	get_mp3x_version
	.p2align	4, 0x90
	.type	get_mp3x_version,@function
get_mp3x_version:                       # @get_mp3x_version
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movl	$lpszVersion, %edi
	movl	$.L.str.3, %esi
	xorl	%edx, %edx
	movl	$82, %ecx
	xorl	%eax, %eax
	callq	sprintf
	movl	$lpszVersion, %eax
	popq	%rcx
	retq
.Lfunc_end3:
	.size	get_mp3x_version, .Lfunc_end3-get_mp3x_version
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"LAME version %s (www.sulaco.org/mp3) \n"
	.size	.L.str, 39

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"GPSYCHO: GPL psycho-acoustic and noise shaping model version %s. \n"
	.size	.L.str.1, 67

	.type	lpszVersion,@object     # @lpszVersion
	.local	lpszVersion
	.comm	lpszVersion,80,16
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d.%02d"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d:%02d"
	.size	.L.str.3, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
