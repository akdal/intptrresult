	.text
	.file	"box_alloc.bc"
	.globl	hypre_BoxInitializeMemory
	.p2align	4, 0x90
	.type	hypre_BoxInitializeMemory,@function
hypre_BoxInitializeMemory:              # @hypre_BoxInitializeMemory
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB0_2
# BB#1:
	movl	%edi, s_at_a_time(%rip)
.LBB0_2:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	hypre_BoxInitializeMemory, .Lfunc_end0-hypre_BoxInitializeMemory
	.cfi_endproc

	.globl	hypre_BoxFinalizeMemory
	.p2align	4, 0x90
	.type	hypre_BoxFinalizeMemory,@function
hypre_BoxFinalizeMemory:                # @hypre_BoxFinalizeMemory
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	(%rdi), %rax
	movq	%rax, s_finalize(%rip)
	callq	hypre_Free
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	s_finalize(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_1
# BB#3:                                 # %._crit_edge
	movq	$0, s_finalize(%rip)
	movq	$0, s_free(%rip)
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	hypre_BoxFinalizeMemory, .Lfunc_end1-hypre_BoxFinalizeMemory
	.cfi_endproc

	.globl	hypre_BoxAlloc
	.p2align	4, 0x90
	.type	hypre_BoxAlloc,@function
hypre_BoxAlloc:                         # @hypre_BoxAlloc
	.cfi_startproc
# BB#0:
	movq	s_free(%rip), %rcx
	testq	%rcx, %rcx
	jne	.LBB2_10
# BB#1:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movl	s_at_a_time(%rip), %eax
	shll	$3, %eax
	leal	(%rax,%rax,2), %edi
	callq	hypre_MAlloc
	movq	s_finalize(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rax, s_finalize(%rip)
	movslq	s_at_a_time(%rip), %rdx
	cmpq	$1, %rdx
	leaq	8(%rsp), %rsp
	jle	.LBB2_2
# BB#3:                                 # %.lr.ph.preheader.i
	movq	s_free(%rip), %rdi
	leal	7(%rdx), %ecx
	leaq	-2(%rdx), %r8
	andq	$7, %rcx
	je	.LBB2_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
	leaq	(%rdx,%rdx,2), %rsi
	leaq	-24(%rax,%rsi,8), %rsi
	negq	%rcx
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	decq	%rdx
	movq	%rdi, (%rsi)
	movq	%rsi, %rdi
	addq	$-24, %rsi
	incq	%rcx
	jne	.LBB2_5
.LBB2_6:                                # %.lr.ph.i.prol.loopexit
	leaq	24(%rax), %rcx
	cmpq	$7, %r8
	jb	.LBB2_9
# BB#7:                                 # %.lr.ph.preheader.i.new
	leaq	(%rdx,%rdx,2), %rsi
	leaq	-192(%rax,%rsi,8), %rax
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	168(%rax), %rsi
	movq	%rdi, 168(%rax)
	leaq	144(%rax), %rdi
	movq	%rsi, 144(%rax)
	leaq	120(%rax), %rsi
	movq	%rdi, 120(%rax)
	leaq	96(%rax), %rdi
	movq	%rsi, 96(%rax)
	leaq	72(%rax), %rsi
	movq	%rdi, 72(%rax)
	leaq	48(%rax), %rdi
	movq	%rsi, 48(%rax)
	leaq	24(%rax), %rsi
	movq	%rdi, 24(%rax)
	addq	$-8, %rdx
	movq	%rsi, (%rax)
	movq	%rax, %rdi
	addq	$-192, %rax
	cmpq	$1, %rdx
	jg	.LBB2_8
.LBB2_9:                                # %._crit_edge.loopexit.i
	movq	%rcx, s_free(%rip)
	jmp	.LBB2_10
.LBB2_2:                                # %.hypre_AllocateBoxBlock.exit_crit_edge
	movq	s_free(%rip), %rcx
.LBB2_10:                               # %hypre_AllocateBoxBlock.exit
	movq	(%rcx), %rax
	movq	%rax, s_free(%rip)
	incl	s_count(%rip)
	movq	%rcx, %rax
	retq
.Lfunc_end2:
	.size	hypre_BoxAlloc, .Lfunc_end2-hypre_BoxAlloc
	.cfi_endproc

	.globl	hypre_BoxFree
	.p2align	4, 0x90
	.type	hypre_BoxFree,@function
hypre_BoxFree:                          # @hypre_BoxFree
	.cfi_startproc
# BB#0:
	movq	s_free(%rip), %rax
	movq	%rax, (%rdi)
	movq	%rdi, s_free(%rip)
	decl	s_count(%rip)
	jne	.LBB3_5
# BB#1:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	(%rdi), %rax
	movq	%rax, s_finalize(%rip)
	callq	hypre_Free
.LBB3_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	s_finalize(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_2
# BB#4:                                 # %hypre_BoxFinalizeMemory.exit
	movq	$0, s_finalize(%rip)
	movq	$0, s_free(%rip)
	addq	$8, %rsp
.LBB3_5:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	hypre_BoxFree, .Lfunc_end3-hypre_BoxFree
	.cfi_endproc

	.type	s_at_a_time,@object     # @s_at_a_time
	.data
	.p2align	2
s_at_a_time:
	.long	1000                    # 0x3e8
	.size	s_at_a_time, 4

	.type	s_finalize,@object      # @s_finalize
	.local	s_finalize
	.comm	s_finalize,8,8
	.type	s_free,@object          # @s_free
	.local	s_free
	.comm	s_free,8,8
	.type	s_count,@object         # @s_count
	.local	s_count
	.comm	s_count,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
