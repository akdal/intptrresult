	.text
	.file	"Wildcard.bc"
	.section	.text._ZN11CStringBaseIwED2Ev,"axG",@progbits,_ZN11CStringBaseIwED2Ev,comdat
	.weak	_ZN11CStringBaseIwED2Ev
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwED2Ev,@function
_ZN11CStringBaseIwED2Ev:                # @_ZN11CStringBaseIwED2Ev
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB0_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB0_1:
	retq
.Lfunc_end0:
	.size	_ZN11CStringBaseIwED2Ev, .Lfunc_end0-_ZN11CStringBaseIwED2Ev
	.cfi_endproc

	.text
	.globl	_Z16CompareFileNamesRK11CStringBaseIwES2_
	.p2align	4, 0x90
	.type	_Z16CompareFileNamesRK11CStringBaseIwES2_,@function
_Z16CompareFileNamesRK11CStringBaseIwES2_: # @_Z16CompareFileNamesRK11CStringBaseIwES2_
	.cfi_startproc
# BB#0:
	cmpb	$0, g_CaseSensitive(%rip)
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	je	.LBB1_2
# BB#1:
	jmp	_Z15MyStringComparePKwS0_ # TAILCALL
.LBB1_2:
	jmp	_Z21MyStringCompareNoCasePKwS0_ # TAILCALL
.Lfunc_end1:
	.size	_Z16CompareFileNamesRK11CStringBaseIwES2_, .Lfunc_end1-_Z16CompareFileNamesRK11CStringBaseIwES2_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.zero	16
	.text
	.globl	_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E
	.p2align	4, 0x90
	.type	_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E,@function
_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E: # @_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	$16, %edi
	callq	_Znam
	movl	$0, (%rax)
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movslq	8(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:                                 # %.preheader
	testl	%edi, %edi
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jle	.LBB2_3
# BB#12:                                # %.lr.ph
	movl	$4, %r15d
	xorl	%r14d, %r14d
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	jmp	.LBB2_13
.LBB2_1:
	movq	%rax, %rdi
	jmp	.LBB2_10
.LBB2_3:
	xorl	%ebp, %ebp
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB2_4
.LBB2_40:                               # %vector.body.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_41
# BB#42:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_43:                               # %vector.body.prol
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r13,%rdi,4), %xmm0
	movups	16(%r13,%rdi,4), %xmm1
	movups	%xmm0, (%rax,%rdi,4)
	movups	%xmm1, 16(%rax,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB2_43
	jmp	.LBB2_44
.LBB2_41:                               #   in Loop: Header=BB2_13 Depth=1
	xorl	%edi, %edi
.LBB2_44:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpq	$24, %rdx
	jb	.LBB2_47
# BB#45:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rax,%rdi,4), %rsi
	leaq	112(%r13,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB2_46:                               # %vector.body
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB2_46
.LBB2_47:                               # %middle.block
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpq	%rcx, %rbx
	jne	.LBB2_31
	jmp	.LBB2_49
	.p2align	4, 0x90
.LBB2_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_43 Depth 2
                                        #     Child Loop BB2_46 Depth 2
                                        #     Child Loop BB2_33 Depth 2
                                        #     Child Loop BB2_36 Depth 2
                                        #     Child Loop BB2_17 Depth 2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	(%rax,%r14,4), %ebx
	cmpl	$47, %ebx
	jne	.LBB2_21
# BB#14:                                #   in Loop: Header=BB2_13 Depth=1
.Ltmp2:
	movq	%rbp, %rbx
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp3:
# BB#15:                                # %.noexc30
                                        #   in Loop: Header=BB2_13 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	%rbx, %r12
	leal	1(%rbx), %ebx
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp4:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp5:
# BB#16:                                # %.noexc.i26
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rax, (%rbp)
	movl	$0, (%rax)
	movl	%ebx, 12(%rbp)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_17:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i27
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB2_17
# BB#18:                                #   in Loop: Header=BB2_13 Depth=1
	movl	%r12d, 8(%rbp)
.Ltmp7:
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp8:
# BB#19:                                #   in Loop: Header=BB2_13 Depth=1
	movq	16(%rbx), %rax
	movslq	12(%rbx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rbx)
	xorl	%ebp, %ebp
	movq	%r13, %r12
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB2_52
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_13 Depth=1
	movl	%r15d, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB2_22
# BB#23:                                #   in Loop: Header=BB2_13 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %r15d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r15d
	jl	.LBB2_25
# BB#24:                                # %select.true.sink
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	%r15d, %edx
	shrl	$31, %edx
	addl	%r15d, %edx
	sarl	%edx
.LBB2_25:                               # %select.end
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r15,%rsi), %eax
	cmpl	%r15d, %eax
	jne	.LBB2_26
.LBB2_22:                               #   in Loop: Header=BB2_13 Depth=1
	movl	%r15d, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB2_51
.LBB2_26:                               #   in Loop: Header=BB2_13 Depth=1
	movq	%rbp, %r12
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movl	%eax, 28(%rsp)          # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp0:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp1:
# BB#27:                                # %.noexc36
                                        #   in Loop: Header=BB2_13 Depth=1
	testl	%r15d, %r15d
	movl	8(%rsp), %ebx           # 4-byte Reload
	movq	%r12, %rbp
	jle	.LBB2_50
# BB#28:                                # %.preheader.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	testl	%ebp, %ebp
	jle	.LBB2_48
# BB#29:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movslq	%ebp, %rbx
	cmpl	$7, %ebp
	jbe	.LBB2_30
# BB#37:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rbx, %rcx
	andq	$-8, %rcx
	je	.LBB2_30
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_13 Depth=1
	leaq	(%r13,%rbx,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB2_40
# BB#39:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_13 Depth=1
	leaq	(%rax,%rbx,4), %rdx
	cmpq	%rdx, 32(%rsp)          # 8-byte Folded Reload
	jae	.LBB2_40
.LBB2_30:                               #   in Loop: Header=BB2_13 Depth=1
	xorl	%ecx, %ecx
.LBB2_31:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbx), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB2_34
# BB#32:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB2_33:                               # %scalar.ph.prol
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rcx,4), %edi
	movl	%edi, (%rax,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_33
.LBB2_34:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpq	$7, %rdx
	jb	.LBB2_49
# BB#35:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB2_13 Depth=1
	subq	%rcx, %rbx
	leaq	28(%rax,%rcx,4), %rdx
	leaq	28(%r13,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB2_36:                               # %scalar.ph
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rbx
	jne	.LBB2_36
	jmp	.LBB2_49
.LBB2_48:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	testq	%r13, %r13
	je	.LBB2_50
.LBB2_49:                               # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movq	%r12, %rbp
	movl	8(%rsp), %ebx           # 4-byte Reload
.LBB2_50:                               # %._crit_edge16.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movslq	%ebp, %rcx
	movl	$0, (%rax,%rcx,4)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	28(%rsp), %ecx          # 4-byte Reload
.LBB2_51:                               # %_ZN11CStringBaseIwEpLEw.exit
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%r13, %r12
	movslq	%ebp, %rdx
	movl	%ebx, (%r12,%rdx,4)
	incl	%ebp
	leaq	4(%r12,%rdx,4), %r13
	movl	%ecx, %r15d
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB2_52:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$0, (%r13)
	incq	%r14
	cmpq	%rdi, %r14
	movq	%r12, %r13
	jl	.LBB2_13
.LBB2_4:                                # %._crit_edge
.Ltmp10:
	movq	%rbp, %rbx
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp11:
# BB#5:                                 # %.noexc
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	%rbx, %r14
	leal	1(%rbx), %ebx
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp12:
	callq	_Znam
.Ltmp13:
# BB#6:                                 # %.noexc.i
	movq	%rax, (%rbp)
	movl	$0, (%rax)
	movl	%ebx, 12(%rbp)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_7:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB2_7
# BB#8:
	movl	%r14d, 8(%rbp)
.Ltmp15:
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp16:
# BB#9:
	movq	16(%rbx), %rax
	movslq	12(%rbx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rbx)
	testq	%r12, %r12
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB2_53
.LBB2_10:                               # %.thread
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZdaPv                  # TAILCALL
.LBB2_53:                               # %_ZN11CStringBaseIwED2Ev.exit37
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_11:
.Ltmp14:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB2_55
.LBB2_54:
.Ltmp17:
	movq	%rax, %rbx
.LBB2_55:                               # %.body32
	movq	%r12, %r13
	testq	%r13, %r13
	jne	.LBB2_57
	jmp	.LBB2_58
.LBB2_59:
.Ltmp6:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	testq	%r13, %r13
	jne	.LBB2_57
	jmp	.LBB2_58
.LBB2_20:
.Ltmp9:
	movq	%rax, %rbx
	testq	%r13, %r13
	je	.LBB2_58
.LBB2_57:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB2_58:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E, .Lfunc_end2-_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp2-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp5-.Ltmp4           #   Call between .Ltmp4 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp1-.Ltmp7           #   Call between .Ltmp7 and .Ltmp1
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Lfunc_end2-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.text
	.globl	_Z16SplitPathToPartsRK11CStringBaseIwERS0_S3_
	.p2align	4, 0x90
	.type	_Z16SplitPathToPartsRK11CStringBaseIwERS0_S3_,@function
_Z16SplitPathToPartsRK11CStringBaseIwERS0_S3_: # @_Z16SplitPathToPartsRK11CStringBaseIwERS0_S3_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %r15
	movq	(%r15), %rax
	movslq	8(%r15), %rcx
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rbx
	testq	%rbx, %rbx
	jle	.LBB4_3
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	leaq	-1(%rbx), %rcx
	cmpl	$47, -4(%rax,%rbx,4)
	jne	.LBB4_1
.LBB4_3:
	movq	%rsp, %r14
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%ebx, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%r13, %r14
	je	.LBB4_4
# BB#5:
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	$0, 8(%r13)
	movq	(%r13), %rbp
	movl	$0, (%rbp)
	movslq	8(%rsp), %r12
	incq	%r12
	movl	12(%r13), %r14d
	cmpl	%r14d, %r12d
	jne	.LBB4_7
# BB#6:
	movq	%rsp, %r14
	jmp	.LBB4_13
.LBB4_4:                                # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	jne	.LBB4_17
	jmp	.LBB4_18
.LBB4_7:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp18:
	callq	_Znam
.Ltmp19:
# BB#8:                                 # %.noexc
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	je	.LBB4_9
# BB#10:                                # %.noexc
	testl	%r14d, %r14d
	movq	%rsp, %r14
	jle	.LBB4_12
# BB#11:                                # %._crit_edge.thread.i.i
	movq	%rbp, %rdi
	movq	%rax, %rbp
	callq	_ZdaPv
	movq	%rbp, %rax
	movslq	8(%r13), %rcx
	jmp	.LBB4_12
.LBB4_9:
	movq	%rsp, %r14
.LBB4_12:                               # %._crit_edge16.i.i
	movq	%rax, (%r13)
	movl	$0, (%rax,%rcx,4)
	movl	%r12d, 12(%r13)
	movq	%rax, %rbp
.LBB4_13:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_14:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB4_14
# BB#15:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%rsp), %eax
	movl	%eax, 8(%r13)
	testq	%rdi, %rdi
	je	.LBB4_18
.LBB4_17:
	callq	_ZdaPv
.LBB4_18:                               # %_ZN11CStringBaseIwED2Ev.exit18
	movl	8(%r15), %ecx
	subl	%ebx, %ecx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%r12, %r14
	je	.LBB4_19
# BB#20:
	movl	$0, 8(%r12)
	movq	(%r12), %rbx
	movl	$0, (%rbx)
	movslq	8(%rsp), %rbp
	incq	%rbp
	movl	12(%r12), %r14d
	cmpl	%r14d, %ebp
	je	.LBB4_26
# BB#21:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp21:
	callq	_Znam
	movq	%rax, %r15
.Ltmp22:
# BB#22:                                # %.noexc28
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB4_25
# BB#23:                                # %.noexc28
	testl	%r14d, %r14d
	jle	.LBB4_25
# BB#24:                                # %._crit_edge.thread.i.i22
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r12), %rax
.LBB4_25:                               # %._crit_edge16.i.i23
	movq	%r15, (%r12)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 12(%r12)
	movq	%r15, %rbx
.LBB4_26:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i24
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_27:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB4_27
# BB#28:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i27
	movl	8(%rsp), %eax
	movl	%eax, 8(%r12)
	testq	%rdi, %rdi
	jne	.LBB4_30
	jmp	.LBB4_31
.LBB4_19:                               # %_ZN11CStringBaseIwED2Ev.exit18._ZN11CStringBaseIwEaSERKS0_.exit29_crit_edge
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB4_31
.LBB4_30:
	callq	_ZdaPv
.LBB4_31:                               # %_ZN11CStringBaseIwED2Ev.exit17
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_33:
.Ltmp23:
	jmp	.LBB4_34
.LBB4_32:
.Ltmp20:
.LBB4_34:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_36
# BB#35:
	callq	_ZdaPv
.LBB4_36:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_Z16SplitPathToPartsRK11CStringBaseIwERS0_S3_, .Lfunc_end4-_Z16SplitPathToPartsRK11CStringBaseIwERS0_S3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp18-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp21-.Ltmp19         #   Call between .Ltmp19 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin1   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z24ExtractDirPrefixFromPathRK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_Z24ExtractDirPrefixFromPathRK11CStringBaseIwE,@function
_Z24ExtractDirPrefixFromPathRK11CStringBaseIwE: # @_Z24ExtractDirPrefixFromPathRK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rsi), %rax
	movslq	8(%rsi), %rdx
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	testq	%rcx, %rcx
	jle	.LBB5_3
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	leaq	-1(%rcx), %rdx
	cmpl	$47, -4(%rax,%rcx,4)
	jne	.LBB5_1
.LBB5_3:
	xorl	%edx, %edx
	movq	%rbx, %rdi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_Z24ExtractDirPrefixFromPathRK11CStringBaseIwE, .Lfunc_end5-_Z24ExtractDirPrefixFromPathRK11CStringBaseIwE
	.cfi_endproc

	.globl	_Z23ExtractFileNameFromPathRK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_Z23ExtractFileNameFromPathRK11CStringBaseIwE,@function
_Z23ExtractFileNameFromPathRK11CStringBaseIwE: # @_Z23ExtractFileNameFromPathRK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rsi), %rax
	movslq	8(%rsi), %rdx
	leaq	-4(%rax,%rdx,4), %rax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rcx
	leaq	(%rdx,%rcx), %rdi
	testq	%rdi, %rdi
	jle	.LBB6_3
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	leaq	-1(%rcx), %rdi
	cmpl	$47, (%rax,%rcx,4)
	jne	.LBB6_1
.LBB6_3:
	addl	%ecx, %edx
	negl	%ecx
	movq	%rbx, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_Z23ExtractFileNameFromPathRK11CStringBaseIwE, .Lfunc_end6-_Z23ExtractFileNameFromPathRK11CStringBaseIwE
	.cfi_endproc

	.globl	_Z23CompareWildCardWithNameRK11CStringBaseIwES2_
	.p2align	4, 0x90
	.type	_Z23CompareWildCardWithNameRK11CStringBaseIwES2_,@function
_Z23CompareWildCardWithNameRK11CStringBaseIwES2_: # @_Z23CompareWildCardWithNameRK11CStringBaseIwES2_
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	jmp	_ZL16EnhancedMaskTestPKwS0_ # TAILCALL
.Lfunc_end7:
	.size	_Z23CompareWildCardWithNameRK11CStringBaseIwES2_, .Lfunc_end7-_Z23CompareWildCardWithNameRK11CStringBaseIwES2_
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL16EnhancedMaskTestPKwS0_,@function
_ZL16EnhancedMaskTestPKwS0_:            # @_ZL16EnhancedMaskTestPKwS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 48
.Lcfi35:
	.cfi_offset %rbx, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	jmp	.LBB8_2
	.p2align	4, 0x90
.LBB8_1:                                #   in Loop: Header=BB8_2 Depth=1
	addq	$4, %rbx
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movl	(%r14), %edi
	movl	(%rbx), %ebp
	cmpl	$42, %edi
	je	.LBB8_8
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpl	$63, %edi
	je	.LBB8_10
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	testl	%edi, %edi
	je	.LBB8_12
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpl	%ebp, %edi
	je	.LBB8_11
# BB#6:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpb	$0, g_CaseSensitive(%rip)
	jne	.LBB8_17
# BB#7:                                 #   in Loop: Header=BB8_2 Depth=1
	callq	_Z11MyCharUpperw
	movl	%eax, %r15d
	movl	%ebp, %edi
	callq	_Z11MyCharUpperw
	cmpl	%eax, %r15d
	je	.LBB8_11
	jmp	.LBB8_17
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_2 Depth=1
	leaq	4(%r14), %rdi
	movq	%rbx, %rsi
	callq	_ZL16EnhancedMaskTestPKwS0_
	testb	%al, %al
	jne	.LBB8_13
# BB#9:                                 #   in Loop: Header=BB8_2 Depth=1
	testl	%ebp, %ebp
	jne	.LBB8_1
	jmp	.LBB8_17
	.p2align	4, 0x90
.LBB8_10:                               #   in Loop: Header=BB8_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB8_17
.LBB8_11:                               #   in Loop: Header=BB8_2 Depth=1
	addq	$4, %r14
	jmp	.LBB8_1
.LBB8_12:
	testl	%ebp, %ebp
	sete	%al
	jmp	.LBB8_18
.LBB8_17:
	xorl	%eax, %eax
	jmp	.LBB8_18
.LBB8_13:
	movb	$1, %al
.LBB8_18:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZL16EnhancedMaskTestPKwS0_, .Lfunc_end8-_ZL16EnhancedMaskTestPKwS0_
	.cfi_endproc

	.globl	_Z23DoesNameContainWildCardRK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_Z23DoesNameContainWildCardRK11CStringBaseIwE,@function
_Z23DoesNameContainWildCardRK11CStringBaseIwE: # @_Z23DoesNameContainWildCardRK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	movslq	8(%rdi), %r10
	testq	%r10, %r10
	movl	$-1, %r8d
	jle	.LBB9_9
# BB#1:                                 # %.lr.ph.i
	movq	(%rdi), %r9
	movq	_ZL16kWildCardCharSet(%rip), %rdx
	movl	(%rdx), %r11d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_4 Depth 2
	movl	(%r9,%rsi,4), %ecx
	cmpl	%ecx, %r11d
	movq	%rdx, %rdi
	je	.LBB9_6
# BB#3:                                 # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	%r11d, %eax
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph.i.i.i
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%eax, %eax
	je	.LBB9_8
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=2
	movl	4(%rdi), %eax
	addq	$4, %rdi
	cmpl	%ecx, %eax
	jne	.LBB9_4
.LBB9_6:                                # %_ZNK11CStringBaseIwE4FindEw.exit.i
                                        #   in Loop: Header=BB9_2 Depth=1
	subq	%rdx, %rdi
	shrq	$2, %rdi
	testl	%edi, %edi
	jns	.LBB9_7
.LBB9_8:                                # %_ZNK11CStringBaseIwE4FindEw.exit.thread.i
                                        #   in Loop: Header=BB9_2 Depth=1
	incq	%rsi
	cmpq	%r10, %rsi
	jl	.LBB9_2
	jmp	.LBB9_9
.LBB9_7:
	movl	%esi, %r8d
.LBB9_9:                                # %_ZNK11CStringBaseIwE9FindOneOfERKS0_.exit
	testl	%r8d, %r8d
	setns	%al
	retq
.Lfunc_end9:
	.size	_Z23DoesNameContainWildCardRK11CStringBaseIwE, .Lfunc_end9-_Z23DoesNameContainWildCardRK11CStringBaseIwE
	.cfi_endproc

	.globl	_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb
	.p2align	4, 0x90
	.type	_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb,@function
_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb: # @_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 64
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testb	%dl, %dl
	jne	.LBB10_2
# BB#1:
	cmpb	$0, 34(%rbx)
	je	.LBB10_13
.LBB10_2:
	movl	12(%r14), %esi
	movl	12(%rbx), %ecx
	subl	%ecx, %esi
	js	.LBB10_9
# BB#3:
	testb	%dl, %dl
	je	.LBB10_10
# BB#4:
	movb	34(%rbx), %dil
	testb	%dil, %dil
	je	.LBB10_23
.LBB10_5:
	movb	33(%rbx), %r8b
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.LBB10_7
# BB#6:
	testb	%r8b, %r8b
	je	.LBB10_27
.LBB10_7:                               # %._crit_edge62
	movb	32(%rbx), %r9b
	cmpb	$0, %r9b
	movl	%esi, %edx
	cmovel	%eax, %edx
	testb	%dil, %dil
	cmovnel	%eax, %edx
	cmpb	$0, %r9b
	je	.LBB10_11
# BB#8:
	movzbl	%r8b, %eax
	leal	-1(%rax,%rsi), %eax
	cmpl	%eax, %edx
	jg	.LBB10_13
	jmp	.LBB10_14
.LBB10_9:
	xorl	%eax, %eax
	jmp	.LBB10_27
.LBB10_10:                              # %.thread
	xorl	%edx, %edx
	cmpb	$0, 32(%rbx)
	cmovel	%edx, %esi
	movl	%esi, %eax
.LBB10_11:
	cmpl	%eax, %edx
	jle	.LBB10_14
.LBB10_13:
	xorl	%eax, %eax
.LBB10_27:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_14:                              # %.preheader.lr.ph
	movslq	%edx, %r15
	cltq
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	(,%r15,8), %r12
	.p2align	4, 0x90
.LBB10_15:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_17 Depth 2
	testl	%ecx, %ecx
	movl	$0, %ebp
	jle	.LBB10_20
# BB#16:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB10_15 Depth=1
	movq	%r12, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_17:                              # %.lr.ph
                                        #   Parent Loop BB10_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	16(%r14), %rcx
	movq	(%rcx,%r13), %rcx
	movq	(%rax), %rdi
	movq	(%rcx), %rsi
	callq	_ZL16EnhancedMaskTestPKwS0_
	testb	%al, %al
	je	.LBB10_19
# BB#18:                                #   in Loop: Header=BB10_17 Depth=2
	incq	%rbp
	movslq	12(%rbx), %rcx
	addq	$8, %r13
	cmpq	%rcx, %rbp
	jl	.LBB10_17
	jmp	.LBB10_20
	.p2align	4, 0x90
.LBB10_19:                              # %.lr.ph.._crit_edge.loopexit_crit_edge
                                        #   in Loop: Header=BB10_15 Depth=1
	movl	12(%rbx), %ecx
.LBB10_20:                              # %._crit_edge
                                        #   in Loop: Header=BB10_15 Depth=1
	cmpl	%ecx, %ebp
	je	.LBB10_26
# BB#21:                                #   in Loop: Header=BB10_15 Depth=1
	addq	$8, %r12
	cmpq	(%rsp), %r15            # 8-byte Folded Reload
	leaq	1(%r15), %r15
	jl	.LBB10_15
# BB#22:
	xorl	%eax, %eax
	jmp	.LBB10_27
.LBB10_23:
	testl	%esi, %esi
	je	.LBB10_5
# BB#24:
	movb	32(%rbx), %al
	testb	%al, %al
	jne	.LBB10_5
# BB#25:
	xorl	%eax, %eax
	jmp	.LBB10_27
.LBB10_26:
	movb	$1, %al
	jmp	.LBB10_27
.Lfunc_end10:
	.size	_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb, .Lfunc_end10-_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb
	.cfi_endproc

	.globl	_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE,@function
_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE: # @_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 48
.Lcfi57:
	.cfi_offset %rbx, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	$-1, %r14d
	cmpl	$0, 36(%rbp)
	jle	.LBB11_8
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movq	40(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	cmpb	$0, g_CaseSensitive(%rip)
	movq	8(%rax), %rdi
	movq	(%r15), %rsi
	je	.LBB11_4
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	jne	.LBB11_6
	jmp	.LBB11_7
	.p2align	4, 0x90
.LBB11_4:                               #   in Loop: Header=BB11_2 Depth=1
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB11_7
.LBB11_6:                               #   in Loop: Header=BB11_2 Depth=1
	incq	%rbx
	movslq	36(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB11_2
	jmp	.LBB11_8
.LBB11_7:                               # %_Z16CompareFileNamesRK11CStringBaseIwES2_.exit.._crit_edge.loopexit_crit_edge
	movl	%ebx, %r14d
.LBB11_8:                               # %._crit_edge
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE, .Lfunc_end11-_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE
	.cfi_endproc

	.globl	_ZN9NWildcard11CCensorNode13AddItemSimpleEbRNS_5CItemE
	.p2align	4, 0x90
	.type	_ZN9NWildcard11CCensorNode13AddItemSimpleEbRNS_5CItemE,@function
_ZN9NWildcard11CCensorNode13AddItemSimpleEbRNS_5CItemE: # @_ZN9NWildcard11CCensorNode13AddItemSimpleEbRNS_5CItemE
	.cfi_startproc
# BB#0:
	testb	%sil, %sil
	je	.LBB12_2
# BB#1:
	addq	$56, %rdi
	movq	%rdx, %rsi
	jmp	_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_ # TAILCALL
.LBB12_2:
	addq	$88, %rdi
	movq	%rdx, %rsi
	jmp	_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_ # TAILCALL
.Lfunc_end12:
	.size	_ZN9NWildcard11CCensorNode13AddItemSimpleEbRNS_5CItemE, .Lfunc_end12-_ZN9NWildcard11CCensorNode13AddItemSimpleEbRNS_5CItemE
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_,@function
_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_: # @_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 32
.Lcfi64:
	.cfi_offset %rbx, -32
.Lcfi65:
	.cfi_offset %r14, -24
.Lcfi66:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	$8, 24(%rbx)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp24:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
.Ltmp25:
# BB#1:
	movb	34(%r15), %al
	movb	%al, 34(%rbx)
	movzwl	32(%r15), %eax
	movw	%ax, 32(%rbx)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rcx
	movslq	12(%r14), %rax
	movq	%rbx, (%rcx,%rax,8)
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r14)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB13_2:
.Ltmp26:
	movq	%rax, %r14
.Ltmp27:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp28:
# BB#3:                                 # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB13_4:
.Ltmp29:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_, .Lfunc_end13-_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp24-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin2   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp27-.Ltmp25         #   Call between .Ltmp25 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin2   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end13-.Ltmp28    #   Call between .Ltmp28 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NWildcard11CCensorNode7AddItemEbRNS_5CItemE
	.p2align	4, 0x90
	.type	_ZN9NWildcard11CCensorNode7AddItemEbRNS_5CItemE,@function
_ZN9NWildcard11CCensorNode7AddItemEbRNS_5CItemE: # @_ZN9NWildcard11CCensorNode7AddItemEbRNS_5CItemE
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 176
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r12d
	movq	%rdi, %r15
	cmpl	$1, 12(%r14)
	jg	.LBB14_4
.LBB14_1:
	testb	%r12b, %r12b
	je	.LBB14_17
# BB#2:
	addq	$56, %r15
	jmp	.LBB14_3
.LBB14_4:
	movq	16(%r14), %rax
	movq	(%rax), %r13
	movslq	8(%r13), %rax
	testq	%rax, %rax
	jle	.LBB14_12
# BB#5:                                 # %.lr.ph.i.i
	movq	(%r13), %r8
	movq	_ZL16kWildCardCharSet(%rip), %rbp
	movl	(%rbp), %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_8 Depth 2
	movl	(%r8,%rdx,4), %ebx
	cmpl	%ebx, %ecx
	movq	%rbp, %rsi
	je	.LBB14_10
# BB#7:                                 # %.lr.ph.i.i.i.i.preheader
                                        #   in Loop: Header=BB14_6 Depth=1
	movl	%ecx, %edi
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB14_8:                               # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB14_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edi, %edi
	je	.LBB14_11
# BB#9:                                 #   in Loop: Header=BB14_8 Depth=2
	movl	4(%rsi), %edi
	addq	$4, %rsi
	cmpl	%ebx, %edi
	jne	.LBB14_8
.LBB14_10:                              # %_ZNK11CStringBaseIwE4FindEw.exit.i.i
                                        #   in Loop: Header=BB14_6 Depth=1
	subq	%rbp, %rsi
	shrq	$2, %rsi
	testl	%esi, %esi
	jns	.LBB14_16
.LBB14_11:                              # %_ZNK11CStringBaseIwE4FindEw.exit.thread.i.i
                                        #   in Loop: Header=BB14_6 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB14_6
	jmp	.LBB14_12
.LBB14_17:
	addq	$88, %r15
.LBB14_3:
	movq	%r15, %rdi
	movq	%r14, %rsi
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_ # TAILCALL
.LBB14_16:                              # %_Z23DoesNameContainWildCardRK11CStringBaseIwE.exit
	testl	%edx, %edx
	jns	.LBB14_1
.LBB14_12:                              # %_Z23DoesNameContainWildCardRK11CStringBaseIwE.exit.thread
	cmpl	$0, 36(%r15)
	jle	.LBB14_21
# BB#13:                                # %.lr.ph.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_14:                              # =>This Inner Loop Header: Depth=1
	movq	40(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	cmpb	$0, g_CaseSensitive(%rip)
	movq	8(%rax), %rdi
	movq	(%r13), %rsi
	je	.LBB14_18
# BB#15:                                #   in Loop: Header=BB14_14 Depth=1
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	jne	.LBB14_20
	jmp	.LBB14_23
	.p2align	4, 0x90
.LBB14_18:                              #   in Loop: Header=BB14_14 Depth=1
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB14_23
.LBB14_20:                              #   in Loop: Header=BB14_14 Depth=1
	incq	%rbp
	movslq	36(%r15), %rax
	cmpq	%rax, %rbp
	jl	.LBB14_14
	jmp	.LBB14_21
.LBB14_23:                              # %_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE.exit
	testl	%ebp, %ebp
	js	.LBB14_21
# BB#24:                                # %_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE.exit._crit_edge
	movl	%r12d, %ebx
	jmp	.LBB14_25
.LBB14_21:                              # %_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE.exit.thread
	movl	%r12d, %ebx
	movq	%r15, (%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rsp)
	movslq	8(%r13), %rbp
	leaq	1(%rbp), %r12
	testl	%r12d, %r12d
	je	.LBB14_22
# BB#31:                                # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 8(%rsp)
	movl	$0, (%rax)
	movl	%r12d, 20(%rsp)
	jmp	.LBB14_32
.LBB14_22:
	xorl	%eax, %eax
.LBB14_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	(%r13), %rcx
	.p2align	4, 0x90
.LBB14_33:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB14_33
# BB#34:                                # %_ZN9NWildcard11CCensorNodeC2ERK11CStringBaseIwEPS0_.exit
	movl	%ebp, 16(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rsp)
	movq	$8, 48(%rsp)
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, 24(%rsp)
	movups	%xmm0, 64(%rsp)
	movq	$8, 80(%rsp)
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 56(%rsp)
	movups	%xmm0, 96(%rsp)
	movq	$8, 112(%rsp)
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 88(%rsp)
.Ltmp30:
	movl	$120, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp31:
# BB#35:                                # %.noexc
.Ltmp32:
	movq	%rsp, %rsi
	movq	%r12, %rdi
	callq	_ZN9NWildcard11CCensorNodeC2ERKS0_
.Ltmp33:
# BB#36:
	leaq	24(%r15), %rdi
.Ltmp35:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp36:
# BB#37:
	movq	40(%r15), %rax
	movslq	36(%r15), %rbp
	movq	%r12, (%rax,%rbp,8)
	leal	1(%rbp), %eax
	movl	%eax, 36(%r15)
	movq	%rsp, %rdi
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.LBB14_25:
	movl	12(%r14), %eax
	cmpl	$2, %eax
	movl	$1, %r13d
	cmovll	%eax, %r13d
	testl	%eax, %eax
	jle	.LBB14_30
# BB#26:                                # %.lr.ph.i23
	movq	16(%r14), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.LBB14_30
# BB#27:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB14_29
# BB#28:
	callq	_ZdaPv
.LBB14_29:                              # %_ZN11CStringBaseIwED2Ev.exit.i
	movq	%r12, %rdi
	callq	_ZdlPv
.LBB14_30:                              # %_ZN9NWildcard11CCensorNode13AddItemSimpleEbRNS_5CItemE.exit
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%r13d, %edx
	callq	_ZN17CBaseRecordVector6DeleteEii
	movq	40(%r15), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rdi
	movzbl	%bl, %esi
	movq	%r14, %rdx
	callq	_ZN9NWildcard11CCensorNode7AddItemEbRNS_5CItemE
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_42:
.Ltmp34:
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZdlPv
	jmp	.LBB14_39
.LBB14_38:
.Ltmp37:
	movq	%rax, %rbx
.LBB14_39:                              # %.body
.Ltmp38:
	movq	%rsp, %rdi
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.Ltmp39:
# BB#40:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB14_41:
.Ltmp40:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN9NWildcard11CCensorNode7AddItemEbRNS_5CItemE, .Lfunc_end14-_ZN9NWildcard11CCensorNode7AddItemEbRNS_5CItemE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp30-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp37-.Lfunc_begin3   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin3   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin3   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp38-.Ltmp36         #   Call between .Ltmp36 and .Ltmp38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin3   #     jumps to .Ltmp40
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Lfunc_end14-.Ltmp39    #   Call between .Ltmp39 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NWildcard11CCensorNodeD2Ev,"axG",@progbits,_ZN9NWildcard11CCensorNodeD2Ev,comdat
	.weak	_ZN9NWildcard11CCensorNodeD2Ev
	.p2align	4, 0x90
	.type	_ZN9NWildcard11CCensorNodeD2Ev,@function
_ZN9NWildcard11CCensorNodeD2Ev:         # @_ZN9NWildcard11CCensorNodeD2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 32
.Lcfi83:
	.cfi_offset %rbx, -32
.Lcfi84:
	.cfi_offset %r14, -24
.Lcfi85:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	88(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 88(%r15)
.Ltmp41:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp42:
# BB#1:
.Ltmp47:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp48:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit
	leaq	56(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 56(%r15)
.Ltmp58:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp59:
# BB#3:
.Ltmp64:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp65:
# BB#4:                                 # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit6
	leaq	24(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, 24(%r15)
.Ltmp76:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp77:
# BB#5:
.Ltmp82:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp83:
# BB#6:                                 # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev.exit
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB15_14
# BB#7:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB15_14:                              # %_ZN11CStringBaseIwED2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB15_26:
.Ltmp84:
	movq	%rax, %r14
	jmp	.LBB15_20
.LBB15_12:
.Ltmp78:
	movq	%rax, %r14
.Ltmp79:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp80:
	jmp	.LBB15_20
.LBB15_13:
.Ltmp81:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_25:
.Ltmp66:
	movq	%rax, %r14
	jmp	.LBB15_18
.LBB15_10:
.Ltmp60:
	movq	%rax, %r14
.Ltmp61:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp62:
	jmp	.LBB15_18
.LBB15_11:
.Ltmp63:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_15:
.Ltmp49:
	movq	%rax, %r14
	jmp	.LBB15_16
.LBB15_8:
.Ltmp43:
	movq	%rax, %r14
.Ltmp44:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp45:
.LBB15_16:                              # %.body
	leaq	56(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 56(%r15)
.Ltmp50:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp51:
# BB#17:
.Ltmp56:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp57:
.LBB15_18:                              # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit11
	leaq	24(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, 24(%r15)
.Ltmp67:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp68:
# BB#19:
.Ltmp73:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp74:
.LBB15_20:                              # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev.exit14
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB15_22
# BB#21:
	callq	_ZdaPv
.LBB15_22:                              # %_ZN11CStringBaseIwED2Ev.exit15
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_9:
.Ltmp46:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_23:
.Ltmp52:
	movq	%rax, %r14
.Ltmp53:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp54:
	jmp	.LBB15_30
.LBB15_24:
.Ltmp55:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_27:
.Ltmp69:
	movq	%rax, %r14
.Ltmp70:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp71:
	jmp	.LBB15_30
.LBB15_28:
.Ltmp72:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_29:
.Ltmp75:
	movq	%rax, %r14
.LBB15_30:                              # %.body9
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN9NWildcard11CCensorNodeD2Ev, .Lfunc_end15-_ZN9NWildcard11CCensorNodeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\331\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Ltmp41-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin4   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin4   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin4   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin4   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin4   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin4   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin4   # >> Call Site 7 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin4   #     jumps to .Ltmp81
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin4   # >> Call Site 8 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin4   #     jumps to .Ltmp63
	.byte	1                       #   On action: 1
	.long	.Ltmp44-.Lfunc_begin4   # >> Call Site 9 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin4   #     jumps to .Ltmp46
	.byte	1                       #   On action: 1
	.long	.Ltmp50-.Lfunc_begin4   # >> Call Site 10 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin4   #     jumps to .Ltmp52
	.byte	1                       #   On action: 1
	.long	.Ltmp56-.Lfunc_begin4   # >> Call Site 11 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp75-.Lfunc_begin4   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp67-.Lfunc_begin4   # >> Call Site 12 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin4   #     jumps to .Ltmp69
	.byte	1                       #   On action: 1
	.long	.Ltmp73-.Lfunc_begin4   # >> Call Site 13 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin4   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp74-.Lfunc_begin4   # >> Call Site 14 <<
	.long	.Ltmp53-.Ltmp74         #   Call between .Ltmp74 and .Ltmp53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin4   # >> Call Site 15 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin4   #     jumps to .Ltmp55
	.byte	1                       #   On action: 1
	.long	.Ltmp70-.Lfunc_begin4   # >> Call Site 16 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin4   #     jumps to .Ltmp72
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 64
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB16_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB16_6
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_5
# BB#4:                                 #   in Loop: Header=BB16_2 Depth=1
	callq	_ZdaPv
.LBB16_5:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB16_6:                               #   in Loop: Header=BB16_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB16_2
.LBB16_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end16:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end16-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.text
	.globl	_ZN9NWildcard11CCensorNode7AddItemEbRK11CStringBaseIwEbbb
	.p2align	4, 0x90
	.type	_ZN9NWildcard11CCensorNode7AddItemEbRK11CStringBaseIwEbbb,@function
_ZN9NWildcard11CCensorNode7AddItemEbRK11CStringBaseIwEbbb: # @_ZN9NWildcard11CCensorNode7AddItemEbRK11CStringBaseIwEbbb
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi104:
	.cfi_def_cfa_offset 96
.Lcfi105:
	.cfi_offset %rbx, -48
.Lcfi106:
	.cfi_offset %r12, -40
.Lcfi107:
	.cfi_offset %r14, -32
.Lcfi108:
	.cfi_offset %r15, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movl	%r8d, %ebp
	movl	%ecx, %ebx
	movl	%esi, %r12d
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rsp)
	movq	$8, 32(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 8(%rsp)
.Ltmp85:
	leaq	8(%rsp), %rsi
	movq	%rdx, %rdi
	callq	_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E
.Ltmp86:
# BB#1:
	movb	%bl, 40(%rsp)
	movb	%bpl, 41(%rsp)
	movb	%r15b, 42(%rsp)
.Ltmp87:
	movzbl	%r12b, %esi
	leaq	8(%rsp), %rdx
	movq	%r14, %rdi
	callq	_ZN9NWildcard11CCensorNode7AddItemEbRNS_5CItemE
.Ltmp88:
# BB#2:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 8(%rsp)
.Ltmp99:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp100:
# BB#3:                                 # %_ZN9NWildcard5CItemD2Ev.exit
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_4:
.Ltmp101:
	movq	%rax, %rbx
.Ltmp102:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp103:
	jmp	.LBB17_5
.LBB17_6:
.Ltmp104:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB17_9:
.Ltmp89:
	movq	%rax, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 8(%rsp)
.Ltmp90:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp91:
# BB#10:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit.i
.Ltmp96:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp97:
.LBB17_5:                               # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB17_11:
.Ltmp98:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB17_7:
.Ltmp92:
	movq	%rax, %rbx
.Ltmp93:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp94:
# BB#12:                                # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB17_8:
.Ltmp95:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN9NWildcard11CCensorNode7AddItemEbRK11CStringBaseIwEbbb, .Lfunc_end17-_ZN9NWildcard11CCensorNode7AddItemEbRK11CStringBaseIwEbbb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp85-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp88-.Ltmp85         #   Call between .Ltmp85 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin5   #     jumps to .Ltmp89
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin5  #     jumps to .Ltmp101
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp102-.Ltmp100       #   Call between .Ltmp100 and .Ltmp102
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin5  #     jumps to .Ltmp104
	.byte	1                       #   On action: 1
	.long	.Ltmp90-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin5   #     jumps to .Ltmp92
	.byte	1                       #   On action: 1
	.long	.Ltmp96-.Lfunc_begin5   # >> Call Site 6 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin5   #     jumps to .Ltmp98
	.byte	1                       #   On action: 1
	.long	.Ltmp97-.Lfunc_begin5   # >> Call Site 7 <<
	.long	.Ltmp93-.Ltmp97         #   Call between .Ltmp97 and .Ltmp93
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp93-.Lfunc_begin5   # >> Call Site 8 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin5   #     jumps to .Ltmp95
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK9NWildcard11CCensorNode16NeedCheckSubDirsEv
	.p2align	4, 0x90
	.type	_ZNK9NWildcard11CCensorNode16NeedCheckSubDirsEv,@function
_ZNK9NWildcard11CCensorNode16NeedCheckSubDirsEv: # @_ZNK9NWildcard11CCensorNode16NeedCheckSubDirsEv
	.cfi_startproc
# BB#0:
	movslq	68(%rdi), %rcx
	testq	%rcx, %rcx
	jle	.LBB18_1
# BB#4:                                 # %.lr.ph
	movq	72(%rdi), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB18_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rdi
	movb	$1, %al
	cmpb	$0, 32(%rdi)
	jne	.LBB18_7
# BB#6:                                 #   in Loop: Header=BB18_5 Depth=1
	cmpl	$2, 12(%rdi)
	jge	.LBB18_7
# BB#2:                                 #   in Loop: Header=BB18_5 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB18_5
# BB#3:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB18_7:                               # %.thread
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB18_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end18:
	.size	_ZNK9NWildcard11CCensorNode16NeedCheckSubDirsEv, .Lfunc_end18-_ZNK9NWildcard11CCensorNode16NeedCheckSubDirsEv
	.cfi_endproc

	.globl	_ZNK9NWildcard11CCensorNode20AreThereIncludeItemsEv
	.p2align	4, 0x90
	.type	_ZNK9NWildcard11CCensorNode20AreThereIncludeItemsEv,@function
_ZNK9NWildcard11CCensorNode20AreThereIncludeItemsEv: # @_ZNK9NWildcard11CCensorNode20AreThereIncludeItemsEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 48
.Lcfi115:
	.cfi_offset %rbx, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movb	$1, %r14b
	cmpl	$0, 68(%rdi)
	jg	.LBB19_1
# BB#2:                                 # %.preheader
	movslq	36(%rdi), %rbx
	testq	%rbx, %rbx
	jle	.LBB19_7
# BB#3:                                 # %.lr.ph
	movq	40(%rdi), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_4:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	callq	_ZNK9NWildcard11CCensorNode20AreThereIncludeItemsEv
	testb	%al, %al
	jne	.LBB19_1
# BB#5:                                 #   in Loop: Header=BB19_4 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB19_4
.LBB19_7:
	xorl	%r14d, %r14d
.LBB19_1:                               # %.loopexit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	_ZNK9NWildcard11CCensorNode20AreThereIncludeItemsEv, .Lfunc_end19-_ZNK9NWildcard11CCensorNode20AreThereIncludeItemsEv
	.cfi_endproc

	.globl	_ZNK9NWildcard11CCensorNode16CheckPathCurrentEbRK13CObjectVectorI11CStringBaseIwEEb
	.p2align	4, 0x90
	.type	_ZNK9NWildcard11CCensorNode16CheckPathCurrentEbRK13CObjectVectorI11CStringBaseIwEEb,@function
_ZNK9NWildcard11CCensorNode16CheckPathCurrentEbRK13CObjectVectorI11CStringBaseIwEEb: # @_ZNK9NWildcard11CCensorNode16CheckPathCurrentEbRK13CObjectVectorI11CStringBaseIwEEb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 48
.Lcfi124:
	.cfi_offset %rbx, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	leaq	56(%rbx), %rax
	addq	$88, %rbx
	testb	%sil, %sil
	cmovneq	%rax, %rbx
	cmpl	$0, 12(%rbx)
	jle	.LBB20_5
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	movzbl	%cl, %r15d
	.p2align	4, 0x90
.LBB20_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb
	testb	%al, %al
	jne	.LBB20_6
# BB#3:                                 #   in Loop: Header=BB20_2 Depth=1
	incq	%rbp
	movslq	12(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB20_2
.LBB20_5:
	xorl	%eax, %eax
	jmp	.LBB20_7
.LBB20_6:
	movb	$1, %al
.LBB20_7:                               # %._crit_edge
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZNK9NWildcard11CCensorNode16CheckPathCurrentEbRK13CObjectVectorI11CStringBaseIwEEb, .Lfunc_end20-_ZNK9NWildcard11CCensorNode16CheckPathCurrentEbRK13CObjectVectorI11CStringBaseIwEEb
	.cfi_endproc

	.globl	_ZNK9NWildcard11CCensorNode9CheckPathER13CObjectVectorI11CStringBaseIwEEbRb
	.p2align	4, 0x90
	.type	_ZNK9NWildcard11CCensorNode9CheckPathER13CObjectVectorI11CStringBaseIwEEbRb,@function
_ZNK9NWildcard11CCensorNode9CheckPathER13CObjectVectorI11CStringBaseIwEEbRb: # @_ZNK9NWildcard11CCensorNode9CheckPathER13CObjectVectorI11CStringBaseIwEEbRb
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi131:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi132:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi134:
	.cfi_def_cfa_offset 112
.Lcfi135:
	.cfi_offset %rbx, -56
.Lcfi136:
	.cfi_offset %r12, -48
.Lcfi137:
	.cfi_offset %r13, -40
.Lcfi138:
	.cfi_offset %r14, -32
.Lcfi139:
	.cfi_offset %r15, -24
.Lcfi140:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r13d
	movq	%rsi, %r12
	movq	%rdi, %r15
	cmpl	$0, 100(%r15)
	jle	.LBB21_4
# BB#1:                                 # %.lr.ph.i
	xorl	%ebx, %ebx
	movzbl	%r13b, %ebp
	.p2align	4, 0x90
.LBB21_2:                               # =>This Inner Loop Header: Depth=1
	movq	104(%r15), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	%r12, %rsi
	movl	%ebp, %edx
	callq	_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb
	testb	%al, %al
	jne	.LBB21_9
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=1
	incq	%rbx
	movslq	100(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB21_2
.LBB21_4:                               # %.loopexit
	movb	$1, (%r14)
	cmpl	$0, 68(%r15)
	jle	.LBB21_10
# BB#5:                                 # %.lr.ph.i21
	xorl	%ebx, %ebx
	movzbl	%r13b, %ebp
	.p2align	4, 0x90
.LBB21_6:                               # =>This Inner Loop Header: Depth=1
	movq	72(%r15), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	%r12, %rsi
	movl	%ebp, %edx
	callq	_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb
	testb	%al, %al
	jne	.LBB21_11
# BB#7:                                 #   in Loop: Header=BB21_6 Depth=1
	incq	%rbx
	movslq	68(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB21_6
.LBB21_10:
	xorl	%eax, %eax
	cmpl	$1, 12(%r12)
	jne	.LBB21_12
	jmp	.LBB21_30
.LBB21_9:                               # %_ZNK9NWildcard11CCensorNode16CheckPathCurrentEbRK13CObjectVectorI11CStringBaseIwEEb.exit
	movb	$0, (%r14)
	movb	$1, %al
	jmp	.LBB21_31
.LBB21_11:
	movb	$1, %al
	cmpl	$1, 12(%r12)
	je	.LBB21_30
.LBB21_12:
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movl	%r13d, 40(%rsp)         # 4-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
	cmpl	$0, 36(%r15)
	jle	.LBB21_29
# BB#13:                                # %.lr.ph.i25
	movq	16(%r12), %rax
	movq	(%rax), %rbp
	xorl	%r13d, %r13d
	movabsq	$4294967296, %rbx       # imm = 0x100000000
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB21_14:                              # =>This Inner Loop Header: Depth=1
	movq	40(%r15), %rax
	movq	(%rax,%r14,8), %rax
	cmpb	$0, g_CaseSensitive(%rip)
	movq	8(%rax), %rdi
	movq	(%rbp), %rsi
	je	.LBB21_16
# BB#15:                                #   in Loop: Header=BB21_14 Depth=1
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	jne	.LBB21_17
	jmp	.LBB21_18
	.p2align	4, 0x90
.LBB21_16:                              #   in Loop: Header=BB21_14 Depth=1
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB21_18
.LBB21_17:                              #   in Loop: Header=BB21_14 Depth=1
	incq	%r14
	movslq	36(%r15), %rax
	addq	%rbx, %r13
	cmpq	%rax, %r14
	jl	.LBB21_14
	jmp	.LBB21_29
.LBB21_18:                              # %_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE.exit
	testl	%r14d, %r14d
	js	.LBB21_29
# BB#19:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rsp)
	movq	$8, 32(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 8(%rsp)
.Ltmp105:
	leaq	8(%rsp), %rdi
	movq	%r12, %rsi
	callq	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
.Ltmp106:
# BB#20:                                # %_ZN13CObjectVectorI11CStringBaseIwEEC2ERKS2_.exit
	movl	20(%rsp), %eax
	cmpl	$2, %eax
	movl	$1, %r12d
	cmovll	%eax, %r12d
	testl	%eax, %eax
	jle	.LBB21_25
# BB#21:                                # %.lr.ph.i28
	movq	24(%rsp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB21_25
# BB#22:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_24
# BB#23:
	callq	_ZdaPv
.LBB21_24:                              # %_ZN11CStringBaseIwED2Ev.exit.i
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB21_25:                              # %._crit_edge.i
.Ltmp111:
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	movl	%r12d, %edx
	callq	_ZN17CBaseRecordVector6DeleteEii
.Ltmp112:
# BB#26:                                # %_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii.exit
	movq	40(%r15), %rax
	sarq	$29, %r13
	movq	(%rax,%r13), %rdi
.Ltmp113:
	movzbl	40(%rsp), %edx          # 1-byte Folded Reload
	leaq	8(%rsp), %rsi
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	_ZNK9NWildcard11CCensorNode9CheckPathER13CObjectVectorI11CStringBaseIwEEbRb
	movl	%eax, %ebx
.Ltmp114:
# BB#27:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 8(%rsp)
.Ltmp125:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp126:
# BB#28:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit31
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movb	$1, %al
	testb	%bl, %bl
	jne	.LBB21_31
.LBB21_29:                              # %_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE.exit.thread
	movl	44(%rsp), %eax          # 4-byte Reload
.LBB21_30:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
.LBB21_31:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_32:
.Ltmp127:
	movq	%rax, %rbx
.Ltmp128:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp129:
	jmp	.LBB21_38
.LBB21_33:
.Ltmp130:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB21_34:
.Ltmp107:
	movq	%rax, %rbx
.Ltmp108:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp109:
	jmp	.LBB21_38
.LBB21_35:
.Ltmp110:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB21_36:
.Ltmp115:
	movq	%rax, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 8(%rsp)
.Ltmp116:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp117:
# BB#37:
.Ltmp122:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp123:
.LBB21_38:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB21_39:
.Ltmp124:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB21_40:
.Ltmp118:
	movq	%rax, %rbx
.Ltmp119:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp120:
# BB#41:                                # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB21_42:
.Ltmp121:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZNK9NWildcard11CCensorNode9CheckPathER13CObjectVectorI11CStringBaseIwEEbRb, .Lfunc_end21-_ZNK9NWildcard11CCensorNode9CheckPathER13CObjectVectorI11CStringBaseIwEEbRb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp105-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp105
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp107-.Lfunc_begin6  #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp114-.Ltmp111       #   Call between .Ltmp111 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin6  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp125-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin6  #     jumps to .Ltmp127
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp128-.Ltmp126       #   Call between .Ltmp126 and .Ltmp128
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin6  #     jumps to .Ltmp130
	.byte	1                       #   On action: 1
	.long	.Ltmp108-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin6  #     jumps to .Ltmp110
	.byte	1                       #   On action: 1
	.long	.Ltmp116-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin6  #     jumps to .Ltmp118
	.byte	1                       #   On action: 1
	.long	.Ltmp122-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin6  #     jumps to .Ltmp124
	.byte	1                       #   On action: 1
	.long	.Ltmp123-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp119-.Ltmp123       #   Call between .Ltmp123 and .Ltmp119
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp119-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin6  #     jumps to .Ltmp121
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi143:
	.cfi_def_cfa_offset 32
.Lcfi144:
	.cfi_offset %rbx, -24
.Lcfi145:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp131:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp132:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB22_2:
.Ltmp133:
	movq	%rax, %r14
.Ltmp134:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp135:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_4:
.Ltmp136:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end22-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp131-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin7  #     jumps to .Ltmp133
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp134-.Ltmp132       #   Call between .Ltmp132 and .Ltmp134
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp135-.Ltmp134       #   Call between .Ltmp134 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin7  #     jumps to .Ltmp136
	.byte	1                       #   On action: 1
	.long	.Ltmp135-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end22-.Ltmp135   #   Call between .Ltmp135 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEbRb
	.p2align	4, 0x90
	.type	_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEbRb,@function
_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEbRb: # @_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEbRb
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi149:
	.cfi_def_cfa_offset 64
.Lcfi150:
	.cfi_offset %rbx, -32
.Lcfi151:
	.cfi_offset %r14, -24
.Lcfi152:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebp
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rsp)
	movq	$8, 24(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rsp)
.Ltmp137:
	movq	%rsp, %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E
.Ltmp138:
# BB#1:
.Ltmp139:
	movzbl	%bpl, %edx
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	movq	%r14, %rcx
	callq	_ZNK9NWildcard11CCensorNode9CheckPathER13CObjectVectorI11CStringBaseIwEEbRb
	movl	%eax, %ebx
.Ltmp140:
# BB#2:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rsp)
.Ltmp151:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp152:
# BB#3:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movl	%ebx, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB23_4:
.Ltmp153:
	movq	%rax, %rbx
.Ltmp154:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp155:
	jmp	.LBB23_5
.LBB23_6:
.Ltmp156:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB23_7:
.Ltmp141:
	movq	%rax, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rsp)
.Ltmp142:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp143:
# BB#8:
.Ltmp148:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp149:
.LBB23_5:                               # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB23_11:
.Ltmp150:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB23_9:
.Ltmp144:
	movq	%rax, %rbx
.Ltmp145:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp146:
# BB#12:                                # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB23_10:
.Ltmp147:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEbRb, .Lfunc_end23-_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEbRb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp137-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp140-.Ltmp137       #   Call between .Ltmp137 and .Ltmp140
	.long	.Ltmp141-.Lfunc_begin8  #     jumps to .Ltmp141
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin8  #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp152-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp154-.Ltmp152       #   Call between .Ltmp152 and .Ltmp154
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin8  #     jumps to .Ltmp156
	.byte	1                       #   On action: 1
	.long	.Ltmp142-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp143-.Ltmp142       #   Call between .Ltmp142 and .Ltmp143
	.long	.Ltmp144-.Lfunc_begin8  #     jumps to .Ltmp144
	.byte	1                       #   On action: 1
	.long	.Ltmp148-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin8  #     jumps to .Ltmp150
	.byte	1                       #   On action: 1
	.long	.Ltmp149-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp145-.Ltmp149       #   Call between .Ltmp149 and .Ltmp145
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp146-.Ltmp145       #   Call between .Ltmp145 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin8  #     jumps to .Ltmp147
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEb
	.p2align	4, 0x90
	.type	_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEb,@function
_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEb: # @_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEb
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi153:
	.cfi_def_cfa_offset 16
	movzbl	%dl, %edx
	leaq	7(%rsp), %rcx
	callq	_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEbRb
	cmpb	$0, 7(%rsp)
	setne	%cl
	andb	%cl, %al
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEb, .Lfunc_end24-_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEb
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI25_0:
	.zero	16
	.text
	.globl	_ZNK9NWildcard11CCensorNode15CheckPathToRootEbR13CObjectVectorI11CStringBaseIwEEb
	.p2align	4, 0x90
	.type	_ZNK9NWildcard11CCensorNode15CheckPathToRootEbR13CObjectVectorI11CStringBaseIwEEb,@function
_ZNK9NWildcard11CCensorNode15CheckPathToRootEbR13CObjectVectorI11CStringBaseIwEEb: # @_ZNK9NWildcard11CCensorNode15CheckPathToRootEbR13CObjectVectorI11CStringBaseIwEEb
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi157:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi158:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi160:
	.cfi_def_cfa_offset 64
.Lcfi161:
	.cfi_offset %rbx, -56
.Lcfi162:
	.cfi_offset %r12, -48
.Lcfi163:
	.cfi_offset %r13, -40
.Lcfi164:
	.cfi_offset %r14, -32
.Lcfi165:
	.cfi_offset %r15, -24
.Lcfi166:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movzbl	%cl, %r13d
	testb	%sil, %sil
	jne	.LBB25_2
	jmp	.LBB25_14
	.p2align	4, 0x90
.LBB25_1:                               # %_ZN13CObjectVectorI11CStringBaseIwEE6InsertEiRKS1_.exit.us
                                        #   in Loop: Header=BB25_2 Depth=1
	movl	%ebp, 8(%r15)
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
	movq	16(%r14), %rax
	movq	%r15, (%rax)
	movq	(%rbx), %rbx
.LBB25_2:                               # %tailrecurse.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_4 Depth 2
                                        #     Child Loop BB25_12 Depth 2
	cmpl	$0, 68(%rbx)
	jle	.LBB25_6
# BB#3:                                 # %.lr.ph.i.us
                                        #   in Loop: Header=BB25_2 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB25_4:                               #   Parent Loop BB25_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	72(%rbx), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	%r14, %rsi
	movl	%r13d, %edx
	callq	_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb
	testb	%al, %al
	jne	.LBB25_25
# BB#5:                                 #   in Loop: Header=BB25_4 Depth=2
	incq	%rbp
	movslq	68(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB25_4
.LBB25_6:                               # %.loopexit.us
                                        #   in Loop: Header=BB25_2 Depth=1
	cmpq	$0, (%rbx)
	je	.LBB25_28
# BB#7:                                 #   in Loop: Header=BB25_2 Depth=1
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movslq	16(%rbx), %rbp
	leaq	1(%rbp), %r12
	testl	%r12d, %r12d
	je	.LBB25_10
# BB#8:                                 # %._crit_edge16.i.i.i.us
                                        #   in Loop: Header=BB25_2 Depth=1
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp160:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp161:
# BB#9:                                 # %.noexc.i.us
                                        #   in Loop: Header=BB25_2 Depth=1
	movq	%rax, (%r15)
	movl	$0, (%rax)
	movl	%r12d, 12(%r15)
	jmp	.LBB25_11
	.p2align	4, 0x90
.LBB25_10:                              #   in Loop: Header=BB25_2 Depth=1
	xorl	%eax, %eax
.LBB25_11:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.us
                                        #   in Loop: Header=BB25_2 Depth=1
	movq	8(%rbx), %rcx
	.p2align	4, 0x90
.LBB25_12:                              #   Parent Loop BB25_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB25_12
	jmp	.LBB25_1
	.p2align	4, 0x90
.LBB25_13:                              # %_ZN13CObjectVectorI11CStringBaseIwEE6InsertEiRKS1_.exit
                                        #   in Loop: Header=BB25_14 Depth=1
	movl	%ebp, 8(%r15)
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
	movq	16(%r14), %rax
	movq	%r15, (%rax)
	movq	(%rbx), %rbx
.LBB25_14:                              # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_16 Depth 2
                                        #     Child Loop BB25_24 Depth 2
	cmpl	$0, 100(%rbx)
	jle	.LBB25_18
# BB#15:                                # %.lr.ph.i
                                        #   in Loop: Header=BB25_14 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB25_16:                              #   Parent Loop BB25_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	104(%rbx), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	%r14, %rsi
	movl	%r13d, %edx
	callq	_ZNK9NWildcard5CItem9CheckPathERK13CObjectVectorI11CStringBaseIwEEb
	testb	%al, %al
	jne	.LBB25_25
# BB#17:                                #   in Loop: Header=BB25_16 Depth=2
	incq	%rbp
	movslq	100(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB25_16
.LBB25_18:                              # %.loopexit
                                        #   in Loop: Header=BB25_14 Depth=1
	cmpq	$0, (%rbx)
	je	.LBB25_28
# BB#19:                                #   in Loop: Header=BB25_14 Depth=1
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movslq	16(%rbx), %rbp
	leaq	1(%rbp), %r12
	testl	%r12d, %r12d
	je	.LBB25_22
# BB#20:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB25_14 Depth=1
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp157:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rcx
.Ltmp158:
# BB#21:                                # %.noexc.i
                                        #   in Loop: Header=BB25_14 Depth=1
	movq	%rcx, (%r15)
	movl	$0, (%rcx)
	movl	%r12d, 12(%r15)
	jmp	.LBB25_23
	.p2align	4, 0x90
.LBB25_22:                              #   in Loop: Header=BB25_14 Depth=1
	xorl	%ecx, %ecx
.LBB25_23:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB25_14 Depth=1
	movq	8(%rbx), %rsi
	.p2align	4, 0x90
.LBB25_24:                              #   Parent Loop BB25_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB25_24
	jmp	.LBB25_13
.LBB25_25:
	movb	$1, %al
.LBB25_26:                              # %_ZNK9NWildcard11CCensorNode16CheckPathCurrentEbRK13CObjectVectorI11CStringBaseIwEEb.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_28:
	xorl	%eax, %eax
	jmp	.LBB25_26
.LBB25_29:                              # %.us-lcssa
.Ltmp159:
	jmp	.LBB25_31
.LBB25_30:                              # %.us-lcssa.us
.Ltmp162:
.LBB25_31:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end25:
	.size	_ZNK9NWildcard11CCensorNode15CheckPathToRootEbR13CObjectVectorI11CStringBaseIwEEb, .Lfunc_end25-_ZNK9NWildcard11CCensorNode15CheckPathToRootEbR13CObjectVectorI11CStringBaseIwEEb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin9-.Lfunc_begin9 # >> Call Site 1 <<
	.long	.Ltmp160-.Lfunc_begin9  #   Call between .Lfunc_begin9 and .Ltmp160
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin9  #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp157-.Ltmp161       #   Call between .Ltmp161 and .Ltmp157
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin9  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Lfunc_end25-.Ltmp158   #   Call between .Ltmp158 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NWildcard11CCensorNode8AddItem2EbRK11CStringBaseIwEb
	.p2align	4, 0x90
	.type	_ZN9NWildcard11CCensorNode8AddItem2EbRK11CStringBaseIwEb,@function
_ZN9NWildcard11CCensorNode8AddItem2EbRK11CStringBaseIwEb: # @_ZN9NWildcard11CCensorNode8AddItem2EbRK11CStringBaseIwEb
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbp
.Lcfi167:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi168:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi169:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi170:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi171:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi173:
	.cfi_def_cfa_offset 80
.Lcfi174:
	.cfi_offset %rbx, -56
.Lcfi175:
	.cfi_offset %r12, -48
.Lcfi176:
	.cfi_offset %r13, -40
.Lcfi177:
	.cfi_offset %r14, -32
.Lcfi178:
	.cfi_offset %r15, -24
.Lcfi179:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %rbp
	movl	%esi, %r12d
	movq	%rdi, %r10
	movslq	8(%rbp), %r14
	testq	%r14, %r14
	je	.LBB26_11
# BB#1:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	%r14d, %eax
	incl	%eax
	je	.LBB26_2
# BB#3:                                 # %._crit_edge16.i.i
	movslq	%eax, %r13
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	movq	%r10, %rbx
	callq	_Znam
	movq	%rbx, %r10
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	%r13d, 12(%rsp)
	jmp	.LBB26_4
.LBB26_2:
	xorl	%ebx, %ebx
.LBB26_4:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rbp), %rax
	movq	%rax, %rcx
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB26_5:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %esi
	addq	$4, %rcx
	movl	%esi, (%rdx)
	addq	$4, %rdx
	testl	%esi, %esi
	jne	.LBB26_5
# BB#6:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r14d, 8(%rsp)
	movb	$1, %dil
	cmpl	$47, -4(%rax,%r14,4)
	jne	.LBB26_8
# BB#7:
	movl	(%rbx,%r14,4), %eax
	movl	%eax, -4(%rbx,%r14,4)
	leal	-1(%r14), %eax
	movl	%eax, 8(%rsp)
	xorl	%edi, %edi
.LBB26_8:                               # %_ZN11CStringBaseIwE6DeleteEii.exit
.Ltmp163:
	movzbl	%r12b, %esi
	movzbl	%r15b, %ecx
	movzbl	%dil, %r8d
	movq	%rsp, %rdx
	movl	$1, %r9d
	movq	%r10, %rdi
	callq	_ZN9NWildcard11CCensorNode7AddItemEbRK11CStringBaseIwEbbb
.Ltmp164:
# BB#9:
	testq	%rbx, %rbx
	je	.LBB26_11
# BB#10:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB26_11:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB26_12:
.Ltmp165:
	movq	%rax, %rbp
	testq	%rbx, %rbx
	je	.LBB26_14
# BB#13:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB26_14:                              # %_ZN11CStringBaseIwED2Ev.exit11
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZN9NWildcard11CCensorNode8AddItem2EbRK11CStringBaseIwEb, .Lfunc_end26-_ZN9NWildcard11CCensorNode8AddItem2EbRK11CStringBaseIwEb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin10-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp163-.Lfunc_begin10 #   Call between .Lfunc_begin10 and .Ltmp163
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin10 #     jumps to .Ltmp165
	.byte	0                       #   On action: cleanup
	.long	.Ltmp164-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Lfunc_end26-.Ltmp164   #   Call between .Ltmp164 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI27_0:
	.zero	16
	.text
	.globl	_ZN9NWildcard11CCensorNode13ExtendExcludeERKS0_
	.p2align	4, 0x90
	.type	_ZN9NWildcard11CCensorNode13ExtendExcludeERKS0_,@function
_ZN9NWildcard11CCensorNode13ExtendExcludeERKS0_: # @_ZN9NWildcard11CCensorNode13ExtendExcludeERKS0_
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%rbp
.Lcfi180:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi181:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi182:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi183:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi184:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi185:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi186:
	.cfi_def_cfa_offset 192
.Lcfi187:
	.cfi_offset %rbx, -56
.Lcfi188:
	.cfi_offset %r12, -48
.Lcfi189:
	.cfi_offset %r13, -40
.Lcfi190:
	.cfi_offset %r14, -32
.Lcfi191:
	.cfi_offset %r15, -24
.Lcfi192:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	88(%rbx), %r15
	movl	100(%r14), %r12d
	movl	100(%rbx), %esi
	addl	%r12d, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r12d, %r12d
	jle	.LBB27_3
# BB#1:                                 # %.lr.ph.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB27_2:                               # =>This Inner Loop Header: Depth=1
	movq	104(%r14), %rax
	movq	(%rax,%rbp,8), %rsi
	movq	%r15, %rdi
	callq	_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB27_2
.LBB27_3:                               # %_ZN13CObjectVectorIN9NWildcard5CItemEEpLERKS2_.exit.preheader
	cmpl	$0, 36(%r14)
	jle	.LBB27_23
# BB#4:                                 # %.lr.ph
	leaq	24(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB27_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_7 Depth 2
                                        #     Child Loop BB27_17 Depth 2
	movq	40(%r14), %rax
	movq	(%rax,%r12,8), %r13
	cmpl	$0, 36(%rbx)
	jle	.LBB27_13
# BB#6:                                 # %.lr.ph.i22
                                        #   in Loop: Header=BB27_5 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB27_7:                               #   Parent Loop BB27_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	cmpb	$0, g_CaseSensitive(%rip)
	movq	8(%rax), %rdi
	movq	8(%r13), %rsi
	je	.LBB27_9
# BB#8:                                 #   in Loop: Header=BB27_7 Depth=2
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	jne	.LBB27_11
	jmp	.LBB27_12
	.p2align	4, 0x90
.LBB27_9:                               #   in Loop: Header=BB27_7 Depth=2
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB27_12
.LBB27_11:                              #   in Loop: Header=BB27_7 Depth=2
	incq	%r15
	movslq	36(%rbx), %rax
	cmpq	%rax, %r15
	jl	.LBB27_7
	jmp	.LBB27_13
	.p2align	4, 0x90
.LBB27_12:                              # %_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE.exit
                                        #   in Loop: Header=BB27_5 Depth=1
	testl	%r15d, %r15d
	jns	.LBB27_22
	.p2align	4, 0x90
.LBB27_13:                              # %_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE.exit.thread
                                        #   in Loop: Header=BB27_5 Depth=1
	movq	%rbx, 16(%rsp)
	leaq	24(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movslq	16(%r13), %r15
	leaq	1(%r15), %rbp
	testl	%ebp, %ebp
	je	.LBB27_14
# BB#15:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB27_5 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, 24(%rsp)
	movl	$0, (%rax)
	movl	%ebp, 36(%rsp)
	jmp	.LBB27_16
	.p2align	4, 0x90
.LBB27_14:                              #   in Loop: Header=BB27_5 Depth=1
	xorl	%eax, %eax
.LBB27_16:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB27_5 Depth=1
	movq	8(%r13), %rcx
	.p2align	4, 0x90
.LBB27_17:                              #   Parent Loop BB27_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB27_17
# BB#18:                                # %_ZN9NWildcard11CCensorNodeC2ERK11CStringBaseIwEPS0_.exit
                                        #   in Loop: Header=BB27_5 Depth=1
	movl	%r15d, 32(%rsp)
	leaq	24(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rax)
	movq	$8, 64(%rsp)
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, 40(%rsp)
	movups	%xmm0, 56(%rax)
	movq	$8, 96(%rsp)
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 72(%rsp)
	movups	%xmm0, 88(%rax)
	movq	$8, 128(%rsp)
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 104(%rsp)
.Ltmp166:
	movl	$120, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp167:
# BB#19:                                # %.noexc
                                        #   in Loop: Header=BB27_5 Depth=1
.Ltmp168:
	movq	%rbp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN9NWildcard11CCensorNodeC2ERKS0_
.Ltmp169:
# BB#20:                                #   in Loop: Header=BB27_5 Depth=1
.Ltmp171:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp172:
# BB#21:                                #   in Loop: Header=BB27_5 Depth=1
	movq	40(%rbx), %rax
	movslq	36(%rbx), %r15
	movq	%rbp, (%rax,%r15,8)
	leal	1(%r15), %eax
	movl	%eax, 36(%rbx)
	leaq	16(%rsp), %rdi
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.LBB27_22:                              # %_ZN13CObjectVectorIN9NWildcard5CItemEEpLERKS2_.exit
                                        #   in Loop: Header=BB27_5 Depth=1
	movq	40(%rbx), %rax
	movslq	%r15d, %rcx
	movq	(%rax,%rcx,8), %rdi
	movq	%r13, %rsi
	callq	_ZN9NWildcard11CCensorNode13ExtendExcludeERKS0_
	incq	%r12
	movslq	36(%r14), %rax
	cmpq	%rax, %r12
	jl	.LBB27_5
.LBB27_23:                              # %_ZN13CObjectVectorIN9NWildcard5CItemEEpLERKS2_.exit._crit_edge
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB27_28:
.Ltmp170:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB27_25
.LBB27_24:
.Ltmp173:
	movq	%rax, %rbx
.LBB27_25:                              # %.body
.Ltmp174:
	leaq	16(%rsp), %rdi
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.Ltmp175:
# BB#26:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB27_27:
.Ltmp176:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZN9NWildcard11CCensorNode13ExtendExcludeERKS0_, .Lfunc_end27-_ZN9NWildcard11CCensorNode13ExtendExcludeERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp166-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp166
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp173-.Lfunc_begin11 #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin11 #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin11 #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Ltmp174-.Ltmp172       #   Call between .Ltmp172 and .Ltmp174
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp174-.Lfunc_begin11 # >> Call Site 6 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin11 #     jumps to .Ltmp176
	.byte	1                       #   On action: 1
	.long	.Ltmp175-.Lfunc_begin11 # >> Call Site 7 <<
	.long	.Lfunc_end27-.Ltmp175   #   Call between .Ltmp175 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK9NWildcard7CCensor10FindPrefixERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZNK9NWildcard7CCensor10FindPrefixERK11CStringBaseIwE,@function
_ZNK9NWildcard7CCensor10FindPrefixERK11CStringBaseIwE: # @_ZNK9NWildcard7CCensor10FindPrefixERK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi195:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi196:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi197:
	.cfi_def_cfa_offset 48
.Lcfi198:
	.cfi_offset %rbx, -40
.Lcfi199:
	.cfi_offset %r14, -32
.Lcfi200:
	.cfi_offset %r15, -24
.Lcfi201:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	$-1, %r14d
	cmpl	$0, 12(%rbp)
	jle	.LBB28_8
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB28_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	cmpb	$0, g_CaseSensitive(%rip)
	movq	(%rax), %rdi
	movq	(%r15), %rsi
	je	.LBB28_4
# BB#3:                                 #   in Loop: Header=BB28_2 Depth=1
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	jne	.LBB28_6
	jmp	.LBB28_7
	.p2align	4, 0x90
.LBB28_4:                               #   in Loop: Header=BB28_2 Depth=1
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB28_7
.LBB28_6:                               #   in Loop: Header=BB28_2 Depth=1
	incq	%rbx
	movslq	12(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB28_2
	jmp	.LBB28_8
.LBB28_7:                               # %_Z16CompareFileNamesRK11CStringBaseIwES2_.exit.._crit_edge.loopexit_crit_edge
	movl	%ebx, %r14d
.LBB28_8:                               # %._crit_edge
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	_ZNK9NWildcard7CCensor10FindPrefixERK11CStringBaseIwE, .Lfunc_end28-_ZNK9NWildcard7CCensor10FindPrefixERK11CStringBaseIwE
	.cfi_endproc

	.globl	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
	.p2align	4, 0x90
	.type	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb,@function
_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb: # @_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%rbp
.Lcfi202:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi203:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi204:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi205:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi206:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi207:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi208:
	.cfi_def_cfa_offset 368
.Lcfi209:
	.cfi_offset %rbx, -56
.Lcfi210:
	.cfi_offset %r12, -48
.Lcfi211:
	.cfi_offset %r13, -40
.Lcfi212:
	.cfi_offset %r14, -32
.Lcfi213:
	.cfi_offset %r15, -24
.Lcfi214:
	.cfi_offset %rbp, -16
	movl	%ecx, 116(%rsp)         # 4-byte Spill
	movl	%esi, %ebx
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rsp)
	movq	$8, 56(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 32(%rsp)
	cmpl	$0, 8(%rdx)
	je	.LBB29_134
# BB#1:
.Ltmp177:
	leaq	32(%rsp), %rsi
	movq	%rdx, %rdi
	callq	_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E
.Ltmp178:
# BB#2:
	movslq	44(%rsp), %rax
	movq	48(%rsp), %rcx
	movq	-8(%rcx,%rax,8), %rax
	cmpl	$0, 8(%rax)
	movl	%ebx, 112(%rsp)         # 4-byte Spill
	je	.LBB29_4
# BB#3:                                 # %._crit_edge189
	movb	$1, %al
	movl	%eax, 68(%rsp)          # 4-byte Spill
	jmp	.LBB29_6
.LBB29_4:
.Ltmp179:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp180:
# BB#5:                                 # %._crit_edge187
	movq	48(%rsp), %rcx
	movl	$0, 68(%rsp)            # 4-byte Folded Spill
.LBB29_6:
	movq	(%rcx), %rax
	movl	8(%rax), %edx
	testl	%edx, %edx
	je	.LBB29_17
# BB#7:
	cmpl	$2, %edx
	jne	.LBB29_9
# BB#8:
	movq	(%rax), %rax
	cmpl	$58, 4(%rax)
	je	.LBB29_17
.LBB29_9:                               # %.preheader
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	cmpl	$0, 44(%rsp)
	jle	.LBB29_18
# BB#10:                                # %.lr.ph179.preheader
	movl	$1, %ebx
	jmp	.LBB29_12
	.p2align	4, 0x90
.LBB29_11:                              # %..lr.ph179_crit_edge
                                        #   in Loop: Header=BB29_12 Depth=1
	movq	48(%rsp), %rcx
	incq	%rbx
.LBB29_12:                              # %.lr.ph179
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rcx,%rbx,8), %rbp
	movq	(%rbp), %rdi
.Ltmp182:
	movl	$.L.str.4, %esi
	callq	_Z15MyStringComparePKwS0_
.Ltmp183:
# BB#13:                                #   in Loop: Header=BB29_12 Depth=1
	testl	%eax, %eax
	je	.LBB29_17
# BB#14:                                #   in Loop: Header=BB29_12 Depth=1
	movq	(%rbp), %rdi
.Ltmp184:
	movl	$.L.str.5, %esi
	callq	_Z15MyStringComparePKwS0_
.Ltmp185:
# BB#15:                                #   in Loop: Header=BB29_12 Depth=1
	testl	%eax, %eax
	je	.LBB29_17
# BB#16:                                #   in Loop: Header=BB29_12 Depth=1
	movslq	44(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB29_11
	jmp	.LBB29_18
.LBB29_17:                              # %.thread148
	movl	44(%rsp), %eax
	leal	-1(%rax), %ecx
	cmpl	$1, %eax
	movl	$1, %eax
	cmovgl	%ecx, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
.LBB29_18:                              # %._crit_edge.thread
.Ltmp187:
	movq	%r14, 160(%rsp)         # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
.Ltmp188:
# BB#19:                                # %_ZN11CStringBaseIwEC2Ev.exit
	movl	$0, (%rax)
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jle	.LBB29_29
# BB#20:                                # %.lr.ph
	movl	$4, %r13d
	xorl	%r9d, %r9d
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%r10d, %r10d
	jmp	.LBB29_21
.LBB29_29:
	xorl	%r10d, %r10d
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB29_100
.LBB29_30:                              # %vector.body202.preheader
                                        #   in Loop: Header=BB29_21 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB29_36
# BB#31:                                # %vector.body202.prol.preheader
                                        #   in Loop: Header=BB29_21 Depth=1
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB29_32:                              # %vector.body202.prol
                                        #   Parent Loop BB29_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rdi,4), %xmm0
	movups	16(%rbp,%rdi,4), %xmm1
	movups	%xmm0, (%r14,%rdi,4)
	movups	%xmm1, 16(%r14,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB29_32
	jmp	.LBB29_37
.LBB29_33:                              # %vector.body.preheader
                                        #   in Loop: Header=BB29_21 Depth=1
	leaq	-8(%rbp), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB29_41
# BB#34:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB29_21 Depth=1
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB29_35:                              # %vector.body.prol
                                        #   Parent Loop BB29_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rsi,4), %xmm0
	movups	16(%rdi,%rsi,4), %xmm1
	movups	%xmm0, (%rax,%rsi,4)
	movups	%xmm1, 16(%rax,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB29_35
	jmp	.LBB29_42
.LBB29_36:                              #   in Loop: Header=BB29_21 Depth=1
	xorl	%edi, %edi
.LBB29_37:                              # %vector.body202.prol.loopexit
                                        #   in Loop: Header=BB29_21 Depth=1
	cmpq	$24, %rdx
	jb	.LBB29_40
# BB#38:                                # %vector.body202.preheader.new
                                        #   in Loop: Header=BB29_21 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r14,%rdi,4), %rsi
	leaq	112(%rbp,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB29_39:                              # %vector.body202
                                        #   Parent Loop BB29_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB29_39
.LBB29_40:                              # %middle.block203
                                        #   in Loop: Header=BB29_21 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB29_60
	jmp	.LBB29_67
.LBB29_41:                              #   in Loop: Header=BB29_21 Depth=1
	xorl	%esi, %esi
.LBB29_42:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB29_21 Depth=1
	cmpq	$24, %rcx
	jb	.LBB29_45
# BB#43:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB29_21 Depth=1
	movq	%rbp, %rcx
	subq	%rsi, %rcx
	leaq	112(%rax,%rsi,4), %rdx
	leaq	112(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB29_44:                              # %vector.body
                                        #   Parent Loop BB29_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB29_44
.LBB29_45:                              # %middle.block
                                        #   in Loop: Header=BB29_21 Depth=1
	cmpq	%rbp, %r12
	jne	.LBB29_84
	jmp	.LBB29_91
	.p2align	4, 0x90
.LBB29_21:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_23 Depth 2
                                        #       Child Loop BB29_25 Depth 3
                                        #     Child Loop BB29_32 Depth 2
                                        #     Child Loop BB29_39 Depth 2
                                        #     Child Loop BB29_62 Depth 2
                                        #     Child Loop BB29_65 Depth 2
                                        #     Child Loop BB29_70 Depth 2
                                        #     Child Loop BB29_35 Depth 2
                                        #     Child Loop BB29_44 Depth 2
                                        #     Child Loop BB29_86 Depth 2
                                        #     Child Loop BB29_89 Depth 2
	movq	48(%rsp), %rax
	movq	(%rax), %r12
	movslq	8(%r12), %r8
	testq	%r8, %r8
	jle	.LBB29_47
# BB#22:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB29_21 Depth=1
	movq	(%r12), %rcx
	movq	_ZL16kWildCardCharSet(%rip), %rbp
	movl	(%rbp), %eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB29_23:                              #   Parent Loop BB29_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB29_25 Depth 3
	movl	(%rcx,%rsi,4), %ebx
	cmpl	%ebx, %eax
	movq	%rbp, %rdx
	je	.LBB29_27
# BB#24:                                # %.lr.ph.i.i.i.i.preheader
                                        #   in Loop: Header=BB29_23 Depth=2
	movl	%eax, %edi
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB29_25:                              # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB29_21 Depth=1
                                        #     Parent Loop BB29_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%edi, %edi
	je	.LBB29_28
# BB#26:                                #   in Loop: Header=BB29_25 Depth=3
	movl	4(%rdx), %edi
	addq	$4, %rdx
	cmpl	%ebx, %edi
	jne	.LBB29_25
.LBB29_27:                              # %_ZNK11CStringBaseIwE4FindEw.exit.i.i
                                        #   in Loop: Header=BB29_23 Depth=2
	subq	%rbp, %rdx
	shrq	$2, %rdx
	testl	%edx, %edx
	jns	.LBB29_46
.LBB29_28:                              # %_ZNK11CStringBaseIwE4FindEw.exit.thread.i.i
                                        #   in Loop: Header=BB29_23 Depth=2
	incq	%rsi
	cmpq	%r8, %rsi
	jl	.LBB29_23
	jmp	.LBB29_47
	.p2align	4, 0x90
.LBB29_46:                              #   in Loop: Header=BB29_21 Depth=1
	testl	%esi, %esi
	jns	.LBB29_100
.LBB29_47:                              # %.thread150
                                        #   in Loop: Header=BB29_21 Depth=1
	movl	%r13d, %ecx
	subl	%r10d, %ecx
	cmpl	%r8d, %ecx
	movq	%r9, 168(%rsp)          # 8-byte Spill
	jg	.LBB29_51
# BB#48:                                #   in Loop: Header=BB29_21 Depth=1
	decl	%ecx
	cmpl	$8, %r13d
	movl	$4, %edx
	movl	$16, %eax
	cmovgl	%eax, %edx
	cmpl	$65, %r13d
	jl	.LBB29_50
# BB#49:                                # %select.true.sink
                                        #   in Loop: Header=BB29_21 Depth=1
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB29_50:                              # %select.end
                                        #   in Loop: Header=BB29_21 Depth=1
	movl	%r8d, %eax
	subl	%ecx, %eax
	addl	%edx, %ecx
	cmpl	%r8d, %ecx
	cmovgel	%edx, %eax
	leal	1(%r13,%rax), %r15d
	cmpl	%r13d, %r15d
	jne	.LBB29_52
.LBB29_51:                              #   in Loop: Header=BB29_21 Depth=1
	movl	%r13d, %r15d
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB29_69
	.p2align	4, 0x90
.LBB29_52:                              #   in Loop: Header=BB29_21 Depth=1
	movl	%r10d, %ebx
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp190:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp191:
# BB#53:                                # %.noexc74
                                        #   in Loop: Header=BB29_21 Depth=1
	testl	%r13d, %r13d
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ebx, %r10d
	jle	.LBB29_68
# BB#54:                                # %.preheader.i.i
                                        #   in Loop: Header=BB29_21 Depth=1
	testl	%r10d, %r10d
	jle	.LBB29_66
# BB#55:                                # %.lr.ph.i.i71
                                        #   in Loop: Header=BB29_21 Depth=1
	movslq	%r10d, %rax
	cmpl	$7, %r10d
	jbe	.LBB29_59
# BB#56:                                # %min.iters.checked206
                                        #   in Loop: Header=BB29_21 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB29_59
# BB#57:                                # %vector.memcheck219
                                        #   in Loop: Header=BB29_21 Depth=1
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, %r14
	jae	.LBB29_30
# BB#58:                                # %vector.memcheck219
                                        #   in Loop: Header=BB29_21 Depth=1
	leaq	(%r14,%rax,4), %rdx
	cmpq	%rdx, 88(%rsp)          # 8-byte Folded Reload
	jae	.LBB29_30
.LBB29_59:                              #   in Loop: Header=BB29_21 Depth=1
	xorl	%ecx, %ecx
.LBB29_60:                              # %scalar.ph204.preheader
                                        #   in Loop: Header=BB29_21 Depth=1
	movl	%r10d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB29_63
# BB#61:                                # %scalar.ph204.prol.preheader
                                        #   in Loop: Header=BB29_21 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB29_62:                              # %scalar.ph204.prol
                                        #   Parent Loop BB29_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rcx,4), %edi
	movl	%edi, (%r14,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB29_62
.LBB29_63:                              # %scalar.ph204.prol.loopexit
                                        #   in Loop: Header=BB29_21 Depth=1
	cmpq	$7, %rdx
	jb	.LBB29_67
# BB#64:                                # %scalar.ph204.preheader.new
                                        #   in Loop: Header=BB29_21 Depth=1
	subq	%rcx, %rax
	leaq	28(%r14,%rcx,4), %rdx
	leaq	28(%rbp,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB29_65:                              # %scalar.ph204
                                        #   Parent Loop BB29_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB29_65
	jmp	.LBB29_67
.LBB29_66:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB29_21 Depth=1
	testq	%rbp, %rbp
	je	.LBB29_68
.LBB29_67:                              # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB29_21 Depth=1
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	_ZdaPv
	movl	%ebx, %r10d
.LBB29_68:                              # %._crit_edge16.i.i
                                        #   in Loop: Header=BB29_21 Depth=1
	movslq	%r10d, %rax
	movl	$0, (%r14,%rax,4)
	movq	%r14, 72(%rsp)          # 8-byte Spill
	movq	%r14, 96(%rsp)          # 8-byte Spill
	movq	%r14, 104(%rsp)         # 8-byte Spill
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
.LBB29_69:                              # %.noexc
                                        #   in Loop: Header=BB29_21 Depth=1
	movslq	%r10d, %rax
	leaq	(%rdi,%rax,4), %rcx
	movq	(%r12), %rdx
	.p2align	4, 0x90
.LBB29_70:                              #   Parent Loop BB29_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rcx)
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB29_70
# BB#71:                                #   in Loop: Header=BB29_21 Depth=1
	movslq	8(%r12), %r12
	addq	%rax, %r12
	movl	%r15d, %eax
	subl	%r12d, %eax
	cmpl	$1, %eax
	jg	.LBB29_75
# BB#72:                                #   in Loop: Header=BB29_21 Depth=1
	cmpl	$8, %r15d
	movl	$4, %ecx
	movl	$16, %edx
	cmovgl	%edx, %ecx
	cmpl	$65, %r15d
	jl	.LBB29_74
# BB#73:                                # %select.true.sink442
                                        #   in Loop: Header=BB29_21 Depth=1
	movl	%r15d, %ecx
	shrl	$31, %ecx
	addl	%r15d, %ecx
	sarl	%ecx
.LBB29_74:                              # %select.end441
                                        #   in Loop: Header=BB29_21 Depth=1
	leal	-1(%rcx,%rax), %edx
	movl	$2, %esi
	subl	%eax, %esi
	testl	%edx, %edx
	cmovgl	%ecx, %esi
	leal	1(%r15,%rsi), %r13d
	cmpl	%r15d, %r13d
	jne	.LBB29_76
.LBB29_75:                              #   in Loop: Header=BB29_21 Depth=1
	movl	%r15d, %r13d
	movq	%r14, 16(%rsp)          # 8-byte Spill
	jmp	.LBB29_93
	.p2align	4, 0x90
.LBB29_76:                              #   in Loop: Header=BB29_21 Depth=1
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp192:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp193:
# BB#77:                                # %.noexc88
                                        #   in Loop: Header=BB29_21 Depth=1
	testl	%r15d, %r15d
	movq	8(%rsp), %rdi           # 8-byte Reload
	jle	.LBB29_92
# BB#78:                                # %.preheader.i.i79
                                        #   in Loop: Header=BB29_21 Depth=1
	testl	%r12d, %r12d
	jle	.LBB29_90
# BB#79:                                # %.lr.ph.i.i80
                                        #   in Loop: Header=BB29_21 Depth=1
	cmpl	$7, %r12d
	jbe	.LBB29_83
# BB#80:                                # %min.iters.checked
                                        #   in Loop: Header=BB29_21 Depth=1
	movq	%r12, %rbp
	andq	$-8, %rbp
	je	.LBB29_83
# BB#81:                                # %vector.memcheck
                                        #   in Loop: Header=BB29_21 Depth=1
	leaq	(%rdi,%r12,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB29_33
# BB#82:                                # %vector.memcheck
                                        #   in Loop: Header=BB29_21 Depth=1
	leaq	(%rax,%r12,4), %rcx
	cmpq	%rcx, 80(%rsp)          # 8-byte Folded Reload
	jae	.LBB29_33
.LBB29_83:                              #   in Loop: Header=BB29_21 Depth=1
	xorl	%ebp, %ebp
.LBB29_84:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB29_21 Depth=1
	movl	%r12d, %edx
	subl	%ebp, %edx
	leaq	-1(%r12), %rcx
	subq	%rbp, %rcx
	andq	$7, %rdx
	je	.LBB29_87
# BB#85:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB29_21 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB29_86:                              # %scalar.ph.prol
                                        #   Parent Loop BB29_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rbp,4), %esi
	movl	%esi, (%rax,%rbp,4)
	incq	%rbp
	incq	%rdx
	jne	.LBB29_86
.LBB29_87:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB29_21 Depth=1
	cmpq	$7, %rcx
	jb	.LBB29_91
# BB#88:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB29_21 Depth=1
	movq	%r12, %rcx
	subq	%rbp, %rcx
	leaq	28(%rax,%rbp,4), %rdx
	leaq	28(%rdi,%rbp,4), %rdi
	.p2align	4, 0x90
.LBB29_89:                              # %scalar.ph
                                        #   Parent Loop BB29_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rdi), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rdi), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rdi), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rdi), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rdi), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rdi), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rdi), %esi
	movl	%esi, -4(%rdx)
	movl	(%rdi), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rdi
	addq	$-8, %rcx
	jne	.LBB29_89
	jmp	.LBB29_91
.LBB29_90:                              # %._crit_edge.i.i81
                                        #   in Loop: Header=BB29_21 Depth=1
	testq	%rdi, %rdi
	je	.LBB29_92
.LBB29_91:                              # %._crit_edge.thread.i.i86
                                        #   in Loop: Header=BB29_21 Depth=1
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
.LBB29_92:                              # %._crit_edge16.i.i87
                                        #   in Loop: Header=BB29_21 Depth=1
	movslq	%r12d, %rcx
	movl	$0, (%rax,%rcx,4)
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
.LBB29_93:                              #   in Loop: Header=BB29_21 Depth=1
	movslq	%r12d, %rax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	$47, (%rdi,%rax,4)
	movl	44(%rsp), %eax
	cmpl	$2, %eax
	movl	$1, %ebx
	cmovll	%eax, %ebx
	testl	%eax, %eax
	jle	.LBB29_98
# BB#94:                                # %.lr.ph.i
                                        #   in Loop: Header=BB29_21 Depth=1
	movq	48(%rsp), %rax
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB29_98
# BB#95:                                #   in Loop: Header=BB29_21 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB29_97
# BB#96:                                #   in Loop: Header=BB29_21 Depth=1
	callq	_ZdaPv
.LBB29_97:                              # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB29_21 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB29_98:                              # %._crit_edge.i
                                        #   in Loop: Header=BB29_21 Depth=1
.Ltmp195:
	xorl	%esi, %esi
	leaq	32(%rsp), %rdi
	movl	%ebx, %edx
	callq	_ZN17CBaseRecordVector6DeleteEii
.Ltmp196:
# BB#99:                                # %_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii.exit
                                        #   in Loop: Header=BB29_21 Depth=1
	leal	1(%r12), %r10d
	movq	168(%rsp), %r9          # 8-byte Reload
	incl	%r9d
	cmpl	28(%rsp), %r9d          # 4-byte Folded Reload
	jl	.LBB29_21
.LBB29_100:                             # %.critedge
	movq	160(%rsp), %rbp         # 8-byte Reload
	cmpl	$0, 12(%rbp)
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%r10d, %r12d
	jle	.LBB29_108
# BB#101:                               # %.lr.ph.i92
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB29_102:                             # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	cmpb	$0, g_CaseSensitive(%rip)
	movq	(%rax), %rdi
	je	.LBB29_104
# BB#103:                               #   in Loop: Header=BB29_102 Depth=1
.Ltmp198:
	callq	_Z15MyStringComparePKwS0_
.Ltmp199:
	jmp	.LBB29_105
	.p2align	4, 0x90
.LBB29_104:                             #   in Loop: Header=BB29_102 Depth=1
.Ltmp200:
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp201:
.LBB29_105:                             # %_Z16CompareFileNamesRK11CStringBaseIwES2_.exit.i
                                        #   in Loop: Header=BB29_102 Depth=1
	testl	%eax, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%r12d, %r10d
	je	.LBB29_107
# BB#106:                               #   in Loop: Header=BB29_102 Depth=1
	incq	%rbx
	movslq	12(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB29_102
	jmp	.LBB29_108
.LBB29_107:                             # %_ZNK9NWildcard7CCensor10FindPrefixERK11CStringBaseIwE.exit
	testl	%ebx, %ebx
	jns	.LBB29_126
.LBB29_108:                             # %_ZNK9NWildcard7CCensor10FindPrefixERK11CStringBaseIwE.exit.thread
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 176(%rsp)
	movl	%r10d, %r15d
	incl	%r15d
	je	.LBB29_111
# BB#109:                               # %._crit_edge16.i.i.i
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp203:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp204:
# BB#110:                               # %.noexc100
	movq	%rbx, 176(%rsp)
	movl	$0, (%rbx)
	movl	%r15d, 188(%rsp)
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%r12d, %r10d
	jmp	.LBB29_112
.LBB29_111:
	xorl	%ebx, %ebx
.LBB29_112:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB29_113:                             # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB29_113
# BB#114:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	%r10d, 184(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 192(%rsp)
	movq	$0, 208(%rsp)
.Ltmp206:
	movl	$16, %edi
	callq	_Znam
.Ltmp207:
# BB#115:
	leaq	192(%rsp), %r14
	movq	%rax, 200(%rsp)
	movl	$0, (%rax)
	movl	$4, 212(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 224(%rsp)
	movq	$8, 240(%rsp)
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, 216(%rsp)
	movaps	%xmm0, 256(%rsp)
	movq	$8, 272(%rsp)
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 248(%rsp)
	movaps	%xmm0, 288(%rsp)
	movq	$8, 304(%rsp)
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 280(%rsp)
.Ltmp209:
	movl	$136, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp210:
# BB#116:                               # %.noexc103
	testl	%r15d, %r15d
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	je	.LBB29_119
# BB#117:                               # %._crit_edge16.i.i.i121
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp211:
	callq	_Znam
.Ltmp212:
# BB#118:                               # %.noexc127
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%r15d, 12(%r13)
	jmp	.LBB29_120
.LBB29_119:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB29_120:                             # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB29_120
# BB#121:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i125
	movl	%r12d, 8(%r13)
	movq	%r13, %rdi
	addq	$16, %rdi
.Ltmp214:
	movq	%r14, %rsi
	callq	_ZN9NWildcard11CCensorNodeC2ERKS0_
.Ltmp215:
# BB#122:                               # %_ZN9NWildcard5CPairC2ERKS0_.exit
.Ltmp217:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp218:
# BB#123:
	movq	16(%rbp), %rax
	movslq	12(%rbp), %rbx
	movq	%r13, (%rax,%rbx,8)
	leal	1(%rbx), %eax
	movl	%eax, 12(%rbp)
.Ltmp223:
	movq	%r14, %rdi
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.Ltmp224:
# BB#124:
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB29_126
# BB#125:
	callq	_ZdaPv
.LBB29_126:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 128(%rsp)
	movq	$8, 144(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 120(%rsp)
.Ltmp226:
	leaq	120(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
.Ltmp227:
# BB#127:
	movb	$1, 154(%rsp)
	movl	68(%rsp), %eax          # 4-byte Reload
	movb	%al, 153(%rsp)
	movl	116(%rsp), %eax         # 4-byte Reload
	movb	%al, 152(%rsp)
	movq	16(%rbp), %rax
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rdi
	addq	$16, %rdi
.Ltmp228:
	movzbl	112(%rsp), %esi         # 1-byte Folded Reload
	leaq	120(%rsp), %rdx
	callq	_ZN9NWildcard11CCensorNode7AddItemEbRNS_5CItemE
.Ltmp229:
# BB#128:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 120(%rsp)
.Ltmp239:
	leaq	120(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp240:
# BB#129:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit.i115
.Ltmp245:
	leaq	120(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp246:
# BB#130:                               # %_ZN9NWildcard5CItemD2Ev.exit119
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB29_132
# BB#131:
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB29_132:                             # %_ZN11CStringBaseIwED2Ev.exit120
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 32(%rsp)
.Ltmp248:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp249:
# BB#133:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit70
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_134:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.3, (%rax)
.Ltmp254:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp255:
# BB#135:
.LBB29_136:
.Ltmp181:
	movq	%rax, %rbp
	jmp	.LBB29_170
.LBB29_137:
.Ltmp213:
	movq	%rax, %rbp
	jmp	.LBB29_144
.LBB29_138:
.Ltmp205:
	jmp	.LBB29_166
.LBB29_139:
.Ltmp225:
	movq	%rax, %rbp
	jmp	.LBB29_140
.LBB29_142:
.Ltmp216:
	movq	%rax, %rbp
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB29_144
# BB#143:
	callq	_ZdaPv
.LBB29_144:                             # %.body128
	movq	%r13, %rdi
	callq	_ZdlPv
	jmp	.LBB29_155
.LBB29_145:
.Ltmp208:
	movq	%rax, %rbp
	testq	%rbx, %rbx
	je	.LBB29_168
# BB#146:
	movq	%rbx, %rdi
	jmp	.LBB29_141
.LBB29_147:
.Ltmp250:
	movq	%rax, %rbp
.Ltmp251:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp252:
	jmp	.LBB29_172
.LBB29_148:
.Ltmp253:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB29_149:
.Ltmp247:
	jmp	.LBB29_166
.LBB29_150:
.Ltmp241:
	movq	%rax, %rbp
.Ltmp242:
	leaq	120(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp243:
	jmp	.LBB29_168
.LBB29_151:
.Ltmp244:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB29_152:
.Ltmp189:
	movq	%rax, %rbp
	jmp	.LBB29_170
.LBB29_153:
.Ltmp256:
	movq	%rax, %rbp
	jmp	.LBB29_170
.LBB29_154:
.Ltmp219:
	movq	%rax, %rbp
.LBB29_155:                             # %.body105
.Ltmp220:
	movq	%r14, %rdi
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.Ltmp221:
.LBB29_140:
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB29_168
.LBB29_141:
	callq	_ZdaPv
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	jne	.LBB29_169
	jmp	.LBB29_170
.LBB29_157:
.Ltmp222:
	movq	%rax, %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB29_176
# BB#158:
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB29_159:
.Ltmp230:
	movq	%rax, %rbp
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 120(%rsp)
.Ltmp231:
	leaq	120(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp232:
# BB#160:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit.i
.Ltmp237:
	leaq	120(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp238:
	jmp	.LBB29_168
.LBB29_161:
.Ltmp233:
	movq	%rax, %rbx
.Ltmp234:
	leaq	120(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp235:
	jmp	.LBB29_176
.LBB29_162:
.Ltmp236:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB29_163:
.Ltmp194:
	movq	%rax, %rbp
	movq	%r14, 16(%rsp)          # 8-byte Spill
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	jne	.LBB29_169
	jmp	.LBB29_170
.LBB29_164:
.Ltmp186:
	movq	%rax, %rbp
	jmp	.LBB29_170
.LBB29_165:
.Ltmp202:
.LBB29_166:
	movq	%rax, %rbp
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	jne	.LBB29_169
	jmp	.LBB29_170
.LBB29_167:
.Ltmp197:
	movq	%rax, %rbp
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, 16(%rsp)          # 8-byte Spill
.LBB29_168:
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB29_170
.LBB29_169:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB29_170:                             # %_ZN11CStringBaseIwED2Ev.exit
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 32(%rsp)
.Ltmp257:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp258:
# BB#171:
.Ltmp263:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp264:
.LBB29_172:                             # %unwind_resume
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB29_173:
.Ltmp259:
	movq	%rax, %rbx
.Ltmp260:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp261:
	jmp	.LBB29_176
.LBB29_174:
.Ltmp262:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB29_175:
.Ltmp265:
	movq	%rax, %rbx
.LBB29_176:                             # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end29:
	.size	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb, .Lfunc_end29-_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\234\003"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\223\003"              # Call site table length
	.long	.Ltmp177-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp178-.Ltmp177       #   Call between .Ltmp177 and .Ltmp178
	.long	.Ltmp256-.Lfunc_begin12 #     jumps to .Ltmp256
	.byte	0                       #   On action: cleanup
	.long	.Ltmp179-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp180-.Ltmp179       #   Call between .Ltmp179 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin12 #     jumps to .Ltmp181
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp185-.Ltmp182       #   Call between .Ltmp182 and .Ltmp185
	.long	.Ltmp186-.Lfunc_begin12 #     jumps to .Ltmp186
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin12 #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp190-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp191-.Ltmp190       #   Call between .Ltmp190 and .Ltmp191
	.long	.Ltmp197-.Lfunc_begin12 #     jumps to .Ltmp197
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp193-.Ltmp192       #   Call between .Ltmp192 and .Ltmp193
	.long	.Ltmp194-.Lfunc_begin12 #     jumps to .Ltmp194
	.byte	0                       #   On action: cleanup
	.long	.Ltmp195-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp197-.Lfunc_begin12 #     jumps to .Ltmp197
	.byte	0                       #   On action: cleanup
	.long	.Ltmp198-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Ltmp201-.Ltmp198       #   Call between .Ltmp198 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin12 #     jumps to .Ltmp202
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin12 # >> Call Site 9 <<
	.long	.Ltmp204-.Ltmp203       #   Call between .Ltmp203 and .Ltmp204
	.long	.Ltmp205-.Lfunc_begin12 #     jumps to .Ltmp205
	.byte	0                       #   On action: cleanup
	.long	.Ltmp206-.Lfunc_begin12 # >> Call Site 10 <<
	.long	.Ltmp207-.Ltmp206       #   Call between .Ltmp206 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin12 #     jumps to .Ltmp208
	.byte	0                       #   On action: cleanup
	.long	.Ltmp209-.Lfunc_begin12 # >> Call Site 11 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp219-.Lfunc_begin12 #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin12 # >> Call Site 12 <<
	.long	.Ltmp212-.Ltmp211       #   Call between .Ltmp211 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin12 #     jumps to .Ltmp213
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin12 # >> Call Site 13 <<
	.long	.Ltmp215-.Ltmp214       #   Call between .Ltmp214 and .Ltmp215
	.long	.Ltmp216-.Lfunc_begin12 #     jumps to .Ltmp216
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin12 # >> Call Site 14 <<
	.long	.Ltmp218-.Ltmp217       #   Call between .Ltmp217 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin12 #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin12 # >> Call Site 15 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin12 #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp226-.Lfunc_begin12 # >> Call Site 16 <<
	.long	.Ltmp229-.Ltmp226       #   Call between .Ltmp226 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin12 #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp239-.Lfunc_begin12 # >> Call Site 17 <<
	.long	.Ltmp240-.Ltmp239       #   Call between .Ltmp239 and .Ltmp240
	.long	.Ltmp241-.Lfunc_begin12 #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.long	.Ltmp245-.Lfunc_begin12 # >> Call Site 18 <<
	.long	.Ltmp246-.Ltmp245       #   Call between .Ltmp245 and .Ltmp246
	.long	.Ltmp247-.Lfunc_begin12 #     jumps to .Ltmp247
	.byte	0                       #   On action: cleanup
	.long	.Ltmp248-.Lfunc_begin12 # >> Call Site 19 <<
	.long	.Ltmp249-.Ltmp248       #   Call between .Ltmp248 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin12 #     jumps to .Ltmp250
	.byte	0                       #   On action: cleanup
	.long	.Ltmp249-.Lfunc_begin12 # >> Call Site 20 <<
	.long	.Ltmp254-.Ltmp249       #   Call between .Ltmp249 and .Ltmp254
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp254-.Lfunc_begin12 # >> Call Site 21 <<
	.long	.Ltmp255-.Ltmp254       #   Call between .Ltmp254 and .Ltmp255
	.long	.Ltmp256-.Lfunc_begin12 #     jumps to .Ltmp256
	.byte	0                       #   On action: cleanup
	.long	.Ltmp251-.Lfunc_begin12 # >> Call Site 22 <<
	.long	.Ltmp252-.Ltmp251       #   Call between .Ltmp251 and .Ltmp252
	.long	.Ltmp253-.Lfunc_begin12 #     jumps to .Ltmp253
	.byte	1                       #   On action: 1
	.long	.Ltmp242-.Lfunc_begin12 # >> Call Site 23 <<
	.long	.Ltmp243-.Ltmp242       #   Call between .Ltmp242 and .Ltmp243
	.long	.Ltmp244-.Lfunc_begin12 #     jumps to .Ltmp244
	.byte	1                       #   On action: 1
	.long	.Ltmp220-.Lfunc_begin12 # >> Call Site 24 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin12 #     jumps to .Ltmp222
	.byte	1                       #   On action: 1
	.long	.Ltmp231-.Lfunc_begin12 # >> Call Site 25 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	.Ltmp233-.Lfunc_begin12 #     jumps to .Ltmp233
	.byte	1                       #   On action: 1
	.long	.Ltmp237-.Lfunc_begin12 # >> Call Site 26 <<
	.long	.Ltmp238-.Ltmp237       #   Call between .Ltmp237 and .Ltmp238
	.long	.Ltmp265-.Lfunc_begin12 #     jumps to .Ltmp265
	.byte	1                       #   On action: 1
	.long	.Ltmp234-.Lfunc_begin12 # >> Call Site 27 <<
	.long	.Ltmp235-.Ltmp234       #   Call between .Ltmp234 and .Ltmp235
	.long	.Ltmp236-.Lfunc_begin12 #     jumps to .Ltmp236
	.byte	1                       #   On action: 1
	.long	.Ltmp257-.Lfunc_begin12 # >> Call Site 28 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp259-.Lfunc_begin12 #     jumps to .Ltmp259
	.byte	1                       #   On action: 1
	.long	.Ltmp263-.Lfunc_begin12 # >> Call Site 29 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin12 #     jumps to .Ltmp265
	.byte	1                       #   On action: 1
	.long	.Ltmp264-.Lfunc_begin12 # >> Call Site 30 <<
	.long	.Ltmp260-.Ltmp264       #   Call between .Ltmp264 and .Ltmp260
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin12 # >> Call Site 31 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin12 #     jumps to .Ltmp262
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI30_0:
	.zero	16
	.section	.text._ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_,@function
_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_: # @_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%rbp
.Lcfi215:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi216:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi217:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi218:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi219:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi220:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi221:
	.cfi_def_cfa_offset 80
.Lcfi222:
	.cfi_offset %rbx, -56
.Lcfi223:
	.cfi_offset %r12, -48
.Lcfi224:
	.cfi_offset %r13, -40
.Lcfi225:
	.cfi_offset %r14, -32
.Lcfi226:
	.cfi_offset %r15, -24
.Lcfi227:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	12(%rbx), %ebx
	movl	12(%r15), %esi
	addl	%ebx, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	testl	%ebx, %ebx
	jle	.LBB30_9
# BB#1:                                 # %.lr.ph.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB30_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_7 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%rbp,8), %r13
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movslq	8(%r13), %r14
	leaq	1(%r14), %rbx
	testl	%ebx, %ebx
	je	.LBB30_3
# BB#4:                                 # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB30_2 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp266:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rcx
.Ltmp267:
# BB#5:                                 # %.noexc.i
                                        #   in Loop: Header=BB30_2 Depth=1
	movq	%rcx, (%r12)
	movl	$0, (%rcx)
	movl	%ebx, 12(%r12)
	jmp	.LBB30_6
	.p2align	4, 0x90
.LBB30_3:                               #   in Loop: Header=BB30_2 Depth=1
	xorl	%ecx, %ecx
.LBB30_6:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB30_2 Depth=1
	movq	(%r13), %rsi
	.p2align	4, 0x90
.LBB30_7:                               #   Parent Loop BB30_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB30_7
# BB#8:                                 # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
                                        #   in Loop: Header=BB30_2 Depth=1
	movl	%r14d, 8(%r12)
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB30_2
.LBB30_9:                               # %_ZN13CObjectVectorI11CStringBaseIwEEpLERKS2_.exit
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB30_10:
.Ltmp268:
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end30:
	.size	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_, .Lfunc_end30-_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp266-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp266
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp266-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp268-.Lfunc_begin13 #     jumps to .Ltmp268
	.byte	0                       #   On action: cleanup
	.long	.Ltmp267-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Lfunc_end30-.Ltmp267   #   Call between .Ltmp267 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK9NWildcard7CCensor9CheckPathERK11CStringBaseIwEb
	.p2align	4, 0x90
	.type	_ZNK9NWildcard7CCensor9CheckPathERK11CStringBaseIwEb,@function
_ZNK9NWildcard7CCensor9CheckPathERK11CStringBaseIwEb: # @_ZNK9NWildcard7CCensor9CheckPathERK11CStringBaseIwEb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi228:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi229:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi230:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi231:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi232:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi233:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi234:
	.cfi_def_cfa_offset 64
.Lcfi235:
	.cfi_offset %rbx, -56
.Lcfi236:
	.cfi_offset %r12, -48
.Lcfi237:
	.cfi_offset %r13, -40
.Lcfi238:
	.cfi_offset %r14, -32
.Lcfi239:
	.cfi_offset %r15, -24
.Lcfi240:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movb	$1, %al
	cmpl	$0, 12(%rbx)
	jle	.LBB31_1
# BB#2:                                 # %.lr.ph
	xorl	%r13d, %r13d
	movzbl	%dl, %r15d
	leaq	7(%rsp), %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_3:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax,%r13,8), %rdi
	addq	$16, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	movq	%r12, %rcx
	callq	_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEbRb
	testb	%al, %al
	je	.LBB31_4
# BB#7:                                 #   in Loop: Header=BB31_3 Depth=1
	movb	$1, %al
	cmpb	$0, 7(%rsp)
	jne	.LBB31_5
	jmp	.LBB31_8
	.p2align	4, 0x90
.LBB31_4:                               #   in Loop: Header=BB31_3 Depth=1
	movl	%ebp, %eax
.LBB31_5:                               #   in Loop: Header=BB31_3 Depth=1
	incq	%r13
	movslq	12(%rbx), %rcx
	cmpq	%rcx, %r13
	movb	%al, %bpl
	jl	.LBB31_3
# BB#6:
	movb	%al, %bpl
	movb	$1, %al
	jmp	.LBB31_9
.LBB31_1:
	xorl	%ebp, %ebp
	jmp	.LBB31_9
.LBB31_8:
	xorl	%eax, %eax
.LBB31_9:                               # %.loopexit
	andb	%al, %bpl
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	_ZNK9NWildcard7CCensor9CheckPathERK11CStringBaseIwEb, .Lfunc_end31-_ZNK9NWildcard7CCensor9CheckPathERK11CStringBaseIwEb
	.cfi_endproc

	.globl	_ZN9NWildcard7CCensor13ExtendExcludeEv
	.p2align	4, 0x90
	.type	_ZN9NWildcard7CCensor13ExtendExcludeEv,@function
_ZN9NWildcard7CCensor13ExtendExcludeEv: # @_ZN9NWildcard7CCensor13ExtendExcludeEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi241:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi242:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi243:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi244:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi245:
	.cfi_def_cfa_offset 48
.Lcfi246:
	.cfi_offset %rbx, -40
.Lcfi247:
	.cfi_offset %r12, -32
.Lcfi248:
	.cfi_offset %r14, -24
.Lcfi249:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movslq	12(%r15), %rax
	testq	%rax, %rax
	jle	.LBB32_1
# BB#2:                                 # %.lr.ph16
	movq	16(%r15), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB32_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rcx,8), %rsi
	cmpl	$0, 8(%rsi)
	je	.LBB32_5
# BB#4:                                 #   in Loop: Header=BB32_3 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB32_3
	jmp	.LBB32_5
.LBB32_1:
	xorl	%ecx, %ecx
.LBB32_5:                               # %._crit_edge
	testl	%eax, %eax
	setle	%dl
	cmpl	%eax, %ecx
	je	.LBB32_11
# BB#6:                                 # %._crit_edge
	testb	%dl, %dl
	jne	.LBB32_11
# BB#7:                                 # %.lr.ph
	movslq	%ecx, %r14
	movl	%ecx, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB32_8:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %r12
	je	.LBB32_10
# BB#9:                                 #   in Loop: Header=BB32_8 Depth=1
	movq	16(%r15), %rax
	movq	(%rax,%rbx,8), %rdi
	addq	$16, %rdi
	movq	(%rax,%r14,8), %rsi
	addq	$16, %rsi
	callq	_ZN9NWildcard11CCensorNode13ExtendExcludeERKS0_
	movl	12(%r15), %eax
.LBB32_10:                              #   in Loop: Header=BB32_8 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB32_8
.LBB32_11:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end32:
	.size	_ZN9NWildcard7CCensor13ExtendExcludeEv, .Lfunc_end32-_ZN9NWildcard7CCensor13ExtendExcludeEv
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN9NWildcard5CItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev,@function
_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev: # @_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi250:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi251:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi252:
	.cfi_def_cfa_offset 32
.Lcfi253:
	.cfi_offset %rbx, -24
.Lcfi254:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, (%rbx)
.Ltmp269:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp270:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB33_2:
.Ltmp271:
	movq	%rax, %r14
.Ltmp272:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp273:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB33_4:
.Ltmp274:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end33:
	.size	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev, .Lfunc_end33-_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp269-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp270-.Ltmp269       #   Call between .Ltmp269 and .Ltmp270
	.long	.Ltmp271-.Lfunc_begin14 #     jumps to .Ltmp271
	.byte	0                       #   On action: cleanup
	.long	.Ltmp270-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp272-.Ltmp270       #   Call between .Ltmp270 and .Ltmp272
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp272-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp273-.Ltmp272       #   Call between .Ltmp272 and .Ltmp273
	.long	.Ltmp274-.Lfunc_begin14 #     jumps to .Ltmp274
	.byte	1                       #   On action: 1
	.long	.Ltmp273-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end33-.Ltmp273   #   Call between .Ltmp273 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev,@function
_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev: # @_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi255:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi256:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi257:
	.cfi_def_cfa_offset 32
.Lcfi258:
	.cfi_offset %rbx, -24
.Lcfi259:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, (%rbx)
.Ltmp275:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp276:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB34_2:
.Ltmp277:
	movq	%rax, %r14
.Ltmp278:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp279:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB34_4:
.Ltmp280:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end34:
	.size	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev, .Lfunc_end34-_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp275-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp276-.Ltmp275       #   Call between .Ltmp275 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin15 #     jumps to .Ltmp277
	.byte	0                       #   On action: cleanup
	.long	.Ltmp276-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp278-.Ltmp276       #   Call between .Ltmp276 and .Ltmp278
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp278-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp279-.Ltmp278       #   Call between .Ltmp278 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin15 #     jumps to .Ltmp280
	.byte	1                       #   On action: 1
	.long	.Ltmp279-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end34-.Ltmp279   #   Call between .Ltmp279 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev,@function
_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev: # @_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi260:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi261:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi262:
	.cfi_def_cfa_offset 32
.Lcfi263:
	.cfi_offset %rbx, -24
.Lcfi264:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, (%rbx)
.Ltmp281:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp282:
# BB#1:
.Ltmp287:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp288:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB35_5:
.Ltmp289:
	movq	%rax, %r14
	jmp	.LBB35_6
.LBB35_3:
.Ltmp283:
	movq	%rax, %r14
.Ltmp284:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp285:
.LBB35_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB35_4:
.Ltmp286:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev, .Lfunc_end35-_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp281-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin16 #     jumps to .Ltmp283
	.byte	0                       #   On action: cleanup
	.long	.Ltmp287-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp289-.Lfunc_begin16 #     jumps to .Ltmp289
	.byte	0                       #   On action: cleanup
	.long	.Ltmp284-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp286-.Lfunc_begin16 #     jumps to .Ltmp286
	.byte	1                       #   On action: 1
	.long	.Ltmp285-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Lfunc_end35-.Ltmp285   #   Call between .Ltmp285 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii,@function
_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii: # @_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi265:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi266:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi267:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi268:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi269:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi270:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi271:
	.cfi_def_cfa_offset 64
.Lcfi272:
	.cfi_offset %rbx, -56
.Lcfi273:
	.cfi_offset %r12, -48
.Lcfi274:
	.cfi_offset %r13, -40
.Lcfi275:
	.cfi_offset %r14, -32
.Lcfi276:
	.cfi_offset %r15, -24
.Lcfi277:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB36_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB36_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB36_5
# BB#3:                                 #   in Loop: Header=BB36_2 Depth=1
.Ltmp290:
	movq	%rbp, %rdi
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.Ltmp291:
# BB#4:                                 #   in Loop: Header=BB36_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB36_5:                               #   in Loop: Header=BB36_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB36_2
.LBB36_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB36_7:
.Ltmp292:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end36:
	.size	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii, .Lfunc_end36-_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp290-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin17 #     jumps to .Ltmp292
	.byte	0                       #   On action: cleanup
	.long	.Ltmp291-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Lfunc_end36-.Ltmp291   #   Call between .Ltmp291 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev,@function
_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev: # @_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi278:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi279:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi280:
	.cfi_def_cfa_offset 32
.Lcfi281:
	.cfi_offset %rbx, -24
.Lcfi282:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, (%rbx)
.Ltmp293:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp294:
# BB#1:
.Ltmp299:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp300:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB37_5:
.Ltmp301:
	movq	%rax, %r14
	jmp	.LBB37_6
.LBB37_3:
.Ltmp295:
	movq	%rax, %r14
.Ltmp296:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp297:
.LBB37_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB37_4:
.Ltmp298:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end37:
	.size	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev, .Lfunc_end37-_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp293-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin18 #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp299-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp300-.Ltmp299       #   Call between .Ltmp299 and .Ltmp300
	.long	.Ltmp301-.Lfunc_begin18 #     jumps to .Ltmp301
	.byte	0                       #   On action: cleanup
	.long	.Ltmp296-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp297-.Ltmp296       #   Call between .Ltmp296 and .Ltmp297
	.long	.Ltmp298-.Lfunc_begin18 #     jumps to .Ltmp298
	.byte	1                       #   On action: 1
	.long	.Ltmp297-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end37-.Ltmp297   #   Call between .Ltmp297 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii,@function
_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii: # @_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi283:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi284:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi285:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi286:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi287:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi288:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi289:
	.cfi_def_cfa_offset 64
.Lcfi290:
	.cfi_offset %rbx, -56
.Lcfi291:
	.cfi_offset %r12, -48
.Lcfi292:
	.cfi_offset %r13, -40
.Lcfi293:
	.cfi_offset %r14, -32
.Lcfi294:
	.cfi_offset %r15, -24
.Lcfi295:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB38_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB38_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB38_6
# BB#3:                                 #   in Loop: Header=BB38_2 Depth=1
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbp)
.Ltmp302:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp303:
# BB#4:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit.i
                                        #   in Loop: Header=BB38_2 Depth=1
.Ltmp308:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp309:
# BB#5:                                 # %_ZN9NWildcard5CItemD2Ev.exit
                                        #   in Loop: Header=BB38_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB38_6:                               #   in Loop: Header=BB38_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB38_2
.LBB38_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB38_8:
.Ltmp304:
	movq	%rax, %rbx
.Ltmp305:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp306:
	jmp	.LBB38_11
.LBB38_9:
.Ltmp307:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB38_10:
.Ltmp310:
	movq	%rax, %rbx
.LBB38_11:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end38:
	.size	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii, .Lfunc_end38-_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp302-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp303-.Ltmp302       #   Call between .Ltmp302 and .Ltmp303
	.long	.Ltmp304-.Lfunc_begin19 #     jumps to .Ltmp304
	.byte	0                       #   On action: cleanup
	.long	.Ltmp308-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Ltmp309-.Ltmp308       #   Call between .Ltmp308 and .Ltmp309
	.long	.Ltmp310-.Lfunc_begin19 #     jumps to .Ltmp310
	.byte	0                       #   On action: cleanup
	.long	.Ltmp309-.Lfunc_begin19 # >> Call Site 3 <<
	.long	.Ltmp305-.Ltmp309       #   Call between .Ltmp309 and .Ltmp305
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp305-.Lfunc_begin19 # >> Call Site 4 <<
	.long	.Ltmp306-.Ltmp305       #   Call between .Ltmp305 and .Ltmp306
	.long	.Ltmp307-.Lfunc_begin19 #     jumps to .Ltmp307
	.byte	1                       #   On action: 1
	.long	.Ltmp306-.Lfunc_begin19 # >> Call Site 5 <<
	.long	.Lfunc_end38-.Ltmp306   #   Call between .Ltmp306 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%rbp
.Lcfi296:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi297:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi298:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi299:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi300:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi301:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi302:
	.cfi_def_cfa_offset 64
.Lcfi303:
	.cfi_offset %rbx, -56
.Lcfi304:
	.cfi_offset %r12, -48
.Lcfi305:
	.cfi_offset %r13, -40
.Lcfi306:
	.cfi_offset %r14, -32
.Lcfi307:
	.cfi_offset %r15, -24
.Lcfi308:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB39_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB39_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB39_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB39_5
.LBB39_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB39_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp311:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp312:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB39_35
# BB#12:
	movq	%rbx, %r13
.LBB39_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB39_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB39_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB39_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB39_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB39_15
.LBB39_14:
	xorl	%esi, %esi
.LBB39_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB39_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB39_16
.LBB39_29:
	movq	%r13, %rbx
.LBB39_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp313:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp314:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB39_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB39_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB39_8
.LBB39_3:
	xorl	%eax, %eax
.LBB39_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB39_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB39_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB39_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB39_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB39_30
.LBB39_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB39_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB39_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB39_24
	jmp	.LBB39_25
.LBB39_22:
	xorl	%ecx, %ecx
.LBB39_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB39_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB39_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB39_27
.LBB39_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB39_15
	jmp	.LBB39_29
.LBB39_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp315:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end39:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end39-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin20-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp311-.Lfunc_begin20 #   Call between .Lfunc_begin20 and .Ltmp311
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp311-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp314-.Ltmp311       #   Call between .Ltmp311 and .Ltmp314
	.long	.Ltmp315-.Lfunc_begin20 #     jumps to .Ltmp315
	.byte	0                       #   On action: cleanup
	.long	.Ltmp314-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Lfunc_end39-.Ltmp314   #   Call between .Ltmp314 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9NWildcard11CCensorNodeC2ERKS0_,"axG",@progbits,_ZN9NWildcard11CCensorNodeC2ERKS0_,comdat
	.weak	_ZN9NWildcard11CCensorNodeC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN9NWildcard11CCensorNodeC2ERKS0_,@function
_ZN9NWildcard11CCensorNodeC2ERKS0_:     # @_ZN9NWildcard11CCensorNodeC2ERKS0_
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%rbp
.Lcfi309:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi310:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi311:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi312:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi313:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi314:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi315:
	.cfi_def_cfa_offset 80
.Lcfi316:
	.cfi_offset %rbx, -56
.Lcfi317:
	.cfi_offset %r12, -48
.Lcfi318:
	.cfi_offset %r13, -40
.Lcfi319:
	.cfi_offset %r14, -32
.Lcfi320:
	.cfi_offset %r15, -24
.Lcfi321:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	(%r15), %rax
	movq	%rax, (%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r14)
	movslq	16(%r15), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB40_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 8(%r14)
	movl	$0, (%rax)
	movl	%ebx, 20(%r14)
	jmp	.LBB40_3
.LBB40_1:
	xorl	%eax, %eax
.LBB40_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	leaq	8(%r14), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	8(%r15), %rcx
	.p2align	4, 0x90
.LBB40_4:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB40_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%ebp, 16(%r14)
	leaq	24(%r14), %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r14)
	movq	$8, 48(%r14)
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, 24(%r14)
.Ltmp316:
	movq	%rbx, %rdi
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp317:
# BB#6:                                 # %.noexc
	movslq	36(%r15), %r12
	movl	36(%r14), %esi
	addl	%r12d, %esi
.Ltmp318:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp319:
# BB#7:                                 # %.noexc32.preheader
	testl	%r12d, %r12d
	jle	.LBB40_13
# BB#8:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB40_9:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r15), %rax
	movq	(%rax,%rbx,8), %rbp
.Ltmp321:
	movl	$120, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp322:
# BB#10:                                # %.noexc34
                                        #   in Loop: Header=BB40_9 Depth=1
.Ltmp323:
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	_ZN9NWildcard11CCensorNodeC2ERKS0_
.Ltmp324:
# BB#11:                                #   in Loop: Header=BB40_9 Depth=1
.Ltmp326:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp327:
# BB#12:                                # %.noexc33
                                        #   in Loop: Header=BB40_9 Depth=1
	movq	40(%r14), %rax
	movslq	36(%r14), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 36(%r14)
	incq	%rbx
	cmpq	%r12, %rbx
	jl	.LBB40_9
.LBB40_13:                              # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEEC2ERKS2_.exit
	leaq	56(%r14), %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%r14)
	movq	$8, 80(%r14)
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 56(%r14)
.Ltmp332:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp333:
# BB#14:                                # %.noexc.i
	movl	68(%r15), %ebx
	movl	68(%r14), %esi
	addl	%ebx, %esi
.Ltmp334:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp335:
# BB#15:                                # %.noexc3.i
	testl	%ebx, %ebx
	jle	.LBB40_19
# BB#16:                                # %.lr.ph.i.i.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB40_17:                              # =>This Inner Loop Header: Depth=1
	movq	72(%r15), %rax
	movq	(%rax,%rbp,8), %rsi
.Ltmp337:
	movq	%r12, %rdi
	callq	_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_
.Ltmp338:
# BB#18:                                # %.noexc4.i
                                        #   in Loop: Header=BB40_17 Depth=1
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB40_17
.LBB40_19:                              # %_ZN13CObjectVectorIN9NWildcard5CItemEEC2ERKS2_.exit
	leaq	88(%r14), %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, 96(%r14)
	movq	$8, 112(%r14)
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 88(%r14)
.Ltmp343:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp344:
# BB#20:                                # %.noexc.i11
	movl	100(%r15), %ebx
	movl	100(%r14), %esi
	addl	%ebx, %esi
.Ltmp345:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp346:
# BB#21:                                # %.noexc3.i12
	testl	%ebx, %ebx
	jle	.LBB40_25
# BB#22:                                # %.lr.ph.i.i.i14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB40_23:                              # =>This Inner Loop Header: Depth=1
	movq	104(%r15), %rax
	movq	(%rax,%rbp,8), %rsi
.Ltmp348:
	movq	%r13, %rdi
	callq	_ZN13CObjectVectorIN9NWildcard5CItemEE3AddERKS1_
.Ltmp349:
# BB#24:                                # %.noexc4.i18
                                        #   in Loop: Header=BB40_23 Depth=1
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB40_23
.LBB40_25:                              # %_ZN13CObjectVectorIN9NWildcard5CItemEEC2ERKS2_.exit26
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB40_36:                              # %.loopexit.split-lp.i22
.Ltmp347:
	jmp	.LBB40_37
.LBB40_32:                              # %.loopexit.split-lp.i
.Ltmp336:
	jmp	.LBB40_33
.LBB40_27:                              # %.loopexit.split-lp
.Ltmp320:
	jmp	.LBB40_28
.LBB40_35:                              # %.loopexit.i20
.Ltmp350:
.LBB40_37:
	movq	%rax, %rbp
.Ltmp351:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp352:
# BB#38:                                # %.body24
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, (%r12)
.Ltmp354:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp355:
# BB#39:
.Ltmp360:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp361:
	jmp	.LBB40_40
.LBB40_46:
.Ltmp356:
	movq	%rax, %rbx
.Ltmp357:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp358:
	jmp	.LBB40_51
.LBB40_47:
.Ltmp359:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_45:
.Ltmp353:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_52:
.Ltmp325:
	movq	%rax, %rbp
	movq	%r13, %rdi
	callq	_ZdlPv
	jmp	.LBB40_29
.LBB40_31:                              # %.loopexit.i
.Ltmp339:
.LBB40_33:
	movq	%rax, %rbp
.Ltmp340:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp341:
.LBB40_40:                              # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, (%rdi)
.Ltmp362:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp363:
# BB#41:
.Ltmp368:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp369:
	jmp	.LBB40_42
.LBB40_34:
.Ltmp342:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_48:
.Ltmp364:
	movq	%rax, %rbx
.Ltmp365:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp366:
	jmp	.LBB40_51
.LBB40_49:
.Ltmp367:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_50:
.Ltmp370:
	movq	%rax, %rbx
.LBB40_51:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB40_26:                              # %.loopexit
.Ltmp328:
.LBB40_28:                              # %.body36
	movq	%rax, %rbp
.LBB40_29:                              # %.body36
.Ltmp329:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp330:
.LBB40_42:                              # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev.exit
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB40_44
# BB#43:
	callq	_ZdaPv
.LBB40_44:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB40_30:
.Ltmp331:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN9NWildcard11CCensorNodeC2ERKS0_, .Lfunc_end40-_ZN9NWildcard11CCensorNodeC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\200\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\367\001"              # Call site table length
	.long	.Lfunc_begin21-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp316-.Lfunc_begin21 #   Call between .Lfunc_begin21 and .Ltmp316
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp316-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Ltmp319-.Ltmp316       #   Call between .Ltmp316 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin21 #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin21 # >> Call Site 3 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp328-.Lfunc_begin21 #     jumps to .Ltmp328
	.byte	0                       #   On action: cleanup
	.long	.Ltmp323-.Lfunc_begin21 # >> Call Site 4 <<
	.long	.Ltmp324-.Ltmp323       #   Call between .Ltmp323 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin21 #     jumps to .Ltmp325
	.byte	0                       #   On action: cleanup
	.long	.Ltmp326-.Lfunc_begin21 # >> Call Site 5 <<
	.long	.Ltmp327-.Ltmp326       #   Call between .Ltmp326 and .Ltmp327
	.long	.Ltmp328-.Lfunc_begin21 #     jumps to .Ltmp328
	.byte	0                       #   On action: cleanup
	.long	.Ltmp332-.Lfunc_begin21 # >> Call Site 6 <<
	.long	.Ltmp335-.Ltmp332       #   Call between .Ltmp332 and .Ltmp335
	.long	.Ltmp336-.Lfunc_begin21 #     jumps to .Ltmp336
	.byte	0                       #   On action: cleanup
	.long	.Ltmp337-.Lfunc_begin21 # >> Call Site 7 <<
	.long	.Ltmp338-.Ltmp337       #   Call between .Ltmp337 and .Ltmp338
	.long	.Ltmp339-.Lfunc_begin21 #     jumps to .Ltmp339
	.byte	0                       #   On action: cleanup
	.long	.Ltmp343-.Lfunc_begin21 # >> Call Site 8 <<
	.long	.Ltmp346-.Ltmp343       #   Call between .Ltmp343 and .Ltmp346
	.long	.Ltmp347-.Lfunc_begin21 #     jumps to .Ltmp347
	.byte	0                       #   On action: cleanup
	.long	.Ltmp348-.Lfunc_begin21 # >> Call Site 9 <<
	.long	.Ltmp349-.Ltmp348       #   Call between .Ltmp348 and .Ltmp349
	.long	.Ltmp350-.Lfunc_begin21 #     jumps to .Ltmp350
	.byte	0                       #   On action: cleanup
	.long	.Ltmp351-.Lfunc_begin21 # >> Call Site 10 <<
	.long	.Ltmp352-.Ltmp351       #   Call between .Ltmp351 and .Ltmp352
	.long	.Ltmp353-.Lfunc_begin21 #     jumps to .Ltmp353
	.byte	1                       #   On action: 1
	.long	.Ltmp354-.Lfunc_begin21 # >> Call Site 11 <<
	.long	.Ltmp355-.Ltmp354       #   Call between .Ltmp354 and .Ltmp355
	.long	.Ltmp356-.Lfunc_begin21 #     jumps to .Ltmp356
	.byte	1                       #   On action: 1
	.long	.Ltmp360-.Lfunc_begin21 # >> Call Site 12 <<
	.long	.Ltmp361-.Ltmp360       #   Call between .Ltmp360 and .Ltmp361
	.long	.Ltmp370-.Lfunc_begin21 #     jumps to .Ltmp370
	.byte	1                       #   On action: 1
	.long	.Ltmp357-.Lfunc_begin21 # >> Call Site 13 <<
	.long	.Ltmp358-.Ltmp357       #   Call between .Ltmp357 and .Ltmp358
	.long	.Ltmp359-.Lfunc_begin21 #     jumps to .Ltmp359
	.byte	1                       #   On action: 1
	.long	.Ltmp340-.Lfunc_begin21 # >> Call Site 14 <<
	.long	.Ltmp341-.Ltmp340       #   Call between .Ltmp340 and .Ltmp341
	.long	.Ltmp342-.Lfunc_begin21 #     jumps to .Ltmp342
	.byte	1                       #   On action: 1
	.long	.Ltmp362-.Lfunc_begin21 # >> Call Site 15 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin21 #     jumps to .Ltmp364
	.byte	1                       #   On action: 1
	.long	.Ltmp368-.Lfunc_begin21 # >> Call Site 16 <<
	.long	.Ltmp369-.Ltmp368       #   Call between .Ltmp368 and .Ltmp369
	.long	.Ltmp370-.Lfunc_begin21 #     jumps to .Ltmp370
	.byte	1                       #   On action: 1
	.long	.Ltmp365-.Lfunc_begin21 # >> Call Site 17 <<
	.long	.Ltmp366-.Ltmp365       #   Call between .Ltmp365 and .Ltmp366
	.long	.Ltmp367-.Lfunc_begin21 #     jumps to .Ltmp367
	.byte	1                       #   On action: 1
	.long	.Ltmp329-.Lfunc_begin21 # >> Call Site 18 <<
	.long	.Ltmp330-.Ltmp329       #   Call between .Ltmp329 and .Ltmp330
	.long	.Ltmp331-.Lfunc_begin21 #     jumps to .Ltmp331
	.byte	1                       #   On action: 1
	.long	.Ltmp330-.Lfunc_begin21 # >> Call Site 19 <<
	.long	.Lfunc_end40-.Ltmp330   #   Call between .Ltmp330 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi322:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi323:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi324:
	.cfi_def_cfa_offset 32
.Lcfi325:
	.cfi_offset %rbx, -24
.Lcfi326:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp371:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp372:
# BB#1:
.Ltmp377:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp378:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB41_5:
.Ltmp379:
	movq	%rax, %r14
	jmp	.LBB41_6
.LBB41_3:
.Ltmp373:
	movq	%rax, %r14
.Ltmp374:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp375:
.LBB41_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB41_4:
.Ltmp376:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end41:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end41-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp371-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp372-.Ltmp371       #   Call between .Ltmp371 and .Ltmp372
	.long	.Ltmp373-.Lfunc_begin22 #     jumps to .Ltmp373
	.byte	0                       #   On action: cleanup
	.long	.Ltmp377-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Ltmp378-.Ltmp377       #   Call between .Ltmp377 and .Ltmp378
	.long	.Ltmp379-.Lfunc_begin22 #     jumps to .Ltmp379
	.byte	0                       #   On action: cleanup
	.long	.Ltmp374-.Lfunc_begin22 # >> Call Site 3 <<
	.long	.Ltmp375-.Ltmp374       #   Call between .Ltmp374 and .Ltmp375
	.long	.Ltmp376-.Lfunc_begin22 #     jumps to .Ltmp376
	.byte	1                       #   On action: 1
	.long	.Ltmp375-.Lfunc_begin22 # >> Call Site 4 <<
	.long	.Lfunc_end41-.Ltmp375   #   Call between .Ltmp375 and .Lfunc_end41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI42_0:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
.LCPI42_1:
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
.LCPI42_2:
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
.LCPI42_3:
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	16                      # 0x10
.LCPI42_4:
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	20                      # 0x14
.LCPI42_5:
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	23                      # 0x17
	.long	24                      # 0x18
.LCPI42_6:
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	27                      # 0x1b
	.long	28                      # 0x1c
.LCPI42_7:
	.long	29                      # 0x1d
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	34                      # 0x22
.LCPI42_8:
	.long	47                      # 0x2f
	.long	58                      # 0x3a
	.long	60                      # 0x3c
	.long	62                      # 0x3e
.LCPI42_9:
	.zero	16
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_Wildcard.ii,@function
_GLOBAL__sub_I_Wildcard.ii:             # @_GLOBAL__sub_I_Wildcard.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi327:
	.cfi_def_cfa_offset 16
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZL16kWildCardCharSet(%rip)
	movl	$12, %edi
	callq	_Znam
	movq	%rax, _ZL16kWildCardCharSet(%rip)
	movl	$3, _ZL16kWildCardCharSet+12(%rip)
	movl	$63, (%rax)
	movl	$42, 4(%rax)
	movl	$0, 8(%rax)
	movl	$2, _ZL16kWildCardCharSet+8(%rip)
	movl	$_ZN11CStringBaseIwED2Ev, %edi
	movl	$_ZL16kWildCardCharSet, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZL29kIllegalWildCardFileNameChars(%rip)
	movl	$156, %edi
	callq	_Znam
	movq	%rax, _ZL29kIllegalWildCardFileNameChars(%rip)
	movl	$39, _ZL29kIllegalWildCardFileNameChars+12(%rip)
	movaps	.LCPI42_0(%rip), %xmm0  # xmm0 = [1,2,3,4]
	movups	%xmm0, (%rax)
	movaps	.LCPI42_1(%rip), %xmm0  # xmm0 = [5,6,7,8]
	movups	%xmm0, 16(%rax)
	movaps	.LCPI42_2(%rip), %xmm0  # xmm0 = [9,10,11,12]
	movups	%xmm0, 32(%rax)
	movaps	.LCPI42_3(%rip), %xmm0  # xmm0 = [13,14,15,16]
	movups	%xmm0, 48(%rax)
	movaps	.LCPI42_4(%rip), %xmm0  # xmm0 = [17,18,19,20]
	movups	%xmm0, 64(%rax)
	movaps	.LCPI42_5(%rip), %xmm0  # xmm0 = [21,22,23,24]
	movups	%xmm0, 80(%rax)
	movaps	.LCPI42_6(%rip), %xmm0  # xmm0 = [25,26,27,28]
	movups	%xmm0, 96(%rax)
	movaps	.LCPI42_7(%rip), %xmm0  # xmm0 = [29,30,31,34]
	movups	%xmm0, 112(%rax)
	movaps	.LCPI42_8(%rip), %xmm0  # xmm0 = [47,58,60,62]
	movups	%xmm0, 128(%rax)
	movl	$92, 144(%rax)
	movl	$124, 148(%rax)
	movl	$0, 152(%rax)
	movl	$38, _ZL29kIllegalWildCardFileNameChars+8(%rip)
	movl	$_ZN11CStringBaseIwED2Ev, %edi
	movl	$_ZL29kIllegalWildCardFileNameChars, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end42:
	.size	_GLOBAL__sub_I_Wildcard.ii, .Lfunc_end42-_GLOBAL__sub_I_Wildcard.ii
	.cfi_endproc

	.type	g_CaseSensitive,@object # @g_CaseSensitive
	.data
	.globl	g_CaseSensitive
g_CaseSensitive:
	.byte	1                       # 0x1
	.size	g_CaseSensitive, 1

	.type	_ZL16kWildCardCharSet,@object # @_ZL16kWildCardCharSet
	.local	_ZL16kWildCardCharSet
	.comm	_ZL16kWildCardCharSet,16,8
	.type	_ZL29kIllegalWildCardFileNameChars,@object # @_ZL29kIllegalWildCardFileNameChars
	.local	_ZL29kIllegalWildCardFileNameChars
	.comm	_ZL29kIllegalWildCardFileNameChars,16,8
	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"Empty file path"
	.size	.L.str.3, 16

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.4:
	.long	46                      # 0x2e
	.long	46                      # 0x2e
	.long	0                       # 0x0
	.size	.L.str.4, 12

	.type	.L.str.5,@object        # @.str.5
	.p2align	2
.L.str.5:
	.long	46                      # 0x2e
	.long	0                       # 0x0
	.size	.L.str.5, 8

	.type	_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE,@object # @_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE
	.section	.rodata._ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE,"aG",@progbits,_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE,comdat
	.weak	_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE
	.p2align	3
_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE
	.quad	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
	.quad	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
	.quad	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
	.size	_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE, 40

	.type	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE,@object # @_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE
	.section	.rodata._ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE,"aG",@progbits,_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE,comdat
	.weak	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE
	.p2align	4
_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE:
	.asciz	"13CObjectVectorIN9NWildcard11CCensorNodeEE"
	.size	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE, 43

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE,@object # @_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE
	.section	.rodata._ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE,"aG",@progbits,_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE,comdat
	.weak	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE
	.p2align	4
_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE, 24

	.type	_ZTV13CObjectVectorIN9NWildcard5CItemEE,@object # @_ZTV13CObjectVectorIN9NWildcard5CItemEE
	.section	.rodata._ZTV13CObjectVectorIN9NWildcard5CItemEE,"aG",@progbits,_ZTV13CObjectVectorIN9NWildcard5CItemEE,comdat
	.weak	_ZTV13CObjectVectorIN9NWildcard5CItemEE
	.p2align	3
_ZTV13CObjectVectorIN9NWildcard5CItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN9NWildcard5CItemEE
	.quad	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
	.quad	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
	.quad	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN9NWildcard5CItemEE, 40

	.type	_ZTS13CObjectVectorIN9NWildcard5CItemEE,@object # @_ZTS13CObjectVectorIN9NWildcard5CItemEE
	.section	.rodata._ZTS13CObjectVectorIN9NWildcard5CItemEE,"aG",@progbits,_ZTS13CObjectVectorIN9NWildcard5CItemEE,comdat
	.weak	_ZTS13CObjectVectorIN9NWildcard5CItemEE
	.p2align	4
_ZTS13CObjectVectorIN9NWildcard5CItemEE:
	.asciz	"13CObjectVectorIN9NWildcard5CItemEE"
	.size	_ZTS13CObjectVectorIN9NWildcard5CItemEE, 36

	.type	_ZTI13CObjectVectorIN9NWildcard5CItemEE,@object # @_ZTI13CObjectVectorIN9NWildcard5CItemEE
	.section	.rodata._ZTI13CObjectVectorIN9NWildcard5CItemEE,"aG",@progbits,_ZTI13CObjectVectorIN9NWildcard5CItemEE,comdat
	.weak	_ZTI13CObjectVectorIN9NWildcard5CItemEE
	.p2align	4
_ZTI13CObjectVectorIN9NWildcard5CItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN9NWildcard5CItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN9NWildcard5CItemEE, 24

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_Wildcard.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
