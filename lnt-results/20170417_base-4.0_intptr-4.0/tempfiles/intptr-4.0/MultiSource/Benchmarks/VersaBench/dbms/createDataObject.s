	.text
	.file	"createDataObject.bc"
	.globl	createDataObject
	.p2align	4, 0x90
	.type	createDataObject,@function
createDataObject:                       # @createDataObject
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movl	%edi, %ebx
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_1
# BB#2:
	leal	-1(%rbx), %eax
	cmpl	$3, %eax
	jae	.LBB0_3
# BB#5:                                 # %switch.lookup
	cltq
	movq	.Lswitch.table(,%rax,8), %r14
	movl	%ebx, (%r15)
	leaq	(,%r14,8), %rdi
	callq	malloc
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.LBB0_6
# BB#7:                                 # %.lr.ph
	movl	$-8388609, (%rax)       # imm = 0xFF7FFFFF
	movl	$-8388609, 8(%rax)      # imm = 0xFF7FFFFF
	movl	$-8388609, 16(%rax)     # imm = 0xFF7FFFFF
	movl	$-8388609, 24(%rax)     # imm = 0xFF7FFFFF
	movl	$-8388609, 32(%rax)     # imm = 0xFF7FFFFF
	movl	$-8388609, 40(%rax)     # imm = 0xFF7FFFFF
	movl	$-8388609, 48(%rax)     # imm = 0xFF7FFFFF
	movl	$-8388609, 56(%rax)     # imm = 0xFF7FFFFF
	addq	$64, %rax
	cmpq	$8, %r14
	movl	$9, %ecx
	cmovgq	%r14, %rcx
	leaq	-64(,%rcx,8), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	memset
	movq	%r15, %r14
	jmp	.LBB0_8
.LBB0_1:
	xorl	%r14d, %r14d
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$createDataObject.name, %edi
	movl	$1, %esi
	callq	errorMessage
	jmp	.LBB0_8
.LBB0_3:
	xorl	%r14d, %r14d
	movl	$.L.str.1, %edi
	jmp	.LBB0_4
.LBB0_6:
	xorl	%r14d, %r14d
	movl	$.L.str, %edi
.LBB0_4:                                # %.loopexit
	xorl	%esi, %esi
	callq	errorMessage
	movl	$createDataObject.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movq	%r15, %rdi
	callq	free
.LBB0_8:                                # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	createDataObject, .Lfunc_end0-createDataObject
	.cfi_endproc

	.type	createDataObject.name,@object # @createDataObject.name
	.data
	.p2align	4
createDataObject.name:
	.asciz	"createDataObject"
	.size	createDataObject.name, 17

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"memory allocation failure"
	.size	.L.str, 26

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"invalid object type to create"
	.size	.L.str.1, 30

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	18                      # 0x12
	.quad	25                      # 0x19
	.quad	51                      # 0x33
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
