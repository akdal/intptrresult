	.text
	.file	"gemver.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4602678819172646912     # double 0.5
.LCPI6_1:
	.quad	4598175219545276416     # double 0.25
.LCPI6_2:
	.quad	4618441417868443648     # double 6
.LCPI6_3:
	.quad	4593671619917905920     # double 0.125
.LCPI6_4:
	.quad	4621256167635550208     # double 9
.LCPI6_5:
	.quad	4661014508095930368     # double 4000
.LCPI6_6:
	.quad	4667994757664866304     # double 12313
.LCPI6_7:
	.quad	4676215806105747456     # double 43532
.LCPI6_9:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_8:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 208
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$128000000, %edx        # imm = 0x7A12000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#1:
	movq	8(%rsp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_88
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$128000000, %edx        # imm = 0x7A12000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_88
# BB#4:                                 # %polybench_alloc_data.exit72
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#5:                                 # %polybench_alloc_data.exit72
	movq	8(%rsp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_88
# BB#6:                                 # %polybench_alloc_data.exit74
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#7:                                 # %polybench_alloc_data.exit74
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_88
# BB#8:                                 # %polybench_alloc_data.exit76
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#9:                                 # %polybench_alloc_data.exit76
	movq	8(%rsp), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_88
# BB#10:                                # %polybench_alloc_data.exit78
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#11:                                # %polybench_alloc_data.exit78
	movq	8(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB6_88
# BB#12:                                # %polybench_alloc_data.exit80
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#13:                                # %polybench_alloc_data.exit80
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_88
# BB#14:                                # %polybench_alloc_data.exit82
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#15:                                # %polybench_alloc_data.exit82
	movq	8(%rsp), %r13
	testq	%r13, %r13
	je	.LBB6_88
# BB#16:                                # %polybench_alloc_data.exit84
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#17:                                # %polybench_alloc_data.exit84
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_88
# BB#18:                                # %polybench_alloc_data.exit86
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#19:                                # %polybench_alloc_data.exit86
	movq	8(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_88
# BB#20:                                # %polybench_alloc_data.exit88
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_88
# BB#21:                                # %polybench_alloc_data.exit88
	movq	8(%rsp), %r8
	testq	%r8, %r8
	je	.LBB6_88
# BB#22:                                # %polybench_alloc_data.exit90
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_0(%rip), %xmm8   # xmm8 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_4(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI6_5(%rip), %xmm5   # xmm5 = mem[0],zero
	.p2align	4, 0x90
.LBB6_24:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_25 Depth 2
	movq	%rcx, %rdx
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%edx, %xmm6
	movq	88(%rsp), %rcx          # 8-byte Reload
	movsd	%xmm6, (%rcx,%rdx,8)
	leaq	1(%rdx), %rcx
	movslq	%ecx, %rsi
	imulq	$274877907, %rsi, %rsi  # imm = 0x10624DD3
	movq	%rsi, %rdi
	shrq	$63, %rdi
	sarq	$40, %rsi
	addl	%edi, %esi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	movapd	%xmm0, %xmm7
	mulsd	%xmm8, %xmm7
	movq	80(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm7, (%rsi,%rdx,8)
	movapd	%xmm0, %xmm7
	mulsd	%xmm1, %xmm7
	movsd	%xmm7, (%rbx,%rdx,8)
	movapd	%xmm0, %xmm7
	divsd	%xmm2, %xmm7
	movsd	%xmm7, (%rbp,%rdx,8)
	movapd	%xmm0, %xmm7
	mulsd	%xmm3, %xmm7
	movsd	%xmm7, (%r12,%rdx,8)
	divsd	%xmm4, %xmm0
	movsd	%xmm0, (%r8,%rdx,8)
	movq	$0, (%r15,%rdx,8)
	movq	$0, (%r13,%rdx,8)
	movq	%rax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_25:                               #   Parent Loop BB6_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	mulsd	%xmm6, %xmm0
	divsd	%xmm5, %xmm0
	movsd	%xmm0, -8(%rdx)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm6, %xmm0
	divsd	%xmm5, %xmm0
	movsd	%xmm0, (%rdx)
	addq	$2, %rsi
	addq	$16, %rdx
	cmpq	$4000, %rsi             # imm = 0xFA0
	jne	.LBB6_25
# BB#23:                                # %.loopexit.i
                                        #   in Loop: Header=BB6_24 Depth=1
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_24
# BB#26:                                # %init_array.exit
	movq	%r15, 112(%rsp)         # 8-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
	leaq	32000(%rbx), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	32000(%rbp), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax), %r9
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	16(%rdx), %rsi
	leaq	8(%rax), %r8
	leaq	8(%rdx), %r11
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_27:                               # %.preheader4.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_33 Depth 2
                                        #     Child Loop BB6_35 Depth 2
	movq	%r11, 72(%rsp)          # 8-byte Spill
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%r9, 56(%rsp)           # 8-byte Spill
	imulq	$32000, %rcx, %rax      # imm = 0x7D00
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax), %rdi
	leaq	32000(%rdx,%rax), %r8
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax), %r9
	leaq	32000(%rdx,%rax), %rax
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rax, %rdi
	sbbb	%al, %al
	cmpq	%r8, %r9
	sbbb	%r9b, %r9b
	andb	%al, %r9b
	cmpq	%rsi, %rdi
	sbbb	%r11b, %r11b
	cmpq	%r8, %rsi
	sbbb	%al, %al
	movb	%al, 23(%rsp)           # 1-byte Spill
	cmpq	128(%rsp), %rdi         # 8-byte Folded Reload
	sbbb	%al, %al
	cmpq	%r8, %rbx
	sbbb	%cl, %cl
	movb	%cl, 22(%rsp)           # 1-byte Spill
	cmpq	%rdx, %rdi
	sbbb	%r15b, %r15b
	cmpq	%r8, %rdx
	sbbb	%cl, %cl
	movb	%cl, 64(%rsp)           # 1-byte Spill
	cmpq	120(%rsp), %rdi         # 8-byte Folded Reload
	sbbb	%r10b, %r10b
	cmpq	%r8, %rbp
	sbbb	%dil, %dil
	testb	$1, %r9b
	jne	.LBB6_34
# BB#28:                                # %.preheader4.i
                                        #   in Loop: Header=BB6_27 Depth=1
	andb	23(%rsp), %r11b         # 1-byte Folded Reload
	andb	$1, %r11b
	jne	.LBB6_34
# BB#29:                                # %.preheader4.i
                                        #   in Loop: Header=BB6_27 Depth=1
	andb	22(%rsp), %al           # 1-byte Folded Reload
	andb	$1, %al
	jne	.LBB6_34
# BB#30:                                # %.preheader4.i
                                        #   in Loop: Header=BB6_27 Depth=1
	andb	64(%rsp), %r15b         # 1-byte Folded Reload
	andb	$1, %r15b
	jne	.LBB6_34
# BB#31:                                # %.preheader4.i
                                        #   in Loop: Header=BB6_27 Depth=1
	andb	%dil, %r10b
	andb	$1, %r10b
	jne	.LBB6_34
# BB#32:                                # %vector.body.preheader
                                        #   in Loop: Header=BB6_27 Depth=1
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	$2, %esi
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r9, %rdi
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_33:                               # %vector.body
                                        #   Parent Loop BB6_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rdi), %xmm2
	movupd	(%rdi), %xmm3
	movupd	-16(%rbx,%rsi,8), %xmm4
	movupd	(%rbx,%rsi,8), %xmm5
	mulpd	%xmm0, %xmm4
	mulpd	%xmm0, %xmm5
	addpd	%xmm2, %xmm4
	addpd	%xmm3, %xmm5
	movupd	-16(%rbp,%rsi,8), %xmm2
	movupd	(%rbp,%rsi,8), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	addpd	%xmm4, %xmm2
	addpd	%xmm5, %xmm3
	movupd	%xmm2, -16(%rdx)
	movupd	%xmm3, (%rdx)
	addq	$32, %rdi
	addq	$4, %rsi
	addq	$32, %rdx
	cmpq	$4002, %rsi             # imm = 0xFA2
	jne	.LBB6_33
	jmp	.LBB6_36
	.p2align	4, 0x90
.LBB6_34:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB6_27 Depth=1
	movq	72(%rsp), %r11          # 8-byte Reload
	movq	%r11, %rdi
	movl	$1, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%r8, %r10
	movq	56(%rsp), %r9           # 8-byte Reload
	.p2align	4, 0x90
.LBB6_35:                               # %scalar.ph
                                        #   Parent Loop BB6_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	-8(%rbx,%rax,8), %xmm0
	addsd	-8(%r10), %xmm0
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	mulsd	-8(%rbp,%rax,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, -8(%rdi)
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbx,%rax,8), %xmm0
	addsd	(%r10), %xmm0
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	mulsd	(%rbp,%rax,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdi)
	addq	$16, %r10
	addq	$2, %rax
	addq	$16, %rdi
	cmpq	$4001, %rax             # imm = 0xFA1
	jne	.LBB6_35
.LBB6_36:                               # %middle.block
                                        #   in Loop: Header=BB6_27 Depth=1
	movq	104(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	addq	$32000, %r9             # imm = 0x7D00
	movq	32(%rsp), %rsi          # 8-byte Reload
	addq	$32000, %rsi            # imm = 0x7D00
	addq	$32000, %r8             # imm = 0x7D00
	addq	$32000, %r11            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_27
# BB#37:                                # %.preheader3.i.preheader
	xorl	%eax, %eax
	movsd	.LCPI6_6(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	96(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_38:                               # %.preheader3.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_39 Depth 2
	movq	$0, (%r14,%rax,8)
	xorpd	%xmm1, %xmm1
	movl	$1, %edx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB6_39:                               #   Parent Loop BB6_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	mulsd	-8(%r12,%rdx,8), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%r14,%rax,8)
	movsd	32000(%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	mulsd	(%r12,%rdx,8), %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%r14,%rax,8)
	addq	$64000, %rsi            # imm = 0xFA00
	addq	$2, %rdx
	cmpq	$4001, %rdx             # imm = 0xFA1
	jne	.LBB6_39
# BB#40:                                #   in Loop: Header=BB6_38 Depth=1
	incq	%rax
	addq	$8, %rcx
	cmpq	$4000, %rax             # imm = 0xFA0
	jne	.LBB6_38
# BB#41:                                # %vector.memcheck188
	leaq	32000(%r14), %rax
	leaq	32000(%rdi), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	cmpq	%rcx, %r14
	movq	%rax, 136(%rsp)         # 8-byte Spill
	jae	.LBB6_43
# BB#42:                                # %vector.memcheck188
	cmpq	%rax, %rdi
	jae	.LBB6_43
# BB#45:                                # %.preheader2.i.preheader
	movl	$4, %eax
	movq	112(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_46:                               # %.preheader2.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	-32(%r14,%rax,8), %xmm1 # xmm1 = mem[0],zero
	addsd	-32(%rdi,%rax,8), %xmm1
	movsd	%xmm1, -32(%r14,%rax,8)
	movsd	-24(%r14,%rax,8), %xmm1 # xmm1 = mem[0],zero
	addsd	-24(%rdi,%rax,8), %xmm1
	movsd	%xmm1, -24(%r14,%rax,8)
	movsd	-16(%r14,%rax,8), %xmm1 # xmm1 = mem[0],zero
	addsd	-16(%rdi,%rax,8), %xmm1
	movsd	%xmm1, -16(%r14,%rax,8)
	movsd	-8(%r14,%rax,8), %xmm1  # xmm1 = mem[0],zero
	addsd	-8(%rdi,%rax,8), %xmm1
	movsd	%xmm1, -8(%r14,%rax,8)
	movsd	(%r14,%rax,8), %xmm1    # xmm1 = mem[0],zero
	addsd	(%rdi,%rax,8), %xmm1
	movsd	%xmm1, (%r14,%rax,8)
	addq	$5, %rax
	cmpq	$4004, %rax             # imm = 0xFA4
	jne	.LBB6_46
	jmp	.LBB6_47
.LBB6_43:                               # %vector.body177.preheader
	movl	$6, %eax
	movq	112(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_44:                               # %vector.body177
                                        # =>This Inner Loop Header: Depth=1
	movupd	-48(%r14,%rax,8), %xmm1
	movupd	-32(%r14,%rax,8), %xmm2
	movupd	-48(%rdi,%rax,8), %xmm3
	movupd	-32(%rdi,%rax,8), %xmm4
	addpd	%xmm1, %xmm3
	addpd	%xmm2, %xmm4
	movupd	%xmm3, -48(%r14,%rax,8)
	movupd	%xmm4, -32(%r14,%rax,8)
	movupd	-16(%r14,%rax,8), %xmm1
	movupd	(%r14,%rax,8), %xmm2
	movupd	-16(%rdi,%rax,8), %xmm3
	movupd	(%rdi,%rax,8), %xmm4
	addpd	%xmm1, %xmm3
	addpd	%xmm2, %xmm4
	movupd	%xmm3, -16(%r14,%rax,8)
	movupd	%xmm4, (%r14,%rax,8)
	addq	$8, %rax
	cmpq	$4006, %rax             # imm = 0xFA6
	jne	.LBB6_44
.LBB6_47:                               # %.preheader.i.preheader
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_7(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB6_48:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_49 Depth 2
	movsd	(%r15,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	movl	$1, %edx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_49:                               #   Parent Loop BB6_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	mulsd	-8(%r14,%rdx,8), %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%r15,%rcx,8)
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	(%r14,%rdx,8), %xmm2
	addsd	%xmm3, %xmm2
	movsd	%xmm2, (%r15,%rcx,8)
	addq	$16, %rsi
	addq	$2, %rdx
	cmpq	$4001, %rdx             # imm = 0xFA1
	jne	.LBB6_49
# BB#50:                                #   in Loop: Header=BB6_48 Depth=1
	incq	%rcx
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_48
# BB#51:                                # %.preheader4.i95.preheader
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	16(%rdx), %r11
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	16(%rsi), %r10
	leaq	8(%rdx), %rax
	leaq	8(%rsi), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_52:                               # %.preheader4.i95
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_58 Depth 2
                                        #     Child Loop BB6_60 Depth 2
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r10, 56(%rsp)          # 8-byte Spill
	movq	%r11, 32(%rsp)          # 8-byte Spill
	imulq	$32000, %rcx, %rax      # imm = 0x7D00
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax), %rdx
	leaq	32000(%rsi,%rax), %rsi
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax), %r10
	leaq	32000(%rdi,%rax), %rax
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx,8), %r9
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	(%rdi,%rcx,8), %r8
	cmpq	%rax, %rdx
	sbbb	%al, %al
	cmpq	%rsi, %r10
	sbbb	%dil, %dil
	andb	%al, %dil
	cmpq	%r9, %rdx
	sbbb	%r10b, %r10b
	cmpq	%rsi, %r9
	sbbb	%al, %al
	movb	%al, 104(%rsp)          # 1-byte Spill
	cmpq	128(%rsp), %rdx         # 8-byte Folded Reload
	sbbb	%cl, %cl
	cmpq	%rsi, %rbx
	sbbb	%al, %al
	movb	%al, 23(%rsp)           # 1-byte Spill
	cmpq	%r8, %rdx
	sbbb	%al, %al
	cmpq	%rsi, %r8
	sbbb	%r11b, %r11b
	movb	%r11b, 22(%rsp)         # 1-byte Spill
	cmpq	120(%rsp), %rdx         # 8-byte Folded Reload
	sbbb	%dl, %dl
	cmpq	%rsi, %rbp
	sbbb	%sil, %sil
	testb	$1, %dil
	jne	.LBB6_59
# BB#53:                                # %.preheader4.i95
                                        #   in Loop: Header=BB6_52 Depth=1
	andb	104(%rsp), %r10b        # 1-byte Folded Reload
	andb	$1, %r10b
	jne	.LBB6_59
# BB#54:                                # %.preheader4.i95
                                        #   in Loop: Header=BB6_52 Depth=1
	andb	23(%rsp), %cl           # 1-byte Folded Reload
	andb	$1, %cl
	jne	.LBB6_59
# BB#55:                                # %.preheader4.i95
                                        #   in Loop: Header=BB6_52 Depth=1
	andb	22(%rsp), %al           # 1-byte Folded Reload
	andb	$1, %al
	jne	.LBB6_59
# BB#56:                                # %.preheader4.i95
                                        #   in Loop: Header=BB6_52 Depth=1
	andb	%sil, %dl
	andb	$1, %dl
	jne	.LBB6_59
# BB#57:                                # %vector.body202.preheader
                                        #   in Loop: Header=BB6_52 Depth=1
	movsd	(%r9), %xmm2            # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movsd	(%r8), %xmm3            # xmm3 = mem[0],zero
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%r10, %rdx
	movl	$2, %edi
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	%r11, %rsi
	movq	48(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_58:                               # %vector.body202
                                        #   Parent Loop BB6_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rsi), %xmm4
	movupd	(%rsi), %xmm5
	movupd	-16(%rbx,%rdi,8), %xmm6
	movupd	(%rbx,%rdi,8), %xmm7
	mulpd	%xmm2, %xmm6
	mulpd	%xmm2, %xmm7
	addpd	%xmm4, %xmm6
	addpd	%xmm5, %xmm7
	movupd	-16(%rbp,%rdi,8), %xmm4
	movupd	(%rbp,%rdi,8), %xmm5
	mulpd	%xmm3, %xmm4
	mulpd	%xmm3, %xmm5
	addpd	%xmm6, %xmm4
	addpd	%xmm7, %xmm5
	movupd	%xmm4, -16(%rdx)
	movupd	%xmm5, (%rdx)
	addq	$32, %rsi
	addq	$4, %rdi
	addq	$32, %rdx
	cmpq	$4002, %rdi             # imm = 0xFA2
	jne	.LBB6_58
	jmp	.LBB6_61
	.p2align	4, 0x90
.LBB6_59:                               # %scalar.ph204.preheader
                                        #   in Loop: Header=BB6_52 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	$1, %esi
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdi
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_60:                               # %scalar.ph204
                                        #   Parent Loop BB6_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r9), %xmm2            # xmm2 = mem[0],zero
	mulsd	-8(%rbx,%rsi,8), %xmm2
	addsd	-8(%rdi), %xmm2
	movsd	(%r8), %xmm3            # xmm3 = mem[0],zero
	mulsd	-8(%rbp,%rsi,8), %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, -8(%rdx)
	movsd	(%r9), %xmm2            # xmm2 = mem[0],zero
	mulsd	(%rbx,%rsi,8), %xmm2
	addsd	(%rdi), %xmm2
	movsd	(%r8), %xmm3            # xmm3 = mem[0],zero
	mulsd	(%rbp,%rsi,8), %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%rdx)
	addq	$16, %rdi
	addq	$2, %rsi
	addq	$16, %rdx
	cmpq	$4001, %rsi             # imm = 0xFA1
	jne	.LBB6_60
.LBB6_61:                               # %middle.block203
                                        #   in Loop: Header=BB6_52 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	addq	$32000, %r11            # imm = 0x7D00
	addq	$32000, %r10            # imm = 0x7D00
	addq	$32000, %rax            # imm = 0x7D00
	addq	$32000, 64(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_52
# BB#62:                                # %.preheader3.i102.preheader
	xorl	%eax, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	96(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_63:                               # %.preheader3.i102
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_64 Depth 2
	movq	$0, (%r14,%rax,8)
	xorpd	%xmm2, %xmm2
	movl	$1, %edx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB6_64:                               #   Parent Loop BB6_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi), %xmm3           # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	mulsd	-8(%r12,%rdx,8), %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%r14,%rax,8)
	movsd	32000(%rsi), %xmm2      # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	mulsd	(%r12,%rdx,8), %xmm2
	addsd	%xmm3, %xmm2
	movsd	%xmm2, (%r14,%rax,8)
	addq	$64000, %rsi            # imm = 0xFA00
	addq	$2, %rdx
	cmpq	$4001, %rdx             # imm = 0xFA1
	jne	.LBB6_64
# BB#65:                                #   in Loop: Header=BB6_63 Depth=1
	incq	%rax
	addq	$8, %rcx
	cmpq	$4000, %rax             # imm = 0xFA0
	jne	.LBB6_63
# BB#66:                                # %vector.memcheck264
	cmpq	144(%rsp), %r14         # 8-byte Folded Reload
	jae	.LBB6_68
# BB#67:                                # %vector.memcheck264
	cmpq	136(%rsp), %rdi         # 8-byte Folded Reload
	jae	.LBB6_68
# BB#70:                                # %.preheader2.i111.preheader
	movl	$4, %eax
	.p2align	4, 0x90
.LBB6_71:                               # %.preheader2.i111
                                        # =>This Inner Loop Header: Depth=1
	movsd	-32(%r14,%rax,8), %xmm0 # xmm0 = mem[0],zero
	addsd	-32(%rdi,%rax,8), %xmm0
	movsd	%xmm0, -32(%r14,%rax,8)
	movsd	-24(%r14,%rax,8), %xmm0 # xmm0 = mem[0],zero
	addsd	-24(%rdi,%rax,8), %xmm0
	movsd	%xmm0, -24(%r14,%rax,8)
	movsd	-16(%r14,%rax,8), %xmm0 # xmm0 = mem[0],zero
	addsd	-16(%rdi,%rax,8), %xmm0
	movsd	%xmm0, -16(%r14,%rax,8)
	movsd	-8(%r14,%rax,8), %xmm0  # xmm0 = mem[0],zero
	addsd	-8(%rdi,%rax,8), %xmm0
	movsd	%xmm0, -8(%r14,%rax,8)
	movsd	(%r14,%rax,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rdi,%rax,8), %xmm0
	movsd	%xmm0, (%r14,%rax,8)
	addq	$5, %rax
	cmpq	$4004, %rax             # imm = 0xFA4
	jne	.LBB6_71
	jmp	.LBB6_72
.LBB6_68:                               # %vector.body253.preheader
	movl	$6, %eax
	.p2align	4, 0x90
.LBB6_69:                               # %vector.body253
                                        # =>This Inner Loop Header: Depth=1
	movupd	-48(%r14,%rax,8), %xmm0
	movupd	-32(%r14,%rax,8), %xmm2
	movupd	-48(%rdi,%rax,8), %xmm3
	movupd	-32(%rdi,%rax,8), %xmm4
	addpd	%xmm0, %xmm3
	addpd	%xmm2, %xmm4
	movupd	%xmm3, -48(%r14,%rax,8)
	movupd	%xmm4, -32(%r14,%rax,8)
	movupd	-16(%r14,%rax,8), %xmm0
	movupd	(%r14,%rax,8), %xmm2
	movupd	-16(%rdi,%rax,8), %xmm3
	movupd	(%rdi,%rax,8), %xmm4
	addpd	%xmm0, %xmm3
	addpd	%xmm2, %xmm4
	movupd	%xmm3, -16(%r14,%rax,8)
	movupd	%xmm4, (%r14,%rax,8)
	addq	$8, %rax
	cmpq	$4006, %rax             # imm = 0xFA6
	jne	.LBB6_69
.LBB6_72:                               # %.preheader.i114.preheader
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_73:                               # %.preheader.i114
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_74 Depth 2
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$1, %edx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_74:                               #   Parent Loop BB6_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	-8(%r14,%rdx,8), %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, (%r13,%rcx,8)
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	mulsd	(%r14,%rdx,8), %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%r13,%rcx,8)
	addq	$16, %rsi
	addq	$2, %rdx
	cmpq	$4001, %rdx             # imm = 0xFA1
	jne	.LBB6_74
# BB#75:                                #   in Loop: Header=BB6_73 Depth=1
	incq	%rcx
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_73
# BB#76:                                # %kernel_gemver_StrictFP.exit.preheader
	movl	$1, %edx
	movapd	.LCPI6_8(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_9(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB6_77:                               # %kernel_gemver_StrictFP.exit
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%r15,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r13,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_78
# BB#80:                                # %kernel_gemver_StrictFP.exit.1287
                                        #   in Loop: Header=BB6_77 Depth=1
	movsd	(%r15,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r13,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_79
# BB#81:                                #   in Loop: Header=BB6_77 Depth=1
	leaq	2(%rdx), %rax
	incq	%rdx
	cmpq	$4000, %rdx             # imm = 0xFA0
	movq	%rax, %rdx
	jl	.LBB6_77
# BB#82:                                # %check_FP.exit.preheader
	xorl	%esi, %esi
	jmp	.LBB6_83
.LBB6_84:                               #   in Loop: Header=BB6_83 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	32(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB6_85
	.p2align	4, 0x90
.LBB6_83:                               # %check_FP.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	stderr(%rip), %rdi
	movsd	(%r13,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	32(%rsp), %rsi          # 8-byte Reload
	movslq	%esi, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB6_84
.LBB6_85:                               #   in Loop: Header=BB6_83 Depth=1
	incq	%rsi
	cmpq	$4000, %rsi             # imm = 0xFA0
	jne	.LBB6_83
# BB#86:                                # %print_array.exit
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbp, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_87
.LBB6_78:                               # %check_FP.exit.threadsplit
	decq	%rdx
.LBB6_79:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_9(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %ecx
	callq	fprintf
	movl	$1, %eax
.LBB6_87:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_88:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d] = %lf and B[%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 68

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%0.2lf "
	.size	.L.str.3, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
