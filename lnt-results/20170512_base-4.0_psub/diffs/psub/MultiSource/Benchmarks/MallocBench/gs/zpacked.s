	.text
	.file	"zpacked.bc"
	.globl	zcurrentpacking
	.p2align	4, 0x90
	.type	zcurrentpacking,@function
zcurrentpacking:                        # @zcurrentpacking
	.cfi_startproc
# BB#0:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB0_2
# BB#1:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB0_2:
	movzwl	array_packing(%rip), %eax
	movw	%ax, 16(%rdi)
	movw	$4, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	zcurrentpacking, .Lfunc_end0-zcurrentpacking
	.cfi_endproc

	.globl	zpackedarray
	.p2align	4, 0x90
	.type	zpackedarray,@function
zpackedarray:                           # @zpackedarray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$10, %esi
	movl	$514, %edx              # imm = 0x202
	movl	$.L.str, %ecx
	callq	make_array
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB1_3
# BB#1:
	movzwl	10(%rbx), %edx
	movq	%rbx, %rax
	subq	osbot(%rip), %rax
	sarq	$4, %rax
	movl	$-17, %ebp
	cmpq	%rax, %rdx
	ja	.LBB1_3
# BB#2:
	movq	(%rbx), %rdi
	movq	%rdx, %r15
	shlq	$4, %r15
	movq	%rbx, %r14
	subq	%r15, %r14
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	refcpy
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	subq	%r15, osp(%rip)
.LBB1_3:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	zpackedarray, .Lfunc_end1-zpackedarray
	.cfi_endproc

	.globl	zsetpacking
	.p2align	4, 0x90
	.type	zsetpacking,@function
zsetpacking:                            # @zsetpacking
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$4, %ecx
	jne	.LBB2_2
# BB#1:
	movzwl	(%rdi), %eax
	movl	%eax, array_packing(%rip)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB2_2:
	retq
.Lfunc_end2:
	.size	zsetpacking, .Lfunc_end2-zsetpacking
	.cfi_endproc

	.globl	zpacked_op_init
	.p2align	4, 0x90
	.type	zpacked_op_init,@function
zpacked_op_init:                        # @zpacked_op_init
	.cfi_startproc
# BB#0:
	movl	$zpacked_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end3:
	.size	zpacked_op_init, .Lfunc_end3-zpacked_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"packedarray"
	.size	.L.str, 12

	.type	zpacked_op_init.my_defs,@object # @zpacked_op_init.my_defs
	.data
	.p2align	4
zpacked_op_init.my_defs:
	.quad	.L.str.1
	.quad	zcurrentpacking
	.quad	.L.str.2
	.quad	zpackedarray
	.quad	.L.str.3
	.quad	zsetpacking
	.zero	16
	.size	zpacked_op_init.my_defs, 64

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"0currentpacking"
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"1packedarray"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"1setpacking"
	.size	.L.str.3, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
