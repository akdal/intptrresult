	.text
	.file	"CAFFileALAC.bc"
	.globl	_Z24FindCAFFPacketTableStartP8_IO_FILEPiS1_
	.p2align	4, 0x90
	.type	_Z24FindCAFFPacketTableStartP8_IO_FILEPiS1_,@function
_Z24FindCAFFPacketTableStartP8_IO_FILEPiS1_: # @_Z24FindCAFFPacketTableStartP8_IO_FILEPiS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbx
	callq	ftell
	movq	%rax, %r15
	movl	$8, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	leaq	12(%rsp), %r13
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	$12, %edx
	movq	%r13, %rdi
	movq	%rbx, %rcx
	callq	fread
	movq	%rax, %rbp
	movzbl	12(%rsp), %eax
	shll	$24, %eax
	movzbl	13(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	14(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	15(%rsp), %ecx
	orl	%eax, %ecx
	cmpl	$1885432692, %ecx       # imm = 0x70616B74
	je	.LBB0_2
# BB#3:                                 # %.backedge
                                        #   in Loop: Header=BB0_1 Depth=1
	movzbl	20(%rsp), %eax
	shlq	$24, %rax
	movzbl	21(%rsp), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	22(%rsp), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	23(%rsp), %esi
	orq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	fseek
	testl	%ebp, %ebp
	jg	.LBB0_1
	jmp	.LBB0_4
.LBB0_2:                                # %.backedge.thread
	movq	%rbx, %rdi
	callq	ftell
	addl	$24, %eax
	movl	%eax, (%r12)
	movzbl	20(%rsp), %eax
	shll	$24, %eax
	movzbl	21(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	22(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	23(%rsp), %ecx
	orl	%eax, %ecx
	movl	%ecx, (%r14)
.LBB0_4:                                # %.loopexit
	movslq	%r15d, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_Z24FindCAFFPacketTableStartP8_IO_FILEPiS1_, .Lfunc_end0-_Z24FindCAFFPacketTableStartP8_IO_FILEPiS1_
	.cfi_endproc

	.globl	_Z18WriteCAFFcaffChunkP8_IO_FILE
	.p2align	4, 0x90
	.type	_Z18WriteCAFFcaffChunkP8_IO_FILE,@function
_Z18WriteCAFFcaffChunkP8_IO_FILE:       # @_Z18WriteCAFFcaffChunkP8_IO_FILE
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	movabsq	$1101229613411, %rcx    # imm = 0x10066666163
	movq	%rcx, (%rsp)
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$8, %edx
	movq	%rax, %rcx
	callq	fwrite
	popq	%rax
	retq
.Lfunc_end1:
	.size	_Z18WriteCAFFcaffChunkP8_IO_FILE, .Lfunc_end1-_Z18WriteCAFFcaffChunkP8_IO_FILE
	.cfi_endproc

	.globl	_Z18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescription
	.p2align	4, 0x90
	.type	_Z18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescription,@function
_Z18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescription: # @_Z18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescription
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 80
.Lcfi18:
	.cfi_offset %rbx, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	92(%rsp), %eax
	movb	$0, 10(%rsp)
	movw	$0, 8(%rsp)
	movq	$1668506980, (%rsp)     # imm = 0x63736564
	movl	88(%rsp), %ebp
	cmpl	$1819304813, %ebp       # imm = 0x6C70636D
	movl	$2, %ebx
	cmovnel	%eax, %ebx
	movsd	80(%rsp), %xmm0         # xmm0 = mem[0],zero
	callq	SwapFloat64NtoB
	movsd	%xmm0, 16(%rsp)
	movl	%ebp, %edi
	callq	Swap32NtoB
	movl	%eax, 24(%rsp)
	movl	%ebx, %edi
	callq	Swap32NtoB
	movl	%eax, 28(%rsp)
	movl	96(%rsp), %edi
	callq	Swap32NtoB
	movl	%eax, 32(%rsp)
	movl	100(%rsp), %edi
	callq	Swap32NtoB
	movl	%eax, 36(%rsp)
	movl	108(%rsp), %edi
	callq	Swap32NtoB
	movl	%eax, 40(%rsp)
	movl	112(%rsp), %edi
	callq	Swap32NtoB
	movl	%eax, 44(%rsp)
	movb	$32, 11(%rsp)
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$12, %edx
	movq	%r14, %rcx
	callq	fwrite
	leaq	16(%rsp), %rdi
	movl	$1, %esi
	movl	$32, %edx
	movq	%r14, %rcx
	callq	fwrite
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_Z18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescription, .Lfunc_end2-_Z18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescription
	.cfi_endproc

	.globl	_Z18WriteCAFFdataChunkP8_IO_FILE
	.p2align	4, 0x90
	.type	_Z18WriteCAFFdataChunkP8_IO_FILE,@function
_Z18WriteCAFFdataChunkP8_IO_FILE:       # @_Z18WriteCAFFdataChunkP8_IO_FILE
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 32
	movq	%rdi, %rax
	movaps	.L_ZZ18WriteCAFFdataChunkP8_IO_FILEE13theReadBuffer(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$16, %edx
	movq	%rax, %rcx
	callq	fwrite
	addq	$24, %rsp
	retq
.Lfunc_end3:
	.size	_Z18WriteCAFFdataChunkP8_IO_FILE, .Lfunc_end3-_Z18WriteCAFFdataChunkP8_IO_FILE
	.cfi_endproc

	.globl	_Z18WriteCAFFkukiChunkP8_IO_FILEPvj
	.p2align	4, 0x90
	.type	_Z18WriteCAFFkukiChunkP8_IO_FILEPvj,@function
_Z18WriteCAFFkukiChunkP8_IO_FILEPvj:    # @_Z18WriteCAFFkukiChunkP8_IO_FILEPvj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movb	$0, 10(%rsp)
	movw	$0, 8(%rsp)
	movq	$1768650091, (%rsp)     # imm = 0x696B756B
	movb	%bl, 11(%rsp)
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$12, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	%ebx, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r15, %rcx
	callq	fwrite
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_Z18WriteCAFFkukiChunkP8_IO_FILEPvj, .Lfunc_end4-_Z18WriteCAFFkukiChunkP8_IO_FILEPvj
	.cfi_endproc

	.globl	_Z18WriteCAFFChunkSizeP8_IO_FILEl
	.p2align	4, 0x90
	.type	_Z18WriteCAFFChunkSizeP8_IO_FILEl,@function
_Z18WriteCAFFChunkSizeP8_IO_FILEl:      # @_Z18WriteCAFFChunkSizeP8_IO_FILEl
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 16
	movq	%rsi, %rcx
	movq	%rdi, %rax
	movq	%rcx, %rdx
	shrq	$56, %rdx
	movb	%dl, (%rsp)
	movq	%rcx, %rdx
	shrq	$48, %rdx
	movb	%dl, 1(%rsp)
	movq	%rcx, %rdx
	shrq	$40, %rdx
	movb	%dl, 2(%rsp)
	movq	%rcx, %rdx
	shrq	$32, %rdx
	movb	%dl, 3(%rsp)
	movq	%rcx, %rdx
	shrq	$24, %rdx
	movb	%dl, 4(%rsp)
	movq	%rcx, %rdx
	shrq	$16, %rdx
	movb	%dl, 5(%rsp)
	movb	%ch, 6(%rsp)  # NOREX
	movb	%cl, 7(%rsp)
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$8, %edx
	movq	%rax, %rcx
	callq	fwrite
	popq	%rax
	retq
.Lfunc_end5:
	.size	_Z18WriteCAFFChunkSizeP8_IO_FILEl, .Lfunc_end5-_Z18WriteCAFFChunkSizeP8_IO_FILEl
	.cfi_endproc

	.globl	_Z18WriteCAFFchanChunkP8_IO_FILEj
	.p2align	4, 0x90
	.type	_Z18WriteCAFFchanChunkP8_IO_FILEj,@function
_Z18WriteCAFFchanChunkP8_IO_FILEj:      # @_Z18WriteCAFFchanChunkP8_IO_FILEj
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 32
	movl	%esi, %ecx
	movq	%rdi, %rax
	movaps	.L_ZZ18WriteCAFFchanChunkP8_IO_FILEjE9theBuffer(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
	movb	$12, 11(%rsp)
	movl	%ecx, %edx
	shrl	$24, %edx
	movb	%dl, 12(%rsp)
	movl	%ecx, %edx
	shrl	$16, %edx
	movb	%dl, 13(%rsp)
	movb	%ch, 14(%rsp)  # NOREX
	movb	%cl, 15(%rsp)
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$24, %edx
	movq	%rax, %rcx
	callq	fwrite
	addq	$24, %rsp
	retq
.Lfunc_end6:
	.size	_Z18WriteCAFFchanChunkP8_IO_FILEj, .Lfunc_end6-_Z18WriteCAFFchanChunkP8_IO_FILEj
	.cfi_endproc

	.globl	_Z18WriteCAFFfreeChunkP8_IO_FILEj
	.p2align	4, 0x90
	.type	_Z18WriteCAFFfreeChunkP8_IO_FILEj,@function
_Z18WriteCAFFfreeChunkP8_IO_FILEj:      # @_Z18WriteCAFFfreeChunkP8_IO_FILEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 48
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movl	$0, 8(%rsp)
	movq	$1701147238, (%rsp)     # imm = 0x65657266
	cmpl	$12, %ebx
	jb	.LBB7_4
# BB#1:
	addl	$-12, %ebx
	movl	%ebx, %eax
	shrl	$24, %eax
	movb	%al, 8(%rsp)
	movl	%ebx, %eax
	shrl	$16, %eax
	movb	%al, 9(%rsp)
	movb	%bh, 10(%rsp)  # NOREX
	movb	%bl, 11(%rsp)
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$12, %edx
	movq	%r14, %rcx
	callq	fwrite
	testl	%ebx, %ebx
	je	.LBB7_4
# BB#2:                                 # %.lr.ph
	movsbl	4(%rsp), %ebp
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	fputc
	decl	%ebx
	jne	.LBB7_3
.LBB7_4:                                # %.loopexit
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_Z18WriteCAFFfreeChunkP8_IO_FILEj, .Lfunc_end7-_Z18WriteCAFFfreeChunkP8_IO_FILEj
	.cfi_endproc

	.globl	_Z24WriteCAFFpaktChunkHeaderP8_IO_FILEP25port_CAFPacketTableHeaderj
	.p2align	4, 0x90
	.type	_Z24WriteCAFFpaktChunkHeaderP8_IO_FILEP25port_CAFPacketTableHeaderj,@function
_Z24WriteCAFFpaktChunkHeaderP8_IO_FILEP25port_CAFPacketTableHeaderj: # @_Z24WriteCAFFpaktChunkHeaderP8_IO_FILEP25port_CAFPacketTableHeaderj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rdi
	callq	Swap64NtoB
	movq	%rax, (%rbx)
	movq	8(%rbx), %rdi
	callq	Swap64NtoB
	movq	%rax, 8(%rbx)
	movl	16(%rbx), %edi
	callq	Swap32NtoB
	movl	%eax, 16(%rbx)
	movl	20(%rbx), %edi
	callq	Swap32NtoB
	movl	%eax, 20(%rbx)
	movb	$112, 4(%rsp)
	movb	$97, 5(%rsp)
	movb	$107, 6(%rsp)
	movb	$116, 7(%rsp)
	movl	%ebp, %ecx
	movl	%ecx, %eax
	shrl	$24, %eax
	movl	$0, 8(%rsp)
	movb	%al, 12(%rsp)
	movl	%ecx, %eax
	shrl	$16, %eax
	movb	%al, 13(%rsp)
	movb	%ch, 14(%rsp)  # NOREX
	movb	%cl, 15(%rsp)
	leaq	4(%rsp), %rdi
	movl	$1, %esi
	movl	$12, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$1, %esi
	movl	$24, %edx
	movq	%rbx, %rdi
	movq	%r14, %rcx
	callq	fwrite
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_Z24WriteCAFFpaktChunkHeaderP8_IO_FILEP25port_CAFPacketTableHeaderj, .Lfunc_end8-_Z24WriteCAFFpaktChunkHeaderP8_IO_FILEP25port_CAFPacketTableHeaderj
	.cfi_endproc

	.globl	_Z13GetBERIntegeriPhPi
	.p2align	4, 0x90
	.type	_Z13GetBERIntegeriPhPi,@function
_Z13GetBERIntegeriPhPi:                 # @_Z13GetBERIntegeriPhPi
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	andl	$127, %eax
	cmpl	%edi, %eax
	jne	.LBB9_2
# BB#1:
	movl	$1, (%rdx)
	movb	%dil, (%rsi)
	retq
.LBB9_2:
	movl	%edi, %ecx
	andl	$16383, %ecx            # imm = 0x3FFF
	cmpl	%edi, %ecx
	jne	.LBB9_4
# BB#3:
	movl	$2, (%rdx)
	shrl	$7, %edi
	orb	$-128, %dil
	movb	%dil, (%rsi)
	movb	%al, 1(%rsi)
	retq
.LBB9_4:
	movl	%edi, %ecx
	andl	$2097151, %ecx          # imm = 0x1FFFFF
	cmpl	%edi, %ecx
	jne	.LBB9_6
# BB#5:
	movl	$3, (%rdx)
	movl	%edi, %ecx
	shrl	$14, %ecx
	orb	$-128, %cl
	movb	%cl, (%rsi)
	shrl	$7, %edi
	orb	$-128, %dil
	movb	%dil, 1(%rsi)
	movb	%al, 2(%rsi)
	retq
.LBB9_6:
	movl	%edi, %ecx
	andl	$268435455, %ecx        # imm = 0xFFFFFFF
	cmpl	%edi, %ecx
	jne	.LBB9_8
# BB#7:
	movl	$4, (%rdx)
	movl	$3, %r8d
	movl	$2, %r9d
	movl	$1, %r10d
	movq	%rsi, %rdx
	jmp	.LBB9_9
.LBB9_8:
	movl	$5, (%rdx)
	movl	%edi, %ecx
	sarl	$28, %ecx
	orb	$-128, %cl
	movb	%cl, (%rsi)
	leaq	1(%rsi), %rdx
	movl	$4, %r8d
	movl	$3, %r9d
	movl	$2, %r10d
.LBB9_9:
	movl	%edi, %ecx
	shrl	$21, %ecx
	orb	$-128, %cl
	movb	%cl, (%rdx)
	movl	%edi, %ecx
	shrl	$14, %ecx
	orb	$-128, %cl
	movb	%cl, (%rsi,%r10)
	shrl	$7, %edi
	orb	$-128, %dil
	movb	%dil, (%rsi,%r9)
	movb	%al, (%rsi,%r8)
	retq
.Lfunc_end9:
	.size	_Z13GetBERIntegeriPhPi, .Lfunc_end9-_Z13GetBERIntegeriPhPi
	.cfi_endproc

	.globl	_Z14ReadBERIntegerPhPi
	.p2align	4, 0x90
	.type	_Z14ReadBERIntegerPhPi,@function
_Z14ReadBERIntegerPhPi:                 # @_Z14ReadBERIntegerPhPi
	.cfi_startproc
# BB#0:
	movzbl	(%rdi), %edx
	movl	%edx, %eax
	andl	$127, %eax
	movl	$1, %ecx
	testb	%dl, %dl
	js	.LBB10_1
.LBB10_2:                               # %.critedge
	movl	%ecx, (%rsi)
	retq
.LBB10_1:
	cmpl	$0, (%rsi)
	jle	.LBB10_2
# BB#4:
	movzbl	1(%rdi), %edx
	movl	%eax, %ecx
	shll	$7, %ecx
	movl	%edx, %eax
	andl	$127, %eax
	orl	%ecx, %eax
	movl	$2, %ecx
	testb	%dl, %dl
	jns	.LBB10_2
# BB#5:
	cmpl	$2, (%rsi)
	jl	.LBB10_2
# BB#6:
	movzbl	2(%rdi), %edx
	movl	%eax, %ecx
	shll	$7, %ecx
	movl	%edx, %eax
	andl	$127, %eax
	orl	%ecx, %eax
	movl	$3, %ecx
	testb	%dl, %dl
	jns	.LBB10_2
# BB#7:
	cmpl	$3, (%rsi)
	jl	.LBB10_2
# BB#8:
	movzbl	3(%rdi), %edx
	movl	%eax, %ecx
	shll	$7, %ecx
	movl	%edx, %eax
	andl	$127, %eax
	orl	%ecx, %eax
	movl	$4, %ecx
	testb	%dl, %dl
	jns	.LBB10_2
# BB#9:
	cmpl	$4, (%rsi)
	jl	.LBB10_2
# BB#10:
	movzbl	4(%rdi), %edx
	movl	%eax, %ecx
	shll	$7, %ecx
	movl	%edx, %eax
	andl	$127, %eax
	orl	%ecx, %eax
	movl	$5, %ecx
	testb	%dl, %dl
	jns	.LBB10_2
# BB#11:
	cmpl	$5, (%rsi)
	jl	.LBB10_2
# BB#12:
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	_Z14ReadBERIntegerPhPi, .Lfunc_end10-_Z14ReadBERIntegerPhPi
	.cfi_endproc

	.globl	_Z20BuildBasePacketTable22AudioFormatDescriptioniPiP25port_CAFPacketTableHeader
	.p2align	4, 0x90
	.type	_Z20BuildBasePacketTable22AudioFormatDescriptioniPiP25port_CAFPacketTableHeader,@function
_Z20BuildBasePacketTable22AudioFormatDescriptioniPiP25port_CAFPacketTableHeader: # @_Z20BuildBasePacketTable22AudioFormatDescriptioniPiP25port_CAFPacketTableHeader
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movl	40(%rsp), %ecx
	shrl	$3, %ecx
	imull	36(%rsp), %ecx
	xorl	%edx, %edx
	movl	%edi, %eax
	divl	%ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 8(%r8)
	movl	%eax, %edx
	shrl	$12, %edx
	movq	%rdx, (%r8)
	movl	$0, 16(%r8)
	movl	%edx, %edi
	shll	$12, %edi
	subl	%eax, %edi
	addl	$4096, %edi             # imm = 0x1000
	movl	%edi, 20(%r8)
	je	.LBB11_2
# BB#1:
	incq	%rdx
	movq	%rdx, (%r8)
.LBB11_2:
	shll	$12, %ecx
	orl	$8, %ecx
	xorl	%eax, %eax
	cmpl	$16383, %ecx            # imm = 0x3FFF
	setg	%al
	orl	$2, %eax
	imull	%eax, %edx
	movl	%edx, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	_Z20BuildBasePacketTable22AudioFormatDescriptioniPiP25port_CAFPacketTableHeader, .Lfunc_end11-_Z20BuildBasePacketTable22AudioFormatDescriptioniPiP25port_CAFPacketTableHeader
	.cfi_endproc

	.globl	_Z30GetMagicCookieSizeFromCAFFkukiP8_IO_FILE
	.p2align	4, 0x90
	.type	_Z30GetMagicCookieSizeFromCAFFkukiP8_IO_FILE,@function
_Z30GetMagicCookieSizeFromCAFFkukiP8_IO_FILE: # @_Z30GetMagicCookieSizeFromCAFFkukiP8_IO_FILE
	.cfi_startproc
# BB#0:                                 # %.lr.ph.split.us.preheader
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 64
.Lcfi50:
	.cfi_offset %rbx, -40
.Lcfi51:
	.cfi_offset %r12, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	callq	ftell
	movq	%rax, %r14
	movl	$8, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	fseek
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB12_1:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	$12, %edx
	movq	%r15, %rdi
	movq	%r12, %rcx
	callq	fread
	movq	%rax, %rbx
	movl	(%rsp), %eax
	shll	$24, %eax
	movzbl	1(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	2(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	3(%rsp), %ecx
	orl	%eax, %ecx
	cmpl	$1802857321, %ecx       # imm = 0x6B756B69
	je	.LBB12_4
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	movzbl	8(%rsp), %eax
	shlq	$24, %rax
	movzbl	9(%rsp), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	10(%rsp), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	11(%rsp), %esi
	orq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	fseek
	testl	%ebx, %ebx
	jg	.LBB12_1
# BB#3:
	movl	$-1, %ebx
	jmp	.LBB12_5
.LBB12_4:                               # %.outer
	movzbl	11(%rsp), %ebx
.LBB12_5:                               # %.outer._crit_edge
	movslq	%r14d, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	fseek
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	_Z30GetMagicCookieSizeFromCAFFkukiP8_IO_FILE, .Lfunc_end12-_Z30GetMagicCookieSizeFromCAFFkukiP8_IO_FILE
	.cfi_endproc

	.globl	_Z26GetMagicCookieFromCAFFkukiP8_IO_FILEPhPj
	.p2align	4, 0x90
	.type	_Z26GetMagicCookieFromCAFFkukiP8_IO_FILEPhPj,@function
_Z26GetMagicCookieFromCAFFkukiP8_IO_FILEPhPj: # @_Z26GetMagicCookieFromCAFFkukiP8_IO_FILEPhPj
	.cfi_startproc
# BB#0:                                 # %.lr.ph.split.us.preheader
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 80
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	callq	ftell
	movq	%rax, %r14
	movl	$8, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	leaq	12(%rsp), %r13
	.p2align	4, 0x90
.LBB13_1:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	$12, %edx
	movq	%r13, %rdi
	movq	%rbx, %rcx
	callq	fread
	movq	%rax, %rbp
	movzbl	12(%rsp), %eax
	shll	$24, %eax
	movzbl	13(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	14(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	15(%rsp), %ecx
	orl	%eax, %ecx
	cmpl	$1802857321, %ecx       # imm = 0x6B756B69
	je	.LBB13_4
# BB#2:                                 #   in Loop: Header=BB13_1 Depth=1
	movzbl	20(%rsp), %eax
	shlq	$24, %rax
	movzbl	21(%rsp), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	22(%rsp), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	23(%rsp), %esi
	orq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	fseek
	testl	%ebp, %ebp
	jg	.LBB13_1
# BB#3:
	movb	$1, %r15b
	xorl	%ebp, %ebp
	jmp	.LBB13_7
.LBB13_4:                               # %.us-lcssa.us
	movzbl	23(%rsp), %r13d
	xorl	%eax, %eax
	cmpl	%r13d, (%r15)
	movl	$0, %ebp
	jb	.LBB13_6
# BB#5:
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	callq	fread
	movb	$1, %bpl
	movl	%r13d, %eax
.LBB13_6:                               # %.outer
	movl	%eax, (%r15)
	xorl	%r15d, %r15d
.LBB13_7:                               # %.outer._crit_edge
	movslq	%r14d, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	testb	%bpl, %bpl
	sete	%al
	orb	%r15b, %al
	movzbl	%al, %eax
	negl	%eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_Z26GetMagicCookieFromCAFFkukiP8_IO_FILEPhPj, .Lfunc_end13-_Z26GetMagicCookieFromCAFFkukiP8_IO_FILEPhPj
	.cfi_endproc

	.globl	_Z17FindCAFFDataStartP8_IO_FILEPiS1_
	.p2align	4, 0x90
	.type	_Z17FindCAFFDataStartP8_IO_FILEPiS1_,@function
_Z17FindCAFFDataStartP8_IO_FILEPiS1_:   # @_Z17FindCAFFDataStartP8_IO_FILEPiS1_
	.cfi_startproc
# BB#0:                                 # %.lr.ph.split.us.preheader
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 80
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	movl	$8, %esi
	xorl	%edx, %edx
	callq	fseek
	leaq	4(%rsp), %r12
	.p2align	4, 0x90
.LBB14_1:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	$12, %edx
	movq	%r12, %rdi
	movq	%r13, %rcx
	callq	fread
	movq	%rax, %rbx
	movzbl	4(%rsp), %eax
	shll	$24, %eax
	movzbl	5(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	6(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	7(%rsp), %ecx
	orl	%eax, %ecx
	cmpl	$1684108385, %ecx       # imm = 0x64617461
	je	.LBB14_4
# BB#2:                                 #   in Loop: Header=BB14_1 Depth=1
	movzbl	12(%rsp), %eax
	shlq	$24, %rax
	movzbl	13(%rsp), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	14(%rsp), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	15(%rsp), %esi
	orq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	callq	fseek
	testl	%ebx, %ebx
	jg	.LBB14_1
# BB#3:
	xorl	%eax, %eax
	jmp	.LBB14_5
.LBB14_4:                               # %.outer
	movzbl	12(%rsp), %r12d
	shll	$24, %r12d
	movzbl	13(%rsp), %ebx
	shll	$16, %ebx
	movzbl	14(%rsp), %ebp
	shll	$8, %ebp
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movzbl	15(%rsp), %r14d
	movq	%r13, %rdi
	callq	ftell
	addl	$4, %eax
	movl	%eax, (%r15)
	orl	%r12d, %ebx
	orl	%ebp, %ebx
	leal	-4(%r14,%rbx), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movb	$1, %al
.LBB14_5:                               # %.outer._crit_edge
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_Z17FindCAFFDataStartP8_IO_FILEPiS1_, .Lfunc_end14-_Z17FindCAFFDataStartP8_IO_FILEPiS1_
	.cfi_endproc

	.globl	_Z17GetCAFFdescFormatP8_IO_FILEP22AudioFormatDescription
	.p2align	4, 0x90
	.type	_Z17GetCAFFdescFormatP8_IO_FILEP22AudioFormatDescription,@function
_Z17GetCAFFdescFormatP8_IO_FILEP22AudioFormatDescription: # @_Z17GetCAFFdescFormatP8_IO_FILEP22AudioFormatDescription
	.cfi_startproc
# BB#0:                                 # %.critedge
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 64
.Lcfi84:
	.cfi_offset %rbx, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$4, %esi
	movl	$1, %edx
	callq	fseek
	movq	%rsp, %r15
	jmp	.LBB15_1
	.p2align	4, 0x90
.LBB15_6:                               #   in Loop: Header=BB15_1 Depth=1
	movl	$1, %esi
	movl	$8, %edx
	movq	%r15, %rdi
	movq	%rbx, %rcx
	callq	fread
	movzbl	4(%rsp), %eax
	shlq	$24, %rax
	movzbl	5(%rsp), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	6(%rsp), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	7(%rsp), %esi
	orq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	fseek
.LBB15_1:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	$4, %edx
	movq	%r15, %rdi
	movq	%rbx, %rcx
	callq	fread
	movl	(%rsp), %eax
	shll	$24, %eax
	movzbl	1(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	2(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	3(%rsp), %ecx
	orl	%eax, %ecx
	cmpl	$1684370275, %ecx       # imm = 0x64657363
	jne	.LBB15_6
# BB#2:
	movl	$8, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	fseek
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$32, %edx
	movq	%rbx, %rcx
	callq	fread
	movl	8(%rsp), %edi
	callq	Swap32BtoN
	movl	%eax, 8(%r14)
	movl	24(%rsp), %edi
	callq	Swap32BtoN
	movl	%eax, 28(%r14)
	movsd	(%rsp), %xmm0           # xmm0 = mem[0],zero
	callq	SwapFloat64BtoN
	movsd	%xmm0, (%r14)
	movl	28(%rsp), %edi
	callq	Swap32BtoN
	movl	%eax, 32(%r14)
	movl	12(%rsp), %edi
	callq	Swap32BtoN
	movl	%eax, 12(%r14)
	movl	16(%rsp), %edi
	callq	Swap32BtoN
	movl	%eax, 16(%r14)
	cmpl	$1634492771, 8(%r14)    # imm = 0x616C6163
	jne	.LBB15_4
# BB#3:
	movl	$0, 24(%r14)
	jmp	.LBB15_5
.LBB15_4:
	movl	%eax, 24(%r14)
	movl	12(%r14), %eax
	movl	%eax, %ecx
	orl	$2, %ecx
	movl	%eax, %edx
	andl	$-4, %edx
	testb	$2, %al
	cmovel	%ecx, %edx
	movl	%edx, 12(%r14)
.LBB15_5:                               # %.critedge25
	movl	20(%rsp), %edi
	callq	Swap32BtoN
	movl	%eax, 20(%r14)
	movl	$0, 36(%r14)
	movb	$1, %al
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	_Z17GetCAFFdescFormatP8_IO_FILEP22AudioFormatDescription, .Lfunc_end15-_Z17GetCAFFdescFormatP8_IO_FILEP22AudioFormatDescription
	.cfi_endproc

	.type	.L_ZZ18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescriptionE13theReadBuffer,@object # @_ZZ18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescriptionE13theReadBuffer
	.section	.rodata,"a",@progbits
.L_ZZ18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescriptionE13theReadBuffer:
	.asciz	"desc\000\000\000\000\000\000\000"
	.size	.L_ZZ18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescriptionE13theReadBuffer, 12

	.type	.L_ZZ18WriteCAFFdataChunkP8_IO_FILEE13theReadBuffer,@object # @_ZZ18WriteCAFFdataChunkP8_IO_FILEE13theReadBuffer
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.L_ZZ18WriteCAFFdataChunkP8_IO_FILEE13theReadBuffer:
	.ascii	"data\000\000\000\000\000\000\000\000\000\000\000\001"
	.size	.L_ZZ18WriteCAFFdataChunkP8_IO_FILEE13theReadBuffer, 16

	.type	.L_ZZ18WriteCAFFkukiChunkP8_IO_FILEPvjE19thekukiHeaderBuffer,@object # @_ZZ18WriteCAFFkukiChunkP8_IO_FILEPvjE19thekukiHeaderBuffer
	.section	.rodata,"a",@progbits
.L_ZZ18WriteCAFFkukiChunkP8_IO_FILEPvjE19thekukiHeaderBuffer:
	.asciz	"kuki\000\000\000\000\000\000\000"
	.size	.L_ZZ18WriteCAFFkukiChunkP8_IO_FILEPvjE19thekukiHeaderBuffer, 12

	.type	.L_ZZ18WriteCAFFchanChunkP8_IO_FILEjE9theBuffer,@object # @_ZZ18WriteCAFFchanChunkP8_IO_FILEjE9theBuffer
	.p2align	4
.L_ZZ18WriteCAFFchanChunkP8_IO_FILEjE9theBuffer:
	.asciz	"chan\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	.L_ZZ18WriteCAFFchanChunkP8_IO_FILEjE9theBuffer, 24

	.type	.L_ZZ18WriteCAFFfreeChunkP8_IO_FILEjE9theBuffer,@object # @_ZZ18WriteCAFFfreeChunkP8_IO_FILEjE9theBuffer
.L_ZZ18WriteCAFFfreeChunkP8_IO_FILEjE9theBuffer:
	.asciz	"free\000\000\000\000\000\000\000"
	.size	.L_ZZ18WriteCAFFfreeChunkP8_IO_FILEjE9theBuffer, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
