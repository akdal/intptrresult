	.text
	.file	"btCollisionAlgorithm.bc"
	.globl	_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	.p2align	4, 0x90
	.type	_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo,@function
_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo: # @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	.cfi_startproc
# BB#0:
	movq	$_ZTV20btCollisionAlgorithm+16, (%rdi)
	movq	(%rsi), %rax
	movq	%rax, 8(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo, .Lfunc_end0-_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	.cfi_endproc

	.section	.text._ZN20btCollisionAlgorithmD2Ev,"axG",@progbits,_ZN20btCollisionAlgorithmD2Ev,comdat
	.weak	_ZN20btCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN20btCollisionAlgorithmD2Ev,@function
_ZN20btCollisionAlgorithmD2Ev:          # @_ZN20btCollisionAlgorithmD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN20btCollisionAlgorithmD2Ev, .Lfunc_end1-_ZN20btCollisionAlgorithmD2Ev
	.cfi_endproc

	.section	.text._ZN20btCollisionAlgorithmD0Ev,"axG",@progbits,_ZN20btCollisionAlgorithmD0Ev,comdat
	.weak	_ZN20btCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN20btCollisionAlgorithmD0Ev,@function
_ZN20btCollisionAlgorithmD0Ev:          # @_ZN20btCollisionAlgorithmD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN20btCollisionAlgorithmD0Ev, .Lfunc_end2-_ZN20btCollisionAlgorithmD0Ev
	.cfi_endproc

	.type	_ZTV20btCollisionAlgorithm,@object # @_ZTV20btCollisionAlgorithm
	.section	.rodata._ZTV20btCollisionAlgorithm,"aG",@progbits,_ZTV20btCollisionAlgorithm,comdat
	.weak	_ZTV20btCollisionAlgorithm
	.p2align	3
_ZTV20btCollisionAlgorithm:
	.quad	0
	.quad	_ZTI20btCollisionAlgorithm
	.quad	_ZN20btCollisionAlgorithmD2Ev
	.quad	_ZN20btCollisionAlgorithmD0Ev
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV20btCollisionAlgorithm, 56

	.type	_ZTS20btCollisionAlgorithm,@object # @_ZTS20btCollisionAlgorithm
	.section	.rodata._ZTS20btCollisionAlgorithm,"aG",@progbits,_ZTS20btCollisionAlgorithm,comdat
	.weak	_ZTS20btCollisionAlgorithm
	.p2align	4
_ZTS20btCollisionAlgorithm:
	.asciz	"20btCollisionAlgorithm"
	.size	_ZTS20btCollisionAlgorithm, 23

	.type	_ZTI20btCollisionAlgorithm,@object # @_ZTI20btCollisionAlgorithm
	.section	.rodata._ZTI20btCollisionAlgorithm,"aG",@progbits,_ZTI20btCollisionAlgorithm,comdat
	.weak	_ZTI20btCollisionAlgorithm
	.p2align	3
_ZTI20btCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS20btCollisionAlgorithm
	.size	_ZTI20btCollisionAlgorithm, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
