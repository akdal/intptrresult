	.text
	.file	"scan.bc"
	.globl	yylex
	.p2align	4, 0x90
	.type	yylex,@function
yylex:                                  # @yylex
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movb	yy_init(%rip), %al
	testb	%al, %al
	jne	.LBB0_11
# BB#1:
	movb	yy_start(%rip), %al
	testb	%al, %al
	jne	.LBB0_3
# BB#2:
	movb	$1, yy_start(%rip)
.LBB0_3:
	movq	yyin(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_5
# BB#4:
	movq	stdin(%rip), %rdi
	movq	%rdi, yyin(%rip)
.LBB0_5:
	cmpq	$0, yyout(%rip)
	jne	.LBB0_7
# BB#6:
	movq	stdout(%rip), %rax
	movq	%rax, yyout(%rip)
.LBB0_7:
	movq	yy_current_buffer(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_9
# BB#8:
	movq	%rdi, (%rax)
	movq	8(%rax), %rcx
	movb	$10, (%rcx)
	movl	$1, 28(%rax)
	movq	8(%rax), %rcx
	movb	$0, 1(%rcx)
	movq	8(%rax), %rcx
	movb	$0, 2(%rcx)
	movq	8(%rax), %rcx
	incq	%rcx
	movq	%rcx, 16(%rax)
	movl	$0, 32(%rax)
	jmp	.LBB0_10
.LBB0_9:
	movl	$16384, %esi            # imm = 0x4000
	callq	yy_create_buffer
	movq	%rax, yy_current_buffer(%rip)
.LBB0_10:
	movl	28(%rax), %ecx
	movl	%ecx, yy_n_chars(%rip)
	movq	16(%rax), %rcx
	movq	%rcx, yy_c_buf_p(%rip)
	movq	%rcx, yytext(%rip)
	movq	(%rax), %rax
	movq	%rax, yyin(%rip)
	movb	(%rcx), %al
	movb	%al, yy_hold_char(%rip)
	movb	$1, yy_init(%rip)
.LBB0_11:                               # %.thread137.preheader
	movabsq	$-4294967296, %r14      # imm = 0xFFFFFFFF00000000
	jmp	.LBB0_12
.LBB0_83:                               #   in Loop: Header=BB0_12 Depth=1
	cmpb	$127, %sil
	jne	.LBB0_86
# BB#84:                                #   in Loop: Header=BB0_12 Depth=1
	movl	$.L.str.3, %edi
.LBB0_85:                               # %.thread137
                                        #   in Loop: Header=BB0_12 Depth=1
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB0_12
.LBB0_56:                               # %.thread
                                        #   in Loop: Header=BB0_12 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$30, %esi
	jmp	.LBB0_57
.LBB0_86:                               #   in Loop: Header=BB0_12 Depth=1
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	yyerror
	jmp	.LBB0_12
.LBB0_117:                              #   in Loop: Header=BB0_12 Depth=1
	movb	yy_did_buffer_switch_on_eof(%rip), %al
	testb	%al, %al
	jne	.LBB0_12
# BB#118:                               #   in Loop: Header=BB0_12 Depth=1
	movq	yy_current_buffer(%rip), %rax
	movq	yyin(%rip), %rcx
	movq	%rcx, (%rax)
	movq	8(%rax), %rcx
	movb	$10, (%rcx)
	movl	$1, 28(%rax)
	movq	8(%rax), %rcx
	movb	$0, 1(%rcx)
	movq	8(%rax), %rcx
	movb	$0, 2(%rcx)
	movq	8(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movl	$0, 32(%rax)
	movl	28(%rax), %esi
	movl	%esi, yy_n_chars(%rip)
	movq	%rdx, yy_c_buf_p(%rip)
	movq	%rdx, yytext(%rip)
	movq	(%rax), %rax
	movq	%rax, yyin(%rip)
	movb	1(%rcx), %al
	movb	%al, yy_hold_char(%rip)
	jmp	.LBB0_12
.LBB0_57:                               # %.thread137
                                        #   in Loop: Header=BB0_12 Depth=1
	movl	$1, %edx
	callq	fwrite
	.p2align	4, 0x90
.LBB0_12:                               # %.thread137
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_13 Depth 2
                                        #       Child Loop BB0_16 Depth 3
                                        #         Child Loop BB0_17 Depth 4
                                        #       Child Loop BB0_21 Depth 3
                                        #         Child Loop BB0_23 Depth 4
                                        #         Child Loop BB0_92 Depth 4
                                        #           Child Loop BB0_97 Depth 5
                                        #             Child Loop BB0_98 Depth 6
                                        #         Child Loop BB0_105 Depth 4
                                        #         Child Loop BB0_114 Depth 4
                                        #           Child Loop BB0_134 Depth 5
                                        #             Child Loop BB0_135 Depth 6
                                        #       Child Loop BB0_121 Depth 3
                                        #         Child Loop BB0_126 Depth 4
                                        #           Child Loop BB0_127 Depth 5
                                        #     Child Loop BB0_53 Depth 2
                                        #       Child Loop BB0_58 Depth 3
	movq	yy_c_buf_p(%rip), %r15
	movb	yy_hold_char(%rip), %al
	movb	%al, (%r15)
	movzbl	yy_start(%rip), %eax
	movq	%r15, %rbx
	jmp	.LBB0_13
.LBB0_108:                              #   in Loop: Header=BB0_13 Depth=2
	incq	%r8
	movq	%r8, yy_c_buf_p(%rip)
	movq	%r8, %rbx
	movl	%ecx, %eax
	.p2align	4, 0x90
.LBB0_13:                               #   Parent Loop BB0_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_16 Depth 3
                                        #         Child Loop BB0_17 Depth 4
                                        #       Child Loop BB0_21 Depth 3
                                        #         Child Loop BB0_23 Depth 4
                                        #         Child Loop BB0_92 Depth 4
                                        #           Child Loop BB0_97 Depth 5
                                        #             Child Loop BB0_98 Depth 6
                                        #         Child Loop BB0_105 Depth 4
                                        #         Child Loop BB0_114 Depth 4
                                        #           Child Loop BB0_134 Depth 5
                                        #             Child Loop BB0_135 Depth 6
                                        #       Child Loop BB0_121 Depth 3
                                        #         Child Loop BB0_126 Depth 4
                                        #           Child Loop BB0_127 Depth 5
	movsbq	(%rbx), %rcx
	movslq	%eax, %rdx
	cmpw	$0, yy_accept(%rdx,%rdx)
	je	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_13 Depth=2
	movl	%eax, yy_last_accepting_state(%rip)
	movq	%rbx, yy_last_accepting_cpos(%rip)
.LBB0_15:                               # %.outer148.preheader
                                        #   in Loop: Header=BB0_13 Depth=2
	movb	yy_ec(%rcx), %cl
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_19:                               #   in Loop: Header=BB0_16 Depth=3
	movb	yy_meta(%rcx), %cl
.LBB0_16:                               # %.outer148
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_17 Depth 4
	movsbq	%cl, %rcx
	.p2align	4, 0x90
.LBB0_17:                               #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_16 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cltq
	movswq	yy_base(%rax,%rax), %rdx
	addq	%rcx, %rdx
	movswl	yy_chk(%rdx,%rdx), %esi
	cmpl	%eax, %esi
	je	.LBB0_20
# BB#18:                                #   in Loop: Header=BB0_17 Depth=4
	movswl	yy_def(%rax,%rax), %eax
	cmpl	$144, %eax
	jl	.LBB0_17
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_13 Depth=2
	movswq	yy_nxt(%rdx,%rdx), %rax
	incq	%rbx
	movzwl	yy_base(%rax,%rax), %ecx
	cmpl	$194, %ecx
	jne	.LBB0_13
	jmp	.LBB0_21
.LBB0_24:                               #   in Loop: Header=BB0_21 Depth=3
	movb	yy_hold_char(%rip), %al
	movb	%al, (%rbx)
	movq	yy_last_accepting_cpos(%rip), %rbx
	movl	yy_last_accepting_state(%rip), %eax
	.p2align	4, 0x90
.LBB0_21:                               # %.thread144
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_23 Depth 4
                                        #         Child Loop BB0_92 Depth 4
                                        #           Child Loop BB0_97 Depth 5
                                        #             Child Loop BB0_98 Depth 6
                                        #         Child Loop BB0_105 Depth 4
                                        #         Child Loop BB0_114 Depth 4
                                        #           Child Loop BB0_134 Depth 5
                                        #             Child Loop BB0_135 Depth 6
	cltq
	movswl	yy_accept(%rax,%rax), %eax
	movq	%r15, yytext(%rip)
	movl	%ebx, %ecx
	subl	%r15d, %ecx
	movl	%ecx, yyleng(%rip)
	movb	(%rbx), %cl
	movb	%cl, yy_hold_char(%rip)
	movb	$0, (%rbx)
	movq	%rbx, yy_c_buf_p(%rip)
	cmpl	$41, %eax
	jbe	.LBB0_23
	jmp	.LBB0_140
.LBB0_139:                              #   in Loop: Header=BB0_23 Depth=4
	movq	yytext(%rip), %rax
	movq	%rax, yy_c_buf_p(%rip)
	movzbl	yy_start(%rip), %ecx
	movl	%ecx, %eax
	notb	%al
	movzbl	%al, %eax
	xorl	$1, %ecx
	andl	$1, %eax
	subl	%ecx, %eax
	sarl	%eax
	addl	$41, %eax
	cmpl	$41, %eax
	ja	.LBB0_140
	.p2align	4, 0x90
.LBB0_23:                               #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_21 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%eax, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_89:                               #   in Loop: Header=BB0_23 Depth=4
	movq	yytext(%rip), %r12
	movzbl	yy_hold_char(%rip), %eax
	movb	%al, (%rbx)
	movq	yy_current_buffer(%rip), %rax
	movslq	yy_n_chars(%rip), %rcx
	addq	8(%rax), %rcx
	cmpq	%rcx, yy_c_buf_p(%rip)
	jbe	.LBB0_90
# BB#109:                               #   in Loop: Header=BB0_23 Depth=4
	callq	yy_get_next_buffer
	cmpl	$1, %eax
	jne	.LBB0_110
# BB#116:                               #   in Loop: Header=BB0_23 Depth=4
	movb	$0, yy_did_buffer_switch_on_eof(%rip)
	callq	open_new_file
	testl	%eax, %eax
	je	.LBB0_139
	jmp	.LBB0_117
.LBB0_90:                               #   in Loop: Header=BB0_21 Depth=3
	movl	%ebx, %ecx
	subl	%r12d, %ecx
	movq	yytext(%rip), %r15
	shlq	$32, %rcx
	addq	%r14, %rcx
	movq	%rcx, %r8
	sarq	$32, %r8
	addq	%r15, %r8
	movq	%r8, yy_c_buf_p(%rip)
	movzbl	yy_start(%rip), %eax
	testq	%rcx, %rcx
	jle	.LBB0_102
# BB#91:                                # %.lr.ph.i128.preheader
                                        #   in Loop: Header=BB0_21 Depth=3
	movq	%r15, %rdx
.LBB0_92:                               # %.lr.ph.i128
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_21 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_97 Depth 5
                                        #             Child Loop BB0_98 Depth 6
	movsbq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_93
# BB#94:                                #   in Loop: Header=BB0_92 Depth=4
	movb	yy_ec(%rcx), %cl
	jmp	.LBB0_95
.LBB0_93:                               #   in Loop: Header=BB0_92 Depth=4
	movb	$1, %cl
.LBB0_95:                               #   in Loop: Header=BB0_92 Depth=4
	movslq	%eax, %rdi
	cmpw	$0, yy_accept(%rdi,%rdi)
	je	.LBB0_97
# BB#96:                                #   in Loop: Header=BB0_92 Depth=4
	movl	%eax, yy_last_accepting_state(%rip)
	movq	%rdx, yy_last_accepting_cpos(%rip)
	jmp	.LBB0_97
.LBB0_100:                              #   in Loop: Header=BB0_97 Depth=5
	movb	yy_meta(%rcx), %cl
.LBB0_97:                               # %.outer.i131
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_21 Depth=3
                                        #         Parent Loop BB0_92 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB0_98 Depth 6
	movsbq	%cl, %rcx
	.p2align	4, 0x90
.LBB0_98:                               #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_21 Depth=3
                                        #         Parent Loop BB0_92 Depth=4
                                        #           Parent Loop BB0_97 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	cltq
	movswq	yy_base(%rax,%rax), %rdi
	addq	%rcx, %rdi
	movswl	yy_chk(%rdi,%rdi), %esi
	cmpl	%eax, %esi
	je	.LBB0_101
# BB#99:                                #   in Loop: Header=BB0_98 Depth=6
	movswl	yy_def(%rax,%rax), %eax
	cmpl	$144, %eax
	jl	.LBB0_98
	jmp	.LBB0_100
.LBB0_101:                              #   in Loop: Header=BB0_92 Depth=4
	movswl	yy_nxt(%rdi,%rdi), %eax
	incq	%rdx
	cmpq	%r8, %rdx
	jne	.LBB0_92
.LBB0_102:                              # %yy_get_previous_state.exit134
                                        #   in Loop: Header=BB0_21 Depth=3
	movslq	%eax, %rcx
	cmpw	$0, yy_accept(%rcx,%rcx)
	je	.LBB0_104
# BB#103:                               #   in Loop: Header=BB0_21 Depth=3
	movl	%eax, yy_last_accepting_state(%rip)
	movq	%r8, yy_last_accepting_cpos(%rip)
.LBB0_104:                              # %.preheader.i
                                        #   in Loop: Header=BB0_21 Depth=3
	movswq	yy_base(%rcx,%rcx), %rdx
	movswl	yy_chk+2(%rdx,%rdx), %esi
	cmpl	%eax, %esi
	je	.LBB0_106
.LBB0_105:                              # %.lr.ph.i135
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_21 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movswq	yy_def(%rcx,%rcx), %rcx
	movswq	yy_base(%rcx,%rcx), %rdx
	cmpw	%cx, yy_chk+2(%rdx,%rdx)
	jne	.LBB0_105
.LBB0_106:                              #   in Loop: Header=BB0_21 Depth=3
	incq	%rdx
	movswq	yy_nxt(%rdx,%rdx), %rcx
	movzwl	yy_base(%rcx,%rcx), %esi
	cmpl	$194, %esi
	je	.LBB0_21
# BB#107:                               # %yy_try_NUL_trans.exit
                                        #   in Loop: Header=BB0_21 Depth=3
	testq	%rdx, %rdx
	je	.LBB0_21
	jmp	.LBB0_108
.LBB0_110:                              #   in Loop: Header=BB0_21 Depth=3
	testl	%eax, %eax
	je	.LBB0_119
# BB#111:                               #   in Loop: Header=BB0_21 Depth=3
	cmpl	$2, %eax
	jne	.LBB0_12
# BB#112:                               #   in Loop: Header=BB0_21 Depth=3
	movq	yy_current_buffer(%rip), %rax
	movslq	yy_n_chars(%rip), %rbx
	addq	8(%rax), %rbx
	movq	%rbx, yy_c_buf_p(%rip)
	movzbl	yy_start(%rip), %eax
	movq	yytext(%rip), %r15
	cmpq	%rbx, %r15
	jae	.LBB0_21
# BB#113:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_21 Depth=3
	movq	%r15, %rcx
.LBB0_114:                              # %.lr.ph.i
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_21 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_134 Depth 5
                                        #             Child Loop BB0_135 Depth 6
	movsbq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB0_115
# BB#131:                               #   in Loop: Header=BB0_114 Depth=4
	movb	yy_ec(%rdx), %dl
	jmp	.LBB0_132
.LBB0_115:                              #   in Loop: Header=BB0_114 Depth=4
	movb	$1, %dl
.LBB0_132:                              #   in Loop: Header=BB0_114 Depth=4
	movslq	%eax, %rsi
	cmpw	$0, yy_accept(%rsi,%rsi)
	je	.LBB0_134
# BB#133:                               #   in Loop: Header=BB0_114 Depth=4
	movl	%eax, yy_last_accepting_state(%rip)
	movq	%rcx, yy_last_accepting_cpos(%rip)
	jmp	.LBB0_134
.LBB0_137:                              #   in Loop: Header=BB0_134 Depth=5
	movb	yy_meta(%rdx), %dl
.LBB0_134:                              # %.outer.i
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_21 Depth=3
                                        #         Parent Loop BB0_114 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB0_135 Depth 6
	movsbq	%dl, %rdx
.LBB0_135:                              #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_21 Depth=3
                                        #         Parent Loop BB0_114 Depth=4
                                        #           Parent Loop BB0_134 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	cltq
	movswq	yy_base(%rax,%rax), %rsi
	addq	%rdx, %rsi
	movswl	yy_chk(%rsi,%rsi), %edi
	cmpl	%eax, %edi
	je	.LBB0_138
# BB#136:                               #   in Loop: Header=BB0_135 Depth=6
	movswl	yy_def(%rax,%rax), %eax
	cmpl	$144, %eax
	jl	.LBB0_135
	jmp	.LBB0_137
.LBB0_138:                              #   in Loop: Header=BB0_114 Depth=4
	movswl	yy_nxt(%rsi,%rsi), %eax
	incq	%rcx
	cmpq	%rbx, %rcx
	jne	.LBB0_114
	jmp	.LBB0_21
.LBB0_119:                              #   in Loop: Header=BB0_13 Depth=2
	subl	%r12d, %ebx
	movq	yytext(%rip), %r15
	shlq	$32, %rbx
	movq	%rbx, %rcx
	addq	%r14, %rcx
	movq	%rcx, %rbx
	sarq	$32, %rbx
	addq	%r15, %rbx
	movq	%rbx, yy_c_buf_p(%rip)
	movzbl	yy_start(%rip), %eax
	testq	%rcx, %rcx
	jle	.LBB0_13
# BB#120:                               # %.lr.ph.i117.preheader
                                        #   in Loop: Header=BB0_13 Depth=2
	movq	%r15, %rcx
.LBB0_121:                              # %.lr.ph.i117
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_126 Depth 4
                                        #           Child Loop BB0_127 Depth 5
	movsbq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB0_122
# BB#123:                               #   in Loop: Header=BB0_121 Depth=3
	movb	yy_ec(%rdx), %dl
	jmp	.LBB0_124
.LBB0_122:                              #   in Loop: Header=BB0_121 Depth=3
	movb	$1, %dl
.LBB0_124:                              #   in Loop: Header=BB0_121 Depth=3
	movslq	%eax, %rsi
	cmpw	$0, yy_accept(%rsi,%rsi)
	je	.LBB0_126
# BB#125:                               #   in Loop: Header=BB0_121 Depth=3
	movl	%eax, yy_last_accepting_state(%rip)
	movq	%rcx, yy_last_accepting_cpos(%rip)
	jmp	.LBB0_126
.LBB0_129:                              #   in Loop: Header=BB0_126 Depth=4
	movb	yy_meta(%rdx), %dl
.LBB0_126:                              # %.outer.i120
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_121 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_127 Depth 5
	movsbq	%dl, %rdx
.LBB0_127:                              #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_121 Depth=3
                                        #         Parent Loop BB0_126 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cltq
	movswq	yy_base(%rax,%rax), %rsi
	addq	%rdx, %rsi
	movswl	yy_chk(%rsi,%rsi), %edi
	cmpl	%eax, %edi
	je	.LBB0_130
# BB#128:                               #   in Loop: Header=BB0_127 Depth=5
	movswl	yy_def(%rax,%rax), %eax
	cmpl	$144, %eax
	jl	.LBB0_127
	jmp	.LBB0_129
.LBB0_130:                              #   in Loop: Header=BB0_121 Depth=3
	movswl	yy_nxt(%rsi,%rsi), %eax
	incq	%rcx
	cmpq	%rbx, %rcx
	jne	.LBB0_121
	jmp	.LBB0_13
.LBB0_51:                               #   in Loop: Header=BB0_12 Depth=1
	incl	line_no(%rip)
	jmp	.LBB0_12
.LBB0_81:                               #   in Loop: Header=BB0_12 Depth=1
	movq	yytext(%rip), %rcx
	movsbl	(%rcx), %esi
	cmpl	$31, %esi
	jg	.LBB0_83
# BB#82:                                #   in Loop: Header=BB0_12 Depth=1
	addl	$64, %esi
	movl	$.L.str.2, %edi
	jmp	.LBB0_85
.LBB0_87:                               #   in Loop: Header=BB0_12 Depth=1
	movq	yytext(%rip), %rdi
	movslq	yyleng(%rip), %rsi
	movq	yyout(%rip), %rcx
	jmp	.LBB0_57
.LBB0_61:                               # %.preheader149
                                        #   in Loop: Header=BB0_53 Depth=2
	cmpl	$47, %eax
	je	.LBB0_12
	jmp	.LBB0_53
.LBB0_59:                               # %.preheader149
                                        #   in Loop: Header=BB0_53 Depth=2
	cmpl	$10, %eax
	jne	.LBB0_55
.LBB0_52:                               #   in Loop: Header=BB0_53 Depth=2
	incl	line_no(%rip)
.LBB0_53:                               # %.backedge
                                        #   Parent Loop BB0_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_58 Depth 3
	callq	input
	cmpl	$10, %eax
	je	.LBB0_52
# BB#54:                                # %.backedge
                                        #   in Loop: Header=BB0_53 Depth=2
	cmpl	$42, %eax
	je	.LBB0_58
.LBB0_55:                               # %.backedge
                                        #   in Loop: Header=BB0_53 Depth=2
	cmpl	$-1, %eax
	jne	.LBB0_53
	jmp	.LBB0_56
.LBB0_58:                               # %.preheader149
                                        #   Parent Loop BB0_12 Depth=1
                                        #     Parent Loop BB0_53 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	callq	input
	cmpl	$41, %eax
	jle	.LBB0_59
# BB#60:                                # %.preheader149
                                        #   in Loop: Header=BB0_58 Depth=3
	cmpl	$42, %eax
	jne	.LBB0_61
	jmp	.LBB0_58
.LBB0_141:                              # %.loopexit
	movl	$268, %ebx              # imm = 0x10C
	jmp	.LBB0_146
.LBB0_142:                              # %.loopexit277
	movl	$269, %ebx              # imm = 0x10D
	jmp	.LBB0_146
.LBB0_143:                              # %.loopexit398
	movl	$270, %ebx              # imm = 0x10E
	jmp	.LBB0_146
.LBB0_144:                              # %.loopexit562
	movl	$271, %ebx              # imm = 0x10F
	jmp	.LBB0_146
.LBB0_145:                              # %.loopexit726
	movl	$272, %ebx              # imm = 0x110
	jmp	.LBB0_146
.LBB0_25:
	movl	$273, %ebx              # imm = 0x111
	jmp	.LBB0_146
.LBB0_26:
	movl	$274, %ebx              # imm = 0x112
	jmp	.LBB0_146
.LBB0_27:
	movl	$275, %ebx              # imm = 0x113
	jmp	.LBB0_146
.LBB0_28:
	movl	$276, %ebx              # imm = 0x114
	jmp	.LBB0_146
.LBB0_29:
	movl	$278, %ebx              # imm = 0x116
	jmp	.LBB0_146
.LBB0_30:
	movl	$279, %ebx              # imm = 0x117
	jmp	.LBB0_146
.LBB0_31:
	movl	$280, %ebx              # imm = 0x118
	jmp	.LBB0_146
.LBB0_32:
	movl	$281, %ebx              # imm = 0x119
	jmp	.LBB0_146
.LBB0_33:
	movl	$277, %ebx              # imm = 0x115
	jmp	.LBB0_146
.LBB0_34:
	movl	$282, %ebx              # imm = 0x11A
	jmp	.LBB0_146
.LBB0_35:
	movl	$284, %ebx              # imm = 0x11C
	jmp	.LBB0_146
.LBB0_36:
	movl	$285, %ebx              # imm = 0x11D
	jmp	.LBB0_146
.LBB0_37:
	movl	$283, %ebx              # imm = 0x11B
	jmp	.LBB0_146
.LBB0_38:
	movl	$286, %ebx              # imm = 0x11E
	jmp	.LBB0_146
.LBB0_39:
	movl	$287, %ebx              # imm = 0x11F
	jmp	.LBB0_146
.LBB0_40:
	movl	$288, %ebx              # imm = 0x120
	jmp	.LBB0_146
.LBB0_41:
	movq	yytext(%rip), %rax
	movb	(%rax), %cl
	movb	%cl, yylval+8(%rip)
	movsbl	(%rax), %ebx
	jmp	.LBB0_146
.LBB0_42:
	movl	$258, %ebx              # imm = 0x102
	jmp	.LBB0_146
.LBB0_43:
	movl	$259, %ebx              # imm = 0x103
	jmp	.LBB0_146
.LBB0_44:
	movl	$260, %ebx              # imm = 0x104
	jmp	.LBB0_146
.LBB0_45:
	movq	yytext(%rip), %rax
	movb	(%rax), %al
	movb	%al, yylval+8(%rip)
	movl	$264, %ebx              # imm = 0x108
	jmp	.LBB0_146
.LBB0_46:
	movq	yytext(%rip), %rax
	movb	(%rax), %al
	movb	%al, yylval+8(%rip)
	movl	$265, %ebx              # imm = 0x109
	jmp	.LBB0_146
.LBB0_47:
	movb	$61, yylval+8(%rip)
	movb	yy_hold_char(%rip), %al
	movb	%al, (%rbx)
	leaq	1(%r15), %rax
	movq	%rax, yy_c_buf_p(%rip)
	movq	%r15, yytext(%rip)
	movl	$1, yyleng(%rip)
	movb	1(%r15), %cl
	movb	%cl, yy_hold_char(%rip)
	movb	$0, 1(%r15)
	movq	%rax, yy_c_buf_p(%rip)
	movl	$265, %ebx              # imm = 0x109
	jmp	.LBB0_146
.LBB0_48:
	movq	yytext(%rip), %rdi
	callq	strcopyof
	movq	%rax, yylval(%rip)
	movl	$266, %ebx              # imm = 0x10A
	jmp	.LBB0_146
.LBB0_49:
	movq	yytext(%rip), %rax
	movb	(%rax), %al
	movb	%al, yylval+8(%rip)
	movl	$267, %ebx              # imm = 0x10B
	jmp	.LBB0_146
.LBB0_50:
	incl	line_no(%rip)
	movl	$257, %ebx              # imm = 0x101
	jmp	.LBB0_146
.LBB0_62:
	movq	yytext(%rip), %rdi
	callq	strcopyof
	movq	%rax, yylval(%rip)
	movl	$262, %ebx              # imm = 0x106
	jmp	.LBB0_146
.LBB0_63:
	movq	yytext(%rip), %rdi
	callq	strcopyof
	movq	%rax, yylval(%rip)
	xorl	%eax, %eax
	movq	yytext(%rip), %rcx
	jmp	.LBB0_64
	.p2align	4, 0x90
.LBB0_69:                               #   in Loop: Header=BB0_64 Depth=1
	xorl	%esi, %esi
	cmpb	$34, %dl
	sete	%sil
	addl	%esi, %eax
	incq	%rcx
.LBB0_64:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	cmpb	$10, %dl
	je	.LBB0_68
# BB#65:                                #   in Loop: Header=BB0_64 Depth=1
	testb	%dl, %dl
	jne	.LBB0_69
	jmp	.LBB0_66
	.p2align	4, 0x90
.LBB0_68:                               #   in Loop: Header=BB0_64 Depth=1
	incl	line_no(%rip)
	movzbl	(%rcx), %edx
	jmp	.LBB0_69
.LBB0_70:
	movq	yytext(%rip), %rbx
	movq	%rbx, %rdi
	callq	strlen
	shlq	$32, %rax
	addq	%r14, %rax
	sarq	$32, %rax
	cmpb	$46, (%rbx,%rax)
	jne	.LBB0_72
# BB#71:
	movb	$0, (%rbx,%rax)
	movq	yytext(%rip), %rbx
.LBB0_72:
	movq	%rbx, %rax
	jmp	.LBB0_73
	.p2align	4, 0x90
.LBB0_147:                              #   in Loop: Header=BB0_73 Depth=1
	incq	%rax
.LBB0_73:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	cmpb	$48, %cl
	je	.LBB0_147
# BB#74:
	testb	%cl, %cl
	jne	.LBB0_76
# BB#75:
	decq	%rax
	jmp	.LBB0_76
.LBB0_88:
	xorl	%ebx, %ebx
	jmp	.LBB0_146
.LBB0_66:
	movl	$261, %ebx              # imm = 0x105
	cmpl	$2, %eax
	je	.LBB0_146
# BB#67:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB0_146
.LBB0_79:                               #   in Loop: Header=BB0_76 Depth=1
	movb	%cl, (%rbx)
	incq	%rbx
.LBB0_76:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_77 Depth 2
	incq	%rax
	jmp	.LBB0_77
	.p2align	4, 0x90
.LBB0_148:                              #   in Loop: Header=BB0_77 Depth=2
	incl	line_no(%rip)
	addq	$2, %rax
.LBB0_77:                               #   Parent Loop BB0_76 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax), %ecx
	cmpb	$92, %cl
	je	.LBB0_148
# BB#78:                                #   in Loop: Header=BB0_76 Depth=1
	testb	%cl, %cl
	jne	.LBB0_79
# BB#80:
	movb	$0, (%rbx)
	movq	yytext(%rip), %rdi
	callq	strcopyof
	movq	%rax, yylval(%rip)
	movl	$263, %ebx              # imm = 0x107
.LBB0_146:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_140:
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	yylex, .Lfunc_end0-yylex
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_24
	.quad	.LBB0_141
	.quad	.LBB0_142
	.quad	.LBB0_143
	.quad	.LBB0_144
	.quad	.LBB0_145
	.quad	.LBB0_25
	.quad	.LBB0_26
	.quad	.LBB0_27
	.quad	.LBB0_28
	.quad	.LBB0_29
	.quad	.LBB0_30
	.quad	.LBB0_31
	.quad	.LBB0_32
	.quad	.LBB0_33
	.quad	.LBB0_34
	.quad	.LBB0_35
	.quad	.LBB0_36
	.quad	.LBB0_37
	.quad	.LBB0_38
	.quad	.LBB0_39
	.quad	.LBB0_40
	.quad	.LBB0_41
	.quad	.LBB0_42
	.quad	.LBB0_43
	.quad	.LBB0_44
	.quad	.LBB0_45
	.quad	.LBB0_46
	.quad	.LBB0_47
	.quad	.LBB0_48
	.quad	.LBB0_49
	.quad	.LBB0_50
	.quad	.LBB0_51
	.quad	.LBB0_12
	.quad	.LBB0_53
	.quad	.LBB0_62
	.quad	.LBB0_63
	.quad	.LBB0_70
	.quad	.LBB0_81
	.quad	.LBB0_87
	.quad	.LBB0_89
	.quad	.LBB0_88

	.text
	.globl	yy_init_buffer
	.p2align	4, 0x90
	.type	yy_init_buffer,@function
yy_init_buffer:                         # @yy_init_buffer
	.cfi_startproc
# BB#0:
	movq	%rsi, (%rdi)
	movq	8(%rdi), %rax
	movb	$10, (%rax)
	movl	$1, 28(%rdi)
	movq	8(%rdi), %rax
	movb	$0, 1(%rax)
	movq	8(%rdi), %rax
	movb	$0, 2(%rax)
	movq	8(%rdi), %rax
	incq	%rax
	movq	%rax, 16(%rdi)
	movl	$0, 32(%rdi)
	retq
.Lfunc_end1:
	.size	yy_init_buffer, .Lfunc_end1-yy_init_buffer
	.cfi_endproc

	.globl	yy_create_buffer
	.p2align	4, 0x90
	.type	yy_create_buffer,@function
yy_create_buffer:                       # @yy_create_buffer
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_3
# BB#1:
	movl	%r15d, 24(%rbx)
	addl	$2, %r15d
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB2_3
# BB#2:
	movq	%r14, (%rbx)
	movb	$10, (%rax)
	movl	$1, 28(%rbx)
	movq	%rax, %rcx
	incq	%rcx
	movb	$0, 1(%rax)
	movb	$0, 2(%rax)
	movq	%rcx, 16(%rbx)
	movl	$0, 32(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB2_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	yy_create_buffer, .Lfunc_end2-yy_create_buffer
	.cfi_endproc

	.globl	yy_load_buffer_state
	.p2align	4, 0x90
	.type	yy_load_buffer_state,@function
yy_load_buffer_state:                   # @yy_load_buffer_state
	.cfi_startproc
# BB#0:
	movq	yy_current_buffer(%rip), %rax
	movl	28(%rax), %ecx
	movl	%ecx, yy_n_chars(%rip)
	movq	16(%rax), %rcx
	movq	%rcx, yy_c_buf_p(%rip)
	movq	%rcx, yytext(%rip)
	movq	(%rax), %rax
	movq	%rax, yyin(%rip)
	movb	(%rcx), %al
	movb	%al, yy_hold_char(%rip)
	retq
.Lfunc_end3:
	.size	yy_load_buffer_state, .Lfunc_end3-yy_load_buffer_state
	.cfi_endproc

	.p2align	4, 0x90
	.type	input,@function
input:                                  # @input
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	movq	yy_c_buf_p(%rip), %rcx
	movb	yy_hold_char(%rip), %al
	movb	%al, (%rcx)
	testb	%al, %al
	jne	.LBB4_13
# BB#1:                                 # %.lr.ph.preheader
	movq	yy_current_buffer(%rip), %rax
	movl	yy_n_chars(%rip), %edx
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rdx
	addq	8(%rax), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB4_3
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	%rcx, yytext(%rip)
	incq	%rcx
	movq	%rcx, yy_c_buf_p(%rip)
	callq	yy_get_next_buffer
	testl	%eax, %eax
	je	.LBB4_10
# BB#5:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpl	$1, %eax
	jne	.LBB4_6
# BB#8:                                 #   in Loop: Header=BB4_2 Depth=1
	callq	open_new_file
	testl	%eax, %eax
	je	.LBB4_9
# BB#12:                                # %tailrecurse
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	yy_current_buffer(%rip), %rax
	movq	yyin(%rip), %rcx
	movq	%rcx, (%rax)
	movq	8(%rax), %rcx
	movb	$10, (%rcx)
	movl	$1, 28(%rax)
	movq	8(%rax), %rcx
	movb	$0, 1(%rcx)
	movq	8(%rax), %rcx
	movb	$0, 2(%rcx)
	movq	8(%rax), %rsi
	leaq	1(%rsi), %rcx
	movq	%rcx, 16(%rax)
	movl	$0, 32(%rax)
	movl	28(%rax), %edx
	movl	%edx, yy_n_chars(%rip)
	movq	%rcx, yy_c_buf_p(%rip)
	movq	%rcx, yytext(%rip)
	movq	(%rax), %rdi
	movq	%rdi, yyin(%rip)
	movzbl	1(%rsi), %esi
	movb	%sil, yy_hold_char(%rip)
	testb	%sil, %sil
	je	.LBB4_2
	jmp	.LBB4_13
.LBB4_6:
	cmpl	$2, %eax
	je	.LBB4_11
# BB#7:                                 # %..loopexit.loopexit_crit_edge
	movq	yy_c_buf_p(%rip), %rcx
	jmp	.LBB4_13
.LBB4_10:
	movq	yytext(%rip), %rcx
	movq	%rcx, yy_c_buf_p(%rip)
.LBB4_13:                               # %.loopexit
	movsbl	(%rcx), %eax
	leaq	1(%rcx), %rdx
	movq	%rdx, yy_c_buf_p(%rip)
	movb	1(%rcx), %cl
	movb	%cl, yy_hold_char(%rip)
	popq	%rcx
	retq
.LBB4_3:
	movb	$0, (%rcx)
	jmp	.LBB4_13
.LBB4_9:
	movq	yytext(%rip), %rax
	movq	%rax, yy_c_buf_p(%rip)
	movl	$-1, %eax
	popq	%rcx
	retq
.LBB4_11:
	movq	stderr(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$32, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	input, .Lfunc_end4-input
	.cfi_endproc

	.p2align	4, 0x90
	.type	yy_get_next_buffer,@function
yy_get_next_buffer:                     # @yy_get_next_buffer
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	yy_current_buffer(%rip), %r9
	movq	8(%r9), %rdi
	movq	yy_c_buf_p(%rip), %r14
	movslq	yy_n_chars(%rip), %rax
	leaq	1(%rdi,%rax), %rax
	cmpq	%rax, %r14
	ja	.LBB5_1
# BB#3:
	movq	yytext(%rip), %rbx
	subq	%rbx, %r14
	testl	%r14d, %r14d
	jle	.LBB5_18
# BB#4:                                 # %.lr.ph.preheader
	leaq	-1(%rbx), %rax
	leal	-1(%r14), %ecx
	incq	%rcx
	cmpq	$32, %rcx
	jb	.LBB5_5
# BB#6:                                 # %min.iters.checked
	movl	%r14d, %r8d
	andl	$31, %r8d
	subq	%r8, %rcx
	je	.LBB5_5
# BB#7:                                 # %vector.memcheck
	movl	$4294967295, %edx       # imm = 0xFFFFFFFF
	addl	%r14d, %edx
	leaq	(%rbx,%rdx), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB5_9
# BB#8:                                 # %vector.memcheck
	leaq	1(%rdi,%rdx), %rdx
	cmpq	%rdx, %rax
	jae	.LBB5_9
.LBB5_5:
	xorl	%ecx, %ecx
	movq	%rdi, %rsi
.LBB5_12:                               # %.lr.ph.preheader50
	movl	%r14d, %ebx
	subl	%ecx, %ebx
	leal	-1(%r14), %edi
	subl	%ecx, %edi
	andl	$7, %ebx
	je	.LBB5_15
# BB#13:                                # %.lr.ph.prol.preheader
	negl	%ebx
	.p2align	4, 0x90
.LBB5_14:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	incq	%rax
	movb	%dl, (%rsi)
	incq	%rsi
	incl	%ecx
	incl	%ebx
	jne	.LBB5_14
.LBB5_15:                               # %.lr.ph.prol.loopexit
	cmpl	$7, %edi
	jb	.LBB5_18
# BB#16:                                # %.lr.ph.preheader50.new
	movl	%r14d, %edi
	subl	%ecx, %edi
	.p2align	4, 0x90
.LBB5_17:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	movb	%cl, (%rsi)
	movzbl	1(%rax), %ecx
	movb	%cl, 1(%rsi)
	movzbl	2(%rax), %ecx
	movb	%cl, 2(%rsi)
	movzbl	3(%rax), %ecx
	movb	%cl, 3(%rsi)
	movzbl	4(%rax), %ecx
	movb	%cl, 4(%rsi)
	movzbl	5(%rax), %ecx
	movb	%cl, 5(%rsi)
	movzbl	6(%rax), %ecx
	movb	%cl, 6(%rsi)
	movzbl	7(%rax), %ecx
	movb	%cl, 7(%rsi)
	addq	$8, %rax
	addq	$8, %rsi
	addl	$-8, %edi
	jne	.LBB5_17
.LBB5_18:                               # %._crit_edge
	cmpl	$0, 32(%r9)
	je	.LBB5_20
# BB#19:                                # %.thread
	movl	$0, yy_n_chars(%rip)
.LBB5_28:                               # %._crit_edge35
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	cmpl	$1, %r14d
	sete	%cl
	setne	%al
	incl	%eax
	incl	%ecx
	movl	%ecx, 32(%r9)
	xorl	%ecx, %ecx
	jmp	.LBB5_29
.LBB5_20:
	movl	24(%r9), %ecx
	subl	%r14d, %ecx
	leal	-1(%rcx), %edx
	movl	$8192, %eax             # imm = 0x2000
	cmpl	$8193, %edx             # imm = 0x2001
	jge	.LBB5_21
# BB#25:
	cmpl	$1, %ecx
	movl	%edx, %eax
	jle	.LBB5_26
.LBB5_21:                               # %.preheader
	movslq	%r14d, %r15
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB5_22:                               # =>This Inner Loop Header: Depth=1
	movq	yyin(%rip), %rdi
	callq	fileno
	movq	yy_current_buffer(%rip), %rcx
	movq	8(%rcx), %rsi
	addq	%r15, %rsi
	movl	%eax, %edi
	movq	%rbx, %rdx
	callq	read
	movq	%rax, %rcx
	movl	%ecx, yy_n_chars(%rip)
	testl	%ecx, %ecx
	jns	.LBB5_27
# BB#23:                                #   in Loop: Header=BB5_22 Depth=1
	callq	__errno_location
	cmpl	$4, (%rax)
	je	.LBB5_22
# BB#24:
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$29, %esi
.LBB5_2:
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %edi
	callq	exit
.LBB5_27:
	xorl	%eax, %eax
	movq	yy_current_buffer(%rip), %r9
	testl	%ecx, %ecx
	je	.LBB5_28
.LBB5_29:                               # %._crit_edge36
	addl	%r14d, %ecx
	movl	%ecx, yy_n_chars(%rip)
	movq	8(%r9), %rdx
	movslq	%ecx, %rcx
	movb	$0, (%rdx,%rcx)
	movq	8(%r9), %rcx
	movslq	yy_n_chars(%rip), %rdx
	movb	$0, 1(%rcx,%rdx)
	movq	yy_current_buffer(%rip), %rcx
	movq	8(%rcx), %rcx
	incq	%rcx
	movq	%rcx, yytext(%rip)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB5_9:                                # %vector.body.preheader
	addq	%rcx, %rax
	addq	$15, %rbx
	leaq	(%rdi,%rcx), %rsi
	addq	$16, %rdi
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB5_10:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rbx
	addq	$32, %rdi
	addq	$-32, %rdx
	jne	.LBB5_10
# BB#11:                                # %middle.block
	testq	%r8, %r8
	jne	.LBB5_12
	jmp	.LBB5_18
.LBB5_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$55, %esi
	jmp	.LBB5_2
.LBB5_26:
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$43, %esi
	jmp	.LBB5_2
.Lfunc_end5:
	.size	yy_get_next_buffer, .Lfunc_end5-yy_get_next_buffer
	.cfi_endproc

	.globl	yywrap
	.p2align	4, 0x90
	.type	yywrap,@function
yywrap:                                 # @yywrap
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 16
	callq	open_new_file
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	movl	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end6:
	.size	yywrap, .Lfunc_end6-yywrap
	.cfi_endproc

	.globl	yyrestart
	.p2align	4, 0x90
	.type	yyrestart,@function
yyrestart:                              # @yyrestart
	.cfi_startproc
# BB#0:
	movq	yy_current_buffer(%rip), %rax
	movq	%rdi, (%rax)
	movq	8(%rax), %rcx
	movb	$10, (%rcx)
	movl	$1, 28(%rax)
	movq	8(%rax), %rcx
	movb	$0, 1(%rcx)
	movq	8(%rax), %rcx
	movb	$0, 2(%rcx)
	movq	8(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movl	$0, 32(%rax)
	movl	28(%rax), %esi
	movl	%esi, yy_n_chars(%rip)
	movq	%rdx, yy_c_buf_p(%rip)
	movq	%rdx, yytext(%rip)
	movq	(%rax), %rax
	movq	%rax, yyin(%rip)
	movb	1(%rcx), %al
	movb	%al, yy_hold_char(%rip)
	retq
.Lfunc_end7:
	.size	yyrestart, .Lfunc_end7-yyrestart
	.cfi_endproc

	.globl	yy_switch_to_buffer
	.p2align	4, 0x90
	.type	yy_switch_to_buffer,@function
yy_switch_to_buffer:                    # @yy_switch_to_buffer
	.cfi_startproc
# BB#0:
	movq	yy_current_buffer(%rip), %rax
	cmpq	%rdi, %rax
	je	.LBB8_4
# BB#1:
	testq	%rax, %rax
	je	.LBB8_3
# BB#2:
	movb	yy_hold_char(%rip), %cl
	movq	yy_c_buf_p(%rip), %rdx
	movb	%cl, (%rdx)
	movq	%rdx, 16(%rax)
	movl	yy_n_chars(%rip), %ecx
	movl	%ecx, 28(%rax)
.LBB8_3:
	movq	%rdi, yy_current_buffer(%rip)
	movl	28(%rdi), %eax
	movl	%eax, yy_n_chars(%rip)
	movq	16(%rdi), %rax
	movq	%rax, yy_c_buf_p(%rip)
	movq	%rax, yytext(%rip)
	movq	(%rdi), %rcx
	movq	%rcx, yyin(%rip)
	movb	(%rax), %al
	movb	%al, yy_hold_char(%rip)
	movb	$1, yy_did_buffer_switch_on_eof(%rip)
.LBB8_4:
	retq
.Lfunc_end8:
	.size	yy_switch_to_buffer, .Lfunc_end8-yy_switch_to_buffer
	.cfi_endproc

	.globl	yy_delete_buffer
	.p2align	4, 0x90
	.type	yy_delete_buffer,@function
yy_delete_buffer:                       # @yy_delete_buffer
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpq	%rbx, yy_current_buffer(%rip)
	jne	.LBB9_2
# BB#1:
	movq	$0, yy_current_buffer(%rip)
.LBB9_2:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end9:
	.size	yy_delete_buffer, .Lfunc_end9-yy_delete_buffer
	.cfi_endproc

	.type	yyin,@object            # @yyin
	.bss
	.globl	yyin
	.p2align	3
yyin:
	.quad	0
	.size	yyin, 8

	.type	yyout,@object           # @yyout
	.globl	yyout
	.p2align	3
yyout:
	.quad	0
	.size	yyout, 8

	.type	yy_init,@object         # @yy_init
	.local	yy_init
	.comm	yy_init,1,4
	.type	yy_start,@object        # @yy_start
	.local	yy_start
	.comm	yy_start,1,4
	.type	yy_current_buffer,@object # @yy_current_buffer
	.local	yy_current_buffer
	.comm	yy_current_buffer,8,8
	.type	yy_c_buf_p,@object      # @yy_c_buf_p
	.local	yy_c_buf_p
	.comm	yy_c_buf_p,8,8
	.type	yy_hold_char,@object    # @yy_hold_char
	.local	yy_hold_char
	.comm	yy_hold_char,1,1
	.type	yy_ec,@object           # @yy_ec
	.section	.rodata,"a",@progbits
	.p2align	4
yy_ec:
	.ascii	"\000\001\001\001\001\001\001\001\001\002\003\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\002\004\005\001\001\006\007\001\b\t\n\013\f\r\016\017\020\020\020\020\020\020\020\020\020\020\001\021\022\023\024\001\001\025\025\025\025\025\025\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\026\027\030\031\032\001\033\034\035\036\037 !\"#$%&'()*+,-./$0$1$234\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.size	yy_ec, 256

	.type	yy_accept,@object       # @yy_accept
	.p2align	4
yy_accept:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	40                      # 0x28
	.short	38                      # 0x26
	.short	33                      # 0x21
	.short	31                      # 0x1f
	.short	25                      # 0x19
	.short	38                      # 0x26
	.short	26                      # 0x1a
	.short	38                      # 0x26
	.short	22                      # 0x16
	.short	26                      # 0x1a
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	38                      # 0x26
	.short	26                      # 0x1a
	.short	37                      # 0x25
	.short	29                      # 0x1d
	.short	27                      # 0x1b
	.short	29                      # 0x1d
	.short	38                      # 0x26
	.short	22                      # 0x16
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	38                      # 0x26
	.short	33                      # 0x21
	.short	29                      # 0x1d
	.short	0                       # 0x0
	.short	36                      # 0x24
	.short	27                      # 0x1b
	.short	23                      # 0x17
	.short	30                      # 0x1e
	.short	37                      # 0x25
	.short	0                       # 0x0
	.short	34                      # 0x22
	.short	37                      # 0x25
	.short	37                      # 0x25
	.short	0                       # 0x0
	.short	28                      # 0x1c
	.short	32                      # 0x20
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	7                       # 0x7
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	24                      # 0x18
	.short	37                      # 0x25
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	37                      # 0x25
	.short	0                       # 0x0
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	6                       # 0x6
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	13                      # 0xd
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	14                      # 0xe
	.short	16                      # 0x10
	.short	35                      # 0x23
	.short	17                      # 0x11
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	3                       # 0x3
	.short	15                      # 0xf
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	9                       # 0x9
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	2                       # 0x2
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	11                      # 0xb
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	12                      # 0xc
	.short	20                      # 0x14
	.short	35                      # 0x23
	.short	10                      # 0xa
	.short	35                      # 0x23
	.short	8                       # 0x8
	.short	35                      # 0x23
	.short	1                       # 0x1
	.short	4                       # 0x4
	.short	21                      # 0x15
	.short	5                       # 0x5
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	19                      # 0x13
	.short	18                      # 0x12
	.short	0                       # 0x0
	.size	yy_accept, 288

	.type	yy_last_accepting_state,@object # @yy_last_accepting_state
	.local	yy_last_accepting_state
	.comm	yy_last_accepting_state,4,4
	.type	yy_last_accepting_cpos,@object # @yy_last_accepting_cpos
	.local	yy_last_accepting_cpos
	.comm	yy_last_accepting_cpos,8,8
	.type	yy_chk,@object          # @yy_chk
	.p2align	4
yy_chk:
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	31                      # 0x1f
	.short	16                      # 0x10
	.short	15                      # 0xf
	.short	145                     # 0x91
	.short	31                      # 0x1f
	.short	14                      # 0xe
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	38                      # 0x26
	.short	15                      # 0xf
	.short	16                      # 0x10
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	17                      # 0x11
	.short	37                      # 0x25
	.short	38                      # 0x26
	.short	19                      # 0x13
	.short	19                      # 0x13
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	32                      # 0x20
	.short	140                     # 0x8c
	.short	71                      # 0x47
	.short	19                      # 0x13
	.short	32                      # 0x20
	.short	47                      # 0x2f
	.short	37                      # 0x25
	.short	139                     # 0x8b
	.short	32                      # 0x20
	.short	19                      # 0x13
	.short	47                      # 0x2f
	.short	50                      # 0x32
	.short	47                      # 0x2f
	.short	51                      # 0x33
	.short	138                     # 0x8a
	.short	51                      # 0x33
	.short	50                      # 0x32
	.short	133                     # 0x85
	.short	50                      # 0x32
	.short	77                      # 0x4d
	.short	51                      # 0x33
	.short	71                      # 0x47
	.short	51                      # 0x33
	.short	79                      # 0x4f
	.short	77                      # 0x4d
	.short	131                     # 0x83
	.short	77                      # 0x4d
	.short	80                      # 0x50
	.short	79                      # 0x4f
	.short	129                     # 0x81
	.short	79                      # 0x4f
	.short	126                     # 0x7e
	.short	80                      # 0x50
	.short	125                     # 0x7d
	.short	80                      # 0x50
	.short	144                     # 0x90
	.short	144                     # 0x90
	.short	123                     # 0x7b
	.short	122                     # 0x7a
	.short	120                     # 0x78
	.short	119                     # 0x77
	.short	117                     # 0x75
	.short	116                     # 0x74
	.short	113                     # 0x71
	.short	112                     # 0x70
	.short	111                     # 0x6f
	.short	110                     # 0x6e
	.short	108                     # 0x6c
	.short	105                     # 0x69
	.short	104                     # 0x68
	.short	103                     # 0x67
	.short	101                     # 0x65
	.short	100                     # 0x64
	.short	99                      # 0x63
	.short	98                      # 0x62
	.short	97                      # 0x61
	.short	96                      # 0x60
	.short	95                      # 0x5f
	.short	94                      # 0x5e
	.short	93                      # 0x5d
	.short	92                      # 0x5c
	.short	91                      # 0x5b
	.short	90                      # 0x5a
	.short	89                      # 0x59
	.short	88                      # 0x58
	.short	86                      # 0x56
	.short	85                      # 0x55
	.short	84                      # 0x54
	.short	83                      # 0x53
	.short	82                      # 0x52
	.short	81                      # 0x51
	.short	78                      # 0x4e
	.short	75                      # 0x4b
	.short	74                      # 0x4a
	.short	73                      # 0x49
	.short	72                      # 0x48
	.short	70                      # 0x46
	.short	69                      # 0x45
	.short	68                      # 0x44
	.short	67                      # 0x43
	.short	66                      # 0x42
	.short	65                      # 0x41
	.short	63                      # 0x3f
	.short	62                      # 0x3e
	.short	61                      # 0x3d
	.short	60                      # 0x3c
	.short	59                      # 0x3b
	.short	58                      # 0x3a
	.short	57                      # 0x39
	.short	56                      # 0x38
	.short	52                      # 0x34
	.short	48                      # 0x30
	.short	42                      # 0x2a
	.short	40                      # 0x28
	.short	39                      # 0x27
	.short	36                      # 0x24
	.short	35                      # 0x23
	.short	34                      # 0x22
	.short	33                      # 0x21
	.short	30                      # 0x1e
	.short	28                      # 0x1c
	.short	27                      # 0x1b
	.short	26                      # 0x1a
	.short	25                      # 0x19
	.short	24                      # 0x18
	.short	23                      # 0x17
	.short	22                      # 0x16
	.short	21                      # 0x15
	.short	20                      # 0x14
	.short	18                      # 0x12
	.short	12                      # 0xc
	.short	10                      # 0xa
	.short	9                       # 0x9
	.short	8                       # 0x8
	.short	7                       # 0x7
	.short	5                       # 0x5
	.short	3                       # 0x3
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.size	yy_chk, 494

	.type	yy_base,@object         # @yy_base
	.p2align	4
yy_base:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	193                     # 0xc1
	.short	194                     # 0xc2
	.short	190                     # 0xbe
	.short	194                     # 0xc2
	.short	172                     # 0xac
	.short	185                     # 0xb9
	.short	170                     # 0xaa
	.short	181                     # 0xb5
	.short	194                     # 0xc2
	.short	168                     # 0xa8
	.short	42                      # 0x2a
	.short	41                      # 0x29
	.short	41                      # 0x29
	.short	46                      # 0x2e
	.short	52                      # 0x34
	.short	167                     # 0xa7
	.short	61                      # 0x3d
	.short	166                     # 0xa6
	.short	181                     # 0xb5
	.short	164                     # 0xa4
	.short	135                     # 0x87
	.short	137                     # 0x89
	.short	139                     # 0x8b
	.short	148                     # 0x94
	.short	140                     # 0x8c
	.short	136                     # 0x88
	.short	0                       # 0x0
	.short	149                     # 0x95
	.short	27                      # 0x1b
	.short	50                      # 0x32
	.short	147                     # 0x93
	.short	130                     # 0x82
	.short	126                     # 0x7e
	.short	141                     # 0x8d
	.short	40                      # 0x28
	.short	36                      # 0x24
	.short	120                     # 0x78
	.short	168                     # 0xa8
	.short	194                     # 0xc2
	.short	164                     # 0xa4
	.short	194                     # 0xc2
	.short	194                     # 0xc2
	.short	194                     # 0xc2
	.short	194                     # 0xc2
	.short	66                      # 0x42
	.short	165                     # 0xa5
	.short	194                     # 0xc2
	.short	72                      # 0x48
	.short	76                      # 0x4c
	.short	164                     # 0xa4
	.short	194                     # 0xc2
	.short	194                     # 0xc2
	.short	0                       # 0x0
	.short	120                     # 0x78
	.short	134                     # 0x86
	.short	124                     # 0x7c
	.short	131                     # 0x83
	.short	117                     # 0x75
	.short	117                     # 0x75
	.short	122                     # 0x7a
	.short	132                     # 0x84
	.short	0                       # 0x0
	.short	113                     # 0x71
	.short	117                     # 0x75
	.short	117                     # 0x75
	.short	128                     # 0x80
	.short	119                     # 0x77
	.short	118                     # 0x76
	.short	52                      # 0x34
	.short	125                     # 0x7d
	.short	107                     # 0x6b
	.short	106                     # 0x6a
	.short	114                     # 0x72
	.short	194                     # 0xc2
	.short	80                      # 0x50
	.short	145                     # 0x91
	.short	84                      # 0x54
	.short	88                      # 0x58
	.short	144                     # 0x90
	.short	105                     # 0x69
	.short	118                     # 0x76
	.short	98                      # 0x62
	.short	108                     # 0x6c
	.short	111                     # 0x6f
	.short	0                       # 0x0
	.short	95                      # 0x5f
	.short	95                      # 0x5f
	.short	93                      # 0x5d
	.short	105                     # 0x69
	.short	102                     # 0x66
	.short	91                      # 0x5b
	.short	95                      # 0x5f
	.short	88                      # 0x58
	.short	103                     # 0x67
	.short	85                      # 0x55
	.short	93                      # 0x5d
	.short	84                      # 0x54
	.short	85                      # 0x55
	.short	90                      # 0x5a
	.short	0                       # 0x0
	.short	90                      # 0x5a
	.short	91                      # 0x5b
	.short	85                      # 0x55
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	93                      # 0x5d
	.short	0                       # 0x0
	.short	77                      # 0x4d
	.short	76                      # 0x4c
	.short	90                      # 0x5a
	.short	74                      # 0x4a
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	75                      # 0x4b
	.short	87                      # 0x57
	.short	0                       # 0x0
	.short	90                      # 0x5a
	.short	85                      # 0x55
	.short	0                       # 0x0
	.short	75                      # 0x4b
	.short	83                      # 0x53
	.short	0                       # 0x0
	.short	76                      # 0x4c
	.short	63                      # 0x3f
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	66                      # 0x42
	.short	0                       # 0x0
	.short	62                      # 0x3e
	.short	0                       # 0x0
	.short	47                      # 0x2f
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	45                      # 0x2d
	.short	53                      # 0x35
	.short	29                      # 0x1d
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	194                     # 0xc2
	.short	111                     # 0x6f
	.short	56                      # 0x38
	.size	yy_base, 292

	.type	yy_def,@object          # @yy_def
	.p2align	4
yy_def:
	.short	0                       # 0x0
	.short	143                     # 0x8f
	.short	1                       # 0x1
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	144                     # 0x90
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	144                     # 0x90
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	0                       # 0x0
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.size	yy_def, 292

	.type	yy_meta,@object         # @yy_meta
	.p2align	4
yy_meta:
	.ascii	"\000\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\002\001\001\001\001\001\001\001\001\001\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\001\001\001"
	.size	yy_meta, 53

	.type	yy_nxt,@object          # @yy_nxt
	.p2align	4
yy_nxt:
	.short	0                       # 0x0
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	11                      # 0xb
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	16                      # 0x10
	.short	17                      # 0x11
	.short	11                      # 0xb
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	17                      # 0x11
	.short	11                      # 0xb
	.short	21                      # 0x15
	.short	11                      # 0xb
	.short	22                      # 0x16
	.short	4                       # 0x4
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	29                      # 0x1d
	.short	30                      # 0x1e
	.short	31                      # 0x1f
	.short	29                      # 0x1d
	.short	29                      # 0x1d
	.short	32                      # 0x20
	.short	29                      # 0x1d
	.short	29                      # 0x1d
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	37                      # 0x25
	.short	29                      # 0x1d
	.short	29                      # 0x1d
	.short	38                      # 0x26
	.short	29                      # 0x1d
	.short	11                      # 0xb
	.short	39                      # 0x27
	.short	11                      # 0xb
	.short	46                      # 0x2e
	.short	46                      # 0x2e
	.short	63                      # 0x3f
	.short	49                      # 0x31
	.short	47                      # 0x2f
	.short	55                      # 0x37
	.short	64                      # 0x40
	.short	44                      # 0x2c
	.short	44                      # 0x2c
	.short	47                      # 0x2f
	.short	74                      # 0x4a
	.short	48                      # 0x30
	.short	44                      # 0x2c
	.short	50                      # 0x32
	.short	53                      # 0x35
	.short	51                      # 0x33
	.short	72                      # 0x48
	.short	75                      # 0x4b
	.short	53                      # 0x35
	.short	53                      # 0x35
	.short	51                      # 0x33
	.short	53                      # 0x35
	.short	52                      # 0x34
	.short	53                      # 0x35
	.short	65                      # 0x41
	.short	142                     # 0x8e
	.short	96                      # 0x60
	.short	41                      # 0x29
	.short	66                      # 0x42
	.short	77                      # 0x4d
	.short	73                      # 0x49
	.short	141                     # 0x8d
	.short	67                      # 0x43
	.short	53                      # 0x35
	.short	77                      # 0x4d
	.short	80                      # 0x50
	.short	78                      # 0x4e
	.short	50                      # 0x32
	.short	140                     # 0x8c
	.short	51                      # 0x33
	.short	80                      # 0x50
	.short	139                     # 0x8b
	.short	81                      # 0x51
	.short	77                      # 0x4d
	.short	51                      # 0x33
	.short	97                      # 0x61
	.short	52                      # 0x34
	.short	47                      # 0x2f
	.short	77                      # 0x4d
	.short	138                     # 0x8a
	.short	78                      # 0x4e
	.short	80                      # 0x50
	.short	47                      # 0x2f
	.short	137                     # 0x89
	.short	48                      # 0x30
	.short	136                     # 0x88
	.short	80                      # 0x50
	.short	135                     # 0x87
	.short	81                      # 0x51
	.short	42                      # 0x2a
	.short	42                      # 0x2a
	.short	134                     # 0x86
	.short	133                     # 0x85
	.short	132                     # 0x84
	.short	131                     # 0x83
	.short	130                     # 0x82
	.short	129                     # 0x81
	.short	128                     # 0x80
	.short	127                     # 0x7f
	.short	126                     # 0x7e
	.short	125                     # 0x7d
	.short	124                     # 0x7c
	.short	123                     # 0x7b
	.short	122                     # 0x7a
	.short	121                     # 0x79
	.short	120                     # 0x78
	.short	119                     # 0x77
	.short	118                     # 0x76
	.short	117                     # 0x75
	.short	116                     # 0x74
	.short	115                     # 0x73
	.short	114                     # 0x72
	.short	113                     # 0x71
	.short	112                     # 0x70
	.short	111                     # 0x6f
	.short	110                     # 0x6e
	.short	109                     # 0x6d
	.short	108                     # 0x6c
	.short	107                     # 0x6b
	.short	106                     # 0x6a
	.short	105                     # 0x69
	.short	104                     # 0x68
	.short	103                     # 0x67
	.short	102                     # 0x66
	.short	80                      # 0x50
	.short	77                      # 0x4d
	.short	101                     # 0x65
	.short	100                     # 0x64
	.short	99                      # 0x63
	.short	98                      # 0x62
	.short	95                      # 0x5f
	.short	94                      # 0x5e
	.short	93                      # 0x5d
	.short	92                      # 0x5c
	.short	91                      # 0x5b
	.short	90                      # 0x5a
	.short	89                      # 0x59
	.short	88                      # 0x58
	.short	87                      # 0x57
	.short	86                      # 0x56
	.short	85                      # 0x55
	.short	84                      # 0x54
	.short	83                      # 0x53
	.short	82                      # 0x52
	.short	51                      # 0x33
	.short	79                      # 0x4f
	.short	43                      # 0x2b
	.short	40                      # 0x28
	.short	76                      # 0x4c
	.short	71                      # 0x47
	.short	70                      # 0x46
	.short	69                      # 0x45
	.short	68                      # 0x44
	.short	62                      # 0x3e
	.short	61                      # 0x3d
	.short	60                      # 0x3c
	.short	59                      # 0x3b
	.short	58                      # 0x3a
	.short	57                      # 0x39
	.short	56                      # 0x38
	.short	44                      # 0x2c
	.short	54                      # 0x36
	.short	41                      # 0x29
	.short	41                      # 0x29
	.short	44                      # 0x2c
	.short	45                      # 0x2d
	.short	44                      # 0x2c
	.short	43                      # 0x2b
	.short	41                      # 0x29
	.short	40                      # 0x28
	.short	143                     # 0x8f
	.short	3                       # 0x3
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.short	143                     # 0x8f
	.size	yy_nxt, 494

	.type	yytext,@object          # @yytext
	.comm	yytext,8,8
	.type	yyleng,@object          # @yyleng
	.comm	yyleng,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"EOF encountered in a comment.\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"NUL character in string."
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"illegal character: ^%c"
	.size	.L.str.2, 23

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"illegal character: \\%3d"
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"illegal character: %s"
	.size	.L.str.4, 22

	.type	yy_n_chars,@object      # @yy_n_chars
	.local	yy_n_chars
	.comm	yy_n_chars,4,4
	.type	yy_did_buffer_switch_on_eof,@object # @yy_did_buffer_switch_on_eof
	.local	yy_did_buffer_switch_on_eof
	.comm	yy_did_buffer_switch_on_eof,1,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"fatal flex scanner internal error--no action found"
	.size	.L.str.5, 51

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"out of dynamic memory in yy_create_buffer()"
	.size	.L.str.6, 44

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"fatal flex scanner internal error--end of buffer missed"
	.size	.L.str.7, 56

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"fatal error - scanner input buffer overflow"
	.size	.L.str.8, 44

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"read() in flex scanner failed"
	.size	.L.str.9, 30

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"unexpected last match in input()"
	.size	.L.str.10, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
