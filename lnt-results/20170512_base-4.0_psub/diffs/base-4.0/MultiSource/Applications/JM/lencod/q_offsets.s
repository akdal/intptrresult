	.text
	.file	"q_offsets.bc"
	.globl	allocate_QOffsets
	.p2align	4, 0x90
	.type	allocate_QOffsets,@function
allocate_QOffsets:                      # @allocate_QOffsets
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	input(%rip), %rax
	movl	5256(%rax), %ecx
	movl	5260(%rax), %eax
	leal	(%rcx,%rcx,2), %ecx
	leal	3(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	imulq	$715827883, %rcx, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rdx
	shrq	$32, %rdx
	shrq	$63, %rcx
	leal	1(%rdx,%rcx), %ecx
	leal	(%rax,%rax,2), %eax
	leal	3(%rax,%rax), %eax
	cltq
	imulq	$715827883, %rax, %rax  # imm = 0x2AAAAAAB
	movq	%rax, %rdx
	shrq	$32, %rdx
	shrq	$63, %rax
	leal	1(%rdx,%rax), %ebx
	cmpl	%ebx, %ecx
	cmovgel	%ecx, %ebx
	movl	$LevelOffset4x4Luma, %edi
	movl	$2, %esi
	movl	$4, %ecx
	movl	$4, %r8d
	movl	%ebx, %edx
	callq	get_mem4Dint
	movl	$LevelOffset4x4Chroma, %edi
	movl	$2, %esi
	movl	$2, %edx
	movl	$4, %r8d
	movl	$4, %r9d
	movl	%ebx, %ecx
	callq	get_mem5Dint
	movl	$LevelOffset8x8Luma, %edi
	movl	$2, %esi
	movl	$8, %ecx
	movl	$8, %r8d
	movl	%ebx, %edx
	callq	get_mem4Dint
	movl	$OffsetList4x4input, %edi
	movl	$15, %esi
	movl	$16, %edx
	callq	get_mem2Dshort
	movl	$OffsetList8x8input, %edi
	movl	$5, %esi
	movl	$64, %edx
	callq	get_mem2Dshort
	movl	$OffsetList4x4, %edi
	movl	$15, %esi
	movl	$16, %edx
	callq	get_mem2Dshort
	movl	$OffsetList8x8, %edi
	movl	$5, %esi
	movl	$64, %edx
	popq	%rbx
	jmp	get_mem2Dshort          # TAILCALL
.Lfunc_end0:
	.size	allocate_QOffsets, .Lfunc_end0-allocate_QOffsets
	.cfi_endproc

	.globl	free_QOffsets
	.p2align	4, 0x90
	.type	free_QOffsets,@function
free_QOffsets:                          # @free_QOffsets
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	input(%rip), %rax
	movl	5256(%rax), %ecx
	movl	5260(%rax), %eax
	leal	(%rcx,%rcx,2), %ecx
	leal	3(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	imulq	$715827883, %rcx, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rdx
	shrq	$32, %rdx
	shrq	$63, %rcx
	leal	1(%rdx,%rcx), %ecx
	leal	(%rax,%rax,2), %eax
	leal	3(%rax,%rax), %eax
	cltq
	imulq	$715827883, %rax, %rax  # imm = 0x2AAAAAAB
	movq	%rax, %rdx
	shrq	$32, %rdx
	shrq	$63, %rax
	leal	1(%rdx,%rax), %ebx
	cmpl	%ebx, %ecx
	cmovgel	%ecx, %ebx
	movq	LevelOffset4x4Luma(%rip), %rdi
	movl	$2, %esi
	movl	%ebx, %edx
	callq	free_mem4Dint
	movq	LevelOffset4x4Chroma(%rip), %rdi
	movl	$2, %esi
	movl	$2, %edx
	movl	%ebx, %ecx
	callq	free_mem5Dint
	movq	LevelOffset8x8Luma(%rip), %rdi
	movl	$2, %esi
	movl	%ebx, %edx
	callq	free_mem4Dint
	movq	OffsetList8x8(%rip), %rdi
	callq	free_mem2Dshort
	movq	OffsetList4x4(%rip), %rdi
	callq	free_mem2Dshort
	movq	OffsetList8x8input(%rip), %rdi
	callq	free_mem2Dshort
	movq	OffsetList4x4input(%rip), %rdi
	popq	%rbx
	jmp	free_mem2Dshort         # TAILCALL
.Lfunc_end1:
	.size	free_QOffsets, .Lfunc_end1-free_QOffsets
	.cfi_endproc

	.globl	CheckOffsetParameterName
	.p2align	4, 0x90
	.type	CheckOffsetParameterName,@function
CheckOffsetParameterName:               # @CheckOffsetParameterName
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 48
.Lcfi9:
	.cfi_offset %rbx, -40
.Lcfi10:
	.cfi_offset %r12, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$0, (%r15)
	movl	$OffsetType4x4, %ebx
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_8
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	cmpq	$14, %r12
	jg	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_1 Depth=1
	incq	%r12
	cmpq	$-24, %rbx
	leaq	24(%rbx), %rbx
	jne	.LBB2_1
.LBB2_4:
	movl	$1, (%r15)
	movl	$OffsetType8x8, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_9
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	incq	%rbx
	movl	$-1, %r12d
	cmpq	$4, %rbx
	jg	.LBB2_10
# BB#7:                                 #   in Loop: Header=BB2_5 Depth=1
	cmpq	$-24, %r15
	leaq	24(%r15), %r15
	jne	.LBB2_5
	jmp	.LBB2_10
.LBB2_8:                                # %.loopexit.loopexit28
	decl	%r12d
	jmp	.LBB2_10
.LBB2_9:                                # %..loopexit.loopexit_crit_edge
	movl	%ebx, %r12d
.LBB2_10:                               # %.loopexit
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	CheckOffsetParameterName, .Lfunc_end2-CheckOffsetParameterName
	.cfi_endproc

	.globl	ParseQOffsetMatrix
	.p2align	4, 0x90
	.type	ParseQOffsetMatrix,@function
ParseQOffsetMatrix:                     # @ParseQOffsetMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$8040, %rsp             # imm = 0x1F68
.Lcfi19:
	.cfi_def_cfa_offset 8096
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	testl	%esi, %esi
	jle	.LBB3_45
# BB#1:                                 # %.lr.ph134.preheader
	movslq	%esi, %rax
	addq	%rdi, %rax
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph134
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
	movsbl	(%rdi), %esi
	addl	$-9, %esi
	cmpl	$35, %esi
	ja	.LBB3_26
# BB#3:                                 # %.lr.ph134
                                        #   in Loop: Header=BB3_2 Depth=1
	jmpq	*.LJTI3_0(,%rsi,8)
.LBB3_11:                               #   in Loop: Header=BB3_2 Depth=1
	leaq	1(%rdi), %rsi
	testl	%edx, %edx
	je	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%rsi, %rdi
	cmpq	%rax, %rdi
	jb	.LBB3_2
	jmp	.LBB3_16
.LBB3_26:                               #   in Loop: Header=BB3_2 Depth=1
	testl	%ecx, %ecx
	jne	.LBB3_28
# BB#27:                                #   in Loop: Header=BB3_2 Depth=1
	movl	12(%rsp), %esi          # 4-byte Reload
	movslq	%esi, %rcx
	incl	%esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	%rdi, 32(%rsp,%rcx,8)
	movl	$-1, %ecx
.LBB3_28:                               #   in Loop: Header=BB3_2 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jb	.LBB3_2
	jmp	.LBB3_16
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movb	$0, (%rdi)
	xorl	%ecx, %ecx
	cmpq	%rax, %rdi
	jae	.LBB3_14
# BB#6:                                 # %._crit_edge138.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	incq	%rdi
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB3_7:                                # %._crit_edge138
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rdx), %rdi
	cmpq	%rax, %rdx
	jae	.LBB3_9
# BB#8:                                 # %._crit_edge138
                                        #   in Loop: Header=BB3_7 Depth=2
	cmpb	$10, (%rdx)
	movq	%rdi, %rdx
	jne	.LBB3_7
.LBB3_9:                                # %.critedge.backedge.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	decq	%rdi
	jmp	.LBB3_14
.LBB3_10:                               #   in Loop: Header=BB3_2 Depth=1
	movb	$0, (%rdi)
	incq	%rdi
	xorl	%ecx, %ecx
	jmp	.LBB3_14
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jb	.LBB3_2
	jmp	.LBB3_16
.LBB3_22:                               #   in Loop: Header=BB3_2 Depth=1
	movb	$0, (%rdi)
	incq	%rdi
	xorl	%esi, %esi
	testl	%edx, %edx
	jne	.LBB3_24
# BB#23:                                #   in Loop: Header=BB3_2 Depth=1
	movl	12(%rsp), %ebp          # 4-byte Reload
	movslq	%ebp, %rsi
	incl	%ebp
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%rdi, 32(%rsp,%rsi,8)
	notl	%ecx
	movl	%ecx, %esi
.LBB3_24:                               #   in Loop: Header=BB3_2 Depth=1
	notl	%edx
	movl	%esi, %ecx
	cmpq	%rax, %rdi
	jb	.LBB3_2
	jmp	.LBB3_16
.LBB3_25:                               #   in Loop: Header=BB3_2 Depth=1
	incq	%rdi
	xorl	%ecx, %ecx
	cmpq	%rax, %rdi
	jb	.LBB3_2
	jmp	.LBB3_16
.LBB3_13:                               #   in Loop: Header=BB3_2 Depth=1
	movb	$0, (%rdi)
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
.LBB3_14:                               # %.critedge.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	xorl	%edx, %edx
	cmpq	%rax, %rdi
	jb	.LBB3_2
.LBB3_16:                               # %.critedge._crit_edge
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	jl	.LBB3_45
# BB#17:                                # %.lr.ph.preheader
	decl	12(%rsp)                # 4-byte Folded Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_18:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_19 Depth 2
                                        #     Child Loop BB3_32 Depth 2
                                        #     Child Loop BB3_41 Depth 2
	movslq	%r14d, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	32(%rsp,%rax,8), %r15
	movl	$OffsetType4x4, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_19:                               #   Parent Loop BB3_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_20
# BB#29:                                #   in Loop: Header=BB3_19 Depth=2
	incq	%r12
	cmpq	$14, %r12
	jg	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_19 Depth=2
	cmpq	$-24, %rbx
	leaq	24(%rbx), %rbx
	jne	.LBB3_19
.LBB3_31:                               # %.preheader.preheader
                                        #   in Loop: Header=BB3_18 Depth=1
	movl	$OffsetType8x8, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_32:                               # %.preheader
                                        #   Parent Loop BB3_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_33
# BB#34:                                #   in Loop: Header=BB3_32 Depth=2
	incq	%r12
	movl	$1, %r13d
	movq	$-1, %rbp
	cmpq	$4, %r12
	jg	.LBB3_36
# BB#35:                                #   in Loop: Header=BB3_32 Depth=2
	cmpq	$-24, %rbx
	leaq	24(%rbx), %rbx
	jne	.LBB3_32
	jmp	.LBB3_36
	.p2align	4, 0x90
.LBB3_20:                               #   in Loop: Header=BB3_18 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_33:                               #   in Loop: Header=BB3_18 Depth=1
	movl	$1, %r13d
.LBB3_21:                               # %CheckOffsetParameterName.exit
                                        #   in Loop: Header=BB3_18 Depth=1
	testl	%r12d, %r12d
	movq	%r12, %rbp
	jns	.LBB3_37
.LBB3_36:                               # %CheckOffsetParameterName.exit.thread
                                        #   in Loop: Header=BB3_18 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str, %edx
	xorl	%eax, %eax
	movq	%r15, %rcx
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
	movq	%rbp, %r12
.LBB3_37:                               #   in Loop: Header=BB3_18 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp,%rax,8), %rax
	cmpb	$61, (%rax)
	jne	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_18 Depth=1
	cmpb	$0, 1(%rax)
	je	.LBB3_40
.LBB3_39:                               # %.thread
                                        #   in Loop: Header=BB3_18 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB3_40:                               #   in Loop: Header=BB3_18 Depth=1
	shlq	$32, %r12
	movq	%r12, %rax
	sarq	$29, %rax
	movq	OffsetList8x8input(%rip), %rcx
	addq	%rax, %rcx
	sarq	$30, %r12
	addq	OffsetList4x4input(%rip), %rax
	addl	$2, %r14d
	testl	%r13d, %r13d
	leaq	offset8x8_check(%r12), %rdx
	leaq	offset4x4_check(%r12), %rsi
	cmovneq	%rcx, %rax
	cmovneq	%rdx, %rsi
	movq	(%rax), %r12
	movl	$1, (%rsi)
	movl	$64, %r13d
	movl	$16, %eax
	cmoveq	%rax, %r13
	movl	%r14d, 16(%rsp)         # 4-byte Spill
	movslq	%r14d, %rax
	leaq	32(%rsp,%rax,8), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_41:                               #   Parent Loop BB3_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rbp,8), %rbx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	leaq	28(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	je	.LBB3_43
# BB#42:                                #   in Loop: Header=BB3_41 Depth=2
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.4, %edx
	xorl	%eax, %eax
	movq	%r15, %rcx
	movq	%rbx, %r8
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB3_43:                               #   in Loop: Header=BB3_41 Depth=2
	movzwl	28(%rsp), %eax
	movw	%ax, (%r12,%rbp,2)
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB3_41
# BB#44:                                #   in Loop: Header=BB3_18 Depth=1
	movl	$46, %edi
	callq	putchar
	movl	16(%rsp), %r14d         # 4-byte Reload
	addl	%ebp, %r14d
	cmpl	12(%rsp), %r14d         # 4-byte Folded Reload
	jl	.LBB3_18
.LBB3_45:                               # %._crit_edge
	addq	$8040, %rsp             # imm = 0x1F68
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	ParseQOffsetMatrix, .Lfunc_end3-ParseQOffsetMatrix
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_11
	.quad	.LBB3_10
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_4
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_11
	.quad	.LBB3_26
	.quad	.LBB3_22
	.quad	.LBB3_5
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_26
	.quad	.LBB3_25

	.text
	.globl	Init_QOffsetMatrix
	.p2align	4, 0x90
	.type	Init_QOffsetMatrix,@function
Init_QOffsetMatrix:                     # @Init_QOffsetMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	callq	allocate_QOffsets
	movq	input(%rip), %rsi
	cmpl	$0, 5648(%rsi)
	je	.LBB4_5
# BB#1:
	addq	$5392, %rsi             # imm = 0x1510
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movq	input(%rip), %rdi
	addq	$5392, %rdi             # imm = 0x1510
	xorl	%esi, %esi
	callq	GetConfigFileContent
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB4_3
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	ParseQOffsetMatrix
	jmp	.LBB4_4
.LBB4_3:
	movl	$.L.str.7, %edi
	movl	$errortext, %esi
	xorl	%eax, %eax
	callq	printf
	movq	input(%rip), %rax
	movl	$0, 5648(%rax)
.LBB4_4:
	movl	$10, %edi
	callq	putchar
	movq	%rbx, %rdi
	callq	free
.LBB4_5:
	popq	%rbx
	jmp	InitOffsetParam         # TAILCALL
.Lfunc_end4:
	.size	Init_QOffsetMatrix, .Lfunc_end4-Init_QOffsetMatrix
	.cfi_endproc

	.globl	InitOffsetParam
	.p2align	4, 0x90
	.type	InitOffsetParam,@function
InitOffsetParam:                        # @InitOffsetParam
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rax
	cmpl	$0, 5648(%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	(%rax), %rdi
	je	.LBB5_1
# BB#2:
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 16
	movq	OffsetList4x4input(%rip), %rax
	movq	(%rax), %rsi
	movl	$480, %edx              # imm = 0x1E0
	callq	memcpy
	movq	OffsetList8x8(%rip), %rax
	movq	(%rax), %rdi
	movq	OffsetList8x8input(%rip), %rax
	movq	(%rax), %rsi
	movl	$640, %edx              # imm = 0x280
	popq	%rax
	jmp	memcpy                  # TAILCALL
.LBB5_1:                                # %.preheader10.preheader14
	movaps	Offset_intra_default_chroma+16(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	movaps	Offset_intra_default_chroma(%rip), %xmm1
	movups	%xmm1, (%rdi)
	movq	OffsetList4x4(%rip), %rax
	movq	8(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	16(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	24(%rax), %rax
	movaps	Offset_inter_default+16(%rip), %xmm0
	movups	%xmm0, 16(%rax)
	movaps	Offset_inter_default(%rip), %xmm1
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	32(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	40(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	48(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	56(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	64(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	72(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	80(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	88(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	96(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	104(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList4x4(%rip), %rax
	movq	112(%rax), %rax
	movups	%xmm0, 16(%rax)
	movups	%xmm1, (%rax)
	movq	OffsetList8x8(%rip), %rax
	movq	(%rax), %rax
	movaps	Offset8_intra_default_intra+112(%rip), %xmm0
	movups	%xmm0, 112(%rax)
	movaps	Offset8_intra_default_intra+96(%rip), %xmm0
	movups	%xmm0, 96(%rax)
	movaps	Offset8_intra_default_intra+80(%rip), %xmm0
	movups	%xmm0, 80(%rax)
	movaps	Offset8_intra_default_intra+64(%rip), %xmm0
	movups	%xmm0, 64(%rax)
	movaps	Offset8_intra_default_intra+48(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movaps	Offset8_intra_default_intra+32(%rip), %xmm0
	movups	%xmm0, 32(%rax)
	movaps	Offset8_intra_default_intra+16(%rip), %xmm0
	movups	%xmm0, 16(%rax)
	movaps	Offset8_intra_default_intra(%rip), %xmm0
	movups	%xmm0, (%rax)
	movq	OffsetList8x8(%rip), %rax
	movq	8(%rax), %rax
	movaps	Offset8_inter_default+112(%rip), %xmm0
	movups	%xmm0, 112(%rax)
	movaps	Offset8_inter_default+96(%rip), %xmm1
	movups	%xmm1, 96(%rax)
	movaps	Offset8_inter_default+80(%rip), %xmm2
	movups	%xmm2, 80(%rax)
	movaps	Offset8_inter_default+64(%rip), %xmm3
	movups	%xmm3, 64(%rax)
	movaps	Offset8_inter_default+48(%rip), %xmm4
	movups	%xmm4, 48(%rax)
	movaps	Offset8_inter_default+32(%rip), %xmm5
	movups	%xmm5, 32(%rax)
	movaps	Offset8_inter_default+16(%rip), %xmm6
	movups	%xmm6, 16(%rax)
	movaps	Offset8_inter_default(%rip), %xmm7
	movups	%xmm7, (%rax)
	movq	OffsetList8x8(%rip), %rax
	movq	16(%rax), %rax
	movups	%xmm0, 112(%rax)
	movups	%xmm1, 96(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, (%rax)
	movq	OffsetList8x8(%rip), %rax
	movq	24(%rax), %rax
	movups	%xmm0, 112(%rax)
	movups	%xmm1, 96(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, (%rax)
	movq	OffsetList8x8(%rip), %rax
	movq	32(%rax), %rax
	movups	%xmm0, 112(%rax)
	movups	%xmm1, 96(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, (%rax)
	retq
.Lfunc_end5:
	.size	InitOffsetParam, .Lfunc_end5-InitOffsetParam
	.cfi_endproc

	.globl	CalculateOffsetParam
	.p2align	4, 0x90
	.type	CalculateOffsetParam,@function
CalculateOffsetParam:                   # @CalculateOffsetParam
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$32, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 88
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rcx
	movl	20(%rcx), %eax
	cmpl	$3, %eax
	je	.LBB6_3
# BB#1:
	movl	$2, -124(%rsp)          # 4-byte Folded Spill
	cmpl	$4, %eax
	je	.LBB6_4
# BB#2:
	movl	%eax, -124(%rsp)        # 4-byte Spill
	jmp	.LBB6_4
.LBB6_3:                                # %.fold.split
	movl	$0, -124(%rsp)          # 4-byte Folded Spill
.LBB6_4:
	movq	qp_per_matrix(%rip), %rax
	movslq	15452(%rcx), %rdx
	movl	204(%rax,%rdx,4), %edx
	incl	%edx
	movslq	15456(%rcx), %rsi
	movl	204(%rax,%rsi,4), %eax
	incl	%eax
	xorl	%esi, %esi
	cmpl	$0, 15360(%rcx)
	setne	%sil
	movslq	-124(%rsp), %rcx        # 4-byte Folded Reload
	leaq	(%rsi,%rsi,4), %rsi
	shlq	$2, %rsi
	addq	input(%rip), %rsi
	movl	5664(%rsi,%rcx,4), %edi
	movl	%edi, AdaptRndWeight(%rip)
	movl	5704(%rsi,%rcx,4), %ecx
	movl	%ecx, AdaptRndCrWeight(%rip)
	cmpl	%eax, %edx
	cmovgel	%edx, %eax
	testl	%eax, %eax
	jle	.LBB6_12
# BB#5:                                 # %.lr.ph
	movq	OffsetList4x4(%rip), %r10
	movq	LevelOffset4x4Luma(%rip), %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movq	LevelOffset4x4Chroma(%rip), %rcx
	movq	(%rcx), %rsi
	movq	8(%rcx), %rcx
	movq	(%rsi), %rdi
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movq	8(%rsi), %rsi
	movq	%rsi, -32(%rsp)         # 8-byte Spill
	movq	(%rcx), %rsi
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	movq	8(%rcx), %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	leaq	96(%r10), %rcx
	leaq	72(%r10), %rbp
	leaq	48(%r10), %rsi
	leaq	24(%r10), %rbx
	xorl	%edi, %edi
	cmpl	$1, -124(%rsp)          # 4-byte Folded Reload
	sete	%dil
	cltq
	movq	%rax, -8(%rsp)          # 8-byte Spill
	cmoveq	%rcx, %rbp
	movq	%rbp, -56(%rsp)         # 8-byte Spill
	leaq	10(%rdi,%rdi,2), %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	leaq	11(%rdi,%rdi,2), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	cmoveq	%rsi, %rbx
	movq	%rbx, -64(%rsp)         # 8-byte Spill
	leaq	4(%rdi,%rdi,2), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	leaq	5(%rdi,%rdi,2), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	.p2align	4, 0x90
.LBB6_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_7 Depth 2
	movq	-120(%rsp), %rax        # 8-byte Reload
	leal	4(%rax), %ecx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB6_7:                                # %.preheader
                                        #   Parent Loop BB6_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$2, -124(%rsp)          # 4-byte Folded Reload
	jne	.LBB6_9
# BB#8:                                 # %.preheader.split.us
                                        #   in Loop: Header=BB6_7 Depth=2
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %r9
	movq	8(%rax), %rax
	movq	-120(%rsp), %rdx        # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r8), %r12
	movq	(%r10), %rbx
	movq	8(%r10), %rbp
	movq	-32(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi,%r8), %r11
	movq	16(%r10), %r15
	movq	-48(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi,%r8), %r14
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	(%r9,%rdx,8), %rsi
	movq	(%rsi,%r8), %r9
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movq	-24(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi,%r8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	-40(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi,%r8), %r13
	movswl	(%rbx,%r8), %esi
	movq	%rbx, %rdi
	movq	%rdi, -112(%rsp)        # 8-byte Spill
	shll	%cl, %esi
	movl	%esi, (%r12)
	movswl	(%rbp,%r8), %esi
	shll	%cl, %esi
	movl	%esi, (%r11)
	movswl	(%r15,%r8), %esi
	shll	%cl, %esi
	movl	%esi, (%r14)
	movq	-56(%rsp), %r14         # 8-byte Reload
	movq	(%r14), %rsi
	movswl	(%rsi,%r8), %esi
	shll	%cl, %esi
	movl	%esi, (%r9)
	movq	-80(%rsp), %r9          # 8-byte Reload
	movq	(%r10,%r9,8), %rsi
	movswl	(%rsi,%r8), %esi
	shll	%cl, %esi
	movl	%esi, (%rax)
	movq	-88(%rsp), %rbx         # 8-byte Reload
	movq	(%r10,%rbx,8), %rsi
	movswl	(%rsi,%r8), %esi
	shll	%cl, %esi
	movl	%esi, (%r13)
	movq	%r13, %rdx
	movswl	2(%rdi,%r8), %esi
	shll	%cl, %esi
	movq	%r12, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%esi, 4(%rax)
	movq	%rbp, %r12
	movswl	2(%r12,%r8), %esi
	shll	%cl, %esi
	movl	%esi, 4(%r11)
	movq	%r15, %rdi
	movq	%rdi, -104(%rsp)        # 8-byte Spill
	movswl	2(%rdi,%r8), %esi
	shll	%cl, %esi
	movq	(%rsp), %r15            # 8-byte Reload
	movl	%esi, 4(%r15)
	movq	(%r14), %rsi
	movswl	2(%rsi,%r8), %esi
	shll	%cl, %esi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%esi, 4(%rbp)
	movq	%r9, %r13
	movq	(%r10,%r13,8), %rsi
	movswl	2(%rsi,%r8), %esi
	shll	%cl, %esi
	movq	16(%rsp), %r9           # 8-byte Reload
	movl	%esi, 4(%r9)
	movq	(%r10,%rbx,8), %rsi
	movswl	2(%rsi,%r8), %esi
	shll	%cl, %esi
	movl	%esi, 4(%rdx)
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movswl	4(%rsi,%r8), %esi
	shll	%cl, %esi
	movl	%esi, 8(%rax)
	movswl	4(%r12,%r8), %esi
	shll	%cl, %esi
	movl	%esi, 8(%r11)
	movswl	4(%rdi,%r8), %esi
	shll	%cl, %esi
	movl	%esi, 8(%r15)
	movq	(%r14), %rsi
	movswl	4(%rsi,%r8), %esi
	shll	%cl, %esi
	movl	%esi, 8(%rbp)
	movq	(%r10,%r13,8), %rsi
	movswl	4(%rsi,%r8), %esi
	shll	%cl, %esi
	movl	%esi, 8(%r9)
	movq	(%r10,%rbx,8), %rsi
	movswl	4(%rsi,%r8), %esi
	shll	%cl, %esi
	movl	%esi, 8(%rdx)
	movq	-112(%rsp), %rax        # 8-byte Reload
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%eax, 12(%rsi)
	movswl	6(%r12,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 12(%r11)
	movq	-104(%rsp), %rax        # 8-byte Reload
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 12(%r15)
	movq	(%r14), %rax
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 12(%rbp)
	movq	(%r10,%r13,8), %rax
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 12(%r9)
	movq	(%r10,%rbx,8), %rax
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 12(%rdx)
	jmp	.LBB6_10
	.p2align	4, 0x90
.LBB6_9:                                # %.preheader.split
                                        #   in Loop: Header=BB6_7 Depth=2
	movq	-16(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rax
	movq	8(%rsi), %rsi
	movq	-120(%rsp), %rdx        # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi,%r8), %rbx
	movq	%rbx, -104(%rsp)        # 8-byte Spill
	movq	-32(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi,%r8), %r15
	movq	-48(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi,%r8), %r12
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r8), %r9
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r8), %r11
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r8), %r14
	movq	-64(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rax
	movq	%rsi, %r13
	movswl	(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, (%rbx)
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	(%r10,%rax,8), %rax
	movswl	(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, (%r15)
	movq	-72(%rsp), %rsi         # 8-byte Reload
	movq	(%r10,%rsi,8), %rax
	movq	%rsi, %rdi
	movswl	(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, (%r12)
	movq	-56(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp), %rax
	movswl	(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, (%r9)
	movq	%r9, %rbx
	movq	-80(%rsp), %rax         # 8-byte Reload
	movq	(%r10,%rax,8), %rax
	movswl	(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, (%r11)
	movq	%r11, %rdx
	movq	-88(%rsp), %rax         # 8-byte Reload
	movq	(%r10,%rax,8), %rax
	movswl	(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, (%r14)
	movq	%r13, %r9
	movq	(%r9), %rax
	movswl	2(%rax,%r8), %eax
	shll	%cl, %eax
	movq	-104(%rsp), %r13        # 8-byte Reload
	movl	%eax, 4(%r13)
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	(%r10,%rax,8), %rax
	movswl	2(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 4(%r15)
	movq	%r15, %r11
	movq	%r11, -112(%rsp)        # 8-byte Spill
	movq	(%r10,%rdi,8), %rax
	movswl	2(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 4(%r12)
	movq	(%rbp), %rax
	movswl	2(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 4(%rbx)
	movq	-80(%rsp), %rsi         # 8-byte Reload
	movq	(%r10,%rsi,8), %rax
	movswl	2(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 4(%rdx)
	movq	-88(%rsp), %rdi         # 8-byte Reload
	movq	(%r10,%rdi,8), %rax
	movswl	2(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 4(%r14)
	movq	(%r9), %rax
	movswl	4(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 8(%r13)
	movq	-96(%rsp), %r15         # 8-byte Reload
	movq	(%r10,%r15,8), %rax
	movswl	4(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 8(%r11)
	movq	-72(%rsp), %r9          # 8-byte Reload
	movq	(%r10,%r9,8), %rax
	movswl	4(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 8(%r12)
	movq	(%rbp), %rax
	movswl	4(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 8(%rbx)
	movq	(%r10,%rsi,8), %rax
	movswl	4(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 8(%rdx)
	movq	%rdx, %r11
	movq	(%r10,%rdi,8), %rax
	movswl	4(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 8(%r14)
	movq	-64(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 12(%r13)
	movq	(%r10,%r15,8), %rax
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movq	-112(%rsp), %rdx        # 8-byte Reload
	movl	%eax, 12(%rdx)
	movq	(%r10,%r9,8), %rax
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 12(%r12)
	movq	(%rbp), %rax
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 12(%rbx)
	movq	(%r10,%rsi,8), %rax
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 12(%r11)
	movq	(%r10,%rdi,8), %rax
	movswl	6(%rax,%r8), %eax
	shll	%cl, %eax
	movl	%eax, 12(%r14)
.LBB6_10:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB6_7 Depth=2
	addq	$8, %r8
	cmpq	$32, %r8
	jne	.LBB6_7
# BB#11:                                #   in Loop: Header=BB6_6 Depth=1
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	cmpq	-8(%rsp), %rcx          # 8-byte Folded Reload
	jl	.LBB6_6
.LBB6_12:                               # %._crit_edge
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	CalculateOffsetParam, .Lfunc_end6-CalculateOffsetParam
	.cfi_endproc

	.globl	CalculateOffset8Param
	.p2align	4, 0x90
	.type	CalculateOffset8Param,@function
CalculateOffset8Param:                  # @CalculateOffset8Param
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	qp_per_matrix(%rip), %rcx
	movq	img(%rip), %rax
	movslq	15452(%rax), %rdx
	movl	204(%rcx,%rdx,4), %edx
	incl	%edx
	movslq	15456(%rax), %rsi
	movl	204(%rcx,%rsi,4), %ecx
	incl	%ecx
	cmpl	%ecx, %edx
	cmovgel	%edx, %ecx
	testl	%ecx, %ecx
	jle	.LBB7_29
# BB#1:                                 # %.lr.ph
	movq	OffsetList8x8(%rip), %rdx
	movq	LevelOffset8x8Luma(%rip), %rsi
	movq	(%rsi), %rdi
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	movq	8(%rsi), %rsi
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	leaq	16(%rdx), %r13
	leaq	8(%rdx), %r12
	movslq	%ecx, %r10
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB7_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_3 Depth 2
	leal	5(%r11), %ecx
	movq	-16(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%r11,8), %r14
	movq	-8(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi,%r11,8), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_3:                                # %.preheader
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rbx), %rsi
	movq	(%r15,%rbx), %r9
	movl	20(%rax), %r8d
	cmpl	$2, %r8d
	movq	%rdx, %rbp
	je	.LBB7_6
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB7_3 Depth=2
	cmpl	$1, %r8d
	movq	%r13, %rbp
	je	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, %rbp
.LBB7_6:                                #   in Loop: Header=BB7_3 Depth=2
	movq	(%rbp), %rdi
	movswl	(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, (%rsi)
	xorl	%edi, %edi
	cmpl	$1, 20(%rax)
	sete	%dil
	movq	24(%rdx,%rdi,8), %rdi
	movswl	(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, (%r9)
	movl	20(%rax), %edi
	cmpl	$1, %edi
	movq	%r13, %rbp
	je	.LBB7_9
# BB#7:                                 #   in Loop: Header=BB7_3 Depth=2
	cmpl	$2, %edi
	movq	%rdx, %rbp
	je	.LBB7_9
# BB#8:                                 #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, %rbp
.LBB7_9:                                #   in Loop: Header=BB7_3 Depth=2
	movq	(%rbp), %rdi
	movswl	2(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 4(%rsi)
	xorl	%edi, %edi
	cmpl	$1, 20(%rax)
	sete	%dil
	movq	24(%rdx,%rdi,8), %rdi
	movswl	2(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 4(%r9)
	movl	20(%rax), %edi
	cmpl	$2, %edi
	movq	%rdx, %rbp
	je	.LBB7_12
# BB#10:                                #   in Loop: Header=BB7_3 Depth=2
	cmpl	$1, %edi
	movq	%r13, %rbp
	je	.LBB7_12
# BB#11:                                #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, %rbp
.LBB7_12:                               #   in Loop: Header=BB7_3 Depth=2
	movq	(%rbp), %rdi
	movswl	4(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 8(%rsi)
	xorl	%edi, %edi
	cmpl	$1, 20(%rax)
	sete	%dil
	movq	24(%rdx,%rdi,8), %rdi
	movswl	4(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 8(%r9)
	movl	20(%rax), %edi
	cmpl	$2, %edi
	movq	%rdx, %rbp
	je	.LBB7_15
# BB#13:                                #   in Loop: Header=BB7_3 Depth=2
	cmpl	$1, %edi
	movq	%r13, %rbp
	je	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, %rbp
.LBB7_15:                               #   in Loop: Header=BB7_3 Depth=2
	movq	(%rbp), %rdi
	movswl	6(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 12(%rsi)
	xorl	%edi, %edi
	cmpl	$1, 20(%rax)
	sete	%dil
	movq	24(%rdx,%rdi,8), %rdi
	movswl	6(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 12(%r9)
	movl	20(%rax), %edi
	cmpl	$2, %edi
	movq	%rdx, %rbp
	je	.LBB7_18
# BB#16:                                #   in Loop: Header=BB7_3 Depth=2
	cmpl	$1, %edi
	movq	%r13, %rbp
	je	.LBB7_18
# BB#17:                                #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, %rbp
.LBB7_18:                               #   in Loop: Header=BB7_3 Depth=2
	movq	(%rbp), %rdi
	movswl	8(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 16(%rsi)
	xorl	%edi, %edi
	cmpl	$1, 20(%rax)
	sete	%dil
	movq	24(%rdx,%rdi,8), %rdi
	movswl	8(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 16(%r9)
	movl	20(%rax), %edi
	cmpl	$2, %edi
	movq	%rdx, %rbp
	je	.LBB7_21
# BB#19:                                #   in Loop: Header=BB7_3 Depth=2
	cmpl	$1, %edi
	movq	%r13, %rbp
	je	.LBB7_21
# BB#20:                                #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, %rbp
.LBB7_21:                               #   in Loop: Header=BB7_3 Depth=2
	movq	(%rbp), %rdi
	movswl	10(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 20(%rsi)
	xorl	%edi, %edi
	cmpl	$1, 20(%rax)
	sete	%dil
	movq	24(%rdx,%rdi,8), %rdi
	movswl	10(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 20(%r9)
	movl	20(%rax), %edi
	cmpl	$2, %edi
	movq	%rdx, %rbp
	je	.LBB7_24
# BB#22:                                #   in Loop: Header=BB7_3 Depth=2
	cmpl	$1, %edi
	movq	%r13, %rbp
	je	.LBB7_24
# BB#23:                                #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, %rbp
.LBB7_24:                               #   in Loop: Header=BB7_3 Depth=2
	movq	(%rbp), %rdi
	movswl	12(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 24(%rsi)
	xorl	%edi, %edi
	cmpl	$1, 20(%rax)
	sete	%dil
	movq	24(%rdx,%rdi,8), %rdi
	movswl	12(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 24(%r9)
	movl	20(%rax), %edi
	cmpl	$2, %edi
	movq	%rdx, %rbp
	je	.LBB7_27
# BB#25:                                #   in Loop: Header=BB7_3 Depth=2
	cmpl	$1, %edi
	movq	%r13, %rbp
	je	.LBB7_27
# BB#26:                                #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, %rbp
.LBB7_27:                               #   in Loop: Header=BB7_3 Depth=2
	movq	(%rbp), %rdi
	movswl	14(%rdi,%rbx,2), %edi
	shll	%cl, %edi
	movl	%edi, 28(%rsi)
	xorl	%esi, %esi
	cmpl	$1, 20(%rax)
	sete	%sil
	movq	24(%rdx,%rsi,8), %rsi
	movswl	14(%rsi,%rbx,2), %esi
	shll	%cl, %esi
	movl	%esi, 28(%r9)
	addq	$8, %rbx
	cmpq	$64, %rbx
	jne	.LBB7_3
# BB#28:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%r11
	cmpq	%r10, %r11
	jl	.LBB7_2
.LBB7_29:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	CalculateOffset8Param, .Lfunc_end7-CalculateOffset8Param
	.cfi_endproc

	.type	offset4x4_check,@object # @offset4x4_check
	.bss
	.globl	offset4x4_check
	.p2align	4
offset4x4_check:
	.zero	24
	.size	offset4x4_check, 24

	.type	offset8x8_check,@object # @offset8x8_check
	.globl	offset8x8_check
	.p2align	2
offset8x8_check:
	.zero	8
	.size	offset8x8_check, 8

	.type	OffsetBits,@object      # @OffsetBits
	.section	.rodata,"a",@progbits
	.globl	OffsetBits
	.p2align	2
OffsetBits:
	.long	11                      # 0xb
	.size	OffsetBits, 4

	.type	LevelOffset4x4Luma,@object # @LevelOffset4x4Luma
	.comm	LevelOffset4x4Luma,8,8
	.type	LevelOffset4x4Chroma,@object # @LevelOffset4x4Chroma
	.comm	LevelOffset4x4Chroma,8,8
	.type	LevelOffset8x8Luma,@object # @LevelOffset8x8Luma
	.comm	LevelOffset8x8Luma,8,8
	.type	OffsetList4x4input,@object # @OffsetList4x4input
	.comm	OffsetList4x4input,8,8
	.type	OffsetList8x8input,@object # @OffsetList8x8input
	.comm	OffsetList8x8input,8,8
	.type	OffsetList4x4,@object   # @OffsetList4x4
	.comm	OffsetList4x4,8,8
	.type	OffsetList8x8,@object   # @OffsetList8x8
	.comm	OffsetList8x8,8,8
	.type	OffsetType4x4,@object   # @OffsetType4x4
	.p2align	4
OffsetType4x4:
	.asciz	"INTRA4X4_LUMA_INTRA\000\000\000\000"
	.asciz	"INTRA4X4_CHROMAU_INTRA\000"
	.asciz	"INTRA4X4_CHROMAV_INTRA\000"
	.asciz	"INTRA4X4_LUMA_INTERP\000\000\000"
	.asciz	"INTRA4X4_CHROMAU_INTERP"
	.asciz	"INTRA4X4_CHROMAV_INTERP"
	.asciz	"INTRA4X4_LUMA_INTERB\000\000\000"
	.asciz	"INTRA4X4_CHROMAU_INTERB"
	.asciz	"INTRA4X4_CHROMAV_INTERB"
	.asciz	"INTER4X4_LUMA_INTERP\000\000\000"
	.asciz	"INTER4X4_CHROMAU_INTERP"
	.asciz	"INTER4X4_CHROMAV_INTERP"
	.asciz	"INTER4X4_LUMA_INTERB\000\000\000"
	.asciz	"INTER4X4_CHROMAU_INTERB"
	.asciz	"INTER4X4_CHROMAV_INTERB"
	.size	OffsetType4x4, 360

	.type	OffsetType8x8,@object   # @OffsetType8x8
	.p2align	4
OffsetType8x8:
	.asciz	"INTRA8X8_LUMA_INTRA\000\000\000\000"
	.asciz	"INTRA8X8_LUMA_INTERP\000\000\000"
	.asciz	"INTRA8X8_LUMA_INTERB\000\000\000"
	.asciz	"INTER8X8_LUMA_INTERP\000\000\000"
	.asciz	"INTER8X8_LUMA_INTERB\000\000\000"
	.size	OffsetType8x8, 120

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" Parsing error in config file: Parameter Name '%s' not recognized."
	.size	.L.str, 67

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" Parsing error in config file: '=' expected as the second token in each item."
	.size	.L.str.2, 78

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" Parsing error: Expected numerical value for Parameter of %s, found '%s'."
	.size	.L.str.4, 74

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Parsing Quantization Offset Matrix file %s "
	.size	.L.str.6, 44

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\nError: %s\nProceeding with default values for all matrices."
	.size	.L.str.7, 60

	.type	Offset_intra_default_chroma,@object # @Offset_intra_default_chroma
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
Offset_intra_default_chroma:
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.size	Offset_intra_default_chroma, 32

	.type	Offset_inter_default,@object # @Offset_inter_default
	.p2align	4
Offset_inter_default:
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.size	Offset_inter_default, 32

	.type	Offset8_intra_default_intra,@object # @Offset8_intra_default_intra
	.section	.rodata,"a",@progbits
	.p2align	4
Offset8_intra_default_intra:
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.short	682                     # 0x2aa
	.size	Offset8_intra_default_intra, 128

	.type	Offset8_inter_default,@object # @Offset8_inter_default
	.p2align	4
Offset8_inter_default:
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.short	342                     # 0x156
	.size	Offset8_inter_default, 128

	.type	AdaptRndWeight,@object  # @AdaptRndWeight
	.comm	AdaptRndWeight,4,4
	.type	AdaptRndCrWeight,@object # @AdaptRndCrWeight
	.comm	AdaptRndCrWeight,4,4
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
