	.text
	.file	"nal.bc"
	.globl	RBSPtoSODB
	.p2align	4, 0x90
	.type	RBSPtoSODB,@function
RBSPtoSODB:                             # @RBSPtoSODB
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movslq	%ebx, %rax
	testb	$1, -1(%r14,%rax)
	jne	.LBB0_7
# BB#1:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpl	$8, %eax
	jne	.LBB0_6
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	testl	%ebx, %ebx
	jne	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.Lstr, %edi
	callq	puts
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	decl	%ebx
	xorl	%eax, %eax
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebx, %rcx
	movzbl	-1(%r14,%rcx), %ecx
	btl	%eax, %ecx
	jae	.LBB0_2
.LBB0_7:                                # %._crit_edge
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	RBSPtoSODB, .Lfunc_end0-RBSPtoSODB
	.cfi_endproc

	.globl	EBSPtoRBSP
	.p2align	4, 0x90
	.type	EBSPtoRBSP,@function
EBSPtoRBSP:                             # @EBSPtoRBSP
	.cfi_startproc
# BB#0:
	cmpl	%edx, %esi
	jge	.LBB1_2
# BB#1:
	movl	%esi, %edx
	movl	%edx, %eax
	retq
.LBB1_2:                                # %.preheader
	cmpl	%esi, %edx
	jge	.LBB1_7
# BB#3:                                 # %.lr.ph.preheader
	movslq	%edx, %r9
	addq	%rdi, %r9
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
	movl	%edx, %ecx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$2, %r11d
	jne	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=1
	movslq	%ecx, %rcx
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	cmpb	$3, (%rdi,%rcx)
	sete	%r10b
	setne	%r11b
	addl	%r10d, %ecx
	addl	%r11d, %r11d
.LBB1_6:                                #   in Loop: Header=BB1_4 Depth=1
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %eax
	movb	%al, (%r9)
	incl	%r11d
	cmpb	$0, (%rdi,%rcx)
	cmovnel	%r8d, %r11d
	incl	%ecx
	incq	%r9
	incl	%edx
	cmpl	%esi, %ecx
	jl	.LBB1_4
.LBB1_7:                                # %.loopexit
	movl	%edx, %eax
	retq
.Lfunc_end1:
	.size	EBSPtoRBSP, .Lfunc_end1-EBSPtoRBSP
	.cfi_endproc

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	" Panic: All zero data sequence in RBSP "
	.size	.Lstr, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
