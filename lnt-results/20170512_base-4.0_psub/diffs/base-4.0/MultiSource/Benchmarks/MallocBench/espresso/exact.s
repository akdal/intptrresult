	.text
	.file	"exact.bc"
	.globl	minimize_exact
	.p2align	4, 0x90
	.type	minimize_exact,@function
minimize_exact:                         # @minimize_exact
	.cfi_startproc
# BB#0:
	xorl	%r8d, %r8d
	jmp	do_minimize             # TAILCALL
.Lfunc_end0:
	.size	minimize_exact, .Lfunc_end0-minimize_exact
	.cfi_endproc

	.globl	minimize_exact_literals
	.p2align	4, 0x90
	.type	minimize_exact_literals,@function
minimize_exact_literals:                # @minimize_exact_literals
	.cfi_startproc
# BB#0:
	movl	$1, %r8d
	jmp	do_minimize             # TAILCALL
.Lfunc_end1:
	.size	minimize_exact_literals, .Lfunc_end1-minimize_exact_literals
	.cfi_endproc

	.p2align	4, 0x90
	.type	do_minimize,@function
do_minimize:                            # @do_minimize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, 16(%rsp)          # 4-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	debug(%rip), %eax
	testb	$4, %ah
	movl	%eax, %r13d
	je	.LBB2_2
# BB#1:
	movl	%eax, %r13d
	orl	$2080, %r13d            # imm = 0x820
	movl	%r13d, debug(%rip)
.LBB2_2:
	movl	%eax, 44(%rsp)          # 4-byte Spill
	testl	%ecx, %ecx
	sete	%bpl
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	cube2list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	primes_consensus
	movq	%rax, %r15
	cmpl	$0, trace(%rip)
	je	.LBB2_4
# BB#3:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r12, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB2_4:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	leaq	8(%rsp), %rdx
	leaq	48(%rsp), %rcx
	movq	%rsp, %r8
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	irred_split_cover
	cmpl	$0, trace(%rip)
	je	.LBB2_6
# BB#5:
	movq	8(%rsp), %r12
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%rbx, %rcx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB2_6:
	shrl	$9, %r13d
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	8(%rsp), %rsi
	movq	(%rsp), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	irred_derive_table
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cmpl	$0, trace(%rip)
	je	.LBB2_8
# BB#7:
	movq	(%rsp), %r12
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%rbx, %rcx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB2_8:
	andl	$4, %r13d
	xorl	%eax, %eax
	movb	%bpl, %al
	movl	%eax, %ebp
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movq	%r14, 16(%rsp)          # 8-byte Spill
	je	.LBB2_9
# BB#10:
	movslq	12(%r15), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %r12
	movq	(%rsp), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB2_14
# BB#11:                                # %.lr.ph94.preheader
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB2_12:                               # %.lr.ph94
                                        # =>This Inner Loop Header: Depth=1
	movl	cube(%rip), %r14d
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	subl	%eax, %r14d
	movzwl	2(%rbx), %eax
	movl	%r14d, (%r12,%rax,4)
	movq	(%rsp), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB2_12
# BB#13:
	movl	24(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB2_14
.LBB2_9:
	xorl	%r12d, %r12d
.LBB2_14:                               # %.loopexit
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	movl	%ebp, %edx
	movl	%r13d, %ecx
	callq	sm_minimum_cover
	movq	%rax, %r13
	cmpl	$0, trace(%rip)
	je	.LBB2_16
# BB#15:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%rbx, %rcx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB2_16:
	testq	%r12, %r12
	je	.LBB2_18
# BB#17:
	movq	%r12, %rdi
	callq	free
.LBB2_18:
	testb	$4, debug+1(%rip)
	je	.LBB2_41
# BB#19:
	movq	8(%rsp), %r12
	movq	48(%rsp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	(%rsp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	filename(%rip), %rbp
	testq	%rbp, %rbp
	movq	%r13, 24(%rsp)          # 8-byte Spill
	je	.LBB2_21
# BB#20:
	movl	$.L.str.4, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_21
# BB#22:
	movq	%rbp, %rdi
	callq	strlen
	leaq	20(%rax), %rdi
	callq	malloc
	movq	%rax, %rbx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	sprintf
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB2_23
.LBB2_24:
	movq	filename(%rip), %rdx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB2_25
# BB#26:
	testq	%rbx, %rbx
	je	.LBB2_28
.LBB2_27:
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB2_28
.LBB2_21:
	movq	stdout(%rip), %r14
	movq	%r14, %rbp
.LBB2_28:
	xorl	%eax, %eax
	callq	new_PLA
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	PLA_labels
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	fpr_header
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	free_PLA
	movl	$.L.str.9, %edi
	movl	$23, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB2_31
# BB#29:                                # %.lr.ph97.i.preheader
	movq	24(%r12), %rbx
	leaq	(%rbx,%rax,4), %r13
	.p2align	4, 0x90
.LBB2_30:                               # %.lr.ph97.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movslq	(%r12), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r13, %rbx
	jb	.LBB2_30
.LBB2_31:                               # %._crit_edge98.i
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	$.L.str.11, %edi
	movl	$31, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	64(%rsp), %r12          # 8-byte Reload
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB2_34
# BB#32:                                # %.lr.ph93.i.preheader
	movq	24(%r12), %rbx
	leaq	(%rbx,%rax,4), %rbp
	.p2align	4, 0x90
.LBB2_33:                               # %.lr.ph93.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movslq	(%r12), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB2_33
.LBB2_34:                               # %._crit_edge94.i
	movl	$.L.str.12, %edi
	movl	$33, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	72(%rsp), %r12          # 8-byte Reload
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	movq	24(%rsp), %r13          # 8-byte Reload
	jle	.LBB2_37
# BB#35:                                # %.lr.ph.i.preheader
	movq	24(%r12), %rbx
	leaq	(%rbx,%rax,4), %rbp
	.p2align	4, 0x90
.LBB2_36:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movslq	(%r12), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB2_36
.LBB2_37:                               # %._crit_edge.i
	cmpq	stdout(%rip), %r14
	je	.LBB2_39
# BB#38:
	movq	%r14, %rdi
	callq	fclose
.LBB2_39:
	xorl	%eax, %eax
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	sm_write
	cmpq	stdout(%rip), %rbx
	je	.LBB2_41
# BB#40:
	movq	%rbx, %rdi
	callq	fclose
.LBB2_41:                               # %dump_irredundant.exit
	movl	cube(%rip), %esi
	movl	$100, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbp
	movq	8(%rsp), %rdi
	movslq	(%rdi), %rcx
	movslq	12(%rdi), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB2_44
# BB#42:                                # %.lr.ph89.preheader
	movq	24(%rdi), %rbx
	leaq	(%rbx,%rax,4), %r14
	.p2align	4, 0x90
.LBB2_43:                               # %.lr.ph89
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	sf_addset
	movq	%rax, %rbp
	movq	8(%rsp), %rdi
	movslq	(%rdi), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r14, %rbx
	jb	.LBB2_43
.LBB2_44:                               # %._crit_edge90
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB2_45
# BB#46:                                # %.lr.ph
	movq	16(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_47:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rax
	movslq	4(%rbx), %rcx
	movslq	(%r15), %rdx
	imulq	%rcx, %rdx
	leaq	(%rax,%rdx,4), %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_addset
	movq	%rax, %rbp
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_47
# BB#48:                                # %._crit_edge.loopexit
	movq	8(%rsp), %rdi
	jmp	.LBB2_49
.LBB2_45:
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB2_49:                               # %._crit_edge
	xorl	%eax, %eax
	callq	sf_free
	movq	48(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	xorl	%eax, %eax
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	sm_free
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sm_row_free
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	andl	$-10273, debug(%rip)    # imm = 0xD7DF
	movq	80(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB2_52
# BB#50:                                # %._crit_edge
	movl	skip_make_sparse(%rip), %eax
	testl	%eax, %eax
	jne	.LBB2_52
# BB#51:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	make_sparse
	movq	%rax, %rbp
.LBB2_52:
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	%eax, debug(%rip)
	movq	%rbp, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_23:
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movq	stdout(%rip), %r14
	jmp	.LBB2_24
.LBB2_25:
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movq	stdout(%rip), %rbp
	testq	%rbx, %rbx
	jne	.LBB2_27
	jmp	.LBB2_28
.Lfunc_end2:
	.size	do_minimize, .Lfunc_end2-do_minimize
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"PRIMES     "
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ESSENTIALS "
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"PI-TABLE   "
	.size	.L.str.2, 12

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"MINCOV     "
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"(stdin)"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s.primes"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"w"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"espresso: Unable to open %s\n"
	.size	.L.str.7, 29

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s.pi"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"# Essential primes are\n"
	.size	.L.str.9, 24

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%s\n"
	.size	.L.str.10, 4

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"# Totally redundant primes are\n"
	.size	.L.str.11, 32

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"# Partially redundant primes are\n"
	.size	.L.str.12, 34


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
