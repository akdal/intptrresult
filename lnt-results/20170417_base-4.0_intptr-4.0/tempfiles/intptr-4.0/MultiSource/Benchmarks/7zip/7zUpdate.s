	.text
	.file	"7zUpdate.bc"
	.globl	_ZNK8NArchive3N7z11CUpdateItem15GetExtensionPosEv
	.p2align	4, 0x90
	.type	_ZNK8NArchive3N7z11CUpdateItem15GetExtensionPosEv,@function
_ZNK8NArchive3N7z11CUpdateItem15GetExtensionPosEv: # @_ZNK8NArchive3N7z11CUpdateItem15GetExtensionPosEv
	.cfi_startproc
# BB#0:
	movslq	48(%rdi), %rax
	testq	%rax, %rax
	je	.LBB0_1
# BB#3:
	movq	40(%rdi), %rcx
	leaq	(,%rax,4), %rdx
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	cmpl	$47, -4(%rcx,%rdx)
	je	.LBB0_5
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=1
	addq	$-4, %rdx
	jne	.LBB0_4
# BB#7:
	movl	$-1, %edx
	jmp	.LBB0_8
.LBB0_1:
	xorl	%eax, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB0_5:
	leaq	-4(%rcx,%rdx), %rdx
	subq	%rcx, %rdx
	shrq	$2, %rdx
.LBB0_8:                                # %_ZN8NArchive3N7zL18GetReverseSlashPosERK11CStringBaseIwE.exit
	movq	%rax, %rsi
	shlq	$2, %rsi
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rcx,%rsi)
	je	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	addq	$-4, %rsi
	jne	.LBB0_9
	jmp	.LBB0_2
.LBB0_11:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	leaq	-4(%rcx,%rsi), %rsi
	subq	%rcx, %rsi
	shrq	$2, %rsi
	testl	%esi, %esi
	js	.LBB0_2
# BB#12:
	leal	1(%rsi), %edi
	cmpl	%edx, %esi
	movl	%edi, %ecx
	cmovll	%eax, %ecx
	testl	%edx, %edx
	cmovsl	%edi, %ecx
	movl	%ecx, %eax
	retq
.LBB0_2:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.thread
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end0:
	.size	_ZNK8NArchive3N7z11CUpdateItem15GetExtensionPosEv, .Lfunc_end0-_ZNK8NArchive3N7z11CUpdateItem15GetExtensionPosEv
	.cfi_endproc

	.globl	_ZNK8NArchive3N7z11CUpdateItem12GetExtensionEv
	.p2align	4, 0x90
	.type	_ZNK8NArchive3N7z11CUpdateItem12GetExtensionEv,@function
_ZNK8NArchive3N7z11CUpdateItem12GetExtensionEv: # @_ZNK8NArchive3N7z11CUpdateItem12GetExtensionEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %rax
	movq	%rdi, %r14
	leaq	40(%rax), %rsi
	movslq	48(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB1_1
# BB#2:
	movq	40(%rax), %rax
	leaq	(,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	$47, -4(%rax,%rdx)
	je	.LBB1_4
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=1
	addq	$-4, %rdx
	jne	.LBB1_3
# BB#6:
	movl	$-1, %edi
	jmp	.LBB1_7
.LBB1_1:
	xorl	%edx, %edx
	jmp	.LBB1_13
.LBB1_4:
	leaq	-4(%rax,%rdx), %rdi
	subq	%rax, %rdi
	shrq	$2, %rdi
.LBB1_7:                                # %_ZN8NArchive3N7zL18GetReverseSlashPosERK11CStringBaseIwE.exit.i
	movq	%rcx, %rdx
	shlq	$2, %rdx
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rax,%rdx)
	je	.LBB1_11
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=1
	addq	$-4, %rdx
	jne	.LBB1_8
# BB#10:
	movl	%ecx, %edx
	jmp	.LBB1_13
.LBB1_11:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.i
	leaq	-4(%rax,%rdx), %rbx
	subq	%rax, %rbx
	shrq	$2, %rbx
	testl	%ebx, %ebx
	movl	%ecx, %edx
	js	.LBB1_13
# BB#12:
	leal	1(%rbx), %eax
	cmpl	%edi, %ebx
	movl	%eax, %edx
	cmovll	%ecx, %edx
	testl	%edi, %edi
	cmovsl	%eax, %edx
.LBB1_13:                               # %_ZNK8NArchive3N7z11CUpdateItem15GetExtensionPosEv.exit
	subl	%edx, %ecx
	movq	%r14, %rdi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZNK8NArchive3N7z11CUpdateItem12GetExtensionEv, .Lfunc_end1-_ZNK8NArchive3N7z11CUpdateItem12GetExtensionEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z11GetExtIndexEPKc
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11GetExtIndexEPKc,@function
_ZN8NArchive3N7z11GetExtIndexEPKc:      # @_ZN8NArchive3N7z11GetExtIndexEPKc
	.cfi_startproc
# BB#0:
	movl	$.L.str, %edx
	movl	$1, %r8d
                                        # implicit-def: %EAX
	.p2align	4, 0x90
.LBB2_1:                                # %.thread45
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
                                        #     Child Loop BB2_13 Depth 2
	movb	(%rdx), %r11b
	incq	%rdx
	cmpb	$32, %r11b
	je	.LBB2_1
# BB#2:                                 # %.thread45
                                        #   in Loop: Header=BB2_1 Depth=1
	testb	%r11b, %r11b
	je	.LBB2_16
# BB#3:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB2_4:                                # %.preheader
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rsi), %r10d
	testb	%r10b, %r10b
	je	.LBB2_5
.LBB2_7:                                #   in Loop: Header=BB2_4 Depth=2
	movl	$4, %r9d
	cmpb	%r10b, %r11b
	jne	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_4 Depth=2
	movzbl	(%rdx), %r11d
	incq	%rdx
	xorl	%r9d, %r9d
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_4 Depth=2
	movl	$1, %r9d
	movl	%r11d, %ecx
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=2
	movl	%r8d, %eax
.LBB2_9:                                #   in Loop: Header=BB2_4 Depth=2
	incq	%rsi
	movl	%r9d, %ecx
	andb	$7, %cl
	je	.LBB2_4
# BB#10:                                #   in Loop: Header=BB2_1 Depth=1
	cmpb	$4, %cl
	jne	.LBB2_15
# BB#11:                                #   in Loop: Header=BB2_1 Depth=1
	incl	%r8d
	testb	%r11b, %r11b
	jne	.LBB2_13
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_13 Depth=2
	movzbl	(%rdx), %r11d
	incq	%rdx
	testb	%r11b, %r11b
	je	.LBB2_16
.LBB2_13:                               #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$32, %r11b
	jne	.LBB2_14
	jmp	.LBB2_1
.LBB2_15:                               #   in Loop: Header=BB2_1 Depth=1
	testl	%r9d, %r9d
	je	.LBB2_1
	jmp	.LBB2_17
.LBB2_16:                               # %.thread.loopexit101
	movl	%r8d, %eax
.LBB2_17:                               # %.thread
	retq
.Lfunc_end2:
	.size	_ZN8NArchive3N7z11GetExtIndexEPKc, .Lfunc_end2-_ZN8NArchive3N7z11GetExtIndexEPKc
	.cfi_endproc

	.globl	_ZN8NArchive3N7z17CFolderOutStream24InitEPKNS0_18CArchiveDatabaseExEjPK13CRecordVectorIbEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream24InitEPKNS0_18CArchiveDatabaseExEjPK13CRecordVectorIbEP20ISequentialOutStream,@function
_ZN8NArchive3N7z17CFolderOutStream24InitEPKNS0_18CArchiveDatabaseExEjPK13CRecordVectorIbEP20ISequentialOutStream: # @_ZN8NArchive3N7z17CFolderOutStream24InitEPKNS0_18CArchiveDatabaseExEjPK13CRecordVectorIbEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%r8, %r14
	movq	%rdi, %rbx
	movq	%rsi, 32(%rbx)
	movl	%edx, 56(%rbx)
	movq	%rcx, 40(%rbx)
	testq	%r14, %r14
	je	.LBB3_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB3_2:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB3_4:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit
	movq	%r14, 48(%rbx)
	movl	$0, 60(%rbx)
	movb	$0, 64(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN8NArchive3N7z17CFolderOutStream217ProcessEmptyFilesEv # TAILCALL
.Lfunc_end3:
	.size	_ZN8NArchive3N7z17CFolderOutStream24InitEPKNS0_18CArchiveDatabaseExEjPK13CRecordVectorIbEP20ISequentialOutStream, .Lfunc_end3-_ZN8NArchive3N7z17CFolderOutStream24InitEPKNS0_18CArchiveDatabaseExEjPK13CRecordVectorIbEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN8NArchive3N7z17CFolderOutStream217ProcessEmptyFilesEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream217ProcessEmptyFilesEv,@function
_ZN8NArchive3N7z17CFolderOutStream217ProcessEmptyFilesEv: # @_ZN8NArchive3N7z17CFolderOutStream217ProcessEmptyFilesEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -48
.Lcfi16:
	.cfi_offset %r12, -40
.Lcfi17:
	.cfi_offset %r13, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdi, %r13
	movl	60(%r13), %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_1:                                # %_ZN8NArchive3N7z17CFolderOutStream221CloseFileAndSetResultEv.exit.thread.outer
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%r13), %rcx
	cmpl	12(%rcx), %eax
	jge	.LBB4_16
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	32(%r13), %rdx
	movl	56(%r13), %esi
	addl	%eax, %esi
	movq	176(%rdx), %rdx
	movslq	%esi, %rsi
	movq	(%rdx,%rsi,8), %rdx
	cmpq	$0, (%rdx)
	jne	.LBB4_16
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	16(%r13), %r12
	movq	16(%rcx), %rcx
	cltq
	cmpb	$0, (%rcx,%rax)
	je	.LBB4_7
# BB#4:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	48(%r13), %r15
	testq	%r15, %r15
	je	.LBB4_7
# BB#5:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*8(%rax)
	jmp	.LBB4_8
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_1 Depth=1
	xorl	%r15d, %r15d
.LBB4_8:                                # %.thread.i
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB4_10:                               # %_ZN8NArchive3N7z17CFolderOutStream28OpenFileEv.exit
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	%r15, 16(%r12)
	movq	16(%r13), %rbx
	movq	$0, 24(%rbx)
	movb	$1, 36(%rbx)
	movl	$-1, 32(%rbx)
	movb	$1, 64(%r13)
	movq	32(%r13), %rcx
	movl	60(%r13), %eax
	movl	56(%r13), %edx
	addl	%eax, %edx
	movq	176(%rcx), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %r15
	movq	(%r15), %rcx
	movq	%rcx, 72(%r13)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_12
# BB#11:                                #   in Loop: Header=BB4_1 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%rbx)
	movl	60(%r13), %eax
.LBB4_12:                               # %_ZN8NArchive3N7z17CFolderOutStream29CloseFileEv.exit.i
                                        #   in Loop: Header=BB4_1 Depth=1
	movb	$0, 64(%r13)
	incl	%eax
	movl	%eax, 60(%r13)
	cmpb	$0, 33(%r15)
	jne	.LBB4_1
# BB#13:                                #   in Loop: Header=BB4_1 Depth=1
	cmpb	$0, 34(%r15)
	je	.LBB4_1
# BB#14:                                # %_ZN8NArchive3N7z17CFolderOutStream221CloseFileAndSetResultEv.exit
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	16(%r13), %rcx
	movl	32(%rcx), %edx
	notl	%edx
	xorl	%ecx, %ecx
	cmpl	%edx, 12(%r15)
	setne	%dl
	je	.LBB4_1
# BB#15:
	movb	%dl, %cl
	movl	%ecx, %r14d
.LBB4_16:                               # %.critedge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN8NArchive3N7z17CFolderOutStream217ProcessEmptyFilesEv, .Lfunc_end4-_ZN8NArchive3N7z17CFolderOutStream217ProcessEmptyFilesEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z17CFolderOutStream216ReleaseOutStreamEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream216ReleaseOutStreamEv,@function
_ZN8NArchive3N7z17CFolderOutStream216ReleaseOutStreamEv: # @_ZN8NArchive3N7z17CFolderOutStream216ReleaseOutStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
.Lcfi21:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 48(%rbx)
.LBB5_2:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
	movq	16(%rbx), %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%rbx)
.LBB5_4:                                # %_ZN17COutStreamWithCRC13ReleaseStreamEv.exit
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZN8NArchive3N7z17CFolderOutStream216ReleaseOutStreamEv, .Lfunc_end5-_ZN8NArchive3N7z17CFolderOutStream216ReleaseOutStreamEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z17CFolderOutStream28OpenFileEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream28OpenFileEv,@function
_ZN8NArchive3N7z17CFolderOutStream28OpenFileEv: # @_ZN8NArchive3N7z17CFolderOutStream28OpenFileEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %r15
	movq	40(%rbx), %rax
	movq	16(%rax), %rax
	movslq	60(%rbx), %rcx
	cmpb	$0, (%rax,%rcx)
	je	.LBB6_4
# BB#1:
	movq	48(%rbx), %r14
	testq	%r14, %r14
	je	.LBB6_4
# BB#2:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
	jmp	.LBB6_5
.LBB6_4:
	xorl	%r14d, %r14d
.LBB6_5:                                # %.thread
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB6_7
# BB#6:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB6_7:                                # %_ZN17COutStreamWithCRC9SetStreamEP20ISequentialOutStream.exit
	movq	%r14, 16(%r15)
	movq	16(%rbx), %rax
	movq	$0, 24(%rax)
	movb	$1, 36(%rax)
	movl	$-1, 32(%rax)
	movb	$1, 64(%rbx)
	movq	32(%rbx), %rax
	movl	60(%rbx), %ecx
	addl	56(%rbx), %ecx
	movq	176(%rax), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	movq	%rax, 72(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZN8NArchive3N7z17CFolderOutStream28OpenFileEv, .Lfunc_end6-_ZN8NArchive3N7z17CFolderOutStream28OpenFileEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z17CFolderOutStream29CloseFileEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream29CloseFileEv,@function
_ZN8NArchive3N7z17CFolderOutStream29CloseFileEv: # @_ZN8NArchive3N7z17CFolderOutStream29CloseFileEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %r14
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB7_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%r14)
.LBB7_2:                                # %_ZN17COutStreamWithCRC13ReleaseStreamEv.exit
	movb	$0, 64(%rbx)
	incl	60(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	_ZN8NArchive3N7z17CFolderOutStream29CloseFileEv, .Lfunc_end7-_ZN8NArchive3N7z17CFolderOutStream29CloseFileEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z17CFolderOutStream221CloseFileAndSetResultEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream221CloseFileAndSetResultEv,@function
_ZN8NArchive3N7z17CFolderOutStream221CloseFileAndSetResultEv: # @_ZN8NArchive3N7z17CFolderOutStream221CloseFileAndSetResultEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 32
.Lcfi36:
	.cfi_offset %rbx, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %r15
	movq	32(%rbx), %rcx
	movl	60(%rbx), %eax
	movl	56(%rbx), %edx
	addl	%eax, %edx
	movq	176(%rcx), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %r14
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%r15)
	movl	60(%rbx), %eax
.LBB8_2:                                # %_ZN8NArchive3N7z17CFolderOutStream29CloseFileEv.exit
	movb	$0, 64(%rbx)
	incl	%eax
	movl	%eax, 60(%rbx)
	cmpb	$0, 33(%r14)
	je	.LBB8_4
.LBB8_6:
	xorl	%eax, %eax
	jmp	.LBB8_7
.LBB8_4:
	cmpb	$0, 34(%r14)
	je	.LBB8_6
# BB#5:
	movq	16(%rbx), %rax
	movl	32(%rax), %eax
	notl	%eax
	cmpl	%eax, 12(%r14)
	setne	%al
.LBB8_7:
	movzbl	%al, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN8NArchive3N7z17CFolderOutStream221CloseFileAndSetResultEv, .Lfunc_end8-_ZN8NArchive3N7z17CFolderOutStream221CloseFileAndSetResultEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z17CFolderOutStream25WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream25WriteEPKvjPj,@function
_ZN8NArchive3N7z17CFolderOutStream25WriteEPKvjPj: # @_ZN8NArchive3N7z17CFolderOutStream25WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 80
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movl	%edx, %r14d
	movq	%rsi, %r12
	movq	%rdi, %rbx
	testq	%r13, %r13
	je	.LBB9_2
# BB#1:
	movl	$0, (%r13)
.LBB9_2:                                # %.preheader
	testl	%r14d, %r14d
	je	.LBB9_30
# BB#3:                                 # %.lr.ph.lr.ph
                                        # implicit-def: %R15D
	movq	%r13, 16(%rsp)          # 8-byte Spill
.LBB9_4:                                # %.lr.ph.split.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 64(%rbx)
	jne	.LBB9_15
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z17CFolderOutStream217ProcessEmptyFilesEv
	testl	%eax, %eax
	cmovnel	%eax, %r15d
	jne	.LBB9_33
# BB#6:                                 #   in Loop: Header=BB9_4 Depth=1
	movslq	60(%rbx), %rax
	movq	40(%rbx), %rcx
	cmpl	12(%rcx), %eax
	je	.LBB9_34
# BB#7:                                 #   in Loop: Header=BB9_4 Depth=1
	movq	16(%rbx), %rbp
	movq	16(%rcx), %rcx
	cmpb	$0, (%rcx,%rax)
	je	.LBB9_11
# BB#8:                                 #   in Loop: Header=BB9_4 Depth=1
	movq	48(%rbx), %r13
	testq	%r13, %r13
	je	.LBB9_11
# BB#9:                                 #   in Loop: Header=BB9_4 Depth=1
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*8(%rax)
	jmp	.LBB9_12
	.p2align	4, 0x90
.LBB9_11:                               #   in Loop: Header=BB9_4 Depth=1
	xorl	%r13d, %r13d
.LBB9_12:                               # %.thread.i
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_14
# BB#13:                                #   in Loop: Header=BB9_4 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB9_14:                               # %_ZN8NArchive3N7z17CFolderOutStream28OpenFileEv.exit
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	%r13, 16(%rbp)
	movq	16(%rbx), %rax
	movq	$0, 24(%rax)
	movb	$1, 36(%rax)
	movl	$-1, 32(%rax)
	movb	$1, 64(%rbx)
	movq	32(%rbx), %rax
	movl	60(%rbx), %ecx
	addl	56(%rbx), %ecx
	movq	176(%rax), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	movq	%rax, 72(%rbx)
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB9_15:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB9_4 Depth=1
	movl	%r14d, %eax
	movq	72(%rbx), %rdx
	cmpq	%rdx, %rax
	cmovbl	%r14d, %edx
	movl	%edx, 8(%rsp)
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	leaq	8(%rsp), %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB9_33
# BB#16:                                #   in Loop: Header=BB9_4 Depth=1
	movl	8(%rsp), %ebp
	testq	%rbp, %rbp
	je	.LBB9_30
# BB#17:                                #   in Loop: Header=BB9_4 Depth=1
	movq	72(%rbx), %rax
	subq	%rbp, %rax
	movq	%rax, 72(%rbx)
	testq	%r13, %r13
	je	.LBB9_19
# BB#18:                                #   in Loop: Header=BB9_4 Depth=1
	addl	%ebp, (%r13)
.LBB9_19:                               #   in Loop: Header=BB9_4 Depth=1
	testq	%rax, %rax
	jne	.LBB9_29
# BB#20:                                #   in Loop: Header=BB9_4 Depth=1
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	movq	16(%rbx), %r15
	movq	32(%rbx), %rcx
	movl	60(%rbx), %eax
	movl	56(%rbx), %edx
	addl	%eax, %edx
	movq	176(%rcx), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %r13
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB9_22
# BB#21:                                #   in Loop: Header=BB9_4 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%r15)
	movl	60(%rbx), %eax
.LBB9_22:                               # %_ZN8NArchive3N7z17CFolderOutStream29CloseFileEv.exit.i
                                        #   in Loop: Header=BB9_4 Depth=1
	movb	$0, 64(%rbx)
	incl	%eax
	movl	%eax, 60(%rbx)
	cmpb	$0, 33(%r13)
	jne	.LBB9_25
# BB#23:                                #   in Loop: Header=BB9_4 Depth=1
	cmpb	$0, 34(%r13)
	je	.LBB9_25
# BB#24:                                # %_ZN8NArchive3N7z17CFolderOutStream221CloseFileAndSetResultEv.exit
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	16(%rbx), %rax
	movl	32(%rax), %ecx
	notl	%ecx
	xorl	%r15d, %r15d
	cmpl	%ecx, 12(%r13)
	setne	%cl
	jne	.LBB9_35
.LBB9_25:                               #   in Loop: Header=BB9_4 Depth=1
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z17CFolderOutStream217ProcessEmptyFilesEv
	testl	%eax, %eax
	setne	%cl
	movl	12(%rsp), %r15d         # 4-byte Reload
	cmovnel	%eax, %r15d
	movb	$2, %al
	je	.LBB9_27
# BB#26:                                #   in Loop: Header=BB9_4 Depth=1
	movl	%ecx, %eax
.LBB9_27:                               #   in Loop: Header=BB9_4 Depth=1
	cmpb	$1, %al
	movq	16(%rsp), %r13          # 8-byte Reload
	je	.LBB9_31
# BB#28:                                #   in Loop: Header=BB9_4 Depth=1
	cmpb	$3, %al
	je	.LBB9_30
.LBB9_29:                               # %.outer.backedge
                                        #   in Loop: Header=BB9_4 Depth=1
	addq	%rbp, %r12
	subl	%ebp, %r14d
	jne	.LBB9_4
.LBB9_30:                               # %.thread49
	xorl	%r15d, %r15d
.LBB9_31:                               # %.loopexit
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_33:
	movl	%eax, %r15d
	jmp	.LBB9_31
.LBB9_34:
	movl	$-2147467259, %r15d     # imm = 0x80004005
	jmp	.LBB9_31
.LBB9_35:
	movb	%cl, %r15b
	jmp	.LBB9_31
.Lfunc_end9:
	.size	_ZN8NArchive3N7z17CFolderOutStream25WriteEPKvjPj, .Lfunc_end9-_ZN8NArchive3N7z17CFolderOutStream25WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN8NArchive3N7z14CThreadDecoder7ExecuteEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z14CThreadDecoder7ExecuteEv,@function
_ZN8NArchive3N7z14CThreadDecoder7ExecuteEv: # @_ZN8NArchive3N7z14CThreadDecoder7ExecuteEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -24
.Lcfi56:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	296(%rbx), %rdi
	movq	264(%rbx), %rdx
	movq	272(%rbx), %rcx
	movq	280(%rbx), %r8
	movq	240(%rbx), %rsi
	movq	256(%rbx), %r9
	movq	288(%rbx), %r10
	movl	532(%rbx), %eax
	movzbl	528(%rbx), %r11d
.Ltmp0:
.Lcfi57:
	.cfi_escape 0x2e, 0x30
	subq	$8, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	leaq	15(%rsp), %r14
	pushq	%rax
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	callq	_ZN8NArchive3N7z8CDecoder6DecodeEP9IInStreamyPKyRKNS0_7CFolderEP20ISequentialOutStreamP21ICompressProgressInfoP22ICryptoGetTextPasswordRbbj
	addq	$48, %rsp
.Lcfi64:
	.cfi_adjust_cfa_offset -48
.Ltmp1:
# BB#1:
	leaq	236(%rbx), %r14
	movl	%eax, 236(%rbx)
.LBB10_3:
	movq	248(%rbx), %rbx
	testl	%eax, %eax
	jne	.LBB10_5
# BB#4:
	movl	60(%rbx), %eax
	movq	40(%rbx), %rcx
	xorl	%edx, %edx
	cmpl	12(%rcx), %eax
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovel	%edx, %eax
	movl	%eax, (%r14)
.LBB10_5:                               # %._crit_edge
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_7
# BB#6:
	movq	(%rdi), %rax
.Lcfi65:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
	movq	$0, 48(%rbx)
.LBB10_7:                               # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit.i
	movq	16(%rbx), %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_9
# BB#8:
	movq	(%rdi), %rax
.Lcfi66:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
	movq	$0, 16(%rbx)
.LBB10_9:                               # %_ZN8NArchive3N7z17CFolderOutStream216ReleaseOutStreamEv.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB10_2:
.Ltmp2:
.Lcfi67:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	leaq	236(%rbx), %r14
	movl	$-2147467259, 236(%rbx) # imm = 0x80004005
.Lcfi68:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	movl	236(%rbx), %eax
	jmp	.LBB10_3
.Lfunc_end10:
	.size	_ZN8NArchive3N7z14CThreadDecoder7ExecuteEv, .Lfunc_end10-_ZN8NArchive3N7z14CThreadDecoder7ExecuteEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\242\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	1                       #   On action: 1
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end10-.Ltmp1     #   Call between .Ltmp1 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z22CCryptoGetTextPassword21CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCryptoGetTextPassword21CryptoGetTextPasswordEPPw,@function
_ZN8NArchive3N7z22CCryptoGetTextPassword21CryptoGetTextPasswordEPPw: # @_ZN8NArchive3N7z22CCryptoGetTextPassword21CryptoGetTextPasswordEPPw
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 16
.Lcfi70:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	16(%rdi), %rdi
	callq	SysAllocString
	movq	%rax, (%rbx)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$-2147024882, %eax      # imm = 0x8007000E
	cmovnel	%ecx, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	_ZN8NArchive3N7z22CCryptoGetTextPassword21CryptoGetTextPasswordEPPw, .Lfunc_end11-_ZN8NArchive3N7z22CCryptoGetTextPassword21CryptoGetTextPasswordEPPw
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.quad	2                       # 0x2
	.quad	4294967296              # 0x100000000
.LCPI12_1:
	.quad	3                       # 0x3
	.quad	8589934592              # 0x200000000
.LCPI12_2:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z6UpdateEP9IInStreamPKNS0_18CArchiveDatabaseExERK13CObjectVectorINS0_11CUpdateItemEERNS0_11COutArchiveERNS0_16CArchiveDatabaseEP20ISequentialOutStreamP22IArchiveUpdateCallbackRKNS0_14CUpdateOptionsEP22ICryptoGetTextPassword
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z6UpdateEP9IInStreamPKNS0_18CArchiveDatabaseExERK13CObjectVectorINS0_11CUpdateItemEERNS0_11COutArchiveERNS0_16CArchiveDatabaseEP20ISequentialOutStreamP22IArchiveUpdateCallbackRKNS0_14CUpdateOptionsEP22ICryptoGetTextPassword,@function
_ZN8NArchive3N7z6UpdateEP9IInStreamPKNS0_18CArchiveDatabaseExERK13CObjectVectorINS0_11CUpdateItemEERNS0_11COutArchiveERNS0_16CArchiveDatabaseEP20ISequentialOutStreamP22IArchiveUpdateCallbackRKNS0_14CUpdateOptionsEP22ICryptoGetTextPassword: # @_ZN8NArchive3N7z6UpdateEP9IInStreamPKNS0_18CArchiveDatabaseExERK13CObjectVectorINS0_11CUpdateItemEERNS0_11COutArchiveERNS0_16CArchiveDatabaseEP20ISequentialOutStreamP22IArchiveUpdateCallbackRKNS0_14CUpdateOptionsEP22ICryptoGetTextPassword
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	subq	$3480, %rsp             # imm = 0xD98
.Lcfi77:
	.cfi_def_cfa_offset 3536
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	movq	3544(%rsp), %rdx
	movq	24(%rdx), %rax
	testq	%rax, %rax
	movl	$1, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	testq	%r14, %r14
	je	.LBB12_3
# BB#1:
	movq	488(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB12_3
# BB#2:
	cmpb	$0, 41(%rdx)
	je	.LBB12_73
.LBB12_3:                               # %.thread
	testq	%r14, %r14
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 400(%rsp)
	movq	$4, 416(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 392(%rsp)
	movdqu	%xmm0, 368(%rsp)
	movq	$12, 384(%rsp)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z13CFolderRepackEE+16, 360(%rsp)
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	%rbx, 184(%rsp)         # 8-byte Spill
	je	.LBB12_63
# BB#4:
	movl	172(%r14), %esi
.Ltmp3:
.Lcfi84:
	.cfi_escape 0x2e, 0x00
	leaq	392(%rsp), %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp4:
# BB#5:                                 # %.preheader1014
	cmpl	$0, 172(%r14)
	jle	.LBB12_9
# BB#6:                                 # %.lr.ph1817
	xorl	%ebp, %ebp
	leaq	392(%rsp), %rbx
	.p2align	4, 0x90
.LBB12_7:                               # =>This Inner Loop Header: Depth=1
.Ltmp6:
.Lcfi85:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp7:
# BB#8:                                 #   in Loop: Header=BB12_7 Depth=1
	movq	408(%rsp), %rax
	movslq	404(%rsp), %rcx
	movl	$-1, (%rax,%rcx,4)
	incl	404(%rsp)
	incl	%ebp
	cmpl	172(%r14), %ebp
	jl	.LBB12_7
.LBB12_9:                               # %.preheader1013
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax), %rbp
	movl	12(%rax), %eax
	testl	%eax, %eax
	jle	.LBB12_14
# BB#10:                                # %.lr.ph1815
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rcx
	movq	408(%rsp), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB12_11:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rdi
	movslq	(%rdi), %rdi
	cmpq	$-1, %rdi
	je	.LBB12_13
# BB#12:                                #   in Loop: Header=BB12_11 Depth=1
	movl	%esi, (%rdx,%rdi,4)
	movl	(%rbp), %eax
.LBB12_13:                              #   in Loop: Header=BB12_11 Depth=1
	incq	%rsi
	movslq	%eax, %rdi
	cmpq	%rdi, %rsi
	jl	.LBB12_11
.LBB12_14:                              # %.preheader1012
	movq	%rbp, 152(%rsp)         # 8-byte Spill
	cmpl	$0, 108(%r14)
	jle	.LBB12_64
# BB#15:                                # %.lr.ph1809
	xorl	%r15d, %r15d
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB12_21
.LBB12_63:                              # %.thread._crit_edge
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax), %rbx
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	jmp	.LBB12_66
.LBB12_64:
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	movq	88(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB12_65
.LBB12_73:
.Lcfi86:
	.cfi_escape 0x2e, 0x00
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, %rsi
	callq	_ZN8NArchive3N7zL10WriteRangeEP9IInStreamP20ISequentialOutStreamyyP21ICompressProgressInfo
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB12_546
	jmp	.LBB12_3
.LBB12_18:                              #   in Loop: Header=BB12_21 Depth=1
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	je	.LBB12_20
	.p2align	4, 0x90
.LBB12_19:                              # %vector.body
                                        #   Parent Loop BB12_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r9,%rdx), %rax
	cltq
	movdqu	(%rcx,%rax,8), %xmm2
	movdqu	16(%rcx,%rax,8), %xmm3
	paddq	%xmm0, %xmm2
	paddq	%xmm1, %xmm3
	addl	$4, %eax
	cltq
	movdqu	(%rcx,%rax,8), %xmm0
	movdqu	16(%rcx,%rax,8), %xmm1
	paddq	%xmm2, %xmm0
	paddq	%xmm3, %xmm1
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	jne	.LBB12_19
.LBB12_20:                              # %middle.block
                                        #   in Loop: Header=BB12_21 Depth=1
	paddq	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %rdi
	cmpq	%rsi, %rbp
	movq	%rsi, %rdx
	jne	.LBB12_53
	jmp	.LBB12_61
	.p2align	4, 0x90
.LBB12_21:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_23 Depth 2
                                        #     Child Loop BB12_30 Depth 2
                                        #     Child Loop BB12_36 Depth 2
                                        #     Child Loop BB12_19 Depth 2
                                        #     Child Loop BB12_55 Depth 2
                                        #     Child Loop BB12_58 Depth 2
	movq	144(%rdx), %rax
	movl	(%rax,%r15,4), %r12d
	testl	%r12d, %r12d
	je	.LBB12_62
# BB#22:                                # %.lr.ph1800
                                        #   in Loop: Header=BB12_21 Depth=1
	movq	176(%rdx), %rax
	movq	632(%rdx), %rcx
	movl	(%rcx,%r15,4), %ecx
	movq	408(%rsp), %rdx
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsi), %rsi
	xorl	%edi, %edi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB12_23:                              #   Parent Loop BB12_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ecx, %rbx
	movq	(%rax,%rbx,8), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB12_27
# BB#24:                                #   in Loop: Header=BB12_23 Depth=2
	incl	%edi
	movslq	(%rdx,%rbx,4), %rbx
	testq	%rbx, %rbx
	js	.LBB12_27
# BB#25:                                #   in Loop: Header=BB12_23 Depth=2
	movq	(%rsi,%rbx,8), %rbx
	cmpb	$0, 60(%rbx)
	jne	.LBB12_27
# BB#26:                                #   in Loop: Header=BB12_23 Depth=2
	incl	%r14d
	addq	(%rbp), %r13
	.p2align	4, 0x90
.LBB12_27:                              #   in Loop: Header=BB12_23 Depth=2
	incl	%ecx
	cmpl	%r12d, %edi
	jb	.LBB12_23
# BB#28:                                # %._crit_edge1801
                                        #   in Loop: Header=BB12_21 Depth=1
	testl	%r14d, %r14d
	je	.LBB12_62
# BB#29:                                #   in Loop: Header=BB12_21 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	112(%rax), %rax
	movq	(%rax,%r15,8), %rcx
	movslq	12(%rcx), %rax
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB12_30:                              #   Parent Loop BB12_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rdx, %rdx
	jle	.LBB12_33
# BB#31:                                #   in Loop: Header=BB12_30 Depth=2
	movq	16(%rcx), %rsi
	movq	-8(%rsi,%rdx,8), %rsi
	decq	%rdx
	cmpq	$116459265, (%rsi)      # imm = 0x6F10701
	jne	.LBB12_30
# BB#32:                                #   in Loop: Header=BB12_21 Depth=1
	movb	$1, %bl
	jmp	.LBB12_34
.LBB12_33:                              #   in Loop: Header=BB12_21 Depth=1
	xorl	%ebx, %ebx
.LBB12_34:                              # %_ZNK8NArchive3N7z7CFolder11IsEncryptedEv.exit
                                        #   in Loop: Header=BB12_21 Depth=1
	testl	%eax, %eax
	movq	%r11, 72(%rsp)          # 8-byte Spill
	jle	.LBB12_39
# BB#35:                                # %.lr.ph.i
                                        #   in Loop: Header=BB12_21 Depth=1
	movq	16(%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_36:                              #   Parent Loop BB12_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	movq	(%rsi), %rsi
	cmpq	$50528515, %rsi         # imm = 0x3030103
	je	.LBB12_17
# BB#37:                                #   in Loop: Header=BB12_36 Depth=2
	cmpq	$50528539, %rsi         # imm = 0x303011B
	je	.LBB12_17
# BB#38:                                #   in Loop: Header=BB12_36 Depth=2
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB12_36
.LBB12_39:                              #   in Loop: Header=BB12_21 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB12_40
.LBB12_17:                              #   in Loop: Header=BB12_21 Depth=1
	movabsq	$4294967296, %rbp       # imm = 0x100000000
.LBB12_40:                              # %_ZN8NArchive3N7zL18Is86FilteredFolderERKNS0_7CFolderE.exit
                                        #   in Loop: Header=BB12_21 Depth=1
.Ltmp9:
.Lcfi87:
	.cfi_escape 0x2e, 0x00
	leaq	360(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp10:
# BB#41:                                #   in Loop: Header=BB12_21 Depth=1
	movzbl	%bl, %eax
	shlq	$33, %rax
	orq	%r15, %rax
	orq	%rbp, %rax
	movq	376(%rsp), %rcx
	movslq	372(%rsp), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movq	%rax, (%rcx,%rdx,4)
	movl	%r14d, 8(%rcx,%rdx,4)
	incl	372(%rsp)
	cmpl	%r12d, %r14d
	movq	72(%rsp), %r11          # 8-byte Reload
	jne	.LBB12_49
# BB#42:                                #   in Loop: Header=BB12_21 Depth=1
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	112(%rcx), %rax
	movq	(%rax,%r15,8), %rax
	movslq	76(%rax), %rbp
	testq	%rbp, %rbp
	jle	.LBB12_60
# BB#43:                                # %.lr.ph.i798
                                        #   in Loop: Header=BB12_21 Depth=1
	movq	600(%rcx), %rax
	movslq	(%rax,%r15,4), %r8
	movl	%r8d, %r9d
	movq	16(%rcx), %rcx
	cmpl	$4, %ebp
	jb	.LBB12_52
# BB#44:                                # %min.iters.checked
                                        #   in Loop: Header=BB12_21 Depth=1
	movq	%rbp, %rsi
	andq	$-4, %rsi
	je	.LBB12_52
# BB#45:                                # %vector.scevcheck
                                        #   in Loop: Header=BB12_21 Depth=1
	leaq	-1(%rbp), %rax
	leal	(%rax,%r8), %edx
	cmpl	%r9d, %edx
	jl	.LBB12_52
# BB#46:                                # %vector.scevcheck
                                        #   in Loop: Header=BB12_21 Depth=1
	shrq	$32, %rax
	movl	$0, %edx
	movl	$0, %edi
	jne	.LBB12_53
# BB#47:                                # %vector.body.preheader
                                        #   in Loop: Header=BB12_21 Depth=1
	leaq	-4(%rsi), %rdx
	movq	%rdx, %rax
	shrq	$2, %rax
	btl	$2, %edx
	jb	.LBB12_18
# BB#48:                                # %vector.body.prol
                                        #   in Loop: Header=BB12_21 Depth=1
	movdqu	(%rcx,%r8,8), %xmm0
	movdqu	16(%rcx,%r8,8), %xmm1
	movl	$4, %edx
	testq	%rax, %rax
	jne	.LBB12_19
	jmp	.LBB12_20
.LBB12_49:                              #   in Loop: Header=BB12_21 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, %r13
	cmovaq	%r13, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testb	%bl, %bl
	movb	$1, %al
	jne	.LBB12_51
# BB#50:                                #   in Loop: Header=BB12_21 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
.LBB12_51:                              #   in Loop: Header=BB12_21 Depth=1
	addq	%r13, %r11
                                        # kill: %AL<def> %AL<kill> %RAX<def>
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB12_62
.LBB12_52:                              #   in Loop: Header=BB12_21 Depth=1
	xorl	%edx, %edx
	xorl	%edi, %edi
.LBB12_53:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB12_21 Depth=1
	movl	%ebp, %eax
	subl	%edx, %eax
	leaq	-1(%rbp), %r10
	subq	%rdx, %r10
	andq	$3, %rax
	je	.LBB12_56
# BB#54:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB12_21 Depth=1
	leal	(%rdx,%r9), %esi
	negq	%rax
	.p2align	4, 0x90
.LBB12_55:                              # %scalar.ph.prol
                                        #   Parent Loop BB12_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rsi
	addq	(%rcx,%rsi,8), %rdi
	incq	%rdx
	incl	%esi
	incq	%rax
	jne	.LBB12_55
.LBB12_56:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB12_21 Depth=1
	cmpq	$3, %r10
	jb	.LBB12_61
# BB#57:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB12_21 Depth=1
	subq	%rdx, %rbp
	movl	%r8d, %eax
	leaq	3(%rax,%rdx), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_58:                              # %scalar.ph
                                        #   Parent Loop BB12_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-3(%rax,%rdx), %esi
	movslq	%esi, %rsi
	addq	(%rcx,%rsi,8), %rdi
	leal	-2(%rax,%rdx), %esi
	movslq	%esi, %rsi
	addq	(%rcx,%rsi,8), %rdi
	leal	-1(%rax,%rdx), %esi
	movslq	%esi, %rsi
	addq	(%rcx,%rsi,8), %rdi
	leal	(%rax,%rdx), %esi
	movslq	%esi, %rsi
	addq	(%rcx,%rsi,8), %rdi
	addq	$4, %rdx
	cmpq	%rdx, %rbp
	jne	.LBB12_58
	jmp	.LBB12_61
.LBB12_60:                              #   in Loop: Header=BB12_21 Depth=1
	xorl	%edi, %edi
.LBB12_61:                              # %._crit_edge1801.thread
                                        #   in Loop: Header=BB12_21 Depth=1
	addq	%rdi, %r11
	.p2align	4, 0x90
.LBB12_62:                              # %._crit_edge1801.thread
                                        #   in Loop: Header=BB12_21 Depth=1
	incq	%r15
	movq	88(%rsp), %rdx          # 8-byte Reload
	movslq	108(%rdx), %rax
	cmpq	%rax, %r15
	jl	.LBB12_21
.LBB12_65:                              # %._crit_edge1810
.Ltmp12:
.Lcfi88:
	.cfi_escape 0x2e, 0x00
	leaq	360(%rsp), %rdi
	movl	$_ZN8NArchive3N7zL20CompareFolderRepacksEPKNS0_13CFolderRepackES3_Pv, %esi
	movq	%r11, %rbx
	callq	_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE4SortEPFiPKS2_S5_PvES6_
	movq	%rbx, %rsi
.Ltmp13:
	movq	152(%rsp), %rbx         # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB12_66:
	movq	$0, 96(%rsp)
	movslq	(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB12_70
# BB#67:                                # %.lr.ph1791
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rdx
	cmpq	$1, 352(%rsp)           # 8-byte Folded Reload
	jne	.LBB12_71
# BB#68:                                # %.lr.ph1791.split.us.preheader
	testb	$1, %al
	jne	.LBB12_74
# BB#69:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	cmpl	$1, %eax
	jne	.LBB12_80
	jmp	.LBB12_99
.LBB12_70:
	xorl	%ecx, %ecx
	cmpq	%rcx, %rbp
	ja	.LBB12_100
	jmp	.LBB12_101
.LBB12_71:                              # %.lr.ph1791.split.preheader
	testb	$1, %al
	jne	.LBB12_77
# BB#72:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	cmpl	$1, %eax
	jne	.LBB12_93
	jmp	.LBB12_99
.LBB12_74:                              # %.lr.ph1791.split.us.prol
	movq	(%rdx), %rcx
	cmpb	$0, 60(%rcx)
	je	.LBB12_79
# BB#75:
	movq	32(%rcx), %rcx
	addq	%rcx, %rsi
	testq	%rcx, %rcx
	je	.LBB12_79
# BB#76:
	movq	%rcx, 96(%rsp)
	movl	$1, %edi
	cmpl	$1, %eax
	jne	.LBB12_80
	jmp	.LBB12_99
.LBB12_77:                              # %.lr.ph1791.split.prol
	movq	(%rdx), %rcx
	cmpb	$0, 60(%rcx)
	je	.LBB12_92
# BB#78:
	movq	32(%rcx), %rcx
	addq	%rcx, %rsi
	movq	%rcx, 96(%rsp)
	movl	$1, %edi
	cmpl	$1, %eax
	jne	.LBB12_93
	jmp	.LBB12_99
.LBB12_79:
	movl	$1, %edi
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	je	.LBB12_99
.LBB12_80:                              # %.lr.ph1791.split.us.preheader.new
	subq	%rdi, %rax
	leaq	8(%rdx,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB12_81:                              # %.lr.ph1791.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %rdi
	cmpb	$0, 60(%rdi)
	je	.LBB12_85
# BB#82:                                #   in Loop: Header=BB12_81 Depth=1
	movq	32(%rdi), %rdi
	addq	%rdi, %rsi
	cmpq	%rcx, %rdi
	jbe	.LBB12_85
# BB#83:                                #   in Loop: Header=BB12_81 Depth=1
	movq	%rdi, 96(%rsp)
	jmp	.LBB12_86
	.p2align	4, 0x90
.LBB12_85:                              #   in Loop: Header=BB12_81 Depth=1
	movq	%rcx, %rdi
.LBB12_86:                              # %.lr.ph1791.split.us.13186
                                        #   in Loop: Header=BB12_81 Depth=1
	movq	(%rdx), %rcx
	cmpb	$0, 60(%rcx)
	je	.LBB12_90
# BB#87:                                #   in Loop: Header=BB12_81 Depth=1
	movq	32(%rcx), %rcx
	addq	%rcx, %rsi
	cmpq	%rdi, %rcx
	jbe	.LBB12_90
# BB#88:                                #   in Loop: Header=BB12_81 Depth=1
	movq	%rcx, 96(%rsp)
	jmp	.LBB12_91
	.p2align	4, 0x90
.LBB12_90:                              #   in Loop: Header=BB12_81 Depth=1
	movq	%rdi, %rcx
.LBB12_91:                              #   in Loop: Header=BB12_81 Depth=1
	addq	$16, %rdx
	addq	$-2, %rax
	jne	.LBB12_81
	jmp	.LBB12_99
.LBB12_92:
	movl	$1, %edi
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	je	.LBB12_99
.LBB12_93:                              # %.lr.ph1791.split.preheader.new
	subq	%rdi, %rax
	leaq	8(%rdx,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB12_94:                              # %.lr.ph1791.split
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %rdi
	cmpb	$0, 60(%rdi)
	je	.LBB12_96
# BB#95:                                #   in Loop: Header=BB12_94 Depth=1
	movq	32(%rdi), %rdi
	addq	%rdi, %rsi
	addq	%rdi, %rcx
	movq	%rcx, 96(%rsp)
.LBB12_96:                              # %.lr.ph1791.split.13190
                                        #   in Loop: Header=BB12_94 Depth=1
	movq	(%rdx), %rdi
	cmpb	$0, 60(%rdi)
	je	.LBB12_98
# BB#97:                                #   in Loop: Header=BB12_94 Depth=1
	movq	32(%rdi), %rdi
	addq	%rdi, %rsi
	addq	%rdi, %rcx
	movq	%rcx, 96(%rsp)
.LBB12_98:                              #   in Loop: Header=BB12_94 Depth=1
	addq	$16, %rdx
	addq	$-2, %rax
	jne	.LBB12_94
.LBB12_99:                              # %._crit_edge1792
	cmpq	%rcx, %rbp
	jbe	.LBB12_101
.LBB12_100:
	movq	%rbp, 96(%rsp)
	movq	%rbp, %rcx
.LBB12_101:
	cmpq	$65535, %rcx            # imm = 0xFFFF
	ja	.LBB12_103
# BB#102:
	movq	$65536, 96(%rsp)        # imm = 0x10000
.LBB12_103:
	movq	3536(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp15:
.Lcfi89:
	.cfi_escape 0x2e, 0x00
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp16:
# BB#104:
	testl	%ebp, %ebp
	jne	.LBB12_544
# BB#105:
.Ltmp17:
.Lcfi90:
	.cfi_escape 0x2e, 0x00
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp18:
# BB#106:
.Ltmp20:
.Lcfi91:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp21:
# BB#107:
	movq	(%rbp), %rax
.Ltmp23:
.Lcfi92:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp24:
# BB#108:                               # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp26:
.Lcfi93:
	.cfi_escape 0x2e, 0x00
	movl	$1, %edx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rdi
	movq	3536(%rsp), %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp27:
# BB#109:
.Ltmp28:
.Lcfi94:
	.cfi_escape 0x2e, 0x00
	leaq	1856(%rsp), %rdi
	callq	_ZN8NArchive3N7z14CThreadDecoderC2Ev
.Ltmp29:
# BB#110:
	cmpl	$0, 372(%rsp)
	je	.LBB12_113
# BB#111:
.Ltmp31:
.Lcfi95:
	.cfi_escape 0x2e, 0x00
	leaq	1856(%rsp), %rdi
	callq	_ZN11CVirtThread6CreateEv
	movl	%eax, %ebp
.Ltmp32:
# BB#112:
	testl	%ebp, %ebp
	jne	.LBB12_542
.LBB12_113:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 120(%rsp)
	movq	$8, 136(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE+16, 112(%rsp)
	movdqu	%xmm0, 264(%rsp)
	movq	$4, 280(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 256(%rsp)
.Ltmp34:
.Lcfi96:
	.cfi_escape 0x2e, 0x00
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp35:
# BB#114:                               # %.noexc805
.Ltmp36:
.Lcfi97:
	.cfi_escape 0x2e, 0x00
	leaq	256(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_ZN13CRecordVectorIjEC2ERKS0_
.Ltmp37:
# BB#115:                               # %_ZN8NArchive3N7z11CSolidGroupC2ERKS1_.exit.i
.Ltmp38:
.Lcfi98:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp39:
# BB#116:
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 124(%rsp)
.Ltmp40:
.Lcfi99:
	.cfi_escape 0x2e, 0x00
	leaq	256(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp41:
# BB#117:                               # %_ZN8NArchive3N7z11CSolidGroupD2Ev.exit
	leaq	264(%rsp), %r14
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r14)
	movq	$4, 280(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 256(%rsp)
.Ltmp42:
.Lcfi100:
	.cfi_escape 0x2e, 0x00
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp43:
# BB#118:                               # %.noexc805.1
.Ltmp44:
.Lcfi101:
	.cfi_escape 0x2e, 0x00
	leaq	256(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_ZN13CRecordVectorIjEC2ERKS0_
.Ltmp45:
# BB#119:                               # %_ZN8NArchive3N7z11CSolidGroupC2ERKS1_.exit.i.1
.Ltmp46:
.Lcfi102:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp47:
# BB#120:
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 124(%rsp)
.Ltmp48:
.Lcfi103:
	.cfi_escape 0x2e, 0x00
	leaq	256(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp49:
# BB#121:                               # %_ZN8NArchive3N7z11CSolidGroupD2Ev.exit.1
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r14)
	movq	$4, 280(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 256(%rsp)
.Ltmp50:
.Lcfi104:
	.cfi_escape 0x2e, 0x00
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp51:
# BB#122:                               # %.noexc805.2
.Ltmp52:
.Lcfi105:
	.cfi_escape 0x2e, 0x00
	leaq	256(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_ZN13CRecordVectorIjEC2ERKS0_
.Ltmp53:
# BB#123:                               # %_ZN8NArchive3N7z11CSolidGroupC2ERKS1_.exit.i.2
.Ltmp54:
.Lcfi106:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp55:
# BB#124:
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 124(%rsp)
.Ltmp56:
.Lcfi107:
	.cfi_escape 0x2e, 0x00
	leaq	256(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp57:
# BB#125:                               # %_ZN8NArchive3N7z11CSolidGroupD2Ev.exit.2
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r14)
	movq	$4, 280(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 256(%rsp)
.Ltmp58:
.Lcfi108:
	.cfi_escape 0x2e, 0x00
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp59:
# BB#126:                               # %.noexc805.3
.Ltmp60:
.Lcfi109:
	.cfi_escape 0x2e, 0x00
	leaq	256(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_ZN13CRecordVectorIjEC2ERKS0_
.Ltmp61:
# BB#127:                               # %_ZN8NArchive3N7z11CSolidGroupC2ERKS1_.exit.i.3
.Ltmp63:
.Lcfi110:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp64:
# BB#128:
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 124(%rsp)
.Ltmp68:
.Lcfi111:
	.cfi_escape 0x2e, 0x00
	leaq	256(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp69:
# BB#129:                               # %_ZN8NArchive3N7z11CSolidGroupD2Ev.exit.3
	movq	3544(%rsp), %rax
	movq	(%rax), %r12
	cmpl	$1, 12(%r12)
	jne	.LBB12_131
# BB#130:
	cmpl	$0, 44(%r12)
	je	.LBB12_143
.LBB12_131:
	xorl	%eax, %eax
.LBB12_132:
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.LBB12_166
# BB#133:                               # %.lr.ph1780
	testb	%al, %al
	je	.LBB12_135
# BB#134:                               # %.lr.ph1780.split.preheader
	xorl	%r14d, %r14d
	leaq	2392(%rsp), %r13
	leaq	1344(%rsp), %rbp
	leaq	1072(%rsp), %r15
	jmp	.LBB12_146
.LBB12_135:                             # %.lr.ph1780.split.us.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_136:                             # %.lr.ph1780.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	cmpb	$0, 60(%rax)
	je	.LBB12_142
# BB#137:                               #   in Loop: Header=BB12_136 Depth=1
	cmpb	$0, 63(%rax)
	jne	.LBB12_142
# BB#138:                               #   in Loop: Header=BB12_136 Depth=1
	cmpb	$0, 62(%rax)
	jne	.LBB12_142
# BB#139:                               # %_ZNK8NArchive3N7z11CUpdateItem9HasStreamEv.exit813.us
                                        #   in Loop: Header=BB12_136 Depth=1
	cmpq	$0, 32(%rax)
	je	.LBB12_142
# BB#140:                               # %_ZN8NArchive3N7zL9IsExeFileERKNS0_11CUpdateItemE.exit.us
                                        #   in Loop: Header=BB12_136 Depth=1
	movq	128(%rsp), %rax
	movzbl	68(%r12), %ecx
	shlq	$4, %rcx
	movq	(%rax,%rcx), %rbx
.Ltmp89:
.Lcfi112:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp90:
# BB#141:                               # %_ZN13CRecordVectorIjE3AddEj.exit.us
                                        #   in Loop: Header=BB12_136 Depth=1
	movq	16(%rbx), %rax
	movslq	12(%rbx), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	12(%rbx)
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	.p2align	4, 0x90
.LBB12_142:                             # %_ZNK8NArchive3N7z11CUpdateItem9HasStreamEv.exit813.thread.us
                                        #   in Loop: Header=BB12_136 Depth=1
	incq	%rbp
	movslq	%ecx, %rax
	cmpq	%rax, %rbp
	jl	.LBB12_136
	jmp	.LBB12_166
.LBB12_143:
	movb	16(%rax), %al
	jmp	.LBB12_132
.LBB12_144:                             # %.critedge.i
                                        #   in Loop: Header=BB12_146 Depth=1
.Ltmp84:
.Lcfi113:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp85:
# BB#145:                               # %.noexc816
                                        #   in Loop: Header=BB12_146 Depth=1
	movl	$1, %eax
	jmp	.LBB12_163
	.p2align	4, 0x90
.LBB12_146:                             # %.lr.ph1780.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_159 Depth 2
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%r14,8), %rbx
	cmpb	$0, 60(%rbx)
	je	.LBB12_165
# BB#147:                               #   in Loop: Header=BB12_146 Depth=1
	cmpb	$0, 63(%rbx)
	jne	.LBB12_165
# BB#148:                               #   in Loop: Header=BB12_146 Depth=1
	cmpb	$0, 62(%rbx)
	jne	.LBB12_165
# BB#149:                               # %_ZNK8NArchive3N7z11CUpdateItem9HasStreamEv.exit813
                                        #   in Loop: Header=BB12_146 Depth=1
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB12_165
# BB#150:                               #   in Loop: Header=BB12_146 Depth=1
	cmpq	$2048, %rax             # imm = 0x800
	jb	.LBB12_162
# BB#151:                               #   in Loop: Header=BB12_146 Depth=1
	movl	56(%rbx), %eax
	movl	%eax, %ecx
	andl	$4784128, %ecx          # imm = 0x490000
	je	.LBB12_162
# BB#152:                               #   in Loop: Header=BB12_146 Depth=1
	testw	%ax, %ax
	movl	$0, %eax
	jns	.LBB12_163
# BB#153:                               #   in Loop: Header=BB12_146 Depth=1
	movl	$-1, 2400(%rsp)
	movq	$0, 2416(%rsp)
.Ltmp71:
.Lcfi114:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	_Znam
.Ltmp72:
# BB#154:                               # %.noexc815
                                        #   in Loop: Header=BB12_146 Depth=1
	movq	%rax, 2408(%rsp)
	movb	$0, (%rax)
	movl	$4, 2420(%rsp)
	movq	$_ZTVN8NWindows5NFile3NIO7CInFileE+16, 2392(%rsp)
	movq	40(%rbx), %rsi
.Ltmp73:
.Lcfi115:
	.cfi_escape 0x2e, 0x00
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb
.Ltmp74:
# BB#155:                               #   in Loop: Header=BB12_146 Depth=1
	testb	%al, %al
	je	.LBB12_161
# BB#156:                               #   in Loop: Header=BB12_146 Depth=1
.Ltmp76:
.Lcfi116:
	.cfi_escape 0x2e, 0x00
	movl	$512, %edx              # imm = 0x200
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rcx
	callq	_ZN8NWindows5NFile3NIO7CInFile4ReadEPvjRj
.Ltmp77:
# BB#157:                               #   in Loop: Header=BB12_146 Depth=1
	movl	1072(%rsp), %ecx
	testl	%ecx, %ecx
	setne	%dl
	andb	%dl, %al
	cmpb	$1, %al
	jne	.LBB12_161
# BB#158:                               # %.lr.ph.i814.preheader
                                        #   in Loop: Header=BB12_146 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB12_159:                             # %.lr.ph.i814
                                        #   Parent Loop BB12_146 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %eax
	cmpb	$0, 1344(%rsp,%rax)
	je	.LBB12_144
# BB#160:                               #   in Loop: Header=BB12_159 Depth=2
	incl	%esi
	cmpl	%ecx, %esi
	jb	.LBB12_159
.LBB12_161:                             #   in Loop: Header=BB12_146 Depth=1
.Ltmp82:
.Lcfi117:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp83:
.LBB12_162:                             # %.noexc817
                                        #   in Loop: Header=BB12_146 Depth=1
	xorl	%eax, %eax
.LBB12_163:                             # %_ZN8NArchive3N7zL9IsExeFileERKNS0_11CUpdateItemE.exit
                                        #   in Loop: Header=BB12_146 Depth=1
	movzbl	68(%r12), %ecx
	leaq	(%rax,%rcx,2), %rax
	movq	128(%rsp), %rcx
	movq	(%rcx,%rax,8), %rbx
.Ltmp86:
.Lcfi118:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp87:
# BB#164:                               # %_ZN13CRecordVectorIjE3AddEj.exit
                                        #   in Loop: Header=BB12_146 Depth=1
	movq	16(%rbx), %rax
	movslq	12(%rbx), %rcx
	movl	%r14d, (%rax,%rcx,4)
	incl	12(%rbx)
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	.p2align	4, 0x90
.LBB12_165:                             # %_ZNK8NArchive3N7z11CUpdateItem9HasStreamEv.exit813.thread
                                        #   in Loop: Header=BB12_146 Depth=1
	incq	%r14
	movslq	%ecx, %rax
	cmpq	%rax, %r14
	jl	.LBB12_146
.LBB12_166:                             # %._crit_edge1781
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testb	$1, 24(%rsp)            # 1-byte Folded Reload
	jne	.LBB12_168
# BB#167:
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	jmp	.LBB12_183
.LBB12_168:
.Ltmp92:
.Lcfi119:
	.cfi_escape 0x2e, 0x00
	movl	$32, %edi
	callq	_Znwm
.Ltmp93:
# BB#169:
	movl	$0, 8(%rax)
	movq	$_ZTVN8NArchive3N7z22CCryptoGetTextPasswordE+16, (%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rax)
.Ltmp94:
.Lcfi120:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	movq	%rax, %rbp
	callq	_Znam
.Ltmp95:
# BB#170:                               # %.noexc822
	movq	%rax, 16(%rbp)
	movl	$0, (%rax)
	movl	$4, 28(%rbp)
	movl	$1, 8(%rbp)
	movq	2144(%rsp), %rdi
	testq	%rdi, %rdi
	movq	%rbp, %rcx
	movq	%rcx, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	je	.LBB12_172
# BB#171:
	movq	(%rdi), %rax
.Ltmp97:
.Lcfi121:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
	movq	80(%rsp), %rcx          # 8-byte Reload
.Ltmp98:
.LBB12_172:
	movq	%rcx, 2144(%rsp)
	movq	3544(%rsp), %rax
	movq	(%rax), %rbp
	cmpb	$0, 68(%rbp)
	je	.LBB12_521
# BB#173:
	movq	%rcx, %rax
	addq	$16, %rax
	leaq	72(%rbp), %r12
	cmpq	%rax, %r12
	je	.LBB12_183
# BB#174:
	movl	$0, 24(%rcx)
	movq	16(%rcx), %rbx
	movl	$0, (%rbx)
	movslq	80(%rbp), %r15
	incq	%r15
	movl	28(%rcx), %r13d
	cmpl	%r13d, %r15d
	je	.LBB12_180
# BB#175:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp99:
.Lcfi122:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rax, %r14
.Ltmp100:
# BB#176:                               # %.noexc825
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB12_179
# BB#177:                               # %.noexc825
	testl	%r13d, %r13d
	jle	.LBB12_179
# BB#178:                               # %._crit_edge.thread.i.i
.Lcfi123:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	80(%rsp), %rcx          # 8-byte Reload
	movslq	24(%rcx), %rax
.LBB12_179:                             # %._crit_edge16.i.i
	movq	%r14, 16(%rcx)
	movl	$0, (%r14,%rax,4)
	movl	%r15d, 28(%rcx)
	movq	%r14, %rbx
.LBB12_180:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r12), %rax
	.p2align	4, 0x90
.LBB12_181:                             # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB12_181
# BB#182:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	80(%rbp), %eax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 24(%rcx)
.LBB12_183:                             # %_ZN11CStringBaseIwEaSERKS0_.exit
.Ltmp111:
.Lcfi124:
	.cfi_escape 0x2e, 0x00
	xorl	%edx, %edx
	movq	176(%rsp), %rdi         # 8-byte Reload
	movq	184(%rsp), %rsi         # 8-byte Reload
	callq	_ZN8NArchive3N7z11COutArchive6CreateEP20ISequentialOutStreamb
	movl	%eax, %ebp
.Ltmp112:
# BB#184:
	testl	%ebp, %ebp
	jne	.LBB12_540
# BB#185:
.Ltmp113:
.Lcfi125:
	.cfi_escape 0x2e, 0x00
	movq	176(%rsp), %rdi         # 8-byte Reload
	callq	_ZN8NArchive3N7z11COutArchive23SkipPrefixArchiveHeaderEv
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%eax, %ebp
.Ltmp114:
# BB#186:
	testl	%ebp, %ebp
	jne	.LBB12_540
# BB#187:
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	$0, 40(%rbp)
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	96(%rax), %rdx
	movq	%rdx, 336(%rsp)         # 8-byte Spill
	subq	$-128, %rax
	movq	%rax, 528(%rsp)         # 8-byte Spill
	cmpq	$1, 352(%rsp)           # 8-byte Folded Reload
	seta	22(%rsp)                # 1-byte Folded Spill
	testq	%rcx, %rcx
	sete	%al
	leaq	16(%rcx), %rdx
	leaq	696(%rsp), %rcx
	movq	%rdx, 744(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rdx
	sete	%cl
	orb	%al, %cl
	movb	%cl, 71(%rsp)           # 1-byte Spill
	xorl	%ebx, %ebx
	leaq	624(%rsp), %r12
	xorl	%eax, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	jmp	.LBB12_190
	.p2align	4, 0x90
.LBB12_188:                             #   in Loop: Header=BB12_190 Depth=1
	incq	%rbx
	cmpq	$4, %rbx
	movq	8(%rsp), %rbp           # 8-byte Reload
	jl	.LBB12_190
	jmp	.LBB12_496
.LBB12_189:                             #   in Loop: Header=BB12_190 Depth=1
	movl	$1, %ebp
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB12_491
	.p2align	4, 0x90
.LBB12_190:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_244 Depth 2
                                        #     Child Loop BB12_250 Depth 2
                                        #       Child Loop BB12_283 Depth 3
                                        #       Child Loop BB12_313 Depth 3
                                        #       Child Loop BB12_316 Depth 3
                                        #       Child Loop BB12_320 Depth 3
                                        #         Child Loop BB12_322 Depth 4
                                        #       Child Loop BB12_326 Depth 3
                                        #       Child Loop BB12_265 Depth 3
                                        #       Child Loop BB12_268 Depth 3
                                        #       Child Loop BB12_274 Depth 3
                                        #       Child Loop BB12_352 Depth 3
                                        #         Child Loop BB12_368 Depth 4
                                        #     Child Loop BB12_382 Depth 2
                                        #     Child Loop BB12_389 Depth 2
                                        #     Child Loop BB12_392 Depth 2
                                        #       Child Loop BB12_395 Depth 3
                                        #         Child Loop BB12_399 Depth 4
                                        #         Child Loop BB12_420 Depth 4
                                        #       Child Loop BB12_439 Depth 3
                                        #       Child Loop BB12_442 Depth 3
                                        #       Child Loop BB12_446 Depth 3
                                        #         Child Loop BB12_448 Depth 4
                                        #       Child Loop BB12_460 Depth 3
	movq	128(%rsp), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp116:
.Lcfi126:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
.Ltmp117:
# BB#191:                               #   in Loop: Header=BB12_190 Depth=1
	movq	%rbx, 344(%rsp)         # 8-byte Spill
	testb	$1, %bl
	movq	3544(%rsp), %rax
	movq	(%rax), %rsi
	jne	.LBB12_193
# BB#192:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp249:
.Lcfi127:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_
.Ltmp250:
	jmp	.LBB12_232
	.p2align	4, 0x90
.LBB12_193:                             #   in Loop: Header=BB12_190 Depth=1
	movb	17(%rax), %bl
.Ltmp119:
.Lcfi128:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_
.Ltmp120:
# BB#194:                               # %.noexc840
                                        #   in Loop: Header=BB12_190 Depth=1
	testb	%bl, %bl
	je	.LBB12_228
# BB#195:                               #   in Loop: Header=BB12_190 Depth=1
	pxor	%xmm0, %xmm0
	leaq	208(%rsp), %rax
	movdqu	%xmm0, (%rax)
	movq	$8, 224(%rsp)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 200(%rsp)
	movq	$50528539, 192(%rsp)    # imm = 0x303011B
	movabsq	$4294967300, %rax       # imm = 0x100000004
	movq	%rax, 232(%rsp)
.Ltmp121:
.Lcfi129:
	.cfi_escape 0x2e, 0x00
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	192(%rsp), %rdx
	leaq	200(%rsp), %rbx
	callq	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6InsertEiRKS2_
.Ltmp122:
# BB#196:                               #   in Loop: Header=BB12_190 Depth=1
	movq	$196865, 192(%rsp)      # imm = 0x30101
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 232(%rsp)
	movw	$0, 2400(%rsp)
	movw	$0, 2402(%rsp)
	movl	$12, 2392(%rsp)
.Ltmp123:
.Lcfi130:
	.cfi_escape 0x2e, 0x00
	movl	$1, %esi
	leaq	2400(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp124:
# BB#197:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp125:
.Lcfi131:
	.cfi_escape 0x2e, 0x00
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp126:
# BB#198:                               # %.noexc.i
                                        #   in Loop: Header=BB12_190 Depth=1
	movl	2392(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp127:
.Lcfi132:
	.cfi_escape 0x2e, 0x00
	leaq	2400(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp128:
# BB#199:                               # %_ZN5CPropC2ERKS_.exit.i.i
                                        #   in Loop: Header=BB12_190 Depth=1
.Ltmp130:
.Lcfi133:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp131:
# BB#200:                               #   in Loop: Header=BB12_190 Depth=1
	movq	216(%rsp), %rax
	movslq	212(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 212(%rsp)
.Ltmp135:
.Lcfi134:
	.cfi_escape 0x2e, 0x00
	leaq	2400(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp136:
# BB#201:                               #   in Loop: Header=BB12_190 Depth=1
	movw	$0, 1352(%rsp)
	movw	$0, 1354(%rsp)
	movl	$9, 1344(%rsp)
.Ltmp138:
.Lcfi135:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.1, %esi
	leaq	1352(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp139:
# BB#202:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp140:
.Lcfi136:
	.cfi_escape 0x2e, 0x00
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp141:
# BB#203:                               # %.noexc79.i
                                        #   in Loop: Header=BB12_190 Depth=1
	movl	1344(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp142:
.Lcfi137:
	.cfi_escape 0x2e, 0x00
	leaq	1352(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp143:
# BB#204:                               # %_ZN5CPropC2ERKS_.exit.i78.i
                                        #   in Loop: Header=BB12_190 Depth=1
.Ltmp145:
.Lcfi138:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp146:
# BB#205:                               #   in Loop: Header=BB12_190 Depth=1
	movq	216(%rsp), %rax
	movslq	212(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 212(%rsp)
.Ltmp150:
.Lcfi139:
	.cfi_escape 0x2e, 0x00
	leaq	1352(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp151:
# BB#206:                               #   in Loop: Header=BB12_190 Depth=1
	movw	$0, 1080(%rsp)
	movw	$0, 1082(%rsp)
	movl	$1, 1072(%rsp)
.Ltmp153:
.Lcfi140:
	.cfi_escape 0x2e, 0x00
	movl	$1048576, %esi          # imm = 0x100000
	leaq	1080(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp154:
# BB#207:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp155:
.Lcfi141:
	.cfi_escape 0x2e, 0x00
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp156:
# BB#208:                               # %.noexc87.i
                                        #   in Loop: Header=BB12_190 Depth=1
	movl	1072(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp157:
.Lcfi142:
	.cfi_escape 0x2e, 0x00
	leaq	1080(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp158:
# BB#209:                               # %_ZN5CPropC2ERKS_.exit.i86.i
                                        #   in Loop: Header=BB12_190 Depth=1
.Ltmp160:
.Lcfi143:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp161:
# BB#210:                               #   in Loop: Header=BB12_190 Depth=1
	movq	216(%rsp), %rax
	movslq	212(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 212(%rsp)
.Ltmp165:
.Lcfi144:
	.cfi_escape 0x2e, 0x00
	leaq	1080(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp166:
# BB#211:                               #   in Loop: Header=BB12_190 Depth=1
	movw	$0, 944(%rsp)
	movw	$0, 946(%rsp)
	movl	$8, 936(%rsp)
.Ltmp168:
.Lcfi145:
	.cfi_escape 0x2e, 0x00
	movl	$64, %esi
	leaq	944(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp169:
# BB#212:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp170:
.Lcfi146:
	.cfi_escape 0x2e, 0x00
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp171:
# BB#213:                               # %.noexc95.i
                                        #   in Loop: Header=BB12_190 Depth=1
	movl	936(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp172:
.Lcfi147:
	.cfi_escape 0x2e, 0x00
	leaq	944(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp173:
# BB#214:                               # %_ZN5CPropC2ERKS_.exit.i94.i
                                        #   in Loop: Header=BB12_190 Depth=1
.Ltmp175:
.Lcfi148:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp176:
# BB#215:                               #   in Loop: Header=BB12_190 Depth=1
	movq	216(%rsp), %rax
	movslq	212(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 212(%rsp)
.Ltmp180:
.Lcfi149:
	.cfi_escape 0x2e, 0x00
	leaq	944(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp181:
# BB#216:                               #   in Loop: Header=BB12_190 Depth=1
	movw	$0, 808(%rsp)
	movw	$0, 810(%rsp)
	movl	$13, 800(%rsp)
.Ltmp183:
.Lcfi150:
	.cfi_escape 0x2e, 0x00
	movl	$1, %esi
	leaq	808(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp184:
# BB#217:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp185:
.Lcfi151:
	.cfi_escape 0x2e, 0x00
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp186:
# BB#218:                               # %.noexc103.i
                                        #   in Loop: Header=BB12_190 Depth=1
	movl	800(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp187:
.Lcfi152:
	.cfi_escape 0x2e, 0x00
	leaq	808(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp188:
# BB#219:                               # %_ZN5CPropC2ERKS_.exit.i102.i
                                        #   in Loop: Header=BB12_190 Depth=1
.Ltmp190:
.Lcfi153:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp191:
# BB#220:                               #   in Loop: Header=BB12_190 Depth=1
	movq	216(%rsp), %rax
	movslq	212(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 212(%rsp)
.Ltmp195:
.Lcfi154:
	.cfi_escape 0x2e, 0x00
	leaq	808(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp196:
# BB#221:                               # %_ZN5CPropD2Ev.exit109.i
                                        #   in Loop: Header=BB12_190 Depth=1
.Ltmp198:
.Lcfi155:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	leaq	192(%rsp), %rsi
	callq	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
.Ltmp199:
# BB#222:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp200:
.Lcfi156:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	leaq	192(%rsp), %rsi
	callq	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
.Ltmp201:
# BB#223:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp203:
.Lcfi157:
	.cfi_escape 0x2e, 0x00
	leaq	656(%rsp), %rbp
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp204:
# BB#224:                               #   in Loop: Header=BB12_190 Depth=1
	movq	672(%rsp), %rax
	movslq	668(%rsp), %rcx
	shlq	$4, %rcx
	movl	$1, %edx
	movd	%rdx, %xmm0
	movdqu	%xmm0, (%rax,%rcx)
	incl	668(%rsp)
.Ltmp205:
.Lcfi158:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp206:
# BB#225:                               #   in Loop: Header=BB12_190 Depth=1
	movq	672(%rsp), %rax
	movslq	668(%rsp), %rcx
	shlq	$4, %rcx
	movdqa	.LCPI12_0(%rip), %xmm0  # xmm0 = [2,4294967296]
	movdqu	%xmm0, (%rax,%rcx)
	incl	668(%rsp)
.Ltmp207:
.Lcfi159:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp208:
# BB#226:                               #   in Loop: Header=BB12_190 Depth=1
	movq	672(%rsp), %rax
	movslq	668(%rsp), %rcx
	shlq	$4, %rcx
	movdqa	.LCPI12_1(%rip), %xmm0  # xmm0 = [3,8589934592]
	movdqu	%xmm0, (%rax,%rcx)
	incl	668(%rsp)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 200(%rsp)
.Ltmp218:
.Lcfi160:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp219:
# BB#227:                               # %_ZN7CMethodD2Ev.exit122.i
                                        #   in Loop: Header=BB12_190 Depth=1
.Ltmp224:
.Lcfi161:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp225:
	jmp	.LBB12_232
.LBB12_228:                             #   in Loop: Header=BB12_190 Depth=1
	leaq	768(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	$8, 784(%rsp)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 760(%rsp)
	movq	$50528515, 752(%rsp)    # imm = 0x3030103
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 792(%rsp)
.Ltmp226:
.Lcfi162:
	.cfi_escape 0x2e, 0x00
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	752(%rsp), %rdx
	callq	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6InsertEiRKS2_
.Ltmp227:
	leaq	656(%rsp), %rdi
# BB#229:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp229:
.Lcfi163:
	.cfi_escape 0x2e, 0x00
	leaq	760(%rsp), %rbx
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp230:
# BB#230:                               #   in Loop: Header=BB12_190 Depth=1
	movq	672(%rsp), %rax
	movslq	668(%rsp), %rcx
	shlq	$4, %rcx
	movl	$1, %edx
	movd	%rdx, %xmm0
	movdqu	%xmm0, (%rax,%rcx)
	incl	668(%rsp)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 760(%rsp)
.Ltmp241:
.Lcfi164:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp242:
# BB#231:                               # %_ZN7CMethodD2Ev.exit144.i
                                        #   in Loop: Header=BB12_190 Depth=1
.Ltmp247:
.Lcfi165:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp248:
	.p2align	4, 0x90
.LBB12_232:                             # %_ZN8NArchive3N7zL13MakeExeMethodERKNS0_22CCompressionMethodModeEbRS1_.exit
                                        #   in Loop: Header=BB12_190 Depth=1
	testb	$2, 344(%rsp)           # 1-byte Folded Reload
	jne	.LBB12_234
# BB#233:                               #   in Loop: Header=BB12_190 Depth=1
	movb	$0, 692(%rsp)
	movl	$0, 704(%rsp)
	movq	696(%rsp), %rax
	movl	$0, (%rax)
	jmp	.LBB12_247
	.p2align	4, 0x90
.LBB12_234:                             #   in Loop: Header=BB12_190 Depth=1
	cmpb	$0, 692(%rsp)
	jne	.LBB12_247
# BB#235:                               #   in Loop: Header=BB12_190 Depth=1
	cmpb	$0, 71(%rsp)            # 1-byte Folded Reload
	jne	.LBB12_246
# BB#236:                               #   in Loop: Header=BB12_190 Depth=1
	movl	$0, 704(%rsp)
	movq	696(%rsp), %rbx
	movl	$0, (%rbx)
	movq	80(%rsp), %rax          # 8-byte Reload
	movslq	24(%rax), %rbp
	incq	%rbp
	movl	708(%rsp), %r15d
	cmpl	%r15d, %ebp
	je	.LBB12_243
# BB#237:                               #   in Loop: Header=BB12_190 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp251:
.Lcfi166:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp252:
# BB#238:                               # %.noexc854
                                        #   in Loop: Header=BB12_190 Depth=1
	testq	%rbx, %rbx
	je	.LBB12_241
# BB#239:                               # %.noexc854
                                        #   in Loop: Header=BB12_190 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	jle	.LBB12_242
# BB#240:                               # %._crit_edge.thread.i.i848
                                        #   in Loop: Header=BB12_190 Depth=1
.Lcfi167:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	704(%rsp), %rax
	jmp	.LBB12_242
.LBB12_241:                             #   in Loop: Header=BB12_190 Depth=1
	xorl	%eax, %eax
.LBB12_242:                             # %._crit_edge16.i.i849
                                        #   in Loop: Header=BB12_190 Depth=1
	movq	%r14, 696(%rsp)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 708(%rsp)
	movq	%r14, %rbx
.LBB12_243:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i850
                                        #   in Loop: Header=BB12_190 Depth=1
	movq	744(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB12_244:                             #   Parent Loop BB12_190 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB12_244
# BB#245:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i853
                                        #   in Loop: Header=BB12_190 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	24(%rax), %eax
	movl	%eax, 704(%rsp)
.LBB12_246:                             # %_ZN11CStringBaseIwEaSERKS0_.exit855
                                        #   in Loop: Header=BB12_190 Depth=1
	movb	$1, 692(%rsp)
	.p2align	4, 0x90
.LBB12_247:                             #   in Loop: Header=BB12_190 Depth=1
.Ltmp253:
.Lcfi168:
	.cfi_escape 0x2e, 0x00
	leaq	1344(%rsp), %rdi
	movq	%r12, %rsi
	callq	_ZN8NArchive3N7z8CEncoderC1ERKNS0_22CCompressionMethodModeE
.Ltmp254:
# BB#248:                               # %.preheader1005
                                        #   in Loop: Header=BB12_190 Depth=1
	movq	168(%rsp), %rbp         # 8-byte Reload
	cmpl	372(%rsp), %ebp
	jge	.LBB12_377
# BB#249:                               # %.lr.ph1727.preheader
                                        #   in Loop: Header=BB12_190 Depth=1
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movslq	%ebp, %rbp
	movq	88(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_250:                             # %.lr.ph1727
                                        #   Parent Loop BB12_190 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_283 Depth 3
                                        #       Child Loop BB12_313 Depth 3
                                        #       Child Loop BB12_316 Depth 3
                                        #       Child Loop BB12_320 Depth 3
                                        #         Child Loop BB12_322 Depth 4
                                        #       Child Loop BB12_326 Depth 3
                                        #       Child Loop BB12_265 Depth 3
                                        #       Child Loop BB12_268 Depth 3
                                        #       Child Loop BB12_274 Depth 3
                                        #       Child Loop BB12_352 Depth 3
                                        #         Child Loop BB12_368 Depth 4
	movq	376(%rsp), %r13
	leaq	(%rbp,%rbp,2), %r12
	movl	4(%r13,%r12,4), %eax
	cmpq	344(%rsp), %rax         # 8-byte Folded Reload
	jne	.LBB12_376
# BB#251:                               #   in Loop: Header=BB12_250 Depth=2
	movl	8(%r13,%r12,4), %eax
	movq	144(%r15), %rcx
	movslq	(%r13,%r12,4), %r14
	cmpl	(%rcx,%r14,4), %eax
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	jne	.LBB12_259
# BB#252:                               #   in Loop: Header=BB12_250 Depth=2
	movq	112(%r15), %rax
	movq	600(%r15), %rcx
	movl	(%rcx,%r14,4), %esi
	movslq	%esi, %r8
	movq	(%rax,%r14,8), %rax
	movslq	76(%rax), %rax
	testq	%rax, %rax
	jle	.LBB12_269
# BB#253:                               # %.lr.ph.i856
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	16(%r15), %rdx
	cmpl	$4, %eax
	jb	.LBB12_262
# BB#254:                               # %min.iters.checked2961
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	%rax, %rbx
	andq	$-4, %rbx
	je	.LBB12_262
# BB#255:                               # %vector.scevcheck2969
                                        #   in Loop: Header=BB12_250 Depth=2
	leaq	-1(%rax), %rcx
	leal	(%rsi,%rcx), %edi
	cmpl	%esi, %edi
	jl	.LBB12_262
# BB#256:                               # %vector.scevcheck2969
                                        #   in Loop: Header=BB12_250 Depth=2
	shrq	$32, %rcx
	movl	$0, %edi
	movl	$0, %ebp
	jne	.LBB12_263
# BB#257:                               # %vector.body2957.preheader
                                        #   in Loop: Header=BB12_250 Depth=2
	leaq	-4(%rbx), %rdi
	movq	%rdi, %rcx
	shrq	$2, %rcx
	btl	$2, %edi
	jb	.LBB12_325
# BB#258:                               # %vector.body2957.prol
                                        #   in Loop: Header=BB12_250 Depth=2
	movdqu	(%rdx,%r8,8), %xmm0
	movdqu	16(%rdx,%r8,8), %xmm1
	movl	$4, %edi
	testq	%rcx, %rcx
	jne	.LBB12_326
	jmp	.LBB12_327
	.p2align	4, 0x90
.LBB12_259:                             #   in Loop: Header=BB12_250 Depth=2
	movq	$0, 1080(%rsp)
	movq	$_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE+16, 1072(%rsp)
	movl	$0, 1096(%rsp)
	movq	$0, 1208(%rsp)
	movq	$_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE+16, 1200(%rsp)
	movq	$0, 1224(%rsp)
.Ltmp256:
.Lcfi169:
	.cfi_escape 0x2e, 0x00
	leaq	1072(%rsp), %rdi
	callq	_ZN13CStreamBinder12CreateEventsEv
.Ltmp257:
# BB#260:                               #   in Loop: Header=BB12_250 Depth=2
	testl	%eax, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmovnel	%eax, %ecx
	je	.LBB12_280
# BB#261:                               #   in Loop: Header=BB12_250 Depth=2
	movl	$1, %ebp
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB12_343
	.p2align	4, 0x90
.LBB12_262:                             #   in Loop: Header=BB12_250 Depth=2
	xorl	%edi, %edi
	xorl	%ebp, %ebp
.LBB12_263:                             # %scalar.ph2959.preheader
                                        #   in Loop: Header=BB12_250 Depth=2
	movl	%eax, %ecx
	subl	%edi, %ecx
	leaq	-1(%rax), %rbx
	subq	%rdi, %rbx
	andq	$3, %rcx
	je	.LBB12_266
# BB#264:                               # %scalar.ph2959.prol.preheader
                                        #   in Loop: Header=BB12_250 Depth=2
	leal	(%rdi,%rsi), %esi
	negq	%rcx
	.p2align	4, 0x90
.LBB12_265:                             # %scalar.ph2959.prol
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%esi, %rsi
	addq	(%rdx,%rsi,8), %rbp
	incq	%rdi
	incl	%esi
	incq	%rcx
	jne	.LBB12_265
.LBB12_266:                             # %scalar.ph2959.prol.loopexit
                                        #   in Loop: Header=BB12_250 Depth=2
	cmpq	$3, %rbx
	jb	.LBB12_270
# BB#267:                               # %scalar.ph2959.preheader.new
                                        #   in Loop: Header=BB12_250 Depth=2
	subq	%rdi, %rax
	movl	%r8d, %ecx
	leaq	3(%rcx,%rdi), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB12_268:                             # %scalar.ph2959
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-3(%rcx,%rsi), %edi
	movslq	%edi, %rdi
	addq	(%rdx,%rdi,8), %rbp
	leal	-2(%rcx,%rsi), %edi
	movslq	%edi, %rdi
	addq	(%rdx,%rdi,8), %rbp
	leal	-1(%rcx,%rsi), %edi
	movslq	%edi, %rdi
	addq	(%rdx,%rdi,8), %rbp
	leal	(%rcx,%rsi), %edi
	movslq	%edi, %rdi
	addq	(%rdx,%rdi,8), %rbp
	addq	$4, %rsi
	cmpq	%rsi, %rax
	jne	.LBB12_268
	jmp	.LBB12_270
.LBB12_269:                             #   in Loop: Header=BB12_250 Depth=2
	xorl	%ebp, %ebp
.LBB12_270:                             # %.loopexit
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	120(%rax), %rsi
	movq	568(%r15), %rax
	movq	(%rax,%r8,8), %rdx
	addq	504(%r15), %rdx
.Ltmp321:
.Lcfi170:
	.cfi_escape 0x2e, 0x00
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	%rbp, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	callq	_ZN8NArchive3N7zL10WriteRangeEP9IInStreamP20ISequentialOutStreamyyP21ICompressProgressInfo
.Ltmp322:
# BB#271:                               #   in Loop: Header=BB12_250 Depth=2
	testl	%eax, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmovnel	%eax, %ecx
	movq	40(%rsp), %rdi          # 8-byte Reload
	jne	.LBB12_189
# BB#272:                               #   in Loop: Header=BB12_250 Depth=2
	movq	%r12, 72(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	%rbp, 40(%rax)
	movq	112(%r15), %rax
	movq	%r14, %r13
	movq	(%rax,%r14,8), %r12
	cmpl	$0, 76(%r12)
	jle	.LBB12_276
# BB#273:                               # %.lr.ph1718.preheader
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	600(%r15), %rax
	movl	(%rax,%r13,4), %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_274:                             # %.lr.ph1718
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%r14,%rbx), %eax
	movq	16(%r15), %rcx
	cltq
	movq	(%rcx,%rax,8), %rbp
.Ltmp324:
.Lcfi171:
	.cfi_escape 0x2e, 0x00
	movq	%rdi, %r15
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp325:
# BB#275:                               #   in Loop: Header=BB12_274 Depth=3
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbx
	movslq	76(%r12), %rax
	cmpq	%rax, %rbx
	movq	%r15, %rdi
	movq	88(%rsp), %r15          # 8-byte Reload
	jl	.LBB12_274
.LBB12_276:                             # %._crit_edge1719
                                        #   in Loop: Header=BB12_250 Depth=2
.Ltmp327:
.Lcfi172:
	.cfi_escape 0x2e, 0x00
	movl	$136, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp328:
# BB#277:                               # %.noexc862
                                        #   in Loop: Header=BB12_250 Depth=2
.Ltmp329:
.Lcfi173:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	_ZN8NArchive3N7z7CFolderC2ERKS1_
.Ltmp330:
# BB#278:                               #   in Loop: Header=BB12_250 Depth=2
.Ltmp332:
.Lcfi174:
	.cfi_escape 0x2e, 0x00
	movq	336(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp333:
# BB#279:                               # %.thread974
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	112(%rdx), %rax
	movslq	108(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 108(%rdx)
	movq	%r13, %r14
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	72(%rsp), %r12          # 8-byte Reload
	jmp	.LBB12_349
.LBB12_280:                             #   in Loop: Header=BB12_250 Depth=2
	movq	%r12, 72(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	$0, 144(%rsp)
	movq	$0, 240(%rsp)
.Ltmp259:
.Lcfi175:
	.cfi_escape 0x2e, 0x00
	leaq	1072(%rsp), %rdi
	leaq	240(%rsp), %rsi
	leaq	144(%rsp), %rdx
	callq	_ZN13CStreamBinder13CreateStreamsEPP19ISequentialInStreamPP20ISequentialOutStream
.Ltmp260:
# BB#281:                               #   in Loop: Header=BB12_250 Depth=2
	leaq	720(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	$1, 736(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 712(%rsp)
	movq	144(%r15), %rax
	movq	632(%r15), %rcx
	movl	(%rax,%r14,4), %r12d
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movl	(%rcx,%r14,4), %ebp
	testl	%r12d, %r12d
	je	.LBB12_291
# BB#282:                               # %.lr.ph1711.preheader
                                        #   in Loop: Header=BB12_250 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_283:                             # %.lr.ph1711
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	176(%r15), %rcx
	movslq	%ebp, %rax
	movq	(%rcx,%rax,8), %rcx
	cmpb	$0, 32(%rcx)
	je	.LBB12_287
# BB#284:                               #   in Loop: Header=BB12_283 Depth=3
	incl	%ebx
	movq	408(%rsp), %rcx
	movslq	(%rcx,%rax,4), %rax
	testq	%rax, %rax
	js	.LBB12_287
# BB#285:                               #   in Loop: Header=BB12_283 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rcx
	movq	(%rcx,%rax,8), %rax
	movb	60(%rax), %r14b
	xorb	$1, %r14b
	jmp	.LBB12_288
	.p2align	4, 0x90
.LBB12_287:                             #   in Loop: Header=BB12_283 Depth=3
	xorl	%r14d, %r14d
.LBB12_288:                             #   in Loop: Header=BB12_283 Depth=3
.Ltmp262:
.Lcfi176:
	.cfi_escape 0x2e, 0x00
	leaq	712(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp263:
# BB#289:                               #   in Loop: Header=BB12_283 Depth=3
	movq	728(%rsp), %rax
	movslq	724(%rsp), %rcx
	movb	%r14b, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 724(%rsp)
	incl	%ebp
	cmpl	%r12d, %ebx
	jb	.LBB12_283
# BB#290:                               # %._crit_edge1712.loopexit
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	632(%r15), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	(%rax,%rcx,4), %ebp
.LBB12_291:                             # %._crit_edge1712
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	2104(%rsp), %rbx
	movq	144(%rsp), %r12
	movq	%r15, 32(%rbx)
	movl	%ebp, 56(%rbx)
	leaq	712(%rsp), %rax
	movq	%rax, 40(%rbx)
	testq	%r12, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB12_293
# BB#292:                               #   in Loop: Header=BB12_250 Depth=2
	movq	(%r12), %rax
.Ltmp265:
.Lcfi177:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp266:
.LBB12_293:                             # %.noexc867
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_295
# BB#294:                               #   in Loop: Header=BB12_250 Depth=2
	movq	(%rdi), %rax
.Ltmp267:
.Lcfi178:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp268:
.LBB12_295:                             # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit.i
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	%r12, 48(%rbx)
	movl	$0, 60(%rbx)
	movb	$0, 64(%rbx)
.Ltmp269:
.Lcfi179:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z17CFolderOutStream217ProcessEmptyFilesEv
.Ltmp270:
# BB#296:                               # %_ZN8NArchive3N7z17CFolderOutStream24InitEPKNS0_18CArchiveDatabaseExEjPK13CRecordVectorIbEP20ISequentialOutStream.exit
                                        #   in Loop: Header=BB12_250 Depth=2
	testl	%eax, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmovnel	%eax, %ecx
	je	.LBB12_298
# BB#297:                               #   in Loop: Header=BB12_250 Depth=2
	movl	$1, %ebp
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jmp	.LBB12_338
.LBB12_298:                             #   in Loop: Header=BB12_250 Depth=2
	movq	%rcx, %r14
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_301
# BB#299:                               #   in Loop: Header=BB12_250 Depth=2
	movq	(%rdi), %rax
.Ltmp271:
.Lcfi180:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp272:
# BB#300:                               # %.noexc871
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	$0, 144(%rsp)
.LBB12_301:                             # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
                                        #   in Loop: Header=BB12_250 Depth=2
	cmpq	$0, 160(%rsp)           # 8-byte Folded Reload
	je	.LBB12_303
# BB#302:                               #   in Loop: Header=BB12_250 Depth=2
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp273:
.Lcfi181:
	.cfi_escape 0x2e, 0x00
	callq	*8(%rax)
.Ltmp274:
.LBB12_303:                             # %.noexc872
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	2096(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_305
# BB#304:                               #   in Loop: Header=BB12_250 Depth=2
	movq	(%rdi), %rax
.Ltmp275:
.Lcfi182:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp276:
.LBB12_305:                             #   in Loop: Header=BB12_250 Depth=2
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	%rax, 2096(%rsp)
	movq	112(%r15), %rax
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 2136(%rsp)
	movq	568(%r15), %rax
	movq	600(%r15), %rcx
	movslq	(%rcx,%rdx,4), %rcx
	movq	(%rax,%rcx,8), %rax
	shlq	$3, %rcx
	addq	504(%r15), %rax
	movq	%rax, 2120(%rsp)
	addq	16(%r15), %rcx
	movq	%rcx, 2128(%rsp)
.Ltmp277:
.Lcfi183:
	.cfi_escape 0x2e, 0x00
	leaq	1856(%rsp), %rdi
	callq	_ZN11CVirtThread5StartEv
.Ltmp278:
# BB#306:                               #   in Loop: Header=BB12_250 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	12(%rax), %r12
	leaq	944(%rsp), %rcx
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rcx)
	movq	$8, 960(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, 936(%rsp)
	movdqu	%xmm0, 32(%rcx)
	movq	$8, 992(%rsp)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE+16, 968(%rsp)
	movdqu	%xmm0, 64(%rcx)
	movq	$4, 1024(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 1000(%rsp)
	movdqu	%xmm0, 96(%rcx)
	movq	$8, 1056(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 1032(%rsp)
	movb	$0, 1068(%rsp)
	movq	240(%rsp), %rsi
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	120(%rcx), %r9
.Ltmp280:
.Lcfi184:
	.cfi_escape 0x2e, 0x10
	xorl	%edx, %edx
	leaq	1344(%rsp), %rdi
	leaq	96(%rsp), %rcx
	leaq	936(%rsp), %r8
	pushq	%rbp
.Lcfi185:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi186:
	.cfi_adjust_cfa_offset 8
	callq	_ZN8NArchive3N7z8CEncoder6EncodeEP19ISequentialInStreamPKyS5_RNS0_7CFolderEP20ISequentialOutStreamR13CRecordVectorIyEP21ICompressProgressInfo
	addq	$16, %rsp
.Lcfi187:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebx
.Ltmp281:
# BB#307:                               #   in Loop: Header=BB12_250 Depth=2
	testl	%ebx, %ebx
	cmovnel	%ebx, %r14d
	movl	$1, %ebp
	jne	.LBB12_336
# BB#308:                               #   in Loop: Header=BB12_250 Depth=2
.Ltmp282:
.Lcfi188:
	.cfi_escape 0x2e, 0x00
	leaq	1968(%rsp), %rdi
	callq	Event_Wait
.Ltmp283:
# BB#309:                               # %_ZN11CVirtThread10WaitFinishEv.exit
                                        #   in Loop: Header=BB12_250 Depth=2
	movl	2092(%rsp), %ebx
	testl	%ebx, %ebx
	cmovnel	%ebx, %r14d
	jne	.LBB12_336
# BB#310:                               # %.preheader996
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	%r14, %rbx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movslq	12(%rcx), %rax
	cmpl	%eax, %r12d
	movq	8(%rsp), %rbp           # 8-byte Reload
	jge	.LBB12_317
# BB#311:                               # %.lr.ph1714
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	16(%rcx), %rdx
	movq	56(%rbp), %rcx
	movl	%eax, %edi
	subl	%r12d, %edi
	leaq	-1(%rax), %rsi
	subq	%r12, %rsi
	andq	$3, %rdi
	je	.LBB12_314
# BB#312:                               # %.prol.preheader
                                        #   in Loop: Header=BB12_250 Depth=2
	negq	%rdi
	.p2align	4, 0x90
.LBB12_313:                             #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	(%rdx,%r12,8), %rcx
	movq	%rcx, 56(%rbp)
	incq	%r12
	incq	%rdi
	jne	.LBB12_313
.LBB12_314:                             # %.prol.loopexit
                                        #   in Loop: Header=BB12_250 Depth=2
	cmpq	$3, %rsi
	jb	.LBB12_317
# BB#315:                               # %.lr.ph1714.new
                                        #   in Loop: Header=BB12_250 Depth=2
	subq	%r12, %rax
	leaq	24(%rdx,%r12,8), %rdx
.LBB12_316:                             #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	-24(%rdx), %rcx
	movq	%rcx, 56(%rbp)
	addq	-16(%rdx), %rcx
	movq	%rcx, 56(%rbp)
	addq	-8(%rdx), %rcx
	movq	%rcx, 56(%rbp)
	addq	(%rdx), %rcx
	movq	%rcx, 56(%rbp)
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB12_316
.LBB12_317:                             # %._crit_edge1715
                                        #   in Loop: Header=BB12_250 Depth=2
	movl	1044(%rsp), %eax
	testl	%eax, %eax
	je	.LBB12_328
# BB#318:                               # %.preheader.i
                                        #   in Loop: Header=BB12_250 Depth=2
	movslq	980(%rsp), %rcx
	testq	%rcx, %rcx
	jle	.LBB12_329
# BB#319:                               # %.preheader.i.split.us.preheader
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	984(%rsp), %rdx
.LBB12_320:                             # %.preheader.i.split.us
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_322 Depth 4
	testl	%eax, %eax
	jle	.LBB12_549
# BB#321:                               # %.lr.ph.i.i.us.preheader
                                        #   in Loop: Header=BB12_320 Depth=3
	decl	%eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB12_322:                             # %.lr.ph.i.i.us
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        #       Parent Loop BB12_320 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	%eax, 4(%rdx,%rsi,8)
	je	.LBB12_324
# BB#323:                               #   in Loop: Header=BB12_322 Depth=4
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB12_322
	jmp	.LBB12_331
.LBB12_324:                             # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.i.us
                                        #   in Loop: Header=BB12_320 Depth=3
	testl	%esi, %esi
	jns	.LBB12_320
	jmp	.LBB12_331
.LBB12_325:                             #   in Loop: Header=BB12_250 Depth=2
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
	testq	%rcx, %rcx
	je	.LBB12_327
	.p2align	4, 0x90
.LBB12_326:                             # %vector.body2957
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%rsi,%rdi), %rcx
	movslq	%ecx, %rcx
	movdqu	(%rdx,%rcx,8), %xmm2
	movdqu	16(%rdx,%rcx,8), %xmm3
	paddq	%xmm0, %xmm2
	paddq	%xmm1, %xmm3
	addl	$4, %ecx
	movslq	%ecx, %rcx
	movdqu	(%rdx,%rcx,8), %xmm0
	movdqu	16(%rdx,%rcx,8), %xmm1
	paddq	%xmm2, %xmm0
	paddq	%xmm3, %xmm1
	addq	$8, %rdi
	cmpq	%rdi, %rbx
	jne	.LBB12_326
.LBB12_327:                             # %middle.block2958
                                        #   in Loop: Header=BB12_250 Depth=2
	paddq	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %rbp
	cmpq	%rbx, %rax
	movq	%rbx, %rdi
	jne	.LBB12_263
	jmp	.LBB12_270
.LBB12_328:                             #   in Loop: Header=BB12_250 Depth=2
	xorl	%eax, %eax
	jmp	.LBB12_332
.LBB12_329:                             # %.preheader.i.split
                                        #   in Loop: Header=BB12_250 Depth=2
	testl	%eax, %eax
	jle	.LBB12_549
# BB#330:                               #   in Loop: Header=BB12_250 Depth=2
	decl	%eax
.LBB12_331:                             # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.thread.i
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	1048(%rsp), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
.LBB12_332:                             # %_ZNK8NArchive3N7z7CFolder13GetUnpackSizeEv.exit
                                        #   in Loop: Header=BB12_250 Depth=2
	addq	%rax, 48(%rbp)
.Ltmp287:
.Lcfi189:
	.cfi_escape 0x2e, 0x00
	movl	$136, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp288:
# BB#333:                               # %.noexc877
                                        #   in Loop: Header=BB12_250 Depth=2
.Ltmp289:
.Lcfi190:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	leaq	936(%rsp), %rsi
	callq	_ZN8NArchive3N7z7CFolderC2ERKS1_
.Ltmp290:
# BB#334:                               #   in Loop: Header=BB12_250 Depth=2
.Ltmp292:
.Lcfi191:
	.cfi_escape 0x2e, 0x00
	movq	336(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp293:
# BB#335:                               # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE3AddERKS2_.exit881
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	112(%rdx), %rax
	movslq	108(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 108(%rdx)
	xorl	%ebp, %ebp
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill>
.LBB12_336:                             #   in Loop: Header=BB12_250 Depth=2
.Ltmp297:
.Lcfi192:
	.cfi_escape 0x2e, 0x00
	leaq	936(%rsp), %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp298:
# BB#337:                               #   in Loop: Header=BB12_250 Depth=2
	movl	%ebx, %eax
.LBB12_338:                             #   in Loop: Header=BB12_250 Depth=2
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	72(%rsp), %r12          # 8-byte Reload
.Ltmp302:
.Lcfi193:
	.cfi_escape 0x2e, 0x00
	leaq	712(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp303:
# BB#339:                               #   in Loop: Header=BB12_250 Depth=2
	movq	240(%rsp), %rdi
	testq	%rdi, %rdi
	movq	48(%rsp), %r14          # 8-byte Reload
	je	.LBB12_341
# BB#340:                               #   in Loop: Header=BB12_250 Depth=2
	movq	(%rdi), %rax
.Ltmp307:
.Lcfi194:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp308:
.LBB12_341:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit883
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_343
# BB#342:                               #   in Loop: Header=BB12_250 Depth=2
	movq	(%rdi), %rax
.Ltmp312:
.Lcfi195:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp313:
.LBB12_343:                             #   in Loop: Header=BB12_250 Depth=2
	movq	1224(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB12_347
# BB#344:                               #   in Loop: Header=BB12_250 Depth=2
	cmpb	$0, 88(%rbx)
	je	.LBB12_346
# BB#345:                               #   in Loop: Header=BB12_250 Depth=2
.Lcfi196:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	pthread_mutex_destroy
	leaq	40(%rbx), %rdi
.Lcfi197:
	.cfi_escape 0x2e, 0x00
	callq	pthread_cond_destroy
.LBB12_346:                             #   in Loop: Header=BB12_250 Depth=2
.Lcfi198:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB12_347:                             #   in Loop: Header=BB12_250 Depth=2
	movq	$0, 1224(%rsp)
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 1200(%rsp)
	movq	$0, 1208(%rsp)
.Ltmp318:
.Lcfi199:
	.cfi_escape 0x2e, 0x00
	leaq	1096(%rsp), %rdi
	callq	Event_Close
.Ltmp319:
# BB#348:                               #   in Loop: Header=BB12_250 Depth=2
	testl	%ebp, %ebp
	jne	.LBB12_491
.LBB12_349:                             #   in Loop: Header=BB12_250 Depth=2
	leaq	8(%r13,%r12,4), %rax
	movl	(%rax), %ebx
.Ltmp335:
.Lcfi200:
	.cfi_escape 0x2e, 0x00
	movq	528(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp336:
# BB#350:                               #   in Loop: Header=BB12_250 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	144(%rdx), %rax
	movslq	140(%rdx), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	140(%rdx)
	movq	144(%r15), %rax
	movl	(%rax,%r14,4), %r13d
	testl	%r13d, %r13d
	je	.LBB12_375
# BB#351:                               # %.lr.ph1723.preheader
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	632(%r15), %rax
	movl	(%rax,%r14,4), %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB12_352:                             # %.lr.ph1723
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_368 Depth 4
	leaq	304(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
.Ltmp338:
.Lcfi201:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp339:
# BB#353:                               #   in Loop: Header=BB12_352 Depth=3
	movq	%rax, 304(%rsp)
	movl	$0, (%rax)
	movl	$4, 316(%rsp)
	movl	$1, 320(%rsp)
.Ltmp341:
.Lcfi202:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	movl	%ebp, %esi
	leaq	288(%rsp), %rdx
	leaq	2392(%rsp), %rcx
	callq	_ZNK8NArchive3N7z16CArchiveDatabase7GetFileEiRNS0_9CFileItemERNS0_10CFileItem2E
.Ltmp342:
# BB#354:                               #   in Loop: Header=BB12_352 Depth=3
	cmpb	$0, 320(%rsp)
	je	.LBB12_372
# BB#355:                               #   in Loop: Header=BB12_352 Depth=3
	incl	%r12d
	movq	408(%rsp), %rax
	movslq	%ebp, %rcx
	movslq	(%rax,%rcx,4), %rax
	testq	%rax, %rax
	js	.LBB12_372
# BB#356:                               #   in Loop: Header=BB12_352 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rcx
	movq	(%rcx,%rax,8), %rbx
	cmpb	$0, 60(%rbx)
	jne	.LBB12_372
# BB#357:                               #   in Loop: Header=BB12_352 Depth=3
	cmpb	$0, 61(%rbx)
	je	.LBB12_371
# BB#358:                               #   in Loop: Header=BB12_352 Depth=3
	leaq	600(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
.Ltmp344:
.Lcfi203:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp345:
# BB#359:                               #   in Loop: Header=BB12_352 Depth=3
	movq	%rax, 600(%rsp)
	movl	$0, (%rax)
	movl	$4, 612(%rsp)
	movl	$1, 616(%rsp)
.Ltmp346:
.Lcfi204:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	leaq	584(%rsp), %rsi
	leaq	2392(%rsp), %rdx
	callq	_ZN8NArchive3N7zL24FromUpdateItemToFileItemERKNS0_11CUpdateItemERNS0_9CFileItemERNS0_10CFileItem2E
.Ltmp347:
# BB#360:                               #   in Loop: Header=BB12_352 Depth=3
	movq	288(%rsp), %rax
	movq	%rax, 584(%rsp)
	movl	300(%rsp), %eax
	movl	%eax, 596(%rsp)
	movb	322(%rsp), %al
	movb	%al, 618(%rsp)
	movb	320(%rsp), %al
	movb	%al, 616(%rsp)
	movdqu	584(%rsp), %xmm0
	movdqa	%xmm0, 288(%rsp)
	movl	$0, 312(%rsp)
	movq	304(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	608(%rsp), %r14
	incq	%r14
	movl	316(%rsp), %eax
	cmpl	%eax, %r14d
	je	.LBB12_367
# BB#361:                               #   in Loop: Header=BB12_352 Depth=3
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp348:
.Lcfi205:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp349:
# BB#362:                               # %.noexc900
                                        #   in Loop: Header=BB12_352 Depth=3
	testq	%rbx, %rbx
	je	.LBB12_365
# BB#363:                               # %.noexc900
                                        #   in Loop: Header=BB12_352 Depth=3
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	movl	$0, %eax
	jle	.LBB12_366
# BB#364:                               # %._crit_edge.thread.i.i.i
                                        #   in Loop: Header=BB12_352 Depth=3
.Lcfi206:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	312(%rsp), %rax
	jmp	.LBB12_366
.LBB12_365:                             #   in Loop: Header=BB12_352 Depth=3
	xorl	%eax, %eax
.LBB12_366:                             # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB12_352 Depth=3
	movq	%r15, 304(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%r14d, 316(%rsp)
	movq	%r15, %rbx
.LBB12_367:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB12_352 Depth=3
	movq	600(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB12_368:                             #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_250 Depth=2
                                        #       Parent Loop BB12_352 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB12_368
# BB#369:                               #   in Loop: Header=BB12_352 Depth=3
	movl	608(%rsp), %eax
	movl	%eax, 312(%rsp)
	movl	616(%rsp), %eax
	movl	%eax, 320(%rsp)
	testq	%rdi, %rdi
	movq	88(%rsp), %r15          # 8-byte Reload
	je	.LBB12_371
# BB#370:                               #   in Loop: Header=BB12_352 Depth=3
.Lcfi207:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB12_371:                             #   in Loop: Header=BB12_352 Depth=3
.Ltmp351:
.Lcfi208:
	.cfi_escape 0x2e, 0x00
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	288(%rsp), %rsi
	leaq	2392(%rsp), %rdx
	callq	_ZN8NArchive3N7z16CArchiveDatabase7AddFileERKNS0_9CFileItemERKNS0_10CFileItem2E
.Ltmp352:
	.p2align	4, 0x90
.LBB12_372:                             #   in Loop: Header=BB12_352 Depth=3
	movq	304(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_374
# BB#373:                               #   in Loop: Header=BB12_352 Depth=3
.Lcfi209:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB12_374:                             # %_ZN8NArchive3N7z9CFileItemD2Ev.exit903
                                        #   in Loop: Header=BB12_352 Depth=3
	incl	%ebp
	cmpl	%r13d, %r12d
	jb	.LBB12_352
.LBB12_375:                             # %.thread977
                                        #   in Loop: Header=BB12_250 Depth=2
	movq	168(%rsp), %rbp         # 8-byte Reload
	incq	%rbp
	movslq	372(%rsp), %rax
	cmpq	%rax, %rbp
	jl	.LBB12_250
.LBB12_376:                             #   in Loop: Header=BB12_190 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB12_377:                             # %.thread979
                                        #   in Loop: Header=BB12_190 Depth=1
	movslq	12(%r13), %r14
	testq	%r14, %r14
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	je	.LBB12_487
# BB#378:                               #   in Loop: Header=BB12_190 Depth=1
	leaq	560(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	$24, 576(%rsp)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z8CRefItemEE+16, 552(%rsp)
.Ltmp354:
.Lcfi210:
	.cfi_escape 0x2e, 0x00
	leaq	552(%rsp), %rdi
	movl	%r14d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp355:
# BB#379:                               #   in Loop: Header=BB12_190 Depth=1
	movb	22(%rsp), %al           # 1-byte Reload
	movb	%al, 23(%rsp)
	testl	%r14d, %r14d
	jle	.LBB12_385
# BB#380:                               # %.lr.ph1734
                                        #   in Loop: Header=BB12_190 Depth=1
	movl	$1, %ebp
	movb	22(%rsp), %bl           # 1-byte Reload
	jmp	.LBB12_382
	.p2align	4, 0x90
.LBB12_381:                             # %._crit_edge2034
                                        #   in Loop: Header=BB12_382 Depth=2
	movzbl	23(%rsp), %ebx
	incq	%rbp
.LBB12_382:                             #   Parent Loop BB12_190 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rax
	movslq	-4(%rax,%rbp,4), %rsi
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%rsi,8), %rdx
	xorl	%ecx, %ecx
	testb	%bl, %bl
	setne	%cl
.Ltmp357:
.Lcfi211:
	.cfi_escape 0x2e, 0x00
	leaq	1320(%rsp), %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN8NArchive3N7z8CRefItemC2EjRKNS0_11CUpdateItemEb
.Ltmp358:
# BB#383:                               #   in Loop: Header=BB12_382 Depth=2
	movq	1336(%rsp), %rax
	movq	%rax, 1312(%rsp)
	movdqu	1320(%rsp), %xmm0
	movdqa	%xmm0, 1296(%rsp)
.Ltmp359:
.Lcfi212:
	.cfi_escape 0x2e, 0x00
	leaq	552(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp360:
# BB#384:                               #   in Loop: Header=BB12_382 Depth=2
	movq	568(%rsp), %rax
	movslq	564(%rsp), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	1312(%rsp), %rdx
	movq	%rdx, 16(%rax,%rcx,8)
	movdqa	1296(%rsp), %xmm0
	movdqu	%xmm0, (%rax,%rcx,8)
	incl	564(%rsp)
	cmpq	%r14, %rbp
	jl	.LBB12_381
.LBB12_385:                             # %._crit_edge1735
                                        #   in Loop: Header=BB12_190 Depth=1
.Ltmp362:
.Lcfi213:
	.cfi_escape 0x2e, 0x00
	movl	$_ZN8NArchive3N7zL18CompareUpdateItemsEPKNS0_8CRefItemES3_Pv, %esi
	leaq	552(%rsp), %rdi
	leaq	23(%rsp), %rdx
	callq	_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE4SortEPFiPKS2_S5_PvES6_
.Ltmp363:
# BB#386:                               #   in Loop: Header=BB12_190 Depth=1
	leaq	464(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	$4, 480(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 456(%rsp)
.Ltmp365:
.Lcfi214:
	.cfi_escape 0x2e, 0x00
	leaq	456(%rsp), %rdi
	movl	%r14d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp366:
# BB#387:                               # %.preheader999
                                        #   in Loop: Header=BB12_190 Depth=1
	testl	%r14d, %r14d
	movq	%r14, 48(%rsp)          # 8-byte Spill
	jle	.LBB12_391
# BB#388:                               # %.lr.ph1737.preheader
                                        #   in Loop: Header=BB12_190 Depth=1
	movl	$8, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_389:                             # %.lr.ph1737
                                        #   Parent Loop BB12_190 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	568(%rsp), %rax
	movl	(%rax,%rbx), %r14d
.Ltmp368:
.Lcfi215:
	.cfi_escape 0x2e, 0x00
	leaq	456(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp369:
# BB#390:                               #   in Loop: Header=BB12_389 Depth=2
	movq	472(%rsp), %rax
	movslq	468(%rsp), %rcx
	movl	%r14d, (%rax,%rcx,4)
	incl	468(%rsp)
	incq	%rbp
	addq	$24, %rbx
	movq	48(%rsp), %r14          # 8-byte Reload
	cmpq	%r14, %rbp
	jl	.LBB12_389
.LBB12_391:                             # %.preheader997.preheader
                                        #   in Loop: Header=BB12_190 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_392:                             # %.preheader997
                                        #   Parent Loop BB12_190 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_395 Depth 3
                                        #         Child Loop BB12_399 Depth 4
                                        #         Child Loop BB12_420 Depth 4
                                        #       Child Loop BB12_439 Depth 3
                                        #       Child Loop BB12_442 Depth 3
                                        #       Child Loop BB12_446 Depth 3
                                        #         Child Loop BB12_448 Depth 4
                                        #       Child Loop BB12_460 Depth 3
	cmpl	%r14d, %ebx
	jge	.LBB12_488
# BB#393:                               #   in Loop: Header=BB12_392 Depth=2
.Ltmp371:
.Lcfi216:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp372:
# BB#394:                               # %.lr.ph1744.preheader
                                        #   in Loop: Header=BB12_392 Depth=2
	movl	$0, (%r12)
	movslq	%ebx, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$4, 24(%rsp)            # 4-byte Folded Spill
	movl	$0, %ebp
	movl	%ebx, 108(%rsp)         # 4-byte Spill
	movl	%ebx, %eax
	movl	$0, %r13d
	movq	%r12, 184(%rsp)         # 8-byte Spill
	movq	3544(%rsp), %rsi
	.p2align	4, 0x90
.LBB12_395:                             # %.lr.ph1744
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_392 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_399 Depth 4
                                        #         Child Loop BB12_420 Depth 4
	movq	472(%rsp), %rcx
	cltq
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rdx
	movslq	(%rcx,%rax,4), %rax
	movq	(%rdx,%rax,8), %rax
	addq	32(%rax), %r13
	cmpq	32(%rsi), %r13
	ja	.LBB12_428
# BB#396:                               #   in Loop: Header=BB12_395 Depth=3
	cmpb	$0, 40(%rsi)
	je	.LBB12_426
# BB#397:                               #   in Loop: Header=BB12_395 Depth=3
	leaq	40(%rax), %rsi
	movslq	48(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB12_402
# BB#398:                               #   in Loop: Header=BB12_395 Depth=3
	movq	40(%rax), %rax
	movq	%rcx, %rdx
	shlq	$2, %rdx
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB12_399:                             #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_392 Depth=2
                                        #       Parent Loop BB12_395 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$47, -4(%rax,%rdi)
	je	.LBB12_403
# BB#400:                               #   in Loop: Header=BB12_399 Depth=4
	addq	$-4, %rdi
	jne	.LBB12_399
# BB#401:                               #   in Loop: Header=BB12_395 Depth=3
	movl	$-1, %edi
	cmpl	$46, -4(%rax,%rdx)
	jne	.LBB12_405
	jmp	.LBB12_407
	.p2align	4, 0x90
.LBB12_402:                             #   in Loop: Header=BB12_395 Depth=3
	xorl	%edx, %edx
	jmp	.LBB12_409
.LBB12_403:                             #   in Loop: Header=BB12_395 Depth=3
	leaq	-4(%rax,%rdi), %rdi
	subq	%rax, %rdi
	shrq	$2, %rdi
	.p2align	4, 0x90
.LBB12_404:                             #   in Loop: Header=BB12_395 Depth=3
	cmpl	$46, -4(%rax,%rdx)
	je	.LBB12_407
.LBB12_405:                             #   in Loop: Header=BB12_395 Depth=3
	addq	$-4, %rdx
	jne	.LBB12_404
# BB#406:                               #   in Loop: Header=BB12_395 Depth=3
	movl	%ecx, %edx
	jmp	.LBB12_409
.LBB12_407:                             # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.i.i
                                        #   in Loop: Header=BB12_395 Depth=3
	leaq	-4(%rax,%rdx), %rbx
	subq	%rax, %rbx
	shrq	$2, %rbx
	testl	%ebx, %ebx
	movl	%ecx, %edx
	js	.LBB12_409
# BB#408:                               #   in Loop: Header=BB12_395 Depth=3
	leal	1(%rbx), %eax
	cmpl	%edi, %ebx
	movl	%eax, %edx
	cmovll	%ecx, %edx
	testl	%edi, %edi
	cmovsl	%eax, %edx
	.p2align	4, 0x90
.LBB12_409:                             # %_ZNK8NArchive3N7z11CUpdateItem15GetExtensionPosEv.exit.i
                                        #   in Loop: Header=BB12_395 Depth=3
	subl	%edx, %ecx
.Ltmp374:
.Lcfi217:
	.cfi_escape 0x2e, 0x00
	leaq	536(%rsp), %rdi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp375:
# BB#410:                               # %_ZNK8NArchive3N7z11CUpdateItem12GetExtensionEv.exit
                                        #   in Loop: Header=BB12_395 Depth=3
	testq	%rbp, %rbp
	je	.LBB12_413
# BB#411:                               #   in Loop: Header=BB12_395 Depth=3
	movq	536(%rsp), %rdi
.Ltmp377:
.Lcfi218:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rsi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp378:
# BB#412:                               # %_ZNK11CStringBaseIwE13CompareNoCaseERKS0_.exit
                                        #   in Loop: Header=BB12_395 Depth=3
	movl	$49, %ebx
	testl	%eax, %eax
	jne	.LBB12_423
	jmp	.LBB12_422
	.p2align	4, 0x90
.LBB12_413:                             #   in Loop: Header=BB12_395 Depth=3
	movl	$0, (%r12)
	movslq	544(%rsp), %rbx
	incq	%rbx
	movl	24(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %ebx
	jne	.LBB12_415
# BB#414:                               #   in Loop: Header=BB12_395 Depth=3
	movq	184(%rsp), %r14         # 8-byte Reload
	movl	%eax, %ebx
	jmp	.LBB12_419
.LBB12_415:                             #   in Loop: Header=BB12_395 Depth=3
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp379:
.Lcfi219:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp380:
# BB#416:                               # %.noexc921
                                        #   in Loop: Header=BB12_395 Depth=3
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB12_418
# BB#417:                               # %._crit_edge.thread.i.i915
                                        #   in Loop: Header=BB12_395 Depth=3
.Lcfi220:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB12_418:                             # %._crit_edge16.i.i916
                                        #   in Loop: Header=BB12_395 Depth=3
	movl	$0, (%r14)
	movq	%r14, %r12
.LBB12_419:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i917
                                        #   in Loop: Header=BB12_395 Depth=3
	movq	536(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_420:                             #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_392 Depth=2
                                        #       Parent Loop BB12_395 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%rax,%rcx), %edx
	movl	%edx, (%r12,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB12_420
# BB#421:                               #   in Loop: Header=BB12_395 Depth=3
	movq	%r14, 184(%rsp)         # 8-byte Spill
	movl	%ebx, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB12_422:                             # %_ZN11CStringBaseIwEaSERKS0_.exit922
                                        #   in Loop: Header=BB12_395 Depth=3
	xorl	%ebx, %ebx
.LBB12_423:                             #   in Loop: Header=BB12_395 Depth=3
	movq	536(%rsp), %rdi
	testq	%rdi, %rdi
	movq	48(%rsp), %r14          # 8-byte Reload
	je	.LBB12_425
# BB#424:                               #   in Loop: Header=BB12_395 Depth=3
.Lcfi221:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB12_425:                             # %_ZN11CStringBaseIwED2Ev.exit925
                                        #   in Loop: Header=BB12_395 Depth=3
	testl	%ebx, %ebx
	movl	%ebp, %r15d
	movq	3544(%rsp), %rsi
	jne	.LBB12_429
.LBB12_426:                             #   in Loop: Header=BB12_395 Depth=3
	leaq	1(%rbp), %r15
	cmpq	352(%rsp), %r15         # 8-byte Folded Reload
	jae	.LBB12_429
# BB#427:                               #   in Loop: Header=BB12_395 Depth=3
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	1(%rbp,%rax), %rax
	cmpq	%r14, %rax
	movq	%r15, %rbp
	jl	.LBB12_395
	jmp	.LBB12_429
	.p2align	4, 0x90
.LBB12_428:                             #   in Loop: Header=BB12_392 Depth=2
	movl	%ebp, %r15d
.LBB12_429:                             # %.critedge
                                        #   in Loop: Header=BB12_392 Depth=2
	testl	%r15d, %r15d
	movl	$1, %eax
	cmovlel	%eax, %r15d
.Ltmp382:
.Lcfi222:
	.cfi_escape 0x2e, 0x00
	movl	$184, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp383:
# BB#430:                               #   in Loop: Header=BB12_392 Depth=2
.Ltmp385:
.Lcfi223:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	callq	_ZN8NArchive3N7z15CFolderInStreamC1Ev
.Ltmp386:
# BB#431:                               #   in Loop: Header=BB12_392 Depth=2
	movq	(%r13), %rax
.Ltmp388:
.Lcfi224:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp389:
# BB#432:                               # %_ZN9CMyComPtrI19ISequentialInStreamEC2EPS0_.exit
                                        #   in Loop: Header=BB12_392 Depth=2
	movq	472(%rsp), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,4), %rdx
.Ltmp391:
.Lcfi225:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	movq	3536(%rsp), %rsi
	movl	%r15d, %ecx
	callq	_ZN8NArchive3N7z15CFolderInStream4InitEP22IArchiveUpdateCallbackPKjj
.Ltmp392:
# BB#433:                               #   in Loop: Header=BB12_392 Depth=2
	leaq	808(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	$8, 824(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, 800(%rsp)
	movdqu	%xmm0, 32(%rax)
	movq	$8, 856(%rsp)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE+16, 832(%rsp)
	movdqu	%xmm0, 64(%rax)
	movq	$4, 888(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 864(%rsp)
	movdqu	%xmm0, 96(%rax)
	movq	$8, 920(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 896(%rsp)
	movb	$0, 932(%rsp)
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	12(%rax), %rbx
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	120(%rcx), %r9
.Ltmp394:
.Lcfi226:
	.cfi_escape 0x2e, 0x10
	xorl	%edx, %edx
	leaq	1344(%rsp), %rdi
	movq	%r13, %rsi
	leaq	96(%rsp), %rcx
	leaq	800(%rsp), %r8
	pushq	%rbp
.Lcfi227:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi228:
	.cfi_adjust_cfa_offset 8
	callq	_ZN8NArchive3N7z8CEncoder6EncodeEP19ISequentialInStreamPKyS5_RNS0_7CFolderEP20ISequentialOutStreamR13CRecordVectorIyEP21ICompressProgressInfo
	addq	$16, %rsp
.Lcfi229:
	.cfi_adjust_cfa_offset -16
.Ltmp395:
# BB#434:                               #   in Loop: Header=BB12_392 Depth=2
	testl	%eax, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmovnel	%eax, %ecx
	je	.LBB12_436
# BB#435:                               #   in Loop: Header=BB12_392 Depth=2
	movl	$1, %ebp
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	108(%rsp), %ebx         # 4-byte Reload
	jmp	.LBB12_482
	.p2align	4, 0x90
.LBB12_436:                             # %.preheader994
                                        #   in Loop: Header=BB12_392 Depth=2
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	12(%rax), %rcx
	cmpl	%ecx, %ebx
	movq	8(%rsp), %rbp           # 8-byte Reload
	jge	.LBB12_443
# BB#437:                               # %.lr.ph1762
                                        #   in Loop: Header=BB12_392 Depth=2
	movq	16(%rax), %rax
	movq	56(%rbp), %rsi
	movl	%ecx, %edi
	subl	%ebx, %edi
	leaq	-1(%rcx), %rdx
	subq	%rbx, %rdx
	andq	$3, %rdi
	je	.LBB12_440
# BB#438:                               # %.prol.preheader3177
                                        #   in Loop: Header=BB12_392 Depth=2
	negq	%rdi
	.p2align	4, 0x90
.LBB12_439:                             #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_392 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	(%rax,%rbx,8), %rsi
	movq	%rsi, 56(%rbp)
	incq	%rbx
	incq	%rdi
	jne	.LBB12_439
.LBB12_440:                             # %.prol.loopexit3178
                                        #   in Loop: Header=BB12_392 Depth=2
	cmpq	$3, %rdx
	jb	.LBB12_443
# BB#441:                               # %.lr.ph1762.new
                                        #   in Loop: Header=BB12_392 Depth=2
	subq	%rbx, %rcx
	leaq	24(%rax,%rbx,8), %rax
	.p2align	4, 0x90
.LBB12_442:                             #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_392 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	-24(%rax), %rsi
	movq	%rsi, 56(%rbp)
	addq	-16(%rax), %rsi
	movq	%rsi, 56(%rbp)
	addq	-8(%rax), %rsi
	movq	%rsi, 56(%rbp)
	addq	(%rax), %rsi
	movq	%rsi, 56(%rbp)
	addq	$32, %rax
	addq	$-4, %rcx
	jne	.LBB12_442
.LBB12_443:                             # %._crit_edge1763
                                        #   in Loop: Header=BB12_392 Depth=2
	movl	908(%rsp), %eax
	testl	%eax, %eax
	movq	%r13, 24(%rsp)          # 8-byte Spill
	je	.LBB12_451
# BB#444:                               # %.preheader.i927
                                        #   in Loop: Header=BB12_392 Depth=2
	movslq	844(%rsp), %rcx
	testq	%rcx, %rcx
	jle	.LBB12_452
# BB#445:                               # %.preheader.i927.split.us.preheader
                                        #   in Loop: Header=BB12_392 Depth=2
	movq	848(%rsp), %rdx
.LBB12_446:                             # %.preheader.i927.split.us
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_392 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_448 Depth 4
	testl	%eax, %eax
	jle	.LBB12_547
# BB#447:                               # %.lr.ph.i.i930.us.preheader
                                        #   in Loop: Header=BB12_446 Depth=3
	decl	%eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB12_448:                             # %.lr.ph.i.i930.us
                                        #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_392 Depth=2
                                        #       Parent Loop BB12_446 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	%eax, 4(%rdx,%rsi,8)
	je	.LBB12_450
# BB#449:                               #   in Loop: Header=BB12_448 Depth=4
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB12_448
	jmp	.LBB12_454
	.p2align	4, 0x90
.LBB12_450:                             # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.i933.us
                                        #   in Loop: Header=BB12_446 Depth=3
	testl	%esi, %esi
	jns	.LBB12_446
	jmp	.LBB12_454
.LBB12_451:                             #   in Loop: Header=BB12_392 Depth=2
	xorl	%eax, %eax
	jmp	.LBB12_455
.LBB12_452:                             # %.preheader.i927.split
                                        #   in Loop: Header=BB12_392 Depth=2
	testl	%eax, %eax
	jle	.LBB12_547
# BB#453:                               #   in Loop: Header=BB12_392 Depth=2
	decl	%eax
.LBB12_454:                             # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.thread.i934
                                        #   in Loop: Header=BB12_392 Depth=2
	movq	912(%rsp), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
.LBB12_455:                             # %_ZNK8NArchive3N7z7CFolder13GetUnpackSizeEv.exit937
                                        #   in Loop: Header=BB12_392 Depth=2
	addq	%rax, 48(%rbp)
.Ltmp399:
.Lcfi230:
	.cfi_escape 0x2e, 0x00
	movl	$136, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp400:
# BB#456:                               # %.noexc938
                                        #   in Loop: Header=BB12_392 Depth=2
.Ltmp401:
.Lcfi231:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	leaq	800(%rsp), %rsi
	callq	_ZN8NArchive3N7z7CFolderC2ERKS1_
.Ltmp402:
# BB#457:                               #   in Loop: Header=BB12_392 Depth=2
.Ltmp404:
.Lcfi232:
	.cfi_escape 0x2e, 0x00
	movq	336(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp405:
# BB#458:                               # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE3AddERKS2_.exit942
                                        #   in Loop: Header=BB12_392 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	112(%rdx), %rax
	movslq	108(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 108(%rdx)
	testl	%r15d, %r15d
	jle	.LBB12_477
# BB#459:                               # %.lr.ph1768
                                        #   in Loop: Header=BB12_392 Depth=2
	movl	%r15d, %r13d
	shlq	$2, 72(%rsp)            # 8-byte Folded Spill
	xorl	%r14d, %r14d
	movl	$0, 104(%rsp)           # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB12_460:                             #   Parent Loop BB12_190 Depth=1
                                        #     Parent Loop BB12_392 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	472(%rsp), %rax
	addq	72(%rsp), %rax          # 8-byte Folded Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rcx
	movslq	(%rax,%r14,4), %rax
	movq	(%rcx,%rax,8), %rbx
	leaq	504(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
.Ltmp407:
.Lcfi233:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp408:
# BB#461:                               #   in Loop: Header=BB12_460 Depth=3
	movq	%rax, 504(%rsp)
	movl	$0, (%rax)
	movl	$4, 516(%rsp)
	movl	$1, 520(%rsp)
	cmpb	$0, 61(%rbx)
	je	.LBB12_463
# BB#462:                               #   in Loop: Header=BB12_460 Depth=3
.Ltmp410:
.Lcfi234:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	leaq	488(%rsp), %rsi
	leaq	2392(%rsp), %rdx
	callq	_ZN8NArchive3N7zL24FromUpdateItemToFileItemERKNS0_11CUpdateItemERNS0_9CFileItemERNS0_10CFileItem2E
.Ltmp411:
	jmp	.LBB12_464
	.p2align	4, 0x90
.LBB12_463:                             #   in Loop: Header=BB12_460 Depth=3
	movl	(%rbx), %esi
.Ltmp412:
.Lcfi235:
	.cfi_escape 0x2e, 0x00
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	488(%rsp), %rdx
	leaq	2392(%rsp), %rcx
	callq	_ZNK8NArchive3N7z16CArchiveDatabase7GetFileEiRNS0_9CFileItemERNS0_10CFileItem2E
.Ltmp413:
.LBB12_464:                             #   in Loop: Header=BB12_460 Depth=3
	movzbl	521(%rsp), %eax
	orb	2428(%rsp), %al
	je	.LBB12_466
# BB#465:                               #   in Loop: Header=BB12_460 Depth=3
	movl	$-2147467259, %eax      # imm = 0x80004005
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %ebp
	jmp	.LBB12_472
	.p2align	4, 0x90
.LBB12_466:                             #   in Loop: Header=BB12_460 Depth=3
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	104(%rcx), %rax
	cmpb	$0, (%rax,%r14)
	je	.LBB12_469
# BB#467:                               #   in Loop: Header=BB12_460 Depth=3
	movq	136(%rcx), %rax
	movl	(%rax,%r14,4), %eax
	movl	%eax, 500(%rsp)
	movq	168(%rcx), %rax
	movq	(%rax,%r14,8), %rax
	movq	%rax, 488(%rsp)
	testq	%rax, %rax
	je	.LBB12_470
# BB#468:                               #   in Loop: Header=BB12_460 Depth=3
	movb	$1, 522(%rsp)
	movb	$1, 520(%rsp)
	incl	104(%rsp)               # 4-byte Folded Spill
	jmp	.LBB12_471
.LBB12_469:                             #   in Loop: Header=BB12_460 Depth=3
	movl	$57, %ebp
	jmp	.LBB12_472
.LBB12_470:                             #   in Loop: Header=BB12_460 Depth=3
	movb	$0, 522(%rsp)
	movb	$0, 520(%rsp)
.LBB12_471:                             #   in Loop: Header=BB12_460 Depth=3
	movq	40(%rsp), %rdi          # 8-byte Reload
	xorl	%ebp, %ebp
.Ltmp414:
.Lcfi236:
	.cfi_escape 0x2e, 0x00
	leaq	488(%rsp), %rsi
	leaq	2392(%rsp), %rdx
	callq	_ZN8NArchive3N7z16CArchiveDatabase7AddFileERKNS0_9CFileItemERKNS0_10CFileItem2E
.Ltmp415:
.LBB12_472:                             #   in Loop: Header=BB12_460 Depth=3
	movq	504(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_474
# BB#473:                               #   in Loop: Header=BB12_460 Depth=3
.Lcfi237:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB12_474:                             # %_ZN8NArchive3N7z9CFileItemD2Ev.exit946
                                        #   in Loop: Header=BB12_460 Depth=3
	movl	%ebp, %eax
	andb	$63, %al
	cmpb	$57, %al
	je	.LBB12_476
# BB#475:                               # %_ZN8NArchive3N7z9CFileItemD2Ev.exit946
                                        #   in Loop: Header=BB12_460 Depth=3
	testb	%al, %al
	jne	.LBB12_480
.LBB12_476:                             #   in Loop: Header=BB12_460 Depth=3
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB12_460
	jmp	.LBB12_478
.LBB12_477:                             #   in Loop: Header=BB12_392 Depth=2
	movl	$0, 104(%rsp)           # 4-byte Folded Spill
.LBB12_478:                             # %._crit_edge1769
                                        #   in Loop: Header=BB12_392 Depth=2
.Ltmp417:
.Lcfi238:
	.cfi_escape 0x2e, 0x00
	movq	528(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp418:
	movq	48(%rsp), %r14          # 8-byte Reload
# BB#479:                               #   in Loop: Header=BB12_392 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	144(%rdx), %rax
	movslq	140(%rdx), %rcx
	movl	104(%rsp), %esi         # 4-byte Reload
	movl	%esi, (%rax,%rcx,4)
	incl	140(%rdx)
	addl	108(%rsp), %r15d        # 4-byte Folded Reload
	xorl	%ebp, %ebp
	movl	%r15d, %ebx
	jmp	.LBB12_481
.LBB12_480:                             # %.thread986.loopexit
                                        #   in Loop: Header=BB12_392 Depth=2
	movq	48(%rsp), %r14          # 8-byte Reload
	movl	108(%rsp), %ebx         # 4-byte Reload
.LBB12_481:                             # %.thread986
                                        #   in Loop: Header=BB12_392 Depth=2
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB12_482:                             # %.thread986
                                        #   in Loop: Header=BB12_392 Depth=2
.Ltmp422:
.Lcfi239:
	.cfi_escape 0x2e, 0x00
	leaq	800(%rsp), %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp423:
# BB#483:                               #   in Loop: Header=BB12_392 Depth=2
	movq	(%r13), %rax
.Ltmp427:
.Lcfi240:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp428:
# BB#484:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit950
                                        #   in Loop: Header=BB12_392 Depth=2
	testq	%r12, %r12
	je	.LBB12_486
# BB#485:                               #   in Loop: Header=BB12_392 Depth=2
.Lcfi241:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB12_486:                             # %_ZN11CStringBaseIwED2Ev.exit951
                                        #   in Loop: Header=BB12_392 Depth=2
	testl	%ebp, %ebp
	je	.LBB12_392
	jmp	.LBB12_489
.LBB12_487:                             #   in Loop: Header=BB12_190 Depth=1
	movl	$25, %ebp
	jmp	.LBB12_491
.LBB12_488:                             #   in Loop: Header=BB12_190 Depth=1
	xorl	%ebp, %ebp
.LBB12_489:                             #   in Loop: Header=BB12_190 Depth=1
.Ltmp432:
.Lcfi242:
	.cfi_escape 0x2e, 0x00
	leaq	456(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp433:
# BB#490:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp437:
.Lcfi243:
	.cfi_escape 0x2e, 0x00
	leaq	552(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp438:
.LBB12_491:                             # %.thread976
                                        #   in Loop: Header=BB12_190 Depth=1
.Ltmp442:
.Lcfi244:
	.cfi_escape 0x2e, 0x00
	leaq	1344(%rsp), %rdi
	callq	_ZN8NArchive3N7z8CEncoderD1Ev
.Ltmp443:
# BB#492:                               #   in Loop: Header=BB12_190 Depth=1
.Ltmp447:
.Lcfi245:
	.cfi_escape 0x2e, 0x00
	leaq	624(%rsp), %r12
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp448:
	movq	344(%rsp), %rbx         # 8-byte Reload
# BB#493:                               #   in Loop: Header=BB12_190 Depth=1
	testl	%ebp, %ebp
	je	.LBB12_188
# BB#494:                               #   in Loop: Header=BB12_190 Depth=1
	cmpl	$25, %ebp
	je	.LBB12_188
# BB#495:
	cmpl	$23, %ebp
	jne	.LBB12_538
.LBB12_496:                             # %.thread988
	movl	$-2147467259, %ebp      # imm = 0x80004005
	movq	168(%rsp), %rax         # 8-byte Reload
	cmpl	372(%rsp), %eax
	jne	.LBB12_540
# BB#497:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 432(%rsp)
	movq	$4, 448(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 424(%rsp)
	movq	152(%rsp), %rbx         # 8-byte Reload
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	movq	88(%rsp), %r14          # 8-byte Reload
	jle	.LBB12_508
# BB#498:                               # %.lr.ph1706
	xorl	%ebp, %ebp
	leaq	424(%rsp), %r15
.LBB12_499:                             # =>This Inner Loop Header: Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	cmpb	$0, 60(%rax)
	je	.LBB12_503
# BB#500:                               #   in Loop: Header=BB12_499 Depth=1
	cmpb	$0, 63(%rax)
	jne	.LBB12_505
# BB#501:                               #   in Loop: Header=BB12_499 Depth=1
	cmpb	$0, 62(%rax)
	jne	.LBB12_505
# BB#502:                               # %_ZNK8NArchive3N7z11CUpdateItem9HasStreamEv.exit
                                        #   in Loop: Header=BB12_499 Depth=1
	cmpq	$0, 32(%rax)
	jne	.LBB12_507
	jmp	.LBB12_505
.LBB12_503:                             #   in Loop: Header=BB12_499 Depth=1
	movslq	(%rax), %rax
	cmpq	$-1, %rax
	je	.LBB12_505
# BB#504:                               #   in Loop: Header=BB12_499 Depth=1
	movq	176(%r14), %rdx
	movq	(%rdx,%rax,8), %rax
	cmpb	$0, 32(%rax)
	jne	.LBB12_507
.LBB12_505:                             # %_ZNK8NArchive3N7z11CUpdateItem9HasStreamEv.exit.thread
                                        #   in Loop: Header=BB12_499 Depth=1
.Ltmp450:
.Lcfi246:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp451:
# BB#506:                               # %_ZN13CRecordVectorIiE3AddEi.exit803
                                        #   in Loop: Header=BB12_499 Depth=1
	movq	440(%rsp), %rax
	movslq	436(%rsp), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	436(%rsp)
	movl	(%rbx), %ecx
.LBB12_507:                             #   in Loop: Header=BB12_499 Depth=1
	incq	%rbp
	movslq	%ecx, %rax
	cmpq	%rax, %rbp
	jl	.LBB12_499
.LBB12_508:                             # %._crit_edge1707
.Ltmp453:
.Lcfi247:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdi
	movl	$_ZN8NArchive3N7zL17CompareEmptyItemsEPKiS2_Pv, %esi
	movq	56(%rsp), %rdx          # 8-byte Reload
	callq	_ZN13CRecordVectorIiE4SortEPFiPKiS2_PvES3_
.Ltmp454:
# BB#509:                               # %.preheader
	cmpl	$0, 436(%rsp)
	jle	.LBB12_519
# BB#510:                               # %.lr.ph
	leaq	1272(%rsp), %r15
	xorl	%ebp, %ebp
	leaq	1256(%rsp), %r14
	leaq	2392(%rsp), %r12
.LBB12_511:                             # =>This Inner Loop Header: Depth=1
	movq	440(%rsp), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rcx
	movslq	(%rax,%rbp,4), %rax
	movq	(%rcx,%rax,8), %rbx
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r15)
.Ltmp456:
.Lcfi248:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp457:
# BB#512:                               #   in Loop: Header=BB12_511 Depth=1
	movq	%rax, 1272(%rsp)
	movl	$0, (%rax)
	movl	$4, 1284(%rsp)
	movl	$1, 1288(%rsp)
	cmpb	$0, 61(%rbx)
	je	.LBB12_514
# BB#513:                               #   in Loop: Header=BB12_511 Depth=1
.Ltmp459:
.Lcfi249:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	_ZN8NArchive3N7zL24FromUpdateItemToFileItemERKNS0_11CUpdateItemERNS0_9CFileItemERNS0_10CFileItem2E
.Ltmp460:
	jmp	.LBB12_515
.LBB12_514:                             #   in Loop: Header=BB12_511 Depth=1
	movl	(%rbx), %esi
.Ltmp461:
.Lcfi250:
	.cfi_escape 0x2e, 0x00
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%r12, %rcx
	callq	_ZNK8NArchive3N7z16CArchiveDatabase7GetFileEiRNS0_9CFileItemERNS0_10CFileItem2E
.Ltmp462:
.LBB12_515:                             #   in Loop: Header=BB12_511 Depth=1
.Ltmp463:
.Lcfi251:
	.cfi_escape 0x2e, 0x00
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	_ZN8NArchive3N7z16CArchiveDatabase7AddFileERKNS0_9CFileItemERKNS0_10CFileItem2E
.Ltmp464:
# BB#516:                               #   in Loop: Header=BB12_511 Depth=1
	movq	1272(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_518
# BB#517:                               #   in Loop: Header=BB12_511 Depth=1
.Lcfi252:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB12_518:                             # %_ZN8NArchive3N7z9CFileItemD2Ev.exit
                                        #   in Loop: Header=BB12_511 Depth=1
	incq	%rbp
	movslq	436(%rsp), %rax
	cmpq	%rax, %rbp
	jl	.LBB12_511
.LBB12_519:                             # %._crit_edge
.Ltmp468:
.Lcfi253:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp469:
# BB#520:
	xorl	%ebp, %ebp
.Ltmp471:
.Lcfi254:
	.cfi_escape 0x2e, 0x00
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZN8NArchive3N7z16CArchiveDatabase11ReserveDownEv
.Ltmp472:
	jmp	.LBB12_540
.LBB12_521:
	movq	3552(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_537
# BB#522:
	movq	$0, 248(%rsp)
	movq	(%rdi), %rax
.Ltmp101:
.Lcfi255:
	.cfi_escape 0x2e, 0x00
	leaq	248(%rsp), %rsi
	callq	*40(%rax)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
.Ltmp102:
# BB#523:
	movl	$1, %ebx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	jne	.LBB12_535
# BB#524:
	movq	248(%rsp), %rbx
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	$0, 24(%rcx)
	movq	16(%rcx), %rbp
	movl	$0, (%rbp)
	xorl	%r15d, %r15d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB12_525:                             # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB12_525
# BB#526:                               # %_Z11MyStringLenIwEiPKT_.exit.i
	movl	28(%rcx), %r12d
	cmpl	%r15d, %r12d
	je	.LBB12_532
# BB#527:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp103:
.Lcfi256:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r14
.Ltmp104:
# BB#528:                               # %.noexc836
	xorl	%eax, %eax
	testq	%rbp, %rbp
	movq	80(%rsp), %rcx          # 8-byte Reload
	je	.LBB12_531
# BB#529:                               # %.noexc836
	testl	%r12d, %r12d
	jle	.LBB12_531
# BB#530:                               # %._crit_edge.thread.i.i830
.Lcfi257:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	80(%rsp), %rcx          # 8-byte Reload
	movslq	24(%rcx), %rax
.LBB12_531:                             # %._crit_edge16.i.i831
	movq	%r14, 16(%rcx)
	movl	$0, (%r14,%rax,4)
	movl	%r15d, 28(%rcx)
	movq	%r14, %rbp
.LBB12_532:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i834.preheader
	decl	%r15d
	.p2align	4, 0x90
.LBB12_533:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i834
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rbp)
	addq	$4, %rbp
	testl	%eax, %eax
	jne	.LBB12_533
# BB#534:                               # %_ZN11CStringBaseIwEaSEPKw.exit
	movl	%r15d, 24(%rcx)
	xorl	%ebx, %ebx
.LBB12_535:
	movq	248(%rsp), %rdi
.Ltmp108:
.Lcfi258:
	.cfi_escape 0x2e, 0x00
	callq	SysFreeString
.Ltmp109:
# BB#536:                               # %_ZN10CMyComBSTRD2Ev.exit838
	testl	%ebx, %ebx
	movq	32(%rsp), %rax          # 8-byte Reload
	jne	.LBB12_539
	jmp	.LBB12_183
.LBB12_537:
	movl	$-2147467263, %ebp      # imm = 0x80004001
	jmp	.LBB12_540
.LBB12_538:                             # %.loopexit1011.loopexit
	movq	32(%rsp), %rax          # 8-byte Reload
.LBB12_539:                             # %.loopexit1011
	movl	%eax, %ebp
.LBB12_540:                             # %.loopexit1011
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE+16, 112(%rsp)
.Ltmp482:
.Lcfi259:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp483:
# BB#541:
.Ltmp488:
.Lcfi260:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp489:
.LBB12_542:
.Ltmp493:
.Lcfi261:
	.cfi_escape 0x2e, 0x00
	leaq	1856(%rsp), %rdi
	callq	_ZN8NArchive3N7z14CThreadDecoderD2Ev
.Ltmp494:
# BB#543:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp498:
.Lcfi262:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp499:
.LBB12_544:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit787
.Ltmp503:
.Lcfi263:
	.cfi_escape 0x2e, 0x00
	leaq	360(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp504:
# BB#545:
.Lcfi264:
	.cfi_escape 0x2e, 0x00
	leaq	392(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.LBB12_546:
	movl	%ebp, %eax
	addq	$3480, %rsp             # imm = 0xD98
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_547:                             # %.us-lcssa1764.us
.Lcfi265:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
.Ltmp396:
.Lcfi266:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp397:
# BB#548:                               # %.noexc936
.LBB12_549:                             # %.us-lcssa.us
.Lcfi267:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
.Ltmp284:
.Lcfi268:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp285:
# BB#550:                               # %.noexc876
.LBB12_551:                             # %.loopexit.split-lp1007
.Ltmp286:
	jmp	.LBB12_601
.LBB12_552:                             # %.loopexit.split-lp
.Ltmp398:
	jmp	.LBB12_698
.LBB12_553:
.Ltmp473:
	jmp	.LBB12_644
.LBB12_554:
.Ltmp470:
	jmp	.LBB12_644
.LBB12_555:
.Ltmp455:
	jmp	.LBB12_588
.LBB12_556:
.Ltmp291:
	movq	%rax, %r14
.Lcfi269:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_602
.LBB12_557:
.Ltmp110:
	jmp	.LBB12_644
.LBB12_558:
.Ltmp105:
	movq	%rax, %r14
	movq	248(%rsp), %rdi
.Ltmp106:
.Lcfi270:
	.cfi_escape 0x2e, 0x00
	callq	SysFreeString
.Ltmp107:
	jmp	.LBB12_645
.LBB12_559:                             # %.us-lcssa1783
.Ltmp78:
	jmp	.LBB12_561
.LBB12_560:                             # %.us-lcssa1782
.Ltmp75:
.LBB12_561:
	movq	%rax, %r14
.Ltmp79:
.Lcfi271:
	.cfi_escape 0x2e, 0x00
	leaq	2392(%rsp), %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp80:
	jmp	.LBB12_645
.LBB12_562:
.Ltmp81:
.Lcfi272:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_563:
.Ltmp243:
	movq	%rax, %r14
.Ltmp244:
.Lcfi273:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp245:
	jmp	.LBB12_650
.LBB12_564:
.Ltmp246:
.Lcfi274:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_565:
.Ltmp231:
	jmp	.LBB12_567
.LBB12_566:
.Ltmp228:
.LBB12_567:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorI5CPropE+16, 760(%rsp)
.Ltmp232:
.Lcfi275:
	.cfi_escape 0x2e, 0x00
	leaq	760(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp233:
# BB#568:                               # %_ZN13CObjectVectorI5CPropED2Ev.exit.i.i
.Ltmp238:
.Lcfi276:
	.cfi_escape 0x2e, 0x00
	leaq	760(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp239:
	jmp	.LBB12_650
.LBB12_569:
.Ltmp234:
	movq	%rax, %rbx
.Ltmp235:
.Lcfi277:
	.cfi_escape 0x2e, 0x00
	leaq	760(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp236:
	jmp	.LBB12_631
.LBB12_570:
.Ltmp237:
.Lcfi278:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_571:                             # %.us-lcssa1785.us
.Ltmp91:
	jmp	.LBB12_644
.LBB12_572:
.Ltmp96:
	movq	%rax, %r14
.Lcfi279:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_645
.LBB12_573:
.Ltmp144:
	movq	%rax, %r14
.Lcfi280:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_621
.LBB12_574:
.Ltmp137:
	jmp	.LBB12_619
.LBB12_575:
.Ltmp197:
	jmp	.LBB12_619
.LBB12_576:
.Ltmp129:
	movq	%rax, %r14
.Lcfi281:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_625
.LBB12_577:
.Ltmp189:
	movq	%rax, %r14
.Lcfi282:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_614
.LBB12_578:
.Ltmp182:
	jmp	.LBB12_619
.LBB12_579:
.Ltmp174:
	movq	%rax, %r14
.Lcfi283:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_617
.LBB12_580:
.Ltmp167:
	jmp	.LBB12_619
.LBB12_581:
.Ltmp220:
	movq	%rax, %r14
.Ltmp221:
.Lcfi284:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp222:
	jmp	.LBB12_650
.LBB12_582:
.Ltmp223:
.Lcfi285:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_583:
.Ltmp159:
	movq	%rax, %r14
.Lcfi286:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_623
.LBB12_584:
.Ltmp152:
	jmp	.LBB12_619
.LBB12_585:
.Ltmp452:
	jmp	.LBB12_588
.LBB12_586:
.Ltmp299:
	jmp	.LBB12_679
.LBB12_587:
.Ltmp458:
.LBB12_588:
	movq	%rax, %r14
	jmp	.LBB12_612
.LBB12_589:
.Ltmp490:
	jmp	.LBB12_598
.LBB12_590:
.Ltmp484:
	movq	%rax, %r14
.Ltmp485:
.Lcfi287:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp486:
	jmp	.LBB12_599
.LBB12_591:
.Ltmp487:
.Lcfi288:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_592:
.Ltmp439:
	jmp	.LBB12_713
.LBB12_593:
.Ltmp434:
	jmp	.LBB12_693
.LBB12_594:
.Ltmp367:
	jmp	.LBB12_673
.LBB12_595:                             # %.loopexit.split-lp1001
.Ltmp364:
	jmp	.LBB12_693
.LBB12_596:
.Ltmp356:
	jmp	.LBB12_693
.LBB12_597:
.Ltmp33:
.LBB12_598:
	movq	%rax, %r14
.LBB12_599:
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB12_723
.LBB12_600:                             # %.loopexit1006
.Ltmp294:
.LBB12_601:                             # %.body879
	movq	%rax, %r14
.LBB12_602:                             # %.body879
.Ltmp295:
.Lcfi289:
	.cfi_escape 0x2e, 0x00
	leaq	936(%rsp), %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp296:
	jmp	.LBB12_680
.LBB12_603:
.Ltmp309:
	movq	%rax, %r14
	jmp	.LBB12_683
.LBB12_604:
.Ltmp314:
	jmp	.LBB12_653
.LBB12_605:                             # %.us-lcssa1785
.Ltmp88:
	jmp	.LBB12_644
.LBB12_606:
.Ltmp500:
	jmp	.LBB12_696
.LBB12_607:
.Ltmp495:
	jmp	.LBB12_642
.LBB12_608:
.Ltmp25:
	jmp	.LBB12_696
.LBB12_609:
.Ltmp22:
	movq	%rax, %r14
.Lcfi290:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_725
.LBB12_610:
.Ltmp465:
	movq	%rax, %r14
	movq	1272(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_612
# BB#611:
.Lcfi291:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB12_612:
.Ltmp466:
.Lcfi292:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp467:
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB12_721
.LBB12_613:
.Ltmp192:
	movq	%rax, %r14
.LBB12_614:                             # %.body105.i
.Ltmp193:
.Lcfi293:
	.cfi_escape 0x2e, 0x00
	leaq	808(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp194:
	jmp	.LBB12_626
.LBB12_615:
.Ltmp202:
	jmp	.LBB12_619
.LBB12_616:
.Ltmp177:
	movq	%rax, %r14
.LBB12_617:                             # %.body97.i
.Ltmp178:
.Lcfi294:
	.cfi_escape 0x2e, 0x00
	leaq	944(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp179:
	jmp	.LBB12_626
.LBB12_618:
.Ltmp209:
.LBB12_619:
	movq	%rax, %r14
	jmp	.LBB12_626
.LBB12_620:
.Ltmp147:
	movq	%rax, %r14
.LBB12_621:                             # %.body81.i
.Ltmp148:
.Lcfi295:
	.cfi_escape 0x2e, 0x00
	leaq	1352(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp149:
	jmp	.LBB12_626
.LBB12_622:
.Ltmp162:
	movq	%rax, %r14
.LBB12_623:                             # %.body89.i
.Ltmp163:
.Lcfi296:
	.cfi_escape 0x2e, 0x00
	leaq	1080(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp164:
	jmp	.LBB12_626
.LBB12_624:
.Ltmp132:
	movq	%rax, %r14
.LBB12_625:                             # %.body75.i
.Ltmp133:
.Lcfi297:
	.cfi_escape 0x2e, 0x00
	leaq	2400(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp134:
.LBB12_626:
	movq	$_ZTV13CObjectVectorI5CPropE+16, 200(%rsp)
.Ltmp210:
.Lcfi298:
	.cfi_escape 0x2e, 0x00
	leaq	200(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp211:
# BB#627:                               # %_ZN13CObjectVectorI5CPropED2Ev.exit.i133.i
.Ltmp216:
.Lcfi299:
	.cfi_escape 0x2e, 0x00
	leaq	200(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp217:
	jmp	.LBB12_650
.LBB12_628:
.Ltmp212:
	movq	%rax, %rbx
.Ltmp213:
.Lcfi300:
	.cfi_escape 0x2e, 0x00
	leaq	200(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp214:
	jmp	.LBB12_631
.LBB12_629:
.Ltmp215:
.Lcfi301:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_630:
.Ltmp240:
	movq	%rax, %rbx
.LBB12_631:                             # %.body.i
.Lcfi302:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB12_632:
.Ltmp304:
	jmp	.LBB12_634
.LBB12_633:
.Ltmp261:
.LBB12_634:
	movq	%rax, %r14
	jmp	.LBB12_681
.LBB12_635:
.Ltmp449:
	jmp	.LBB12_644
.LBB12_636:
.Ltmp444:
	movq	%rax, %r14
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB12_720
.LBB12_637:
.Ltmp118:
	movq	%rax, %r14
	jmp	.LBB12_721
.LBB12_638:
.Ltmp115:
	jmp	.LBB12_644
.LBB12_639:                             # %.loopexit.split-lp1016
.Ltmp14:
	jmp	.LBB12_696
.LBB12_640:
.Ltmp5:
	jmp	.LBB12_696
.LBB12_641:
.Ltmp30:
.LBB12_642:
	movq	%rax, %r14
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB12_724
.LBB12_643:
.Ltmp70:
.LBB12_644:
	movq	%rax, %r14
.LBB12_645:
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB12_721
.LBB12_646:
.Ltmp62:
	movq	%rax, %r14
.Lcfi303:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_660
.LBB12_647:
.Ltmp419:
	jmp	.LBB12_698
.LBB12_648:
.Ltmp505:
	movq	%rax, %r14
	jmp	.LBB12_726
.LBB12_649:
.Ltmp255:
	movq	%rax, %r14
.LBB12_650:                             # %.body841
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB12_720
.LBB12_651:
.Ltmp331:
	movq	%rax, %r14
.Lcfi304:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_718
.LBB12_652:
.Ltmp258:
.LBB12_653:
	movq	%rax, %r14
	jmp	.LBB12_685
.LBB12_654:
.Ltmp323:
	jmp	.LBB12_713
.LBB12_655:                             # %.body885
.Ltmp320:
	movq	%rax, %r14
	jmp	.LBB12_690
.LBB12_656:
.Ltmp403:
	movq	%rax, %r14
.Lcfi305:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_701
.LBB12_657:
.Ltmp19:
	jmp	.LBB12_696
.LBB12_658:
.Ltmp279:
	jmp	.LBB12_679
.LBB12_659:
.Ltmp65:
	movq	%rax, %r14
.LBB12_660:                             # %.body807
.Ltmp66:
.Lcfi306:
	.cfi_escape 0x2e, 0x00
	leaq	256(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp67:
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB12_721
.LBB12_661:
.Ltmp337:
	jmp	.LBB12_713
.LBB12_662:
.Ltmp334:
	jmp	.LBB12_713
.LBB12_663:
.Ltmp393:
	jmp	.LBB12_666
.LBB12_664:
.Ltmp390:
	jmp	.LBB12_670
.LBB12_665:
.Ltmp424:
.LBB12_666:
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%rax, %r14
	jmp	.LBB12_702
.LBB12_667:
.Ltmp429:
	jmp	.LBB12_670
.LBB12_668:
.Ltmp387:
	movq	%rax, %r14
.Lcfi307:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZdlPv
	testq	%r12, %r12
	jne	.LBB12_709
	jmp	.LBB12_710
.LBB12_669:
.Ltmp384:
.LBB12_670:
	movq	%rax, %r14
	testq	%r12, %r12
	jne	.LBB12_709
	jmp	.LBB12_710
.LBB12_671:
.Ltmp373:
	jmp	.LBB12_673
.LBB12_672:
.Ltmp370:
.LBB12_673:                             # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rax, %r14
	jmp	.LBB12_710
.LBB12_674:
.Ltmp11:
	jmp	.LBB12_696
.LBB12_675:
.Ltmp350:
	movq	%rax, %r14
	movq	600(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_716
# BB#676:
.Lcfi308:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB12_716
.LBB12_677:                             # %.loopexit998
.Ltmp406:
	movq	%r13, 24(%rsp)          # 8-byte Spill
	jmp	.LBB12_698
.LBB12_678:
.Ltmp264:
.LBB12_679:
	movq	%rax, %r14
.LBB12_680:
.Ltmp300:
.Lcfi309:
	.cfi_escape 0x2e, 0x00
	leaq	712(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp301:
.LBB12_681:
	movq	240(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_683
# BB#682:
	movq	(%rdi), %rax
.Ltmp305:
.Lcfi310:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp306:
.LBB12_683:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit888
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_685
# BB#684:
	movq	(%rdi), %rax
.Ltmp310:
.Lcfi311:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp311:
.LBB12_685:
	movq	1224(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB12_689
# BB#686:
	cmpb	$0, 88(%rbp)
	je	.LBB12_688
# BB#687:
.Lcfi312:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	pthread_mutex_destroy
	leaq	40(%rbp), %rdi
.Lcfi313:
	.cfi_escape 0x2e, 0x00
	callq	pthread_cond_destroy
.LBB12_688:
.Lcfi314:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB12_689:
	movq	$0, 1224(%rsp)
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 1200(%rsp)
	movq	$0, 1208(%rsp)
.Ltmp315:
.Lcfi315:
	.cfi_escape 0x2e, 0x00
	leaq	1096(%rsp), %rdi
	callq	Event_Close
.Ltmp316:
.LBB12_690:                             # %_ZN13CStreamBinderD2Ev.exit893
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 1072(%rsp)
	movq	$0, 1080(%rsp)
	jmp	.LBB12_718
.LBB12_691:
.Ltmp317:
	movq	%rax, %rbx
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 1072(%rsp)
	movq	$0, 1080(%rsp)
	jmp	.LBB12_731
.LBB12_692:                             # %.loopexit1000
.Ltmp361:
.LBB12_693:
	movq	%rax, %r14
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB12_711
.LBB12_694:
.Ltmp353:
	jmp	.LBB12_715
.LBB12_695:                             # %.loopexit1015
.Ltmp8:
.LBB12_696:
	movq	%rax, %r14
	jmp	.LBB12_725
.LBB12_697:
.Ltmp409:
.LBB12_698:
	movq	%rax, %r14
	jmp	.LBB12_701
.LBB12_699:
.Ltmp416:
	movq	%rax, %r14
	movq	504(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_701
# BB#700:
.Lcfi316:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB12_701:
.Ltmp420:
.Lcfi317:
	.cfi_escape 0x2e, 0x00
	leaq	800(%rsp), %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp421:
.LBB12_702:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp425:
.Lcfi318:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp426:
	jmp	.LBB12_708
.LBB12_703:
.Ltmp381:
	movq	%rax, %r14
	movq	536(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_708
# BB#704:
.Lcfi319:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	testq	%r12, %r12
	jne	.LBB12_709
	jmp	.LBB12_710
.LBB12_705:
.Ltmp326:
	jmp	.LBB12_713
.LBB12_707:
.Ltmp376:
	movq	%rax, %r14
.LBB12_708:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	testq	%r12, %r12
	je	.LBB12_710
.LBB12_709:
.Lcfi320:
	.cfi_escape 0x2e, 0x00
	movq	184(%rsp), %rdi         # 8-byte Reload
	callq	_ZdaPv
.LBB12_710:                             # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp430:
.Lcfi321:
	.cfi_escape 0x2e, 0x00
	leaq	456(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp431:
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB12_711:
.Ltmp435:
.Lcfi322:
	.cfi_escape 0x2e, 0x00
	leaq	552(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp436:
	jmp	.LBB12_719
.LBB12_712:
.Ltmp340:
.LBB12_713:
	movq	%rax, %r14
	jmp	.LBB12_718
.LBB12_714:
.Ltmp343:
.LBB12_715:
	movq	%rax, %r14
.LBB12_716:
	movq	304(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_718
# BB#717:
.Lcfi323:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB12_718:                             # %_ZN8NArchive3N7z9CFileItemD2Ev.exit904
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB12_719:
.Ltmp440:
.Lcfi324:
	.cfi_escape 0x2e, 0x00
	leaq	1344(%rsp), %rdi
	callq	_ZN8NArchive3N7z8CEncoderD1Ev
.Ltmp441:
.LBB12_720:
.Ltmp445:
.Lcfi325:
	.cfi_escape 0x2e, 0x00
	leaq	624(%rsp), %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp446:
.LBB12_721:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE+16, 112(%rsp)
.Ltmp474:
.Lcfi326:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp475:
# BB#722:
.Ltmp480:
.Lcfi327:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp481:
.LBB12_723:
.Ltmp491:
.Lcfi328:
	.cfi_escape 0x2e, 0x00
	leaq	1856(%rsp), %rdi
	callq	_ZN8NArchive3N7z14CThreadDecoderD2Ev
.Ltmp492:
.LBB12_724:
	movq	(%rbp), %rax
.Ltmp496:
.Lcfi329:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp497:
.LBB12_725:
.Ltmp501:
.Lcfi330:
	.cfi_escape 0x2e, 0x00
	leaq	360(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp502:
.LBB12_726:
.Ltmp506:
.Lcfi331:
	.cfi_escape 0x2e, 0x00
	leaq	392(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp507:
# BB#727:
.Lcfi332:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_728:
.Ltmp476:
	movq	%rax, %rbx
.Ltmp477:
.Lcfi333:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp478:
	jmp	.LBB12_731
.LBB12_729:
.Ltmp479:
.Lcfi334:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_730:
.Ltmp508:
	movq	%rax, %rbx
.LBB12_731:                             # %.body
.Lcfi335:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN8NArchive3N7z6UpdateEP9IInStreamPKNS0_18CArchiveDatabaseExERK13CObjectVectorINS0_11CUpdateItemEERNS0_11COutArchiveERNS0_16CArchiveDatabaseEP20ISequentialOutStreamP22IArchiveUpdateCallbackRKNS0_14CUpdateOptionsEP22ICryptoGetTextPassword, .Lfunc_end12-_ZN8NArchive3N7z6UpdateEP9IInStreamPKNS0_18CArchiveDatabaseExERK13CObjectVectorINS0_11CUpdateItemEERNS0_11COutArchiveERNS0_16CArchiveDatabaseEP20ISequentialOutStreamP22IArchiveUpdateCallbackRKNS0_14CUpdateOptionsEP22ICryptoGetTextPassword
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\232\217\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\221\017"              # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp18-.Ltmp15         #   Call between .Ltmp15 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin1   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin1   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin1   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp29-.Ltmp26         #   Call between .Ltmp26 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin1   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp62-.Lfunc_begin1   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp70-.Lfunc_begin1   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin1   # >> Call Site 15 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin1   # >> Call Site 16 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp62-.Lfunc_begin1   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 17 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin1   # >> Call Site 18 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp70-.Lfunc_begin1   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin1   # >> Call Site 19 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin1   # >> Call Site 20 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp62-.Lfunc_begin1   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin1   # >> Call Site 21 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin1   # >> Call Site 22 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp70-.Lfunc_begin1   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin1   # >> Call Site 23 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin1   # >> Call Site 24 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin1   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin1   # >> Call Site 25 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin1   # >> Call Site 26 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin1   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin1   # >> Call Site 27 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin1   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin1   # >> Call Site 28 <<
	.long	.Ltmp72-.Ltmp84         #   Call between .Ltmp84 and .Ltmp72
	.long	.Ltmp88-.Lfunc_begin1   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin1   # >> Call Site 29 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin1   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin1   # >> Call Site 30 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin1   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin1   # >> Call Site 31 <<
	.long	.Ltmp87-.Ltmp82         #   Call between .Ltmp82 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin1   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin1   # >> Call Site 32 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp115-.Lfunc_begin1  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin1   # >> Call Site 33 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin1   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin1   # >> Call Site 34 <<
	.long	.Ltmp114-.Ltmp97        #   Call between .Ltmp97 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin1  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp116-.Lfunc_begin1  # >> Call Site 35 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin1  #     jumps to .Ltmp118
	.byte	0                       #   On action: cleanup
	.long	.Ltmp249-.Lfunc_begin1  # >> Call Site 36 <<
	.long	.Ltmp120-.Ltmp249       #   Call between .Ltmp249 and .Ltmp120
	.long	.Ltmp255-.Lfunc_begin1  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin1  # >> Call Site 37 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp202-.Lfunc_begin1  #     jumps to .Ltmp202
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin1  # >> Call Site 38 <<
	.long	.Ltmp126-.Ltmp123       #   Call between .Ltmp123 and .Ltmp126
	.long	.Ltmp132-.Lfunc_begin1  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin1  # >> Call Site 39 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin1  #     jumps to .Ltmp129
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin1  # >> Call Site 40 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin1  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp135-.Lfunc_begin1  # >> Call Site 41 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin1  #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin1  # >> Call Site 42 <<
	.long	.Ltmp141-.Ltmp138       #   Call between .Ltmp138 and .Ltmp141
	.long	.Ltmp147-.Lfunc_begin1  #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp142-.Lfunc_begin1  # >> Call Site 43 <<
	.long	.Ltmp143-.Ltmp142       #   Call between .Ltmp142 and .Ltmp143
	.long	.Ltmp144-.Lfunc_begin1  #     jumps to .Ltmp144
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin1  # >> Call Site 44 <<
	.long	.Ltmp146-.Ltmp145       #   Call between .Ltmp145 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin1  #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin1  # >> Call Site 45 <<
	.long	.Ltmp151-.Ltmp150       #   Call between .Ltmp150 and .Ltmp151
	.long	.Ltmp152-.Lfunc_begin1  #     jumps to .Ltmp152
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin1  # >> Call Site 46 <<
	.long	.Ltmp156-.Ltmp153       #   Call between .Ltmp153 and .Ltmp156
	.long	.Ltmp162-.Lfunc_begin1  #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin1  # >> Call Site 47 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin1  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin1  # >> Call Site 48 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin1  #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp165-.Lfunc_begin1  # >> Call Site 49 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin1  #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin1  # >> Call Site 50 <<
	.long	.Ltmp171-.Ltmp168       #   Call between .Ltmp168 and .Ltmp171
	.long	.Ltmp177-.Lfunc_begin1  #     jumps to .Ltmp177
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin1  # >> Call Site 51 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin1  #     jumps to .Ltmp174
	.byte	0                       #   On action: cleanup
	.long	.Ltmp175-.Lfunc_begin1  # >> Call Site 52 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin1  #     jumps to .Ltmp177
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin1  # >> Call Site 53 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin1  #     jumps to .Ltmp182
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin1  # >> Call Site 54 <<
	.long	.Ltmp186-.Ltmp183       #   Call between .Ltmp183 and .Ltmp186
	.long	.Ltmp192-.Lfunc_begin1  #     jumps to .Ltmp192
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin1  # >> Call Site 55 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin1  #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp190-.Lfunc_begin1  # >> Call Site 56 <<
	.long	.Ltmp191-.Ltmp190       #   Call between .Ltmp190 and .Ltmp191
	.long	.Ltmp192-.Lfunc_begin1  #     jumps to .Ltmp192
	.byte	0                       #   On action: cleanup
	.long	.Ltmp195-.Lfunc_begin1  # >> Call Site 57 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp197-.Lfunc_begin1  #     jumps to .Ltmp197
	.byte	0                       #   On action: cleanup
	.long	.Ltmp198-.Lfunc_begin1  # >> Call Site 58 <<
	.long	.Ltmp201-.Ltmp198       #   Call between .Ltmp198 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin1  #     jumps to .Ltmp202
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin1  # >> Call Site 59 <<
	.long	.Ltmp208-.Ltmp203       #   Call between .Ltmp203 and .Ltmp208
	.long	.Ltmp209-.Lfunc_begin1  #     jumps to .Ltmp209
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin1  # >> Call Site 60 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp220-.Lfunc_begin1  #     jumps to .Ltmp220
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin1  # >> Call Site 61 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp255-.Lfunc_begin1  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp226-.Lfunc_begin1  # >> Call Site 62 <<
	.long	.Ltmp227-.Ltmp226       #   Call between .Ltmp226 and .Ltmp227
	.long	.Ltmp228-.Lfunc_begin1  #     jumps to .Ltmp228
	.byte	0                       #   On action: cleanup
	.long	.Ltmp229-.Lfunc_begin1  # >> Call Site 63 <<
	.long	.Ltmp230-.Ltmp229       #   Call between .Ltmp229 and .Ltmp230
	.long	.Ltmp231-.Lfunc_begin1  #     jumps to .Ltmp231
	.byte	0                       #   On action: cleanup
	.long	.Ltmp241-.Lfunc_begin1  # >> Call Site 64 <<
	.long	.Ltmp242-.Ltmp241       #   Call between .Ltmp241 and .Ltmp242
	.long	.Ltmp243-.Lfunc_begin1  #     jumps to .Ltmp243
	.byte	0                       #   On action: cleanup
	.long	.Ltmp247-.Lfunc_begin1  # >> Call Site 65 <<
	.long	.Ltmp254-.Ltmp247       #   Call between .Ltmp247 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin1  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp256-.Lfunc_begin1  # >> Call Site 66 <<
	.long	.Ltmp257-.Ltmp256       #   Call between .Ltmp256 and .Ltmp257
	.long	.Ltmp258-.Lfunc_begin1  #     jumps to .Ltmp258
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin1  # >> Call Site 67 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin1  #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp324-.Lfunc_begin1  # >> Call Site 68 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp326-.Lfunc_begin1  #     jumps to .Ltmp326
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin1  # >> Call Site 69 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp334-.Lfunc_begin1  #     jumps to .Ltmp334
	.byte	0                       #   On action: cleanup
	.long	.Ltmp329-.Lfunc_begin1  # >> Call Site 70 <<
	.long	.Ltmp330-.Ltmp329       #   Call between .Ltmp329 and .Ltmp330
	.long	.Ltmp331-.Lfunc_begin1  #     jumps to .Ltmp331
	.byte	0                       #   On action: cleanup
	.long	.Ltmp332-.Lfunc_begin1  # >> Call Site 71 <<
	.long	.Ltmp333-.Ltmp332       #   Call between .Ltmp332 and .Ltmp333
	.long	.Ltmp334-.Lfunc_begin1  #     jumps to .Ltmp334
	.byte	0                       #   On action: cleanup
	.long	.Ltmp259-.Lfunc_begin1  # >> Call Site 72 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	.Ltmp261-.Lfunc_begin1  #     jumps to .Ltmp261
	.byte	0                       #   On action: cleanup
	.long	.Ltmp262-.Lfunc_begin1  # >> Call Site 73 <<
	.long	.Ltmp263-.Ltmp262       #   Call between .Ltmp262 and .Ltmp263
	.long	.Ltmp264-.Lfunc_begin1  #     jumps to .Ltmp264
	.byte	0                       #   On action: cleanup
	.long	.Ltmp265-.Lfunc_begin1  # >> Call Site 74 <<
	.long	.Ltmp278-.Ltmp265       #   Call between .Ltmp265 and .Ltmp278
	.long	.Ltmp279-.Lfunc_begin1  #     jumps to .Ltmp279
	.byte	0                       #   On action: cleanup
	.long	.Ltmp280-.Lfunc_begin1  # >> Call Site 75 <<
	.long	.Ltmp288-.Ltmp280       #   Call between .Ltmp280 and .Ltmp288
	.long	.Ltmp294-.Lfunc_begin1  #     jumps to .Ltmp294
	.byte	0                       #   On action: cleanup
	.long	.Ltmp289-.Lfunc_begin1  # >> Call Site 76 <<
	.long	.Ltmp290-.Ltmp289       #   Call between .Ltmp289 and .Ltmp290
	.long	.Ltmp291-.Lfunc_begin1  #     jumps to .Ltmp291
	.byte	0                       #   On action: cleanup
	.long	.Ltmp292-.Lfunc_begin1  # >> Call Site 77 <<
	.long	.Ltmp293-.Ltmp292       #   Call between .Ltmp292 and .Ltmp293
	.long	.Ltmp294-.Lfunc_begin1  #     jumps to .Ltmp294
	.byte	0                       #   On action: cleanup
	.long	.Ltmp297-.Lfunc_begin1  # >> Call Site 78 <<
	.long	.Ltmp298-.Ltmp297       #   Call between .Ltmp297 and .Ltmp298
	.long	.Ltmp299-.Lfunc_begin1  #     jumps to .Ltmp299
	.byte	0                       #   On action: cleanup
	.long	.Ltmp302-.Lfunc_begin1  # >> Call Site 79 <<
	.long	.Ltmp303-.Ltmp302       #   Call between .Ltmp302 and .Ltmp303
	.long	.Ltmp304-.Lfunc_begin1  #     jumps to .Ltmp304
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin1  # >> Call Site 80 <<
	.long	.Ltmp308-.Ltmp307       #   Call between .Ltmp307 and .Ltmp308
	.long	.Ltmp309-.Lfunc_begin1  #     jumps to .Ltmp309
	.byte	0                       #   On action: cleanup
	.long	.Ltmp312-.Lfunc_begin1  # >> Call Site 81 <<
	.long	.Ltmp313-.Ltmp312       #   Call between .Ltmp312 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin1  #     jumps to .Ltmp314
	.byte	0                       #   On action: cleanup
	.long	.Ltmp318-.Lfunc_begin1  # >> Call Site 82 <<
	.long	.Ltmp319-.Ltmp318       #   Call between .Ltmp318 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin1  #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp335-.Lfunc_begin1  # >> Call Site 83 <<
	.long	.Ltmp336-.Ltmp335       #   Call between .Ltmp335 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin1  #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp338-.Lfunc_begin1  # >> Call Site 84 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin1  #     jumps to .Ltmp340
	.byte	0                       #   On action: cleanup
	.long	.Ltmp341-.Lfunc_begin1  # >> Call Site 85 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp343-.Lfunc_begin1  #     jumps to .Ltmp343
	.byte	0                       #   On action: cleanup
	.long	.Ltmp344-.Lfunc_begin1  # >> Call Site 86 <<
	.long	.Ltmp345-.Ltmp344       #   Call between .Ltmp344 and .Ltmp345
	.long	.Ltmp353-.Lfunc_begin1  #     jumps to .Ltmp353
	.byte	0                       #   On action: cleanup
	.long	.Ltmp346-.Lfunc_begin1  # >> Call Site 87 <<
	.long	.Ltmp349-.Ltmp346       #   Call between .Ltmp346 and .Ltmp349
	.long	.Ltmp350-.Lfunc_begin1  #     jumps to .Ltmp350
	.byte	0                       #   On action: cleanup
	.long	.Ltmp351-.Lfunc_begin1  # >> Call Site 88 <<
	.long	.Ltmp352-.Ltmp351       #   Call between .Ltmp351 and .Ltmp352
	.long	.Ltmp353-.Lfunc_begin1  #     jumps to .Ltmp353
	.byte	0                       #   On action: cleanup
	.long	.Ltmp354-.Lfunc_begin1  # >> Call Site 89 <<
	.long	.Ltmp355-.Ltmp354       #   Call between .Ltmp354 and .Ltmp355
	.long	.Ltmp356-.Lfunc_begin1  #     jumps to .Ltmp356
	.byte	0                       #   On action: cleanup
	.long	.Ltmp357-.Lfunc_begin1  # >> Call Site 90 <<
	.long	.Ltmp360-.Ltmp357       #   Call between .Ltmp357 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin1  #     jumps to .Ltmp361
	.byte	0                       #   On action: cleanup
	.long	.Ltmp362-.Lfunc_begin1  # >> Call Site 91 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin1  #     jumps to .Ltmp364
	.byte	0                       #   On action: cleanup
	.long	.Ltmp365-.Lfunc_begin1  # >> Call Site 92 <<
	.long	.Ltmp366-.Ltmp365       #   Call between .Ltmp365 and .Ltmp366
	.long	.Ltmp367-.Lfunc_begin1  #     jumps to .Ltmp367
	.byte	0                       #   On action: cleanup
	.long	.Ltmp368-.Lfunc_begin1  # >> Call Site 93 <<
	.long	.Ltmp369-.Ltmp368       #   Call between .Ltmp368 and .Ltmp369
	.long	.Ltmp370-.Lfunc_begin1  #     jumps to .Ltmp370
	.byte	0                       #   On action: cleanup
	.long	.Ltmp371-.Lfunc_begin1  # >> Call Site 94 <<
	.long	.Ltmp372-.Ltmp371       #   Call between .Ltmp371 and .Ltmp372
	.long	.Ltmp373-.Lfunc_begin1  #     jumps to .Ltmp373
	.byte	0                       #   On action: cleanup
	.long	.Ltmp374-.Lfunc_begin1  # >> Call Site 95 <<
	.long	.Ltmp375-.Ltmp374       #   Call between .Ltmp374 and .Ltmp375
	.long	.Ltmp376-.Lfunc_begin1  #     jumps to .Ltmp376
	.byte	0                       #   On action: cleanup
	.long	.Ltmp377-.Lfunc_begin1  # >> Call Site 96 <<
	.long	.Ltmp380-.Ltmp377       #   Call between .Ltmp377 and .Ltmp380
	.long	.Ltmp381-.Lfunc_begin1  #     jumps to .Ltmp381
	.byte	0                       #   On action: cleanup
	.long	.Ltmp382-.Lfunc_begin1  # >> Call Site 97 <<
	.long	.Ltmp383-.Ltmp382       #   Call between .Ltmp382 and .Ltmp383
	.long	.Ltmp384-.Lfunc_begin1  #     jumps to .Ltmp384
	.byte	0                       #   On action: cleanup
	.long	.Ltmp385-.Lfunc_begin1  # >> Call Site 98 <<
	.long	.Ltmp386-.Ltmp385       #   Call between .Ltmp385 and .Ltmp386
	.long	.Ltmp387-.Lfunc_begin1  #     jumps to .Ltmp387
	.byte	0                       #   On action: cleanup
	.long	.Ltmp388-.Lfunc_begin1  # >> Call Site 99 <<
	.long	.Ltmp389-.Ltmp388       #   Call between .Ltmp388 and .Ltmp389
	.long	.Ltmp390-.Lfunc_begin1  #     jumps to .Ltmp390
	.byte	0                       #   On action: cleanup
	.long	.Ltmp391-.Lfunc_begin1  # >> Call Site 100 <<
	.long	.Ltmp392-.Ltmp391       #   Call between .Ltmp391 and .Ltmp392
	.long	.Ltmp393-.Lfunc_begin1  #     jumps to .Ltmp393
	.byte	0                       #   On action: cleanup
	.long	.Ltmp394-.Lfunc_begin1  # >> Call Site 101 <<
	.long	.Ltmp400-.Ltmp394       #   Call between .Ltmp394 and .Ltmp400
	.long	.Ltmp406-.Lfunc_begin1  #     jumps to .Ltmp406
	.byte	0                       #   On action: cleanup
	.long	.Ltmp401-.Lfunc_begin1  # >> Call Site 102 <<
	.long	.Ltmp402-.Ltmp401       #   Call between .Ltmp401 and .Ltmp402
	.long	.Ltmp403-.Lfunc_begin1  #     jumps to .Ltmp403
	.byte	0                       #   On action: cleanup
	.long	.Ltmp404-.Lfunc_begin1  # >> Call Site 103 <<
	.long	.Ltmp405-.Ltmp404       #   Call between .Ltmp404 and .Ltmp405
	.long	.Ltmp406-.Lfunc_begin1  #     jumps to .Ltmp406
	.byte	0                       #   On action: cleanup
	.long	.Ltmp407-.Lfunc_begin1  # >> Call Site 104 <<
	.long	.Ltmp408-.Ltmp407       #   Call between .Ltmp407 and .Ltmp408
	.long	.Ltmp409-.Lfunc_begin1  #     jumps to .Ltmp409
	.byte	0                       #   On action: cleanup
	.long	.Ltmp410-.Lfunc_begin1  # >> Call Site 105 <<
	.long	.Ltmp415-.Ltmp410       #   Call between .Ltmp410 and .Ltmp415
	.long	.Ltmp416-.Lfunc_begin1  #     jumps to .Ltmp416
	.byte	0                       #   On action: cleanup
	.long	.Ltmp417-.Lfunc_begin1  # >> Call Site 106 <<
	.long	.Ltmp418-.Ltmp417       #   Call between .Ltmp417 and .Ltmp418
	.long	.Ltmp419-.Lfunc_begin1  #     jumps to .Ltmp419
	.byte	0                       #   On action: cleanup
	.long	.Ltmp422-.Lfunc_begin1  # >> Call Site 107 <<
	.long	.Ltmp423-.Ltmp422       #   Call between .Ltmp422 and .Ltmp423
	.long	.Ltmp424-.Lfunc_begin1  #     jumps to .Ltmp424
	.byte	0                       #   On action: cleanup
	.long	.Ltmp427-.Lfunc_begin1  # >> Call Site 108 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp429-.Lfunc_begin1  #     jumps to .Ltmp429
	.byte	0                       #   On action: cleanup
	.long	.Ltmp432-.Lfunc_begin1  # >> Call Site 109 <<
	.long	.Ltmp433-.Ltmp432       #   Call between .Ltmp432 and .Ltmp433
	.long	.Ltmp434-.Lfunc_begin1  #     jumps to .Ltmp434
	.byte	0                       #   On action: cleanup
	.long	.Ltmp437-.Lfunc_begin1  # >> Call Site 110 <<
	.long	.Ltmp438-.Ltmp437       #   Call between .Ltmp437 and .Ltmp438
	.long	.Ltmp439-.Lfunc_begin1  #     jumps to .Ltmp439
	.byte	0                       #   On action: cleanup
	.long	.Ltmp442-.Lfunc_begin1  # >> Call Site 111 <<
	.long	.Ltmp443-.Ltmp442       #   Call between .Ltmp442 and .Ltmp443
	.long	.Ltmp444-.Lfunc_begin1  #     jumps to .Ltmp444
	.byte	0                       #   On action: cleanup
	.long	.Ltmp447-.Lfunc_begin1  # >> Call Site 112 <<
	.long	.Ltmp448-.Ltmp447       #   Call between .Ltmp447 and .Ltmp448
	.long	.Ltmp449-.Lfunc_begin1  #     jumps to .Ltmp449
	.byte	0                       #   On action: cleanup
	.long	.Ltmp450-.Lfunc_begin1  # >> Call Site 113 <<
	.long	.Ltmp451-.Ltmp450       #   Call between .Ltmp450 and .Ltmp451
	.long	.Ltmp452-.Lfunc_begin1  #     jumps to .Ltmp452
	.byte	0                       #   On action: cleanup
	.long	.Ltmp453-.Lfunc_begin1  # >> Call Site 114 <<
	.long	.Ltmp454-.Ltmp453       #   Call between .Ltmp453 and .Ltmp454
	.long	.Ltmp455-.Lfunc_begin1  #     jumps to .Ltmp455
	.byte	0                       #   On action: cleanup
	.long	.Ltmp456-.Lfunc_begin1  # >> Call Site 115 <<
	.long	.Ltmp457-.Ltmp456       #   Call between .Ltmp456 and .Ltmp457
	.long	.Ltmp458-.Lfunc_begin1  #     jumps to .Ltmp458
	.byte	0                       #   On action: cleanup
	.long	.Ltmp459-.Lfunc_begin1  # >> Call Site 116 <<
	.long	.Ltmp464-.Ltmp459       #   Call between .Ltmp459 and .Ltmp464
	.long	.Ltmp465-.Lfunc_begin1  #     jumps to .Ltmp465
	.byte	0                       #   On action: cleanup
	.long	.Ltmp468-.Lfunc_begin1  # >> Call Site 117 <<
	.long	.Ltmp469-.Ltmp468       #   Call between .Ltmp468 and .Ltmp469
	.long	.Ltmp470-.Lfunc_begin1  #     jumps to .Ltmp470
	.byte	0                       #   On action: cleanup
	.long	.Ltmp471-.Lfunc_begin1  # >> Call Site 118 <<
	.long	.Ltmp472-.Ltmp471       #   Call between .Ltmp471 and .Ltmp472
	.long	.Ltmp473-.Lfunc_begin1  #     jumps to .Ltmp473
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin1  # >> Call Site 119 <<
	.long	.Ltmp104-.Ltmp101       #   Call between .Ltmp101 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin1  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin1  # >> Call Site 120 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin1  #     jumps to .Ltmp110
	.byte	0                       #   On action: cleanup
	.long	.Ltmp482-.Lfunc_begin1  # >> Call Site 121 <<
	.long	.Ltmp483-.Ltmp482       #   Call between .Ltmp482 and .Ltmp483
	.long	.Ltmp484-.Lfunc_begin1  #     jumps to .Ltmp484
	.byte	0                       #   On action: cleanup
	.long	.Ltmp488-.Lfunc_begin1  # >> Call Site 122 <<
	.long	.Ltmp489-.Ltmp488       #   Call between .Ltmp488 and .Ltmp489
	.long	.Ltmp490-.Lfunc_begin1  #     jumps to .Ltmp490
	.byte	0                       #   On action: cleanup
	.long	.Ltmp493-.Lfunc_begin1  # >> Call Site 123 <<
	.long	.Ltmp494-.Ltmp493       #   Call between .Ltmp493 and .Ltmp494
	.long	.Ltmp495-.Lfunc_begin1  #     jumps to .Ltmp495
	.byte	0                       #   On action: cleanup
	.long	.Ltmp498-.Lfunc_begin1  # >> Call Site 124 <<
	.long	.Ltmp499-.Ltmp498       #   Call between .Ltmp498 and .Ltmp499
	.long	.Ltmp500-.Lfunc_begin1  #     jumps to .Ltmp500
	.byte	0                       #   On action: cleanup
	.long	.Ltmp503-.Lfunc_begin1  # >> Call Site 125 <<
	.long	.Ltmp504-.Ltmp503       #   Call between .Ltmp503 and .Ltmp504
	.long	.Ltmp505-.Lfunc_begin1  #     jumps to .Ltmp505
	.byte	0                       #   On action: cleanup
	.long	.Ltmp504-.Lfunc_begin1  # >> Call Site 126 <<
	.long	.Ltmp396-.Ltmp504       #   Call between .Ltmp504 and .Ltmp396
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp396-.Lfunc_begin1  # >> Call Site 127 <<
	.long	.Ltmp397-.Ltmp396       #   Call between .Ltmp396 and .Ltmp397
	.long	.Ltmp398-.Lfunc_begin1  #     jumps to .Ltmp398
	.byte	0                       #   On action: cleanup
	.long	.Ltmp397-.Lfunc_begin1  # >> Call Site 128 <<
	.long	.Ltmp284-.Ltmp397       #   Call between .Ltmp397 and .Ltmp284
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp284-.Lfunc_begin1  # >> Call Site 129 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp286-.Lfunc_begin1  #     jumps to .Ltmp286
	.byte	0                       #   On action: cleanup
	.long	.Ltmp106-.Lfunc_begin1  # >> Call Site 130 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp508-.Lfunc_begin1  #     jumps to .Ltmp508
	.byte	1                       #   On action: 1
	.long	.Ltmp79-.Lfunc_begin1   # >> Call Site 131 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin1   #     jumps to .Ltmp81
	.byte	1                       #   On action: 1
	.long	.Ltmp244-.Lfunc_begin1  # >> Call Site 132 <<
	.long	.Ltmp245-.Ltmp244       #   Call between .Ltmp244 and .Ltmp245
	.long	.Ltmp246-.Lfunc_begin1  #     jumps to .Ltmp246
	.byte	1                       #   On action: 1
	.long	.Ltmp232-.Lfunc_begin1  # >> Call Site 133 <<
	.long	.Ltmp233-.Ltmp232       #   Call between .Ltmp232 and .Ltmp233
	.long	.Ltmp234-.Lfunc_begin1  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp238-.Lfunc_begin1  # >> Call Site 134 <<
	.long	.Ltmp239-.Ltmp238       #   Call between .Ltmp238 and .Ltmp239
	.long	.Ltmp240-.Lfunc_begin1  #     jumps to .Ltmp240
	.byte	1                       #   On action: 1
	.long	.Ltmp235-.Lfunc_begin1  # >> Call Site 135 <<
	.long	.Ltmp236-.Ltmp235       #   Call between .Ltmp235 and .Ltmp236
	.long	.Ltmp237-.Lfunc_begin1  #     jumps to .Ltmp237
	.byte	1                       #   On action: 1
	.long	.Ltmp221-.Lfunc_begin1  # >> Call Site 136 <<
	.long	.Ltmp222-.Ltmp221       #   Call between .Ltmp221 and .Ltmp222
	.long	.Ltmp223-.Lfunc_begin1  #     jumps to .Ltmp223
	.byte	1                       #   On action: 1
	.long	.Ltmp485-.Lfunc_begin1  # >> Call Site 137 <<
	.long	.Ltmp486-.Ltmp485       #   Call between .Ltmp485 and .Ltmp486
	.long	.Ltmp487-.Lfunc_begin1  #     jumps to .Ltmp487
	.byte	1                       #   On action: 1
	.long	.Ltmp295-.Lfunc_begin1  # >> Call Site 138 <<
	.long	.Ltmp467-.Ltmp295       #   Call between .Ltmp295 and .Ltmp467
	.long	.Ltmp508-.Lfunc_begin1  #     jumps to .Ltmp508
	.byte	1                       #   On action: 1
	.long	.Ltmp193-.Lfunc_begin1  # >> Call Site 139 <<
	.long	.Ltmp134-.Ltmp193       #   Call between .Ltmp193 and .Ltmp134
	.long	.Ltmp240-.Lfunc_begin1  #     jumps to .Ltmp240
	.byte	1                       #   On action: 1
	.long	.Ltmp210-.Lfunc_begin1  # >> Call Site 140 <<
	.long	.Ltmp211-.Ltmp210       #   Call between .Ltmp210 and .Ltmp211
	.long	.Ltmp212-.Lfunc_begin1  #     jumps to .Ltmp212
	.byte	1                       #   On action: 1
	.long	.Ltmp216-.Lfunc_begin1  # >> Call Site 141 <<
	.long	.Ltmp217-.Ltmp216       #   Call between .Ltmp216 and .Ltmp217
	.long	.Ltmp240-.Lfunc_begin1  #     jumps to .Ltmp240
	.byte	1                       #   On action: 1
	.long	.Ltmp213-.Lfunc_begin1  # >> Call Site 142 <<
	.long	.Ltmp214-.Ltmp213       #   Call between .Ltmp213 and .Ltmp214
	.long	.Ltmp215-.Lfunc_begin1  #     jumps to .Ltmp215
	.byte	1                       #   On action: 1
	.long	.Ltmp66-.Lfunc_begin1   # >> Call Site 143 <<
	.long	.Ltmp311-.Ltmp66        #   Call between .Ltmp66 and .Ltmp311
	.long	.Ltmp508-.Lfunc_begin1  #     jumps to .Ltmp508
	.byte	1                       #   On action: 1
	.long	.Ltmp315-.Lfunc_begin1  # >> Call Site 144 <<
	.long	.Ltmp316-.Ltmp315       #   Call between .Ltmp315 and .Ltmp316
	.long	.Ltmp317-.Lfunc_begin1  #     jumps to .Ltmp317
	.byte	1                       #   On action: 1
	.long	.Ltmp420-.Lfunc_begin1  # >> Call Site 145 <<
	.long	.Ltmp446-.Ltmp420       #   Call between .Ltmp420 and .Ltmp446
	.long	.Ltmp508-.Lfunc_begin1  #     jumps to .Ltmp508
	.byte	1                       #   On action: 1
	.long	.Ltmp474-.Lfunc_begin1  # >> Call Site 146 <<
	.long	.Ltmp475-.Ltmp474       #   Call between .Ltmp474 and .Ltmp475
	.long	.Ltmp476-.Lfunc_begin1  #     jumps to .Ltmp476
	.byte	1                       #   On action: 1
	.long	.Ltmp480-.Lfunc_begin1  # >> Call Site 147 <<
	.long	.Ltmp507-.Ltmp480       #   Call between .Ltmp480 and .Ltmp507
	.long	.Ltmp508-.Lfunc_begin1  #     jumps to .Ltmp508
	.byte	1                       #   On action: 1
	.long	.Ltmp507-.Lfunc_begin1  # >> Call Site 148 <<
	.long	.Ltmp477-.Ltmp507       #   Call between .Ltmp507 and .Ltmp477
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp477-.Lfunc_begin1  # >> Call Site 149 <<
	.long	.Ltmp478-.Ltmp477       #   Call between .Ltmp477 and .Ltmp478
	.long	.Ltmp479-.Lfunc_begin1  #     jumps to .Ltmp479
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7zL10WriteRangeEP9IInStreamP20ISequentialOutStreamyyP21ICompressProgressInfo,@function
_ZN8NArchive3N7zL10WriteRangeEP9IInStreamP20ISequentialOutStreamyyP21ICompressProgressInfo: # @_ZN8NArchive3N7zL10WriteRangeEP9IInStreamP20ISequentialOutStreamyyP21ICompressProgressInfo
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi336:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi337:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi338:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi339:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi340:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi341:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi342:
	.cfi_def_cfa_offset 64
.Lcfi343:
	.cfi_offset %rbx, -56
.Lcfi344:
	.cfi_offset %r12, -48
.Lcfi345:
	.cfi_offset %r13, -40
.Lcfi346:
	.cfi_offset %r14, -32
.Lcfi347:
	.cfi_offset %r15, -24
.Lcfi348:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r14
	movq	%rdx, %rax
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%rbx), %rbp
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	callq	*48(%rbp)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB13_11
# BB#1:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %r13
	movl	$0, 8(%r13)
	movq	$_ZTV26CLimitedSequentialInStream+16, (%r13)
	movq	$0, 16(%r13)
	movq	%r13, %rdi
	callq	*_ZTV26CLimitedSequentialInStream+24(%rip)
	movq	(%rbx), %rax
.Ltmp509:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp510:
# BB#2:                                 # %.noexc
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB13_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp511:
	callq	*16(%rax)
.Ltmp512:
.LBB13_4:
	movq	%rbx, 16(%r13)
	movq	%r14, 24(%r13)
	movq	$0, 32(%r13)
	movb	$0, 40(%r13)
.Ltmp513:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp514:
# BB#5:
	movl	$0, 16(%rbx)
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbx)
.Ltmp516:
	movq	%rbx, %rdi
	callq	*_ZTVN9NCompress10CCopyCoderE+24(%rip)
.Ltmp517:
# BB#6:                                 # %_ZN9CMyComPtrI14ICompressCoderEC2EPS0_.exit
	movq	(%rbx), %rax
.Ltmp519:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %r9
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp520:
# BB#7:
	testl	%ebp, %ebp
	jne	.LBB13_9
# BB#8:
	xorl	%eax, %eax
	cmpq	%r14, 32(%rbx)
	movl	$-2147467259, %ebp      # imm = 0x80004005
	cmovel	%eax, %ebp
.LBB13_9:
	movq	(%rbx), %rax
.Ltmp524:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp525:
# BB#10:                                # %_ZN9CMyComPtrI26CLimitedSequentialInStreamED2Ev.exit44
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*16(%rax)
.LBB13_11:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_15:
.Ltmp526:
	jmp	.LBB13_16
.LBB13_14:
.Ltmp521:
	movq	%rax, %rbp
	movq	(%rbx), %rax
.Ltmp522:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp523:
	jmp	.LBB13_17
.LBB13_13:
.Ltmp518:
	jmp	.LBB13_16
.LBB13_12:
.Ltmp515:
.LBB13_16:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit47
	movq	%rax, %rbp
.LBB13_17:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit47
	movq	(%r13), %rax
.Ltmp527:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp528:
# BB#18:                                # %_ZN9CMyComPtrI26CLimitedSequentialInStreamED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB13_19:
.Ltmp529:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN8NArchive3N7zL10WriteRangeEP9IInStreamP20ISequentialOutStreamyyP21ICompressProgressInfo, .Lfunc_end13-_ZN8NArchive3N7zL10WriteRangeEP9IInStreamP20ISequentialOutStreamyyP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp509-.Lfunc_begin2  #   Call between .Lfunc_begin2 and .Ltmp509
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp509-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp514-.Ltmp509       #   Call between .Ltmp509 and .Ltmp514
	.long	.Ltmp515-.Lfunc_begin2  #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp516-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp517-.Ltmp516       #   Call between .Ltmp516 and .Ltmp517
	.long	.Ltmp518-.Lfunc_begin2  #     jumps to .Ltmp518
	.byte	0                       #   On action: cleanup
	.long	.Ltmp519-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Ltmp520-.Ltmp519       #   Call between .Ltmp519 and .Ltmp520
	.long	.Ltmp521-.Lfunc_begin2  #     jumps to .Ltmp521
	.byte	0                       #   On action: cleanup
	.long	.Ltmp524-.Lfunc_begin2  # >> Call Site 5 <<
	.long	.Ltmp525-.Ltmp524       #   Call between .Ltmp524 and .Ltmp525
	.long	.Ltmp526-.Lfunc_begin2  #     jumps to .Ltmp526
	.byte	0                       #   On action: cleanup
	.long	.Ltmp525-.Lfunc_begin2  # >> Call Site 6 <<
	.long	.Ltmp522-.Ltmp525       #   Call between .Ltmp525 and .Ltmp522
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp522-.Lfunc_begin2  # >> Call Site 7 <<
	.long	.Ltmp528-.Ltmp522       #   Call between .Ltmp522 and .Ltmp528
	.long	.Ltmp529-.Lfunc_begin2  #     jumps to .Ltmp529
	.byte	1                       #   On action: 1
	.long	.Ltmp528-.Lfunc_begin2  # >> Call Site 8 <<
	.long	.Lfunc_end13-.Ltmp528   #   Call between .Ltmp528 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE4SortEPFiPKS2_S5_PvES6_,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE4SortEPFiPKS2_S5_PvES6_,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE4SortEPFiPKS2_S5_PvES6_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE4SortEPFiPKS2_S5_PvES6_,@function
_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE4SortEPFiPKS2_S5_PvES6_: # @_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE4SortEPFiPKS2_S5_PvES6_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi349:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi350:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi351:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi352:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi353:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi354:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi355:
	.cfi_def_cfa_offset 112
.Lcfi356:
	.cfi_offset %rbx, -56
.Lcfi357:
	.cfi_offset %r12, -48
.Lcfi358:
	.cfi_offset %r13, -40
.Lcfi359:
	.cfi_offset %r14, -32
.Lcfi360:
	.cfi_offset %r15, -24
.Lcfi361:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r8
	movslq	12(%rdi), %rsi
	cmpq	$2, %rsi
	jl	.LBB14_20
# BB#1:
	movq	16(%rdi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	-12(%rax), %r13
	movl	%esi, %edx
	shrl	%edx
	movq	(%rsp), %r9             # 8-byte Reload
	movq	%r8, %r15
	.p2align	4, 0x90
.LBB14_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_5 Depth 2
	leaq	(%rdx,%rdx,2), %rax
	movl	8(%r13,%rax,4), %ecx
	movl	%ecx, 16(%rsp)
	movq	(%r13,%rax,4), %rax
	movq	%rax, 8(%rsp)
	leal	(%rdx,%rdx), %ebx
	cmpl	%esi, %ebx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	jle	.LBB14_4
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	movl	%edx, %r14d
	jmp	.LBB14_11
	.p2align	4, 0x90
.LBB14_4:                               # %.lr.ph.i28.preheader
                                        #   in Loop: Header=BB14_2 Depth=1
	movl	%edx, %ebp
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph.i28
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %r12
	cmpl	%esi, %ebx
	jge	.LBB14_6
# BB#7:                                 #   in Loop: Header=BB14_5 Depth=2
	movslq	%ebx, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r13,%rax,4), %rsi
	leaq	12(%r13,%rax,4), %rdi
	movq	%r9, %rdx
	callq	*%r15
	movq	(%rsp), %r9             # 8-byte Reload
	movq	%r15, %r8
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setg	%r14b
	orl	%ebx, %r14d
	jmp	.LBB14_8
	.p2align	4, 0x90
.LBB14_6:                               #   in Loop: Header=BB14_5 Depth=2
	movl	%ebx, %r14d
.LBB14_8:                               #   in Loop: Header=BB14_5 Depth=2
	movslq	%r14d, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r13,%rax,4), %rbx
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r9, %rdx
	callq	*%r8
	testl	%eax, %eax
	jns	.LBB14_9
# BB#10:                                #   in Loop: Header=BB14_5 Depth=2
	movslq	%ebp, %rax
	leaq	(%rax,%rax,2), %rax
	movl	8(%rbx), %ecx
	movl	%ecx, 8(%r13,%rax,4)
	movq	(%rbx), %rcx
	movq	%rcx, (%r13,%rax,4)
	leal	(%r14,%r14), %ebx
	movq	%r12, %rsi
	cmpl	%esi, %ebx
	movl	%r14d, %ebp
	movq	%r15, %r8
	movq	(%rsp), %r9             # 8-byte Reload
	jle	.LBB14_5
	jmp	.LBB14_11
	.p2align	4, 0x90
.LBB14_9:                               #   in Loop: Header=BB14_2 Depth=1
	movl	%ebp, %r14d
	movq	%r15, %r8
	movq	%r12, %rsi
	movq	(%rsp), %r9             # 8-byte Reload
.LBB14_11:                              # %_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE11SortRefDownEPS2_iiPFiPKS2_S6_PvES7_.exit32
                                        #   in Loop: Header=BB14_2 Depth=1
	movslq	%r14d, %rax
	leaq	(%rax,%rax,2), %rax
	movl	16(%rsp), %ecx
	movl	%ecx, 8(%r13,%rax,4)
	movq	8(%rsp), %rcx
	movq	%rcx, (%r13,%rax,4)
	movq	24(%rsp), %rdx          # 8-byte Reload
	decq	%rdx
	testl	%edx, %edx
	jne	.LBB14_2
	.p2align	4, 0x90
.LBB14_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_14 Depth 2
	movq	%rsi, %rdi
	leaq	(%rdi,%rdi,2), %rax
	movl	8(%r13,%rax,4), %ecx
	movl	%ecx, 40(%rsp)
	movq	(%r13,%rax,4), %rcx
	movq	%rcx, 32(%rsp)
	leaq	-1(%rdi), %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	8(%rdx), %ecx
	movl	%ecx, 8(%r13,%rax,4)
	movq	(%rdx), %rcx
	movq	%rcx, (%r13,%rax,4)
	movl	40(%rsp), %eax
	movl	%eax, 8(%rdx)
	movq	32(%rsp), %rax
	movq	%rax, (%rdx)
	movl	40(%rsp), %eax
	movl	%eax, 16(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$1, %r12d
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	cmpq	$3, %rdi
	jl	.LBB14_19
# BB#13:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB14_12 Depth=1
	movl	$1, %r14d
	movl	$2, %eax
	.p2align	4, 0x90
.LBB14_14:                              # %.lr.ph.i
                                        #   Parent Loop BB14_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%eax, %rbx
	movq	%rsi, %rbp
	cmpq	%rsi, %rbx
	movl	%eax, %r12d
	jge	.LBB14_16
# BB#15:                                #   in Loop: Header=BB14_14 Depth=2
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r13,%rax,4), %rsi
	leaq	12(%r13,%rax,4), %rdi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	*%r15
	movq	%r15, %r8
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setg	%cl
	orl	%ecx, %ebx
	movl	%ebx, %r12d
.LBB14_16:                              #   in Loop: Header=BB14_14 Depth=2
	movslq	%r12d, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r13,%rax,4), %rbx
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	*%r8
	testl	%eax, %eax
	jns	.LBB14_17
# BB#18:                                #   in Loop: Header=BB14_14 Depth=2
	movslq	%r14d, %rax
	leaq	(%rax,%rax,2), %rax
	movl	8(%rbx), %ecx
	movl	%ecx, 8(%r13,%rax,4)
	movq	(%rbx), %rcx
	movq	%rcx, (%r13,%rax,4)
	leal	(%r12,%r12), %eax
	movslq	%eax, %rcx
	cmpq	24(%rsp), %rcx          # 8-byte Folded Reload
	movl	%r12d, %r14d
	movq	%r15, %r8
	movq	%rbp, %rsi
	jl	.LBB14_14
	jmp	.LBB14_19
	.p2align	4, 0x90
.LBB14_17:                              #   in Loop: Header=BB14_12 Depth=1
	movl	%r14d, %r12d
	movq	%r15, %r8
	movq	%rbp, %rsi
.LBB14_19:                              # %_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE11SortRefDownEPS2_iiPFiPKS2_S6_PvES7_.exit
                                        #   in Loop: Header=BB14_12 Depth=1
	movslq	%r12d, %rax
	leaq	(%rax,%rax,2), %rax
	movl	16(%rsp), %ecx
	movl	%ecx, 8(%r13,%rax,4)
	movq	8(%rsp), %rcx
	movq	%rcx, (%r13,%rax,4)
	cmpq	$1, %rsi
	jg	.LBB14_12
.LBB14_20:                              # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE4SortEPFiPKS2_S5_PvES6_, .Lfunc_end14-_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEE4SortEPFiPKS2_S5_PvES6_
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7zL20CompareFolderRepacksEPKNS0_13CFolderRepackES3_Pv,@function
_ZN8NArchive3N7zL20CompareFolderRepacksEPKNS0_13CFolderRepackES3_Pv: # @_ZN8NArchive3N7zL20CompareFolderRepacksEPKNS0_13CFolderRepackES3_Pv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi362:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi363:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi364:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi365:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi366:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi367:
	.cfi_def_cfa_offset 56
.Lcfi368:
	.cfi_offset %rbx, -56
.Lcfi369:
	.cfi_offset %r12, -48
.Lcfi370:
	.cfi_offset %r13, -40
.Lcfi371:
	.cfi_offset %r14, -32
.Lcfi372:
	.cfi_offset %r15, -24
.Lcfi373:
	.cfi_offset %rbp, -16
	movl	4(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	4(%rsi), %ecx
	setne	%al
	movl	$-1, %ebx
	cmovll	%ebx, %eax
	testl	%eax, %eax
	jne	.LBB15_24
# BB#1:
	movslq	(%rdi), %rcx
	movslq	(%rsi), %r9
	movq	112(%rdx), %rax
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r10
	movq	(%rax,%r9,8), %r11
	movslq	12(%r10), %rcx
	xorl	%eax, %eax
	cmpl	12(%r11), %ecx
	setne	%al
	cmovll	%ebx, %eax
	testl	%eax, %eax
	jne	.LBB15_22
# BB#2:                                 # %.preheader68.i
	testl	%ecx, %ecx
	jle	.LBB15_14
# BB#3:                                 # %.lr.ph78.i
	movq	16(%r10), %r13
	movq	16(%r11), %r8
	xorl	%edi, %edi
	movl	$-1, %r14d
	jmp	.LBB15_4
	.p2align	4, 0x90
.LBB15_13:                              # %_ZN8NArchive3N7zL13CompareCodersERKNS0_10CCoderInfoES3_.exit.thread.i
                                        #   in Loop: Header=BB15_4 Depth=1
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB15_4
	jmp	.LBB15_14
.LBB15_8:                               # %.preheader.i.i.i
                                        #   in Loop: Header=BB15_4 Depth=1
	testq	%r15, %r15
	je	.LBB15_13
# BB#9:                                 # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB15_4 Depth=1
	movq	24(%rbx), %r12
	movq	24(%rbp), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_11:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB15_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r12,%rbx), %edx
	xorl	%eax, %eax
	cmpb	(%rbp,%rbx), %dl
	setne	%al
	cmovbl	%r14d, %eax
	testl	%eax, %eax
	jne	.LBB15_12
# BB#10:                                #   in Loop: Header=BB15_11 Depth=2
	incq	%rbx
	cmpq	%r15, %rbx
	jb	.LBB15_11
	jmp	.LBB15_13
	.p2align	4, 0x90
.LBB15_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_11 Depth 2
	movq	(%r13,%rdi,8), %rbx
	movq	(%r8,%rdi,8), %rbp
	movl	32(%rbx), %esi
	xorl	%eax, %eax
	cmpl	32(%rbp), %esi
	setne	%al
	cmovbl	%r14d, %eax
	testl	%eax, %eax
	jne	.LBB15_12
# BB#5:                                 #   in Loop: Header=BB15_4 Depth=1
	movl	36(%rbx), %esi
	xorl	%eax, %eax
	cmpl	36(%rbp), %esi
	setne	%al
	cmovbl	%r14d, %eax
	testl	%eax, %eax
	jne	.LBB15_12
# BB#6:                                 #   in Loop: Header=BB15_4 Depth=1
	movq	(%rbx), %rsi
	xorl	%eax, %eax
	cmpq	(%rbp), %rsi
	setne	%al
	cmovbl	%r14d, %eax
	testl	%eax, %eax
	jne	.LBB15_12
# BB#7:                                 #   in Loop: Header=BB15_4 Depth=1
	movq	16(%rbx), %r15
	xorl	%eax, %eax
	cmpq	16(%rbp), %r15
	setne	%al
	cmovbl	%r14d, %eax
	testl	%eax, %eax
	je	.LBB15_8
	.p2align	4, 0x90
.LBB15_12:                              # %_ZN8NArchive3N7zL13CompareCodersERKNS0_10CCoderInfoES3_.exit.i
                                        #   in Loop: Header=BB15_4 Depth=1
	testl	%eax, %eax
	je	.LBB15_13
	jmp	.LBB15_22
.LBB15_14:                              # %._crit_edge.i
	movslq	44(%r10), %rcx
	xorl	%edx, %edx
	cmpl	44(%r11), %ecx
	setne	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	testl	%eax, %eax
	je	.LBB15_15
.LBB15_22:                              # %_ZN8NArchive3N7zL14CompareFoldersERKNS0_7CFolderES3_.exit
	testl	%eax, %eax
	jne	.LBB15_24
.LBB15_23:                              # %_ZN8NArchive3N7zL14CompareFoldersERKNS0_7CFolderES3_.exit.thread
	xorl	%ecx, %ecx
	cmpl	%r9d, -8(%rsp)          # 4-byte Folded Reload
	setne	%cl
	movl	$-1, %eax
	cmovgel	%ecx, %eax
.LBB15_24:                              # %_ZN8NArchive3N7zL14CompareFoldersERKNS0_7CFolderES3_.exit.thread33
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_15:                              # %.preheader.i
	testl	%ecx, %ecx
	jle	.LBB15_23
# BB#16:                                # %.lr.ph.i
	movq	48(%r10), %rdx
	movq	48(%r11), %rsi
	xorl	%edi, %edi
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB15_18:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rdi,8), %ebx
	xorl	%eax, %eax
	cmpl	(%rsi,%rdi,8), %ebx
	setne	%al
	cmovbl	%ebp, %eax
	testl	%eax, %eax
	jne	.LBB15_21
# BB#19:                                #   in Loop: Header=BB15_18 Depth=1
	movl	4(%rsi,%rdi,8), %ebx
	xorl	%eax, %eax
	cmpl	%ebx, 4(%rdx,%rdi,8)
	setne	%bl
	jb	.LBB15_25
# BB#20:                                #   in Loop: Header=BB15_18 Depth=1
	movb	%bl, %al
.LBB15_21:                              # %_ZN8NArchive3N7zL16CompareBindPairsERKNS0_9CBindPairES3_.exit.i
                                        #   in Loop: Header=BB15_18 Depth=1
	testl	%eax, %eax
	jne	.LBB15_22
# BB#17:                                #   in Loop: Header=BB15_18 Depth=1
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB15_18
	jmp	.LBB15_23
.LBB15_25:
	movl	$-1, %eax
	jmp	.LBB15_24
.Lfunc_end15:
	.size	_ZN8NArchive3N7zL20CompareFolderRepacksEPKNS0_13CFolderRepackES3_Pv, .Lfunc_end15-_ZN8NArchive3N7zL20CompareFolderRepacksEPKNS0_13CFolderRepackES3_Pv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z14CThreadDecoderC2Ev,"axG",@progbits,_ZN8NArchive3N7z14CThreadDecoderC2Ev,comdat
	.weak	_ZN8NArchive3N7z14CThreadDecoderC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z14CThreadDecoderC2Ev,@function
_ZN8NArchive3N7z14CThreadDecoderC2Ev:   # @_ZN8NArchive3N7z14CThreadDecoderC2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi374:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi375:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi376:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi377:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi378:
	.cfi_def_cfa_offset 48
.Lcfi379:
	.cfi_offset %rbx, -48
.Lcfi380:
	.cfi_offset %r12, -40
.Lcfi381:
	.cfi_offset %r13, -32
.Lcfi382:
	.cfi_offset %r14, -24
.Lcfi383:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	$0, 8(%r12)
	movl	$0, 112(%r12)
	movl	$0, 224(%r12)
	movq	$_ZTVN8NArchive3N7z14CThreadDecoderE+16, (%r12)
	movq	$0, 240(%r12)
	movq	$0, 256(%r12)
	movq	$0, 288(%r12)
	leaq	296(%r12), %r14
.Ltmp530:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z8CDecoderC1Eb
.Ltmp531:
# BB#1:
	movb	$0, 528(%r12)
	movl	$1, 532(%r12)
.Ltmp533:
	movl	$80, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp534:
# BB#2:
	movl	$0, 8(%r13)
	movq	$_ZTVN8NArchive3N7z17CFolderOutStream2E+16, (%r13)
	movq	$0, 24(%r13)
	movq	$0, 48(%r13)
.Ltmp535:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp536:
# BB#3:
	movl	$0, 8(%rbx)
	movq	$_ZTV17COutStreamWithCRC+16, (%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, 16(%r13)
.Ltmp537:
	movq	%rbx, %rdi
	callq	*_ZTV17COutStreamWithCRC+24(%rip)
.Ltmp538:
# BB#4:                                 # %.noexc5.i
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB16_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp539:
	callq	*16(%rax)
.Ltmp540:
.LBB16_6:
	movq	%rbx, 24(%r13)
	movq	%r13, 248(%r12)
	movq	(%r13), %rax
.Ltmp547:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp548:
# BB#7:                                 # %.noexc6
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB16_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp549:
	callq	*16(%rax)
.Ltmp550:
.LBB16_9:
	movq	%r13, 256(%r12)
	movl	$-2147467259, 236(%r12) # imm = 0x80004005
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB16_16:
.Ltmp532:
	movq	%rax, %r15
	jmp	.LBB16_19
.LBB16_17:
.Ltmp551:
	movq	%rax, %r15
	jmp	.LBB16_18
.LBB16_10:
.Ltmp541:
	movq	%rax, %r15
	movq	48(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB16_12
# BB#11:
	movq	(%rdi), %rax
.Ltmp542:
	callq	*16(%rax)
.Ltmp543:
.LBB16_12:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit4.i
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB16_14
# BB#13:
	movq	(%rdi), %rax
.Ltmp544:
	callq	*16(%rax)
.Ltmp545:
.LBB16_14:                              # %.body
	movq	%r13, %rdi
	callq	_ZdlPv
.LBB16_18:
.Ltmp552:
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z8CDecoderD2Ev
.Ltmp553:
.LBB16_19:
	movq	288(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB16_21
# BB#20:
	movq	(%rdi), %rax
.Ltmp554:
	callq	*16(%rax)
.Ltmp555:
.LBB16_21:                              # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB16_23
# BB#22:
	movq	(%rdi), %rax
.Ltmp556:
	callq	*16(%rax)
.Ltmp557:
.LBB16_23:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB16_25
# BB#24:
	movq	(%rdi), %rax
.Ltmp558:
	callq	*16(%rax)
.Ltmp559:
.LBB16_25:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
.Ltmp560:
	movq	%r12, %rdi
	callq	_ZN11CVirtThreadD2Ev
.Ltmp561:
# BB#26:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB16_15:
.Ltmp546:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB16_27:
.Ltmp562:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN8NArchive3N7z14CThreadDecoderC2Ev, .Lfunc_end16-_ZN8NArchive3N7z14CThreadDecoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp530-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp531-.Ltmp530       #   Call between .Ltmp530 and .Ltmp531
	.long	.Ltmp532-.Lfunc_begin3  #     jumps to .Ltmp532
	.byte	0                       #   On action: cleanup
	.long	.Ltmp533-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp534-.Ltmp533       #   Call between .Ltmp533 and .Ltmp534
	.long	.Ltmp551-.Lfunc_begin3  #     jumps to .Ltmp551
	.byte	0                       #   On action: cleanup
	.long	.Ltmp535-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp540-.Ltmp535       #   Call between .Ltmp535 and .Ltmp540
	.long	.Ltmp541-.Lfunc_begin3  #     jumps to .Ltmp541
	.byte	0                       #   On action: cleanup
	.long	.Ltmp547-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp550-.Ltmp547       #   Call between .Ltmp547 and .Ltmp550
	.long	.Ltmp551-.Lfunc_begin3  #     jumps to .Ltmp551
	.byte	0                       #   On action: cleanup
	.long	.Ltmp542-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp545-.Ltmp542       #   Call between .Ltmp542 and .Ltmp545
	.long	.Ltmp546-.Lfunc_begin3  #     jumps to .Ltmp546
	.byte	1                       #   On action: 1
	.long	.Ltmp552-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp561-.Ltmp552       #   Call between .Ltmp552 and .Ltmp561
	.long	.Ltmp562-.Lfunc_begin3  #     jumps to .Ltmp562
	.byte	1                       #   On action: 1
	.long	.Ltmp561-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Lfunc_end16-.Ltmp561   #   Call between .Ltmp561 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end17:
	.size	__clang_call_terminate, .Lfunc_end17-__clang_call_terminate

	.section	.text._ZN8NArchive3N7z22CCompressionMethodModeC2Ev,"axG",@progbits,_ZN8NArchive3N7z22CCompressionMethodModeC2Ev,comdat
	.weak	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev,@function
_ZN8NArchive3N7z22CCompressionMethodModeC2Ev: # @_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi384:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi385:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi386:
	.cfi_def_cfa_offset 32
.Lcfi387:
	.cfi_offset %rbx, -32
.Lcfi388:
	.cfi_offset %r14, -24
.Lcfi389:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	$8, 24(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
	leaq	32(%rbx), %r15
	movups	%xmm0, 40(%rbx)
	movq	$16, 56(%rbx)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE+16, 32(%rbx)
	movl	$1, 64(%rbx)
	movb	$0, 68(%rbx)
	movups	%xmm0, 72(%rbx)
.Ltmp563:
	movl	$16, %edi
	callq	_Znam
.Ltmp564:
# BB#1:
	movq	%rax, 72(%rbx)
	movl	$0, (%rax)
	movl	$4, 84(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB18_2:
.Ltmp565:
	movq	%rax, %r14
.Ltmp566:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp567:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp568:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp569:
# BB#4:
.Ltmp574:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp575:
# BB#5:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_6:
.Ltmp570:
	movq	%rax, %r14
.Ltmp571:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp572:
	jmp	.LBB18_9
.LBB18_7:
.Ltmp573:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_8:
.Ltmp576:
	movq	%rax, %r14
.LBB18_9:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev, .Lfunc_end18-_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp563-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp564-.Ltmp563       #   Call between .Ltmp563 and .Ltmp564
	.long	.Ltmp565-.Lfunc_begin4  #     jumps to .Ltmp565
	.byte	0                       #   On action: cleanup
	.long	.Ltmp566-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp567-.Ltmp566       #   Call between .Ltmp566 and .Ltmp567
	.long	.Ltmp576-.Lfunc_begin4  #     jumps to .Ltmp576
	.byte	1                       #   On action: 1
	.long	.Ltmp568-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp569-.Ltmp568       #   Call between .Ltmp568 and .Ltmp569
	.long	.Ltmp570-.Lfunc_begin4  #     jumps to .Ltmp570
	.byte	1                       #   On action: 1
	.long	.Ltmp574-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp575-.Ltmp574       #   Call between .Ltmp574 and .Ltmp575
	.long	.Ltmp576-.Lfunc_begin4  #     jumps to .Ltmp576
	.byte	1                       #   On action: 1
	.long	.Ltmp575-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp571-.Ltmp575       #   Call between .Ltmp575 and .Ltmp571
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp571-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp572-.Ltmp571       #   Call between .Ltmp571 and .Ltmp572
	.long	.Ltmp573-.Lfunc_begin4  #     jumps to .Ltmp573
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_,"axG",@progbits,_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_,comdat
	.weak	_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_,@function
_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_: # @_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi390:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi391:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi392:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi393:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi394:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi395:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi396:
	.cfi_def_cfa_offset 80
.Lcfi397:
	.cfi_offset %rbx, -56
.Lcfi398:
	.cfi_offset %r12, -48
.Lcfi399:
	.cfi_offset %r13, -40
.Lcfi400:
	.cfi_offset %r14, -32
.Lcfi401:
	.cfi_offset %r15, -24
.Lcfi402:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	12(%r14), %ebx
	movl	12(%r15), %esi
	addl	%ebx, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%ebx, %ebx
	jle	.LBB19_3
# BB#1:                                 # %.lr.ph.i.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbp,8), %rsi
	movq	%r15, %rdi
	callq	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB19_2
.LBB19_3:                               # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEEaSERKS3_.exit
	leaq	32(%r15), %r12
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	44(%r14), %r13d
	movl	44(%r15), %esi
	addl	%r13d, %esi
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r13d, %r13d
	jle	.LBB19_6
# BB#4:                                 # %.lr.ph.i.i6
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_5:                               # =>This Inner Loop Header: Depth=1
	movq	48(%r14), %rax
	movups	(%rax,%rbx), %xmm0
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	48(%r15), %rax
	movslq	44(%r15), %rcx
	shlq	$4, %rcx
	movaps	(%rsp), %xmm0           # 16-byte Reload
	movups	%xmm0, (%rax,%rcx)
	incl	44(%r15)
	addq	$16, %rbx
	decq	%r13
	jne	.LBB19_5
.LBB19_6:                               # %_ZN13CRecordVectorIN8NArchive3N7z5CBindEEaSERKS3_.exit
	movb	68(%r14), %al
	movb	%al, 68(%r15)
	movl	64(%r14), %eax
	movl	%eax, 64(%r15)
	cmpq	%r15, %r14
	je	.LBB19_15
# BB#7:
	movl	$0, 80(%r15)
	movq	72(%r15), %rbx
	movl	$0, (%rbx)
	movslq	80(%r14), %r12
	incq	%r12
	movl	84(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB19_12
# BB#8:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB19_11
# BB#9:
	testl	%ebp, %ebp
	jle	.LBB19_11
# BB#10:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	80(%r15), %rax
.LBB19_11:                              # %._crit_edge16.i.i
	movq	%r13, 72(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 84(%r15)
	movq	%r13, %rbx
.LBB19_12:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	72(%r14), %rax
	.p2align	4, 0x90
.LBB19_13:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB19_13
# BB#14:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	80(%r14), %eax
	movl	%eax, 80(%r15)
.LBB19_15:                              # %_ZN11CStringBaseIwEaSERKS0_.exit
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_, .Lfunc_end19-_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z7CFolderD2Ev,"axG",@progbits,_ZN8NArchive3N7z7CFolderD2Ev,comdat
	.weak	_ZN8NArchive3N7z7CFolderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z7CFolderD2Ev,@function
_ZN8NArchive3N7z7CFolderD2Ev:           # @_ZN8NArchive3N7z7CFolderD2Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi403:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi404:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi405:
	.cfi_def_cfa_offset 32
.Lcfi406:
	.cfi_offset %rbx, -24
.Lcfi407:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	96(%rbx), %rdi
.Ltmp577:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp578:
# BB#1:
	leaq	64(%rbx), %rdi
.Ltmp582:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp583:
# BB#2:
	leaq	32(%rbx), %rdi
.Ltmp587:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp588:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp599:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp600:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB20_5:
.Ltmp601:
	movq	%rax, %r14
.Ltmp602:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp603:
	jmp	.LBB20_6
.LBB20_7:
.Ltmp604:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB20_9:
.Ltmp589:
	movq	%rax, %r14
	jmp	.LBB20_12
.LBB20_10:
.Ltmp584:
	movq	%rax, %r14
	jmp	.LBB20_11
.LBB20_8:
.Ltmp579:
	movq	%rax, %r14
	leaq	64(%rbx), %rdi
.Ltmp580:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp581:
.LBB20_11:
	leaq	32(%rbx), %rdi
.Ltmp585:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp586:
.LBB20_12:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp590:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp591:
# BB#13:
.Ltmp596:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp597:
.LBB20_6:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_14:
.Ltmp592:
	movq	%rax, %r14
.Ltmp593:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp594:
	jmp	.LBB20_17
.LBB20_15:
.Ltmp595:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB20_16:
.Ltmp598:
	movq	%rax, %r14
.LBB20_17:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end20:
	.size	_ZN8NArchive3N7z7CFolderD2Ev, .Lfunc_end20-_ZN8NArchive3N7z7CFolderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp577-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp578-.Ltmp577       #   Call between .Ltmp577 and .Ltmp578
	.long	.Ltmp579-.Lfunc_begin5  #     jumps to .Ltmp579
	.byte	0                       #   On action: cleanup
	.long	.Ltmp582-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp583-.Ltmp582       #   Call between .Ltmp582 and .Ltmp583
	.long	.Ltmp584-.Lfunc_begin5  #     jumps to .Ltmp584
	.byte	0                       #   On action: cleanup
	.long	.Ltmp587-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp588-.Ltmp587       #   Call between .Ltmp587 and .Ltmp588
	.long	.Ltmp589-.Lfunc_begin5  #     jumps to .Ltmp589
	.byte	0                       #   On action: cleanup
	.long	.Ltmp599-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp600-.Ltmp599       #   Call between .Ltmp599 and .Ltmp600
	.long	.Ltmp601-.Lfunc_begin5  #     jumps to .Ltmp601
	.byte	0                       #   On action: cleanup
	.long	.Ltmp600-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp602-.Ltmp600       #   Call between .Ltmp600 and .Ltmp602
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp602-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp603-.Ltmp602       #   Call between .Ltmp602 and .Ltmp603
	.long	.Ltmp604-.Lfunc_begin5  #     jumps to .Ltmp604
	.byte	1                       #   On action: 1
	.long	.Ltmp580-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp586-.Ltmp580       #   Call between .Ltmp580 and .Ltmp586
	.long	.Ltmp598-.Lfunc_begin5  #     jumps to .Ltmp598
	.byte	1                       #   On action: 1
	.long	.Ltmp590-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp591-.Ltmp590       #   Call between .Ltmp590 and .Ltmp591
	.long	.Ltmp592-.Lfunc_begin5  #     jumps to .Ltmp592
	.byte	1                       #   On action: 1
	.long	.Ltmp596-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp597-.Ltmp596       #   Call between .Ltmp596 and .Ltmp597
	.long	.Ltmp598-.Lfunc_begin5  #     jumps to .Ltmp598
	.byte	1                       #   On action: 1
	.long	.Ltmp597-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp593-.Ltmp597       #   Call between .Ltmp597 and .Ltmp593
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp593-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp594-.Ltmp593       #   Call between .Ltmp593 and .Ltmp594
	.long	.Ltmp595-.Lfunc_begin5  #     jumps to .Ltmp595
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7zL24FromUpdateItemToFileItemERKNS0_11CUpdateItemERNS0_9CFileItemERNS0_10CFileItem2E,@function
_ZN8NArchive3N7zL24FromUpdateItemToFileItemERKNS0_11CUpdateItemERNS0_9CFileItemERNS0_10CFileItem2E: # @_ZN8NArchive3N7zL24FromUpdateItemToFileItemERKNS0_11CUpdateItemERNS0_9CFileItemERNS0_10CFileItem2E
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi408:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi409:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi410:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi411:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi412:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi413:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi414:
	.cfi_def_cfa_offset 80
.Lcfi415:
	.cfi_offset %rbx, -56
.Lcfi416:
	.cfi_offset %r12, -48
.Lcfi417:
	.cfi_offset %r13, -40
.Lcfi418:
	.cfi_offset %r14, -32
.Lcfi419:
	.cfi_offset %r15, -24
.Lcfi420:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	leaq	40(%r12), %rsi
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	callq	_ZN8NArchive9NItemName13MakeLegalNameERK11CStringBaseIwE
	leaq	16(%r14), %rax
	cmpq	%rax, %rbx
	je	.LBB21_1
# BB#2:
	movl	$0, 24(%r14)
	movq	16(%r14), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %r13
	incq	%r13
	movl	28(%r14), %ebp
	cmpl	%ebp, %r13d
	je	.LBB21_8
# BB#3:
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp605:
	callq	_Znam
.Ltmp606:
# BB#4:                                 # %.noexc
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB21_7
# BB#5:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB21_7
# BB#6:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	24(%r14), %rcx
.LBB21_7:                               # %._crit_edge16.i.i
	movq	%rax, 16(%r14)
	movl	$0, (%rax,%rcx,4)
	movl	%r13d, 28(%r14)
	movq	%rax, %rbx
.LBB21_8:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB21_9:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB21_9
# BB#10:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	16(%rsp), %eax
	movl	%eax, 24(%r14)
	testq	%rdi, %rdi
	jne	.LBB21_12
	jmp	.LBB21_13
.LBB21_1:                               # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_13
.LBB21_12:
	callq	_ZdaPv
.LBB21_13:                              # %_ZN11CStringBaseIwED2Ev.exit
	cmpb	$0, 64(%r12)
	je	.LBB21_15
# BB#14:
	movl	56(%r12), %eax
	movb	$1, 35(%r14)
	movl	%eax, 8(%r14)
.LBB21_15:                              # %_ZNK8NArchive3N7z11CUpdateItem9HasStreamEv.exit
	movb	65(%r12), %al
	movb	%al, 32(%r15)
	movups	8(%r12), %xmm0
	movups	%xmm0, (%r15)
	movb	66(%r12), %al
	movb	%al, 33(%r15)
	movq	24(%r12), %rax
	movq	%rax, 16(%r15)
	movb	67(%r12), %al
	movb	%al, 34(%r15)
	movb	62(%r12), %al
	movb	%al, 36(%r15)
	movb	$0, 35(%r15)
	movq	32(%r12), %rcx
	movq	%rcx, (%r14)
	movb	63(%r12), %dl
	movb	%dl, 33(%r14)
	orb	%al, %dl
	sete	%al
	testq	%rcx, %rcx
	setne	%cl
	andb	%al, %cl
	movb	%cl, 32(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_16:
.Ltmp607:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_18
# BB#17:
	callq	_ZdaPv
.LBB21_18:                              # %_ZN11CStringBaseIwED2Ev.exit27
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZN8NArchive3N7zL24FromUpdateItemToFileItemERKNS0_11CUpdateItemERNS0_9CFileItemERNS0_10CFileItem2E, .Lfunc_end21-_ZN8NArchive3N7zL24FromUpdateItemToFileItemERKNS0_11CUpdateItemERNS0_9CFileItemERNS0_10CFileItem2E
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp605-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp605
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp605-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp606-.Ltmp605       #   Call between .Ltmp605 and .Ltmp606
	.long	.Ltmp607-.Lfunc_begin6  #     jumps to .Ltmp607
	.byte	0                       #   On action: cleanup
	.long	.Ltmp606-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Lfunc_end21-.Ltmp606   #   Call between .Ltmp606 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive3N7z8CRefItemC2EjRKNS0_11CUpdateItemEb,"axG",@progbits,_ZN8NArchive3N7z8CRefItemC2EjRKNS0_11CUpdateItemEb,comdat
	.weak	_ZN8NArchive3N7z8CRefItemC2EjRKNS0_11CUpdateItemEb
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CRefItemC2EjRKNS0_11CUpdateItemEb,@function
_ZN8NArchive3N7z8CRefItemC2EjRKNS0_11CUpdateItemEb: # @_ZN8NArchive3N7z8CRefItemC2EjRKNS0_11CUpdateItemEb
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi421:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi422:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi423:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi424:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi425:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi426:
	.cfi_def_cfa_offset 80
.Lcfi427:
	.cfi_offset %rbx, -48
.Lcfi428:
	.cfi_offset %r12, -40
.Lcfi429:
	.cfi_offset %r14, -32
.Lcfi430:
	.cfi_offset %r15, -24
.Lcfi431:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	%rdx, (%r12)
	movl	%esi, 8(%r12)
	movl	$0, 12(%r12)
	movl	$0, 16(%r12)
	movl	$0, 20(%r12)
	testb	%cl, %cl
	je	.LBB22_15
# BB#1:
	movslq	48(%rdx), %rax
	testq	%rax, %rax
	je	.LBB22_2
# BB#3:
	leaq	40(%rdx), %rsi
	movq	(%rsi), %rdi
	leaq	(,%rax,4), %rcx
	.p2align	4, 0x90
.LBB22_4:                               # =>This Inner Loop Header: Depth=1
	cmpl	$47, -4(%rdi,%rcx)
	je	.LBB22_5
# BB#6:                                 #   in Loop: Header=BB22_4 Depth=1
	addq	$-4, %rcx
	jne	.LBB22_4
# BB#7:
	movl	$-1, %ecx
	jmp	.LBB22_8
.LBB22_2:                               # %_ZN8NArchive3N7zL18GetReverseSlashPosERK11CStringBaseIwE.exit.thread
	movl	$0, 16(%r12)
	jmp	.LBB22_14
.LBB22_5:
	leaq	-4(%rdi,%rcx), %rcx
	subq	%rdi, %rcx
	shrq	$2, %rcx
.LBB22_8:                               # %_ZN8NArchive3N7zL18GetReverseSlashPosERK11CStringBaseIwE.exit
	leal	1(%rcx), %edi
	xorl	%ebp, %ebp
	testl	%ecx, %ecx
	cmovnsl	%edi, %ebp
	testl	%eax, %eax
	movl	%ebp, 16(%r12)
	je	.LBB22_14
# BB#9:
	movq	(%rsi), %rdi
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB22_10:                              # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rdi,%rax)
	je	.LBB22_12
# BB#11:                                #   in Loop: Header=BB22_10 Depth=1
	addq	$-4, %rax
	jne	.LBB22_10
	jmp	.LBB22_14
.LBB22_12:                              # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	testl	%ecx, %ecx
	setns	%r8b
	leaq	-4(%rdi,%rax), %rax
	subq	%rdi, %rax
	shrq	$2, %rax
	cmpl	%ecx, %eax
	setl	%cl
	testl	%eax, %eax
	js	.LBB22_14
# BB#13:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	andb	%cl, %r8b
	jne	.LBB22_14
# BB#16:
	incl	%eax
	movl	%eax, 12(%r12)
	movl	48(%rdx), %ecx
	subl	%eax, %ecx
	movq	%rsp, %rdi
	movl	%eax, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpl	$0, 8(%rsp)
	je	.LBB22_46
# BB#17:
	movq	(%rsp), %rdi
.Ltmp608:
	callq	_Z13MyStringLowerPw
.Ltmp609:
# BB#18:                                # %_ZN11CStringBaseIwE9MakeLowerEv.exit
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
.Ltmp611:
	movl	$4, %edi
	callq	_Znam
.Ltmp612:
# BB#19:                                # %_ZN11CStringBaseIcEC2Ev.exit
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	$4, 28(%rsp)
	movl	8(%rsp), %ecx
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	testl	%ecx, %ecx
	jle	.LBB22_20
# BB#28:                                # %.lr.ph.preheader
	leaq	16(%rsp), %r14
	.p2align	4, 0x90
.LBB22_29:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax
	movl	(%rax,%rbp,4), %eax
	cmpl	$127, %eax
	jg	.LBB22_20
# BB#30:                                #   in Loop: Header=BB22_29 Depth=1
.Ltmp614:
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp615:
# BB#31:                                #   in Loop: Header=BB22_29 Depth=1
	incq	%rbp
	movslq	8(%rsp), %rcx
	cmpq	%rcx, %rbp
	jl	.LBB22_29
.LBB22_20:                              # %.critedge
	movq	16(%rsp), %rdi
	cmpl	%ecx, %ebp
	jne	.LBB22_44
# BB#21:                                # %.thread45.outer.i.preheader
	movl	$.L.str, %eax
	movl	$1, %r15d
                                        # implicit-def: %R8D
	.p2align	4, 0x90
.LBB22_22:                              # %.thread45.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_25 Depth 2
                                        #     Child Loop BB22_40 Depth 2
	movb	(%rax), %bl
	incq	%rax
	cmpb	$32, %bl
	je	.LBB22_22
# BB#23:                                # %.thread45.i
                                        #   in Loop: Header=BB22_22 Depth=1
	testb	%bl, %bl
	je	.LBB22_44
# BB#24:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB22_22 Depth=1
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB22_25:                              # %.preheader.i
                                        #   Parent Loop BB22_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rsi), %ecx
	testb	%cl, %cl
	je	.LBB22_26
.LBB22_34:                              #   in Loop: Header=BB22_25 Depth=2
	movl	$4, %ebp
	cmpb	%cl, %bl
	jne	.LBB22_36
# BB#35:                                #   in Loop: Header=BB22_25 Depth=2
	movzbl	(%rax), %ebx
	incq	%rax
	xorl	%ebp, %ebp
	jmp	.LBB22_36
.LBB22_26:                              #   in Loop: Header=BB22_25 Depth=2
	movl	$1, %ebp
	movl	%ebx, %edx
	orb	$32, %dl
	cmpb	$32, %dl
	jne	.LBB22_34
# BB#27:                                #   in Loop: Header=BB22_25 Depth=2
	movl	%r15d, %r8d
.LBB22_36:                              #   in Loop: Header=BB22_25 Depth=2
	incq	%rsi
	movl	%ebp, %ecx
	andb	$7, %cl
	je	.LBB22_25
# BB#37:                                #   in Loop: Header=BB22_22 Depth=1
	cmpb	$4, %cl
	jne	.LBB22_42
# BB#38:                                #   in Loop: Header=BB22_22 Depth=1
	incl	%r15d
	testb	%bl, %bl
	jne	.LBB22_40
	jmp	.LBB22_44
.LBB22_41:                              #   in Loop: Header=BB22_40 Depth=2
	movzbl	(%rax), %ebx
	incq	%rax
	testb	%bl, %bl
	je	.LBB22_44
.LBB22_40:                              #   Parent Loop BB22_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$32, %bl
	jne	.LBB22_41
	jmp	.LBB22_22
.LBB22_42:                              #   in Loop: Header=BB22_22 Depth=1
	testl	%ebp, %ebp
	je	.LBB22_22
# BB#43:                                # %_ZN8NArchive3N7z11GetExtIndexEPKc.exit.thread
	movl	%r8d, 20(%r12)
	jmp	.LBB22_45
.LBB22_14:                              # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.thread
	movl	48(%rdx), %eax
	movl	%eax, 12(%r12)
.LBB22_15:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_44:                              # %_ZN8NArchive3N7z11GetExtIndexEPKc.exit
	movl	%r15d, 20(%r12)
	testq	%rdi, %rdi
	je	.LBB22_46
.LBB22_45:
	callq	_ZdaPv
.LBB22_46:                              # %._crit_edge
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB22_15
# BB#47:
	callq	_ZdaPv
	jmp	.LBB22_15
.LBB22_53:
.Ltmp613:
	jmp	.LBB22_49
.LBB22_48:
.Ltmp610:
.LBB22_49:
	movq	%rax, %rbx
	jmp	.LBB22_50
.LBB22_32:
.Ltmp616:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB22_50
# BB#33:
	callq	_ZdaPv
.LBB22_50:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB22_52
# BB#51:
	callq	_ZdaPv
.LBB22_52:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN8NArchive3N7z8CRefItemC2EjRKNS0_11CUpdateItemEb, .Lfunc_end22-_ZN8NArchive3N7z8CRefItemC2EjRKNS0_11CUpdateItemEb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp608-.Lfunc_begin7  #   Call between .Lfunc_begin7 and .Ltmp608
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp608-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp609-.Ltmp608       #   Call between .Ltmp608 and .Ltmp609
	.long	.Ltmp610-.Lfunc_begin7  #     jumps to .Ltmp610
	.byte	0                       #   On action: cleanup
	.long	.Ltmp611-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp612-.Ltmp611       #   Call between .Ltmp611 and .Ltmp612
	.long	.Ltmp613-.Lfunc_begin7  #     jumps to .Ltmp613
	.byte	0                       #   On action: cleanup
	.long	.Ltmp614-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp615-.Ltmp614       #   Call between .Ltmp614 and .Ltmp615
	.long	.Ltmp616-.Lfunc_begin7  #     jumps to .Ltmp616
	.byte	0                       #   On action: cleanup
	.long	.Ltmp615-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Lfunc_end22-.Ltmp615   #   Call between .Ltmp615 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE4SortEPFiPKS2_S5_PvES6_,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE4SortEPFiPKS2_S5_PvES6_,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE4SortEPFiPKS2_S5_PvES6_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE4SortEPFiPKS2_S5_PvES6_,@function
_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE4SortEPFiPKS2_S5_PvES6_: # @_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE4SortEPFiPKS2_S5_PvES6_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi432:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi433:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi434:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi435:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi436:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi437:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi438:
	.cfi_def_cfa_offset 144
.Lcfi439:
	.cfi_offset %rbx, -56
.Lcfi440:
	.cfi_offset %r12, -48
.Lcfi441:
	.cfi_offset %r13, -40
.Lcfi442:
	.cfi_offset %r14, -32
.Lcfi443:
	.cfi_offset %r15, -24
.Lcfi444:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r8
	movslq	12(%rdi), %rsi
	cmpq	$2, %rsi
	jl	.LBB23_20
# BB#1:
	movq	16(%rdi), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	-24(%rax), %r13
	movl	%esi, %edx
	shrl	%edx
	movq	(%rsp), %r9             # 8-byte Reload
	movq	%r8, %r15
	.p2align	4, 0x90
.LBB23_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_5 Depth 2
	leaq	(%rdx,%rdx,2), %rax
	movq	16(%r13,%rax,8), %rcx
	movq	%rcx, 32(%rsp)
	movups	(%r13,%rax,8), %xmm0
	movaps	%xmm0, 16(%rsp)
	leal	(%rdx,%rdx), %ebx
	cmpl	%esi, %ebx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	jle	.LBB23_4
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=1
	movl	%edx, %r14d
	jmp	.LBB23_11
	.p2align	4, 0x90
.LBB23_4:                               # %.lr.ph.i28.preheader
                                        #   in Loop: Header=BB23_2 Depth=1
	movl	%edx, %ebp
	.p2align	4, 0x90
.LBB23_5:                               # %.lr.ph.i28
                                        #   Parent Loop BB23_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %r12
	cmpl	%esi, %ebx
	jge	.LBB23_6
# BB#7:                                 #   in Loop: Header=BB23_5 Depth=2
	movslq	%ebx, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r13,%rax,8), %rsi
	leaq	24(%r13,%rax,8), %rdi
	movq	%r9, %rdx
	callq	*%r15
	movq	(%rsp), %r9             # 8-byte Reload
	movq	%r15, %r8
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setg	%r14b
	orl	%ebx, %r14d
	jmp	.LBB23_8
	.p2align	4, 0x90
.LBB23_6:                               #   in Loop: Header=BB23_5 Depth=2
	movl	%ebx, %r14d
.LBB23_8:                               #   in Loop: Header=BB23_5 Depth=2
	movslq	%r14d, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r13,%rax,8), %rbx
	leaq	16(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r9, %rdx
	callq	*%r8
	testl	%eax, %eax
	jns	.LBB23_9
# BB#10:                                #   in Loop: Header=BB23_5 Depth=2
	movslq	%ebp, %rax
	leaq	(%rax,%rax,2), %rax
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%r13,%rax,8)
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r13,%rax,8)
	leal	(%r14,%r14), %ebx
	movq	%r12, %rsi
	cmpl	%esi, %ebx
	movl	%r14d, %ebp
	movq	%r15, %r8
	movq	(%rsp), %r9             # 8-byte Reload
	jle	.LBB23_5
	jmp	.LBB23_11
	.p2align	4, 0x90
.LBB23_9:                               #   in Loop: Header=BB23_2 Depth=1
	movl	%ebp, %r14d
	movq	%r15, %r8
	movq	%r12, %rsi
	movq	(%rsp), %r9             # 8-byte Reload
.LBB23_11:                              # %_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE11SortRefDownEPS2_iiPFiPKS2_S6_PvES7_.exit32
                                        #   in Loop: Header=BB23_2 Depth=1
	movslq	%r14d, %rax
	leaq	(%rax,%rax,2), %rax
	movq	32(%rsp), %rcx
	movq	%rcx, 16(%r13,%rax,8)
	movaps	16(%rsp), %xmm0
	movups	%xmm0, (%r13,%rax,8)
	movq	8(%rsp), %rdx           # 8-byte Reload
	decq	%rdx
	testl	%edx, %edx
	jne	.LBB23_2
	.p2align	4, 0x90
.LBB23_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_14 Depth 2
	movq	%rsi, %rdi
	leaq	(%rdi,%rdi,2), %rax
	movq	16(%r13,%rax,8), %rcx
	movq	%rcx, 64(%rsp)
	movups	(%r13,%rax,8), %xmm0
	movaps	%xmm0, 48(%rsp)
	leaq	-1(%rdi), %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rcx
	movq	%rcx, 16(%r13,%rax,8)
	movups	(%rdx), %xmm0
	movups	%xmm0, (%r13,%rax,8)
	movq	64(%rsp), %rax
	movq	%rax, 16(%rdx)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, (%rdx)
	movq	64(%rsp), %rax
	movq	%rax, 32(%rsp)
	movaps	48(%rsp), %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$1, %r12d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	cmpq	$3, %rdi
	jl	.LBB23_19
# BB#13:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB23_12 Depth=1
	movl	$1, %r14d
	movl	$2, %eax
	.p2align	4, 0x90
.LBB23_14:                              # %.lr.ph.i
                                        #   Parent Loop BB23_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%eax, %rbx
	movq	%rsi, %rbp
	cmpq	%rsi, %rbx
	movl	%eax, %r12d
	jge	.LBB23_16
# BB#15:                                #   in Loop: Header=BB23_14 Depth=2
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r13,%rax,8), %rsi
	leaq	24(%r13,%rax,8), %rdi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	*%r15
	movq	%r15, %r8
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setg	%cl
	orl	%ecx, %ebx
	movl	%ebx, %r12d
.LBB23_16:                              #   in Loop: Header=BB23_14 Depth=2
	movslq	%r12d, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r13,%rax,8), %rbx
	leaq	16(%rsp), %rdi
	movq	%rbx, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	*%r8
	testl	%eax, %eax
	jns	.LBB23_17
# BB#18:                                #   in Loop: Header=BB23_14 Depth=2
	movslq	%r14d, %rax
	leaq	(%rax,%rax,2), %rax
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%r13,%rax,8)
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r13,%rax,8)
	leal	(%r12,%r12), %eax
	movslq	%eax, %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	movl	%r12d, %r14d
	movq	%r15, %r8
	movq	%rbp, %rsi
	jl	.LBB23_14
	jmp	.LBB23_19
	.p2align	4, 0x90
.LBB23_17:                              #   in Loop: Header=BB23_12 Depth=1
	movl	%r14d, %r12d
	movq	%r15, %r8
	movq	%rbp, %rsi
.LBB23_19:                              # %_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE11SortRefDownEPS2_iiPFiPKS2_S6_PvES7_.exit
                                        #   in Loop: Header=BB23_12 Depth=1
	movslq	%r12d, %rax
	leaq	(%rax,%rax,2), %rax
	movq	32(%rsp), %rcx
	movq	%rcx, 16(%r13,%rax,8)
	movaps	16(%rsp), %xmm0
	movups	%xmm0, (%r13,%rax,8)
	cmpq	$1, %rsi
	jg	.LBB23_12
.LBB23_20:                              # %.loopexit
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE4SortEPFiPKS2_S5_PvES6_, .Lfunc_end23-_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEE4SortEPFiPKS2_S5_PvES6_
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7zL18CompareUpdateItemsEPKNS0_8CRefItemES3_Pv,@function
_ZN8NArchive3N7zL18CompareUpdateItemsEPKNS0_8CRefItemES3_Pv: # @_ZN8NArchive3N7zL18CompareUpdateItemsEPKNS0_8CRefItemES3_Pv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi445:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi446:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi447:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi448:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi449:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi450:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi451:
	.cfi_def_cfa_offset 64
.Lcfi452:
	.cfi_offset %rbx, -56
.Lcfi453:
	.cfi_offset %r12, -48
.Lcfi454:
	.cfi_offset %r13, -40
.Lcfi455:
	.cfi_offset %r14, -32
.Lcfi456:
	.cfi_offset %r15, -24
.Lcfi457:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %r12
	movq	(%r14), %r15
	movb	63(%r12), %al
	cmpb	63(%r15), %al
	jne	.LBB24_1
# BB#2:
	testb	%al, %al
	je	.LBB24_5
# BB#3:
	movb	62(%r12), %al
	cmpb	62(%r15), %al
	jne	.LBB24_1
# BB#4:
	movq	40(%r12), %rdi
	movq	40(%r15), %rsi
	callq	_Z21MyStringCompareNoCasePKwS0_
	negl	%eax
	jmp	.LBB24_17
.LBB24_1:
	cmpb	$1, %al
	sbbl	%eax, %eax
	orl	$1, %eax
.LBB24_17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_5:
	cmpb	$0, (%rdx)
	je	.LBB24_6
# BB#8:
	movl	20(%rbx), %eax
	xorl	%ecx, %ecx
	cmpl	20(%r14), %eax
	setne	%cl
	movl	$-1, %eax
	cmovgel	%ecx, %eax
	testl	%eax, %eax
	jne	.LBB24_17
# BB#9:
	movl	12(%rbx), %edi
	shlq	$2, %rdi
	addq	40(%r12), %rdi
	movq	40(%r15), %rax
	movl	12(%r14), %ecx
	leaq	(%rax,%rcx,4), %rsi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	jne	.LBB24_17
# BB#10:
	leaq	40(%r12), %rbp
	leaq	40(%r15), %r13
	movl	16(%rbx), %edi
	shlq	$2, %rdi
	addq	(%rbp), %rdi
	movq	(%r13), %rax
	movl	16(%r14), %ecx
	leaq	(%rax,%rcx,4), %rsi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	jne	.LBB24_17
# BB#11:
	movb	67(%r15), %cl
	cmpb	$0, 67(%r12)
	je	.LBB24_12
# BB#13:                                # %.thread
	testb	%cl, %cl
	je	.LBB24_14
# BB#15:
	movq	24(%r12), %rax
	xorl	%ecx, %ecx
	cmpq	24(%r15), %rax
	setne	%cl
	movl	$-1, %eax
	cmovael	%ecx, %eax
	testl	%eax, %eax
	jne	.LBB24_17
	jmp	.LBB24_16
.LBB24_6:                               # %._crit_edge
	addq	$40, %r12
	addq	$40, %r15
	movq	%r15, %r13
	movq	%r12, %rbp
.LBB24_7:
	movq	(%rbp), %rdi
	movq	(%r13), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_Z21MyStringCompareNoCasePKwS0_ # TAILCALL
.LBB24_12:
	movl	$1, %eax
	testb	%cl, %cl
	jne	.LBB24_17
.LBB24_16:                              # %.thread87
	movq	32(%r12), %rax
	xorl	%ecx, %ecx
	cmpq	32(%r15), %rax
	setne	%cl
	movl	$-1, %eax
	cmovael	%ecx, %eax
	testl	%eax, %eax
	jne	.LBB24_17
	jmp	.LBB24_7
.LBB24_14:
	movl	$-1, %eax
	jmp	.LBB24_17
.Lfunc_end24:
	.size	_ZN8NArchive3N7zL18CompareUpdateItemsEPKNS0_8CRefItemES3_Pv, .Lfunc_end24-_ZN8NArchive3N7zL18CompareUpdateItemsEPKNS0_8CRefItemES3_Pv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z22CCompressionMethodModeD2Ev,"axG",@progbits,_ZN8NArchive3N7z22CCompressionMethodModeD2Ev,comdat
	.weak	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev,@function
_ZN8NArchive3N7z22CCompressionMethodModeD2Ev: # @_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi458:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi459:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi460:
	.cfi_def_cfa_offset 32
.Lcfi461:
	.cfi_offset %rbx, -24
.Lcfi462:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_2
# BB#1:
	callq	_ZdaPv
.LBB25_2:                               # %_ZN11CStringBaseIwED2Ev.exit
	leaq	32(%rbx), %rdi
.Ltmp617:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp618:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp629:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp630:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB25_5:
.Ltmp631:
	movq	%rax, %r14
.Ltmp632:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp633:
	jmp	.LBB25_6
.LBB25_7:
.Ltmp634:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB25_8:
.Ltmp619:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp620:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp621:
# BB#9:
.Ltmp626:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp627:
.LBB25_6:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB25_12:
.Ltmp628:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB25_10:
.Ltmp622:
	movq	%rax, %r14
.Ltmp623:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp624:
# BB#13:                                # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB25_11:
.Ltmp625:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev, .Lfunc_end25-_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp617-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp618-.Ltmp617       #   Call between .Ltmp617 and .Ltmp618
	.long	.Ltmp619-.Lfunc_begin8  #     jumps to .Ltmp619
	.byte	0                       #   On action: cleanup
	.long	.Ltmp629-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp630-.Ltmp629       #   Call between .Ltmp629 and .Ltmp630
	.long	.Ltmp631-.Lfunc_begin8  #     jumps to .Ltmp631
	.byte	0                       #   On action: cleanup
	.long	.Ltmp630-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp632-.Ltmp630       #   Call between .Ltmp630 and .Ltmp632
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp632-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp633-.Ltmp632       #   Call between .Ltmp632 and .Ltmp633
	.long	.Ltmp634-.Lfunc_begin8  #     jumps to .Ltmp634
	.byte	1                       #   On action: 1
	.long	.Ltmp620-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp621-.Ltmp620       #   Call between .Ltmp620 and .Ltmp621
	.long	.Ltmp622-.Lfunc_begin8  #     jumps to .Ltmp622
	.byte	1                       #   On action: 1
	.long	.Ltmp626-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp627-.Ltmp626       #   Call between .Ltmp626 and .Ltmp627
	.long	.Ltmp628-.Lfunc_begin8  #     jumps to .Ltmp628
	.byte	1                       #   On action: 1
	.long	.Ltmp627-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp623-.Ltmp627       #   Call between .Ltmp627 and .Ltmp623
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp623-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp624-.Ltmp623       #   Call between .Ltmp623 and .Ltmp624
	.long	.Ltmp625-.Lfunc_begin8  #     jumps to .Ltmp625
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIiE4SortEPFiPKiS2_PvES3_,"axG",@progbits,_ZN13CRecordVectorIiE4SortEPFiPKiS2_PvES3_,comdat
	.weak	_ZN13CRecordVectorIiE4SortEPFiPKiS2_PvES3_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiE4SortEPFiPKiS2_PvES3_,@function
_ZN13CRecordVectorIiE4SortEPFiPKiS2_PvES3_: # @_ZN13CRecordVectorIiE4SortEPFiPKiS2_PvES3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi463:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi464:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi465:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi466:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi467:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi468:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi469:
	.cfi_def_cfa_offset 96
.Lcfi470:
	.cfi_offset %rbx, -56
.Lcfi471:
	.cfi_offset %r12, -48
.Lcfi472:
	.cfi_offset %r13, -40
.Lcfi473:
	.cfi_offset %r14, -32
.Lcfi474:
	.cfi_offset %r15, -24
.Lcfi475:
	.cfi_offset %rbp, -16
	movq	%rsi, %r8
	movslq	12(%rdi), %rsi
	cmpq	$2, %rsi
	jl	.LBB26_22
# BB#1:
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	16(%rdi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-4(%rax), %r12
	movl	%esi, %edx
	shrl	%edx
	movq	%r8, (%rsp)             # 8-byte Spill
	.p2align	4, 0x90
.LBB26_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_5 Depth 2
	movl	(%r12,%rdx,4), %eax
	movl	%eax, 12(%rsp)
	leal	(%rdx,%rdx), %ebx
	cmpl	%esi, %ebx
	jle	.LBB26_4
# BB#3:                                 #   in Loop: Header=BB26_2 Depth=1
	movl	%edx, %r14d
	jmp	.LBB26_12
	.p2align	4, 0x90
.LBB26_4:                               # %.lr.ph.i29.preheader
                                        #   in Loop: Header=BB26_2 Depth=1
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r15d
	.p2align	4, 0x90
.LBB26_5:                               # %.lr.ph.i29
                                        #   Parent Loop BB26_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %r13
	cmpl	%esi, %ebx
	jge	.LBB26_6
# BB#7:                                 #   in Loop: Header=BB26_5 Depth=2
	movslq	%ebx, %rax
	leaq	(%r12,%rax,4), %rsi
	leaq	4(%r12,%rax,4), %rdi
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	callq	*(%rsp)                 # 8-byte Folded Reload
	movq	(%rsp), %r8             # 8-byte Reload
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setg	%r14b
	orl	%ebx, %r14d
	jmp	.LBB26_8
	.p2align	4, 0x90
.LBB26_6:                               #   in Loop: Header=BB26_5 Depth=2
	movl	%ebx, %r14d
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB26_8:                               #   in Loop: Header=BB26_5 Depth=2
	movslq	%r14d, %rax
	leaq	(%r12,%rax,4), %rbx
	leaq	12(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	*%r8
	testl	%eax, %eax
	jns	.LBB26_9
# BB#10:                                #   in Loop: Header=BB26_5 Depth=2
	movl	(%rbx), %eax
	movslq	%r15d, %rcx
	movl	%eax, (%r12,%rcx,4)
	leal	(%r14,%r14), %ebx
	movq	%r13, %rsi
	cmpl	%esi, %ebx
	movl	%r14d, %r15d
	movq	(%rsp), %r8             # 8-byte Reload
	jle	.LBB26_5
	jmp	.LBB26_11
	.p2align	4, 0x90
.LBB26_9:                               #   in Loop: Header=BB26_2 Depth=1
	movl	%r15d, %r14d
	movq	(%rsp), %r8             # 8-byte Reload
	movq	%r13, %rsi
.LBB26_11:                              # %._crit_edge.loopexit.i34
                                        #   in Loop: Header=BB26_2 Depth=1
	movl	12(%rsp), %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB26_12:                              # %_ZN13CRecordVectorIiE11SortRefDownEPiiiPFiPKiS3_PvES4_.exit36
                                        #   in Loop: Header=BB26_2 Depth=1
	movslq	%r14d, %rcx
	movl	%eax, (%r12,%rcx,4)
	decq	%rdx
	testl	%edx, %edx
	jne	.LBB26_2
# BB#13:                                # %.preheader
	movq	24(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB26_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_16 Depth 2
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	-4(%rdx,%rsi,4), %eax
	movl	(%rdx), %ecx
	movl	%ecx, -4(%rdx,%rsi,4)
	movl	%eax, (%rdx)
	movl	%eax, 8(%rsp)
	cmpq	$3, %rsi
	jl	.LBB26_23
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB26_14 Depth=1
	leaq	-1(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r15d
	movl	$2, %eax
	.p2align	4, 0x90
.LBB26_16:                              # %.lr.ph.i
                                        #   Parent Loop BB26_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %r13
	movslq	%eax, %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	movl	%eax, %ebp
	jge	.LBB26_18
# BB#17:                                #   in Loop: Header=BB26_16 Depth=2
	leaq	(%r12,%rbx,4), %rsi
	leaq	4(%r12,%rbx,4), %rdi
	movq	%r14, %rdx
	callq	*(%rsp)                 # 8-byte Folded Reload
	movq	(%rsp), %r8             # 8-byte Reload
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setg	%cl
	orl	%ecx, %ebx
	movl	%ebx, %ebp
.LBB26_18:                              #   in Loop: Header=BB26_16 Depth=2
	movslq	%ebp, %rax
	leaq	(%r12,%rax,4), %rbx
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	*%r8
	testl	%eax, %eax
	jns	.LBB26_19
# BB#20:                                #   in Loop: Header=BB26_16 Depth=2
	movl	(%rbx), %eax
	movslq	%r15d, %rcx
	movl	%eax, (%r12,%rcx,4)
	leal	(%rbp,%rbp), %eax
	movslq	%eax, %rcx
	movq	%r13, %rsi
	cmpq	%rsi, %rcx
	movl	%ebp, %r15d
	movq	(%rsp), %r8             # 8-byte Reload
	jl	.LBB26_16
	jmp	.LBB26_21
	.p2align	4, 0x90
.LBB26_19:                              #   in Loop: Header=BB26_14 Depth=1
	movl	%r15d, %ebp
	movq	(%rsp), %r8             # 8-byte Reload
.LBB26_21:                              # %_ZN13CRecordVectorIiE11SortRefDownEPiiiPFiPKiS3_PvES4_.exit
                                        #   in Loop: Header=BB26_14 Depth=1
	movl	8(%rsp), %eax
	movslq	%ebp, %rcx
	movl	%eax, (%r12,%rcx,4)
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$1, %rax
	movq	%rax, %rsi
	jg	.LBB26_14
	jmp	.LBB26_22
.LBB26_23:                              # %_ZN13CRecordVectorIiE11SortRefDownEPiiiPFiPKiS3_PvES4_.exit.thread
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB26_22:                              # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	_ZN13CRecordVectorIiE4SortEPFiPKiS2_PvES3_, .Lfunc_end26-_ZN13CRecordVectorIiE4SortEPFiPKiS2_PvES3_
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7zL17CompareEmptyItemsEPKiS2_Pv,@function
_ZN8NArchive3N7zL17CompareEmptyItemsEPKiS2_Pv: # @_ZN8NArchive3N7zL17CompareEmptyItemsEPKiS2_Pv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi476:
	.cfi_def_cfa_offset 16
	movq	16(%rdx), %rcx
	movslq	(%rdi), %rax
	movq	(%rcx,%rax,8), %rax
	movslq	(%rsi), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movb	63(%rax), %dl
	cmpb	63(%rcx), %dl
	jne	.LBB27_1
# BB#3:
	movb	62(%rax), %sil
	movb	62(%rcx), %dil
	testb	%dl, %dl
	je	.LBB27_6
# BB#4:
	cmpb	%dil, %sil
	jne	.LBB27_5
# BB#8:
	movq	40(%rax), %rdi
	movq	40(%rcx), %rsi
	callq	_Z21MyStringCompareNoCasePKwS0_
	negl	%eax
	popq	%rcx
	retq
.LBB27_1:
	cmpb	$1, %dl
	jmp	.LBB27_2
.LBB27_6:
	cmpb	%dil, %sil
	je	.LBB27_7
.LBB27_5:
	cmpb	$1, %sil
.LBB27_2:
	sbbl	%eax, %eax
	orl	$1, %eax
	popq	%rcx
	retq
.LBB27_7:
	movq	40(%rax), %rdi
	movq	40(%rcx), %rsi
	popq	%rax
	jmp	_Z21MyStringCompareNoCasePKwS0_ # TAILCALL
.Lfunc_end27:
	.size	_ZN8NArchive3N7zL17CompareEmptyItemsEPKiS2_Pv, .Lfunc_end27-_ZN8NArchive3N7zL17CompareEmptyItemsEPKiS2_Pv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z16CArchiveDatabase11ReserveDownEv,"axG",@progbits,_ZN8NArchive3N7z16CArchiveDatabase11ReserveDownEv,comdat
	.weak	_ZN8NArchive3N7z16CArchiveDatabase11ReserveDownEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CArchiveDatabase11ReserveDownEv,@function
_ZN8NArchive3N7z16CArchiveDatabase11ReserveDownEv: # @_ZN8NArchive3N7z16CArchiveDatabase11ReserveDownEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi477:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi478:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi479:
	.cfi_def_cfa_offset 32
.Lcfi480:
	.cfi_offset %rbx, -24
.Lcfi481:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	32(%rbx), %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	64(%rbx), %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	96(%rbx), %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	movq	%rbx, %rdi
	subq	$-128, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	160(%rbx), %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	192(%rbx), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	256(%rbx), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	320(%rbx), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	384(%rbx), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	addq	$448, %rbx              # imm = 0x1C0
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVector11ReserveDownEv # TAILCALL
.Lfunc_end28:
	.size	_ZN8NArchive3N7z16CArchiveDatabase11ReserveDownEv, .Lfunc_end28-_ZN8NArchive3N7z16CArchiveDatabase11ReserveDownEv
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED2Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi482:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi483:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi484:
	.cfi_def_cfa_offset 32
.Lcfi485:
	.cfi_offset %rbx, -24
.Lcfi486:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE+16, (%rbx)
.Ltmp635:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp636:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB29_2:
.Ltmp637:
	movq	%rax, %r14
.Ltmp638:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp639:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB29_4:
.Ltmp640:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end29:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED2Ev, .Lfunc_end29-_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp635-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp636-.Ltmp635       #   Call between .Ltmp635 and .Ltmp636
	.long	.Ltmp637-.Lfunc_begin9  #     jumps to .Ltmp637
	.byte	0                       #   On action: cleanup
	.long	.Ltmp636-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp638-.Ltmp636       #   Call between .Ltmp636 and .Ltmp638
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp638-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp639-.Ltmp638       #   Call between .Ltmp638 and .Ltmp639
	.long	.Ltmp640-.Lfunc_begin9  #     jumps to .Ltmp640
	.byte	1                       #   On action: 1
	.long	.Ltmp639-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end29-.Ltmp639   #   Call between .Ltmp639 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z14CThreadDecoderD2Ev,"axG",@progbits,_ZN8NArchive3N7z14CThreadDecoderD2Ev,comdat
	.weak	_ZN8NArchive3N7z14CThreadDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z14CThreadDecoderD2Ev,@function
_ZN8NArchive3N7z14CThreadDecoderD2Ev:   # @_ZN8NArchive3N7z14CThreadDecoderD2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi487:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi488:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi489:
	.cfi_def_cfa_offset 32
.Lcfi490:
	.cfi_offset %rbx, -24
.Lcfi491:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive3N7z14CThreadDecoderE+16, (%rbx)
	leaq	296(%rbx), %rdi
.Ltmp641:
	callq	_ZN8NArchive3N7z8CDecoderD2Ev
.Ltmp642:
# BB#1:
	movq	288(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB30_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp646:
	callq	*16(%rax)
.Ltmp647:
.LBB30_3:                               # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB30_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp651:
	callq	*16(%rax)
.Ltmp652:
.LBB30_5:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB30_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp656:
	callq	*16(%rax)
.Ltmp657:
.LBB30_7:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN11CVirtThreadD2Ev    # TAILCALL
.LBB30_13:
.Ltmp658:
	movq	%rax, %r14
	jmp	.LBB30_17
.LBB30_14:
.Ltmp653:
	movq	%rax, %r14
	jmp	.LBB30_15
.LBB30_10:
.Ltmp648:
	movq	%rax, %r14
	jmp	.LBB30_11
.LBB30_8:
.Ltmp643:
	movq	%rax, %r14
	movq	288(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB30_11
# BB#9:
	movq	(%rdi), %rax
.Ltmp644:
	callq	*16(%rax)
.Ltmp645:
.LBB30_11:                              # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit8
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB30_15
# BB#12:
	movq	(%rdi), %rax
.Ltmp649:
	callq	*16(%rax)
.Ltmp650:
.LBB30_15:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit10
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB30_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp654:
	callq	*16(%rax)
.Ltmp655:
.LBB30_17:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit12
.Ltmp659:
	movq	%rbx, %rdi
	callq	_ZN11CVirtThreadD2Ev
.Ltmp660:
# BB#18:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB30_19:
.Ltmp661:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end30:
	.size	_ZN8NArchive3N7z14CThreadDecoderD2Ev, .Lfunc_end30-_ZN8NArchive3N7z14CThreadDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp641-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp642-.Ltmp641       #   Call between .Ltmp641 and .Ltmp642
	.long	.Ltmp643-.Lfunc_begin10 #     jumps to .Ltmp643
	.byte	0                       #   On action: cleanup
	.long	.Ltmp646-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp647-.Ltmp646       #   Call between .Ltmp646 and .Ltmp647
	.long	.Ltmp648-.Lfunc_begin10 #     jumps to .Ltmp648
	.byte	0                       #   On action: cleanup
	.long	.Ltmp651-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp652-.Ltmp651       #   Call between .Ltmp651 and .Ltmp652
	.long	.Ltmp653-.Lfunc_begin10 #     jumps to .Ltmp653
	.byte	0                       #   On action: cleanup
	.long	.Ltmp656-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp657-.Ltmp656       #   Call between .Ltmp656 and .Ltmp657
	.long	.Ltmp658-.Lfunc_begin10 #     jumps to .Ltmp658
	.byte	0                       #   On action: cleanup
	.long	.Ltmp657-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp644-.Ltmp657       #   Call between .Ltmp657 and .Ltmp644
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp644-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp660-.Ltmp644       #   Call between .Ltmp644 and .Ltmp660
	.long	.Ltmp661-.Lfunc_begin10 #     jumps to .Ltmp661
	.byte	1                       #   On action: 1
	.long	.Ltmp660-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Lfunc_end30-.Ltmp660   #   Call between .Ltmp660 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z17CFolderOutStream214QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive3N7z17CFolderOutStream214QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive3N7z17CFolderOutStream214QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream214QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive3N7z17CFolderOutStream214QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive3N7z17CFolderOutStream214QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi492:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB31_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB31_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB31_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB31_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB31_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB31_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB31_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB31_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB31_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB31_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB31_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB31_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB31_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB31_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB31_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB31_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB31_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZN8NArchive3N7z17CFolderOutStream214QueryInterfaceERK4GUIDPPv, .Lfunc_end31-_ZN8NArchive3N7z17CFolderOutStream214QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z17CFolderOutStream26AddRefEv,"axG",@progbits,_ZN8NArchive3N7z17CFolderOutStream26AddRefEv,comdat
	.weak	_ZN8NArchive3N7z17CFolderOutStream26AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream26AddRefEv,@function
_ZN8NArchive3N7z17CFolderOutStream26AddRefEv: # @_ZN8NArchive3N7z17CFolderOutStream26AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end32:
	.size	_ZN8NArchive3N7z17CFolderOutStream26AddRefEv, .Lfunc_end32-_ZN8NArchive3N7z17CFolderOutStream26AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z17CFolderOutStream27ReleaseEv,"axG",@progbits,_ZN8NArchive3N7z17CFolderOutStream27ReleaseEv,comdat
	.weak	_ZN8NArchive3N7z17CFolderOutStream27ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream27ReleaseEv,@function
_ZN8NArchive3N7z17CFolderOutStream27ReleaseEv: # @_ZN8NArchive3N7z17CFolderOutStream27ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi493:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB33_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB33_2:
	popq	%rcx
	retq
.Lfunc_end33:
	.size	_ZN8NArchive3N7z17CFolderOutStream27ReleaseEv, .Lfunc_end33-_ZN8NArchive3N7z17CFolderOutStream27ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z17CFolderOutStream2D2Ev,"axG",@progbits,_ZN8NArchive3N7z17CFolderOutStream2D2Ev,comdat
	.weak	_ZN8NArchive3N7z17CFolderOutStream2D2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream2D2Ev,@function
_ZN8NArchive3N7z17CFolderOutStream2D2Ev: # @_ZN8NArchive3N7z17CFolderOutStream2D2Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi494:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi495:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi496:
	.cfi_def_cfa_offset 32
.Lcfi497:
	.cfi_offset %rbx, -24
.Lcfi498:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive3N7z17CFolderOutStream2E+16, (%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB34_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp662:
	callq	*16(%rax)
.Ltmp663:
.LBB34_2:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB34_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp668:
	callq	*16(%rax)
.Ltmp669:
.LBB34_4:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit4
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB34_7:
.Ltmp670:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB34_5:
.Ltmp664:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB34_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp665:
	callq	*16(%rax)
.Ltmp666:
.LBB34_8:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit6
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB34_9:
.Ltmp667:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end34:
	.size	_ZN8NArchive3N7z17CFolderOutStream2D2Ev, .Lfunc_end34-_ZN8NArchive3N7z17CFolderOutStream2D2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp662-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp663-.Ltmp662       #   Call between .Ltmp662 and .Ltmp663
	.long	.Ltmp664-.Lfunc_begin11 #     jumps to .Ltmp664
	.byte	0                       #   On action: cleanup
	.long	.Ltmp668-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp669-.Ltmp668       #   Call between .Ltmp668 and .Ltmp669
	.long	.Ltmp670-.Lfunc_begin11 #     jumps to .Ltmp670
	.byte	0                       #   On action: cleanup
	.long	.Ltmp669-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp665-.Ltmp669       #   Call between .Ltmp669 and .Ltmp665
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp665-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp666-.Ltmp665       #   Call between .Ltmp665 and .Ltmp666
	.long	.Ltmp667-.Lfunc_begin11 #     jumps to .Ltmp667
	.byte	1                       #   On action: 1
	.long	.Ltmp666-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Lfunc_end34-.Ltmp666   #   Call between .Ltmp666 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z17CFolderOutStream2D0Ev,"axG",@progbits,_ZN8NArchive3N7z17CFolderOutStream2D0Ev,comdat
	.weak	_ZN8NArchive3N7z17CFolderOutStream2D0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z17CFolderOutStream2D0Ev,@function
_ZN8NArchive3N7z17CFolderOutStream2D0Ev: # @_ZN8NArchive3N7z17CFolderOutStream2D0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi499:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi500:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi501:
	.cfi_def_cfa_offset 32
.Lcfi502:
	.cfi_offset %rbx, -24
.Lcfi503:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive3N7z17CFolderOutStream2E+16, (%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp671:
	callq	*16(%rax)
.Ltmp672:
.LBB35_2:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit.i
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp677:
	callq	*16(%rax)
.Ltmp678:
.LBB35_4:                               # %_ZN8NArchive3N7z17CFolderOutStream2D2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB35_8:
.Ltmp679:
	movq	%rax, %r14
	jmp	.LBB35_9
.LBB35_5:
.Ltmp673:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp674:
	callq	*16(%rax)
.Ltmp675:
.LBB35_9:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB35_7:
.Ltmp676:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN8NArchive3N7z17CFolderOutStream2D0Ev, .Lfunc_end35-_ZN8NArchive3N7z17CFolderOutStream2D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp671-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp672-.Ltmp671       #   Call between .Ltmp671 and .Ltmp672
	.long	.Ltmp673-.Lfunc_begin12 #     jumps to .Ltmp673
	.byte	0                       #   On action: cleanup
	.long	.Ltmp677-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp678-.Ltmp677       #   Call between .Ltmp677 and .Ltmp678
	.long	.Ltmp679-.Lfunc_begin12 #     jumps to .Ltmp679
	.byte	0                       #   On action: cleanup
	.long	.Ltmp674-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp675-.Ltmp674       #   Call between .Ltmp674 and .Ltmp675
	.long	.Ltmp676-.Lfunc_begin12 #     jumps to .Ltmp676
	.byte	1                       #   On action: 1
	.long	.Ltmp675-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Lfunc_end35-.Ltmp675   #   Call between .Ltmp675 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z22CCryptoGetTextPassword14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive3N7z22CCryptoGetTextPassword14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive3N7z22CCryptoGetTextPassword14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCryptoGetTextPassword14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive3N7z22CCryptoGetTextPassword14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive3N7z22CCryptoGetTextPassword14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi504:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB36_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB36_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB36_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB36_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB36_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB36_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB36_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB36_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB36_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB36_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB36_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB36_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB36_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB36_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB36_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB36_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB36_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end36:
	.size	_ZN8NArchive3N7z22CCryptoGetTextPassword14QueryInterfaceERK4GUIDPPv, .Lfunc_end36-_ZN8NArchive3N7z22CCryptoGetTextPassword14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z22CCryptoGetTextPassword6AddRefEv,"axG",@progbits,_ZN8NArchive3N7z22CCryptoGetTextPassword6AddRefEv,comdat
	.weak	_ZN8NArchive3N7z22CCryptoGetTextPassword6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCryptoGetTextPassword6AddRefEv,@function
_ZN8NArchive3N7z22CCryptoGetTextPassword6AddRefEv: # @_ZN8NArchive3N7z22CCryptoGetTextPassword6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end37:
	.size	_ZN8NArchive3N7z22CCryptoGetTextPassword6AddRefEv, .Lfunc_end37-_ZN8NArchive3N7z22CCryptoGetTextPassword6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z22CCryptoGetTextPassword7ReleaseEv,"axG",@progbits,_ZN8NArchive3N7z22CCryptoGetTextPassword7ReleaseEv,comdat
	.weak	_ZN8NArchive3N7z22CCryptoGetTextPassword7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCryptoGetTextPassword7ReleaseEv,@function
_ZN8NArchive3N7z22CCryptoGetTextPassword7ReleaseEv: # @_ZN8NArchive3N7z22CCryptoGetTextPassword7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi505:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB38_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB38_2:
	popq	%rcx
	retq
.Lfunc_end38:
	.size	_ZN8NArchive3N7z22CCryptoGetTextPassword7ReleaseEv, .Lfunc_end38-_ZN8NArchive3N7z22CCryptoGetTextPassword7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z22CCryptoGetTextPasswordD2Ev,"axG",@progbits,_ZN8NArchive3N7z22CCryptoGetTextPasswordD2Ev,comdat
	.weak	_ZN8NArchive3N7z22CCryptoGetTextPasswordD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCryptoGetTextPasswordD2Ev,@function
_ZN8NArchive3N7z22CCryptoGetTextPasswordD2Ev: # @_ZN8NArchive3N7z22CCryptoGetTextPasswordD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTVN8NArchive3N7z22CCryptoGetTextPasswordE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB39_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB39_1:                               # %_ZN11CStringBaseIwED2Ev.exit
	retq
.Lfunc_end39:
	.size	_ZN8NArchive3N7z22CCryptoGetTextPasswordD2Ev, .Lfunc_end39-_ZN8NArchive3N7z22CCryptoGetTextPasswordD2Ev
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z22CCryptoGetTextPasswordD0Ev,"axG",@progbits,_ZN8NArchive3N7z22CCryptoGetTextPasswordD0Ev,comdat
	.weak	_ZN8NArchive3N7z22CCryptoGetTextPasswordD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCryptoGetTextPasswordD0Ev,@function
_ZN8NArchive3N7z22CCryptoGetTextPasswordD0Ev: # @_ZN8NArchive3N7z22CCryptoGetTextPasswordD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi506:
	.cfi_def_cfa_offset 16
.Lcfi507:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive3N7z22CCryptoGetTextPasswordE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_2
# BB#1:
	callq	_ZdaPv
.LBB40_2:                               # %_ZN8NArchive3N7z22CCryptoGetTextPasswordD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end40:
	.size	_ZN8NArchive3N7z22CCryptoGetTextPasswordD0Ev, .Lfunc_end40-_ZN8NArchive3N7z22CCryptoGetTextPasswordD0Ev
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z8CDecoderD2Ev,"axG",@progbits,_ZN8NArchive3N7z8CDecoderD2Ev,comdat
	.weak	_ZN8NArchive3N7z8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CDecoderD2Ev,@function
_ZN8NArchive3N7z8CDecoderD2Ev:          # @_ZN8NArchive3N7z8CDecoderD2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r15
.Lcfi508:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi509:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi510:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi511:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi512:
	.cfi_def_cfa_offset 48
.Lcfi513:
	.cfi_offset %rbx, -40
.Lcfi514:
	.cfi_offset %r12, -32
.Lcfi515:
	.cfi_offset %r14, -24
.Lcfi516:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	leaq	200(%r12), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, 200(%r12)
.Ltmp680:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp681:
# BB#1:
.Ltmp686:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp687:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev.exit
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB41_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp691:
	callq	*16(%rax)
.Ltmp692:
.LBB41_4:                               # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	leaq	8(%r12), %r15
	leaq	136(%r12), %rdi
.Ltmp735:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp736:
# BB#5:                                 # %_ZN8NArchive3N7z11CBindInfoExD2Ev.exit
	leaq	104(%r12), %rdi
.Ltmp757:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp758:
# BB#6:
	leaq	72(%r12), %rdi
.Ltmp762:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp763:
# BB#7:
	addq	$40, %r12
.Ltmp767:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp768:
# BB#8:                                 # %_ZN11NCoderMixer9CBindInfoD2Ev.exit
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB41_33:
.Ltmp693:
	movq	%rax, %r14
	jmp	.LBB41_34
.LBB41_25:
.Ltmp769:
	movq	%rax, %r14
	jmp	.LBB41_28
.LBB41_26:
.Ltmp764:
	movq	%rax, %r14
	jmp	.LBB41_27
.LBB41_24:
.Ltmp759:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp760:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp761:
.LBB41_27:
	addq	$40, %r12
.Ltmp765:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp766:
.LBB41_28:
.Ltmp770:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp771:
	jmp	.LBB41_15
.LBB41_29:
.Ltmp772:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_11:
.Ltmp737:
	movq	%rax, %r14
	leaq	104(%r12), %rdi
.Ltmp738:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp739:
# BB#12:
	leaq	72(%r12), %rdi
.Ltmp743:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp744:
# BB#13:
	addq	$40, %r12
.Ltmp748:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp749:
# BB#14:
.Ltmp754:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp755:
	jmp	.LBB41_15
.LBB41_22:
.Ltmp756:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_17:
.Ltmp750:
	movq	%rax, %r14
	jmp	.LBB41_20
.LBB41_18:
.Ltmp745:
	movq	%rax, %r14
	jmp	.LBB41_19
.LBB41_16:
.Ltmp740:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp741:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp742:
.LBB41_19:
	addq	$40, %r12
.Ltmp746:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp747:
.LBB41_20:
.Ltmp751:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp752:
# BB#23:                                # %.body14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_21:
.Ltmp753:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_30:
.Ltmp688:
	movq	%rax, %r14
	jmp	.LBB41_31
.LBB41_9:
.Ltmp682:
	movq	%rax, %r14
.Ltmp683:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp684:
.LBB41_31:                              # %.body
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB41_34
# BB#32:
	movq	(%rdi), %rax
.Ltmp689:
	callq	*16(%rax)
.Ltmp690:
.LBB41_34:                              # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit4
	leaq	8(%r12), %r15
	leaq	136(%r12), %rdi
.Ltmp694:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp695:
# BB#35:
	leaq	104(%r12), %rdi
.Ltmp716:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp717:
# BB#36:
	leaq	72(%r12), %rdi
.Ltmp721:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp722:
# BB#37:
	addq	$40, %r12
.Ltmp726:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp727:
# BB#38:
.Ltmp732:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp733:
.LBB41_15:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB41_10:
.Ltmp685:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_40:
.Ltmp728:
	movq	%rax, %r14
	jmp	.LBB41_43
.LBB41_41:
.Ltmp723:
	movq	%rax, %r14
	jmp	.LBB41_42
.LBB41_39:
.Ltmp718:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp719:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp720:
.LBB41_42:
	addq	$40, %r12
.Ltmp724:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp725:
.LBB41_43:
.Ltmp729:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp730:
	jmp	.LBB41_58
.LBB41_44:
.Ltmp731:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_45:
.Ltmp696:
	movq	%rax, %r14
	leaq	104(%r12), %rdi
.Ltmp697:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp698:
# BB#46:
	leaq	72(%r12), %rdi
.Ltmp702:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp703:
# BB#47:
	addq	$40, %r12
.Ltmp707:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp708:
# BB#48:
.Ltmp713:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp714:
	jmp	.LBB41_58
.LBB41_55:
.Ltmp715:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_50:
.Ltmp709:
	movq	%rax, %r14
	jmp	.LBB41_53
.LBB41_51:
.Ltmp704:
	movq	%rax, %r14
	jmp	.LBB41_52
.LBB41_49:
.Ltmp699:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp700:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp701:
.LBB41_52:
	addq	$40, %r12
.Ltmp705:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp706:
.LBB41_53:
.Ltmp710:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp711:
# BB#56:                                # %.body30
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_54:
.Ltmp712:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_57:
.Ltmp734:
	movq	%rax, %r14
.LBB41_58:                              # %.body6
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end41:
	.size	_ZN8NArchive3N7z8CDecoderD2Ev, .Lfunc_end41-_ZN8NArchive3N7z8CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\365\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\354\002"              # Call site table length
	.long	.Ltmp680-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp681-.Ltmp680       #   Call between .Ltmp680 and .Ltmp681
	.long	.Ltmp682-.Lfunc_begin13 #     jumps to .Ltmp682
	.byte	0                       #   On action: cleanup
	.long	.Ltmp686-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp687-.Ltmp686       #   Call between .Ltmp686 and .Ltmp687
	.long	.Ltmp688-.Lfunc_begin13 #     jumps to .Ltmp688
	.byte	0                       #   On action: cleanup
	.long	.Ltmp691-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp692-.Ltmp691       #   Call between .Ltmp691 and .Ltmp692
	.long	.Ltmp693-.Lfunc_begin13 #     jumps to .Ltmp693
	.byte	0                       #   On action: cleanup
	.long	.Ltmp735-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp736-.Ltmp735       #   Call between .Ltmp735 and .Ltmp736
	.long	.Ltmp737-.Lfunc_begin13 #     jumps to .Ltmp737
	.byte	0                       #   On action: cleanup
	.long	.Ltmp757-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Ltmp758-.Ltmp757       #   Call between .Ltmp757 and .Ltmp758
	.long	.Ltmp759-.Lfunc_begin13 #     jumps to .Ltmp759
	.byte	0                       #   On action: cleanup
	.long	.Ltmp762-.Lfunc_begin13 # >> Call Site 6 <<
	.long	.Ltmp763-.Ltmp762       #   Call between .Ltmp762 and .Ltmp763
	.long	.Ltmp764-.Lfunc_begin13 #     jumps to .Ltmp764
	.byte	0                       #   On action: cleanup
	.long	.Ltmp767-.Lfunc_begin13 # >> Call Site 7 <<
	.long	.Ltmp768-.Ltmp767       #   Call between .Ltmp767 and .Ltmp768
	.long	.Ltmp769-.Lfunc_begin13 #     jumps to .Ltmp769
	.byte	0                       #   On action: cleanup
	.long	.Ltmp768-.Lfunc_begin13 # >> Call Site 8 <<
	.long	.Ltmp760-.Ltmp768       #   Call between .Ltmp768 and .Ltmp760
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp760-.Lfunc_begin13 # >> Call Site 9 <<
	.long	.Ltmp771-.Ltmp760       #   Call between .Ltmp760 and .Ltmp771
	.long	.Ltmp772-.Lfunc_begin13 #     jumps to .Ltmp772
	.byte	1                       #   On action: 1
	.long	.Ltmp738-.Lfunc_begin13 # >> Call Site 10 <<
	.long	.Ltmp739-.Ltmp738       #   Call between .Ltmp738 and .Ltmp739
	.long	.Ltmp740-.Lfunc_begin13 #     jumps to .Ltmp740
	.byte	1                       #   On action: 1
	.long	.Ltmp743-.Lfunc_begin13 # >> Call Site 11 <<
	.long	.Ltmp744-.Ltmp743       #   Call between .Ltmp743 and .Ltmp744
	.long	.Ltmp745-.Lfunc_begin13 #     jumps to .Ltmp745
	.byte	1                       #   On action: 1
	.long	.Ltmp748-.Lfunc_begin13 # >> Call Site 12 <<
	.long	.Ltmp749-.Ltmp748       #   Call between .Ltmp748 and .Ltmp749
	.long	.Ltmp750-.Lfunc_begin13 #     jumps to .Ltmp750
	.byte	1                       #   On action: 1
	.long	.Ltmp754-.Lfunc_begin13 # >> Call Site 13 <<
	.long	.Ltmp755-.Ltmp754       #   Call between .Ltmp754 and .Ltmp755
	.long	.Ltmp756-.Lfunc_begin13 #     jumps to .Ltmp756
	.byte	1                       #   On action: 1
	.long	.Ltmp741-.Lfunc_begin13 # >> Call Site 14 <<
	.long	.Ltmp752-.Ltmp741       #   Call between .Ltmp741 and .Ltmp752
	.long	.Ltmp753-.Lfunc_begin13 #     jumps to .Ltmp753
	.byte	1                       #   On action: 1
	.long	.Ltmp683-.Lfunc_begin13 # >> Call Site 15 <<
	.long	.Ltmp684-.Ltmp683       #   Call between .Ltmp683 and .Ltmp684
	.long	.Ltmp685-.Lfunc_begin13 #     jumps to .Ltmp685
	.byte	1                       #   On action: 1
	.long	.Ltmp689-.Lfunc_begin13 # >> Call Site 16 <<
	.long	.Ltmp690-.Ltmp689       #   Call between .Ltmp689 and .Ltmp690
	.long	.Ltmp734-.Lfunc_begin13 #     jumps to .Ltmp734
	.byte	1                       #   On action: 1
	.long	.Ltmp694-.Lfunc_begin13 # >> Call Site 17 <<
	.long	.Ltmp695-.Ltmp694       #   Call between .Ltmp694 and .Ltmp695
	.long	.Ltmp696-.Lfunc_begin13 #     jumps to .Ltmp696
	.byte	1                       #   On action: 1
	.long	.Ltmp716-.Lfunc_begin13 # >> Call Site 18 <<
	.long	.Ltmp717-.Ltmp716       #   Call between .Ltmp716 and .Ltmp717
	.long	.Ltmp718-.Lfunc_begin13 #     jumps to .Ltmp718
	.byte	1                       #   On action: 1
	.long	.Ltmp721-.Lfunc_begin13 # >> Call Site 19 <<
	.long	.Ltmp722-.Ltmp721       #   Call between .Ltmp721 and .Ltmp722
	.long	.Ltmp723-.Lfunc_begin13 #     jumps to .Ltmp723
	.byte	1                       #   On action: 1
	.long	.Ltmp726-.Lfunc_begin13 # >> Call Site 20 <<
	.long	.Ltmp727-.Ltmp726       #   Call between .Ltmp726 and .Ltmp727
	.long	.Ltmp728-.Lfunc_begin13 #     jumps to .Ltmp728
	.byte	1                       #   On action: 1
	.long	.Ltmp732-.Lfunc_begin13 # >> Call Site 21 <<
	.long	.Ltmp733-.Ltmp732       #   Call between .Ltmp732 and .Ltmp733
	.long	.Ltmp734-.Lfunc_begin13 #     jumps to .Ltmp734
	.byte	1                       #   On action: 1
	.long	.Ltmp733-.Lfunc_begin13 # >> Call Site 22 <<
	.long	.Ltmp719-.Ltmp733       #   Call between .Ltmp733 and .Ltmp719
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp719-.Lfunc_begin13 # >> Call Site 23 <<
	.long	.Ltmp730-.Ltmp719       #   Call between .Ltmp719 and .Ltmp730
	.long	.Ltmp731-.Lfunc_begin13 #     jumps to .Ltmp731
	.byte	1                       #   On action: 1
	.long	.Ltmp697-.Lfunc_begin13 # >> Call Site 24 <<
	.long	.Ltmp698-.Ltmp697       #   Call between .Ltmp697 and .Ltmp698
	.long	.Ltmp699-.Lfunc_begin13 #     jumps to .Ltmp699
	.byte	1                       #   On action: 1
	.long	.Ltmp702-.Lfunc_begin13 # >> Call Site 25 <<
	.long	.Ltmp703-.Ltmp702       #   Call between .Ltmp702 and .Ltmp703
	.long	.Ltmp704-.Lfunc_begin13 #     jumps to .Ltmp704
	.byte	1                       #   On action: 1
	.long	.Ltmp707-.Lfunc_begin13 # >> Call Site 26 <<
	.long	.Ltmp708-.Ltmp707       #   Call between .Ltmp707 and .Ltmp708
	.long	.Ltmp709-.Lfunc_begin13 #     jumps to .Ltmp709
	.byte	1                       #   On action: 1
	.long	.Ltmp713-.Lfunc_begin13 # >> Call Site 27 <<
	.long	.Ltmp714-.Ltmp713       #   Call between .Ltmp713 and .Ltmp714
	.long	.Ltmp715-.Lfunc_begin13 #     jumps to .Ltmp715
	.byte	1                       #   On action: 1
	.long	.Ltmp700-.Lfunc_begin13 # >> Call Site 28 <<
	.long	.Ltmp711-.Ltmp700       #   Call between .Ltmp700 and .Ltmp711
	.long	.Ltmp712-.Lfunc_begin13 #     jumps to .Ltmp712
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi517:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi518:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi519:
	.cfi_def_cfa_offset 32
.Lcfi520:
	.cfi_offset %rbx, -24
.Lcfi521:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, (%rbx)
.Ltmp773:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp774:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB42_2:
.Ltmp775:
	movq	%rax, %r14
.Ltmp776:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp777:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB42_4:
.Ltmp778:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end42:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev, .Lfunc_end42-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp773-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp774-.Ltmp773       #   Call between .Ltmp773 and .Ltmp774
	.long	.Ltmp775-.Lfunc_begin14 #     jumps to .Ltmp775
	.byte	0                       #   On action: cleanup
	.long	.Ltmp774-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp776-.Ltmp774       #   Call between .Ltmp774 and .Ltmp776
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp776-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp777-.Ltmp776       #   Call between .Ltmp776 and .Ltmp777
	.long	.Ltmp778-.Lfunc_begin14 #     jumps to .Ltmp778
	.byte	1                       #   On action: 1
	.long	.Ltmp777-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end42-.Ltmp777   #   Call between .Ltmp777 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi522:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi523:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi524:
	.cfi_def_cfa_offset 32
.Lcfi525:
	.cfi_offset %rbx, -24
.Lcfi526:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, (%rbx)
.Ltmp779:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp780:
# BB#1:
.Ltmp785:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp786:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB43_5:
.Ltmp787:
	movq	%rax, %r14
	jmp	.LBB43_6
.LBB43_3:
.Ltmp781:
	movq	%rax, %r14
.Ltmp782:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp783:
.LBB43_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB43_4:
.Ltmp784:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end43:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev, .Lfunc_end43-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp779-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp780-.Ltmp779       #   Call between .Ltmp779 and .Ltmp780
	.long	.Ltmp781-.Lfunc_begin15 #     jumps to .Ltmp781
	.byte	0                       #   On action: cleanup
	.long	.Ltmp785-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp786-.Ltmp785       #   Call between .Ltmp785 and .Ltmp786
	.long	.Ltmp787-.Lfunc_begin15 #     jumps to .Ltmp787
	.byte	0                       #   On action: cleanup
	.long	.Ltmp782-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp783-.Ltmp782       #   Call between .Ltmp782 and .Ltmp783
	.long	.Ltmp784-.Lfunc_begin15 #     jumps to .Ltmp784
	.byte	1                       #   On action: 1
	.long	.Ltmp783-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end43-.Ltmp783   #   Call between .Ltmp783 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi527:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi528:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi529:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi530:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi531:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi532:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi533:
	.cfi_def_cfa_offset 64
.Lcfi534:
	.cfi_offset %rbx, -56
.Lcfi535:
	.cfi_offset %r12, -48
.Lcfi536:
	.cfi_offset %r13, -40
.Lcfi537:
	.cfi_offset %r14, -32
.Lcfi538:
	.cfi_offset %r15, -24
.Lcfi539:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB44_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB44_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB44_6
# BB#3:                                 #   in Loop: Header=BB44_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB44_5
# BB#4:                                 #   in Loop: Header=BB44_2 Depth=1
	movq	(%rdi), %rax
.Ltmp788:
	callq	*16(%rax)
.Ltmp789:
.LBB44_5:                               # %_ZN9CMyComPtrI8IUnknownED2Ev.exit
                                        #   in Loop: Header=BB44_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB44_6:                               #   in Loop: Header=BB44_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB44_2
.LBB44_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB44_8:
.Ltmp790:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end44:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii, .Lfunc_end44-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table44:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp788-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp789-.Ltmp788       #   Call between .Ltmp788 and .Ltmp789
	.long	.Ltmp790-.Lfunc_begin16 #     jumps to .Ltmp790
	.byte	0                       #   On action: cleanup
	.long	.Ltmp789-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Lfunc_end44-.Ltmp789   #   Call between .Ltmp789 and .Lfunc_end44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO7CInFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO7CInFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFileD0Ev,@function
_ZN8NWindows5NFile3NIO7CInFileD0Ev:     # @_ZN8NWindows5NFile3NIO7CInFileD0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi540:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi541:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi542:
	.cfi_def_cfa_offset 32
.Lcfi543:
	.cfi_offset %rbx, -24
.Lcfi544:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp791:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp792:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB45_2:
.Ltmp793:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end45:
	.size	_ZN8NWindows5NFile3NIO7CInFileD0Ev, .Lfunc_end45-_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp791-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp792-.Ltmp791       #   Call between .Ltmp791 and .Ltmp792
	.long	.Ltmp793-.Lfunc_begin17 #     jumps to .Ltmp793
	.byte	0                       #   On action: cleanup
	.long	.Ltmp792-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Lfunc_end45-.Ltmp792   #   Call between .Ltmp792 and .Lfunc_end45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi545:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi546:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi547:
	.cfi_def_cfa_offset 32
.Lcfi548:
	.cfi_offset %rbx, -24
.Lcfi549:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp794:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp795:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB46_2:
.Ltmp796:
	movq	%rax, %r14
.Ltmp797:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp798:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB46_4:
.Ltmp799:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end46:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev, .Lfunc_end46-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table46:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp794-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp795-.Ltmp794       #   Call between .Ltmp794 and .Ltmp795
	.long	.Ltmp796-.Lfunc_begin18 #     jumps to .Ltmp796
	.byte	0                       #   On action: cleanup
	.long	.Ltmp795-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp797-.Ltmp795       #   Call between .Ltmp795 and .Ltmp797
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp797-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp798-.Ltmp797       #   Call between .Ltmp797 and .Ltmp798
	.long	.Ltmp799-.Lfunc_begin18 #     jumps to .Ltmp799
	.byte	1                       #   On action: 1
	.long	.Ltmp798-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end46-.Ltmp798   #   Call between .Ltmp798 and .Lfunc_end46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi550:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi551:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi552:
	.cfi_def_cfa_offset 32
.Lcfi553:
	.cfi_offset %rbx, -24
.Lcfi554:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp800:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp801:
# BB#1:
.Ltmp806:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp807:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB47_5:
.Ltmp808:
	movq	%rax, %r14
	jmp	.LBB47_6
.LBB47_3:
.Ltmp802:
	movq	%rax, %r14
.Ltmp803:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp804:
.LBB47_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB47_4:
.Ltmp805:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end47:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev, .Lfunc_end47-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table47:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp800-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp801-.Ltmp800       #   Call between .Ltmp800 and .Ltmp801
	.long	.Ltmp802-.Lfunc_begin19 #     jumps to .Ltmp802
	.byte	0                       #   On action: cleanup
	.long	.Ltmp806-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Ltmp807-.Ltmp806       #   Call between .Ltmp806 and .Ltmp807
	.long	.Ltmp808-.Lfunc_begin19 #     jumps to .Ltmp808
	.byte	0                       #   On action: cleanup
	.long	.Ltmp803-.Lfunc_begin19 # >> Call Site 3 <<
	.long	.Ltmp804-.Ltmp803       #   Call between .Ltmp803 and .Ltmp804
	.long	.Ltmp805-.Lfunc_begin19 #     jumps to .Ltmp805
	.byte	1                       #   On action: 1
	.long	.Ltmp804-.Lfunc_begin19 # >> Call Site 4 <<
	.long	.Lfunc_end47-.Ltmp804   #   Call between .Ltmp804 and .Lfunc_end47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi555:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi556:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi557:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi558:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi559:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi560:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi561:
	.cfi_def_cfa_offset 80
.Lcfi562:
	.cfi_offset %rbx, -56
.Lcfi563:
	.cfi_offset %r12, -48
.Lcfi564:
	.cfi_offset %r13, -40
.Lcfi565:
	.cfi_offset %r14, -32
.Lcfi566:
	.cfi_offset %r15, -24
.Lcfi567:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	subl	%esi, %edi
	cmpl	%ecx, %eax
	cmovlel	%edx, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jle	.LBB48_7
# BB#1:                                 # %.lr.ph
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	movslq	12(%rsp), %r14          # 4-byte Folded Reload
	shlq	$3, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB48_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%r13, %rax
	movq	(%rax,%r15,8), %rbp
	testq	%rbp, %rbp
	je	.LBB48_6
# BB#3:                                 #   in Loop: Header=BB48_2 Depth=1
	leaq	8(%rbp), %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, 8(%rbp)
.Ltmp809:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp810:
# BB#4:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit.i
                                        #   in Loop: Header=BB48_2 Depth=1
.Ltmp815:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp816:
# BB#5:                                 # %_ZN7CMethodD2Ev.exit
                                        #   in Loop: Header=BB48_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB48_6:                               #   in Loop: Header=BB48_2 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jl	.LBB48_2
.LBB48_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	12(%rsp), %edx          # 4-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB48_8:
.Ltmp811:
	movq	%rax, %r14
.Ltmp812:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp813:
	jmp	.LBB48_11
.LBB48_9:
.Ltmp814:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB48_10:
.Ltmp817:
	movq	%rax, %r14
.LBB48_11:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end48:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii, .Lfunc_end48-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table48:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp809-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp810-.Ltmp809       #   Call between .Ltmp809 and .Ltmp810
	.long	.Ltmp811-.Lfunc_begin20 #     jumps to .Ltmp811
	.byte	0                       #   On action: cleanup
	.long	.Ltmp815-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp816-.Ltmp815       #   Call between .Ltmp815 and .Ltmp816
	.long	.Ltmp817-.Lfunc_begin20 #     jumps to .Ltmp817
	.byte	0                       #   On action: cleanup
	.long	.Ltmp816-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Ltmp812-.Ltmp816       #   Call between .Ltmp816 and .Ltmp812
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp812-.Lfunc_begin20 # >> Call Site 4 <<
	.long	.Ltmp813-.Ltmp812       #   Call between .Ltmp812 and .Ltmp813
	.long	.Ltmp814-.Lfunc_begin20 #     jumps to .Ltmp814
	.byte	1                       #   On action: 1
	.long	.Ltmp813-.Lfunc_begin20 # >> Call Site 5 <<
	.long	.Lfunc_end48-.Ltmp813   #   Call between .Ltmp813 and .Lfunc_end48
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED2Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED2Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED2Ev,@function
_ZN13CObjectVectorI5CPropED2Ev:         # @_ZN13CObjectVectorI5CPropED2Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi568:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi569:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi570:
	.cfi_def_cfa_offset 32
.Lcfi571:
	.cfi_offset %rbx, -24
.Lcfi572:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp818:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp819:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB49_2:
.Ltmp820:
	movq	%rax, %r14
.Ltmp821:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp822:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB49_4:
.Ltmp823:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end49:
	.size	_ZN13CObjectVectorI5CPropED2Ev, .Lfunc_end49-_ZN13CObjectVectorI5CPropED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table49:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp818-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp819-.Ltmp818       #   Call between .Ltmp818 and .Ltmp819
	.long	.Ltmp820-.Lfunc_begin21 #     jumps to .Ltmp820
	.byte	0                       #   On action: cleanup
	.long	.Ltmp819-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Ltmp821-.Ltmp819       #   Call between .Ltmp819 and .Ltmp821
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp821-.Lfunc_begin21 # >> Call Site 3 <<
	.long	.Ltmp822-.Ltmp821       #   Call between .Ltmp821 and .Ltmp822
	.long	.Ltmp823-.Lfunc_begin21 #     jumps to .Ltmp823
	.byte	1                       #   On action: 1
	.long	.Ltmp822-.Lfunc_begin21 # >> Call Site 4 <<
	.long	.Lfunc_end49-.Ltmp822   #   Call between .Ltmp822 and .Lfunc_end49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED0Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED0Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED0Ev,@function
_ZN13CObjectVectorI5CPropED0Ev:         # @_ZN13CObjectVectorI5CPropED0Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi573:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi574:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi575:
	.cfi_def_cfa_offset 32
.Lcfi576:
	.cfi_offset %rbx, -24
.Lcfi577:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp824:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp825:
# BB#1:
.Ltmp830:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp831:
# BB#2:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB50_5:
.Ltmp832:
	movq	%rax, %r14
	jmp	.LBB50_6
.LBB50_3:
.Ltmp826:
	movq	%rax, %r14
.Ltmp827:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp828:
.LBB50_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB50_4:
.Ltmp829:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end50:
	.size	_ZN13CObjectVectorI5CPropED0Ev, .Lfunc_end50-_ZN13CObjectVectorI5CPropED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table50:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp824-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp825-.Ltmp824       #   Call between .Ltmp824 and .Ltmp825
	.long	.Ltmp826-.Lfunc_begin22 #     jumps to .Ltmp826
	.byte	0                       #   On action: cleanup
	.long	.Ltmp830-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Ltmp831-.Ltmp830       #   Call between .Ltmp830 and .Ltmp831
	.long	.Ltmp832-.Lfunc_begin22 #     jumps to .Ltmp832
	.byte	0                       #   On action: cleanup
	.long	.Ltmp827-.Lfunc_begin22 # >> Call Site 3 <<
	.long	.Ltmp828-.Ltmp827       #   Call between .Ltmp827 and .Ltmp828
	.long	.Ltmp829-.Lfunc_begin22 #     jumps to .Ltmp829
	.byte	1                       #   On action: 1
	.long	.Ltmp828-.Lfunc_begin22 # >> Call Site 4 <<
	.long	.Lfunc_end50-.Ltmp828   #   Call between .Ltmp828 and .Lfunc_end50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI5CPropE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI5CPropE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropE6DeleteEii,@function
_ZN13CObjectVectorI5CPropE6DeleteEii:   # @_ZN13CObjectVectorI5CPropE6DeleteEii
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi578:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi579:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi580:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi581:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi582:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi583:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi584:
	.cfi_def_cfa_offset 64
.Lcfi585:
	.cfi_offset %rbx, -56
.Lcfi586:
	.cfi_offset %r12, -48
.Lcfi587:
	.cfi_offset %r13, -40
.Lcfi588:
	.cfi_offset %r14, -32
.Lcfi589:
	.cfi_offset %r15, -24
.Lcfi590:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB51_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB51_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB51_5
# BB#3:                                 #   in Loop: Header=BB51_2 Depth=1
	leaq	8(%rbp), %rdi
.Ltmp833:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp834:
# BB#4:                                 # %_ZN5CPropD2Ev.exit
                                        #   in Loop: Header=BB51_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB51_5:                               #   in Loop: Header=BB51_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB51_2
.LBB51_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB51_7:
.Ltmp835:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end51:
	.size	_ZN13CObjectVectorI5CPropE6DeleteEii, .Lfunc_end51-_ZN13CObjectVectorI5CPropE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table51:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp833-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp834-.Ltmp833       #   Call between .Ltmp833 and .Ltmp834
	.long	.Ltmp835-.Lfunc_begin23 #     jumps to .Ltmp835
	.byte	0                       #   On action: cleanup
	.long	.Ltmp834-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Lfunc_end51-.Ltmp834   #   Call between .Ltmp834 and .Lfunc_end51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,@function
_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev: # @_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%r14
.Lcfi591:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi592:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi593:
	.cfi_def_cfa_offset 32
.Lcfi594:
	.cfi_offset %rbx, -24
.Lcfi595:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp836:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp837:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB52_2:
.Ltmp838:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end52:
	.size	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev, .Lfunc_end52-_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table52:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp836-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp837-.Ltmp836       #   Call between .Ltmp836 and .Ltmp837
	.long	.Ltmp838-.Lfunc_begin24 #     jumps to .Ltmp838
	.byte	0                       #   On action: cleanup
	.long	.Ltmp837-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Lfunc_end52-.Ltmp837   #   Call between .Ltmp837 and .Lfunc_end52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6InsertEiRKS2_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6InsertEiRKS2_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6InsertEiRKS2_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6InsertEiRKS2_,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6InsertEiRKS2_: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6InsertEiRKS2_
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%rbp
.Lcfi596:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi597:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi598:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi599:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi600:
	.cfi_def_cfa_offset 48
.Lcfi601:
	.cfi_offset %rbx, -48
.Lcfi602:
	.cfi_offset %r12, -40
.Lcfi603:
	.cfi_offset %r14, -32
.Lcfi604:
	.cfi_offset %r15, -24
.Lcfi605:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	(%rbp), %rax
	movq	%rax, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	$8, 32(%rbx)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 8(%rbx)
	leaq	8(%rbx), %r12
.Ltmp839:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp840:
# BB#1:                                 # %.noexc.i.i.i
	leaq	8(%rbp), %rsi
.Ltmp841:
	movq	%r12, %rdi
	callq	_ZN13CObjectVectorI5CPropEpLERKS1_
.Ltmp842:
# BB#2:
	movq	40(%rbp), %rax
	movq	%rax, 40(%rbx)
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
	movq	16(%r15), %rax
	movslq	%r14d, %rcx
	movq	%rbx, (%rax,%rcx,8)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB53_3:
.Ltmp843:
	movq	%rax, %rbp
.Ltmp844:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp845:
# BB#4:                                 # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB53_5:
.Ltmp846:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end53:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6InsertEiRKS2_, .Lfunc_end53-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6InsertEiRKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table53:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin25-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp839-.Lfunc_begin25 #   Call between .Lfunc_begin25 and .Ltmp839
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp839-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Ltmp842-.Ltmp839       #   Call between .Ltmp839 and .Ltmp842
	.long	.Ltmp843-.Lfunc_begin25 #     jumps to .Ltmp843
	.byte	0                       #   On action: cleanup
	.long	.Ltmp842-.Lfunc_begin25 # >> Call Site 3 <<
	.long	.Ltmp844-.Ltmp842       #   Call between .Ltmp842 and .Ltmp844
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp844-.Lfunc_begin25 # >> Call Site 4 <<
	.long	.Ltmp845-.Ltmp844       #   Call between .Ltmp844 and .Ltmp845
	.long	.Ltmp846-.Lfunc_begin25 #     jumps to .Ltmp846
	.byte	1                       #   On action: 1
	.long	.Ltmp845-.Lfunc_begin25 # >> Call Site 5 <<
	.long	.Lfunc_end53-.Ltmp845   #   Call between .Ltmp845 and .Lfunc_end53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:
	pushq	%r15
.Lcfi606:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi607:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi608:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi609:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi610:
	.cfi_def_cfa_offset 48
.Lcfi611:
	.cfi_offset %rbx, -40
.Lcfi612:
	.cfi_offset %r12, -32
.Lcfi613:
	.cfi_offset %r14, -24
.Lcfi614:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	(%r15), %rax
	movq	%rax, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	$8, 32(%rbx)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 8(%rbx)
	leaq	8(%rbx), %r12
.Ltmp847:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp848:
# BB#1:                                 # %.noexc.i.i.i
	leaq	8(%r15), %rsi
.Ltmp849:
	movq	%r12, %rdi
	callq	_ZN13CObjectVectorI5CPropEpLERKS1_
.Ltmp850:
# BB#2:
	movq	40(%r15), %rax
	movq	%rax, 40(%rbx)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rcx
	movslq	12(%r14), %rax
	movq	%rbx, (%rcx,%rax,8)
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r14)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB54_3:
.Ltmp851:
	movq	%rax, %r14
.Ltmp852:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp853:
# BB#4:                                 # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB54_5:
.Ltmp854:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end54:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_, .Lfunc_end54-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table54:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin26-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp847-.Lfunc_begin26 #   Call between .Lfunc_begin26 and .Ltmp847
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp847-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Ltmp850-.Ltmp847       #   Call between .Ltmp847 and .Ltmp850
	.long	.Ltmp851-.Lfunc_begin26 #     jumps to .Ltmp851
	.byte	0                       #   On action: cleanup
	.long	.Ltmp850-.Lfunc_begin26 # >> Call Site 3 <<
	.long	.Ltmp852-.Ltmp850       #   Call between .Ltmp850 and .Ltmp852
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp852-.Lfunc_begin26 # >> Call Site 4 <<
	.long	.Ltmp853-.Ltmp852       #   Call between .Ltmp852 and .Ltmp853
	.long	.Ltmp854-.Lfunc_begin26 #     jumps to .Ltmp854
	.byte	1                       #   On action: 1
	.long	.Ltmp853-.Lfunc_begin26 # >> Call Site 5 <<
	.long	.Lfunc_end54-.Ltmp853   #   Call between .Ltmp853 and .Lfunc_end54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropEpLERKS1_,"axG",@progbits,_ZN13CObjectVectorI5CPropEpLERKS1_,comdat
	.weak	_ZN13CObjectVectorI5CPropEpLERKS1_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropEpLERKS1_,@function
_ZN13CObjectVectorI5CPropEpLERKS1_:     # @_ZN13CObjectVectorI5CPropEpLERKS1_
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:
	pushq	%rbp
.Lcfi615:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi616:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi617:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi618:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi619:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi620:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi621:
	.cfi_def_cfa_offset 64
.Lcfi622:
	.cfi_offset %rbx, -56
.Lcfi623:
	.cfi_offset %r12, -48
.Lcfi624:
	.cfi_offset %r13, -40
.Lcfi625:
	.cfi_offset %r14, -32
.Lcfi626:
	.cfi_offset %r15, -24
.Lcfi627:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	12(%r14), %r13
	movl	12(%r15), %esi
	addl	%r13d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testq	%r13, %r13
	jle	.LBB55_4
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB55_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbp,8), %rbx
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r12
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	leaq	8(%r12), %rdi
	addq	$8, %rbx
.Ltmp855:
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp856:
# BB#3:                                 # %_ZN13CObjectVectorI5CPropE3AddERKS0_.exit
                                        #   in Loop: Header=BB55_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB55_2
.LBB55_4:                               # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB55_5:
.Ltmp857:
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end55:
	.size	_ZN13CObjectVectorI5CPropEpLERKS1_, .Lfunc_end55-_ZN13CObjectVectorI5CPropEpLERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table55:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin27-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp855-.Lfunc_begin27 #   Call between .Lfunc_begin27 and .Ltmp855
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp855-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Ltmp856-.Ltmp855       #   Call between .Ltmp855 and .Ltmp856
	.long	.Ltmp857-.Lfunc_begin27 #     jumps to .Ltmp857
	.byte	0                       #   On action: cleanup
	.long	.Ltmp856-.Lfunc_begin27 # >> Call Site 3 <<
	.long	.Lfunc_end55-.Ltmp856   #   Call between .Ltmp856 and .Lfunc_end55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,"axG",@progbits,_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,comdat
	.weak	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,@function
_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv: # @_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_startproc
# BB#0:
	cmpb	$0, 17(%rdi)
	je	.LBB56_1
# BB#2:
	movb	$1, %al
	cmpb	$0, 16(%rdi)
	jne	.LBB56_4
# BB#3:
	movb	$0, 17(%rdi)
.LBB56_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB56_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end56:
	.size	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv, .Lfunc_end56-_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:
	pushq	%r14
.Lcfi628:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi629:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi630:
	.cfi_def_cfa_offset 32
.Lcfi631:
	.cfi_offset %rbx, -24
.Lcfi632:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp858:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp859:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB57_2:
.Ltmp860:
	movq	%rax, %r14
.Ltmp861:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp862:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB57_4:
.Ltmp863:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end57:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev, .Lfunc_end57-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table57:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp858-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp859-.Ltmp858       #   Call between .Ltmp858 and .Ltmp859
	.long	.Ltmp860-.Lfunc_begin28 #     jumps to .Ltmp860
	.byte	0                       #   On action: cleanup
	.long	.Ltmp859-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Ltmp861-.Ltmp859       #   Call between .Ltmp859 and .Ltmp861
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp861-.Lfunc_begin28 # >> Call Site 3 <<
	.long	.Ltmp862-.Ltmp861       #   Call between .Ltmp861 and .Ltmp862
	.long	.Ltmp863-.Lfunc_begin28 #     jumps to .Ltmp863
	.byte	1                       #   On action: 1
	.long	.Ltmp862-.Lfunc_begin28 # >> Call Site 4 <<
	.long	.Lfunc_end57-.Ltmp862   #   Call between .Ltmp862 and .Lfunc_end57
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:
	pushq	%r14
.Lcfi633:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi634:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi635:
	.cfi_def_cfa_offset 32
.Lcfi636:
	.cfi_offset %rbx, -24
.Lcfi637:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp864:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp865:
# BB#1:
.Ltmp870:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp871:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB58_5:
.Ltmp872:
	movq	%rax, %r14
	jmp	.LBB58_6
.LBB58_3:
.Ltmp866:
	movq	%rax, %r14
.Ltmp867:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp868:
.LBB58_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB58_4:
.Ltmp869:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end58:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev, .Lfunc_end58-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table58:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp864-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp865-.Ltmp864       #   Call between .Ltmp864 and .Ltmp865
	.long	.Ltmp866-.Lfunc_begin29 #     jumps to .Ltmp866
	.byte	0                       #   On action: cleanup
	.long	.Ltmp870-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Ltmp871-.Ltmp870       #   Call between .Ltmp870 and .Ltmp871
	.long	.Ltmp872-.Lfunc_begin29 #     jumps to .Ltmp872
	.byte	0                       #   On action: cleanup
	.long	.Ltmp867-.Lfunc_begin29 # >> Call Site 3 <<
	.long	.Ltmp868-.Ltmp867       #   Call between .Ltmp867 and .Ltmp868
	.long	.Ltmp869-.Lfunc_begin29 #     jumps to .Ltmp869
	.byte	1                       #   On action: 1
	.long	.Ltmp868-.Lfunc_begin29 # >> Call Site 4 <<
	.long	.Lfunc_end58-.Ltmp868   #   Call between .Ltmp868 and .Lfunc_end58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi638:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi639:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi640:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi641:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi642:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi643:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi644:
	.cfi_def_cfa_offset 64
.Lcfi645:
	.cfi_offset %rbx, -56
.Lcfi646:
	.cfi_offset %r12, -48
.Lcfi647:
	.cfi_offset %r13, -40
.Lcfi648:
	.cfi_offset %r14, -32
.Lcfi649:
	.cfi_offset %r15, -24
.Lcfi650:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB59_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB59_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB59_6
# BB#3:                                 #   in Loop: Header=BB59_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB59_5
# BB#4:                                 #   in Loop: Header=BB59_2 Depth=1
	callq	_ZdaPv
.LBB59_5:                               # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit
                                        #   in Loop: Header=BB59_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB59_6:                               #   in Loop: Header=BB59_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB59_2
.LBB59_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end59:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii, .Lfunc_end59-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB60_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB60_1:
	retq
.Lfunc_end60:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end60-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi651:
	.cfi_def_cfa_offset 16
.Lcfi652:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB61_2
# BB#1:
	callq	_ZdaPv
.LBB61_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end61:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end61-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev,@function
_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev: # @_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
.Lfunc_begin30:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception30
# BB#0:
	pushq	%r14
.Lcfi653:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi654:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi655:
	.cfi_def_cfa_offset 32
.Lcfi656:
	.cfi_offset %rbx, -24
.Lcfi657:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp873:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp874:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB62_2:
.Ltmp875:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end62:
	.size	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev, .Lfunc_end62-_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table62:
.Lexception30:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp873-.Lfunc_begin30 # >> Call Site 1 <<
	.long	.Ltmp874-.Ltmp873       #   Call between .Ltmp873 and .Ltmp874
	.long	.Ltmp875-.Lfunc_begin30 #     jumps to .Ltmp875
	.byte	0                       #   On action: cleanup
	.long	.Ltmp874-.Lfunc_begin30 # >> Call Site 2 <<
	.long	.Lfunc_end62-.Ltmp874   #   Call between .Ltmp874 and .Lfunc_end62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin31:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception31
# BB#0:
	pushq	%r14
.Lcfi658:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi659:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi660:
	.cfi_def_cfa_offset 32
.Lcfi661:
	.cfi_offset %rbx, -24
.Lcfi662:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp876:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp877:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB63_2:
.Ltmp878:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end63:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end63-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table63:
.Lexception31:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp876-.Lfunc_begin31 # >> Call Site 1 <<
	.long	.Ltmp877-.Ltmp876       #   Call between .Ltmp876 and .Ltmp877
	.long	.Ltmp878-.Lfunc_begin31 #     jumps to .Ltmp878
	.byte	0                       #   On action: cleanup
	.long	.Ltmp877-.Lfunc_begin31 # >> Call Site 2 <<
	.long	.Lfunc_end63-.Ltmp877   #   Call between .Ltmp877 and .Lfunc_end63
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi663:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi664:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi665:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi666:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi667:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi668:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi669:
	.cfi_def_cfa_offset 64
.Lcfi670:
	.cfi_offset %rbx, -56
.Lcfi671:
	.cfi_offset %r12, -48
.Lcfi672:
	.cfi_offset %r13, -40
.Lcfi673:
	.cfi_offset %r14, -32
.Lcfi674:
	.cfi_offset %r15, -24
.Lcfi675:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB64_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB64_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB64_3:                               # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB64_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB64_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB64_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB64_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB64_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB64_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB64_17
.LBB64_7:
	xorl	%ecx, %ecx
.LBB64_8:                               # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB64_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB64_10:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB64_10
.LBB64_11:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB64_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB64_13:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB64_13
	jmp	.LBB64_26
.LBB64_25:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB64_27
.LBB64_26:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB64_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB64_28:                              # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB64_17:                              # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB64_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB64_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB64_20
	jmp	.LBB64_21
.LBB64_18:
	xorl	%ebx, %ebx
.LBB64_21:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB64_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB64_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB64_23
.LBB64_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB64_8
	jmp	.LBB64_26
.Lfunc_end64:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end64-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin32:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception32
# BB#0:
	pushq	%r14
.Lcfi676:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi677:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi678:
	.cfi_def_cfa_offset 32
.Lcfi679:
	.cfi_offset %rbx, -24
.Lcfi680:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp879:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp880:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB65_2:
.Ltmp881:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end65:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end65-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table65:
.Lexception32:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp879-.Lfunc_begin32 # >> Call Site 1 <<
	.long	.Ltmp880-.Ltmp879       #   Call between .Ltmp879 and .Ltmp880
	.long	.Ltmp881-.Lfunc_begin32 #     jumps to .Ltmp881
	.byte	0                       #   On action: cleanup
	.long	.Ltmp880-.Lfunc_begin32 # >> Call Site 2 <<
	.long	.Lfunc_end65-.Ltmp880   #   Call between .Ltmp880 and .Lfunc_end65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin33:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception33
# BB#0:
	pushq	%rbp
.Lcfi681:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi682:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi683:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi684:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi685:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi686:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi687:
	.cfi_def_cfa_offset 64
.Lcfi688:
	.cfi_offset %rbx, -56
.Lcfi689:
	.cfi_offset %r12, -48
.Lcfi690:
	.cfi_offset %r13, -40
.Lcfi691:
	.cfi_offset %r14, -32
.Lcfi692:
	.cfi_offset %r15, -24
.Lcfi693:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB66_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB66_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB66_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB66_5
.LBB66_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB66_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp882:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp883:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB66_35
# BB#12:
	movq	%rbx, %r13
.LBB66_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB66_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB66_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB66_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB66_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB66_15
.LBB66_14:
	xorl	%esi, %esi
.LBB66_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB66_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB66_16
.LBB66_29:
	movq	%r13, %rbx
.LBB66_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp884:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp885:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB66_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB66_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB66_8
.LBB66_3:
	xorl	%eax, %eax
.LBB66_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB66_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB66_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB66_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB66_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB66_30
.LBB66_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB66_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB66_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB66_24
	jmp	.LBB66_25
.LBB66_22:
	xorl	%ecx, %ecx
.LBB66_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB66_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB66_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB66_27
.LBB66_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB66_15
	jmp	.LBB66_29
.LBB66_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp886:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end66:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end66-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table66:
.Lexception33:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin33-.Lfunc_begin33 # >> Call Site 1 <<
	.long	.Ltmp882-.Lfunc_begin33 #   Call between .Lfunc_begin33 and .Ltmp882
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp882-.Lfunc_begin33 # >> Call Site 2 <<
	.long	.Ltmp885-.Ltmp882       #   Call between .Ltmp882 and .Ltmp885
	.long	.Ltmp886-.Lfunc_begin33 #     jumps to .Ltmp886
	.byte	0                       #   On action: cleanup
	.long	.Ltmp885-.Lfunc_begin33 # >> Call Site 3 <<
	.long	.Lfunc_end66-.Ltmp885   #   Call between .Ltmp885 and .Lfunc_end66
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIiED0Ev,"axG",@progbits,_ZN13CRecordVectorIiED0Ev,comdat
	.weak	_ZN13CRecordVectorIiED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiED0Ev,@function
_ZN13CRecordVectorIiED0Ev:              # @_ZN13CRecordVectorIiED0Ev
.Lfunc_begin34:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception34
# BB#0:
	pushq	%r14
.Lcfi694:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi695:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi696:
	.cfi_def_cfa_offset 32
.Lcfi697:
	.cfi_offset %rbx, -24
.Lcfi698:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp887:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp888:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB67_2:
.Ltmp889:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end67:
	.size	_ZN13CRecordVectorIiED0Ev, .Lfunc_end67-_ZN13CRecordVectorIiED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table67:
.Lexception34:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp887-.Lfunc_begin34 # >> Call Site 1 <<
	.long	.Ltmp888-.Ltmp887       #   Call between .Ltmp887 and .Ltmp888
	.long	.Ltmp889-.Lfunc_begin34 #     jumps to .Ltmp889
	.byte	0                       #   On action: cleanup
	.long	.Ltmp888-.Lfunc_begin34 # >> Call Site 2 <<
	.long	.Lfunc_end67-.Ltmp888   #   Call between .Ltmp888 and .Lfunc_end67
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEED0Ev,@function
_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEED0Ev: # @_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEED0Ev
.Lfunc_begin35:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception35
# BB#0:
	pushq	%r14
.Lcfi699:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi700:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi701:
	.cfi_def_cfa_offset 32
.Lcfi702:
	.cfi_offset %rbx, -24
.Lcfi703:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp890:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp891:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB68_2:
.Ltmp892:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end68:
	.size	_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEED0Ev, .Lfunc_end68-_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table68:
.Lexception35:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp890-.Lfunc_begin35 # >> Call Site 1 <<
	.long	.Ltmp891-.Ltmp890       #   Call between .Ltmp890 and .Ltmp891
	.long	.Ltmp892-.Lfunc_begin35 #     jumps to .Ltmp892
	.byte	0                       #   On action: cleanup
	.long	.Ltmp891-.Lfunc_begin35 # >> Call Site 2 <<
	.long	.Lfunc_end68-.Ltmp891   #   Call between .Ltmp891 and .Lfunc_end68
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED0Ev
.Lfunc_begin36:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception36
# BB#0:
	pushq	%r14
.Lcfi704:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi705:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi706:
	.cfi_def_cfa_offset 32
.Lcfi707:
	.cfi_offset %rbx, -24
.Lcfi708:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE+16, (%rbx)
.Ltmp893:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp894:
# BB#1:
.Ltmp899:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp900:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB69_5:
.Ltmp901:
	movq	%rax, %r14
	jmp	.LBB69_6
.LBB69_3:
.Ltmp895:
	movq	%rax, %r14
.Ltmp896:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp897:
.LBB69_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB69_4:
.Ltmp898:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end69:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED0Ev, .Lfunc_end69-_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table69:
.Lexception36:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp893-.Lfunc_begin36 # >> Call Site 1 <<
	.long	.Ltmp894-.Ltmp893       #   Call between .Ltmp893 and .Ltmp894
	.long	.Ltmp895-.Lfunc_begin36 #     jumps to .Ltmp895
	.byte	0                       #   On action: cleanup
	.long	.Ltmp899-.Lfunc_begin36 # >> Call Site 2 <<
	.long	.Ltmp900-.Ltmp899       #   Call between .Ltmp899 and .Ltmp900
	.long	.Ltmp901-.Lfunc_begin36 #     jumps to .Ltmp901
	.byte	0                       #   On action: cleanup
	.long	.Ltmp896-.Lfunc_begin36 # >> Call Site 3 <<
	.long	.Ltmp897-.Ltmp896       #   Call between .Ltmp896 and .Ltmp897
	.long	.Ltmp898-.Lfunc_begin36 #     jumps to .Ltmp898
	.byte	1                       #   On action: 1
	.long	.Ltmp897-.Lfunc_begin36 # >> Call Site 4 <<
	.long	.Lfunc_end69-.Ltmp897   #   Call between .Ltmp897 and .Lfunc_end69
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEE6DeleteEii
.Lfunc_begin37:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception37
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi709:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi710:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi711:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi712:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi713:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi714:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi715:
	.cfi_def_cfa_offset 64
.Lcfi716:
	.cfi_offset %rbx, -56
.Lcfi717:
	.cfi_offset %r12, -48
.Lcfi718:
	.cfi_offset %r13, -40
.Lcfi719:
	.cfi_offset %r14, -32
.Lcfi720:
	.cfi_offset %r15, -24
.Lcfi721:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB70_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB70_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB70_5
# BB#3:                                 #   in Loop: Header=BB70_2 Depth=1
.Ltmp902:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp903:
# BB#4:                                 # %_ZN8NArchive3N7z11CSolidGroupD2Ev.exit
                                        #   in Loop: Header=BB70_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB70_5:                               #   in Loop: Header=BB70_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB70_2
.LBB70_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB70_7:
.Ltmp904:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end70:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEE6DeleteEii, .Lfunc_end70-_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table70:
.Lexception37:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp902-.Lfunc_begin37 # >> Call Site 1 <<
	.long	.Ltmp903-.Ltmp902       #   Call between .Ltmp902 and .Ltmp903
	.long	.Ltmp904-.Lfunc_begin37 #     jumps to .Ltmp904
	.byte	0                       #   On action: cleanup
	.long	.Ltmp903-.Lfunc_begin37 # >> Call Site 2 <<
	.long	.Lfunc_end70-.Ltmp903   #   Call between .Ltmp903 and .Lfunc_end70
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIjEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIjEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIjEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjEC2ERKS0_,@function
_ZN13CRecordVectorIjEC2ERKS0_:          # @_ZN13CRecordVectorIjEC2ERKS0_
.Lfunc_begin38:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception38
# BB#0:
	pushq	%rbp
.Lcfi722:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi723:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi724:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi725:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi726:
	.cfi_def_cfa_offset 48
.Lcfi727:
	.cfi_offset %rbx, -48
.Lcfi728:
	.cfi_offset %r12, -40
.Lcfi729:
	.cfi_offset %r14, -32
.Lcfi730:
	.cfi_offset %r15, -24
.Lcfi731:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$4, 24(%r12)
	movq	$_ZTV13CRecordVectorIjE+16, (%r12)
.Ltmp905:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp906:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp907:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp908:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB71_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB71_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movl	(%rax,%rbx,4), %ebp
.Ltmp910:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp911:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB71_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB71_4
.LBB71_6:                               # %_ZN13CRecordVectorIjEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB71_8:                               # %.loopexit.split-lp
.Ltmp909:
	jmp	.LBB71_9
.LBB71_7:                               # %.loopexit
.Ltmp912:
.LBB71_9:
	movq	%rax, %r14
.Ltmp913:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp914:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB71_11:
.Ltmp915:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end71:
	.size	_ZN13CRecordVectorIjEC2ERKS0_, .Lfunc_end71-_ZN13CRecordVectorIjEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table71:
.Lexception38:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp905-.Lfunc_begin38 # >> Call Site 1 <<
	.long	.Ltmp908-.Ltmp905       #   Call between .Ltmp905 and .Ltmp908
	.long	.Ltmp909-.Lfunc_begin38 #     jumps to .Ltmp909
	.byte	0                       #   On action: cleanup
	.long	.Ltmp910-.Lfunc_begin38 # >> Call Site 2 <<
	.long	.Ltmp911-.Ltmp910       #   Call between .Ltmp910 and .Ltmp911
	.long	.Ltmp912-.Lfunc_begin38 #     jumps to .Ltmp912
	.byte	0                       #   On action: cleanup
	.long	.Ltmp913-.Lfunc_begin38 # >> Call Site 3 <<
	.long	.Ltmp914-.Ltmp913       #   Call between .Ltmp913 and .Ltmp914
	.long	.Ltmp915-.Lfunc_begin38 #     jumps to .Ltmp915
	.byte	1                       #   On action: 1
	.long	.Ltmp914-.Lfunc_begin38 # >> Call Site 4 <<
	.long	.Lfunc_end71-.Ltmp914   #   Call between .Ltmp914 and .Lfunc_end71
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z7CFolderC2ERKS1_,"axG",@progbits,_ZN8NArchive3N7z7CFolderC2ERKS1_,comdat
	.weak	_ZN8NArchive3N7z7CFolderC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z7CFolderC2ERKS1_,@function
_ZN8NArchive3N7z7CFolderC2ERKS1_:       # @_ZN8NArchive3N7z7CFolderC2ERKS1_
.Lfunc_begin39:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception39
# BB#0:
	pushq	%r15
.Lcfi732:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi733:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi734:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi735:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi736:
	.cfi_def_cfa_offset 48
.Lcfi737:
	.cfi_offset %rbx, -48
.Lcfi738:
	.cfi_offset %r12, -40
.Lcfi739:
	.cfi_offset %r13, -32
.Lcfi740:
	.cfi_offset %r14, -24
.Lcfi741:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	callq	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
	leaq	32(%r12), %r14
	leaq	32(%rbx), %rsi
.Ltmp916:
	movq	%r14, %rdi
	callq	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
.Ltmp917:
# BB#1:
	leaq	64(%r12), %r13
	leaq	64(%rbx), %rsi
.Ltmp919:
	movq	%r13, %rdi
	callq	_ZN13CRecordVectorIjEC2ERKS0_
.Ltmp920:
# BB#2:
	leaq	96(%r12), %rdi
	leaq	96(%rbx), %rsi
.Ltmp922:
	callq	_ZN13CRecordVectorIyEC2ERKS0_
.Ltmp923:
# BB#3:
	movb	132(%rbx), %al
	movb	%al, 132(%r12)
	movl	128(%rbx), %eax
	movl	%eax, 128(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB72_6:
.Ltmp924:
	movq	%rax, %r15
.Ltmp925:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp926:
	jmp	.LBB72_7
.LBB72_5:
.Ltmp921:
	movq	%rax, %r15
.LBB72_7:
.Ltmp927:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp928:
	jmp	.LBB72_8
.LBB72_4:
.Ltmp918:
	movq	%rax, %r15
.LBB72_8:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%r12)
.Ltmp929:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp930:
# BB#9:
.Ltmp935:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp936:
# BB#10:                                # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB72_11:
.Ltmp931:
	movq	%rax, %rbx
.Ltmp932:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp933:
	jmp	.LBB72_14
.LBB72_12:
.Ltmp934:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB72_13:
.Ltmp937:
	movq	%rax, %rbx
.LBB72_14:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end72:
	.size	_ZN8NArchive3N7z7CFolderC2ERKS1_, .Lfunc_end72-_ZN8NArchive3N7z7CFolderC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table72:
.Lexception39:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin39-.Lfunc_begin39 # >> Call Site 1 <<
	.long	.Ltmp916-.Lfunc_begin39 #   Call between .Lfunc_begin39 and .Ltmp916
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp916-.Lfunc_begin39 # >> Call Site 2 <<
	.long	.Ltmp917-.Ltmp916       #   Call between .Ltmp916 and .Ltmp917
	.long	.Ltmp918-.Lfunc_begin39 #     jumps to .Ltmp918
	.byte	0                       #   On action: cleanup
	.long	.Ltmp919-.Lfunc_begin39 # >> Call Site 3 <<
	.long	.Ltmp920-.Ltmp919       #   Call between .Ltmp919 and .Ltmp920
	.long	.Ltmp921-.Lfunc_begin39 #     jumps to .Ltmp921
	.byte	0                       #   On action: cleanup
	.long	.Ltmp922-.Lfunc_begin39 # >> Call Site 4 <<
	.long	.Ltmp923-.Ltmp922       #   Call between .Ltmp922 and .Ltmp923
	.long	.Ltmp924-.Lfunc_begin39 #     jumps to .Ltmp924
	.byte	0                       #   On action: cleanup
	.long	.Ltmp925-.Lfunc_begin39 # >> Call Site 5 <<
	.long	.Ltmp928-.Ltmp925       #   Call between .Ltmp925 and .Ltmp928
	.long	.Ltmp937-.Lfunc_begin39 #     jumps to .Ltmp937
	.byte	1                       #   On action: 1
	.long	.Ltmp929-.Lfunc_begin39 # >> Call Site 6 <<
	.long	.Ltmp930-.Ltmp929       #   Call between .Ltmp929 and .Ltmp930
	.long	.Ltmp931-.Lfunc_begin39 #     jumps to .Ltmp931
	.byte	1                       #   On action: 1
	.long	.Ltmp935-.Lfunc_begin39 # >> Call Site 7 <<
	.long	.Ltmp936-.Ltmp935       #   Call between .Ltmp935 and .Ltmp936
	.long	.Ltmp937-.Lfunc_begin39 #     jumps to .Ltmp937
	.byte	1                       #   On action: 1
	.long	.Ltmp936-.Lfunc_begin39 # >> Call Site 8 <<
	.long	.Ltmp932-.Ltmp936       #   Call between .Ltmp936 and .Ltmp932
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp932-.Lfunc_begin39 # >> Call Site 9 <<
	.long	.Ltmp933-.Ltmp932       #   Call between .Ltmp932 and .Ltmp933
	.long	.Ltmp934-.Lfunc_begin39 #     jumps to .Ltmp934
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI73_0:
	.zero	16
	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
.Lfunc_begin40:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception40
# BB#0:
	pushq	%rbp
.Lcfi742:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi743:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi744:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi745:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi746:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi747:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi748:
	.cfi_def_cfa_offset 80
.Lcfi749:
	.cfi_offset %rbx, -56
.Lcfi750:
	.cfi_offset %r12, -48
.Lcfi751:
	.cfi_offset %r13, -40
.Lcfi752:
	.cfi_offset %r14, -32
.Lcfi753:
	.cfi_offset %r15, -24
.Lcfi754:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbp)
	movq	$8, 24(%rbp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbp)
.Ltmp938:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp939:
# BB#1:                                 # %.noexc
	movl	12(%rbx), %r15d
	movl	12(%rbp), %esi
	addl	%r15d, %esi
.Ltmp940:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp941:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB73_10
# BB#3:                                 # %.lr.ph.i.i
	xorl	%r13d, %r13d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB73_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax,%r13,8), %r14
.Ltmp943:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp944:
# BB#5:                                 # %.noexc5
                                        #   in Loop: Header=BB73_4 Depth=1
	movq	(%r14), %rax
	movq	%rax, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	16(%r14), %r12
	testq	%r12, %r12
	je	.LBB73_8
# BB#6:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i
                                        #   in Loop: Header=BB73_4 Depth=1
.Ltmp945:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp946:
# BB#7:                                 # %.noexc.i
                                        #   in Loop: Header=BB73_4 Depth=1
	movq	%rax, 24(%rbx)
	movq	%r12, 16(%rbx)
	movq	24(%r14), %rsi
	movq	%rax, %rdi
	movq	%r12, %rdx
	callq	memmove
.LBB73_8:                               #   in Loop: Header=BB73_4 Depth=1
	movq	32(%r14), %rax
	movq	%rax, 32(%rbx)
.Ltmp948:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp949:
# BB#9:                                 # %.noexc4
                                        #   in Loop: Header=BB73_4 Depth=1
	movq	16(%rbp), %rax
	movslq	12(%rbp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rbp)
	incq	%r13
	cmpq	%r13, %r15
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB73_4
.LBB73_10:                              # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEaSERKS3_.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB73_12:                              # %.loopexit.split-lp
.Ltmp942:
	jmp	.LBB73_13
.LBB73_17:
.Ltmp947:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB73_14
.LBB73_11:                              # %.loopexit
.Ltmp950:
.LBB73_13:                              # %.body
	movq	%rax, %r14
.LBB73_14:                              # %.body
.Ltmp951:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp952:
# BB#15:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB73_16:
.Ltmp953:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end73:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_, .Lfunc_end73-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table73:
.Lexception40:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp938-.Lfunc_begin40 # >> Call Site 1 <<
	.long	.Ltmp941-.Ltmp938       #   Call between .Ltmp938 and .Ltmp941
	.long	.Ltmp942-.Lfunc_begin40 #     jumps to .Ltmp942
	.byte	0                       #   On action: cleanup
	.long	.Ltmp943-.Lfunc_begin40 # >> Call Site 2 <<
	.long	.Ltmp944-.Ltmp943       #   Call between .Ltmp943 and .Ltmp944
	.long	.Ltmp950-.Lfunc_begin40 #     jumps to .Ltmp950
	.byte	0                       #   On action: cleanup
	.long	.Ltmp945-.Lfunc_begin40 # >> Call Site 3 <<
	.long	.Ltmp946-.Ltmp945       #   Call between .Ltmp945 and .Ltmp946
	.long	.Ltmp947-.Lfunc_begin40 #     jumps to .Ltmp947
	.byte	0                       #   On action: cleanup
	.long	.Ltmp946-.Lfunc_begin40 # >> Call Site 4 <<
	.long	.Ltmp948-.Ltmp946       #   Call between .Ltmp946 and .Ltmp948
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp948-.Lfunc_begin40 # >> Call Site 5 <<
	.long	.Ltmp949-.Ltmp948       #   Call between .Ltmp948 and .Ltmp949
	.long	.Ltmp950-.Lfunc_begin40 #     jumps to .Ltmp950
	.byte	0                       #   On action: cleanup
	.long	.Ltmp951-.Lfunc_begin40 # >> Call Site 6 <<
	.long	.Ltmp952-.Ltmp951       #   Call between .Ltmp951 and .Ltmp952
	.long	.Ltmp953-.Lfunc_begin40 #     jumps to .Ltmp953
	.byte	1                       #   On action: 1
	.long	.Ltmp952-.Lfunc_begin40 # >> Call Site 7 <<
	.long	.Lfunc_end73-.Ltmp952   #   Call between .Ltmp952 and .Lfunc_end73
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_,@function
_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_: # @_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
.Lfunc_begin41:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception41
# BB#0:
	pushq	%r15
.Lcfi755:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi756:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi757:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi758:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi759:
	.cfi_def_cfa_offset 48
.Lcfi760:
	.cfi_offset %rbx, -48
.Lcfi761:
	.cfi_offset %r12, -40
.Lcfi762:
	.cfi_offset %r13, -32
.Lcfi763:
	.cfi_offset %r14, -24
.Lcfi764:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE+16, (%r12)
.Ltmp954:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp955:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp956:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp957:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB74_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB74_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp959:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp960:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB74_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB74_4
.LBB74_6:                               # %_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEaSERKS3_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB74_8:                               # %.loopexit.split-lp
.Ltmp958:
	jmp	.LBB74_9
.LBB74_7:                               # %.loopexit
.Ltmp961:
.LBB74_9:
	movq	%rax, %r14
.Ltmp962:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp963:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB74_11:
.Ltmp964:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end74:
	.size	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_, .Lfunc_end74-_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table74:
.Lexception41:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp954-.Lfunc_begin41 # >> Call Site 1 <<
	.long	.Ltmp957-.Ltmp954       #   Call between .Ltmp954 and .Ltmp957
	.long	.Ltmp958-.Lfunc_begin41 #     jumps to .Ltmp958
	.byte	0                       #   On action: cleanup
	.long	.Ltmp959-.Lfunc_begin41 # >> Call Site 2 <<
	.long	.Ltmp960-.Ltmp959       #   Call between .Ltmp959 and .Ltmp960
	.long	.Ltmp961-.Lfunc_begin41 #     jumps to .Ltmp961
	.byte	0                       #   On action: cleanup
	.long	.Ltmp962-.Lfunc_begin41 # >> Call Site 3 <<
	.long	.Ltmp963-.Ltmp962       #   Call between .Ltmp962 and .Ltmp963
	.long	.Ltmp964-.Lfunc_begin41 #     jumps to .Ltmp964
	.byte	1                       #   On action: 1
	.long	.Ltmp963-.Lfunc_begin41 # >> Call Site 4 <<
	.long	.Lfunc_end74-.Ltmp963   #   Call between .Ltmp963 and .Lfunc_end74
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIyEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIyEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIyEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyEC2ERKS0_,@function
_ZN13CRecordVectorIyEC2ERKS0_:          # @_ZN13CRecordVectorIyEC2ERKS0_
.Lfunc_begin42:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception42
# BB#0:
	pushq	%r15
.Lcfi765:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi766:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi767:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi768:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi769:
	.cfi_def_cfa_offset 48
.Lcfi770:
	.cfi_offset %rbx, -48
.Lcfi771:
	.cfi_offset %r12, -40
.Lcfi772:
	.cfi_offset %r13, -32
.Lcfi773:
	.cfi_offset %r14, -24
.Lcfi774:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIyE+16, (%r12)
.Ltmp965:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp966:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp967:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp968:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB75_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB75_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp970:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp971:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB75_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB75_4
.LBB75_6:                               # %_ZN13CRecordVectorIyEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB75_8:                               # %.loopexit.split-lp
.Ltmp969:
	jmp	.LBB75_9
.LBB75_7:                               # %.loopexit
.Ltmp972:
.LBB75_9:
	movq	%rax, %r14
.Ltmp973:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp974:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB75_11:
.Ltmp975:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end75:
	.size	_ZN13CRecordVectorIyEC2ERKS0_, .Lfunc_end75-_ZN13CRecordVectorIyEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table75:
.Lexception42:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp965-.Lfunc_begin42 # >> Call Site 1 <<
	.long	.Ltmp968-.Ltmp965       #   Call between .Ltmp965 and .Ltmp968
	.long	.Ltmp969-.Lfunc_begin42 #     jumps to .Ltmp969
	.byte	0                       #   On action: cleanup
	.long	.Ltmp970-.Lfunc_begin42 # >> Call Site 2 <<
	.long	.Ltmp971-.Ltmp970       #   Call between .Ltmp970 and .Ltmp971
	.long	.Ltmp972-.Lfunc_begin42 #     jumps to .Ltmp972
	.byte	0                       #   On action: cleanup
	.long	.Ltmp973-.Lfunc_begin42 # >> Call Site 3 <<
	.long	.Ltmp974-.Ltmp973       #   Call between .Ltmp973 and .Ltmp974
	.long	.Ltmp975-.Lfunc_begin42 #     jumps to .Ltmp975
	.byte	1                       #   On action: 1
	.long	.Ltmp974-.Lfunc_begin42 # >> Call Site 4 <<
	.long	.Lfunc_end75-.Ltmp974   #   Call between .Ltmp974 and .Lfunc_end75
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIbED0Ev,"axG",@progbits,_ZN13CRecordVectorIbED0Ev,comdat
	.weak	_ZN13CRecordVectorIbED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIbED0Ev,@function
_ZN13CRecordVectorIbED0Ev:              # @_ZN13CRecordVectorIbED0Ev
.Lfunc_begin43:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception43
# BB#0:
	pushq	%r14
.Lcfi775:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi776:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi777:
	.cfi_def_cfa_offset 32
.Lcfi778:
	.cfi_offset %rbx, -24
.Lcfi779:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp976:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp977:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB76_2:
.Ltmp978:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end76:
	.size	_ZN13CRecordVectorIbED0Ev, .Lfunc_end76-_ZN13CRecordVectorIbED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table76:
.Lexception43:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp976-.Lfunc_begin43 # >> Call Site 1 <<
	.long	.Ltmp977-.Ltmp976       #   Call between .Ltmp976 and .Ltmp977
	.long	.Ltmp978-.Lfunc_begin43 #     jumps to .Ltmp978
	.byte	0                       #   On action: cleanup
	.long	.Ltmp977-.Lfunc_begin43 # >> Call Site 2 <<
	.long	.Lfunc_end76-.Ltmp977   #   Call between .Ltmp977 and .Lfunc_end76
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z8CRefItemEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEED0Ev,@function
_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEED0Ev: # @_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEED0Ev
.Lfunc_begin44:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception44
# BB#0:
	pushq	%r14
.Lcfi780:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi781:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi782:
	.cfi_def_cfa_offset 32
.Lcfi783:
	.cfi_offset %rbx, -24
.Lcfi784:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp979:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp980:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB77_2:
.Ltmp981:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end77:
	.size	_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEED0Ev, .Lfunc_end77-_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table77:
.Lexception44:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp979-.Lfunc_begin44 # >> Call Site 1 <<
	.long	.Ltmp980-.Ltmp979       #   Call between .Ltmp979 and .Ltmp980
	.long	.Ltmp981-.Lfunc_begin44 #     jumps to .Ltmp981
	.byte	0                       #   On action: cleanup
	.long	.Ltmp980-.Lfunc_begin44 # >> Call Site 2 <<
	.long	.Lfunc_end77-.Ltmp980   #   Call between .Ltmp980 and .Lfunc_end77
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTVN8NArchive3N7z17CFolderOutStream2E,@object # @_ZTVN8NArchive3N7z17CFolderOutStream2E
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive3N7z17CFolderOutStream2E
	.p2align	3
_ZTVN8NArchive3N7z17CFolderOutStream2E:
	.quad	0
	.quad	_ZTIN8NArchive3N7z17CFolderOutStream2E
	.quad	_ZN8NArchive3N7z17CFolderOutStream214QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive3N7z17CFolderOutStream26AddRefEv
	.quad	_ZN8NArchive3N7z17CFolderOutStream27ReleaseEv
	.quad	_ZN8NArchive3N7z17CFolderOutStream2D2Ev
	.quad	_ZN8NArchive3N7z17CFolderOutStream2D0Ev
	.quad	_ZN8NArchive3N7z17CFolderOutStream25WriteEPKvjPj
	.size	_ZTVN8NArchive3N7z17CFolderOutStream2E, 64

	.type	_ZTSN8NArchive3N7z17CFolderOutStream2E,@object # @_ZTSN8NArchive3N7z17CFolderOutStream2E
	.globl	_ZTSN8NArchive3N7z17CFolderOutStream2E
	.p2align	4
_ZTSN8NArchive3N7z17CFolderOutStream2E:
	.asciz	"N8NArchive3N7z17CFolderOutStream2E"
	.size	_ZTSN8NArchive3N7z17CFolderOutStream2E, 35

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive3N7z17CFolderOutStream2E,@object # @_ZTIN8NArchive3N7z17CFolderOutStream2E
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive3N7z17CFolderOutStream2E
	.p2align	4
_ZTIN8NArchive3N7z17CFolderOutStream2E:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive3N7z17CFolderOutStream2E
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive3N7z17CFolderOutStream2E, 56

	.type	_ZTVN8NArchive3N7z14CThreadDecoderE,@object # @_ZTVN8NArchive3N7z14CThreadDecoderE
	.globl	_ZTVN8NArchive3N7z14CThreadDecoderE
	.p2align	3
_ZTVN8NArchive3N7z14CThreadDecoderE:
	.quad	0
	.quad	_ZTIN8NArchive3N7z14CThreadDecoderE
	.quad	_ZN8NArchive3N7z14CThreadDecoder7ExecuteEv
	.size	_ZTVN8NArchive3N7z14CThreadDecoderE, 24

	.type	_ZTSN8NArchive3N7z14CThreadDecoderE,@object # @_ZTSN8NArchive3N7z14CThreadDecoderE
	.globl	_ZTSN8NArchive3N7z14CThreadDecoderE
	.p2align	4
_ZTSN8NArchive3N7z14CThreadDecoderE:
	.asciz	"N8NArchive3N7z14CThreadDecoderE"
	.size	_ZTSN8NArchive3N7z14CThreadDecoderE, 32

	.type	_ZTS11CVirtThread,@object # @_ZTS11CVirtThread
	.section	.rodata._ZTS11CVirtThread,"aG",@progbits,_ZTS11CVirtThread,comdat
	.weak	_ZTS11CVirtThread
_ZTS11CVirtThread:
	.asciz	"11CVirtThread"
	.size	_ZTS11CVirtThread, 14

	.type	_ZTI11CVirtThread,@object # @_ZTI11CVirtThread
	.section	.rodata._ZTI11CVirtThread,"aG",@progbits,_ZTI11CVirtThread,comdat
	.weak	_ZTI11CVirtThread
	.p2align	3
_ZTI11CVirtThread:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS11CVirtThread
	.size	_ZTI11CVirtThread, 16

	.type	_ZTIN8NArchive3N7z14CThreadDecoderE,@object # @_ZTIN8NArchive3N7z14CThreadDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive3N7z14CThreadDecoderE
	.p2align	4
_ZTIN8NArchive3N7z14CThreadDecoderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NArchive3N7z14CThreadDecoderE
	.quad	_ZTI11CVirtThread
	.size	_ZTIN8NArchive3N7z14CThreadDecoderE, 24

	.type	_ZTVN8NArchive3N7z22CCryptoGetTextPasswordE,@object # @_ZTVN8NArchive3N7z22CCryptoGetTextPasswordE
	.globl	_ZTVN8NArchive3N7z22CCryptoGetTextPasswordE
	.p2align	3
_ZTVN8NArchive3N7z22CCryptoGetTextPasswordE:
	.quad	0
	.quad	_ZTIN8NArchive3N7z22CCryptoGetTextPasswordE
	.quad	_ZN8NArchive3N7z22CCryptoGetTextPassword14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive3N7z22CCryptoGetTextPassword6AddRefEv
	.quad	_ZN8NArchive3N7z22CCryptoGetTextPassword7ReleaseEv
	.quad	_ZN8NArchive3N7z22CCryptoGetTextPasswordD2Ev
	.quad	_ZN8NArchive3N7z22CCryptoGetTextPasswordD0Ev
	.quad	_ZN8NArchive3N7z22CCryptoGetTextPassword21CryptoGetTextPasswordEPPw
	.size	_ZTVN8NArchive3N7z22CCryptoGetTextPasswordE, 64

	.type	_ZTSN8NArchive3N7z22CCryptoGetTextPasswordE,@object # @_ZTSN8NArchive3N7z22CCryptoGetTextPasswordE
	.globl	_ZTSN8NArchive3N7z22CCryptoGetTextPasswordE
	.p2align	4
_ZTSN8NArchive3N7z22CCryptoGetTextPasswordE:
	.asciz	"N8NArchive3N7z22CCryptoGetTextPasswordE"
	.size	_ZTSN8NArchive3N7z22CCryptoGetTextPasswordE, 40

	.type	_ZTS22ICryptoGetTextPassword,@object # @_ZTS22ICryptoGetTextPassword
	.section	.rodata._ZTS22ICryptoGetTextPassword,"aG",@progbits,_ZTS22ICryptoGetTextPassword,comdat
	.weak	_ZTS22ICryptoGetTextPassword
	.p2align	4
_ZTS22ICryptoGetTextPassword:
	.asciz	"22ICryptoGetTextPassword"
	.size	_ZTS22ICryptoGetTextPassword, 25

	.type	_ZTI22ICryptoGetTextPassword,@object # @_ZTI22ICryptoGetTextPassword
	.section	.rodata._ZTI22ICryptoGetTextPassword,"aG",@progbits,_ZTI22ICryptoGetTextPassword,comdat
	.weak	_ZTI22ICryptoGetTextPassword
	.p2align	4
_ZTI22ICryptoGetTextPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22ICryptoGetTextPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI22ICryptoGetTextPassword, 24

	.type	_ZTIN8NArchive3N7z22CCryptoGetTextPasswordE,@object # @_ZTIN8NArchive3N7z22CCryptoGetTextPasswordE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive3N7z22CCryptoGetTextPasswordE
	.p2align	4
_ZTIN8NArchive3N7z22CCryptoGetTextPasswordE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive3N7z22CCryptoGetTextPasswordE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI22ICryptoGetTextPassword
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive3N7z22CCryptoGetTextPasswordE, 56

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" lzma 7z ace arc arj bz bz2 deb lzo lzx gz pak rpm sit tgz tbz tbz2 tgz cab ha lha lzh rar zoo zip jar ear war msi 3gp avi mov mpeg mpg mpe wmv aac ape fla flac la mp3 m4a mp4 ofr ogg pac ra rm rka shn swa tta wv wma wav swf  chm hxi hxs gif jpeg jpg jp2 png tiff  bmp ico psd psp awg ps eps cgm dxf svg vrml wmf emf ai md cad dwg pps key sxi max 3ds iso bin nrg mdf img pdi tar cpio xpi vfd vhd vud vmc vsv vmdk dsk nvram vmem vmsd vmsn vmss vmtm inl inc idl acf asa h hpp hxx c cpp cxx rc java cs pas bas vb cls ctl frm dlg def f77 f f90 f95 asm sql manifest dep  mak clw csproj vcproj sln dsp dsw  class  bat cmd xml xsd xsl xslt hxk hxc htm html xhtml xht mht mhtml htw asp aspx css cgi jsp shtml awk sed hta js php php3 php4 php5 phptml pl pm py pyo rb sh tcl vbs text txt tex ans asc srt reg ini doc docx mcw dot rtf hlp xls xlr xlt xlw ppt pdf sxc sxd sxi sxg sxw stc sti stw stm odt ott odg otg odp otp ods ots odf abw afp cwk lwp wpd wps wpt wrf wri abf afm bdf fon mgf otf pcf pfa snf ttf dbf mdb nsf ntf wdb db fdb gdb exe dll ocx vbx sfx sys tlb awx com obj lib out o so  pdb pch idb ncb opt"
	.size	.L.str, 1104

	.type	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.asciz	"13CObjectVectorI9CMyComPtrI8IUnknownEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE, 39

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE, 24

	.type	_ZTVN8NWindows5NFile3NIO7CInFileE,@object # @_ZTVN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO7CInFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO7CInFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO7CInFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO7CInFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO7CInFileE,@object # @_ZTSN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO7CInFileE:
	.asciz	"N8NWindows5NFile3NIO7CInFileE"
	.size	_ZTSN8NWindows5NFile3NIO7CInFileE, 30

	.type	_ZTIN8NWindows5NFile3NIO7CInFileE,@object # @_ZTIN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO7CInFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO7CInFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO7CInFileE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z11CMethodFullEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 46

	.type	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 24

	.type	_ZTV13CObjectVectorI5CPropE,@object # @_ZTV13CObjectVectorI5CPropE
	.section	.rodata._ZTV13CObjectVectorI5CPropE,"aG",@progbits,_ZTV13CObjectVectorI5CPropE,comdat
	.weak	_ZTV13CObjectVectorI5CPropE
	.p2align	3
_ZTV13CObjectVectorI5CPropE:
	.quad	0
	.quad	_ZTI13CObjectVectorI5CPropE
	.quad	_ZN13CObjectVectorI5CPropED2Ev
	.quad	_ZN13CObjectVectorI5CPropED0Ev
	.quad	_ZN13CObjectVectorI5CPropE6DeleteEii
	.size	_ZTV13CObjectVectorI5CPropE, 40

	.type	_ZTS13CObjectVectorI5CPropE,@object # @_ZTS13CObjectVectorI5CPropE
	.section	.rodata._ZTS13CObjectVectorI5CPropE,"aG",@progbits,_ZTS13CObjectVectorI5CPropE,comdat
	.weak	_ZTS13CObjectVectorI5CPropE
	.p2align	4
_ZTS13CObjectVectorI5CPropE:
	.asciz	"13CObjectVectorI5CPropE"
	.size	_ZTS13CObjectVectorI5CPropE, 24

	.type	_ZTI13CObjectVectorI5CPropE,@object # @_ZTI13CObjectVectorI5CPropE
	.section	.rodata._ZTI13CObjectVectorI5CPropE,"aG",@progbits,_ZTI13CObjectVectorI5CPropE,comdat
	.weak	_ZTI13CObjectVectorI5CPropE
	.p2align	4
_ZTI13CObjectVectorI5CPropE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI5CPropE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI5CPropE, 24

	.type	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE:
	.asciz	"13CRecordVectorIN8NArchive3N7z5CBindEE"
	.size	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE, 39

	.type	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE, 24

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.1:
	.long	66                      # 0x42
	.long	84                      # 0x54
	.long	50                      # 0x32
	.long	0                       # 0x0
	.size	.L.str.1, 16

	.type	_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE,@object # @_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE
	.section	.rodata._ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE,"aG",@progbits,_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE,comdat
	.weak	_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE
	.p2align	3
_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE:
	.quad	0
	.quad	_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE
	.quad	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.size	_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE, 24

	.type	_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE,@object # @_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE:
	.asciz	"N8NWindows16NSynchronization21CManualResetEventWFMOE"
	.size	_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE, 53

	.type	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE:
	.asciz	"N8NWindows16NSynchronization14CBaseEventWFMOE"
	.size	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE, 46

	.type	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.asciz	"N8NWindows16NSynchronization15CBaseHandleWFMOE"
	.size	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE, 47

	.type	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	3
_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE, 16

	.type	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE, 24

	.type	_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE,@object # @_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE
	.p2align	4
_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE
	.quad	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.size	_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE, 24

	.type	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	3
_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	0
	.quad	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.size	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z10CCoderInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 45

	.type	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 24

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE,@object # @_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE,@object # @_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE:
	.asciz	"13CRecordVectorIN8NArchive3N7z9CBindPairEE"
	.size	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE, 43

	.type	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE,@object # @_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CRecordVectorIiE,@object # @_ZTV13CRecordVectorIiE
	.section	.rodata._ZTV13CRecordVectorIiE,"aG",@progbits,_ZTV13CRecordVectorIiE,comdat
	.weak	_ZTV13CRecordVectorIiE
	.p2align	3
_ZTV13CRecordVectorIiE:
	.quad	0
	.quad	_ZTI13CRecordVectorIiE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIiED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIiE, 40

	.type	_ZTS13CRecordVectorIiE,@object # @_ZTS13CRecordVectorIiE
	.section	.rodata._ZTS13CRecordVectorIiE,"aG",@progbits,_ZTS13CRecordVectorIiE,comdat
	.weak	_ZTS13CRecordVectorIiE
	.p2align	4
_ZTS13CRecordVectorIiE:
	.asciz	"13CRecordVectorIiE"
	.size	_ZTS13CRecordVectorIiE, 19

	.type	_ZTI13CRecordVectorIiE,@object # @_ZTI13CRecordVectorIiE
	.section	.rodata._ZTI13CRecordVectorIiE,"aG",@progbits,_ZTI13CRecordVectorIiE,comdat
	.weak	_ZTI13CRecordVectorIiE
	.p2align	4
_ZTI13CRecordVectorIiE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIiE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIiE, 24

	.type	_ZTV13CRecordVectorIN8NArchive3N7z13CFolderRepackEE,@object # @_ZTV13CRecordVectorIN8NArchive3N7z13CFolderRepackEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive3N7z13CFolderRepackEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive3N7z13CFolderRepackEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive3N7z13CFolderRepackEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive3N7z13CFolderRepackEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive3N7z13CFolderRepackEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive3N7z13CFolderRepackEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive3N7z13CFolderRepackEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive3N7z13CFolderRepackEE,@object # @_ZTS13CRecordVectorIN8NArchive3N7z13CFolderRepackEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive3N7z13CFolderRepackEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive3N7z13CFolderRepackEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive3N7z13CFolderRepackEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive3N7z13CFolderRepackEE:
	.asciz	"13CRecordVectorIN8NArchive3N7z13CFolderRepackEE"
	.size	_ZTS13CRecordVectorIN8NArchive3N7z13CFolderRepackEE, 48

	.type	_ZTI13CRecordVectorIN8NArchive3N7z13CFolderRepackEE,@object # @_ZTI13CRecordVectorIN8NArchive3N7z13CFolderRepackEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive3N7z13CFolderRepackEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive3N7z13CFolderRepackEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive3N7z13CFolderRepackEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive3N7z13CFolderRepackEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive3N7z13CFolderRepackEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive3N7z13CFolderRepackEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z11CSolidGroupEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CSolidGroupEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z11CSolidGroupEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z11CSolidGroupEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z11CSolidGroupEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z11CSolidGroupEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z11CSolidGroupEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z11CSolidGroupEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z11CSolidGroupEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z11CSolidGroupEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z11CSolidGroupEE, 46

	.type	_ZTI13CObjectVectorIN8NArchive3N7z11CSolidGroupEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z11CSolidGroupEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z11CSolidGroupEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z11CSolidGroupEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z11CSolidGroupEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z11CSolidGroupEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z11CSolidGroupEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z11CSolidGroupEE, 24

	.type	_ZTV13CRecordVectorIbE,@object # @_ZTV13CRecordVectorIbE
	.section	.rodata._ZTV13CRecordVectorIbE,"aG",@progbits,_ZTV13CRecordVectorIbE,comdat
	.weak	_ZTV13CRecordVectorIbE
	.p2align	3
_ZTV13CRecordVectorIbE:
	.quad	0
	.quad	_ZTI13CRecordVectorIbE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIbED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIbE, 40

	.type	_ZTS13CRecordVectorIbE,@object # @_ZTS13CRecordVectorIbE
	.section	.rodata._ZTS13CRecordVectorIbE,"aG",@progbits,_ZTS13CRecordVectorIbE,comdat
	.weak	_ZTS13CRecordVectorIbE
	.p2align	4
_ZTS13CRecordVectorIbE:
	.asciz	"13CRecordVectorIbE"
	.size	_ZTS13CRecordVectorIbE, 19

	.type	_ZTI13CRecordVectorIbE,@object # @_ZTI13CRecordVectorIbE
	.section	.rodata._ZTI13CRecordVectorIbE,"aG",@progbits,_ZTI13CRecordVectorIbE,comdat
	.weak	_ZTI13CRecordVectorIbE
	.p2align	4
_ZTI13CRecordVectorIbE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIbE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIbE, 24

	.type	_ZTV13CRecordVectorIN8NArchive3N7z8CRefItemEE,@object # @_ZTV13CRecordVectorIN8NArchive3N7z8CRefItemEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive3N7z8CRefItemEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive3N7z8CRefItemEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive3N7z8CRefItemEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive3N7z8CRefItemEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive3N7z8CRefItemEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive3N7z8CRefItemEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive3N7z8CRefItemEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive3N7z8CRefItemEE,@object # @_ZTS13CRecordVectorIN8NArchive3N7z8CRefItemEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive3N7z8CRefItemEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive3N7z8CRefItemEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive3N7z8CRefItemEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive3N7z8CRefItemEE:
	.asciz	"13CRecordVectorIN8NArchive3N7z8CRefItemEE"
	.size	_ZTS13CRecordVectorIN8NArchive3N7z8CRefItemEE, 42

	.type	_ZTI13CRecordVectorIN8NArchive3N7z8CRefItemEE,@object # @_ZTI13CRecordVectorIN8NArchive3N7z8CRefItemEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive3N7z8CRefItemEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive3N7z8CRefItemEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive3N7z8CRefItemEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive3N7z8CRefItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive3N7z8CRefItemEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive3N7z8CRefItemEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
