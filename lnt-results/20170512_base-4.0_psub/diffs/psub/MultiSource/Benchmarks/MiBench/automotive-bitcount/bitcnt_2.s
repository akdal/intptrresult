	.text
	.file	"bitcnt_2.bc"
	.globl	bitcount
	.p2align	4, 0x90
	.type	bitcount,@function
bitcount:                               # @bitcount
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	shrq	%rax
	andl	$1431655765, %eax       # imm = 0x55555555
	andl	$1431655765, %edi       # imm = 0x55555555
	addq	%rax, %rdi
	movq	%rdi, %rax
	shrq	$2, %rax
	andl	$858993459, %eax        # imm = 0x33333333
	andl	$858993459, %edi        # imm = 0x33333333
	addq	%rax, %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	andl	$117901063, %eax        # imm = 0x7070707
	andl	$117901063, %edi        # imm = 0x7070707
	addq	%rax, %rdi
	movq	%rdi, %rax
	shrq	$8, %rax
	andl	$983055, %eax           # imm = 0xF000F
	andl	$983055, %edi           # imm = 0xF000F
	addq	%rax, %rdi
	movq	%rdi, %rax
	shrq	$16, %rax
	andl	$31, %edi
	addl	%edi, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end0:
	.size	bitcount, .Lfunc_end0-bitcount
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
