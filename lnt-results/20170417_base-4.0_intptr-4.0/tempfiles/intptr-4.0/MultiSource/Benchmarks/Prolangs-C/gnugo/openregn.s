	.text
	.file	"openregn.bc"
	.globl	openregion
	.p2align	4, 0x90
	.type	openregion,@function
openregion:                             # @openregion
	.cfi_startproc
# BB#0:
	cmpl	%edx, %edi
	movl	%edx, %eax
	cmovlel	%edi, %eax
	cmovll	%edx, %edi
	cmpl	%ecx, %esi
	movl	%ecx, %edx
	cmovlel	%esi, %edx
	cmovll	%ecx, %esi
	movslq	%edx, %r9
	movslq	%esi, %rcx
	movslq	%eax, %rdx
	movslq	%edi, %r8
	imulq	$19, %rdx, %rax
	leaq	p(%rax,%r9), %rsi
	decq	%r9
.LBB0_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
	movq	%rsi, %rdi
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB0_3:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rdi)
	jne	.LBB0_4
# BB#2:                                 #   in Loop: Header=BB0_3 Depth=2
	incq	%rax
	incq	%rdi
	cmpq	%rcx, %rax
	jl	.LBB0_3
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	addq	$19, %rsi
	cmpq	%r8, %rdx
	leaq	1(%rdx), %rdx
	jl	.LBB0_1
# BB#6:
	movl	$1, %eax
	retq
.LBB0_4:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	openregion, .Lfunc_end0-openregion
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
