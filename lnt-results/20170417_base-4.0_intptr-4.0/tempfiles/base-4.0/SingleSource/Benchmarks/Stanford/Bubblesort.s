	.text
	.file	"Bubblesort.bc"
	.globl	Initrand
	.p2align	4, 0x90
	.type	Initrand,@function
Initrand:                               # @Initrand
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	retq
.Lfunc_end0:
	.size	Initrand, .Lfunc_end0-Initrand
	.cfi_endproc

	.globl	Rand
	.p2align	4, 0x90
	.type	Rand,@function
Rand:                                   # @Rand
	.cfi_startproc
# BB#0:
	imull	$1309, seed(%rip), %eax # imm = 0x51D
	addl	$13849, %eax            # imm = 0x3619
	movzwl	%ax, %eax
	movq	%rax, seed(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	Rand, .Lfunc_end1-Rand
	.cfi_endproc

	.globl	bInitarr
	.p2align	4, 0x90
	.type	bInitarr,@function
bInitarr:                               # @bInitarr
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	movl	$0, biggest(%rip)
	movl	$0, littlest(%rip)
	xorl	%edx, %edx
	movq	$-499, %rax             # imm = 0xFE0D
	movl	$74755, %ecx            # imm = 0x12403
	jmp	.LBB2_1
	.p2align	4, 0x90
.LBB2_7:                                # %._crit_edge
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	biggest(%rip), %edx
	incq	%rax
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	imull	$1309, %ecx, %ecx       # imm = 0x51D
	addl	$13849, %ecx            # imm = 0x3619
	movzwl	%cx, %ecx
	leal	-50000(%rcx), %esi
	movl	%esi, sortlist+2000(,%rax,4)
	cmpl	%edx, %esi
	jle	.LBB2_3
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	$biggest, %edx
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	littlest(%rip), %esi
	jge	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	$littlest, %edx
.LBB2_5:                                # %.sink.split
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	%esi, (%rdx)
.LBB2_6:                                #   in Loop: Header=BB2_1 Depth=1
	testq	%rax, %rax
	jne	.LBB2_7
# BB#8:
	movq	%rcx, seed(%rip)
	retq
.Lfunc_end2:
	.size	bInitarr, .Lfunc_end2-bInitarr
	.cfi_endproc

	.globl	Bubble
	.p2align	4, 0x90
	.type	Bubble,@function
Bubble:                                 # @Bubble
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movq	$74755, seed(%rip)      # imm = 0x12403
	movl	$0, biggest(%rip)
	movl	$0, littlest(%rip)
	xorl	%edx, %edx
	movq	$-499, %rax             # imm = 0xFE0D
	movl	$74755, %ecx            # imm = 0x12403
	jmp	.LBB3_1
	.p2align	4, 0x90
.LBB3_7:                                # %._crit_edge.i
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	biggest(%rip), %edx
	incq	%rax
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	imull	$1309, %ecx, %ecx       # imm = 0x51D
	addl	$13849, %ecx            # imm = 0x3619
	movzwl	%cx, %ecx
	leal	-50000(%rcx), %esi
	movl	%esi, sortlist+2000(,%rax,4)
	cmpl	%edx, %esi
	jle	.LBB3_3
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	movl	$biggest, %edx
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_3:                                #   in Loop: Header=BB3_1 Depth=1
	cmpl	littlest(%rip), %esi
	jge	.LBB3_6
# BB#4:                                 #   in Loop: Header=BB3_1 Depth=1
	movl	$littlest, %edx
.LBB3_5:                                # %.sink.split.i
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	%esi, (%rdx)
.LBB3_6:                                #   in Loop: Header=BB3_1 Depth=1
	testq	%rax, %rax
	jne	.LBB3_7
# BB#8:                                 # %bInitarr.exit
	movq	%rcx, seed(%rip)
	movl	$500, top(%rip)         # imm = 0x1F4
	movl	$500, %eax              # imm = 0x1F4
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_16 Depth 2
	movl	$499, %edx              # imm = 0x1F3
	subl	%ecx, %edx
	movl	sortlist+4(%rip), %ebx
	testb	$1, %dl
	jne	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=1
	movl	$1, %esi
	cmpq	$498, %rcx              # imm = 0x1F2
	jne	.LBB3_16
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph.prol
                                        #   in Loop: Header=BB3_9 Depth=1
	movl	sortlist+8(%rip), %esi
	cmpl	%esi, %ebx
	jle	.LBB3_12
# BB#13:                                #   in Loop: Header=BB3_9 Depth=1
	movl	%esi, sortlist+4(%rip)
	movl	%ebx, sortlist+8(%rip)
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_12:                               #   in Loop: Header=BB3_9 Depth=1
	movl	%esi, %ebx
.LBB3_14:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB3_9 Depth=1
	movl	$2, %esi
	cmpq	$498, %rcx              # imm = 0x1F2
	je	.LBB3_21
	.p2align	4, 0x90
.LBB3_16:                               # %.lr.ph
                                        #   Parent Loop BB3_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	sortlist+4(,%rsi,4), %edi
	cmpl	%edi, %ebx
	jle	.LBB3_17
# BB#18:                                #   in Loop: Header=BB3_16 Depth=2
	movl	%edi, sortlist(,%rsi,4)
	movl	%ebx, sortlist+4(,%rsi,4)
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_17:                               #   in Loop: Header=BB3_16 Depth=2
	movl	%edi, %ebx
.LBB3_19:                               # %.backedge
                                        #   in Loop: Header=BB3_16 Depth=2
	leaq	2(%rsi), %rdi
	movl	sortlist+8(,%rsi,4), %edx
	cmpl	%edx, %ebx
	jle	.LBB3_20
# BB#26:                                #   in Loop: Header=BB3_16 Depth=2
	movl	%edx, sortlist+4(,%rsi,4)
	movl	%ebx, sortlist+8(,%rsi,4)
	jmp	.LBB3_27
	.p2align	4, 0x90
.LBB3_20:                               #   in Loop: Header=BB3_16 Depth=2
	movl	%edx, %ebx
.LBB3_27:                               # %.backedge.1
                                        #   in Loop: Header=BB3_16 Depth=2
	leaq	(%rcx,%rdi), %rdx
	cmpq	$500, %rdx              # imm = 0x1F4
	movq	%rdi, %rsi
	jne	.LBB3_16
.LBB3_21:                               # %._crit_edge
                                        #   in Loop: Header=BB3_9 Depth=1
	decl	%eax
	incq	%rcx
	cmpl	$1, %eax
	jg	.LBB3_9
# BB#22:
	movl	$1, top(%rip)
	movl	sortlist+4(%rip), %eax
	cmpl	littlest(%rip), %eax
	jne	.LBB3_24
# BB#23:
	movl	sortlist+2000(%rip), %eax
	cmpl	biggest(%rip), %eax
	je	.LBB3_25
.LBB3_24:
	movl	$.Lstr, %edi
	callq	puts
.LBB3_25:
	movslq	%ebp, %rax
	movl	sortlist+4(,%rax,4), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end3:
	.size	Bubble, .Lfunc_end3-Bubble
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	callq	Bubble
	incl	%ebx
	cmpl	$100, %ebx
	jne	.LBB4_1
# BB#2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	main, .Lfunc_end4-main
	.cfi_endproc

	.type	seed,@object            # @seed
	.comm	seed,8,8
	.type	biggest,@object         # @biggest
	.comm	biggest,4,4
	.type	littlest,@object        # @littlest
	.comm	littlest,4,4
	.type	sortlist,@object        # @sortlist
	.comm	sortlist,20004,16
	.type	top,@object             # @top
	.comm	top,4,4
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%d\n"
	.size	.L.str.1, 4

	.type	value,@object           # @value
	.comm	value,4,4
	.type	fixed,@object           # @fixed
	.comm	fixed,4,4
	.type	floated,@object         # @floated
	.comm	floated,4,4
	.type	permarray,@object       # @permarray
	.comm	permarray,44,16
	.type	pctr,@object            # @pctr
	.comm	pctr,4,4
	.type	tree,@object            # @tree
	.comm	tree,8,8
	.type	stack,@object           # @stack
	.comm	stack,16,16
	.type	cellspace,@object       # @cellspace
	.comm	cellspace,152,16
	.type	freelist,@object        # @freelist
	.comm	freelist,4,4
	.type	movesdone,@object       # @movesdone
	.comm	movesdone,4,4
	.type	ima,@object             # @ima
	.comm	ima,6724,16
	.type	imb,@object             # @imb
	.comm	imb,6724,16
	.type	imr,@object             # @imr
	.comm	imr,6724,16
	.type	rma,@object             # @rma
	.comm	rma,6724,16
	.type	rmb,@object             # @rmb
	.comm	rmb,6724,16
	.type	rmr,@object             # @rmr
	.comm	rmr,6724,16
	.type	piececount,@object      # @piececount
	.comm	piececount,16,16
	.type	class,@object           # @class
	.comm	class,52,16
	.type	piecemax,@object        # @piecemax
	.comm	piecemax,52,16
	.type	puzzl,@object           # @puzzl
	.comm	puzzl,2048,16
	.type	p,@object               # @p
	.comm	p,26624,16
	.type	n,@object               # @n
	.comm	n,4,4
	.type	kount,@object           # @kount
	.comm	kount,4,4
	.type	z,@object               # @z
	.comm	z,2056,16
	.type	w,@object               # @w
	.comm	w,2056,16
	.type	e,@object               # @e
	.comm	e,1040,16
	.type	zr,@object              # @zr
	.comm	zr,4,4
	.type	zi,@object              # @zi
	.comm	zi,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Error3 in Bubble."
	.size	.Lstr, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
