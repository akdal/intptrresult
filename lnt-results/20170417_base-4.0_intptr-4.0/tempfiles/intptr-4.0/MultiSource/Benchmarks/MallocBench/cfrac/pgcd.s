	.text
	.file	"pgcd.bc"
	.globl	pgcd
	.p2align	4, 0x90
	.type	pgcd,@function
pgcd:                                   # @pgcd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB0_2
# BB#1:
	incw	(%rbp)
.LBB0_2:
	movq	%rbp, %rdi
	callq	pabs
	movq	%rax, %rdi
	callq	pnew
	movq	%rax, %r12
	movq	%r12, 24(%rsp)
	testq	%r14, %r14
	je	.LBB0_4
# BB#3:
	incw	(%r14)
.LBB0_4:
	movq	%r14, %rdi
	callq	pabs
	movq	%rax, %rdi
	callq	pnew
	movq	%rax, %rbx
	movq	%rbx, 8(%rsp)
	movq	$0, 32(%rsp)
	movq	$0, 16(%rsp)
	movq	%rbx, %rdi
	callq	pcmpz
	testl	%eax, %eax
	je	.LBB0_9
# BB#5:                                 # %.lr.ph.preheader
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
	leaq	32(%rsp), %r13
	leaq	16(%rsp), %rbp
	leaq	24(%rsp), %r14
	leaq	8(%rsp), %r15
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph..lr.ph_crit_edge
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	24(%rsp), %r12
	movq	8(%rsp), %rbx
.LBB0_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%rbp, %rcx
	callq	pdivmod
	movq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	psetq
	movq	16(%rsp), %rsi
	movq	%r15, %rdi
	callq	psetq
	movq	8(%rsp), %rdi
	callq	pcmpz
	testl	%eax, %eax
	jne	.LBB0_7
# BB#8:                                 # %._crit_edge.loopexit
	movq	8(%rsp), %rbx
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB0_9:                                # %._crit_edge
	testq	%rbx, %rbx
	je	.LBB0_12
# BB#10:
	decw	(%rbx)
	jne	.LBB0_12
# BB#11:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pfree
.LBB0_12:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_15
# BB#13:
	decw	(%rdi)
	jne	.LBB0_15
# BB#14:
	xorl	%eax, %eax
	callq	pfree
.LBB0_15:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_18
# BB#16:
	decw	(%rdi)
	jne	.LBB0_18
# BB#17:
	xorl	%eax, %eax
	callq	pfree
.LBB0_18:
	testq	%rbp, %rbp
	je	.LBB0_21
# BB#19:
	decw	(%rbp)
	jne	.LBB0_21
# BB#20:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	pfree
.LBB0_21:
	testq	%r14, %r14
	je	.LBB0_24
# BB#22:
	decw	(%r14)
	jne	.LBB0_24
# BB#23:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pfree
.LBB0_24:
	movq	24(%rsp), %rdi
	callq	presult
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	pgcd, .Lfunc_end0-pgcd
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
