	.text
	.file	"formatBitstream.bc"
	.globl	InitFormatBitStream
	.p2align	4, 0x90
	.type	InitFormatBitStream,@function
InitFormatBitStream:                    # @InitFormatBitStream
	.cfi_startproc
# BB#0:
	movl	$0, BitCount(%rip)
	movl	$0, ThisFrameSize(%rip)
	movl	$0, BitsRemaining(%rip)
	retq
.Lfunc_end0:
	.size	InitFormatBitStream, .Lfunc_end0-InitFormatBitStream
	.cfi_endproc

	.globl	BF_BitstreamFrame
	.p2align	4, 0x90
	.type	BF_BitstreamFrame,@function
BF_BitstreamFrame:                      # @BF_BitstreamFrame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movq	side_queue_free(%rip), %r8
	testq	%r8, %r8
	movq	%r15, 24(%rsp)          # 8-byte Spill
	je	.LBB1_1
# BB#13:
	movq	(%r8), %rax
	movq	%rax, side_queue_free(%rip)
	movq	$0, (%r8)
	leaq	4(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	4(%r15), %edx
	movl	8(%r15), %ecx
	leaq	8(%r15), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	16(%r15), %r13
	movq	16(%r15), %r12
.LBB1_14:                               # %.loopexit171.i
	movl	(%r15), %eax
	movl	%eax, 8(%r8)
	movl	%edx, 16(%r8)
	movl	%ecx, 20(%r8)
	movq	24(%r8), %rax
	movq	8(%rax), %rcx
	movl	$0, (%rcx)
	cmpl	$0, (%r12)
	movq	%r8, 8(%rsp)            # 8-byte Spill
	je	.LBB1_20
# BB#15:                                # %.lr.ph.i.i
	xorl	%esi, %esi
	movl	$1, %ebx
	jmp	.LBB1_16
	.p2align	4, 0x90
.LBB1_121:                              # %BF_addElement.exit._crit_edge.i.i
                                        #   in Loop: Header=BB1_16 Depth=1
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
	incl	%ebx
.LBB1_16:                               # =>This Inner Loop Header: Depth=1
	leal	-1(%rbx), %r14d
	movq	8(%r12), %rbp
	leal	1(%rsi), %edx
	cmpl	(%rax), %edx
	jle	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_16 Depth=1
	addl	$9, %esi
	movq	%rax, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	BF_resizePartHolder
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
.LBB1_18:                               # %BF_addElement.exit.i.i
                                        #   in Loop: Header=BB1_16 Depth=1
	movq	8(%rcx), %rdx
	leal	1(%rsi), %edi
	movl	%edi, (%rcx)
	movl	%esi, %ecx
	movq	(%rbp,%r14,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	cmpl	(%r12), %ebx
	jb	.LBB1_121
# BB#19:
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB1_20:                               # %BF_LoadHolderFromBitstreamPart.exit.i
	movq	%rax, 24(%r8)
	movq	32(%r8), %rax
	movq	24(%r15), %r14
	movq	8(%rax), %rcx
	movl	$0, (%rcx)
	cmpl	$0, (%r14)
	je	.LBB1_26
# BB#21:                                # %.lr.ph.i100.i
	xorl	%esi, %esi
	movl	$1, %ebp
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_122:                              # %BF_addElement.exit._crit_edge.i113.i
                                        #   in Loop: Header=BB1_22 Depth=1
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
	incl	%ebp
.LBB1_22:                               # =>This Inner Loop Header: Depth=1
	leal	-1(%rbp), %r15d
	movq	8(%r14), %rbx
	leal	1(%rsi), %edx
	cmpl	(%rax), %edx
	jle	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_22 Depth=1
	addl	$9, %esi
	movq	%rax, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	BF_resizePartHolder
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
.LBB1_24:                               # %BF_addElement.exit.i108.i
                                        #   in Loop: Header=BB1_22 Depth=1
	movq	8(%rcx), %rdx
	leal	1(%rsi), %edi
	movl	%edi, (%rcx)
	movl	%esi, %ecx
	movq	(%rbx,%r15,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	cmpl	(%r14), %ebp
	jb	.LBB1_122
# BB#25:
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB1_26:                               # %BF_LoadHolderFromBitstreamPart.exit115.i
	movq	%rax, 32(%r8)
	movq	(%r13), %rdx
	movl	(%rdx), %ecx
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	movl	$0, %eax
	je	.LBB1_37
# BB#27:                                # %.lr.ph.i116.i
	movq	8(%rdx), %rdi
	leal	-1(%rcx), %esi
	incq	%rsi
	xorl	%eax, %eax
	cmpq	$8, %rsi
	jae	.LBB1_29
# BB#28:
	xorl	%esi, %esi
	movq	%rdi, %rdx
	jmp	.LBB1_35
.LBB1_1:
	movl	$1, %edi
	movl	$88, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_120
# BB#2:
	movq	16(%r15), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movslq	(%rax), %r12
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %r13
	movl	%r12d, (%r13)
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, 8(%r13)
	movl	$8, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, 8(%rbp)
	movq	%r13, 24(%r14)
	movq	24(%r15), %rax
	movslq	(%rax), %r12
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %rbp
	movl	%r12d, (%rbp)
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %r13
	movq	%r13, 8(%rbp)
	movl	$8, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, 8(%r13)
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 32(%r14)
	movslq	8(%r15), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB1_5
# BB#3:                                 # %.lr.ph193.i
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	40(%rax), %r14
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	32(%rax), %rbx
	movq	48(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movslq	(%rax), %r12
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %rbp
	movl	%r12d, (%rbp)
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %r15
	movq	%r15, 8(%rbp)
	movl	$8, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, 8(%r15)
	movq	%rbp, (%r14)
	addq	$8, %r14
	addq	$8, %rbx
	decq	%r13
	jne	.LBB1_4
.LBB1_5:                                # %.preheader170.i
	movq	24(%rsp), %r15          # 8-byte Reload
	leaq	16(%r15), %r13
	leaq	8(%r15), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	4(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movslq	4(%r15), %rdx
	testq	%rdx, %rdx
	movq	80(%rsp), %r12          # 8-byte Reload
	jle	.LBB1_6
# BB#7:                                 # %.preheader169.lr.ph.i
	movq	48(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	movq	8(%rsp), %r8            # 8-byte Reload
	jle	.LBB1_14
# BB#8:                                 # %.preheader169.us.preheader.i
	movq	%r13, 88(%rsp)          # 8-byte Spill
	movq	%r8, %r13
	addq	$56, %r13
	leaq	48(%r15), %rsi
	xorl	%edi, %edi
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_9:                                # %.preheader169.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_10 Depth 2
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%rcx, %r15
	.p2align	4, 0x90
.LBB1_10:                               #   Parent Loop BB1_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movslq	(%rax), %r12
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %rbx
	movl	%r12d, (%rbx)
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %r14
	movq	%r14, 8(%rbx)
	movl	$8, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, 8(%r14)
	movq	%rbx, (%r13)
	addq	$8, %r13
	addq	$8, %rbp
	decq	%r15
	jne	.LBB1_10
# BB#11:                                # %._crit_edge189.us.i
                                        #   in Loop: Header=BB1_9 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	incq	%rdi
	movq	32(%rsp), %r13          # 8-byte Reload
	addq	$16, %r13
	movq	40(%rsp), %rsi          # 8-byte Reload
	addq	$16, %rsi
	movq	96(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %rdi
	movq	48(%rsp), %rcx          # 8-byte Reload
	jne	.LBB1_9
# BB#12:
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	88(%rsp), %r13          # 8-byte Reload
	jmp	.LBB1_14
.LBB1_29:                               # %min.iters.checked
	movq	%rsi, %rdx
	andq	$7, %rdx
	movl	$8, %ebp
	cmovneq	%rdx, %rbp
	subq	%rbp, %rsi
	je	.LBB1_30
# BB#31:                                # %vector.body.preheader
	leaq	(%rdi,%rsi,8), %rdx
	addq	$36, %rdi
	pxor	%xmm1, %xmm1
	movq	%rsi, %rax
	pxor	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_32:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-32(%rdi), %xmm3
	movdqu	-16(%rdi), %xmm4
	movdqu	(%rdi), %xmm5
	movdqu	16(%rdi), %xmm6
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	pshufd	$232, %xmm6, %xmm4      # xmm4 = xmm6[0,2,2,3]
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshuflw	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3,4,5,6,7]
	punpckldq	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	punpcklwd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3]
	paddd	%xmm3, %xmm2
	paddd	%xmm5, %xmm0
	addq	$64, %rdi
	addq	$-8, %rax
	jne	.LBB1_32
# BB#33:                                # %middle.block
	paddd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	jmp	.LBB1_34
.LBB1_6:
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB1_14
.LBB1_30:
	xorl	%esi, %esi
	movq	%rdi, %rdx
.LBB1_34:                               # %scalar.ph.preheader
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB1_35:                               # %scalar.ph.preheader
	subl	%esi, %ecx
	addq	$4, %rdx
	.p2align	4, 0x90
.LBB1_36:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdx), %esi
	addl	%esi, %eax
	addq	$8, %rdx
	decl	%ecx
	jne	.LBB1_36
.LBB1_37:                               # %BF_PartLength.exit.i
	movq	24(%r15), %rdx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	je	.LBB1_46
# BB#38:                                # %.lr.ph.i117.i
	movq	8(%rdx), %rdi
	leal	-1(%rcx), %ebp
	incq	%rbp
	xorl	%ebx, %ebx
	cmpq	$8, %rbp
	jb	.LBB1_39
# BB#40:                                # %min.iters.checked110
	movq	%rbp, %rdx
	andq	$7, %rdx
	movl	$8, %esi
	cmovneq	%rdx, %rsi
	subq	%rsi, %rbp
	je	.LBB1_39
# BB#41:                                # %vector.body106.preheader
	leaq	(%rdi,%rbp,8), %rdx
	addq	$36, %rdi
	pxor	%xmm1, %xmm1
	movq	%rbp, %rsi
	pxor	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_42:                               # %vector.body106
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-32(%rdi), %xmm3
	movdqu	-16(%rdi), %xmm4
	movdqu	(%rdi), %xmm5
	movdqu	16(%rdi), %xmm6
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	pshufd	$232, %xmm6, %xmm4      # xmm4 = xmm6[0,2,2,3]
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshuflw	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3,4,5,6,7]
	punpckldq	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	punpcklwd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3]
	paddd	%xmm3, %xmm2
	paddd	%xmm5, %xmm0
	addq	$64, %rdi
	addq	$-8, %rsi
	jne	.LBB1_42
# BB#43:                                # %middle.block107
	paddd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	jmp	.LBB1_44
.LBB1_39:
	xorl	%ebp, %ebp
	movq	%rdi, %rdx
.LBB1_44:                               # %scalar.ph108.preheader
	subl	%ebp, %ecx
	addq	$4, %rdx
	movq	8(%rsp), %r8            # 8-byte Reload
	.p2align	4, 0x90
.LBB1_45:                               # %scalar.ph108
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdx), %esi
	addl	%esi, %ebx
	addq	$8, %rdx
	decl	%ecx
	jne	.LBB1_45
.LBB1_46:                               # %BF_PartLength.exit122.i
	addl	%eax, %ebx
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.LBB1_56
# BB#47:                                # %.lr.ph185.i.preheader
	movl	%ebx, %r12d
	xorl	%ebp, %ebp
	pxor	%xmm6, %xmm6
	.p2align	4, 0x90
.LBB1_48:                               # %.lr.ph185.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_50 Depth 2
                                        #     Child Loop BB1_73 Depth 2
                                        #     Child Loop BB1_76 Depth 2
	movq	40(%r8,%rbp,8), %rax
	movq	32(%r15,%rbp,8), %r14
	movq	8(%rax), %rcx
	movl	$0, (%rcx)
	cmpl	$0, (%r14)
	je	.LBB1_54
# BB#49:                                # %.lr.ph.i123.i
                                        #   in Loop: Header=BB1_48 Depth=1
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$1, %ebx
	jmp	.LBB1_50
	.p2align	4, 0x90
.LBB1_68:                               # %BF_addElement.exit._crit_edge.i136.i
                                        #   in Loop: Header=BB1_50 Depth=2
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
	incl	%ebx
.LBB1_50:                               #   Parent Loop BB1_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rbx), %r13d
	movq	8(%r14), %rbp
	leal	1(%rsi), %edx
	cmpl	(%rax), %edx
	jle	.LBB1_52
# BB#51:                                #   in Loop: Header=BB1_50 Depth=2
	addl	$9, %esi
	movq	%rax, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	BF_resizePartHolder
	pxor	%xmm6, %xmm6
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
.LBB1_52:                               # %BF_addElement.exit.i131.i
                                        #   in Loop: Header=BB1_50 Depth=2
	movq	8(%rcx), %rdx
	leal	1(%rsi), %edi
	movl	%edi, (%rcx)
	movl	%esi, %ecx
	movq	(%rbp,%r13,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	cmpl	(%r14), %ebx
	jb	.LBB1_68
# BB#53:                                #   in Loop: Header=BB1_48 Depth=1
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB1_54:                               # %BF_LoadHolderFromBitstreamPart.exit138.i
                                        #   in Loop: Header=BB1_48 Depth=1
	movq	%rax, 40(%r8,%rbp,8)
	movq	32(%r15,%rbp,8), %rcx
	movl	(%rcx), %eax
	testl	%eax, %eax
	je	.LBB1_55
# BB#69:                                # %.lr.ph.i139.i
                                        #   in Loop: Header=BB1_48 Depth=1
	movq	8(%rcx), %rsi
	leal	-1(%rax), %edx
	incq	%rdx
	xorl	%edi, %edi
	cmpq	$8, %rdx
	jb	.LBB1_70
# BB#71:                                # %min.iters.checked146
                                        #   in Loop: Header=BB1_48 Depth=1
	movq	%rdx, %rcx
	andq	$7, %rcx
	movl	$8, %ebx
	cmoveq	%rbx, %rcx
	subq	%rcx, %rdx
	je	.LBB1_70
# BB#72:                                # %vector.body142.preheader
                                        #   in Loop: Header=BB1_48 Depth=1
	leaq	(%rsi,%rdx,8), %rcx
	addq	$36, %rsi
	pxor	%xmm0, %xmm0
	movq	%rdx, %rdi
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB1_73:                               # %vector.body142
                                        #   Parent Loop BB1_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-32(%rsi), %xmm2
	movdqu	-16(%rsi), %xmm3
	movdqu	(%rsi), %xmm4
	movdqu	16(%rsi), %xmm5
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3,4,5,6,7]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	punpcklwd	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1],xmm2[2],xmm6[2],xmm2[3],xmm6[3]
	pshufd	$232, %xmm5, %xmm3      # xmm3 = xmm5[0,2,2,3]
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	punpckldq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	punpcklwd	%xmm6, %xmm4    # xmm4 = xmm4[0],xmm6[0],xmm4[1],xmm6[1],xmm4[2],xmm6[2],xmm4[3],xmm6[3]
	paddd	%xmm2, %xmm0
	paddd	%xmm4, %xmm1
	addq	$64, %rsi
	addq	$-8, %rdi
	jne	.LBB1_73
# BB#74:                                # %middle.block143
                                        #   in Loop: Header=BB1_48 Depth=1
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edi
	jmp	.LBB1_75
	.p2align	4, 0x90
.LBB1_70:                               #   in Loop: Header=BB1_48 Depth=1
	xorl	%edx, %edx
	movq	%rsi, %rcx
.LBB1_75:                               # %scalar.ph144.preheader
                                        #   in Loop: Header=BB1_48 Depth=1
	subl	%edx, %eax
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB1_76:                               # %scalar.ph144
                                        #   Parent Loop BB1_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rcx), %edx
	addl	%edx, %edi
	addq	$8, %rcx
	decl	%eax
	jne	.LBB1_76
	jmp	.LBB1_77
	.p2align	4, 0x90
.LBB1_55:                               #   in Loop: Header=BB1_48 Depth=1
	xorl	%edi, %edi
.LBB1_77:                               # %BF_PartLength.exit144.i
                                        #   in Loop: Header=BB1_48 Depth=1
	addl	%edi, %r12d
	incq	%rbp
	movq	56(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %rax
	cmpq	%rax, %rbp
	jl	.LBB1_48
# BB#78:
	movl	%r12d, %ebx
.LBB1_56:                               # %.preheader168.i
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jle	.LBB1_90
# BB#57:                                # %.preheader167.i.preheader
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	pxor	%xmm6, %xmm6
	.p2align	4, 0x90
.LBB1_58:                               # %.preheader167.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_60 Depth 2
                                        #       Child Loop BB1_62 Depth 3
                                        #       Child Loop BB1_84 Depth 3
                                        #       Child Loop BB1_87 Depth 3
	testl	%eax, %eax
	jle	.LBB1_89
# BB#59:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_58 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_60:                               # %.lr.ph.i
                                        #   Parent Loop BB1_58 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_62 Depth 3
                                        #       Child Loop BB1_84 Depth 3
                                        #       Child Loop BB1_87 Depth 3
	movq	16(%rsp), %rcx          # 8-byte Reload
	shlq	$4, %rcx
	leaq	(%r8,%rcx), %rax
	leaq	56(%rax,%r12,8), %rdx
	movq	56(%rax,%r12,8), %rax
	addq	%r15, %rcx
	leaq	48(%rcx,%r12,8), %r13
	movq	48(%rcx,%r12,8), %r14
	movq	8(%rax), %rcx
	movl	$0, (%rcx)
	cmpl	$0, (%r14)
	je	.LBB1_66
# BB#61:                                # %.lr.ph.i145.i
                                        #   in Loop: Header=BB1_60 Depth=2
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	xorl	%esi, %esi
	movl	$1, %ebx
	jmp	.LBB1_62
	.p2align	4, 0x90
.LBB1_79:                               # %BF_addElement.exit._crit_edge.i158.i
                                        #   in Loop: Header=BB1_62 Depth=3
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
	incl	%ebx
.LBB1_62:                               #   Parent Loop BB1_58 Depth=1
                                        #     Parent Loop BB1_60 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%rbx), %ebp
	movq	8(%r14), %r15
	leal	1(%rsi), %edx
	cmpl	(%rax), %edx
	jle	.LBB1_64
# BB#63:                                #   in Loop: Header=BB1_62 Depth=3
	addl	$9, %esi
	movq	%rax, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	BF_resizePartHolder
	pxor	%xmm6, %xmm6
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
.LBB1_64:                               # %BF_addElement.exit.i153.i
                                        #   in Loop: Header=BB1_62 Depth=3
	movq	8(%rcx), %rdx
	leal	1(%rsi), %edi
	movl	%edi, (%rcx)
	movl	%esi, %ecx
	movq	(%r15,%rbp,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	cmpl	(%r14), %ebx
	jb	.LBB1_79
# BB#65:                                #   in Loop: Header=BB1_60 Depth=2
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	32(%rsp), %ebx          # 4-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
.LBB1_66:                               # %BF_LoadHolderFromBitstreamPart.exit160.i
                                        #   in Loop: Header=BB1_60 Depth=2
	movq	%rax, (%rdx)
	movq	(%r13), %rcx
	movl	(%rcx), %eax
	testl	%eax, %eax
	je	.LBB1_67
# BB#80:                                # %.lr.ph.i161.i
                                        #   in Loop: Header=BB1_60 Depth=2
	movq	8(%rcx), %rsi
	leal	-1(%rax), %edx
	incq	%rdx
	xorl	%edi, %edi
	cmpq	$8, %rdx
	jb	.LBB1_81
# BB#82:                                # %min.iters.checked182
                                        #   in Loop: Header=BB1_60 Depth=2
	movq	%rdx, %rcx
	andq	$7, %rcx
	movl	$8, %ebp
	cmoveq	%rbp, %rcx
	subq	%rcx, %rdx
	je	.LBB1_81
# BB#83:                                # %vector.body178.preheader
                                        #   in Loop: Header=BB1_60 Depth=2
	leaq	(%rsi,%rdx,8), %rcx
	addq	$36, %rsi
	pxor	%xmm0, %xmm0
	movq	%rdx, %rdi
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB1_84:                               # %vector.body178
                                        #   Parent Loop BB1_58 Depth=1
                                        #     Parent Loop BB1_60 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	-32(%rsi), %xmm2
	movdqu	-16(%rsi), %xmm3
	movdqu	(%rsi), %xmm4
	movdqu	16(%rsi), %xmm5
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3,4,5,6,7]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	punpcklwd	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1],xmm2[2],xmm6[2],xmm2[3],xmm6[3]
	pshufd	$232, %xmm5, %xmm3      # xmm3 = xmm5[0,2,2,3]
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	punpckldq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	punpcklwd	%xmm6, %xmm4    # xmm4 = xmm4[0],xmm6[0],xmm4[1],xmm6[1],xmm4[2],xmm6[2],xmm4[3],xmm6[3]
	paddd	%xmm2, %xmm0
	paddd	%xmm4, %xmm1
	addq	$64, %rsi
	addq	$-8, %rdi
	jne	.LBB1_84
# BB#85:                                # %middle.block179
                                        #   in Loop: Header=BB1_60 Depth=2
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edi
	jmp	.LBB1_86
	.p2align	4, 0x90
.LBB1_81:                               #   in Loop: Header=BB1_60 Depth=2
	xorl	%edx, %edx
	movq	%rsi, %rcx
.LBB1_86:                               # %scalar.ph180.preheader
                                        #   in Loop: Header=BB1_60 Depth=2
	subl	%edx, %eax
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB1_87:                               # %scalar.ph180
                                        #   Parent Loop BB1_58 Depth=1
                                        #     Parent Loop BB1_60 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%rcx), %edx
	addl	%edx, %edi
	addq	$8, %rcx
	decl	%eax
	jne	.LBB1_87
	jmp	.LBB1_88
	.p2align	4, 0x90
.LBB1_67:                               #   in Loop: Header=BB1_60 Depth=2
	xorl	%edi, %edi
.LBB1_88:                               # %BF_PartLength.exit166.i
                                        #   in Loop: Header=BB1_60 Depth=2
	addl	%edi, %ebx
	incq	%r12
	movq	56(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %rax
	cmpq	%rax, %r12
	jl	.LBB1_60
.LBB1_89:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_58 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rsi
	incq	%rsi
	movq	72(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx), %rcx
	movq	%rsi, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	cmpq	%rcx, %rsi
	jl	.LBB1_58
.LBB1_90:                               # %._crit_edge181.i
	movl	%ebx, 12(%r8)
	movq	side_queue_head(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB1_91
	.p2align	4, 0x90
.LBB1_92:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB1_92
	jmp	.LBB1_93
.LBB1_91:
	movl	$side_queue_head, %edx
.LBB1_93:                               # %store_side_info.exit
	movq	%r8, (%rdx)
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	%ebx, (%rdx)
	movl	$0, 4(%rdx)
	testl	%ecx, %ecx
	movl	$0, %r13d
	jle	.LBB1_111
# BB#94:                                # %.preheader.lr.ph.i
	xorl	%edx, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_95:                               # %.preheader.i7
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_98 Depth 2
                                        #       Child Loop BB1_100 Depth 3
                                        #       Child Loop BB1_103 Depth 3
                                        #       Child Loop BB1_107 Depth 3
	testl	%eax, %eax
	jle	.LBB1_96
# BB#97:                                # %.lr.ph.i9.preheader
                                        #   in Loop: Header=BB1_95 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_98:                               # %.lr.ph.i9
                                        #   Parent Loop BB1_95 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_100 Depth 3
                                        #       Child Loop BB1_103 Depth 3
                                        #       Child Loop BB1_107 Depth 3
	movl	%edx, 40(%rsp)          # 4-byte Spill
	movq	16(%rsp), %r14          # 8-byte Reload
	shlq	$4, %r14
	addq	%r15, %r14
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	80(%r14,%r12,8), %r12
	xorl	%ebx, %ebx
	cmpl	$0, (%r12)
	movl	$0, %r13d
	je	.LBB1_101
# BB#99:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB1_98 Depth=2
	movq	8(%r12), %r15
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_100:                              # %.lr.ph.i.i10
                                        #   Parent Loop BB1_95 Depth=1
                                        #     Parent Loop BB1_98 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r15,%rbp,8), %edi
	movzwl	4(%r15,%rbp,8), %esi
	callq	WriteMainDataBits
	movzwl	4(%r15,%rbp,8), %eax
	addl	%eax, %r13d
	incq	%rbp
	cmpl	(%r12), %ebp
	jb	.LBB1_100
.LBB1_101:                              # %writePartMainData.exit.i
                                        #   in Loop: Header=BB1_98 Depth=2
	addl	40(%rsp), %r13d         # 4-byte Folded Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	112(%r14,%rax,8), %r15
	cmpl	$0, (%r15)
	je	.LBB1_104
# BB#102:                               # %.lr.ph.preheader.i38.i
                                        #   in Loop: Header=BB1_98 Depth=2
	movq	8(%r15), %r12
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_103:                              # %.lr.ph.i42.i
                                        #   Parent Loop BB1_95 Depth=1
                                        #     Parent Loop BB1_98 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r12,%rbp,8), %edi
	movzwl	4(%r12,%rbp,8), %esi
	callq	WriteMainDataBits
	movzwl	4(%r12,%rbp,8), %eax
	addl	%eax, %ebx
	incq	%rbp
	cmpl	(%r15), %ebp
	jb	.LBB1_103
.LBB1_104:                              # %writePartMainData.exit44.i
                                        #   in Loop: Header=BB1_98 Depth=2
	addl	%ebx, %r13d
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	144(%r14,%r12,8), %r14
	cmpl	$0, (%r14)
	je	.LBB1_105
# BB#106:                               # %.lr.ph.preheader.i45.i
                                        #   in Loop: Header=BB1_98 Depth=2
	movq	8(%r14), %r15
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_107:                              # %.lr.ph.i49.i
                                        #   Parent Loop BB1_95 Depth=1
                                        #     Parent Loop BB1_98 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r15,%rbp,8), %edi
	movzwl	4(%r15,%rbp,8), %esi
	callq	WriteMainDataBits
	movzwl	4(%r15,%rbp,8), %eax
	addl	%eax, %ebx
	incq	%rbp
	cmpl	(%r14), %ebp
	jb	.LBB1_107
	jmp	.LBB1_108
	.p2align	4, 0x90
.LBB1_105:                              #   in Loop: Header=BB1_98 Depth=2
	xorl	%ebx, %ebx
.LBB1_108:                              # %writePartMainData.exit51.i
                                        #   in Loop: Header=BB1_98 Depth=2
	addl	%ebx, %r13d
	incq	%r12
	movq	24(%rsp), %r15          # 8-byte Reload
	movslq	8(%r15), %rax
	cmpq	%rax, %r12
	movl	%r13d, %edx
	jl	.LBB1_98
# BB#109:                               # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB1_95 Depth=1
	movl	4(%r15), %ecx
	jmp	.LBB1_110
	.p2align	4, 0x90
.LBB1_96:                               #   in Loop: Header=BB1_95 Depth=1
	movl	%edx, %r13d
.LBB1_110:                              # %._crit_edge.i14
                                        #   in Loop: Header=BB1_95 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rdi
	incq	%rdi
	movslq	%ecx, %rdx
	movq	%rdi, %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rdi
	movl	%r13d, %edx
	jl	.LBB1_95
.LBB1_111:                              # %._crit_edge66.i
	movq	176(%r15), %r14
	cmpl	$0, (%r14)
	movl	$0, %ebp
	je	.LBB1_114
# BB#112:                               # %.lr.ph.preheader.i52.i
	movq	8(%r14), %r15
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_113:                              # %.lr.ph.i56.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rbx,8), %edi
	movzwl	4(%r15,%rbx,8), %esi
	callq	WriteMainDataBits
	movzwl	4(%r15,%rbx,8), %eax
	addl	%eax, %ebp
	incq	%rbx
	cmpl	(%r14), %ebx
	jb	.LBB1_113
.LBB1_114:                              # %main_data.exit
	addl	%r13d, %ebp
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	%ebp, 4(%rax)
	movl	$0, forwardFrameLength(%rip)
	movl	$0, forwardSILength(%rip)
	movq	side_queue_head(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB1_115
# BB#116:                               # %.lr.ph.i15.preheader
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_117:                              # %.lr.ph.i15
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	addl	8(%rdx), %esi
	addl	12(%rdx), %ecx
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_117
# BB#118:                               # %._crit_edge.i16
	movl	%esi, forwardFrameLength(%rip)
	movl	%ecx, forwardSILength(%rip)
	movl	%esi, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%esi, %edx
	sarl	$3, %edx
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$29, %esi
	addl	%ecx, %esi
	sarl	$3, %esi
	negl	%esi
	jmp	.LBB1_119
.LBB1_115:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
.LBB1_119:                              # %side_queue_elements.exit
	movl	%eax, elements(%rip)
	movl	BitsRemaining(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	addl	%edx, %ecx
	addl	%esi, %ecx
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	%ecx, 8(%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_120:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	BF_BitstreamFrame, .Lfunc_end1-BF_BitstreamFrame
	.cfi_endproc

	.globl	BF_FlushBitstream
	.p2align	4, 0x90
	.type	BF_FlushBitstream,@function
BF_FlushBitstream:                      # @BF_FlushBitstream
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	cmpl	$0, elements(%rip)
	je	.LBB2_5
# BB#1:
	movl	forwardFrameLength(%rip), %ebx
	subl	forwardSILength(%rip), %ebx
	leal	31(%rbx), %eax
	cmpl	$63, %eax
	jb	.LBB2_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%ebx, %ebp
	sarl	$31, %ebp
	shrl	$27, %ebp
	addl	%ebx, %ebp
	sarl	$5, %ebp
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movl	$32, %esi
	callq	WriteMainDataBits
	decl	%ebp
	jne	.LBB2_3
.LBB2_4:                                # %._crit_edge
	movl	%ebx, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	addl	%ebx, %eax
	andl	$-32, %eax
	subl	%eax, %ebx
	xorl	%edi, %edi
	movl	%ebx, %esi
	callq	WriteMainDataBits
.LBB2_5:
	movl	forwardFrameLength(%rip), %eax
	movl	forwardSILength(%rip), %ecx
	subl	%ecx, %eax
	movl	%eax, 4(%r14)
	movl	%ecx, (%r14)
	movl	$0, 8(%r14)
	movq	side_queue_head(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_7
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph14.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	free_side_info_link
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB2_6
.LBB2_7:                                # %._crit_edge15.i
	movq	$0, side_queue_head(%rip)
	movq	side_queue_free(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_9
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	free_side_info_link
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB2_8
.LBB2_9:                                # %free_side_queues.exit
	movq	$0, side_queue_free(%rip)
	movl	$0, BitCount(%rip)
	movl	$0, ThisFrameSize(%rip)
	movl	$0, BitsRemaining(%rip)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	BF_FlushBitstream, .Lfunc_end2-BF_FlushBitstream
	.cfi_endproc

	.p2align	4, 0x90
	.type	WriteMainDataBits,@function
WriteMainDataBits:                      # @WriteMainDataBits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movl	%edi, %ebp
	testl	%ebx, %ebx
	je	.LBB3_7
# BB#1:
	movl	BitCount(%rip), %eax
	cmpl	ThisFrameSize(%rip), %eax
	jne	.LBB3_2
# BB#3:
	callq	write_side_info
	movl	%eax, BitCount(%rip)
	movl	ThisFrameSize(%rip), %esi
	subl	%eax, %esi
	movl	%esi, BitsRemaining(%rip)
	cmpl	%ebx, %esi
	jb	.LBB3_5
	jmp	.LBB3_6
.LBB3_2:                                # %._crit_edge
	movl	BitsRemaining(%rip), %esi
	cmpl	%ebx, %esi
	jae	.LBB3_6
.LBB3_5:
	subl	%esi, %ebx
	movl	%ebp, %edi
	movl	%ebx, %ecx
	shrl	%cl, %edi
	callq	putMyBits
	callq	write_side_info
	movl	%eax, BitCount(%rip)
	movl	ThisFrameSize(%rip), %ecx
	subl	%eax, %ecx
	movl	%ecx, BitsRemaining(%rip)
.LBB3_6:
	movl	%ebp, %edi
	movl	%ebx, %esi
	callq	putMyBits
	addl	%ebx, BitCount(%rip)
	subl	%ebx, BitsRemaining(%rip)
.LBB3_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end3:
	.size	WriteMainDataBits, .Lfunc_end3-WriteMainDataBits
	.cfi_endproc

	.globl	BF_PartLength
	.p2align	4, 0x90
	.type	BF_PartLength,@function
BF_PartLength:                          # @BF_PartLength
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	je	.LBB4_1
# BB#2:                                 # %.lr.ph
	movq	8(%rdi), %rdi
	leal	-1(%rcx), %edx
	incq	%rdx
	xorl	%eax, %eax
	cmpq	$8, %rdx
	jb	.LBB4_3
# BB#4:                                 # %min.iters.checked
	movq	%rdx, %r8
	andq	$7, %r8
	movl	$8, %esi
	cmovneq	%r8, %rsi
	subq	%rsi, %rdx
	je	.LBB4_3
# BB#5:                                 # %vector.body.preheader
	leaq	(%rdi,%rdx,8), %rsi
	addq	$36, %rdi
	pxor	%xmm1, %xmm1
	movq	%rdx, %rax
	pxor	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_6:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-32(%rdi), %xmm3
	movdqu	-16(%rdi), %xmm4
	movdqu	(%rdi), %xmm5
	movdqu	16(%rdi), %xmm6
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	pshufd	$232, %xmm6, %xmm4      # xmm4 = xmm6[0,2,2,3]
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshuflw	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3,4,5,6,7]
	punpckldq	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	punpcklwd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3]
	paddd	%xmm3, %xmm2
	paddd	%xmm5, %xmm0
	addq	$64, %rdi
	addq	$-8, %rax
	jne	.LBB4_6
# BB#7:                                 # %middle.block
	paddd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	jmp	.LBB4_8
.LBB4_3:
	xorl	%edx, %edx
	movq	%rdi, %rsi
.LBB4_8:                                # %scalar.ph.preheader
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB4_9:                                # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rsi), %edi
	addl	%edi, %eax
	incl	%edx
	addq	$8, %rsi
	cmpl	%ecx, %edx
	jb	.LBB4_9
# BB#10:                                # %._crit_edge
	retq
.LBB4_1:
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	BF_PartLength, .Lfunc_end4-BF_PartLength
	.cfi_endproc

	.globl	BF_newPartHolder
	.p2align	4, 0x90
	.type	BF_newPartHolder,@function
BF_newPartHolder:                       # @BF_newPartHolder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %rbx
	movl	%r14d, (%rbx)
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, 8(%rbx)
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, 8(%rbp)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	BF_newPartHolder, .Lfunc_end5-BF_newPartHolder
	.cfi_endproc

	.globl	BF_NewHolderFromBitstreamPart
	.p2align	4, 0x90
	.type	BF_NewHolderFromBitstreamPart,@function
BF_NewHolderFromBitstreamPart:          # @BF_NewHolderFromBitstreamPart
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	(%r14), %r12
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %r15
	movl	%r12d, (%r15)
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, 8(%r15)
	movl	$8, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, 8(%rbx)
	cmpl	$0, (%r14)
	je	.LBB6_6
# BB#1:                                 # %.lr.ph.i
	xorl	%esi, %esi
	movl	$1, %r12d
	jmp	.LBB6_2
	.p2align	4, 0x90
.LBB6_5:                                # %BF_addElement.exit._crit_edge.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	8(%r15), %rbx
	movl	(%rbx), %esi
	incl	%r12d
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	leal	-1(%r12), %r13d
	movq	8(%r14), %rbp
	leal	1(%rsi), %eax
	cmpl	(%r15), %eax
	jle	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	addl	$9, %esi
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	BF_resizePartHolder
	movq	%rax, %r15
	movq	8(%r15), %rbx
	movl	(%rbx), %esi
.LBB6_4:                                # %BF_addElement.exit.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rbx), %rax
	leal	1(%rsi), %ecx
	movl	%ecx, (%rbx)
	movl	%esi, %ecx
	movq	(%rbp,%r13,8), %rdx
	movq	%rdx, (%rax,%rcx,8)
	cmpl	(%r14), %r12d
	jb	.LBB6_5
.LBB6_6:                                # %BF_LoadHolderFromBitstreamPart.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	BF_NewHolderFromBitstreamPart, .Lfunc_end6-BF_NewHolderFromBitstreamPart
	.cfi_endproc

	.globl	BF_LoadHolderFromBitstreamPart
	.p2align	4, 0x90
	.type	BF_LoadHolderFromBitstreamPart,@function
BF_LoadHolderFromBitstreamPart:         # @BF_LoadHolderFromBitstreamPart
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	8(%rdi), %rax
	movl	$0, (%rax)
	cmpl	$0, (%r14)
	je	.LBB7_6
# BB#1:                                 # %.lr.ph
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_5:                                # %BF_addElement.exit._crit_edge
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	8(%rdi), %rax
	movl	(%rax), %esi
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %r15
	movl	%ecx, %ebx
	leal	1(%rsi), %ecx
	cmpl	(%rdi), %ecx
	jle	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	addl	$9, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	BF_resizePartHolder
	movq	%rax, %rdi
	movq	8(%rdi), %rax
	movl	(%rax), %esi
.LBB7_4:                                # %BF_addElement.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	8(%rax), %rcx
	leal	1(%rsi), %edx
	movl	%edx, (%rax)
	movl	%esi, %eax
	movq	(%r15,%rbx,8), %rdx
	movq	%rdx, (%rcx,%rax,8)
	leal	1(%rbx), %ecx
	cmpl	(%r14), %ecx
	jb	.LBB7_5
.LBB7_6:                                # %._crit_edge
	movq	%rdi, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	BF_LoadHolderFromBitstreamPart, .Lfunc_end7-BF_LoadHolderFromBitstreamPart
	.cfi_endproc

	.globl	BF_addElement
	.p2align	4, 0x90
	.type	BF_addElement,@function
BF_addElement:                          # @BF_addElement
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 16
.Lcfi50:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	8(%rdi), %rax
	movl	(%rax), %esi
	leal	1(%rsi), %ecx
	cmpl	(%rdi), %ecx
	jle	.LBB8_2
# BB#1:
	addl	$9, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	BF_resizePartHolder
	movq	%rax, %rdi
	movq	8(%rdi), %rax
	movl	(%rax), %esi
.LBB8_2:
	movq	8(%rax), %rcx
	leal	1(%rsi), %edx
	movl	%edx, (%rax)
	movl	%esi, %eax
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx,%rax,8)
	movq	%rdi, %rax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	BF_addElement, .Lfunc_end8-BF_addElement
	.cfi_endproc

	.globl	BF_resizePartHolder
	.p2align	4, 0x90
	.type	BF_resizePartHolder,@function
BF_resizePartHolder:                    # @BF_resizePartHolder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 48
.Lcfi56:
	.cfi_offset %rbx, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %r15
	movl	%ebp, (%r15)
	movl	$1, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, 8(%r15)
	movslq	%ebp, %rbp
	movl	$8, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, 8(%rbx)
	movl	(%r14), %eax
	cmpl	%ebp, %eax
	cmovgl	%ebp, %eax
	movl	%eax, (%rbx)
	leaq	8(%r14), %rbp
	testl	%eax, %eax
	jle	.LBB9_5
# BB#1:                                 # %.lr.ph
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB9_3
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	(%rbp), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%rdi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB9_2
.LBB9_3:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB9_5
	.p2align	4, 0x90
.LBB9_4:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdx
	movq	(%rbp), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	movq	8(%rbx), %rdx
	movq	(%rbp), %rsi
	movq	8(%rsi), %rsi
	movq	8(%rsi,%rcx,8), %rsi
	movq	%rsi, 8(%rdx,%rcx,8)
	movq	8(%rbx), %rdx
	movq	(%rbp), %rsi
	movq	8(%rsi), %rsi
	movq	16(%rsi,%rcx,8), %rsi
	movq	%rsi, 16(%rdx,%rcx,8)
	movq	8(%rbx), %rdx
	movq	(%rbp), %rsi
	movq	8(%rsi), %rsi
	movq	24(%rsi,%rcx,8), %rsi
	movq	%rsi, 24(%rdx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rax, %rcx
	jl	.LBB9_4
.LBB9_5:                                # %._crit_edge
	movq	(%rbp), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	(%rbp), %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	BF_resizePartHolder, .Lfunc_end9-BF_resizePartHolder
	.cfi_endproc

	.globl	BF_freePartHolder
	.p2align	4, 0x90
	.type	BF_freePartHolder,@function
BF_freePartHolder:                      # @BF_freePartHolder
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 16
.Lcfi61:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	BF_freePartHolder, .Lfunc_end10-BF_freePartHolder
	.cfi_endproc

	.globl	BF_addEntry
	.p2align	4, 0x90
	.type	BF_addEntry,@function
BF_addEntry:                            # @BF_addEntry
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 32
.Lcfi65:
	.cfi_offset %rbx, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %ebx
	testl	%ebp, %ebp
	je	.LBB11_4
# BB#1:
	movq	8(%rdi), %rax
	movl	(%rax), %esi
	leal	1(%rsi), %ecx
	cmpl	(%rdi), %ecx
	jle	.LBB11_3
# BB#2:
	addl	$9, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	BF_resizePartHolder
	movq	%rax, %rdi
	movq	8(%rdi), %rax
	movl	(%rax), %esi
.LBB11_3:                               # %BF_addElement.exit
	movq	8(%rax), %rcx
	leal	1(%rsi), %edx
	movl	%edx, (%rax)
	movl	%esi, %eax
	movzwl	%bp, %edx
	shlq	$32, %rdx
	movl	%ebx, %esi
	orq	%rdx, %rsi
	movq	%rsi, (%rcx,%rax,8)
.LBB11_4:
	movq	%rdi, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end11:
	.size	BF_addEntry, .Lfunc_end11-BF_addEntry
	.cfi_endproc

	.p2align	4, 0x90
	.type	write_side_info,@function
write_side_info:                        # @write_side_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 80
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	side_queue_free(%rip), %rax
	movq	side_queue_head(%rip), %rdx
	movq	(%rdx), %rcx
	movq	%rcx, side_queue_head(%rip)
	movq	%rdx, side_queue_free(%rip)
	movq	%rax, (%rdx)
	movl	8(%rdx), %eax
	movl	%eax, ThisFrameSize(%rip)
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	24(%rdx), %rax
	movq	8(%rax), %r12
	xorl	%r15d, %r15d
	cmpl	$0, (%r12)
	movl	$0, %ebx
	je	.LBB12_3
# BB#1:                                 # %.lr.ph.preheader.i37
	movq	8(%r12), %r14
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_2:                               # %.lr.ph.i41
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rbp,8), %edi
	movzwl	4(%r14,%rbp,8), %esi
	callq	putMyBits
	movzwl	4(%r14,%rbp,8), %eax
	addl	%eax, %ebx
	incq	%rbp
	cmpl	(%r12), %ebp
	jb	.LBB12_2
.LBB12_3:                               # %writePartSideInfo.exit43
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	32(%rax), %rax
	movq	8(%rax), %r12
	cmpl	$0, (%r12)
	je	.LBB12_6
# BB#4:                                 # %.lr.ph.preheader.i44
	movq	8(%r12), %r14
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB12_5:                               # %.lr.ph.i48
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rbp,8), %edi
	movzwl	4(%r14,%rbp,8), %esi
	callq	putMyBits
	movzwl	4(%r14,%rbp,8), %eax
	addl	%eax, %r15d
	incq	%rbp
	cmpl	(%r12), %ebp
	jb	.LBB12_5
.LBB12_6:                               # %writePartSideInfo.exit50
	addl	%ebx, %r15d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	20(%rsi), %eax
	testl	%eax, %eax
	jle	.LBB12_7
# BB#14:                                # %.lr.ph71.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB12_15:                              # %.lr.ph71
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_17 Depth 2
	movq	40(%rsi,%r13,8), %rcx
	movq	8(%rcx), %r14
	cmpl	$0, (%r14)
	movl	$0, %r12d
	je	.LBB12_19
# BB#16:                                # %.lr.ph.preheader.i51
                                        #   in Loop: Header=BB12_15 Depth=1
	movq	8(%r14), %rbp
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB12_17:                              # %.lr.ph.i55
                                        #   Parent Loop BB12_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rbx,8), %edi
	movzwl	4(%rbp,%rbx,8), %esi
	callq	putMyBits
	movzwl	4(%rbp,%rbx,8), %eax
	addl	%eax, %r12d
	incq	%rbx
	cmpl	(%r14), %ebx
	jb	.LBB12_17
# BB#18:                                # %writePartSideInfo.exit57.loopexit
                                        #   in Loop: Header=BB12_15 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	20(%rsi), %eax
.LBB12_19:                              # %writePartSideInfo.exit57
                                        #   in Loop: Header=BB12_15 Depth=1
	addl	%r15d, %r12d
	incq	%r13
	movslq	%eax, %rcx
	cmpq	%rcx, %r13
	movl	%r12d, %r15d
	jl	.LBB12_15
	jmp	.LBB12_8
.LBB12_7:
	movl	%r15d, %r12d
.LBB12_8:                               # %.preheader58
	movl	16(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.LBB12_26
# BB#9:                                 # %.preheader.preheader
	xorl	%edx, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB12_10:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_12 Depth 2
                                        #       Child Loop BB12_21 Depth 3
	testl	%eax, %eax
	jle	.LBB12_25
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB12_10 Depth=1
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB12_12:                              # %.lr.ph
                                        #   Parent Loop BB12_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_21 Depth 3
	movq	16(%rsp), %rcx          # 8-byte Reload
	shlq	$4, %rcx
	addq	%rsi, %rcx
	movq	56(%rcx,%r13,8), %rcx
	movq	8(%rcx), %rbp
	cmpl	$0, (%rbp)
	je	.LBB12_13
# BB#20:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB12_12 Depth=2
	movq	8(%rbp), %r14
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_21:                              # %.lr.ph.i
                                        #   Parent Loop BB12_10 Depth=1
                                        #     Parent Loop BB12_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r14,%r15,8), %edi
	movzwl	4(%r14,%r15,8), %esi
	callq	putMyBits
	movzwl	4(%r14,%r15,8), %eax
	addl	%eax, %ebx
	incq	%r15
	cmpl	(%rbp), %r15d
	jb	.LBB12_21
# BB#22:                                # %writePartSideInfo.exit.loopexit
                                        #   in Loop: Header=BB12_12 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	20(%rsi), %eax
	jmp	.LBB12_23
	.p2align	4, 0x90
.LBB12_13:                              #   in Loop: Header=BB12_12 Depth=2
	xorl	%ebx, %ebx
.LBB12_23:                              # %writePartSideInfo.exit
                                        #   in Loop: Header=BB12_12 Depth=2
	addl	%ebx, %r12d
	incq	%r13
	movslq	%eax, %rcx
	cmpq	%rcx, %r13
	jl	.LBB12_12
# BB#24:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB12_10 Depth=1
	movl	16(%rsi), %ecx
.LBB12_25:                              # %._crit_edge
                                        #   in Loop: Header=BB12_10 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rbp
	incq	%rbp
	movslq	%ecx, %rdx
	movq	%rbp, %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rbp
	jl	.LBB12_10
.LBB12_26:                              # %._crit_edge67
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	write_side_info, .Lfunc_end12-write_side_info
	.cfi_endproc

	.p2align	4, 0x90
	.type	free_side_info_link,@function
free_side_info_link:                    # @free_side_info_link
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi83:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi84:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 64
.Lcfi87:
	.cfi_offset %rbx, -56
.Lcfi88:
	.cfi_offset %r12, -48
.Lcfi89:
	.cfi_offset %r13, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	24(%r15), %r14
	movq	8(%r14), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%r14), %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	$0, 24(%r15)
	movq	32(%r15), %r14
	movq	8(%r14), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%r14), %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	$0, 32(%r15)
	movl	20(%r15), %eax
	testl	%eax, %eax
	jle	.LBB13_3
# BB#1:                                 # %.lr.ph33.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph33
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%r15,%rbx,8), %r14
	movq	8(%r14), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%r14), %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	$0, 40(%r15,%rbx,8)
	incq	%rbx
	movslq	20(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB13_2
.LBB13_3:                               # %.preheader27
	movl	16(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB13_10
# BB#4:                                 # %.preheader.preheader
	leaq	56(%r15), %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB13_5:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_7 Depth 2
	testl	%eax, %eax
	jle	.LBB13_9
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB13_5 Depth=1
	movq	%r14, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_7:                               # %.lr.ph
                                        #   Parent Loop BB13_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %r13
	movq	8(%r13), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%r13), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	$0, (%rbx)
	incq	%rbp
	movslq	20(%r15), %rax
	addq	$8, %rbx
	cmpq	%rax, %rbp
	jl	.LBB13_7
# BB#8:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB13_5 Depth=1
	movl	16(%r15), %ecx
.LBB13_9:                               # %._crit_edge
                                        #   in Loop: Header=BB13_5 Depth=1
	incq	%r12
	movslq	%ecx, %rdx
	addq	$16, %r14
	cmpq	%rdx, %r12
	jl	.LBB13_5
.LBB13_10:                              # %._crit_edge30
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end13:
	.size	free_side_info_link, .Lfunc_end13-free_side_info_link
	.cfi_endproc

	.type	BitCount,@object        # @BitCount
	.local	BitCount
	.comm	BitCount,4,4
	.type	ThisFrameSize,@object   # @ThisFrameSize
	.local	ThisFrameSize
	.comm	ThisFrameSize,4,4
	.type	BitsRemaining,@object   # @BitsRemaining
	.local	BitsRemaining
	.comm	BitsRemaining,4,4
	.type	forwardFrameLength,@object # @forwardFrameLength
	.local	forwardFrameLength
	.comm	forwardFrameLength,4,4
	.type	forwardSILength,@object # @forwardSILength
	.local	forwardSILength
	.comm	forwardSILength,4,4
	.type	elements,@object        # @elements
	.local	elements
	.comm	elements,4,4
	.type	side_queue_free,@object # @side_queue_free
	.local	side_queue_free
	.comm	side_queue_free,8,8
	.type	side_queue_head,@object # @side_queue_head
	.local	side_queue_head
	.comm	side_queue_head,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cannot allocate side_info_link"
	.size	.L.str, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
