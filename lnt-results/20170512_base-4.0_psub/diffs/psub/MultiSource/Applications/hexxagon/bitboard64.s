	.text
	.file	"bitboard64.bc"
	.globl	_ZN10BitBoard64coEv
	.p2align	4, 0x90
	.type	_ZN10BitBoard64coEv,@function
_ZN10BitBoard64coEv:                    # @_ZN10BitBoard64coEv
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	movl	4(%rdi), %edx
	notl	%edx
	shlq	$32, %rdx
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	xorq	%rcx, %rax
	orq	%rdx, %rax
	retq
.Lfunc_end0:
	.size	_ZN10BitBoard64coEv, .Lfunc_end0-_ZN10BitBoard64coEv
	.cfi_endproc

	.globl	_ZN10BitBoard64anERKS_
	.p2align	4, 0x90
	.type	_ZN10BitBoard64anERKS_,@function
_ZN10BitBoard64anERKS_:                 # @_ZN10BitBoard64anERKS_
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	4(%rsi), %ecx
	andl	(%rdi), %eax
	andl	4(%rdi), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	retq
.Lfunc_end1:
	.size	_ZN10BitBoard64anERKS_, .Lfunc_end1-_ZN10BitBoard64anERKS_
	.cfi_endproc

	.globl	_ZN10BitBoard64orERKS_
	.p2align	4, 0x90
	.type	_ZN10BitBoard64orERKS_,@function
_ZN10BitBoard64orERKS_:                 # @_ZN10BitBoard64orERKS_
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	4(%rsi), %ecx
	orl	(%rdi), %eax
	orl	4(%rdi), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	retq
.Lfunc_end2:
	.size	_ZN10BitBoard64orERKS_, .Lfunc_end2-_ZN10BitBoard64orERKS_
	.cfi_endproc

	.globl	_ZN10BitBoard64aSERKS_
	.p2align	4, 0x90
	.type	_ZN10BitBoard64aSERKS_,@function
_ZN10BitBoard64aSERKS_:                 # @_ZN10BitBoard64aSERKS_
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movq	(%rdi), %rax
	retq
.Lfunc_end3:
	.size	_ZN10BitBoard64aSERKS_, .Lfunc_end3-_ZN10BitBoard64aSERKS_
	.cfi_endproc

	.globl	_ZN10BitBoard64eoERKS_
	.p2align	4, 0x90
	.type	_ZN10BitBoard64eoERKS_,@function
_ZN10BitBoard64eoERKS_:                 # @_ZN10BitBoard64eoERKS_
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	4(%rsi), %ecx
	xorl	(%rdi), %eax
	xorl	4(%rdi), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	retq
.Lfunc_end4:
	.size	_ZN10BitBoard64eoERKS_, .Lfunc_end4-_ZN10BitBoard64eoERKS_
	.cfi_endproc

	.globl	_ZN10BitBoard64cvbEv
	.p2align	4, 0x90
	.type	_ZN10BitBoard64cvbEv,@function
_ZN10BitBoard64cvbEv:                   # @_ZN10BitBoard64cvbEv
	.cfi_startproc
# BB#0:
	movb	$1, %al
	cmpl	$0, (%rdi)
	jne	.LBB5_2
# BB#1:
	cmpl	$0, 4(%rdi)
	setne	%al
.LBB5_2:
	retq
.Lfunc_end5:
	.size	_ZN10BitBoard64cvbEv, .Lfunc_end5-_ZN10BitBoard64cvbEv
	.cfi_endproc

	.globl	_ZN10BitBoard646setBitEi
	.p2align	4, 0x90
	.type	_ZN10BitBoard646setBitEi,@function
_ZN10BitBoard646setBitEi:               # @_ZN10BitBoard646setBitEi
	.cfi_startproc
# BB#0:
	cmpl	$63, %esi
	ja	.LBB6_4
# BB#1:                                 # %.sink.split
	leaq	4(%rdi), %rax
	cmpl	$32, %esi
	cmovlq	%rdi, %rax
	jl	.LBB6_3
# BB#2:                                 # %.sink.split
	addl	$-32, %esi
.LBB6_3:                                # %.sink.split
	movl	$1, %edx
	movl	%esi, %ecx
	shll	%cl, %edx
	orl	%edx, (%rax)
.LBB6_4:
	retq
.Lfunc_end6:
	.size	_ZN10BitBoard646setBitEi, .Lfunc_end6-_ZN10BitBoard646setBitEi
	.cfi_endproc

	.globl	_ZN10BitBoard648unSetBitEi
	.p2align	4, 0x90
	.type	_ZN10BitBoard648unSetBitEi,@function
_ZN10BitBoard648unSetBitEi:             # @_ZN10BitBoard648unSetBitEi
	.cfi_startproc
# BB#0:
	cmpl	$63, %esi
	ja	.LBB7_4
# BB#1:                                 # %.sink.split
	leaq	4(%rdi), %rax
	cmpl	$32, %esi
	cmovlq	%rdi, %rax
	jl	.LBB7_3
# BB#2:                                 # %.sink.split
	addl	$-32, %esi
.LBB7_3:                                # %.sink.split
	movl	$-2, %edx
	movl	%esi, %ecx
	roll	%cl, %edx
	andl	%edx, (%rax)
.LBB7_4:
	retq
.Lfunc_end7:
	.size	_ZN10BitBoard648unSetBitEi, .Lfunc_end7-_ZN10BitBoard648unSetBitEi
	.cfi_endproc

	.globl	_ZN10BitBoard646getBitEi
	.p2align	4, 0x90
	.type	_ZN10BitBoard646getBitEi,@function
_ZN10BitBoard646getBitEi:               # @_ZN10BitBoard646getBitEi
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	cmpl	$63, %esi
	ja	.LBB8_4
# BB#1:
	cmpl	$31, %esi
	jg	.LBB8_3
# BB#2:
	movl	$1, %eax
	movl	%esi, %ecx
	shll	%cl, %eax
	andl	(%rdi), %eax
	retq
.LBB8_3:
	addl	$-32, %esi
	movl	$1, %eax
	movl	%esi, %ecx
	shll	%cl, %eax
	andl	4(%rdi), %eax
.LBB8_4:
	retq
.Lfunc_end8:
	.size	_ZN10BitBoard646getBitEi, .Lfunc_end8-_ZN10BitBoard646getBitEi
	.cfi_endproc

	.globl	_ZN10BitBoard6412readFromFileEP8_IO_FILE
	.p2align	4, 0x90
	.type	_ZN10BitBoard6412readFromFileEP8_IO_FILE,@function
_ZN10BitBoard6412readFromFileEP8_IO_FILE: # @_ZN10BitBoard6412readFromFileEP8_IO_FILE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	$0, (%r14)
	movb	$0, 7(%rsp)
	leaq	7(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB9_9
# BB#1:
	movzbl	7(%rsp), %eax
	orl	%eax, (%r14)
	movb	$0, 7(%rsp)
	leaq	7(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB9_9
# BB#2:
	movzbl	7(%rsp), %eax
	shll	$8, %eax
	orl	%eax, (%r14)
	movb	$0, 7(%rsp)
	leaq	7(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB9_9
# BB#3:
	movzbl	7(%rsp), %eax
	shll	$16, %eax
	orl	%eax, (%r14)
	movb	$0, 7(%rsp)
	leaq	7(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB9_9
# BB#4:                                 # %.thread.preheader.preheader31
	movzbl	7(%rsp), %eax
	shll	$24, %eax
	orl	%eax, (%r14)
	movb	$0, 6(%rsp)
	leaq	6(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB9_9
# BB#5:                                 # %.thread
	movzbl	6(%rsp), %eax
	orl	%eax, 4(%r14)
	movb	$0, 6(%rsp)
	leaq	6(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB9_9
# BB#6:                                 # %.thread.1
	movzbl	6(%rsp), %eax
	shll	$8, %eax
	orl	%eax, 4(%r14)
	movb	$0, 6(%rsp)
	leaq	6(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB9_9
# BB#7:                                 # %.thread.2
	movzbl	6(%rsp), %eax
	shll	$16, %eax
	orl	%eax, 4(%r14)
	movb	$0, 6(%rsp)
	leaq	6(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB9_9
# BB#8:                                 # %.thread.3
	movzbl	6(%rsp), %eax
	shll	$24, %eax
	orl	%eax, 4(%r14)
	xorl	%eax, %eax
	jmp	.LBB9_10
.LBB9_9:
	movl	$-1, %eax
.LBB9_10:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN10BitBoard6412readFromFileEP8_IO_FILE, .Lfunc_end9-_ZN10BitBoard6412readFromFileEP8_IO_FILE
	.cfi_endproc

	.globl	_ZN10BitBoard6411writeToFileEP8_IO_FILE
	.p2align	4, 0x90
	.type	_ZN10BitBoard6411writeToFileEP8_IO_FILE,@function
_ZN10BitBoard6411writeToFileEP8_IO_FILE: # @_ZN10BitBoard6411writeToFileEP8_IO_FILE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movb	(%rbx), %al
	movb	%al, 7(%rsp)
	leaq	7(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB10_6
# BB#1:
	movb	1(%rbx), %al
	movb	%al, 7(%rsp)
	leaq	7(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB10_6
# BB#2:
	movb	2(%rbx), %al
	movb	%al, 7(%rsp)
	leaq	7(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB10_6
# BB#3:
	movb	3(%rbx), %al
	movb	%al, 7(%rsp)
	leaq	7(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB10_6
# BB#4:                                 # %.thread.preheader32
	movb	4(%rbx), %al
	movb	%al, 6(%rsp)
	leaq	6(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB10_6
# BB#5:                                 # %.thread
	movb	5(%rbx), %al
	movb	%al, 6(%rsp)
	leaq	6(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB10_6
# BB#8:                                 # %.thread.1
	movb	6(%rbx), %al
	movb	%al, 6(%rsp)
	leaq	6(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB10_6
# BB#9:                                 # %.thread.2
	movb	7(%rbx), %al
	movb	%al, 6(%rsp)
	leaq	6(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	xorl	%ecx, %ecx
	cmpq	$1, %rax
	movl	$-1, %eax
	cmovel	%ecx, %eax
	jmp	.LBB10_7
.LBB10_6:                               # %.loopexit
	movl	$-1, %eax
.LBB10_7:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_ZN10BitBoard6411writeToFileEP8_IO_FILE, .Lfunc_end10-_ZN10BitBoard6411writeToFileEP8_IO_FILE
	.cfi_endproc

	.globl	_Z6getBFPii
	.p2align	4, 0x90
	.type	_Z6getBFPii,@function
_Z6getBFPii:                            # @_Z6getBFPii
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	-1(%rdi), %ecx
	movl	$-1, %eax
	cmpl	$8, %ecx
	ja	.LBB11_7
# BB#1:
	leal	-1(%rsi), %ecx
	cmpl	$8, %ecx
	ja	.LBB11_7
# BB#2:
	cmpl	$4, %esi
	jg	.LBB11_4
# BB#3:
	leal	4(%rsi), %ecx
	cmpl	%edi, %ecx
	jl	.LBB11_7
.LBB11_4:
	cmpl	$6, %esi
	setl	%cl
	leal	-5(%rsi), %edx
	cmpl	%edi, %edx
	jl	.LBB11_6
# BB#5:
	testb	%cl, %cl
	je	.LBB11_7
.LBB11_6:
	xorl	%eax, %eax
	cmpl	$5, %esi
	setg	%al
	leal	(%rsi,%rsi,8), %ecx
	addl	%edi, %ecx
	xorl	%edx, %edx
	cmpl	$2, %esi
	setl	%dl
	leal	-14(%rcx,%rdx,4), %edi
	leal	-17(%rcx,%rdx,4), %ecx
	cmovlel	%edi, %ecx
	leal	-2(%rcx), %edx
	cmpl	$3, %esi
	cmovlel	%ecx, %edx
	xorl	%ecx, %ecx
	cmpl	$4, %esi
	movl	$-1, %edi
	cmovlel	%ecx, %edi
	subl	%eax, %edi
	leal	(%rdi,%rdx), %eax
	cmpl	$6, %esi
	leal	-2(%rdi,%rdx), %ecx
	cmovlel	%eax, %ecx
	leal	-3(%rcx), %edx
	cmpl	$7, %esi
	cmovlel	%ecx, %edx
	leal	-4(%rdx), %eax
	cmpl	$8, %esi
	cmovlel	%edx, %eax
.LBB11_7:
	retq
.Lfunc_end11:
	.size	_Z6getBFPii, .Lfunc_end11-_Z6getBFPii
	.cfi_endproc

	.globl	_ZN10BitBoard645printEv
	.p2align	4, 0x90
	.type	_ZN10BitBoard645printEv,@function
_ZN10BitBoard645printEv:                # @_ZN10BitBoard645printEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 96
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	4(%rdi), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$1, %ebx
	movl	$4, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB12_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_3 Depth 2
                                        #     Child Loop BB12_5 Depth 2
	cmpl	$4, %ebx
	jg	.LBB12_4
# BB#2:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB12_1 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_3:                               # %.lr.ph
                                        #   Parent Loop BB12_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	incl	%ebp
	cmpl	%ebp, %r14d
	jne	.LBB12_3
.LBB12_4:                               # %.preheader.split.us.preheader
                                        #   in Loop: Header=BB12_1 Depth=1
	movl	%r14d, 8(%rsp)          # 4-byte Spill
	leal	4(%rbx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	cmpl	$5, %ebx
	setg	%al
	cmpl	$6, %ebx
	setl	%r14b
	leal	-5(%rbx), %r15d
	xorl	%ecx, %ecx
	cmpl	$2, %ebx
	setl	%cl
	cmpl	$4, %ebx
	movl	$0, %r12d
	movl	$-1, %edx
	cmovgl	%edx, %r12d
	subl	%eax, %r12d
	movq	%r13, 16(%rsp)          # 8-byte Spill
	leal	-14(%r13,%rcx,4), %ebp
	movl	$10, %r13d
	jmp	.LBB12_5
.LBB12_12:                              #   in Loop: Header=BB12_5 Depth=2
	movl	$_ZSt4cout, %edi
	movl	$.L.str.2, %esi
	movl	$2, %edx
	jmp	.LBB12_16
	.p2align	4, 0x90
.LBB12_5:                               # %.preheader.split.us
                                        #   Parent Loop BB12_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$4, %ebx
	setg	%al
	leal	-9(%r13), %edx
	cmpl	%edx, 12(%rsp)          # 4-byte Folded Reload
	setge	%cl
	cmpl	%edx, %r15d
	setl	%dl
	orb	%r14b, %dl
	cmpb	$1, %dl
	jne	.LBB12_15
# BB#6:                                 # %.preheader.split.us
                                        #   in Loop: Header=BB12_5 Depth=2
	orb	%cl, %al
	je	.LBB12_15
# BB#7:                                 # %_Z6getBFPii.exit.us
                                        #   in Loop: Header=BB12_5 Depth=2
	leal	(%rbp,%r13), %eax
	cmpl	$2, %ebx
	leal	-3(%rbp,%r13), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %ebx
	cmovlel	%ecx, %eax
	leal	(%r12,%rax), %ecx
	cmpl	$6, %ebx
	leal	-2(%r12,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$7, %ebx
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %eax
	cmpl	$8, %ebx
	cmovlel	%ecx, %eax
	cmpl	$-1, %eax
	je	.LBB12_15
# BB#8:                                 #   in Loop: Header=BB12_5 Depth=2
	cmpl	$63, %eax
	ja	.LBB12_14
# BB#9:                                 # %_ZN10BitBoard646getBitEi.exit.us
                                        #   in Loop: Header=BB12_5 Depth=2
	cmpl	$32, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmovlq	32(%rsp), %rcx          # 8-byte Folded Reload
	movl	(%rcx), %ecx
	jl	.LBB12_11
# BB#10:                                # %_ZN10BitBoard646getBitEi.exit.us
                                        #   in Loop: Header=BB12_5 Depth=2
	addl	$-32, %eax
.LBB12_11:                              # %_ZN10BitBoard646getBitEi.exit.us
                                        #   in Loop: Header=BB12_5 Depth=2
	movzbl	%al, %eax
	btl	%eax, %ecx
	jae	.LBB12_12
.LBB12_14:                              # %_ZN10BitBoard646getBitEi.exit.thread.us
                                        #   in Loop: Header=BB12_5 Depth=2
	movl	$_ZSt4cout, %edi
	movl	$.L.str.1, %esi
	movl	$2, %edx
	jmp	.LBB12_16
	.p2align	4, 0x90
.LBB12_15:                              # %_Z6getBFPii.exit.thread.us
                                        #   in Loop: Header=BB12_5 Depth=2
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$1, %edx
.LBB12_16:                              #   in Loop: Header=BB12_5 Depth=2
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	incl	%r13d
	cmpl	$19, %r13d
	jne	.LBB12_5
# BB#17:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB12_1 Depth=1
	movl	$_ZSt4cout, %edi
	movl	$.L.str.3, %esi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	incl	%ebx
	movl	8(%rsp), %r14d          # 4-byte Reload
	decl	%r14d
	movq	16(%rsp), %r13          # 8-byte Reload
	addl	$9, %r13d
	cmpl	$10, %ebx
	jne	.LBB12_1
# BB#18:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN10BitBoard645printEv, .Lfunc_end12-_ZN10BitBoard645printEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_bitboard64.ii,@function
_GLOBAL__sub_I_bitboard64.ii:           # @_GLOBAL__sub_I_bitboard64.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end13:
	.size	_GLOBAL__sub_I_bitboard64.ii, .Lfunc_end13-_GLOBAL__sub_I_bitboard64.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" "
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"x "
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	". "
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n"
	.size	.L.str.3, 2

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_bitboard64.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
