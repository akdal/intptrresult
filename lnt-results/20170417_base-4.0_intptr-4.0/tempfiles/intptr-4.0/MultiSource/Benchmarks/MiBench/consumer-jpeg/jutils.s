	.text
	.file	"jutils.bc"
	.globl	jdiv_round_up
	.p2align	4, 0x90
	.type	jdiv_round_up,@function
jdiv_round_up:                          # @jdiv_round_up
	.cfi_startproc
# BB#0:
	leaq	-1(%rdi,%rsi), %rax
	cqto
	idivq	%rsi
	retq
.Lfunc_end0:
	.size	jdiv_round_up, .Lfunc_end0-jdiv_round_up
	.cfi_endproc

	.globl	jround_up
	.p2align	4, 0x90
	.type	jround_up,@function
jround_up:                              # @jround_up
	.cfi_startproc
# BB#0:
	leaq	-1(%rdi,%rsi), %rcx
	movq	%rcx, %rax
	cqto
	idivq	%rsi
	subq	%rdx, %rcx
	movq	%rcx, %rax
	retq
.Lfunc_end1:
	.size	jround_up, .Lfunc_end1-jround_up
	.cfi_endproc

	.globl	jcopy_sample_rows
	.p2align	4, 0x90
	.type	jcopy_sample_rows,@function
jcopy_sample_rows:                      # @jcopy_sample_rows
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	testl	%r15d, %r15d
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%r9d, %r14d
	movslq	%ecx, %rax
	leaq	(%rdx,%rax,8), %rbx
	movslq	%esi, %rax
	leaq	(%rdi,%rax,8), %rbp
	incl	%r15d
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	addq	$8, %rbp
	movq	(%rbx), %rdi
	addq	$8, %rbx
	movq	%r14, %rdx
	callq	memcpy
	decl	%r15d
	cmpl	$1, %r15d
	jg	.LBB2_2
.LBB2_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	jcopy_sample_rows, .Lfunc_end2-jcopy_sample_rows
	.cfi_endproc

	.globl	jcopy_block_row
	.p2align	4, 0x90
	.type	jcopy_block_row,@function
jcopy_block_row:                        # @jcopy_block_row
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movl	%edx, %edx
	shlq	$7, %rdx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	memcpy                  # TAILCALL
.Lfunc_end3:
	.size	jcopy_block_row, .Lfunc_end3-jcopy_block_row
	.cfi_endproc

	.globl	jzero_far
	.p2align	4, 0x90
	.type	jzero_far,@function
jzero_far:                              # @jzero_far
	.cfi_startproc
# BB#0:
	movq	%rsi, %rax
	xorl	%esi, %esi
	movq	%rax, %rdx
	jmp	memset                  # TAILCALL
.Lfunc_end4:
	.size	jzero_far, .Lfunc_end4-jzero_far
	.cfi_endproc

	.type	jpeg_natural_order,@object # @jpeg_natural_order
	.section	.rodata,"a",@progbits
	.globl	jpeg_natural_order
	.p2align	4
jpeg_natural_order:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	9                       # 0x9
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	10                      # 0xa
	.long	17                      # 0x11
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	25                      # 0x19
	.long	18                      # 0x12
	.long	11                      # 0xb
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	12                      # 0xc
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	33                      # 0x21
	.long	40                      # 0x28
	.long	48                      # 0x30
	.long	41                      # 0x29
	.long	34                      # 0x22
	.long	27                      # 0x1b
	.long	20                      # 0x14
	.long	13                      # 0xd
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	14                      # 0xe
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	35                      # 0x23
	.long	42                      # 0x2a
	.long	49                      # 0x31
	.long	56                      # 0x38
	.long	57                      # 0x39
	.long	50                      # 0x32
	.long	43                      # 0x2b
	.long	36                      # 0x24
	.long	29                      # 0x1d
	.long	22                      # 0x16
	.long	15                      # 0xf
	.long	23                      # 0x17
	.long	30                      # 0x1e
	.long	37                      # 0x25
	.long	44                      # 0x2c
	.long	51                      # 0x33
	.long	58                      # 0x3a
	.long	59                      # 0x3b
	.long	52                      # 0x34
	.long	45                      # 0x2d
	.long	38                      # 0x26
	.long	31                      # 0x1f
	.long	39                      # 0x27
	.long	46                      # 0x2e
	.long	53                      # 0x35
	.long	60                      # 0x3c
	.long	61                      # 0x3d
	.long	54                      # 0x36
	.long	47                      # 0x2f
	.long	55                      # 0x37
	.long	62                      # 0x3e
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.long	63                      # 0x3f
	.size	jpeg_natural_order, 320


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
