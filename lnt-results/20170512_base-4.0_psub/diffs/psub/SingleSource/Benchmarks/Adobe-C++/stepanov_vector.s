	.text
	.file	"stepanov_vector.bc"
	.globl	_Z13record_resultdPKc
	.p2align	4, 0x90
	.type	_Z13record_resultdPKc,@function
_Z13record_resultdPKc:                  # @_Z13record_resultdPKc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	results(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	movl	current_test(%rip), %ecx
	movl	allocated_results(%rip), %edx
	cmpl	%edx, %ecx
	jge	.LBB0_3
	jmp	.LBB0_5
.LBB0_1:                                # %._crit_edge
	movl	allocated_results(%rip), %edx
.LBB0_3:
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movslq	%edx, %rcx
	movq	%rcx, %rsi
	shlq	$4, %rsi
	addl	$10, %ecx
	movl	%ecx, allocated_results(%rip)
	addq	$160, %rsi
	movq	%rax, %rdi
	callq	realloc
	movq	%rax, results(%rip)
	testq	%rax, %rax
	je	.LBB0_6
# BB#4:                                 # %._crit_edge2
	movl	current_test(%rip), %ecx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB0_5:
	movslq	%ecx, %rcx
	movq	%rcx, %rdx
	shlq	$4, %rdx
	movsd	%xmm0, (%rax,%rdx)
	movq	%rbx, 8(%rax,%rdx)
	incl	%ecx
	movl	%ecx, current_test(%rip)
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB0_6:
	movl	allocated_results(%rip), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	_Z13record_resultdPKc, .Lfunc_end0-_Z13record_resultdPKc
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.globl	_Z9summarizePKciiii
	.p2align	4, 0x90
	.type	_Z9summarizePKciiii,@function
_Z9summarizePKciiii:                    # @_Z9summarizePKciiii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi9:
	.cfi_def_cfa_offset 80
.Lcfi10:
	.cfi_offset %rbx, -56
.Lcfi11:
	.cfi_offset %r12, -48
.Lcfi12:
	.cfi_offset %r13, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%edx, %r12d
	movl	%esi, %r13d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movslq	current_test(%rip), %rbx
	testq	%rbx, %rbx
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph63
	movq	results(%rip), %r14
	addq	$8, %r14
	movl	$12, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdi
	callq	strlen
	cmpl	%ebp, %eax
	cmovgel	%eax, %ebp
	incq	%r15
	addq	$16, %r14
	cmpq	%rbx, %r15
	jl	.LBB1_3
	jmp	.LBB1_4
.LBB1_1:
	movl	$12, %ebp
.LBB1_4:                                # %._crit_edge64
	leal	-12(%rbp), %esi
	movl	$.L.str.1, %edi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.3, %edi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	xorpd	%xmm0, %xmm0
	cmpl	$0, current_test(%rip)
	jle	.LBB1_5
# BB#20:                                # %.lr.ph59
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	cvtsi2sdl	%r12d, %xmm1
	mulsd	%xmm0, %xmm1
	divsd	.LCPI1_0(%rip), %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movl	%ebp, %r12d
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB1_21:                               # =>This Inner Loop Header: Depth=1
	movq	results(%rip), %r14
	movq	(%r14,%rbp), %r13
	movq	%r13, %rdi
	callq	strlen
	movl	%r12d, %edx
	subl	%eax, %edx
	movsd	-8(%r14,%rbp), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	divsd	(%r14), %xmm2
	movl	$.L.str.4, %edi
	movl	$.L.str.5, %ecx
	movb	$3, %al
	movq	%r13, %r8
	movl	%ebx, %esi
	callq	printf
	incq	%rbx
	movslq	current_test(%rip), %rax
	addq	$16, %rbp
	cmpq	%rax, %rbx
	jl	.LBB1_21
# BB#6:                                 # %.preheader48
	testl	%eax, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	xorpd	%xmm0, %xmm0
	jle	.LBB1_14
# BB#7:                                 # %.lr.ph54
	movq	results(%rip), %rbp
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB1_8
# BB#9:                                 # %.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	addsd	(%rdx), %xmm0
	incq	%rcx
	addq	$16, %rdx
	cmpq	%rcx, %rdi
	jne	.LBB1_10
	jmp	.LBB1_11
.LBB1_5:
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_14
.LBB1_8:
	xorl	%ecx, %ecx
	xorpd	%xmm0, %xmm0
.LBB1_11:                               # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB1_14
# BB#12:                                # %.lr.ph54.new
	movq	%rcx, %rdx
	shlq	$4, %rdx
	leaq	48(%rbp,%rdx), %rdx
	.p2align	4, 0x90
.LBB1_13:                               # =>This Inner Loop Header: Depth=1
	addsd	-48(%rdx), %xmm0
	addsd	-32(%rdx), %xmm0
	addsd	-16(%rdx), %xmm0
	addsd	(%rdx), %xmm0
	addq	$4, %rcx
	addq	$64, %rdx
	cmpq	%rax, %rcx
	jl	.LBB1_13
.LBB1_14:                               # %._crit_edge55
	movl	$.L.str.6, %edi
	movb	$1, %al
	movq	%r14, %rsi
	callq	printf
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB1_19
# BB#15:                                # %._crit_edge55
	cmpl	$2, current_test(%rip)
	jl	.LBB1_19
# BB#16:                                # %.lr.ph.preheader
	xorpd	%xmm1, %xmm1
	movl	$1, %ebx
	movl	$16, %ebp
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movq	results(%rip), %rax
	movsd	(%rax,%rbp), %xmm0      # xmm0 = mem[0],zero
	divsd	(%rax), %xmm0
	callq	log
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	incq	%rbx
	movslq	current_test(%rip), %rax
	addq	$16, %rbp
	cmpq	%rax, %rbx
	jl	.LBB1_17
# BB#18:                                # %._crit_edge
	decl	%eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	callq	exp
	movl	$.L.str.7, %edi
	movb	$1, %al
	movq	%r14, %rsi
	callq	printf
.LBB1_19:
	movl	$0, current_test(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_Z9summarizePKciiii, .Lfunc_end1-_Z9summarizePKciiii
	.cfi_endproc

	.globl	_Z17summarize_simplefP8_IO_FILEPKc
	.p2align	4, 0x90
	.type	_Z17summarize_simplefP8_IO_FILEPKc,@function
_Z17summarize_simplefP8_IO_FILEPKc:     # @_Z17summarize_simplefP8_IO_FILEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 64
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	current_test(%rip), %r12
	testq	%r12, %r12
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph42
	movq	results(%rip), %rbp
	addq	$8, %rbp
	movl	$12, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	callq	strlen
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	incq	%rbx
	addq	$16, %rbp
	cmpq	%r12, %rbx
	jl	.LBB2_3
	jmp	.LBB2_4
.LBB2_1:
	movl	$12, %r13d
.LBB2_4:                                # %._crit_edge43
	leal	-12(%r13), %edx
	movl	$.L.str.8, %esi
	movl	$.L.str.2, %ecx
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	$.L.str.9, %esi
	movl	$.L.str.2, %ecx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%r13d, %edx
	callq	fprintf
	xorpd	%xmm0, %xmm0
	cmpl	$0, current_test(%rip)
	jle	.LBB2_15
# BB#5:                                 # %.lr.ph38
	movq	%r14, (%rsp)            # 8-byte Spill
	movl	%r13d, %r13d
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movq	results(%rip), %r14
	movq	(%r14,%rbp), %r12
	movq	%r12, %rdi
	callq	strlen
	movl	%r13d, %ecx
	subl	%eax, %ecx
	movsd	-8(%r14,%rbp), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.10, %esi
	movl	$.L.str.5, %r8d
	movb	$1, %al
	movq	%r15, %rdi
	movq	%r12, %r9
	movl	%ebx, %edx
	callq	fprintf
	incq	%rbx
	movslq	current_test(%rip), %rax
	addq	$16, %rbp
	cmpq	%rax, %rbx
	jl	.LBB2_6
# BB#7:                                 # %.preheader
	testl	%eax, %eax
	movq	(%rsp), %r14            # 8-byte Reload
	xorpd	%xmm0, %xmm0
	jle	.LBB2_15
# BB#8:                                 # %.lr.ph
	movq	results(%rip), %rbp
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB2_9
# BB#10:                                # %.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB2_11:                               # =>This Inner Loop Header: Depth=1
	addsd	(%rdx), %xmm0
	incq	%rcx
	addq	$16, %rdx
	cmpq	%rcx, %rdi
	jne	.LBB2_11
	jmp	.LBB2_12
.LBB2_9:
	xorl	%ecx, %ecx
	xorpd	%xmm0, %xmm0
.LBB2_12:                               # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB2_15
# BB#13:                                # %.lr.ph.new
	movq	%rcx, %rdx
	shlq	$4, %rdx
	leaq	48(%rbp,%rdx), %rdx
	.p2align	4, 0x90
.LBB2_14:                               # =>This Inner Loop Header: Depth=1
	addsd	-48(%rdx), %xmm0
	addsd	-32(%rdx), %xmm0
	addsd	-16(%rdx), %xmm0
	addsd	(%rdx), %xmm0
	addq	$4, %rcx
	addq	$64, %rdx
	cmpq	%rax, %rcx
	jl	.LBB2_14
.LBB2_15:                               # %._crit_edge
	movl	$.L.str.6, %esi
	movb	$1, %al
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	fprintf
	movl	$0, current_test(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_Z17summarize_simplefP8_IO_FILEPKc, .Lfunc_end2-_Z17summarize_simplefP8_IO_FILEPKc
	.cfi_endproc

	.globl	_Z11start_timerv
	.p2align	4, 0x90
	.type	_Z11start_timerv,@function
_Z11start_timerv:                       # @_Z11start_timerv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 16
	callq	clock
	movq	%rax, start_time(%rip)
	popq	%rax
	retq
.Lfunc_end3:
	.size	_Z11start_timerv, .Lfunc_end3-_Z11start_timerv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.globl	_Z5timerv
	.p2align	4, 0x90
	.type	_Z5timerv,@function
_Z5timerv:                              # @_Z5timerv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 16
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI4_0(%rip), %xmm0
	popq	%rax
	retq
.Lfunc_end4:
	.size	_Z5timerv, .Lfunc_end4-_Z5timerv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4656510908468559872     # double 2000
.LCPI5_1:
	.quad	0                       # double 0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi37:
	.cfi_def_cfa_offset 496
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	cmpl	$2, %ebp
	jl	.LBB5_3
# BB#1:
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, iterations(%rip)
	cmpl	$2, %ebp
	je	.LBB5_3
# BB#2:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	callq	strtod
	movsd	%xmm0, init_value(%rip)
.LBB5_3:                                # %.thread
	movabsq	$4611686018427387900, %r14 # imm = 0x3FFFFFFFFFFFFFFC
	cvttsd2si	init_value(%rip), %edi
	addl	$123, %edi
	callq	srand
	movq	dpb(%rip), %rax
	movq	dpe(%rip), %rcx
	cmpq	%rcx, %rax
	je	.LBB5_16
# BB#4:                                 # %.lr.ph.i.preheader
	movq	init_value(%rip), %rdx
	leaq	-8(%rcx), %rsi
	subq	%rax, %rsi
	shrq	$3, %rsi
	incq	%rsi
	cmpq	$4, %rsi
	jb	.LBB5_15
# BB#5:                                 # %min.iters.checked
	movq	%rsi, %rbx
	andq	%r14, %rbx
	je	.LBB5_15
# BB#6:                                 # %vector.ph
	movd	%rdx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rbx), %r8
	movl	%r8d, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB5_7
# BB#8:                                 # %vector.body.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_9:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rax,%rbp,8)
	movdqu	%xmm0, 16(%rax,%rbp,8)
	addq	$4, %rbp
	incq	%rdi
	jne	.LBB5_9
	jmp	.LBB5_10
.LBB5_7:
	xorl	%ebp, %ebp
.LBB5_10:                               # %vector.body.prol.loopexit
	cmpq	$28, %r8
	jb	.LBB5_13
# BB#11:                                # %vector.ph.new
	movq	%rbx, %rdi
	subq	%rbp, %rdi
	leaq	240(%rax,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB5_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rbp)
	movdqu	%xmm0, -224(%rbp)
	movdqu	%xmm0, -208(%rbp)
	movdqu	%xmm0, -192(%rbp)
	movdqu	%xmm0, -176(%rbp)
	movdqu	%xmm0, -160(%rbp)
	movdqu	%xmm0, -144(%rbp)
	movdqu	%xmm0, -128(%rbp)
	movdqu	%xmm0, -112(%rbp)
	movdqu	%xmm0, -96(%rbp)
	movdqu	%xmm0, -80(%rbp)
	movdqu	%xmm0, -64(%rbp)
	movdqu	%xmm0, -48(%rbp)
	movdqu	%xmm0, -32(%rbp)
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	addq	$256, %rbp              # imm = 0x100
	addq	$-32, %rdi
	jne	.LBB5_12
.LBB5_13:                               # %middle.block
	cmpq	%rbx, %rsi
	je	.LBB5_16
# BB#14:
	leaq	(%rax,%rbx,8), %rax
	.p2align	4, 0x90
.LBB5_15:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.LBB5_15
.LBB5_16:                               # %.loopexit731
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 64(%rsp)
	movq	$0, 80(%rsp)
	movq	$0, 96(%rsp)
.Ltmp0:
	leaq	64(%rsp), %rdi
	leaq	96(%rsp), %rcx
	xorl	%esi, %esi
	movl	$2000, %edx             # imm = 0x7D0
	callq	_ZNSt6vectorIdSaIdEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPdS1_EEmRKd
.Ltmp1:
# BB#17:
	movq	64(%rsp), %r15
	movq	72(%rsp), %r12
	cmpq	%r12, %r15
	je	.LBB5_30
# BB#18:                                # %.lr.ph.i76.preheader
	movq	init_value(%rip), %rax
	leaq	-8(%r12), %rcx
	subq	%r15, %rcx
	shrq	$3, %rcx
	incq	%rcx
	cmpq	$4, %rcx
	movq	%r15, %rsi
	jb	.LBB5_29
# BB#19:                                # %min.iters.checked891
	movq	%rcx, %rdx
	andq	%r14, %rdx
	movq	%r15, %rsi
	je	.LBB5_29
# BB#20:                                # %vector.ph895
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB5_21
# BB#22:                                # %vector.body883.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_23:                               # %vector.body883.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%r15,%rdi,8)
	movdqu	%xmm0, 16(%r15,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB5_23
	jmp	.LBB5_24
.LBB5_21:
	xorl	%edi, %edi
.LBB5_24:                               # %vector.body883.prol.loopexit
	cmpq	$28, %rbp
	jb	.LBB5_27
# BB#25:                                # %vector.ph895.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	240(%r15,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB5_26:                               # %vector.body883
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdi)
	movdqu	%xmm0, -224(%rdi)
	movdqu	%xmm0, -208(%rdi)
	movdqu	%xmm0, -192(%rdi)
	movdqu	%xmm0, -176(%rdi)
	movdqu	%xmm0, -160(%rdi)
	movdqu	%xmm0, -144(%rdi)
	movdqu	%xmm0, -128(%rdi)
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$256, %rdi              # imm = 0x100
	addq	$-32, %rsi
	jne	.LBB5_26
.LBB5_27:                               # %middle.block884
	cmpq	%rdx, %rcx
	je	.LBB5_30
# BB#28:
	leaq	(%r15,%rdx,8), %rsi
	.p2align	4, 0x90
.LBB5_29:                               # %.lr.ph.i76
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %r12
	jne	.LBB5_29
.LBB5_30:                               # %.loopexit610
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB5_158
# BB#31:                                # %.lr.ph.i129
	movq	dpb(%rip), %r14
	movq	dpe(%rip), %rbp
	cmpq	%rbp, %r14
	je	.LBB5_35
# BB#32:                                # %.lr.ph.split.i.preheader
	leaq	-8(%rbp), %rbx
	subq	%r14, %rbx
	movq	%rbx, (%rsp)            # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	shrl	$3, %ebx
	incl	%ebx
	andl	$7, %ebx
	movq	%rbx, %rcx
	negq	%rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_33:                               # %.lr.ph.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_40 Depth 2
                                        #     Child Loop BB5_42 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%rbx, %rbx
	je	.LBB5_34
# BB#39:                                # %.lr.ph.i.i130.prol.preheader
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB5_40:                               # %.lr.ph.i.i130.prol
                                        #   Parent Loop BB5_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_40
	jmp	.LBB5_41
	.p2align	4, 0x90
.LBB5_34:                               #   in Loop: Header=BB5_33 Depth=1
	movq	%r14, %rcx
.LBB5_41:                               # %.lr.ph.i.i130.prol.loopexit
                                        #   in Loop: Header=BB5_33 Depth=1
	cmpq	$56, (%rsp)             # 8-byte Folded Reload
	jb	.LBB5_43
	.p2align	4, 0x90
.LBB5_42:                               # %.lr.ph.i.i130
                                        #   Parent Loop BB5_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbp, %rcx
	jne	.LBB5_42
.LBB5_43:                               # %_ZN9benchmark10accumulateIPddEET0_T_S3_S2_.exit.i
                                        #   in Loop: Header=BB5_33 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_44
	jnp	.LBB5_45
.LBB5_44:                               #   in Loop: Header=BB5_33 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_45:                               # %_Z9check_sumd.exit.i
                                        #   in Loop: Header=BB5_33 Depth=1
	incl	%r13d
	cmpl	%eax, %r13d
	jl	.LBB5_33
	jmp	.LBB5_46
.LBB5_35:                               # %.lr.ph.split.us.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_36:                               # %.lr.ph.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_37
	jnp	.LBB5_38
.LBB5_37:                               #   in Loop: Header=BB5_36 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_38:                               # %_Z9check_sumd.exit.us.i
                                        #   in Loop: Header=BB5_36 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_36
.LBB5_46:                               # %.loopexit608
	testl	%eax, %eax
	jle	.LBB5_158
# BB#47:                                # %.lr.ph.i131
	movq	64(%rsp), %r14
	movq	72(%rsp), %rbp
	cmpq	%rbp, %r14
	je	.LBB5_51
# BB#48:                                # %.lr.ph.split.i134.preheader
	leaq	-8(%rbp), %rbx
	subq	%r14, %rbx
	movq	%rbx, (%rsp)            # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	shrl	$3, %ebx
	incl	%ebx
	andl	$7, %ebx
	movq	%rbx, %rcx
	negq	%rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_49:                               # %.lr.ph.split.i134
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_56 Depth 2
                                        #     Child Loop BB5_58 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%rbx, %rbx
	je	.LBB5_50
# BB#55:                                # %.lr.ph.i.i135.prol.preheader
                                        #   in Loop: Header=BB5_49 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB5_56:                               # %.lr.ph.i.i135.prol
                                        #   Parent Loop BB5_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_56
	jmp	.LBB5_57
	.p2align	4, 0x90
.LBB5_50:                               #   in Loop: Header=BB5_49 Depth=1
	movq	%r14, %rcx
.LBB5_57:                               # %.lr.ph.i.i135.prol.loopexit
                                        #   in Loop: Header=BB5_49 Depth=1
	cmpq	$56, (%rsp)             # 8-byte Folded Reload
	jb	.LBB5_59
	.p2align	4, 0x90
.LBB5_58:                               # %.lr.ph.i.i135
                                        #   Parent Loop BB5_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbp, %rcx
	jne	.LBB5_58
.LBB5_59:                               # %_ZN9benchmark10accumulateIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEET0_T_S9_S8_.exit.i
                                        #   in Loop: Header=BB5_49 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_60
	jnp	.LBB5_61
.LBB5_60:                               #   in Loop: Header=BB5_49 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_61:                               # %_Z9check_sumd.exit.i137
                                        #   in Loop: Header=BB5_49 Depth=1
	incl	%r13d
	cmpl	%eax, %r13d
	jl	.LBB5_49
	jmp	.LBB5_62
.LBB5_51:                               # %.lr.ph.split.us.i132.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_52:                               # %.lr.ph.split.us.i132
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_53
	jnp	.LBB5_54
.LBB5_53:                               #   in Loop: Header=BB5_52 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_54:                               # %_Z9check_sumd.exit.us.i133
                                        #   in Loop: Header=BB5_52 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_52
.LBB5_62:                               # %_Z15test_accumulateIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_T0_PKc.exit
	testl	%eax, %eax
	jle	.LBB5_158
# BB#63:                                # %.lr.ph.i138.preheader
	movq	rdpb(%rip), %r14
	movq	rdpe(%rip), %rbp
	cmpq	%rbp, %r14
	je	.LBB5_67
# BB#64:                                # %.lr.ph.i138.preheader976
	leaq	-8(%r14), %rbx
	subq	%rbp, %rbx
	movq	%rbx, (%rsp)            # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	shrl	$3, %ebx
	incl	%ebx
	andl	$7, %ebx
	movq	%rbx, %rcx
	negq	%rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_65:                               # %.lr.ph.i138
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_72 Depth 2
                                        #     Child Loop BB5_74 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%rbx, %rbx
	je	.LBB5_66
# BB#71:                                # %.lr.ph.i.i140.prol.preheader
                                        #   in Loop: Header=BB5_65 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB5_72:                               # %.lr.ph.i.i140.prol
                                        #   Parent Loop BB5_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addq	$-8, %rcx
	incq	%rdx
	jne	.LBB5_72
	jmp	.LBB5_73
	.p2align	4, 0x90
.LBB5_66:                               #   in Loop: Header=BB5_65 Depth=1
	movq	%r14, %rcx
.LBB5_73:                               # %.lr.ph.i.i140.prol.loopexit
                                        #   in Loop: Header=BB5_65 Depth=1
	cmpq	$56, (%rsp)             # 8-byte Folded Reload
	jb	.LBB5_75
	.p2align	4, 0x90
.LBB5_74:                               # %.lr.ph.i.i140
                                        #   Parent Loop BB5_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addsd	-16(%rcx), %xmm0
	addsd	-24(%rcx), %xmm0
	addsd	-32(%rcx), %xmm0
	addsd	-40(%rcx), %xmm0
	addsd	-48(%rcx), %xmm0
	addsd	-56(%rcx), %xmm0
	addsd	-64(%rcx), %xmm0
	addq	$-64, %rcx
	cmpq	%rcx, %rbp
	jne	.LBB5_74
.LBB5_75:                               # %_ZN9benchmark10accumulateISt16reverse_iteratorIPdEdEET0_T_S5_S4_.exit.i
                                        #   in Loop: Header=BB5_65 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_76
	jnp	.LBB5_77
.LBB5_76:                               #   in Loop: Header=BB5_65 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_77:                               # %_Z9check_sumd.exit.i142
                                        #   in Loop: Header=BB5_65 Depth=1
	incl	%r13d
	cmpl	%eax, %r13d
	jl	.LBB5_65
	jmp	.LBB5_78
.LBB5_67:                               # %.lr.ph.i138.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_68:                               # %.lr.ph.i138.us
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_69
	jnp	.LBB5_70
.LBB5_69:                               #   in Loop: Header=BB5_68 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_70:                               # %_Z9check_sumd.exit.i142.us
                                        #   in Loop: Header=BB5_68 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_68
.LBB5_78:                               # %_Z15test_accumulateIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_T0_PKc.exit.thread
	testl	%eax, %eax
	jle	.LBB5_158
# BB#79:                                # %.lr.ph.i155.preheader
	movq	64(%rsp), %rbx
	movq	72(%rsp), %r14
	cmpq	%rbx, %r14
	je	.LBB5_83
# BB#80:                                # %.lr.ph.i155.preheader974
	leaq	-8(%r14), %rbp
	subq	%rbx, %rbp
	movq	%rbp, (%rsp)            # 8-byte Spill
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	shrl	$3, %ebp
	incl	%ebp
	andl	$7, %ebp
	movq	%rbp, %rcx
	negq	%rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_81:                               # %.lr.ph.i155
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_88 Depth 2
                                        #     Child Loop BB5_90 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%rbp, %rbp
	je	.LBB5_82
# BB#87:                                # %.lr.ph.i.i157.prol.preheader
                                        #   in Loop: Header=BB5_81 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB5_88:                               # %.lr.ph.i.i157.prol
                                        #   Parent Loop BB5_81 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addq	$-8, %rcx
	incq	%rdx
	jne	.LBB5_88
	jmp	.LBB5_89
	.p2align	4, 0x90
.LBB5_82:                               #   in Loop: Header=BB5_81 Depth=1
	movq	%r14, %rcx
.LBB5_89:                               # %.lr.ph.i.i157.prol.loopexit
                                        #   in Loop: Header=BB5_81 Depth=1
	cmpq	$56, (%rsp)             # 8-byte Folded Reload
	jb	.LBB5_91
	.p2align	4, 0x90
.LBB5_90:                               # %.lr.ph.i.i157
                                        #   Parent Loop BB5_81 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addsd	-16(%rcx), %xmm0
	addsd	-24(%rcx), %xmm0
	addsd	-32(%rcx), %xmm0
	addsd	-40(%rcx), %xmm0
	addsd	-48(%rcx), %xmm0
	addsd	-56(%rcx), %xmm0
	addsd	-64(%rcx), %xmm0
	addq	$-64, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB5_90
.LBB5_91:                               # %_ZN9benchmark10accumulateISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEET0_T_SB_SA_.exit.i
                                        #   in Loop: Header=BB5_81 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_92
	jnp	.LBB5_93
.LBB5_92:                               #   in Loop: Header=BB5_81 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_93:                               # %_Z9check_sumd.exit.i160
                                        #   in Loop: Header=BB5_81 Depth=1
	incl	%r13d
	cmpl	%eax, %r13d
	jl	.LBB5_81
	jmp	.LBB5_94
.LBB5_83:                               # %.lr.ph.i155.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_84:                               # %.lr.ph.i155.us
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_85
	jnp	.LBB5_86
.LBB5_85:                               #   in Loop: Header=BB5_84 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_86:                               # %_Z9check_sumd.exit.i160.us
                                        #   in Loop: Header=BB5_84 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_84
.LBB5_94:                               # %_Z15test_accumulateISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEvT_S9_T0_PKc.exit
	testl	%eax, %eax
	jle	.LBB5_158
# BB#95:                                # %.lr.ph.i174.preheader
	cmpq	%r12, %r15
	je	.LBB5_99
# BB#96:                                # %.lr.ph.i174.preheader972
	leaq	-8(%r12), %r13
	subq	%r15, %r13
	movl	%r13d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andl	$7, %ebx
	movq	%rbx, %r14
	negq	%r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_97:                               # %.lr.ph.i174
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_104 Depth 2
                                        #     Child Loop BB5_106 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%rbx, %rbx
	je	.LBB5_98
# BB#103:                               # %.lr.ph.i.i177.prol.preheader
                                        #   in Loop: Header=BB5_97 Depth=1
	movq	%r14, %rdx
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB5_104:                              # %.lr.ph.i.i177.prol
                                        #   Parent Loop BB5_97 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addq	$-8, %rcx
	incq	%rdx
	jne	.LBB5_104
	jmp	.LBB5_105
	.p2align	4, 0x90
.LBB5_98:                               #   in Loop: Header=BB5_97 Depth=1
	movq	%r12, %rcx
.LBB5_105:                              # %.lr.ph.i.i177.prol.loopexit
                                        #   in Loop: Header=BB5_97 Depth=1
	cmpq	$56, %r13
	jb	.LBB5_107
	.p2align	4, 0x90
.LBB5_106:                              # %.lr.ph.i.i177
                                        #   Parent Loop BB5_97 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addsd	-16(%rcx), %xmm0
	addsd	-24(%rcx), %xmm0
	addsd	-32(%rcx), %xmm0
	addsd	-40(%rcx), %xmm0
	addsd	-48(%rcx), %xmm0
	addsd	-56(%rcx), %xmm0
	addsd	-64(%rcx), %xmm0
	addq	$-64, %rcx
	cmpq	%rcx, %r15
	jne	.LBB5_106
.LBB5_107:                              # %_ZN9benchmark10accumulateISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEET0_T_SB_SA_.exit.i179
                                        #   in Loop: Header=BB5_97 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_108
	jnp	.LBB5_109
.LBB5_108:                              #   in Loop: Header=BB5_97 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_109:                              # %_Z9check_sumd.exit.i181
                                        #   in Loop: Header=BB5_97 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB5_97
	jmp	.LBB5_110
.LBB5_99:                               # %.lr.ph.i174.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_100:                              # %.lr.ph.i174.us
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_101
	jnp	.LBB5_102
.LBB5_101:                              #   in Loop: Header=BB5_100 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_102:                              # %_Z9check_sumd.exit.i181.us
                                        #   in Loop: Header=BB5_100 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_100
.LBB5_110:                              # %_Z15test_accumulateISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEvT_S9_T0_PKc.exit.thread
	testl	%eax, %eax
	jle	.LBB5_158
# BB#111:                               # %.lr.ph.i195.preheader
	movq	rrdpb+8(%rip), %r14
	movq	rrdpe+8(%rip), %rbp
	cmpq	%rbp, %r14
	je	.LBB5_115
# BB#112:                               # %.lr.ph.i195.preheader970
	leaq	-8(%rbp), %rbx
	subq	%r14, %rbx
	movq	%rbx, (%rsp)            # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	shrl	$3, %ebx
	incl	%ebx
	andl	$7, %ebx
	movq	%rbx, %rcx
	negq	%rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_113:                              # %.lr.ph.i195
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_120 Depth 2
                                        #     Child Loop BB5_122 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%rbx, %rbx
	je	.LBB5_114
# BB#119:                               # %.lr.ph.i.i198.prol.preheader
                                        #   in Loop: Header=BB5_113 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB5_120:                              # %.lr.ph.i.i198.prol
                                        #   Parent Loop BB5_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_120
	jmp	.LBB5_121
	.p2align	4, 0x90
.LBB5_114:                              #   in Loop: Header=BB5_113 Depth=1
	movq	%r14, %rcx
.LBB5_121:                              # %.lr.ph.i.i198.prol.loopexit
                                        #   in Loop: Header=BB5_113 Depth=1
	cmpq	$56, (%rsp)             # 8-byte Folded Reload
	jb	.LBB5_123
	.p2align	4, 0x90
.LBB5_122:                              # %.lr.ph.i.i198
                                        #   Parent Loop BB5_113 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbp, %rcx
	jne	.LBB5_122
.LBB5_123:                              # %_ZN9benchmark10accumulateISt16reverse_iteratorIS1_IPdEEdEET0_T_S6_S5_.exit.i
                                        #   in Loop: Header=BB5_113 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_124
	jnp	.LBB5_125
.LBB5_124:                              #   in Loop: Header=BB5_113 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_125:                              # %_Z9check_sumd.exit.i201
                                        #   in Loop: Header=BB5_113 Depth=1
	incl	%r13d
	cmpl	%eax, %r13d
	jl	.LBB5_113
	jmp	.LBB5_126
.LBB5_115:                              # %.lr.ph.i195.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_116:                              # %.lr.ph.i195.us
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_117
	jnp	.LBB5_118
.LBB5_117:                              #   in Loop: Header=BB5_116 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_118:                              # %_Z9check_sumd.exit.i201.us
                                        #   in Loop: Header=BB5_116 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_116
.LBB5_126:                              # %_Z15test_accumulateISt16reverse_iteratorIS0_IPdEEdEvT_S4_T0_PKc.exit
	testl	%eax, %eax
	jle	.LBB5_158
# BB#127:                               # %.lr.ph.i213.preheader
	cmpq	%r12, %r15
	je	.LBB5_131
# BB#128:                               # %.lr.ph.i213.preheader968
	leaq	-8(%r12), %r13
	subq	%r15, %r13
	movl	%r13d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andl	$7, %ebx
	movq	%rbx, %r14
	negq	%r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_129:                              # %.lr.ph.i213
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_136 Depth 2
                                        #     Child Loop BB5_138 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%rbx, %rbx
	je	.LBB5_130
# BB#135:                               # %.lr.ph.i.i216.prol.preheader
                                        #   in Loop: Header=BB5_129 Depth=1
	movq	%r14, %rdx
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_136:                              # %.lr.ph.i.i216.prol
                                        #   Parent Loop BB5_129 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_136
	jmp	.LBB5_137
	.p2align	4, 0x90
.LBB5_130:                              #   in Loop: Header=BB5_129 Depth=1
	movq	%r15, %rcx
.LBB5_137:                              # %.lr.ph.i.i216.prol.loopexit
                                        #   in Loop: Header=BB5_129 Depth=1
	cmpq	$56, %r13
	jb	.LBB5_139
	.p2align	4, 0x90
.LBB5_138:                              # %.lr.ph.i.i216
                                        #   Parent Loop BB5_129 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%r12, %rcx
	jne	.LBB5_138
.LBB5_139:                              # %_ZN9benchmark10accumulateISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEET0_T_SC_SB_.exit.i
                                        #   in Loop: Header=BB5_129 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_140
	jnp	.LBB5_141
.LBB5_140:                              #   in Loop: Header=BB5_129 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_141:                              # %_Z9check_sumd.exit.i219
                                        #   in Loop: Header=BB5_129 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB5_129
	jmp	.LBB5_142
.LBB5_131:                              # %.lr.ph.i213.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_132:                              # %.lr.ph.i213.us
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_133
	jnp	.LBB5_134
.LBB5_133:                              #   in Loop: Header=BB5_132 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_134:                              # %_Z9check_sumd.exit.i219.us
                                        #   in Loop: Header=BB5_132 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_132
.LBB5_142:                              # %_Z15test_accumulateISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEvT_SA_T0_PKc.exit
	testl	%eax, %eax
	jle	.LBB5_158
# BB#143:                               # %.lr.ph.i232.preheader
	cmpq	%r12, %r15
	je	.LBB5_147
# BB#144:                               # %.lr.ph.i232.preheader966
	leaq	-8(%r12), %r13
	subq	%r15, %r13
	movl	%r13d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andl	$7, %ebx
	movq	%rbx, %r14
	negq	%r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_145:                              # %.lr.ph.i232
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_152 Depth 2
                                        #     Child Loop BB5_154 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%rbx, %rbx
	je	.LBB5_146
# BB#151:                               # %.lr.ph.i.i235.prol.preheader
                                        #   in Loop: Header=BB5_145 Depth=1
	movq	%r14, %rdx
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_152:                              # %.lr.ph.i.i235.prol
                                        #   Parent Loop BB5_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_152
	jmp	.LBB5_153
.LBB5_146:                              #   in Loop: Header=BB5_145 Depth=1
	movq	%r15, %rcx
.LBB5_153:                              # %.lr.ph.i.i235.prol.loopexit
                                        #   in Loop: Header=BB5_145 Depth=1
	cmpq	$56, %r13
	jb	.LBB5_155
	.p2align	4, 0x90
.LBB5_154:                              # %.lr.ph.i.i235
                                        #   Parent Loop BB5_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%r12, %rcx
	jne	.LBB5_154
.LBB5_155:                              # %_ZN9benchmark10accumulateISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEET0_T_SC_SB_.exit.i237
                                        #   in Loop: Header=BB5_145 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_156
	jnp	.LBB5_157
.LBB5_156:                              #   in Loop: Header=BB5_145 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_157:                              # %_Z9check_sumd.exit.i239
                                        #   in Loop: Header=BB5_145 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB5_145
	jmp	.LBB5_158
.LBB5_147:                              # %.lr.ph.i232.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_148:                              # %.lr.ph.i232.us
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_149
	jnp	.LBB5_150
.LBB5_149:                              #   in Loop: Header=BB5_148 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_150:                              # %_Z9check_sumd.exit.i239.us
                                        #   in Loop: Header=BB5_148 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_148
.LBB5_158:                              # %.loopexit716
	cltq
	imulq	$274877907, %rax, %rax  # imm = 0x10624DD3
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$38, %rax
	addl	%ecx, %eax
	movl	%eax, iterations(%rip)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 96(%rsp)
	movq	$0, 112(%rsp)
	movq	$0, 432(%rsp)
.Ltmp3:
	leaq	96(%rsp), %rdi
	leaq	432(%rsp), %rcx
	xorl	%esi, %esi
	movl	$2000, %edx             # imm = 0x7D0
	callq	_ZNSt6vectorIdSaIdEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPdS1_EEmRKd
.Ltmp4:
# BB#159:
	movq	dMpb(%rip), %rbx
	movq	dMpe(%rip), %rbp
	cmpq	%rbp, %rbx
	movabsq	$4611686018427387900, %r14 # imm = 0x3FFFFFFFFFFFFFFC
	je	.LBB5_160
	.p2align	4, 0x90
.LBB5_161:                              # %.lr.ph.i254
                                        # =>This Inner Loop Header: Depth=1
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %rbp
	jne	.LBB5_161
# BB#162:                               # %.loopexit606
	movq	dMpb(%rip), %rbx
	movq	dMpe(%rip), %rsi
	movq	96(%rsp), %r9
	cmpq	%rsi, %rbx
	movq	%r9, (%rsp)             # 8-byte Spill
	je	.LBB5_181
# BB#163:                               # %.lr.ph.i256.preheader
	leaq	-8(%rsi), %r8
	movq	%r8, %rax
	subq	%rbx, %rax
	movq	%rax, %rcx
	shrq	$3, %rcx
	incq	%rcx
	cmpq	$4, %rcx
	movq	%rbx, %rdx
	movq	%r9, %rdi
	jb	.LBB5_176
# BB#164:                               # %min.iters.checked914
	andq	%rcx, %r14
	movq	%rbx, %rdx
	movq	%r9, %rdi
	je	.LBB5_176
# BB#165:                               # %vector.memcheck
	andq	$-8, %rax
	leaq	8(%rbx,%rax), %rdx
	cmpq	%rdx, %r9
	jae	.LBB5_167
# BB#166:                               # %vector.memcheck
	leaq	8(%r9,%rax), %rax
	cmpq	%rax, %rbx
	movq	%rbx, %rdx
	movq	%r9, %rdi
	jb	.LBB5_176
.LBB5_167:                              # %vector.body905.preheader
	leaq	-4(%r14), %rdx
	movl	%edx, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB5_168
# BB#169:                               # %vector.body905.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_170:                              # %vector.body905.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbx,%rbp,8), %xmm0
	movupd	16(%rbx,%rbp,8), %xmm1
	movupd	%xmm0, (%r9,%rbp,8)
	movupd	%xmm1, 16(%r9,%rbp,8)
	addq	$4, %rbp
	incq	%rdi
	jne	.LBB5_170
	jmp	.LBB5_171
.LBB5_160:                              # %.loopexit606.thread
	movq	96(%rsp), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbx, %rsi
	jmp	.LBB5_181
.LBB5_168:
	xorl	%ebp, %ebp
.LBB5_171:                              # %vector.body905.prol.loopexit
	cmpq	$12, %rdx
	jb	.LBB5_174
# BB#172:                               # %vector.body905.preheader.new
	movq	%r14, %rdx
	subq	%rbp, %rdx
	leaq	112(%r9,%rbp,8), %rdi
	leaq	112(%rbx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB5_173:                              # %vector.body905
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movupd	-16(%rbp), %xmm0
	movupd	(%rbp), %xmm1
	movupd	%xmm0, -16(%rdi)
	movupd	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbp
	addq	$-16, %rdx
	jne	.LBB5_173
.LBB5_174:                              # %middle.block906
	cmpq	%r14, %rcx
	je	.LBB5_181
# BB#175:
	leaq	(%rbx,%r14,8), %rdx
	leaq	(%r9,%r14,8), %rdi
.LBB5_176:                              # %.lr.ph.i256.preheader965
	subq	%rdx, %r8
	movl	%r8d, %ecx
	shrl	$3, %ecx
	incl	%ecx
	andq	$7, %rcx
	je	.LBB5_179
# BB#177:                               # %.lr.ph.i256.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB5_178:                              # %.lr.ph.i256.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rax
	addq	$8, %rdx
	movq	%rax, (%rdi)
	addq	$8, %rdi
	incq	%rcx
	jne	.LBB5_178
.LBB5_179:                              # %.lr.ph.i256.prol.loopexit
	cmpq	$56, %r8
	jb	.LBB5_181
	.p2align	4, 0x90
.LBB5_180:                              # %.lr.ph.i256
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rax
	movq	%rax, (%rdi)
	movq	8(%rdx), %rax
	movq	%rax, 8(%rdi)
	movq	16(%rdx), %rax
	movq	%rax, 16(%rdi)
	movq	24(%rdx), %rax
	movq	%rax, 24(%rdi)
	movq	32(%rdx), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rdx), %rax
	movq	%rax, 40(%rdi)
	movq	48(%rdx), %rax
	movq	%rax, 48(%rdi)
	movq	56(%rdx), %rax
	movq	%rax, 56(%rdi)
	addq	$64, %rdx
	addq	$64, %rdi
	cmpq	%rsi, %rdx
	jne	.LBB5_180
.LBB5_181:                              # %.loopexit605
	movq	104(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	dpb(%rip), %rdx
	movq	dpe(%rip), %rcx
.Ltmp6:
	xorpd	%xmm0, %xmm0
	movl	$.L.str.26, %r8d
	movq	%rbx, %rdi
	callq	_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc
.Ltmp7:
# BB#182:
	movq	96(%rsp), %rdi
	movq	104(%rsp), %rsi
	movq	64(%rsp), %rdx
	movq	72(%rsp), %rcx
.Ltmp8:
	xorpd	%xmm0, %xmm0
	movl	$.L.str.27, %r8d
	callq	_Z19test_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
.Ltmp9:
# BB#183:
	movl	iterations(%rip), %r8d
	testl	%r8d, %r8d
	jle	.LBB5_522
# BB#184:                               # %.lr.ph.i332.preheader
	movq	rdMpb(%rip), %r13
	movq	rdMpe(%rip), %r14
	movq	rdpb(%rip), %rbx
	movq	rdpe(%rip), %rbp
	leaq	-8(%rbx), %r9
	cmpq	%r14, %r13
	movq	(%rsp), %r10            # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r9, 40(%rsp)           # 8-byte Spill
	je	.LBB5_197
# BB#185:                               # %.lr.ph.i332.preheader963
	leaq	-8(%r13), %r11
	subq	%r14, %r11
	movl	%r11d, %ecx
	shrl	$3, %ecx
	incl	%ecx
	leaq	-16(%rbx), %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	subq	%rbp, %rdx
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$1, %edx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	negq	%rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%r15, 232(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB5_186:                              # %.lr.ph.i332
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_188 Depth 2
                                        #     Child Loop BB5_190 Depth 2
                                        #     Child Loop BB5_194 Depth 2
                                        #     Child Loop BB5_225 Depth 2
                                        #       Child Loop BB5_227 Depth 3
                                        #       Child Loop BB5_231 Depth 3
                                        #     Child Loop BB5_236 Depth 2
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	%rbx, %rcx
	movq	%r13, %rdx
	je	.LBB5_189
# BB#187:                               # %.lr.ph.i3.i335.prol.preheader
                                        #   in Loop: Header=BB5_186 Depth=1
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rcx
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB5_188:                              # %.lr.ph.i3.i335.prol
                                        #   Parent Loop BB5_186 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx), %rdi
	addq	$-8, %rdx
	movq	%rdi, -8(%rcx)
	addq	$-8, %rcx
	incq	%rsi
	jne	.LBB5_188
.LBB5_189:                              # %.lr.ph.i3.i335.prol.loopexit
                                        #   in Loop: Header=BB5_186 Depth=1
	cmpq	$56, %r11
	jb	.LBB5_191
	.p2align	4, 0x90
.LBB5_190:                              # %.lr.ph.i3.i335
                                        #   Parent Loop BB5_186 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	movq	-32(%rdx), %rsi
	movq	%rsi, -32(%rcx)
	movq	-40(%rdx), %rsi
	movq	%rsi, -40(%rcx)
	movq	-48(%rdx), %rsi
	movq	%rsi, -48(%rcx)
	movq	-56(%rdx), %rsi
	movq	%rsi, -56(%rcx)
	movq	-64(%rdx), %rsi
	addq	$-64, %rdx
	movq	%rsi, -64(%rcx)
	addq	$-64, %rcx
	cmpq	%rdx, %r14
	jne	.LBB5_190
.LBB5_191:                              # %_ZN9benchmark4copyISt16reverse_iteratorIPdES3_EEvT_S4_T0_.exit.i336
                                        #   in Loop: Header=BB5_186 Depth=1
	cmpq	%rbp, %r9
	je	.LBB5_235
# BB#192:                               # %.lr.ph25.i.i.preheader
                                        #   in Loop: Header=BB5_186 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movq	%r9, %rcx
	jne	.LBB5_224
# BB#193:                               # %.lr.ph.i.i337.preheader.prol
                                        #   in Loop: Header=BB5_186 Depth=1
	movsd	-16(%rbx), %xmm0        # xmm0 = mem[0],zero
	movq	$-8, %rcx
	.p2align	4, 0x90
.LBB5_194:                              # %.lr.ph.i.i337.prol
                                        #   Parent Loop BB5_186 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbx,%rcx), %xmm1      # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_222
# BB#195:                               #   in Loop: Header=BB5_194 Depth=2
	movsd	%xmm1, -8(%rbx,%rcx)
	addq	$8, %rcx
	jne	.LBB5_194
# BB#196:                               #   in Loop: Header=BB5_186 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB5_223
.LBB5_222:                              # %.lr.ph.i.i337.prol..critedge.i.i339.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_186 Depth=1
	addq	%rbx, %rcx
.LBB5_223:                              # %.critedge.i.i339.prol
                                        #   in Loop: Header=BB5_186 Depth=1
	movsd	%xmm0, -8(%rcx)
	movq	120(%rsp), %rcx         # 8-byte Reload
.LBB5_224:                              # %.lr.ph25.i.i.prol.loopexit
                                        #   in Loop: Header=BB5_186 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB5_235
	.p2align	4, 0x90
.LBB5_225:                              # %.lr.ph25.i.i
                                        #   Parent Loop BB5_186 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_227 Depth 3
                                        #       Child Loop BB5_231 Depth 3
	leaq	-8(%rcx), %rdx
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	je	.LBB5_229
# BB#226:                               # %.lr.ph.i.i337.preheader
                                        #   in Loop: Header=BB5_225 Depth=2
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_227:                              # %.lr.ph.i.i337
                                        #   Parent Loop BB5_186 Depth=1
                                        #     Parent Loop BB5_225 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_230
# BB#228:                               #   in Loop: Header=BB5_227 Depth=3
	movsd	%xmm1, -8(%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB5_227
.LBB5_229:                              #   in Loop: Header=BB5_225 Depth=2
	movq	%rbx, %rsi
.LBB5_230:                              # %.critedge.i.i339
                                        #   in Loop: Header=BB5_225 Depth=2
	movsd	%xmm0, -8(%rsi)
	movsd	-16(%rcx), %xmm0        # xmm0 = mem[0],zero
	addq	$-16, %rcx
	cmpq	%rbx, %rdx
	je	.LBB5_233
	.p2align	4, 0x90
.LBB5_231:                              # %.lr.ph.i.i337.1
                                        #   Parent Loop BB5_186 Depth=1
                                        #     Parent Loop BB5_225 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_234
# BB#232:                               #   in Loop: Header=BB5_231 Depth=3
	movsd	%xmm1, -8(%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB5_231
.LBB5_233:                              #   in Loop: Header=BB5_225 Depth=2
	movq	%rbx, %rdx
.LBB5_234:                              # %.critedge.i.i339.1
                                        #   in Loop: Header=BB5_225 Depth=2
	movsd	%xmm0, -8(%rdx)
	cmpq	%rbp, %rcx
	jne	.LBB5_225
.LBB5_235:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIPdEdEEvT_S4_.exit.i.preheader
                                        #   in Loop: Header=BB5_186 Depth=1
	movq	%r9, %rcx
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB5_236:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIPdEdEEvT_S4_.exit.i
                                        #   Parent Loop BB5_186 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %rbp
	je	.LBB5_239
# BB#237:                               #   in Loop: Header=BB5_236 Depth=2
	movsd	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rdx
	ucomisd	-8(%rcx), %xmm0
	leaq	-8(%rcx), %rcx
	jbe	.LBB5_236
# BB#238:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIPdEEEbT_S4_.exit.i.i343
                                        #   in Loop: Header=BB5_186 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	movq	%r11, %r15
	callq	printf
	movq	%r15, %r11
	movq	40(%rsp), %r9           # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB5_239:                              # %_Z13verify_sortedISt16reverse_iteratorIPdEEvT_S3_.exit.i344
                                        #   in Loop: Header=BB5_186 Depth=1
	movl	8(%rsp), %ecx           # 4-byte Reload
	incl	%ecx
	cmpl	%r8d, %ecx
	movq	(%rsp), %r10            # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	232(%rsp), %r15         # 8-byte Reload
	jl	.LBB5_186
	jmp	.LBB5_240
.LBB5_197:                              # %.lr.ph.i332.us.preheader
	leaq	-16(%rbx), %r13
	movq	%r13, 24(%rsp)          # 8-byte Spill
	subq	%rbp, %r13
	shrq	$3, %r13
	movl	%r13d, %eax
	andl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_198:                              # %.lr.ph.i332.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_201 Depth 2
                                        #     Child Loop BB5_207 Depth 2
                                        #       Child Loop BB5_209 Depth 3
                                        #       Child Loop BB5_213 Depth 3
                                        #     Child Loop BB5_218 Depth 2
	cmpq	%rbp, %r9
	je	.LBB5_217
# BB#199:                               # %.lr.ph25.i.i.us.preheader
                                        #   in Loop: Header=BB5_198 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r9, %rcx
	jne	.LBB5_206
# BB#200:                               # %.lr.ph.i.i337.us.preheader.prol
                                        #   in Loop: Header=BB5_198 Depth=1
	movsd	-16(%rbx), %xmm0        # xmm0 = mem[0],zero
	movq	$-8, %rcx
	.p2align	4, 0x90
.LBB5_201:                              # %.lr.ph.i.i337.us.prol
                                        #   Parent Loop BB5_198 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbx,%rcx), %xmm1      # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_204
# BB#202:                               #   in Loop: Header=BB5_201 Depth=2
	movsd	%xmm1, -8(%rbx,%rcx)
	addq	$8, %rcx
	jne	.LBB5_201
# BB#203:                               #   in Loop: Header=BB5_198 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB5_205
.LBB5_204:                              # %.lr.ph.i.i337.us.prol..critedge.i.i339.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_198 Depth=1
	addq	%rbx, %rcx
.LBB5_205:                              # %.critedge.i.i339.us.prol
                                        #   in Loop: Header=BB5_198 Depth=1
	movsd	%xmm0, -8(%rcx)
	movq	24(%rsp), %rcx          # 8-byte Reload
.LBB5_206:                              # %.lr.ph25.i.i.us.prol.loopexit
                                        #   in Loop: Header=BB5_198 Depth=1
	testq	%r13, %r13
	je	.LBB5_217
	.p2align	4, 0x90
.LBB5_207:                              # %.lr.ph25.i.i.us
                                        #   Parent Loop BB5_198 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_209 Depth 3
                                        #       Child Loop BB5_213 Depth 3
	leaq	-8(%rcx), %rdx
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	je	.LBB5_211
# BB#208:                               # %.lr.ph.i.i337.us.preheader
                                        #   in Loop: Header=BB5_207 Depth=2
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_209:                              # %.lr.ph.i.i337.us
                                        #   Parent Loop BB5_198 Depth=1
                                        #     Parent Loop BB5_207 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_212
# BB#210:                               #   in Loop: Header=BB5_209 Depth=3
	movsd	%xmm1, -8(%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB5_209
.LBB5_211:                              #   in Loop: Header=BB5_207 Depth=2
	movq	%rbx, %rsi
.LBB5_212:                              # %.critedge.i.i339.us
                                        #   in Loop: Header=BB5_207 Depth=2
	movsd	%xmm0, -8(%rsi)
	movsd	-16(%rcx), %xmm0        # xmm0 = mem[0],zero
	addq	$-16, %rcx
	cmpq	%rbx, %rdx
	je	.LBB5_215
	.p2align	4, 0x90
.LBB5_213:                              # %.lr.ph.i.i337.us.1
                                        #   Parent Loop BB5_198 Depth=1
                                        #     Parent Loop BB5_207 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_216
# BB#214:                               #   in Loop: Header=BB5_213 Depth=3
	movsd	%xmm1, -8(%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB5_213
.LBB5_215:                              #   in Loop: Header=BB5_207 Depth=2
	movq	%rbx, %rdx
.LBB5_216:                              # %.critedge.i.i339.us.1
                                        #   in Loop: Header=BB5_207 Depth=2
	movsd	%xmm0, -8(%rdx)
	cmpq	%rbp, %rcx
	jne	.LBB5_207
.LBB5_217:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIPdEdEEvT_S4_.exit.i.us.preheader
                                        #   in Loop: Header=BB5_198 Depth=1
	movq	%r9, %rcx
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB5_218:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIPdEdEEvT_S4_.exit.i.us
                                        #   Parent Loop BB5_198 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %rbp
	je	.LBB5_221
# BB#219:                               #   in Loop: Header=BB5_218 Depth=2
	movsd	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rdx
	ucomisd	-8(%rcx), %xmm0
	leaq	-8(%rcx), %rcx
	jbe	.LBB5_218
# BB#220:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIPdEEEbT_S4_.exit.i.i343.us
                                        #   in Loop: Header=BB5_198 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rsp), %r10            # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB5_221:                              # %_Z13verify_sortedISt16reverse_iteratorIPdEEvT_S3_.exit.i344.us
                                        #   in Loop: Header=BB5_198 Depth=1
	incl	%r14d
	cmpl	%r8d, %r14d
	jl	.LBB5_198
.LBB5_240:                              # %.us-lcssa629.us
	testl	%r8d, %r8d
	jle	.LBB5_522
# BB#241:                               # %.lr.ph.i315.preheader
	movq	96(%rsp), %r14
	movq	104(%rsp), %rax
	movq	64(%rsp), %rbx
	movq	72(%rsp), %rbp
	leaq	-8(%rbp), %r9
	cmpq	%r14, %rax
	movq	%r9, 32(%rsp)           # 8-byte Spill
	je	.LBB5_254
# BB#242:                               # %.lr.ph.i315.preheader961
	leaq	-8(%rax), %r13
	subq	%r14, %r13
	movl	%r13d, %ecx
	shrl	$3, %ecx
	incl	%ecx
	leaq	-16(%rbp), %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	subq	%rbx, %rdx
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$1, %edx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	negq	%rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_243:                              # %.lr.ph.i315
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_245 Depth 2
                                        #     Child Loop BB5_247 Depth 2
                                        #     Child Loop BB5_251 Depth 2
                                        #     Child Loop BB5_282 Depth 2
                                        #       Child Loop BB5_284 Depth 3
                                        #       Child Loop BB5_288 Depth 3
                                        #     Child Loop BB5_293 Depth 2
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	movq	%rbp, %rcx
	movq	%rax, %rdx
	je	.LBB5_246
# BB#244:                               # %.lr.ph.i3.i318.prol.preheader
                                        #   in Loop: Header=BB5_243 Depth=1
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	%rbp, %rcx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB5_245:                              # %.lr.ph.i3.i318.prol
                                        #   Parent Loop BB5_243 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx), %rdi
	addq	$-8, %rdx
	movq	%rdi, -8(%rcx)
	addq	$-8, %rcx
	incq	%rsi
	jne	.LBB5_245
.LBB5_246:                              # %.lr.ph.i3.i318.prol.loopexit
                                        #   in Loop: Header=BB5_243 Depth=1
	cmpq	$56, %r13
	jb	.LBB5_248
	.p2align	4, 0x90
.LBB5_247:                              # %.lr.ph.i3.i318
                                        #   Parent Loop BB5_243 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	movq	-32(%rdx), %rsi
	movq	%rsi, -32(%rcx)
	movq	-40(%rdx), %rsi
	movq	%rsi, -40(%rcx)
	movq	-48(%rdx), %rsi
	movq	%rsi, -48(%rcx)
	movq	-56(%rdx), %rsi
	movq	%rsi, -56(%rcx)
	movq	-64(%rdx), %rsi
	addq	$-64, %rdx
	movq	%rsi, -64(%rcx)
	addq	$-64, %rcx
	cmpq	%rdx, %r14
	jne	.LBB5_247
.LBB5_248:                              # %_ZN9benchmark4copyISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEES9_EEvT_SA_T0_.exit.i319
                                        #   in Loop: Header=BB5_243 Depth=1
	cmpq	%rbx, %r9
	je	.LBB5_292
# BB#249:                               # %.lr.ph29.i.i321.preheader
                                        #   in Loop: Header=BB5_243 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	movq	%r9, %rcx
	jne	.LBB5_281
# BB#250:                               # %.lr.ph.i.i322.preheader.prol
                                        #   in Loop: Header=BB5_243 Depth=1
	movsd	-16(%rbp), %xmm0        # xmm0 = mem[0],zero
	movq	$-8, %rcx
	.p2align	4, 0x90
.LBB5_251:                              # %.lr.ph.i.i322.prol
                                        #   Parent Loop BB5_243 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp,%rcx), %xmm1      # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_279
# BB#252:                               #   in Loop: Header=BB5_251 Depth=2
	movsd	%xmm1, -8(%rbp,%rcx)
	addq	$8, %rcx
	jne	.LBB5_251
# BB#253:                               #   in Loop: Header=BB5_243 Depth=1
	movq	%rbp, %rcx
	jmp	.LBB5_280
.LBB5_279:                              # %.lr.ph.i.i322.prol..critedge.i.i324.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_243 Depth=1
	addq	%rbp, %rcx
.LBB5_280:                              # %.critedge.i.i324.prol
                                        #   in Loop: Header=BB5_243 Depth=1
	movsd	%xmm0, -8(%rcx)
	movq	128(%rsp), %rcx         # 8-byte Reload
.LBB5_281:                              # %.lr.ph29.i.i321.prol.loopexit
                                        #   in Loop: Header=BB5_243 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB5_292
	.p2align	4, 0x90
.LBB5_282:                              # %.lr.ph29.i.i321
                                        #   Parent Loop BB5_243 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_284 Depth 3
                                        #       Child Loop BB5_288 Depth 3
	leaq	-8(%rcx), %rdx
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cmpq	%rbp, %rcx
	je	.LBB5_286
# BB#283:                               # %.lr.ph.i.i322.preheader
                                        #   in Loop: Header=BB5_282 Depth=2
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_284:                              # %.lr.ph.i.i322
                                        #   Parent Loop BB5_243 Depth=1
                                        #     Parent Loop BB5_282 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_287
# BB#285:                               #   in Loop: Header=BB5_284 Depth=3
	movsd	%xmm1, -8(%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %rbp
	jne	.LBB5_284
.LBB5_286:                              #   in Loop: Header=BB5_282 Depth=2
	movq	%rbp, %rsi
.LBB5_287:                              # %.critedge.i.i324
                                        #   in Loop: Header=BB5_282 Depth=2
	movsd	%xmm0, -8(%rsi)
	movsd	-16(%rcx), %xmm0        # xmm0 = mem[0],zero
	addq	$-16, %rcx
	cmpq	%rbp, %rdx
	je	.LBB5_290
	.p2align	4, 0x90
.LBB5_288:                              # %.lr.ph.i.i322.1
                                        #   Parent Loop BB5_243 Depth=1
                                        #     Parent Loop BB5_282 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_291
# BB#289:                               #   in Loop: Header=BB5_288 Depth=3
	movsd	%xmm1, -8(%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %rbp
	jne	.LBB5_288
.LBB5_290:                              #   in Loop: Header=BB5_282 Depth=2
	movq	%rbp, %rdx
.LBB5_291:                              # %.critedge.i.i324.1
                                        #   in Loop: Header=BB5_282 Depth=2
	movsd	%xmm0, -8(%rdx)
	cmpq	%rbx, %rcx
	jne	.LBB5_282
.LBB5_292:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_.exit.i326.preheader
                                        #   in Loop: Header=BB5_243 Depth=1
	movq	%r9, %rcx
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB5_293:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_.exit.i326
                                        #   Parent Loop BB5_243 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %rbx
	je	.LBB5_296
# BB#294:                               #   in Loop: Header=BB5_293 Depth=2
	movsd	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rdx
	ucomisd	-8(%rcx), %xmm0
	leaq	-8(%rcx), %rcx
	jbe	.LBB5_293
# BB#295:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i329
                                        #   in Loop: Header=BB5_243 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB5_296:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i330
                                        #   in Loop: Header=BB5_243 Depth=1
	movl	8(%rsp), %ecx           # 4-byte Reload
	incl	%ecx
	cmpl	%r8d, %ecx
	movq	(%rsp), %r10            # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jl	.LBB5_243
	jmp	.LBB5_297
.LBB5_254:                              # %.lr.ph.i315.us.preheader
	leaq	-16(%rbp), %r13
	movq	%r13, 24(%rsp)          # 8-byte Spill
	subq	%rbx, %r13
	shrq	$3, %r13
	movl	%r13d, %eax
	andl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_255:                              # %.lr.ph.i315.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_258 Depth 2
                                        #     Child Loop BB5_264 Depth 2
                                        #       Child Loop BB5_266 Depth 3
                                        #       Child Loop BB5_270 Depth 3
                                        #     Child Loop BB5_275 Depth 2
	cmpq	%rbx, %r9
	je	.LBB5_274
# BB#256:                               # %.lr.ph29.i.i321.us.preheader
                                        #   in Loop: Header=BB5_255 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r9, %rcx
	jne	.LBB5_263
# BB#257:                               # %.lr.ph.i.i322.us.preheader.prol
                                        #   in Loop: Header=BB5_255 Depth=1
	movsd	-16(%rbp), %xmm0        # xmm0 = mem[0],zero
	movq	$-8, %rcx
	.p2align	4, 0x90
.LBB5_258:                              # %.lr.ph.i.i322.us.prol
                                        #   Parent Loop BB5_255 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp,%rcx), %xmm1      # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_261
# BB#259:                               #   in Loop: Header=BB5_258 Depth=2
	movsd	%xmm1, -8(%rbp,%rcx)
	addq	$8, %rcx
	jne	.LBB5_258
# BB#260:                               #   in Loop: Header=BB5_255 Depth=1
	movq	%rbp, %rcx
	jmp	.LBB5_262
.LBB5_261:                              # %.lr.ph.i.i322.us.prol..critedge.i.i324.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_255 Depth=1
	addq	%rbp, %rcx
.LBB5_262:                              # %.critedge.i.i324.us.prol
                                        #   in Loop: Header=BB5_255 Depth=1
	movsd	%xmm0, -8(%rcx)
	movq	24(%rsp), %rcx          # 8-byte Reload
.LBB5_263:                              # %.lr.ph29.i.i321.us.prol.loopexit
                                        #   in Loop: Header=BB5_255 Depth=1
	testq	%r13, %r13
	je	.LBB5_274
	.p2align	4, 0x90
.LBB5_264:                              # %.lr.ph29.i.i321.us
                                        #   Parent Loop BB5_255 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_266 Depth 3
                                        #       Child Loop BB5_270 Depth 3
	leaq	-8(%rcx), %rdx
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cmpq	%rbp, %rcx
	je	.LBB5_268
# BB#265:                               # %.lr.ph.i.i322.us.preheader
                                        #   in Loop: Header=BB5_264 Depth=2
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_266:                              # %.lr.ph.i.i322.us
                                        #   Parent Loop BB5_255 Depth=1
                                        #     Parent Loop BB5_264 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_269
# BB#267:                               #   in Loop: Header=BB5_266 Depth=3
	movsd	%xmm1, -8(%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %rbp
	jne	.LBB5_266
.LBB5_268:                              #   in Loop: Header=BB5_264 Depth=2
	movq	%rbp, %rsi
.LBB5_269:                              # %.critedge.i.i324.us
                                        #   in Loop: Header=BB5_264 Depth=2
	movsd	%xmm0, -8(%rsi)
	movsd	-16(%rcx), %xmm0        # xmm0 = mem[0],zero
	addq	$-16, %rcx
	cmpq	%rbp, %rdx
	je	.LBB5_272
	.p2align	4, 0x90
.LBB5_270:                              # %.lr.ph.i.i322.us.1
                                        #   Parent Loop BB5_255 Depth=1
                                        #     Parent Loop BB5_264 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_273
# BB#271:                               #   in Loop: Header=BB5_270 Depth=3
	movsd	%xmm1, -8(%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %rbp
	jne	.LBB5_270
.LBB5_272:                              #   in Loop: Header=BB5_264 Depth=2
	movq	%rbp, %rdx
.LBB5_273:                              # %.critedge.i.i324.us.1
                                        #   in Loop: Header=BB5_264 Depth=2
	movsd	%xmm0, -8(%rdx)
	cmpq	%rbx, %rcx
	jne	.LBB5_264
.LBB5_274:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_.exit.i326.us.preheader
                                        #   in Loop: Header=BB5_255 Depth=1
	movq	%r9, %rcx
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB5_275:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_.exit.i326.us
                                        #   Parent Loop BB5_255 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %rbx
	je	.LBB5_278
# BB#276:                               #   in Loop: Header=BB5_275 Depth=2
	movsd	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rdx
	ucomisd	-8(%rcx), %xmm0
	leaq	-8(%rcx), %rcx
	jbe	.LBB5_275
# BB#277:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i329.us
                                        #   in Loop: Header=BB5_255 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rsp), %r10            # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB5_278:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i330.us
                                        #   in Loop: Header=BB5_255 Depth=1
	incl	%r14d
	cmpl	%r8d, %r14d
	jl	.LBB5_255
.LBB5_297:                              # %_Z19test_insertion_sortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEvT_S9_S9_S9_T0_PKc.exit331
	testl	%r8d, %r8d
	jle	.LBB5_522
# BB#298:                               # %.lr.ph.i304.preheader
	leaq	-8(%r12), %rbx
	cmpq	%r10, %rdi
	je	.LBB5_302
# BB#299:                               # %.lr.ph.i304.preheader959
	leaq	-8(%rdi), %r14
	subq	%r10, %r14
	movl	%r14d, %r13d
	shrl	$3, %r13d
	incl	%r13d
	leaq	-16(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	subq	%r15, %rax
	shrq	$3, %rax
	andl	$7, %r13d
	movq	%rax, 24(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rax
	negq	%rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_300:                              # %.lr.ph.i304
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_326 Depth 2
                                        #     Child Loop BB5_328 Depth 2
                                        #     Child Loop BB5_332 Depth 2
                                        #     Child Loop BB5_338 Depth 2
                                        #       Child Loop BB5_340 Depth 3
                                        #       Child Loop BB5_344 Depth 3
                                        #     Child Loop BB5_347 Depth 2
	testq	%r13, %r13
	je	.LBB5_301
# BB#325:                               # %.lr.ph.i3.i305.prol.preheader
                                        #   in Loop: Header=BB5_300 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rcx
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB5_326:                              # %.lr.ph.i3.i305.prol
                                        #   Parent Loop BB5_300 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx), %rdi
	addq	$-8, %rdx
	movq	%rdi, -8(%rcx)
	addq	$-8, %rcx
	incq	%rsi
	jne	.LBB5_326
	jmp	.LBB5_327
	.p2align	4, 0x90
.LBB5_301:                              #   in Loop: Header=BB5_300 Depth=1
	movq	%r12, %rcx
	movq	%rdi, %rdx
.LBB5_327:                              # %.lr.ph.i3.i305.prol.loopexit
                                        #   in Loop: Header=BB5_300 Depth=1
	cmpq	$56, %r14
	jb	.LBB5_329
	.p2align	4, 0x90
.LBB5_328:                              # %.lr.ph.i3.i305
                                        #   Parent Loop BB5_300 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	movq	-32(%rdx), %rsi
	movq	%rsi, -32(%rcx)
	movq	-40(%rdx), %rsi
	movq	%rsi, -40(%rcx)
	movq	-48(%rdx), %rsi
	movq	%rsi, -48(%rcx)
	movq	-56(%rdx), %rsi
	movq	%rsi, -56(%rcx)
	movq	-64(%rdx), %rsi
	addq	$-64, %rdx
	movq	%rsi, -64(%rcx)
	addq	$-64, %rcx
	cmpq	%rdx, %r10
	jne	.LBB5_328
.LBB5_329:                              # %_ZN9benchmark4copyISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEES9_EEvT_SA_T0_.exit.i306
                                        #   in Loop: Header=BB5_300 Depth=1
	cmpq	%r15, %rbx
	je	.LBB5_346
# BB#330:                               # %.lr.ph29.i.i.preheader
                                        #   in Loop: Header=BB5_300 Depth=1
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	movq	%rbx, %rcx
	jne	.LBB5_337
# BB#331:                               # %.lr.ph.i.i307.preheader.prol
                                        #   in Loop: Header=BB5_300 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movq	$-8, %rcx
	.p2align	4, 0x90
.LBB5_332:                              # %.lr.ph.i.i307.prol
                                        #   Parent Loop BB5_300 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r12,%rcx), %xmm1      # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_335
# BB#333:                               #   in Loop: Header=BB5_332 Depth=2
	movsd	%xmm1, -8(%r12,%rcx)
	addq	$8, %rcx
	jne	.LBB5_332
# BB#334:                               #   in Loop: Header=BB5_300 Depth=1
	movq	%r12, %rcx
	jmp	.LBB5_336
.LBB5_335:                              # %.lr.ph.i.i307.prol..critedge.i.i309.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_300 Depth=1
	addq	%r12, %rcx
.LBB5_336:                              # %.critedge.i.i309.prol
                                        #   in Loop: Header=BB5_300 Depth=1
	movsd	%xmm0, -8(%rcx)
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB5_337:                              # %.lr.ph29.i.i.prol.loopexit
                                        #   in Loop: Header=BB5_300 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB5_346
	.p2align	4, 0x90
.LBB5_338:                              # %.lr.ph29.i.i
                                        #   Parent Loop BB5_300 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_340 Depth 3
                                        #       Child Loop BB5_344 Depth 3
	leaq	-8(%rcx), %rdx
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cmpq	%r12, %rcx
	movq	%r12, %rsi
	je	.LBB5_343
# BB#339:                               # %.lr.ph.i.i307.preheader
                                        #   in Loop: Header=BB5_338 Depth=2
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_340:                              # %.lr.ph.i.i307
                                        #   Parent Loop BB5_300 Depth=1
                                        #     Parent Loop BB5_338 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_343
# BB#341:                               #   in Loop: Header=BB5_340 Depth=3
	movsd	%xmm1, -8(%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %r12
	jne	.LBB5_340
# BB#342:                               #   in Loop: Header=BB5_338 Depth=2
	movq	%r12, %rsi
.LBB5_343:                              # %.critedge.i.i309
                                        #   in Loop: Header=BB5_338 Depth=2
	movsd	%xmm0, -8(%rsi)
	movsd	-16(%rcx), %xmm0        # xmm0 = mem[0],zero
	addq	$-16, %rcx
	cmpq	%r12, %rdx
	movq	%r12, %rsi
	je	.LBB5_838
	.p2align	4, 0x90
.LBB5_344:                              # %.lr.ph.i.i307.1
                                        #   Parent Loop BB5_300 Depth=1
                                        #     Parent Loop BB5_338 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_345
# BB#836:                               #   in Loop: Header=BB5_344 Depth=3
	movsd	%xmm1, -8(%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %r12
	jne	.LBB5_344
# BB#837:                               #   in Loop: Header=BB5_338 Depth=2
	movq	%r12, %rsi
	jmp	.LBB5_838
	.p2align	4, 0x90
.LBB5_345:                              #   in Loop: Header=BB5_338 Depth=2
	movq	%rdx, %rsi
.LBB5_838:                              # %.critedge.i.i309.1
                                        #   in Loop: Header=BB5_338 Depth=2
	movsd	%xmm0, -8(%rsi)
	cmpq	%r15, %rcx
	jne	.LBB5_338
.LBB5_346:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_.exit.i.preheader
                                        #   in Loop: Header=BB5_300 Depth=1
	movq	%rbx, %rcx
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB5_347:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_.exit.i
                                        #   Parent Loop BB5_300 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r15
	je	.LBB5_350
# BB#348:                               #   in Loop: Header=BB5_347 Depth=2
	movsd	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rdx
	ucomisd	-8(%rcx), %xmm0
	leaq	-8(%rcx), %rcx
	jbe	.LBB5_347
# BB#349:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i313
                                        #   in Loop: Header=BB5_300 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %r10            # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB5_350:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i314
                                        #   in Loop: Header=BB5_300 Depth=1
	incl	%ebp
	cmpl	%r8d, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
	jl	.LBB5_300
	jmp	.LBB5_351
.LBB5_302:                              # %.lr.ph.i304.us.preheader
	leaq	-16(%r12), %rbp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	subq	%r15, %rbp
	shrq	$3, %rbp
	movl	%ebp, %r13d
	andl	$1, %r13d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_303:                              # %.lr.ph.i304.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_306 Depth 2
                                        #     Child Loop BB5_312 Depth 2
                                        #       Child Loop BB5_314 Depth 3
                                        #       Child Loop BB5_318 Depth 3
                                        #     Child Loop BB5_321 Depth 2
	cmpq	%r15, %rbx
	je	.LBB5_320
# BB#304:                               # %.lr.ph29.i.i.us.preheader
                                        #   in Loop: Header=BB5_303 Depth=1
	testq	%r13, %r13
	movq	%rbx, %rcx
	jne	.LBB5_311
# BB#305:                               # %.lr.ph.i.i307.us.preheader.prol
                                        #   in Loop: Header=BB5_303 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movq	$-8, %rcx
	.p2align	4, 0x90
.LBB5_306:                              # %.lr.ph.i.i307.us.prol
                                        #   Parent Loop BB5_303 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r12,%rcx), %xmm1      # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_309
# BB#307:                               #   in Loop: Header=BB5_306 Depth=2
	movsd	%xmm1, -8(%r12,%rcx)
	addq	$8, %rcx
	jne	.LBB5_306
# BB#308:                               #   in Loop: Header=BB5_303 Depth=1
	movq	%r12, %rcx
	jmp	.LBB5_310
.LBB5_309:                              # %.lr.ph.i.i307.us.prol..critedge.i.i309.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_303 Depth=1
	addq	%r12, %rcx
.LBB5_310:                              # %.critedge.i.i309.us.prol
                                        #   in Loop: Header=BB5_303 Depth=1
	movsd	%xmm0, -8(%rcx)
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB5_311:                              # %.lr.ph29.i.i.us.prol.loopexit
                                        #   in Loop: Header=BB5_303 Depth=1
	testq	%rbp, %rbp
	je	.LBB5_320
	.p2align	4, 0x90
.LBB5_312:                              # %.lr.ph29.i.i.us
                                        #   Parent Loop BB5_303 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_314 Depth 3
                                        #       Child Loop BB5_318 Depth 3
	leaq	-8(%rcx), %rdx
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	cmpq	%r12, %rcx
	movq	%r12, %rsi
	je	.LBB5_317
# BB#313:                               # %.lr.ph.i.i307.us.preheader
                                        #   in Loop: Header=BB5_312 Depth=2
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_314:                              # %.lr.ph.i.i307.us
                                        #   Parent Loop BB5_303 Depth=1
                                        #     Parent Loop BB5_312 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_317
# BB#315:                               #   in Loop: Header=BB5_314 Depth=3
	movsd	%xmm1, -8(%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %r12
	jne	.LBB5_314
# BB#316:                               #   in Loop: Header=BB5_312 Depth=2
	movq	%r12, %rsi
.LBB5_317:                              # %.critedge.i.i309.us
                                        #   in Loop: Header=BB5_312 Depth=2
	movsd	%xmm0, -8(%rsi)
	movsd	-16(%rcx), %xmm0        # xmm0 = mem[0],zero
	addq	$-16, %rcx
	cmpq	%r12, %rdx
	movq	%r12, %rsi
	je	.LBB5_835
	.p2align	4, 0x90
.LBB5_318:                              # %.lr.ph.i.i307.us.1
                                        #   Parent Loop BB5_303 Depth=1
                                        #     Parent Loop BB5_312 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_319
# BB#833:                               #   in Loop: Header=BB5_318 Depth=3
	movsd	%xmm1, -8(%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %r12
	jne	.LBB5_318
# BB#834:                               #   in Loop: Header=BB5_312 Depth=2
	movq	%r12, %rsi
	jmp	.LBB5_835
	.p2align	4, 0x90
.LBB5_319:                              #   in Loop: Header=BB5_312 Depth=2
	movq	%rdx, %rsi
.LBB5_835:                              # %.critedge.i.i309.us.1
                                        #   in Loop: Header=BB5_312 Depth=2
	movsd	%xmm0, -8(%rsi)
	cmpq	%r15, %rcx
	jne	.LBB5_312
.LBB5_320:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_.exit.i.us.preheader
                                        #   in Loop: Header=BB5_303 Depth=1
	movq	%rbx, %rcx
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB5_321:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_.exit.i.us
                                        #   Parent Loop BB5_303 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r15
	je	.LBB5_324
# BB#322:                               #   in Loop: Header=BB5_321 Depth=2
	movsd	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rdx
	ucomisd	-8(%rcx), %xmm0
	leaq	-8(%rcx), %rcx
	jbe	.LBB5_321
# BB#323:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i313.us
                                        #   in Loop: Header=BB5_303 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rsp), %r10            # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB5_324:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i314.us
                                        #   in Loop: Header=BB5_303 Depth=1
	incl	%r14d
	cmpl	%r8d, %r14d
	jl	.LBB5_303
.LBB5_351:                              # %.us-lcssa.us
	testl	%r8d, %r8d
	jle	.LBB5_522
# BB#352:                               # %.lr.ph.i287.preheader
	movq	rrdMpb+8(%rip), %rax
	movq	rrdMpe+8(%rip), %rbp
	movq	rrdpb+8(%rip), %rbx
	movq	rrdpe+8(%rip), %r14
	leaq	8(%rbx), %r9
	cmpq	%rbp, %rax
	movq	%r9, 32(%rsp)           # 8-byte Spill
	je	.LBB5_365
# BB#353:                               # %.lr.ph.i287.preheader957
	leaq	-8(%rbp), %r13
	subq	%rax, %r13
	movl	%r13d, %ecx
	shrl	$3, %ecx
	incl	%ecx
	movq	%r14, %rdx
	subq	%rbx, %rdx
	addq	$-16, %rdx
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$1, %edx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	negq	%rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_354:                              # %.lr.ph.i287
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_356 Depth 2
                                        #     Child Loop BB5_358 Depth 2
                                        #     Child Loop BB5_362 Depth 2
                                        #     Child Loop BB5_394 Depth 2
                                        #       Child Loop BB5_396 Depth 3
                                        #       Child Loop BB5_401 Depth 3
                                        #     Child Loop BB5_406 Depth 2
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	movq	%rbx, %rcx
	movq	%rax, %rdx
	je	.LBB5_357
# BB#355:                               # %.lr.ph.i3.i290.prol.preheader
                                        #   in Loop: Header=BB5_354 Depth=1
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	%rbx, %rcx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB5_356:                              # %.lr.ph.i3.i290.prol
                                        #   Parent Loop BB5_354 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdi
	addq	$8, %rdx
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB5_356
.LBB5_357:                              # %.lr.ph.i3.i290.prol.loopexit
                                        #   in Loop: Header=BB5_354 Depth=1
	cmpq	$56, %r13
	jb	.LBB5_359
	.p2align	4, 0x90
.LBB5_358:                              # %.lr.ph.i3.i290
                                        #   Parent Loop BB5_354 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	movq	8(%rdx), %rsi
	movq	%rsi, 8(%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
	movq	24(%rdx), %rsi
	movq	%rsi, 24(%rcx)
	movq	32(%rdx), %rsi
	movq	%rsi, 32(%rcx)
	movq	40(%rdx), %rsi
	movq	%rsi, 40(%rcx)
	movq	48(%rdx), %rsi
	movq	%rsi, 48(%rcx)
	movq	56(%rdx), %rsi
	movq	%rsi, 56(%rcx)
	addq	$64, %rcx
	addq	$64, %rdx
	cmpq	%rbp, %rdx
	jne	.LBB5_358
.LBB5_359:                              # %_ZN9benchmark4copyISt16reverse_iteratorIS1_IPdEES4_EEvT_S5_T0_.exit.i291
                                        #   in Loop: Header=BB5_354 Depth=1
	cmpq	%r14, %r9
	je	.LBB5_405
# BB#360:                               # %.lr.ph35.i.i292.preheader
                                        #   in Loop: Header=BB5_354 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	movq	%r9, %rcx
	jne	.LBB5_393
# BB#361:                               # %.lr.ph.i.i295.preheader.prol
                                        #   in Loop: Header=BB5_354 Depth=1
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB5_362:                              # %.lr.ph.i.i295.prol
                                        #   Parent Loop BB5_354 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_391
# BB#363:                               #   in Loop: Header=BB5_362 Depth=2
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	leaq	(%rbx,%rcx), %rdx
	addq	$-8, %rcx
	cmpq	$-8, %rcx
	jne	.LBB5_362
# BB#364:                               #   in Loop: Header=BB5_354 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB5_392
.LBB5_391:                              # %.lr.ph.i.i295.prol..critedge.i.i297.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_354 Depth=1
	leaq	8(%rbx,%rcx), %rcx
.LBB5_392:                              # %.critedge.i.i297.prol
                                        #   in Loop: Header=BB5_354 Depth=1
	movsd	%xmm0, (%rcx)
	movq	128(%rsp), %rcx         # 8-byte Reload
.LBB5_393:                              # %.lr.ph35.i.i292.prol.loopexit
                                        #   in Loop: Header=BB5_354 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB5_405
	.p2align	4, 0x90
.LBB5_394:                              # %.lr.ph35.i.i292
                                        #   Parent Loop BB5_354 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_396 Depth 3
                                        #       Child Loop BB5_401 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	je	.LBB5_398
# BB#395:                               # %.lr.ph.i.i295.preheader
                                        #   in Loop: Header=BB5_394 Depth=2
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_396:                              # %.lr.ph.i.i295
                                        #   Parent Loop BB5_354 Depth=1
                                        #     Parent Loop BB5_394 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_399
# BB#397:                               #   in Loop: Header=BB5_396 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB5_396
.LBB5_398:                              #   in Loop: Header=BB5_394 Depth=2
	movq	%rbx, %rdx
.LBB5_399:                              # %.critedge.i.i297
                                        #   in Loop: Header=BB5_394 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%rbx, %rdx
	je	.LBB5_403
# BB#400:                               # %.lr.ph.i.i295.preheader.1
                                        #   in Loop: Header=BB5_394 Depth=2
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB5_401:                              # %.lr.ph.i.i295.1
                                        #   Parent Loop BB5_354 Depth=1
                                        #     Parent Loop BB5_394 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_404
# BB#402:                               #   in Loop: Header=BB5_401 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB5_401
.LBB5_403:                              #   in Loop: Header=BB5_394 Depth=2
	movq	%rbx, %rdx
.LBB5_404:                              # %.critedge.i.i297.1
                                        #   in Loop: Header=BB5_394 Depth=2
	movsd	%xmm0, (%rdx)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB5_394
.LBB5_405:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_.exit.i.preheader
                                        #   in Loop: Header=BB5_354 Depth=1
	movq	%r9, %rcx
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB5_406:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_.exit.i
                                        #   Parent Loop BB5_354 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB5_409
# BB#407:                               #   in Loop: Header=BB5_406 Depth=2
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rdx
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB5_406
# BB#408:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IPdEEEEbT_S5_.exit.i.i302
                                        #   in Loop: Header=BB5_354 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB5_409:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IPdEEEvT_S4_.exit.i303
                                        #   in Loop: Header=BB5_354 Depth=1
	movl	8(%rsp), %ecx           # 4-byte Reload
	incl	%ecx
	cmpl	%r8d, %ecx
	movq	(%rsp), %r10            # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jl	.LBB5_354
	jmp	.LBB5_410
.LBB5_365:                              # %.lr.ph.i287.us.preheader
	movq	%r14, %rbp
	subq	%rbx, %rbp
	addq	$-16, %rbp
	shrq	$3, %rbp
	movl	%ebp, %eax
	andl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	16(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_366:                              # %.lr.ph.i287.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_369 Depth 2
                                        #     Child Loop BB5_375 Depth 2
                                        #       Child Loop BB5_377 Depth 3
                                        #       Child Loop BB5_382 Depth 3
                                        #     Child Loop BB5_387 Depth 2
	cmpq	%r14, %r9
	je	.LBB5_386
# BB#367:                               # %.lr.ph35.i.i292.us.preheader
                                        #   in Loop: Header=BB5_366 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r9, %rcx
	jne	.LBB5_374
# BB#368:                               # %.lr.ph.i.i295.us.preheader.prol
                                        #   in Loop: Header=BB5_366 Depth=1
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB5_369:                              # %.lr.ph.i.i295.us.prol
                                        #   Parent Loop BB5_366 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_372
# BB#370:                               #   in Loop: Header=BB5_369 Depth=2
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	leaq	(%rbx,%rcx), %rdx
	addq	$-8, %rcx
	cmpq	$-8, %rcx
	jne	.LBB5_369
# BB#371:                               #   in Loop: Header=BB5_366 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB5_373
.LBB5_372:                              # %.lr.ph.i.i295.us.prol..critedge.i.i297.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_366 Depth=1
	leaq	8(%rbx,%rcx), %rcx
.LBB5_373:                              # %.critedge.i.i297.us.prol
                                        #   in Loop: Header=BB5_366 Depth=1
	movsd	%xmm0, (%rcx)
	movq	24(%rsp), %rcx          # 8-byte Reload
.LBB5_374:                              # %.lr.ph35.i.i292.us.prol.loopexit
                                        #   in Loop: Header=BB5_366 Depth=1
	testq	%rbp, %rbp
	je	.LBB5_386
	.p2align	4, 0x90
.LBB5_375:                              # %.lr.ph35.i.i292.us
                                        #   Parent Loop BB5_366 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_377 Depth 3
                                        #       Child Loop BB5_382 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	je	.LBB5_379
# BB#376:                               # %.lr.ph.i.i295.us.preheader
                                        #   in Loop: Header=BB5_375 Depth=2
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_377:                              # %.lr.ph.i.i295.us
                                        #   Parent Loop BB5_366 Depth=1
                                        #     Parent Loop BB5_375 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_380
# BB#378:                               #   in Loop: Header=BB5_377 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB5_377
.LBB5_379:                              #   in Loop: Header=BB5_375 Depth=2
	movq	%rbx, %rdx
.LBB5_380:                              # %.critedge.i.i297.us
                                        #   in Loop: Header=BB5_375 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%rbx, %rdx
	je	.LBB5_384
# BB#381:                               # %.lr.ph.i.i295.us.preheader.1
                                        #   in Loop: Header=BB5_375 Depth=2
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB5_382:                              # %.lr.ph.i.i295.us.1
                                        #   Parent Loop BB5_366 Depth=1
                                        #     Parent Loop BB5_375 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_385
# BB#383:                               #   in Loop: Header=BB5_382 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB5_382
.LBB5_384:                              #   in Loop: Header=BB5_375 Depth=2
	movq	%rbx, %rdx
.LBB5_385:                              # %.critedge.i.i297.us.1
                                        #   in Loop: Header=BB5_375 Depth=2
	movsd	%xmm0, (%rdx)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB5_375
.LBB5_386:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_.exit.i.us.preheader
                                        #   in Loop: Header=BB5_366 Depth=1
	movq	%r9, %rcx
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB5_387:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_.exit.i.us
                                        #   Parent Loop BB5_366 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB5_390
# BB#388:                               #   in Loop: Header=BB5_387 Depth=2
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rdx
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB5_387
# BB#389:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IPdEEEEbT_S5_.exit.i.i302.us
                                        #   in Loop: Header=BB5_366 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rsp), %r10            # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB5_390:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IPdEEEvT_S4_.exit.i303.us
                                        #   in Loop: Header=BB5_366 Depth=1
	incl	%r13d
	cmpl	%r8d, %r13d
	jl	.LBB5_366
.LBB5_410:                              # %_Z19test_insertion_sortISt16reverse_iteratorIS0_IPdEEdEvT_S4_S4_S4_T0_PKc.exit
	testl	%r8d, %r8d
	jle	.LBB5_522
# BB#411:                               # %.lr.ph.i267.preheader
	leaq	8(%r15), %rbx
	cmpq	%r10, %rdi
	je	.LBB5_415
# BB#412:                               # %.lr.ph.i267.preheader955
	leaq	-8(%rdi), %r13
	subq	%r10, %r13
	movl	%r13d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	leaq	-16(%r12), %rax
	subq	%r15, %rax
	shrq	$3, %rax
	andl	$7, %ebp
	movq	%rax, 8(%rsp)           # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	16(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %rax
	negq	%rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_413:                              # %.lr.ph.i267
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_440 Depth 2
                                        #     Child Loop BB5_442 Depth 2
                                        #     Child Loop BB5_446 Depth 2
                                        #     Child Loop BB5_452 Depth 2
                                        #       Child Loop BB5_454 Depth 3
                                        #       Child Loop BB5_459 Depth 3
                                        #     Child Loop BB5_462 Depth 2
	testq	%rbp, %rbp
	je	.LBB5_414
# BB#439:                               # %.lr.ph.i3.i270.prol.preheader
                                        #   in Loop: Header=BB5_413 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rcx
	movq	%r10, %rdx
	.p2align	4, 0x90
.LBB5_440:                              # %.lr.ph.i3.i270.prol
                                        #   Parent Loop BB5_413 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdi
	addq	$8, %rdx
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB5_440
	jmp	.LBB5_441
	.p2align	4, 0x90
.LBB5_414:                              #   in Loop: Header=BB5_413 Depth=1
	movq	%r15, %rcx
	movq	%r10, %rdx
.LBB5_441:                              # %.lr.ph.i3.i270.prol.loopexit
                                        #   in Loop: Header=BB5_413 Depth=1
	cmpq	$56, %r13
	movq	16(%rsp), %rdi          # 8-byte Reload
	jb	.LBB5_443
	.p2align	4, 0x90
.LBB5_442:                              # %.lr.ph.i3.i270
                                        #   Parent Loop BB5_413 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	movq	8(%rdx), %rsi
	movq	%rsi, 8(%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
	movq	24(%rdx), %rsi
	movq	%rsi, 24(%rcx)
	movq	32(%rdx), %rsi
	movq	%rsi, 32(%rcx)
	movq	40(%rdx), %rsi
	movq	%rsi, 40(%rcx)
	movq	48(%rdx), %rsi
	movq	%rsi, 48(%rcx)
	movq	56(%rdx), %rsi
	movq	%rsi, 56(%rcx)
	addq	$64, %rcx
	addq	$64, %rdx
	cmpq	%rdi, %rdx
	jne	.LBB5_442
.LBB5_443:                              # %_ZN9benchmark4copyISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEESA_EEvT_SB_T0_.exit.i271
                                        #   in Loop: Header=BB5_413 Depth=1
	cmpq	%r12, %rbx
	je	.LBB5_461
# BB#444:                               # %.lr.ph35.i.i272.preheader
                                        #   in Loop: Header=BB5_413 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	%rbx, %rcx
	jne	.LBB5_451
# BB#445:                               # %.lr.ph.i.i275.preheader.prol
                                        #   in Loop: Header=BB5_413 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB5_446:                              # %.lr.ph.i.i275.prol
                                        #   Parent Loop BB5_413 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_449
# BB#447:                               #   in Loop: Header=BB5_446 Depth=2
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	leaq	(%r15,%rcx), %rdx
	addq	$-8, %rcx
	cmpq	$-8, %rcx
	jne	.LBB5_446
# BB#448:                               #   in Loop: Header=BB5_413 Depth=1
	movq	%r15, %rcx
	jmp	.LBB5_450
.LBB5_449:                              # %.lr.ph.i.i275.prol..critedge.i.i277.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_413 Depth=1
	leaq	8(%r15,%rcx), %rcx
.LBB5_450:                              # %.critedge.i.i277.prol
                                        #   in Loop: Header=BB5_413 Depth=1
	movsd	%xmm0, (%rcx)
	movq	32(%rsp), %rcx          # 8-byte Reload
.LBB5_451:                              # %.lr.ph35.i.i272.prol.loopexit
                                        #   in Loop: Header=BB5_413 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB5_461
	.p2align	4, 0x90
.LBB5_452:                              # %.lr.ph35.i.i272
                                        #   Parent Loop BB5_413 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_454 Depth 3
                                        #       Child Loop BB5_459 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%r15, %rcx
	movq	%r15, %rdx
	je	.LBB5_457
# BB#453:                               # %.lr.ph.i.i275.preheader
                                        #   in Loop: Header=BB5_452 Depth=2
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_454:                              # %.lr.ph.i.i275
                                        #   Parent Loop BB5_413 Depth=1
                                        #     Parent Loop BB5_452 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_457
# BB#455:                               #   in Loop: Header=BB5_454 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %r15
	jne	.LBB5_454
# BB#456:                               #   in Loop: Header=BB5_452 Depth=2
	movq	%r15, %rdx
.LBB5_457:                              # %.critedge.i.i277
                                        #   in Loop: Header=BB5_452 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%r15, %rdx
	movq	%r15, %rsi
	je	.LBB5_832
# BB#458:                               # %.lr.ph.i.i275.preheader.1
                                        #   in Loop: Header=BB5_452 Depth=2
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB5_459:                              # %.lr.ph.i.i275.1
                                        #   Parent Loop BB5_413 Depth=1
                                        #     Parent Loop BB5_452 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_460
# BB#830:                               #   in Loop: Header=BB5_459 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %r15
	jne	.LBB5_459
# BB#831:                               #   in Loop: Header=BB5_452 Depth=2
	movq	%r15, %rsi
	jmp	.LBB5_832
	.p2align	4, 0x90
.LBB5_460:                              #   in Loop: Header=BB5_452 Depth=2
	movq	%rdx, %rsi
.LBB5_832:                              # %.critedge.i.i277.1
                                        #   in Loop: Header=BB5_452 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r12, %rcx
	jne	.LBB5_452
.LBB5_461:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_.exit.i280.preheader
                                        #   in Loop: Header=BB5_413 Depth=1
	movq	%rbx, %rcx
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB5_462:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_.exit.i280
                                        #   Parent Loop BB5_413 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r12
	je	.LBB5_465
# BB#463:                               #   in Loop: Header=BB5_462 Depth=2
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rdx
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB5_462
# BB#464:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i283
                                        #   in Loop: Header=BB5_413 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB5_465:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i284
                                        #   in Loop: Header=BB5_413 Depth=1
	incl	%r14d
	cmpl	%r8d, %r14d
	movq	(%rsp), %r10            # 8-byte Reload
	jl	.LBB5_413
	jmp	.LBB5_466
.LBB5_415:                              # %.lr.ph.i267.us.preheader
	leaq	-16(%r12), %rbp
	subq	%r15, %rbp
	shrq	$3, %rbp
	movl	%ebp, %r14d
	andl	$1, %r14d
	leaq	16(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_416:                              # %.lr.ph.i267.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_419 Depth 2
                                        #     Child Loop BB5_425 Depth 2
                                        #       Child Loop BB5_427 Depth 3
                                        #       Child Loop BB5_432 Depth 3
                                        #     Child Loop BB5_435 Depth 2
	cmpq	%r12, %rbx
	je	.LBB5_434
# BB#417:                               # %.lr.ph35.i.i272.us.preheader
                                        #   in Loop: Header=BB5_416 Depth=1
	testq	%r14, %r14
	movq	%rbx, %rcx
	jne	.LBB5_424
# BB#418:                               # %.lr.ph.i.i275.us.preheader.prol
                                        #   in Loop: Header=BB5_416 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB5_419:                              # %.lr.ph.i.i275.us.prol
                                        #   Parent Loop BB5_416 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_422
# BB#420:                               #   in Loop: Header=BB5_419 Depth=2
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	leaq	(%r15,%rcx), %rdx
	addq	$-8, %rcx
	cmpq	$-8, %rcx
	jne	.LBB5_419
# BB#421:                               #   in Loop: Header=BB5_416 Depth=1
	movq	%r15, %rcx
	jmp	.LBB5_423
.LBB5_422:                              # %.lr.ph.i.i275.us.prol..critedge.i.i277.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_416 Depth=1
	leaq	8(%r15,%rcx), %rcx
.LBB5_423:                              # %.critedge.i.i277.us.prol
                                        #   in Loop: Header=BB5_416 Depth=1
	movsd	%xmm0, (%rcx)
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB5_424:                              # %.lr.ph35.i.i272.us.prol.loopexit
                                        #   in Loop: Header=BB5_416 Depth=1
	testq	%rbp, %rbp
	je	.LBB5_434
	.p2align	4, 0x90
.LBB5_425:                              # %.lr.ph35.i.i272.us
                                        #   Parent Loop BB5_416 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_427 Depth 3
                                        #       Child Loop BB5_432 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%r15, %rcx
	movq	%r15, %rdx
	je	.LBB5_430
# BB#426:                               # %.lr.ph.i.i275.us.preheader
                                        #   in Loop: Header=BB5_425 Depth=2
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_427:                              # %.lr.ph.i.i275.us
                                        #   Parent Loop BB5_416 Depth=1
                                        #     Parent Loop BB5_425 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_430
# BB#428:                               #   in Loop: Header=BB5_427 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %r15
	jne	.LBB5_427
# BB#429:                               #   in Loop: Header=BB5_425 Depth=2
	movq	%r15, %rdx
.LBB5_430:                              # %.critedge.i.i277.us
                                        #   in Loop: Header=BB5_425 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%r15, %rdx
	movq	%r15, %rsi
	je	.LBB5_829
# BB#431:                               # %.lr.ph.i.i275.us.preheader.1
                                        #   in Loop: Header=BB5_425 Depth=2
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB5_432:                              # %.lr.ph.i.i275.us.1
                                        #   Parent Loop BB5_416 Depth=1
                                        #     Parent Loop BB5_425 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_433
# BB#827:                               #   in Loop: Header=BB5_432 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %r15
	jne	.LBB5_432
# BB#828:                               #   in Loop: Header=BB5_425 Depth=2
	movq	%r15, %rsi
	jmp	.LBB5_829
	.p2align	4, 0x90
.LBB5_433:                              #   in Loop: Header=BB5_425 Depth=2
	movq	%rdx, %rsi
.LBB5_829:                              # %.critedge.i.i277.us.1
                                        #   in Loop: Header=BB5_425 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r12, %rcx
	jne	.LBB5_425
.LBB5_434:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_.exit.i280.us.preheader
                                        #   in Loop: Header=BB5_416 Depth=1
	movq	%rbx, %rcx
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB5_435:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_.exit.i280.us
                                        #   Parent Loop BB5_416 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r12
	je	.LBB5_438
# BB#436:                               #   in Loop: Header=BB5_435 Depth=2
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rdx
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB5_435
# BB#437:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i283.us
                                        #   in Loop: Header=BB5_416 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rsp), %r10            # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB5_438:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i284.us
                                        #   in Loop: Header=BB5_416 Depth=1
	incl	%r13d
	cmpl	%r8d, %r13d
	jl	.LBB5_416
.LBB5_466:                              # %_Z19test_insertion_sortISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEvT_SA_SA_SA_T0_PKc.exit285
	testl	%r8d, %r8d
	jle	.LBB5_522
# BB#467:                               # %.lr.ph.i258.preheader
	cmpq	%r10, %rdi
	je	.LBB5_471
# BB#468:                               # %.lr.ph.i258.preheader953
	leaq	-8(%rdi), %r13
	subq	%r10, %r13
	movl	%r13d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	leaq	-16(%r12), %rax
	subq	%r15, %rax
	shrq	$3, %rax
	andl	$7, %ebp
	movq	%rax, 8(%rsp)           # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	16(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %rax
	negq	%rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_469:                              # %.lr.ph.i258
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_496 Depth 2
                                        #     Child Loop BB5_498 Depth 2
                                        #     Child Loop BB5_502 Depth 2
                                        #     Child Loop BB5_508 Depth 2
                                        #       Child Loop BB5_510 Depth 3
                                        #       Child Loop BB5_515 Depth 3
                                        #     Child Loop BB5_518 Depth 2
	testq	%rbp, %rbp
	je	.LBB5_470
# BB#495:                               # %.lr.ph.i3.i.prol.preheader
                                        #   in Loop: Header=BB5_469 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rcx
	movq	%r10, %rdx
	.p2align	4, 0x90
.LBB5_496:                              # %.lr.ph.i3.i.prol
                                        #   Parent Loop BB5_469 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdi
	addq	$8, %rdx
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB5_496
	jmp	.LBB5_497
	.p2align	4, 0x90
.LBB5_470:                              #   in Loop: Header=BB5_469 Depth=1
	movq	%r15, %rcx
	movq	%r10, %rdx
.LBB5_497:                              # %.lr.ph.i3.i.prol.loopexit
                                        #   in Loop: Header=BB5_469 Depth=1
	cmpq	$56, %r13
	movq	16(%rsp), %rax          # 8-byte Reload
	jb	.LBB5_499
	.p2align	4, 0x90
.LBB5_498:                              # %.lr.ph.i3.i
                                        #   Parent Loop BB5_469 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	movq	8(%rdx), %rsi
	movq	%rsi, 8(%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
	movq	24(%rdx), %rsi
	movq	%rsi, 24(%rcx)
	movq	32(%rdx), %rsi
	movq	%rsi, 32(%rcx)
	movq	40(%rdx), %rsi
	movq	%rsi, 40(%rcx)
	movq	48(%rdx), %rsi
	movq	%rsi, 48(%rcx)
	movq	56(%rdx), %rsi
	movq	%rsi, 56(%rcx)
	addq	$64, %rcx
	addq	$64, %rdx
	cmpq	%rax, %rdx
	jne	.LBB5_498
.LBB5_499:                              # %_ZN9benchmark4copyISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEESA_EEvT_SB_T0_.exit.i259
                                        #   in Loop: Header=BB5_469 Depth=1
	cmpq	%r12, %rbx
	je	.LBB5_517
# BB#500:                               # %.lr.ph35.i.i.preheader
                                        #   in Loop: Header=BB5_469 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	%rbx, %rcx
	jne	.LBB5_507
# BB#501:                               # %.lr.ph.i.i260.preheader.prol
                                        #   in Loop: Header=BB5_469 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB5_502:                              # %.lr.ph.i.i260.prol
                                        #   Parent Loop BB5_469 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_505
# BB#503:                               #   in Loop: Header=BB5_502 Depth=2
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	leaq	(%r15,%rcx), %rdx
	addq	$-8, %rcx
	cmpq	$-8, %rcx
	jne	.LBB5_502
# BB#504:                               #   in Loop: Header=BB5_469 Depth=1
	movq	%r15, %rcx
	jmp	.LBB5_506
.LBB5_505:                              # %.lr.ph.i.i260.prol..critedge.i.i.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_469 Depth=1
	leaq	8(%r15,%rcx), %rcx
.LBB5_506:                              # %.critedge.i.i.prol
                                        #   in Loop: Header=BB5_469 Depth=1
	movsd	%xmm0, (%rcx)
	movq	32(%rsp), %rcx          # 8-byte Reload
.LBB5_507:                              # %.lr.ph35.i.i.prol.loopexit
                                        #   in Loop: Header=BB5_469 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB5_517
	.p2align	4, 0x90
.LBB5_508:                              # %.lr.ph35.i.i
                                        #   Parent Loop BB5_469 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_510 Depth 3
                                        #       Child Loop BB5_515 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%r15, %rcx
	movq	%r15, %rdx
	je	.LBB5_513
# BB#509:                               # %.lr.ph.i.i260.preheader
                                        #   in Loop: Header=BB5_508 Depth=2
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_510:                              # %.lr.ph.i.i260
                                        #   Parent Loop BB5_469 Depth=1
                                        #     Parent Loop BB5_508 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_513
# BB#511:                               #   in Loop: Header=BB5_510 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %r15
	jne	.LBB5_510
# BB#512:                               #   in Loop: Header=BB5_508 Depth=2
	movq	%r15, %rdx
.LBB5_513:                              # %.critedge.i.i
                                        #   in Loop: Header=BB5_508 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%r15, %rdx
	movq	%r15, %rsi
	je	.LBB5_826
# BB#514:                               # %.lr.ph.i.i260.preheader.1
                                        #   in Loop: Header=BB5_508 Depth=2
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB5_515:                              # %.lr.ph.i.i260.1
                                        #   Parent Loop BB5_469 Depth=1
                                        #     Parent Loop BB5_508 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_516
# BB#824:                               #   in Loop: Header=BB5_515 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %r15
	jne	.LBB5_515
# BB#825:                               #   in Loop: Header=BB5_508 Depth=2
	movq	%r15, %rsi
	jmp	.LBB5_826
	.p2align	4, 0x90
.LBB5_516:                              #   in Loop: Header=BB5_508 Depth=2
	movq	%rdx, %rsi
.LBB5_826:                              # %.critedge.i.i.1
                                        #   in Loop: Header=BB5_508 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r12, %rcx
	jne	.LBB5_508
.LBB5_517:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_.exit.i.preheader
                                        #   in Loop: Header=BB5_469 Depth=1
	movq	%rbx, %rcx
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB5_518:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_.exit.i
                                        #   Parent Loop BB5_469 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r12
	je	.LBB5_521
# BB#519:                               #   in Loop: Header=BB5_518 Depth=2
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rdx
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB5_518
# BB#520:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i265
                                        #   in Loop: Header=BB5_469 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %r8d
.LBB5_521:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i266
                                        #   in Loop: Header=BB5_469 Depth=1
	incl	%r14d
	cmpl	%r8d, %r14d
	movq	(%rsp), %r10            # 8-byte Reload
	jl	.LBB5_469
	jmp	.LBB5_522
.LBB5_471:                              # %.lr.ph.i258.us.preheader
	leaq	-16(%r12), %rbp
	subq	%r15, %rbp
	shrq	$3, %rbp
	movl	%ebp, %r14d
	andl	$1, %r14d
	leaq	16(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_472:                              # %.lr.ph.i258.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_475 Depth 2
                                        #     Child Loop BB5_481 Depth 2
                                        #       Child Loop BB5_483 Depth 3
                                        #       Child Loop BB5_488 Depth 3
                                        #     Child Loop BB5_491 Depth 2
	cmpq	%r12, %rbx
	je	.LBB5_490
# BB#473:                               # %.lr.ph35.i.i.us.preheader
                                        #   in Loop: Header=BB5_472 Depth=1
	testq	%r14, %r14
	movq	%rbx, %rcx
	jne	.LBB5_480
# BB#474:                               # %.lr.ph.i.i260.us.preheader.prol
                                        #   in Loop: Header=BB5_472 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB5_475:                              # %.lr.ph.i.i260.us.prol
                                        #   Parent Loop BB5_472 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_478
# BB#476:                               #   in Loop: Header=BB5_475 Depth=2
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	leaq	(%r15,%rcx), %rdx
	addq	$-8, %rcx
	cmpq	$-8, %rcx
	jne	.LBB5_475
# BB#477:                               #   in Loop: Header=BB5_472 Depth=1
	movq	%r15, %rcx
	jmp	.LBB5_479
.LBB5_478:                              # %.lr.ph.i.i260.us.prol..critedge.i.i.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB5_472 Depth=1
	leaq	8(%r15,%rcx), %rcx
.LBB5_479:                              # %.critedge.i.i.us.prol
                                        #   in Loop: Header=BB5_472 Depth=1
	movsd	%xmm0, (%rcx)
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB5_480:                              # %.lr.ph35.i.i.us.prol.loopexit
                                        #   in Loop: Header=BB5_472 Depth=1
	testq	%rbp, %rbp
	je	.LBB5_490
	.p2align	4, 0x90
.LBB5_481:                              # %.lr.ph35.i.i.us
                                        #   Parent Loop BB5_472 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_483 Depth 3
                                        #       Child Loop BB5_488 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%r15, %rcx
	movq	%r15, %rdx
	je	.LBB5_486
# BB#482:                               # %.lr.ph.i.i260.us.preheader
                                        #   in Loop: Header=BB5_481 Depth=2
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_483:                              # %.lr.ph.i.i260.us
                                        #   Parent Loop BB5_472 Depth=1
                                        #     Parent Loop BB5_481 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_486
# BB#484:                               #   in Loop: Header=BB5_483 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %r15
	jne	.LBB5_483
# BB#485:                               #   in Loop: Header=BB5_481 Depth=2
	movq	%r15, %rdx
.LBB5_486:                              # %.critedge.i.i.us
                                        #   in Loop: Header=BB5_481 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%r15, %rdx
	movq	%r15, %rsi
	je	.LBB5_823
# BB#487:                               # %.lr.ph.i.i260.us.preheader.1
                                        #   in Loop: Header=BB5_481 Depth=2
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB5_488:                              # %.lr.ph.i.i260.us.1
                                        #   Parent Loop BB5_472 Depth=1
                                        #     Parent Loop BB5_481 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_489
# BB#821:                               #   in Loop: Header=BB5_488 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %r15
	jne	.LBB5_488
# BB#822:                               #   in Loop: Header=BB5_481 Depth=2
	movq	%r15, %rsi
	jmp	.LBB5_823
	.p2align	4, 0x90
.LBB5_489:                              #   in Loop: Header=BB5_481 Depth=2
	movq	%rdx, %rsi
.LBB5_823:                              # %.critedge.i.i.us.1
                                        #   in Loop: Header=BB5_481 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r12, %rcx
	jne	.LBB5_481
.LBB5_490:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_.exit.i.us.preheader
                                        #   in Loop: Header=BB5_472 Depth=1
	movq	%rbx, %rcx
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB5_491:                              # %_ZN9benchmark13insertionSortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_.exit.i.us
                                        #   Parent Loop BB5_472 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r12
	je	.LBB5_494
# BB#492:                               #   in Loop: Header=BB5_491 Depth=2
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rdx
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB5_491
# BB#493:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i265.us
                                        #   in Loop: Header=BB5_472 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %r8d
.LBB5_494:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i266.us
                                        #   in Loop: Header=BB5_472 Depth=1
	incl	%r13d
	cmpl	%r8d, %r13d
	jl	.LBB5_472
.LBB5_522:                              # %_Z19test_insertion_sortISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEvT_SA_SA_SA_T0_PKc.exit
	shll	$3, %r8d
	movl	%r8d, iterations(%rip)
	movq	dMpb(%rip), %rdi
	movq	dMpe(%rip), %rsi
	movq	dpb(%rip), %rdx
	movq	dpe(%rip), %rcx
.Ltmp10:
	xorpd	%xmm0, %xmm0
	movl	$.L.str.34, %r8d
	callq	_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc
.Ltmp11:
# BB#523:
	movq	96(%rsp), %rdi
	movq	104(%rsp), %rsi
	movq	64(%rsp), %rdx
	movq	72(%rsp), %rcx
.Ltmp12:
	xorpd	%xmm0, %xmm0
	movl	$.L.str.35, %r8d
	callq	_Z14test_quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
.Ltmp13:
# BB#524:
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	jle	.LBB5_545
# BB#525:                               # %.lr.ph.i241
	movq	rdMpb(%rip), %rcx
	movq	rdMpe(%rip), %r14
	movq	rdpb(%rip), %rax
	movq	rdpe(%rip), %rbx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	%r14, %rcx
	je	.LBB5_538
# BB#526:                               # %.lr.ph.i241.split.preheader
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	-8(%rcx), %rax
	subq	%r14, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$3, %eax
	incl	%eax
	andl	$7, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	negq	%rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_527:                              # %.lr.ph.i241.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_529 Depth 2
                                        #     Child Loop BB5_531 Depth 2
                                        #     Child Loop BB5_534 Depth 2
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movq	%rbp, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, %rcx
	je	.LBB5_530
# BB#528:                               # %.lr.ph.i.i244.prol.preheader
                                        #   in Loop: Header=BB5_527 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rax
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB5_529:                              # %.lr.ph.i.i244.prol
                                        #   Parent Loop BB5_527 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rsi
	addq	$-8, %rcx
	movq	%rsi, -8(%rax)
	addq	$-8, %rax
	incq	%rdx
	jne	.LBB5_529
.LBB5_530:                              # %.lr.ph.i.i244.prol.loopexit
                                        #   in Loop: Header=BB5_527 Depth=1
	cmpq	$56, 32(%rsp)           # 8-byte Folded Reload
	jb	.LBB5_532
	.p2align	4, 0x90
.LBB5_531:                              # %.lr.ph.i.i244
                                        #   Parent Loop BB5_527 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rdx
	movq	%rdx, -8(%rax)
	movq	-16(%rcx), %rdx
	movq	%rdx, -16(%rax)
	movq	-24(%rcx), %rdx
	movq	%rdx, -24(%rax)
	movq	-32(%rcx), %rdx
	movq	%rdx, -32(%rax)
	movq	-40(%rcx), %rdx
	movq	%rdx, -40(%rax)
	movq	-48(%rcx), %rdx
	movq	%rdx, -48(%rax)
	movq	-56(%rcx), %rdx
	movq	%rdx, -56(%rax)
	movq	-64(%rcx), %rdx
	addq	$-64, %rcx
	movq	%rdx, -64(%rax)
	addq	$-64, %rax
	cmpq	%rcx, %r14
	jne	.LBB5_531
.LBB5_532:                              # %_ZN9benchmark4copyISt16reverse_iteratorIPdES3_EEvT_S4_T0_.exit.i246
                                        #   in Loop: Header=BB5_527 Depth=1
	movq	%rbp, 224(%rsp)
	movq	%rbx, 216(%rsp)
.Ltmp14:
	leaq	224(%rsp), %rdi
	leaq	216(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_
.Ltmp15:
# BB#533:                               # %.noexc251.preheader
                                        #   in Loop: Header=BB5_527 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_534:                              # %.noexc251
                                        #   Parent Loop BB5_527 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB5_537
# BB#535:                               #   in Loop: Header=BB5_534 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_534
# BB#536:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIPdEEEbT_S4_.exit.i.i249
                                        #   in Loop: Header=BB5_527 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_537:                              # %_Z13verify_sortedISt16reverse_iteratorIPdEEvT_S3_.exit.i250
                                        #   in Loop: Header=BB5_527 Depth=1
	incl	%r13d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r13d
	jl	.LBB5_527
	jmp	.LBB5_545
.LBB5_538:                              # %.lr.ph.i241.split.us.preheader
	leaq	216(%rsp), %r13
	xorl	%r14d, %r14d
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_539:                              # %.lr.ph.i241.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_541 Depth 2
	movq	%rbp, 224(%rsp)
	movq	%rbx, 216(%rsp)
.Ltmp17:
	leaq	224(%rsp), %rdi
	movq	%r13, %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_
.Ltmp18:
# BB#540:                               # %.noexc251.us.preheader
                                        #   in Loop: Header=BB5_539 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_541:                              # %.noexc251.us
                                        #   Parent Loop BB5_539 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB5_544
# BB#542:                               #   in Loop: Header=BB5_541 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_541
# BB#543:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIPdEEEbT_S4_.exit.i.i249.us
                                        #   in Loop: Header=BB5_539 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_544:                              # %_Z13verify_sortedISt16reverse_iteratorIPdEEvT_S3_.exit.i250.us
                                        #   in Loop: Header=BB5_539 Depth=1
	incl	%r14d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r14d
	jl	.LBB5_539
.LBB5_545:                              # %.loopexit604
	testl	%eax, %eax
	jle	.LBB5_559
# BB#546:                               # %.lr.ph.i220
	movq	96(%rsp), %r13
	movq	104(%rsp), %rcx
	movq	64(%rsp), %rbp
	movq	72(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	%r13, %rcx
	je	.LBB5_564
# BB#547:                               # %.lr.ph.i220.split.preheader
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	-8(%rcx), %rax
	subq	%r13, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$3, %eax
	incl	%eax
	andl	$7, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	negq	%rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_548:                              # %.lr.ph.i220.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_550 Depth 2
                                        #     Child Loop BB5_552 Depth 2
                                        #     Child Loop BB5_555 Depth 2
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, %rcx
	je	.LBB5_551
# BB#549:                               # %.lr.ph.i.i223.prol.preheader
                                        #   in Loop: Header=BB5_548 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %rax
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB5_550:                              # %.lr.ph.i.i223.prol
                                        #   Parent Loop BB5_548 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rsi
	addq	$-8, %rcx
	movq	%rsi, -8(%rax)
	addq	$-8, %rax
	incq	%rdx
	jne	.LBB5_550
.LBB5_551:                              # %.lr.ph.i.i223.prol.loopexit
                                        #   in Loop: Header=BB5_548 Depth=1
	cmpq	$56, 32(%rsp)           # 8-byte Folded Reload
	jb	.LBB5_553
	.p2align	4, 0x90
.LBB5_552:                              # %.lr.ph.i.i223
                                        #   Parent Loop BB5_548 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rdx
	movq	%rdx, -8(%rax)
	movq	-16(%rcx), %rdx
	movq	%rdx, -16(%rax)
	movq	-24(%rcx), %rdx
	movq	%rdx, -24(%rax)
	movq	-32(%rcx), %rdx
	movq	%rdx, -32(%rax)
	movq	-40(%rcx), %rdx
	movq	%rdx, -40(%rax)
	movq	-48(%rcx), %rdx
	movq	%rdx, -48(%rax)
	movq	-56(%rcx), %rdx
	movq	%rdx, -56(%rax)
	movq	-64(%rcx), %rdx
	addq	$-64, %rcx
	movq	%rdx, -64(%rax)
	addq	$-64, %rax
	cmpq	%rcx, %r13
	jne	.LBB5_552
.LBB5_553:                              # %_ZN9benchmark4copyISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEES9_EEvT_SA_T0_.exit.i225
                                        #   in Loop: Header=BB5_548 Depth=1
	movq	%rbx, 208(%rsp)
	movq	%rbp, 200(%rsp)
.Ltmp20:
	leaq	208(%rsp), %rdi
	leaq	200(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
.Ltmp21:
# BB#554:                               # %.noexc230.preheader
                                        #   in Loop: Header=BB5_548 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbx, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_555:                              # %.noexc230
                                        #   Parent Loop BB5_548 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbp
	je	.LBB5_558
# BB#556:                               #   in Loop: Header=BB5_555 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_555
# BB#557:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i228
                                        #   in Loop: Header=BB5_548 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_558:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i229
                                        #   in Loop: Header=BB5_548 Depth=1
	incl	%r14d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r14d
	jl	.LBB5_548
	jmp	.LBB5_559
.LBB5_564:                              # %.lr.ph.i220.split.us.preheader
	leaq	200(%rsp), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_565:                              # %.lr.ph.i220.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_567 Depth 2
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, 208(%rsp)
	movq	%rbp, 200(%rsp)
.Ltmp23:
	leaq	208(%rsp), %rdi
	movq	%r13, %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
.Ltmp24:
# BB#566:                               # %.noexc230.us.preheader
                                        #   in Loop: Header=BB5_565 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbx, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_567:                              # %.noexc230.us
                                        #   Parent Loop BB5_565 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbp
	je	.LBB5_570
# BB#568:                               #   in Loop: Header=BB5_567 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_567
# BB#569:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i228.us
                                        #   in Loop: Header=BB5_565 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_570:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i229.us
                                        #   in Loop: Header=BB5_565 Depth=1
	incl	%r14d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r14d
	jl	.LBB5_565
.LBB5_559:                              # %.loopexit600
	testl	%eax, %eax
	jle	.LBB5_590
# BB#560:                               # %.lr.ph.i202
	leaq	-8(%r12), %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	%rdi, %rax
	je	.LBB5_572
# BB#561:                               # %.lr.ph.i202.split.preheader
	leaq	-8(%rax), %r14
	subq	%rdi, %r14
	movl	%r14d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andl	$7, %ebx
	movq	%rbx, %rax
	negq	%rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_562:                              # %.lr.ph.i202.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_581 Depth 2
                                        #     Child Loop BB5_583 Depth 2
                                        #     Child Loop BB5_586 Depth 2
	testq	%rbx, %rbx
	je	.LBB5_563
# BB#580:                               # %.lr.ph.i.i205.prol.preheader
                                        #   in Loop: Header=BB5_562 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r12, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_581:                              # %.lr.ph.i.i205.prol
                                        #   Parent Loop BB5_562 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rsi
	addq	$-8, %rcx
	movq	%rsi, -8(%rax)
	addq	$-8, %rax
	incq	%rdx
	jne	.LBB5_581
	jmp	.LBB5_582
	.p2align	4, 0x90
.LBB5_563:                              #   in Loop: Header=BB5_562 Depth=1
	movq	%r12, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB5_582:                              # %.lr.ph.i.i205.prol.loopexit
                                        #   in Loop: Header=BB5_562 Depth=1
	cmpq	$56, %r14
	jb	.LBB5_584
	.p2align	4, 0x90
.LBB5_583:                              # %.lr.ph.i.i205
                                        #   Parent Loop BB5_562 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rdx
	movq	%rdx, -8(%rax)
	movq	-16(%rcx), %rdx
	movq	%rdx, -16(%rax)
	movq	-24(%rcx), %rdx
	movq	%rdx, -24(%rax)
	movq	-32(%rcx), %rdx
	movq	%rdx, -32(%rax)
	movq	-40(%rcx), %rdx
	movq	%rdx, -40(%rax)
	movq	-48(%rcx), %rdx
	movq	%rdx, -48(%rax)
	movq	-56(%rcx), %rdx
	movq	%rdx, -56(%rax)
	movq	-64(%rcx), %rdx
	addq	$-64, %rcx
	movq	%rdx, -64(%rax)
	addq	$-64, %rax
	cmpq	%rcx, %rdi
	jne	.LBB5_583
.LBB5_584:                              # %_ZN9benchmark4copyISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEES9_EEvT_SA_T0_.exit.i207
                                        #   in Loop: Header=BB5_562 Depth=1
	movq	%r12, 192(%rsp)
	movq	%r15, 184(%rsp)
.Ltmp26:
	leaq	192(%rsp), %rdi
	leaq	184(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
.Ltmp27:
# BB#585:                               # %.noexc212.preheader
                                        #   in Loop: Header=BB5_562 Depth=1
	movq	%rbp, %rax
	movq	%r12, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_586:                              # %.noexc212
                                        #   Parent Loop BB5_562 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r15
	je	.LBB5_589
# BB#587:                               #   in Loop: Header=BB5_586 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_586
# BB#588:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i210
                                        #   in Loop: Header=BB5_562 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_589:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i211
                                        #   in Loop: Header=BB5_562 Depth=1
	incl	%r13d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r13d
	jl	.LBB5_562
	jmp	.LBB5_590
.LBB5_572:                              # %.lr.ph.i202.split.us.preheader
	leaq	192(%rsp), %r14
	leaq	184(%rsp), %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_573:                              # %.lr.ph.i202.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_575 Depth 2
	movq	%r12, 192(%rsp)
	movq	%r15, 184(%rsp)
.Ltmp29:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
.Ltmp30:
# BB#574:                               # %.noexc212.us.preheader
                                        #   in Loop: Header=BB5_573 Depth=1
	movq	%rbp, %rax
	movq	%r12, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_575:                              # %.noexc212.us
                                        #   Parent Loop BB5_573 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r15
	je	.LBB5_578
# BB#576:                               #   in Loop: Header=BB5_575 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_575
# BB#577:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i210.us
                                        #   in Loop: Header=BB5_573 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_578:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i211.us
                                        #   in Loop: Header=BB5_573 Depth=1
	incl	%ebx
	movl	iterations(%rip), %eax
	cmpl	%eax, %ebx
	jl	.LBB5_573
.LBB5_590:                              # %.loopexit597
	testl	%eax, %eax
	jle	.LBB5_604
# BB#591:                               # %.lr.ph.i183
	movq	rrdMpb+8(%rip), %rax
	movq	rrdMpe+8(%rip), %r13
	movq	rrdpb+8(%rip), %rbp
	movq	rrdpe+8(%rip), %rbx
	cmpq	%r13, %rax
	je	.LBB5_609
# BB#592:                               # %.lr.ph.i183.split.preheader
	leaq	-8(%r13), %rcx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	subq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	shrl	$3, %ecx
	incl	%ecx
	andl	$7, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	negq	%rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	8(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_593:                              # %.lr.ph.i183.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_595 Depth 2
                                        #     Child Loop BB5_597 Depth 2
                                        #     Child Loop BB5_600 Depth 2
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	movq	%rbp, %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rsi, %rcx
	je	.LBB5_596
# BB#594:                               # %.lr.ph.i.i186.prol.preheader
                                        #   in Loop: Header=BB5_593 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rax
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB5_595:                              # %.lr.ph.i.i186.prol
                                        #   Parent Loop BB5_593 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rsi
	addq	$8, %rcx
	movq	%rsi, (%rax)
	addq	$8, %rax
	incq	%rdx
	jne	.LBB5_595
.LBB5_596:                              # %.lr.ph.i.i186.prol.loopexit
                                        #   in Loop: Header=BB5_593 Depth=1
	cmpq	$56, 24(%rsp)           # 8-byte Folded Reload
	jb	.LBB5_598
	.p2align	4, 0x90
.LBB5_597:                              # %.lr.ph.i.i186
                                        #   Parent Loop BB5_593 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%r13, %rcx
	jne	.LBB5_597
.LBB5_598:                              # %_ZN9benchmark4copyISt16reverse_iteratorIS1_IPdEES4_EEvT_S5_T0_.exit.i188
                                        #   in Loop: Header=BB5_593 Depth=1
	movq	%rbp, 424(%rsp)
	movq	%rbx, 408(%rsp)
.Ltmp32:
	leaq	416(%rsp), %rdi
	leaq	400(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
.Ltmp33:
# BB#599:                               # %.noexc194.preheader
                                        #   in Loop: Header=BB5_593 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rbp, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_600:                              # %.noexc194
                                        #   Parent Loop BB5_593 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB5_603
# BB#601:                               #   in Loop: Header=BB5_600 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_600
# BB#602:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IPdEEEEbT_S5_.exit.i.i192
                                        #   in Loop: Header=BB5_593 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_603:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IPdEEEvT_S4_.exit.i193
                                        #   in Loop: Header=BB5_593 Depth=1
	incl	%r14d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r14d
	jl	.LBB5_593
	jmp	.LBB5_604
.LBB5_609:                              # %.lr.ph.i183.split.us.preheader
	leaq	8(%rbp), %r14
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_610:                              # %.lr.ph.i183.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_612 Depth 2
	movq	%rbp, 424(%rsp)
	movq	%rbx, 408(%rsp)
.Ltmp35:
	leaq	416(%rsp), %rdi
	leaq	400(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
.Ltmp36:
# BB#611:                               # %.noexc194.us.preheader
                                        #   in Loop: Header=BB5_610 Depth=1
	movq	%r14, %rax
	movq	%rbp, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_612:                              # %.noexc194.us
                                        #   Parent Loop BB5_610 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB5_615
# BB#613:                               #   in Loop: Header=BB5_612 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_612
# BB#614:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IPdEEEEbT_S5_.exit.i.i192.us
                                        #   in Loop: Header=BB5_610 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_615:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IPdEEEvT_S4_.exit.i193.us
                                        #   in Loop: Header=BB5_610 Depth=1
	incl	%r13d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r13d
	jl	.LBB5_610
.LBB5_604:                              # %.loopexit593
	testl	%eax, %eax
	jle	.LBB5_635
# BB#605:                               # %.lr.ph.i161
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, %rdi
	je	.LBB5_617
# BB#606:                               # %.lr.ph.i161.split.preheader
	leaq	-8(%rax), %rbx
	subq	%rdi, %rbx
	movl	%ebx, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andl	$7, %ebp
	movq	%rbp, %rax
	negq	%rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	8(%r15), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_607:                              # %.lr.ph.i161.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_626 Depth 2
                                        #     Child Loop BB5_628 Depth 2
                                        #     Child Loop BB5_631 Depth 2
	testq	%rbp, %rbp
	je	.LBB5_608
# BB#625:                               # %.lr.ph.i.i164.prol.preheader
                                        #   in Loop: Header=BB5_607 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r15, %rax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB5_626:                              # %.lr.ph.i.i164.prol
                                        #   Parent Loop BB5_607 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rsi
	addq	$8, %rcx
	movq	%rsi, (%rax)
	addq	$8, %rax
	incq	%rdx
	jne	.LBB5_626
	jmp	.LBB5_627
	.p2align	4, 0x90
.LBB5_608:                              #   in Loop: Header=BB5_607 Depth=1
	movq	%r15, %rax
	movq	%rdi, %rcx
.LBB5_627:                              # %.lr.ph.i.i164.prol.loopexit
                                        #   in Loop: Header=BB5_607 Depth=1
	cmpq	$56, %rbx
	movq	16(%rsp), %rsi          # 8-byte Reload
	jb	.LBB5_629
	.p2align	4, 0x90
.LBB5_628:                              # %.lr.ph.i.i164
                                        #   Parent Loop BB5_607 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB5_628
.LBB5_629:                              # %_ZN9benchmark4copyISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEESA_EEvT_SB_T0_.exit.i166
                                        #   in Loop: Header=BB5_607 Depth=1
	movq	%r15, 392(%rsp)
	movq	%r12, 376(%rsp)
.Ltmp38:
	leaq	384(%rsp), %rdi
	leaq	368(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
.Ltmp39:
# BB#630:                               # %.noexc172.preheader
                                        #   in Loop: Header=BB5_607 Depth=1
	movq	%r13, %rax
	movq	%r15, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_631:                              # %.noexc172
                                        #   Parent Loop BB5_607 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	je	.LBB5_634
# BB#632:                               #   in Loop: Header=BB5_631 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_631
# BB#633:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i170
                                        #   in Loop: Header=BB5_607 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_634:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i171
                                        #   in Loop: Header=BB5_607 Depth=1
	incl	%r14d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r14d
	jl	.LBB5_607
	jmp	.LBB5_635
.LBB5_617:                              # %.lr.ph.i161.split.us.preheader
	leaq	8(%r15), %rbp
	leaq	384(%rsp), %r14
	leaq	368(%rsp), %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_618:                              # %.lr.ph.i161.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_620 Depth 2
	movq	%r15, 392(%rsp)
	movq	%r12, 376(%rsp)
.Ltmp41:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
.Ltmp42:
# BB#619:                               # %.noexc172.us.preheader
                                        #   in Loop: Header=BB5_618 Depth=1
	movq	%rbp, %rax
	movq	%r15, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_620:                              # %.noexc172.us
                                        #   Parent Loop BB5_618 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	je	.LBB5_623
# BB#621:                               #   in Loop: Header=BB5_620 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_620
# BB#622:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i170.us
                                        #   in Loop: Header=BB5_618 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_623:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i171.us
                                        #   in Loop: Header=BB5_618 Depth=1
	incl	%ebx
	movl	iterations(%rip), %eax
	cmpl	%eax, %ebx
	jl	.LBB5_618
.LBB5_635:                              # %.loopexit590
	testl	%eax, %eax
	jle	.LBB5_658
# BB#636:                               # %.lr.ph.i143
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, %rdi
	je	.LBB5_640
# BB#637:                               # %.lr.ph.i143.split.preheader
	leaq	-8(%rax), %rbx
	subq	%rdi, %rbx
	movl	%ebx, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andl	$7, %ebp
	movq	%rbp, %rax
	negq	%rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	8(%r15), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_638:                              # %.lr.ph.i143.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_649 Depth 2
                                        #     Child Loop BB5_651 Depth 2
                                        #     Child Loop BB5_654 Depth 2
	testq	%rbp, %rbp
	je	.LBB5_639
# BB#648:                               # %.lr.ph.i.i146.prol.preheader
                                        #   in Loop: Header=BB5_638 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r15, %rax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB5_649:                              # %.lr.ph.i.i146.prol
                                        #   Parent Loop BB5_638 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rsi
	addq	$8, %rcx
	movq	%rsi, (%rax)
	addq	$8, %rax
	incq	%rdx
	jne	.LBB5_649
	jmp	.LBB5_650
	.p2align	4, 0x90
.LBB5_639:                              #   in Loop: Header=BB5_638 Depth=1
	movq	%r15, %rax
	movq	%rdi, %rcx
.LBB5_650:                              # %.lr.ph.i.i146.prol.loopexit
                                        #   in Loop: Header=BB5_638 Depth=1
	cmpq	$56, %rbx
	movq	16(%rsp), %rsi          # 8-byte Reload
	jb	.LBB5_652
	.p2align	4, 0x90
.LBB5_651:                              # %.lr.ph.i.i146
                                        #   Parent Loop BB5_638 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB5_651
.LBB5_652:                              # %_ZN9benchmark4copyISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEESA_EEvT_SB_T0_.exit.i148
                                        #   in Loop: Header=BB5_638 Depth=1
	movq	%r15, 360(%rsp)
	movq	%r12, 344(%rsp)
.Ltmp44:
	leaq	352(%rsp), %rdi
	leaq	336(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
.Ltmp45:
# BB#653:                               # %.noexc154.preheader
                                        #   in Loop: Header=BB5_638 Depth=1
	movq	%r13, %rax
	movq	%r15, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_654:                              # %.noexc154
                                        #   Parent Loop BB5_638 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	je	.LBB5_657
# BB#655:                               #   in Loop: Header=BB5_654 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_654
# BB#656:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i152
                                        #   in Loop: Header=BB5_638 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_657:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i153
                                        #   in Loop: Header=BB5_638 Depth=1
	incl	%r14d
	cmpl	iterations(%rip), %r14d
	jl	.LBB5_638
	jmp	.LBB5_658
.LBB5_640:                              # %.lr.ph.i143.split.us.preheader
	leaq	8(%r15), %rbp
	leaq	352(%rsp), %r14
	leaq	336(%rsp), %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_641:                              # %.lr.ph.i143.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_643 Depth 2
	movq	%r15, 360(%rsp)
	movq	%r12, 344(%rsp)
.Ltmp47:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
.Ltmp48:
# BB#642:                               # %.noexc154.us.preheader
                                        #   in Loop: Header=BB5_641 Depth=1
	movq	%rbp, %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_643:                              # %.noexc154.us
                                        #   Parent Loop BB5_641 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	je	.LBB5_646
# BB#644:                               #   in Loop: Header=BB5_643 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_643
# BB#645:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i152.us
                                        #   in Loop: Header=BB5_641 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
.LBB5_646:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i153.us
                                        #   in Loop: Header=BB5_641 Depth=1
	incl	%ebx
	cmpl	iterations(%rip), %ebx
	jl	.LBB5_641
.LBB5_658:                              # %.loopexit586
	movq	dMpb(%rip), %rdi
	movq	dMpe(%rip), %rsi
	movq	dpb(%rip), %rdx
	movq	dpe(%rip), %rcx
.Ltmp50:
	xorpd	%xmm0, %xmm0
	movl	$.L.str.42, %r8d
	callq	_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc
.Ltmp51:
# BB#659:
	movq	96(%rsp), %rdi
	movq	104(%rsp), %rsi
	movq	64(%rsp), %rdx
	movq	72(%rsp), %rcx
.Ltmp52:
	xorpd	%xmm0, %xmm0
	movl	$.L.str.43, %r8d
	callq	_Z14test_heap_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
.Ltmp53:
# BB#660:
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	jle	.LBB5_674
# BB#661:                               # %.lr.ph.i121
	movq	rdMpb(%rip), %rcx
	movq	rdMpe(%rip), %r14
	movq	rdpb(%rip), %rax
	movq	rdpe(%rip), %rbx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	%r14, %rcx
	je	.LBB5_693
# BB#662:                               # %.lr.ph.i121.split.preheader
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	-8(%rcx), %rax
	subq	%r14, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$3, %eax
	incl	%eax
	andl	$7, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	negq	%rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_663:                              # %.lr.ph.i121.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_665 Depth 2
                                        #     Child Loop BB5_667 Depth 2
                                        #     Child Loop BB5_670 Depth 2
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movq	%rbp, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, %rcx
	je	.LBB5_666
# BB#664:                               # %.lr.ph.i.i124.prol.preheader
                                        #   in Loop: Header=BB5_663 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rax
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB5_665:                              # %.lr.ph.i.i124.prol
                                        #   Parent Loop BB5_663 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rsi
	addq	$-8, %rcx
	movq	%rsi, -8(%rax)
	addq	$-8, %rax
	incq	%rdx
	jne	.LBB5_665
.LBB5_666:                              # %.lr.ph.i.i124.prol.loopexit
                                        #   in Loop: Header=BB5_663 Depth=1
	cmpq	$56, 32(%rsp)           # 8-byte Folded Reload
	jb	.LBB5_668
	.p2align	4, 0x90
.LBB5_667:                              # %.lr.ph.i.i124
                                        #   Parent Loop BB5_663 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rdx
	movq	%rdx, -8(%rax)
	movq	-16(%rcx), %rdx
	movq	%rdx, -16(%rax)
	movq	-24(%rcx), %rdx
	movq	%rdx, -24(%rax)
	movq	-32(%rcx), %rdx
	movq	%rdx, -32(%rax)
	movq	-40(%rcx), %rdx
	movq	%rdx, -40(%rax)
	movq	-48(%rcx), %rdx
	movq	%rdx, -48(%rax)
	movq	-56(%rcx), %rdx
	movq	%rdx, -56(%rax)
	movq	-64(%rcx), %rdx
	addq	$-64, %rcx
	movq	%rdx, -64(%rax)
	addq	$-64, %rax
	cmpq	%rcx, %r14
	jne	.LBB5_667
.LBB5_668:                              # %_ZN9benchmark4copyISt16reverse_iteratorIPdES3_EEvT_S4_T0_.exit.i
                                        #   in Loop: Header=BB5_663 Depth=1
	movq	%rbp, 176(%rsp)
	movq	%rbx, 168(%rsp)
.Ltmp55:
	leaq	176(%rsp), %rdi
	leaq	168(%rsp), %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIPdEdEEvT_S4_
.Ltmp56:
# BB#669:                               # %.noexc128.preheader
                                        #   in Loop: Header=BB5_663 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_670:                              # %.noexc128
                                        #   Parent Loop BB5_663 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB5_673
# BB#671:                               #   in Loop: Header=BB5_670 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_670
# BB#672:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIPdEEEbT_S4_.exit.i.i
                                        #   in Loop: Header=BB5_663 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_673:                              # %_Z13verify_sortedISt16reverse_iteratorIPdEEvT_S3_.exit.i
                                        #   in Loop: Header=BB5_663 Depth=1
	incl	%r13d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r13d
	jl	.LBB5_663
	jmp	.LBB5_674
.LBB5_693:                              # %.lr.ph.i121.split.us.preheader
	leaq	168(%rsp), %r13
	xorl	%r14d, %r14d
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_694:                              # %.lr.ph.i121.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_696 Depth 2
	movq	%rbp, 176(%rsp)
	movq	%rbx, 168(%rsp)
.Ltmp58:
	leaq	176(%rsp), %rdi
	movq	%r13, %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIPdEdEEvT_S4_
.Ltmp59:
# BB#695:                               # %.noexc128.us.preheader
                                        #   in Loop: Header=BB5_694 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_696:                              # %.noexc128.us
                                        #   Parent Loop BB5_694 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB5_699
# BB#697:                               #   in Loop: Header=BB5_696 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_696
# BB#698:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIPdEEEbT_S4_.exit.i.i.us
                                        #   in Loop: Header=BB5_694 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_699:                              # %_Z13verify_sortedISt16reverse_iteratorIPdEEvT_S3_.exit.i.us
                                        #   in Loop: Header=BB5_694 Depth=1
	incl	%r14d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r14d
	jl	.LBB5_694
.LBB5_674:                              # %.loopexit583
	testl	%eax, %eax
	jle	.LBB5_688
# BB#675:                               # %.lr.ph.i109
	movq	96(%rsp), %r13
	movq	104(%rsp), %rcx
	movq	64(%rsp), %rbp
	movq	72(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	%r13, %rcx
	je	.LBB5_701
# BB#676:                               # %.lr.ph.i109.split.preheader
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	-8(%rcx), %rax
	subq	%r13, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$3, %eax
	incl	%eax
	andl	$7, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	negq	%rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_677:                              # %.lr.ph.i109.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_679 Depth 2
                                        #     Child Loop BB5_681 Depth 2
                                        #     Child Loop BB5_684 Depth 2
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, %rcx
	je	.LBB5_680
# BB#678:                               # %.lr.ph.i.i112.prol.preheader
                                        #   in Loop: Header=BB5_677 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %rax
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB5_679:                              # %.lr.ph.i.i112.prol
                                        #   Parent Loop BB5_677 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rsi
	addq	$-8, %rcx
	movq	%rsi, -8(%rax)
	addq	$-8, %rax
	incq	%rdx
	jne	.LBB5_679
.LBB5_680:                              # %.lr.ph.i.i112.prol.loopexit
                                        #   in Loop: Header=BB5_677 Depth=1
	cmpq	$56, 32(%rsp)           # 8-byte Folded Reload
	jb	.LBB5_682
	.p2align	4, 0x90
.LBB5_681:                              # %.lr.ph.i.i112
                                        #   Parent Loop BB5_677 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rdx
	movq	%rdx, -8(%rax)
	movq	-16(%rcx), %rdx
	movq	%rdx, -16(%rax)
	movq	-24(%rcx), %rdx
	movq	%rdx, -24(%rax)
	movq	-32(%rcx), %rdx
	movq	%rdx, -32(%rax)
	movq	-40(%rcx), %rdx
	movq	%rdx, -40(%rax)
	movq	-48(%rcx), %rdx
	movq	%rdx, -48(%rax)
	movq	-56(%rcx), %rdx
	movq	%rdx, -56(%rax)
	movq	-64(%rcx), %rdx
	addq	$-64, %rcx
	movq	%rdx, -64(%rax)
	addq	$-64, %rax
	cmpq	%rcx, %r13
	jne	.LBB5_681
.LBB5_682:                              # %_ZN9benchmark4copyISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEES9_EEvT_SA_T0_.exit.i114
                                        #   in Loop: Header=BB5_677 Depth=1
	movq	%rbx, 160(%rsp)
	movq	%rbp, 152(%rsp)
.Ltmp61:
	leaq	160(%rsp), %rdi
	leaq	152(%rsp), %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
.Ltmp62:
# BB#683:                               # %.noexc119.preheader
                                        #   in Loop: Header=BB5_677 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbx, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_684:                              # %.noexc119
                                        #   Parent Loop BB5_677 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbp
	je	.LBB5_687
# BB#685:                               #   in Loop: Header=BB5_684 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_684
# BB#686:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i117
                                        #   in Loop: Header=BB5_677 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_687:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i118
                                        #   in Loop: Header=BB5_677 Depth=1
	incl	%r14d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r14d
	jl	.LBB5_677
	jmp	.LBB5_688
.LBB5_701:                              # %.lr.ph.i109.split.us.preheader
	leaq	152(%rsp), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_702:                              # %.lr.ph.i109.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_704 Depth 2
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, 160(%rsp)
	movq	%rbp, 152(%rsp)
.Ltmp64:
	leaq	160(%rsp), %rdi
	movq	%r13, %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
.Ltmp65:
# BB#703:                               # %.noexc119.us.preheader
                                        #   in Loop: Header=BB5_702 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbx, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_704:                              # %.noexc119.us
                                        #   Parent Loop BB5_702 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbp
	je	.LBB5_707
# BB#705:                               #   in Loop: Header=BB5_704 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_704
# BB#706:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i117.us
                                        #   in Loop: Header=BB5_702 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_707:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i118.us
                                        #   in Loop: Header=BB5_702 Depth=1
	incl	%r14d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r14d
	jl	.LBB5_702
.LBB5_688:                              # %.loopexit579
	testl	%eax, %eax
	jle	.LBB5_727
# BB#689:                               # %.lr.ph.i103
	leaq	-8(%r12), %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	%rdi, %rax
	je	.LBB5_709
# BB#690:                               # %.lr.ph.i103.split.preheader
	leaq	-8(%rax), %r14
	subq	%rdi, %r14
	movl	%r14d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andl	$7, %ebx
	movq	%rbx, %rax
	negq	%rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_691:                              # %.lr.ph.i103.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_718 Depth 2
                                        #     Child Loop BB5_720 Depth 2
                                        #     Child Loop BB5_723 Depth 2
	testq	%rbx, %rbx
	je	.LBB5_692
# BB#717:                               # %.lr.ph.i.i105.prol.preheader
                                        #   in Loop: Header=BB5_691 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r12, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_718:                              # %.lr.ph.i.i105.prol
                                        #   Parent Loop BB5_691 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rsi
	addq	$-8, %rcx
	movq	%rsi, -8(%rax)
	addq	$-8, %rax
	incq	%rdx
	jne	.LBB5_718
	jmp	.LBB5_719
	.p2align	4, 0x90
.LBB5_692:                              #   in Loop: Header=BB5_691 Depth=1
	movq	%r12, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB5_719:                              # %.lr.ph.i.i105.prol.loopexit
                                        #   in Loop: Header=BB5_691 Depth=1
	cmpq	$56, %r14
	jb	.LBB5_721
	.p2align	4, 0x90
.LBB5_720:                              # %.lr.ph.i.i105
                                        #   Parent Loop BB5_691 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rdx
	movq	%rdx, -8(%rax)
	movq	-16(%rcx), %rdx
	movq	%rdx, -16(%rax)
	movq	-24(%rcx), %rdx
	movq	%rdx, -24(%rax)
	movq	-32(%rcx), %rdx
	movq	%rdx, -32(%rax)
	movq	-40(%rcx), %rdx
	movq	%rdx, -40(%rax)
	movq	-48(%rcx), %rdx
	movq	%rdx, -48(%rax)
	movq	-56(%rcx), %rdx
	movq	%rdx, -56(%rax)
	movq	-64(%rcx), %rdx
	addq	$-64, %rcx
	movq	%rdx, -64(%rax)
	addq	$-64, %rax
	cmpq	%rcx, %rdi
	jne	.LBB5_720
.LBB5_721:                              # %_ZN9benchmark4copyISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEES9_EEvT_SA_T0_.exit.i
                                        #   in Loop: Header=BB5_691 Depth=1
	movq	%r12, 144(%rsp)
	movq	%r15, 136(%rsp)
.Ltmp67:
	leaq	144(%rsp), %rdi
	leaq	136(%rsp), %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
.Ltmp68:
# BB#722:                               # %.noexc108.preheader
                                        #   in Loop: Header=BB5_691 Depth=1
	movq	%rbp, %rax
	movq	%r12, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_723:                              # %.noexc108
                                        #   Parent Loop BB5_691 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r15
	je	.LBB5_726
# BB#724:                               #   in Loop: Header=BB5_723 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_723
# BB#725:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i
                                        #   in Loop: Header=BB5_691 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_726:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i
                                        #   in Loop: Header=BB5_691 Depth=1
	incl	%r13d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r13d
	jl	.LBB5_691
	jmp	.LBB5_727
.LBB5_709:                              # %.lr.ph.i103.split.us.preheader
	leaq	144(%rsp), %r14
	leaq	136(%rsp), %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_710:                              # %.lr.ph.i103.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_712 Depth 2
	movq	%r12, 144(%rsp)
	movq	%r15, 136(%rsp)
.Ltmp70:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
.Ltmp71:
# BB#711:                               # %.noexc108.us.preheader
                                        #   in Loop: Header=BB5_710 Depth=1
	movq	%rbp, %rax
	movq	%r12, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_712:                              # %.noexc108.us
                                        #   Parent Loop BB5_710 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r15
	je	.LBB5_715
# BB#713:                               #   in Loop: Header=BB5_712 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	-8(%rax), %xmm0
	leaq	-8(%rax), %rax
	jbe	.LBB5_712
# BB#714:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEbT_SA_.exit.i.i.us
                                        #   in Loop: Header=BB5_710 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_715:                              # %_Z13verify_sortedISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEvT_S9_.exit.i.us
                                        #   in Loop: Header=BB5_710 Depth=1
	incl	%ebx
	movl	iterations(%rip), %eax
	cmpl	%eax, %ebx
	jl	.LBB5_710
.LBB5_727:                              # %.loopexit576
	testl	%eax, %eax
	jle	.LBB5_741
# BB#728:                               # %.lr.ph.i94
	movq	rrdMpb+8(%rip), %rax
	movq	rrdMpe+8(%rip), %r13
	movq	rrdpb+8(%rip), %rbp
	movq	rrdpe+8(%rip), %rbx
	cmpq	%r13, %rax
	je	.LBB5_746
# BB#729:                               # %.lr.ph.i94.split.preheader
	leaq	-8(%r13), %rcx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	subq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	shrl	$3, %ecx
	incl	%ecx
	andl	$7, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	negq	%rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	8(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_730:                              # %.lr.ph.i94.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_732 Depth 2
                                        #     Child Loop BB5_734 Depth 2
                                        #     Child Loop BB5_737 Depth 2
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	movq	%rbp, %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rsi, %rcx
	je	.LBB5_733
# BB#731:                               # %.lr.ph.i.i97.prol.preheader
                                        #   in Loop: Header=BB5_730 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rax
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB5_732:                              # %.lr.ph.i.i97.prol
                                        #   Parent Loop BB5_730 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rsi
	addq	$8, %rcx
	movq	%rsi, (%rax)
	addq	$8, %rax
	incq	%rdx
	jne	.LBB5_732
.LBB5_733:                              # %.lr.ph.i.i97.prol.loopexit
                                        #   in Loop: Header=BB5_730 Depth=1
	cmpq	$56, 24(%rsp)           # 8-byte Folded Reload
	jb	.LBB5_735
	.p2align	4, 0x90
.LBB5_734:                              # %.lr.ph.i.i97
                                        #   Parent Loop BB5_730 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%r13, %rcx
	jne	.LBB5_734
.LBB5_735:                              # %_ZN9benchmark4copyISt16reverse_iteratorIS1_IPdEES4_EEvT_S5_T0_.exit.i
                                        #   in Loop: Header=BB5_730 Depth=1
	movq	%rbp, 328(%rsp)
	movq	%rbx, 312(%rsp)
.Ltmp73:
	leaq	320(%rsp), %rdi
	leaq	304(%rsp), %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
.Ltmp74:
# BB#736:                               # %.noexc102.preheader
                                        #   in Loop: Header=BB5_730 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rbp, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_737:                              # %.noexc102
                                        #   Parent Loop BB5_730 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB5_740
# BB#738:                               #   in Loop: Header=BB5_737 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_737
# BB#739:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IPdEEEEbT_S5_.exit.i.i
                                        #   in Loop: Header=BB5_730 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_740:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IPdEEEvT_S4_.exit.i
                                        #   in Loop: Header=BB5_730 Depth=1
	incl	%r14d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r14d
	jl	.LBB5_730
	jmp	.LBB5_741
.LBB5_746:                              # %.lr.ph.i94.split.us.preheader
	leaq	8(%rbp), %r14
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_747:                              # %.lr.ph.i94.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_749 Depth 2
	movq	%rbp, 328(%rsp)
	movq	%rbx, 312(%rsp)
.Ltmp76:
	leaq	320(%rsp), %rdi
	leaq	304(%rsp), %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
.Ltmp77:
# BB#748:                               # %.noexc102.us.preheader
                                        #   in Loop: Header=BB5_747 Depth=1
	movq	%r14, %rax
	movq	%rbp, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_749:                              # %.noexc102.us
                                        #   Parent Loop BB5_747 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB5_752
# BB#750:                               #   in Loop: Header=BB5_749 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_749
# BB#751:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IPdEEEEbT_S5_.exit.i.i.us
                                        #   in Loop: Header=BB5_747 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_752:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IPdEEEvT_S4_.exit.i.us
                                        #   in Loop: Header=BB5_747 Depth=1
	incl	%r13d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r13d
	jl	.LBB5_747
.LBB5_741:                              # %.loopexit572
	testl	%eax, %eax
	jle	.LBB5_772
# BB#742:                               # %.lr.ph.i80
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, %rdi
	je	.LBB5_754
# BB#743:                               # %.lr.ph.i80.split.preheader
	leaq	-8(%rax), %rbx
	subq	%rdi, %rbx
	movl	%ebx, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andl	$7, %ebp
	movq	%rbp, %rax
	negq	%rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	8(%r15), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_744:                              # %.lr.ph.i80.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_763 Depth 2
                                        #     Child Loop BB5_765 Depth 2
                                        #     Child Loop BB5_768 Depth 2
	testq	%rbp, %rbp
	je	.LBB5_745
# BB#762:                               # %.lr.ph.i.i83.prol.preheader
                                        #   in Loop: Header=BB5_744 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r15, %rax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB5_763:                              # %.lr.ph.i.i83.prol
                                        #   Parent Loop BB5_744 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rsi
	addq	$8, %rcx
	movq	%rsi, (%rax)
	addq	$8, %rax
	incq	%rdx
	jne	.LBB5_763
	jmp	.LBB5_764
	.p2align	4, 0x90
.LBB5_745:                              #   in Loop: Header=BB5_744 Depth=1
	movq	%r15, %rax
	movq	%rdi, %rcx
.LBB5_764:                              # %.lr.ph.i.i83.prol.loopexit
                                        #   in Loop: Header=BB5_744 Depth=1
	cmpq	$56, %rbx
	movq	16(%rsp), %rsi          # 8-byte Reload
	jb	.LBB5_766
	.p2align	4, 0x90
.LBB5_765:                              # %.lr.ph.i.i83
                                        #   Parent Loop BB5_744 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB5_765
.LBB5_766:                              # %_ZN9benchmark4copyISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEESA_EEvT_SB_T0_.exit.i85
                                        #   in Loop: Header=BB5_744 Depth=1
	movq	%r15, 296(%rsp)
	movq	%r12, 280(%rsp)
.Ltmp79:
	leaq	288(%rsp), %rdi
	leaq	272(%rsp), %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
.Ltmp80:
# BB#767:                               # %.noexc91.preheader
                                        #   in Loop: Header=BB5_744 Depth=1
	movq	%r13, %rax
	movq	%r15, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_768:                              # %.noexc91
                                        #   Parent Loop BB5_744 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	je	.LBB5_771
# BB#769:                               #   in Loop: Header=BB5_768 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_768
# BB#770:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i89
                                        #   in Loop: Header=BB5_744 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_771:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i90
                                        #   in Loop: Header=BB5_744 Depth=1
	incl	%r14d
	movl	iterations(%rip), %eax
	cmpl	%eax, %r14d
	jl	.LBB5_744
	jmp	.LBB5_772
.LBB5_754:                              # %.lr.ph.i80.split.us.preheader
	leaq	8(%r15), %rbp
	leaq	288(%rsp), %r14
	leaq	272(%rsp), %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_755:                              # %.lr.ph.i80.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_757 Depth 2
	movq	%r15, 296(%rsp)
	movq	%r12, 280(%rsp)
.Ltmp82:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
.Ltmp83:
# BB#756:                               # %.noexc91.us.preheader
                                        #   in Loop: Header=BB5_755 Depth=1
	movq	%rbp, %rax
	movq	%r15, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_757:                              # %.noexc91.us
                                        #   Parent Loop BB5_755 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	je	.LBB5_760
# BB#758:                               #   in Loop: Header=BB5_757 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_757
# BB#759:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i89.us
                                        #   in Loop: Header=BB5_755 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_760:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i90.us
                                        #   in Loop: Header=BB5_755 Depth=1
	incl	%ebx
	movl	iterations(%rip), %eax
	cmpl	%eax, %ebx
	jl	.LBB5_755
.LBB5_772:                              # %.loopexit569
	testl	%eax, %eax
	jle	.LBB5_795
# BB#773:                               # %.lr.ph.i78
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, %rdi
	je	.LBB5_777
# BB#774:                               # %.lr.ph.i78.split.preheader
	leaq	-8(%rax), %rbx
	subq	%rdi, %rbx
	movl	%ebx, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andl	$7, %ebp
	movq	%rbp, %rax
	negq	%rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	8(%r15), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_775:                              # %.lr.ph.i78.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_786 Depth 2
                                        #     Child Loop BB5_788 Depth 2
                                        #     Child Loop BB5_791 Depth 2
	testq	%rbp, %rbp
	je	.LBB5_776
# BB#785:                               # %.lr.ph.i.i.prol.preheader
                                        #   in Loop: Header=BB5_775 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r15, %rax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB5_786:                              # %.lr.ph.i.i.prol
                                        #   Parent Loop BB5_775 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rsi
	addq	$8, %rcx
	movq	%rsi, (%rax)
	addq	$8, %rax
	incq	%rdx
	jne	.LBB5_786
	jmp	.LBB5_787
	.p2align	4, 0x90
.LBB5_776:                              #   in Loop: Header=BB5_775 Depth=1
	movq	%r15, %rax
	movq	%rdi, %rcx
.LBB5_787:                              # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB5_775 Depth=1
	cmpq	$56, %rbx
	movq	16(%rsp), %rsi          # 8-byte Reload
	jb	.LBB5_789
	.p2align	4, 0x90
.LBB5_788:                              # %.lr.ph.i.i
                                        #   Parent Loop BB5_775 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB5_788
.LBB5_789:                              # %_ZN9benchmark4copyISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEESA_EEvT_SB_T0_.exit.i
                                        #   in Loop: Header=BB5_775 Depth=1
	movq	%r15, 264(%rsp)
	movq	%r12, 248(%rsp)
.Ltmp85:
	leaq	256(%rsp), %rdi
	leaq	240(%rsp), %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
.Ltmp86:
# BB#790:                               # %.noexc79.preheader
                                        #   in Loop: Header=BB5_775 Depth=1
	movq	%r13, %rax
	movq	%r15, %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_791:                              # %.noexc79
                                        #   Parent Loop BB5_775 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	je	.LBB5_794
# BB#792:                               #   in Loop: Header=BB5_791 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_791
# BB#793:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i
                                        #   in Loop: Header=BB5_775 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB5_794:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i
                                        #   in Loop: Header=BB5_775 Depth=1
	incl	%r14d
	cmpl	iterations(%rip), %r14d
	jl	.LBB5_775
	jmp	.LBB5_795
.LBB5_777:                              # %.lr.ph.i78.split.us.preheader
	leaq	8(%r15), %rbp
	leaq	256(%rsp), %r14
	leaq	240(%rsp), %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_778:                              # %.lr.ph.i78.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_780 Depth 2
	movq	%r15, 264(%rsp)
	movq	%r12, 248(%rsp)
.Ltmp88:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
.Ltmp89:
# BB#779:                               # %.noexc79.us.preheader
                                        #   in Loop: Header=BB5_778 Depth=1
	movq	%rbp, %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_780:                              # %.noexc79.us
                                        #   Parent Loop BB5_778 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	je	.LBB5_783
# BB#781:                               #   in Loop: Header=BB5_780 Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB5_780
# BB#782:                               # %_ZN9benchmark9is_sortedISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEEbT_SB_.exit.i.i.us
                                        #   in Loop: Header=BB5_778 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
.LBB5_783:                              # %_Z13verify_sortedISt16reverse_iteratorIS0_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEEvT_SA_.exit.i.us
                                        #   in Loop: Header=BB5_778 Depth=1
	incl	%ebx
	cmpl	iterations(%rip), %ebx
	jl	.LBB5_778
.LBB5_795:                              # %.loopexit565
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_797
# BB#796:
	callq	_ZdlPv
.LBB5_797:                              # %_ZNSt6vectorIdSaIdEED2Ev.exit77
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_799
# BB#798:
	callq	_ZdlPv
.LBB5_799:                              # %_ZNSt6vectorIdSaIdEED2Ev.exit75
	xorl	%eax, %eax
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_801:
.Ltmp5:
	jmp	.LBB5_816
.LBB5_800:
.Ltmp2:
	movq	%rax, %rbx
	jmp	.LBB5_818
.LBB5_814:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp
.Ltmp54:
	jmp	.LBB5_816
.LBB5_784:                              # %.loopexit.us-lcssa.us
.Ltmp90:
	jmp	.LBB5_816
.LBB5_761:                              # %.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp84:
	jmp	.LBB5_816
.LBB5_753:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp78:
	jmp	.LBB5_816
.LBB5_716:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp72:
	jmp	.LBB5_816
.LBB5_708:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp66:
	jmp	.LBB5_816
.LBB5_700:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp60:
	jmp	.LBB5_816
.LBB5_647:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp49:
	jmp	.LBB5_816
.LBB5_624:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp43:
	jmp	.LBB5_816
.LBB5_616:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp37:
	jmp	.LBB5_816
.LBB5_579:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp31:
	jmp	.LBB5_816
.LBB5_571:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp25:
	jmp	.LBB5_816
.LBB5_815:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa.us
.Ltmp19:
	jmp	.LBB5_816
.LBB5_802:                              # %.loopexit.us-lcssa
.Ltmp87:
	jmp	.LBB5_816
.LBB5_803:                              # %.loopexit.split-lp.loopexit.us-lcssa
.Ltmp81:
	jmp	.LBB5_816
.LBB5_804:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa
.Ltmp75:
	jmp	.LBB5_816
.LBB5_805:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa
.Ltmp69:
	jmp	.LBB5_816
.LBB5_806:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa
.Ltmp63:
	jmp	.LBB5_816
.LBB5_807:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa
.Ltmp57:
	jmp	.LBB5_816
.LBB5_808:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa
.Ltmp46:
	jmp	.LBB5_816
.LBB5_809:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa
.Ltmp40:
	jmp	.LBB5_816
.LBB5_810:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa
.Ltmp34:
	jmp	.LBB5_816
.LBB5_811:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa
.Ltmp28:
	jmp	.LBB5_816
.LBB5_812:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa
.Ltmp22:
	jmp	.LBB5_816
.LBB5_813:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.us-lcssa
.Ltmp16:
.LBB5_816:                              # %.loopexit
	movq	%rax, %rbx
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_818
# BB#817:
	callq	_ZdlPv
.LBB5_818:
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_820
# BB#819:
	callq	_ZdlPv
.LBB5_820:                              # %_ZNSt6vectorIdSaIdEED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\374\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\371\002"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp6          #   Call between .Ltmp6 and .Ltmp13
	.long	.Ltmp54-.Lfunc_begin0   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin0   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin0   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin0   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin0   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp53-.Ltmp50         #   Call between .Ltmp50 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin0   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin0   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin0   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin0   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin0   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin0   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin0   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin0   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin0   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin0   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin0   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin0   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin0   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin0   # >> Call Site 29 <<
	.long	.Lfunc_end5-.Ltmp89     #   Call between .Ltmp89 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc,"axG",@progbits,_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc,comdat
	.weak	_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc
	.p2align	4, 0x90
	.type	_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc,@function
_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc: # @_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 176
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	iterations(%rip), %r8d
	testl	%r8d, %r8d
	jle	.LBB6_64
# BB#1:                                 # %.lr.ph
	leaq	8(%rbx), %r13
	cmpq	%r15, %r12
	je	.LBB6_2
# BB#25:                                # %.lr.ph.split.preheader
	leaq	-8(%r15), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	subq	%r12, %rcx
	shrq	$3, %rcx
	leaq	8(%rbx,%rcx,8), %rdx
	leaq	8(%r12,%rcx,8), %rsi
	leaq	1(%rcx), %r9
	movabsq	$4611686018427387900, %rax # imm = 0x3FFFFFFFFFFFFFFC
	andq	%r9, %rax
	leaq	-4(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	%ecx, %edi
	shrl	$2, %edi
	incl	%edi
	leaq	-16(%r14), %rcx
	subq	%rbx, %rcx
	shrq	$3, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	cmpq	%rsi, %rbx
	sbbb	%cl, %cl
	cmpq	%rdx, %r12
	sbbb	%dl, %dl
	andb	%cl, %dl
	andb	$1, %dl
	movb	%dl, 15(%rsp)           # 1-byte Spill
	leaq	(%rbx,%rax,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	112(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_26:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_32 Depth 2
                                        #     Child Loop BB6_35 Depth 2
                                        #     Child Loop BB6_39 Depth 2
                                        #     Child Loop BB6_41 Depth 2
                                        #     Child Loop BB6_45 Depth 2
                                        #     Child Loop BB6_51 Depth 2
                                        #       Child Loop BB6_53 Depth 3
                                        #       Child Loop BB6_57 Depth 3
                                        #     Child Loop BB6_60 Depth 2
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cmpq	$4, %r9
	movq	%rbx, %rcx
	movq	%r12, %rdx
	jb	.LBB6_37
# BB#27:                                # %min.iters.checked
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	%rbx, %rcx
	movq	%r12, %rdx
	je	.LBB6_37
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	movq	%rbx, %rcx
	movq	%r12, %rdx
	jne	.LBB6_37
# BB#29:                                # %vector.body.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB6_30
# BB#31:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_32:                               # %vector.body.prol
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%r12,%rsi,8), %xmm0
	movupd	16(%r12,%rsi,8), %xmm1
	movupd	%xmm0, (%rbx,%rsi,8)
	movupd	%xmm1, 16(%rbx,%rsi,8)
	addq	$4, %rsi
	incq	%rcx
	jne	.LBB6_32
	jmp	.LBB6_33
.LBB6_30:                               #   in Loop: Header=BB6_26 Depth=1
	xorl	%esi, %esi
.LBB6_33:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$12, 80(%rsp)           # 8-byte Folded Reload
	jb	.LBB6_36
# BB#34:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB6_26 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB6_35:                               # %vector.body
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movupd	-16(%rdx), %xmm0
	movupd	(%rdx), %xmm1
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB6_35
.LBB6_36:                               # %middle.block
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	24(%rsp), %r9           # 8-byte Folded Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	je	.LBB6_42
	.p2align	4, 0x90
.LBB6_37:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	subq	%rdx, %rsi
	movl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB6_40
# BB#38:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB6_39:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rax
	addq	$8, %rdx
	movq	%rax, (%rcx)
	addq	$8, %rcx
	incq	%rdi
	jne	.LBB6_39
.LBB6_40:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$56, %rsi
	jb	.LBB6_42
	.p2align	4, 0x90
.LBB6_41:                               # %.lr.ph.i
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
	movq	8(%rdx), %rax
	movq	%rax, 8(%rcx)
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	movq	24(%rdx), %rax
	movq	%rax, 24(%rcx)
	movq	32(%rdx), %rax
	movq	%rax, 32(%rcx)
	movq	40(%rdx), %rax
	movq	%rax, 40(%rcx)
	movq	48(%rdx), %rax
	movq	%rax, 48(%rcx)
	movq	56(%rdx), %rax
	movq	%rax, 56(%rcx)
	addq	$64, %rdx
	addq	$64, %rcx
	cmpq	%r15, %rdx
	jne	.LBB6_41
.LBB6_42:                               # %_ZN9benchmark4copyIPdS1_EEvT_S2_T0_.exit
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	%r14, %r13
	je	.LBB6_59
# BB#43:                                # %.lr.ph32.i.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	movq	%r13, %rcx
	jne	.LBB6_50
# BB#44:                                # %.lr.ph.i9.preheader.prol
                                        #   in Loop: Header=BB6_26 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	movl	$8, %ecx
	.p2align	4, 0x90
.LBB6_45:                               # %.lr.ph.i9.prol
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx,%rcx), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_48
# BB#46:                                #   in Loop: Header=BB6_45 Depth=2
	movsd	%xmm1, (%rbx,%rcx)
	addq	$-8, %rcx
	jne	.LBB6_45
# BB#47:                                #   in Loop: Header=BB6_26 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB6_49
.LBB6_48:                               # %.lr.ph.i9.prol..critedge.i.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB6_26 Depth=1
	addq	%rbx, %rcx
.LBB6_49:                               # %.critedge.i.prol
                                        #   in Loop: Header=BB6_26 Depth=1
	movsd	%xmm0, (%rcx)
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB6_50:                               # %.lr.ph32.i.prol.loopexit
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB6_59
	.p2align	4, 0x90
.LBB6_51:                               # %.lr.ph32.i
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_53 Depth 3
                                        #       Child Loop BB6_57 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	movq	%rbx, %rdx
	je	.LBB6_56
# BB#52:                                # %.lr.ph.i9.preheader
                                        #   in Loop: Header=BB6_51 Depth=2
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB6_53:                               # %.lr.ph.i9
                                        #   Parent Loop BB6_26 Depth=1
                                        #     Parent Loop BB6_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_56
# BB#54:                                #   in Loop: Header=BB6_53 Depth=3
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB6_53
# BB#55:                                #   in Loop: Header=BB6_51 Depth=2
	movq	%rbx, %rdx
.LBB6_56:                               # %.critedge.i
                                        #   in Loop: Header=BB6_51 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%rbx, %rdx
	movq	%rbx, %rsi
	je	.LBB6_70
	.p2align	4, 0x90
.LBB6_57:                               # %.lr.ph.i9.1
                                        #   Parent Loop BB6_26 Depth=1
                                        #     Parent Loop BB6_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_58
# BB#68:                                #   in Loop: Header=BB6_57 Depth=3
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB6_57
# BB#69:                                #   in Loop: Header=BB6_51 Depth=2
	movq	%rbx, %rsi
	jmp	.LBB6_70
	.p2align	4, 0x90
.LBB6_58:                               #   in Loop: Header=BB6_51 Depth=2
	movq	%rdx, %rsi
.LBB6_70:                               # %.critedge.i.1
                                        #   in Loop: Header=BB6_51 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB6_51
.LBB6_59:                               # %_ZN9benchmark13insertionSortIPddEEvT_S2_.exit.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB6_60:                               # %_ZN9benchmark13insertionSortIPddEEvT_S2_.exit
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB6_63
# BB#61:                                #   in Loop: Header=BB6_60 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB6_60
# BB#62:                                # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i
                                        #   in Loop: Header=BB6_26 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	movq	%r13, %rbp
	movq	%r9, %r13
	callq	printf
	movq	%r13, %r9
	movq	%rbp, %r13
	movl	iterations(%rip), %r8d
.LBB6_63:                               # %_Z13verify_sortedIPdEvT_S1_.exit
                                        #   in Loop: Header=BB6_26 Depth=1
	movl	16(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	%r8d, %eax
	jl	.LBB6_26
	jmp	.LBB6_64
.LBB6_2:                                # %.lr.ph.split.us.preheader
	leaq	-16(%r14), %rbp
	subq	%rbx, %rbp
	shrq	$3, %rbp
	movl	%ebp, %r15d
	andl	$1, %r15d
	leaq	16(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_6 Depth 2
                                        #     Child Loop BB6_12 Depth 2
                                        #       Child Loop BB6_14 Depth 3
                                        #       Child Loop BB6_18 Depth 3
                                        #     Child Loop BB6_21 Depth 2
	cmpq	%r14, %r13
	je	.LBB6_20
# BB#4:                                 # %.lr.ph32.i.us.preheader
                                        #   in Loop: Header=BB6_3 Depth=1
	testq	%r15, %r15
	movq	%r13, %rcx
	jne	.LBB6_11
# BB#5:                                 # %.lr.ph.i9.us.preheader.prol
                                        #   in Loop: Header=BB6_3 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	movl	$8, %ecx
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph.i9.us.prol
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx,%rcx), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_9
# BB#7:                                 #   in Loop: Header=BB6_6 Depth=2
	movsd	%xmm1, (%rbx,%rcx)
	addq	$-8, %rcx
	jne	.LBB6_6
# BB#8:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB6_10
.LBB6_9:                                # %.lr.ph.i9.us.prol..critedge.i.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB6_3 Depth=1
	addq	%rbx, %rcx
.LBB6_10:                               # %.critedge.i.us.prol
                                        #   in Loop: Header=BB6_3 Depth=1
	movsd	%xmm0, (%rcx)
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB6_11:                               # %.lr.ph32.i.us.prol.loopexit
                                        #   in Loop: Header=BB6_3 Depth=1
	testq	%rbp, %rbp
	je	.LBB6_20
	.p2align	4, 0x90
.LBB6_12:                               # %.lr.ph32.i.us
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_14 Depth 3
                                        #       Child Loop BB6_18 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	movq	%rbx, %rdx
	je	.LBB6_17
# BB#13:                                # %.lr.ph.i9.us.preheader
                                        #   in Loop: Header=BB6_12 Depth=2
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB6_14:                               # %.lr.ph.i9.us
                                        #   Parent Loop BB6_3 Depth=1
                                        #     Parent Loop BB6_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_17
# BB#15:                                #   in Loop: Header=BB6_14 Depth=3
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB6_14
# BB#16:                                #   in Loop: Header=BB6_12 Depth=2
	movq	%rbx, %rdx
.LBB6_17:                               # %.critedge.i.us
                                        #   in Loop: Header=BB6_12 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%rbx, %rdx
	movq	%rbx, %rsi
	je	.LBB6_67
	.p2align	4, 0x90
.LBB6_18:                               # %.lr.ph.i9.us.1
                                        #   Parent Loop BB6_3 Depth=1
                                        #     Parent Loop BB6_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_19
# BB#65:                                #   in Loop: Header=BB6_18 Depth=3
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB6_18
# BB#66:                                #   in Loop: Header=BB6_12 Depth=2
	movq	%rbx, %rsi
	jmp	.LBB6_67
	.p2align	4, 0x90
.LBB6_19:                               #   in Loop: Header=BB6_12 Depth=2
	movq	%rdx, %rsi
.LBB6_67:                               # %.critedge.i.us.1
                                        #   in Loop: Header=BB6_12 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB6_12
.LBB6_20:                               # %_ZN9benchmark13insertionSortIPddEEvT_S2_.exit.us.preheader
                                        #   in Loop: Header=BB6_3 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB6_21:                               # %_ZN9benchmark13insertionSortIPddEEvT_S2_.exit.us
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB6_24
# BB#22:                                #   in Loop: Header=BB6_21 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB6_21
# BB#23:                                # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i.us
                                        #   in Loop: Header=BB6_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %r8d
.LBB6_24:                               # %_Z13verify_sortedIPdEvT_S1_.exit.us
                                        #   in Loop: Header=BB6_3 Depth=1
	incl	%r12d
	cmpl	%r8d, %r12d
	jl	.LBB6_3
.LBB6_64:                               # %._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc, .Lfunc_end6-_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_endproc

	.section	.text._Z19test_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc,"axG",@progbits,_Z19test_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc,comdat
	.weak	_Z19test_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
	.p2align	4, 0x90
	.type	_Z19test_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc,@function
_Z19test_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc: # @_Z19test_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 176
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	iterations(%rip), %r8d
	testl	%r8d, %r8d
	jle	.LBB7_66
# BB#1:                                 # %.lr.ph
	leaq	8(%rbx), %r13
	cmpq	%r15, %r12
	je	.LBB7_2
# BB#26:                                # %.lr.ph.split.preheader
	leaq	-8(%r15), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	subq	%r12, %rcx
	shrq	$3, %rcx
	leaq	8(%rbx,%rcx,8), %rdx
	leaq	8(%r12,%rcx,8), %rsi
	leaq	1(%rcx), %r9
	movabsq	$4611686018427387900, %rax # imm = 0x3FFFFFFFFFFFFFFC
	andq	%r9, %rax
	leaq	-4(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	%ecx, %edi
	shrl	$2, %edi
	incl	%edi
	leaq	-16(%r14), %rcx
	subq	%rbx, %rcx
	shrq	$3, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	cmpq	%rsi, %rbx
	sbbb	%cl, %cl
	cmpq	%rdx, %r12
	sbbb	%dl, %dl
	andb	%cl, %dl
	andb	$1, %dl
	movb	%dl, 15(%rsp)           # 1-byte Spill
	leaq	(%r12,%rax,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	112(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_27:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_33 Depth 2
                                        #     Child Loop BB7_36 Depth 2
                                        #     Child Loop BB7_40 Depth 2
                                        #     Child Loop BB7_42 Depth 2
                                        #     Child Loop BB7_46 Depth 2
                                        #     Child Loop BB7_52 Depth 2
                                        #       Child Loop BB7_54 Depth 3
                                        #       Child Loop BB7_59 Depth 3
                                        #     Child Loop BB7_62 Depth 2
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cmpq	$4, %r9
	movq	%r12, %rcx
	movq	%rbx, %rdx
	jb	.LBB7_38
# BB#28:                                # %min.iters.checked
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	%r12, %rcx
	movq	%rbx, %rdx
	je	.LBB7_38
# BB#29:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	movq	%r12, %rcx
	movq	%rbx, %rdx
	jne	.LBB7_38
# BB#30:                                # %vector.body.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB7_31
# BB#32:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB7_33:                               # %vector.body.prol
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%r12,%rsi,8), %xmm0
	movupd	16(%r12,%rsi,8), %xmm1
	movupd	%xmm0, (%rbx,%rsi,8)
	movupd	%xmm1, 16(%rbx,%rsi,8)
	addq	$4, %rsi
	incq	%rcx
	jne	.LBB7_33
	jmp	.LBB7_34
.LBB7_31:                               #   in Loop: Header=BB7_27 Depth=1
	xorl	%esi, %esi
.LBB7_34:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$12, 80(%rsp)           # 8-byte Folded Reload
	jb	.LBB7_37
# BB#35:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB7_27 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB7_36:                               # %vector.body
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movupd	-16(%rsi), %xmm0
	movupd	(%rsi), %xmm1
	movupd	%xmm0, -16(%rdx)
	movupd	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB7_36
.LBB7_37:                               # %middle.block
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	24(%rsp), %r9           # 8-byte Folded Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	je	.LBB7_43
	.p2align	4, 0x90
.LBB7_38:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	subq	%rcx, %rsi
	movl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB7_41
# BB#39:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB7_40:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	addq	$8, %rcx
	movq	%rax, (%rdx)
	addq	$8, %rdx
	incq	%rdi
	jne	.LBB7_40
.LBB7_41:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$56, %rsi
	jb	.LBB7_43
	.p2align	4, 0x90
.LBB7_42:                               # %.lr.ph.i
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	movq	8(%rcx), %rax
	movq	%rax, 8(%rdx)
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	movq	24(%rcx), %rax
	movq	%rax, 24(%rdx)
	movq	32(%rcx), %rax
	movq	%rax, 32(%rdx)
	movq	40(%rcx), %rax
	movq	%rax, 40(%rdx)
	movq	48(%rcx), %rax
	movq	%rax, 48(%rdx)
	movq	56(%rcx), %rax
	movq	%rax, 56(%rdx)
	addq	$64, %rcx
	addq	$64, %rdx
	cmpq	%r15, %rcx
	jne	.LBB7_42
.LBB7_43:                               # %_ZN9benchmark4copyIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEES7_EEvT_S8_T0_.exit
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	%r14, %r13
	je	.LBB7_61
# BB#44:                                # %.lr.ph24.i.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	movq	%r13, %rcx
	jne	.LBB7_51
# BB#45:                                # %.lr.ph.i13.preheader.prol
                                        #   in Loop: Header=BB7_27 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB7_46:                               # %.lr.ph.i13.prol
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_49
# BB#47:                                #   in Loop: Header=BB7_46 Depth=2
	addq	$-8, %rdx
	movsd	%xmm1, (%rsi)
	leaq	(%rbx,%rcx), %rsi
	addq	$-8, %rcx
	cmpq	$-8, %rcx
	jne	.LBB7_46
# BB#48:                                #   in Loop: Header=BB7_27 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB7_50
.LBB7_49:                               # %.lr.ph.i13.prol..critedge.i.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB7_27 Depth=1
	leaq	8(%rbx,%rcx), %rcx
.LBB7_50:                               # %.critedge.i.prol
                                        #   in Loop: Header=BB7_27 Depth=1
	movsd	%xmm0, (%rcx)
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB7_51:                               # %.lr.ph24.i.prol.loopexit
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB7_61
	.p2align	4, 0x90
.LBB7_52:                               # %.lr.ph24.i
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_54 Depth 3
                                        #       Child Loop BB7_59 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	movq	%rbx, %rdx
	je	.LBB7_57
# BB#53:                                # %.lr.ph.i13.preheader
                                        #   in Loop: Header=BB7_52 Depth=2
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB7_54:                               # %.lr.ph.i13
                                        #   Parent Loop BB7_27 Depth=1
                                        #     Parent Loop BB7_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_57
# BB#55:                                #   in Loop: Header=BB7_54 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB7_54
# BB#56:                                #   in Loop: Header=BB7_52 Depth=2
	movq	%rbx, %rdx
.LBB7_57:                               # %.critedge.i
                                        #   in Loop: Header=BB7_52 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%rbx, %rdx
	movq	%rbx, %rsi
	je	.LBB7_72
# BB#58:                                # %.lr.ph.i13.preheader.1
                                        #   in Loop: Header=BB7_52 Depth=2
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB7_59:                               # %.lr.ph.i13.1
                                        #   Parent Loop BB7_27 Depth=1
                                        #     Parent Loop BB7_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_60
# BB#70:                                #   in Loop: Header=BB7_59 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB7_59
# BB#71:                                #   in Loop: Header=BB7_52 Depth=2
	movq	%rbx, %rsi
	jmp	.LBB7_72
	.p2align	4, 0x90
.LBB7_60:                               #   in Loop: Header=BB7_52 Depth=2
	movq	%rdx, %rsi
.LBB7_72:                               # %.critedge.i.1
                                        #   in Loop: Header=BB7_52 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB7_52
.LBB7_61:                               # %_ZN9benchmark13insertionSortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_.exit.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB7_62:                               # %_ZN9benchmark13insertionSortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_.exit
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB7_65
# BB#63:                                #   in Loop: Header=BB7_62 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB7_62
# BB#64:                                # %_ZN9benchmark9is_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEbT_S8_.exit.i
                                        #   in Loop: Header=BB7_27 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	movq	%r13, %rbp
	movq	%r9, %r13
	callq	printf
	movq	%r13, %r9
	movq	%rbp, %r13
	movl	iterations(%rip), %r8d
.LBB7_65:                               # %_Z13verify_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEvT_S7_.exit
                                        #   in Loop: Header=BB7_27 Depth=1
	movl	16(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	%r8d, %eax
	jl	.LBB7_27
	jmp	.LBB7_66
.LBB7_2:                                # %.lr.ph.split.us.preheader
	leaq	-16(%r14), %rbp
	subq	%rbx, %rbp
	shrq	$3, %rbp
	movl	%ebp, %r15d
	andl	$1, %r15d
	leaq	16(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_6 Depth 2
                                        #     Child Loop BB7_12 Depth 2
                                        #       Child Loop BB7_14 Depth 3
                                        #       Child Loop BB7_19 Depth 3
                                        #     Child Loop BB7_22 Depth 2
	cmpq	%r14, %r13
	je	.LBB7_21
# BB#4:                                 # %.lr.ph24.i.us.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	testq	%r15, %r15
	movq	%r13, %rcx
	jne	.LBB7_11
# BB#5:                                 # %.lr.ph.i13.us.preheader.prol
                                        #   in Loop: Header=BB7_3 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph.i13.us.prol
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_9
# BB#7:                                 #   in Loop: Header=BB7_6 Depth=2
	addq	$-8, %rdx
	movsd	%xmm1, (%rsi)
	leaq	(%rbx,%rcx), %rsi
	addq	$-8, %rcx
	cmpq	$-8, %rcx
	jne	.LBB7_6
# BB#8:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB7_10
.LBB7_9:                                # %.lr.ph.i13.us.prol..critedge.i.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB7_3 Depth=1
	leaq	8(%rbx,%rcx), %rcx
.LBB7_10:                               # %.critedge.i.us.prol
                                        #   in Loop: Header=BB7_3 Depth=1
	movsd	%xmm0, (%rcx)
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB7_11:                               # %.lr.ph24.i.us.prol.loopexit
                                        #   in Loop: Header=BB7_3 Depth=1
	testq	%rbp, %rbp
	je	.LBB7_21
	.p2align	4, 0x90
.LBB7_12:                               # %.lr.ph24.i.us
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_14 Depth 3
                                        #       Child Loop BB7_19 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	movq	%rbx, %rdx
	je	.LBB7_17
# BB#13:                                # %.lr.ph.i13.us.preheader
                                        #   in Loop: Header=BB7_12 Depth=2
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB7_14:                               # %.lr.ph.i13.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_17
# BB#15:                                #   in Loop: Header=BB7_14 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB7_14
# BB#16:                                #   in Loop: Header=BB7_12 Depth=2
	movq	%rbx, %rdx
.LBB7_17:                               # %.critedge.i.us
                                        #   in Loop: Header=BB7_12 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%rbx, %rdx
	movq	%rbx, %rsi
	je	.LBB7_69
# BB#18:                                # %.lr.ph.i13.us.preheader.1
                                        #   in Loop: Header=BB7_12 Depth=2
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph.i13.us.1
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_20
# BB#67:                                #   in Loop: Header=BB7_19 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB7_19
# BB#68:                                #   in Loop: Header=BB7_12 Depth=2
	movq	%rbx, %rsi
	jmp	.LBB7_69
	.p2align	4, 0x90
.LBB7_20:                               #   in Loop: Header=BB7_12 Depth=2
	movq	%rdx, %rsi
.LBB7_69:                               # %.critedge.i.us.1
                                        #   in Loop: Header=BB7_12 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB7_12
.LBB7_21:                               # %_ZN9benchmark13insertionSortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_.exit.us.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB7_22:                               # %_ZN9benchmark13insertionSortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_.exit.us
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB7_25
# BB#23:                                #   in Loop: Header=BB7_22 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB7_22
# BB#24:                                # %_ZN9benchmark9is_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEbT_S8_.exit.i.us
                                        #   in Loop: Header=BB7_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %r8d
.LBB7_25:                               # %_Z13verify_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEvT_S7_.exit.us
                                        #   in Loop: Header=BB7_3 Depth=1
	incl	%r12d
	cmpl	%r8d, %r12d
	jl	.LBB7_3
.LBB7_66:                               # %._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_Z19test_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc, .Lfunc_end7-_Z19test_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc,"axG",@progbits,_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc,comdat
	.weak	_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc,@function
_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc: # @_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 144
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB8_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB8_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_9:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_15 Depth 2
                                        #     Child Loop BB8_18 Depth 2
                                        #     Child Loop BB8_22 Depth 2
                                        #     Child Loop BB8_24 Depth 2
                                        #     Child Loop BB8_26 Depth 2
	cmpq	$4, %rbp
	movq	%r14, %rax
	movq	%r12, %rcx
	jb	.LBB8_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB8_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	je	.LBB8_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	jne	.LBB8_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB8_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB8_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_15:                               # %vector.body.prol
                                        #   Parent Loop BB8_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB8_15
	jmp	.LBB8_16
.LBB8_13:                               #   in Loop: Header=BB8_9 Depth=1
	xorl	%edx, %edx
.LBB8_16:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB8_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB8_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB8_18:                               # %vector.body
                                        #   Parent Loop BB8_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB8_18
.LBB8_19:                               # %middle.block
                                        #   in Loop: Header=BB8_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB8_25
	.p2align	4, 0x90
.LBB8_20:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rcx, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB8_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB8_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB8_22:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB8_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	addq	$8, %rcx
	movq	%rdi, (%rax)
	addq	$8, %rax
	incq	%rsi
	jne	.LBB8_22
.LBB8_23:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB8_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB8_25
	.p2align	4, 0x90
.LBB8_24:                               # %.lr.ph.i
                                        #   Parent Loop BB8_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rcx
	addq	$64, %rax
	cmpq	%r15, %rcx
	jne	.LBB8_24
.LBB8_25:                               # %_ZN9benchmark4copyIPdS1_EEvT_S2_T0_.exit
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIPddEEvT_S2_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_26:                               #   Parent Loop BB8_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB8_29
# BB#27:                                #   in Loop: Header=BB8_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB8_26
# BB#28:                                # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i
                                        #   in Loop: Header=BB8_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
.LBB8_29:                               # %_Z13verify_sortedIPdEvT_S1_.exit
                                        #   in Loop: Header=BB8_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB8_9
	jmp	.LBB8_30
.LBB8_2:                                # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIPddEEvT_S2_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB8_4:                                #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB8_7
# BB#5:                                 #   in Loop: Header=BB8_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB8_4
# BB#6:                                 # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i.us
                                        #   in Loop: Header=BB8_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
.LBB8_7:                                # %_Z13verify_sortedIPdEvT_S1_.exit.us
                                        #   in Loop: Header=BB8_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB8_3
.LBB8_30:                               # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc, .Lfunc_end8-_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc,"axG",@progbits,_Z14test_quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc,comdat
	.weak	_Z14test_quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc,@function
_Z14test_quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc: # @_Z14test_quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 144
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB9_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB9_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB9_9:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_15 Depth 2
                                        #     Child Loop BB9_18 Depth 2
                                        #     Child Loop BB9_22 Depth 2
                                        #     Child Loop BB9_24 Depth 2
                                        #     Child Loop BB9_26 Depth 2
	cmpq	$4, %rbp
	movq	%r12, %rax
	movq	%r14, %rcx
	jb	.LBB9_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB9_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	je	.LBB9_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB9_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	jne	.LBB9_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB9_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB9_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB9_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_15:                               # %vector.body.prol
                                        #   Parent Loop BB9_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB9_15
	jmp	.LBB9_16
.LBB9_13:                               #   in Loop: Header=BB9_9 Depth=1
	xorl	%edx, %edx
.LBB9_16:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB9_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB9_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB9_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB9_18:                               # %vector.body
                                        #   Parent Loop BB9_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB9_18
.LBB9_19:                               # %middle.block
                                        #   in Loop: Header=BB9_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB9_25
	.p2align	4, 0x90
.LBB9_20:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB9_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB9_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB9_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB9_22:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB9_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	addq	$8, %rax
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB9_22
.LBB9_23:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB9_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB9_25
	.p2align	4, 0x90
.LBB9_24:                               # %.lr.ph.i
                                        #   Parent Loop BB9_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rcx)
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rcx)
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rcx)
	movq	48(%rax), %rdx
	movq	%rdx, 48(%rcx)
	movq	56(%rax), %rdx
	movq	%rdx, 56(%rcx)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%r15, %rax
	jne	.LBB9_24
.LBB9_25:                               # %_ZN9benchmark4copyIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEES7_EEvT_S8_T0_.exit
                                        #   in Loop: Header=BB9_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_26:                               #   Parent Loop BB9_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB9_29
# BB#27:                                #   in Loop: Header=BB9_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB9_26
# BB#28:                                # %_ZN9benchmark9is_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEbT_S8_.exit.i
                                        #   in Loop: Header=BB9_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
.LBB9_29:                               # %_Z13verify_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEvT_S7_.exit
                                        #   in Loop: Header=BB9_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB9_9
	jmp	.LBB9_30
.LBB9_2:                                # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB9_4:                                #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB9_7
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB9_4
# BB#6:                                 # %_ZN9benchmark9is_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEbT_S8_.exit.i.us
                                        #   in Loop: Header=BB9_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
.LBB9_7:                                # %_Z13verify_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEvT_S7_.exit.us
                                        #   in Loop: Header=BB9_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB9_3
.LBB9_30:                               # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_Z14test_quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc, .Lfunc_end9-_Z14test_quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc,"axG",@progbits,_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc,comdat
	.weak	_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc,@function
_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc: # @_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 144
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB10_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB10_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB10_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_15 Depth 2
                                        #     Child Loop BB10_18 Depth 2
                                        #     Child Loop BB10_22 Depth 2
                                        #     Child Loop BB10_24 Depth 2
                                        #     Child Loop BB10_26 Depth 2
	cmpq	$4, %rbp
	movq	%r14, %rax
	movq	%r12, %rcx
	jb	.LBB10_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	je	.LBB10_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	jne	.LBB10_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB10_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_15:                              # %vector.body.prol
                                        #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB10_15
	jmp	.LBB10_16
.LBB10_13:                              #   in Loop: Header=BB10_9 Depth=1
	xorl	%edx, %edx
.LBB10_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB10_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB10_18:                              # %vector.body
                                        #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB10_18
.LBB10_19:                              # %middle.block
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB10_25
	.p2align	4, 0x90
.LBB10_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rcx, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB10_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB10_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB10_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	addq	$8, %rcx
	movq	%rdi, (%rax)
	addq	$8, %rax
	incq	%rsi
	jne	.LBB10_22
.LBB10_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB10_25
	.p2align	4, 0x90
.LBB10_24:                              # %.lr.ph.i
                                        #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rcx
	addq	$64, %rax
	cmpq	%r15, %rcx
	jne	.LBB10_24
.LBB10_25:                              # %_ZN9benchmark4copyIPdS1_EEvT_S2_T0_.exit
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortIPddEEvT_S2_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB10_26:                              #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB10_29
# BB#27:                                #   in Loop: Header=BB10_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB10_26
# BB#28:                                # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i
                                        #   in Loop: Header=BB10_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_29:                              # %_Z13verify_sortedIPdEvT_S1_.exit
                                        #   in Loop: Header=BB10_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB10_9
	jmp	.LBB10_30
.LBB10_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortIPddEEvT_S2_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB10_4:                               #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB10_7
# BB#5:                                 #   in Loop: Header=BB10_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB10_4
# BB#6:                                 # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i.us
                                        #   in Loop: Header=BB10_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_7:                               # %_Z13verify_sortedIPdEvT_S1_.exit.us
                                        #   in Loop: Header=BB10_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB10_3
.LBB10_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc, .Lfunc_end10-_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_heap_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc,"axG",@progbits,_Z14test_heap_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc,comdat
	.weak	_Z14test_heap_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_heap_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc,@function
_Z14test_heap_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc: # @_Z14test_heap_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi115:
	.cfi_def_cfa_offset 144
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB11_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB11_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_15 Depth 2
                                        #     Child Loop BB11_18 Depth 2
                                        #     Child Loop BB11_22 Depth 2
                                        #     Child Loop BB11_24 Depth 2
                                        #     Child Loop BB11_26 Depth 2
	cmpq	$4, %rbp
	movq	%r12, %rax
	movq	%r14, %rcx
	jb	.LBB11_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB11_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	je	.LBB11_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB11_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	jne	.LBB11_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB11_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB11_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB11_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_15:                              # %vector.body.prol
                                        #   Parent Loop BB11_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB11_15
	jmp	.LBB11_16
.LBB11_13:                              #   in Loop: Header=BB11_9 Depth=1
	xorl	%edx, %edx
.LBB11_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB11_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB11_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB11_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB11_18:                              # %vector.body
                                        #   Parent Loop BB11_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB11_18
.LBB11_19:                              # %middle.block
                                        #   in Loop: Header=BB11_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB11_25
	.p2align	4, 0x90
.LBB11_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB11_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB11_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB11_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB11_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB11_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	addq	$8, %rax
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB11_22
.LBB11_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB11_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB11_25
	.p2align	4, 0x90
.LBB11_24:                              # %.lr.ph.i
                                        #   Parent Loop BB11_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rcx)
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rcx)
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rcx)
	movq	48(%rax), %rdx
	movq	%rdx, 48(%rcx)
	movq	56(%rax), %rdx
	movq	%rdx, 56(%rcx)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%r15, %rax
	jne	.LBB11_24
.LBB11_25:                              # %_ZN9benchmark4copyIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEES7_EEvT_S8_T0_.exit
                                        #   in Loop: Header=BB11_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB11_26:                              #   Parent Loop BB11_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB11_29
# BB#27:                                #   in Loop: Header=BB11_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB11_26
# BB#28:                                # %_ZN9benchmark9is_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEbT_S8_.exit.i
                                        #   in Loop: Header=BB11_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
.LBB11_29:                              # %_Z13verify_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEvT_S7_.exit
                                        #   in Loop: Header=BB11_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB11_9
	jmp	.LBB11_30
.LBB11_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB11_4:                               #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB11_7
# BB#5:                                 #   in Loop: Header=BB11_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB11_4
# BB#6:                                 # %_ZN9benchmark9is_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEbT_S8_.exit.i.us
                                        #   in Loop: Header=BB11_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
.LBB11_7:                               # %_Z13verify_sortedIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEvT_S7_.exit.us
                                        #   in Loop: Header=BB11_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB11_3
.LBB11_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_Z14test_heap_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc, .Lfunc_end11-_Z14test_heap_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEvT_S7_S7_S7_T0_PKc
	.cfi_endproc

	.section	.text._ZNSt6vectorIdSaIdEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPdS1_EEmRKd,"axG",@progbits,_ZNSt6vectorIdSaIdEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPdS1_EEmRKd,comdat
	.weak	_ZNSt6vectorIdSaIdEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPdS1_EEmRKd
	.p2align	4, 0x90
	.type	_ZNSt6vectorIdSaIdEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPdS1_EEmRKd,@function
_ZNSt6vectorIdSaIdEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPdS1_EEmRKd: # @_ZNSt6vectorIdSaIdEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPdS1_EEmRKd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi126:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi128:
	.cfi_def_cfa_offset 64
.Lcfi129:
	.cfi_offset %rbx, -56
.Lcfi130:
	.cfi_offset %r12, -48
.Lcfi131:
	.cfi_offset %r13, -40
.Lcfi132:
	.cfi_offset %r14, -32
.Lcfi133:
	.cfi_offset %r15, -24
.Lcfi134:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB12_75
# BB#1:
	movq	8(%r15), %r12
	movq	16(%r15), %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	jae	.LBB12_2
# BB#49:
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	movq	%r15, %rbp
	movq	(%r15), %r15
	subq	%r15, %r12
	sarq	$3, %r12
	movq	%rax, %rcx
	subq	%r12, %rcx
	cmpq	%r14, %rcx
	jb	.LBB12_76
# BB#50:                                # %_ZNKSt6vectorIdSaIdEE12_M_check_lenEmPKc.exit
	cmpq	%r14, %r12
	movq	%r12, %rcx
	cmovbq	%r14, %rcx
	leaq	(%rcx,%r12), %rdx
	cmpq	%rax, %rdx
	cmovaq	%rax, %rdx
	addq	%r12, %rcx
	cmovbq	%rax, %rdx
	testq	%rdx, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	je	.LBB12_51
# BB#52:
	cmpq	%rax, %rdx
	ja	.LBB12_77
# BB#53:                                # %_ZN9__gnu_cxx14__alloc_traitsISaIdEE8allocateERS1_m.exit.i
	leaq	(,%rdx,8), %rdi
	callq	_Znwm
	movq	%rax, %r12
	jmp	.LBB12_54
.LBB12_2:
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	(%r13), %rbp
	movq	%r12, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %r13
	sarq	$3, %r13
	cmpq	%r14, %r13
	jbe	.LBB12_20
# BB#3:
	leaq	(,%r14,8), %rdx
	movq	%r12, %r13
	subq	%rdx, %r13
	testq	%rdx, %rdx
	movq	%r12, %rax
	je	.LBB12_5
# BB#4:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memmove
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	8(%r15), %rax
.LBB12_5:                               # %_ZSt22__uninitialized_move_aIPdS0_SaIdEET0_T_S3_S2_RT1_.exit
	leaq	(%rax,%r14,8), %rax
	movq	%rax, 8(%r15)
	subq	%rbx, %r13
	movq	%r13, %rax
	sarq	$3, %rax
	je	.LBB12_7
# BB#6:
	shlq	$3, %rax
	subq	%rax, %r12
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r9, %r15
	callq	memmove
	movq	%r15, %r9
.LBB12_7:                               # %.lr.ph.i.i68.preheader
	leaq	-8(,%r14,8), %rax
	shrq	$3, %rax
	incq	%rax
	cmpq	$4, %rax
	movq	%rbx, %rcx
	jb	.LBB12_18
# BB#8:                                 # %min.iters.checked121
	andq	%rax, %r9
	movq	%rbx, %rcx
	je	.LBB12_18
# BB#9:                                 # %vector.ph125
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB12_10
# BB#11:                                # %vector.body117.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_12:                              # %vector.body117.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rdx,8)
	movdqu	%xmm0, 16(%rbx,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB12_12
	jmp	.LBB12_13
.LBB12_20:
	subq	%r13, %r14
	movq	%r12, %rdi
	je	.LBB12_34
# BB#21:                                # %.lr.ph.i.i.i.i.i64.preheader
	cmpq	$3, %r14
	movq	%r14, %rsi
	movq	%r12, %rax
	jbe	.LBB12_32
# BB#22:                                # %min.iters.checked
	movq	%r14, %r8
	andq	$-4, %r8
	movq	%r14, %rcx
	andq	$-4, %rcx
	movq	%r14, %rsi
	movq	%r12, %rax
	je	.LBB12_32
# BB#23:                                # %vector.ph
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rcx), %rax
	movl	%eax, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB12_24
# BB#25:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB12_26:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%r12,%rdi,8)
	movdqu	%xmm0, 16(%r12,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB12_26
	jmp	.LBB12_27
.LBB12_51:
	xorl	%r12d, %r12d
.LBB12_54:                              # %_ZNSt12_Vector_baseIdSaIdEE11_M_allocateEm.exit
	movq	%rbx, %rdi
	subq	%r15, %rdi
	sarq	$3, %rdi
	leaq	(%r12,%rdi,8), %rax
	movq	(%r13), %rcx
	cmpq	$4, %r14
	jae	.LBB12_56
# BB#55:
	movq	%r14, %rdx
	movq	%rbp, %r15
	jmp	.LBB12_67
.LBB12_56:                              # %min.iters.checked139
	movq	%r14, %r8
	andq	$-4, %r8
	movq	%r14, %r9
	andq	$-4, %r9
	movq	%rbp, %r15
	je	.LBB12_57
# BB#58:                                # %vector.ph143
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %r10
	movl	%r10d, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB12_59
# BB#60:                                # %vector.body135.prol.preheader
	leaq	16(%r12,%rdi,8), %rbp
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB12_61:                              # %vector.body135.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbp,%rsi,8)
	movdqu	%xmm0, (%rbp,%rsi,8)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB12_61
	jmp	.LBB12_62
.LBB12_57:
	movq	%r14, %rdx
	jmp	.LBB12_67
.LBB12_59:
	xorl	%esi, %esi
.LBB12_62:                              # %vector.body135.prol.loopexit
	cmpq	$28, %r10
	jb	.LBB12_65
# BB#63:                                # %vector.ph143.new
	movq	%r9, %rdx
	subq	%rsi, %rdx
	addq	%rsi, %rdi
	leaq	240(%r12,%rdi,8), %rsi
	.p2align	4, 0x90
.LBB12_64:                              # %vector.body135
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rsi)
	movdqu	%xmm0, -224(%rsi)
	movdqu	%xmm0, -208(%rsi)
	movdqu	%xmm0, -192(%rsi)
	movdqu	%xmm0, -176(%rsi)
	movdqu	%xmm0, -160(%rsi)
	movdqu	%xmm0, -144(%rsi)
	movdqu	%xmm0, -128(%rsi)
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$256, %rsi              # imm = 0x100
	addq	$-32, %rdx
	jne	.LBB12_64
.LBB12_65:                              # %middle.block136
	cmpq	%r14, %r9
	je	.LBB12_68
# BB#66:
	movq	%r14, %rdx
	subq	%r8, %rdx
	leaq	(%rax,%r9,8), %rax
	.p2align	4, 0x90
.LBB12_67:                              # %.lr.ph.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rax)
	addq	$8, %rax
	decq	%rdx
	jne	.LBB12_67
.LBB12_68:                              # %_ZSt24__uninitialized_fill_n_aIPdmddET_S1_T0_RKT1_RSaIT2_E.exit
	movq	(%r15), %r13
	movq	%rbx, %rdx
	subq	%r13, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB12_70
# BB#69:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memmove
.LBB12_70:
	leaq	(%r12,%rbp,8), %rax
	leaq	(%rax,%r14,8), %r14
	movq	8(%r15), %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB12_72
# BB#71:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memmove
.LBB12_72:
	leaq	(%r14,%rbp,8), %rbx
	testq	%r13, %r13
	je	.LBB12_74
# BB#73:
	movq	%r13, %rdi
	callq	_ZdlPv
.LBB12_74:                              # %_ZNSt12_Vector_baseIdSaIdEE13_M_deallocateEPdm.exit57
	movq	%r12, (%r15)
	movq	%rbx, 8(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 16(%r15)
	jmp	.LBB12_75
.LBB12_10:
	xorl	%edx, %edx
.LBB12_13:                              # %vector.body117.prol.loopexit
	cmpq	$28, %rcx
	jb	.LBB12_16
# BB#14:                                # %vector.ph125.new
	movq	%r9, %rcx
	subq	%rdx, %rcx
	leaq	240(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB12_15:                              # %vector.body117
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdx)
	movdqu	%xmm0, -224(%rdx)
	movdqu	%xmm0, -208(%rdx)
	movdqu	%xmm0, -192(%rdx)
	movdqu	%xmm0, -176(%rdx)
	movdqu	%xmm0, -160(%rdx)
	movdqu	%xmm0, -144(%rdx)
	movdqu	%xmm0, -128(%rdx)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB12_15
.LBB12_16:                              # %middle.block118
	cmpq	%r9, %rax
	je	.LBB12_75
# BB#17:
	leaq	(%rbx,%r9,8), %rcx
.LBB12_18:                              # %.lr.ph.i.i68.preheader160
	leaq	(%rbx,%r14,8), %rax
	.p2align	4, 0x90
.LBB12_19:                              # %.lr.ph.i.i68
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB12_19
	jmp	.LBB12_75
.LBB12_24:
	xorl	%edi, %edi
.LBB12_27:                              # %vector.body.prol.loopexit
	cmpq	$28, %rax
	jb	.LBB12_30
# BB#28:                                # %vector.ph.new
	movq	%rcx, %rsi
	subq	%rdi, %rsi
	leaq	240(%r12,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB12_29:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdi)
	movdqu	%xmm0, -224(%rdi)
	movdqu	%xmm0, -208(%rdi)
	movdqu	%xmm0, -192(%rdi)
	movdqu	%xmm0, -176(%rdi)
	movdqu	%xmm0, -160(%rdi)
	movdqu	%xmm0, -144(%rdi)
	movdqu	%xmm0, -128(%rdi)
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$256, %rdi              # imm = 0x100
	addq	$-32, %rsi
	jne	.LBB12_29
.LBB12_30:                              # %middle.block
	cmpq	%rcx, %r14
	je	.LBB12_33
# BB#31:
	movq	%r14, %rsi
	subq	%r8, %rsi
	leaq	(%r12,%rcx,8), %rax
	.p2align	4, 0x90
.LBB12_32:                              # %.lr.ph.i.i.i.i.i64
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rax)
	addq	$8, %rax
	decq	%rsi
	jne	.LBB12_32
.LBB12_33:                              # %._crit_edge.loopexit.i.i.i.i.i61
	leaq	(%r12,%r14,8), %rdi
.LBB12_34:                              # %_ZSt24__uninitialized_fill_n_aIPdmddET_S1_T0_RKT1_RSaIT2_E.exit66
	movq	%rdi, 8(%r15)
	testq	%rdx, %rdx
	je	.LBB12_36
# BB#35:
	movq	%rbx, %rsi
	movq	%r9, %r14
	callq	memmove
	movq	%r14, %r9
	movq	8(%r15), %rdi
.LBB12_36:                              # %_ZSt22__uninitialized_move_aIPdS0_SaIdEET0_T_S3_S2_RT1_.exit59
	leaq	(%rdi,%r13,8), %rax
	movq	%rax, 8(%r15)
	cmpq	%rbx, %r12
	je	.LBB12_75
# BB#37:                                # %.lr.ph.i.i.preheader
	leaq	-8(%r12), %rax
	subq	%rbx, %rax
	shrq	$3, %rax
	incq	%rax
	cmpq	$4, %rax
	jb	.LBB12_48
# BB#38:                                # %min.iters.checked103
	andq	%rax, %r9
	je	.LBB12_48
# BB#39:                                # %vector.ph107
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB12_40
# BB#41:                                # %vector.body97.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_42:                              # %vector.body97.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rdx,8)
	movdqu	%xmm0, 16(%rbx,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB12_42
	jmp	.LBB12_43
.LBB12_40:
	xorl	%edx, %edx
.LBB12_43:                              # %vector.body97.prol.loopexit
	cmpq	$28, %rcx
	jb	.LBB12_46
# BB#44:                                # %vector.ph107.new
	movq	%r9, %rcx
	subq	%rdx, %rcx
	leaq	240(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB12_45:                              # %vector.body97
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdx)
	movdqu	%xmm0, -224(%rdx)
	movdqu	%xmm0, -208(%rdx)
	movdqu	%xmm0, -192(%rdx)
	movdqu	%xmm0, -176(%rdx)
	movdqu	%xmm0, -160(%rdx)
	movdqu	%xmm0, -144(%rdx)
	movdqu	%xmm0, -128(%rdx)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB12_45
.LBB12_46:                              # %middle.block98
	cmpq	%r9, %rax
	je	.LBB12_75
# BB#47:
	leaq	(%rbx,%r9,8), %rbx
	.p2align	4, 0x90
.LBB12_48:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.LBB12_48
.LBB12_75:                              # %_ZSt4fillIPddEvT_S1_RKT0_.exit69
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_76:
	movl	$.L.str.50, %edi
	callq	_ZSt20__throw_length_errorPKc
.LBB12_77:
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end12:
	.size	_ZNSt6vectorIdSaIdEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPdS1_EEmRKd, .Lfunc_end12-_ZNSt6vectorIdSaIdEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPdS1_EEmRKd
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortIPddEEvT_S2_,"axG",@progbits,_ZN9benchmark9quicksortIPddEEvT_S2_,comdat
	.weak	_ZN9benchmark9quicksortIPddEEvT_S2_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortIPddEEvT_S2_,@function
_ZN9benchmark9quicksortIPddEEvT_S2_:    # @_ZN9benchmark9quicksortIPddEEvT_S2_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi137:
	.cfi_def_cfa_offset 32
.Lcfi138:
	.cfi_offset %rbx, -24
.Lcfi139:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB13_5
	.p2align	4, 0x90
.LBB13_1:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_2 Depth 2
                                        #       Child Loop BB13_7 Depth 3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	movq	%r14, %rbx
	movq	%rdi, %rax
	jmp	.LBB13_2
.LBB13_9:                               #   in Loop: Header=BB13_2 Depth=2
	movsd	%xmm1, (%rbx)
	movsd	%xmm2, (%rax)
	movapd	%xmm2, %xmm1
	.p2align	4, 0x90
.LBB13_2:                               #   Parent Loop BB13_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_7 Depth 3
	movsd	-8(%rbx), %xmm2         # xmm2 = mem[0],zero
	addq	$-8, %rbx
	ucomisd	%xmm0, %xmm2
	ja	.LBB13_2
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=2
	cmpq	%rbx, %rax
	jb	.LBB13_7
	jmp	.LBB13_4
	.p2align	4, 0x90
.LBB13_6:                               # %.preheader..preheader_crit_edge
                                        #   in Loop: Header=BB13_7 Depth=3
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	addq	$8, %rax
.LBB13_7:                               # %.preheader..preheader_crit_edge
                                        #   Parent Loop BB13_1 Depth=1
                                        #     Parent Loop BB13_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	%xmm1, %xmm0
	ja	.LBB13_6
# BB#8:                                 # %.preheader._crit_edge
                                        #   in Loop: Header=BB13_2 Depth=2
	cmpq	%rbx, %rax
	jb	.LBB13_9
.LBB13_4:                               # %tailrecurse
                                        #   in Loop: Header=BB13_1 Depth=1
	addq	$8, %rbx
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIPddEEvT_S2_
	movq	%r14, %rax
	subq	%rbx, %rax
	cmpq	$8, %rax
	movq	%rbx, %rdi
	jg	.LBB13_1
.LBB13_5:                               # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	_ZN9benchmark9quicksortIPddEEvT_S2_, .Lfunc_end13-_ZN9benchmark9quicksortIPddEEvT_S2_
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_,"axG",@progbits,_ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_,comdat
	.weak	_ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_,@function
_ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_: # @_ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi142:
	.cfi_def_cfa_offset 32
.Lcfi143:
	.cfi_offset %rbx, -24
.Lcfi144:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB14_9
	.p2align	4, 0x90
.LBB14_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_2 Depth 2
                                        #       Child Loop BB14_5 Depth 3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movq	%rdi, %rax
	movq	%r14, %rcx
	jmp	.LBB14_2
.LBB14_7:                               #   in Loop: Header=BB14_2 Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movsd	%xmm1, (%rax)
	.p2align	4, 0x90
.LBB14_2:                               #   Parent Loop BB14_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_5 Depth 3
	movq	%rcx, %rbx
	leaq	-8(%rbx), %rcx
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB14_2
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=2
	cmpq	%rcx, %rax
	jae	.LBB14_8
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB14_2 Depth=2
	ucomisd	(%rax), %xmm0
	jbe	.LBB14_6
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph
                                        #   Parent Loop BB14_1 Depth=1
                                        #     Parent Loop BB14_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	8(%rax), %xmm0
	leaq	8(%rax), %rax
	ja	.LBB14_5
.LBB14_6:                               # %._crit_edge
                                        #   in Loop: Header=BB14_2 Depth=2
	cmpq	%rcx, %rax
	jb	.LBB14_7
.LBB14_8:                               # %tailrecurse
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	movq	%r14, %rax
	subq	%rbx, %rax
	cmpq	$8, %rax
	movq	%rbx, %rdi
	jg	.LBB14_1
.LBB14_9:                               # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	_ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_, .Lfunc_end14-_ZN9benchmark9quicksortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_,"axG",@progbits,_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_,comdat
	.weak	_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_,@function
_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_: # @_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi147:
	.cfi_def_cfa_offset 64
.Lcfi148:
	.cfi_offset %rbx, -24
.Lcfi149:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	(%rdi), %r8
	movq	(%r14), %rcx
	movq	%r8, %rdx
	subq	%rcx, %rdx
	cmpq	$9, %rdx
	jl	.LBB15_10
# BB#1:
	movsd	-8(%r8), %xmm0          # xmm0 = mem[0],zero
	movq	%r8, %rdx
	jmp	.LBB15_2
.LBB15_8:                               #   in Loop: Header=BB15_2 Depth=1
	movq	(%rbx), %rdi
	movq	(%rsi), %rax
	movq	%rax, (%rbx)
	movq	%rdi, (%rsi)
	.p2align	4, 0x90
.LBB15_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_5 Depth 2
	movq	%rcx, %rbx
	leaq	8(%rbx), %rcx
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB15_2
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	cmpq	%rdx, %rcx
	jae	.LBB15_9
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB15_2 Depth=1
	leaq	-8(%rdx), %rsi
	ucomisd	-8(%rdx), %xmm0
	jbe	.LBB15_7
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ucomisd	-8(%rsi), %xmm0
	leaq	-8(%rsi), %rsi
	ja	.LBB15_5
# BB#6:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB15_2 Depth=1
	leaq	8(%rsi), %rdx
.LBB15_7:                               # %._crit_edge
                                        #   in Loop: Header=BB15_2 Depth=1
	cmpq	%rdx, %rcx
	jb	.LBB15_8
.LBB15_9:
	movq	%r8, 32(%rsp)
	movq	%rbx, 24(%rsp)
	leaq	32(%rsp), %rdi
	leaq	24(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_
	movq	%rbx, 16(%rsp)
	movq	(%r14), %rax
	movq	%rax, 8(%rsp)
	leaq	16(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_
.LBB15_10:
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_, .Lfunc_end15-_ZN9benchmark9quicksortISt16reverse_iteratorIPdEdEEvT_S4_
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_,"axG",@progbits,_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_,comdat
	.weak	_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_,@function
_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_: # @_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi152:
	.cfi_def_cfa_offset 64
.Lcfi153:
	.cfi_offset %rbx, -24
.Lcfi154:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	(%rdi), %r8
	movq	(%r14), %rcx
	movq	%r8, %rdx
	subq	%rcx, %rdx
	cmpq	$9, %rdx
	jl	.LBB16_10
# BB#1:
	movsd	-8(%r8), %xmm0          # xmm0 = mem[0],zero
	movq	%r8, %rdx
	jmp	.LBB16_2
.LBB16_8:                               #   in Loop: Header=BB16_2 Depth=1
	movq	(%rbx), %rdi
	movq	(%rsi), %rax
	movq	%rax, (%rbx)
	movq	%rdi, (%rsi)
	.p2align	4, 0x90
.LBB16_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_5 Depth 2
	movq	%rcx, %rbx
	leaq	8(%rbx), %rcx
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB16_2
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	cmpq	%rdx, %rcx
	jae	.LBB16_9
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB16_2 Depth=1
	leaq	-8(%rdx), %rsi
	ucomisd	-8(%rdx), %xmm0
	jbe	.LBB16_7
	.p2align	4, 0x90
.LBB16_5:                               # %.lr.ph
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ucomisd	-8(%rsi), %xmm0
	leaq	-8(%rsi), %rsi
	ja	.LBB16_5
# BB#6:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB16_2 Depth=1
	leaq	8(%rsi), %rdx
.LBB16_7:                               # %._crit_edge
                                        #   in Loop: Header=BB16_2 Depth=1
	cmpq	%rdx, %rcx
	jb	.LBB16_8
.LBB16_9:
	movq	%r8, 32(%rsp)
	movq	%rbx, 24(%rsp)
	leaq	32(%rsp), %rdi
	leaq	24(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
	movq	%rbx, 16(%rsp)
	movq	(%r14), %rax
	movq	%rax, 8(%rsp)
	leaq	16(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
.LBB16_10:
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end16:
	.size	_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_, .Lfunc_end16-_ZN9benchmark9quicksortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_,"axG",@progbits,_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_,comdat
	.weak	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_,@function
_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_: # @_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi155:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi157:
	.cfi_def_cfa_offset 96
.Lcfi158:
	.cfi_offset %rbx, -24
.Lcfi159:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	8(%rdi), %rax
	movq	8(%r14), %rcx
	movq	%rcx, %rdx
	subq	%rax, %rdx
	cmpq	$9, %rdx
	jl	.LBB17_9
# BB#1:
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movq	%rax, %rdx
	jmp	.LBB17_2
.LBB17_7:                               #   in Loop: Header=BB17_2 Depth=1
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	movsd	%xmm1, (%rdx)
	.p2align	4, 0x90
.LBB17_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_5 Depth 2
	movq	%rcx, %rbx
	leaq	-8(%rbx), %rcx
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB17_2
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	cmpq	%rcx, %rdx
	jae	.LBB17_8
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB17_2 Depth=1
	ucomisd	(%rdx), %xmm0
	jbe	.LBB17_6
	.p2align	4, 0x90
.LBB17_5:                               # %.lr.ph
                                        #   Parent Loop BB17_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ucomisd	8(%rdx), %xmm0
	leaq	8(%rdx), %rdx
	ja	.LBB17_5
.LBB17_6:                               # %._crit_edge
                                        #   in Loop: Header=BB17_2 Depth=1
	cmpq	%rcx, %rdx
	jb	.LBB17_7
.LBB17_8:
	movq	%rax, 64(%rsp)
	movq	%rbx, 48(%rsp)
	leaq	56(%rsp), %rdi
	leaq	40(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
	movq	%rbx, 32(%rsp)
	movq	8(%r14), %rax
	movq	%rax, 16(%rsp)
	leaq	24(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
.LBB17_9:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_, .Lfunc_end17-_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_,"axG",@progbits,_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_,comdat
	.weak	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_,@function
_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_: # @_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi162:
	.cfi_def_cfa_offset 96
.Lcfi163:
	.cfi_offset %rbx, -24
.Lcfi164:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	8(%rdi), %rax
	movq	8(%r14), %rcx
	movq	%rcx, %rdx
	subq	%rax, %rdx
	cmpq	$9, %rdx
	jl	.LBB18_9
# BB#1:
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movq	%rax, %rdx
	jmp	.LBB18_2
.LBB18_7:                               #   in Loop: Header=BB18_2 Depth=1
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	movsd	%xmm1, (%rdx)
	.p2align	4, 0x90
.LBB18_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_5 Depth 2
	movq	%rcx, %rbx
	leaq	-8(%rbx), %rcx
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB18_2
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	cmpq	%rcx, %rdx
	jae	.LBB18_8
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB18_2 Depth=1
	ucomisd	(%rdx), %xmm0
	jbe	.LBB18_6
	.p2align	4, 0x90
.LBB18_5:                               # %.lr.ph
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ucomisd	8(%rdx), %xmm0
	leaq	8(%rdx), %rdx
	ja	.LBB18_5
.LBB18_6:                               # %._crit_edge
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpq	%rcx, %rdx
	jb	.LBB18_7
.LBB18_8:
	movq	%rax, 64(%rsp)
	movq	%rbx, 48(%rsp)
	leaq	56(%rsp), %rdi
	leaq	40(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
	movq	%rbx, 32(%rsp)
	movq	8(%r14), %rax
	movq	%rax, 16(%rsp)
	leaq	24(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
.LBB18_9:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_, .Lfunc_end18-_ZN9benchmark9quicksortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortIPddEEvT_S2_,"axG",@progbits,_ZN9benchmark8heapsortIPddEEvT_S2_,comdat
	.weak	_ZN9benchmark8heapsortIPddEEvT_S2_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortIPddEEvT_S2_,@function
_ZN9benchmark8heapsortIPddEEvT_S2_:     # @_ZN9benchmark8heapsortIPddEEvT_S2_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 16
.Lcfi166:
	.cfi_offset %rbx, -16
	subq	%rdi, %rsi
	cmpq	$9, %rsi
	jl	.LBB19_27
# BB#1:                                 # %.lr.ph54
	movq	%rsi, %r11
	sarq	$3, %r11
	movq	%r11, %r9
	shrq	$63, %r9
	addq	%r11, %r9
	sarq	%r9
	leaq	-1(%r11), %r8
	.p2align	4, 0x90
.LBB19_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_4 Depth 2
                                        #     Child Loop BB19_10 Depth 2
	movq	%r9, %r10
	leaq	-1(%r10), %r9
	movsd	-8(%rdi,%r10,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r10,%r10), %rcx
	addq	$2, %rcx
	cmpq	%r11, %rcx
	jge	.LBB19_5
# BB#3:                                 # %.lr.ph51.i31.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB19_4:                               # %.lr.ph51.i31
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%edx, %edx
	ucomisd	-8(%rdi,%rcx,8), %xmm1
	seta	%dl
	leaq	(%rdx,%rcx), %rbx
	leaq	-1(%rdx,%rcx), %rdx
	movq	-8(%rdi,%rbx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	addq	%rbx, %rbx
	cmpq	%r11, %rbx
	movq	%rdx, %rax
	movq	%rbx, %rcx
	jl	.LBB19_4
	jmp	.LBB19_6
	.p2align	4, 0x90
.LBB19_5:                               #   in Loop: Header=BB19_2 Depth=1
	movq	%rcx, %rbx
	movq	%r9, %rdx
.LBB19_6:                               # %._crit_edge.i34
                                        #   in Loop: Header=BB19_2 Depth=1
	cmpq	%r11, %rbx
	jne	.LBB19_8
# BB#7:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	-8(%rdi,%r11,8), %rax
	movq	%rax, (%rdi,%rdx,8)
	movq	%r8, %rdx
.LBB19_8:                               # %.preheader.i36
                                        #   in Loop: Header=BB19_2 Depth=1
	cmpq	%r10, %rdx
	jl	.LBB19_12
	.p2align	4, 0x90
.LBB19_10:                              # %.lr.ph.i40
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB19_12
# BB#11:                                #   in Loop: Header=BB19_10 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r10, %rax
	movq	%rax, %rdx
	jge	.LBB19_10
	jmp	.LBB19_13
	.p2align	4, 0x90
.LBB19_12:                              #   in Loop: Header=BB19_2 Depth=1
	movq	%rdx, %rax
.LBB19_13:                              # %_ZN9benchmark7sift_inIPddEEvlT_lT0_.exit42
                                        #   in Loop: Header=BB19_2 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	cmpq	$1, %r10
	jg	.LBB19_2
# BB#14:                                # %.preheader
	cmpq	$9, %rsi
	jl	.LBB19_27
	.p2align	4, 0x90
.LBB19_15:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_17 Depth 2
                                        #     Child Loop BB19_22 Depth 2
	movq	%r11, %rdx
	leaq	-1(%rdx), %r11
	movsd	-8(%rdi,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%rdi), %rax
	movq	%rax, -8(%rdi,%rdx,8)
	cmpq	$3, %r11
	jl	.LBB19_18
# BB#16:                                # %.lr.ph51.i.preheader
                                        #   in Loop: Header=BB19_15 Depth=1
	xorl	%ecx, %ecx
	movl	$2, %eax
	.p2align	4, 0x90
.LBB19_17:                              # %.lr.ph51.i
                                        #   Parent Loop BB19_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%esi, %esi
	ucomisd	-8(%rdi,%rax,8), %xmm1
	seta	%sil
	orq	%rax, %rsi
	movq	-8(%rdi,%rsi,8), %rax
	movq	%rax, (%rdi,%rcx,8)
	leaq	-1(%rsi), %rcx
	addq	%rsi, %rsi
	cmpq	%r11, %rsi
	movq	%rsi, %rax
	jl	.LBB19_17
	jmp	.LBB19_19
	.p2align	4, 0x90
.LBB19_18:                              #   in Loop: Header=BB19_15 Depth=1
	movl	$2, %esi
	xorl	%ecx, %ecx
.LBB19_19:                              # %._crit_edge.i
                                        #   in Loop: Header=BB19_15 Depth=1
	cmpq	%r11, %rsi
	jne	.LBB19_21
# BB#20:                                #   in Loop: Header=BB19_15 Depth=1
	movq	-16(%rdi,%rdx,8), %rax
	addq	$-2, %rdx
	movq	%rax, (%rdi,%rcx,8)
	movq	%rdx, %rcx
.LBB19_21:                              # %.preheader.i
                                        #   in Loop: Header=BB19_15 Depth=1
	testq	%rcx, %rcx
	jle	.LBB19_25
	.p2align	4, 0x90
.LBB19_22:                              # %.lr.ph.i
                                        #   Parent Loop BB19_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rcx), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB19_25
# BB#23:                                #   in Loop: Header=BB19_22 Depth=2
	movsd	%xmm1, (%rdi,%rcx,8)
	cmpq	$1, %rdx
	movq	%rax, %rcx
	jg	.LBB19_22
	jmp	.LBB19_26
	.p2align	4, 0x90
.LBB19_25:                              #   in Loop: Header=BB19_15 Depth=1
	movq	%rcx, %rax
.LBB19_26:                              # %_ZN9benchmark7sift_inIPddEEvlT_lT0_.exit
                                        #   in Loop: Header=BB19_15 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	cmpq	$1, %r11
	jg	.LBB19_15
.LBB19_27:                              # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end19:
	.size	_ZN9benchmark8heapsortIPddEEvT_S2_, .Lfunc_end19-_ZN9benchmark8heapsortIPddEEvT_S2_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_,"axG",@progbits,_ZN9benchmark8heapsortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_,comdat
	.weak	_ZN9benchmark8heapsortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_,@function
_ZN9benchmark8heapsortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_: # @_ZN9benchmark8heapsortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi167:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi168:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi169:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi170:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi171:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 56
.Lcfi173:
	.cfi_offset %rbx, -56
.Lcfi174:
	.cfi_offset %r12, -48
.Lcfi175:
	.cfi_offset %r13, -40
.Lcfi176:
	.cfi_offset %r14, -32
.Lcfi177:
	.cfi_offset %r15, -24
.Lcfi178:
	.cfi_offset %rbp, -16
	subq	%rdi, %rsi
	cmpq	$9, %rsi
	jl	.LBB20_25
# BB#1:                                 # %.lr.ph100
	movq	%rsi, %r13
	sarq	$3, %r13
	movq	%r13, %r12
	shrq	$63, %r12
	addq	%r13, %r12
	sarq	%r12
	leaq	-1(%r13), %r14
	leaq	-16(%rsp), %r8
	leaq	-8(%rsp), %r9
	leaq	-32(%rsp), %r10
	leaq	-24(%rsp), %r11
	jmp	.LBB20_2
	.p2align	4, 0x90
.LBB20_12:                              # %.sink.split.i36
                                        #   in Loop: Header=BB20_2 Depth=1
	leaq	(%rdi,%rbx,8), %rbp
	movq	%rbp, (%rax)
	movq	(%rdi,%rbx,8), %rax
	leaq	(%rdi,%rcx,8), %rbp
	movq	%rbp, (%rdx)
	movq	%rax, (%rdi,%rcx,8)
	movq	%rbx, %rcx
.LBB20_13:                              #   in Loop: Header=BB20_2 Depth=1
	cmpq	%r12, %rcx
	jl	.LBB20_4
# BB#14:                                #   in Loop: Header=BB20_2 Depth=1
	leaq	-1(%rcx), %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rbx
	sarq	%rbx
	ucomisd	(%rdi,%rbx,8), %xmm0
	jbe	.LBB20_4
# BB#15:                                #   in Loop: Header=BB20_2 Depth=1
	movq	%r11, %rax
	movq	%r10, %rdx
	jmp	.LBB20_12
	.p2align	4, 0x90
.LBB20_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_10 Depth 2
	leaq	-1(%r12), %r15
	movsd	-8(%rdi,%r12,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r12,%r12), %rdx
	addq	$2, %rdx
	cmpq	%r13, %rdx
	jge	.LBB20_3
# BB#9:                                 # %.lr.ph.i28.preheader
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB20_10:                              # %.lr.ph.i28
                                        #   Parent Loop BB20_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	-8(%rdi,%rdx,8), %xmm1
	seta	%cl
	leaq	(%rcx,%rdx), %rbx
	leaq	-1(%rcx,%rdx), %rcx
	movq	-8(%rdi,%rbx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	addq	%rbx, %rbx
	cmpq	%r13, %rbx
	movq	%rcx, %rax
	movq	%rbx, %rdx
	jl	.LBB20_10
	jmp	.LBB20_11
	.p2align	4, 0x90
.LBB20_3:                               #   in Loop: Header=BB20_2 Depth=1
	movq	%rdx, %rbx
	movq	%r15, %rcx
.LBB20_11:                              # %._crit_edge.i31
                                        #   in Loop: Header=BB20_2 Depth=1
	cmpq	%r13, %rbx
	movq	%r14, %rbx
	movq	%r9, %rax
	movq	%r8, %rdx
	je	.LBB20_12
	jmp	.LBB20_13
	.p2align	4, 0x90
.LBB20_4:                               # %_ZN9benchmark7sift_inIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvlT_lT0_.exit40
                                        #   in Loop: Header=BB20_2 Depth=1
	movsd	%xmm0, (%rdi,%rcx,8)
	cmpq	$1, %r12
	movq	%r15, %r12
	jg	.LBB20_2
# BB#5:                                 # %.preheader
	cmpq	$9, %rsi
	jl	.LBB20_25
# BB#6:
	leaq	-16(%rsp), %r10
	leaq	-8(%rsp), %rbx
	leaq	-32(%rsp), %r8
	leaq	-24(%rsp), %r9
	.p2align	4, 0x90
.LBB20_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_17 Depth 2
	leaq	-1(%r13), %rsi
	movsd	-8(%rdi,%r13,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%rdi), %rax
	movq	%rax, -8(%rdi,%r13,8)
	cmpq	$3, %rsi
	jl	.LBB20_8
# BB#16:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB20_7 Depth=1
	xorl	%eax, %eax
	movl	$2, %edx
	.p2align	4, 0x90
.LBB20_17:                              # %.lr.ph.i
                                        #   Parent Loop BB20_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	-8(%rdi,%rdx,8), %xmm1
	seta	%cl
	orq	%rdx, %rcx
	movq	-8(%rdi,%rcx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	leaq	-1(%rcx), %rax
	addq	%rcx, %rcx
	cmpq	%rsi, %rcx
	movq	%rcx, %rdx
	jl	.LBB20_17
	jmp	.LBB20_18
	.p2align	4, 0x90
.LBB20_8:                               #   in Loop: Header=BB20_7 Depth=1
	movl	$2, %ecx
	xorl	%eax, %eax
.LBB20_18:                              # %._crit_edge.i
                                        #   in Loop: Header=BB20_7 Depth=1
	cmpq	%rsi, %rcx
	jne	.LBB20_21
# BB#19:                                #   in Loop: Header=BB20_7 Depth=1
	addq	$-2, %r13
	movq	%rbx, %rcx
	movq	%r10, %rdx
.LBB20_20:                              # %.sink.split.i
                                        #   in Loop: Header=BB20_7 Depth=1
	leaq	(%rdi,%r13,8), %rbp
	movq	%rbp, (%rcx)
	movq	(%rdi,%r13,8), %rcx
	leaq	(%rdi,%rax,8), %rbp
	movq	%rbp, (%rdx)
	movq	%rcx, (%rdi,%rax,8)
	movq	%r13, %rax
.LBB20_21:                              #   in Loop: Header=BB20_7 Depth=1
	testq	%rax, %rax
	jle	.LBB20_24
# BB#22:                                #   in Loop: Header=BB20_7 Depth=1
	leaq	-1(%rax), %rcx
	shrq	$63, %rcx
	leaq	-1(%rax,%rcx), %r13
	sarq	%r13
	ucomisd	(%rdi,%r13,8), %xmm0
	jbe	.LBB20_24
# BB#23:                                #   in Loop: Header=BB20_7 Depth=1
	movq	%r9, %rcx
	movq	%r8, %rdx
	jmp	.LBB20_20
	.p2align	4, 0x90
.LBB20_24:                              # %_ZN9benchmark7sift_inIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvlT_lT0_.exit
                                        #   in Loop: Header=BB20_7 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	cmpq	$1, %rsi
	movq	%rsi, %r13
	jg	.LBB20_7
.LBB20_25:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZN9benchmark8heapsortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_, .Lfunc_end20-_ZN9benchmark8heapsortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdEEvT_S8_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortISt16reverse_iteratorIPdEdEEvT_S4_,"axG",@progbits,_ZN9benchmark8heapsortISt16reverse_iteratorIPdEdEEvT_S4_,comdat
	.weak	_ZN9benchmark8heapsortISt16reverse_iteratorIPdEdEEvT_S4_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortISt16reverse_iteratorIPdEdEEvT_S4_,@function
_ZN9benchmark8heapsortISt16reverse_iteratorIPdEdEEvT_S4_: # @_ZN9benchmark8heapsortISt16reverse_iteratorIPdEdEEvT_S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi179:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi180:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi181:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 40
.Lcfi183:
	.cfi_offset %rbx, -40
.Lcfi184:
	.cfi_offset %r12, -32
.Lcfi185:
	.cfi_offset %r14, -24
.Lcfi186:
	.cfi_offset %r15, -16
	movq	(%rdi), %r12
	movq	%r12, %r8
	subq	(%rsi), %r8
	cmpq	$9, %r8
	jl	.LBB21_26
# BB#1:                                 # %.lr.ph55
	movq	%r8, %r15
	sarq	$3, %r15
	movq	%r15, %r14
	shrq	$63, %r14
	addq	%r15, %r14
	sarq	%r14
	leaq	-1(%r15), %r9
	movl	$1, %r10d
	subq	%r15, %r10
	jmp	.LBB21_2
	.p2align	4, 0x90
.LBB21_13:                              # %.sink.split.i31
                                        #   in Loop: Header=BB21_2 Depth=1
	shlq	$3, %rcx
	movq	%r12, %rdx
	subq	%rcx, %rdx
	movq	%rax, -8(%rdx)
	movq	%rsi, %rcx
.LBB21_14:                              #   in Loop: Header=BB21_2 Depth=1
	cmpq	%r14, %rcx
	jl	.LBB21_4
# BB#15:                                #   in Loop: Header=BB21_2 Depth=1
	leaq	-1(%rcx), %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rsi
	sarq	%rsi
	leaq	(,%rsi,8), %rax
	movq	%r12, %rdx
	subq	%rax, %rdx
	movq	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB21_4
# BB#16:                                #   in Loop: Header=BB21_2 Depth=1
	movd	%xmm1, %rax
	jmp	.LBB21_13
	.p2align	4, 0x90
.LBB21_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_10 Depth 2
	leaq	-1(%r14), %r11
	movl	$1, %eax
	subq	%r14, %rax
	movsd	-8(%r12,%rax,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r14,%r14), %rax
	addq	$2, %rax
	cmpq	%r15, %rax
	jge	.LBB21_3
# BB#9:                                 # %.lr.ph.i20.preheader
                                        #   in Loop: Header=BB21_2 Depth=1
	movq	%r11, %rbx
	.p2align	4, 0x90
.LBB21_10:                              # %.lr.ph.i20
                                        #   Parent Loop BB21_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %ecx
	subq	%rax, %rcx
	leaq	(,%rax,8), %rsi
	negq	%rsi
	movq	-8(%r12,%rsi), %xmm1    # xmm1 = mem[0],zero
	xorl	%edx, %edx
	ucomisd	-8(%r12,%rcx,8), %xmm1
	seta	%dl
	leaq	(%rdx,%rax), %rsi
	leaq	-1(%rdx,%rax), %rcx
	movl	$1, %eax
	subq	%rsi, %rax
	movq	-8(%r12,%rax,8), %rax
	shlq	$3, %rbx
	negq	%rbx
	movq	%rax, -8(%r12,%rbx)
	addq	%rsi, %rsi
	cmpq	%r15, %rsi
	movq	%rcx, %rbx
	movq	%rsi, %rax
	jl	.LBB21_10
	jmp	.LBB21_11
	.p2align	4, 0x90
.LBB21_3:                               #   in Loop: Header=BB21_2 Depth=1
	movq	%rax, %rsi
	movq	%r11, %rcx
.LBB21_11:                              # %._crit_edge.i26
                                        #   in Loop: Header=BB21_2 Depth=1
	cmpq	%r15, %rsi
	jne	.LBB21_14
# BB#12:                                #   in Loop: Header=BB21_2 Depth=1
	movq	-8(%r12,%r10,8), %rax
	movq	%r9, %rsi
	jmp	.LBB21_13
	.p2align	4, 0x90
.LBB21_4:                               # %_ZN9benchmark7sift_inISt16reverse_iteratorIPdEdEEvlT_lT0_.exit35
                                        #   in Loop: Header=BB21_2 Depth=1
	shlq	$3, %rcx
	movq	%r12, %rax
	subq	%rcx, %rax
	movsd	%xmm0, -8(%rax)
	cmpq	$1, %r14
	movq	%r11, %r14
	jg	.LBB21_2
# BB#5:                                 # %.preheader
	cmpq	$9, %r8
	jl	.LBB21_26
# BB#6:                                 # %.lr.ph.preheader
	movq	(%rdi), %rcx
	.p2align	4, 0x90
.LBB21_7:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_18 Depth 2
	leaq	-1(%r15), %rdx
	movl	$1, %eax
	subq	%r15, %rax
	movsd	-8(%rcx,%rax,8), %xmm0  # xmm0 = mem[0],zero
	movq	-8(%rcx), %rsi
	movq	%rsi, -8(%rcx,%rax,8)
	cmpq	$3, %rdx
	jl	.LBB21_8
# BB#17:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB21_7 Depth=1
	xorl	%esi, %esi
	movl	$2, %eax
	.p2align	4, 0x90
.LBB21_18:                              # %.lr.ph.i
                                        #   Parent Loop BB21_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %ebx
	subq	%rax, %rbx
	leaq	(,%rax,8), %rdi
	negq	%rdi
	movq	-8(%rcx,%rdi), %xmm1    # xmm1 = mem[0],zero
	xorl	%edi, %edi
	ucomisd	-8(%rcx,%rbx,8), %xmm1
	seta	%dil
	orq	%rax, %rdi
	movl	$1, %eax
	subq	%rdi, %rax
	movq	-8(%rcx,%rax,8), %rax
	shlq	$3, %rsi
	negq	%rsi
	movq	%rax, -8(%rcx,%rsi)
	leaq	-1(%rdi), %rsi
	addq	%rdi, %rdi
	cmpq	%rdx, %rdi
	movq	%rdi, %rax
	jl	.LBB21_18
	jmp	.LBB21_19
	.p2align	4, 0x90
.LBB21_8:                               #   in Loop: Header=BB21_7 Depth=1
	movl	$2, %edi
	xorl	%esi, %esi
.LBB21_19:                              # %._crit_edge.i
                                        #   in Loop: Header=BB21_7 Depth=1
	cmpq	%rdx, %rdi
	jne	.LBB21_22
# BB#20:                                #   in Loop: Header=BB21_7 Depth=1
	movl	$2, %eax
	subq	%r15, %rax
	addq	$-2, %r15
	movq	-8(%rcx,%rax,8), %rax
.LBB21_21:                              # %.sink.split.i
                                        #   in Loop: Header=BB21_7 Depth=1
	shlq	$3, %rsi
	movq	%rcx, %rdi
	subq	%rsi, %rdi
	movq	%rax, -8(%rdi)
	movq	%r15, %rsi
.LBB21_22:                              #   in Loop: Header=BB21_7 Depth=1
	testq	%rsi, %rsi
	jle	.LBB21_25
# BB#23:                                #   in Loop: Header=BB21_7 Depth=1
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %r15
	sarq	%r15
	leaq	(,%r15,8), %rax
	movq	%rcx, %rdi
	subq	%rax, %rdi
	movq	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB21_25
# BB#24:                                #   in Loop: Header=BB21_7 Depth=1
	movd	%xmm1, %rax
	jmp	.LBB21_21
	.p2align	4, 0x90
.LBB21_25:                              # %_ZN9benchmark7sift_inISt16reverse_iteratorIPdEdEEvlT_lT0_.exit
                                        #   in Loop: Header=BB21_7 Depth=1
	shlq	$3, %rsi
	movq	%rcx, %rax
	subq	%rsi, %rax
	movsd	%xmm0, -8(%rax)
	cmpq	$1, %rdx
	movq	%rdx, %r15
	jg	.LBB21_7
.LBB21_26:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	_ZN9benchmark8heapsortISt16reverse_iteratorIPdEdEEvT_S4_, .Lfunc_end21-_ZN9benchmark8heapsortISt16reverse_iteratorIPdEdEEvT_S4_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_,"axG",@progbits,_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_,comdat
	.weak	_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_,@function
_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_: # @_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi187:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi188:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi189:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi190:
	.cfi_def_cfa_offset 40
.Lcfi191:
	.cfi_offset %rbx, -40
.Lcfi192:
	.cfi_offset %r12, -32
.Lcfi193:
	.cfi_offset %r14, -24
.Lcfi194:
	.cfi_offset %r15, -16
	movq	(%rdi), %r12
	movq	%r12, %r8
	subq	(%rsi), %r8
	cmpq	$9, %r8
	jl	.LBB22_26
# BB#1:                                 # %.lr.ph58
	movq	%r8, %r15
	sarq	$3, %r15
	movq	%r15, %r14
	shrq	$63, %r14
	addq	%r15, %r14
	sarq	%r14
	leaq	-1(%r15), %r9
	movl	$1, %r10d
	subq	%r15, %r10
	jmp	.LBB22_2
	.p2align	4, 0x90
.LBB22_13:                              # %.sink.split.i33
                                        #   in Loop: Header=BB22_2 Depth=1
	shlq	$3, %rcx
	movq	%r12, %rdx
	subq	%rcx, %rdx
	movq	%rax, -8(%rdx)
	movq	%rsi, %rcx
.LBB22_14:                              #   in Loop: Header=BB22_2 Depth=1
	cmpq	%r14, %rcx
	jl	.LBB22_4
# BB#15:                                #   in Loop: Header=BB22_2 Depth=1
	leaq	-1(%rcx), %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rsi
	sarq	%rsi
	leaq	(,%rsi,8), %rax
	movq	%r12, %rdx
	subq	%rax, %rdx
	movq	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB22_4
# BB#16:                                #   in Loop: Header=BB22_2 Depth=1
	movd	%xmm1, %rax
	jmp	.LBB22_13
	.p2align	4, 0x90
.LBB22_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_10 Depth 2
	leaq	-1(%r14), %r11
	movl	$1, %eax
	subq	%r14, %rax
	movsd	-8(%r12,%rax,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r14,%r14), %rax
	addq	$2, %rax
	cmpq	%r15, %rax
	jge	.LBB22_3
# BB#9:                                 # %.lr.ph.i22.preheader
                                        #   in Loop: Header=BB22_2 Depth=1
	movq	%r11, %rbx
	.p2align	4, 0x90
.LBB22_10:                              # %.lr.ph.i22
                                        #   Parent Loop BB22_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %ecx
	subq	%rax, %rcx
	leaq	(,%rax,8), %rsi
	negq	%rsi
	movq	-8(%r12,%rsi), %xmm1    # xmm1 = mem[0],zero
	xorl	%edx, %edx
	ucomisd	-8(%r12,%rcx,8), %xmm1
	seta	%dl
	leaq	(%rdx,%rax), %rsi
	leaq	-1(%rdx,%rax), %rcx
	movl	$1, %eax
	subq	%rsi, %rax
	movq	-8(%r12,%rax,8), %rax
	shlq	$3, %rbx
	negq	%rbx
	movq	%rax, -8(%r12,%rbx)
	addq	%rsi, %rsi
	cmpq	%r15, %rsi
	movq	%rcx, %rbx
	movq	%rsi, %rax
	jl	.LBB22_10
	jmp	.LBB22_11
	.p2align	4, 0x90
.LBB22_3:                               #   in Loop: Header=BB22_2 Depth=1
	movq	%rax, %rsi
	movq	%r11, %rcx
.LBB22_11:                              # %._crit_edge.i28
                                        #   in Loop: Header=BB22_2 Depth=1
	cmpq	%r15, %rsi
	jne	.LBB22_14
# BB#12:                                #   in Loop: Header=BB22_2 Depth=1
	movq	-8(%r12,%r10,8), %rax
	movq	%r9, %rsi
	jmp	.LBB22_13
	.p2align	4, 0x90
.LBB22_4:                               # %_ZN9benchmark7sift_inISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvlT_lT0_.exit37
                                        #   in Loop: Header=BB22_2 Depth=1
	shlq	$3, %rcx
	movq	%r12, %rax
	subq	%rcx, %rax
	movsd	%xmm0, -8(%rax)
	cmpq	$1, %r14
	movq	%r11, %r14
	jg	.LBB22_2
# BB#5:                                 # %.preheader
	cmpq	$9, %r8
	jl	.LBB22_26
# BB#6:                                 # %.lr.ph.preheader
	movq	(%rdi), %rcx
	.p2align	4, 0x90
.LBB22_7:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_18 Depth 2
	leaq	-1(%r15), %rdx
	movl	$1, %eax
	subq	%r15, %rax
	movsd	-8(%rcx,%rax,8), %xmm0  # xmm0 = mem[0],zero
	movq	-8(%rcx), %rsi
	movq	%rsi, -8(%rcx,%rax,8)
	cmpq	$3, %rdx
	jl	.LBB22_8
# BB#17:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB22_7 Depth=1
	xorl	%esi, %esi
	movl	$2, %eax
	.p2align	4, 0x90
.LBB22_18:                              # %.lr.ph.i
                                        #   Parent Loop BB22_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %ebx
	subq	%rax, %rbx
	leaq	(,%rax,8), %rdi
	negq	%rdi
	movq	-8(%rcx,%rdi), %xmm1    # xmm1 = mem[0],zero
	xorl	%edi, %edi
	ucomisd	-8(%rcx,%rbx,8), %xmm1
	seta	%dil
	orq	%rax, %rdi
	movl	$1, %eax
	subq	%rdi, %rax
	movq	-8(%rcx,%rax,8), %rax
	shlq	$3, %rsi
	negq	%rsi
	movq	%rax, -8(%rcx,%rsi)
	leaq	-1(%rdi), %rsi
	addq	%rdi, %rdi
	cmpq	%rdx, %rdi
	movq	%rdi, %rax
	jl	.LBB22_18
	jmp	.LBB22_19
	.p2align	4, 0x90
.LBB22_8:                               #   in Loop: Header=BB22_7 Depth=1
	movl	$2, %edi
	xorl	%esi, %esi
.LBB22_19:                              # %._crit_edge.i
                                        #   in Loop: Header=BB22_7 Depth=1
	cmpq	%rdx, %rdi
	jne	.LBB22_22
# BB#20:                                #   in Loop: Header=BB22_7 Depth=1
	movl	$2, %eax
	subq	%r15, %rax
	addq	$-2, %r15
	movq	-8(%rcx,%rax,8), %rax
.LBB22_21:                              # %.sink.split.i
                                        #   in Loop: Header=BB22_7 Depth=1
	shlq	$3, %rsi
	movq	%rcx, %rdi
	subq	%rsi, %rdi
	movq	%rax, -8(%rdi)
	movq	%r15, %rsi
.LBB22_22:                              #   in Loop: Header=BB22_7 Depth=1
	testq	%rsi, %rsi
	jle	.LBB22_25
# BB#23:                                #   in Loop: Header=BB22_7 Depth=1
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %r15
	sarq	%r15
	leaq	(,%r15,8), %rax
	movq	%rcx, %rdi
	subq	%rax, %rdi
	movq	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB22_25
# BB#24:                                #   in Loop: Header=BB22_7 Depth=1
	movd	%xmm1, %rax
	jmp	.LBB22_21
	.p2align	4, 0x90
.LBB22_25:                              # %_ZN9benchmark7sift_inISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvlT_lT0_.exit
                                        #   in Loop: Header=BB22_7 Depth=1
	shlq	$3, %rsi
	movq	%rcx, %rax
	subq	%rsi, %rax
	movsd	%xmm0, -8(%rax)
	cmpq	$1, %rdx
	movq	%rdx, %r15
	jg	.LBB22_7
.LBB22_26:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_, .Lfunc_end22-_ZN9benchmark8heapsortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEdEEvT_SA_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_,"axG",@progbits,_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_,comdat
	.weak	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_,@function
_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_: # @_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi195:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi196:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi197:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi198:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi199:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi200:
	.cfi_def_cfa_offset 56
.Lcfi201:
	.cfi_offset %rbx, -56
.Lcfi202:
	.cfi_offset %r12, -48
.Lcfi203:
	.cfi_offset %r13, -40
.Lcfi204:
	.cfi_offset %r14, -32
.Lcfi205:
	.cfi_offset %r15, -24
.Lcfi206:
	.cfi_offset %rbp, -16
	movq	8(%rdi), %rdx
	movq	8(%rsi), %r8
	subq	%rdx, %r8
	cmpq	$9, %r8
	jl	.LBB23_25
# BB#1:                                 # %.lr.ph57
	movq	%r8, %r13
	sarq	$3, %r13
	movq	%r13, %r12
	shrq	$63, %r12
	addq	%r13, %r12
	sarq	%r12
	leaq	-1(%r13), %r9
	leaq	-8(%rdx,%r13,8), %r14
	leaq	-16(%rsp), %r10
	leaq	-32(%rsp), %r11
	jmp	.LBB23_2
	.p2align	4, 0x90
.LBB23_12:                              # %.sink.split.i33
                                        #   in Loop: Header=BB23_2 Depth=1
	leaq	(%rdx,%rsi,8), %rbp
	movq	(%rbx), %rbx
	movq	%rbp, 8(%rcx)
	movq	%rbx, (%rdx,%rsi,8)
	movq	%rax, %rsi
.LBB23_13:                              #   in Loop: Header=BB23_2 Depth=1
	cmpq	%r12, %rsi
	jl	.LBB23_4
# BB#14:                                #   in Loop: Header=BB23_2 Depth=1
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %rax
	sarq	%rax
	ucomisd	(%rdx,%rax,8), %xmm0
	jbe	.LBB23_4
# BB#15:                                #   in Loop: Header=BB23_2 Depth=1
	leaq	(%rdx,%rax,8), %rbx
	movq	%r11, %rcx
	jmp	.LBB23_12
	.p2align	4, 0x90
.LBB23_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_10 Depth 2
	leaq	-1(%r12), %r15
	movsd	-8(%rdx,%r12,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r12,%r12), %rbx
	addq	$2, %rbx
	cmpq	%r13, %rbx
	jge	.LBB23_3
# BB#9:                                 # %.lr.ph.i20.preheader
                                        #   in Loop: Header=BB23_2 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB23_10:                              # %.lr.ph.i20
                                        #   Parent Loop BB23_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%esi, %esi
	ucomisd	-8(%rdx,%rbx,8), %xmm1
	seta	%sil
	leaq	(%rsi,%rbx), %rax
	leaq	-1(%rsi,%rbx), %rsi
	movq	-8(%rdx,%rax,8), %rbx
	movq	%rbx, (%rdx,%rcx,8)
	addq	%rax, %rax
	cmpq	%r13, %rax
	movq	%rsi, %rcx
	movq	%rax, %rbx
	jl	.LBB23_10
	jmp	.LBB23_11
	.p2align	4, 0x90
.LBB23_3:                               #   in Loop: Header=BB23_2 Depth=1
	movq	%rbx, %rax
	movq	%r15, %rsi
.LBB23_11:                              # %._crit_edge.i26
                                        #   in Loop: Header=BB23_2 Depth=1
	cmpq	%r13, %rax
	movq	%r10, %rcx
	movq	%r14, %rbx
	movq	%r9, %rax
	je	.LBB23_12
	jmp	.LBB23_13
	.p2align	4, 0x90
.LBB23_4:                               # %_ZN9benchmark7sift_inISt16reverse_iteratorIS1_IPdEEdEEvlT_lT0_.exit37
                                        #   in Loop: Header=BB23_2 Depth=1
	movsd	%xmm0, (%rdx,%rsi,8)
	cmpq	$1, %r12
	movq	%r15, %r12
	jg	.LBB23_2
# BB#5:                                 # %.preheader
	cmpq	$9, %r8
	jl	.LBB23_25
# BB#6:                                 # %.lr.ph
	movq	8(%rdi), %rcx
	leaq	-16(%rsp), %r9
	leaq	-32(%rsp), %r8
	.p2align	4, 0x90
.LBB23_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_17 Depth 2
	leaq	-1(%r13), %rdi
	movsd	-8(%rcx,%r13,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%rcx), %rax
	movq	%rax, -8(%rcx,%r13,8)
	cmpq	$3, %rdi
	jl	.LBB23_8
# BB#16:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB23_7 Depth=1
	xorl	%eax, %eax
	movl	$2, %ebx
	.p2align	4, 0x90
.LBB23_17:                              # %.lr.ph.i
                                        #   Parent Loop BB23_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rcx,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%esi, %esi
	ucomisd	-8(%rcx,%rbx,8), %xmm1
	seta	%sil
	orq	%rbx, %rsi
	movq	-8(%rcx,%rsi,8), %rdx
	movq	%rdx, (%rcx,%rax,8)
	leaq	-1(%rsi), %rax
	addq	%rsi, %rsi
	cmpq	%rdi, %rsi
	movq	%rsi, %rbx
	jl	.LBB23_17
	jmp	.LBB23_18
	.p2align	4, 0x90
.LBB23_8:                               #   in Loop: Header=BB23_7 Depth=1
	movl	$2, %esi
	xorl	%eax, %eax
.LBB23_18:                              # %._crit_edge.i
                                        #   in Loop: Header=BB23_7 Depth=1
	cmpq	%rdi, %rsi
	jne	.LBB23_21
# BB#19:                                #   in Loop: Header=BB23_7 Depth=1
	leaq	-16(%rcx,%r13,8), %rsi
	addq	$-2, %r13
	movq	%r9, %rdx
.LBB23_20:                              # %.sink.split.i
                                        #   in Loop: Header=BB23_7 Depth=1
	leaq	(%rcx,%rax,8), %rbp
	movq	(%rsi), %rsi
	movq	%rbp, 8(%rdx)
	movq	%rsi, (%rcx,%rax,8)
	movq	%r13, %rax
.LBB23_21:                              #   in Loop: Header=BB23_7 Depth=1
	testq	%rax, %rax
	jle	.LBB23_24
# BB#22:                                #   in Loop: Header=BB23_7 Depth=1
	leaq	-1(%rax), %rdx
	shrq	$63, %rdx
	leaq	-1(%rax,%rdx), %r13
	sarq	%r13
	ucomisd	(%rcx,%r13,8), %xmm0
	jbe	.LBB23_24
# BB#23:                                #   in Loop: Header=BB23_7 Depth=1
	leaq	(%rcx,%r13,8), %rsi
	movq	%r8, %rdx
	jmp	.LBB23_20
	.p2align	4, 0x90
.LBB23_24:                              # %_ZN9benchmark7sift_inISt16reverse_iteratorIS1_IPdEEdEEvlT_lT0_.exit
                                        #   in Loop: Header=BB23_7 Depth=1
	movsd	%xmm0, (%rcx,%rax,8)
	cmpq	$1, %rdi
	movq	%rdi, %r13
	jg	.LBB23_7
.LBB23_25:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_, .Lfunc_end23-_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IPdEEdEEvT_S5_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_,"axG",@progbits,_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_,comdat
	.weak	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_,@function
_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_: # @_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi210:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi211:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi212:
	.cfi_def_cfa_offset 56
.Lcfi213:
	.cfi_offset %rbx, -56
.Lcfi214:
	.cfi_offset %r12, -48
.Lcfi215:
	.cfi_offset %r13, -40
.Lcfi216:
	.cfi_offset %r14, -32
.Lcfi217:
	.cfi_offset %r15, -24
.Lcfi218:
	.cfi_offset %rbp, -16
	movq	8(%rdi), %rdx
	movq	8(%rsi), %r8
	subq	%rdx, %r8
	cmpq	$9, %r8
	jl	.LBB24_25
# BB#1:                                 # %.lr.ph57
	movq	%r8, %r13
	sarq	$3, %r13
	movq	%r13, %r12
	shrq	$63, %r12
	addq	%r13, %r12
	sarq	%r12
	leaq	-1(%r13), %r9
	leaq	-8(%rdx,%r13,8), %r14
	leaq	-16(%rsp), %r10
	leaq	-32(%rsp), %r11
	jmp	.LBB24_2
	.p2align	4, 0x90
.LBB24_12:                              # %.sink.split.i33
                                        #   in Loop: Header=BB24_2 Depth=1
	leaq	(%rdx,%rsi,8), %rbp
	movq	(%rbx), %rbx
	movq	%rbp, 8(%rcx)
	movq	%rbx, (%rdx,%rsi,8)
	movq	%rax, %rsi
.LBB24_13:                              #   in Loop: Header=BB24_2 Depth=1
	cmpq	%r12, %rsi
	jl	.LBB24_4
# BB#14:                                #   in Loop: Header=BB24_2 Depth=1
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %rax
	sarq	%rax
	ucomisd	(%rdx,%rax,8), %xmm0
	jbe	.LBB24_4
# BB#15:                                #   in Loop: Header=BB24_2 Depth=1
	leaq	(%rdx,%rax,8), %rbx
	movq	%r11, %rcx
	jmp	.LBB24_12
	.p2align	4, 0x90
.LBB24_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_10 Depth 2
	leaq	-1(%r12), %r15
	movsd	-8(%rdx,%r12,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r12,%r12), %rbx
	addq	$2, %rbx
	cmpq	%r13, %rbx
	jge	.LBB24_3
# BB#9:                                 # %.lr.ph.i20.preheader
                                        #   in Loop: Header=BB24_2 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB24_10:                              # %.lr.ph.i20
                                        #   Parent Loop BB24_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%esi, %esi
	ucomisd	-8(%rdx,%rbx,8), %xmm1
	seta	%sil
	leaq	(%rsi,%rbx), %rax
	leaq	-1(%rsi,%rbx), %rsi
	movq	-8(%rdx,%rax,8), %rbx
	movq	%rbx, (%rdx,%rcx,8)
	addq	%rax, %rax
	cmpq	%r13, %rax
	movq	%rsi, %rcx
	movq	%rax, %rbx
	jl	.LBB24_10
	jmp	.LBB24_11
	.p2align	4, 0x90
.LBB24_3:                               #   in Loop: Header=BB24_2 Depth=1
	movq	%rbx, %rax
	movq	%r15, %rsi
.LBB24_11:                              # %._crit_edge.i26
                                        #   in Loop: Header=BB24_2 Depth=1
	cmpq	%r13, %rax
	movq	%r10, %rcx
	movq	%r14, %rbx
	movq	%r9, %rax
	je	.LBB24_12
	jmp	.LBB24_13
	.p2align	4, 0x90
.LBB24_4:                               # %_ZN9benchmark7sift_inISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvlT_lT0_.exit37
                                        #   in Loop: Header=BB24_2 Depth=1
	movsd	%xmm0, (%rdx,%rsi,8)
	cmpq	$1, %r12
	movq	%r15, %r12
	jg	.LBB24_2
# BB#5:                                 # %.preheader
	cmpq	$9, %r8
	jl	.LBB24_25
# BB#6:                                 # %.lr.ph
	movq	8(%rdi), %rcx
	leaq	-16(%rsp), %r9
	leaq	-32(%rsp), %r8
	.p2align	4, 0x90
.LBB24_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_17 Depth 2
	leaq	-1(%r13), %rdi
	movsd	-8(%rcx,%r13,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%rcx), %rax
	movq	%rax, -8(%rcx,%r13,8)
	cmpq	$3, %rdi
	jl	.LBB24_8
# BB#16:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB24_7 Depth=1
	xorl	%eax, %eax
	movl	$2, %ebx
	.p2align	4, 0x90
.LBB24_17:                              # %.lr.ph.i
                                        #   Parent Loop BB24_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rcx,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%esi, %esi
	ucomisd	-8(%rcx,%rbx,8), %xmm1
	seta	%sil
	orq	%rbx, %rsi
	movq	-8(%rcx,%rsi,8), %rdx
	movq	%rdx, (%rcx,%rax,8)
	leaq	-1(%rsi), %rax
	addq	%rsi, %rsi
	cmpq	%rdi, %rsi
	movq	%rsi, %rbx
	jl	.LBB24_17
	jmp	.LBB24_18
	.p2align	4, 0x90
.LBB24_8:                               #   in Loop: Header=BB24_7 Depth=1
	movl	$2, %esi
	xorl	%eax, %eax
.LBB24_18:                              # %._crit_edge.i
                                        #   in Loop: Header=BB24_7 Depth=1
	cmpq	%rdi, %rsi
	jne	.LBB24_21
# BB#19:                                #   in Loop: Header=BB24_7 Depth=1
	leaq	-16(%rcx,%r13,8), %rsi
	addq	$-2, %r13
	movq	%r9, %rdx
.LBB24_20:                              # %.sink.split.i
                                        #   in Loop: Header=BB24_7 Depth=1
	leaq	(%rcx,%rax,8), %rbp
	movq	(%rsi), %rsi
	movq	%rbp, 8(%rdx)
	movq	%rsi, (%rcx,%rax,8)
	movq	%r13, %rax
.LBB24_21:                              #   in Loop: Header=BB24_7 Depth=1
	testq	%rax, %rax
	jle	.LBB24_24
# BB#22:                                #   in Loop: Header=BB24_7 Depth=1
	leaq	-1(%rax), %rdx
	shrq	$63, %rdx
	leaq	-1(%rax,%rdx), %r13
	sarq	%r13
	ucomisd	(%rcx,%r13,8), %xmm0
	jbe	.LBB24_24
# BB#23:                                #   in Loop: Header=BB24_7 Depth=1
	leaq	(%rcx,%r13,8), %rsi
	movq	%r8, %rdx
	jmp	.LBB24_20
	.p2align	4, 0x90
.LBB24_24:                              # %_ZN9benchmark7sift_inISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvlT_lT0_.exit
                                        #   in Loop: Header=BB24_7 Depth=1
	movsd	%xmm0, (%rcx,%rax,8)
	cmpq	$1, %rdi
	movq	%rdi, %r13
	jg	.LBB24_7
.LBB24_25:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_, .Lfunc_end24-_ZN9benchmark8heapsortISt16reverse_iteratorIS1_IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEEdEEvT_SB_
	.cfi_endproc

	.type	results,@object         # @results
	.bss
	.globl	results
	.p2align	3
results:
	.quad	0
	.size	results, 8

	.type	current_test,@object    # @current_test
	.globl	current_test
	.p2align	2
current_test:
	.long	0                       # 0x0
	.size	current_test, 4

	.type	allocated_results,@object # @allocated_results
	.globl	allocated_results
	.p2align	2
allocated_results:
	.long	0                       # 0x0
	.size	allocated_results, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Could not allocate %d results\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\ntest %*s description   absolute   operations   ratio with\n"
	.size	.L.str.1, 60

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" "
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"number %*s time       per second   test0\n\n"
	.size	.L.str.3, 43

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%2i %*s\"%s\"  %5.2f sec   %5.2f M     %.2f\n"
	.size	.L.str.4, 43

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.zero	1
	.size	.L.str.5, 1

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\nTotal absolute time for %s: %.2f sec\n"
	.size	.L.str.6, 39

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n%s Penalty: %.2f\n\n"
	.size	.L.str.7, 20

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\ntest %*s description   absolute\n"
	.size	.L.str.8, 34

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"number %*s time\n\n"
	.size	.L.str.9, 18

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%2i %*s\"%s\"  %5.2f sec\n"
	.size	.L.str.10, 24

	.type	start_time,@object      # @start_time
	.bss
	.globl	start_time
	.p2align	3
start_time:
	.quad	0                       # 0x0
	.size	start_time, 8

	.type	end_time,@object        # @end_time
	.globl	end_time
	.p2align	3
end_time:
	.quad	0                       # 0x0
	.size	end_time, 8

	.type	iterations,@object      # @iterations
	.data
	.globl	iterations
	.p2align	2
iterations:
	.long	60000                   # 0xea60
	.size	iterations, 4

	.type	init_value,@object      # @init_value
	.globl	init_value
	.p2align	3
init_value:
	.quad	4613937818241073152     # double 3
	.size	init_value, 8

	.type	data,@object            # @data
	.bss
	.globl	data
	.p2align	4
data:
	.zero	16000
	.size	data, 16000

	.type	dataMaster,@object      # @dataMaster
	.globl	dataMaster
	.p2align	4
dataMaster:
	.zero	16000
	.size	dataMaster, 16000

	.type	dpb,@object             # @dpb
	.data
	.globl	dpb
	.p2align	3
dpb:
	.quad	data
	.size	dpb, 8

	.type	dpe,@object             # @dpe
	.globl	dpe
	.p2align	3
dpe:
	.quad	data+16000
	.size	dpe, 8

	.type	dMpb,@object            # @dMpb
	.globl	dMpb
	.p2align	3
dMpb:
	.quad	dataMaster
	.size	dMpb, 8

	.type	dMpe,@object            # @dMpe
	.globl	dMpe
	.p2align	3
dMpe:
	.quad	dataMaster+16000
	.size	dMpe, 8

	.type	rdpb,@object            # @rdpb
	.globl	rdpb
	.p2align	3
rdpb:
	.quad	data+16000
	.size	rdpb, 8

	.type	rdpe,@object            # @rdpe
	.globl	rdpe
	.p2align	3
rdpe:
	.quad	data
	.size	rdpe, 8

	.type	rdMpb,@object           # @rdMpb
	.globl	rdMpb
	.p2align	3
rdMpb:
	.quad	dataMaster+16000
	.size	rdMpb, 8

	.type	rdMpe,@object           # @rdMpe
	.globl	rdMpe
	.p2align	3
rdMpe:
	.quad	dataMaster
	.size	rdMpe, 8

	.type	rrdpb,@object           # @rrdpb
	.globl	rrdpb
	.p2align	3
rrdpb:
	.zero	8
	.quad	data
	.size	rrdpb, 16

	.type	rrdpe,@object           # @rrdpe
	.globl	rrdpe
	.p2align	3
rrdpe:
	.zero	8
	.quad	data+16000
	.size	rrdpe, 16

	.type	rrdMpb,@object          # @rrdMpb
	.globl	rrdMpb
	.p2align	3
rrdMpb:
	.zero	8
	.quad	dataMaster
	.size	rrdMpb, 16

	.type	rrdMpe,@object          # @rrdMpe
	.globl	rrdMpe
	.p2align	3
rrdMpe:
	.zero	8
	.quad	dataMaster+16000
	.size	rrdMpe, 16

	.type	.L.str.26,@object       # @.str.26
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.26:
	.asciz	"insertion_sort double pointer verify2"
	.size	.L.str.26, 38

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"insertion_sort double vector iterator"
	.size	.L.str.27, 38

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"quicksort double pointer verify2"
	.size	.L.str.34, 33

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"quicksort double vector iterator"
	.size	.L.str.35, 33

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"heap_sort double pointer verify2"
	.size	.L.str.42, 33

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"heap_sort double vector iterator"
	.size	.L.str.43, 33

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"vector::_M_fill_insert"
	.size	.L.str.50, 23

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"test %i failed\n"
	.size	.L.str.51, 16

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"sort test %i failed\n"
	.size	.L.str.52, 21


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
