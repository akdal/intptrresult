	.text
	.file	"getDeleteCommand.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	4286578687              # float -3.40282347E+38
	.long	4286578687              # float -3.40282347E+38
	.long	4286578687              # float -3.40282347E+38
	.long	4286578687              # float -3.40282347E+38
.LCPI0_1:
	.long	2139095039              # float 3.40282347E+38
	.long	2139095039              # float 3.40282347E+38
	.long	2139095039              # float 3.40282347E+38
	.long	2139095039              # float 3.40282347E+38
	.text
	.globl	getDeleteCommand
	.p2align	4, 0x90
	.type	getDeleteCommand,@function
getDeleteCommand:                       # @getDeleteCommand
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [-3.402823e+38,-3.402823e+38,-3.402823e+38,-3.402823e+38]
	movups	%xmm0, (%r15)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [3.402823e+38,3.402823e+38,3.402823e+38,3.402823e+38]
	movups	%xmm0, 16(%r15)
	movq	$0, (%r12)
	leaq	16(%rsp), %r13
	movl	$3, %r14d
	leaq	8(%rsp), %rbp
	jmp	.LBB0_1
.LBB0_28:                               # %.thread51
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	16(%rsp), %rcx
	movq	%rcx, (%rax)
	movq	8(%rsp), %rcx
	movq	%rcx, 8(%rax)
	movq	(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	%rax, (%r12)
	.p2align	4, 0x90
.LBB0_1:                                # %.backedge60
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	getInt
	testq	%rax, %rax
	je	.LBB0_5
# BB#2:                                 # %.backedge60
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	%rax, %rcx
	addq	$-2, %rcx
	cmpq	$2, %rcx
	jb	.LBB0_7
.LBB0_3:                                # %.backedge
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpq	$1, %rax
	jne	.LBB0_1
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	movq	16(%rsp), %rax
	cmpq	$51, %rax
	jae	.LBB0_6
# BB#8:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	cmpq	$7, %rax
	jg	.LBB0_23
# BB#9:                                 #   in Loop: Header=BB0_1 Depth=1
	callq	getKeyAttribute
	testq	%rax, %rax
	je	.LBB0_13
# BB#10:                                #   in Loop: Header=BB0_1 Depth=1
	cmpq	$1, %rax
	je	.LBB0_26
# BB#11:                                #   in Loop: Header=BB0_1 Depth=1
	cmpq	$2, %rax
	jne	.LBB0_3
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_23:                               #   in Loop: Header=BB0_1 Depth=1
	callq	getNonKeyAttribute
	testq	%rax, %rax
	je	.LBB0_27
# BB#24:                                #   in Loop: Header=BB0_1 Depth=1
	cmpq	$1, %rax
	je	.LBB0_31
# BB#25:                                #   in Loop: Header=BB0_1 Depth=1
	cmpq	$2, %rax
	jne	.LBB0_1
	jmp	.LBB0_26
.LBB0_13:                               #   in Loop: Header=BB0_1 Depth=1
	movq	16(%rsp), %rcx
	cmpq	$7, %rcx
	ja	.LBB0_3
# BB#14:                                #   in Loop: Header=BB0_1 Depth=1
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_22:                               #   in Loop: Header=BB0_1 Depth=1
	movl	8(%rsp), %ecx
	movl	%ecx, (%r15)
	cmpq	$1, %rax
	jne	.LBB0_1
	jmp	.LBB0_4
.LBB0_27:                               #   in Loop: Header=BB0_1 Depth=1
	movl	$24, %edi
	callq	malloc
	testq	%rax, %rax
	jne	.LBB0_28
	jmp	.LBB0_29
.LBB0_15:                               #   in Loop: Header=BB0_1 Depth=1
	movl	8(%rsp), %ecx
	movl	%ecx, 4(%r15)
	cmpq	$1, %rax
	jne	.LBB0_1
	jmp	.LBB0_4
.LBB0_16:                               #   in Loop: Header=BB0_1 Depth=1
	movl	8(%rsp), %ecx
	movl	%ecx, 8(%r15)
	cmpq	$1, %rax
	jne	.LBB0_1
	jmp	.LBB0_4
.LBB0_17:                               #   in Loop: Header=BB0_1 Depth=1
	movl	8(%rsp), %ecx
	movl	%ecx, 12(%r15)
	cmpq	$1, %rax
	jne	.LBB0_1
	jmp	.LBB0_4
.LBB0_18:                               #   in Loop: Header=BB0_1 Depth=1
	movl	8(%rsp), %ecx
	movl	%ecx, 16(%r15)
	cmpq	$1, %rax
	jne	.LBB0_1
	jmp	.LBB0_4
.LBB0_19:                               #   in Loop: Header=BB0_1 Depth=1
	movl	8(%rsp), %ecx
	movl	%ecx, 20(%r15)
	cmpq	$1, %rax
	jne	.LBB0_1
	jmp	.LBB0_4
.LBB0_20:                               #   in Loop: Header=BB0_1 Depth=1
	movl	8(%rsp), %ecx
	movl	%ecx, 24(%r15)
	cmpq	$1, %rax
	jne	.LBB0_1
	jmp	.LBB0_4
.LBB0_21:                               #   in Loop: Header=BB0_1 Depth=1
	movl	8(%rsp), %ecx
	movl	%ecx, 28(%r15)
	cmpq	$1, %rax
	jne	.LBB0_1
.LBB0_4:
	xorl	%r14d, %r14d
	jmp	.LBB0_7
.LBB0_6:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getDeleteCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$3, %r14d
	jmp	.LBB0_7
.LBB0_26:
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getDeleteCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$2, %r14d
	jmp	.LBB0_7
.LBB0_12:
	movl	$.L.str.2, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getDeleteCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$1, %r14d
	jmp	.LBB0_7
.LBB0_31:
	movl	$.L.str.4, %edi
	jmp	.LBB0_30
.LBB0_29:
	movl	$.L.str.3, %edi
.LBB0_30:                               # %.loopexit
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getDeleteCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$4, %r14d
.LBB0_7:                                # %.loopexit
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	getDeleteCommand, .Lfunc_end0-getDeleteCommand
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_22
	.quad	.LBB0_15
	.quad	.LBB0_16
	.quad	.LBB0_17
	.quad	.LBB0_18
	.quad	.LBB0_19
	.quad	.LBB0_20
	.quad	.LBB0_21

	.type	getDeleteCommand.name,@object # @getDeleteCommand.name
	.data
	.p2align	4
getDeleteCommand.name:
	.asciz	"getDeleteCommand"
	.size	getDeleteCommand.name, 17

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"code out-of-range"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"improper format - early EOI"
	.size	.L.str.1, 28

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"low-level I/O error"
	.size	.L.str.2, 20

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"allocation failure"
	.size	.L.str.3, 19

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"allocation of non-key attribute"
	.size	.L.str.4, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
