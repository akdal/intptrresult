; ModuleID = 'yacr2.base-4.0/channel.bc'
source_filename = "channel.i"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }

@channelFile = common local_unnamed_addr global i8* null, align 8
@.str = private unnamed_addr constant [2 x i8] c"r\00", align 1
@.str.3 = private unnamed_addr constant [7 x i8] c"%u%u%u\00", align 1
@.str.4 = private unnamed_addr constant [47 x i8] c"\09Channel file description invalid at line %d.\0A\00", align 1
@channelColumns = common local_unnamed_addr global i64 0, align 8
@channelNets = common local_unnamed_addr global i64 0, align 8
@TOP = common local_unnamed_addr global i64* null, align 8
@BOT = common local_unnamed_addr global i64* null, align 8
@FIRST = common local_unnamed_addr global i64* null, align 8
@LAST = common local_unnamed_addr global i64* null, align 8
@DENSITY = common local_unnamed_addr global i64* null, align 8
@CROSSING = common local_unnamed_addr global i64* null, align 8
@channelTracks = common local_unnamed_addr global i64 0, align 8
@channelDensity = common local_unnamed_addr global i64 0, align 8
@channelDensityColumn = common local_unnamed_addr global i64 0, align 8
@channelTracksCopy = common local_unnamed_addr global i64 0, align 8
@str.1 = private unnamed_addr constant [30 x i8] c"\09Channel description invalid.\00"
@str.2 = private unnamed_addr constant [29 x i8] c"\09Channel has null dimension.\00"
@str.10 = private unnamed_addr constant [32 x i8] c"\09Channel file cannot be closed.\00"
@str.12 = private unnamed_addr constant [33 x i8] c"\09Incorrect number of specifiers.\00"
@str.14 = private unnamed_addr constant [29 x i8] c"\09Column number out of range.\00"
@str.15 = private unnamed_addr constant [7 x i8] c"Error:\00"
@str.16 = private unnamed_addr constant [32 x i8] c"\09Channel file cannot be opened.\00"

; Function Attrs: nounwind uwtable
define void @BuildChannel() local_unnamed_addr #0 {
entry:
  tail call void @DimensionChannel()
  tail call void @DescribeChannel()
  tail call void @DensityChannel()
  ret void
}

; Function Attrs: nounwind uwtable
define void @DimensionChannel() local_unnamed_addr #0 {
entry:
  %c1 = alloca i32, align 4
  %b1 = alloca i32, align 4
  %t1 = alloca i32, align 4
  %0 = load i8*, i8** @channelFile, align 8, !tbaa !1
  %call = tail call %struct._IO_FILE* @fopen(i8* %0, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i64 0, i64 0))
  %cmp = icmp eq %struct._IO_FILE* %call, null
  br i1 %cmp, label %if.then, label %do.body.preheader

do.body.preheader:                                ; preds = %entry
  %1 = bitcast i32* %c1 to i8*
  %2 = bitcast i32* %b1 to i8*
  %3 = bitcast i32* %t1 to i8*
  br label %do.body

if.then:                                          ; preds = %entry
  %puts27 = tail call i32 @puts(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @str.15, i64 0, i64 0))
  %puts28 = tail call i32 @puts(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @str.16, i64 0, i64 0))
  tail call void @exit(i32 1) #5
  unreachable

do.body:                                          ; preds = %do.body.preheader, %if.end29.thread
  %net.0 = phi i64 [ %conv6.net.1, %if.end29.thread ], [ 0, %do.body.preheader ]
  %dim.0 = phi i64 [ %conv4.dim.0, %if.end29.thread ], [ 0, %do.body.preheader ]
  %line.0 = phi i64 [ %inc, %if.end29.thread ], [ 0, %do.body.preheader ]
  %inc = add i64 %line.0, 1
  call void @llvm.lifetime.start(i64 4, i8* nonnull %1) #4
  call void @llvm.lifetime.start(i64 4, i8* nonnull %2) #4
  call void @llvm.lifetime.start(i64 4, i8* nonnull %3) #4
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* nonnull %call, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i64 0, i64 0), i32* nonnull %c1, i32* nonnull %b1, i32* nonnull %t1)
  %4 = load i32, i32* %c1, align 4, !tbaa !5
  %conv4 = zext i32 %4 to i64
  %5 = load i32, i32* %b1, align 4, !tbaa !5
  %conv5 = zext i32 %5 to i64
  %6 = load i32, i32* %t1, align 4, !tbaa !5
  %conv6 = zext i32 %6 to i64
  switch i32 %call3, label %if.else [
    i32 -1, label %do.end
    i32 3, label %if.end29.thread
  ]

if.end29.thread:                                  ; preds = %do.body
  %cmp13 = icmp ugt i64 %conv4, %dim.0
  %conv4.dim.0 = select i1 %cmp13, i64 %conv4, i64 %dim.0
  %cmp17 = icmp ugt i64 %conv5, %net.0
  %net.1 = select i1 %cmp17, i64 %conv5, i64 %net.0
  %cmp21 = icmp ugt i64 %conv6, %net.1
  %conv6.net.1 = select i1 %cmp21, i64 %conv6, i64 %net.1
  call void @llvm.lifetime.end(i64 4, i8* nonnull %3) #4
  call void @llvm.lifetime.end(i64 4, i8* nonnull %2) #4
  call void @llvm.lifetime.end(i64 4, i8* nonnull %1) #4
  br label %do.body

if.else:                                          ; preds = %do.body
  %puts25 = call i32 @puts(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @str.15, i64 0, i64 0))
  %call26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.4, i64 0, i64 0), i64 %inc)
  %puts26 = call i32 @puts(i8* getelementptr inbounds ([33 x i8], [33 x i8]* @str.12, i64 0, i64 0))
  call void @exit(i32 1) #5
  unreachable

do.end:                                           ; preds = %do.body
  call void @llvm.lifetime.end(i64 4, i8* nonnull %3) #4
  call void @llvm.lifetime.end(i64 4, i8* nonnull %2) #4
  call void @llvm.lifetime.end(i64 4, i8* nonnull %1) #4
  %call32 = call i32 @fclose(%struct._IO_FILE* nonnull %call)
  %cmp33 = icmp eq i32 %call32, -1
  br i1 %cmp33, label %if.then35, label %if.end38

if.then35:                                        ; preds = %do.end
  %puts23 = call i32 @puts(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @str.15, i64 0, i64 0))
  %puts24 = call i32 @puts(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @str.10, i64 0, i64 0))
  call void @exit(i32 1) #5
  unreachable

if.end38:                                         ; preds = %do.end
  %cmp39 = icmp eq i64 %dim.0, 0
  br i1 %cmp39, label %if.then41, label %if.end45

if.then41:                                        ; preds = %if.end38
  %puts = call i32 @puts(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @str.15, i64 0, i64 0))
  %puts21 = call i32 @puts(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @str.1, i64 0, i64 0))
  %puts22 = call i32 @puts(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @str.2, i64 0, i64 0))
  call void @exit(i32 1) #5
  unreachable

if.end45:                                         ; preds = %if.end38
  store i64 %dim.0, i64* @channelColumns, align 8, !tbaa !7
  store i64 %net.0, i64* @channelNets, align 8, !tbaa !7
  ret void
}

; Function Attrs: nounwind uwtable
define void @DescribeChannel() local_unnamed_addr #0 {
entry:
  %c1 = alloca i32, align 4
  %b1 = alloca i32, align 4
  %t1 = alloca i32, align 4
  %0 = load i64, i64* @channelColumns, align 8, !tbaa !7
  %add = shl i64 %0, 3
  %mul = add i64 %add, 8
  %call = tail call noalias i8* @malloc(i64 %mul) #4
  store i8* %call, i8** bitcast (i64** @TOP to i8**), align 8, !tbaa !1
  %call3 = tail call noalias i8* @malloc(i64 %mul) #4
  store i8* %call3, i8** bitcast (i64** @BOT to i8**), align 8, !tbaa !1
  %.cast = bitcast i8* %call to i64*
  %.cast37 = bitcast i8* %call3 to i64*
  br label %for.body

for.body:                                         ; preds = %entry, %for.body
  %col.036 = phi i64 [ 0, %entry ], [ %inc, %for.body ]
  %arrayidx = getelementptr inbounds i64, i64* %.cast, i64 %col.036
  store i64 0, i64* %arrayidx, align 8, !tbaa !7
  %arrayidx4 = getelementptr inbounds i64, i64* %.cast37, i64 %col.036
  store i64 0, i64* %arrayidx4, align 8, !tbaa !7
  %inc = add i64 %col.036, 1
  %cmp = icmp ugt i64 %inc, %0
  br i1 %cmp, label %for.end, label %for.body

for.end:                                          ; preds = %for.body
  %1 = load i8*, i8** @channelFile, align 8, !tbaa !1
  %call5 = tail call %struct._IO_FILE* @fopen(i8* %1, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i64 0, i64 0))
  %cmp6 = icmp eq %struct._IO_FILE* %call5, null
  br i1 %cmp6, label %if.then, label %do.body.preheader

do.body.preheader:                                ; preds = %for.end
  %2 = bitcast i32* %c1 to i8*
  %3 = bitcast i32* %b1 to i8*
  %4 = bitcast i32* %t1 to i8*
  br label %do.body

if.then:                                          ; preds = %for.end
  %puts24 = tail call i32 @puts(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @str.15, i64 0, i64 0))
  %puts25 = tail call i32 @puts(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @str.16, i64 0, i64 0))
  tail call void @exit(i32 1) #5
  unreachable

do.body:                                          ; preds = %do.body.preheader, %if.else
  %line.0 = phi i64 [ %inc9, %if.else ], [ 0, %do.body.preheader ]
  %inc9 = add i64 %line.0, 1
  call void @llvm.lifetime.start(i64 4, i8* nonnull %2) #4
  call void @llvm.lifetime.start(i64 4, i8* nonnull %3) #4
  call void @llvm.lifetime.start(i64 4, i8* nonnull %4) #4
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* nonnull %call5, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i64 0, i64 0), i32* nonnull %c1, i32* nonnull %b1, i32* nonnull %t1)
  %5 = load i32, i32* %c1, align 4, !tbaa !5
  %conv11 = zext i32 %5 to i64
  %6 = load i32, i32* %b1, align 4, !tbaa !5
  %conv12 = zext i32 %6 to i64
  %7 = load i32, i32* %t1, align 4, !tbaa !5
  %conv13 = zext i32 %7 to i64
  switch i32 %call10, label %if.else29 [
    i32 -1, label %do.end
    i32 3, label %if.then19
  ]

if.then19:                                        ; preds = %do.body
  %8 = load i64, i64* @channelColumns, align 8, !tbaa !7
  %cmp20 = icmp ugt i64 %conv11, %8
  br i1 %cmp20, label %if.then22, label %if.else

if.then22:                                        ; preds = %if.then19
  %puts22 = call i32 @puts(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @str.15, i64 0, i64 0))
  %call24 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.4, i64 0, i64 0), i64 %inc9)
  %puts23 = call i32 @puts(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @str.14, i64 0, i64 0))
  call void @exit(i32 1) #5
  unreachable

if.else:                                          ; preds = %if.then19
  %9 = load i64*, i64** @BOT, align 8, !tbaa !1
  %arrayidx26 = getelementptr inbounds i64, i64* %9, i64 %conv11
  store i64 %conv12, i64* %arrayidx26, align 8, !tbaa !7
  %10 = load i64*, i64** @TOP, align 8, !tbaa !1
  %arrayidx27 = getelementptr inbounds i64, i64* %10, i64 %conv11
  store i64 %conv13, i64* %arrayidx27, align 8, !tbaa !7
  call void @llvm.lifetime.end(i64 4, i8* nonnull %4) #4
  call void @llvm.lifetime.end(i64 4, i8* nonnull %3) #4
  call void @llvm.lifetime.end(i64 4, i8* nonnull %2) #4
  br label %do.body

if.else29:                                        ; preds = %do.body
  %puts20 = call i32 @puts(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @str.15, i64 0, i64 0))
  %call31 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.4, i64 0, i64 0), i64 %inc9)
  %puts21 = call i32 @puts(i8* getelementptr inbounds ([33 x i8], [33 x i8]* @str.12, i64 0, i64 0))
  call void @exit(i32 1) #5
  unreachable

do.end:                                           ; preds = %do.body
  call void @llvm.lifetime.end(i64 4, i8* nonnull %4) #4
  call void @llvm.lifetime.end(i64 4, i8* nonnull %3) #4
  call void @llvm.lifetime.end(i64 4, i8* nonnull %2) #4
  %call37 = call i32 @fclose(%struct._IO_FILE* nonnull %call5)
  %cmp38 = icmp eq i32 %call37, -1
  br i1 %cmp38, label %if.then40, label %if.end43

if.then40:                                        ; preds = %do.end
  %puts = call i32 @puts(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @str.15, i64 0, i64 0))
  %puts19 = call i32 @puts(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @str.10, i64 0, i64 0))
  call void @exit(i32 1) #5
  unreachable

if.end43:                                         ; preds = %do.end
  ret void
}

; Function Attrs: nounwind uwtable
define void @DensityChannel() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !7
  %add = shl i64 %0, 3
  %mul = add i64 %add, 8
  %call = tail call noalias i8* @malloc(i64 %mul) #4
  store i8* %call, i8** bitcast (i64** @FIRST to i8**), align 8, !tbaa !1
  %call3 = tail call noalias i8* @malloc(i64 %mul) #4
  store i8* %call3, i8** bitcast (i64** @LAST to i8**), align 8, !tbaa !1
  %1 = load i64, i64* @channelColumns, align 8, !tbaa !7
  %add4 = shl i64 %1, 3
  %mul5 = add i64 %add4, 8
  %call6 = tail call noalias i8* @malloc(i64 %mul5) #4
  store i8* %call6, i8** bitcast (i64** @DENSITY to i8**), align 8, !tbaa !1
  %call9 = tail call noalias i8* @malloc(i64 %mul) #4
  store i8* %call9, i8** bitcast (i64** @CROSSING to i8**), align 8, !tbaa !1
  %2 = bitcast i8* %call to i64*
  %3 = bitcast i8* %call3 to i64*
  %.cast = bitcast i8* %call9 to i64*
  %4 = bitcast i8* %call6 to i64*
  %5 = load i64, i64* @channelNets, align 8, !tbaa !7
  br label %for.body

for.body:                                         ; preds = %entry, %for.body
  %init.058 = phi i64 [ 0, %entry ], [ %inc, %for.body ]
  %arrayidx = getelementptr inbounds i64, i64* %2, i64 %init.058
  store i64 0, i64* %arrayidx, align 8, !tbaa !7
  %arrayidx10 = getelementptr inbounds i64, i64* %3, i64 %init.058
  store i64 0, i64* %arrayidx10, align 8, !tbaa !7
  %arrayidx11 = getelementptr inbounds i64, i64* %.cast, i64 %init.058
  store i64 0, i64* %arrayidx11, align 8, !tbaa !7
  %inc = add i64 %init.058, 1
  %cmp = icmp ugt i64 %inc, %5
  br i1 %cmp, label %for.body14.preheader, label %for.body

for.body14.preheader:                             ; preds = %for.body
  %6 = load i64, i64* @channelColumns, align 8, !tbaa !7
  br label %for.body14

for.cond19.preheader:                             ; preds = %for.body14
  %7 = load i64, i64* @channelNets, align 8, !tbaa !7
  %cmp2055 = icmp eq i64 %7, 0
  br i1 %cmp2055, label %for.end58, label %for.cond22.preheader.lr.ph

for.cond22.preheader.lr.ph:                       ; preds = %for.cond19.preheader
  %8 = load i64*, i64** @FIRST, align 8
  %9 = load i64*, i64** @LAST, align 8
  %10 = load i64*, i64** @DENSITY, align 8
  %11 = load i64*, i64** @BOT, align 8
  %12 = load i64*, i64** @TOP, align 8
  br label %for.cond22.preheader

for.body14:                                       ; preds = %for.body14.preheader, %for.body14
  %init.157 = phi i64 [ %inc17, %for.body14 ], [ 0, %for.body14.preheader ]
  %arrayidx15 = getelementptr inbounds i64, i64* %4, i64 %init.157
  store i64 0, i64* %arrayidx15, align 8, !tbaa !7
  %inc17 = add i64 %init.157, 1
  %cmp13 = icmp ugt i64 %inc17, %6
  br i1 %cmp13, label %for.cond19.preheader, label %for.body14

for.cond22.preheader:                             ; preds = %for.inc56, %for.cond22.preheader.lr.ph
  %13 = phi i64 [ %6, %for.cond22.preheader.lr.ph ], [ %.pre62, %for.inc56 ]
  %which.056 = phi i64 [ 1, %for.cond22.preheader.lr.ph ], [ %inc57, %for.inc56 ]
  %cmp2348 = icmp eq i64 %13, 0
  br i1 %cmp2348, label %for.end45, label %for.body24.preheader

for.body24.preheader:                             ; preds = %for.cond22.preheader
  br label %for.body24

for.body24:                                       ; preds = %for.body24.preheader, %for.inc30
  %col.049 = phi i64 [ %inc31, %for.inc30 ], [ 1, %for.body24.preheader ]
  %arrayidx25 = getelementptr inbounds i64, i64* %11, i64 %col.049
  %14 = load i64, i64* %arrayidx25, align 8, !tbaa !7
  %cmp26 = icmp eq i64 %14, %which.056
  br i1 %cmp26, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body24
  %arrayidx27 = getelementptr inbounds i64, i64* %12, i64 %col.049
  %15 = load i64, i64* %arrayidx27, align 8, !tbaa !7
  %cmp28 = icmp eq i64 %15, %which.056
  br i1 %cmp28, label %if.then, label %for.inc30

if.then:                                          ; preds = %lor.lhs.false, %for.body24
  %arrayidx29 = getelementptr inbounds i64, i64* %8, i64 %which.056
  store i64 %col.049, i64* %arrayidx29, align 8, !tbaa !7
  %.pre61 = load i64, i64* @channelColumns, align 8, !tbaa !7
  br label %for.end32

for.inc30:                                        ; preds = %lor.lhs.false
  %inc31 = add i64 %col.049, 1
  %cmp23 = icmp ugt i64 %inc31, %13
  br i1 %cmp23, label %for.end32.loopexit, label %for.body24

for.end32.loopexit:                               ; preds = %for.inc30
  br label %for.end32

for.end32:                                        ; preds = %for.end32.loopexit, %if.then
  %16 = phi i64 [ %.pre61, %if.then ], [ %13, %for.end32.loopexit ]
  %cmp3450 = icmp eq i64 %16, 0
  br i1 %cmp3450, label %for.end45, label %for.body35.preheader

for.body35.preheader:                             ; preds = %for.end32
  br label %for.body35

for.body35:                                       ; preds = %for.body35.preheader, %for.inc44
  %col.151 = phi i64 [ %dec, %for.inc44 ], [ %16, %for.body35.preheader ]
  %arrayidx36 = getelementptr inbounds i64, i64* %11, i64 %col.151
  %17 = load i64, i64* %arrayidx36, align 8, !tbaa !7
  %cmp37 = icmp eq i64 %17, %which.056
  br i1 %cmp37, label %if.then41, label %lor.lhs.false38

lor.lhs.false38:                                  ; preds = %for.body35
  %arrayidx39 = getelementptr inbounds i64, i64* %12, i64 %col.151
  %18 = load i64, i64* %arrayidx39, align 8, !tbaa !7
  %cmp40 = icmp eq i64 %18, %which.056
  br i1 %cmp40, label %if.then41, label %for.inc44

if.then41:                                        ; preds = %lor.lhs.false38, %for.body35
  %arrayidx42 = getelementptr inbounds i64, i64* %9, i64 %which.056
  store i64 %col.151, i64* %arrayidx42, align 8, !tbaa !7
  br label %for.end45

for.inc44:                                        ; preds = %lor.lhs.false38
  %dec = add i64 %col.151, -1
  %cmp34 = icmp eq i64 %dec, 0
  br i1 %cmp34, label %for.end45.loopexit, label %for.body35

for.end45.loopexit:                               ; preds = %for.inc44
  br label %for.end45

for.end45:                                        ; preds = %for.end45.loopexit, %for.cond22.preheader, %for.end32, %if.then41
  %arrayidx46 = getelementptr inbounds i64, i64* %8, i64 %which.056
  %19 = load i64, i64* %arrayidx46, align 8, !tbaa !7
  %arrayidx4852 = getelementptr inbounds i64, i64* %9, i64 %which.056
  %20 = load i64, i64* %arrayidx4852, align 8, !tbaa !7
  %cmp4953 = icmp ugt i64 %19, %20
  br i1 %cmp4953, label %for.inc56, label %for.body50.preheader

for.body50.preheader:                             ; preds = %for.end45
  br label %for.body50

for.body50:                                       ; preds = %for.body50.preheader, %for.body50
  %col.254 = phi i64 [ %inc54, %for.body50 ], [ %19, %for.body50.preheader ]
  %arrayidx51 = getelementptr inbounds i64, i64* %10, i64 %col.254
  %21 = load i64, i64* %arrayidx51, align 8, !tbaa !7
  %inc52 = add i64 %21, 1
  store i64 %inc52, i64* %arrayidx51, align 8, !tbaa !7
  %inc54 = add i64 %col.254, 1
  %22 = load i64, i64* %arrayidx4852, align 8, !tbaa !7
  %cmp49 = icmp ugt i64 %inc54, %22
  br i1 %cmp49, label %for.inc56.loopexit, label %for.body50

for.inc56.loopexit:                               ; preds = %for.body50
  br label %for.inc56

for.inc56:                                        ; preds = %for.inc56.loopexit, %for.end45
  %inc57 = add i64 %which.056, 1
  %23 = load i64, i64* @channelNets, align 8, !tbaa !7
  %cmp20 = icmp ugt i64 %inc57, %23
  %.pre62 = load i64, i64* @channelColumns, align 8, !tbaa !7
  br i1 %cmp20, label %for.end58.loopexit, label %for.cond22.preheader

for.end58.loopexit:                               ; preds = %for.inc56
  br label %for.end58

for.end58:                                        ; preds = %for.end58.loopexit, %for.cond19.preheader
  %24 = phi i64 [ %6, %for.cond19.preheader ], [ %.pre62, %for.end58.loopexit ]
  %cmp6043 = icmp eq i64 %24, 0
  br i1 %cmp6043, label %for.end69, label %for.body61.lr.ph

for.body61.lr.ph:                                 ; preds = %for.end58
  %25 = load i64*, i64** @DENSITY, align 8, !tbaa !1
  %26 = add i64 %24, -1
  %xtraiter = and i64 %24, 3
  %lcmp.mod = icmp eq i64 %xtraiter, 0
  br i1 %lcmp.mod, label %for.body61.prol.loopexit, label %for.body61.prol.preheader

for.body61.prol.preheader:                        ; preds = %for.body61.lr.ph
  br label %for.body61.prol

for.body61.prol:                                  ; preds = %for.body61.prol, %for.body61.prol.preheader
  %boundColumn.046.prol = phi i64 [ undef, %for.body61.prol.preheader ], [ %col.3.boundColumn.0.prol, %for.body61.prol ]
  %bound.045.prol = phi i64 [ 0, %for.body61.prol.preheader ], [ %.bound.0.prol, %for.body61.prol ]
  %col.344.prol = phi i64 [ %24, %for.body61.prol.preheader ], [ %dec68.prol, %for.body61.prol ]
  %prol.iter = phi i64 [ %xtraiter, %for.body61.prol.preheader ], [ %prol.iter.sub, %for.body61.prol ]
  %arrayidx62.prol = getelementptr inbounds i64, i64* %25, i64 %col.344.prol
  %27 = load i64, i64* %arrayidx62.prol, align 8, !tbaa !7
  %cmp63.prol = icmp ugt i64 %27, %bound.045.prol
  %.bound.0.prol = select i1 %cmp63.prol, i64 %27, i64 %bound.045.prol
  %col.3.boundColumn.0.prol = select i1 %cmp63.prol, i64 %col.344.prol, i64 %boundColumn.046.prol
  %dec68.prol = add i64 %col.344.prol, -1
  %prol.iter.sub = add i64 %prol.iter, -1
  %prol.iter.cmp = icmp eq i64 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %for.body61.prol.loopexit.unr-lcssa, label %for.body61.prol, !llvm.loop !9

for.body61.prol.loopexit.unr-lcssa:               ; preds = %for.body61.prol
  br label %for.body61.prol.loopexit

for.body61.prol.loopexit:                         ; preds = %for.body61.lr.ph, %for.body61.prol.loopexit.unr-lcssa
  %.bound.0.lcssa.unr = phi i64 [ undef, %for.body61.lr.ph ], [ %.bound.0.prol, %for.body61.prol.loopexit.unr-lcssa ]
  %col.3.boundColumn.0.lcssa.unr = phi i64 [ undef, %for.body61.lr.ph ], [ %col.3.boundColumn.0.prol, %for.body61.prol.loopexit.unr-lcssa ]
  %boundColumn.046.unr = phi i64 [ undef, %for.body61.lr.ph ], [ %col.3.boundColumn.0.prol, %for.body61.prol.loopexit.unr-lcssa ]
  %bound.045.unr = phi i64 [ 0, %for.body61.lr.ph ], [ %.bound.0.prol, %for.body61.prol.loopexit.unr-lcssa ]
  %col.344.unr = phi i64 [ %24, %for.body61.lr.ph ], [ %dec68.prol, %for.body61.prol.loopexit.unr-lcssa ]
  %28 = icmp ult i64 %26, 3
  br i1 %28, label %for.end69.loopexit, label %for.body61.lr.ph.new

for.body61.lr.ph.new:                             ; preds = %for.body61.prol.loopexit
  br label %for.body61

for.body61:                                       ; preds = %for.body61, %for.body61.lr.ph.new
  %boundColumn.046 = phi i64 [ %boundColumn.046.unr, %for.body61.lr.ph.new ], [ %col.3.boundColumn.0.3, %for.body61 ]
  %bound.045 = phi i64 [ %bound.045.unr, %for.body61.lr.ph.new ], [ %.bound.0.3, %for.body61 ]
  %col.344 = phi i64 [ %col.344.unr, %for.body61.lr.ph.new ], [ %dec68.3, %for.body61 ]
  %arrayidx62 = getelementptr inbounds i64, i64* %25, i64 %col.344
  %29 = load i64, i64* %arrayidx62, align 8, !tbaa !7
  %cmp63 = icmp ugt i64 %29, %bound.045
  %.bound.0 = select i1 %cmp63, i64 %29, i64 %bound.045
  %col.3.boundColumn.0 = select i1 %cmp63, i64 %col.344, i64 %boundColumn.046
  %dec68 = add i64 %col.344, -1
  %arrayidx62.1 = getelementptr inbounds i64, i64* %25, i64 %dec68
  %30 = load i64, i64* %arrayidx62.1, align 8, !tbaa !7
  %cmp63.1 = icmp ugt i64 %30, %.bound.0
  %.bound.0.1 = select i1 %cmp63.1, i64 %30, i64 %.bound.0
  %col.3.boundColumn.0.1 = select i1 %cmp63.1, i64 %dec68, i64 %col.3.boundColumn.0
  %dec68.1 = add i64 %col.344, -2
  %arrayidx62.2 = getelementptr inbounds i64, i64* %25, i64 %dec68.1
  %31 = load i64, i64* %arrayidx62.2, align 8, !tbaa !7
  %cmp63.2 = icmp ugt i64 %31, %.bound.0.1
  %.bound.0.2 = select i1 %cmp63.2, i64 %31, i64 %.bound.0.1
  %col.3.boundColumn.0.2 = select i1 %cmp63.2, i64 %dec68.1, i64 %col.3.boundColumn.0.1
  %dec68.2 = add i64 %col.344, -3
  %arrayidx62.3 = getelementptr inbounds i64, i64* %25, i64 %dec68.2
  %32 = load i64, i64* %arrayidx62.3, align 8, !tbaa !7
  %cmp63.3 = icmp ugt i64 %32, %.bound.0.2
  %.bound.0.3 = select i1 %cmp63.3, i64 %32, i64 %.bound.0.2
  %col.3.boundColumn.0.3 = select i1 %cmp63.3, i64 %dec68.2, i64 %col.3.boundColumn.0.2
  %dec68.3 = add i64 %col.344, -4
  %cmp60.3 = icmp eq i64 %dec68.3, 0
  br i1 %cmp60.3, label %for.end69.loopexit.unr-lcssa, label %for.body61

for.end69.loopexit.unr-lcssa:                     ; preds = %for.body61
  br label %for.end69.loopexit

for.end69.loopexit:                               ; preds = %for.body61.prol.loopexit, %for.end69.loopexit.unr-lcssa
  %.bound.0.lcssa = phi i64 [ %.bound.0.lcssa.unr, %for.body61.prol.loopexit ], [ %.bound.0.3, %for.end69.loopexit.unr-lcssa ]
  %col.3.boundColumn.0.lcssa = phi i64 [ %col.3.boundColumn.0.lcssa.unr, %for.body61.prol.loopexit ], [ %col.3.boundColumn.0.3, %for.end69.loopexit.unr-lcssa ]
  br label %for.end69

for.end69:                                        ; preds = %for.end69.loopexit, %for.end58
  %bound.0.lcssa = phi i64 [ 0, %for.end58 ], [ %.bound.0.lcssa, %for.end69.loopexit ]
  %boundColumn.0.lcssa = phi i64 [ undef, %for.end58 ], [ %col.3.boundColumn.0.lcssa, %for.end69.loopexit ]
  store i64 %bound.0.lcssa, i64* @channelTracks, align 8, !tbaa !7
  store i64 %bound.0.lcssa, i64* @channelDensity, align 8, !tbaa !7
  store i64 %boundColumn.0.lcssa, i64* @channelDensityColumn, align 8, !tbaa !7
  ret void
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare noalias %struct._IO_FILE* @fopen(i8* nocapture readonly, i8* nocapture readonly) local_unnamed_addr #2

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #2

; Function Attrs: noreturn nounwind
declare void @exit(i32) local_unnamed_addr #3

; Function Attrs: nounwind
declare i32 @fscanf(%struct._IO_FILE* nocapture, i8* nocapture readonly, ...) local_unnamed_addr #2

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @fclose(%struct._IO_FILE* nocapture) local_unnamed_addr #2

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) local_unnamed_addr #2

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #4

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { noreturn nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"any pointer", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"int", !3, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"long", !3, i64 0}
!9 = distinct !{!9, !10}
!10 = !{!"llvm.loop.unroll.disable"}
