	.text
	.file	"bitcnt_3.bc"
	.globl	ntbl_bitcount
	.p2align	4, 0x90
	.type	ntbl_bitcount,@function
ntbl_bitcount:                          # @ntbl_bitcount
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	andl	$15, %eax
	movsbl	bits(%rax), %eax
	movl	%edi, %ecx
	shrl	$4, %ecx
	andl	$15, %ecx
	movsbl	bits(%rcx), %ecx
	addl	%eax, %ecx
	movl	%edi, %eax
	shrl	$8, %eax
	andl	$15, %eax
	movsbl	bits(%rax), %eax
	addl	%ecx, %eax
	movl	%edi, %ecx
	shrl	$12, %ecx
	andl	$15, %ecx
	movsbl	bits(%rcx), %ecx
	addl	%eax, %ecx
	movl	%edi, %eax
	shrl	$16, %eax
	andl	$15, %eax
	movsbl	bits(%rax), %eax
	addl	%ecx, %eax
	movl	%edi, %ecx
	shrl	$20, %ecx
	andl	$15, %ecx
	movsbl	bits(%rcx), %ecx
	addl	%eax, %ecx
	movl	%edi, %eax
	shrl	$24, %eax
	andl	$15, %eax
	movsbl	bits(%rax), %edx
	addl	%ecx, %edx
	shrl	$28, %edi
	movsbl	bits(%rdi), %eax
	addl	%edx, %eax
	retq
.Lfunc_end0:
	.size	ntbl_bitcount, .Lfunc_end0-ntbl_bitcount
	.cfi_endproc

	.globl	BW_btbl_bitcount
	.p2align	4, 0x90
	.type	BW_btbl_bitcount,@function
BW_btbl_bitcount:                       # @BW_btbl_bitcount
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movzbl	%ah, %ecx  # NOREX
	movl	%eax, %edx
	movzbl	%al, %esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	shrl	$16, %eax
	movzbl	%al, %eax
	shrl	$24, %edx
	movsbl	bits(%rsi), %esi
	movsbl	bits(%rcx), %ecx
	addl	%esi, %ecx
	movsbl	bits(%rdx), %edx
	addl	%ecx, %edx
	movsbl	bits(%rax), %eax
	addl	%edx, %eax
	retq
.Lfunc_end1:
	.size	BW_btbl_bitcount, .Lfunc_end1-BW_btbl_bitcount
	.cfi_endproc

	.globl	AR_btbl_bitcount
	.p2align	4, 0x90
	.type	AR_btbl_bitcount,@function
AR_btbl_bitcount:                       # @AR_btbl_bitcount
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movzbl	%ah, %ecx  # NOREX
	movl	%eax, %edx
	movzbl	%al, %esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	shrl	$16, %eax
	movzbl	%al, %eax
	shrl	$24, %edx
	movsbl	bits(%rsi), %esi
	movsbl	bits(%rcx), %ecx
	addl	%esi, %ecx
	movsbl	bits(%rax), %esi
	addl	%ecx, %esi
	movsbl	bits(%rdx), %eax
	addl	%esi, %eax
	retq
.Lfunc_end2:
	.size	AR_btbl_bitcount, .Lfunc_end2-AR_btbl_bitcount
	.cfi_endproc

	.type	bits,@object            # @bits
	.section	.rodata,"a",@progbits
	.p2align	4
bits:
	.ascii	"\000\001\001\002\001\002\002\003\001\002\002\003\002\003\003\004\001\002\002\003\002\003\003\004\002\003\003\004\003\004\004\005\001\002\002\003\002\003\003\004\002\003\003\004\003\004\004\005\002\003\003\004\003\004\004\005\003\004\004\005\004\005\005\006\001\002\002\003\002\003\003\004\002\003\003\004\003\004\004\005\002\003\003\004\003\004\004\005\003\004\004\005\004\005\005\006\002\003\003\004\003\004\004\005\003\004\004\005\004\005\005\006\003\004\004\005\004\005\005\006\004\005\005\006\005\006\006\007\001\002\002\003\002\003\003\004\002\003\003\004\003\004\004\005\002\003\003\004\003\004\004\005\003\004\004\005\004\005\005\006\002\003\003\004\003\004\004\005\003\004\004\005\004\005\005\006\003\004\004\005\004\005\005\006\004\005\005\006\005\006\006\007\002\003\003\004\003\004\004\005\003\004\004\005\004\005\005\006\003\004\004\005\004\005\005\006\004\005\005\006\005\006\006\007\003\004\004\005\004\005\005\006\004\005\005\006\005\006\006\007\004\005\005\006\005\006\006\007\005\006\006\007\006\007\007\b"
	.size	bits, 256


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
