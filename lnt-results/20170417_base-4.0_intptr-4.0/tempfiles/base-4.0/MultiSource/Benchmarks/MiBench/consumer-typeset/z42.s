	.text
	.file	"z42.bc"
	.globl	ColourInit
	.p2align	4, 0x90
	.type	ColourInit,@function
ColourInit:                             # @ColourInit
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$1608, %edi             # imm = 0x648
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$42, %edi
	movl	$1, %esi
	movl	$.L.str.7, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_2:                                # %ctab_new.exit
	movl	$100, (%rbx)
	movq	%rbx, %rdi
	addq	$4, %rdi
	xorl	%esi, %esi
	movl	$1604, %edx             # imm = 0x644
	callq	memset
	movq	%rbx, col_tab(%rip)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	ColourInit, .Lfunc_end0-ColourInit
	.cfi_endproc

	.globl	ColourChange
	.p2align	4, 0x90
	.type	ColourChange,@function
ColourChange:                           # @ColourChange
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 64
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	leaq	32(%rbp), %r15
	movzbl	32(%rbp), %r12d
	movl	%r12d, %eax
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB1_1
# BB#3:
	movzbl	64(%rbp), %ebx
	testl	%ebx, %ebx
	je	.LBB1_4
# BB#6:
	leaq	64(%rbp), %r13
	movl	$.L.str.4, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_18
# BB#7:
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	col_tab(%rip), %rcx
	addq	$65, %rbp
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	movzbl	(%rbp), %edx
	leal	(%rdx,%rax), %ebx
	incq	%rbp
	testl	%edx, %edx
	jne	.LBB1_8
# BB#9:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	(%rcx)
	movslq	%edx, %rax
	shlq	$4, %rax
	movq	16(%rcx,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB1_16
# BB#10:                                # %.preheader39.i.preheader
	movq	%rbx, %r14
	.p2align	4, 0x90
.LBB1_11:                               # %.preheader39.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_13 Depth 2
	movq	8(%r14), %r14
	cmpq	%rbx, %r14
	je	.LBB1_16
# BB#12:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB1_11 Depth=1
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader.i
                                        #   Parent Loop BB1_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB1_13
# BB#14:                                #   in Loop: Header=BB1_11 Depth=1
	leaq	64(%rbp), %rsi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_11
# BB#15:                                # %ctab_retrieve.exit
	testq	%rbp, %rbp
	jne	.LBB1_17
.LBB1_16:                               # %ctab_retrieve.exit.thread
	movl	%r12d, %edi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	MakeWord
	movq	%rax, %rbp
	movl	$col_tab, %esi
	movq	%rbp, %rdi
	callq	ctab_insert
.LBB1_17:
	movl	$4190208, %eax          # imm = 0x3FF000
	andl	40(%rbp), %eax
	movl	$-4190209, %ecx         # imm = 0xFFC00FFF
	movq	(%rsp), %rdx            # 8-byte Reload
	andl	12(%rdx), %ecx
	orl	%eax, %ecx
	movl	%ecx, 12(%rdx)
	jmp	.LBB1_18
.LBB1_1:
	movl	$42, %edi
	movl	$3, %esi
	movl	$.L.str, %edx
.LBB1_2:
	movl	$2, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	Error                   # TAILCALL
.LBB1_4:
	movq	BackEnd(%rip), %rax
	cmpl	$0, 44(%rax)
	je	.LBB1_18
# BB#5:
	movl	$42, %edi
	movl	$4, %esi
	movl	$.L.str.3, %edx
	jmp	.LBB1_2
.LBB1_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	ColourChange, .Lfunc_end1-ColourChange
	.cfi_endproc

	.p2align	4, 0x90
	.type	ctab_insert,@function
ctab_insert:                            # @ctab_insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 80
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	(%r12), %r14
	movl	4(%r14), %eax
	movslq	(%r14), %rbp
	leal	-1(%rbp), %ecx
	cmpl	%ecx, %eax
	jne	.LBB2_14
# BB#1:
	movq	%r12, 16(%rsp)          # 8-byte Spill
	leal	(%rbp,%rbp), %r12d
	movq	%rbp, %rdi
	shlq	$5, %rdi
	orq	$8, %rdi
	callq	malloc
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB2_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$42, %edi
	movl	$1, %esi
	movl	$.L.str.7, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB2_3:
	movl	%r12d, (%r13)
	movl	$0, 4(%r13)
	testl	%ebp, %ebp
	jle	.LBB2_5
# BB#4:                                 # %.lr.ph.i.i
	movq	%r13, %rdi
	addq	$8, %rdi
	decl	%r12d
	shlq	$4, %r12
	addq	$16, %r12
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
.LBB2_5:                                # %ctab_new.exit.i
	movq	%r13, 8(%rsp)
	cmpl	$0, 4(%r14)
	jle	.LBB2_8
# BB#6:                                 # %.lr.ph42.preheader
	leaq	24(%r14), %rbx
	xorl	%ebp, %ebp
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph42
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	callq	ctab_insert
	movslq	4(%r14), %rax
	incq	%rbp
	addq	$16, %rbx
	cmpq	%rax, %rbp
	jl	.LBB2_7
.LBB2_8:                                # %.preheader
	movl	(%r14), %eax
	testl	%eax, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	jle	.LBB2_13
# BB#9:                                 # %.lr.ph.preheader
	leaq	16(%r14), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_10 Depth=1
	callq	DisposeObject
	movl	(%r14), %eax
.LBB2_12:                               #   in Loop: Header=BB2_10 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	addq	$16, %rbp
	cmpq	%rcx, %rbx
	jl	.LBB2_10
.LBB2_13:                               # %ctab_rehash.exit
	movq	%r14, %rdi
	callq	free
	movq	8(%rsp), %r14
	movq	%r14, (%r12)
	movl	4(%r14), %eax
.LBB2_14:
	leal	1(%rax), %ebp
	movl	%ebp, 4(%r14)
	cmpl	$4096, %eax             # imm = 0x1000
	jl	.LBB2_16
# BB#15:
	leaq	32(%r15), %r8
	movl	$42, %edi
	movl	$2, %esi
	movl	$.L.str.8, %edx
	movl	$1, %ecx
	movl	$4096, %r9d             # imm = 0x1000
	xorl	%eax, %eax
	callq	Error
.LBB2_16:
	movzbl	64(%r15), %edx
	leaq	65(%r15), %rcx
	.p2align	4, 0x90
.LBB2_17:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %eax
	movzbl	(%rcx), %esi
	leal	(%rsi,%rax), %edx
	incq	%rcx
	testl	%esi, %esi
	jne	.LBB2_17
# BB#18:
	movq	(%r12), %rcx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	(%rcx)
	movslq	%edx, %rbx
	shlq	$4, %rbx
	cmpq	$0, 16(%rcx,%rbx)
	jne	.LBB2_23
# BB#19:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_20
# BB#21:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_22
.LBB2_20:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_22:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	(%r12), %rcx
	movq	%rax, 16(%rcx,%rbx)
.LBB2_23:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_24
# BB#25:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_26
.LBB2_24:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_26:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r12), %rcx
	movq	16(%rcx,%rbx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_29
# BB#27:
	testq	%rcx, %rcx
	je	.LBB2_29
# BB#28:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_29:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB2_32
# BB#30:
	testq	%rax, %rax
	je	.LBB2_32
# BB#31:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_32:
	movslq	%ebp, %rax
	shll	$12, %ebp
	andl	$4190208, %ebp          # imm = 0x3FF000
	movl	$-4190209, %ecx         # imm = 0xFFC00FFF
	andl	40(%r15), %ecx
	orl	%ebp, %ecx
	movl	%ecx, 40(%r15)
	movq	(%r12), %rcx
	shlq	$4, %rax
	movq	%r15, 8(%rcx,%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	ctab_insert, .Lfunc_end2-ctab_insert
	.cfi_endproc

	.globl	ColourCommand
	.p2align	4, 0x90
	.type	ColourCommand,@function
ColourCommand:                          # @ColourCommand
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	testl	%ebx, %ebx
	je	.LBB3_2
# BB#1:
	movq	col_tab(%rip), %rax
	cmpl	%ebx, 4(%rax)
	jae	.LBB3_3
.LBB3_2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	col_tab(%rip), %rax
.LBB3_3:
	movl	%ebx, %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rax
	addq	$64, %rax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	ColourCommand, .Lfunc_end3-ColourCommand
	.cfi_endproc

	.type	col_tab,@object         # @col_tab
	.local	col_tab
	.comm	col_tab,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s ignored (illegal left parameter)"
	.size	.L.str, 36

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"@SetColour"
	.size	.L.str.1, 11

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s ignored (empty left parameter)"
	.size	.L.str.3, 34

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"nochange"
	.size	.L.str.4, 9

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"assert failed in %s"
	.size	.L.str.5, 20

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ColourCommand: number"
	.size	.L.str.6, 22

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"ran out of memory when enlarging colour table"
	.size	.L.str.7, 46

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"too many colours (maximum is %d)"
	.size	.L.str.8, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
