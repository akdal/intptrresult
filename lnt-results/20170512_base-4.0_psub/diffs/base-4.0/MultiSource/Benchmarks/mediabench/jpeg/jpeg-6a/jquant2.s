	.text
	.file	"jquant2.bc"
	.globl	jinit_2pass_quantizer
	.p2align	4, 0x90
	.type	jinit_2pass_quantizer,@function
jinit_2pass_quantizer:                  # @jinit_2pass_quantizer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$88, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 608(%rbx)
	movq	$start_pass_2_quant, (%r14)
	movq	$new_color_map_2_quant, 24(%r14)
	movq	$0, 64(%r14)
	movq	$0, 80(%r14)
	cmpl	$3, 136(%rbx)
	je	.LBB0_2
# BB#1:
	movq	(%rbx), %rax
	movl	$46, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_2:
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$256, %edx              # imm = 0x100
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 48(%r14)
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	48(%r14), %rcx
	movq	%rax, (%rcx,%rbp,8)
	incq	%rbp
	cmpq	$32, %rbp
	jne	.LBB0_3
# BB#4:
	movl	$1, 56(%r14)
	cmpl	$0, 124(%rbx)
	je	.LBB0_11
# BB#5:
	movl	112(%rbx), %ebp
	cmpl	$7, %ebp
	jg	.LBB0_7
# BB#6:                                 # %.thread
	movq	(%rbx), %rax
	movl	$55, 40(%rax)
	movl	$8, 44(%rax)
	jmp	.LBB0_9
.LBB0_11:
	movq	$0, 32(%r14)
	cmpl	$0, 104(%rbx)
	jne	.LBB0_14
	jmp	.LBB0_13
.LBB0_7:
	cmpl	$257, %ebp              # imm = 0x101
	jl	.LBB0_10
# BB#8:
	movq	(%rbx), %rax
	movl	$56, 40(%rax)
	movl	$256, 44(%rax)          # imm = 0x100
.LBB0_9:
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_10:
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$3, %ecx
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	*16(%rax)
	movq	%rax, 32(%r14)
	movl	%ebp, 40(%r14)
	cmpl	$0, 104(%rbx)
	je	.LBB0_13
.LBB0_14:
	movl	$2, 104(%rbx)
	movq	8(%rbx), %rax
	movl	128(%rbx), %ecx
	addl	$2, %ecx
	addq	%rcx, %rcx
	leaq	(%rcx,%rcx,2), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	%rax, 64(%r14)
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	init_error_limit        # TAILCALL
.LBB0_13:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jinit_2pass_quantizer, .Lfunc_end0-jinit_2pass_quantizer
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_2_quant,@function
start_pass_2_quant:                     # @start_pass_2_quant
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -48
.Lcfi12:
	.cfi_offset %r12, -40
.Lcfi13:
	.cfi_offset %r13, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	608(%r15), %r12
	movq	48(%r12), %r13
	cmpl	$0, 104(%r15)
	je	.LBB1_1
# BB#2:
	movl	$2, 104(%r15)
	movl	$2, %eax
	testl	%esi, %esi
	jne	.LBB1_4
	jmp	.LBB1_5
.LBB1_1:
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.LBB1_5
.LBB1_4:
	movq	$prescan_quantize, 8(%r12)
	movq	$finish_pass1, 16(%r12)
	movl	$1, 56(%r12)
	cmpl	$0, 56(%r12)
	jne	.LBB1_17
	jmp	.LBB1_20
.LBB1_5:
	cmpl	$2, %eax
	movl	$pass2_fs_dither, %eax
	movl	$pass2_no_dither, %ecx
	cmoveq	%rax, %rcx
	movq	%rcx, 8(%r12)
	movq	$finish_pass2, 16(%r12)
	movl	148(%r15), %eax
	testl	%eax, %eax
	jle	.LBB1_6
# BB#7:
	cmpl	$257, %eax              # imm = 0x101
	jl	.LBB1_10
# BB#8:
	movq	(%r15), %rax
	movl	$56, 40(%rax)
	movl	$256, 44(%rax)          # imm = 0x100
	jmp	.LBB1_9
.LBB1_6:                                # %.thread
	movq	(%r15), %rax
	movl	$55, 40(%rax)
	movl	$1, 44(%rax)
.LBB1_9:
	movq	%r15, %rdi
	callq	*(%rax)
.LBB1_10:
	cmpl	$2, 104(%r15)
	jne	.LBB1_16
# BB#11:
	movl	128(%r15), %eax
	addl	$2, %eax
	addq	%rax, %rax
	leaq	(%rax,%rax,2), %r14
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_13
# BB#12:
	movq	8(%r15), %rax
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	*8(%rax)
	movq	%rax, %rdi
	movq	%rdi, 64(%r12)
.LBB1_13:
	movq	%r14, %rsi
	callq	jzero_far
	cmpq	$0, 80(%r12)
	jne	.LBB1_15
# BB#14:
	movq	%r15, %rdi
	callq	init_error_limit
.LBB1_15:
	movl	$0, 72(%r12)
.LBB1_16:
	cmpl	$0, 56(%r12)
	je	.LBB1_20
.LBB1_17:                               # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_18:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %rdi
	movl	$4096, %esi             # imm = 0x1000
	callq	jzero_far
	incq	%rbx
	cmpq	$32, %rbx
	jne	.LBB1_18
# BB#19:
	movl	$0, 56(%r12)
.LBB1_20:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	start_pass_2_quant, .Lfunc_end1-start_pass_2_quant
	.cfi_endproc

	.p2align	4, 0x90
	.type	new_color_map_2_quant,@function
new_color_map_2_quant:                  # @new_color_map_2_quant
	.cfi_startproc
# BB#0:
	movq	608(%rdi), %rax
	movl	$1, 56(%rax)
	retq
.Lfunc_end2:
	.size	new_color_map_2_quant, .Lfunc_end2-new_color_map_2_quant
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	4294967293              # 0xfffffffd
	.long	4294967294              # 0xfffffffe
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
.LCPI3_1:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
.LCPI3_2:
	.long	4294967289              # 0xfffffff9
	.long	4294967290              # 0xfffffffa
	.long	4294967291              # 0xfffffffb
	.long	4294967292              # 0xfffffffc
.LCPI3_3:
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
.LCPI3_4:
	.long	4294967285              # 0xfffffff5
	.long	4294967286              # 0xfffffff6
	.long	4294967287              # 0xfffffff7
	.long	4294967288              # 0xfffffff8
.LCPI3_5:
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
.LCPI3_6:
	.long	4294967281              # 0xfffffff1
	.long	4294967282              # 0xfffffff2
	.long	4294967283              # 0xfffffff3
	.long	4294967284              # 0xfffffff4
	.text
	.p2align	4, 0x90
	.type	init_error_limit,@function
init_error_limit:                       # @init_error_limit
	.cfi_startproc
# BB#0:                                 # %.lr.ph45.preheader
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	608(%rdi), %rbx
	movq	8(%rdi), %rax
	movl	$1, %esi
	movl	$2044, %edx             # imm = 0x7FC
	callq	*(%rax)
	leaq	1020(%rax), %rcx
	movq	%rcx, 80(%rbx)
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [4294967293,4294967294,4294967295,0]
	movups	%xmm0, 1008(%rax)
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [1,2,3,4]
	movups	%xmm0, 1024(%rax)
	movaps	.LCPI3_2(%rip), %xmm0   # xmm0 = [4294967289,4294967290,4294967291,4294967292]
	movups	%xmm0, 992(%rax)
	movaps	.LCPI3_3(%rip), %xmm0   # xmm0 = [5,6,7,8]
	movups	%xmm0, 1040(%rax)
	movaps	.LCPI3_4(%rip), %xmm0   # xmm0 = [4294967285,4294967286,4294967287,4294967288]
	movups	%xmm0, 976(%rax)
	movaps	.LCPI3_5(%rip), %xmm0   # xmm0 = [9,10,11,12]
	movups	%xmm0, 1056(%rax)
	movl	$13, 1072(%rax)
	movl	$14, 1076(%rax)
	movl	$15, 1080(%rax)
	movaps	.LCPI3_6(%rip), %xmm0   # xmm0 = [4294967281,4294967282,4294967283,4294967284]
	movups	%xmm0, 960(%rax)
	movl	$16, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_1:                                # %.lr.ph45
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, 1084(%rax,%rsi)
	leal	-16(%rdx), %edi
	movl	%edi, 956(%rax,%rdx,8)
	movl	%ecx, 1088(%rax,%rsi)
	movl	%edi, 952(%rax,%rdx,8)
	incl	%ecx
	addq	$8, %rsi
	decq	%rdx
	cmpq	$128, %rsi
	jne	.LBB3_1
# BB#2:                                 # %.lr.ph
	addq	$-16, %rdx
	movq	%rax, %rsi
	addq	$1224, %rsi             # imm = 0x4C8
	movl	$208, %edi
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, -12(%rsi)
	movl	%edx, -4(%rax,%rdi,4)
	movl	%ecx, -8(%rsi)
	movl	%edx, -8(%rax,%rdi,4)
	movl	%ecx, -4(%rsi)
	movl	%edx, -12(%rax,%rdi,4)
	movl	%ecx, (%rsi)
	movl	%edx, -16(%rax,%rdi,4)
	addq	$16, %rsi
	addq	$-4, %rdi
	jne	.LBB3_3
# BB#4:                                 # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end3:
	.size	init_error_limit, .Lfunc_end3-init_error_limit
	.cfi_endproc

	.p2align	4, 0x90
	.type	prescan_quantize,@function
prescan_quantize:                       # @prescan_quantize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	testl	%ecx, %ecx
	jle	.LBB4_6
# BB#1:
	movl	128(%rdi), %r8d
	testl	%r8d, %r8d
	je	.LBB4_6
# BB#2:                                 # %.lr.ph36.split.preheader
	movq	608(%rdi), %rax
	movq	48(%rax), %r11
	movl	%ecx, %r9d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph36.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	movq	(%rsi,%r10,8), %rax
	movl	%r8d, %ecx
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edi
	andl	$248, %edi
	movzbl	1(%rax), %edx
	shlq	$4, %rdx
	andl	$4032, %edx             # imm = 0xFC0
	movzbl	2(%rax), %ebx
	shrq	$2, %rbx
	andl	$62, %ebx
	addq	(%r11,%rdi), %rdx
	movzwl	(%rbx,%rdx), %edi
	movl	%edi, %ebp
	incl	%ebp
	testw	%bp, %bp
	cmovnew	%bp, %di
	movw	%di, (%rbx,%rdx)
	addq	$3, %rax
	decl	%ecx
	jne	.LBB4_4
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB4_3 Depth=1
	incq	%r10
	cmpq	%r9, %r10
	jne	.LBB4_3
.LBB4_6:                                # %._crit_edge37
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	prescan_quantize, .Lfunc_end4-prescan_quantize
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	0                       # 0x0
	.long	31                      # 0x1f
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.text
	.p2align	4, 0x90
	.type	finish_pass1,@function
finish_pass1:                           # @finish_pass1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 160
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	608(%rbx), %rcx
	movq	32(%rcx), %rax
	movq	%rax, 152(%rbx)
	movq	8(%rbx), %rax
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	40(%rcx), %r14
	leaq	(,%r14,8), %rcx
	leaq	(%rcx,%rcx,4), %rdx
	movl	$1, %ebp
	movl	$1, %esi
	callq	*(%rax)
	movq	%rax, %r15
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [0,31,0,63]
	movups	%xmm0, (%r15)
	movabsq	$133143986176, %rax     # imm = 0x1F00000000
	movq	%rax, 16(%r15)
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	update_box
	cmpq	$2, %r14
	movq	%r15, 8(%rsp)           # 8-byte Spill
	jl	.LBB5_36
# BB#1:                                 # %.lr.ph.i.i.preheader
	leaq	40(%r15), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	movl	$1, %r12d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_11 Depth 2
                                        #     Child Loop BB5_19 Depth 2
                                        #     Child Loop BB5_22 Depth 2
	movl	%r12d, %ecx
	leal	(%rcx,%rcx), %eax
	cmpl	%r14d, %eax
	jle	.LBB5_3
# BB#16:                                # %.lr.ph.i6.i.i.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	leal	1(%r9), %eax
	movq	%rcx, %r8
	testb	$3, %al
	je	.LBB5_17
# BB#18:                                # %.lr.ph.i6.i.i.prol.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	%r13d, %edx
	andl	$3, %edx
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_19:                               # %.lr.ph.i6.i.i.prol
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rcx), %rbp
	cmpq	%rax, %rbp
	cmovgeq	%rbp, %rax
	cmovgq	%rcx, %rsi
	incq	%rdi
	addq	$40, %rcx
	cmpq	%rdi, %rdx
	jne	.LBB5_19
	jmp	.LBB5_20
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rcx, %r8
	testb	$1, %r9b
	jne	.LBB5_4
# BB#5:                                 # %.lr.ph.i.i.i.prol
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	32(%r15), %rax
	testq	%rax, %rax
	jle	.LBB5_6
# BB#7:                                 #   in Loop: Header=BB5_2 Depth=1
	cmpq	$0, 24(%r15)
	movl	$0, %ecx
	cmovleq	%rcx, %rax
	movl	$0, %esi
	cmovgq	%r15, %rsi
	movl	$1, %ebp
	movq	%rsi, %rdi
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_4:                                #   in Loop: Header=BB5_2 Depth=1
                                        # implicit-def: %RSI
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	xorl	%eax, %eax
	movq	%r15, %rcx
	testq	%r9, %r9
	jne	.LBB5_10
	jmp	.LBB5_23
.LBB5_17:                               #   in Loop: Header=BB5_2 Depth=1
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r15, %rcx
.LBB5_20:                               # %.lr.ph.i6.i.i.prol.loopexit
                                        #   in Loop: Header=BB5_2 Depth=1
	cmpq	$3, %r9
	jb	.LBB5_23
# BB#21:                                # %.lr.ph.i6.i.i.preheader.new
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%r12, %rdx
	subq	%rdi, %rdx
	addq	$80, %rcx
	.p2align	4, 0x90
.LBB5_22:                               # %.lr.ph.i6.i.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-80(%rcx), %rdi
	movq	-56(%rcx), %rbp
	movq	-16(%rcx), %rbx
	cmpq	%rax, %rbp
	cmovgeq	%rbp, %rax
	cmovleq	%rsi, %rdi
	leaq	-40(%rcx), %rbp
	cmpq	%rax, %rbx
	cmovgeq	%rbx, %rax
	cmovleq	%rdi, %rbp
	movq	24(%rcx), %rsi
	cmpq	%rax, %rsi
	cmovgeq	%rsi, %rax
	cmovgq	%rcx, %rbp
	leaq	40(%rcx), %rsi
	movq	64(%rcx), %rdi
	cmpq	%rax, %rdi
	cmovgeq	%rdi, %rax
	cmovleq	%rbp, %rsi
	addq	$160, %rcx
	addq	$-4, %rdx
	jne	.LBB5_22
	jmp	.LBB5_23
.LBB5_6:                                #   in Loop: Header=BB5_2 Depth=1
	xorl	%esi, %esi
	movl	$1, %ebp
	xorl	%edi, %edi
	xorl	%eax, %eax
.LBB5_8:                                # %.lr.ph.i.i.i.prol.loopexit
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	testq	%r9, %r9
	je	.LBB5_23
.LBB5_10:                               # %.lr.ph.i.i.i.preheader.new
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%r12, %rdx
	subq	%rbp, %rdx
	addq	$40, %rcx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB5_11:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rdi
	cmpq	%rax, %rdi
	jle	.LBB5_13
# BB#12:                                #   in Loop: Header=BB5_11 Depth=2
	leaq	-40(%rcx), %rbp
	cmpq	$0, -16(%rcx)
	cmovgq	%rdi, %rax
	cmovgq	%rbp, %rsi
.LBB5_13:                               # %.lr.ph.i.i.i.112
                                        #   in Loop: Header=BB5_11 Depth=2
	movq	32(%rcx), %rdi
	cmpq	%rax, %rdi
	jle	.LBB5_15
# BB#14:                                #   in Loop: Header=BB5_11 Depth=2
	cmpq	$0, 24(%rcx)
	cmovgq	%rdi, %rax
	cmovgq	%rcx, %rsi
.LBB5_15:                               #   in Loop: Header=BB5_11 Depth=2
	addq	$80, %rcx
	addq	$-2, %rdx
	jne	.LBB5_11
.LBB5_23:                               # %find_biggest_color_pop.exit.i.i
                                        #   in Loop: Header=BB5_2 Depth=1
	testq	%rsi, %rsi
	je	.LBB5_35
# BB#24:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movl	%r13d, (%rsp)           # 4-byte Spill
	movq	%r14, %r13
	leaq	(%r12,%r12,4), %rcx
	movl	20(%rsi), %eax
	movl	%eax, 20(%r15,%rcx,8)
	movups	(%rsi), %xmm0
	movups	%xmm0, (%r15,%rcx,8)
	movl	16(%rsi), %eax
	movl	%eax, 16(%r15,%rcx,8)
	movl	(%rsi), %r10d
	movl	4(%rsi), %r8d
	movl	%r8d, %ebx
	subl	%r10d, %ebx
	shll	$4, %ebx
	movl	12(%rsi), %r9d
	movl	8(%rsi), %edi
	movl	%r9d, %eax
	subl	%edi, %eax
	shll	$2, %eax
	leal	(%rax,%rax,2), %edx
	movl	20(%rsi), %r11d
	movl	16(%rsi), %ebp
	movl	%r11d, %eax
	subl	%ebp, %eax
	shll	$3, %eax
	cmpl	%edx, %ebx
	setle	%r14b
	cmovll	%edx, %ebx
	cmpl	%ebx, %eax
	movb	$2, %bl
	jg	.LBB5_26
# BB#25:                                #   in Loop: Header=BB5_2 Depth=1
	movl	%r14d, %ebx
.LBB5_26:                               #   in Loop: Header=BB5_2 Depth=1
	leaq	(%r15,%rcx,8), %r15
	cmpb	$2, %bl
	je	.LBB5_31
# BB#27:                                #   in Loop: Header=BB5_2 Depth=1
	cmpb	$1, %bl
	movq	%r13, %r14
	je	.LBB5_30
# BB#28:                                #   in Loop: Header=BB5_2 Depth=1
	testb	%bl, %bl
	movl	(%rsp), %r13d           # 4-byte Reload
	jne	.LBB5_34
# BB#29:                                #   in Loop: Header=BB5_2 Depth=1
	addl	%r8d, %r10d
	movl	%r10d, %ecx
	shrl	$31, %ecx
	addl	%r10d, %ecx
	sarl	%ecx
	movl	%ecx, 4(%rsi)
	movq	%r15, %rax
	jmp	.LBB5_33
	.p2align	4, 0x90
.LBB5_31:                               #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	16(%rax,%rcx,8), %rax
	addl	%r11d, %ebp
	movl	%ebp, %ecx
	shrl	$31, %ecx
	addl	%ebp, %ecx
	sarl	%ecx
	movl	%ecx, 20(%rsi)
	movq	%r13, %r14
	jmp	.LBB5_32
	.p2align	4, 0x90
.LBB5_30:                               #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	8(%rax,%rcx,8), %rax
	addl	%r9d, %edi
	movl	%edi, %ecx
	shrl	$31, %ecx
	addl	%edi, %ecx
	sarl	%ecx
	movl	%ecx, 12(%rsi)
.LBB5_32:                               # %.sink.split.i.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	(%rsp), %r13d           # 4-byte Reload
.LBB5_33:                               # %.sink.split.i.i
                                        #   in Loop: Header=BB5_2 Depth=1
	incl	%ecx
	movl	%ecx, (%rax)
.LBB5_34:                               #   in Loop: Header=BB5_2 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	update_box
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	update_box
	incq	%r12
	incl	%r13d
	movq	32(%rsp), %r9           # 8-byte Reload
	incq	%r9
	cmpq	%r14, %r12
	movl	%r13d, %r8d
	movq	8(%rsp), %r15           # 8-byte Reload
	jl	.LBB5_2
.LBB5_35:                               # %median_cut.exit.i
	testl	%r8d, %r8d
	movq	%r8, %rbp
	jle	.LBB5_51
.LBB5_36:                               # %.lr.ph.i
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	%ebp, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r9d, %r9d
	jmp	.LBB5_37
.LBB5_41:                               # %.preheader.i.preheader.i
                                        #   in Loop: Header=BB5_37 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	608(%rax), %rax
	movq	48(%rax), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	-1(%rdi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leal	4(,%rdi,8), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	%r10, 88(%rsp)          # 8-byte Spill
	movq	%r8, 80(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB5_42:                               # %.preheader.i.i
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_43 Depth 3
                                        #         Child Loop BB5_44 Depth 4
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%r11,8), %rbp
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%r11, 96(%rsp)          # 8-byte Spill
	leal	4(,%r11,8), %ebp
	movslq	%ebp, %r15
	movq	%r8, %r9
	.p2align	4, 0x90
.LBB5_43:                               # %.lr.ph113.split.i.i
                                        #   Parent Loop BB5_37 Depth=1
                                        #     Parent Loop BB5_42 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_44 Depth 4
	movq	%r9, %rbp
	shlq	$6, %rbp
	addq	32(%rsp), %rbp          # 8-byte Folded Reload
	movq	%rdi, %r12
	leaq	(%rbp,%rdi,2), %r10
	leal	2(,%r9,4), %ebp
	movslq	%ebp, %r11
	movl	24(%rsp), %r14d         # 4-byte Reload
	movq	(%rsp), %r8             # 8-byte Reload
	.p2align	4, 0x90
.LBB5_44:                               #   Parent Loop BB5_37 Depth=1
                                        #     Parent Loop BB5_42 Depth=2
                                        #       Parent Loop BB5_43 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzwl	(%r10), %edi
	testq	%rdi, %rdi
	je	.LBB5_46
# BB#45:                                #   in Loop: Header=BB5_44 Depth=4
	addq	%rdi, %rsi
	movq	%rdi, %rbp
	imulq	%r15, %rbp
	addq	%rbp, %rax
	movq	%rdi, %rbp
	imulq	%r11, %rbp
	addq	%rbp, %rcx
	movslq	%r14d, %rbp
	imulq	%rdi, %rbp
	addq	%rbp, %rbx
.LBB5_46:                               #   in Loop: Header=BB5_44 Depth=4
	addq	$2, %r10
	incq	%r8
	addl	$8, %r14d
	cmpq	%rdx, %r8
	jl	.LBB5_44
# BB#47:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB5_43 Depth=3
	cmpq	%r13, %r9
	leaq	1(%r9), %r9
	movq	%r12, %rdi
	jl	.LBB5_43
# BB#48:                                # %._crit_edge114.i.i
                                        #   in Loop: Header=BB5_42 Depth=2
	movq	96(%rsp), %r11          # 8-byte Reload
	movq	88(%rsp), %r10          # 8-byte Reload
	cmpq	%r10, %r11
	leaq	1(%r11), %r11
	movq	80(%rsp), %r8           # 8-byte Reload
	jl	.LBB5_42
	jmp	.LBB5_49
	.p2align	4, 0x90
.LBB5_37:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_42 Depth 2
                                        #       Child Loop BB5_43 Depth 3
                                        #         Child Loop BB5_44 Depth 4
	leaq	(%r9,%r9,4), %rax
	movslq	(%r15,%rax,8), %r11
	movslq	4(%r15,%rax,8), %r10
	cmpl	%r10d, %r11d
	movq	%r9, 64(%rsp)           # 8-byte Spill
	jg	.LBB5_38
# BB#39:                                # %.preheader.lr.ph.i.i
                                        #   in Loop: Header=BB5_37 Depth=1
	movslq	8(%r15,%rax,8), %r8
	movslq	12(%r15,%rax,8), %r13
	cmpl	%r13d, %r8d
	jg	.LBB5_38
# BB#40:                                # %.preheader.preheader.i.i
                                        #   in Loop: Header=BB5_37 Depth=1
	movslq	16(%r15,%rax,8), %rdi
	movslq	20(%r15,%rax,8), %rdx
	cmpl	%edx, %edi
	jle	.LBB5_41
	.p2align	4, 0x90
.LBB5_38:                               #   in Loop: Header=BB5_37 Depth=1
	xorl	%esi, %esi
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
.LBB5_49:                               # %compute_color.exit.i
                                        #   in Loop: Header=BB5_37 Depth=1
	movq	%rsi, %rdi
	sarq	%rdi
	addq	%rdi, %rax
	cqto
	idivq	%rsi
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	152(%r8), %rdx
	movq	(%rdx), %rdx
	movq	64(%rsp), %r9           # 8-byte Reload
	movb	%al, (%rdx,%r9)
	addq	%rdi, %rcx
	movq	%rcx, %rax
	cqto
	idivq	%rsi
	movq	152(%r8), %rcx
	movq	8(%rcx), %rcx
	movb	%al, (%rcx,%r9)
	addq	%rbx, %rdi
	movq	%rdi, %rax
	cqto
	idivq	%rsi
	movq	152(%r8), %rcx
	movq	16(%rcx), %rcx
	movb	%al, (%rcx,%r9)
	incq	%r9
	cmpq	56(%rsp), %r9           # 8-byte Folded Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	jne	.LBB5_37
# BB#50:
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB5_51:                               # %select_colors.exit
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, 148(%rdi)
	movq	(%rdi), %rax
	movl	$95, 40(%rax)
	movl	%ebp, 44(%rax)
	movl	$1, %esi
	callq	*8(%rax)
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	$1, 56(%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	finish_pass1, .Lfunc_end5-finish_pass1
	.cfi_endproc

	.p2align	4, 0x90
	.type	pass2_fs_dither,@function
pass2_fs_dither:                        # @pass2_fs_dither
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 272
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB6_13
# BB#1:                                 # %.lr.ph221
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	608(%rax), %rdx
	movq	48(%rdx), %r8
	movq	80(%rdx), %r9
	movl	128(%rax), %r14d
	movq	408(%rax), %r10
	movq	152(%rax), %rax
	movq	(%rax), %r11
	movq	8(%rax), %rdi
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	movq	16(%rax), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leal	-1(%r14), %eax
	leal	-3(%r14,%r14,2), %edi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	leal	3(%r14,%r14,2), %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	2(%rax,%rax), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	%r8, 160(%rsp)          # 8-byte Spill
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%r10, 144(%rsp)         # 8-byte Spill
	movq	%r11, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rsi,8), %r12
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rsi,8), %rbp
	cmpl	$0, 72(%rdx)
	je	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	addq	64(%rsp), %r12          # 8-byte Folded Reload
	addq	72(%rsp), %rbp          # 8-byte Folded Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax), %rbx
	addq	64(%rdx), %rbx
	xorl	%ecx, %ecx
	movl	$-3, %r15d
	movq	$-1, 32(%rsp)           # 8-byte Folded Spill
	jmp	.LBB6_5
	.p2align	4, 0x90
.LBB6_4:                                #   in Loop: Header=BB6_2 Depth=1
	movq	64(%rdx), %rbx
	movl	$1, %ecx
	movl	$3, %r15d
	movl	$1, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB6_5:                                #   in Loop: Header=BB6_2 Depth=1
	testl	%r14d, %r14d
	movl	%ecx, 72(%rdx)
	je	.LBB6_6
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	movslq	%r15d, %rcx
	movq	48(%rsp), %rax          # 8-byte Reload
	imulq	%rcx, %rax
	addq	%rbx, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%rbx, 208(%rsp)         # 8-byte Spill
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	leaq	(%rbx,%rcx,2), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movl	$2, %ebx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	xorl	%edi, %edi
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	%r14d, %esi
	movq	%r12, 200(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_8:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movq	184(%rsp), %rbp         # 8-byte Reload
	movswl	-4(%rbp,%rbx,2), %esi
	leal	8(%rax,%rsi), %eax
	sarl	$4, %eax
	movswl	-2(%rbp,%rbx,2), %esi
	leal	8(%rcx,%rsi), %ecx
	sarl	$4, %ecx
	movswl	(%rbp,%rbx,2), %esi
	leal	8(%rdx,%rsi), %edx
	sarl	$4, %edx
	cltq
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	movzbl	-2(%r12,%rbx), %esi
	movslq	(%r9,%rax,4), %rax
	addq	%rsi, %rax
	movzbl	-1(%r12,%rbx), %esi
	movslq	(%r9,%rcx,4), %rcx
	addq	%rsi, %rcx
	movzbl	(%r12,%rbx), %esi
	movslq	(%r9,%rdx,4), %rdx
	addq	%rsi, %rdx
	movzbl	(%r10,%rax), %r12d
	movzbl	(%r10,%rcx), %r14d
	movzbl	(%r10,%rdx), %r13d
	movl	%r12d, %esi
	shrl	$3, %esi
	leal	(,%rsi,8), %edi
	movl	%r14d, %edx
	shrl	$2, %edx
	movl	%r13d, %ecx
	shrl	$3, %ecx
	movq	%rdx, %rax
	shlq	$6, %rax
	addq	(%r8,%rdi), %rax
	movzwl	(%rax,%rcx,2), %edi
	testw	%di, %di
	jne	.LBB6_10
# BB#9:                                 #   in Loop: Header=BB6_8 Depth=2
	leaq	(%rax,%rcx,2), %rbp
	movq	24(%rsp), %rdi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fill_inverse_cmap
	movq	136(%rsp), %r11         # 8-byte Reload
	movq	144(%rsp), %r10         # 8-byte Reload
	movq	152(%rsp), %r9          # 8-byte Reload
	movq	160(%rsp), %r8          # 8-byte Reload
	movzwl	(%rbp), %edi
.LBB6_10:                               #   in Loop: Header=BB6_8 Depth=2
	movzwl	%di, %eax
	leaq	-1(%rax), %rcx
	movq	40(%rsp), %rdi          # 8-byte Reload
	movb	%cl, (%rdi)
	movzbl	-1(%r11,%rax), %ecx
	subl	%ecx, %r12d
	movq	176(%rsp), %rcx         # 8-byte Reload
	movzbl	-1(%rcx,%rax), %ecx
	subl	%ecx, %r14d
	movq	168(%rsp), %rcx         # 8-byte Reload
	movzbl	-1(%rcx,%rax), %eax
	subl	%eax, %r13d
	leal	(%r12,%r12,2), %eax
	addl	%r15d, %eax
	movq	208(%rsp), %rsi         # 8-byte Reload
	movw	%ax, -4(%rsi,%rbx,2)
	leal	(%r12,%r12,4), %r15d
	addl	4(%rsp), %r15d          # 4-byte Folded Reload
	leal	(%r14,%r14,2), %ecx
	addl	20(%rsp), %ecx          # 4-byte Folded Reload
	leal	(,%r12,8), %eax
	subl	%r12d, %eax
	movw	%cx, -2(%rsi,%rbx,2)
	leal	(%r14,%r14,4), %ebp
	addl	8(%rsp), %ebp           # 4-byte Folded Reload
	leal	(%r13,%r13,2), %edx
	addl	(%rsp), %edx            # 4-byte Folded Reload
	leal	(,%r14,8), %ecx
	subl	%r14d, %ecx
	movw	%dx, (%rsi,%rbx,2)
	leal	(%r13,%r13,4), %edx
	addl	12(%rsp), %edx          # 4-byte Folded Reload
	movl	%edx, (%rsp)            # 4-byte Spill
	leal	(,%r13,8), %edx
	subl	%r13d, %edx
	addq	32(%rsp), %rdi          # 8-byte Folded Reload
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	addq	192(%rsp), %rbx         # 8-byte Folded Reload
	movl	16(%rsp), %esi          # 4-byte Reload
	decl	%esi
	movl	%r12d, %edi
	movl	%edi, 4(%rsp)           # 4-byte Spill
	movl	%r14d, %edi
	movl	%edi, 8(%rsp)           # 4-byte Spill
	movl	%r13d, %edi
	movq	200(%rsp), %r12         # 8-byte Reload
	jne	.LBB6_8
# BB#11:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	120(%rsp), %rbx         # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movl	(%rsp), %eax            # 4-byte Reload
	jmp	.LBB6_12
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_2 Depth=1
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
.LBB6_12:                               # %._crit_edge
                                        #   in Loop: Header=BB6_2 Depth=1
	movw	%r15w, (%rbx)
	movw	%bp, 2(%rbx)
	movw	%ax, 4(%rbx)
	incq	%rsi
	cmpq	96(%rsp), %rsi          # 8-byte Folded Reload
	jne	.LBB6_2
.LBB6_13:                               # %._crit_edge222
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	pass2_fs_dither, .Lfunc_end6-pass2_fs_dither
	.cfi_endproc

	.p2align	4, 0x90
	.type	pass2_no_dither,@function
pass2_no_dither:                        # @pass2_no_dither
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 96
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	testl	%ecx, %ecx
	jle	.LBB7_8
# BB#1:
	movl	128(%rbx), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	testl	%eax, %eax
	je	.LBB7_8
# BB#2:                                 # %.lr.ph55.split.preheader
	movq	608(%rbx), %rax
	movq	48(%rax), %rbp
	movl	%ecx, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph55.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %r15
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r13
	movl	4(%rsp), %r12d          # 4-byte Reload
	.p2align	4, 0x90
.LBB7_4:                                #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r15), %esi
	shrl	$3, %esi
	movzbl	1(%r15), %edx
	shrl	$2, %edx
	movzbl	2(%r15), %ecx
	shrl	$3, %ecx
	leal	(,%rsi,8), %eax
	movq	%rdx, %rdi
	shlq	$6, %rdi
	addq	(%rbp,%rax), %rdi
	movzwl	(%rdi,%rcx,2), %eax
	testw	%ax, %ax
	jne	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=2
	leaq	(%rdi,%rcx,2), %r14
	movq	%rbx, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fill_inverse_cmap
	movzwl	(%r14), %eax
.LBB7_6:                                #   in Loop: Header=BB7_4 Depth=2
	decb	%al
	movb	%al, (%r13)
	incq	%r13
	addq	$3, %r15
	decl	%r12d
	jne	.LBB7_4
# BB#7:                                 # %._crit_edge
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	jne	.LBB7_3
.LBB7_8:                                # %._crit_edge56
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	pass2_no_dither, .Lfunc_end7-pass2_no_dither
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_pass2,@function
finish_pass2:                           # @finish_pass2
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	finish_pass2, .Lfunc_end8-finish_pass2
	.cfi_endproc

	.p2align	4, 0x90
	.type	update_box,@function
update_box:                             # @update_box
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 56
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	608(%rdi), %rax
	movq	48(%rax), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movl	(%r12), %r9d
	movl	4(%r12), %r10d
	movslq	%r10d, %rcx
	movl	8(%r12), %r13d
	movslq	%r13d, %r8
	movl	12(%r12), %edi
	movslq	%edi, %r14
	movl	16(%r12), %r11d
	movslq	%r11d, %r15
	movl	20(%r12), %eax
	cmpl	%r9d, %ecx
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	jle	.LBB9_12
# BB#1:                                 # %.preheader245.lr.ph
	cmpl	%edi, %r13d
	jg	.LBB9_12
# BB#2:                                 # %.preheader245.lr.ph
	cmpl	%eax, %r11d
	jg	.LBB9_12
# BB#3:                                 # %.preheader245.preheader
	movq	%r13, -24(%rsp)         # 8-byte Spill
	movslq	%r9d, %rdx
	leal	-1(%r11), %r13d
.LBB9_4:                                # %.preheader245
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_5 Depth 2
                                        #       Child Loop BB9_6 Depth 3
	movq	-32(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rbx
	movq	%r8, %rbp
.LBB9_5:                                # %.lr.ph518
                                        #   Parent Loop BB9_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_6 Depth 3
	movq	%rbp, %rsi
	shlq	$6, %rsi
	addq	%rbx, %rsi
	leaq	(%rsi,%r15,2), %rdi
	movl	%r13d, %esi
	.p2align	4, 0x90
.LBB9_6:                                #   Parent Loop BB9_4 Depth=1
                                        #     Parent Loop BB9_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpw	$0, (%rdi)
	jne	.LBB9_7
# BB#8:                                 #   in Loop: Header=BB9_6 Depth=3
	addq	$2, %rdi
	incl	%esi
	cmpl	%eax, %esi
	jl	.LBB9_6
# BB#9:                                 # %._crit_edge519
                                        #   in Loop: Header=BB9_5 Depth=2
	cmpq	%r14, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB9_5
# BB#10:                                # %._crit_edge522
                                        #   in Loop: Header=BB9_4 Depth=1
	cmpq	%rcx, %rdx
	leaq	1(%rdx), %rdx
	jl	.LBB9_4
	jmp	.LBB9_11
.LBB9_7:                                # %.us-lcssa523
	movl	%edx, (%r12)
	movl	%edx, %r9d
.LBB9_11:
	movq	-24(%rsp), %r13         # 8-byte Reload
	movq	-40(%rsp), %rdi         # 8-byte Reload
.LBB9_12:                               # %.loopexit247
	cmpl	%r9d, %r10d
	jle	.LBB9_24
# BB#13:                                # %.preheader242.lr.ph
	cmpl	%edi, %r13d
	jg	.LBB9_24
# BB#14:                                # %.preheader242.lr.ph
	cmpl	%eax, %r11d
	jg	.LBB9_24
# BB#15:                                # %.preheader242.preheader
	movslq	%r9d, %rdx
	movq	%rdx, -24(%rsp)         # 8-byte Spill
	leal	-1(%r11), %ebp
.LBB9_16:                               # %.preheader242
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_17 Depth 2
                                        #       Child Loop BB9_18 Depth 3
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rcx,8), %rbx
	movq	%r8, %rdx
.LBB9_17:                               # %.lr.ph478
                                        #   Parent Loop BB9_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_18 Depth 3
	movq	%rdx, %rsi
	shlq	$6, %rsi
	addq	%rbx, %rsi
	leaq	(%rsi,%r15,2), %rdi
	movl	%ebp, %esi
	.p2align	4, 0x90
.LBB9_18:                               #   Parent Loop BB9_16 Depth=1
                                        #     Parent Loop BB9_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpw	$0, (%rdi)
	jne	.LBB9_19
# BB#20:                                #   in Loop: Header=BB9_18 Depth=3
	addq	$2, %rdi
	incl	%esi
	cmpl	%eax, %esi
	jl	.LBB9_18
# BB#21:                                # %._crit_edge479
                                        #   in Loop: Header=BB9_17 Depth=2
	cmpq	%r14, %rdx
	leaq	1(%rdx), %rdx
	jl	.LBB9_17
# BB#22:                                # %._crit_edge482
                                        #   in Loop: Header=BB9_16 Depth=1
	cmpq	-24(%rsp), %rcx         # 8-byte Folded Reload
	leaq	-1(%rcx), %rcx
	jg	.LBB9_16
	jmp	.LBB9_23
.LBB9_19:                               # %.us-lcssa483
	movl	%ecx, 4(%r12)
	movl	%ecx, %r10d
.LBB9_23:
	movq	-40(%rsp), %rdi         # 8-byte Reload
.LBB9_24:                               # %.loopexit244
	cmpl	%r13d, %edi
	jle	.LBB9_36
# BB#25:                                # %.preheader239.lr.ph
	cmpl	%r9d, %r10d
	jl	.LBB9_36
# BB#26:                                # %.preheader239.lr.ph
	cmpl	%eax, %r11d
	jg	.LBB9_36
# BB#27:                                # %.preheader239.preheader
	movslq	%r9d, %rcx
	movslq	%r10d, %rdx
	leal	-1(%r11), %ebp
.LBB9_28:                               # %.preheader239
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_29 Depth 2
                                        #       Child Loop BB9_30 Depth 3
	movq	%rcx, %rbx
.LBB9_29:                               # %.lr.ph438
                                        #   Parent Loop BB9_28 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_30 Depth 3
	movq	%r8, %rsi
	shlq	$6, %rsi
	movq	-32(%rsp), %rdi         # 8-byte Reload
	addq	(%rdi,%rbx,8), %rsi
	leaq	(%rsi,%r15,2), %rdi
	movl	%ebp, %esi
	.p2align	4, 0x90
.LBB9_30:                               #   Parent Loop BB9_28 Depth=1
                                        #     Parent Loop BB9_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpw	$0, (%rdi)
	jne	.LBB9_31
# BB#32:                                #   in Loop: Header=BB9_30 Depth=3
	addq	$2, %rdi
	incl	%esi
	cmpl	%eax, %esi
	jl	.LBB9_30
# BB#33:                                # %._crit_edge439
                                        #   in Loop: Header=BB9_29 Depth=2
	cmpq	%rdx, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB9_29
# BB#34:                                # %._crit_edge442
                                        #   in Loop: Header=BB9_28 Depth=1
	cmpq	%r14, %r8
	leaq	1(%r8), %r8
	jl	.LBB9_28
	jmp	.LBB9_35
.LBB9_31:                               # %.us-lcssa443
	movl	%r8d, 8(%r12)
	movl	%r8d, %r13d
.LBB9_35:
	movq	-40(%rsp), %rdi         # 8-byte Reload
.LBB9_36:                               # %.loopexit241
	cmpl	%r13d, %edi
	jle	.LBB9_48
# BB#37:                                # %.preheader236.lr.ph
	cmpl	%r9d, %r10d
	jl	.LBB9_48
# BB#38:                                # %.preheader236.lr.ph
	cmpl	%eax, %r11d
	jg	.LBB9_48
# BB#39:                                # %.preheader236.preheader
	movslq	%r9d, %rbp
	movslq	%r10d, %rdx
	movslq	%r13d, %r8
	leal	-1(%r11), %ebx
.LBB9_40:                               # %.preheader236
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_41 Depth 2
                                        #       Child Loop BB9_42 Depth 3
	movq	%rbp, %rcx
.LBB9_41:                               # %.lr.ph398
                                        #   Parent Loop BB9_40 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_42 Depth 3
	movq	%r14, %rsi
	shlq	$6, %rsi
	movq	-32(%rsp), %rdi         # 8-byte Reload
	addq	(%rdi,%rcx,8), %rsi
	leaq	(%rsi,%r15,2), %rdi
	movl	%ebx, %esi
	.p2align	4, 0x90
.LBB9_42:                               #   Parent Loop BB9_40 Depth=1
                                        #     Parent Loop BB9_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpw	$0, (%rdi)
	jne	.LBB9_43
# BB#44:                                #   in Loop: Header=BB9_42 Depth=3
	addq	$2, %rdi
	incl	%esi
	cmpl	%eax, %esi
	jl	.LBB9_42
# BB#45:                                # %._crit_edge399
                                        #   in Loop: Header=BB9_41 Depth=2
	cmpq	%rdx, %rcx
	leaq	1(%rcx), %rcx
	jl	.LBB9_41
# BB#46:                                # %._crit_edge402
                                        #   in Loop: Header=BB9_40 Depth=1
	cmpq	%r8, %r14
	leaq	-1(%r14), %r14
	jg	.LBB9_40
# BB#47:
	movq	-40(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB9_48
.LBB9_43:                               # %.us-lcssa403
	movl	%r14d, 12(%r12)
	movl	%r14d, %edi
.LBB9_48:                               # %.loopexit238
	movslq	%eax, %rcx
	cmpl	%r11d, %eax
	jle	.LBB9_60
# BB#49:                                # %.preheader233.lr.ph
	cmpl	%r9d, %r10d
	jl	.LBB9_60
# BB#50:                                # %.preheader233.lr.ph
	cmpl	%r13d, %edi
	jl	.LBB9_60
# BB#51:                                # %.preheader233.preheader
	movq	%rdi, %r8
	movslq	%r13d, %rdx
	movslq	%r9d, %r14
	movslq	%r10d, %rsi
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	leal	-1(%r13), %ebx
	shlq	$6, %rdx
.LBB9_52:                               # %.preheader233
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_53 Depth 2
                                        #       Child Loop BB9_54 Depth 3
	movq	%r14, %rbp
.LBB9_53:                               # %.lr.ph358
                                        #   Parent Loop BB9_52 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_54 Depth 3
	movq	-32(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rbp,8), %rsi
	addq	%rdx, %rsi
	leaq	(%rsi,%r15,2), %rdi
	movl	%ebx, %esi
	.p2align	4, 0x90
.LBB9_54:                               #   Parent Loop BB9_52 Depth=1
                                        #     Parent Loop BB9_53 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpw	$0, (%rdi)
	jne	.LBB9_55
# BB#56:                                #   in Loop: Header=BB9_54 Depth=3
	addq	$64, %rdi
	incl	%esi
	cmpl	%r8d, %esi
	jl	.LBB9_54
# BB#57:                                # %._crit_edge359
                                        #   in Loop: Header=BB9_53 Depth=2
	cmpq	-40(%rsp), %rbp         # 8-byte Folded Reload
	leaq	1(%rbp), %rbp
	jl	.LBB9_53
# BB#58:                                # %._crit_edge362
                                        #   in Loop: Header=BB9_52 Depth=1
	cmpq	%rcx, %r15
	leaq	1(%r15), %r15
	jl	.LBB9_52
	jmp	.LBB9_59
.LBB9_55:                               # %.us-lcssa363
	movl	%r15d, 16(%r12)
	movl	%r15d, %r11d
.LBB9_59:
	movq	%r8, %rdi
.LBB9_60:                               # %.loopexit235
	cmpl	%r11d, %eax
	jle	.LBB9_72
# BB#61:                                # %.preheader231.lr.ph
	cmpl	%r9d, %r10d
	jl	.LBB9_72
# BB#62:                                # %.preheader231.lr.ph
	cmpl	%r13d, %edi
	jl	.LBB9_72
# BB#63:                                # %.preheader231.preheader
	movq	%rdi, %r8
	movslq	%r13d, %rdx
	movslq	%r9d, %r14
	movslq	%r10d, %r15
	movslq	%r11d, %rsi
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	leal	-1(%r13), %esi
	shlq	$6, %rdx
.LBB9_64:                               # %.preheader231
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_65 Depth 2
                                        #       Child Loop BB9_66 Depth 3
	movq	%r14, %rbp
.LBB9_65:                               # %.lr.ph319
                                        #   Parent Loop BB9_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_66 Depth 3
	movq	-32(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%rbp,8), %rdi
	addq	%rdx, %rdi
	leaq	(%rdi,%rcx,2), %rdi
	movl	%esi, %ebx
	.p2align	4, 0x90
.LBB9_66:                               #   Parent Loop BB9_64 Depth=1
                                        #     Parent Loop BB9_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpw	$0, (%rdi)
	jne	.LBB9_67
# BB#68:                                #   in Loop: Header=BB9_66 Depth=3
	addq	$64, %rdi
	incl	%ebx
	cmpl	%r8d, %ebx
	jl	.LBB9_66
# BB#69:                                # %._crit_edge320
                                        #   in Loop: Header=BB9_65 Depth=2
	cmpq	%r15, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB9_65
# BB#70:                                # %._crit_edge323
                                        #   in Loop: Header=BB9_64 Depth=1
	cmpq	-40(%rsp), %rcx         # 8-byte Folded Reload
	leaq	-1(%rcx), %rcx
	jg	.LBB9_64
	jmp	.LBB9_71
.LBB9_67:                               # %.us-lcssa
	movl	%ecx, 20(%r12)
	movl	%ecx, %eax
.LBB9_71:
	movq	%r8, %rdi
.LBB9_72:                               # %.loopexit
	movl	%r10d, %ecx
	subl	%r9d, %ecx
	shll	$4, %ecx
	movslq	%ecx, %rcx
	movl	%edi, %edx
	subl	%r13d, %edx
	shll	$2, %edx
	leal	(%rdx,%rdx,2), %edx
	movslq	%edx, %rdx
	movl	%eax, %esi
	subl	%r11d, %esi
	shll	$3, %esi
	movslq	%esi, %rsi
	imulq	%rcx, %rcx
	imulq	%rdx, %rdx
	addq	%rcx, %rdx
	imulq	%rsi, %rsi
	addq	%rdx, %rsi
	cmpl	%r9d, %r10d
	movq	%rsi, 24(%r12)
	movq	%r12, -16(%rsp)         # 8-byte Spill
	jge	.LBB9_74
# BB#73:
	xorl	%ebx, %ebx
	jmp	.LBB9_89
.LBB9_74:                               # %.preheader.lr.ph
	xorl	%ebx, %ebx
	cmpl	%r13d, %edi
	jl	.LBB9_89
# BB#75:                                # %.preheader.lr.ph
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	cmpl	%r11d, %eax
	jl	.LBB9_89
# BB#76:                                # %.preheader.preheader
	movslq	%r11d, %rcx
	movslq	%r13d, %r12
	movq	-40(%rsp), %rsi         # 8-byte Reload
	movslq	%esi, %rdx
	movslq	%r9d, %r9
	movslq	%r10d, %rdi
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	leal	1(%rsi), %r14d
	subl	%r13d, %r14d
	andl	$1, %r14d
	leaq	1(%r12), %rsi
	movq	%rsi, -8(%rsp)          # 8-byte Spill
	decl	%r11d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_77:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_80 Depth 2
                                        #     Child Loop BB9_83 Depth 2
                                        #       Child Loop BB9_84 Depth 3
                                        #       Child Loop BB9_86 Depth 3
	testq	%r14, %r14
	movq	-32(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%r9,8), %rbp
	jne	.LBB9_79
# BB#78:                                #   in Loop: Header=BB9_77 Depth=1
	movq	%r12, %r8
	jmp	.LBB9_82
	.p2align	4, 0x90
.LBB9_79:                               # %.lr.ph.prol
                                        #   in Loop: Header=BB9_77 Depth=1
	movq	%r12, %rsi
	shlq	$6, %rsi
	addq	%rbp, %rsi
	leaq	(%rsi,%rcx,2), %rsi
	movl	%r11d, %edi
	.p2align	4, 0x90
.LBB9_80:                               #   Parent Loop BB9_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpw	$1, (%rsi)
	sbbq	$-1, %rbx
	addq	$2, %rsi
	incl	%edi
	cmpl	%eax, %edi
	jl	.LBB9_80
# BB#81:                                #   in Loop: Header=BB9_77 Depth=1
	movq	-8(%rsp), %r8           # 8-byte Reload
.LBB9_82:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB9_77 Depth=1
	movq	%r13, %r10
	cmpl	%r13d, -40(%rsp)        # 4-byte Folded Reload
	je	.LBB9_88
	.p2align	4, 0x90
.LBB9_83:                               # %.lr.ph
                                        #   Parent Loop BB9_77 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_84 Depth 3
                                        #       Child Loop BB9_86 Depth 3
	movq	%r8, %rsi
	shlq	$6, %rsi
	addq	%rbp, %rsi
	leaq	(%rsi,%rcx,2), %rsi
	movl	%r11d, %edi
	.p2align	4, 0x90
.LBB9_84:                               #   Parent Loop BB9_77 Depth=1
                                        #     Parent Loop BB9_83 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpw	$1, (%rsi)
	sbbq	$-1, %rbx
	addq	$2, %rsi
	incl	%edi
	cmpl	%eax, %edi
	jl	.LBB9_84
# BB#85:                                # %._crit_edge
                                        #   in Loop: Header=BB9_83 Depth=2
	leaq	1(%r8), %rsi
	movq	%rsi, %rdi
	shlq	$6, %rdi
	addq	%rbp, %rdi
	leaq	(%rdi,%rcx,2), %r15
	movl	%r11d, %r13d
	.p2align	4, 0x90
.LBB9_86:                               #   Parent Loop BB9_77 Depth=1
                                        #     Parent Loop BB9_83 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpw	$1, (%r15)
	sbbq	$-1, %rbx
	addq	$2, %r15
	incl	%r13d
	cmpl	%eax, %r13d
	jl	.LBB9_86
# BB#87:                                # %._crit_edge.1
                                        #   in Loop: Header=BB9_83 Depth=2
	addq	$2, %r8
	cmpq	%rdx, %rsi
	jl	.LBB9_83
.LBB9_88:                               # %._crit_edge260
                                        #   in Loop: Header=BB9_77 Depth=1
	cmpq	-24(%rsp), %r9          # 8-byte Folded Reload
	leaq	1(%r9), %r9
	movq	%r10, %r13
	jl	.LBB9_77
.LBB9_89:                               # %._crit_edge264
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	%rbx, 32(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	update_box, .Lfunc_end9-update_box
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.quad	2147483647              # 0x7fffffff
	.quad	2147483647              # 0x7fffffff
	.text
	.p2align	4, 0x90
	.type	fill_inverse_cmap,@function
fill_inverse_cmap:                      # @fill_inverse_cmap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	subq	$2408, %rsp             # imm = 0x968
.Lcfi79:
	.cfi_def_cfa_offset 2464
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	608(%rdi), %rax
	movq	48(%rax), %r8
	sarl	$2, %esi
	sarl	$3, %edx
	sarl	$2, %ecx
	movl	%esi, %ebx
	shll	$5, %ebx
	leal	4(%rbx), %eax
	movl	%eax, -96(%rsp)         # 4-byte Spill
	movl	%edx, %r9d
	shll	$5, %r9d
	leal	2(%r9), %eax
	movl	%eax, -120(%rsp)        # 4-byte Spill
	movl	%ecx, %eax
	shll	$5, %eax
	leal	4(%rax), %r10d
	movl	148(%rdi), %r13d
	testl	%r13d, %r13d
	movl	%esi, -80(%rsp)         # 4-byte Spill
	movl	%edx, -84(%rsp)         # 4-byte Spill
	movl	%ecx, -88(%rsp)         # 4-byte Spill
	movq	%r8, 80(%rsp)           # 8-byte Spill
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	jle	.LBB10_1
# BB#48:                                # %.lr.ph168.i
	leal	28(%rbx), %ecx
	leal	32(%rbx,%rbx), %edx
	sarl	%edx
	movl	%edx, -68(%rsp)         # 4-byte Spill
	leal	30(%r9), %edx
	movl	%edx, -112(%rsp)        # 4-byte Spill
	leal	32(%r9,%r9), %edx
	sarl	%edx
	movl	%edx, -72(%rsp)         # 4-byte Spill
	leal	28(%rax), %r9d
	leal	32(%rax,%rax), %eax
	sarl	%eax
	movl	%eax, -76(%rsp)         # 4-byte Spill
	movq	152(%rdi), %rax
	movq	(%rax), %r12
	movq	8(%rax), %rdi
	movq	16(%rax), %rbx
	leaq	96(%rsp), %rax
	movl	$2147483647, %r8d       # imm = 0x7FFFFFFF
	movq	%r13, 72(%rsp)          # 8-byte Spill
	movl	%ecx, -104(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB10_49:                              # =>This Inner Loop Header: Depth=1
	movq	%r8, %r14
	movzbl	(%r12), %r15d
	movl	-96(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %r15d
	jge	.LBB10_52
# BB#50:                                #   in Loop: Header=BB10_49 Depth=1
	movslq	%edx, %rsi
	movq	%r15, %r11
	subq	%rsi, %r11
	addq	%r11, %r11
	imulq	%r11, %r11
	movslq	%ecx, %rsi
	jmp	.LBB10_51
	.p2align	4, 0x90
.LBB10_52:                              #   in Loop: Header=BB10_49 Depth=1
	cmpl	%ecx, %r15d
	jle	.LBB10_54
# BB#53:                                #   in Loop: Header=BB10_49 Depth=1
	movslq	%ecx, %rsi
	movq	%r15, %r11
	subq	%rsi, %r11
	addq	%r11, %r11
	imulq	%r11, %r11
	movslq	%edx, %rsi
.LBB10_51:                              #   in Loop: Header=BB10_49 Depth=1
	subq	%rsi, %r15
	addq	%r15, %r15
	imulq	%r15, %r15
	jmp	.LBB10_58
	.p2align	4, 0x90
.LBB10_54:                              #   in Loop: Header=BB10_49 Depth=1
	cmpl	-68(%rsp), %r15d        # 4-byte Folded Reload
	jle	.LBB10_55
# BB#56:                                #   in Loop: Header=BB10_49 Depth=1
	movslq	%edx, %rsi
	jmp	.LBB10_57
.LBB10_55:                              #   in Loop: Header=BB10_49 Depth=1
	movslq	%ecx, %rsi
.LBB10_57:                              #   in Loop: Header=BB10_49 Depth=1
	subq	%rsi, %r15
	addq	%r15, %r15
	imulq	%r15, %r15
	xorl	%r11d, %r11d
.LBB10_58:                              #   in Loop: Header=BB10_49 Depth=1
	movl	-120(%rsp), %ebp        # 4-byte Reload
	movl	-112(%rsp), %ecx        # 4-byte Reload
	movzbl	(%rdi), %edx
	cmpl	%ebp, %edx
	jge	.LBB10_60
# BB#59:                                #   in Loop: Header=BB10_49 Depth=1
	movslq	%ebp, %rbp
	movq	%rdx, %rsi
	subq	%rbp, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	imulq	%rsi, %rsi
	addq	%rsi, %r11
	movslq	%ecx, %rsi
	subq	%rsi, %rdx
	jmp	.LBB10_66
	.p2align	4, 0x90
.LBB10_60:                              #   in Loop: Header=BB10_49 Depth=1
	cmpl	%ecx, %edx
	jle	.LBB10_63
# BB#61:                                #   in Loop: Header=BB10_49 Depth=1
	movslq	%ecx, %rsi
	movq	%rdx, %rcx
	subq	%rsi, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	imulq	%rcx, %rcx
	addq	%rcx, %r11
	jmp	.LBB10_62
	.p2align	4, 0x90
.LBB10_63:                              #   in Loop: Header=BB10_49 Depth=1
	cmpl	-72(%rsp), %edx         # 4-byte Folded Reload
	jle	.LBB10_64
.LBB10_62:                              #   in Loop: Header=BB10_49 Depth=1
	movslq	%ebp, %rcx
	jmp	.LBB10_65
.LBB10_64:                              #   in Loop: Header=BB10_49 Depth=1
	movslq	%ecx, %rcx
.LBB10_65:                              #   in Loop: Header=BB10_49 Depth=1
	subq	%rcx, %rdx
.LBB10_66:                              #   in Loop: Header=BB10_49 Depth=1
	leaq	(%rdx,%rdx,2), %r8
	imulq	%r8, %r8
	addq	%r15, %r8
	movzbl	(%rbx), %esi
	cmpl	%r10d, %esi
	jge	.LBB10_68
# BB#67:                                #   in Loop: Header=BB10_49 Depth=1
	movslq	%r10d, %rcx
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	imulq	%rdx, %rdx
	addq	%rdx, %r11
.LBB10_72:                              #   in Loop: Header=BB10_49 Depth=1
	movslq	%r9d, %rcx
	jmp	.LBB10_73
	.p2align	4, 0x90
.LBB10_68:                              #   in Loop: Header=BB10_49 Depth=1
	cmpl	%r9d, %esi
	jle	.LBB10_71
# BB#69:                                #   in Loop: Header=BB10_49 Depth=1
	movslq	%r9d, %rcx
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	imulq	%rdx, %rdx
	addq	%rdx, %r11
	jmp	.LBB10_70
	.p2align	4, 0x90
.LBB10_71:                              #   in Loop: Header=BB10_49 Depth=1
	cmpl	-76(%rsp), %esi         # 4-byte Folded Reload
	jle	.LBB10_72
.LBB10_70:                              #   in Loop: Header=BB10_49 Depth=1
	movslq	%r10d, %rcx
.LBB10_73:                              #   in Loop: Header=BB10_49 Depth=1
	subq	%rcx, %rsi
	imulq	%rsi, %rsi
	movl	-104(%rsp), %ecx        # 4-byte Reload
	addq	%rsi, %r8
	movq	%r11, (%rax)
	cmpq	%r14, %r8
	cmovgq	%r14, %r8
	addq	$8, %rax
	incq	%rbx
	incq	%rdi
	incq	%r12
	decq	%r13
	jne	.LBB10_49
# BB#74:                                # %.lr.ph.i.preheader
	movq	72(%rsp), %rdi          # 8-byte Reload
	testb	$1, %dil
	jne	.LBB10_76
# BB#75:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	cmpl	$1, %edi
	jne	.LBB10_81
	jmp	.LBB10_2
.LBB10_1:
	xorl	%eax, %eax
	jmp	.LBB10_2
.LBB10_76:                              # %.lr.ph.i.prol
	cmpq	%r8, 96(%rsp)
	jle	.LBB10_78
# BB#77:
	xorl	%eax, %eax
	jmp	.LBB10_79
.LBB10_78:
	movb	$0, 2144(%rsp)
	movl	$1, %eax
.LBB10_79:                              # %.lr.ph.i.prol.loopexit
	movl	$1, %ecx
	cmpl	$1, %edi
	je	.LBB10_2
	.p2align	4, 0x90
.LBB10_81:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r8, 96(%rsp,%rcx,8)
	jg	.LBB10_83
# BB#82:                                #   in Loop: Header=BB10_81 Depth=1
	movslq	%eax, %rdx
	incl	%eax
	movb	%cl, 2144(%rsp,%rdx)
.LBB10_83:                              # %.lr.ph.i.178
                                        #   in Loop: Header=BB10_81 Depth=1
	cmpq	%r8, 104(%rsp,%rcx,8)
	jg	.LBB10_85
# BB#84:                                #   in Loop: Header=BB10_81 Depth=1
	leal	1(%rcx), %edx
	movslq	%eax, %rsi
	incl	%eax
	movb	%dl, 2144(%rsp,%rsi)
.LBB10_85:                              #   in Loop: Header=BB10_81 Depth=1
	addq	$2, %rcx
	cmpq	%rcx, %rdi
	jne	.LBB10_81
.LBB10_2:                               # %vector.body
	movaps	.LCPI10_0(%rip), %xmm0  # xmm0 = [2147483647,2147483647]
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 176(%rsp)
	movaps	%xmm0, 192(%rsp)
	movaps	%xmm0, 208(%rsp)
	movaps	%xmm0, 224(%rsp)
	movaps	%xmm0, 240(%rsp)
	movaps	%xmm0, 256(%rsp)
	movaps	%xmm0, 272(%rsp)
	movaps	%xmm0, 288(%rsp)
	movaps	%xmm0, 304(%rsp)
	movaps	%xmm0, 320(%rsp)
	movaps	%xmm0, 336(%rsp)
	movaps	%xmm0, 352(%rsp)
	movaps	%xmm0, 368(%rsp)
	movaps	%xmm0, 384(%rsp)
	movaps	%xmm0, 400(%rsp)
	movaps	%xmm0, 416(%rsp)
	movaps	%xmm0, 432(%rsp)
	movaps	%xmm0, 448(%rsp)
	movaps	%xmm0, 464(%rsp)
	movaps	%xmm0, 480(%rsp)
	movaps	%xmm0, 496(%rsp)
	movaps	%xmm0, 512(%rsp)
	movaps	%xmm0, 528(%rsp)
	movaps	%xmm0, 544(%rsp)
	movaps	%xmm0, 560(%rsp)
	movaps	%xmm0, 576(%rsp)
	movaps	%xmm0, 592(%rsp)
	movaps	%xmm0, 608(%rsp)
	movaps	%xmm0, 624(%rsp)
	movaps	%xmm0, 640(%rsp)
	movaps	%xmm0, 656(%rsp)
	movaps	%xmm0, 672(%rsp)
	movaps	%xmm0, 688(%rsp)
	movaps	%xmm0, 704(%rsp)
	movaps	%xmm0, 720(%rsp)
	movaps	%xmm0, 736(%rsp)
	movaps	%xmm0, 752(%rsp)
	movaps	%xmm0, 768(%rsp)
	movaps	%xmm0, 784(%rsp)
	movaps	%xmm0, 800(%rsp)
	movaps	%xmm0, 816(%rsp)
	movaps	%xmm0, 832(%rsp)
	movaps	%xmm0, 848(%rsp)
	movaps	%xmm0, 864(%rsp)
	movaps	%xmm0, 880(%rsp)
	movaps	%xmm0, 896(%rsp)
	movaps	%xmm0, 912(%rsp)
	movaps	%xmm0, 928(%rsp)
	movaps	%xmm0, 944(%rsp)
	movaps	%xmm0, 960(%rsp)
	movaps	%xmm0, 976(%rsp)
	movaps	%xmm0, 992(%rsp)
	movaps	%xmm0, 1008(%rsp)
	movaps	%xmm0, 1024(%rsp)
	movaps	%xmm0, 1040(%rsp)
	movaps	%xmm0, 1056(%rsp)
	movaps	%xmm0, 1072(%rsp)
	movaps	%xmm0, 1088(%rsp)
	movaps	%xmm0, 1104(%rsp)
	testl	%eax, %eax
	jle	.LBB10_45
# BB#3:                                 # %.lr.ph.i59
	movl	%eax, %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	xorl	%r11d, %r11d
	movslq	-96(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movslq	-120(%rsp), %rax        # 4-byte Folded Reload
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movslq	%r10d, %rax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	.p2align	4, 0x90
.LBB10_4:                               # %.preheader101.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_5 Depth 2
                                        #     Child Loop BB10_15 Depth 2
                                        #     Child Loop BB10_25 Depth 2
                                        #     Child Loop BB10_35 Depth 2
	movzbl	2144(%rsp,%r11), %edi
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	152(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movzbl	(%rcx,%rdi), %ecx
	movq	-96(%rsp), %r9          # 8-byte Reload
	subq	%rcx, %r9
	leaq	(%r9,%r9), %rcx
	imulq	%rcx, %rcx
	movzbl	(%rdx,%rdi), %edx
	movq	-120(%rsp), %rsi        # 8-byte Reload
	subq	%rdx, %rsi
	leaq	(%rsi,%rsi,2), %rdx
	imulq	%rdx, %rdx
	addq	%rcx, %rdx
	movq	16(%rax), %rax
	movzbl	(%rax,%rdi), %eax
	movq	-112(%rsp), %rcx        # 8-byte Reload
	subq	%rax, %rcx
	movq	%rcx, %r8
	imulq	%r8, %r8
	addq	%rdx, %r8
	shlq	$6, %r9
	leaq	256(%r9), %r14
	leaq	(%rsi,%rsi,8), %rax
	leaq	144(,%rax,8), %r10
	shlq	$4, %rcx
	leaq	64(%rcx), %r15
	leaq	192(%rcx), %r12
	leaq	320(%rcx), %r13
	movl	$8, %eax
	leaq	120(%rsp), %rsi
	leaq	-61(%rsp), %rcx
	movq	%r10, %rbp
	movq	%r8, %rbx
	.p2align	4, 0x90
.LBB10_5:                               # %.preheader.i61
                                        #   Parent Loop BB10_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	-24(%rsi), %rbx
	jge	.LBB10_7
# BB#6:                                 #   in Loop: Header=BB10_5 Depth=2
	movq	%rbx, -24(%rsi)
	movb	%dil, -3(%rcx)
.LBB10_7:                               #   in Loop: Header=BB10_5 Depth=2
	leaq	(%r15,%rbx), %rdx
	cmpq	-16(%rsi), %rdx
	jge	.LBB10_9
# BB#8:                                 #   in Loop: Header=BB10_5 Depth=2
	movq	%rdx, -16(%rsi)
	movb	%dil, -2(%rcx)
.LBB10_9:                               #   in Loop: Header=BB10_5 Depth=2
	addq	%r12, %rdx
	cmpq	-8(%rsi), %rdx
	jge	.LBB10_11
# BB#10:                                #   in Loop: Header=BB10_5 Depth=2
	movq	%rdx, -8(%rsi)
	movb	%dil, -1(%rcx)
.LBB10_11:                              #   in Loop: Header=BB10_5 Depth=2
	addq	%r13, %rdx
	cmpq	(%rsi), %rdx
	jge	.LBB10_13
# BB#12:                                #   in Loop: Header=BB10_5 Depth=2
	movq	%rdx, (%rsi)
	movb	%dil, (%rcx)
.LBB10_13:                              #   in Loop: Header=BB10_5 Depth=2
	addq	%rbp, %rbx
	addq	$288, %rbp              # imm = 0x120
	addq	$4, %rcx
	addq	$32, %rsi
	decl	%eax
	jg	.LBB10_5
# BB#14:                                # %.preheader101.i.172
                                        #   in Loop: Header=BB10_4 Depth=1
	addq	%r14, %r8
	leaq	768(%r9), %rdx
	movl	$8, %eax
	leaq	376(%rsp), %r14
	leaq	-29(%rsp), %rsi
	movq	%r10, %rcx
	movq	%r8, %rbp
	.p2align	4, 0x90
.LBB10_15:                              # %.preheader.i61.1
                                        #   Parent Loop BB10_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	-24(%r14), %rbp
	jge	.LBB10_17
# BB#16:                                #   in Loop: Header=BB10_15 Depth=2
	movq	%rbp, -24(%r14)
	movb	%dil, -3(%rsi)
.LBB10_17:                              #   in Loop: Header=BB10_15 Depth=2
	leaq	(%r15,%rbp), %rbx
	cmpq	-16(%r14), %rbx
	jge	.LBB10_19
# BB#18:                                #   in Loop: Header=BB10_15 Depth=2
	movq	%rbx, -16(%r14)
	movb	%dil, -2(%rsi)
.LBB10_19:                              #   in Loop: Header=BB10_15 Depth=2
	addq	%r12, %rbx
	cmpq	-8(%r14), %rbx
	jge	.LBB10_21
# BB#20:                                #   in Loop: Header=BB10_15 Depth=2
	movq	%rbx, -8(%r14)
	movb	%dil, -1(%rsi)
.LBB10_21:                              #   in Loop: Header=BB10_15 Depth=2
	addq	%r13, %rbx
	cmpq	(%r14), %rbx
	jge	.LBB10_23
# BB#22:                                #   in Loop: Header=BB10_15 Depth=2
	movq	%rbx, (%r14)
	movb	%dil, (%rsi)
.LBB10_23:                              #   in Loop: Header=BB10_15 Depth=2
	addq	%rcx, %rbp
	addq	$288, %rcx              # imm = 0x120
	addq	$4, %rsi
	addq	$32, %r14
	decl	%eax
	jg	.LBB10_15
# BB#24:                                # %.preheader101.i.273
                                        #   in Loop: Header=BB10_4 Depth=1
	addq	%rdx, %r8
	addq	$1280, %r9              # imm = 0x500
	movl	$8, %eax
	leaq	632(%rsp), %rsi
	leaq	3(%rsp), %rcx
	movq	%r10, %rbp
	movq	%r8, %rbx
	.p2align	4, 0x90
.LBB10_25:                              # %.preheader.i61.2
                                        #   Parent Loop BB10_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	-24(%rsi), %rbx
	jge	.LBB10_27
# BB#26:                                #   in Loop: Header=BB10_25 Depth=2
	movq	%rbx, -24(%rsi)
	movb	%dil, -3(%rcx)
.LBB10_27:                              #   in Loop: Header=BB10_25 Depth=2
	leaq	(%r15,%rbx), %rdx
	cmpq	-16(%rsi), %rdx
	jge	.LBB10_29
# BB#28:                                #   in Loop: Header=BB10_25 Depth=2
	movq	%rdx, -16(%rsi)
	movb	%dil, -2(%rcx)
.LBB10_29:                              #   in Loop: Header=BB10_25 Depth=2
	addq	%r12, %rdx
	cmpq	-8(%rsi), %rdx
	jge	.LBB10_31
# BB#30:                                #   in Loop: Header=BB10_25 Depth=2
	movq	%rdx, -8(%rsi)
	movb	%dil, -1(%rcx)
.LBB10_31:                              #   in Loop: Header=BB10_25 Depth=2
	addq	%r13, %rdx
	cmpq	(%rsi), %rdx
	jge	.LBB10_33
# BB#32:                                #   in Loop: Header=BB10_25 Depth=2
	movq	%rdx, (%rsi)
	movb	%dil, (%rcx)
.LBB10_33:                              #   in Loop: Header=BB10_25 Depth=2
	addq	%rbp, %rbx
	addq	$288, %rbp              # imm = 0x120
	addq	$4, %rcx
	addq	$32, %rsi
	decl	%eax
	jg	.LBB10_25
# BB#34:                                # %.preheader101.i.374
                                        #   in Loop: Header=BB10_4 Depth=1
	addq	%r9, %r8
	movl	$8, %eax
	leaq	888(%rsp), %rsi
	leaq	35(%rsp), %rcx
	.p2align	4, 0x90
.LBB10_35:                              # %.preheader.i61.3
                                        #   Parent Loop BB10_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	-24(%rsi), %r8
	jge	.LBB10_37
# BB#36:                                #   in Loop: Header=BB10_35 Depth=2
	movq	%r8, -24(%rsi)
	movb	%dil, -3(%rcx)
.LBB10_37:                              #   in Loop: Header=BB10_35 Depth=2
	leaq	(%r15,%r8), %rdx
	cmpq	-16(%rsi), %rdx
	jge	.LBB10_39
# BB#38:                                #   in Loop: Header=BB10_35 Depth=2
	movq	%rdx, -16(%rsi)
	movb	%dil, -2(%rcx)
.LBB10_39:                              #   in Loop: Header=BB10_35 Depth=2
	addq	%r12, %rdx
	cmpq	-8(%rsi), %rdx
	jge	.LBB10_41
# BB#40:                                #   in Loop: Header=BB10_35 Depth=2
	movq	%rdx, -8(%rsi)
	movb	%dil, -1(%rcx)
.LBB10_41:                              #   in Loop: Header=BB10_35 Depth=2
	addq	%r13, %rdx
	cmpq	(%rsi), %rdx
	jge	.LBB10_43
# BB#42:                                #   in Loop: Header=BB10_35 Depth=2
	movq	%rdx, (%rsi)
	movb	%dil, (%rcx)
.LBB10_43:                              #   in Loop: Header=BB10_35 Depth=2
	addq	%r10, %r8
	addq	$288, %r10              # imm = 0x120
	addq	$4, %rcx
	addq	$32, %rsi
	decl	%eax
	jg	.LBB10_35
# BB#44:                                #   in Loop: Header=BB10_4 Depth=1
	incq	%r11
	cmpq	-104(%rsp), %r11        # 8-byte Folded Reload
	jne	.LBB10_4
.LBB10_45:                              # %find_best_colors.exit
	movl	-80(%rsp), %ecx         # 4-byte Reload
	shll	$2, %ecx
	movl	-84(%rsp), %edx         # 4-byte Reload
	shll	$3, %edx
	movl	-88(%rsp), %eax         # 4-byte Reload
	shll	$2, %eax
	cltq
	movslq	%edx, %rbx
	movslq	%ecx, %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	xorl	%edx, %edx
	leaq	1(%rbx), %r8
	leaq	2(%rbx), %r9
	leaq	3(%rbx), %r10
	leaq	4(%rbx), %r11
	leaq	5(%rbx), %r14
	leaq	6(%rbx), %r15
	leaq	7(%rbx), %r12
	shlq	$6, %rbx
	shlq	$6, %r8
	shlq	$6, %r9
	shlq	$6, %r10
	shlq	$6, %r11
	shlq	$6, %r14
	shlq	$6, %r15
	shlq	$6, %r12
	.p2align	4, 0x90
.LBB10_46:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx), %rsi
	leaq	(%rsi,%rbx), %rdi
	movzbl	-64(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, (%rdi,%rax,2)
	movzbl	-63(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 2(%rdi,%rax,2)
	movzbl	-62(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 4(%rdi,%rax,2)
	movzbl	-61(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 6(%rdi,%rax,2)
	leaq	(%rsi,%r8), %rdi
	movzbl	-60(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, (%rdi,%rax,2)
	movzbl	-59(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 2(%rdi,%rax,2)
	movzbl	-58(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 4(%rdi,%rax,2)
	movzbl	-57(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 6(%rdi,%rax,2)
	leaq	(%rsi,%r9), %rdi
	movzbl	-56(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, (%rdi,%rax,2)
	movzbl	-55(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 2(%rdi,%rax,2)
	movzbl	-54(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 4(%rdi,%rax,2)
	movzbl	-53(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 6(%rdi,%rax,2)
	addq	%r10, %rsi
	movzbl	-52(%rsp,%rdx,4), %edi
	incl	%edi
	movw	%di, (%rsi,%rax,2)
	movzbl	-51(%rsp,%rdx,4), %edi
	incl	%edi
	movw	%di, 2(%rsi,%rax,2)
	movzbl	-50(%rsp,%rdx,4), %edi
	incl	%edi
	movw	%di, 4(%rsi,%rax,2)
	movzbl	-49(%rsp,%rdx,4), %edi
	incl	%edi
	movw	%di, 6(%rsi,%rax,2)
	movq	(%rcx,%rdx), %rsi
	leaq	(%rsi,%r11), %rdi
	movzbl	-48(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, (%rdi,%rax,2)
	movzbl	-47(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 2(%rdi,%rax,2)
	movzbl	-46(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 4(%rdi,%rax,2)
	movzbl	-45(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 6(%rdi,%rax,2)
	leaq	(%rsi,%r14), %rdi
	movzbl	-44(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, (%rdi,%rax,2)
	movzbl	-43(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 2(%rdi,%rax,2)
	movzbl	-42(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 4(%rdi,%rax,2)
	movzbl	-41(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 6(%rdi,%rax,2)
	leaq	(%rsi,%r15), %rdi
	movzbl	-40(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, (%rdi,%rax,2)
	movzbl	-39(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 2(%rdi,%rax,2)
	movzbl	-38(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 4(%rdi,%rax,2)
	movzbl	-37(%rsp,%rdx,4), %ebp
	incl	%ebp
	movw	%bp, 6(%rdi,%rax,2)
	addq	%r12, %rsi
	movzbl	-36(%rsp,%rdx,4), %edi
	incl	%edi
	movw	%di, (%rsi,%rax,2)
	movzbl	-35(%rsp,%rdx,4), %edi
	incl	%edi
	movw	%di, 2(%rsi,%rax,2)
	movzbl	-34(%rsp,%rdx,4), %edi
	incl	%edi
	movw	%di, 4(%rsi,%rax,2)
	movzbl	-33(%rsp,%rdx,4), %edi
	incl	%edi
	movw	%di, 6(%rsi,%rax,2)
	addq	$8, %rdx
	cmpq	$32, %rdx
	jne	.LBB10_46
# BB#47:
	addq	$2408, %rsp             # imm = 0x968
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	fill_inverse_cmap, .Lfunc_end10-fill_inverse_cmap
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
