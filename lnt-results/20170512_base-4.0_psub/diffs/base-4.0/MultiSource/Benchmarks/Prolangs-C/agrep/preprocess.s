	.text
	.file	"preprocess.bc"
	.globl	preprocess
	.p2align	4, 0x90
	.type	preprocess,@function
preprocess:                             # @preprocess
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1064, %rsp             # imm = 0x428
.Lcfi6:
	.cfi_def_cfa_offset 1120
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r13
	movq	%r15, %rdi
	callq	strlen
	testl	%eax, %eax
	jle	.LBB0_8
# BB#1:                                 # %.lr.ph260.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph260
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rdx
	movzbl	(%r15,%rdx), %edx
	cmpb	$42, %dl
	je	.LBB0_6
# BB#3:                                 # %.lr.ph260
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpb	$124, %dl
	je	.LBB0_6
# BB#4:                                 # %.lr.ph260
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpb	$92, %dl
	jne	.LBB0_7
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	incl	%ecx
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$1, REGEX(%rip)
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	incl	%ecx
	cmpl	%eax, %ecx
	jl	.LBB0_2
.LBB0_8:                                # %._crit_edge261
	movabsq	$8589934592, %r12       # imm = 0x200000000
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	strlen
	leaq	(%rbx,%rax,2), %rdi
	callq	malloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	32(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
	cmpl	$0, WHOLELINE(%rip)
	je	.LBB0_10
# BB#9:
	movq	%r14, %rax
	shlq	$32, %rax
	movslq	%r14d, %rcx
	movb	$-10, 32(%rsp,%rcx)
	leaq	(%rax,%rbp), %rcx
	sarq	$32, %rcx
	movb	$-17, 32(%rsp,%rcx)
	movq	%r12, %rbx
	leaq	(%rax,%rbx), %rcx
	sarq	$32, %rcx
	movb	$-9, 32(%rsp,%rcx)
	movabsq	$12884901888, %rcx      # imm = 0x300000000
	addq	%rcx, %rax
	sarq	$32, %rax
	movb	$0, 32(%rsp,%rax)
	leaq	32(%rsp), %r12
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	strcat
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %r12
	movb	$10, %al
	jmp	.LBB0_15
.LBB0_10:
	movabsq	$12884901888, %rdx      # imm = 0x300000000
	cmpl	$0, WORDBOUND(%rip)
	movq	%r12, %rbx
	je	.LBB0_12
# BB#11:
	movq	%r14, %rax
	shlq	$32, %rax
	movslq	%r14d, %rcx
	movb	$-10, 32(%rsp,%rcx)
	leaq	(%rax,%rbp), %rcx
	sarq	$32, %rcx
	movb	$-15, 32(%rsp,%rcx)
	leaq	(%rax,%rbx), %rcx
	sarq	$32, %rcx
	movb	$-9, 32(%rsp,%rcx)
	addq	%rdx, %rax
	sarq	$32, %rax
	movb	$0, 32(%rsp,%rax)
.LBB0_12:
	leaq	32(%rsp), %r12
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	strcat
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %r12
	cmpl	$0, WORDBOUND(%rip)
	je	.LBB0_13
# BB#14:
	movb	$-15, %al
.LBB0_15:                               # %.sink.split
	movq	%r13, %rdi
	movq	%r12, %rcx
	shlq	$32, %rcx
	movslq	%r12d, %rdx
	movb	$-10, 32(%rsp,%rdx)
	addq	%rcx, %rbp
	sarq	$32, %rbp
	movb	%al, 32(%rsp,%rbp)
	addl	$3, %r12d
	addq	%rbx, %rcx
	sarq	$32, %rcx
	movb	$-9, 32(%rsp,%rcx)
.LBB0_16:
	movslq	%r12d, %rax
	movb	$0, 32(%rsp,%rax)
	movl	$0, D_length(%rip)
	xorl	%ebp, %ebp
	cmpl	$3, %r14d
	movl	$0, %r13d
	jl	.LBB0_31
# BB#17:                                # %.lr.ph255.preheader
	leal	-2(%r14), %esi
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph255
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rdx
	movzbl	32(%rsp,%rdx), %ebx
	movl	%ebx, %eax
	addb	$-60, %al
	cmpb	$34, %al
	ja	.LBB0_19
# BB#21:                                # %.lr.ph255
                                        #   in Loop: Header=BB0_18 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_23:                               #   in Loop: Header=BB0_18 Depth=1
	movb	$-10, (%r15,%r13)
	jmp	.LBB0_27
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph255
                                        #   in Loop: Header=BB0_18 Depth=1
	cmpb	$36, %bl
	jne	.LBB0_26
# BB#20:                                #   in Loop: Header=BB0_18 Depth=1
	movb	$10, (%r15,%r13)
	movslq	D_length(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, D_length(%rip)
	movb	$36, (%rsp,%rax)
	jmp	.LBB0_27
.LBB0_26:                               #   in Loop: Header=BB0_18 Depth=1
	movb	%bl, (%r15,%r13)
	movslq	D_length(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, D_length(%rip)
	movb	%bl, (%rsp,%rax)
	jmp	.LBB0_27
.LBB0_24:                               #   in Loop: Header=BB0_18 Depth=1
	movb	$-9, (%r15,%r13)
	jmp	.LBB0_27
.LBB0_22:                               #   in Loop: Header=BB0_18 Depth=1
	movzbl	33(%rsp,%rdx), %eax
	incq	%rdx
	movb	%al, (%r15,%r13)
	movslq	D_length(%rip), %rcx
	leal	1(%rcx), %ebx
	movl	%ebx, D_length(%rip)
	movb	%al, (%rsp,%rcx)
	movl	%edx, %ecx
	jmp	.LBB0_27
.LBB0_25:                               #   in Loop: Header=BB0_18 Depth=1
	movb	$10, (%r15,%r13)
	movslq	D_length(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, D_length(%rip)
	movb	$94, (%rsp,%rax)
	.p2align	4, 0x90
.LBB0_27:                               #   in Loop: Header=BB0_18 Depth=1
	incq	%r13
	incl	%ecx
	cmpl	%esi, %ecx
	jl	.LBB0_18
# BB#28:                                # %._crit_edge256
	cmpl	$8, D_length(%rip)
	jg	.LBB0_29
.LBB0_31:                               # %._crit_edge256.thread
	movslq	%r13d, %rbx
	movb	$-4, (%r15,%rbx)
	movslq	D_length(%rip), %rax
	movb	$0, (%rsp,%rax)
	movq	%rsp, %rsi
	callq	strcpy
	incl	D_length(%rip)
	movb	$0, 1(%r15,%rbx)
	leaq	1(%rbx), %rdx
	cmpl	$0, REGEX(%rip)
	je	.LBB0_32
# BB#33:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movb	$46, (%rdi)
	movb	$40, 1(%rdi)
	addl	$2, %r13d
	movb	$-18, (%r15,%rdx)
	movl	$1, HEAD(%rip)
	movl	$2, %ebp
	movl	%r13d, %edx
	cmpl	%r12d, %r14d
	jl	.LBB0_36
	jmp	.LBB0_35
.LBB0_32:
	movq	24(%rsp), %rdi          # 8-byte Reload
	cmpl	%r12d, %r14d
	jge	.LBB0_35
.LBB0_36:                               # %.lr.ph249.preheader
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.LBB0_37
.LBB0_35:
	movl	%edx, %esi
	cmpl	$0, REGEX(%rip)
	jne	.LBB0_68
	jmp	.LBB0_69
.LBB0_42:                               #   in Loop: Header=BB0_37 Depth=1
	movb	$-7, (%r15,%rax)
	jmp	.LBB0_66
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph249
                                        # =>This Inner Loop Header: Depth=1
	movslq	%r14d, %rbx
	movzbl	32(%rsp,%rbx), %eax
	movl	%eax, %ecx
	addb	$17, %cl
	cmpb	$-115, %cl
	ja	.LBB0_64
# BB#38:                                # %.lr.ph249
                                        #   in Loop: Header=BB0_37 Depth=1
	movzbl	%cl, %ecx
	jmpq	*.LJTI0_1(,%rcx,8)
.LBB0_63:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-17, (%r15,%rax)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$78, (%rdi,%rax)
	jmp	.LBB0_65
.LBB0_64:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rcx
	incl	%edx
	movb	%al, (%r15,%rcx)
	movslq	%ebp, %rcx
	incl	%ebp
	movb	%al, (%rdi,%rcx)
	jmp	.LBB0_65
.LBB0_40:                               #   in Loop: Header=BB0_37 Depth=1
	leal	1(%rdx), %esi
	cmpl	$0, REGEX(%rip)
	movslq	%edx, %rax
	je	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_37 Depth=1
	movb	$-18, (%r15,%rax)
	movslq	%ebp, %rax
	movw	$10798, (%rdi,%rax)     # imm = 0x2A2E
	addl	$2, %ebp
	jmp	.LBB0_66
.LBB0_52:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$10, (%r15,%rax)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$36, (%rdi,%rax)
	jmp	.LBB0_65
.LBB0_43:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-14, (%r15,%rax)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$40, (%rdi,%rax)
	jmp	.LBB0_65
.LBB0_44:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-13, (%r15,%rax)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$41, (%rdi,%rax)
	jmp	.LBB0_65
.LBB0_54:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-3, (%r15,%rax)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$42, (%rdi,%rax)
	jmp	.LBB0_65
.LBB0_56:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-5, (%r15,%rax)
	movl	$1, RE_ERR(%rip)
	jmp	.LBB0_65
.LBB0_60:                               #   in Loop: Header=BB0_37 Depth=1
	testl	%r9d, %r9d
	movslq	%edx, %rax
	movb	$45, %cl
	je	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_37 Depth=1
	movb	$-19, %cl
.LBB0_62:                               #   in Loop: Header=BB0_37 Depth=1
	movb	%cl, (%r15,%rax)
	incl	%edx
	movslq	%ebp, %rax
	incl	%ebp
	movb	$45, (%rdi,%rax)
	jmp	.LBB0_65
.LBB0_53:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-18, (%r15,%rax)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$46, (%rdi,%rax)
	jmp	.LBB0_65
.LBB0_57:                               #   in Loop: Header=BB0_37 Depth=1
	testl	%r8d, %r8d
	je	.LBB0_59
# BB#58:                                #   in Loop: Header=BB0_37 Depth=1
	movl	$1, RE_ERR(%rip)
.LBB0_59:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-4, (%r15,%rax)
	movl	$1, %r8d
	jmp	.LBB0_65
.LBB0_47:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-10, (%r15,%rax)
	jmp	.LBB0_65
.LBB0_48:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-9, (%r15,%rax)
	jmp	.LBB0_65
.LBB0_45:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-12, (%r15,%rax)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$91, (%rdi,%rax)
	movl	$1, %r9d
	jmp	.LBB0_65
.LBB0_39:                               #   in Loop: Header=BB0_37 Depth=1
	movzbl	33(%rsp,%rbx), %eax
	incq	%rbx
	movslq	%edx, %rcx
	incl	%edx
	movb	%al, (%r15,%rcx)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$111, (%rdi,%rax)
	movl	%edx, %esi
	movl	%ebx, %r14d
	jmp	.LBB0_66
.LBB0_46:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-11, (%r15,%rax)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$93, (%rdi,%rax)
	xorl	%r9d, %r9d
	jmp	.LBB0_65
.LBB0_49:                               #   in Loop: Header=BB0_37 Depth=1
	cmpb	$91, 31(%rsp,%rbx)
	movslq	%edx, %rax
	movb	$-8, %cl
	je	.LBB0_51
# BB#50:                                #   in Loop: Header=BB0_37 Depth=1
	movb	$10, %cl
.LBB0_51:                               #   in Loop: Header=BB0_37 Depth=1
	incl	%edx
	movb	%cl, (%r15,%rax)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$94, (%rdi,%rax)
	jmp	.LBB0_65
.LBB0_55:                               #   in Loop: Header=BB0_37 Depth=1
	movslq	%edx, %rax
	incl	%edx
	movb	$-6, (%r15,%rax)
	movslq	%ebp, %rax
	incl	%ebp
	movb	$124, (%rdi,%rax)
	.p2align	4, 0x90
.LBB0_65:                               #   in Loop: Header=BB0_37 Depth=1
	movl	%edx, %esi
.LBB0_66:                               #   in Loop: Header=BB0_37 Depth=1
	incl	%r14d
	cmpl	%r12d, %r14d
	movl	%esi, %edx
	jl	.LBB0_37
# BB#67:                                # %._crit_edge
	cmpl	$0, REGEX(%rip)
	je	.LBB0_69
.LBB0_68:
	movslq	%ebp, %rax
	movb	$41, (%rdi,%rax)
	leal	2(%rax), %ebp
	movb	$46, 1(%rdi,%rax)
	movslq	%esi, %rax
	incl	%esi
	movb	$-18, (%r15,%rax)
	movl	$1, TAIL(%rip)
.LBB0_69:
	movslq	%esi, %rax
	movb	$0, (%r15,%rax)
	movslq	%ebp, %rax
	movb	$0, (%rdi,%rax)
	cmpl	$0, REGEX(%rip)
	je	.LBB0_83
# BB#70:
	movl	WORDBOUND(%rip), %eax
	orl	DELIMITER(%rip), %eax
	jne	.LBB0_71
# BB#72:
	cmpl	$0, RE_ERR(%rip)
	jne	.LBB0_79
# BB#73:                                # %.preheader
	testl	%esi, %esi
	movq	%r15, %rbx
	jle	.LBB0_78
# BB#74:                                # %.preheader
	cmpb	$-18, (%r15)
	movq	%r15, %rbx
	je	.LBB0_78
# BB#75:
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB0_76:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rax), %rbx
	cmpl	$2, %esi
	jl	.LBB0_78
# BB#77:                                # %.lr.ph
                                        #   in Loop: Header=BB0_76 Depth=1
	decl	%esi
	cmpb	$-18, 1(%rax)
	movq	%rbx, %rax
	jne	.LBB0_76
.LBB0_78:                               # %.critedge
	movl	$table, %esi
	callq	init
	testl	%eax, %eax
	jle	.LBB0_79
# BB#80:
	cmpl	$31, %eax
	jge	.LBB0_81
# BB#82:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	strcpy
.LBB0_83:
	addq	$1064, %rsp             # imm = 0x428
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_13:
	movq	%r13, %rdi
	jmp	.LBB0_16
.LBB0_79:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	jmp	.LBB0_30
.LBB0_71:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	jmp	.LBB0_30
.LBB0_81:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	jmp	.LBB0_30
.LBB0_29:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
.LBB0_30:
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.Lfunc_end0:
	.size	preprocess, .Lfunc_end0-preprocess
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_23
	.quad	.LBB0_26
	.quad	.LBB0_24
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_26
	.quad	.LBB0_22
	.quad	.LBB0_26
	.quad	.LBB0_25
.LJTI0_1:
	.quad	.LBB0_63
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_40
	.quad	.LBB0_52
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_43
	.quad	.LBB0_44
	.quad	.LBB0_54
	.quad	.LBB0_64
	.quad	.LBB0_56
	.quad	.LBB0_60
	.quad	.LBB0_53
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_57
	.quad	.LBB0_47
	.quad	.LBB0_64
	.quad	.LBB0_48
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_45
	.quad	.LBB0_39
	.quad	.LBB0_46
	.quad	.LBB0_49
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_55

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s: delimiter pattern too long\n"
	.size	.L.str, 32

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%s: -d or -w option is not supported for this pattern\n"
	.size	.L.str.1, 55

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s: illegal regular expression\n"
	.size	.L.str.2, 32

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s: regular expression too long\n"
	.size	.L.str.3, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
