	.text
	.file	"DeflateProps.bc"
	.globl	_ZN8NArchive13CDeflateProps9NormalizeEv
	.p2align	4, 0x90
	.type	_ZN8NArchive13CDeflateProps9NormalizeEv,@function
_ZN8NArchive13CDeflateProps9NormalizeEv: # @_ZN8NArchive13CDeflateProps9NormalizeEv
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	cmpl	$-1, %ecx
	movl	$5, %eax
	cmovnel	%ecx, %eax
	cmpl	$-1, 12(%rdi)
	jne	.LBB0_2
# BB#1:
	xorl	%ecx, %ecx
	cmpl	$4, %eax
	seta	%cl
	movl	%ecx, 12(%rdi)
.LBB0_2:
	cmpl	$-1, 4(%rdi)
	jne	.LBB0_4
# BB#3:
	xorl	%ecx, %ecx
	cmpl	$6, %eax
	seta	%cl
	cmpl	$8, %eax
	leal	1(%rcx,%rcx), %ecx
	movl	$10, %edx
	cmovbel	%ecx, %edx
	movl	%edx, 4(%rdi)
.LBB0_4:
	cmpl	$-1, 8(%rdi)
	jne	.LBB0_6
# BB#5:
	cmpl	$6, %eax
	movl	$64, %ecx
	movl	$32, %edx
	cmoval	%ecx, %edx
	cmpl	$8, %eax
	movl	$128, %eax
	cmovbel	%edx, %eax
	movl	%eax, 8(%rdi)
.LBB0_6:
	retq
.Lfunc_end0:
	.size	_ZN8NArchive13CDeflateProps9NormalizeEv, .Lfunc_end0-_ZN8NArchive13CDeflateProps9NormalizeEv
	.cfi_endproc

	.globl	_ZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderProperties
	.p2align	4, 0x90
	.type	_ZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderProperties,@function
_ZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderProperties: # @_ZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderProperties
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$88, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 128
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	(%rdi), %edx
	movl	12(%rdi), %eax
	cmpl	$-1, %edx
	movl	$5, %ecx
	cmovnel	%edx, %ecx
	cmpl	$-1, %eax
	jne	.LBB1_2
# BB#1:
	xorl	%eax, %eax
	cmpl	$4, %ecx
	seta	%al
	movl	%eax, 12(%rdi)
.LBB1_2:
	movl	4(%rdi), %edx
	cmpl	$-1, %edx
	jne	.LBB1_4
# BB#3:
	xorl	%edx, %edx
	cmpl	$6, %ecx
	seta	%dl
	cmpl	$8, %ecx
	leal	1(%rdx,%rdx), %ebp
	movl	$10, %edx
	cmovbel	%ebp, %edx
	movl	%edx, 4(%rdi)
.LBB1_4:
	movl	8(%rdi), %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_6
# BB#5:
	cmpl	$6, %ecx
	movl	$64, %ebp
	movl	$32, %ebx
	cmoval	%ebp, %ebx
	cmpl	$8, %ecx
	movl	$128, %ebp
	cmovbel	%ebx, %ebp
	movl	%ebp, 8(%rdi)
.LBB1_6:                                # %_ZN8NArchive13CDeflateProps9NormalizeEv.exit
	movw	$19, (%rsp)
	movw	$0, 2(%rsp)
	movl	%eax, 8(%rsp)
	movw	$19, 16(%rsp)
	movw	$0, 18(%rsp)
	movl	%edx, 24(%rsp)
	movw	$19, 32(%rsp)
	movw	$0, 34(%rsp)
	movl	%ebp, 40(%rsp)
	movl	16(%rdi), %eax
	movw	$19, 48(%rsp)
	movw	$0, 50(%rsp)
	movl	%eax, 56(%rsp)
	movaps	.L_ZZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderPropertiesE7propIDs(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movzbl	20(%rdi), %ecx
	addl	$3, %ecx
	movq	(%rsi), %rbp
.Ltmp0:
	leaq	64(%rsp), %rax
	movq	%rsp, %r15
	movq	%rsi, %rdi
	movq	%rax, %rsi
	movq	%r15, %rdx
	callq	*40(%rbp)
	movl	%eax, %ebp
.Ltmp1:
# BB#7:
	leaq	48(%rsp), %rbx
.Ltmp12:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp13:
# BB#8:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit11
	leaq	32(%rsp), %rbx
.Ltmp14:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp15:
# BB#9:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit11.1
	leaq	16(%rsp), %rbx
.Ltmp16:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp17:
# BB#10:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit11.2
.Ltmp18:
	movq	%r15, %rbx
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp19:
# BB#11:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit11.3
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_15:
.Ltmp2:
	movq	%rax, %r14
	leaq	48(%rsp), %rdi
.Ltmp3:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp4:
# BB#16:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	leaq	32(%rsp), %rdi
.Ltmp5:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp6:
# BB#17:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit.1
	leaq	16(%rsp), %rdi
.Ltmp7:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp8:
# BB#18:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit.2
.Ltmp9:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp10:
	jmp	.LBB1_19
.LBB1_21:                               # %.loopexit.split-lp
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_12:
.Ltmp20:
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB1_13:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %r15
	je	.LBB1_19
# BB#14:                                # %.preheader
                                        #   in Loop: Header=BB1_13 Depth=1
	addq	$-16, %rbx
.Ltmp21:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp22:
	jmp	.LBB1_13
.LBB1_19:                               # %.loopexit12
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB1_20:                               # %.loopexit
.Ltmp23:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderProperties, .Lfunc_end1-_ZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderProperties
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp12         #   Call between .Ltmp12 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp3          #   Call between .Ltmp3 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin0   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Lfunc_end1-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.zero	16
	.text
	.globl	_ZN8NArchive13CDeflateProps13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZN8NArchive13CDeflateProps13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZN8NArchive13CDeflateProps13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZN8NArchive13CDeflateProps13SetPropertiesEPPKwPK14tagPROPVARIANTi
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 144
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, (%rdi)
	movl	$-1, 16(%rdi)
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movb	$0, 20(%rdi)
	testl	%ecx, %ecx
	jle	.LBB3_1
# BB#4:                                 # %.lr.ph
	movslq	%ecx, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	leaq	16(%rsp), %r14
                                        # implicit-def: %EAX
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_3:                                #   in Loop: Header=BB3_5 Depth=1
	incq	%r15
	cmpq	72(%rsp), %r15          # 8-byte Folded Reload
	jl	.LBB3_5
	jmp	.LBB3_1
.LBB3_84:                               # %.thread140
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	8(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 16(%rcx)
	movb	$1, 20(%rcx)
	xorl	%r13d, %r13d
	jmp	.LBB3_92
	.p2align	4, 0x90
.LBB3_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_6 Depth 2
                                        #     Child Loop BB3_8 Depth 2
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r15,8), %rbx
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 32(%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB3_6:                                #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB3_6
# BB#7:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB3_5 Depth=1
	leal	1(%rbp), %eax
	movslq	%eax, %r12
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, 32(%rsp)
	movl	$0, (%rax)
	movl	%r12d, 44(%rsp)
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB3_8:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB3_8
# BB#9:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	%ebp, 40(%rsp)
.Ltmp24:
	movq	%rax, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp25:
# BB#10:                                # %_ZN11CStringBaseIwE9MakeUpperEv.exit
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	40(%rsp), %ecx
	movl	$1, %r13d
	testl	%ecx, %ecx
	je	.LBB3_11
# BB#12:                                #   in Loop: Header=BB3_5 Depth=1
	movq	%r15, %r12
	shlq	$4, %r12
	addq	64(%rsp), %r12          # 8-byte Folded Reload
	movq	32(%rsp), %rax
	cmpl	$88, (%rax)
	jne	.LBB3_22
# BB#13:                                #   in Loop: Header=BB3_5 Depth=1
	movl	$9, 8(%rsp)
	decl	%ecx
.Ltmp75:
	movl	$1, %edx
	movq	%r14, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp76:
# BB#14:                                # %_ZNK11CStringBaseIwE3MidEi.exit
                                        #   in Loop: Header=BB3_5 Depth=1
.Ltmp78:
	movq	%r14, %rdi
	movq	%r12, %rsi
	leaq	8(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %ebp
.Ltmp79:
# BB#15:                                #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_5 Depth=1
	callq	_ZdaPv
.LBB3_17:                               # %_ZN11CStringBaseIwED2Ev.exit106
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	jne	.LBB3_91
# BB#18:                                # %.thread
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	8(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	xorl	%r13d, %r13d
	jmp	.LBB3_92
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$-2147024809, 12(%rsp)  # 4-byte Folded Spill
                                        # imm = 0x80070057
	jmp	.LBB3_92
	.p2align	4, 0x90
.LBB3_22:                               #   in Loop: Header=BB3_5 Depth=1
.Ltmp27:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp28:
# BB#23:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
.Ltmp30:
	movl	$.L.str, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebp
.Ltmp31:
# BB#24:                                #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_26
# BB#25:                                #   in Loop: Header=BB3_5 Depth=1
	callq	_ZdaPv
.LBB3_26:                               # %_ZN11CStringBaseIwED2Ev.exit108
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	je	.LBB3_27
# BB#39:                                #   in Loop: Header=BB3_5 Depth=1
.Ltmp33:
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r14, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp34:
# BB#40:                                # %_ZNK11CStringBaseIwE4LeftEi.exit113
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
.Ltmp36:
	movl	$.L.str.1, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebp
.Ltmp37:
# BB#41:                                #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_43
# BB#42:                                #   in Loop: Header=BB3_5 Depth=1
	callq	_ZdaPv
.LBB3_43:                               # %_ZN11CStringBaseIwED2Ev.exit115
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	je	.LBB3_44
# BB#56:                                #   in Loop: Header=BB3_5 Depth=1
.Ltmp39:
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%r14, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp40:
# BB#57:                                # %_ZNK11CStringBaseIwE4LeftEi.exit120
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
.Ltmp42:
	movl	$.L.str.2, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebp
.Ltmp43:
# BB#58:                                #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_60
# BB#59:                                #   in Loop: Header=BB3_5 Depth=1
	callq	_ZdaPv
.LBB3_60:                               # %_ZN11CStringBaseIwED2Ev.exit122
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	je	.LBB3_61
# BB#73:                                #   in Loop: Header=BB3_5 Depth=1
.Ltmp45:
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%r14, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp46:
# BB#74:                                # %_ZNK11CStringBaseIwE4LeftEi.exit127
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
.Ltmp48:
	movl	$.L.str.3, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebp
.Ltmp49:
# BB#75:                                #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_77
# BB#76:                                #   in Loop: Header=BB3_5 Depth=1
	callq	_ZdaPv
.LBB3_77:                               # %_ZN11CStringBaseIwED2Ev.exit129
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	je	.LBB3_79
# BB#78:                                #   in Loop: Header=BB3_5 Depth=1
	movl	$-2147024809, 12(%rsp)  # 4-byte Folded Spill
                                        # imm = 0x80070057
	jmp	.LBB3_92
.LBB3_27:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$1, 8(%rsp)
	movl	40(%rsp), %ecx
	decl	%ecx
.Ltmp69:
	movl	$1, %edx
	movq	%r14, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp70:
# BB#28:                                # %_ZNK11CStringBaseIwE3MidEi.exit109
                                        #   in Loop: Header=BB3_5 Depth=1
.Ltmp72:
	movq	%r14, %rdi
	movq	%r12, %rsi
	leaq	8(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %ebp
.Ltmp73:
# BB#29:                                #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_5 Depth=1
	callq	_ZdaPv
.LBB3_31:                               # %_ZN11CStringBaseIwED2Ev.exit110
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	jne	.LBB3_91
# BB#32:                                # %.thread134
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	8(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 12(%rcx)
	xorl	%r13d, %r13d
	jmp	.LBB3_92
.LBB3_44:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$10, 8(%rsp)
	movl	40(%rsp), %ecx
	addl	$-4, %ecx
.Ltmp63:
	movl	$4, %edx
	movq	%r14, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp64:
# BB#45:                                # %_ZNK11CStringBaseIwE3MidEi.exit116
                                        #   in Loop: Header=BB3_5 Depth=1
.Ltmp66:
	movq	%r14, %rdi
	movq	%r12, %rsi
	leaq	8(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %ebp
.Ltmp67:
# BB#46:                                #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_48
# BB#47:                                #   in Loop: Header=BB3_5 Depth=1
	callq	_ZdaPv
.LBB3_48:                               # %_ZN11CStringBaseIwED2Ev.exit117
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	jne	.LBB3_91
# BB#49:                                # %.thread136
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	8(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 4(%rcx)
	xorl	%r13d, %r13d
	jmp	.LBB3_92
.LBB3_61:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$128, 8(%rsp)
	movl	40(%rsp), %ecx
	addl	$-2, %ecx
.Ltmp57:
	movl	$2, %edx
	movq	%r14, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp58:
# BB#62:                                # %_ZNK11CStringBaseIwE3MidEi.exit123
                                        #   in Loop: Header=BB3_5 Depth=1
.Ltmp60:
	movq	%r14, %rdi
	movq	%r12, %rsi
	leaq	8(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %ebp
.Ltmp61:
# BB#63:                                #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_65
# BB#64:                                #   in Loop: Header=BB3_5 Depth=1
	callq	_ZdaPv
.LBB3_65:                               # %_ZN11CStringBaseIwED2Ev.exit124
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	jne	.LBB3_91
# BB#66:                                # %.thread138
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	8(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 8(%rcx)
	xorl	%r13d, %r13d
	jmp	.LBB3_92
.LBB3_79:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$-1, 8(%rsp)
	movl	40(%rsp), %ecx
	addl	$-2, %ecx
.Ltmp51:
	movl	$2, %edx
	movq	%r14, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp52:
# BB#80:                                # %_ZNK11CStringBaseIwE3MidEi.exit130
                                        #   in Loop: Header=BB3_5 Depth=1
.Ltmp54:
	movq	%r14, %rdi
	movq	%r12, %rsi
	leaq	8(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %ebp
.Ltmp55:
# BB#81:                                #   in Loop: Header=BB3_5 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_83
# BB#82:                                #   in Loop: Header=BB3_5 Depth=1
	callq	_ZdaPv
.LBB3_83:                               # %_ZN11CStringBaseIwED2Ev.exit131
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	je	.LBB3_84
	.p2align	4, 0x90
.LBB3_91:                               #   in Loop: Header=BB3_5 Depth=1
	movl	%ebp, 12(%rsp)          # 4-byte Spill
.LBB3_92:                               #   in Loop: Header=BB3_5 Depth=1
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_94
# BB#93:                                #   in Loop: Header=BB3_5 Depth=1
	callq	_ZdaPv
.LBB3_94:                               # %_ZN11CStringBaseIwED2Ev.exit104
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%r13d, %r13d
	je	.LBB3_3
# BB#95:
	movl	12(%rsp), %eax          # 4-byte Reload
	jmp	.LBB3_2
.LBB3_1:
	xorl	%eax, %eax
.LBB3_2:                                # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_89:
.Ltmp56:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_98
# BB#90:
	callq	_ZdaPv
	jmp	.LBB3_98
.LBB3_88:
.Ltmp53:
	jmp	.LBB3_97
.LBB3_71:
.Ltmp62:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_98
# BB#72:
	callq	_ZdaPv
	jmp	.LBB3_98
.LBB3_70:
.Ltmp59:
	jmp	.LBB3_97
.LBB3_54:
.Ltmp68:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_98
# BB#55:
	callq	_ZdaPv
	jmp	.LBB3_98
.LBB3_53:
.Ltmp65:
	jmp	.LBB3_97
.LBB3_86:
.Ltmp50:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_98
# BB#87:
	callq	_ZdaPv
	jmp	.LBB3_98
.LBB3_85:
.Ltmp47:
	jmp	.LBB3_97
.LBB3_37:
.Ltmp74:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_98
# BB#38:
	callq	_ZdaPv
	jmp	.LBB3_98
.LBB3_36:
.Ltmp71:
	jmp	.LBB3_97
.LBB3_68:
.Ltmp44:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_98
# BB#69:
	callq	_ZdaPv
	jmp	.LBB3_98
.LBB3_67:
.Ltmp41:
	jmp	.LBB3_97
.LBB3_51:
.Ltmp38:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_98
# BB#52:
	callq	_ZdaPv
	jmp	.LBB3_98
.LBB3_50:
.Ltmp35:
	jmp	.LBB3_97
.LBB3_34:
.Ltmp32:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_98
# BB#35:
	callq	_ZdaPv
	jmp	.LBB3_98
.LBB3_33:
.Ltmp29:
	jmp	.LBB3_97
.LBB3_20:
.Ltmp80:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_98
# BB#21:
	callq	_ZdaPv
	jmp	.LBB3_98
.LBB3_19:
.Ltmp77:
	jmp	.LBB3_97
.LBB3_96:
.Ltmp26:
.LBB3_97:
	movq	%rax, %rbx
.LBB3_98:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_100
# BB#99:
	callq	_ZdaPv
.LBB3_100:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN8NArchive13CDeflateProps13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end3-_ZN8NArchive13CDeflateProps13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\224\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\221\002"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp24-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin1   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin1   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin1   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin1   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin1   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin1   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin1   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin1   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin1   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin1   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin1   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin1   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin1   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin1   # >> Call Site 15 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin1   # >> Call Site 16 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin1   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin1   # >> Call Site 17 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin1   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin1   # >> Call Site 18 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin1   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin1   # >> Call Site 19 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin1   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin1   # >> Call Site 20 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin1   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin1   # >> Call Site 21 <<
	.long	.Lfunc_end3-.Ltmp55     #   Call between .Ltmp55 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 64
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB4_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB4_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB4_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB4_5
.LBB4_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB4_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp81:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp82:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB4_35
# BB#12:
	movq	%rbx, %r13
.LBB4_13:                               # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB4_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB4_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB4_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB4_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB4_15
.LBB4_14:
	xorl	%esi, %esi
.LBB4_15:                               # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB4_16:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB4_16
.LBB4_29:
	movq	%r13, %rbx
.LBB4_30:                               # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp83:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp84:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB4_32:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB4_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB4_8
.LBB4_3:
	xorl	%eax, %eax
.LBB4_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB4_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB4_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB4_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB4_30
.LBB4_21:                               # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB4_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_24:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB4_24
	jmp	.LBB4_25
.LBB4_22:
	xorl	%ecx, %ecx
.LBB4_25:                               # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB4_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB4_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB4_27
.LBB4_28:                               # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB4_15
	jmp	.LBB4_29
.LBB4_34:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp85:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end4-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp81-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp81
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp84-.Ltmp81         #   Call between .Ltmp81 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin2   #     jumps to .Ltmp85
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end4-.Ltmp84     #   Call between .Ltmp84 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L_ZZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderPropertiesE7propIDs,@object # @_ZZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderPropertiesE7propIDs
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.L_ZZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderPropertiesE7propIDs:
	.long	12                      # 0xc
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	10                      # 0xa
	.size	.L_ZZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderPropertiesE7propIDs, 16

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	65                      # 0x41
	.long	0                       # 0x0
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	80                      # 0x50
	.long	65                      # 0x41
	.long	83                      # 0x53
	.long	83                      # 0x53
	.long	0                       # 0x0
	.size	.L.str.1, 20

	.type	.L.str.2,@object        # @.str.2
	.p2align	2
.L.str.2:
	.long	70                      # 0x46
	.long	66                      # 0x42
	.long	0                       # 0x0
	.size	.L.str.2, 12

	.type	.L.str.3,@object        # @.str.3
	.p2align	2
.L.str.3:
	.long	77                      # 0x4d
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.3, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
