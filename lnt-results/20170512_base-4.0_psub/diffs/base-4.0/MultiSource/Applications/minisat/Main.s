	.text
	.file	"Main.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1120403456              # float 100
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_1:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI0_2:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	_Z10printStatsR6Solver
	.p2align	4, 0x90
	.type	_Z10printStatsR6Solver,@function
_Z10printStatsR6Solver:                 # @_Z10printStatsR6Solver
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$280, %rsp              # imm = 0x118
.Lcfi2:
	.cfi_def_cfa_offset 304
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	leaq	16(%rsp), %rsi
	xorl	%edi, %edi
	callq	getrusage
	callq	getpid
	movl	%eax, %ecx
	leaq	16(%rsp), %rbx
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ecx, %edx
	callq	sprintf
	movl	$.L.str.35, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	leaq	12(%rsp), %rdx
	movl	$.L.str.51, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movq	%rbx, %rdi
	callq	fclose
.LBB0_2:                                # %_ZL7memUsedv.exit
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movq	104(%r14), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movq	136(%r14), %rdx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movq	112(%r14), %rdx
	movq	120(%r14), %rax
	testq	%rax, %rax
	js	.LBB0_3
# BB#4:                                 # %_ZL7memUsedv.exit
	cvtsi2ssq	%rax, %xmm0
	jmp	.LBB0_5
.LBB0_3:
	movq	%rax, %rcx
	shrq	%rcx
	andl	$1, %eax
	orq	%rcx, %rax
	cvtsi2ssq	%rax, %xmm0
	addss	%xmm0, %xmm0
.LBB0_5:                                # %_ZL7memUsedv.exit
	mulss	.LCPI0_0(%rip), %xmm0
	testq	%rdx, %rdx
	js	.LBB0_6
# BB#7:                                 # %_ZL7memUsedv.exit
	cvtsi2ssq	%rdx, %xmm1
	jmp	.LBB0_8
.LBB0_6:
	movq	%rdx, %rax
	shrq	%rax
	movl	%edx, %ecx
	andl	$1, %ecx
	orq	%rax, %rcx
	cvtsi2ssq	%rcx, %xmm1
	addss	%xmm1, %xmm1
.LBB0_8:                                # %_ZL7memUsedv.exit
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.6, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movq	128(%r14), %rdx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movq	160(%r14), %rax
	movq	168(%r14), %rdx
	movd	%rax, %xmm1
	subq	%rdx, %rax
	imulq	$100, %rax, %rax
	movd	%rax, %xmm2
	movdqa	.LCPI0_1(%rip), %xmm3   # xmm3 = [1127219200,1160773632,0,0]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movapd	.LCPI0_2(%rip), %xmm4   # xmm4 = [4.503600e+15,1.934281e+25]
	subpd	%xmm4, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	addpd	%xmm2, %xmm0
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	subpd	%xmm4, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movl	$.L.str.8, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_Z10printStatsR6Solver, .Lfunc_end0-_Z10printStatsR6Solver
	.cfi_endproc

	.globl	_Z10printUsagePPc
	.p2align	4, 0x90
	.type	_Z10printUsagePPc,@function
_Z10printUsagePPc:                      # @_Z10printUsagePPc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movq	(%rbx), %rdx
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$36, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stderr(%rip), %rdi
	popq	%rbx
	jmp	fflush                  # TAILCALL
.Lfunc_end1:
	.size	_Z10printUsagePPc, .Lfunc_end1-_Z10printUsagePPc
	.cfi_endproc

	.globl	_Z9hasPrefixPKcS0_
	.p2align	4, 0x90
	.type	_Z9hasPrefixPKcS0_,@function
_Z9hasPrefixPKcS0_:                     # @_Z9hasPrefixPKcS0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	%rbx, %rdi
	callq	strlen
	movslq	%eax, %r14
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_2
# BB#1:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB2_2:
	addq	%r14, %r15
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	strdup                  # TAILCALL
.Lfunc_end2:
	.size	_Z9hasPrefixPKcS0_, .Lfunc_end2-_Z9hasPrefixPKcS0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4607182418800017408     # double 1
.LCPI3_1:
	.quad	0                       # double 0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$712, %rsp              # imm = 0x2C8
.Lcfi19:
	.cfi_def_cfa_offset 768
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %ebx
	leaq	176(%rsp), %rdi
	callq	_ZN6SolverC1Ev
	movl	$1, 272(%rsp)
	testl	%ebx, %ebx
	jle	.LBB3_1
# BB#2:                                 # %.lr.ph142
	movslq	%ebx, %rbp
	leaq	32(%rsp), %r13
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15,%r14,8), %rbx
	movl	$.L.str.16, %esi
	movl	$15, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_4
.LBB3_6:                                # %_Z9hasPrefixPKcS0_.exit.thread
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.21, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_7
.LBB3_9:                                # %_Z9hasPrefixPKcS0_.exit106.thread
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.24, %esi
	movl	$7, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_10
.LBB3_12:                               # %_Z9hasPrefixPKcS0_.exit109.thread
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.26, %esi
	movl	$11, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_13
.LBB3_15:                               # %_Z9hasPrefixPKcS0_.exit112.thread
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.28, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_18
# BB#16:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.29, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_18
# BB#17:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.30, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_18
# BB#42:                                #   in Loop: Header=BB3_3 Depth=1
	cmpb	$45, (%rbx)
	je	.LBB3_43
# BB#44:                                #   in Loop: Header=BB3_3 Depth=1
	movslq	%r12d, %rax
	incl	%r12d
	movq	%rbx, (%r15,%rax,8)
	jmp	.LBB3_45
	.p2align	4, 0x90
.LBB3_4:                                # %_Z9hasPrefixPKcS0_.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	addq	$15, %rbx
	movq	%rbx, %rdi
	callq	strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_5
# BB#19:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.17, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_20
# BB#21:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.18, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_22
# BB#23:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.19, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_25
# BB#24:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$3, 268(%rsp)
	jmp	.LBB3_45
	.p2align	4, 0x90
.LBB3_7:                                # %_Z9hasPrefixPKcS0_.exit106
                                        #   in Loop: Header=BB3_3 Depth=1
	addq	$10, %rbx
	movq	%rbx, %rdi
	callq	strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_8
# BB#28:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	sscanf
	movsd	32(%rsp), %xmm0         # xmm0 = mem[0],zero
	ucomisd	.LCPI3_0(%rip), %xmm0
	ja	.LBB3_31
# BB#29:                                #   in Loop: Header=BB3_3 Depth=1
	testl	%eax, %eax
	jle	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_3 Depth=1
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	ja	.LBB3_31
# BB#32:                                #   in Loop: Header=BB3_3 Depth=1
	movsd	%xmm0, 224(%rsp)
	jmp	.LBB3_45
	.p2align	4, 0x90
.LBB3_10:                               # %_Z9hasPrefixPKcS0_.exit109
                                        #   in Loop: Header=BB3_3 Depth=1
	addq	$7, %rbx
	movq	%rbx, %rdi
	callq	strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_11
# BB#33:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	sscanf
	movsd	32(%rsp), %xmm0         # xmm0 = mem[0],zero
	ucomisd	.LCPI3_0(%rip), %xmm0
	ja	.LBB3_36
# BB#34:                                #   in Loop: Header=BB3_3 Depth=1
	testl	%eax, %eax
	jle	.LBB3_36
# BB#35:                                #   in Loop: Header=BB3_3 Depth=1
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jae	.LBB3_36
# BB#37:                                #   in Loop: Header=BB3_3 Depth=1
	movsd	.LCPI3_0(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 208(%rsp)
	jmp	.LBB3_45
.LBB3_13:                               # %_Z9hasPrefixPKcS0_.exit112
                                        #   in Loop: Header=BB3_3 Depth=1
	addq	$11, %rbx
	movq	%rbx, %rdi
	callq	strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_14
# BB#38:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$10, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	strtol
	movq	32(%rsp), %rcx
	cmpq	%rbx, %rcx
	je	.LBB3_40
# BB#39:                                #   in Loop: Header=BB3_3 Depth=1
	cmpb	$0, (%rcx)
	jne	.LBB3_40
# BB#41:                                #   in Loop: Header=BB3_3 Depth=1
	movl	%eax, 272(%rsp)
	jmp	.LBB3_45
.LBB3_5:                                # %_Z9hasPrefixPKcS0_.exit._Z9hasPrefixPKcS0_.exit.thread_crit_edge
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	(%r15,%r14,8), %rbx
	jmp	.LBB3_6
.LBB3_8:                                # %_Z9hasPrefixPKcS0_.exit106._Z9hasPrefixPKcS0_.exit106.thread_crit_edge
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	(%r15,%r14,8), %rbx
	jmp	.LBB3_9
.LBB3_20:                               #   in Loop: Header=BB3_3 Depth=1
	movl	$0, 268(%rsp)
	jmp	.LBB3_45
.LBB3_11:                               # %_Z9hasPrefixPKcS0_.exit109._Z9hasPrefixPKcS0_.exit109.thread_crit_edge
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	(%r15,%r14,8), %rbx
	jmp	.LBB3_12
.LBB3_14:                               # %_Z9hasPrefixPKcS0_.exit112._Z9hasPrefixPKcS0_.exit112.thread_crit_edge
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	(%r15,%r14,8), %rbx
	jmp	.LBB3_15
.LBB3_22:                               #   in Loop: Header=BB3_3 Depth=1
	movl	$1, 268(%rsp)
	.p2align	4, 0x90
.LBB3_45:                               #   in Loop: Header=BB3_3 Depth=1
	incq	%r14
	cmpq	%rbp, %r14
	jl	.LBB3_3
.LBB3_46:                               # %._crit_edge143
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	movl	$.L.str.33, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	#APP
	fnstcw	22(%rsp)
	#NO_APP
	movzwl	22(%rsp), %eax
	andl	$64767, %eax            # imm = 0xFCFF
	orl	$512, %eax              # imm = 0x200
	movw	%ax, 20(%rsp)
	#APP
	fldcw	20(%rsp)
	#NO_APP
	leaq	32(%rsp), %rsi
	xorl	%edi, %edi
	callq	getrusage
	leaq	176(%rsp), %rax
	movq	%rax, solver(%rip)
	cmpl	$1, %r12d
	je	.LBB3_47
# BB#48:                                # %.critedge
	movq	8(%r15), %rdi
	movl	$.L.str.35, %esi
	callq	fopen
	movq	%rax, %rbp
.LBB3_49:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	testq	%rbp, %rbp
	je	.LBB3_50
# BB#54:
	movl	$.L.str.38, %edi
	movl	$80, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$80, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
.Ltmp0:
	movl	$1048592, %edi          # imm = 0x100010
	callq	_Znwm
	movq	%rax, %r14
.Ltmp1:
# BB#55:                                # %.noexc
	movq	%rbp, (%r14)
	movq	$0, 1048584(%r14)
	movq	%rbp, %rdi
	callq	fileno
	movq	%r14, %rsi
	addq	$8, %rsi
.Ltmp3:
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	read
.Ltmp4:
# BB#56:
	movl	%eax, 1048588(%r14)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 32(%rsp)
	leaq	176(%rsp), %r13
	jmp	.LBB3_57
	.p2align	4, 0x90
.LBB3_66:                               # %.lr.ph14.i.i.i
                                        #   in Loop: Header=BB3_65 Depth=2
	movl	%ebp, %ebx
	negl	%ebx
	cmovll	%ebp, %ebx
	.p2align	4, 0x90
.LBB3_67:                               # %.lr.ph14.i.i.i
                                        #   Parent Loop BB3_57 Depth=1
                                        #     Parent Loop BB3_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	448(%rsp), %ebx
	jle	.LBB3_69
# BB#68:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB3_67 Depth=3
.Ltmp35:
	movl	$1, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	callq	_ZN6Solver6newVarEbb
.Ltmp36:
	jmp	.LBB3_67
	.p2align	4, 0x90
.LBB3_69:                               # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB3_65 Depth=2
	xorl	%eax, %eax
	testl	%ebp, %ebp
	setle	%al
	leal	-2(%rax,%rbx,2), %ebp
	movl	40(%rsp), %ecx
	cmpl	44(%rsp), %ecx
	jne	.LBB3_70
# BB#118:                               #   in Loop: Header=BB3_65 Depth=2
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 44(%rsp)
	movq	32(%rsp), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 32(%rsp)
	movl	40(%rsp), %ecx
	jmp	.LBB3_119
	.p2align	4, 0x90
.LBB3_70:                               # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB3_65 Depth=2
	movq	32(%rsp), %rax
.LBB3_119:                              # %_ZN3vecI3LitE4pushERKS0_.exit.i.i.i
                                        #   in Loop: Header=BB3_65 Depth=2
	leal	1(%rcx), %edx
	movl	%edx, 40(%rsp)
	movslq	%ecx, %rcx
	movl	%ebp, (%rax,%rcx,4)
.Ltmp38:
	movq	%r14, %rdi
	callq	_ZL8parseIntI12StreamBufferEiRT_
	movl	%eax, %ebp
.Ltmp39:
	jmp	.LBB3_65
.LBB3_109:                              # %.preheader.i.loopexit.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	cmpl	%eax, %ecx
	jl	.LBB3_112
	jmp	.LBB3_111
	.p2align	4, 0x90
.LBB3_117:                              # %_ZN12StreamBufferppEv.exit.i38._crit_edge.i.i
                                        #   in Loop: Header=BB3_112 Depth=2
	movl	1048584(%r14), %ecx
	cmpl	%eax, %ecx
	jge	.LBB3_111
.LBB3_112:                              # %_ZN12StreamBufferdeEv.exit.i36.i.i
                                        #   Parent Loop BB3_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ecx, %rdx
	movzbl	8(%r14,%rdx), %ebp
	movl	%ebp, %edx
	incb	%dl
	cmpb	$2, %dl
	jb	.LBB3_111
# BB#113:                               # %_ZN12StreamBufferdeEv.exit5.i37.i.i
                                        #   in Loop: Header=BB3_112 Depth=2
	incl	%ecx
	movl	%ecx, 1048584(%r14)
	cmpl	%eax, %ecx
	jl	.LBB3_116
# BB#114:                               #   in Loop: Header=BB3_112 Depth=2
	movl	$0, 1048584(%r14)
	movq	(%r14), %rdi
	callq	fileno
.Ltmp6:
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	read
.Ltmp7:
# BB#115:                               # %.noexc39.i.i
                                        #   in Loop: Header=BB3_112 Depth=2
	movl	%eax, 1048588(%r14)
.LBB3_116:                              # %_ZN12StreamBufferppEv.exit.i38.i.i
                                        #   in Loop: Header=BB3_112 Depth=2
	cmpb	$10, %bpl
	jne	.LBB3_117
	jmp	.LBB3_111
.LBB3_120:                              # %_ZL10readClauseI12StreamBufferEvRT_R6SolverR3vecI3LitE.exit.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
.Ltmp41:
	movq	%r13, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN6Solver9addClauseER3vecI3LitE
.Ltmp42:
.LBB3_111:                              # %_ZL8skipLineI12StreamBufferEvRT_.exit.i.backedge.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movl	1048588(%r14), %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB3_57
	.p2align	4, 0x90
.LBB3_100:                              # %.noexc.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movl	%eax, 1048588(%r14)
.LBB3_57:                               # %_ZN12StreamBufferppEv.exit.outer.i.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_58 Depth 2
                                        #     Child Loop BB3_112 Depth 2
                                        #     Child Loop BB3_65 Depth 2
                                        #       Child Loop BB3_67 Depth 3
	movslq	1048584(%r14), %rcx
	movslq	%eax, %rsi
	.p2align	4, 0x90
.LBB3_58:                               # %_ZN12StreamBufferppEv.exit.i.i.i
                                        #   Parent Loop BB3_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %rcx
	jge	.LBB3_121
# BB#59:                                # %_ZN12StreamBufferdeEv.exit.i.i.i
                                        #   in Loop: Header=BB3_58 Depth=2
	movzbl	8(%r14,%rcx), %edx
	cmpb	$31, %dl
	jg	.LBB3_71
# BB#60:                                # %_ZN12StreamBufferdeEv.exit.i.i.i
                                        #   in Loop: Header=BB3_58 Depth=2
	movl	%edx, %ebx
	addb	$-9, %bl
	cmpb	$5, %bl
	jae	.LBB3_61
.LBB3_98:                               # %.critedge.i.i.i
                                        #   in Loop: Header=BB3_58 Depth=2
	leal	1(%rcx), %edx
	incq	%rcx
	movl	%edx, 1048584(%r14)
	cmpq	%rsi, %rcx
	jl	.LBB3_58
	jmp	.LBB3_99
.LBB3_71:                               # %_ZN12StreamBufferdeEv.exit.i.i.i
                                        #   in Loop: Header=BB3_58 Depth=2
	cmpb	$32, %dl
	je	.LBB3_98
# BB#72:                                # %_ZN12StreamBufferdeEv.exit.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	cmpb	$99, %dl
	je	.LBB3_109
# BB#73:                                # %_ZN12StreamBufferdeEv.exit.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	cmpb	$112, %dl
	jne	.LBB3_62
# BB#74:                                # %_ZN12StreamBufferdeEv.exit.i26.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	leal	1(%rcx), %ecx
	movl	%ecx, 1048584(%r14)
	cmpl	%eax, %ecx
	jl	.LBB3_75
# BB#105:                               #   in Loop: Header=BB3_57 Depth=1
	movl	$0, 1048584(%r14)
	movq	(%r14), %rdi
	callq	fileno
.Ltmp9:
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	read
.Ltmp10:
# BB#106:                               # %.noexc28.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movl	%eax, 1048588(%r14)
	movl	1048584(%r14), %ecx
.LBB3_75:                               # %_ZN12StreamBufferppEv.exit.backedge.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	cmpl	%eax, %ecx
	jge	.LBB3_108
# BB#76:                                # %_ZN12StreamBufferdeEv.exit.1.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movslq	%ecx, %rdx
	cmpb	$32, 8(%r14,%rdx)
	jne	.LBB3_108
# BB#77:                                #   in Loop: Header=BB3_57 Depth=1
	incl	%ecx
	movl	%ecx, 1048584(%r14)
	cmpl	%eax, %ecx
	jl	.LBB3_80
# BB#78:                                #   in Loop: Header=BB3_57 Depth=1
	movl	$0, 1048584(%r14)
	movq	(%r14), %rdi
	callq	fileno
.Ltmp11:
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	read
.Ltmp12:
# BB#79:                                # %.noexc29.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movl	%eax, 1048588(%r14)
	movl	1048584(%r14), %ecx
.LBB3_80:                               # %_ZN12StreamBufferppEv.exit.backedge.1.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	cmpl	%eax, %ecx
	jge	.LBB3_108
# BB#81:                                # %_ZN12StreamBufferdeEv.exit.2.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movslq	%ecx, %rdx
	cmpb	$99, 8(%r14,%rdx)
	jne	.LBB3_108
# BB#82:                                #   in Loop: Header=BB3_57 Depth=1
	incl	%ecx
	movl	%ecx, 1048584(%r14)
	cmpl	%eax, %ecx
	jl	.LBB3_85
# BB#83:                                #   in Loop: Header=BB3_57 Depth=1
	movl	$0, 1048584(%r14)
	movq	(%r14), %rdi
	callq	fileno
.Ltmp13:
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	read
.Ltmp14:
# BB#84:                                # %.noexc30.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movl	%eax, 1048588(%r14)
	movl	1048584(%r14), %ecx
.LBB3_85:                               # %_ZN12StreamBufferppEv.exit.backedge.2.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	cmpl	%eax, %ecx
	jge	.LBB3_108
# BB#86:                                # %_ZN12StreamBufferdeEv.exit.3.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movslq	%ecx, %rdx
	cmpb	$110, 8(%r14,%rdx)
	jne	.LBB3_108
# BB#87:                                #   in Loop: Header=BB3_57 Depth=1
	incl	%ecx
	movl	%ecx, 1048584(%r14)
	cmpl	%eax, %ecx
	jl	.LBB3_90
# BB#88:                                #   in Loop: Header=BB3_57 Depth=1
	movl	$0, 1048584(%r14)
	movq	(%r14), %rdi
	callq	fileno
.Ltmp15:
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	read
.Ltmp16:
# BB#89:                                # %.noexc31.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movl	%eax, 1048588(%r14)
	movl	1048584(%r14), %ecx
.LBB3_90:                               # %_ZN12StreamBufferppEv.exit.backedge.3.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	cmpl	%eax, %ecx
	jge	.LBB3_108
# BB#91:                                # %_ZN12StreamBufferdeEv.exit.4.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movslq	%ecx, %rdx
	cmpb	$102, 8(%r14,%rdx)
	jne	.LBB3_108
# BB#92:                                #   in Loop: Header=BB3_57 Depth=1
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	incl	%ecx
	movl	%ecx, 1048584(%r14)
	cmpl	%eax, %ecx
	jl	.LBB3_95
# BB#93:                                #   in Loop: Header=BB3_57 Depth=1
	movl	$0, 1048584(%r14)
	movq	(%r14), %rdi
	callq	fileno
.Ltmp17:
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	read
.Ltmp18:
# BB#94:                                # %.noexc32.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movl	%eax, 1048588(%r14)
.LBB3_95:                               # %_ZL5matchI12StreamBufferEbRT_Pc.exit.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
.Ltmp19:
	movq	%r14, %rdi
	callq	_ZL8parseIntI12StreamBufferEiRT_
	movl	%eax, %ebx
.Ltmp20:
# BB#96:                                #   in Loop: Header=BB3_57 Depth=1
.Ltmp21:
	movq	%r14, %rdi
	callq	_ZL8parseIntI12StreamBufferEiRT_
	movl	%eax, %ebp
.Ltmp22:
# BB#97:                                #   in Loop: Header=BB3_57 Depth=1
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.53, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.54, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	jmp	.LBB3_111
	.p2align	4, 0x90
.LBB3_99:                               #   in Loop: Header=BB3_57 Depth=1
	movl	$0, 1048584(%r14)
	movq	(%r14), %rdi
	callq	fileno
.Ltmp30:
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	read
.Ltmp31:
	jmp	.LBB3_100
.LBB3_61:                               # %_ZN12StreamBufferdeEv.exit.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	cmpb	$-1, %dl
	je	.LBB3_121
.LBB3_62:                               # %_ZN12StreamBufferdeEv.exit34.thread.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	cmpq	$0, 32(%rsp)
	je	.LBB3_64
# BB#63:                                # %.preheader.i.i40.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movl	$0, 40(%rsp)
.LBB3_64:                               # %_ZN3vecI3LitE5clearEb.exit.preheader.i.i.i
                                        #   in Loop: Header=BB3_57 Depth=1
.Ltmp33:
	movq	%r14, %rdi
	callq	_ZL8parseIntI12StreamBufferEiRT_
	movl	%eax, %ebp
.Ltmp34:
	.p2align	4, 0x90
.LBB3_65:                               # %.noexc41.i.i
                                        #   Parent Loop BB3_57 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_67 Depth 3
	testl	%ebp, %ebp
	jne	.LBB3_66
	jmp	.LBB3_120
.LBB3_121:                              # %_ZN12StreamBufferdeEv.exit.thread.i.i
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_123
# BB#122:                               # %.preheader.i.i24.i.i
	movl	$0, 40(%rsp)
	callq	free
	movq	$0, 32(%rsp)
	movl	$0, 44(%rsp)
.LBB3_123:
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbp, %rdi
	callq	fclose
	cmpl	$3, %r12d
	jl	.LBB3_124
# BB#129:
	movq	16(%r15), %rdi
	movl	$.L.str.40, %esi
	callq	fopen
	movq	%rax, %r12
	jmp	.LBB3_130
.LBB3_124:
	xorl	%r12d, %r12d
.LBB3_130:
.Ltmp24:
	leaq	176(%rsp), %rdi
	callq	_ZN6Solver8simplifyEv
.Ltmp25:
# BB#131:
	testb	%al, %al
	je	.LBB3_132
# BB#135:
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 32(%rsp)
.Ltmp27:
	leaq	176(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN6Solver5solveERK3vecI3LitE
	movl	%eax, %r14d
.Ltmp28:
# BB#136:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_138
# BB#137:                               # %.preheader.i.i2.i
	movl	$0, 40(%rsp)
	callq	free
	movq	$0, 32(%rsp)
	movl	$0, 44(%rsp)
.LBB3_138:
	leaq	176(%rsp), %rdi
	callq	_Z10printStatsR6Solver
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rsi
	movl	$10, %r15d
	movl	$10, %edi
	callq	fputc
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$.L.str.44, %eax
	movl	$.L.str.43, %edi
	testb	%r14b, %r14b
	cmovneq	%rax, %rdi
	xorl	%eax, %eax
	callq	printf
	testq	%r12, %r12
	je	.LBB3_150
# BB#139:
	testb	%r14b, %r14b
	je	.LBB3_148
# BB#140:
	movl	$.L.str.45, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	448(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB3_147
# BB#141:                               # %.lr.ph
	movl	$.L.str.47, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_142:                              # =>This Inner Loop Header: Depth=1
	movq	176(%rsp), %rcx
	movzbl	(%rcx,%rbp), %ecx
	testb	%cl, %cl
	je	.LBB3_143
# BB#145:                               #   in Loop: Header=BB3_142 Depth=1
	testq	%rbp, %rbp
	movl	$.L.str.48, %edx
	cmoveq	%rbx, %rdx
	cmpb	$1, %cl
	movl	$.L.str.31, %ecx
	cmoveq	%rbx, %rcx
	incq	%rbp
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebp, %r8d
	callq	fprintf
	movl	448(%rsp), %eax
	jmp	.LBB3_146
	.p2align	4, 0x90
.LBB3_143:                              # %._crit_edge168
                                        #   in Loop: Header=BB3_142 Depth=1
	incq	%rbp
.LBB3_146:                              #   in Loop: Header=BB3_142 Depth=1
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB3_142
.LBB3_147:                              # %._crit_edge
	movl	$.L.str.49, %edi
	movl	$3, %esi
.LBB3_149:
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movq	%r12, %rdi
	callq	fclose
.LBB3_150:
	testb	%r14b, %r14b
	movl	$20, %edi
	cmovnel	%r15d, %edi
	callq	exit
.LBB3_1:
	xorl	%r12d, %r12d
	jmp	.LBB3_46
.LBB3_47:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	movl	$.L.str.34, %edi
	movl	$62, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdin(%rip), %rbp
	jmp	.LBB3_49
.LBB3_18:
	movq	%r15, %rdi
	callq	_Z10printUsagePPc
	xorl	%edi, %edi
	callq	exit
.LBB3_43:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movq	(%r15,%r14,8), %rdx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	jmp	.LBB3_27
.LBB3_40:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.27, %esi
	jmp	.LBB3_26
.LBB3_31:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.23, %esi
	jmp	.LBB3_26
.LBB3_36:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.25, %esi
	jmp	.LBB3_26
.LBB3_108:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rbp
	movq	%r14, %rdi
	callq	_ZN12StreamBufferdeEv
	movl	%eax, %ecx
	movl	$.L.str.55, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movl	%ecx, %edx
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$3, %edi
	callq	exit
.LBB3_25:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.20, %esi
.LBB3_26:
	xorl	%eax, %eax
	movq	%rbx, %rdx
.LBB3_27:
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	xorl	%edi, %edi
	callq	exit
.LBB3_50:
	cmpl	$1, %r12d
	jne	.LBB3_52
# BB#51:
	movl	$.L.str.37, %edx
	jmp	.LBB3_53
.LBB3_132:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	movl	$.L.str.41, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	testq	%r12, %r12
	je	.LBB3_134
# BB#133:
	movl	$.L.str.42, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movq	%r12, %rdi
	callq	fclose
.LBB3_134:
	movl	$.Lstr, %edi
	callq	puts
	movl	$20, %edi
	callq	exit
.LBB3_148:
	movl	$.L.str.42, %edi
	movl	$6, %esi
	jmp	.LBB3_149
.LBB3_52:
	movq	8(%r15), %rdx
.LBB3_53:
	movl	$.L.str.36, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.LBB3_107:
.Ltmp23:
	jmp	.LBB3_126
.LBB3_101:                              # %.loopexit.split-lp.loopexit.i.i
.Ltmp8:
	jmp	.LBB3_126
.LBB3_103:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.i.i
.Ltmp32:
	jmp	.LBB3_126
.LBB3_144:
.Ltmp29:
	jmp	.LBB3_126
.LBB3_156:
.Ltmp26:
	jmp	.LBB3_152
.LBB3_128:
.Ltmp5:
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	_ZdlPv
	jmp	.LBB3_153
.LBB3_151:
.Ltmp2:
.LBB3_152:
	movq	%rax, %rbp
	jmp	.LBB3_153
.LBB3_104:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.i.i
.Ltmp43:
	jmp	.LBB3_126
.LBB3_102:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.i.i
.Ltmp40:
	jmp	.LBB3_126
.LBB3_125:                              # %.loopexit.i.i
.Ltmp37:
.LBB3_126:                              # %.loopexit.split-lp.i.i
	movq	%rax, %rbp
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_153
# BB#127:                               # %.preheader.i.i.i.i
	movl	$0, 40(%rsp)
	callq	free
	movq	$0, 32(%rsp)
	movl	$0, 44(%rsp)
.LBB3_153:
.Ltmp44:
	leaq	176(%rsp), %rdi
	callq	_ZN6SolverD1Ev
.Ltmp45:
# BB#154:
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB3_155:
.Ltmp46:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\277\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin0   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp20-.Ltmp41         #   Call between .Ltmp41 and .Ltmp20
	.long	.Ltmp43-.Lfunc_begin0   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin0   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin0   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp43-.Lfunc_begin0   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin0   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin0   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin0   #     jumps to .Ltmp46
	.byte	1                       #   On action: 1
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Lfunc_end3-.Ltmp45     #   Call between .Ltmp45 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.section	.text._ZN12StreamBufferdeEv,"axG",@progbits,_ZN12StreamBufferdeEv,comdat
	.weak	_ZN12StreamBufferdeEv
	.p2align	4, 0x90
	.type	_ZN12StreamBufferdeEv,@function
_ZN12StreamBufferdeEv:                  # @_ZN12StreamBufferdeEv
	.cfi_startproc
# BB#0:
	movslq	1048584(%rdi), %rcx
	movl	$-1, %eax
	cmpl	1048588(%rdi), %ecx
	jge	.LBB5_2
# BB#1:
	movsbl	8(%rdi,%rcx), %eax
.LBB5_2:
	retq
.Lfunc_end5:
	.size	_ZN12StreamBufferdeEv, .Lfunc_end5-_ZN12StreamBufferdeEv
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZL8parseIntI12StreamBufferEiRT_,@function
_ZL8parseIntI12StreamBufferEiRT_:       # @_ZL8parseIntI12StreamBufferEiRT_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -48
.Lcfi32:
	.cfi_offset %r12, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	leaq	8(%rbp), %r14
	movl	1048588(%rbp), %eax
	movabsq	$4294983168, %rbx       # imm = 0x100003E00
	jmp	.LBB6_1
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_1 Depth=1
	movl	$0, 1048584(%rbp)
	movq	(%rbp), %rdi
	callq	fileno
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	read
	movl	%eax, 1048588(%rbp)
.LBB6_1:                                # %_ZN12StreamBufferppEv.exit.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_2 Depth 2
	movslq	1048584(%rbp), %rcx
	.p2align	4, 0x90
.LBB6_2:                                # %_ZN12StreamBufferppEv.exit.i
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%eax, %ecx
	jge	.LBB6_13
# BB#3:                                 # %_ZN12StreamBufferdeEv.exit.i
                                        #   in Loop: Header=BB6_2 Depth=2
	movzbl	8(%rbp,%rcx), %edx
	cmpq	$32, %rdx
	ja	.LBB6_7
# BB#4:                                 # %_ZN12StreamBufferdeEv.exit.i
                                        #   in Loop: Header=BB6_2 Depth=2
	btq	%rdx, %rbx
	jae	.LBB6_7
# BB#5:                                 # %.critedge.i
                                        #   in Loop: Header=BB6_2 Depth=2
	incq	%rcx
	movl	%ecx, 1048584(%rbp)
	cmpl	%eax, %ecx
	jl	.LBB6_2
	jmp	.LBB6_6
.LBB6_7:                                # %_ZN12StreamBufferdeEv.exit
	cmpb	$45, 8(%rbp,%rcx)
	jne	.LBB6_10
# BB#8:
	incl	%ecx
	movl	%ecx, 1048584(%rbp)
	movb	$1, %r15b
	cmpl	%eax, %ecx
	jl	.LBB6_14
# BB#9:
	movl	$0, 1048584(%rbp)
	movq	(%rbp), %rdi
	callq	fileno
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	read
	movl	%eax, 1048588(%rbp)
	jmp	.LBB6_14
.LBB6_10:                               # %_ZN12StreamBufferdeEv.exit16
	cmpb	$43, 8(%rbp,%rcx)
	jne	.LBB6_13
# BB#11:
	incl	%ecx
	movl	%ecx, 1048584(%rbp)
	cmpl	%eax, %ecx
	jl	.LBB6_13
# BB#12:
	movl	$0, 1048584(%rbp)
	movq	(%rbp), %rdi
	callq	fileno
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	read
	movl	%eax, 1048588(%rbp)
.LBB6_13:                               # %_ZN12StreamBufferppEv.exit
	xorl	%r15d, %r15d
.LBB6_14:                               # %_ZN12StreamBufferppEv.exit
	movl	1048584(%rbp), %ecx
	cmpl	%eax, %ecx
	jge	.LBB6_25
# BB#15:                                # %_ZN12StreamBufferdeEv.exit18
	movslq	%ecx, %rdx
	movb	8(%rbp,%rdx), %bl
	movl	%ebx, %edx
	addb	$-48, %dl
	cmpb	$9, %dl
	ja	.LBB6_25
# BB#16:                                # %_ZN12StreamBufferdeEv.exit21.preheader
	xorl	%edx, %edx
	cmpb	$48, %bl
	jl	.LBB6_24
# BB#17:                                # %.lr.ph.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, %r12d
	cmpl	%eax, %ecx
	jge	.LBB6_21
# BB#19:                                # %_ZN12StreamBufferdeEv.exit22
                                        #   in Loop: Header=BB6_18 Depth=1
	cmpb	$57, %bl
	jg	.LBB6_24
# BB#20:                                #   in Loop: Header=BB6_18 Depth=1
	movsbl	%bl, %r12d
.LBB6_21:                               # %_ZN12StreamBufferdeEv.exit23
                                        #   in Loop: Header=BB6_18 Depth=1
	leal	(%rdx,%rdx,4), %ebx
	incl	%ecx
	movl	%ecx, 1048584(%rbp)
	cmpl	%eax, %ecx
	jl	.LBB6_22
# BB#26:                                #   in Loop: Header=BB6_18 Depth=1
	movl	$0, 1048584(%rbp)
	movq	(%rbp), %rdi
	callq	fileno
	movl	$1048576, %edx          # imm = 0x100000
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	read
	movl	%eax, 1048588(%rbp)
	movl	1048584(%rbp), %ecx
.LBB6_22:                               # %_ZN12StreamBufferppEv.exit20.backedge
                                        #   in Loop: Header=BB6_18 Depth=1
	leal	-48(%r12,%rbx,2), %edx
	cmpl	%eax, %ecx
	jge	.LBB6_24
# BB#23:                                # %_ZN12StreamBufferppEv.exit20.backedge._ZN12StreamBufferdeEv.exit21_crit_edge
                                        #   in Loop: Header=BB6_18 Depth=1
	movslq	%ecx, %rsi
	movzbl	8(%rbp,%rsi), %ebx
	cmpb	$47, %bl
	jg	.LBB6_18
.LBB6_24:                               # %.critedge
	movl	%edx, %eax
	negl	%eax
	testb	%r15b, %r15b
	cmovel	%edx, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_25:                               # %_ZN12StreamBufferdeEv.exit18.thread
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %r14
	movq	%rbp, %rdi
	callq	_ZN12StreamBufferdeEv
	movl	%eax, %ecx
	movl	$.L.str.55, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ecx, %edx
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$3, %edi
	callq	exit
.Lfunc_end6:
	.size	_ZL8parseIntI12StreamBufferEiRT_, .Lfunc_end6-_ZL8parseIntI12StreamBufferEiRT_
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"restarts              : %lld\n"
	.size	.L.str, 30

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"conflicts             : %-12lld\n"
	.size	.L.str.5, 33

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"decisions             : %-12lld   (%4.2f %% random)\n"
	.size	.L.str.6, 53

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"propagations          : %-12lld\n"
	.size	.L.str.7, 33

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"conflict literals     : %-12lld   (%4.2f %% deleted)\n"
	.size	.L.str.8, 54

	.type	solver,@object          # @solver
	.bss
	.globl	solver
	.p2align	3
solver:
	.quad	0
	.size	solver, 8

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.9:
	.asciz	"USAGE: %s [options] <input-file> <result-output-file>\n\n  where input may be either in plain or gzipped DIMACS.\n\n"
	.size	.L.str.9, 113

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"OPTIONS:\n\n"
	.size	.L.str.10, 11

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"  -polarity-mode = {true,false,rnd}\n"
	.size	.L.str.11, 37

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"  -decay         = <num> [ 0 - 1 ]\n"
	.size	.L.str.12, 36

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"  -rnd-freq      = <num> [ 0 - 1 ]\n"
	.size	.L.str.13, 36

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"  -verbosity     = {0,1,2}\n"
	.size	.L.str.14, 28

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"-polarity-mode="
	.size	.L.str.16, 16

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"true"
	.size	.L.str.17, 5

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"false"
	.size	.L.str.18, 6

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"rnd"
	.size	.L.str.19, 4

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"ERROR! unknown polarity-mode %s\n"
	.size	.L.str.20, 33

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"-rnd-freq="
	.size	.L.str.21, 11

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%lf"
	.size	.L.str.22, 4

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"ERROR! illegal rnd-freq constant %s\n"
	.size	.L.str.23, 37

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"-decay="
	.size	.L.str.24, 8

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"ERROR! illegal decay constant %s\n"
	.size	.L.str.25, 34

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"-verbosity="
	.size	.L.str.26, 12

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"ERROR! illegal verbosity level %s\n"
	.size	.L.str.27, 35

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"-h"
	.size	.L.str.28, 3

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"-help"
	.size	.L.str.29, 6

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"--help"
	.size	.L.str.30, 7

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"-"
	.size	.L.str.31, 2

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"ERROR! unknown flag %s\n"
	.size	.L.str.32, 24

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"This is MiniSat 2.0 beta\n"
	.size	.L.str.33, 26

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Reading from standard input... Use '-h' or '--help' for help.\n"
	.size	.L.str.34, 63

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"rb"
	.size	.L.str.35, 3

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"ERROR! Could not open file: %s\n"
	.size	.L.str.36, 32

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"<stdin>"
	.size	.L.str.37, 8

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"============================[ Problem Statistics ]=============================\n"
	.size	.L.str.38, 81

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"|                                                                             |\n"
	.size	.L.str.39, 81

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"wb"
	.size	.L.str.40, 3

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"Solved by unit propagation\n"
	.size	.L.str.41, 28

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"UNSAT\n"
	.size	.L.str.42, 7

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"UNSATISFIABLE\n"
	.size	.L.str.43, 15

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"SATISFIABLE\n"
	.size	.L.str.44, 13

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"SAT\n"
	.size	.L.str.45, 5

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"%s%s%d"
	.size	.L.str.46, 7

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.zero	1
	.size	.L.str.47, 1

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	" "
	.size	.L.str.48, 2

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	" 0\n"
	.size	.L.str.49, 4

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"/proc/%d/statm"
	.size	.L.str.50, 15

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"%d"
	.size	.L.str.51, 3

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"|  Number of variables:  %-12d                                         |\n"
	.size	.L.str.53, 74

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"|  Number of clauses:    %-12d                                         |\n"
	.size	.L.str.54, 74

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"PARSE ERROR! Unexpected char: %c\n"
	.size	.L.str.55, 34

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"UNSATISFIABLE"
	.size	.Lstr, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
