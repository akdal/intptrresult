	.text
	.file	"bstr_i.bc"
	.globl	bstr_i
	.p2align	4, 0x90
	.type	bstr_i,@function
bstr_i:                                 # @bstr_i
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#1:                                 # %.lr.ph.preheader
	movb	(%rdi), %dl
	testb	%dl, %dl
	je	.LBB0_6
# BB#2:                                 # %.lr.ph21.preheader
	incq	%rdi
	xorl	%eax, %eax
	movabsq	$844424930131969, %r8   # imm = 0x3000000000001
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph21
                                        # =>This Inner Loop Header: Depth=1
	movsbq	%dl, %rcx
	movl	%ecx, %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	cmpq	$63, %rcx
	ja	.LBB0_6
# BB#4:                                 # %.lr.ph21
                                        #   in Loop: Header=BB0_3 Depth=1
	andq	%r8, %rsi
	je	.LBB0_6
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_3 Depth=1
	andb	$1, %dl
	movzbl	%dl, %ecx
	leal	(%rcx,%rax,2), %eax
	movzbl	(%rdi), %edx
	incq	%rdi
	testb	%dl, %dl
	jne	.LBB0_3
.LBB0_6:                                # %.critedge
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end0:
	.size	bstr_i, .Lfunc_end0-bstr_i
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
