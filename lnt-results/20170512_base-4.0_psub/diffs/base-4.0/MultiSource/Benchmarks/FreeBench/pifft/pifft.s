	.text
	.file	"pifft.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4602678819172646912     # double 0.5
.LCPI0_1:
	.quad	4372995238176751616     # double 2.2204460492503131E-16
.LCPI0_2:
	.quad	4636737291354636288     # double 100
.LCPI0_3:
	.quad	4599075939470750515     # double 0.29999999999999999
.LCPI0_4:
	.quad	4613937818241073152     # double 3
.LCPI0_5:
	.quad	4632233691727265792     # double 50
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$2, %ebp
	jne	.LBB0_69
# BB#1:
	movl	$.Lstr, %edi
	callq	puts
	movq	8(%rbx), %rdi
	movl	$.L.str.5, %esi
	callq	fopen
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB0_2
# BB#4:
	xorl	%ebx, %ebx
	leaq	8(%rsp), %rdx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fscanf
	movl	$.Lstr.1, %edi
	callq	puts
	movl	8(%rsp), %eax
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	incl	%ebx
	movl	$1, %r13d
	movl	%ebx, %ecx
	shll	%cl, %r13d
	cmpl	%eax, %r13d
	jl	.LBB0_5
# BB#6:
	movl	%r13d, 8(%rsp)
	cvtsi2sdl	%r13d, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_8
# BB#7:                                 # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB0_8:                                # %.split
	cvttsd2si	%xmm1, %eax
	cltq
	leaq	12(,%rax,4), %rdi
	callq	malloc
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movslq	8(%rsp), %rbp
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	movslq	%eax, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leal	4(%r13), %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movslq	%eax, %r14
	shlq	$2, %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	leaq	16(,%rbp,8), %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 112(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_9
# BB#10:
	movq	%r13, 144(%rsp)         # 8-byte Spill
	leal	2(%r13), %ebp
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax)
	movq	%rax, %r8
	movl	$10, %r14d
	movl	$10, %esi
	movl	%ebp, %edi
	movq	40(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%r12, %rcx
	movq	64(%rsp), %r9           # 8-byte Reload
	callq	mp_mul_radix_test
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	imull	$100, %ebp, %eax
	sarl	$2, %eax
	cvtsi2sdl	%eax, %xmm1
	mulsd	.LCPI0_1(%rip), %xmm1
	addsd	%xmm0, %xmm1
	mulsd	.LCPI0_2(%rip), %xmm1
	movl	$1, %r12d
	movsd	.LCPI0_3(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	movq	104(%rsp), %r13         # 8-byte Reload
	jbe	.LBB0_13
# BB#11:                                # %.lr.ph
	mulsd	.LCPI0_2(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB0_12
# BB#56:                                # %.lr.ph.1
	mulsd	.LCPI0_2(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB0_57
# BB#58:                                # %.lr.ph.2
	mulsd	.LCPI0_2(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB0_59
# BB#60:                                # %.lr.ph.3
	mulsd	.LCPI0_2(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB0_61
# BB#62:                                # %.lr.ph.4
	mulsd	.LCPI0_2(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB0_63
# BB#64:                                # %.lr.ph.5
	mulsd	.LCPI0_2(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB0_65
# BB#66:                                # %.lr.ph.6
	mulsd	.LCPI0_2(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB0_67
# BB#68:                                # %.lr.ph.7
	movl	$1000000000, %r14d      # imm = 0x3B9ACA00
	movl	$9, %r12d
	jmp	.LBB0_13
.LBB0_12:
	movl	$100, %r14d
	movl	$2, %r12d
	jmp	.LBB0_13
.LBB0_57:
	movl	$1000, %r14d            # imm = 0x3E8
	movl	$3, %r12d
	jmp	.LBB0_13
.LBB0_59:
	movl	$10000, %r14d           # imm = 0x2710
	movl	$4, %r12d
	jmp	.LBB0_13
.LBB0_61:
	movl	$100000, %r14d          # imm = 0x186A0
	movl	$5, %r12d
	jmp	.LBB0_13
.LBB0_63:
	movl	$1000000, %r14d         # imm = 0xF4240
	movl	$6, %r12d
	jmp	.LBB0_13
.LBB0_65:
	movl	$10000000, %r14d        # imm = 0x989680
	movl	$7, %r12d
	jmp	.LBB0_13
.LBB0_67:
	movl	$100000000, %r14d       # imm = 0x5F5E100
	movl	$8, %r12d
.LBB0_13:                               # %._crit_edge
	movl	8(%rsp), %esi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	printf
	movl	%r12d, %esi
	movl	%ebx, 140(%rsp)         # 4-byte Spill
	movl	%ebx, %ecx
	shll	%cl, %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.12, %edx
	movl	%r14d, %ebp
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	movl	12(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %edi
	movl	%r12d, 80(%rsp)         # 4-byte Spill
	movl	%r12d, %esi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	callq	mp_sscanf
	movl	8(%rsp), %eax
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	104(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rcx
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	movl	%r14d, %r12d
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	callq	mp_sqrt
	addq	$48, %rsp
.Lcfi19:
	.cfi_adjust_cfa_offset -48
	movl	(%rbp), %eax
	movl	%eax, (%r15)
	movl	40(%rsp), %ebx          # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	addq	$4, %rbp
	leaq	4(%r15), %rdx
	movsd	.LCPI0_4(%rip), %xmm1   # xmm1 = mem[0],zero
	movl	%r12d, %edi
	movsd	%xmm0, 160(%rsp)        # 8-byte Spill
	movq	%rbp, %rsi
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	callq	mp_unsgn_imul
	cmpl	$0, (%r15)
	movl	%ebx, %ebp
	jne	.LBB0_15
# BB#14:
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
.LBB0_15:                               # %mp_imul.exit
	movl	$.L.str.13, %edx
	movl	%r12d, %edi
	movl	80(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rcx
	callq	mp_sscanf
	movl	%r12d, %edi
	movl	%ebp, %esi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	mp_add
	movl	8(%rsp), %eax
	subq	$8, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	movl	%r12d, %edi
	movl	%ebp, %esi
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	callq	mp_sqrt
	addq	$48, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset -48
	movl	$.L.str.14, %edx
	movl	%r12d, %edi
	movl	%ebx, %esi
	movq	%r15, %rcx
	callq	mp_sscanf
	movl	%r12d, %edi
	movl	%ebp, %esi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r15, %r8
	callq	mp_sub
	movl	%r12d, %edi
	movl	%ebp, %esi
	movq	%r13, %rdx
	movq	%r13, %rcx
	movq	%r13, %r8
	callq	mp_add
	movl	%r12d, %edi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r8
	callq	mp_sub
	movl	%r12d, %edi
	movl	%ebp, %esi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	mp_add
	movl	$.Lstr.2, %edi
	callq	puts
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	3(%rax), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	cltq
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	$4, %eax
	movl	%r12d, %r14d
	movq	%r15, 176(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_16:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_23 Depth 2
	movl	%eax, %r12d
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r13, %rcx
	movq	%r15, %r8
	callq	mp_add
	movl	8(%r15), %eax
	xorl	%edx, %edx
	cmpl	$1, %eax
	sete	%dl
	movl	$0, %ecx
	movl	$-1, %esi
	cmovel	%esi, %ecx
	subl	%edx, 4(%r15)
	movl	52(%rsp), %esi          # 4-byte Reload
	subl	%edx, %esi
	cmpl	$2, %esi
	jge	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_16 Depth=1
	movq	32(%rsp), %r10          # 8-byte Reload
	cmpl	$1, %eax
	je	.LBB0_25
	jmp	.LBB0_26
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_16 Depth=1
	xorl	%esi, %esi
	cmpl	$1, %eax
	sete	%bl
	movq	128(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rcx), %edx
	testb	$1, %dl
	movq	32(%rsp), %r10          # 8-byte Reload
	jne	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_16 Depth=1
	movl	$2, %edi
	cmpl	$3, %edx
	jne	.LBB0_22
	jmp	.LBB0_24
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph.i.prol
                                        #   in Loop: Header=BB0_16 Depth=1
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	sete	%cl
	movl	$0, %edi
	cmovel	%ebp, %edi
	addl	8(%r15,%rcx,4), %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	negl	%ecx
	sarl	%edi
	movl	%edi, 8(%r15)
	movl	$3, %edi
	cmpl	$3, %edx
	je	.LBB0_24
.LBB0_22:                               # %.lr.ph.preheader.i.new
                                        #   in Loop: Header=BB0_16 Depth=1
	movb	%bl, %sil
	subq	%rdi, %rdx
	addq	%rdi, %rsi
	movq	152(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,4), %rsi
	leaq	(%rbx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph.i
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	andl	%ebp, %ecx
	addl	-4(%rsi), %ecx
	movl	%ecx, %ebx
	andl	$1, %ebx
	negl	%ebx
	sarl	%ecx
	movl	%ecx, -4(%rdi)
	andl	%ebp, %ebx
	addl	(%rsi), %ebx
	movl	%ebx, %ecx
	andl	$1, %ecx
	negl	%ecx
	sarl	%ebx
	movl	%ebx, (%rdi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB0_23
.LBB0_24:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_16 Depth=1
	cmpl	$1, %eax
	jne	.LBB0_26
.LBB0_25:                               #   in Loop: Header=BB0_16 Depth=1
	andl	%ebp, %ecx
	sarl	%ecx
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	%ecx, (%r15,%rax,4)
.LBB0_26:                               # %mp_idiv_2.exit
                                        #   in Loop: Header=BB0_16 Depth=1
	leal	(%r12,%r12), %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movl	8(%rsp), %eax
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r13, %rcx
	movq	%rdx, %r8
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r9
	movq	%rdx, %r13
	movq	%r12, 168(%rsp)         # 8-byte Spill
	movq	64(%rsp), %r12          # 8-byte Reload
	pushq	%r12
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	movq	80(%rsp), %rbx          # 8-byte Reload
	pushq	%rbx
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	movq	%r10, %rbp
	movq	56(%rsp), %r15          # 8-byte Reload
	pushq	%r15
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	callq	mp_mul
	addq	$48, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset -48
	movl	8(%rsp), %eax
	subq	$8, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	movl	20(%rsp), %edi          # 4-byte Reload
	movl	48(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rdx
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	%r14, %r8
	movl	20(%rsp), %r14d         # 4-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	pushq	%r12
	movq	120(%rsp), %r12         # 8-byte Reload
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	movl	72(%rsp), %ebp          # 4-byte Reload
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	movq	216(%rsp), %r15         # 8-byte Reload
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	callq	mp_sqrt
	addq	$48, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset -48
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	callq	mp_sub
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	%r12, %rcx
	movq	%r12, %r8
	callq	mp_add
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	%rdx, %r8
	callq	mp_sub
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%r13, %r8
	movq	%r12, %r13
	callq	mp_add
	xorl	%ebx, %ebx
	subl	4(%r15), %ebx
	cmpl	$0, (%r15)
	cmovel	%r14d, %ebx
	shll	$2, %ebx
	movl	%ebx, %esi
	imull	80(%rsp), %esi          # 4-byte Folded Reload
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	%r14d, %ebx
	movl	84(%rsp), %eax          # 4-byte Reload
	jle	.LBB0_16
# BB#27:
	movl	8(%r15), %eax
	xorl	%edx, %edx
	xorl	%esi, %esi
	cmpl	$1, %eax
	sete	%sil
	movl	$-1, %ecx
	cmovnel	%edx, %ecx
	subl	%esi, 4(%r15)
	movl	52(%rsp), %edx          # 4-byte Reload
	subl	%esi, %edx
	cmpl	$2, %edx
	jge	.LBB0_29
# BB#28:
	movq	24(%rsp), %r9           # 8-byte Reload
	cmpl	$1, %eax
	je	.LBB0_36
	jmp	.LBB0_37
.LBB0_29:                               # %.lr.ph.preheader.i300
	xorl	%esi, %esi
	cmpl	$1, %eax
	sete	%r8b
	movq	128(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rcx), %edx
	testb	$1, %dl
	movq	24(%rsp), %r9           # 8-byte Reload
	jne	.LBB0_31
# BB#30:
	movl	$2, %edi
	jmp	.LBB0_32
.LBB0_31:                               # %.lr.ph.i305.prol
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	sete	%cl
	cmovel	40(%rsp), %edi          # 4-byte Folded Reload
	addl	8(%r15,%rcx,4), %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	negl	%ecx
	sarl	%edi
	movl	%edi, 8(%r15)
	movl	$3, %edi
.LBB0_32:                               # %.lr.ph.i305.prol.loopexit
	cmpl	$3, %edx
	movl	40(%rsp), %ebp          # 4-byte Reload
	je	.LBB0_35
# BB#33:                                # %.lr.ph.preheader.i300.new
	movb	%r8b, %sil
	subq	%rdi, %rdx
	addq	%rdi, %rsi
	leaq	4(%r15,%rsi,4), %rsi
	leaq	4(%r15,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_34:                               # %.lr.ph.i305
                                        # =>This Inner Loop Header: Depth=1
	andl	%ebp, %ecx
	addl	-4(%rsi), %ecx
	movl	%ecx, %ebx
	andl	$1, %ebx
	negl	%ebx
	sarl	%ecx
	movl	%ecx, -4(%rdi)
	andl	%ebp, %ebx
	addl	(%rsi), %ebx
	movl	%ebx, %ecx
	andl	$1, %ecx
	negl	%ecx
	sarl	%ebx
	movl	%ebx, (%rdi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB0_34
.LBB0_35:                               # %._crit_edge.i307
	cmpl	$1, %eax
	jne	.LBB0_37
.LBB0_36:
	andl	40(%rsp), %ecx          # 4-byte Folded Reload
	sarl	%ecx
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	%ecx, (%r15,%rax,4)
.LBB0_37:                               # %mp_idiv_2.exit308
	movl	8(%rsp), %r8d
	movl	%r14d, %edi
	movl	40(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	movq	%r15, %rdx
	movq	%r15, %rcx
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	movq	80(%rsp), %rax          # 8-byte Reload
	pushq	%rax
	movq	%rax, %r12
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	callq	mp_squh
	addq	$16, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset -16
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movq	%rbx, %r8
	callq	mp_add
	movl	8(%rsp), %r10d
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	%rbx, %r8
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r9
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	callq	mp_mul
	addq	$48, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset -48
	movl	12(%rsp), %edi          # 4-byte Reload
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r8
	callq	mp_sub
	movl	8(%rsp), %r10d
	subq	$8, %rsp
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	movl	20(%rsp), %edi          # 4-byte Reload
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	%r14, %r12
	movl	20(%rsp), %ebx          # 4-byte Reload
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	96(%rsp), %r9           # 8-byte Reload
	movq	72(%rsp), %r14          # 8-byte Reload
	pushq	%r14
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	callq	mp_inv
	addq	$48, %rsp
.Lcfi57:
	.cfi_adjust_cfa_offset -48
	movl	8(%rsp), %r9d
	movl	%ebx, %edi
	movl	%ebp, %esi
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	pushq	%r14
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	callq	mp_squ
	addq	$32, %rsp
.Lcfi62:
	.cfi_adjust_cfa_offset -32
	movl	%ebx, %r14d
	movl	%ebx, %edi
	movl	%ebp, %esi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r13, %r8
	callq	mp_sub
	movl	8(%r15), %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	cmpl	$1, %r8d
	sete	%sil
	movl	$-1, %ecx
	cmovnel	%edx, %ecx
	subl	%esi, 4(%r15)
	movl	52(%rsp), %edx          # 4-byte Reload
	subl	%esi, %edx
	cmpl	$2, %edx
	jge	.LBB0_39
# BB#38:
	movq	56(%rsp), %r12          # 8-byte Reload
	cmpl	$1, %r8d
	je	.LBB0_46
	jmp	.LBB0_47
.LBB0_39:                               # %.lr.ph.preheader.i311
	xorl	%edx, %edx
	cmpl	$1, %r8d
	sete	%sil
	movq	128(%rsp), %rax         # 8-byte Reload
	addl	%ecx, %eax
	testb	$1, %al
	jne	.LBB0_41
# BB#40:
	movl	$2, %edi
	jmp	.LBB0_42
.LBB0_41:                               # %.lr.ph.i316.prol
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	cmpl	$1, %r8d
	sete	%cl
	cmovel	%ebp, %edi
	addl	8(%r15,%rcx,4), %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	negl	%ecx
	sarl	%edi
	movl	%edi, 8(%r15)
	movl	$3, %edi
.LBB0_42:                               # %.lr.ph.i316.prol.loopexit
	movq	56(%rsp), %r12          # 8-byte Reload
	cmpl	$3, %eax
	je	.LBB0_45
# BB#43:                                # %.lr.ph.preheader.i311.new
	movb	%sil, %dl
	subq	%rdi, %rax
	addq	%rdi, %rdx
	leaq	4(%r15,%rdx,4), %rdx
	leaq	4(%r15,%rdi,4), %rsi
	.p2align	4, 0x90
.LBB0_44:                               # %.lr.ph.i316
                                        # =>This Inner Loop Header: Depth=1
	andl	%ebp, %ecx
	addl	-4(%rdx), %ecx
	movl	%ecx, %edi
	andl	$1, %edi
	negl	%edi
	sarl	%ecx
	movl	%ecx, -4(%rsi)
	andl	%ebp, %edi
	addl	(%rdx), %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	negl	%ecx
	sarl	%edi
	movl	%edi, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rax
	jne	.LBB0_44
.LBB0_45:                               # %._crit_edge.i318
	cmpl	$1, %r8d
	jne	.LBB0_47
.LBB0_46:
	andl	%ebp, %ecx
	sarl	%ecx
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	%ecx, (%r15,%rax,4)
.LBB0_47:                               # %mp_idiv_2.exit319
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r8
	callq	mp_sub
	movl	8(%rsp), %eax
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%rbx, %r8
	movq	%r12, %r9
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %r12
	pushq	%rbp
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	movq	80(%rsp), %rbp          # 8-byte Reload
	pushq	%rbp
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	callq	mp_mul
	addq	$48, %rsp
.Lcfi69:
	.cfi_adjust_cfa_offset -48
	cmpl	$0, 168(%rsp)           # 4-byte Folded Reload
	je	.LBB0_55
# BB#48:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	jle	.LBB0_50
# BB#49:
	movl	84(%rsp), %ecx          # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB0_54
	jmp	.LBB0_52
.LBB0_50:
	negl	%eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	84(%rsp), %ecx          # 4-byte Reload
	negl	%ecx
	testl	%eax, %eax
	je	.LBB0_52
.LBB0_54:
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	$4, %rsi
	cvtsi2sdl	%ecx, %xmm1
	movl	%r14d, %edi
	movsd	160(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%rsi, %rdx
	callq	mp_unsgn_idiv
	jmp	.LBB0_55
.LBB0_52:
	cmpl	$-1, %r14d
	jl	.LBB0_55
# BB#53:                                # %.lr.ph.preheader.i.i
	movl	52(%rsp), %eax          # 4-byte Reload
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	memset
.LBB0_55:                               # %mp_idiv.exit
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	80(%rsp), %ebx          # 4-byte Reload
	imull	%ebx, %r14d
	addl	$32, %r14d
	movslq	%r14d, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	144(%rsp), %rdi         # 8-byte Reload
	orl	$1, %edi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%ebx, %esi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	mp_sprintf
	movq	%r14, %rdi
	callq	puts
	movq	%r14, %rdi
	callq	free
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	movl	8(%rsp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_5(%rip), %xmm0
	movl	140(%rsp), %eax         # 4-byte Reload
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm0
	movl	$.L.str.18, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_69:
	movl	$1, %edi
	callq	exit
.LBB0_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$35, %esi
	jmp	.LBB0_3
.LBB0_9:
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$20, %esi
.LBB0_3:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4611686018427387904     # double 2
.LCPI1_1:
	.quad	4607182418800017408     # double 1
.LCPI1_2:
	.quad	4602678819172646912     # double 0.5
.LCPI1_4:
	.quad	-4620693217682128896    # double -0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_3:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	mp_mul_radix_test
	.p2align	4, 0x90
	.type	mp_mul_radix_test,@function
mp_mul_radix_test:                      # @mp_mul_radix_test
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 80
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rcx, %rbx
	movl	%edx, %r12d
	movl	%esi, %r13d
	movl	%r12d, %eax
	sarl	%eax
	leal	1(%rax), %ebp
	cmpl	%edi, %eax
	cmovgel	%edi, %ebp
	leal	-1(%r13), %eax
	cvtsi2sdl	%eax, %xmm2
	movslq	%r12d, %r15
	movsd	%xmm2, 8(%rbx,%r15,8)
	cmpl	%r15d, %ebp
	jge	.LBB1_2
# BB#1:                                 # %.lr.ph49.preheader
	leal	-1(%r15), %eax
	subl	%ebp, %eax
	movq	%r15, %rcx
	subq	%rax, %rcx
	leaq	(%rbx,%rcx,8), %rdi
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	memset
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB1_2:                                # %._crit_edge50
	cmpl	$3, %ebp
	jl	.LBB1_16
# BB#3:                                 # %.lr.ph
	leal	1(%r13), %eax
	shrl	$31, %eax
	leal	1(%r13,%rax), %eax
	sarl	%eax
	cvtsi2sdl	%eax, %xmm0
	movslq	%ebp, %rax
	leaq	-2(%rax), %rcx
	cmpq	$4, %rcx
	jb	.LBB1_15
# BB#4:                                 # %min.iters.checked
	movq	%rcx, %r9
	andq	$-4, %r9
	movq	%rcx, %rsi
	andq	$-4, %rsi
	je	.LBB1_15
# BB#5:                                 # %vector.ph
	leaq	-4(%rsi), %r8
	movl	%r8d, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_6
# BB#7:                                 # %vector.body.prol.preheader
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	-8(%rbx,%rax,8), %rdx
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, (%rdx,%rbp,8)
	movups	%xmm1, -16(%rdx,%rbp,8)
	addq	$-4, %rbp
	incq	%rdi
	jne	.LBB1_8
# BB#9:                                 # %vector.body.prol.loopexit.unr-lcssa
	negq	%rbp
	cmpq	$12, %r8
	jae	.LBB1_11
	jmp	.LBB1_13
.LBB1_6:
	xorl	%ebp, %ebp
	cmpq	$12, %r8
	jb	.LBB1_13
.LBB1_11:                               # %vector.ph.new
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movq	%rsi, %rdi
	subq	%rbp, %rdi
	leaq	-1(%rax), %rdx
	subq	%rbp, %rdx
	leaq	(%rbx,%rdx,8), %rbp
	.p2align	4, 0x90
.LBB1_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, (%rbp)
	movups	%xmm1, -16(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	%xmm1, -48(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	%xmm1, -80(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	%xmm1, -112(%rbp)
	addq	$-128, %rbp
	addq	$-16, %rdi
	jne	.LBB1_12
.LBB1_13:                               # %middle.block
	cmpq	%rsi, %rcx
	je	.LBB1_16
# BB#14:
	subq	%r9, %rax
	.p2align	4, 0x90
.LBB1_15:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, (%rbx,%rax,8)
	decq	%rax
	cmpq	$2, %rax
	jg	.LBB1_15
.LBB1_16:                               # %._crit_edge
	leaq	1(%r15), %r14
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	%xmm0, 16(%rbx)
	leaq	8(%rbx), %r13
	movsd	%xmm2, 8(%rbx)
	movq	$0, (%rbx)
	movl	$1, %esi
	movl	%r15d, %edi
	movq	%r13, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %r8
	callq	rdft
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	movsd	%xmm0, (%rbx)
	movupd	8(%rbx), %xmm0
	mulpd	%xmm0, %xmm0
	movupd	%xmm0, 8(%rbx)
	cmpl	$4, %r15d
	jl	.LBB1_25
# BB#17:                                # %.lr.ph.preheader.i
	leaq	-4(%r15), %rax
	shrq	%rax
	leaq	1(%rax), %rcx
	cmpq	$2, %rcx
	jae	.LBB1_19
# BB#18:
	movl	$3, %eax
	jmp	.LBB1_24
.LBB1_19:                               # %min.iters.checked62
	movl	%ecx, %edx
	andl	$1, %edx
	subq	%rdx, %rcx
	je	.LBB1_20
# BB#21:                                # %vector.body58.preheader
	leaq	5(%rax,%rax), %rax
	leaq	(%rdx,%rdx), %rsi
	subq	%rsi, %rax
	leaq	24(%rbx), %rsi
	.p2align	4, 0x90
.LBB1_22:                               # %vector.body58
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rsi), %xmm0
	movupd	16(%rsi), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	mulpd	%xmm1, %xmm1
	mulpd	%xmm0, %xmm0
	movapd	%xmm0, %xmm4
	unpcklpd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0]
	unpckhpd	%xmm1, %xmm0    # xmm0 = xmm0[1],xmm1[1]
	subpd	%xmm0, %xmm4
	addpd	%xmm2, %xmm2
	mulpd	%xmm3, %xmm2
	movapd	%xmm4, %xmm0
	unpcklpd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0]
	movhlps	%xmm4, %xmm2            # xmm2 = xmm4[1],xmm2[1]
	movups	%xmm2, 16(%rsi)
	movupd	%xmm0, (%rsi)
	addq	$32, %rsi
	addq	$-2, %rcx
	jne	.LBB1_22
# BB#23:                                # %middle.block59
	testq	%rdx, %rdx
	jne	.LBB1_24
	jmp	.LBB1_25
.LBB1_20:
	movl	$3, %eax
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rbx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rbx,%rax,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx,%rax,8)
	movsd	%xmm0, 8(%rbx,%rax,8)
	addq	$2, %rax
	cmpq	%r15, %rax
	jl	.LBB1_24
.LBB1_25:                               # %mp_mul_csqu.exit
	movsd	(%rbx,%r14,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, (%rbx,%r14,8)
	movl	$-1, %esi
	movl	%r12d, %edi
	movq	%r13, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rbp, %r8
	callq	rdft
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%r12d, %xmm2
	movsd	(%rbx,%r14,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	mulsd	.LCPI1_2(%rip), %xmm1
	movapd	.LCPI1_3(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm1, %xmm3
	xorpd	%xmm0, %xmm0
	movapd	%xmm1, %xmm4
	cmpltsd	%xmm0, %xmm4
	movapd	%xmm4, %xmm5
	andnpd	%xmm1, %xmm5
	andpd	%xmm3, %xmm4
	orpd	%xmm5, %xmm4
	movsd	8(%rbx), %xmm6          # xmm6 = mem[0],zero
	subsd	%xmm4, %xmm6
	movsd	%xmm6, (%rbx,%r14,8)
	testl	%r12d, %r12d
	jle	.LBB1_32
# BB#26:                                # %.lr.ph.i44.preheader
	movsd	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm2, %xmm1
	movsd	.LCPI1_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	(%rsp), %xmm5           # 8-byte Reload
                                        # xmm5 = mem[0],zero
	divsd	%xmm5, %xmm2
	mulsd	%xmm2, %xmm2
	decq	%r14
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	movsd	.LCPI1_2(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI1_4(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	.LCPI1_3(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00]
	xorl	%ecx, %ecx
	jmp	.LBB1_27
	.p2align	4, 0x90
.LBB1_31:                               # %..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB1_27 Depth=1
	addl	%ecx, %eax
	movsd	(%rbx,%r14,8), %xmm6    # xmm6 = mem[0],zero
	decq	%r14
	movl	%edx, %ecx
.LBB1_27:                               # %.lr.ph.i44
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm1, %xmm6
	xorps	%xmm7, %xmm7
	cvtsi2sdl	%eax, %xmm7
	addsd	%xmm6, %xmm7
	addsd	%xmm3, %xmm7
	mulsd	%xmm2, %xmm7
	cvttsd2si	%xmm7, %edx
	decl	%edx
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%edx, %xmm6
	subsd	%xmm6, %xmm7
	mulsd	%xmm5, %xmm7
	cvttsd2si	%xmm7, %eax
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%eax, %xmm6
	subsd	%xmm6, %xmm7
	mulsd	%xmm5, %xmm7
	cvttsd2si	%xmm7, %esi
	addsd	%xmm4, %xmm7
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%esi, %xmm6
	subsd	%xmm6, %xmm7
	ucomisd	%xmm0, %xmm7
	ja	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_27 Depth=1
	xorpd	%xmm8, %xmm7
	ucomisd	%xmm0, %xmm7
	jbe	.LBB1_30
.LBB1_29:                               #   in Loop: Header=BB1_27 Depth=1
	movapd	%xmm7, %xmm0
.LBB1_30:                               #   in Loop: Header=BB1_27 Depth=1
	cmpq	$2, %r14
	jge	.LBB1_31
.LBB1_32:                               # %mp_mul_d2i_test.exit
	addsd	%xmm0, %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	mp_mul_radix_test, .Lfunc_end1-mp_mul_radix_test
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI2_1:
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
.LCPI2_2:
	.long	100000000               # 0x5f5e100
	.long	100000000               # 0x5f5e100
	.long	100000000               # 0x5f5e100
	.long	100000000               # 0x5f5e100
	.text
	.globl	mp_sscanf
	.p2align	4, 0x90
	.type	mp_sscanf,@function
mp_sscanf:                              # @mp_sscanf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 48
.Lcfi88:
	.cfi_offset %rbx, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movl	%edi, %r14d
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	cmpb	$32, (%rbx)
	leaq	1(%rbx), %rbx
	je	.LBB2_1
# BB#2:
	movl	$1, (%r15)
	movb	-1(%rbx), %al
	cmpb	$43, %al
	je	.LBB2_6
# BB#3:
	cmpb	$45, %al
	jne	.LBB2_4
# BB#5:
	movl	$-1, (%r15)
	jmp	.LBB2_6
.LBB2_4:
	decq	%rbx
	jmp	.LBB2_6
	.p2align	4, 0x90
.LBB2_7:                                # %.critedge
                                        #   in Loop: Header=BB2_6 Depth=1
	incq	%rbx
.LBB2_6:                                # %.preheader101
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %eax
	orb	$16, %al
	cmpb	$48, %al
	je	.LBB2_7
# BB#8:
	movl	$0, 4(%rsp)
	leaq	1(%rbx), %rdi
	movabsq	$12884901891, %rax      # imm = 0x300000003
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_11:                               #   in Loop: Header=BB2_9 Depth=1
	incq	%rdi
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rdi), %ecx
	movl	%ecx, %edx
	addb	$-68, %dl
	cmpb	$33, %dl
	ja	.LBB2_10
# BB#12:                                #   in Loop: Header=BB2_9 Depth=1
	movzbl	%dl, %edx
	btq	%rdx, %rax
	jb	.LBB2_13
.LBB2_10:                               #   in Loop: Header=BB2_9 Depth=1
	testb	%cl, %cl
	jne	.LBB2_11
	jmp	.LBB2_15
.LBB2_13:
	leaq	4(%rsp), %rdx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	callq	sscanf
	cmpl	$1, %eax
	je	.LBB2_15
# BB#14:
	movl	$0, 4(%rsp)
.LBB2_15:                               # %.loopexit100
	movb	(%rbx), %al
	cmpb	$46, %al
	je	.LBB2_22
# BB#16:                                # %.loopexit100
	testb	%al, %al
	je	.LBB2_26
# BB#17:
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB2_18:                               # %.preheader96
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%rax), %ecx
	incq	%rax
	cmpb	$32, %cl
	je	.LBB2_18
# BB#19:                                # %.preheader95
	testb	%cl, %cl
	je	.LBB2_26
# BB#20:                                # %.preheader95
	addb	$-48, %cl
	cmpb	$9, %cl
	ja	.LBB2_26
# BB#21:                                # %.lr.ph115.preheader
	movl	4(%rsp), %ecx
	.p2align	4, 0x90
.LBB2_37:                               # %.lr.ph115
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_38 Depth 2
	incl	%ecx
	movl	%ecx, 4(%rsp)
	.p2align	4, 0x90
.LBB2_38:                               #   Parent Loop BB2_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	1(%rax), %edx
	incq	%rax
	cmpb	$32, %dl
	je	.LBB2_38
# BB#35:                                # %.loopexit
                                        #   in Loop: Header=BB2_37 Depth=1
	testb	%dl, %dl
	je	.LBB2_26
# BB#36:                                # %.loopexit
                                        #   in Loop: Header=BB2_37 Depth=1
	addb	$-48, %dl
	cmpb	$9, %dl
	jbe	.LBB2_37
	jmp	.LBB2_26
.LBB2_22:                               # %.preheader99.preheader
	movl	4(%rsp), %eax
.LBB2_23:                               # %.preheader99
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_24 Depth 2
	decl	%eax
	movl	%eax, 4(%rsp)
	.p2align	4, 0x90
.LBB2_24:                               #   Parent Loop BB2_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	1(%rbx), %ecx
	incq	%rbx
	cmpb	$32, %cl
	je	.LBB2_24
# BB#25:                                #   in Loop: Header=BB2_23 Depth=1
	cmpb	$48, %cl
	je	.LBB2_23
.LBB2_26:                               # %.critedge2
	movl	4(%rsp), %ecx
	movl	%ecx, %eax
	cltd
	idivl	%ebp
	movl	%eax, %edx
	imull	%ebp, %edx
	subl	%edx, %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	addl	%edx, %eax
	andl	%ebp, %edx
	addl	%ecx, %edx
	movl	%eax, 4(%r15)
	leal	1(%r14), %r10d
	decl	%ebp
	xorl	%esi, %esi
	movl	$2, %ecx
	jmp	.LBB2_27
	.p2align	4, 0x90
.LBB2_60:                               #   in Loop: Header=BB2_27 Depth=1
	incq	%rbx
.LBB2_27:                               # =>This Inner Loop Header: Depth=1
	movsbl	(%rbx), %edi
	cmpl	$32, %edi
	je	.LBB2_60
# BB#28:                                #   in Loop: Header=BB2_27 Depth=1
	cmpb	$46, %dil
	je	.LBB2_60
# BB#29:                                #   in Loop: Header=BB2_27 Depth=1
	testb	%dil, %dil
	je	.LBB2_30
# BB#54:                                #   in Loop: Header=BB2_27 Depth=1
	movl	%edi, %eax
	addb	$-48, %al
	cmpb	$9, %al
	ja	.LBB2_30
# BB#55:                                #   in Loop: Header=BB2_27 Depth=1
	leal	(%rsi,%rsi,4), %eax
	leal	-48(%rdi,%rax,2), %esi
	leal	-1(%rdx), %edi
	testl	%edx, %edx
	jle	.LBB2_57
# BB#56:                                #   in Loop: Header=BB2_27 Depth=1
	movl	%edi, %edx
	incq	%rbx
	jmp	.LBB2_27
.LBB2_57:                               #   in Loop: Header=BB2_27 Depth=1
	cmpl	%r10d, %ecx
	jg	.LBB2_58
# BB#59:                                #   in Loop: Header=BB2_27 Depth=1
	movslq	%ecx, %rax
	incl	%ecx
	movl	%esi, (%r15,%rax,4)
	xorl	%esi, %esi
	movl	%ebp, %edx
	jmp	.LBB2_60
.LBB2_58:
	movl	%edi, %edx
.LBB2_30:                               # %.preheader94
	testl	%edx, %edx
	js	.LBB2_48
# BB#31:                                # %.lr.ph110.preheader
	movl	%edx, %edi
	notl	%edi
	cmpl	$-2, %edi
	movl	$-1, %ebp
	cmovgl	%edi, %ebp
	leal	2(%rdx,%rbp), %edi
	cmpl	$7, %edi
	jbe	.LBB2_46
# BB#32:                                # %min.iters.checked
	movl	%edi, %r8d
	andl	$-8, %r8d
	movl	%edi, %ebx
	andl	$-8, %ebx
	je	.LBB2_46
# BB#33:                                # %vector.ph
	movl	$1, %ebp
	movd	%ebp, %xmm1
	movd	%esi, %xmm0
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	pshufd	$80, %xmm1, %xmm1       # xmm1 = xmm1[0,0,1,1]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	leal	-8(%rbx), %r9d
	movl	%r9d, %esi
	shrl	$3, %esi
	incl	%esi
	andl	$7, %esi
	je	.LBB2_34
# BB#39:                                # %vector.body.prol.preheader
	negl	%esi
	movdqa	.LCPI2_0(%rip), %xmm1   # xmm1 = [1,1,1,1]
	xorl	%ebp, %ebp
	movdqa	.LCPI2_1(%rip), %xmm2   # xmm2 = [10,10,10,10]
	.p2align	4, 0x90
.LBB2_40:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	pshufd	$245, %xmm0, %xmm3      # xmm3 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	pshufd	$245, %xmm1, %xmm3      # xmm3 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	addl	$8, %ebp
	incl	%esi
	jne	.LBB2_40
	jmp	.LBB2_41
.LBB2_34:
	xorl	%ebp, %ebp
	movdqa	.LCPI2_0(%rip), %xmm1   # xmm1 = [1,1,1,1]
.LBB2_41:                               # %vector.body.prol.loopexit
	cmpl	$56, %r9d
	jb	.LBB2_44
# BB#42:                                # %vector.ph.new
	movl	%ebx, %esi
	subl	%ebp, %esi
	movdqa	.LCPI2_2(%rip), %xmm2   # xmm2 = [100000000,100000000,100000000,100000000]
	.p2align	4, 0x90
.LBB2_43:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	pshufd	$245, %xmm0, %xmm3      # xmm3 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	pshufd	$245, %xmm1, %xmm3      # xmm3 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	addl	$-64, %esi
	jne	.LBB2_43
.LBB2_44:                               # %middle.block
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movd	%xmm0, %esi
	cmpl	%ebx, %edi
	je	.LBB2_48
# BB#45:
	subl	%r8d, %edx
.LBB2_46:                               # %.lr.ph110.preheader146
	incl	%edx
	.p2align	4, 0x90
.LBB2_47:                               # %.lr.ph110
                                        # =>This Inner Loop Header: Depth=1
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %esi
	decl	%edx
	testl	%edx, %edx
	jg	.LBB2_47
.LBB2_48:                               # %.preheader
	cmpl	%r10d, %ecx
	jg	.LBB2_51
# BB#49:                                # %.lr.ph.preheader
	movslq	%ecx, %rax
	movslq	%r14d, %rcx
	decq	%rax
	.p2align	4, 0x90
.LBB2_50:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, 4(%r15,%rax,4)
	incq	%rax
	xorl	%esi, %esi
	cmpq	%rcx, %rax
	jle	.LBB2_50
.LBB2_51:                               # %._crit_edge
	cmpl	$0, 8(%r15)
	jne	.LBB2_53
# BB#52:
	movq	$0, (%r15)
.LBB2_53:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	mp_sscanf, .Lfunc_end2-mp_sscanf
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4372995238176751616     # double 2.2204460492503131E-16
.LCPI3_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	mp_sqrt
	.p2align	4, 0x90
	.type	mp_sqrt,@function
mp_sqrt:                                # @mp_sqrt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi98:
	.cfi_def_cfa_offset 96
.Lcfi99:
	.cfi_offset %rbx, -56
.Lcfi100:
	.cfi_offset %r12, -48
.Lcfi101:
	.cfi_offset %r13, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r13
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movl	%edi, %ebx
	movl	(%r13), %eax
	testl	%eax, %eax
	js	.LBB3_1
# BB#2:
	je	.LBB3_3
# BB#5:
	movl	96(%rsp), %r15d
	movl	4(%rsp), %eax           # 4-byte Reload
	cvtsi2sdl	%eax, %xmm0
	movl	$1, %r12d
	movsd	.LCPI3_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI3_1(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	addl	%r12d, %r12d
	cmpl	%r15d, %r12d
	jge	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_6 Depth=1
	mulsd	%xmm0, %xmm0
	movapd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm2
	ja	.LBB3_6
.LBB3_8:                                # %mp_get_nfft_init.exit
	leal	2(%r12), %edi
	cmpl	%ebx, %edi
	cmovgl	%ebx, %edi
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	%r13, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	callq	mp_sqrt_init
	movl	$0, 20(%rsp)
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	leal	2(%r12), %r14d
	cmpl	%ebx, %r14d
	cmovgl	%ebx, %r14d
	movl	%r14d, %edi
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	%r13, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	leaq	20(%rsp), %rax
	pushq	%rax
.Lcfi105:
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
.Lcfi109:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi110:
	.cfi_adjust_cfa_offset 8
	callq	mp_sqrt_newton
	addq	$48, %rsp
.Lcfi111:
	.cfi_adjust_cfa_offset -48
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%ebp, %ecx
	imull	%r12d, %ecx
	cmpl	%r15d, %ecx
	jge	.LBB3_10
# BB#11:                                #   in Loop: Header=BB3_9 Depth=1
	leal	(%rax,%rax,2), %eax
	addl	$-2, %r14d
	cmpl	%r14d, %eax
	setl	%cl
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_10:                               #   in Loop: Header=BB3_9 Depth=1
	addl	%eax, %eax
	addl	$-2, %r14d
	cmpl	%r14d, %eax
	setle	%cl
	xorl	%ebp, %ebp
.LBB3_12:                               #   in Loop: Header=BB3_9 Depth=1
	sarl	%cl, %r12d
	addl	%r12d, %r12d
	cmpl	%r15d, %r12d
	jle	.LBB3_9
# BB#13:
	xorl	%ebp, %ebp
	jmp	.LBB3_14
.LBB3_1:
	movl	$-1, %ebp
	jmp	.LBB3_14
.LBB3_3:
	xorl	%ebp, %ebp
	cmpl	$-1, %ebx
	jl	.LBB3_14
# BB#4:                                 # %.lr.ph.preheader.i
	incl	%ebx
	leaq	4(,%rbx,4), %rdx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	memset
.LBB3_14:                               # %mp_load_0.exit
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	mp_sqrt, .Lfunc_end3-mp_sqrt
	.cfi_endproc

	.globl	mp_imul
	.p2align	4, 0x90
	.type	mp_imul,@function
mp_imul:                                # @mp_imul
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 32
.Lcfi115:
	.cfi_offset %rbx, -24
.Lcfi116:
	.cfi_offset %r14, -16
	movq	%r8, %r14
	testl	%ecx, %ecx
	jle	.LBB4_2
# BB#1:
	movl	(%rdx), %eax
	movl	%eax, (%r14)
	jmp	.LBB4_5
.LBB4_2:
	js	.LBB4_3
# BB#4:
	movl	$0, (%r14)
	xorl	%ecx, %ecx
	jmp	.LBB4_5
.LBB4_3:
	xorl	%eax, %eax
	subl	(%rdx), %eax
	movl	%eax, (%r14)
	negl	%ecx
.LBB4_5:
	cvtsi2sdl	%esi, %xmm0
	addq	$4, %rdx
	cvtsi2sdl	%ecx, %xmm1
	leaq	4(%r14), %rbx
	movq	%rdx, %rsi
	movq	%rbx, %rdx
	callq	mp_unsgn_imul
	cmpl	$0, (%r14)
	jne	.LBB4_7
# BB#6:
	movl	$0, (%rbx)
.LBB4_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	mp_imul, .Lfunc_end4-mp_imul
	.cfi_endproc

	.globl	mp_add
	.p2align	4, 0x90
	.type	mp_add,@function
mp_add:                                 # @mp_add
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi121:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 64
.Lcfi124:
	.cfi_offset %rbx, -56
.Lcfi125:
	.cfi_offset %r12, -48
.Lcfi126:
	.cfi_offset %r13, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, %rax
	movq	%rdx, %r14
	movl	(%r14), %r15d
	movl	4(%r14), %ecx
	movl	(%rax), %r8d
	movl	4(%rax), %ebx
	movl	%ecx, %edx
	subl	%ebx, %edx
	movl	%ecx, %r12d
	cmovsl	%ebx, %r12d
	movl	%r8d, %ebp
	imull	%r15d, %ebp
	testl	%ebp, %ebp
	js	.LBB5_7
# BB#1:
	je	.LBB5_2
# BB#3:
	testl	%edx, %edx
	jns	.LBB5_4
# BB#6:
	negl	%edx
	addq	$8, %rax
	addq	$8, %r14
	leaq	8(%r13), %r9
	movq	%rax, %rcx
	movq	%r14, %r8
	jmp	.LBB5_5
.LBB5_7:
	testl	%edi, %edi
	js	.LBB5_8
# BB#9:                                 # %.lr.ph.preheader.i
	subl	%ebx, %ecx
	testl	%edi, %edi
	je	.LBB5_14
# BB#10:                                # %.lr.ph.preheader.i
	testl	%ecx, %ecx
	jne	.LBB5_14
# BB#11:                                # %.lr.ph.i..lr.ph.i_crit_edge.preheader
	movslq	%edi, %rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB5_12:                               # %.lr.ph.i..lr.ph.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%r14,%rbp,4), %ecx
	subl	4(%rax,%rbp,4), %ecx
	cmpq	%rbx, %rbp
	jge	.LBB5_14
# BB#13:                                # %.lr.ph.i..lr.ph.i_crit_edge
                                        #   in Loop: Header=BB5_12 Depth=1
	incq	%rbp
	testl	%ecx, %ecx
	je	.LBB5_12
.LBB5_14:                               # %._crit_edge.i
	movq	%r13, %rbp
	movl	$1, %r13d
	testl	%ecx, %ecx
	jg	.LBB5_16
	jmp	.LBB5_15
.LBB5_2:                                # %.thread
	addl	%r8d, %r15d
	addl	%ecx, %ebx
	xorl	%edx, %edx
	movl	%ebx, %r12d
.LBB5_4:
	addq	$8, %r14
	addq	$8, %rax
	leaq	8(%r13), %r9
	movq	%r14, %rcx
	movq	%rax, %r8
.LBB5_5:
	callq	mp_unexp_add
	addl	%r12d, %eax
	jmp	.LBB5_17
.LBB5_8:
	movq	%r13, %rbp
	xorl	%ecx, %ecx
.LBB5_15:                               # %._crit_edge.thread.i
	negl	%ecx
	sbbl	%r13d, %r13d
.LBB5_16:                               # %mp_unsgn_cmp.exit
	movl	%edx, %ecx
	negl	%ecx
	testl	%r13d, %r13d
	movq	%r14, %r8
	cmovnsq	%rax, %r8
	cmovnsq	%r14, %rax
	cmovnsl	%edx, %ecx
	addq	$8, %rax
	addq	$8, %r8
	leaq	8(%rbp), %r9
	movl	%ecx, %edx
	movq	%rax, %rcx
	movl	%edi, %ebx
	callq	mp_unexp_sub
	subl	%eax, %r12d
	imull	(%r14), %r13d
	xorl	%r15d, %r15d
	cmpl	%ebx, %eax
	cmovnel	%r13d, %r15d
	movq	%rbp, %r13
	movl	%r12d, %eax
.LBB5_17:
	testl	%r15d, %r15d
	cmovel	%r15d, %eax
	movl	%r15d, (%r13)
	movl	%eax, 4(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	mp_add, .Lfunc_end5-mp_add
	.cfi_endproc

	.globl	mp_sub
	.p2align	4, 0x90
	.type	mp_sub,@function
mp_sub:                                 # @mp_sub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 64
.Lcfi137:
	.cfi_offset %rbx, -56
.Lcfi138:
	.cfi_offset %r12, -48
.Lcfi139:
	.cfi_offset %r13, -40
.Lcfi140:
	.cfi_offset %r14, -32
.Lcfi141:
	.cfi_offset %r15, -24
.Lcfi142:
	.cfi_offset %rbp, -16
	movq	%r8, %r9
	movq	%rcx, %rax
	movq	%rdx, %rbx
	movl	%edi, %r15d
	movl	(%rbx), %r14d
	movl	4(%rbx), %ecx
	movl	(%rax), %r8d
	movl	4(%rax), %edi
	movl	%ecx, %edx
	subl	%edi, %edx
	movl	%ecx, %r12d
	cmovsl	%edi, %r12d
	movl	%r8d, %ebp
	imull	%r14d, %ebp
	testl	%ebp, %ebp
	jle	.LBB6_1
# BB#7:
	testl	%r15d, %r15d
	js	.LBB6_8
# BB#9:                                 # %.lr.ph.preheader.i
	subl	%edi, %ecx
	testl	%r15d, %r15d
	je	.LBB6_14
# BB#10:                                # %.lr.ph.preheader.i
	testl	%ecx, %ecx
	jne	.LBB6_14
# BB#11:                                # %.lr.ph.i..lr.ph.i_crit_edge.preheader
	movslq	%r15d, %rdi
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB6_12:                               # %.lr.ph.i..lr.ph.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rbx,%rbp,4), %ecx
	subl	4(%rax,%rbp,4), %ecx
	cmpq	%rdi, %rbp
	jge	.LBB6_14
# BB#13:                                # %.lr.ph.i..lr.ph.i_crit_edge
                                        #   in Loop: Header=BB6_12 Depth=1
	incq	%rbp
	testl	%ecx, %ecx
	je	.LBB6_12
.LBB6_14:                               # %._crit_edge.i
	movl	$1, %r13d
	testl	%ecx, %ecx
	jg	.LBB6_16
	jmp	.LBB6_15
.LBB6_1:
	js	.LBB6_3
# BB#2:                                 # %.thread
	subl	%r8d, %r14d
	addl	%ecx, %edi
	xorl	%edx, %edx
	movl	%edi, %r12d
	jmp	.LBB6_4
.LBB6_8:
	xorl	%ecx, %ecx
.LBB6_15:                               # %._crit_edge.thread.i
	negl	%ecx
	sbbl	%r13d, %r13d
.LBB6_16:                               # %mp_unsgn_cmp.exit
	movl	%edx, %ecx
	negl	%ecx
	testl	%r13d, %r13d
	movq	%rbx, %r8
	cmovnsq	%rax, %r8
	cmovnsq	%rbx, %rax
	cmovnsl	%edx, %ecx
	addq	$8, %rax
	addq	$8, %r8
	movq	%r9, %rbp
	leaq	8(%rbp), %r9
	movl	%r15d, %edi
	movl	%ecx, %edx
	movq	%rax, %rcx
	callq	mp_unexp_sub
	movq	%rbp, %rcx
	subl	%eax, %r12d
	imull	(%rbx), %r13d
	xorl	%r14d, %r14d
	cmpl	%r15d, %eax
	cmovnel	%r13d, %r14d
	movl	%r12d, %eax
	jmp	.LBB6_17
.LBB6_3:
	testl	%edx, %edx
	js	.LBB6_6
.LBB6_4:
	addq	$8, %rbx
	addq	$8, %rax
	movq	%r9, %rbp
	leaq	8(%rbp), %r9
	movl	%r15d, %edi
	movq	%rbx, %rcx
	movq	%rax, %r8
.LBB6_5:
	callq	mp_unexp_add
	movq	%rbp, %rcx
	addl	%r12d, %eax
.LBB6_17:
	testl	%r14d, %r14d
	cmovel	%r14d, %eax
	movl	%r14d, (%rcx)
	movl	%eax, 4(%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_6:
	negl	%edx
	addq	$8, %rax
	addq	$8, %rbx
	movq	%r9, %rbp
	leaq	8(%rbp), %r9
	movl	%r15d, %edi
	movq	%rax, %rcx
	movq	%rbx, %r8
	jmp	.LBB6_5
.Lfunc_end6:
	.size	mp_sub, .Lfunc_end6-mp_sub
	.cfi_endproc

	.globl	mp_idiv_2
	.p2align	4, 0x90
	.type	mp_idiv_2,@function
mp_idiv_2:                              # @mp_idiv_2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 24
.Lcfi145:
	.cfi_offset %rbx, -24
.Lcfi146:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	(%rdx), %eax
	movl	%eax, (%rcx)
	movl	4(%rdx), %r8d
	movl	8(%rdx), %r9d
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	cmpl	$1, %r9d
	sete	%al
	movl	$-1, %ebp
	cmovnel	%r10d, %ebp
	subl	%eax, %r8d
	movl	%r8d, 4(%rcx)
	leal	1(%rdi), %r8d
	movl	%r8d, %ebx
	subl	%eax, %ebx
	cmpl	$2, %ebx
	jl	.LBB7_7
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r10d, %r10d
	cmpl	$1, %r9d
	sete	%r11b
	addl	$2, %edi
	subl	%eax, %edi
	testb	$1, %dil
	jne	.LBB7_3
# BB#2:
	movl	$2, %ebx
	cmpl	$3, %edi
	jne	.LBB7_5
	jmp	.LBB7_7
.LBB7_3:                                # %.lr.ph.prol
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	cmpl	$1, %r9d
	sete	%al
	cmovel	%esi, %ebx
	addl	8(%rdx,%rax,4), %ebx
	movl	%ebx, %ebp
	andl	$1, %ebp
	negl	%ebp
	sarl	%ebx
	movl	%ebx, 8(%rcx)
	movl	$3, %ebx
	cmpl	$3, %edi
	je	.LBB7_7
.LBB7_5:                                # %.lr.ph.preheader.new
	movb	%r11b, %r10b
	subq	%rbx, %rdi
	leaq	4(%rcx,%rbx,4), %r11
	addq	%rbx, %r10
	leaq	4(%rdx,%r10,4), %rdx
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	andl	%esi, %ebp
	addl	-4(%rdx), %ebp
	movl	%ebp, %eax
	andl	$1, %eax
	negl	%eax
	sarl	%ebp
	movl	%ebp, -4(%r11)
	andl	%esi, %eax
	addl	(%rdx), %eax
	movl	%eax, %ebp
	andl	$1, %ebp
	negl	%ebp
	sarl	%eax
	movl	%eax, (%r11)
	addq	$8, %r11
	addq	$8, %rdx
	addq	$-2, %rdi
	jne	.LBB7_6
.LBB7_7:                                # %._crit_edge
	cmpl	$1, %r9d
	jne	.LBB7_9
# BB#8:
	andl	%esi, %ebp
	sarl	%ebp
	movslq	%r8d, %rax
	movl	%ebp, (%rcx,%rax,4)
.LBB7_9:
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end7:
	.size	mp_idiv_2, .Lfunc_end7-mp_idiv_2
	.cfi_endproc

	.globl	mp_mul
	.p2align	4, 0x90
	.type	mp_mul,@function
mp_mul:                                 # @mp_mul
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi150:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi151:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi153:
	.cfi_def_cfa_offset 192
.Lcfi154:
	.cfi_offset %rbx, -56
.Lcfi155:
	.cfi_offset %r12, -48
.Lcfi156:
	.cfi_offset %r13, -40
.Lcfi157:
	.cfi_offset %r14, -32
.Lcfi158:
	.cfi_offset %r15, -24
.Lcfi159:
	.cfi_offset %rbp, -16
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%edi, %r9d
	movq	232(%rsp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	224(%rsp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	216(%rsp), %r13
	movq	208(%rsp), %r15
	movq	200(%rsp), %r14
	movl	192(%rsp), %ebp
	movl	%ebp, %esi
	sarl	%esi
	movslq	%esi, %rdi
	movslq	%r9d, %rax
	incq	%rdi
	movl	%eax, %edx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %r12
	cmpq	%rax, %r12
	jge	.LBB8_3
# BB#2:                                 #   in Loop: Header=BB8_1 Depth=1
	movl	8(%rbx,%r12,4), %edx
	leaq	1(%r12), %rdi
	addl	8(%rcx,%r12,4), %edx
	je	.LBB8_1
.LBB8_3:
	movl	%r9d, %eax
	shrl	$31, %eax
	addl	%r9d, %eax
	sarl	%eax
	incl	%eax
	movl	%r9d, %edx
	subl	%r12d, %edx
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	testl	%r9d, %r9d
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%r8, 128(%rsp)          # 8-byte Spill
	movl	%edx, 84(%rsp)          # 4-byte Spill
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	jle	.LBB8_4
# BB#5:
	movl	8(%rbx), %r10d
	leal	1(%rsi), %edx
	cmpl	%r9d, %esi
	cmovgel	%r9d, %edx
	jmp	.LBB8_6
.LBB8_4:
	xorl	%edx, %edx
	xorl	%r10d, %r10d
.LBB8_6:
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	(%rbx), %eax
	imull	%r10d, %eax
	cvtsi2sdl	%eax, %xmm0
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	%eax, %rbp
	movsd	%xmm0, 8(%r14,%rbp,8)
	cmpl	%eax, %edx
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	jge	.LBB8_8
# BB#7:                                 # %.lr.ph61.preheader.i
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	subl	%edx, %eax
	movq	%rbp, %rcx
	subq	%rax, %rcx
	leaq	(%r14,%rcx,8), %rdi
	movl	%edx, 48(%rsp)          # 4-byte Spill
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movl	%r9d, %ebx
	movl	%r10d, 16(%rsp)         # 4-byte Spill
	callq	memset
	movl	48(%rsp), %edx          # 4-byte Reload
	movl	16(%rsp), %r10d         # 4-byte Reload
	movl	%ebx, %r9d
	movq	88(%rsp), %rbx          # 8-byte Reload
.LBB8_8:                                # %._crit_edge62.i
	cmpl	$1, %edx
	movl	%r9d, 28(%rsp)          # 4-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jle	.LBB8_13
# BB#9:
	xorl	%ecx, %ecx
	cmpl	$2, %edx
	movl	12(%rsp), %esi          # 4-byte Reload
	je	.LBB8_12
# BB#10:                                # %.lr.ph.preheader.i
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%esi, %eax
	sarl	%eax
	incl	%edx
	movslq	%edx, %rdx
	xorl	%ecx, %ecx
	movl	$-1, %r8d
	.p2align	4, 0x90
.LBB8_11:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rdx,4), %edi
	subl	%ecx, %edi
	cmpl	%eax, %edi
	movl	$0, %ecx
	cmovgel	%r8d, %ecx
	movl	$0, %ebp
	cmovgel	%esi, %ebp
	subl	%ebp, %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	movsd	%xmm0, -8(%r14,%rdx,8)
	decq	%rdx
	cmpq	$3, %rdx
	jg	.LBB8_11
.LBB8_12:                               # %._crit_edge.i
	movl	12(%rbx), %eax
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 16(%r14)
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB8_13:                               # %mp_mul_i2d.exit
	leaq	1(%rbp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r10d, %xmm0
	leaq	8(%r14), %rdx
	movsd	%xmm0, 8(%r14)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	4(%rbx), %xmm0
	movsd	%xmm0, (%r14)
	movl	$1, %esi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edi
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movl	28(%rsp), %edi          # 4-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%ebx, %edx
	movl	%r12d, %ecx
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	%r13, %r9
	callq	mp_mul_i2d
	leaq	8(%r13), %rdx
	movl	$1, %esi
	movl	%ebx, %edi
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movl	28(%rsp), %r9d          # 4-byte Reload
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	addsd	(%r13), %xmm0
	movsd	%xmm0, (%r13)
	movsd	8(%r14), %xmm0          # xmm0 = mem[0],zero
	mulsd	8(%r13), %xmm0
	movsd	%xmm0, 8(%r13)
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	mulsd	16(%r13), %xmm0
	movsd	%xmm0, 16(%r13)
	cmpl	$4, %ebx
	jl	.LBB8_25
# BB#14:                                # %.lr.ph.preheader.i88
	leaq	-4(%rbp), %rdx
	movq	%rdx, %rax
	shrq	%rax
	leaq	1(%rax), %rcx
	cmpq	$2, %rcx
	jae	.LBB8_16
# BB#15:
	movl	$3, %eax
	jmp	.LBB8_24
.LBB8_16:                               # %min.iters.checked
	movl	%ecx, %r8d
	andl	$1, %r8d
	subq	%r8, %rcx
	je	.LBB8_17
# BB#18:                                # %vector.memcheck
	leaq	24(%r13), %rsi
	movabsq	$2305843009213693950, %rdi # imm = 0x1FFFFFFFFFFFFFFE
	andq	%rdi, %rdx
	leaq	24(%r14), %rdi
	leaq	40(%r14,%rdx,8), %rbp
	cmpq	%rbp, %rsi
	jae	.LBB8_21
# BB#19:                                # %vector.memcheck
	leaq	40(%r13,%rdx,8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB8_21
# BB#20:
	movl	$3, %eax
	movl	28(%rsp), %r9d          # 4-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB8_24
.LBB8_17:
	movl	$3, %eax
	jmp	.LBB8_24
.LBB8_21:                               # %vector.body.preheader
	leaq	5(%rax,%rax), %rax
	leaq	(%r8,%r8), %rdx
	subq	%rdx, %rax
	movl	28(%rsp), %r9d          # 4-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_22:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rdi), %xmm0
	movupd	16(%rdi), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	movupd	(%rsi), %xmm4
	movupd	16(%rsi), %xmm5
	movapd	%xmm4, %xmm6
	unpcklpd	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0]
	mulpd	%xmm4, %xmm0
	unpckhpd	%xmm5, %xmm4    # xmm4 = xmm4[1],xmm5[1]
	mulpd	%xmm5, %xmm1
	movapd	%xmm0, %xmm5
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	movhlps	%xmm0, %xmm1            # xmm1 = xmm0[1],xmm1[1]
	subpd	%xmm1, %xmm5
	mulpd	%xmm2, %xmm4
	mulpd	%xmm3, %xmm6
	addpd	%xmm4, %xmm6
	movapd	%xmm5, %xmm0
	unpcklpd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0]
	movhlps	%xmm5, %xmm6            # xmm6 = xmm5[1],xmm6[1]
	movups	%xmm6, 16(%rsi)
	movupd	%xmm0, (%rsi)
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-2, %rcx
	jne	.LBB8_22
# BB#23:                                # %middle.block
	testq	%r8, %r8
	je	.LBB8_25
	.p2align	4, 0x90
.LBB8_24:                               # %.lr.ph.i91
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%r14,%rax,8), %xmm0
	movsd	(%r13,%rax,8), %xmm1    # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movsd	8(%r13,%rax,8), %xmm2   # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	subpd	%xmm2, %xmm0
	addpd	%xmm2, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	%xmm1, (%r13,%rax,8)
	addq	$2, %rax
	cmpq	%rbp, %rax
	jl	.LBB8_24
.LBB8_25:                               # %mp_mul_cmul.exit
	movq	48(%rsp), %rdi          # 8-byte Reload
	movsd	(%r14,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r13,%rdi,8), %xmm0
	movsd	%xmm0, (%r13,%rdi,8)
	testl	%r9d, %r9d
	jle	.LBB8_26
# BB#27:
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	8(%rbx), %r10d
	movq	112(%rsp), %rcx         # 8-byte Reload
	leal	1(%rcx), %eax
	cmpl	%r9d, %ecx
	movq	72(%rsp), %rdx          # 8-byte Reload
	cmovll	%eax, %edx
	movl	12(%rsp), %esi          # 4-byte Reload
	jmp	.LBB8_28
.LBB8_26:
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
.LBB8_28:
	movl	(%rbx), %eax
	imull	%r10d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%r15,%rdi,8)
	cmpl	32(%rsp), %edx          # 4-byte Folded Reload
	jge	.LBB8_30
# BB#29:                                # %.lr.ph61.preheader.i98
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	subl	%edx, %eax
	movq	%rbp, %rcx
	subq	%rax, %rcx
	leaq	(%r15,%rcx,8), %rdi
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movl	%r10d, %ebx
	callq	memset
	movl	%ebx, %r10d
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
.LBB8_30:                               # %._crit_edge62.i99
	cmpl	$1, %edx
	jle	.LBB8_35
# BB#31:
	xorl	%ecx, %ecx
	cmpl	$2, %edx
	je	.LBB8_34
# BB#32:                                # %.lr.ph.preheader.i100
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%esi, %eax
	sarl	%eax
	incl	%edx
	movslq	%edx, %rdx
	xorl	%ecx, %ecx
	movl	$-1, %r8d
	.p2align	4, 0x90
.LBB8_33:                               # %.lr.ph.i104
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rdx,4), %edi
	subl	%ecx, %edi
	cmpl	%eax, %edi
	movl	$0, %ecx
	cmovgel	%r8d, %ecx
	movl	$0, %ebp
	cmovgel	%esi, %ebp
	subl	%ebp, %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	movsd	%xmm0, -8(%r15,%rdx,8)
	decq	%rdx
	cmpq	$3, %rdx
	jg	.LBB8_33
.LBB8_34:                               # %._crit_edge.i106
	movl	12(%rbx), %eax
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 16(%r15)
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB8_35:                               # %mp_mul_i2d.exit107
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r10d, %xmm0
	leaq	8(%r15), %rdx
	movsd	%xmm0, 8(%r15)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	4(%rbx), %xmm0
	movsd	%xmm0, (%r15)
	movl	$1, %esi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	addsd	(%r14), %xmm0
	movsd	%xmm0, (%r14)
	movsd	8(%r15), %xmm0          # xmm0 = mem[0],zero
	mulsd	8(%r14), %xmm0
	movsd	%xmm0, 8(%r14)
	movsd	16(%r15), %xmm0         # xmm0 = mem[0],zero
	mulsd	16(%r14), %xmm0
	movsd	%xmm0, 16(%r14)
	cmpl	$4, %ebx
	movq	48(%rsp), %rbx          # 8-byte Reload
	jl	.LBB8_47
# BB#36:                                # %.lr.ph.preheader.i113
	leaq	-4(%rbp), %rdx
	movq	%rdx, %rax
	shrq	%rax
	leaq	1(%rax), %rcx
	cmpq	$2, %rcx
	jae	.LBB8_38
# BB#37:
	movl	$3, %eax
	jmp	.LBB8_46
.LBB8_38:                               # %min.iters.checked141
	movl	%ecx, %r8d
	andl	$1, %r8d
	subq	%r8, %rcx
	je	.LBB8_39
# BB#40:                                # %vector.memcheck158
	leaq	24(%r14), %rsi
	movabsq	$2305843009213693950, %rdi # imm = 0x1FFFFFFFFFFFFFFE
	andq	%rdi, %rdx
	leaq	24(%r15), %rdi
	leaq	40(%r15,%rdx,8), %rbp
	cmpq	%rbp, %rsi
	jae	.LBB8_43
# BB#41:                                # %vector.memcheck158
	leaq	40(%r14,%rdx,8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB8_43
# BB#42:
	movl	$3, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB8_46
.LBB8_39:
	movl	$3, %eax
	jmp	.LBB8_46
.LBB8_43:                               # %vector.body137.preheader
	leaq	5(%rax,%rax), %rax
	leaq	(%r8,%r8), %rdx
	subq	%rdx, %rax
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_44:                               # %vector.body137
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rdi), %xmm0
	movupd	16(%rdi), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	movupd	(%rsi), %xmm4
	movupd	16(%rsi), %xmm5
	movapd	%xmm4, %xmm6
	unpcklpd	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0]
	mulpd	%xmm4, %xmm0
	unpckhpd	%xmm5, %xmm4    # xmm4 = xmm4[1],xmm5[1]
	mulpd	%xmm5, %xmm1
	movapd	%xmm0, %xmm5
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	movhlps	%xmm0, %xmm1            # xmm1 = xmm0[1],xmm1[1]
	subpd	%xmm1, %xmm5
	mulpd	%xmm2, %xmm4
	mulpd	%xmm3, %xmm6
	addpd	%xmm4, %xmm6
	movapd	%xmm5, %xmm0
	unpcklpd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0]
	movhlps	%xmm5, %xmm6            # xmm6 = xmm5[1],xmm6[1]
	movups	%xmm6, 16(%rsi)
	movupd	%xmm0, (%rsi)
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-2, %rcx
	jne	.LBB8_44
# BB#45:                                # %middle.block138
	testq	%r8, %r8
	je	.LBB8_47
	.p2align	4, 0x90
.LBB8_46:                               # %.lr.ph.i116
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%r15,%rax,8), %xmm0
	movsd	(%r14,%rax,8), %xmm1    # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movsd	8(%r14,%rax,8), %xmm2   # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	subpd	%xmm2, %xmm0
	addpd	%xmm2, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	%xmm1, (%r14,%rax,8)
	addq	$2, %rax
	cmpq	%rbp, %rax
	jl	.LBB8_46
.LBB8_47:                               # %mp_mul_cmul.exit118
	movsd	(%r15,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r14,%rbx,8), %xmm0
	movsd	%xmm0, (%r14,%rbx,8)
	movl	$-1, %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movl	28(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %edi
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r14, %rcx
	movq	104(%rsp), %r8          # 8-byte Reload
	callq	mp_mul_d2i
	movl	%ebx, %edi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%ebp, %esi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%ebx, %edx
	movl	%r12d, %ecx
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	%r14, %r9
	callq	mp_mul_i2d
	movl	$1, %esi
	movl	%ebx, %edi
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movsd	8(%r14), %xmm0          # xmm0 = mem[0],zero
	mulsd	8(%r15), %xmm0
	addsd	8(%r13), %xmm0
	movsd	%xmm0, 8(%r13)
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	mulsd	16(%r15), %xmm0
	addsd	16(%r13), %xmm0
	movsd	%xmm0, 16(%r13)
	cmpl	$4, %ebx
	jl	.LBB8_59
# BB#48:                                # %.lr.ph.preheader.i108
	leaq	-4(%rbp), %rax
	movq	%rax, %r9
	shrq	%r9
	leaq	1(%r9), %rcx
	cmpq	$2, %rcx
	jae	.LBB8_50
# BB#49:
	movl	$3, %eax
	jmp	.LBB8_58
.LBB8_50:                               # %min.iters.checked180
	movl	%ecx, %r8d
	andl	$1, %r8d
	subq	%r8, %rcx
	je	.LBB8_51
# BB#52:                                # %vector.memcheck204
	leaq	24(%r13), %rsi
	movabsq	$2305843009213693950, %rdx # imm = 0x1FFFFFFFFFFFFFFE
	andq	%rdx, %rax
	leaq	40(%r13,%rax,8), %rbp
	leaq	24(%r14), %rdi
	leaq	40(%r14,%rax,8), %r10
	leaq	24(%r15), %rdx
	leaq	40(%r15,%rax,8), %r11
	cmpq	%r10, %rsi
	sbbb	%al, %al
	cmpq	%rbp, %rdi
	sbbb	%r12b, %r12b
	andb	%al, %r12b
	cmpq	%r11, %rsi
	sbbb	%r10b, %r10b
	cmpq	%rbp, %rdx
	sbbb	%r11b, %r11b
	movl	$3, %eax
	testb	$1, %r12b
	jne	.LBB8_53
# BB#54:                                # %vector.memcheck204
	andb	%r11b, %r10b
	andb	$1, %r10b
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB8_58
# BB#55:                                # %vector.body176.preheader
	leaq	5(%r9,%r9), %rax
	leaq	(%r8,%r8), %rbp
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB8_56:                               # %vector.body176
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rdi), %xmm1
	movupd	16(%rdi), %xmm2
	movapd	%xmm1, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movapd	%xmm1, %xmm4
	unpckhpd	%xmm2, %xmm4    # xmm4 = xmm4[1],xmm2[1]
	movupd	(%rdx), %xmm5
	movupd	16(%rdx), %xmm6
	movapd	%xmm5, %xmm0
	unpcklpd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0]
	mulpd	%xmm5, %xmm1
	unpckhpd	%xmm6, %xmm5    # xmm5 = xmm5[1],xmm6[1]
	mulpd	%xmm6, %xmm2
	movapd	%xmm1, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	movhlps	%xmm1, %xmm2            # xmm2 = xmm1[1],xmm2[1]
	subpd	%xmm2, %xmm6
	movupd	(%rsi), %xmm1
	movupd	16(%rsi), %xmm2
	movapd	%xmm1, %xmm7
	unpcklpd	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0]
	unpckhpd	%xmm2, %xmm1    # xmm1 = xmm1[1],xmm2[1]
	addpd	%xmm6, %xmm7
	mulpd	%xmm3, %xmm5
	mulpd	%xmm4, %xmm0
	addpd	%xmm5, %xmm0
	addpd	%xmm1, %xmm0
	movapd	%xmm7, %xmm1
	unpcklpd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movhlps	%xmm7, %xmm0            # xmm0 = xmm7[1],xmm0[1]
	movups	%xmm0, 16(%rsi)
	movupd	%xmm1, (%rsi)
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$32, %rdx
	addq	$-2, %rcx
	jne	.LBB8_56
# BB#57:                                # %middle.block177
	testq	%r8, %r8
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB8_58
	jmp	.LBB8_59
.LBB8_51:
	movl	$3, %eax
	jmp	.LBB8_58
.LBB8_53:
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_58:                               # %.lr.ph.i111
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%r14,%rax,8), %xmm0
	movsd	(%r15,%rax,8), %xmm1    # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movsd	8(%r15,%rax,8), %xmm2   # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	subpd	%xmm2, %xmm0
	addpd	%xmm2, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	(%r13,%rax,8), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%r13,%rax,8)
	addq	$2, %rax
	cmpq	%rbp, %rax
	jl	.LBB8_58
.LBB8_59:                               # %mp_mul_cmuladd.exit
	movq	48(%rsp), %rax          # 8-byte Reload
	movsd	(%r14,%rax,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r15,%rax,8), %xmm0
	addsd	(%r13,%rax,8), %xmm0
	movsd	%xmm0, (%r13,%rax,8)
	movl	$-1, %esi
	movl	%ebx, %edi
	movq	120(%rsp), %rdx         # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movl	84(%rsp), %edi          # 4-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	movl	%ebx, %edx
	movq	%r13, %rcx
	movq	128(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %r8
	callq	mp_mul_d2i
	movl	28(%rsp), %edi          # 4-byte Reload
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%rbx, %r8
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	mp_add                  # TAILCALL
.Lfunc_end8:
	.size	mp_mul, .Lfunc_end8-mp_mul
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_0:
	.quad	4611686018427387904     # double 2
	.text
	.globl	mp_squh
	.p2align	4, 0x90
	.type	mp_squh,@function
mp_squh:                                # @mp_squh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi166:
	.cfi_def_cfa_offset 112
.Lcfi167:
	.cfi_offset %rbx, -56
.Lcfi168:
	.cfi_offset %r12, -48
.Lcfi169:
	.cfi_offset %r13, -40
.Lcfi170:
	.cfi_offset %r14, -32
.Lcfi171:
	.cfi_offset %r15, -24
.Lcfi172:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rdx, %r14
	movl	%esi, %r15d
	movq	120(%rsp), %rbp
	movq	112(%rsp), %rax
	testl	%edi, %edi
	movl	%edi, 20(%rsp)          # 4-byte Spill
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jle	.LBB9_1
# BB#2:
	movl	8(%r14), %r11d
	movl	%r8d, %eax
	sarl	%eax
	leal	1(%rax), %r13d
	cmpl	%edi, %eax
	cmovgel	%edi, %r13d
	jmp	.LBB9_3
.LBB9_1:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
.LBB9_3:
	movl	(%r14), %eax
	imull	%r11d, %eax
	cvtsi2sdl	%eax, %xmm0
	movslq	%r8d, %r12
	movsd	%xmm0, 8(%rbx,%r12,8)
	cmpl	%r8d, %r13d
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	jge	.LBB9_5
# BB#4:                                 # %.lr.ph61.preheader.i
	leal	-1(%r8), %eax
	subl	%r13d, %eax
	movq	%r12, %rcx
	subq	%rax, %rcx
	leaq	(%rbx,%rcx,8), %rdi
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movl	%r11d, 8(%rsp)          # 4-byte Spill
	callq	memset
	movl	8(%rsp), %r11d          # 4-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
.LBB9_5:                                # %._crit_edge62.i
	cmpl	$1, %r13d
	jle	.LBB9_10
# BB#6:
	movq	%rbp, %r10
	xorl	%ecx, %ecx
	cmpl	$2, %r13d
	je	.LBB9_9
# BB#7:                                 # %.lr.ph.preheader.i
	movl	%r15d, %eax
	shrl	$31, %eax
	addl	%r15d, %eax
	sarl	%eax
	incl	%r13d
	movslq	%r13d, %rdx
	xorl	%ecx, %ecx
	movl	$-1, %esi
	.p2align	4, 0x90
.LBB9_8:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rdx,4), %edi
	subl	%ecx, %edi
	cmpl	%eax, %edi
	movl	$0, %ecx
	cmovgel	%esi, %ecx
	movl	$0, %ebp
	cmovgel	%r15d, %ebp
	subl	%ebp, %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	movsd	%xmm0, -8(%rbx,%rdx,8)
	decq	%rdx
	cmpq	$3, %rdx
	jg	.LBB9_8
.LBB9_9:                                # %._crit_edge.i
	movl	12(%r14), %eax
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 16(%rbx)
	movq	%r10, %rbp
.LBB9_10:                               # %mp_mul_i2d.exit
	leaq	1(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r11d, %xmm0
	leaq	8(%rbx), %rdx
	movsd	%xmm0, 8(%rbx)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	4(%r14), %xmm0
	movsd	%xmm0, (%rbx)
	movl	$1, %esi
	movl	%r8d, %edi
	movq	%rdx, %r14
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rcx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %r8
	callq	rdft
	movq	24(%rsp), %rbp          # 8-byte Reload
	movsd	.LCPI9_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movhpd	8(%rbx), %xmm0          # xmm0 = xmm0[0],mem[0]
	movupd	(%rbx), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	%xmm1, (%rbx)
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rbx)
	cmpl	$4, %ebp
	jl	.LBB9_19
# BB#11:                                # %.lr.ph.preheader.i19
	leaq	-4(%r12), %rax
	shrq	%rax
	leaq	1(%rax), %rcx
	cmpq	$2, %rcx
	jae	.LBB9_13
# BB#12:
	movl	$3, %eax
	jmp	.LBB9_18
.LBB9_13:                               # %min.iters.checked
	movl	%ecx, %edx
	andl	$1, %edx
	subq	%rdx, %rcx
	je	.LBB9_14
# BB#15:                                # %vector.body.preheader
	leaq	5(%rax,%rax), %rax
	leaq	(%rdx,%rdx), %rsi
	subq	%rsi, %rax
	leaq	24(%rbx), %rsi
	.p2align	4, 0x90
.LBB9_16:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rsi), %xmm0
	movupd	16(%rsi), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	mulpd	%xmm1, %xmm1
	mulpd	%xmm0, %xmm0
	movapd	%xmm0, %xmm4
	unpcklpd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0]
	unpckhpd	%xmm1, %xmm0    # xmm0 = xmm0[1],xmm1[1]
	subpd	%xmm0, %xmm4
	addpd	%xmm2, %xmm2
	mulpd	%xmm3, %xmm2
	movapd	%xmm4, %xmm0
	unpcklpd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0]
	movhlps	%xmm4, %xmm2            # xmm2 = xmm4[1],xmm2[1]
	movups	%xmm2, 16(%rsi)
	movupd	%xmm0, (%rsi)
	addq	$32, %rsi
	addq	$-2, %rcx
	jne	.LBB9_16
# BB#17:                                # %middle.block
	testq	%rdx, %rdx
	jne	.LBB9_18
	jmp	.LBB9_19
.LBB9_14:
	movl	$3, %eax
	.p2align	4, 0x90
.LBB9_18:                               # %.lr.ph.i22
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rbx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rbx,%rax,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx,%rax,8)
	movsd	%xmm0, 8(%rbx,%rax,8)
	addq	$2, %rax
	cmpq	%r12, %rax
	jl	.LBB9_18
.LBB9_19:                               # %mp_mul_csqu.exit
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	(%rbx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	movl	$-1, %esi
	movl	%ebp, %edi
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	callq	rdft
	movl	20(%rsp), %edi          # 4-byte Reload
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	mp_mul_d2i              # TAILCALL
.Lfunc_end9:
	.size	mp_squh, .Lfunc_end9-mp_squh
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	4372995238176751616     # double 2.2204460492503131E-16
.LCPI10_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	mp_inv
	.p2align	4, 0x90
	.type	mp_inv,@function
mp_inv:                                 # @mp_inv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi176:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi177:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi179:
	.cfi_def_cfa_offset 96
.Lcfi180:
	.cfi_offset %rbx, -56
.Lcfi181:
	.cfi_offset %r12, -48
.Lcfi182:
	.cfi_offset %r13, -40
.Lcfi183:
	.cfi_offset %r14, -32
.Lcfi184:
	.cfi_offset %r15, -24
.Lcfi185:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r9
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%edi, 20(%rsp)          # 4-byte Spill
	movl	(%r9), %ecx
	testl	%ecx, %ecx
	je	.LBB10_1
# BB#2:
	movl	96(%rsp), %ebx
	cvtsi2sdl	%esi, %xmm0
	movl	$1, %r12d
	movsd	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI10_1(%rip), %xmm2  # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm3
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	addl	%r12d, %r12d
	cmpl	%ebx, %r12d
	jge	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=1
	mulsd	%xmm3, %xmm3
	movapd	%xmm3, %xmm4
	mulsd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm2
	ja	.LBB10_3
.LBB10_5:                               # %mp_get_nfft_init.exit
	leal	2(%r12), %eax
	movl	20(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %eax
	cmovgl	%edx, %eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%ecx, (%rdx)
	xorl	%ecx, %ecx
	subl	4(%r9), %ecx
	testl	%eax, %eax
	jle	.LBB10_6
# BB#7:                                 # %.lr.ph.preheader.i.i
	movapd	%xmm2, %xmm1
	divsd	%xmm0, %xmm1
	movslq	%eax, %rdx
	incq	%rdx
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB10_8:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm1, %xmm3
	xorps	%xmm4, %xmm4
	cvtsi2sdl	(%r9,%rdx,4), %xmm4
	addsd	%xmm4, %xmm3
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB10_8
	jmp	.LBB10_9
.LBB10_1:
	movl	$-1, %eax
	jmp	.LBB10_31
.LBB10_6:
	xorpd	%xmm3, %xmm3
.LBB10_9:                               # %mp_unexp_mp2d.exit.i
	movapd	%xmm2, %xmm1
	divsd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm2
	jbe	.LBB10_12
	.p2align	4, 0x90
.LBB10_10:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm0, %xmm1
	decl	%ecx
	ucomisd	%xmm1, %xmm2
	ja	.LBB10_10
.LBB10_12:                              # %._crit_edge.i
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%ecx, 4(%rdx)
	testl	%eax, %eax
	jle	.LBB10_25
# BB#13:                                # %.lr.ph.i19.i
	leal	-1(%rsi), %ecx
	movl	%eax, %edx
	testb	$1, %dl
	jne	.LBB10_15
# BB#14:
	xorl	%edi, %edi
	cmpl	$1, %eax
	jne	.LBB10_19
	jmp	.LBB10_25
.LBB10_15:
	cvttsd2si	%xmm1, %r8d
	cmpl	%esi, %r8d
	jl	.LBB10_17
# BB#16:
	movapd	%xmm0, %xmm1
.LBB10_17:
	cmovgel	%ecx, %r8d
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%r8d, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r8d, 8(%rdi)
	movl	$1, %edi
	cmpl	$1, %eax
	je	.LBB10_25
.LBB10_19:                              # %.lr.ph.i19.i.new
	subq	%rdi, %rdx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	12(%rax,%rdi,4), %rax
	.p2align	4, 0x90
.LBB10_20:                              # =>This Inner Loop Header: Depth=1
	cvttsd2si	%xmm1, %edi
	cmpl	%esi, %edi
	jl	.LBB10_22
# BB#21:                                #   in Loop: Header=BB10_20 Depth=1
	movapd	%xmm0, %xmm1
.LBB10_22:                              #   in Loop: Header=BB10_20 Depth=1
	cmovgel	%ecx, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movl	%edi, -4(%rax)
	cvttsd2si	%xmm1, %edi
	cmpl	%esi, %edi
	jl	.LBB10_24
# BB#23:                                #   in Loop: Header=BB10_20 Depth=1
	movapd	%xmm0, %xmm1
.LBB10_24:                              #   in Loop: Header=BB10_20 Depth=1
	cmovgel	%ecx, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movl	%edi, (%rax)
	addq	$8, %rax
	addq	$-2, %rdx
	jne	.LBB10_20
.LBB10_25:                              # %mp_inv_init.exit.preheader
	movl	$8, %r15d
	.p2align	4, 0x90
.LBB10_26:                              # %mp_inv_init.exit
                                        # =>This Inner Loop Header: Depth=1
	leal	2(%r12), %r14d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %r14d
	cmovgl	%eax, %r14d
	subq	$8, %rsp
.Lcfi186:
	.cfi_adjust_cfa_offset 8
	movl	%r14d, %edi
	movq	%rsi, %r13
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r9, %rbp
	movq	%r9, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	pushq	136(%rsp)
.Lcfi187:
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
.Lcfi188:
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
.Lcfi189:
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
.Lcfi190:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi191:
	.cfi_adjust_cfa_offset 8
	callq	mp_inv_newton
	addq	$48, %rsp
.Lcfi192:
	.cfi_adjust_cfa_offset -48
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%r15d, %ecx
	imull	%r12d, %ecx
	cmpl	%ebx, %ecx
	jge	.LBB10_27
# BB#28:                                #   in Loop: Header=BB10_26 Depth=1
	leal	(%rax,%rax,2), %eax
	addl	$-2, %r14d
	cmpl	%r14d, %eax
	setl	%cl
	jmp	.LBB10_29
	.p2align	4, 0x90
.LBB10_27:                              #   in Loop: Header=BB10_26 Depth=1
	addl	%eax, %eax
	addl	$-2, %r14d
	cmpl	%r14d, %eax
	setle	%cl
	xorl	%r15d, %r15d
.LBB10_29:                              #   in Loop: Header=BB10_26 Depth=1
	sarl	%cl, %r12d
	addl	%r12d, %r12d
	cmpl	%ebx, %r12d
	movq	%r13, %rsi
	movq	%rbp, %r9
	jle	.LBB10_26
# BB#30:
	xorl	%eax, %eax
.LBB10_31:                              # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	mp_inv, .Lfunc_end10-mp_inv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4611686018427387904     # double 2
	.text
	.globl	mp_squ
	.p2align	4, 0x90
	.type	mp_squ,@function
mp_squ:                                 # @mp_squ
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi195:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi196:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi197:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi198:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi199:
	.cfi_def_cfa_offset 144
.Lcfi200:
	.cfi_offset %rbx, -56
.Lcfi201:
	.cfi_offset %r12, -48
.Lcfi202:
	.cfi_offset %r13, -40
.Lcfi203:
	.cfi_offset %r14, -32
.Lcfi204:
	.cfi_offset %r15, -24
.Lcfi205:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movl	%esi, %r8d
	movq	168(%rsp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	160(%rsp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	152(%rsp), %r14
	movq	144(%rsp), %rbx
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%r9d, %eax
	sarl	%eax
	movslq	%eax, %rdx
	movslq	%edi, %rcx
	incq	%rdx
	movl	%ecx, %ebp
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	movq	%rdx, %r15
	cmpq	%rcx, %r15
	jge	.LBB11_3
# BB#2:                                 #   in Loop: Header=BB11_1 Depth=1
	leaq	1(%r15), %rdx
	cmpl	$0, 8(%r12,%r15,4)
	je	.LBB11_1
.LBB11_3:
	movl	%edi, %ecx
	shrl	$31, %ecx
	addl	%edi, %ecx
	sarl	%ecx
	incl	%ecx
	movl	%edi, %edx
	subl	%r15d, %edx
	cmpl	%edx, %ecx
	cmovgel	%ecx, %edx
	movl	%edx, 60(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jle	.LBB11_4
# BB#5:
	movl	8(%r12), %r9d
	leal	1(%rax), %ecx
	cmpl	%edi, %eax
	cmovll	%ecx, %ebp
	jmp	.LBB11_6
.LBB11_4:
	xorl	%ebp, %ebp
	xorl	%r9d, %r9d
.LBB11_6:
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	(%r12), %eax
	imull	%r9d, %eax
	cvtsi2sdl	%eax, %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	%eax, %r13
	movsd	%xmm0, 8(%rbx,%r13,8)
	cmpl	%eax, %ebp
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	jge	.LBB11_8
# BB#7:                                 # %.lr.ph61.preheader.i
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	subl	%ebp, %eax
	movq	%r13, %rcx
	subq	%rax, %rcx
	leaq	(%rbx,%rcx,8), %rdi
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movl	%r9d, 32(%rsp)          # 4-byte Spill
	callq	memset
	movl	32(%rsp), %r9d          # 4-byte Reload
	movl	12(%rsp), %r8d          # 4-byte Reload
.LBB11_8:                               # %._crit_edge62.i
	cmpl	$1, %ebp
	jle	.LBB11_13
# BB#9:
	xorl	%ecx, %ecx
	cmpl	$2, %ebp
	je	.LBB11_12
# BB#10:                                # %.lr.ph.preheader.i
	movl	%r8d, %eax
	shrl	$31, %eax
	addl	%r8d, %eax
	sarl	%eax
	incl	%ebp
	movslq	%ebp, %rdx
	xorl	%ecx, %ecx
	movl	$-1, %esi
	.p2align	4, 0x90
.LBB11_11:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rdx,4), %edi
	subl	%ecx, %edi
	cmpl	%eax, %edi
	movl	$0, %ecx
	cmovgel	%esi, %ecx
	movl	$0, %ebp
	cmovgel	%r8d, %ebp
	subl	%ebp, %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	movsd	%xmm0, -8(%rbx,%rdx,8)
	decq	%rdx
	cmpq	$3, %rdx
	jg	.LBB11_11
.LBB11_12:                              # %._crit_edge.i
	movl	12(%r12), %eax
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 16(%rbx)
.LBB11_13:                              # %mp_mul_i2d.exit
	leaq	1(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r9d, %xmm0
	leaq	8(%rbx), %rdx
	movsd	%xmm0, 8(%rbx)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	4(%r12), %xmm0
	movsd	%xmm0, (%rbx)
	movl	$1, %esi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %edi
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movl	28(%rsp), %edi          # 4-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %edx
	movl	%r15d, %ecx
	movq	%r12, %r8
	movq	%r14, %r9
	callq	mp_mul_i2d
	leaq	8(%r14), %r12
	movl	$1, %esi
	movl	%ebp, %edi
	movq	%r12, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	addsd	(%r14), %xmm0
	movsd	%xmm0, (%r14)
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	mulsd	8(%r14), %xmm0
	movsd	%xmm0, 8(%r14)
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulsd	16(%r14), %xmm0
	movsd	%xmm0, 16(%r14)
	cmpl	$4, %ebp
	jl	.LBB11_25
# BB#14:                                # %.lr.ph.preheader.i67
	leaq	-4(%r13), %rbp
	movq	%rbp, %rax
	shrq	%rax
	leaq	1(%rax), %rcx
	cmpq	$2, %rcx
	jae	.LBB11_16
# BB#15:
	movl	$3, %eax
	jmp	.LBB11_24
.LBB11_16:                              # %min.iters.checked
	movl	%ecx, %r8d
	andl	$1, %r8d
	subq	%r8, %rcx
	je	.LBB11_17
# BB#18:                                # %vector.memcheck
	leaq	24(%r14), %rsi
	movabsq	$2305843009213693950, %rdi # imm = 0x1FFFFFFFFFFFFFFE
	andq	%rdi, %rbp
	leaq	24(%rbx), %rdi
	leaq	40(%rbx,%rbp,8), %rdx
	cmpq	%rdx, %rsi
	jae	.LBB11_21
# BB#19:                                # %vector.memcheck
	leaq	40(%r14,%rbp,8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB11_21
# BB#20:
	movl	$3, %eax
	jmp	.LBB11_24
.LBB11_17:
	movl	$3, %eax
	jmp	.LBB11_24
.LBB11_21:                              # %vector.body.preheader
	leaq	5(%rax,%rax), %rax
	leaq	(%r8,%r8), %rdx
	subq	%rdx, %rax
	.p2align	4, 0x90
.LBB11_22:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rdi), %xmm0
	movupd	16(%rdi), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	movupd	(%rsi), %xmm4
	movupd	16(%rsi), %xmm5
	movapd	%xmm4, %xmm6
	unpcklpd	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0]
	mulpd	%xmm4, %xmm0
	unpckhpd	%xmm5, %xmm4    # xmm4 = xmm4[1],xmm5[1]
	mulpd	%xmm5, %xmm1
	movapd	%xmm0, %xmm5
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	movhlps	%xmm0, %xmm1            # xmm1 = xmm0[1],xmm1[1]
	subpd	%xmm1, %xmm5
	mulpd	%xmm2, %xmm4
	mulpd	%xmm3, %xmm6
	addpd	%xmm4, %xmm6
	movapd	%xmm5, %xmm0
	unpcklpd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0]
	movhlps	%xmm5, %xmm6            # xmm6 = xmm5[1],xmm6[1]
	movups	%xmm6, 16(%rsi)
	movupd	%xmm0, (%rsi)
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-2, %rcx
	jne	.LBB11_22
# BB#23:                                # %middle.block
	testq	%r8, %r8
	je	.LBB11_25
	.p2align	4, 0x90
.LBB11_24:                              # %.lr.ph.i70
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbx,%rax,8), %xmm0
	movsd	(%r14,%rax,8), %xmm1    # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movsd	8(%r14,%rax,8), %xmm2   # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	subpd	%xmm2, %xmm0
	addpd	%xmm2, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	%xmm1, (%r14,%rax,8)
	addq	$2, %rax
	cmpq	%r13, %rax
	jl	.LBB11_24
.LBB11_25:                              # %mp_mul_cmul.exit
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	(%rbx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r14,%rax,8), %xmm0
	movsd	%xmm0, (%r14,%rax,8)
	movl	$-1, %esi
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %edi
	movq	%r12, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movl	60(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %edi
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	movl	%r15d, %edx
	movq	%r14, %rcx
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r8
	callq	mp_mul_d2i
	movl	%r12d, %edi
	movl	%ebp, %esi
	movq	%r14, %rdx
	movq	%r14, %rcx
	movq	%r14, %r8
	callq	mp_add
	movsd	.LCPI11_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movhpd	8(%rbx), %xmm0          # xmm0 = xmm0[0],mem[0]
	movupd	(%rbx), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	%xmm1, (%rbx)
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rbx)
	cmpl	$4, %r15d
	jl	.LBB11_34
# BB#26:                                # %.lr.ph.preheader.i72
	leaq	-4(%r13), %rax
	shrq	%rax
	leaq	1(%rax), %rcx
	cmpq	$2, %rcx
	jae	.LBB11_28
# BB#27:
	movl	$3, %eax
	jmp	.LBB11_33
.LBB11_28:                              # %min.iters.checked93
	movl	%ecx, %edx
	andl	$1, %edx
	subq	%rdx, %rcx
	je	.LBB11_29
# BB#30:                                # %vector.body89.preheader
	leaq	5(%rax,%rax), %rax
	leaq	(%rdx,%rdx), %rsi
	subq	%rsi, %rax
	leaq	24(%rbx), %rsi
	.p2align	4, 0x90
.LBB11_31:                              # %vector.body89
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rsi), %xmm0
	movupd	16(%rsi), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	mulpd	%xmm1, %xmm1
	mulpd	%xmm0, %xmm0
	movapd	%xmm0, %xmm4
	unpcklpd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0]
	unpckhpd	%xmm1, %xmm0    # xmm0 = xmm0[1],xmm1[1]
	subpd	%xmm0, %xmm4
	addpd	%xmm2, %xmm2
	mulpd	%xmm3, %xmm2
	movapd	%xmm4, %xmm0
	unpcklpd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0]
	movhlps	%xmm4, %xmm2            # xmm2 = xmm4[1],xmm2[1]
	movups	%xmm2, 16(%rsi)
	movupd	%xmm0, (%rsi)
	addq	$32, %rsi
	addq	$-2, %rcx
	jne	.LBB11_31
# BB#32:                                # %middle.block90
	testq	%rdx, %rdx
	jne	.LBB11_33
	jmp	.LBB11_34
.LBB11_29:
	movl	$3, %eax
	.p2align	4, 0x90
.LBB11_33:                              # %.lr.ph.i75
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rbx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rbx,%rax,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx,%rax,8)
	movsd	%xmm0, 8(%rbx,%rax,8)
	addq	$2, %rax
	cmpq	%r13, %rax
	jl	.LBB11_33
.LBB11_34:                              # %mp_mul_csqu.exit
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	(%rbx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	movl	$-1, %esi
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %edi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movl	28(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %edi
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	movl	%r12d, %edx
	movq	%rbx, %rcx
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r8
	callq	mp_mul_d2i
	movl	%r15d, %edi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%rbx, %r8
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	mp_add                  # TAILCALL
.Lfunc_end11:
	.size	mp_squ, .Lfunc_end11-mp_squ
	.cfi_endproc

	.globl	mp_idiv
	.p2align	4, 0x90
	.type	mp_idiv,@function
mp_idiv:                                # @mp_idiv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi206:
	.cfi_def_cfa_offset 16
.Lcfi207:
	.cfi_offset %rbx, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%ecx, %ecx
	je	.LBB12_1
# BB#2:
	movl	(%rdx), %eax
	jle	.LBB12_4
# BB#3:
	movl	%eax, (%r8)
	cmpl	$0, (%rdx)
	jne	.LBB12_8
	jmp	.LBB12_6
.LBB12_1:
	movl	$-1, %ebx
	jmp	.LBB12_9
.LBB12_4:
	negl	%eax
	movl	%eax, (%r8)
	negl	%ecx
	cmpl	$0, (%rdx)
	je	.LBB12_6
.LBB12_8:
	cvtsi2sdl	%esi, %xmm0
	addq	$4, %rdx
	cvtsi2sdl	%ecx, %xmm1
	addq	$4, %r8
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%rdx, %rsi
	movq	%r8, %rdx
	callq	mp_unsgn_idiv
	xorl	%ebx, %ebx
	jmp	.LBB12_9
.LBB12_6:
	xorl	%ebx, %ebx
	cmpl	$-1, %edi
	jl	.LBB12_9
# BB#7:                                 # %.lr.ph.preheader.i
	incl	%edi
	leaq	4(,%rdi,4), %rdx
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movq	%r8, %rdi
	callq	memset
.LBB12_9:                               # %mp_load_0.exit
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end12:
	.size	mp_idiv, .Lfunc_end12-mp_idiv
	.cfi_endproc

	.globl	mp_sprintf
	.p2align	4, 0x90
	.type	mp_sprintf,@function
mp_sprintf:                             # @mp_sprintf
	.cfi_startproc
# BB#0:
	movl	%esi, %r8d
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	cmpl	$0, (%rdx)
	jns	.LBB13_2
# BB#1:
	movb	$45, (%rcx)
	incq	%rcx
.LBB13_2:
	pushq	%r15
.Lcfi208:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi210:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi211:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi212:
	.cfi_def_cfa_offset 48
.Lcfi213:
	.cfi_offset %rbx, -48
.Lcfi214:
	.cfi_offset %r12, -40
.Lcfi215:
	.cfi_offset %r13, -32
.Lcfi216:
	.cfi_offset %r14, -24
.Lcfi217:
	.cfi_offset %r15, -16
	testl	%r8d, %r8d
	movl	%r8d, %r9d
	jle	.LBB13_5
# BB#3:                                 # %.lr.ph88.preheader
	movl	8(%rdx), %ebx
	movslq	%r8d, %r11
	incq	%r11
	movl	%r8d, %r10d
	movl	%r8d, %r9d
	.p2align	4, 0x90
.LBB13_4:                               # %.lr.ph88
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rsi
	imulq	$1717986919, %rsi, %rbx # imm = 0x66666667
	movq	%rbx, %rax
	shrq	$63, %rax
	sarq	$34, %rbx
	addl	%eax, %ebx
	leal	(%rbx,%rbx), %eax
	leal	(%rax,%rax,4), %eax
	subl	%eax, %esi
	cmovnel	%r10d, %r9d
	addl	$48, %esi
	movb	%sil, -1(%rcx,%r11)
	decq	%r11
	decl	%r10d
	cmpq	$1, %r11
	jg	.LBB13_4
.LBB13_5:                               # %._crit_edge89
	movslq	%r9d, %r11
	movb	(%rcx,%r11), %al
	movb	%al, (%rcx)
	movb	$46, 1(%rcx)
	movl	%r8d, %r10d
	subl	%r11d, %r10d
	jle	.LBB13_26
# BB#6:                                 # %.lr.ph81.preheader
	leal	1(%r8), %r12d
	subl	%r9d, %r12d
	leaq	-1(%r12), %r9
	cmpq	$32, %r9
	jae	.LBB13_8
# BB#7:
	movl	$1, %esi
	jmp	.LBB13_20
.LBB13_8:                               # %min.iters.checked
	movq	%r9, %rsi
	andq	$-32, %rsi
	je	.LBB13_12
# BB#9:                                 # %vector.memcheck
	leaq	2(%rcx), %r14
	leaq	(%r11,%r12), %rbx
	addq	%rcx, %rbx
	cmpq	%rbx, %r14
	jae	.LBB13_13
# BB#10:                                # %vector.memcheck
	leaq	1(%rcx,%r12), %r14
	leaq	1(%rcx,%r11), %rbx
	cmpq	%r14, %rbx
	jae	.LBB13_13
.LBB13_12:
	movl	$1, %esi
.LBB13_20:                              # %.lr.ph81.preheader110
	movl	%r12d, %ebx
	subl	%esi, %ebx
	subq	%rsi, %r9
	andq	$3, %rbx
	je	.LBB13_23
# BB#21:                                # %.lr.ph81.prol.preheader
	leaq	(%rcx,%r11), %r14
	negq	%rbx
	.p2align	4, 0x90
.LBB13_22:                              # %.lr.ph81.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rsi), %eax
	movb	%al, 1(%rcx,%rsi)
	incq	%rsi
	incq	%rbx
	jne	.LBB13_22
.LBB13_23:                              # %.lr.ph81.prol.loopexit
	cmpq	$3, %r9
	jb	.LBB13_26
# BB#24:                                # %.lr.ph81.preheader110.new
	subq	%rsi, %r12
	leaq	3(%rcx,%rsi), %rsi
	.p2align	4, 0x90
.LBB13_25:                              # %.lr.ph81
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%r11,%rsi), %eax
	movb	%al, -2(%rsi)
	movzbl	-2(%r11,%rsi), %eax
	movb	%al, -1(%rsi)
	movzbl	-1(%r11,%rsi), %eax
	movb	%al, (%rsi)
	movzbl	(%r11,%rsi), %eax
	movb	%al, 1(%rsi)
	addq	$4, %rsi
	addq	$-4, %r12
	jne	.LBB13_25
.LBB13_26:                              # %._crit_edge82
	movslq	%r10d, %rax
	leaq	2(%rcx,%rax), %r9
	leal	1(%rdi), %esi
	cmpl	$3, %esi
	jl	.LBB13_33
# BB#27:                                # %.lr.ph77
	addq	$2, %rax
	movslq	%r8d, %r14
	leal	-2(%rdi), %r11d
	incq	%r11
	imulq	%r14, %r11
	addq	%rax, %r11
	testl	%r8d, %r8d
	jle	.LBB13_32
# BB#28:                                # %.lr.ph77.split.us.preheader
	addl	$2, %edi
	leaq	1(%r14), %r15
	movl	$3, %r12d
	.p2align	4, 0x90
.LBB13_29:                              # %.lr.ph77.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_30 Depth 2
	movl	(%rdx,%r12,4), %r13d
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB13_30:                              #   Parent Loop BB13_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%r13d, %rbx
	imulq	$1717986919, %rbx, %rbx # imm = 0x66666667
	movq	%rbx, %rsi
	shrq	$63, %rsi
	sarq	$34, %rbx
	addl	%esi, %ebx
	leal	(%rbx,%rbx), %esi
	leal	(%rsi,%rsi,4), %esi
	negl	%esi
	leal	48(%r13,%rsi), %esi
	movb	%sil, -2(%r9,%rax)
	decq	%rax
	cmpq	$1, %rax
	movl	%ebx, %r13d
	jg	.LBB13_30
# BB#31:                                # %._crit_edge.us
                                        #   in Loop: Header=BB13_29 Depth=1
	addq	%r14, %r9
	incq	%r12
	cmpq	%rdi, %r12
	jne	.LBB13_29
.LBB13_32:                              # %._crit_edge78.loopexit
	addq	%r11, %rcx
	movq	%rcx, %r9
.LBB13_33:                              # %._crit_edge78
	movb	$101, (%r9)
	incq	%r9
	imull	4(%rdx), %r8d
	addl	%r10d, %r8d
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r9, %rdi
	movl	%r8d, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	sprintf                 # TAILCALL
.LBB13_13:                              # %vector.body.preheader
	leaq	-32(%rsi), %rbx
	movq	%rbx, %r14
	shrq	$5, %r14
	btl	$5, %ebx
	jb	.LBB13_15
# BB#14:                                # %vector.body.prol
	movups	1(%rcx,%r11), %xmm0
	movups	17(%rcx,%r11), %xmm1
	movups	%xmm0, 2(%rcx)
	movups	%xmm1, 18(%rcx)
	movl	$32, %r15d
	testq	%r14, %r14
	jne	.LBB13_16
	jmp	.LBB13_18
.LBB13_15:
	xorl	%r15d, %r15d
	testq	%r14, %r14
	je	.LBB13_18
.LBB13_16:                              # %vector.body.preheader.new
	movq	%rsi, %r14
	subq	%r15, %r14
	leaq	49(%rcx,%r15), %rbx
	.p2align	4, 0x90
.LBB13_17:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%r11,%rbx), %xmm0
	movups	-32(%r11,%rbx), %xmm1
	movups	%xmm0, -47(%rbx)
	movups	%xmm1, -31(%rbx)
	movups	-16(%r11,%rbx), %xmm0
	movups	(%r11,%rbx), %xmm1
	movups	%xmm0, -15(%rbx)
	movups	%xmm1, 1(%rbx)
	addq	$64, %rbx
	addq	$-64, %r14
	jne	.LBB13_17
.LBB13_18:                              # %middle.block
	cmpq	%rsi, %r9
	je	.LBB13_26
# BB#19:
	orq	$1, %rsi
	jmp	.LBB13_20
.Lfunc_end13:
	.size	mp_sprintf, .Lfunc_end13-mp_sprintf
	.cfi_endproc

	.globl	mp_load_0
	.p2align	4, 0x90
	.type	mp_load_0,@function
mp_load_0:                              # @mp_load_0
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	cmpl	$-1, %edi
	jl	.LBB14_2
# BB#1:                                 # %.lr.ph.preheader
	pushq	%rax
.Lcfi218:
	.cfi_def_cfa_offset 16
	incl	%edi
	leaq	4(,%rdi,4), %rax
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, %rdx
	callq	memset
	addq	$8, %rsp
.LBB14_2:                               # %._crit_edge
	retq
.Lfunc_end14:
	.size	mp_load_0, .Lfunc_end14-mp_load_0
	.cfi_endproc

	.globl	mp_load_1
	.p2align	4, 0x90
	.type	mp_load_1,@function
mp_load_1:                              # @mp_load_1
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	$1, (%rdx)
	movl	$1, 8(%rdx)
	leal	1(%rdi), %eax
	cmpl	$3, %eax
	jl	.LBB15_2
# BB#1:                                 # %.lr.ph.preheader
	pushq	%rax
.Lcfi219:
	.cfi_def_cfa_offset 16
	addq	$12, %rdx
	addl	$-2, %edi
	leaq	4(,%rdi,4), %rax
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, %rdx
	callq	memset
	addq	$8, %rsp
.LBB15_2:                               # %._crit_edge
	retq
.Lfunc_end15:
	.size	mp_load_1, .Lfunc_end15-mp_load_1
	.cfi_endproc

	.globl	mp_round
	.p2align	4, 0x90
	.type	mp_round,@function
mp_round:                               # @mp_round
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi220:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi221:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi222:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi223:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi224:
	.cfi_def_cfa_offset 48
.Lcfi225:
	.cfi_offset %rbx, -40
.Lcfi226:
	.cfi_offset %r14, -32
.Lcfi227:
	.cfi_offset %r15, -24
.Lcfi228:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movl	%edx, %ebp
	movl	%esi, %r14d
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	cmpl	%edi, %ebp
	jge	.LBB16_11
# BB#1:
	leal	2(%rbp), %r15d
	cmpl	%edi, %r15d
	jg	.LBB16_3
# BB#2:                                 # %.lr.ph38.preheader
	leal	1(%rdi), %eax
	cltq
	movl	%edi, %ecx
	notl	%ecx
	movl	$-3, %edx
	subl	%ebp, %edx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	leal	1(%rdx,%rdi), %ecx
	subq	%rcx, %rax
	leaq	(%rbx,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB16_3:                               # %._crit_edge
	movslq	%r15d, %rcx
	movl	(%rbx,%rcx,4), %eax
	addl	%eax, %eax
	movl	$0, (%rbx,%rcx,4)
	cmpl	%r14d, %eax
	jl	.LBB16_11
# BB#4:
	testl	%ebp, %ebp
	jle	.LBB16_9
# BB#5:                                 # %.lr.ph.preheader
	incl	%ebp
	movslq	%ebp, %rcx
	.p2align	4, 0x90
.LBB16_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %eax
	incl	%eax
	cmpl	%r14d, %eax
	jl	.LBB16_7
# BB#8:                                 #   in Loop: Header=BB16_6 Depth=1
	movl	$0, (%rbx,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB16_6
	jmp	.LBB16_9
.LBB16_7:
	movl	%eax, (%rbx,%rcx,4)
.LBB16_9:                               # %.loopexit
	cmpl	%r14d, %eax
	jl	.LBB16_11
# BB#10:
	movl	$1, 8(%rbx)
	incl	4(%rbx)
.LBB16_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	mp_round, .Lfunc_end16-mp_round
	.cfi_endproc

	.globl	mp_cmp
	.p2align	4, 0x90
	.type	mp_cmp,@function
mp_cmp:                                 # @mp_cmp
	.cfi_startproc
# BB#0:
	movl	(%rdx), %r8d
	movl	$1, %eax
	cmpl	(%rcx), %r8d
	jg	.LBB17_10
# BB#1:
	movl	$-1, %eax
	jl	.LBB17_10
# BB#2:
	testl	%edi, %edi
	js	.LBB17_3
# BB#4:                                 # %.lr.ph.preheader.i
	movslq	%edi, %rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB17_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rdx,%rsi,4), %edi
	subl	4(%rcx,%rsi,4), %edi
	cmpq	%rax, %rsi
	jge	.LBB17_7
# BB#6:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB17_5 Depth=1
	incq	%rsi
	testl	%edi, %edi
	je	.LBB17_5
.LBB17_7:                               # %._crit_edge.i
	movl	$1, %eax
	testl	%edi, %edi
	jg	.LBB17_9
	jmp	.LBB17_8
.LBB17_3:
	xorl	%edi, %edi
.LBB17_8:                               # %._crit_edge.thread.i
	negl	%edi
	sbbl	%eax, %eax
.LBB17_9:                               # %mp_unsgn_cmp.exit
	imull	%r8d, %eax
.LBB17_10:
	retq
.Lfunc_end17:
	.size	mp_cmp, .Lfunc_end17-mp_cmp
	.cfi_endproc

	.globl	mp_unsgn_cmp
	.p2align	4, 0x90
	.type	mp_unsgn_cmp,@function
mp_unsgn_cmp:                           # @mp_unsgn_cmp
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	js	.LBB18_1
# BB#2:                                 # %.lr.ph.preheader
	movslq	%edi, %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB18_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rdi,4), %eax
	subl	(%rdx,%rdi,4), %eax
	cmpq	%rcx, %rdi
	jge	.LBB18_5
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB18_3 Depth=1
	incq	%rdi
	testl	%eax, %eax
	je	.LBB18_3
.LBB18_5:                               # %._crit_edge
	testl	%eax, %eax
	jle	.LBB18_6
# BB#7:
	movl	$1, %eax
	retq
.LBB18_1:
	xorl	%eax, %eax
.LBB18_6:                               # %._crit_edge.thread
	negl	%eax
	sbbl	%eax, %eax
	retq
.Lfunc_end18:
	.size	mp_unsgn_cmp, .Lfunc_end18-mp_unsgn_cmp
	.cfi_endproc

	.globl	mp_unexp_add
	.p2align	4, 0x90
	.type	mp_unexp_add,@function
mp_unexp_add:                           # @mp_unexp_add
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi229:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi230:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi231:
	.cfi_def_cfa_offset 32
.Lcfi232:
	.cfi_offset %rbx, -32
.Lcfi233:
	.cfi_offset %r14, -24
.Lcfi234:
	.cfi_offset %rbp, -16
	testl	%edx, %edx
	jne	.LBB19_8
# BB#1:
	movl	(%r8), %eax
	addl	(%rcx), %eax
	cmpl	%esi, %eax
	jge	.LBB19_2
.LBB19_8:
	cmpl	%edi, %edx
	movl	%edx, %r11d
	cmovgl	%edi, %r11d
	xorl	%r10d, %r10d
	cmpl	%edx, %edi
	jle	.LBB19_11
# BB#9:                                 # %.lr.ph92.preheader
	movslq	%edi, %rdx
	movslq	%r11d, %r14
	movq	%r14, %rax
	notq	%rax
	leaq	(%r8,%rax,4), %rax
	xorl	%r10d, %r10d
	movl	$-1, %r8d
	.p2align	4, 0x90
.LBB19_10:                              # %.lr.ph92
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx,%rdx,4), %ebp
	subl	%r10d, %ebp
	addl	(%rax,%rdx,4), %ebp
	cmpl	%esi, %ebp
	movl	$0, %r10d
	cmovgel	%r8d, %r10d
	movl	$0, %ebx
	cmovgel	%esi, %ebx
	subl	%ebx, %ebp
	movl	%ebp, -4(%r9,%rdx,4)
	decq	%rdx
	cmpq	%r14, %rdx
	jg	.LBB19_10
.LBB19_11:                              # %.preheader82
	testl	%r11d, %r11d
	jle	.LBB19_14
# BB#12:                                # %.lr.ph88.preheader
	movslq	%r11d, %rax
	incq	%rax
	movl	$-1, %edx
	.p2align	4, 0x90
.LBB19_13:                              # %.lr.ph88
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rcx,%rax,4), %ebp
	subl	%r10d, %ebp
	cmpl	%esi, %ebp
	movl	$0, %r10d
	cmovgel	%edx, %r10d
	movl	$0, %ebx
	cmovgel	%esi, %ebx
	subl	%ebx, %ebp
	movl	%ebp, -8(%r9,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB19_13
.LBB19_14:                              # %._crit_edge
	testl	%r10d, %r10d
	je	.LBB19_15
# BB#16:                                # %.preheader
	movl	$-1, %eax
	cmpl	$2, %edi
	jl	.LBB19_24
# BB#17:                                # %.lr.ph.preheader
	movslq	%edi, %rcx
	leaq	-1(%rcx), %rax
	addq	$-2, %rcx
	movq	%rax, %rdx
	andq	$7, %rdx
	je	.LBB19_20
# BB#18:                                # %.lr.ph.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB19_19:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r9,%rax,4), %esi
	movl	%esi, (%r9,%rax,4)
	decq	%rax
	incq	%rdx
	jne	.LBB19_19
.LBB19_20:                              # %.lr.ph.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB19_23
# BB#21:                                # %.lr.ph.preheader.new
	incq	%rax
	.p2align	4, 0x90
.LBB19_22:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%r9,%rax,4), %ecx
	movl	%ecx, -4(%r9,%rax,4)
	movl	-12(%r9,%rax,4), %ecx
	movl	%ecx, -8(%r9,%rax,4)
	movl	-16(%r9,%rax,4), %ecx
	movl	%ecx, -12(%r9,%rax,4)
	movl	-20(%r9,%rax,4), %ecx
	movl	%ecx, -16(%r9,%rax,4)
	movl	-24(%r9,%rax,4), %ecx
	movl	%ecx, -20(%r9,%rax,4)
	movl	-28(%r9,%rax,4), %ecx
	movl	%ecx, -24(%r9,%rax,4)
	movl	-32(%r9,%rax,4), %ecx
	movl	%ecx, -28(%r9,%rax,4)
	movl	-36(%r9,%rax,4), %ecx
	movl	%ecx, -32(%r9,%rax,4)
	addq	$-8, %rax
	cmpq	$1, %rax
	jg	.LBB19_22
.LBB19_23:
	movl	%r10d, %eax
	jmp	.LBB19_24
.LBB19_15:
	xorl	%eax, %eax
	jmp	.LBB19_25
.LBB19_2:
	movslq	%edi, %r10
	movl	-4(%r8,%r10,4), %eax
	addl	-4(%rcx,%r10,4), %eax
	xorl	%edx, %edx
	cmpl	%esi, %eax
	movl	$-1, %eax
	cmovll	%edx, %eax
	cmpl	$2, %r10d
	jl	.LBB19_24
# BB#3:                                 # %.lr.ph97.preheader
	leaq	-1(%r10), %rdx
	testb	$1, %dl
	je	.LBB19_5
# BB#4:                                 # %.lr.ph97.prol
	movl	-8(%rcx,%r10,4), %edx
	subl	%eax, %edx
	addl	-8(%r8,%r10,4), %edx
	xorl	%ebx, %ebx
	cmpl	%esi, %edx
	movl	$-1, %eax
	cmovll	%ebx, %eax
	cmovgel	%esi, %ebx
	subl	%ebx, %edx
	movl	%edx, -4(%r9,%r10,4)
	leaq	-2(%r10), %rdx
.LBB19_5:                               # %.lr.ph97.prol.loopexit
	cmpl	$2, %edi
	je	.LBB19_24
# BB#6:                                 # %.lr.ph97.preheader.new
	incq	%rdx
	movl	$-1, %r10d
	.p2align	4, 0x90
.LBB19_7:                               # %.lr.ph97
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rcx,%rdx,4), %ebx
	subl	%eax, %ebx
	addl	-8(%r8,%rdx,4), %ebx
	xorl	%edi, %edi
	cmpl	%esi, %ebx
	setge	%dil
	movl	$0, %eax
	cmovgel	%esi, %eax
	subl	%eax, %ebx
	movl	%ebx, -4(%r9,%rdx,4)
	addl	-12(%rcx,%rdx,4), %edi
	addl	-12(%r8,%rdx,4), %edi
	cmpl	%esi, %edi
	movl	$0, %eax
	cmovgel	%r10d, %eax
	movl	$0, %ebx
	cmovgel	%esi, %ebx
	subl	%ebx, %edi
	movl	%edi, -8(%r9,%rdx,4)
	addq	$-2, %rdx
	cmpq	$1, %rdx
	jg	.LBB19_7
.LBB19_24:                              # %.sink.split
	movl	%eax, %ecx
	negl	%ecx
	movl	%ecx, (%r9)
.LBB19_25:
	negl	%eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end19:
	.size	mp_unexp_add, .Lfunc_end19-mp_unexp_add
	.cfi_endproc

	.globl	mp_unexp_sub
	.p2align	4, 0x90
	.type	mp_unexp_sub,@function
mp_unexp_sub:                           # @mp_unexp_sub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi235:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi236:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi237:
	.cfi_def_cfa_offset 32
.Lcfi238:
	.cfi_offset %rbx, -24
.Lcfi239:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	cmpl	%edi, %edx
	movl	%edx, %r10d
	cmovgl	%edi, %r10d
	xorl	%eax, %eax
	cmpl	%edx, %edi
	jle	.LBB20_3
# BB#1:                                 # %.lr.ph90.preheader
	movslq	%edi, %rdx
	movslq	%r10d, %r11
	movq	%r11, %rax
	notq	%rax
	leaq	(%r8,%rax,4), %r8
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB20_2:                               # %.lr.ph90
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx,%rdx,4), %ebx
	subl	(%r8,%rdx,4), %ebx
	addl	%eax, %ebx
	movl	%ebx, %eax
	sarl	$31, %eax
	movl	%eax, %ebp
	andl	%esi, %ebp
	addl	%ebx, %ebp
	movl	%ebp, -4(%r9,%rdx,4)
	decq	%rdx
	cmpq	%r11, %rdx
	jg	.LBB20_2
.LBB20_3:                               # %.preheader72
	testl	%r10d, %r10d
	jle	.LBB20_6
# BB#4:                                 # %.lr.ph86.preheader
	movslq	%r10d, %rdx
	incq	%rdx
	.p2align	4, 0x90
.LBB20_5:                               # %.lr.ph86
                                        # =>This Inner Loop Header: Depth=1
	addl	-8(%rcx,%rdx,4), %eax
	movl	%eax, %ebx
	sarl	$31, %ebx
	movl	%ebx, %ebp
	andl	%esi, %ebp
	addl	%eax, %ebp
	movl	%ebp, -8(%r9,%rdx,4)
	decq	%rdx
	cmpq	$1, %rdx
	movl	%ebx, %eax
	jg	.LBB20_5
.LBB20_6:                               # %.preheader70
	testl	%edi, %edi
	jle	.LBB20_7
# BB#8:                                 # %.lr.ph81.preheader
	movslq	%edi, %rax
	xorl	%ebx, %ebx
	movl	%edi, %ecx
	.p2align	4, 0x90
.LBB20_9:                               # %.lr.ph81
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%r9,%rbx,4)
	jne	.LBB20_11
# BB#10:                                #   in Loop: Header=BB20_9 Depth=1
	incq	%rbx
	decl	%ecx
	cmpq	%rax, %rbx
	jl	.LBB20_9
	jmp	.LBB20_29
.LBB20_7:
	xorl	%ebx, %ebx
	jmp	.LBB20_29
.LBB20_11:                              # %.critedge
	testl	%ebx, %ebx
	jle	.LBB20_29
# BB#12:                                # %.preheader69
	movl	%edi, %r10d
	subl	%ebx, %r10d
	jle	.LBB20_28
# BB#13:                                # %.lr.ph79.preheader
	movl	%r10d, %r11d
	cmpl	$7, %r10d
	jbe	.LBB20_14
# BB#22:                                # %min.iters.checked
	movl	%r10d, %r8d
	andl	$7, %r8d
	movq	%r11, %rsi
	subq	%r8, %rsi
	je	.LBB20_14
# BB#23:                                # %vector.memcheck
	movslq	%ebx, %rax
	leaq	(%rax,%r11), %rdx
	leaq	(%r9,%rdx,4), %rdx
	cmpq	%r9, %rdx
	jbe	.LBB20_25
# BB#24:                                # %vector.memcheck
	leaq	(%r9,%r11,4), %rdx
	leaq	(%r9,%rax,4), %rax
	cmpq	%rdx, %rax
	jae	.LBB20_25
.LBB20_14:
	xorl	%esi, %esi
.LBB20_15:                              # %.lr.ph79.preheader113
	movl	%r11d, %eax
	subl	%esi, %eax
	decq	%r11
	subq	%rsi, %r11
	testb	$3, %al
	je	.LBB20_19
# BB#16:                                # %.lr.ph79.prol.preheader
	leaq	(%r9,%rsi,4), %rax
	negq	%rsi
	leal	(%rsi,%rcx), %edx
	andl	$3, %edx
	negq	%rdx
	.p2align	4, 0x90
.LBB20_17:                              # %.lr.ph79.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rbx,4), %ebp
	movl	%ebp, (%rax)
	decq	%rsi
	addq	$4, %rax
	incq	%rdx
	jne	.LBB20_17
# BB#18:                                # %.lr.ph79.prol.loopexit.unr-lcssa
	negq	%rsi
.LBB20_19:                              # %.lr.ph79.prol.loopexit
	cmpq	$3, %r11
	jb	.LBB20_28
# BB#20:                                # %.lr.ph79.preheader113.new
	movl	%ecx, %eax
	subq	%rsi, %rax
	leaq	12(%r9,%rsi,4), %rcx
	.p2align	4, 0x90
.LBB20_21:                              # %.lr.ph79
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rcx,%rbx,4), %edx
	movl	%edx, -12(%rcx)
	movl	-8(%rcx,%rbx,4), %edx
	movl	%edx, -8(%rcx)
	movl	-4(%rcx,%rbx,4), %edx
	movl	%edx, -4(%rcx)
	movl	(%rcx,%rbx,4), %edx
	movl	%edx, (%rcx)
	addq	$16, %rcx
	addq	$-4, %rax
	jne	.LBB20_21
.LBB20_28:                              # %.lr.ph.preheader
	movslq	%r10d, %rax
	leaq	(%r9,%rax,4), %rax
	leal	1(%rdi), %ecx
	subl	%ebx, %ecx
	cmpl	%edi, %ecx
	cmovll	%edi, %ecx
	leal	-1(%rbx,%rcx), %ecx
	subl	%edi, %ecx
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	memset
.LBB20_29:                              # %.critedge.thread
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB20_25:                              # %vector.body.preheader
	leaq	16(%r9), %rax
	movl	%ecx, %ebp
	movl	%ecx, %edx
	andb	$7, %dl
	movzbl	%dl, %edx
	subq	%rdx, %rbp
	.p2align	4, 0x90
.LBB20_26:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rax,%rbx,4), %xmm0
	movups	(%rax,%rbx,4), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %rax
	addq	$-8, %rbp
	jne	.LBB20_26
# BB#27:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB20_15
	jmp	.LBB20_28
.Lfunc_end20:
	.size	mp_unexp_sub, .Lfunc_end20-mp_unexp_sub
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI21_0:
	.quad	4607182418800017408     # double 1
.LCPI21_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	mp_unsgn_imul
	.p2align	4, 0x90
	.type	mp_unsgn_imul,@function
mp_unsgn_imul:                          # @mp_unsgn_imul
	.cfi_startproc
# BB#0:
	movsd	.LCPI21_0(%rip), %xmm3  # xmm3 = mem[0],zero
	movapd	%xmm3, %xmm2
	divsd	%xmm0, %xmm2
	testl	%edi, %edi
	jle	.LBB21_1
# BB#2:                                 # %.lr.ph80.preheader
	movslq	%edi, %rcx
	incq	%rcx
	xorl	%r11d, %r11d
	movsd	.LCPI21_1(%rip), %xmm4  # xmm4 = mem[0],zero
	.p2align	4, 0x90
.LBB21_3:                               # %.lr.ph80
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2sdl	-4(%rsi,%rcx,4), %xmm5
	mulsd	%xmm1, %xmm5
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%r11d, %xmm6
	addsd	%xmm5, %xmm6
	addsd	%xmm4, %xmm6
	movapd	%xmm2, %xmm5
	mulsd	%xmm6, %xmm5
	cvttsd2si	%xmm5, %r11d
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%r11d, %xmm5
	mulsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm6
	cvttsd2si	%xmm6, %eax
	movl	%eax, -4(%rdx,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB21_3
	jmp	.LBB21_4
.LBB21_1:
	xorl	%r11d, %r11d
.LBB21_4:                               # %._crit_edge81
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r11d, %xmm1
	addsd	.LCPI21_1(%rip), %xmm1
	ucomisd	.LCPI21_0(%rip), %xmm1
	jbe	.LBB21_22
# BB#5:                                 # %.lr.ph75.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB21_6:                               # %.lr.ph75
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm2, %xmm1
	incl	%ecx
	ucomisd	%xmm3, %xmm1
	ja	.LBB21_6
# BB#7:                                 # %._crit_edge
	movl	(%rsi), %eax
	addl	%ecx, %eax
	movl	%eax, (%rdx)
	movl	%ecx, %r10d
	decl	%r10d
	js	.LBB21_23
# BB#8:                                 # %.preheader64
	cmpl	%edi, %r10d
	jge	.LBB21_9
# BB#11:                                # %.preheader63
	cmpl	%edi, %ecx
	jge	.LBB21_18
# BB#12:                                # %.lr.ph68.preheader
	movslq	%edi, %rsi
	movslq	%ecx, %r8
	movl	%edi, %eax
	subl	%ecx, %eax
	leaq	-1(%rsi), %r9
	subq	%r8, %r9
	testb	$3, %al
	je	.LBB21_15
# BB#13:                                # %.lr.ph68.prol.preheader
	addb	$3, %dil
	subb	%r10b, %dil
	movzbl	%dil, %edi
	andl	$3, %edi
	negq	%rdi
	.p2align	4, 0x90
.LBB21_14:                              # %.lr.ph68.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rax
	subq	%r8, %rax
	movl	(%rdx,%rax,4), %eax
	movl	%eax, (%rdx,%rsi,4)
	decq	%rsi
	incq	%rdi
	jne	.LBB21_14
.LBB21_15:                              # %.lr.ph68.prol.loopexit
	cmpq	$3, %r9
	jb	.LBB21_16
# BB#20:                                # %.lr.ph68.preheader.new
	decq	%rsi
	.p2align	4, 0x90
.LBB21_21:                              # %.lr.ph68
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rsi), %rax
	subq	%r8, %rax
	movl	(%rdx,%rax,4), %eax
	movl	%eax, 4(%rdx,%rsi,4)
	movq	%rsi, %rax
	subq	%r8, %rax
	movl	(%rdx,%rax,4), %eax
	movl	%eax, (%rdx,%rsi,4)
	leaq	-1(%rsi), %rax
	subq	%r8, %rax
	movl	(%rdx,%rax,4), %eax
	movl	%eax, -4(%rdx,%rsi,4)
	leaq	-2(%rsi), %rax
	subq	%r8, %rax
	movl	(%rdx,%rax,4), %eax
	movl	%eax, -8(%rdx,%rsi,4)
	leaq	-4(%rsi), %rax
	addq	$-3, %rsi
	cmpq	%r8, %rsi
	movq	%rax, %rsi
	jg	.LBB21_21
.LBB21_16:
	movl	%ecx, %edi
	jmp	.LBB21_17
.LBB21_22:                              # %._crit_edge.thread
	movl	(%rsi), %eax
	movl	%eax, (%rdx)
	retq
.LBB21_9:
	movsd	.LCPI21_1(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB21_10:                              # %.lr.ph71
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%r11d, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	%xmm1, %xmm3
	cvttsd2si	%xmm3, %r11d
	decl	%ecx
	cmpl	%edi, %ecx
	jg	.LBB21_10
.LBB21_17:                              # %.preheader
	testl	%edi, %edi
	movl	%edi, %ecx
	jle	.LBB21_23
.LBB21_18:                              # %.lr.ph.preheader
	movslq	%ecx, %rcx
	incq	%rcx
	movsd	.LCPI21_1(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB21_19:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%r11d, %xmm3
	addsd	%xmm1, %xmm3
	movapd	%xmm2, %xmm4
	mulsd	%xmm3, %xmm4
	cvttsd2si	%xmm4, %r11d
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%r11d, %xmm4
	mulsd	%xmm0, %xmm4
	subsd	%xmm4, %xmm3
	cvttsd2si	%xmm3, %eax
	movl	%eax, -4(%rdx,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB21_19
.LBB21_23:                              # %.loopexit
	retq
.Lfunc_end21:
	.size	mp_unsgn_imul, .Lfunc_end21-mp_unsgn_imul
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI22_0:
	.quad	-4620693217682128896    # double -0.5
.LCPI22_1:
	.quad	4607182418800017408     # double 1
.LCPI22_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	mp_unsgn_idiv
	.p2align	4, 0x90
	.type	mp_unsgn_idiv,@function
mp_unsgn_idiv:                          # @mp_unsgn_idiv
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movsd	.LCPI22_0(%rip), %xmm2  # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	movslq	%edi, %r10
	xorpd	%xmm3, %xmm3
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB22_1:                               # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	mulsd	%xmm0, %xmm3
	cmpq	%r10, %rax
	jge	.LBB22_3
# BB#2:                                 #   in Loop: Header=BB22_1 Depth=1
	cvtsi2sdl	4(%rsi,%rax,4), %xmm4
	addsd	%xmm4, %xmm3
.LBB22_3:                               #   in Loop: Header=BB22_1 Depth=1
	leaq	1(%rax), %rcx
	ucomisd	%xmm3, %xmm2
	ja	.LBB22_1
# BB#4:
	movsd	.LCPI22_1(%rip), %xmm2  # xmm2 = mem[0],zero
	divsd	%xmm1, %xmm2
	addsd	.LCPI22_2(%rip), %xmm3
	movapd	%xmm2, %xmm4
	mulsd	%xmm3, %xmm4
	cvttsd2si	%xmm4, %r9d
	movl	%r9d, 4(%rdx)
	movl	(%rsi), %r11d
	subl	%ecx, %r11d
	decl	%ecx
	incl	%r11d
	leal	-1(%r10), %r8d
	cmpq	%r10, %rax
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%r9d, %xmm4
	mulsd	%xmm1, %xmm4
	subsd	%xmm4, %xmm3
	movl	%r11d, (%rdx)
	cmovll	%ecx, %r8d
	cvttsd2si	%xmm3, %ecx
	movl	%r10d, %r9d
	subl	%r8d, %r9d
	cmpl	$2, %r9d
	jl	.LBB22_7
# BB#5:                                 # %.lr.ph82.preheader
	movslq	%r8d, %r11
	incl	%edi
	subl	%r8d, %edi
	leaq	8(%rdx), %rax
	leaq	8(%rsi,%r11,4), %rsi
	addq	$-2, %rdi
	movsd	.LCPI22_2(%rip), %xmm3  # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB22_6:                               # %.lr.ph82
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm4, %xmm4
	cvtsi2sdl	(%rsi), %xmm4
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%ecx, %xmm5
	mulsd	%xmm0, %xmm5
	addsd	%xmm4, %xmm5
	addsd	%xmm3, %xmm5
	movapd	%xmm2, %xmm4
	mulsd	%xmm5, %xmm4
	cvttsd2si	%xmm4, %ecx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	mulsd	%xmm1, %xmm4
	subsd	%xmm4, %xmm5
	movl	%ecx, (%rax)
	cvttsd2si	%xmm5, %ecx
	addq	$4, %rax
	addq	$4, %rsi
	decq	%rdi
	jne	.LBB22_6
.LBB22_7:                               # %.preheader
	testl	%r8d, %r8d
	jle	.LBB22_10
# BB#8:                                 # %.lr.ph.preheader
	movslq	%r9d, %rax
	movsd	.LCPI22_2(%rip), %xmm3  # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB22_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	mulsd	%xmm0, %xmm4
	addsd	%xmm3, %xmm4
	movapd	%xmm2, %xmm5
	mulsd	%xmm4, %xmm5
	cvttsd2si	%xmm5, %esi
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%esi, %xmm5
	mulsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm4
	cvttsd2si	%xmm4, %ecx
	movl	%esi, 4(%rdx,%rax,4)
	incq	%rax
	cmpq	%r10, %rax
	jl	.LBB22_9
.LBB22_10:                              # %._crit_edge
	retq
.Lfunc_end22:
	.size	mp_unsgn_idiv, .Lfunc_end22-mp_unsgn_idiv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI23_0:
	.quad	4611686018427387904     # double 2
	.text
	.globl	mp_mul_csqu
	.p2align	4, 0x90
	.type	mp_mul_csqu,@function
mp_mul_csqu:                            # @mp_mul_csqu
	.cfi_startproc
# BB#0:
	movsd	.LCPI23_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movhpd	8(%rsi), %xmm0          # xmm0 = xmm0[0],mem[0]
	movupd	(%rsi), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	%xmm1, (%rsi)
	movsd	16(%rsi), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsi)
	cmpl	$4, %edi
	jl	.LBB23_9
# BB#1:                                 # %.lr.ph.preheader
	movslq	%edi, %r9
	leaq	-4(%r9), %rax
	shrq	%rax
	leaq	1(%rax), %rdx
	cmpq	$2, %rdx
	jae	.LBB23_3
# BB#2:
	movl	$3, %ecx
	jmp	.LBB23_8
.LBB23_3:                               # %min.iters.checked
	movl	%edx, %r8d
	andl	$1, %r8d
	subq	%r8, %rdx
	je	.LBB23_4
# BB#5:                                 # %vector.body.preheader
	leaq	5(%rax,%rax), %rcx
	leaq	(%r8,%r8), %rax
	subq	%rax, %rcx
	leaq	24(%rsi), %rax
	.p2align	4, 0x90
.LBB23_6:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rax), %xmm0
	movupd	16(%rax), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	mulpd	%xmm1, %xmm1
	mulpd	%xmm0, %xmm0
	movapd	%xmm0, %xmm4
	unpcklpd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0]
	unpckhpd	%xmm1, %xmm0    # xmm0 = xmm0[1],xmm1[1]
	subpd	%xmm0, %xmm4
	addpd	%xmm2, %xmm2
	mulpd	%xmm3, %xmm2
	movapd	%xmm4, %xmm0
	unpcklpd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0]
	movhlps	%xmm4, %xmm2            # xmm2 = xmm4[1],xmm2[1]
	movups	%xmm2, 16(%rax)
	movupd	%xmm0, (%rax)
	addq	$32, %rax
	addq	$-2, %rdx
	jne	.LBB23_6
# BB#7:                                 # %middle.block
	testq	%r8, %r8
	jne	.LBB23_8
	jmp	.LBB23_9
.LBB23_4:
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB23_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rsi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rsi,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rsi,%rcx,8)
	movsd	%xmm0, 8(%rsi,%rcx,8)
	addq	$2, %rcx
	cmpq	%r9, %rcx
	jl	.LBB23_8
.LBB23_9:                               # %._crit_edge
	movslq	%edi, %rax
	movsd	8(%rsi,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsi,%rax,8)
	retq
.Lfunc_end23:
	.size	mp_mul_csqu, .Lfunc_end23-mp_mul_csqu
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI24_0:
	.quad	4611686018427387904     # double 2
.LCPI24_1:
	.quad	4607182418800017408     # double 1
.LCPI24_2:
	.quad	4602678819172646912     # double 0.5
.LCPI24_4:
	.quad	-4620693217682128896    # double -0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI24_3:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	mp_mul_d2i_test
	.p2align	4, 0x90
	.type	mp_mul_d2i_test,@function
mp_mul_d2i_test:                        # @mp_mul_d2i_test
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%esi, %xmm2
	movslq	%esi, %rax
	movsd	8(%rdx,%rax,8), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	mulsd	.LCPI24_2(%rip), %xmm1
	movapd	.LCPI24_3(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm1, %xmm3
	xorpd	%xmm0, %xmm0
	movapd	%xmm1, %xmm4
	cmpltsd	%xmm0, %xmm4
	movapd	%xmm4, %xmm5
	andnpd	%xmm1, %xmm5
	andpd	%xmm3, %xmm4
	orpd	%xmm5, %xmm4
	movsd	8(%rdx), %xmm7          # xmm7 = mem[0],zero
	subsd	%xmm4, %xmm7
	movsd	%xmm7, 8(%rdx,%rax,8)
	testl	%eax, %eax
	jle	.LBB24_7
# BB#1:                                 # %.lr.ph.preheader
	movsd	.LCPI24_0(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	movsd	.LCPI24_1(%rip), %xmm3  # xmm3 = mem[0],zero
	divsd	%xmm2, %xmm3
	mulsd	%xmm3, %xmm3
	incq	%rax
	decq	%rax
	xorl	%ecx, %ecx
	xorpd	%xmm0, %xmm0
	movsd	.LCPI24_2(%rip), %xmm4  # xmm4 = mem[0],zero
	movsd	.LCPI24_4(%rip), %xmm5  # xmm5 = mem[0],zero
	movapd	.LCPI24_3(%rip), %xmm8  # xmm8 = [-0.000000e+00,-0.000000e+00]
	xorl	%r8d, %r8d
	jmp	.LBB24_2
	.p2align	4, 0x90
.LBB24_6:                               # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB24_2 Depth=1
	addl	%r8d, %ecx
	movsd	(%rdx,%rax,8), %xmm7    # xmm7 = mem[0],zero
	decq	%rax
	movl	%edi, %r8d
.LBB24_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm1, %xmm7
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%ecx, %xmm6
	addsd	%xmm7, %xmm6
	addsd	%xmm4, %xmm6
	mulsd	%xmm3, %xmm6
	cvttsd2si	%xmm6, %edi
	decl	%edi
	xorps	%xmm7, %xmm7
	cvtsi2sdl	%edi, %xmm7
	subsd	%xmm7, %xmm6
	mulsd	%xmm2, %xmm6
	cvttsd2si	%xmm6, %ecx
	xorps	%xmm7, %xmm7
	cvtsi2sdl	%ecx, %xmm7
	subsd	%xmm7, %xmm6
	mulsd	%xmm2, %xmm6
	cvttsd2si	%xmm6, %esi
	addsd	%xmm5, %xmm6
	xorps	%xmm7, %xmm7
	cvtsi2sdl	%esi, %xmm7
	subsd	%xmm7, %xmm6
	ucomisd	%xmm0, %xmm6
	ja	.LBB24_4
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=1
	xorpd	%xmm8, %xmm6
	ucomisd	%xmm0, %xmm6
	jbe	.LBB24_5
.LBB24_4:                               #   in Loop: Header=BB24_2 Depth=1
	movapd	%xmm6, %xmm0
.LBB24_5:                               #   in Loop: Header=BB24_2 Depth=1
	cmpq	$2, %rax
	jge	.LBB24_6
.LBB24_7:                               # %._crit_edge
	retq
.Lfunc_end24:
	.size	mp_mul_d2i_test, .Lfunc_end24-mp_mul_d2i_test
	.cfi_endproc

	.globl	mp_mul_i2d
	.p2align	4, 0x90
	.type	mp_mul_i2d,@function
mp_mul_i2d:                             # @mp_mul_i2d
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi240:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi241:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi242:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi243:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi244:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi245:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi246:
	.cfi_def_cfa_offset 64
.Lcfi247:
	.cfi_offset %rbx, -56
.Lcfi248:
	.cfi_offset %r12, -48
.Lcfi249:
	.cfi_offset %r13, -40
.Lcfi250:
	.cfi_offset %r14, -32
.Lcfi251:
	.cfi_offset %r15, -24
.Lcfi252:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, %r15
	movl	%ecx, %r14d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r12d
	xorl	%r13d, %r13d
	subl	%r14d, %edi
	movl	$0, %r9d
	jle	.LBB25_2
# BB#1:
	movslq	%r14d, %rax
	movl	8(%r15,%rax,4), %r9d
	movl	%edx, %eax
	sarl	%eax
	leal	1(%rax), %r13d
	cmpl	%edi, %eax
	cmovgel	%edi, %r13d
.LBB25_2:
	movl	(%r15), %eax
	imull	%r9d, %eax
	cvtsi2sdl	%eax, %xmm0
	movslq	%edx, %rax
	movsd	%xmm0, 8(%rbx,%rax,8)
	cmpl	%edx, %r13d
	jge	.LBB25_4
# BB#3:                                 # %.lr.ph61.preheader
	decl	%edx
	subl	%r13d, %edx
	subq	%rdx, %rax
	leaq	(%rbx,%rax,8), %rdi
	leaq	8(,%rdx,8), %rdx
	xorl	%esi, %esi
	movl	%r9d, %ebp
	callq	memset
	movl	%ebp, %r9d
.LBB25_4:                               # %._crit_edge62
	cmpl	$2, %r13d
	jl	.LBB25_9
# BB#5:
	xorl	%ecx, %ecx
	cmpl	$2, %r13d
	je	.LBB25_8
# BB#6:                                 # %.lr.ph.preheader
	movl	%r12d, %eax
	shrl	$31, %eax
	addl	%r12d, %eax
	sarl	%eax
	incl	%r13d
	movslq	%r13d, %rdx
	movslq	%r14d, %rcx
	leaq	(%r15,%rcx,4), %rsi
	xorl	%ecx, %ecx
	movl	$-1, %r8d
	.p2align	4, 0x90
.LBB25_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rdx,4), %ebp
	subl	%ecx, %ebp
	cmpl	%eax, %ebp
	movl	$0, %ecx
	cmovgel	%r8d, %ecx
	movl	$0, %edi
	cmovgel	%r12d, %edi
	subl	%edi, %ebp
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	movsd	%xmm0, -8(%rbx,%rdx,8)
	decq	%rdx
	cmpq	$3, %rdx
	jg	.LBB25_7
.LBB25_8:                               # %._crit_edge
	movslq	%r14d, %rax
	movl	12(%r15,%rax,4), %eax
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 16(%rbx)
.LBB25_9:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r9d, %xmm0
	movsd	%xmm0, 8(%rbx)
	movl	4(%r15), %eax
	subl	%r14d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	mp_mul_i2d, .Lfunc_end25-mp_mul_i2d
	.cfi_endproc

	.globl	mp_mul_cmul
	.p2align	4, 0x90
	.type	mp_mul_cmul,@function
mp_mul_cmul:                            # @mp_mul_cmul
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi253:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi254:
	.cfi_def_cfa_offset 24
.Lcfi255:
	.cfi_offset %rbx, -24
.Lcfi256:
	.cfi_offset %r14, -16
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rdx), %xmm0
	movsd	%xmm0, (%rdx)
	movsd	8(%rsi), %xmm0          # xmm0 = mem[0],zero
	mulsd	8(%rdx), %xmm0
	movsd	%xmm0, 8(%rdx)
	movsd	16(%rsi), %xmm0         # xmm0 = mem[0],zero
	mulsd	16(%rdx), %xmm0
	movsd	%xmm0, 16(%rdx)
	cmpl	$4, %edi
	jl	.LBB26_12
# BB#1:                                 # %.lr.ph.preheader
	movslq	%edi, %r8
	leaq	-4(%r8), %r14
	movq	%r14, %rcx
	shrq	%rcx
	leaq	1(%rcx), %r10
	cmpq	$2, %r10
	jae	.LBB26_3
# BB#2:
	movl	$3, %ecx
	jmp	.LBB26_11
.LBB26_3:                               # %min.iters.checked
	movl	%r10d, %r9d
	andl	$1, %r9d
	subq	%r9, %r10
	je	.LBB26_4
# BB#5:                                 # %vector.memcheck
	leaq	24(%rdx), %rax
	movabsq	$2305843009213693950, %rbx # imm = 0x1FFFFFFFFFFFFFFE
	andq	%rbx, %r14
	leaq	24(%rsi), %r11
	leaq	40(%rsi,%r14,8), %rbx
	cmpq	%rbx, %rax
	jae	.LBB26_8
# BB#6:                                 # %vector.memcheck
	leaq	40(%rdx,%r14,8), %rbx
	cmpq	%rbx, %r11
	jae	.LBB26_8
# BB#7:
	movl	$3, %ecx
	jmp	.LBB26_11
.LBB26_4:
	movl	$3, %ecx
	jmp	.LBB26_11
.LBB26_8:                               # %vector.body.preheader
	leaq	5(%rcx,%rcx), %rcx
	leaq	(%r9,%r9), %rbx
	subq	%rbx, %rcx
	.p2align	4, 0x90
.LBB26_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%r11), %xmm0
	movupd	16(%r11), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	movupd	(%rax), %xmm4
	movupd	16(%rax), %xmm5
	movapd	%xmm4, %xmm6
	unpcklpd	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0]
	mulpd	%xmm4, %xmm0
	unpckhpd	%xmm5, %xmm4    # xmm4 = xmm4[1],xmm5[1]
	mulpd	%xmm5, %xmm1
	movapd	%xmm0, %xmm5
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	movhlps	%xmm0, %xmm1            # xmm1 = xmm0[1],xmm1[1]
	subpd	%xmm1, %xmm5
	mulpd	%xmm2, %xmm4
	mulpd	%xmm3, %xmm6
	addpd	%xmm4, %xmm6
	movapd	%xmm5, %xmm0
	unpcklpd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0]
	movhlps	%xmm5, %xmm6            # xmm6 = xmm5[1],xmm6[1]
	movups	%xmm6, 16(%rax)
	movupd	%xmm0, (%rax)
	addq	$32, %r11
	addq	$32, %rax
	addq	$-2, %r10
	jne	.LBB26_9
# BB#10:                                # %middle.block
	testq	%r9, %r9
	je	.LBB26_12
	.p2align	4, 0x90
.LBB26_11:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rsi,%rcx,8), %xmm0
	movsd	(%rdx,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movsd	8(%rdx,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	subpd	%xmm2, %xmm0
	addpd	%xmm2, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	%xmm1, (%rdx,%rcx,8)
	addq	$2, %rcx
	cmpq	%r8, %rcx
	jl	.LBB26_11
.LBB26_12:                              # %._crit_edge
	movslq	%edi, %rax
	movsd	8(%rsi,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	8(%rdx,%rax,8), %xmm0
	movsd	%xmm0, 8(%rdx,%rax,8)
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end26:
	.size	mp_mul_cmul, .Lfunc_end26-mp_mul_cmul
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI27_0:
	.quad	4611686018427387904     # double 2
.LCPI27_1:
	.quad	4607182418800017408     # double 1
.LCPI27_3:
	.quad	4602678819172646912     # double 0.5
.LCPI27_4:
	.quad	4372995238176751616     # double 2.2204460492503131E-16
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI27_2:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	mp_mul_d2i
	.p2align	4, 0x90
	.type	mp_mul_d2i,@function
mp_mul_d2i:                             # @mp_mul_d2i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi257:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi258:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi259:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi260:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi261:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi262:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi263:
	.cfi_def_cfa_offset 128
.Lcfi264:
	.cfi_offset %rbx, -56
.Lcfi265:
	.cfi_offset %r12, -48
.Lcfi266:
	.cfi_offset %r13, -40
.Lcfi267:
	.cfi_offset %r14, -32
.Lcfi268:
	.cfi_offset %r15, -24
.Lcfi269:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movq	%rcx, %r14
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%edi, %r15d
	cvtsi2sdl	%edx, %xmm11
	movsd	.LCPI27_0(%rip), %xmm3  # xmm3 = mem[0],zero
	cvtsi2sdl	%esi, %xmm5
	movsd	.LCPI27_1(%rip), %xmm7  # xmm7 = mem[0],zero
	movapd	%xmm7, %xmm10
	divsd	%xmm5, %xmm10
	leal	1(%rdx), %esi
	movslq	%edx, %r13
	movsd	8(%r14,%r13,8), %xmm8   # xmm8 = mem[0],zero
	movapd	.LCPI27_2(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm8, %xmm0
	xorpd	%xmm6, %xmm6
	movapd	%xmm8, %xmm1
	cmpltsd	%xmm6, %xmm1
	movapd	%xmm1, %xmm2
	andnpd	%xmm8, %xmm2
	andpd	%xmm0, %xmm1
	orpd	%xmm2, %xmm1
	movsd	.LCPI27_3(%rip), %xmm4  # xmm4 = mem[0],zero
	movapd	%xmm11, %xmm0
	mulsd	%xmm4, %xmm0
	mulsd	%xmm1, %xmm0
	movapd	%xmm1, %xmm9
	addsd	%xmm4, %xmm9
	xorl	%r12d, %r12d
	ucomisd	%xmm5, %xmm9
	setae	%r12b
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%r14,%r13,8)
	movsd	%xmm0, 8(%r14)
	leal	1(%r12,%rdx), %ebp
	cmpl	%r15d, %ebp
	movl	%r15d, %eax
	jge	.LBB27_2
# BB#1:                                 # %.lr.ph156.preheader
	leal	1(%r15), %eax
	cltq
	movl	$-3, %ecx
	subl	%edx, %ecx
	subl	%r12d, %ecx
	movl	%r15d, %edx
	notl	%edx
	cmpl	%edx, %ecx
	cmovgel	%ecx, %edx
	leal	1(%rdx,%r15), %ecx
	subq	%rcx, %rax
	leaq	(%rbx,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	movl	%esi, 4(%rsp)           # 4-byte Spill
	xorl	%esi, %esi
	movapd	%xmm8, 48(%rsp)         # 16-byte Spill
	movsd	%xmm5, 24(%rsp)         # 8-byte Spill
	movapd	%xmm9, 32(%rsp)         # 16-byte Spill
	movsd	%xmm10, 16(%rsp)        # 8-byte Spill
	movsd	%xmm11, 8(%rsp)         # 8-byte Spill
	callq	memset
	movsd	8(%rsp), %xmm11         # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movl	4(%rsp), %esi           # 4-byte Reload
	movsd	.LCPI27_1(%rip), %xmm7  # xmm7 = mem[0],zero
	xorpd	%xmm6, %xmm6
	movsd	.LCPI27_0(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	16(%rsp), %xmm10        # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movapd	32(%rsp), %xmm9         # 16-byte Reload
	movsd	24(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	.LCPI27_3(%rip), %xmm4  # xmm4 = mem[0],zero
	movapd	48(%rsp), %xmm8         # 16-byte Reload
	movl	%ebp, %eax
.LBB27_2:                               # %.loopexit
	divsd	%xmm11, %xmm3
	movapd	%xmm10, %xmm0
	mulsd	%xmm0, %xmm0
	xorl	%edx, %edx
	ucomisd	%xmm5, %xmm9
	setae	%dl
	movl	%eax, %ecx
	subl	%edx, %ecx
	incl	%ecx
	cmpl	%esi, %ecx
	jg	.LBB27_6
# BB#3:                                 # %.lr.ph149.preheader
	movslq	%ecx, %rcx
	decq	%rcx
	xorpd	%xmm6, %xmm6
	movsd	.LCPI27_4(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB27_5:                               # %.lr.ph149
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm6, %xmm2
	movapd	%xmm7, %xmm6
	movapd	%xmm10, %xmm7
	mulsd	%xmm6, %xmm7
	mulsd	8(%r14,%rcx,8), %xmm6
	addsd	%xmm2, %xmm6
	ucomisd	%xmm7, %xmm1
	ja	.LBB27_6
# BB#4:                                 #   in Loop: Header=BB27_5 Depth=1
	incq	%rcx
	cmpq	%r13, %rcx
	jle	.LBB27_5
.LBB27_6:                               # %._crit_edge150
	mulsd	%xmm3, %xmm6
	addsd	%xmm4, %xmm6
	mulsd	%xmm0, %xmm6
	cvttsd2si	%xmm6, %ecx
	decl	%ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	subsd	%xmm1, %xmm6
	mulsd	%xmm5, %xmm6
	addsd	%xmm4, %xmm6
	cvttsd2si	%xmm6, %esi
	cmpl	$2, %eax
	jl	.LBB27_7
# BB#8:                                 # %.lr.ph142.preheader
	xorl	%edi, %edi
	ucomisd	%xmm5, %xmm9
	setae	%dil
	cltq
	shlq	$3, %rdi
	movq	%r14, %rdx
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB27_9:                               # %.lr.ph142
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm3, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	addsd	%xmm1, %xmm2
	addsd	%xmm4, %xmm2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %edi
	decl	%edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	subsd	%xmm1, %xmm2
	mulsd	%xmm5, %xmm2
	cvttsd2si	%xmm2, %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	subsd	%xmm1, %xmm2
	mulsd	%xmm5, %xmm2
	cvttsd2si	%xmm2, %esi
	movl	%esi, 4(%rbx,%rax,4)
	movl	%ecx, %esi
	addl	%ebp, %esi
	decq	%rax
	cmpq	$1, %rax
	movl	%edi, %ecx
	jg	.LBB27_9
	jmp	.LBB27_10
.LBB27_7:
	movl	%ecx, %edi
.LBB27_10:                              # %._crit_edge143
	ucomisd	%xmm5, %xmm9
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm5, %xmm0
	addsd	%xmm1, %xmm0
	addsd	.LCPI27_3(%rip), %xmm0
	jae	.LBB27_12
# BB#11:
	mulsd	8(%r14), %xmm3
	addsd	%xmm3, %xmm0
.LBB27_12:
	mulsd	%xmm0, %xmm10
	cvttsd2si	%xmm10, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm5, %xmm1
	subsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %ecx
	movl	%ecx, 8(%rbx)
	testl	%eax, %eax
	jle	.LBB27_17
# BB#13:
	cmpl	$2, %r15d
	jl	.LBB27_16
# BB#14:                                # %.lr.ph.preheader
	incl	%r15d
	movslq	%r15d, %rcx
	.p2align	4, 0x90
.LBB27_15:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbx,%rcx,4), %edx
	movl	%edx, (%rbx,%rcx,4)
	decq	%rcx
	cmpq	$2, %rcx
	jg	.LBB27_15
.LBB27_16:                              # %._crit_edge
	xorl	%r12d, %r12d
	ucomisd	%xmm5, %xmm9
	setae	%r12b
	movl	%eax, 8(%rbx)
	incl	%r12d
	movl	%eax, %ecx
.LBB27_17:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	addsd	(%r14), %xmm0
	addsd	%xmm4, %xmm0
	cvttsd2si	%xmm0, %eax
	leal	-1(%rax), %edx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	subsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %edx
	leal	-1(%rdx,%rax), %eax
	movl	%eax, 4(%rbx)
	ucomisd	%xmm4, %xmm8
	movl	$1, %eax
	movl	$-1, %edx
	cmoval	%eax, %edx
	movl	%edx, (%rbx)
	testl	%ecx, %ecx
	jne	.LBB27_19
# BB#18:
	movq	$0, (%rbx)
.LBB27_19:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	mp_mul_d2i, .Lfunc_end27-mp_mul_d2i
	.cfi_endproc

	.globl	mp_mul_cmuladd
	.p2align	4, 0x90
	.type	mp_mul_cmuladd,@function
mp_mul_cmuladd:                         # @mp_mul_cmuladd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi270:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi271:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi272:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi273:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi274:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi275:
	.cfi_def_cfa_offset 56
.Lcfi276:
	.cfi_offset %rbx, -56
.Lcfi277:
	.cfi_offset %r12, -48
.Lcfi278:
	.cfi_offset %r13, -40
.Lcfi279:
	.cfi_offset %r14, -32
.Lcfi280:
	.cfi_offset %r15, -24
.Lcfi281:
	.cfi_offset %rbp, -16
	movsd	8(%rsi), %xmm0          # xmm0 = mem[0],zero
	mulsd	8(%rdx), %xmm0
	addsd	8(%rcx), %xmm0
	movsd	%xmm0, 8(%rcx)
	movsd	16(%rsi), %xmm0         # xmm0 = mem[0],zero
	mulsd	16(%rdx), %xmm0
	addsd	16(%rcx), %xmm0
	movsd	%xmm0, 16(%rcx)
	cmpl	$4, %edi
	jl	.LBB28_11
# BB#1:                                 # %.lr.ph.preheader
	movslq	%edi, %r8
	leaq	-4(%r8), %rax
	movq	%rax, %r15
	shrq	%r15
	leaq	1(%r15), %r9
	cmpq	$2, %r9
	jae	.LBB28_3
# BB#2:
	movl	$3, %eax
	jmp	.LBB28_10
.LBB28_3:                               # %min.iters.checked
	movl	%r9d, %ebp
	andl	$1, %ebp
	subq	%rbp, %r9
	je	.LBB28_4
# BB#5:                                 # %vector.memcheck
	movq	%rbp, -8(%rsp)          # 8-byte Spill
	leaq	24(%rcx), %r11
	movabsq	$2305843009213693950, %rbx # imm = 0x1FFFFFFFFFFFFFFE
	andq	%rbx, %rax
	leaq	40(%rcx,%rax,8), %r12
	leaq	24(%rsi), %r14
	leaq	40(%rsi,%rax,8), %rbp
	leaq	24(%rdx), %rbx
	leaq	40(%rdx,%rax,8), %r13
	cmpq	%rbp, %r11
	sbbb	%al, %al
	cmpq	%r12, %r14
	sbbb	%r10b, %r10b
	andb	%al, %r10b
	cmpq	%r13, %r11
	sbbb	%bpl, %bpl
	cmpq	%r12, %rbx
	sbbb	%r12b, %r12b
	movl	$3, %eax
	testb	$1, %r10b
	jne	.LBB28_10
# BB#6:                                 # %vector.memcheck
	andb	%r12b, %bpl
	andb	$1, %bpl
	jne	.LBB28_10
# BB#7:                                 # %vector.body.preheader
	leaq	5(%r15,%r15), %rax
	movq	-8(%rsp), %r10          # 8-byte Reload
	leaq	(%r10,%r10), %rbp
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB28_8:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%r14), %xmm1
	movupd	16(%r14), %xmm2
	movapd	%xmm1, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movapd	%xmm1, %xmm4
	unpckhpd	%xmm2, %xmm4    # xmm4 = xmm4[1],xmm2[1]
	movupd	(%rbx), %xmm5
	movupd	16(%rbx), %xmm6
	movapd	%xmm5, %xmm0
	unpcklpd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0]
	mulpd	%xmm5, %xmm1
	unpckhpd	%xmm6, %xmm5    # xmm5 = xmm5[1],xmm6[1]
	mulpd	%xmm6, %xmm2
	movapd	%xmm1, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	movhlps	%xmm1, %xmm2            # xmm2 = xmm1[1],xmm2[1]
	subpd	%xmm2, %xmm6
	movupd	(%r11), %xmm1
	movupd	16(%r11), %xmm2
	movapd	%xmm1, %xmm7
	unpcklpd	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0]
	unpckhpd	%xmm2, %xmm1    # xmm1 = xmm1[1],xmm2[1]
	addpd	%xmm6, %xmm7
	mulpd	%xmm3, %xmm5
	mulpd	%xmm4, %xmm0
	addpd	%xmm5, %xmm0
	addpd	%xmm1, %xmm0
	movapd	%xmm7, %xmm1
	unpcklpd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movhlps	%xmm7, %xmm0            # xmm0 = xmm7[1],xmm0[1]
	movups	%xmm0, 16(%r11)
	movupd	%xmm1, (%r11)
	addq	$32, %r14
	addq	$32, %r11
	addq	$32, %rbx
	addq	$-2, %r9
	jne	.LBB28_8
# BB#9:                                 # %middle.block
	testq	%r10, %r10
	jne	.LBB28_10
	jmp	.LBB28_11
.LBB28_4:
	movl	$3, %eax
	.p2align	4, 0x90
.LBB28_10:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rsi,%rax,8), %xmm0
	movsd	(%rdx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movsd	8(%rdx,%rax,8), %xmm2   # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	subpd	%xmm2, %xmm0
	addpd	%xmm2, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	(%rcx,%rax,8), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rcx,%rax,8)
	addq	$2, %rax
	cmpq	%r8, %rax
	jl	.LBB28_10
.LBB28_11:                              # %._crit_edge
	movslq	%edi, %rax
	movsd	8(%rsi,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	8(%rdx,%rax,8), %xmm0
	addsd	8(%rcx,%rax,8), %xmm0
	movsd	%xmm0, 8(%rcx,%rax,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	mp_mul_cmuladd, .Lfunc_end28-mp_mul_cmuladd
	.cfi_endproc

	.globl	mp_mulh
	.p2align	4, 0x90
	.type	mp_mulh,@function
mp_mulh:                                # @mp_mulh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi282:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi283:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi284:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi285:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi286:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi287:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi288:
	.cfi_def_cfa_offset 128
.Lcfi289:
	.cfi_offset %rbx, -56
.Lcfi290:
	.cfi_offset %r12, -48
.Lcfi291:
	.cfi_offset %r13, -40
.Lcfi292:
	.cfi_offset %r14, -32
.Lcfi293:
	.cfi_offset %r15, -24
.Lcfi294:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%rdx, %r13
	movq	152(%rsp), %rbp
	movq	144(%rsp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	136(%rsp), %rbx
	movq	128(%rsp), %r15
	testl	%edi, %edi
	movl	%edi, 60(%rsp)          # 4-byte Spill
	jle	.LBB29_1
# BB#2:
	movl	8(%r13), %r11d
	movl	%r9d, %eax
	sarl	%eax
	leal	1(%rax), %r14d
	cmpl	%edi, %eax
	cmovgel	%edi, %r14d
	jmp	.LBB29_3
.LBB29_1:
	xorl	%r14d, %r14d
	xorl	%r11d, %r11d
.LBB29_3:
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	(%r13), %eax
	imull	%r11d, %eax
	cvtsi2sdl	%eax, %xmm0
	movslq	%r9d, %r12
	movsd	%xmm0, 8(%r15,%r12,8)
	cmpl	%r9d, %r14d
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	jge	.LBB29_5
# BB#4:                                 # %.lr.ph61.preheader.i
	leal	-1(%r9), %eax
	subl	%r14d, %eax
	movq	%r12, %rcx
	subq	%rax, %rcx
	leaq	(%r15,%rcx,8), %rdi
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movl	%r11d, 16(%rsp)         # 4-byte Spill
	callq	memset
	movl	16(%rsp), %r11d         # 4-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
.LBB29_5:                               # %._crit_edge62.i
	cmpl	$2, %r14d
	jl	.LBB29_10
# BB#6:
	movq	%rbp, %r10
	xorl	%ecx, %ecx
	cmpl	$2, %r14d
	je	.LBB29_9
# BB#7:                                 # %.lr.ph.preheader.i
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%esi, %eax
	sarl	%eax
	incl	%r14d
	movslq	%r14d, %rdx
	xorl	%ecx, %ecx
	movl	$-1, %r8d
	.p2align	4, 0x90
.LBB29_8:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rdx,4), %edi
	subl	%ecx, %edi
	cmpl	%eax, %edi
	movl	$0, %ecx
	cmovgel	%r8d, %ecx
	movl	$0, %ebp
	cmovgel	%esi, %ebp
	subl	%ebp, %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	movsd	%xmm0, -8(%r15,%rdx,8)
	decq	%rdx
	cmpq	$3, %rdx
	jg	.LBB29_8
.LBB29_9:                               # %._crit_edge.i
	movl	12(%r13), %eax
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 16(%r15)
	movq	%r10, %rbp
.LBB29_10:                              # %mp_mul_i2d.exit
	leaq	1(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r11d, %xmm0
	leaq	8(%r15), %rdx
	movsd	%xmm0, 8(%r15)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	4(%r13), %xmm0
	movsd	%xmm0, (%r15)
	movl	$1, %esi
	movl	%r9d, %edi
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rbp, %r8
	movq	%r9, %r14
	callq	rdft
	movl	60(%rsp), %r13d         # 4-byte Reload
	testl	%r13d, %r13d
	jle	.LBB29_11
# BB#12:
	movq	32(%rsp), %r10          # 8-byte Reload
	movl	8(%r10), %r11d
	movl	%r14d, %eax
	sarl	%eax
	leal	1(%rax), %ebp
	cmpl	%r13d, %eax
	cmovgel	%r13d, %ebp
	movq	%r14, %rdi
	jmp	.LBB29_13
.LBB29_11:
	xorl	%ebp, %ebp
	xorl	%r11d, %r11d
	movq	%r14, %rdi
	movq	32(%rsp), %r10          # 8-byte Reload
.LBB29_13:
	movl	(%r10), %eax
	imull	%r11d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	movsd	%xmm0, (%rbx,%rax,8)
	cmpl	%edi, %ebp
	jge	.LBB29_15
# BB#14:                                # %.lr.ph61.preheader.i34
	leal	-1(%rdi), %eax
	subl	%ebp, %eax
	movq	%r12, %rcx
	subq	%rax, %rcx
	leaq	(%rbx,%rcx,8), %rdi
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movl	%r11d, %r14d
	callq	memset
	movl	%r14d, %r11d
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
.LBB29_15:                              # %._crit_edge62.i35
	cmpl	$1, %ebp
	jle	.LBB29_20
# BB#16:
	xorl	%ecx, %ecx
	cmpl	$2, %ebp
	movl	12(%rsp), %r9d          # 4-byte Reload
	je	.LBB29_19
# BB#17:                                # %.lr.ph.preheader.i36
	movl	%r9d, %eax
	shrl	$31, %eax
	addl	%r9d, %eax
	sarl	%eax
	incl	%ebp
	movslq	%ebp, %rdx
	xorl	%ecx, %ecx
	movl	$-1, %r8d
	.p2align	4, 0x90
.LBB29_18:                              # %.lr.ph.i40
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r10,%rdx,4), %esi
	subl	%ecx, %esi
	cmpl	%eax, %esi
	movl	$0, %ecx
	cmovgel	%r8d, %ecx
	movl	$0, %ebp
	cmovgel	%r9d, %ebp
	subl	%ebp, %esi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	movsd	%xmm0, -8(%rbx,%rdx,8)
	decq	%rdx
	cmpq	$3, %rdx
	jg	.LBB29_18
.LBB29_19:                              # %._crit_edge.i42
	movl	12(%r10), %eax
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 16(%rbx)
.LBB29_20:                              # %mp_mul_i2d.exit43
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r11d, %xmm0
	leaq	8(%rbx), %r14
	movsd	%xmm0, 8(%rbx)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	4(%r10), %xmm0
	movsd	%xmm0, (%rbx)
	movl	$1, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %r8
	callq	rdft
	movq	40(%rsp), %rdi          # 8-byte Reload
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rbx), %xmm0
	movsd	%xmm0, (%rbx)
	movsd	8(%r15), %xmm0          # xmm0 = mem[0],zero
	mulsd	8(%rbx), %xmm0
	movsd	%xmm0, 8(%rbx)
	movsd	16(%r15), %xmm0         # xmm0 = mem[0],zero
	mulsd	16(%rbx), %xmm0
	movsd	%xmm0, 16(%rbx)
	cmpl	$4, %edi
	jl	.LBB29_32
# BB#21:                                # %.lr.ph.preheader.i44
	leaq	-4(%r12), %rdx
	movq	%rdx, %rax
	shrq	%rax
	leaq	1(%rax), %r9
	cmpq	$2, %r9
	jae	.LBB29_23
# BB#22:
	movl	$3, %eax
	jmp	.LBB29_31
.LBB29_23:                              # %min.iters.checked
	movl	%r9d, %r8d
	andl	$1, %r8d
	subq	%r8, %r9
	je	.LBB29_24
# BB#25:                                # %vector.memcheck
	leaq	24(%rbx), %rsi
	movabsq	$2305843009213693950, %rcx # imm = 0x1FFFFFFFFFFFFFFE
	andq	%rcx, %rdx
	leaq	24(%r15), %rcx
	leaq	40(%r15,%rdx,8), %rbp
	cmpq	%rbp, %rsi
	jae	.LBB29_28
# BB#26:                                # %vector.memcheck
	leaq	40(%rbx,%rdx,8), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB29_28
# BB#27:
	movl	$3, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB29_31
.LBB29_24:
	movl	$3, %eax
	jmp	.LBB29_31
.LBB29_28:                              # %vector.body.preheader
	leaq	5(%rax,%rax), %rax
	leaq	(%r8,%r8), %rdx
	subq	%rdx, %rax
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB29_29:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rcx), %xmm0
	movupd	16(%rcx), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	movupd	(%rsi), %xmm4
	movupd	16(%rsi), %xmm5
	movapd	%xmm4, %xmm6
	unpcklpd	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0]
	mulpd	%xmm4, %xmm0
	unpckhpd	%xmm5, %xmm4    # xmm4 = xmm4[1],xmm5[1]
	mulpd	%xmm5, %xmm1
	movapd	%xmm0, %xmm5
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	movhlps	%xmm0, %xmm1            # xmm1 = xmm0[1],xmm1[1]
	subpd	%xmm1, %xmm5
	mulpd	%xmm2, %xmm4
	mulpd	%xmm3, %xmm6
	addpd	%xmm4, %xmm6
	movapd	%xmm5, %xmm0
	unpcklpd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0]
	movhlps	%xmm5, %xmm6            # xmm6 = xmm5[1],xmm6[1]
	movups	%xmm6, 16(%rsi)
	movupd	%xmm0, (%rsi)
	addq	$32, %rcx
	addq	$32, %rsi
	addq	$-2, %r9
	jne	.LBB29_29
# BB#30:                                # %middle.block
	testq	%r8, %r8
	je	.LBB29_32
	.p2align	4, 0x90
.LBB29_31:                              # %.lr.ph.i47
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%r15,%rax,8), %xmm0
	movsd	(%rbx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movsd	8(%rbx,%rax,8), %xmm2   # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	subpd	%xmm2, %xmm0
	addpd	%xmm2, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	%xmm1, (%rbx,%rax,8)
	addq	$2, %rax
	cmpq	%r12, %rax
	jl	.LBB29_31
.LBB29_32:                              # %mp_mul_cmul.exit
	movq	16(%rsp), %rax          # 8-byte Reload
	movsd	(%r15,%rax,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rbx,%rax,8), %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	movl	$-1, %esi
	movq	%r14, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, %r8
	movq	%rdi, %rbp
	callq	rdft
	movl	%r13d, %edi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movq	64(%rsp), %r8           # 8-byte Reload
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	mp_mul_d2i              # TAILCALL
.Lfunc_end29:
	.size	mp_mulh, .Lfunc_end29-mp_mulh
	.cfi_endproc

	.globl	mp_mulh_use_in1fft
	.p2align	4, 0x90
	.type	mp_mulh_use_in1fft,@function
mp_mulh_use_in1fft:                     # @mp_mulh_use_in1fft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi295:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi296:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi297:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi298:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi299:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi300:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi301:
	.cfi_def_cfa_offset 80
.Lcfi302:
	.cfi_offset %rbx, -56
.Lcfi303:
	.cfi_offset %r12, -48
.Lcfi304:
	.cfi_offset %r13, -40
.Lcfi305:
	.cfi_offset %r14, -32
.Lcfi306:
	.cfi_offset %r15, -24
.Lcfi307:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rdx, %r15
	movl	%edi, %r12d
	movq	104(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	88(%rsp), %rbp
	movl	80(%rsp), %r14d
	cmpl	%r12d, %ecx
	jge	.LBB30_4
# BB#1:                                 # %.lr.ph.preheader
	movslq	%ecx, %rcx
	movslq	%r12d, %rax
	.p2align	4, 0x90
.LBB30_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 8(%r8,%rcx,4)
	jne	.LBB30_4
# BB#3:                                 #   in Loop: Header=BB30_2 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB30_2
.LBB30_4:                               # %._crit_edge
	movl	%r12d, %edi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movl	%r14d, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rbp, %r9
	callq	mp_mul_i2d
	leaq	8(%rbp), %r13
	movl	$1, %esi
	movl	%r14d, %edi
	movq	%r13, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rbp), %xmm0
	movsd	%xmm0, (%rbp)
	movsd	8(%r15), %xmm0          # xmm0 = mem[0],zero
	mulsd	8(%rbp), %xmm0
	movsd	%xmm0, 8(%rbp)
	movsd	16(%r15), %xmm0         # xmm0 = mem[0],zero
	mulsd	16(%rbp), %xmm0
	movsd	%xmm0, 16(%rbp)
	cmpl	$4, %r14d
	jl	.LBB30_16
# BB#5:                                 # %.lr.ph.preheader.i
	movslq	%r14d, %rax
	leaq	-4(%rax), %r9
	movq	%r9, %rcx
	shrq	%rcx
	leaq	1(%rcx), %rdx
	cmpq	$2, %rdx
	jae	.LBB30_7
# BB#6:
	movl	$3, %ecx
	jmp	.LBB30_15
.LBB30_7:                               # %min.iters.checked
	movl	%edx, %r8d
	andl	$1, %r8d
	subq	%r8, %rdx
	je	.LBB30_8
# BB#9:                                 # %vector.memcheck
	movq	%rbx, %r10
	leaq	24(%rbp), %rdi
	movabsq	$2305843009213693950, %rsi # imm = 0x1FFFFFFFFFFFFFFE
	andq	%rsi, %r9
	leaq	24(%r15), %rsi
	leaq	40(%r15,%r9,8), %rbx
	cmpq	%rbx, %rdi
	jae	.LBB30_12
# BB#10:                                # %vector.memcheck
	leaq	40(%rbp,%r9,8), %rbx
	cmpq	%rbx, %rsi
	jae	.LBB30_12
# BB#11:
	movl	$3, %ecx
	movq	%r10, %rbx
	jmp	.LBB30_15
.LBB30_8:
	movl	$3, %ecx
	jmp	.LBB30_15
.LBB30_12:                              # %vector.body.preheader
	leaq	5(%rcx,%rcx), %rcx
	leaq	(%r8,%r8), %rbx
	subq	%rbx, %rcx
	.p2align	4, 0x90
.LBB30_13:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rsi), %xmm0
	movupd	16(%rsi), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	movupd	(%rdi), %xmm4
	movupd	16(%rdi), %xmm5
	movapd	%xmm4, %xmm6
	unpcklpd	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0]
	mulpd	%xmm4, %xmm0
	unpckhpd	%xmm5, %xmm4    # xmm4 = xmm4[1],xmm5[1]
	mulpd	%xmm5, %xmm1
	movapd	%xmm0, %xmm5
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	movhlps	%xmm0, %xmm1            # xmm1 = xmm0[1],xmm1[1]
	subpd	%xmm1, %xmm5
	mulpd	%xmm2, %xmm4
	mulpd	%xmm3, %xmm6
	addpd	%xmm4, %xmm6
	movapd	%xmm5, %xmm0
	unpcklpd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0]
	movhlps	%xmm5, %xmm6            # xmm6 = xmm5[1],xmm6[1]
	movups	%xmm6, 16(%rdi)
	movupd	%xmm0, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-2, %rdx
	jne	.LBB30_13
# BB#14:                                # %middle.block
	testq	%r8, %r8
	movq	%r10, %rbx
	je	.LBB30_16
	.p2align	4, 0x90
.LBB30_15:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%r15,%rcx,8), %xmm0
	movsd	(%rbp,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movsd	8(%rbp,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	subpd	%xmm2, %xmm0
	addpd	%xmm2, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	%xmm1, (%rbp,%rcx,8)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jl	.LBB30_15
.LBB30_16:                              # %mp_mul_cmul.exit
	movslq	%r14d, %rax
	movsd	8(%r15,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	8(%rbp,%rax,8), %xmm0
	movsd	%xmm0, 8(%rbp,%rax,8)
	movl	$-1, %esi
	movl	%r14d, %edi
	movq	%r13, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	rdft
	movl	%r12d, %edi
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	%r14d, %edx
	movq	%rbp, %rcx
	movq	%rbx, %r8
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	mp_mul_d2i              # TAILCALL
.Lfunc_end30:
	.size	mp_mulh_use_in1fft, .Lfunc_end30-mp_mulh_use_in1fft
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI31_0:
	.quad	4611686018427387904     # double 2
	.text
	.globl	mp_squh_use_in1fft
	.p2align	4, 0x90
	.type	mp_squh_use_in1fft,@function
mp_squh_use_in1fft:                     # @mp_squh_use_in1fft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi308:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi309:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi310:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi311:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi312:
	.cfi_def_cfa_offset 48
.Lcfi313:
	.cfi_offset %rbx, -48
.Lcfi314:
	.cfi_offset %r12, -40
.Lcfi315:
	.cfi_offset %r14, -32
.Lcfi316:
	.cfi_offset %r15, -24
.Lcfi317:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movl	%esi, %r15d
	movl	%edi, %r12d
	movq	48(%rsp), %r8
	leaq	8(%rbx), %rdx
	movsd	.LCPI31_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movhpd	8(%rbx), %xmm0          # xmm0 = xmm0[0],mem[0]
	movupd	(%rbx), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	%xmm1, (%rbx)
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rbx)
	cmpl	$4, %ebp
	jl	.LBB31_9
# BB#1:                                 # %.lr.ph.preheader.i
	movslq	%ebp, %rax
	leaq	-4(%rax), %rcx
	shrq	%rcx
	leaq	1(%rcx), %rsi
	cmpq	$2, %rsi
	jae	.LBB31_3
# BB#2:
	movl	$3, %ecx
	jmp	.LBB31_8
.LBB31_3:                               # %min.iters.checked
	movl	%esi, %r10d
	andl	$1, %r10d
	subq	%r10, %rsi
	je	.LBB31_4
# BB#5:                                 # %vector.body.preheader
	leaq	5(%rcx,%rcx), %rcx
	leaq	(%r10,%r10), %rdi
	subq	%rdi, %rcx
	leaq	24(%rbx), %rdi
	.p2align	4, 0x90
.LBB31_6:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rdi), %xmm0
	movupd	16(%rdi), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	mulpd	%xmm1, %xmm1
	mulpd	%xmm0, %xmm0
	movapd	%xmm0, %xmm4
	unpcklpd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0]
	unpckhpd	%xmm1, %xmm0    # xmm0 = xmm0[1],xmm1[1]
	subpd	%xmm0, %xmm4
	addpd	%xmm2, %xmm2
	mulpd	%xmm3, %xmm2
	movapd	%xmm4, %xmm0
	unpcklpd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0]
	movhlps	%xmm4, %xmm2            # xmm2 = xmm4[1],xmm2[1]
	movups	%xmm2, 16(%rdi)
	movupd	%xmm0, (%rdi)
	addq	$32, %rdi
	addq	$-2, %rsi
	jne	.LBB31_6
# BB#7:                                 # %middle.block
	testq	%r10, %r10
	jne	.LBB31_8
	jmp	.LBB31_9
.LBB31_4:
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB31_8:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rbx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rbx,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx,%rcx,8)
	movsd	%xmm0, 8(%rbx,%rcx,8)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jl	.LBB31_8
.LBB31_9:                               # %mp_mul_csqu.exit
	movslq	%ebp, %rax
	movsd	8(%rbx,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rbx,%rax,8)
	movl	$-1, %esi
	movl	%ebp, %edi
	movq	%r9, %rcx
	callq	rdft
	movl	%r12d, %edi
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movq	%r14, %r8
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	mp_mul_d2i              # TAILCALL
.Lfunc_end31:
	.size	mp_squh_use_in1fft, .Lfunc_end31-mp_squh_use_in1fft
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI32_0:
	.quad	4372995238176751616     # double 2.2204460492503131E-16
.LCPI32_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	mp_get_nfft_init
	.p2align	4, 0x90
	.type	mp_get_nfft_init,@function
mp_get_nfft_init:                       # @mp_get_nfft_init
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%edi, %xmm0
	movl	$1, %eax
	movsd	.LCPI32_0(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI32_1(%rip), %xmm2  # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB32_1:                               # =>This Inner Loop Header: Depth=1
	addl	%eax, %eax
	cmpl	%esi, %eax
	jge	.LBB32_3
# BB#2:                                 #   in Loop: Header=BB32_1 Depth=1
	mulsd	%xmm0, %xmm0
	movapd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm2
	ja	.LBB32_1
.LBB32_3:                               # %.critedge
	retq
.Lfunc_end32:
	.size	mp_get_nfft_init, .Lfunc_end32-mp_get_nfft_init
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI33_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	mp_inv_init
	.p2align	4, 0x90
	.type	mp_inv_init,@function
mp_inv_init:                            # @mp_inv_init
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	(%rdx), %eax
	movl	%eax, (%rcx)
	xorl	%r8d, %r8d
	subl	4(%rdx), %r8d
	cvtsi2sdl	%esi, %xmm0
	movsd	.LCPI33_0(%rip), %xmm2  # xmm2 = mem[0],zero
	testl	%edi, %edi
	jle	.LBB33_1
# BB#2:                                 # %.lr.ph.preheader.i
	movapd	%xmm2, %xmm1
	divsd	%xmm0, %xmm1
	movslq	%edi, %rax
	incq	%rax
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB33_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm1, %xmm3
	xorps	%xmm4, %xmm4
	cvtsi2sdl	(%rdx,%rax,4), %xmm4
	addsd	%xmm4, %xmm3
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB33_3
	jmp	.LBB33_4
.LBB33_1:
	xorpd	%xmm3, %xmm3
.LBB33_4:                               # %mp_unexp_mp2d.exit
	movapd	%xmm2, %xmm1
	divsd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm2
	jbe	.LBB33_7
	.p2align	4, 0x90
.LBB33_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm0, %xmm1
	decl	%r8d
	ucomisd	%xmm1, %xmm2
	ja	.LBB33_5
.LBB33_7:                               # %._crit_edge
	movl	%r8d, 4(%rcx)
	testl	%edi, %edi
	jle	.LBB33_20
# BB#8:                                 # %.lr.ph.i19
	leal	-1(%rsi), %eax
	movl	%edi, %edx
	testb	$1, %dl
	jne	.LBB33_10
# BB#9:
	xorl	%r8d, %r8d
	cmpl	$1, %edi
	jne	.LBB33_14
	jmp	.LBB33_20
.LBB33_10:
	cvttsd2si	%xmm1, %r8d
	cmpl	%esi, %r8d
	jl	.LBB33_12
# BB#11:
	movapd	%xmm0, %xmm1
.LBB33_12:
	cmovgel	%eax, %r8d
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%r8d, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movl	%r8d, 8(%rcx)
	movl	$1, %r8d
	cmpl	$1, %edi
	je	.LBB33_20
.LBB33_14:                              # %.lr.ph.i19.new
	subq	%r8, %rdx
	leaq	12(%rcx,%r8,4), %rcx
	.p2align	4, 0x90
.LBB33_15:                              # =>This Inner Loop Header: Depth=1
	cvttsd2si	%xmm1, %edi
	cmpl	%esi, %edi
	jl	.LBB33_17
# BB#16:                                #   in Loop: Header=BB33_15 Depth=1
	movapd	%xmm0, %xmm1
.LBB33_17:                              #   in Loop: Header=BB33_15 Depth=1
	cmovgel	%eax, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movl	%edi, -4(%rcx)
	cvttsd2si	%xmm1, %edi
	cmpl	%esi, %edi
	jl	.LBB33_19
# BB#18:                                #   in Loop: Header=BB33_15 Depth=1
	movapd	%xmm0, %xmm1
.LBB33_19:                              #   in Loop: Header=BB33_15 Depth=1
	cmovgel	%eax, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movl	%edi, (%rcx)
	addq	$8, %rcx
	addq	$-2, %rdx
	jne	.LBB33_15
.LBB33_20:                              # %mp_unexp_d2mp.exit
	retq
.Lfunc_end33:
	.size	mp_inv_init, .Lfunc_end33-mp_inv_init
	.cfi_endproc

	.globl	mp_inv_newton
	.p2align	4, 0x90
	.type	mp_inv_newton,@function
mp_inv_newton:                          # @mp_inv_newton
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi318:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi319:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi320:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi321:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi322:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi323:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi324:
	.cfi_def_cfa_offset 80
.Lcfi325:
	.cfi_offset %rbx, -56
.Lcfi326:
	.cfi_offset %r12, -48
.Lcfi327:
	.cfi_offset %r13, -40
.Lcfi328:
	.cfi_offset %r14, -32
.Lcfi329:
	.cfi_offset %r15, -24
.Lcfi330:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movl	%esi, %r14d
	movl	%edi, %r13d
	movq	112(%rsp), %r11
	movq	104(%rsp), %r10
	movl	80(%rsp), %r9d
	movl	%r9d, %ebp
	sarl	%ebp
	leal	1(%rbp), %edx
	movl	%r13d, %eax
	shrl	$31, %eax
	addl	%r13d, %eax
	sarl	%eax
	incl	%eax
	movl	%r13d, %ecx
	subl	%edx, %ecx
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%edx, 8(%rsp)           # 4-byte Spill
	cmpl	%r13d, %edx
	movq	%r12, 16(%rsp)          # 8-byte Spill
	jge	.LBB34_11
# BB#1:
	leal	3(%rbp), %eax
	cmpl	%r13d, %eax
	jg	.LBB34_3
# BB#2:                                 # %.lr.ph38.preheader.i
	movl	%eax, (%rsp)            # 4-byte Spill
	leal	1(%r13), %eax
	cltq
	movl	%r13d, %ecx
	notl	%ecx
	movl	$-4, %edx
	subl	%ebp, %edx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	leal	1(%r13,%rdx), %ecx
	subq	%rcx, %rax
	leaq	(%rbx,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	movq	%r8, %r12
	callq	memset
	movl	(%rsp), %eax            # 4-byte Reload
	movq	112(%rsp), %r11
	movq	104(%rsp), %r10
	movq	%r12, %r8
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	80(%rsp), %r9d
.LBB34_3:                               # %._crit_edge.i
	movslq	%eax, %rcx
	movl	(%rbx,%rcx,4), %eax
	addl	%eax, %eax
	movl	$0, (%rbx,%rcx,4)
	cmpl	%r14d, %eax
	jl	.LBB34_11
# BB#4:
	testl	%ebp, %ebp
	js	.LBB34_9
# BB#5:                                 # %.lr.ph.preheader.i
	addl	$2, %ebp
	movslq	%ebp, %rcx
	.p2align	4, 0x90
.LBB34_6:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %eax
	incl	%eax
	cmpl	%r14d, %eax
	jl	.LBB34_7
# BB#8:                                 #   in Loop: Header=BB34_6 Depth=1
	movl	$0, (%rbx,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB34_6
	jmp	.LBB34_9
.LBB34_7:
	movl	%eax, (%rbx,%rcx,4)
.LBB34_9:                               # %.loopexit.i
	cmpl	%r14d, %eax
	jl	.LBB34_11
# BB#10:
	movl	$1, 8(%rbx)
	incl	4(%rbx)
.LBB34_11:                              # %mp_round.exit
	movl	%r13d, %edi
	movl	%r14d, %esi
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	%rbx, %rdx
	movq	%r12, %rcx
	movq	%r8, %rbp
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	movq	%r11, %rbx
	pushq	%r11
.Lcfi331:
	.cfi_adjust_cfa_offset 8
	movq	%r10, %r12
	pushq	%r10
.Lcfi332:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)
.Lcfi333:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)
.Lcfi334:
	.cfi_adjust_cfa_offset 8
	callq	mp_mulh
	addq	$32, %rsp
.Lcfi335:
	.cfi_adjust_cfa_offset -32
	movq	$1, (%r15)
	movl	$1, 8(%r15)
	leal	1(%r13), %eax
	cmpl	$3, %eax
	jl	.LBB34_13
# BB#12:                                # %.lr.ph.preheader.i68
	leaq	12(%r15), %rdi
	leal	-2(%r13), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB34_13:                              # %mp_load_1.exit
	movl	%r13d, %edi
	movl	%r14d, %esi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movq	%r15, %r8
	callq	mp_sub
	movl	%r13d, %edi
	movl	%r14d, %esi
	movq	88(%rsp), %rax
	movq	%rax, %rdx
	movl	8(%rsp), %ecx           # 4-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	pushq	%rbx
.Lcfi336:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi337:
	.cfi_adjust_cfa_offset 8
	movq	112(%rsp), %rax
	pushq	%rax
.Lcfi338:
	.cfi_adjust_cfa_offset 8
	movl	104(%rsp), %r12d
	pushq	%r12
.Lcfi339:
	.cfi_adjust_cfa_offset 8
	callq	mp_mulh_use_in1fft
	addq	$32, %rsp
.Lcfi340:
	.cfi_adjust_cfa_offset -32
	movl	12(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %edi
	movl	%r14d, %esi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movq	%r15, %r8
	callq	mp_sub
	xorl	%eax, %eax
	subl	4(%r15), %eax
	leal	1(%r12), %ebp
	cmpl	$0, (%r15)
	cmovnel	%eax, %ebp
	movl	$0, %ecx
	movl	%ebx, %edi
	movl	%r14d, %esi
	movq	88(%rsp), %rdx
	movq	%r15, %r8
	movq	%r15, %r9
	pushq	112(%rsp)
.Lcfi341:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)
.Lcfi342:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)
.Lcfi343:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi344:
	.cfi_adjust_cfa_offset 8
	callq	mp_mulh_use_in1fft
	addq	$32, %rsp
.Lcfi345:
	.cfi_adjust_cfa_offset -32
	movl	%r13d, %edi
	movl	%r14d, %esi
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%r15, %rcx
	movq	%rdx, %r8
	callq	mp_add
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end34:
	.size	mp_inv_newton, .Lfunc_end34-mp_inv_newton
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI35_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	mp_sqrt_init
	.p2align	4, 0x90
	.type	mp_sqrt_init,@function
mp_sqrt_init:                           # @mp_sqrt_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi346:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi347:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi348:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi349:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi350:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi351:
	.cfi_def_cfa_offset 64
.Lcfi352:
	.cfi_offset %rbx, -48
.Lcfi353:
	.cfi_offset %r12, -40
.Lcfi354:
	.cfi_offset %r14, -32
.Lcfi355:
	.cfi_offset %r15, -24
.Lcfi356:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movl	%esi, %ebx
	movl	%edi, %r12d
	movl	$1, (%r15)
	movl	$1, (%r14)
	movl	4(%rdx), %eax
	cvtsi2sdl	%ebx, %xmm3
	movsd	.LCPI35_0(%rip), %xmm4  # xmm4 = mem[0],zero
	testl	%r12d, %r12d
	jle	.LBB35_1
# BB#2:                                 # %.lr.ph.preheader.i
	movapd	%xmm4, %xmm0
	divsd	%xmm3, %xmm0
	movslq	%r12d, %rcx
	incq	%rcx
	xorpd	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB35_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm0, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2sdl	(%rdx,%rcx,4), %xmm2
	addsd	%xmm2, %xmm1
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB35_3
	jmp	.LBB35_4
.LBB35_1:
	xorpd	%xmm1, %xmm1
.LBB35_4:                               # %mp_unexp_mp2d.exit
	movl	%eax, %ecx
	andl	$1, %ecx
	je	.LBB35_6
# BB#5:                                 # %mp_unexp_mp2d.exit
	mulsd	%xmm3, %xmm1
.LBB35_6:                               # %mp_unexp_mp2d.exit
	subl	%ecx, %eax
	movl	%eax, %ebp
	shrl	$31, %ebp
	addl	%eax, %ebp
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB35_8
# BB#7:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	.LCPI35_0(%rip), %xmm4  # xmm4 = mem[0],zero
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB35_8:                               # %mp_unexp_mp2d.exit.split
	sarl	%ebp
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm4
	seta	%al
	jbe	.LBB35_10
# BB#9:
	mulsd	%xmm3, %xmm0
.LBB35_10:                              # %mp_unexp_mp2d.exit.split
	subl	%eax, %ebp
	movl	%ebp, 4(%r15)
	testl	%r12d, %r12d
	jle	.LBB35_23
# BB#11:                                # %.lr.ph.i43
	leal	-1(%rbx), %eax
	movl	%r12d, %ecx
	testb	$1, %cl
	jne	.LBB35_13
# BB#12:
	xorl	%edx, %edx
	movapd	%xmm0, %xmm1
	cmpl	$1, %r12d
	jne	.LBB35_17
	jmp	.LBB35_23
.LBB35_13:
	cvttsd2si	%xmm0, %edx
	cmpl	%ebx, %edx
	movapd	%xmm0, %xmm1
	jl	.LBB35_15
# BB#14:
	movapd	%xmm3, %xmm1
.LBB35_15:
	cmovgel	%eax, %edx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movl	%edx, 8(%r15)
	movl	$1, %edx
	cmpl	$1, %r12d
	je	.LBB35_23
.LBB35_17:                              # %.lr.ph.i43.new
	subq	%rdx, %rcx
	leaq	12(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB35_18:                              # =>This Inner Loop Header: Depth=1
	cvttsd2si	%xmm1, %esi
	cmpl	%ebx, %esi
	jl	.LBB35_20
# BB#19:                                #   in Loop: Header=BB35_18 Depth=1
	movapd	%xmm3, %xmm1
.LBB35_20:                              #   in Loop: Header=BB35_18 Depth=1
	cmovgel	%eax, %esi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movl	%esi, -4(%rdx)
	cvttsd2si	%xmm1, %esi
	cmpl	%ebx, %esi
	jl	.LBB35_22
# BB#21:                                #   in Loop: Header=BB35_18 Depth=1
	movapd	%xmm3, %xmm1
.LBB35_22:                              #   in Loop: Header=BB35_18 Depth=1
	cmovgel	%eax, %esi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movl	%esi, (%rdx)
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB35_18
.LBB35_23:                              # %mp_unexp_d2mp.exit50
	negl	%ebp
	movapd	%xmm4, %xmm1
	divsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm4
	jbe	.LBB35_26
	.p2align	4, 0x90
.LBB35_24:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm3, %xmm1
	decl	%ebp
	ucomisd	%xmm1, %xmm4
	ja	.LBB35_24
.LBB35_26:                              # %._crit_edge
	movl	%ebp, 4(%r14)
	testl	%r12d, %r12d
	jle	.LBB35_39
# BB#27:                                # %.lr.ph.i39
	leal	-1(%rbx), %eax
	movl	%r12d, %ecx
	testb	$1, %cl
	jne	.LBB35_29
# BB#28:
	xorl	%edx, %edx
	cmpl	$1, %r12d
	jne	.LBB35_33
	jmp	.LBB35_39
.LBB35_29:
	cvttsd2si	%xmm1, %edx
	cmpl	%ebx, %edx
	jl	.LBB35_31
# BB#30:
	movapd	%xmm3, %xmm1
.LBB35_31:
	cmovgel	%eax, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	subsd	%xmm0, %xmm1
	mulsd	%xmm3, %xmm1
	movl	%edx, 8(%r14)
	movl	$1, %edx
	cmpl	$1, %r12d
	je	.LBB35_39
.LBB35_33:                              # %.lr.ph.i39.new
	subq	%rdx, %rcx
	leaq	12(%r14,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB35_34:                              # =>This Inner Loop Header: Depth=1
	cvttsd2si	%xmm1, %esi
	cmpl	%ebx, %esi
	jl	.LBB35_36
# BB#35:                                #   in Loop: Header=BB35_34 Depth=1
	movapd	%xmm3, %xmm1
.LBB35_36:                              #   in Loop: Header=BB35_34 Depth=1
	cmovgel	%eax, %esi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	subsd	%xmm0, %xmm1
	mulsd	%xmm3, %xmm1
	movl	%esi, -4(%rdx)
	cvttsd2si	%xmm1, %esi
	cmpl	%ebx, %esi
	jl	.LBB35_38
# BB#37:                                #   in Loop: Header=BB35_34 Depth=1
	movapd	%xmm3, %xmm1
.LBB35_38:                              #   in Loop: Header=BB35_34 Depth=1
	cmovgel	%eax, %esi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	subsd	%xmm0, %xmm1
	mulsd	%xmm3, %xmm1
	movl	%esi, (%rdx)
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB35_34
.LBB35_39:                              # %mp_unexp_d2mp.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end35:
	.size	mp_sqrt_init, .Lfunc_end35-mp_sqrt_init
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI36_0:
	.quad	4611686018427387904     # double 2
	.text
	.globl	mp_sqrt_newton
	.p2align	4, 0x90
	.type	mp_sqrt_newton,@function
mp_sqrt_newton:                         # @mp_sqrt_newton
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi357:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi358:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi359:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi360:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi361:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi362:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi363:
	.cfi_def_cfa_offset 128
.Lcfi364:
	.cfi_offset %rbx, -56
.Lcfi365:
	.cfi_offset %r12, -48
.Lcfi366:
	.cfi_offset %r13, -40
.Lcfi367:
	.cfi_offset %r14, -32
.Lcfi368:
	.cfi_offset %r15, -24
.Lcfi369:
	.cfi_offset %rbp, -16
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movq	%r8, %r13
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	%esi, %r14d
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	168(%rsp), %r12
	movl	128(%rsp), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	sarl	%eax
	leal	1(%rax), %ecx
	cmpl	$1, %eax
	movl	$2, %ebp
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cmovgl	%eax, %ebp
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	incl	%eax
	movq	%rdi, 48(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	subl	%ecx, %edi
	cmpl	%edi, %eax
	cmovgel	%eax, %edi
	movl	%ebp, %ebx
	shrl	%ebx
	leal	1(%rbx), %eax
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	cmpl	%edi, %eax
	jge	.LBB36_9
# BB#1:
	leal	3(%rbx), %r15d
	cmpl	16(%rsp), %r15d         # 4-byte Folded Reload
	jg	.LBB36_3
# BB#2:                                 # %.lr.ph38.preheader.i
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rsi
	leal	1(%rsi), %eax
	cltq
	movl	%esi, %ecx
	notl	%ecx
	movl	$-4, %edx
	subl	%ebx, %edx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	leal	1(%rsi,%rdx), %ecx
	subq	%rcx, %rax
	leaq	(%r13,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB36_3:                               # %._crit_edge.i
	movl	%r15d, %eax
	movl	(%r13,%rax,4), %ecx
	addl	%ecx, %ecx
	movl	$0, (%r13,%rax,4)
	cmpl	%r14d, %ecx
	jl	.LBB36_9
# BB#4:
	addl	$2, %ebx
	.p2align	4, 0x90
.LBB36_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rbx,4), %eax
	incl	%eax
	cmpl	%r14d, %eax
	jl	.LBB36_6
# BB#7:                                 #   in Loop: Header=BB36_5 Depth=1
	movl	$0, (%r13,%rbx,4)
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB36_5
# BB#8:
	movl	$1, 8(%r13)
	incl	4(%r13)
	jmp	.LBB36_9
.LBB36_6:                               # %.loopexit.i.thread
	movl	%eax, (%r13,%rbx,4)
.LBB36_9:                               # %mp_round.exit
	movq	160(%rsp), %r10
	movq	152(%rsp), %rbx
	movq	136(%rsp), %r15
	cmpl	%ebp, (%r12)
	jne	.LBB36_10
# BB#11:
	leaq	8(%r15), %rdx
	movsd	.LCPI36_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movhpd	8(%r15), %xmm0          # xmm0 = xmm0[0],mem[0]
	movupd	(%r15), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	%xmm1, (%r15)
	movsd	16(%r15), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%r15)
	cmpl	$4, %ebp
	jl	.LBB36_20
# BB#12:                                # %.lr.ph.preheader.i.i
	movl	%ebp, %eax
	leaq	-4(%rax), %rcx
	shrq	%rcx
	leaq	1(%rcx), %rsi
	cmpq	$2, %rsi
	jae	.LBB36_14
# BB#13:
	movl	$3, %ecx
	jmp	.LBB36_19
.LBB36_10:
	movq	16(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r14d, %esi
	movq	%r13, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ebp, %r8d
	movq	%r15, %r9
	pushq	%r10
.Lcfi370:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi371:
	.cfi_adjust_cfa_offset 8
	callq	mp_squh
	addq	$16, %rsp
.Lcfi372:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB36_21
.LBB36_14:                              # %min.iters.checked
	movl	%esi, %edi
	andl	$1, %edi
	subq	%rdi, %rsi
	je	.LBB36_15
# BB#16:                                # %vector.body.preheader
	movq	%rbx, %r8
	leaq	5(%rcx,%rcx), %rcx
	leaq	(%rdi,%rdi), %rbx
	subq	%rbx, %rcx
	leaq	24(%r15), %rbx
	.p2align	4, 0x90
.LBB36_17:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbx), %xmm0
	movupd	16(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	mulpd	%xmm1, %xmm1
	mulpd	%xmm0, %xmm0
	movapd	%xmm0, %xmm4
	unpcklpd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0]
	unpckhpd	%xmm1, %xmm0    # xmm0 = xmm0[1],xmm1[1]
	subpd	%xmm0, %xmm4
	addpd	%xmm2, %xmm2
	mulpd	%xmm3, %xmm2
	movapd	%xmm4, %xmm0
	unpcklpd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0]
	movhlps	%xmm4, %xmm2            # xmm2 = xmm4[1],xmm2[1]
	movups	%xmm2, 16(%rbx)
	movupd	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-2, %rsi
	jne	.LBB36_17
# BB#18:                                # %middle.block
	testq	%rdi, %rdi
	movq	%r8, %rbx
	jne	.LBB36_19
	jmp	.LBB36_20
.LBB36_15:
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB36_19:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%r15,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%r15,%rcx,8)
	movsd	%xmm0, 8(%r15,%rcx,8)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jl	.LBB36_19
.LBB36_20:                              # %mp_squh_use_in1fft.exit
	movl	%ebp, %eax
	movsd	8(%r15,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%r15,%rax,8)
	movl	$-1, %esi
	movl	%ebp, %edi
	movq	%rbx, %rcx
	movq	%r10, %r8
	callq	rdft
	movq	16(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r14d, %esi
	movl	%ebp, %edx
	movq	%r15, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	callq	mp_mul_d2i
.LBB36_21:
	movq	144(%rsp), %r11
	movq	48(%rsp), %rsi          # 8-byte Reload
	cmpl	%esi, 44(%rsp)          # 4-byte Folded Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	160(%rsp), %r10
	movq	32(%rsp), %rdx          # 8-byte Reload
	jge	.LBB36_32
# BB#22:
	leal	3(%rbp), %ebx
	cmpl	%esi, %ebx
	jg	.LBB36_24
# BB#23:                                # %.lr.ph38.preheader.i109
	leal	1(%rsi), %eax
	cltq
	movl	%esi, %ecx
	notl	%ecx
	movl	$-4, %edx
	subl	%ebp, %edx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	leal	1(%rsi,%rdx), %ecx
	subq	%rcx, %rax
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	movq	%r11, %r12
	callq	memset
	movq	%r12, %r11
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	160(%rsp), %r10
.LBB36_24:                              # %._crit_edge.i111
	movslq	%ebx, %rcx
	movl	(%rdx,%rcx,4), %eax
	addl	%eax, %eax
	movl	$0, (%rdx,%rcx,4)
	cmpl	%r14d, %eax
	jl	.LBB36_32
# BB#25:
	testl	%ebp, %ebp
	js	.LBB36_30
# BB#26:                                # %.lr.ph.preheader.i
	leal	2(%rbp), %eax
	movslq	%eax, %rcx
	.p2align	4, 0x90
.LBB36_27:                              # %.lr.ph.i113
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rcx,4), %eax
	incl	%eax
	cmpl	%r14d, %eax
	jl	.LBB36_28
# BB#29:                                #   in Loop: Header=BB36_27 Depth=1
	movl	$0, (%rdx,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB36_27
	jmp	.LBB36_30
.LBB36_28:
	movl	%eax, (%rdx,%rcx,4)
.LBB36_30:                              # %.loopexit.i115
	cmpl	%r14d, %eax
	jl	.LBB36_32
# BB#31:
	movl	$1, 8(%rdx)
	incl	4(%rdx)
.LBB36_32:                              # %mp_round.exit116
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %edi
	movl	%r14d, %esi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rcx
	movq	%rbx, %r8
	movl	128(%rsp), %ebp
	movl	%ebp, %r9d
	pushq	%r10
.Lcfi373:
	.cfi_adjust_cfa_offset 8
	pushq	160(%rsp)
.Lcfi374:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi375:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi376:
	.cfi_adjust_cfa_offset 8
	callq	mp_mulh
	addq	$32, %rsp
.Lcfi377:
	.cfi_adjust_cfa_offset -32
	movl	%r12d, %edi
	movl	%r14d, %esi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movq	%rbx, %r8
	callq	mp_sub
	movl	%r12d, %edi
	movl	%r14d, %esi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movq	%r13, %r8
	callq	mp_add
	leaq	8(%r15), %rdx
	movsd	.LCPI36_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movhpd	8(%r15), %xmm0          # xmm0 = xmm0[0],mem[0]
	movupd	(%r15), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	%xmm1, (%r15)
	movsd	16(%r15), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%r15)
	cmpl	$4, %ebp
	jl	.LBB36_41
# BB#33:                                # %.lr.ph.preheader.i.i117
	movl	128(%rsp), %eax
	cltq
	leaq	-4(%rax), %rcx
	shrq	%rcx
	leaq	1(%rcx), %rsi
	cmpq	$2, %rsi
	jae	.LBB36_35
# BB#34:
	movl	$3, %ecx
	jmp	.LBB36_40
.LBB36_35:                              # %min.iters.checked195
	movl	%esi, %edi
	andl	$1, %edi
	subq	%rdi, %rsi
	je	.LBB36_36
# BB#37:                                # %vector.body191.preheader
	leaq	5(%rcx,%rcx), %rcx
	leaq	(%rdi,%rdi), %rbx
	subq	%rbx, %rcx
	leaq	24(%r15), %rbx
	.p2align	4, 0x90
.LBB36_38:                              # %vector.body191
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbx), %xmm0
	movupd	16(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	mulpd	%xmm1, %xmm1
	mulpd	%xmm0, %xmm0
	movapd	%xmm0, %xmm4
	unpcklpd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0]
	unpckhpd	%xmm1, %xmm0    # xmm0 = xmm0[1],xmm1[1]
	subpd	%xmm0, %xmm4
	addpd	%xmm2, %xmm2
	mulpd	%xmm3, %xmm2
	movapd	%xmm4, %xmm0
	unpcklpd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0]
	movhlps	%xmm4, %xmm2            # xmm2 = xmm4[1],xmm2[1]
	movups	%xmm2, 16(%rbx)
	movupd	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-2, %rsi
	jne	.LBB36_38
# BB#39:                                # %middle.block192
	testq	%rdi, %rdi
	jne	.LBB36_40
	jmp	.LBB36_41
.LBB36_36:
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB36_40:                              # %.lr.ph.i.i120
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%r15,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%r15,%rcx,8)
	movsd	%xmm0, 8(%r15,%rcx,8)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jl	.LBB36_40
.LBB36_41:                              # %mp_squh_use_in1fft.exit121
	movl	128(%rsp), %ebp
	leal	1(%rbp), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movslq	%ebp, %rax
	movsd	8(%r15,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%r15,%rax,8)
	movl	$-1, %esi
	movl	%ebp, %edi
	movq	152(%rsp), %rcx
	movq	160(%rsp), %r8
	callq	rdft
	movq	48(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edi
	movl	%r14d, %esi
	movl	%ebp, %edx
	movq	%r15, %rcx
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	%r12, %r8
	callq	mp_mul_d2i
	movl	%ebx, %edi
	movl	%r14d, %esi
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%r12, %rcx
	movq	%r12, %r8
	callq	mp_sub
	movl	4(%rbp), %eax
	movl	8(%rbp), %ecx
	subl	4(%r12), %eax
	xorl	%edx, %edx
	cmpl	8(%r12), %ecx
	setg	%dl
	addl	%eax, %edx
	cmpl	$0, (%r12)
	cmovel	60(%rsp), %edx          # 4-byte Folded Reload
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movq	16(%rsp), %r12          # 8-byte Reload
	cmpl	%r12d, 44(%rsp)         # 4-byte Folded Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	144(%rsp), %r10
	jge	.LBB36_52
# BB#42:
	leal	3(%rbp), %ebx
	cmpl	%r12d, %ebx
	jg	.LBB36_44
# BB#43:                                # %.lr.ph38.preheader.i125
	leal	1(%r12), %eax
	cltq
	movl	%r12d, %ecx
	notl	%ecx
	movl	$-4, %edx
	subl	%ebp, %edx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	leal	1(%r12,%rdx), %ecx
	subq	%rcx, %rax
	leaq	(%r13,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	144(%rsp), %r10
.LBB36_44:                              # %._crit_edge.i127
	movslq	%ebx, %rcx
	movl	(%r13,%rcx,4), %eax
	addl	%eax, %eax
	movl	$0, (%r13,%rcx,4)
	cmpl	%r14d, %eax
	jl	.LBB36_52
# BB#45:
	testl	%ebp, %ebp
	js	.LBB36_50
# BB#46:                                # %.lr.ph.preheader.i128
	addl	$2, %ebp
	movslq	%ebp, %rcx
	.p2align	4, 0x90
.LBB36_47:                              # %.lr.ph.i130
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rcx,4), %eax
	incl	%eax
	cmpl	%r14d, %eax
	jl	.LBB36_48
# BB#49:                                #   in Loop: Header=BB36_47 Depth=1
	movl	$0, (%r13,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB36_47
	jmp	.LBB36_50
.LBB36_48:
	movl	%eax, (%r13,%rcx,4)
.LBB36_50:                              # %.loopexit.i133
	cmpl	%r14d, %eax
	jl	.LBB36_52
# BB#51:
	movl	$1, 8(%r13)
	incl	4(%r13)
.LBB36_52:                              # %mp_round.exit134
	movl	%r12d, %edi
	movl	%r14d, %esi
	movq	%r13, %rdx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rcx
	movq	%rbx, %r8
	movl	128(%rsp), %ebp
	movl	%ebp, %r9d
	pushq	160(%rsp)
.Lcfi378:
	.cfi_adjust_cfa_offset 8
	pushq	160(%rsp)
.Lcfi379:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi380:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi381:
	.cfi_adjust_cfa_offset 8
	callq	mp_mulh
	addq	$32, %rsp
.Lcfi382:
	.cfi_adjust_cfa_offset -32
	movq	168(%rsp), %rax
	movl	%ebp, (%rax)
	movl	8(%rbx), %eax
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	cmpl	$1, %eax
	sete	%sil
	movl	$-1, %ecx
	cmovel	%ecx, %ebp
	subl	%esi, 4(%rbx)
	leal	1(%r12), %r9d
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$2, %edi
	jge	.LBB36_54
# BB#53:
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	24(%rsp), %r15d         # 4-byte Reload
	cmpl	$1, %eax
	je	.LBB36_61
	jmp	.LBB36_62
.LBB36_54:                              # %.lr.ph.preheader.i135
	xorl	%edi, %edi
	cmpl	$1, %eax
	sete	%r8b
	leal	2(%r12,%rbp), %esi
	testb	$1, %sil
	movq	32(%rsp), %rdx          # 8-byte Reload
	jne	.LBB36_56
# BB#55:
	movl	$2, %ebx
	jmp	.LBB36_57
.LBB36_56:                              # %.lr.ph.i138.prol
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	cmpl	$1, %eax
	sete	%bpl
	cmovel	%r14d, %ebx
	movq	8(%rsp), %rcx           # 8-byte Reload
	addl	8(%rcx,%rbp,4), %ebx
	movl	%ebx, %ebp
	andl	$1, %ebp
	negl	%ebp
	sarl	%ebx
	movl	%ebx, 8(%rcx)
	movl	$3, %ebx
.LBB36_57:                              # %.lr.ph.i138.prol.loopexit
	movl	24(%rsp), %r15d         # 4-byte Reload
	cmpl	$3, %esi
	je	.LBB36_60
# BB#58:                                # %.lr.ph.preheader.i135.new
	movb	%r8b, %dil
	subq	%rbx, %rsi
	addq	%rbx, %rdi
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	4(%rcx,%rdi,4), %rdi
	leaq	4(%rcx,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB36_59:                              # %.lr.ph.i138
                                        # =>This Inner Loop Header: Depth=1
	andl	%r14d, %ebp
	addl	-4(%rdi), %ebp
	movl	%ebp, %ecx
	andl	$1, %ecx
	negl	%ecx
	sarl	%ebp
	movl	%ebp, -4(%rbx)
	andl	%r14d, %ecx
	addl	(%rdi), %ecx
	movl	%ecx, %ebp
	andl	$1, %ebp
	negl	%ebp
	sarl	%ecx
	movl	%ecx, (%rbx)
	addq	$8, %rdi
	addq	$8, %rbx
	addq	$-2, %rsi
	jne	.LBB36_59
.LBB36_60:                              # %._crit_edge.i139
	cmpl	$1, %eax
	jne	.LBB36_62
.LBB36_61:
	andl	%r14d, %ebp
	sarl	%ebp
	movslq	%r9d, %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ebp, (%rcx,%rax,4)
.LBB36_62:                              # %mp_idiv_2.exit
	movq	48(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r14d, %esi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rdx, %r8
	callq	mp_add
	movl	%r15d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end36:
	.size	mp_sqrt_newton, .Lfunc_end36-mp_sqrt_newton
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI37_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	mp_unexp_mp2d
	.p2align	4, 0x90
	.type	mp_unexp_mp2d,@function
mp_unexp_mp2d:                          # @mp_unexp_mp2d
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB37_1
# BB#2:                                 # %.lr.ph.preheader
	cvtsi2sdl	%esi, %xmm0
	movsd	.LCPI37_0(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movslq	%edi, %rax
	incq	%rax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB37_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	cvtsi2sdl	-8(%rdx,%rax,4), %xmm2
	addsd	%xmm2, %xmm0
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB37_3
# BB#4:                                 # %._crit_edge
	retq
.LBB37_1:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end37:
	.size	mp_unexp_mp2d, .Lfunc_end37-mp_unexp_mp2d
	.cfi_endproc

	.globl	mp_unexp_d2mp
	.p2align	4, 0x90
	.type	mp_unexp_d2mp,@function
mp_unexp_d2mp:                          # @mp_unexp_d2mp
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%edi, %edi
	jle	.LBB38_13
# BB#1:                                 # %.lr.ph
	leal	-1(%rsi), %eax
	cvtsi2sdl	%esi, %xmm1
	movl	%edi, %ecx
	testb	$1, %cl
	jne	.LBB38_3
# BB#2:
	xorl	%r8d, %r8d
	cmpl	$1, %edi
	jne	.LBB38_7
	jmp	.LBB38_13
.LBB38_3:
	cvttsd2si	%xmm0, %r8d
	cmpl	%esi, %r8d
	jl	.LBB38_5
# BB#4:
	movapd	%xmm1, %xmm0
.LBB38_5:
	cmovgel	%eax, %r8d
	cvtsi2sdl	%r8d, %xmm2
	subsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	movl	%r8d, (%rdx)
	movl	$1, %r8d
	cmpl	$1, %edi
	je	.LBB38_13
.LBB38_7:                               # %.lr.ph.new
	subq	%r8, %rcx
	leaq	4(%rdx,%r8,4), %rdx
	.p2align	4, 0x90
.LBB38_8:                               # =>This Inner Loop Header: Depth=1
	cvttsd2si	%xmm0, %edi
	cmpl	%esi, %edi
	jl	.LBB38_10
# BB#9:                                 #   in Loop: Header=BB38_8 Depth=1
	movapd	%xmm1, %xmm0
.LBB38_10:                              #   in Loop: Header=BB38_8 Depth=1
	cmovgel	%eax, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	subsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	movl	%edi, -4(%rdx)
	cvttsd2si	%xmm0, %edi
	cmpl	%esi, %edi
	jl	.LBB38_12
# BB#11:                                #   in Loop: Header=BB38_8 Depth=1
	movapd	%xmm1, %xmm0
.LBB38_12:                              #   in Loop: Header=BB38_8 Depth=1
	cmovgel	%eax, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	subsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	movl	%edi, (%rdx)
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB38_8
.LBB38_13:                              # %._crit_edge
	retq
.Lfunc_end38:
	.size	mp_unexp_d2mp, .Lfunc_end38-mp_unexp_d2mp
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Compile date: %s\n"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"today"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Compiler switches: %s\n"
	.size	.L.str.2, 23

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.zero	1
	.size	.L.str.3, 1

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"r"
	.size	.L.str.5, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ERROR: Could not open indata file.\n"
	.size	.L.str.6, 36

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%d"
	.size	.L.str.7, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Allocation Failure!\n"
	.size	.L.str.9, 21

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"nfft= %d\nradix= %d\n"
	.size	.L.str.10, 20

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"calculating %d digits of PI...\n"
	.size	.L.str.11, 32

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"0.125"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"1"
	.size	.L.str.13, 2

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"0.625"
	.size	.L.str.14, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"precision= %d\n"
	.size	.L.str.16, 15

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"floating point operation: %g op.\n"
	.size	.L.str.18, 34

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"PI calculation to estimate the FFT benchmarks"
	.size	.Lstr, 46

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"initializing..."
	.size	.Lstr.1, 16

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"AGM iteration"
	.size	.Lstr.2, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
