	.text
	.file	"gxfill.bc"
	.globl	gx_fill_path
	.p2align	4, 0x90
	.type	gx_fill_path,@function
gx_fill_path:                           # @gx_fill_path
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi6:
	.cfi_def_cfa_offset 400
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%rdx, %rbx
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movq	264(%rbx), %r15
	callq	gx_path_close_subpath
	cmpl	$0, 112(%r14)
	movq	%r14, %r12
	je	.LBB0_2
# BB#1:
	movss	440(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	200(%rsp), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	gx_path_flatten
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_24
.LBB0_2:
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movl	104(%r12), %r13d
	addl	108(%r12), %r13d
	addl	108(%r15), %r13d
	addl	104(%r15), %r13d
	movl	%r13d, 72(%rsp)
	movl	$88, %esi
	movl	$.L.str, %edx
	movl	%r13d, %edi
	callq	gs_malloc
	movq	%rax, %rbp
	movq	%rbp, 64(%rsp)
	testq	%rbp, %rbp
	je	.LBB0_3
# BB#4:
	movq	%rbp, 80(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rsp)
	leaq	8(%rsp), %rsi
	movq	%r12, %rdi
	callq	gx_path_bbox
	movq	32(%rsp), %rax
	cmpq	80(%r15), %rax
	jg	.LBB0_9
# BB#5:
	movq	24(%rsp), %rcx
	cmpq	72(%r15), %rcx
	jg	.LBB0_9
# BB#6:
	movq	16(%rsp), %rcx
	cmpq	64(%r15), %rcx
	jl	.LBB0_9
# BB#7:
	movq	8(%rsp), %rcx
	cmpq	56(%r15), %rcx
	jge	.LBB0_8
.LBB0_9:
	movl	$0, 192(%rsp)
	movq	16(%r15), %rdx
	movq	8(%rsp), %rcx
	cmpq	%rcx, %rdx
	jle	.LBB0_11
# BB#10:
	movq	%rdx, 8(%rsp)
	movq	%rdx, %rcx
.LBB0_11:
	movq	24(%r15), %rsi
	movq	16(%rsp), %rdx
	cmpq	%rdx, %rsi
	jle	.LBB0_13
# BB#12:
	movq	%rsi, 16(%rsp)
	movq	%rsi, %rdx
.LBB0_13:
	movq	32(%r15), %rdi
	movq	24(%rsp), %rsi
	cmpq	%rsi, %rdi
	jge	.LBB0_15
# BB#14:
	movq	%rdi, 24(%rsp)
	movq	%rdi, %rsi
.LBB0_15:
	movq	40(%r15), %rdi
	cmpq	%rax, %rdi
	jge	.LBB0_17
# BB#16:
	movq	%rdi, 32(%rsp)
	movq	%rdi, %rax
.LBB0_17:
	cmpq	%rsi, %rcx
	jge	.LBB0_21
# BB#18:
	cmpq	%rax, %rdx
	jge	.LBB0_21
# BB#19:
	leaq	64(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movl	$1, %esi
	movq	%r15, %rdi
	callq	add_y_list
.LBB0_20:
	leaq	64(%rsp), %rbp
	leaq	8(%rsp), %r15
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	callq	add_y_list
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	%rbp, %rdx
	movq	%r15, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	fill_loop
	movq	64(%rsp), %rbp
	movl	72(%rsp), %r13d
.LBB0_21:
	movl	$88, %edx
	movl	$.L.str, %ecx
	movq	%rbp, %rdi
	movl	%r13d, %esi
	callq	gs_free
	xorl	%ebp, %ebp
	cmpq	%r14, %r12
	jne	.LBB0_23
	jmp	.LBB0_24
.LBB0_3:
	movl	$-25, %ebp
	cmpq	%r14, %r12
	je	.LBB0_24
.LBB0_23:
	movq	%r12, %rdi
	callq	gx_path_release
.LBB0_24:
	movl	%ebp, %eax
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_8:
	movl	$1, 192(%rsp)
	jmp	.LBB0_20
.Lfunc_end0:
	.size	gx_fill_path, .Lfunc_end0-gx_fill_path
	.cfi_endproc

	.globl	alloc_line_list
	.p2align	4, 0x90
	.type	alloc_line_list,@function
alloc_line_list:                        # @alloc_line_list
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movl	%esi, %eax
	movq	%rdi, %rbx
	movl	%eax, 8(%rbx)
	movl	$88, %esi
	movl	$.L.str, %edx
	movl	%eax, %edi
	callq	gs_malloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB1_1
# BB#2:
	movq	%rax, 16(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB1_1:
	movl	$-25, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	alloc_line_list, .Lfunc_end1-alloc_line_list
	.cfi_endproc

	.globl	add_y_list
	.p2align	4, 0x90
	.type	add_y_list,@function
add_y_list:                             # @add_y_list
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 96
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	88(%rdi), %rax
	movq	8(%rcx), %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	16(%rcx), %r14
	movq	24(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, (%rsp)            # 8-byte Spill
	movw	%si, 12(%rdx)
	testq	%rax, %rax
	je	.LBB2_34
# BB#1:                                 # %.lr.ph.preheader
                                        # implicit-def: %RCX
	movq	%rcx, 24(%rsp)          # 8-byte Spill
                                        # implicit-def: %RCX
	movq	%rcx, 32(%rsp)          # 8-byte Spill
                                        # implicit-def: %R13
                                        # implicit-def: %EDX
                                        # implicit-def: %RBX
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r12
	movq	%rax, %rbx
	cmpl	$0, 16(%rbx)
	je	.LBB2_3
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	32(%rbx), %r15
	movq	32(%r12), %rax
	cmpq	%r14, 24(%r12)
	jle	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	xorl	%ebp, %ebp
	cmpq	%r14, 24(%rbx)
	jg	.LBB2_10
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r15, %rcx
	subq	%rax, %rcx
	jle	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	setl	%al
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	setg	%cl
	andb	%al, %cl
	movzbl	%cl, %ebp
	cmpl	%edx, %ebp
	jg	.LBB2_11
	jmp	.LBB2_15
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_2 Depth=1
	movq	40(%rbx), %r13
	xorl	%ebp, %ebp
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	jmp	.LBB2_33
.LBB2_8:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%ebp, %ebp
	testq	%rcx, %rcx
	jns	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpq	8(%rsp), %r15           # 8-byte Folded Reload
	setl	%cl
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	setg	%al
	andb	%cl, %al
	movzbl	%al, %ebp
	negl	%ebp
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_2 Depth=1
	cmpl	%edx, %ebp
	jle	.LBB2_15
.LBB2_11:                               #   in Loop: Header=BB2_2 Depth=1
	testl	%edx, %edx
	je	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%r12), %rdi
	movq	%r12, %rsi
	movq	(%rsp), %rcx            # 8-byte Reload
	callq	add_y_line
.LBB2_13:                               #   in Loop: Header=BB2_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	movq	(%rsp), %rcx            # 8-byte Reload
	callq	add_y_line
.LBB2_15:                               #   in Loop: Header=BB2_2 Depth=1
	cmpq	%r13, %rbx
	jne	.LBB2_33
# BB#16:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$2, 16(%rbx)
	jne	.LBB2_17
# BB#29:                                #   in Loop: Header=BB2_2 Depth=1
	testl	%ebp, %ebp
	jns	.LBB2_32
# BB#30:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	jmp	.LBB2_31
.LBB2_17:                               #   in Loop: Header=BB2_2 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rax
	cmpq	%r14, 24(%rbx)
	jle	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%r13d, %r13d
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpq	%r14, 24(%rcx)
	jg	.LBB2_23
.LBB2_19:                               #   in Loop: Header=BB2_2 Depth=1
	movq	%rax, %rcx
	subq	%r15, %rcx
	jle	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_2 Depth=1
	cmpq	8(%rsp), %r15           # 8-byte Folded Reload
	setl	%cl
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	setg	%al
	andb	%cl, %al
	movzbl	%al, %r13d
	testl	%ebp, %ebp
	jne	.LBB2_24
	jmp	.LBB2_26
.LBB2_21:                               #   in Loop: Header=BB2_2 Depth=1
	xorl	%r13d, %r13d
	testq	%rcx, %rcx
	jns	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_2 Depth=1
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	setl	%al
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	setg	%cl
	andb	%al, %cl
	movzbl	%cl, %r13d
	negl	%r13d
.LBB2_23:                               #   in Loop: Header=BB2_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB2_26
.LBB2_24:                               #   in Loop: Header=BB2_2 Depth=1
	cmpl	%ebp, %r13d
	jle	.LBB2_26
# BB#25:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	movq	(%rsp), %rcx            # 8-byte Reload
	callq	add_y_line
.LBB2_26:                               #   in Loop: Header=BB2_2 Depth=1
	cmpl	%ebp, %r13d
	setg	%al
	testl	%r13d, %r13d
	setne	%cl
	testl	%r13d, %r13d
	js	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_2 Depth=1
	andb	%cl, %al
	je	.LBB2_32
.LBB2_28:                               #   in Loop: Header=BB2_2 Depth=1
	movq	%rbx, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	%r13d, %edx
.LBB2_31:                               #   in Loop: Header=BB2_2 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	callq	add_y_line
.LBB2_32:                               #   in Loop: Header=BB2_2 Depth=1
	movq	%rbx, %r13
.LBB2_33:                               #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rbx), %rax
	testq	%rax, %rax
	movl	%ebp, %edx
	jne	.LBB2_2
.LBB2_34:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	add_y_list, .Lfunc_end2-add_y_list
	.cfi_endproc

	.globl	fill_loop
	.p2align	4, 0x90
	.type	fill_loop,@function
fill_loop:                              # @fill_loop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 240
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	%esi, %r11d
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	24(%rdx), %r12
	testq	%r12, %r12
	je	.LBB3_88
# BB#1:
	movq	24(%rcx), %rcx
	movq	8(%r12), %r13
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	$0, 120(%rax)
	movw	$-4, 110(%rax)
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	cmpq	%rcx, %r13
	jge	.LBB3_88
# BB#2:                                 # %.preheader236.lr.ph.lr.ph
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	120(%rax), %rbx
	leaq	40(%rax), %r10
	leaq	(%r9,%r9), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 56(%rsp)          # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %R14
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	%r10, 96(%rsp)          # 8-byte Spill
	jmp	.LBB3_70
.LBB3_3:                                #   in Loop: Header=BB3_70 Depth=1
	testb	%al, %al
	movq	%r13, 80(%rsp)          # 8-byte Spill
	je	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_70 Depth=1
	movq	8(%r12), %r13
	jmp	.LBB3_6
.LBB3_5:                                #   in Loop: Header=BB3_70 Depth=1
	movl	$2147483647, %r13d      # imm = 0x7FFFFFFF
.LBB3_6:                                # %.lr.ph261.preheader
                                        #   in Loop: Header=BB3_70 Depth=1
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph261
                                        #   Parent Loop BB3_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rax), %rcx
	movq	80(%rax), %rax
	cmpq	%r13, %rcx
	cmovleq	%rcx, %r13
	testq	%rax, %rax
	jne	.LBB3_7
# BB#8:                                 # %.lr.ph270.preheader
                                        #   in Loop: Header=BB3_70 Depth=1
	movq	$-2147483648, %rdi      # imm = 0x80000000
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %rax
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movl	%r11d, 76(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph270
                                        #   Parent Loop BB3_70 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_17 Depth 3
	movq	%rax, %r14
	movq	24(%r14), %r9
	cmpq	%r9, %r13
	jne	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=2
	movq	16(%r14), %r8
	movq	%r8, %rcx
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_9 Depth=2
	movq	(%r14), %rcx
	movq	8(%r14), %rdx
	movq	%r13, %rbp
	subq	%rdx, %rbp
	cmpq	32(%r14), %r13
	movq	16(%r14), %r8
	jle	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_9 Depth=2
	cvtsi2sdq	%rbp, %xmm0
	movq	%r8, %rax
	subq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm1
	mulsd	%xmm0, %xmm1
	movq	%r9, %rax
	subq	%rdx, %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %rax
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_13:                               #   in Loop: Header=BB3_9 Depth=2
	movq	%r8, %rax
	subq	%rcx, %rax
	imulq	%rbp, %rax
	movq	%r9, %rbp
	subq	%rdx, %rbp
	cqto
	idivq	%rbp
.LBB3_14:                               #   in Loop: Header=BB3_9 Depth=2
	addq	%rax, %rcx
.LBB3_15:                               #   in Loop: Header=BB3_9 Depth=2
	movq	%rcx, 48(%r14)
	cmpq	%rdi, %rcx
	jge	.LBB3_29
# BB#16:                                #   in Loop: Header=BB3_9 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	24(%rax), %r11
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%r11, %rsi
	subq	%rdx, %rsi
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	movq	(%r14), %rsi
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	8(%r14), %rbp
	movq	%r9, %rdi
	subq	%rbp, %rdi
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rdi, %xmm1
	cvtsi2sdq	%rcx, %xmm2
	cvtsi2sdq	%r11, %xmm4
	mulsd	%xmm2, %xmm4
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
	movq	16(%rax), %r15
	cvtsi2sdq	%r15, %xmm3
	mulsd	%xmm2, %xmm3
	subsd	%xmm3, %xmm4
	mulsd	%xmm1, %xmm4
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%rsi, %xmm2
	xorps	%xmm3, %xmm3
	cvtsi2sdq	%r9, %xmm3
	mulsd	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%rbp, %xmm2
	cvtsi2sdq	%r8, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm4
	movq	%r8, %r10
	movq	%rsi, 168(%rsp)         # 8-byte Spill
	subq	%rsi, %r10
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%r10, %xmm2
	movapd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	movq	%r15, %r14
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	subq	%rcx, %r14
	xorps	%xmm3, %xmm3
	cvtsi2sdq	%r14, %xmm3
	movapd	%xmm1, %xmm6
	mulsd	%xmm3, %xmm6
	subsd	%xmm6, %xmm5
	divsd	%xmm5, %xmm4
	cvttsd2si	%xmm4, %rsi
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	imulq	%r14, %rdi
	negq	%rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	leaq	-1(%rsi), %rbx
	subq	%rbp, %rsi
	imulq	%r10, %rsi
	negq	%rbp
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	negq	%r9
	negq	%r11
	.p2align	4, 0x90
.LBB3_17:                               #   Parent Loop BB3_70 Depth=1
                                        #     Parent Loop BB3_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	1(%rbx), %r13
	leaq	(%r9,%rbx), %rcx
	leaq	(%r11,%rbx), %rax
	cmpq	$-1, %rax
	movq	%r15, %rbp
	je	.LBB3_22
# BB#18:                                #   in Loop: Header=BB3_17 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	32(%rax), %r13
	jle	.LBB3_20
# BB#19:                                #   in Loop: Header=BB3_17 Depth=3
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	1(%rax,%rbx), %rax
	xorps	%xmm4, %xmm4
	cvtsi2sdq	%rax, %xmm4
	mulsd	%xmm3, %xmm4
	divsd	%xmm0, %xmm4
	cvttsd2si	%xmm4, %rbp
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_20:                               #   in Loop: Header=BB3_17 Depth=3
	movq	%rdi, %rax
	cqto
	idivq	152(%rsp)               # 8-byte Folded Reload
	movq	%rax, %rbp
.LBB3_21:                               #   in Loop: Header=BB3_17 Depth=3
	addq	64(%rsp), %rbp          # 8-byte Folded Reload
.LBB3_22:                               #   in Loop: Header=BB3_17 Depth=3
	cmpq	$-1, %rcx
	movq	%r8, %rcx
	je	.LBB3_27
# BB#23:                                #   in Loop: Header=BB3_17 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	32(%rax), %r13
	jle	.LBB3_25
# BB#24:                                #   in Loop: Header=BB3_17 Depth=3
	movq	144(%rsp), %rax         # 8-byte Reload
	leaq	1(%rax,%rbx), %rax
	xorps	%xmm4, %xmm4
	cvtsi2sdq	%rax, %xmm4
	mulsd	%xmm2, %xmm4
	divsd	%xmm1, %xmm4
	cvttsd2si	%xmm4, %rcx
	jmp	.LBB3_26
	.p2align	4, 0x90
.LBB3_25:                               #   in Loop: Header=BB3_17 Depth=3
	movq	%rsi, %rax
	cqto
	idivq	136(%rsp)               # 8-byte Folded Reload
	movq	%rax, %rcx
.LBB3_26:                               #   in Loop: Header=BB3_17 Depth=3
	addq	168(%rsp), %rcx         # 8-byte Folded Reload
.LBB3_27:                               #   in Loop: Header=BB3_17 Depth=3
	addq	%r14, %rdi
	addq	%r10, %rsi
	cmpq	%rbp, %rcx
	movq	%r13, %rbx
	jg	.LBB3_17
# BB#28:                                #   in Loop: Header=BB3_9 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbp, 48(%rdi)
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%rcx, 48(%r14)
	movq	24(%rsp), %rsi          # 8-byte Reload
	leal	4(%rsi), %edx
	leal	7(%rsi), %eax
	movw	%ax, 70(%rdi)
	addl	$5, %esi
	movl	%esi, %eax
	movq	%rdi, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	76(%rsp), %r11d         # 4-byte Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
	jmp	.LBB3_30
	.p2align	4, 0x90
.LBB3_29:                               #   in Loop: Header=BB3_9 Depth=2
	cmpq	%r9, %r13
	movw	$-2, %ax
	cmovew	24(%rsp), %ax           # 2-byte Folded Reload
.LBB3_30:                               #   in Loop: Header=BB3_9 Depth=2
	movw	%ax, 70(%r14)
	movq	80(%r14), %rax
	testq	%rax, %rax
	movq	%rcx, %rdi
	movq	%r14, 8(%rsp)           # 8-byte Spill
	jne	.LBB3_9
# BB#31:                                # %.preheader
                                        #   in Loop: Header=BB3_70 Depth=1
	movq	(%rbx), %r8
	cmpq	%rsi, %r8
	movq	%r8, %rcx
	movq	88(%rsp), %r9           # 8-byte Reload
	je	.LBB3_39
	.p2align	4, 0x90
.LBB3_32:                               # %.lr.ph278
                                        #   Parent Loop BB3_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rcx), %rbp
	cmpq	%rbp, %r13
	jne	.LBB3_34
# BB#33:                                #   in Loop: Header=BB3_32 Depth=2
	movq	16(%rcx), %rdi
	jmp	.LBB3_38
	.p2align	4, 0x90
.LBB3_34:                               #   in Loop: Header=BB3_32 Depth=2
	movq	%r8, %r15
	movq	(%rcx), %rdi
	movq	8(%rcx), %r8
	movq	%r13, %rdx
	subq	%r8, %rdx
	cmpq	32(%rcx), %r13
	movq	16(%rcx), %rax
	jle	.LBB3_36
# BB#35:                                #   in Loop: Header=BB3_32 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	subq	%rdi, %rax
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	mulsd	%xmm0, %xmm1
	subq	%r8, %rbp
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rbp, %xmm0
	divsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %rax
	jmp	.LBB3_37
	.p2align	4, 0x90
.LBB3_36:                               #   in Loop: Header=BB3_32 Depth=2
	subq	%rdi, %rax
	imulq	%rdx, %rax
	subq	%r8, %rbp
	cqto
	idivq	%rbp
.LBB3_37:                               #   in Loop: Header=BB3_32 Depth=2
	movq	%r15, %r8
	addq	%rax, %rdi
.LBB3_38:                               #   in Loop: Header=BB3_32 Depth=2
	movq	%rdi, 48(%rcx)
	movq	80(%rcx), %rcx
	cmpq	%rsi, %rcx
	jne	.LBB3_32
.LBB3_39:                               # %._crit_edge279
                                        #   in Loop: Header=BB3_70 Depth=1
	movl	$0, (%rsp)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	128(%rax), %eax
	movl	%eax, 4(%rsp)
	testq	%r8, %r8
	movq	16(%rsp), %rdi          # 8-byte Reload
	je	.LBB3_87
# BB#40:                                # %.lr.ph284.preheader
                                        #   in Loop: Header=BB3_70 Depth=1
	movq	%r13, %rax
	subq	80(%rsp), %rax          # 8-byte Folded Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_41:                               # %.lr.ph284
                                        #   Parent Loop BB3_70 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_52 Depth 3
                                        #       Child Loop BB3_56 Depth 3
	movq	%r14, %r15
	movq	%r8, %rbp
	movq	40(%rbp), %rsi
	movq	48(%rbp), %r14
	movq	80(%rbp), %r8
	testl	%r11d, %eax
	je	.LBB3_46
# BB#42:                                #   in Loop: Header=BB3_41 Depth=2
	movl	272(%rdi), %eax
	testl	4(%rsp), %eax
	je	.LBB3_46
# BB#43:                                #   in Loop: Header=BB3_41 Depth=2
	movl	64(%rbp), %ecx
	movswq	68(%rbp), %rdx
	addl	%ecx, (%rsp,%rdx,4)
	movl	(%rsp), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	testl	%r11d, %ecx
	je	.LBB3_45
# BB#44:                                #   in Loop: Header=BB3_41 Depth=2
	testl	4(%rsp), %eax
	jne	.LBB3_48
.LBB3_45:                               #   in Loop: Header=BB3_41 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r9), %rdi
	movq	120(%rsp), %rdx         # 8-byte Reload
	leaq	(%rax,%rdx), %rax
	subq	%rax, %rsi
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r9), %rcx
	leaq	(%rax,%rdx), %rax
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%r14, %r8
	subq	%rax, %r8
	subq	$8, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	movl	$0, %eax
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	144(%rsp)               # 8-byte Folded Reload
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	movl	%r11d, %ebx
	callq	gz_fill_trapezoid_fixed
	movq	64(%rsp), %r8           # 8-byte Reload
	movl	%ebx, %r11d
	movq	128(%rsp), %r10         # 8-byte Reload
	movq	136(%rsp), %rbx         # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	120(%rsp), %r9          # 8-byte Reload
	addq	$32, %rsp
.Lcfi45:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB3_48
	.p2align	4, 0x90
.LBB3_46:                               #   in Loop: Header=BB3_41 Depth=2
	movl	64(%rbp), %eax
	movswq	68(%rbp), %rcx
	addl	%eax, (%rsp,%rcx,4)
	movl	(%rsp), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	testl	%r11d, %eax
	je	.LBB3_48
# BB#47:                                #   in Loop: Header=BB3_41 Depth=2
	movl	272(%rdi), %eax
	andl	4(%rsp), %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmovneq	%rsi, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	movq	56(%rsp), %rax          # 8-byte Reload
	cmovneq	%r14, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
.LBB3_48:                               #   in Loop: Header=BB3_41 Depth=2
	movq	%r14, 40(%rbp)
	movswl	70(%rbp), %eax
	cmpl	24(%rsp), %eax          # 4-byte Folded Reload
	movq	%r15, %r14
	jl	.LBB3_69
# BB#49:                                #   in Loop: Header=BB3_41 Depth=2
	andb	$3, %al
	je	.LBB3_57
# BB#50:                                #   in Loop: Header=BB3_41 Depth=2
	cmpb	$1, %al
	jne	.LBB3_69
# BB#51:                                #   in Loop: Header=BB3_41 Depth=2
	movq	80(%rbp), %rdx
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB3_52:                               #   Parent Loop BB3_70 Depth=1
                                        #     Parent Loop BB3_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, %rcx
	movq	72(%rcx), %rax
	movzwl	70(%rax), %esi
	andl	$3, %esi
	cmpl	$3, %esi
	je	.LBB3_52
# BB#53:                                #   in Loop: Header=BB3_41 Depth=2
	movq	%rbp, 80(%rax)
	testq	%rdx, %rdx
	je	.LBB3_55
# BB#54:                                #   in Loop: Header=BB3_41 Depth=2
	movq	%rcx, 72(%rdx)
.LBB3_55:                               #   in Loop: Header=BB3_41 Depth=2
	movq	%rdx, 72(%rcx)
	movq	%rax, 80(%rbp)
	.p2align	4, 0x90
.LBB3_56:                               #   Parent Loop BB3_70 Depth=1
                                        #     Parent Loop BB3_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	72(%rcx), %rdx
	movq	80(%rcx), %rsi
	movq	%rdx, 80(%rcx)
	movq	%rsi, 72(%rcx)
	cmpq	%rax, %rsi
	movq	%rsi, %rcx
	jne	.LBB3_56
	jmp	.LBB3_69
.LBB3_57:                               #   in Loop: Header=BB3_41 Depth=2
	movq	56(%rbp), %rcx
	movq	32(%rcx), %rax
	cmpl	$1, 64(%rbp)
	jne	.LBB3_61
# BB#58:                                #   in Loop: Header=BB3_41 Depth=2
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB3_64
# BB#59:                                #   in Loop: Header=BB3_41 Depth=2
	cmpl	$0, 16(%rdx)
	jne	.LBB3_65
	jmp	.LBB3_64
.LBB3_61:                               #   in Loop: Header=BB3_41 Depth=2
	cmpl	$0, 16(%rcx)
	je	.LBB3_64
# BB#62:                                #   in Loop: Header=BB3_41 Depth=2
	movq	(%rcx), %rdx
	jmp	.LBB3_65
.LBB3_64:                               #   in Loop: Header=BB3_41 Depth=2
	movq	%rcx, %rdx
.LBB3_65:                               #   in Loop: Header=BB3_41 Depth=2
	movq	32(%rdx), %rcx
	cmpq	%rax, %rcx
	jle	.LBB3_67
# BB#66:                                #   in Loop: Header=BB3_41 Depth=2
	movq	%rdx, 56(%rbp)
	movq	24(%rdx), %rsi
	movq	%rsi, 176(%rsp)
	leaq	16(%rbp), %rax
	movq	16(%rbp), %rdi
	cmpq	%rdi, %rsi
	leaq	176(%rsp), %rdx
	cmovleq	%rdx, %rax
	cmovgeq	%rsi, %rdi
	subq	(%rax), %rdi
	orq	$1, %rdi
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	xorl	%edx, %edx
	idivq	%rdi
	movq	16(%rsp), %rdi          # 8-byte Reload
	addq	24(%rbp), %rax
	movq	%rax, 32(%rbp)
	movupd	16(%rbp), %xmm0
	movupd	%xmm0, (%rbp)
	movq	%rsi, 16(%rbp)
	movq	%rcx, 24(%rbp)
	jmp	.LBB3_69
.LBB3_67:                               #   in Loop: Header=BB3_41 Depth=2
	movq	72(%rbp), %rax
	movq	80(%rbp), %rcx
	movq	%rcx, 80(%rax)
	testq	%rcx, %rcx
	je	.LBB3_69
# BB#68:                                #   in Loop: Header=BB3_41 Depth=2
	movq	%rax, 72(%rcx)
	.p2align	4, 0x90
.LBB3_69:                               # %swap_group.exit.backedge
                                        #   in Loop: Header=BB3_41 Depth=2
	testq	%r8, %r8
	movl	8(%rsp), %eax           # 4-byte Reload
	jne	.LBB3_41
	jmp	.LBB3_87
	.p2align	4, 0x90
.LBB3_70:                               # %.preheader236
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_71 Depth 2
                                        #       Child Loop BB3_74 Depth 3
                                        #     Child Loop BB3_7 Depth 2
                                        #     Child Loop BB3_9 Depth 2
                                        #       Child Loop BB3_17 Depth 3
                                        #     Child Loop BB3_32 Depth 2
                                        #     Child Loop BB3_41 Depth 2
                                        #       Child Loop BB3_52 Depth 3
                                        #       Child Loop BB3_56 Depth 3
	testq	%r12, %r12
	je	.LBB3_82
	.p2align	4, 0x90
.LBB3_71:                               # %.lr.ph
                                        #   Parent Loop BB3_70 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_74 Depth 3
	cmpq	%r13, 8(%r12)
	jne	.LBB3_83
# BB#72:                                #   in Loop: Header=BB3_71 Depth=2
	movq	(%r12), %rcx
	movq	80(%r12), %rax
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	movq	%r10, %rsi
	movq	%rbx, %rdx
	je	.LBB3_79
# BB#73:                                # %.lr.ph.i
                                        #   in Loop: Header=BB3_71 Depth=2
	movq	%rbx, %rdx
	movq	%r10, %rsi
	.p2align	4, 0x90
.LBB3_74:                               #   Parent Loop BB3_70 Depth=1
                                        #     Parent Loop BB3_71 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rsi, %rbp
	movq	%rdi, %rsi
	cmpq	%rcx, 40(%rsi)
	jl	.LBB3_78
# BB#75:                                #   in Loop: Header=BB3_74 Depth=3
	jne	.LBB3_80
# BB#76:                                #   in Loop: Header=BB3_74 Depth=3
	cmpq	%rcx, (%rsi)
	jg	.LBB3_78
# BB#77:                                #   in Loop: Header=BB3_74 Depth=3
	movq	16(%rsi), %rdi
	cmpq	16(%r12), %rdi
	jge	.LBB3_80
.LBB3_78:                               # %.critedge26.backedge.i
                                        #   in Loop: Header=BB3_74 Depth=3
	leaq	80(%rsi), %rdx
	movq	80(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_74
.LBB3_79:                               # %.critedge29.i
                                        #   in Loop: Header=BB3_71 Depth=2
	movq	%rsi, %rdi
	movq	$0, 80(%r12)
	movq	%r12, %rsi
	jmp	.LBB3_81
	.p2align	4, 0x90
.LBB3_80:                               # %.critedge.i
                                        #   in Loop: Header=BB3_71 Depth=2
	movq	%rsi, 80(%r12)
	movq	%rbp, 72(%r12)
	movq	%r12, %rdi
.LBB3_81:                               # %insert_x_new.exit
                                        #   in Loop: Header=BB3_71 Depth=2
	movq	%rdi, 72(%rsi)
	movq	%r12, (%rdx)
	movq	%rcx, 40(%r12)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB3_71
.LBB3_82:                               #   in Loop: Header=BB3_70 Depth=1
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	jmp	.LBB3_84
	.p2align	4, 0x90
.LBB3_83:                               #   in Loop: Header=BB3_70 Depth=1
	movb	$1, %al
.LBB3_84:                               # %.critedge
                                        #   in Loop: Header=BB3_70 Depth=1
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.LBB3_3
# BB#85:                                #   in Loop: Header=BB3_70 Depth=1
	testq	%r12, %r12
	je	.LBB3_88
# BB#86:                                #   in Loop: Header=BB3_70 Depth=1
	movq	8(%r12), %r13
.LBB3_87:                               # %.outer
                                        #   in Loop: Header=BB3_70 Depth=1
	cmpq	112(%rsp), %r13         # 8-byte Folded Reload
	jl	.LBB3_70
.LBB3_88:                               # %.thread
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	fill_loop, .Lfunc_end3-fill_loop
	.cfi_endproc

	.globl	add_y_line
	.p2align	4, 0x90
	.type	add_y_line,@function
add_y_line:                             # @add_y_line
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -32
.Lcfi50:
	.cfi_offset %r14, -24
.Lcfi51:
	.cfi_offset %r15, -16
	movq	16(%rcx), %r8
	leaq	88(%r8), %rax
	movq	%rax, 16(%rcx)
	movq	24(%rsi), %r10
	movq	%r10, -8(%rsp)
	movq	32(%rsi), %r11
	movq	24(%rdi), %rbx
	movq	%rbx, -16(%rsp)
	movq	32(%rdi), %r15
	movzwl	12(%rcx), %eax
	movw	%ax, 68(%r8)
	movl	%edx, 64(%r8)
	testl	%edx, %edx
	jle	.LBB4_2
# BB#1:
	cmpq	%rbx, %r10
	leaq	-8(%rsp), %rax
	leaq	-16(%rsp), %rdi
	movq	%rdi, %rdx
	cmovgq	%rax, %rdx
	cmovgq	%rdi, %rax
	movq	%r15, %r9
	movq	%rbx, %r14
	jmp	.LBB4_3
.LBB4_2:
	cmpq	%r10, %rbx
	leaq	-16(%rsp), %rax
	leaq	-8(%rsp), %rsi
	movq	%rsi, %rdx
	cmovgq	%rax, %rdx
	cmovgq	%rsi, %rax
	movq	%r11, %r9
	movq	%r10, %r14
	movq	%rbx, %r10
	movq	%r15, %r11
	movq	%rdi, %rsi
.LBB4_3:
	movq	(%rdx), %rdi
	subq	(%rax), %rdi
	orq	$1, %rdi
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	xorl	%edx, %edx
	idivq	%rdi
	addq	%r9, %rax
	movq	%rax, 32(%r8)
	movq	%r14, (%r8)
	movq	%r9, 8(%r8)
	movq	%r10, 16(%r8)
	movq	%r11, 24(%r8)
	movq	%rsi, 56(%r8)
	movq	32(%rcx), %rax
	testq	%rax, %rax
	je	.LBB4_4
# BB#5:
	cmpq	8(%rax), %r9
	jge	.LBB4_6
	.p2align	4, 0x90
.LBB4_9:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdx
	movq	72(%rdx), %rax
	testq	%rax, %rax
	je	.LBB4_10
# BB#11:                                #   in Loop: Header=BB4_9 Depth=1
	cmpq	8(%rax), %r9
	jl	.LBB4_9
# BB#12:
	movb	$1, %sil
	jmp	.LBB4_13
	.p2align	4, 0x90
.LBB4_6:                                # %.preheader130
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdx
	movq	80(%rdx), %rax
	testq	%rax, %rax
	je	.LBB4_14
# BB#7:                                 #   in Loop: Header=BB4_6 Depth=1
	cmpq	8(%rax), %r9
	jg	.LBB4_6
# BB#8:
	movq	%rax, 80(%r8)
	movq	%rdx, 72(%r8)
	movq	%r8, 80(%rdx)
	addq	$72, %rax
	jmp	.LBB4_15
.LBB4_4:
	leaq	24(%rcx), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%r8)
	jmp	.LBB4_15
.LBB4_10:
	xorl	%eax, %eax
	xorl	%esi, %esi
.LBB4_13:                               # %.critedge117
	movq	%rax, 72(%r8)
	movq	%rdx, 80(%r8)
	movq	%r8, 72(%rdx)
	leaq	24(%rcx), %rdx
	addq	$80, %rax
	testb	%sil, %sil
	cmoveq	%rdx, %rax
	jmp	.LBB4_15
.LBB4_14:                               # %.critedge116
	movq	$0, 80(%r8)
	movq	%rdx, 72(%r8)
	addq	$80, %rdx
	movq	%rdx, %rax
.LBB4_15:
	movq	%r8, (%rax)
	movq	%r8, 32(%rcx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	add_y_line, .Lfunc_end4-add_y_line
	.cfi_endproc

	.globl	find_cross_y
	.p2align	4, 0x90
	.type	find_cross_y,@function
find_cross_y:                           # @find_cross_y
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rcx
	movq	(%rdi), %r8
	movq	8(%rdi), %rdx
	cvtsi2sdq	%rcx, %xmm3
	subq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm1
	movq	24(%rsi), %rcx
	movq	(%rsi), %r9
	movq	8(%rsi), %rax
	cvtsi2sdq	%rcx, %xmm4
	subq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm2
	cvtsi2sdq	%r8, %xmm0
	mulsd	%xmm3, %xmm0
	xorps	%xmm3, %xmm3
	cvtsi2sdq	%rdx, %xmm3
	movq	16(%rdi), %rcx
	cvtsi2sdq	%rcx, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm0
	mulsd	%xmm2, %xmm0
	xorps	%xmm3, %xmm3
	cvtsi2sdq	%r9, %xmm3
	mulsd	%xmm4, %xmm3
	xorps	%xmm4, %xmm4
	cvtsi2sdq	%rax, %xmm4
	movq	16(%rsi), %rax
	xorps	%xmm5, %xmm5
	cvtsi2sdq	%rax, %xmm5
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm0
	subq	%r9, %rax
	xorps	%xmm3, %xmm3
	cvtsi2sdq	%rax, %xmm3
	mulsd	%xmm1, %xmm3
	subq	%r8, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm3
	divsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %rax
	retq
.Lfunc_end5:
	.size	find_cross_y, .Lfunc_end5-find_cross_y
	.cfi_endproc

	.globl	insert_x_new
	.p2align	4, 0x90
	.type	insert_x_new,@function
insert_x_new:                           # @insert_x_new
	.cfi_startproc
# BB#0:
	movq	(%rdi), %r8
	movq	120(%rsi), %rdx
	leaq	40(%rsi), %rax
	leaq	120(%rsi), %rcx
	testq	%rdx, %rdx
	jne	.LBB6_2
	jmp	.LBB6_7
	.p2align	4, 0x90
.LBB6_6:                                # %.critedge26.backedge
                                        #   in Loop: Header=BB6_2 Depth=1
	leaq	80(%rax), %rcx
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB6_7
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rax, %rsi
	movq	%rdx, %rax
	cmpq	%r8, 40(%rax)
	jl	.LBB6_6
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	jne	.LBB6_9
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpq	%r8, (%rax)
	jg	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	16(%rax), %rdx
	cmpq	16(%rdi), %rdx
	jl	.LBB6_6
.LBB6_9:                                # %.critedge
	movq	%rax, 80(%rdi)
	movq	%rsi, 72(%rdi)
	movq	%rdi, %rdx
	jmp	.LBB6_8
.LBB6_7:                                # %.critedge29
	movq	%rax, %rdx
	movq	$0, 80(%rdi)
	movq	%rdi, %rax
.LBB6_8:
	movq	%rdx, 72(%rax)
	movq	%rdi, (%rcx)
	movq	%r8, 40(%rdi)
	retq
.Lfunc_end6:
	.size	insert_x_new, .Lfunc_end6-insert_x_new
	.cfi_endproc

	.globl	swap_group
	.p2align	4, 0x90
	.type	swap_group,@function
swap_group:                             # @swap_group
	.cfi_startproc
# BB#0:
	movq	80(%rdi), %rdx
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rax, %rcx
	movq	72(%rcx), %rax
	movzwl	70(%rax), %esi
	andl	$3, %esi
	cmpl	$3, %esi
	je	.LBB7_1
# BB#2:
	movq	%rdi, 80(%rax)
	testq	%rdx, %rdx
	je	.LBB7_4
# BB#3:
	movq	%rcx, 72(%rdx)
.LBB7_4:
	movq	%rdx, 72(%rcx)
	movq	%rax, 80(%rdi)
	.p2align	4, 0x90
.LBB7_5:                                # =>This Inner Loop Header: Depth=1
	movq	72(%rcx), %rdx
	movq	80(%rcx), %rsi
	movq	%rdx, 80(%rcx)
	movq	%rsi, 72(%rcx)
	cmpq	%rax, %rsi
	movq	%rsi, %rcx
	jne	.LBB7_5
# BB#6:
	retq
.Lfunc_end7:
	.size	swap_group, .Lfunc_end7-swap_group
	.cfi_endproc

	.globl	ended_line
	.p2align	4, 0x90
	.type	ended_line,@function
ended_line:                             # @ended_line
	.cfi_startproc
# BB#0:
	movq	56(%rdi), %rcx
	movq	32(%rcx), %rax
	cmpl	$1, 64(%rdi)
	jne	.LBB8_4
# BB#1:
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB8_7
# BB#2:
	cmpl	$0, 16(%rdx)
	jne	.LBB8_8
	jmp	.LBB8_7
.LBB8_4:
	cmpl	$0, 16(%rcx)
	je	.LBB8_7
# BB#5:
	movq	(%rcx), %rdx
	jmp	.LBB8_8
.LBB8_7:
	movq	%rcx, %rdx
.LBB8_8:
	movq	32(%rdx), %r8
	cmpq	%rax, %r8
	jle	.LBB8_10
# BB#9:
	movq	%rdx, 56(%rdi)
	movq	24(%rdx), %rsi
	movq	%rsi, -8(%rsp)
	leaq	16(%rdi), %rax
	movq	16(%rdi), %rcx
	cmpq	%rcx, %rsi
	leaq	-8(%rsp), %rdx
	cmovgq	%rax, %rdx
	cmovgeq	%rsi, %rcx
	subq	(%rdx), %rcx
	orq	$1, %rcx
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	xorl	%edx, %edx
	idivq	%rcx
	addq	24(%rdi), %rax
	movq	%rax, 32(%rdi)
	movups	16(%rdi), %xmm0
	movups	%xmm0, (%rdi)
	movq	%rsi, 16(%rdi)
	movq	%r8, 24(%rdi)
	retq
.LBB8_10:
	movq	72(%rdi), %rax
	movq	80(%rdi), %rcx
	movq	%rcx, 80(%rax)
	testq	%rcx, %rcx
	je	.LBB8_12
# BB#11:
	movq	%rax, 72(%rcx)
	retq
.LBB8_12:
	retq
.Lfunc_end8:
	.size	ended_line, .Lfunc_end8-ended_line
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"active lines"
	.size	.L.str, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
