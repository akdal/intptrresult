	.text
	.file	"libclamav_chmunpack.bc"
	.globl	chm_unpack
	.p2align	4, 0x90
	.type	chm_unpack,@function
chm_unpack:                             # @chm_unpack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$9656, %rsp             # imm = 0x25B8
.Lcfi6:
	.cfi_def_cfa_offset 9712
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$40, %edi
	callq	cli_malloc
	movq	%rax, %r13
	xorl	%r15d, %r15d
	testq	%r13, %r13
	je	.LBB0_23
# BB#1:
	movq	$0, 32(%r13)
	movq	$0, (%r13)
	movl	$40, %edi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_5
# BB#2:
	movq	$0, 32(%r14)
	movq	$0, (%r14)
	leaq	288(%rsp), %rdx
	movl	$1, %edi
	movl	%ebp, %esi
	callq	__fxstat
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB0_6
.LBB0_3:                                # %.thread
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	testq	%rax, %rax
	je	.LBB0_9
# BB#4:
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	jmp	.LBB0_14
.LBB0_5:
	movq	%r13, %rdi
	callq	free
	jmp	.LBB0_23
.LBB0_6:
	movl	%ebp, %r12d
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movq	336(%rsp), %rbp
	xorl	%r15d, %r15d
	cmpq	$96, %rbp
	movl	$0, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	$0, %ebx
	jl	.LBB0_14
# BB#7:
	xorl	%edi, %edi
	movl	$1, %edx
	movl	$2, %ecx
	xorl	%r9d, %r9d
	movq	%rbp, %rsi
	movl	%r12d, %r8d
	callq	mmap
	movq	%rax, %rbx
	leaq	1(%rbx), %rax
	cmpq	$2, %rax
	jae	.LBB0_10
# BB#8:
	movq	%rbp, %rcx
	movq	88(%rsp), %rbx          # 8-byte Reload
	movl	%r12d, %ebp
	jmp	.LBB0_3
.LBB0_9:
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	leaq	192(%rsp), %rsi
	movl	$96, %edx
	movl	%ebp, %r12d
	movl	%ebp, %edi
	callq	cli_readn
	xorl	%ebx, %ebx
	movb	$1, %cl
	cmpl	$96, %eax
	movl	$0, %r15d
	je	.LBB0_11
	jmp	.LBB0_14
.LBB0_10:
	movups	80(%rbx), %xmm0
	movaps	%xmm0, 272(%rsp)
	movups	64(%rbx), %xmm0
	movaps	%xmm0, 256(%rsp)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	48(%rbx), %xmm3
	movaps	%xmm3, 240(%rsp)
	movaps	%xmm2, 224(%rsp)
	movaps	%xmm1, 208(%rsp)
	movaps	%xmm0, 192(%rsp)
	xorl	%ecx, %ecx
	movq	%rbp, 80(%rsp)          # 8-byte Spill
.LBB0_11:                               # %chm_read_data.exit.i
	cmpl	$1179866185, 192(%rsp)  # imm = 0x46535449
	je	.LBB0_24
# BB#12:
	xorl	%r15d, %r15d
	movl	$.L.str.1, %edi
.LBB0_13:                               # %.lr.ph.i63.preheader
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_14:                               # %.lr.ph.i63.preheader
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph.i63
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	movq	32(%r13), %rbx
	testq	%rdi, %rdi
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_15 Depth=1
	callq	free
.LBB0_17:                               #   in Loop: Header=BB0_15 Depth=1
	movq	%r13, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %r13
	jne	.LBB0_15
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph.i65
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdi
	movq	32(%r14), %rbx
	testq	%rdi, %rdi
	je	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_18 Depth=1
	callq	free
.LBB0_20:                               #   in Loop: Header=BB0_18 Depth=1
	movq	%r14, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %r14
	jne	.LBB0_18
# BB#21:                                # %free_file_list.exit66
	movq	%rbp, %rdi
	testq	%rdi, %rdi
	je	.LBB0_23
# BB#22:
	movq	80(%rsp), %rsi          # 8-byte Reload
	callq	munmap
.LBB0_23:
	movl	%r15d, %eax
	addq	$9656, %rsp             # imm = 0x25B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_24:                               # %itsf_read_header.exit
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	192(%rsp), %esi
	movzbl	193(%rsp), %edx
	movzbl	194(%rsp), %ecx
	movzbl	195(%rsp), %r8d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	196(%rsp), %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	200(%rsp), %esi
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	212(%rsp), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	248(%rsp), %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	256(%rsp), %rsi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	264(%rsp), %rsi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	272(%rsp), %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$3, 196(%rsp)
	jl	.LBB0_26
# BB#25:
	movq	280(%rsp), %rsi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_26:                               # %itsf_print_header.exit
	movq	264(%rsp), %rbp
	testq	%rbp, %rbp
	js	.LBB0_14
# BB#27:
	cmpb	$0, 48(%rsp)            # 1-byte Folded Reload
	je	.LBB0_30
# BB#28:
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	movl	%r12d, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	jne	.LBB0_14
# BB#29:
	leaq	96(%rsp), %rsi
	movl	$84, %edx
	movl	%r12d, %edi
	callq	cli_readn
	cmpl	$84, %eax
	jne	.LBB0_14
	jmp	.LBB0_32
.LBB0_30:
	leaq	84(%rbp), %rax
	cmpq	80(%rsp), %rax          # 8-byte Folded Reload
	jg	.LBB0_14
# BB#31:
	movl	80(%rbx,%rbp), %eax
	movl	%eax, 176(%rsp)
	movups	64(%rbx,%rbp), %xmm0
	movaps	%xmm0, 160(%rsp)
	movups	(%rbx,%rbp), %xmm0
	movups	16(%rbx,%rbp), %xmm1
	movups	32(%rbx,%rbp), %xmm2
	movups	48(%rbx,%rbp), %xmm3
	movaps	%xmm3, 144(%rsp)
	movaps	%xmm2, 128(%rsp)
	movaps	%xmm1, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
.LBB0_32:                               # %chm_read_data.exit.i58
	cmpl	$1347638345, 96(%rsp)   # imm = 0x50535449
	je	.LBB0_34
# BB#33:
	xorl	%r15d, %r15d
	movl	$.L.str.13, %edi
	jmp	.LBB0_13
.LBB0_34:
	cmpl	$1, 100(%rsp)
	jne	.LBB0_42
# BB#35:
	cmpl	$84, 104(%rsp)
	jne	.LBB0_42
# BB#36:                                # %itsp_read_header.exit
	movl	%r12d, 24(%rsp)         # 4-byte Spill
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	96(%rsp), %esi
	movzbl	97(%rsp), %edx
	movzbl	98(%rsp), %ecx
	movzbl	99(%rsp), %r8d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	100(%rsp), %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	112(%rsp), %esi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	116(%rsp), %esi
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	120(%rsp), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	124(%rsp), %esi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	128(%rsp), %esi
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	132(%rsp), %esi
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	140(%rsp), %esi
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	144(%rsp), %esi
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	264(%rsp), %rax
	addq	$84, %rax
	movl	128(%rsp), %ecx
	testl	%ecx, %ecx
	movq	%rax, %rdx
	jle	.LBB0_38
# BB#37:
	movl	112(%rsp), %edx
	imull	%ecx, %edx
	addq	%rax, %rdx
.LBB0_38:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movl	132(%rsp), %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	$2, 196(%rsp)
	jg	.LBB0_40
# BB#39:
	movl	140(%rsp), %ecx
	imull	112(%rsp), %ecx
	addq	%rcx, %rax
	movq	%rax, 280(%rsp)
.LBB0_40:                               # %.preheader
	testl	%esi, %esi
	je	.LBB0_105
# BB#41:                                # %.lr.ph.preheader
	movl	112(%rsp), %ebp
	xorl	%r15d, %r15d
	jmp	.LBB0_104
.LBB0_42:
	xorl	%r15d, %r15d
	movl	$.L.str.14, %edi
	jmp	.LBB0_13
.LBB0_43:                               #   in Loop: Header=BB0_104 Depth=1
	movl	$40, %edi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_110
# BB#44:                                #   in Loop: Header=BB0_104 Depth=1
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	%ebp, %edi
	movq	%rdi, %rbp
	callq	cli_malloc
	movq	%rax, %r12
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%r12, 24(%rbx)
	testq	%r12, %r12
	je	.LBB0_114
# BB#45:                                #   in Loop: Header=BB0_104 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	testq	%rdi, %rdi
	movl	48(%rsp), %ecx          # 4-byte Reload
	movq	%rbp, %rdx
	js	.LBB0_108
# BB#46:                                #   in Loop: Header=BB0_104 Depth=1
	testb	%cl, %cl
	je	.LBB0_50
# BB#47:                                #   in Loop: Header=BB0_104 Depth=1
	xorl	%edx, %edx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	lseek
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jne	.LBB0_108
# BB#48:                                #   in Loop: Header=BB0_104 Depth=1
	movl	$8, %edx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	cli_readn
	movq	%rbp, %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	48(%rsp), %ecx          # 4-byte Reload
	cmpl	$8, %eax
	jne	.LBB0_108
# BB#49:                                # %.chm_read_data.exit_crit_edge.i
                                        #   in Loop: Header=BB0_104 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %r12
	jmp	.LBB0_52
.LBB0_50:                               #   in Loop: Header=BB0_104 Depth=1
	leaq	8(%rdi), %rax
	cmpq	80(%rsp), %rax          # 8-byte Folded Reload
	jg	.LBB0_108
# BB#51:                                #   in Loop: Header=BB0_104 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdi), %rax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rax, (%rsi)
.LBB0_52:                               # %chm_read_data.exit.i60
                                        #   in Loop: Header=BB0_104 Depth=1
	movq	%rdx, %rax
	addq	%rdi, %rax
	js	.LBB0_108
# BB#53:                                #   in Loop: Header=BB0_104 Depth=1
	testb	%cl, %cl
	je	.LBB0_56
# BB#54:                                #   in Loop: Header=BB0_104 Depth=1
	xorl	%edx, %edx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	lseek
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jne	.LBB0_108
# BB#55:                                #   in Loop: Header=BB0_104 Depth=1
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	%r12, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_readn
	movq	%rbp, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	48(%rsp), %ecx          # 4-byte Reload
	cltq
	cmpq	%rsi, %rax
	je	.LBB0_58
	jmp	.LBB0_108
.LBB0_56:                               #   in Loop: Header=BB0_104 Depth=1
	cmpq	80(%rsp), %rax          # 8-byte Folded Reload
	jg	.LBB0_108
# BB#57:                                #   in Loop: Header=BB0_104 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rax,%rcx), %rsi
	movq	%r12, %rdi
	callq	memcpy
	movq	%rbp, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	48(%rsp), %ecx          # 4-byte Reload
.LBB0_58:                               # %chm_read_data.exit51.i
                                        #   in Loop: Header=BB0_104 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	cmpl	$1279741264, (%r12)     # imm = 0x4C474D50
	je	.LBB0_63
# BB#59:                                #   in Loop: Header=BB0_104 Depth=1
	cmpl	$1229409616, (%r12)     # imm = 0x49474D50
	jne	.LBB0_170
.LBB0_60:                               # %read_chunk_entries.exit.i
                                        #   in Loop: Header=BB0_104 Depth=1
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	(%r12), %esi
	movzbl	1(%r12), %edx
	movzbl	2(%r12), %ecx
	movzbl	3(%r12), %r8d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%r12), %esi
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$1279741264, (%r12)     # imm = 0x4C474D50
	jne	.LBB0_62
# BB#61:                                # %.critedge83
                                        #   in Loop: Header=BB0_104 Depth=1
	movl	12(%r12), %esi
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	16(%r12), %esi
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	32(%r12), %esi
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_62:                               # %.critedge
                                        #   in Loop: Header=BB0_104 Depth=1
	movq	24(%r12), %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movl	112(%rsp), %ebp
	movq	(%rsp), %rdx            # 8-byte Reload
	addq	%rbp, %rdx
	movl	44(%rsp), %esi          # 4-byte Reload
	decl	%esi
	jne	.LBB0_104
	jmp	.LBB0_105
.LBB0_63:                               #   in Loop: Header=BB0_104 Depth=1
	movq	%r12, %rbx
	addq	$8, %rbx
	leaq	8(%rdx), %r12
	testb	%cl, %cl
	je	.LBB0_66
# BB#64:                                #   in Loop: Header=BB0_104 Depth=1
	xorl	%edx, %edx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	%r12, %rsi
	callq	lseek
	cmpq	%r12, %rax
	jne	.LBB0_108
# BB#65:                                #   in Loop: Header=BB0_104 Depth=1
	movl	$12, %edx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	%rbx, %rsi
	callq	cli_readn
	movq	%rbp, %rsi
	cmpl	$12, %eax
	je	.LBB0_68
	jmp	.LBB0_108
.LBB0_66:                               #   in Loop: Header=BB0_104 Depth=1
	leaq	20(%rdx), %rax
	cmpq	80(%rsp), %rax          # 8-byte Folded Reload
	jg	.LBB0_108
# BB#67:                                #   in Loop: Header=BB0_104 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	8(%rcx,%r12), %eax
	movl	%eax, 8(%rbx)
	movq	(%rcx,%r12), %rax
	movq	%rax, (%rbx)
.LBB0_68:                               # %chm_read_data.exit53.i
                                        #   in Loop: Header=BB0_104 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	24(%r12), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	leal	-2(%rdx), %eax
	movzbl	(%rcx,%rax), %eax
	decl	%edx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movzbl	(%rcx,%rdx), %ebx
	shll	$8, %ebx
	orl	%eax, %ebx
	movw	%bx, 32(%r12)
	testw	%bx, %bx
	je	.LBB0_60
# BB#69:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB0_104 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	addq	%rax, %rsi
	leaq	20(%rax), %rax
	movq	%rsi, 8(%rsp)           # 8-byte Spill
.LBB0_70:                               # %.lr.ph.i.i
                                        #   Parent Loop BB0_104 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_73 Depth 3
                                        #       Child Loop BB0_83 Depth 3
                                        #       Child Loop BB0_88 Depth 3
                                        #       Child Loop BB0_93 Depth 3
	movq	%r12, %rbp
	movq	%rax, %r12
	cmpq	%rsi, %rax
	ja	.LBB0_100
# BB#71:                                #   in Loop: Header=BB0_70 Depth=2
	movl	$40, %edi
	callq	cli_malloc
	movq	%rax, %rsi
	testq	%rsi, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r12, %rdi
	movq	%rbp, %r12
	je	.LBB0_60
# BB#72:                                #   in Loop: Header=BB0_70 Depth=2
	decl	%ebx
	movl	%ebx, 56(%rsp)          # 4-byte Spill
	movq	$0, 32(%rsi)
	xorl	%ebx, %ebx
	movq	%rdi, %r12
.LBB0_73:                               # %.preheader.i.i.i
                                        #   Parent Loop BB0_104 Depth=1
                                        #     Parent Loop BB0_70 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rdx, %r12
	ja	.LBB0_75
# BB#74:                                #   in Loop: Header=BB0_73 Depth=3
	shlq	$7, %rbx
	movzbl	(%r12), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	orq	%rcx, %rbx
	incq	%r12
	testb	%al, %al
	js	.LBB0_73
	jmp	.LBB0_76
.LBB0_75:                               #   in Loop: Header=BB0_70 Depth=2
	movq	%rdi, %r12
	xorl	%ebx, %ebx
.LBB0_76:                               # %read_enc_int.exit.i.i
                                        #   in Loop: Header=BB0_70 Depth=2
	leaq	(%r12,%rbx), %rax
	cmpq	%rdx, %rax
	ja	.LBB0_101
# BB#77:                                # %read_enc_int.exit.i.i
                                        #   in Loop: Header=BB0_70 Depth=2
	cmpq	72(%rsp), %rax          # 8-byte Folded Reload
	jb	.LBB0_101
# BB#78:                                #   in Loop: Header=BB0_70 Depth=2
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	cmpq	$16777216, %rbx         # imm = 0x1000000
	jb	.LBB0_80
# BB#79:                                #   in Loop: Header=BB0_70 Depth=2
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.29, %edi
	callq	cli_strdup
	movq	%rbp, %rdi
	movq	%rax, (%rdi)
	testq	%rax, %rax
	jne	.LBB0_82
	jmp	.LBB0_103
.LBB0_80:                               #   in Loop: Header=BB0_70 Depth=2
	leaq	1(%rbx), %rdi
	callq	cli_malloc
	movq	%rax, (%rbp)
	testq	%rax, %rax
	je	.LBB0_102
# BB#81:                                #   in Loop: Header=BB0_70 Depth=2
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	strncpy
	movq	(%rbp), %rax
	movb	$0, (%rax,%rbx)
	movq	%rbp, %rdi
.LBB0_82:                               # %.preheader.i17.i.i.preheader
                                        #   in Loop: Header=BB0_70 Depth=2
	xorl	%esi, %esi
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB0_83:                               # %.preheader.i17.i.i
                                        #   Parent Loop BB0_104 Depth=1
                                        #     Parent Loop BB0_70 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r8, %rcx
	ja	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_83 Depth=3
	shlq	$7, %rsi
	movzbl	(%rcx), %eax
	movl	%eax, %edx
	andl	$127, %edx
	orq	%rdx, %rsi
	incq	%rcx
	testb	%al, %al
	js	.LBB0_83
	jmp	.LBB0_86
.LBB0_85:                               #   in Loop: Header=BB0_70 Depth=2
	movq	%rbp, %rcx
	xorl	%esi, %esi
.LBB0_86:                               # %read_enc_int.exit19.i.i
                                        #   in Loop: Header=BB0_70 Depth=2
	movq	%rsi, 8(%rdi)
	cmpq	%r8, %rcx
	ja	.LBB0_90
# BB#87:                                # %.preheader.i12.i.i.preheader
                                        #   in Loop: Header=BB0_70 Depth=2
	xorl	%edx, %edx
	movq	%rcx, %r10
.LBB0_88:                               # %.preheader.i12.i.i
                                        #   Parent Loop BB0_104 Depth=1
                                        #     Parent Loop BB0_70 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r8, %r10
	ja	.LBB0_90
# BB#89:                                #   in Loop: Header=BB0_88 Depth=3
	shlq	$7, %rdx
	movzbl	(%r10), %r9d
	movl	%r9d, %ebp
	andl	$127, %ebp
	orq	%rbp, %rdx
	incq	%r10
	testb	%r9b, %r9b
	js	.LBB0_88
	jmp	.LBB0_91
.LBB0_90:                               #   in Loop: Header=BB0_70 Depth=2
	movq	%rcx, %r10
	xorl	%edx, %edx
.LBB0_91:                               # %read_enc_int.exit14.i.i
                                        #   in Loop: Header=BB0_70 Depth=2
	movq	%rdx, 16(%rdi)
	cmpq	%r8, %r10
	ja	.LBB0_95
# BB#92:                                # %.preheader.i7.i.i.preheader
                                        #   in Loop: Header=BB0_70 Depth=2
	xorl	%ecx, %ecx
	movq	%r10, %rax
.LBB0_93:                               # %.preheader.i7.i.i
                                        #   Parent Loop BB0_104 Depth=1
                                        #     Parent Loop BB0_70 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r8, %rax
	ja	.LBB0_95
# BB#94:                                #   in Loop: Header=BB0_93 Depth=3
	shlq	$7, %rcx
	movzbl	(%rax), %r9d
	movl	%r9d, %ebp
	andl	$127, %ebp
	orq	%rbp, %rcx
	incq	%rax
	testb	%r9b, %r9b
	js	.LBB0_93
	jmp	.LBB0_96
.LBB0_95:                               #   in Loop: Header=BB0_70 Depth=2
	movq	%r10, %rax
	xorl	%ecx, %ecx
.LBB0_96:                               # %read_enc_int.exit9.i.i
                                        #   in Loop: Header=BB0_70 Depth=2
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rdi)
	movq	(%rdi), %r8
	cmpq	$2, %rbx
	movq	%r13, %rax
	jb	.LBB0_99
# BB#97:                                #   in Loop: Header=BB0_70 Depth=2
	cmpb	$58, (%r8)
	movq	%r13, %rax
	jne	.LBB0_99
# BB#98:                                #   in Loop: Header=BB0_70 Depth=2
	cmpb	$58, 1(%r8)
	movq	%r13, %rax
	cmoveq	%r14, %rax
.LBB0_99:                               # %read_enc_int.exit9._crit_edge.i.i
                                        #   in Loop: Header=BB0_70 Depth=2
	movq	32(%rax), %rbp
	movq	%rbp, 32(%rdi)
	movq	%rdi, 32(%rax)
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	56(%rsp), %ebx          # 4-byte Reload
	testw	%bx, %bx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	64(%rsp), %rax          # 8-byte Reload
	jne	.LBB0_70
	jmp	.LBB0_60
.LBB0_100:                              #   in Loop: Header=BB0_104 Depth=1
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbp, %r12
	jmp	.LBB0_60
.LBB0_101:                              #   in Loop: Header=BB0_104 Depth=1
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	movq	%rsi, %rbx
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	jmp	.LBB0_103
.LBB0_102:                              #   in Loop: Header=BB0_104 Depth=1
	movq	%rbp, %rdi
.LBB0_103:                              #   in Loop: Header=BB0_104 Depth=1
	callq	free
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_60
.LBB0_104:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_70 Depth 2
                                        #       Child Loop BB0_73 Depth 3
                                        #       Child Loop BB0_83 Depth 3
                                        #       Child Loop BB0_88 Depth 3
                                        #       Child Loop BB0_93 Depth 3
	movl	%esi, 44(%rsp)          # 4-byte Spill
	movq	%rdx, (%rsp)            # 8-byte Spill
	leal	-8(%rbp), %eax
	cmpl	$33554424, %eax         # imm = 0x1FFFFF8
	jbe	.LBB0_43
	jmp	.LBB0_110
.LBB0_105:                              # %._crit_edge
	leaq	432(%rsp), %rbp
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.36, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	88(%rsp), %rcx          # 8-byte Reload
	callq	snprintf
	movl	$577, %esi              # imm = 0x241
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	testl	%eax, %eax
	js	.LBB0_111
# BB#106:                               # %read_sys_control.exit.preheader.i
	movq	32(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB0_112
# BB#107:                               # %.lr.ph28.i
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movl	$0, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$0, %r15d
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r12d, %r12d
	jmp	.LBB0_131
.LBB0_108:                              # %read_chunk.exit
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	24(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
.LBB0_109:                              # %.lr.ph.i63.preheader
	callq	free
.LBB0_110:                              # %.lr.ph.i63.preheader
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_14
.LBB0_111:
	leaq	432(%rsp), %rsi
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_113
.LBB0_112:                              # %.thread63.i
	movl	%eax, %edi
	callq	close
.LBB0_113:                              # %.lr.ph.i63.preheader
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	$1, %r15d
	jmp	.LBB0_14
.LBB0_114:
	movq	16(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_109
.LBB0_115:                              #   in Loop: Header=BB0_131 Depth=1
	cmpl	$1, %ebp
	je	.LBB0_118
# BB#116:                               #   in Loop: Header=BB0_131 Depth=1
	cmpl	$2, %ebp
	jne	.LBB0_125
# BB#117:                               #   in Loop: Header=BB0_131 Depth=1
	movl	(%rsp), %ecx            # 4-byte Reload
	shll	$15, %ecx
	movq	%r12, %rax
	movl	%ecx, 12(%rax)
	shll	$15, 16(%rax)
.LBB0_118:                              # %print_sys_control.exit.i.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	(%r12), %esi
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	4(%r12), %esi
	movzbl	5(%r12), %edx
	movzbl	6(%r12), %ecx
	movzbl	7(%r12), %r8d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%r12), %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	12(%r12), %esi
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	16(%r12), %esi
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	20(%r12), %esi
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r12, (%rsp)            # 8-byte Spill
	jmp	.LBB0_157
.LBB0_119:                              #   in Loop: Header=BB0_131 Depth=1
	cmpq	80(%rsp), %rbp          # 8-byte Folded Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	jg	.LBB0_129
# BB#120:                               #   in Loop: Header=BB0_131 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	32(%rcx,%r12), %eax
	movl	%eax, 32(%rdx)
	movups	(%rcx,%r12), %xmm0
	movups	16(%rcx,%r12), %xmm1
	movups	%xmm1, 16(%rdx)
	movups	%xmm0, (%rdx)
.LBB0_121:                              # %chm_read_data.exit.i2.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movq	28(%rdx), %rsi
	cmpq	$32768, %rsi            # imm = 0x8000
	jne	.LBB0_126
# BB#122:                               #   in Loop: Header=BB0_131 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	4(%rax), %esi
	cmpl	$4, %esi
	je	.LBB0_124
# BB#123:                               #   in Loop: Header=BB0_131 Depth=1
	cmpl	$8, %esi
	jne	.LBB0_127
.LBB0_124:                              # %print_sys_reset_table.exit.i.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movl	$.L.str.62, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	(%rbp), %esi
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%rbp), %esi
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%rbp), %esi
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	12(%rbp), %rsi
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	20(%rbp), %rsi
	movl	$.L.str.67, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	28(%rbp), %rsi
	movl	$.L.str.68, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	64(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_157
.LBB0_125:                              #   in Loop: Header=BB0_131 Depth=1
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	jmp	.LBB0_155
.LBB0_126:                              #   in Loop: Header=BB0_131 Depth=1
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_128
.LBB0_127:                              #   in Loop: Header=BB0_131 Depth=1
	movl	$.L.str.61, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_128:                              # %chm_read_data.exit.thread.i3.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB0_129:                              # %chm_read_data.exit.thread.i3.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movq	%rdx, %rdi
	callq	free
.LBB0_130:                              #   in Loop: Header=BB0_131 Depth=1
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	64(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_157
.LBB0_131:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbp
	movl	$.L.str.38, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_136
# BB#132:                               #   in Loop: Header=BB0_131 Depth=1
	movl	$.L.str.39, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_142
# BB#133:                               #   in Loop: Header=BB0_131 Depth=1
	movl	$.L.str.40, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_157
# BB#134:                               #   in Loop: Header=BB0_131 Depth=1
	cmpq	$40, 24(%rbx)
	jae	.LBB0_145
# BB#135:                               #   in Loop: Header=BB0_131 Depth=1
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jmp	.LBB0_157
.LBB0_136:                              #   in Loop: Header=BB0_131 Depth=1
	cmpq	$28, 24(%rbx)
	jne	.LBB0_156
# BB#137:                               #   in Loop: Header=BB0_131 Depth=1
	movq	16(%rbx), %rbp
	addq	280(%rsp), %rbp
	js	.LBB0_156
# BB#138:                               #   in Loop: Header=BB0_131 Depth=1
	movl	$24, %edi
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_156
# BB#139:                               #   in Loop: Header=BB0_131 Depth=1
	cmpb	$0, 48(%rsp)            # 1-byte Folded Reload
	je	.LBB0_151
# BB#140:                               #   in Loop: Header=BB0_131 Depth=1
	xorl	%edx, %edx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	jne	.LBB0_155
# BB#141:                               #   in Loop: Header=BB0_131 Depth=1
	movl	$24, %edx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$24, %eax
	je	.LBB0_153
	jmp	.LBB0_155
.LBB0_142:                              #   in Loop: Header=BB0_131 Depth=1
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_144
# BB#143:                               #   in Loop: Header=BB0_131 Depth=1
	movq	16(%rbx), %rax
	addq	280(%rsp), %rax
	movq	%rax, (%r15)
	movq	24(%rbx), %rax
	movq	%rax, 8(%r15)
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%r15), %rsi
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%r15), %rsi
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r15, 16(%rsp)          # 8-byte Spill
	jmp	.LBB0_157
.LBB0_144:                              #   in Loop: Header=BB0_131 Depth=1
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB0_157
.LBB0_145:                              #   in Loop: Header=BB0_131 Depth=1
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	16(%rbx), %rbp
	addq	280(%rsp), %rbp
	movq	%rbp, %r12
	addq	$4, %r12
	js	.LBB0_130
# BB#146:                               #   in Loop: Header=BB0_131 Depth=1
	movl	$44, %edi
	callq	cli_malloc
	movq	%rax, %rdx
	testq	%rdx, %rdx
	je	.LBB0_130
# BB#147:                               #   in Loop: Header=BB0_131 Depth=1
	movq	%rbp, 36(%rdx)
	addq	$40, %rbp
	js	.LBB0_129
# BB#148:                               #   in Loop: Header=BB0_131 Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	cmpb	$0, 48(%rsp)            # 1-byte Folded Reload
	je	.LBB0_119
# BB#149:                               #   in Loop: Header=BB0_131 Depth=1
	xorl	%edx, %edx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	%r12, %rsi
	callq	lseek
	cmpq	%r12, %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	jne	.LBB0_129
# BB#150:                               #   in Loop: Header=BB0_131 Depth=1
	movl	$36, %edx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	cli_readn
	movq	8(%rsp), %rdx           # 8-byte Reload
	cmpl	$36, %eax
	je	.LBB0_121
	jmp	.LBB0_129
.LBB0_151:                              #   in Loop: Header=BB0_131 Depth=1
	leaq	24(%rbp), %rax
	cmpq	80(%rsp), %rax          # 8-byte Folded Reload
	jg	.LBB0_155
# BB#152:                               #   in Loop: Header=BB0_131 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx,%rbp), %rax
	movq	%rax, 16(%r12)
	movups	(%rcx,%rbp), %xmm0
	movups	%xmm0, (%r12)
.LBB0_153:                              # %chm_read_data.exit.i.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movl	8(%r12), %ebp
	movq	%r12, %rax
	movl	12(%rax), %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	movq	%rax, %rsi
	addq	$4, %rsi
	movl	$.L.str.49, %edi
	movl	$4, %edx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_115
# BB#154:                               #   in Loop: Header=BB0_131 Depth=1
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_155:                              # %chm_read_data.exit.thread.i.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movq	%r12, %rdi
	callq	free
.LBB0_156:                              #   in Loop: Header=BB0_131 Depth=1
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB0_157:                              # %read_sys_control.exit.backedge.i
                                        #   in Loop: Header=BB0_131 Depth=1
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_131
# BB#158:                               # %read_sys_control.exit._crit_edge.i
	movq	%r12, 64(%rsp)          # 8-byte Spill
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB0_181
# BB#159:                               # %read_sys_control.exit._crit_edge.i
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB0_181
# BB#160:                               # %read_sys_control.exit._crit_edge.i
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB0_181
# BB#161:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	16(%rax), %esi
	cmpl	$262143, %esi           # imm = 0x3FFFF
	jle	.LBB0_166
# BB#162:
	cmpl	$1048575, %esi          # imm = 0xFFFFF
	jg	.LBB0_171
# BB#163:
	cmpl	$262144, %esi           # imm = 0x40000
	je	.LBB0_174
# BB#164:
	cmpl	$524288, %esi           # imm = 0x80000
	jne	.LBB0_212
# BB#165:
	movl	$19, %r12d
	jmp	.LBB0_178
.LBB0_166:
	cmpl	$32768, %esi            # imm = 0x8000
	je	.LBB0_175
# BB#167:
	cmpl	$65536, %esi            # imm = 0x10000
	je	.LBB0_176
# BB#168:
	cmpl	$131072, %esi           # imm = 0x20000
	jne	.LBB0_212
# BB#169:
	movl	$17, %r12d
	jmp	.LBB0_178
.LBB0_170:                              # %itsp_read_header.exit.thread.critedge
	movq	24(%r12), %rdi
	callq	free
	movq	%r12, %rdi
	jmp	.LBB0_109
.LBB0_171:
	cmpl	$1048576, %esi          # imm = 0x100000
	je	.LBB0_177
# BB#172:
	cmpl	$2097152, %esi          # imm = 0x200000
	jne	.LBB0_212
# BB#173:
	movl	$21, %r12d
	jmp	.LBB0_178
.LBB0_174:
	movl	$18, %r12d
	jmp	.LBB0_178
.LBB0_175:
	movl	$15, %r12d
	jmp	.LBB0_178
.LBB0_176:
	movl	$16, %r12d
	jmp	.LBB0_178
.LBB0_177:
	movl	$20, %r12d
.LBB0_178:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	12(%rax), %ebp
	testw	$32767, %bp             # imm = 0x7FFF
	je	.LBB0_190
# BB#179:
	movl	$.L.str.42, %edi
.LBB0_180:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_181:
	movl	44(%rsp), %edi          # 4-byte Reload
	testl	%edi, %edi
	js	.LBB0_183
# BB#182:                               # %.thread49.i
	callq	close
.LBB0_183:                              # %.thread.i
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB0_185
.LBB0_184:
	movq	%r15, %rdi
	callq	free
.LBB0_185:
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB0_187
# BB#186:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
.LBB0_187:
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_189
.LBB0_188:
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	free
.LBB0_189:                              # %chm_decompress_stream.exit
	movl	$1, %r15d
	jmp	.LBB0_14
.LBB0_190:
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %eax
	addl	%ebp, %eax
	negl	%ebp
	andl	%eax, %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%rsp), %rax            # 8-byte Reload
	movl	12(%rax), %ecx
	shrl	$15, %ecx
	movslq	%ebp, %r9
	movl	$4096, %r8d             # imm = 0x1000
	movl	24(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %edi
	movl	44(%rsp), %esi          # 4-byte Reload
	movl	%r12d, %edx
	movq	%r9, %r12
	pushq	$0
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	lzx_init
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %rbp
	xorl	%edx, %edx
	movl	%ebx, %edi
	movq	48(%rsp), %rsi          # 8-byte Reload
	callq	lseek
	testq	%rbp, %rbp
	je	.LBB0_213
# BB#191:
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	lzx_decompress
	movq	%rbp, %rdi
	callq	lzx_free
	movq	32(%r13), %rbx
	movl	44(%rsp), %edi          # 4-byte Reload
	callq	close
	leaq	432(%rsp), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, 48(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	js	.LBB0_214
# BB#192:
	leaq	432(%rsp), %rdi
	callq	unlink
	testq	%rbx, %rbx
	je	.LBB0_211
# BB#193:                               # %.lr.ph.lr.ph.i
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
.LBB0_194:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_199 Depth 2
	cmpq	$1, 8(%rbx)
	jne	.LBB0_203
# BB#195:                               #   in Loop: Header=BB0_194 Depth=1
	movq	16(%rbx), %rsi
	xorl	%edx, %edx
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	lseek
	movq	%rax, %rbp
	cmpq	16(%rbx), %rbp
	jne	.LBB0_202
# BB#196:                               #   in Loop: Header=BB0_194 Depth=1
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.47, %edx
	xorl	%eax, %eax
	leaq	432(%rsp), %r12
	movq	%r12, %rdi
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	16(%rsp), %r8d          # 4-byte Reload
	movq	%rbp, %r9
	callq	snprintf
	movl	$577, %esi              # imm = 0x241
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	open
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB0_203
# BB#197:                               #   in Loop: Header=BB0_194 Depth=1
	movq	24(%rbx), %rcx
	movq	%rcx, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	testq	%rcx, %rcx
	je	.LBB0_204
# BB#198:                               # %.lr.ph.i.i62.preheader
                                        #   in Loop: Header=BB0_194 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB0_199:                              # %.lr.ph.i.i62
                                        #   Parent Loop BB0_194 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpq	$8192, %rax             # imm = 0x2000
	movl	$8192, %ebp             # imm = 0x2000
	cmovbq	%rax, %rbp
	movl	48(%rsp), %edi          # 4-byte Reload
	leaq	1456(%rsp), %rsi
	movl	%ebp, %edx
	callq	cli_readn
	movl	%eax, %ecx
	cltq
	cmpq	%rbp, %rax
	jne	.LBB0_205
# BB#200:                               #   in Loop: Header=BB0_199 Depth=2
	movl	%r12d, %edi
	leaq	1456(%rsp), %rsi
	movl	%ecx, %edx
	movl	%edx, 44(%rsp)          # 4-byte Spill
	callq	cli_writen
	cmpl	44(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB0_206
# BB#201:                               #   in Loop: Header=BB0_199 Depth=2
	subq	%rbp, 24(%rsp)          # 8-byte Folded Spill
	jne	.LBB0_199
	jmp	.LBB0_208
.LBB0_202:                              #   in Loop: Header=BB0_194 Depth=1
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_203:                              # %.backedge.i
                                        #   in Loop: Header=BB0_194 Depth=1
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_194
	jmp	.LBB0_211
.LBB0_204:                              #   in Loop: Header=BB0_194 Depth=1
	xorl	%eax, %eax
	jmp	.LBB0_207
.LBB0_205:                              #   in Loop: Header=BB0_194 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	subq	%rax, 72(%rsp)          # 8-byte Folded Spill
	jmp	.LBB0_208
.LBB0_206:                              #   in Loop: Header=BB0_194 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	subq	24(%rsp), %rax          # 8-byte Folded Reload
	subq	%rbp, %rax
.LBB0_207:                              # %chm_copy_file_data.exit.i
                                        #   in Loop: Header=BB0_194 Depth=1
	movq	%rax, 72(%rsp)          # 8-byte Spill
.LBB0_208:                              # %chm_copy_file_data.exit.i
                                        #   in Loop: Header=BB0_194 Depth=1
	movq	24(%rbx), %rsi
	cmpq	%rsi, 72(%rsp)          # 8-byte Folded Reload
	je	.LBB0_210
# BB#209:                               #   in Loop: Header=BB0_194 Depth=1
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_210:                              # %.outer.i
                                        #   in Loop: Header=BB0_194 Depth=1
	movl	%r12d, %edi
	callq	close
	movq	32(%rbx), %rbx
	incl	16(%rsp)                # 4-byte Folded Spill
	testq	%rbx, %rbx
	jne	.LBB0_194
.LBB0_211:                              # %.outer._crit_edge.i
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	close
	jmp	.LBB0_184
.LBB0_212:
	movl	$.L.str.41, %edi
	jmp	.LBB0_180
.LBB0_213:
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_181
.LBB0_214:
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_188
.Lfunc_end0:
	.size	chm_unpack, .Lfunc_end0-chm_unpack
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ITSF"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ITSF signature mismatch\n"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"---- ITSF ----\n"
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Signature:\t%c%c%c%c\n"
	.size	.L.str.3, 21

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Version:\t%d\n"
	.size	.L.str.4, 13

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Header len:\t%ld\n"
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Lang ID:\t%d\n"
	.size	.L.str.6, 13

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Sec0 offset:\t%llu\n"
	.size	.L.str.7, 19

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Sec0 len:\t%llu\n"
	.size	.L.str.8, 16

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Dir offset:\t%llu\n"
	.size	.L.str.9, 18

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Dir len:\t%llu\n"
	.size	.L.str.10, 15

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Data offset:\t%llu\n\n"
	.size	.L.str.11, 20

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"ITSP"
	.size	.L.str.12, 5

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"ITSP signature mismatch\n"
	.size	.L.str.13, 25

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"ITSP header mismatch\n"
	.size	.L.str.14, 22

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"---- ITSP ----\n"
	.size	.L.str.15, 16

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Block len:\t%ld\n"
	.size	.L.str.16, 16

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Block idx int:\t%d\n"
	.size	.L.str.17, 19

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Index depth:\t%d\n"
	.size	.L.str.18, 17

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Index root:\t%d\n"
	.size	.L.str.19, 16

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Index head:\t%u\n"
	.size	.L.str.20, 16

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Index tail:\t%u\n"
	.size	.L.str.21, 16

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Num Blocks:\t%u\n"
	.size	.L.str.22, 16

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Lang ID:\t%lu\n\n"
	.size	.L.str.23, 15

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"PMGL"
	.size	.L.str.24, 5

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"PMGI"
	.size	.L.str.25, 5

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"read chunk entries failed\n"
	.size	.L.str.26, 27

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Bad CHM name_len detected\n"
	.size	.L.str.27, 27

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"CHM file name too long: %llu\n"
	.size	.L.str.28, 30

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"truncated"
	.size	.L.str.29, 10

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Section: %llu Offset: %llu Length: %llu, Name: %s\n"
	.size	.L.str.30, 51

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"---- Chunk ----\n"
	.size	.L.str.31, 17

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Free Space:\t%u\n"
	.size	.L.str.32, 16

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Prev Block:\t%d\n"
	.size	.L.str.33, 16

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Next Block:\t%d\n"
	.size	.L.str.34, 16

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Num entries:\t%d\n\n"
	.size	.L.str.35, 18

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"%s/clamav-unchm.bin"
	.size	.L.str.36, 20

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"open failed for %s\n"
	.size	.L.str.37, 20

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"::DataSpace/Storage/MSCompressed/ControlData"
	.size	.L.str.38, 45

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"::DataSpace/Storage/MSCompressed/Content"
	.size	.L.str.39, 41

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"::DataSpace/Storage/MSCompressed/Transform/{7FC28940-9D31-11D0-9B27-00A0C91E9C7C}/InstanceData/ResetTable"
	.size	.L.str.40, 106

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"bad control window size: 0x%x\n"
	.size	.L.str.41, 31

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"bad reset_interval: 0x%x\n"
	.size	.L.str.42, 26

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"Compressed offset: %llu\n"
	.size	.L.str.43, 25

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"lzx_init failed\n"
	.size	.L.str.44, 17

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"re-open output failed\n"
	.size	.L.str.45, 23

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"seek in output failed\n"
	.size	.L.str.46, 23

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"%s/%d-%llu.chm"
	.size	.L.str.47, 15

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"failed to copy %lu bytes\n"
	.size	.L.str.48, 26

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"LZXC"
	.size	.L.str.49, 5

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"bad sys_control signature"
	.size	.L.str.50, 26

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Unknown sys_control version:%d\n"
	.size	.L.str.51, 32

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"---- Control ----\n"
	.size	.L.str.52, 19

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"Length:\t\t%lu\n"
	.size	.L.str.53, 14

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"Reset Interval:\t%d\n"
	.size	.L.str.54, 20

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"Window Size:\t%d\n"
	.size	.L.str.55, 17

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"Cache Size:\t%d\n\n"
	.size	.L.str.56, 17

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"---- Content ----\n"
	.size	.L.str.57, 19

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"Offset:\t%llu\n"
	.size	.L.str.58, 14

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"Length:\t%llu\n\n"
	.size	.L.str.59, 15

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"bad sys_reset_table frame_len: 0x%x\n"
	.size	.L.str.60, 37

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"bad sys_reset_table entry_size: 0x%x\n"
	.size	.L.str.61, 38

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"---- Reset Table ----\n"
	.size	.L.str.62, 23

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"Num Entries:\t%lu\n"
	.size	.L.str.63, 18

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"Entry Size:\t%lu\n"
	.size	.L.str.64, 17

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"Table Offset:\t%lu\n"
	.size	.L.str.65, 19

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"Uncom Len:\t%llu\n"
	.size	.L.str.66, 17

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"Com Len:\t%llu\n"
	.size	.L.str.67, 15

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"Frame Len:\t%llu\n\n"
	.size	.L.str.68, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
