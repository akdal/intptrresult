	.text
	.file	"jacobi-2d-imper.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4611686018427387904     # double 2
.LCPI6_1:
	.quad	4652007308841189376     # double 1000
.LCPI6_2:
	.quad	4613937818241073152     # double 3
.LCPI6_4:
	.quad	4596373779694328218     # double 0.20000000000000001
.LCPI6_6:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_3:
	.quad	4596373779694328218     # double 0.20000000000000001
	.quad	4596373779694328218     # double 0.20000000000000001
.LCPI6_5:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 128
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_78
# BB#1:
	movq	(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_78
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_78
# BB#3:                                 # %polybench_alloc_data.exit
	movq	(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_78
# BB#4:                                 # %polybench_alloc_data.exit32
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_78
# BB#5:                                 # %polybench_alloc_data.exit32
	movq	(%rsp), %r13
	testq	%r13, %r13
	je	.LBB6_78
# BB#6:                                 # %polybench_alloc_data.exit34
	xorl	%eax, %eax
	movsd	.LCPI6_0(%rip), %xmm8   # xmm8 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_2(%rip), %xmm4   # xmm4 = mem[0],zero
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_7:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	$-1000, %rdx            # imm = 0xFC18
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_8:                                #   Parent Loop BB6_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1002(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm8, %xmm1
	divsd	%xmm3, %xmm1
	movsd	%xmm1, (%r14,%rsi)
	leal	1003(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm4, %xmm1
	divsd	%xmm3, %xmm1
	movsd	%xmm1, (%r13,%rsi)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB6_8
# BB#9:                                 #   in Loop: Header=BB6_7 Depth=1
	incq	%rcx
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB6_7
# BB#10:                                # %.preheader3.i.preheader
	leaq	16024(%r14), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	8024(%r13), %r12
	leaq	8088(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	8088(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	8000(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8000(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	.LCPI6_3(%rip), %xmm1   # xmm1 = [2.000000e-01,2.000000e-01]
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movq	%r12, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_11:                               # %.preheader3.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_12 Depth 2
                                        #       Child Loop BB6_16 Depth 3
                                        #       Child Loop BB6_19 Depth 3
                                        #     Child Loop BB6_22 Depth 2
                                        #       Child Loop BB6_26 Depth 3
                                        #       Child Loop BB6_29 Depth 3
                                        #       Child Loop BB6_32 Depth 3
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	$8000, %eax             # imm = 0x1F40
	movq	%r12, %r9
	movq	48(%rsp), %rbp          # 8-byte Reload
	xorl	%edi, %edi
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB6_12:                               # %.preheader1.i
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_16 Depth 3
                                        #       Child Loop BB6_19 Depth 3
	imulq	$8000, %rdi, %rcx       # imm = 0x1F40
	leaq	8008(%r13,%rcx), %rdx
	leaq	23992(%r14,%rcx), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB6_15
# BB#13:                                # %.preheader1.i
                                        #   in Loop: Header=BB6_12 Depth=2
	leaq	15992(%r13,%rcx), %rdx
	leaq	8(%r14,%rcx), %rcx
	cmpq	%rdx, %rcx
	jae	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_12 Depth=2
	movl	$1, %ebx
	jmp	.LBB6_18
	.p2align	4, 0x90
.LBB6_15:                               # %vector.body99.preheader
                                        #   in Loop: Header=BB6_12 Depth=2
	movl	$996, %edx              # imm = 0x3E4
	movq	%r9, %rcx
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB6_16:                               # %vector.body99
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-8016(%rsi), %xmm5
	movupd	-8000(%rsi), %xmm6
	movupd	-8024(%rsi), %xmm7
	movupd	-8008(%rsi), %xmm2
	addpd	%xmm5, %xmm7
	addpd	%xmm2, %xmm6
	movupd	-7992(%rsi), %xmm5
	addpd	%xmm2, %xmm7
	addpd	%xmm6, %xmm5
	movupd	-16(%rsi), %xmm2
	movupd	(%rsi), %xmm6
	addpd	%xmm7, %xmm2
	addpd	%xmm5, %xmm6
	movupd	-16016(%rsi), %xmm5
	movupd	-16000(%rsi), %xmm7
	addpd	%xmm2, %xmm5
	addpd	%xmm6, %xmm7
	mulpd	%xmm1, %xmm5
	mulpd	%xmm1, %xmm7
	movupd	%xmm5, -16(%rcx)
	movupd	%xmm7, (%rcx)
	addq	$32, %rsi
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB6_16
# BB#17:                                #   in Loop: Header=BB6_12 Depth=2
	movl	$997, %ebx              # imm = 0x3E5
.LBB6_18:                               # %scalar.ph101.preheader
                                        #   in Loop: Header=BB6_12 Depth=2
	incq	%r8
	leaq	(%rax,%rbx,8), %rcx
	leaq	(%r13,%rcx), %rdx
	movl	$999, %esi              # imm = 0x3E7
	subq	%rbx, %rsi
	addq	%r14, %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_19:                               # %scalar.ph101
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rcx,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	addsd	-8(%rcx,%rbx,8), %xmm2
	addsd	8(%rcx,%rbx,8), %xmm2
	addsd	8000(%rcx,%rbx,8), %xmm2
	addsd	-8000(%rcx,%rbx,8), %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%rdx,%rbx,8)
	incq	%rbx
	cmpq	%rbx, %rsi
	jne	.LBB6_19
# BB#20:                                # %.loopexit201
                                        #   in Loop: Header=BB6_12 Depth=2
	incq	%rdi
	addq	$8000, %rbp             # imm = 0x1F40
	addq	$8000, %r9              # imm = 0x1F40
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$999, %r8               # imm = 0x3E7
	jne	.LBB6_12
# BB#21:                                # %.preheader.i38.preheader
                                        #   in Loop: Header=BB6_11 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	xorl	%r8d, %r8d
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB6_22:                               # %.preheader.i38
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_26 Depth 3
                                        #       Child Loop BB6_29 Depth 3
                                        #       Child Loop BB6_32 Depth 3
	imulq	$8000, %r8, %rcx        # imm = 0x1F40
	leaq	8008(%r14,%rcx), %rdx
	leaq	15992(%r13,%rcx), %rbp
	cmpq	%rbp, %rdx
	jae	.LBB6_25
# BB#23:                                # %.preheader.i38
                                        #   in Loop: Header=BB6_22 Depth=2
	leaq	15992(%r14,%rcx), %rdx
	leaq	8008(%r13,%rcx), %rcx
	cmpq	%rdx, %rcx
	jae	.LBB6_25
# BB#24:                                #   in Loop: Header=BB6_22 Depth=2
	movl	$1, %r10d
	jmp	.LBB6_28
	.p2align	4, 0x90
.LBB6_25:                               # %vector.body.preheader
                                        #   in Loop: Header=BB6_22 Depth=2
	movq	%rsi, %rdx
	movq	%rbx, %rbp
	movl	$996, %ecx              # imm = 0x3E4
	.p2align	4, 0x90
.LBB6_26:                               # %vector.body
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-80(%rdx), %xmm2
	movups	-64(%rdx), %xmm5
	movups	%xmm2, -80(%rbp)
	movups	%xmm5, -64(%rbp)
	movups	-48(%rdx), %xmm2
	movups	-32(%rdx), %xmm5
	movups	%xmm2, -48(%rbp)
	movups	%xmm5, -32(%rbp)
	movupd	-16(%rdx), %xmm2
	movupd	(%rdx), %xmm5
	movupd	%xmm2, -16(%rbp)
	movupd	%xmm5, (%rbp)
	addq	$96, %rbp
	addq	$96, %rdx
	addq	$-12, %rcx
	jne	.LBB6_26
# BB#27:                                #   in Loop: Header=BB6_22 Depth=2
	movl	$997, %r10d             # imm = 0x3E5
.LBB6_28:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB6_22 Depth=2
	movl	$998, %ecx              # imm = 0x3E6
	subq	%r10, %rcx
	leaq	(%rdi,%r10,8), %rbp
	leaq	(%rax,%r10,8), %r11
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_29:                               # %scalar.ph.prol
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r11,%rdx,8), %r14
	movq	%r14, (%rbp,%rdx,8)
	incq	%rdx
	cmpq	$2, %rdx
	jne	.LBB6_29
# BB#30:                                # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB6_22 Depth=2
	cmpq	$3, %rcx
	jb	.LBB6_33
# BB#31:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB6_22 Depth=2
	addq	%rdx, %r10
	leaq	(%r12,%r10,8), %rcx
	.p2align	4, 0x90
.LBB6_32:                               # %scalar.ph
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rcx), %rdx
	movq	%rdx, (%rdi,%r10,8)
	movq	-16(%rcx), %rdx
	movq	%rdx, 8(%rdi,%r10,8)
	movq	-8(%rcx), %rdx
	movq	%rdx, 16(%rdi,%r10,8)
	movq	(%rcx), %rdx
	movq	%rdx, 24(%rdi,%r10,8)
	addq	$4, %r10
	addq	$32, %rcx
	cmpq	$999, %r10              # imm = 0x3E7
	jne	.LBB6_32
.LBB6_33:                               # %.loopexit200
                                        #   in Loop: Header=BB6_22 Depth=2
	incq	%r9
	incq	%r8
	addq	$8000, %rbx             # imm = 0x1F40
	addq	$8000, %rsi             # imm = 0x1F40
	addq	$8000, %rdi             # imm = 0x1F40
	addq	$8000, %rax             # imm = 0x1F40
	addq	$8000, %r12             # imm = 0x1F40
	cmpq	$999, %r9               # imm = 0x3E7
	movq	64(%rsp), %r14          # 8-byte Reload
	jne	.LBB6_22
# BB#34:                                #   in Loop: Header=BB6_11 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	incl	%eax
	cmpl	$20, %eax
	movq	40(%rsp), %r12          # 8-byte Reload
	jne	.LBB6_11
# BB#35:                                # %kernel_jacobi_2d_imper.exit
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_36:                               # %.preheader.i40
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_37 Depth 2
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%ecx, %xmm5
	movq	$-1000, %rdx            # imm = 0xFC18
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_37:                               #   Parent Loop BB6_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1002(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm5, %xmm2
	addsd	%xmm8, %xmm2
	divsd	%xmm3, %xmm2
	movsd	%xmm2, (%r15,%rsi)
	leal	1003(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm5, %xmm2
	addsd	%xmm4, %xmm2
	divsd	%xmm3, %xmm2
	movsd	%xmm2, (%r13,%rsi)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB6_37
# BB#38:                                #   in Loop: Header=BB6_36 Depth=1
	incq	%rcx
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB6_36
# BB#39:                                # %.preheader3.i48.preheader
	leaq	16024(%r15), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	8024(%r13), %r12
	leaq	8088(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	8088(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	8000(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8000(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%r12, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_40:                               # %.preheader3.i48
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_41 Depth 2
                                        #       Child Loop BB6_45 Depth 3
                                        #       Child Loop BB6_48 Depth 3
                                        #     Child Loop BB6_51 Depth 2
                                        #       Child Loop BB6_55 Depth 3
                                        #       Child Loop BB6_58 Depth 3
                                        #       Child Loop BB6_61 Depth 3
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	$8000, %eax             # imm = 0x1F40
	movq	%r12, %r9
	movq	48(%rsp), %rbp          # 8-byte Reload
	xorl	%edi, %edi
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB6_41:                               # %.preheader1.i51
                                        #   Parent Loop BB6_40 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_45 Depth 3
                                        #       Child Loop BB6_48 Depth 3
	imulq	$8000, %rdi, %rcx       # imm = 0x1F40
	leaq	8008(%r13,%rcx), %rdx
	leaq	23992(%r15,%rcx), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB6_44
# BB#42:                                # %.preheader1.i51
                                        #   in Loop: Header=BB6_41 Depth=2
	leaq	15992(%r13,%rcx), %rdx
	leaq	8(%r15,%rcx), %rcx
	cmpq	%rdx, %rcx
	jae	.LBB6_44
# BB#43:                                #   in Loop: Header=BB6_41 Depth=2
	movl	$1, %ebx
	jmp	.LBB6_47
	.p2align	4, 0x90
.LBB6_44:                               # %vector.body163.preheader
                                        #   in Loop: Header=BB6_41 Depth=2
	movl	$996, %edx              # imm = 0x3E4
	movq	%r9, %rcx
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB6_45:                               # %vector.body163
                                        #   Parent Loop BB6_40 Depth=1
                                        #     Parent Loop BB6_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-8016(%rsi), %xmm2
	movupd	-8000(%rsi), %xmm3
	movupd	-8024(%rsi), %xmm4
	movupd	-8008(%rsi), %xmm5
	addpd	%xmm2, %xmm4
	addpd	%xmm5, %xmm3
	movupd	-7992(%rsi), %xmm2
	addpd	%xmm5, %xmm4
	addpd	%xmm3, %xmm2
	movupd	-16(%rsi), %xmm3
	movupd	(%rsi), %xmm5
	addpd	%xmm4, %xmm3
	addpd	%xmm2, %xmm5
	movupd	-16016(%rsi), %xmm2
	movupd	-16000(%rsi), %xmm4
	addpd	%xmm3, %xmm2
	addpd	%xmm5, %xmm4
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm4
	movupd	%xmm2, -16(%rcx)
	movupd	%xmm4, (%rcx)
	addq	$32, %rsi
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB6_45
# BB#46:                                #   in Loop: Header=BB6_41 Depth=2
	movl	$997, %ebx              # imm = 0x3E5
.LBB6_47:                               # %scalar.ph165.preheader
                                        #   in Loop: Header=BB6_41 Depth=2
	incq	%r8
	leaq	(%rax,%rbx,8), %rcx
	leaq	(%r13,%rcx), %rdx
	movl	$999, %esi              # imm = 0x3E7
	subq	%rbx, %rsi
	addq	%r15, %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_48:                               # %scalar.ph165
                                        #   Parent Loop BB6_40 Depth=1
                                        #     Parent Loop BB6_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rcx,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	addsd	-8(%rcx,%rbx,8), %xmm2
	addsd	8(%rcx,%rbx,8), %xmm2
	addsd	8000(%rcx,%rbx,8), %xmm2
	addsd	-8000(%rcx,%rbx,8), %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%rdx,%rbx,8)
	incq	%rbx
	cmpq	%rbx, %rsi
	jne	.LBB6_48
# BB#49:                                # %.loopexit199
                                        #   in Loop: Header=BB6_41 Depth=2
	incq	%rdi
	addq	$8000, %rbp             # imm = 0x1F40
	addq	$8000, %r9              # imm = 0x1F40
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$999, %r8               # imm = 0x3E7
	jne	.LBB6_41
# BB#50:                                # %.preheader.i57.preheader
                                        #   in Loop: Header=BB6_40 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	xorl	%r8d, %r8d
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB6_51:                               # %.preheader.i57
                                        #   Parent Loop BB6_40 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_55 Depth 3
                                        #       Child Loop BB6_58 Depth 3
                                        #       Child Loop BB6_61 Depth 3
	imulq	$8000, %r8, %rcx        # imm = 0x1F40
	leaq	8008(%r15,%rcx), %rdx
	leaq	15992(%r13,%rcx), %rbp
	cmpq	%rbp, %rdx
	jae	.LBB6_54
# BB#52:                                # %.preheader.i57
                                        #   in Loop: Header=BB6_51 Depth=2
	leaq	15992(%r15,%rcx), %rdx
	leaq	8008(%r13,%rcx), %rcx
	cmpq	%rdx, %rcx
	jae	.LBB6_54
# BB#53:                                #   in Loop: Header=BB6_51 Depth=2
	movl	$1, %r10d
	jmp	.LBB6_57
	.p2align	4, 0x90
.LBB6_54:                               # %vector.body135.preheader
                                        #   in Loop: Header=BB6_51 Depth=2
	movq	%r14, %rdx
	movq	%rbx, %rbp
	movl	$996, %ecx              # imm = 0x3E4
	.p2align	4, 0x90
.LBB6_55:                               # %vector.body135
                                        #   Parent Loop BB6_40 Depth=1
                                        #     Parent Loop BB6_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-80(%rdx), %xmm2
	movups	-64(%rdx), %xmm3
	movups	%xmm2, -80(%rbp)
	movups	%xmm3, -64(%rbp)
	movups	-48(%rdx), %xmm2
	movups	-32(%rdx), %xmm3
	movups	%xmm2, -48(%rbp)
	movups	%xmm3, -32(%rbp)
	movupd	-16(%rdx), %xmm2
	movupd	(%rdx), %xmm3
	movupd	%xmm2, -16(%rbp)
	movupd	%xmm3, (%rbp)
	addq	$96, %rbp
	addq	$96, %rdx
	addq	$-12, %rcx
	jne	.LBB6_55
# BB#56:                                #   in Loop: Header=BB6_51 Depth=2
	movl	$997, %r10d             # imm = 0x3E5
.LBB6_57:                               # %scalar.ph137.prol.preheader
                                        #   in Loop: Header=BB6_51 Depth=2
	movl	$998, %ecx              # imm = 0x3E6
	subq	%r10, %rcx
	leaq	(%rdi,%r10,8), %rbp
	leaq	(%rax,%r10,8), %r11
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_58:                               # %scalar.ph137.prol
                                        #   Parent Loop BB6_40 Depth=1
                                        #     Parent Loop BB6_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r11,%rdx,8), %rsi
	movq	%rsi, (%rbp,%rdx,8)
	incq	%rdx
	cmpq	$2, %rdx
	jne	.LBB6_58
# BB#59:                                # %scalar.ph137.prol.loopexit
                                        #   in Loop: Header=BB6_51 Depth=2
	cmpq	$3, %rcx
	jb	.LBB6_62
# BB#60:                                # %scalar.ph137.preheader.new
                                        #   in Loop: Header=BB6_51 Depth=2
	addq	%rdx, %r10
	leaq	(%r12,%r10,8), %rcx
	.p2align	4, 0x90
.LBB6_61:                               # %scalar.ph137
                                        #   Parent Loop BB6_40 Depth=1
                                        #     Parent Loop BB6_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rcx), %rdx
	movq	%rdx, (%rdi,%r10,8)
	movq	-16(%rcx), %rdx
	movq	%rdx, 8(%rdi,%r10,8)
	movq	-8(%rcx), %rdx
	movq	%rdx, 16(%rdi,%r10,8)
	movq	(%rcx), %rdx
	movq	%rdx, 24(%rdi,%r10,8)
	addq	$4, %r10
	addq	$32, %rcx
	cmpq	$999, %r10              # imm = 0x3E7
	jne	.LBB6_61
.LBB6_62:                               # %.loopexit
                                        #   in Loop: Header=BB6_51 Depth=2
	incq	%r9
	incq	%r8
	addq	$8000, %rbx             # imm = 0x1F40
	addq	$8000, %r14             # imm = 0x1F40
	addq	$8000, %rdi             # imm = 0x1F40
	addq	$8000, %rax             # imm = 0x1F40
	addq	$8000, %r12             # imm = 0x1F40
	cmpq	$999, %r9               # imm = 0x3E7
	jne	.LBB6_51
# BB#63:                                #   in Loop: Header=BB6_40 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	incl	%eax
	cmpl	$20, %eax
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	jne	.LBB6_40
# BB#64:                                # %.preheader.i64.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_5(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_6(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB6_65:                               # %.preheader.i64
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_66 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_66:                               #   Parent Loop BB6_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbp,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r15,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_67
# BB#69:                                #   in Loop: Header=BB6_66 Depth=2
	movsd	(%rbp,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r15,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_68
# BB#70:                                #   in Loop: Header=BB6_66 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$1000, %rcx             # imm = 0x3E8
	movq	%rdi, %rcx
	jl	.LBB6_66
# BB#71:                                #   in Loop: Header=BB6_65 Depth=1
	incq	%rdx
	addq	$1000, %rax             # imm = 0x3E8
	cmpq	$1000, %rdx             # imm = 0x3E8
	jl	.LBB6_65
# BB#72:                                # %check_FP.exit
	movl	$16001, %edi            # imm = 0x3E81
	callq	malloc
	movq	%rax, %r12
	movb	$0, 16000(%r12)
	xorl	%r14d, %r14d
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB6_73:                               # %.preheader.i68
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_74 Depth 2
	movq	%rbp, %rax
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_74:                               #   Parent Loop BB6_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movl	%edx, %ebx
	andb	$15, %bl
	orb	$48, %bl
	movb	%bl, -15(%r12,%rcx)
	movb	%bl, -14(%r12,%rcx)
	movq	%rdx, %rsi
	shrq	$8, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -13(%r12,%rcx)
	movb	%sil, -12(%r12,%rcx)
	movq	%rdx, %rsi
	shrq	$16, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -11(%r12,%rcx)
	movb	%sil, -10(%r12,%rcx)
	movl	%edx, %esi
	shrl	$24, %esi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -9(%r12,%rcx)
	movb	%sil, -8(%r12,%rcx)
	movq	%rdx, %rsi
	shrq	$32, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -7(%r12,%rcx)
	movb	%sil, -6(%r12,%rcx)
	movq	%rdx, %rsi
	shrq	$40, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -5(%r12,%rcx)
	movb	%sil, -4(%r12,%rcx)
	movq	%rdx, %rsi
	shrq	$48, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -3(%r12,%rcx)
	movb	%sil, -2(%r12,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r12,%rcx)
	movb	%dl, (%r12,%rcx)
	addq	$16, %rcx
	addq	$8, %rax
	cmpq	$16015, %rcx            # imm = 0x3E8F
	jne	.LBB6_74
# BB#75:                                #   in Loop: Header=BB6_73 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r12, %rdi
	callq	fputs
	incq	%r14
	addq	$8000, %rbp             # imm = 0x1F40
	cmpq	$1000, %r14             # imm = 0x3E8
	jne	.LBB6_73
# BB#76:                                # %print_array.exit
	movq	%r12, %rdi
	callq	free
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_77
.LBB6_67:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_68:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_77:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_78:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
