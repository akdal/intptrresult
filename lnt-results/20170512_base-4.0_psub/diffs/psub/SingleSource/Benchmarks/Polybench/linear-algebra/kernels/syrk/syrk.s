	.text
	.file	"syrk.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4562146422526312448     # double 9.765625E-4
.LCPI6_2:
	.quad	4674638556675702784     # double 32412
.LCPI6_4:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_1:
	.quad	4656886941445259264     # double 2123
	.quad	4656886941445259264     # double 2123
.LCPI6_3:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_48
# BB#1:
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_48
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_48
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_48
# BB#4:                                 # %polybench_alloc_data.exit31
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_48
# BB#5:                                 # %polybench_alloc_data.exit31
	movq	8(%rsp), %r10
	testq	%r10, %r10
	je	.LBB6_48
# BB#6:                                 # %polybench_alloc_data.exit33
	leaq	24(%r10), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_7:                                # %.preheader2.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movq	%rax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_8:                                #   Parent Loop BB6_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -24(%rdx)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -16(%rdx)
	movl	%esi, %edi
	orl	$2, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%rdx)
	movl	%esi, %edi
	orl	$3, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%rdx)
	addq	$4, %rsi
	addq	$32, %rdx
	cmpq	$1024, %rsi             # imm = 0x400
	jne	.LBB6_8
# BB#9:                                 #   in Loop: Header=BB6_7 Depth=1
	incq	%rcx
	addq	$8192, %rax             # imm = 0x2000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_7
# BB#10:                                # %.preheader.i.preheader
	xorl	%eax, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_11:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_12 Depth 2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movq	%rcx, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_12:                               #   Parent Loop BB6_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%r14,%rdx,8)
	movsd	%xmm2, -8(%r15,%rdx,8)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%r14,%rdx,8)
	movsd	%xmm2, (%r15,%rdx,8)
	addq	$2, %rsi
	addq	$2, %rdx
	cmpq	$1024, %rsi             # imm = 0x400
	jne	.LBB6_12
# BB#13:                                #   in Loop: Header=BB6_11 Depth=1
	incq	%rax
	addq	$1024, %rcx             # imm = 0x400
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB6_11
# BB#14:                                # %.preheader3.i.preheader
	leaq	48(%r14), %rax
	xorl	%ecx, %ecx
	movapd	.LCPI6_1(%rip), %xmm1   # xmm1 = [2.123000e+03,2.123000e+03]
	.p2align	4, 0x90
.LBB6_15:                               # %.preheader3.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_16 Depth 2
	movl	$1024, %edx             # imm = 0x400
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_16:                               # %vector.body
                                        #   Parent Loop BB6_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rsi), %xmm0
	movupd	-32(%rsi), %xmm2
	mulpd	%xmm1, %xmm0
	mulpd	%xmm1, %xmm2
	movupd	%xmm0, -48(%rsi)
	movupd	%xmm2, -32(%rsi)
	movupd	-16(%rsi), %xmm0
	movupd	(%rsi), %xmm2
	mulpd	%xmm1, %xmm0
	mulpd	%xmm1, %xmm2
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm2, (%rsi)
	addq	$64, %rsi
	addq	$-8, %rdx
	jne	.LBB6_16
# BB#17:                                # %middle.block
                                        #   in Loop: Header=BB6_15 Depth=1
	incq	%rcx
	addq	$8192, %rax             # imm = 0x2000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_15
# BB#18:                                # %.preheader1.i.preheader
	leaq	8(%r10), %r8
	xorl	%r9d, %r9d
	movsd	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB6_19:                               # %.preheader1.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_20 Depth 2
                                        #       Child Loop BB6_21 Depth 3
	movq	%r8, %rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_20:                               # %.preheader.i34
                                        #   Parent Loop BB6_19 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_21 Depth 3
	movq	%r9, %rax
	shlq	$13, %rax
	addq	%r14, %rax
	leaq	(%rax,%rdi,8), %rbp
	movsd	(%rax,%rdi,8), %xmm2    # xmm2 = mem[0],zero
	movl	$1024, %eax             # imm = 0x400
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB6_21:                               #   Parent Loop BB6_19 Depth=1
                                        #     Parent Loop BB6_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	mulsd	-8(%rsi), %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%rbp)
	movsd	(%rdx), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	mulsd	(%rsi), %xmm2
	addsd	%xmm3, %xmm2
	movsd	%xmm2, (%rbp)
	addq	$16, %rdx
	addq	$16, %rsi
	addq	$-2, %rax
	jne	.LBB6_21
# BB#22:                                #   in Loop: Header=BB6_20 Depth=2
	incq	%rdi
	addq	$8192, %rbx             # imm = 0x2000
	cmpq	$1024, %rdi             # imm = 0x400
	jne	.LBB6_20
# BB#23:                                #   in Loop: Header=BB6_19 Depth=1
	incq	%r9
	addq	$8192, %rcx             # imm = 0x2000
	cmpq	$1024, %r9              # imm = 0x400
	jne	.LBB6_19
# BB#24:                                # %.preheader3.i39.preheader
	leaq	48(%r15), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_25:                               # %.preheader3.i39
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_26 Depth 2
	movl	$1024, %edx             # imm = 0x400
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_26:                               # %vector.body92
                                        #   Parent Loop BB6_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rsi), %xmm2
	movupd	-32(%rsi), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -48(%rsi)
	movupd	%xmm3, -32(%rsi)
	movupd	-16(%rsi), %xmm2
	movupd	(%rsi), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rsi)
	movupd	%xmm3, (%rsi)
	addq	$64, %rsi
	addq	$-8, %rdx
	jne	.LBB6_26
# BB#27:                                # %middle.block93
                                        #   in Loop: Header=BB6_25 Depth=1
	incq	%rcx
	addq	$8192, %rax             # imm = 0x2000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_25
# BB#28:                                # %.preheader1.i46.preheader
	leaq	8(%r10), %r8
	xorl	%r9d, %r9d
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB6_29:                               # %.preheader1.i46
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_30 Depth 2
                                        #       Child Loop BB6_31 Depth 3
	movq	%r8, %rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_30:                               # %.preheader.i49
                                        #   Parent Loop BB6_29 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_31 Depth 3
	movq	%r9, %rax
	shlq	$13, %rax
	addq	%r15, %rax
	leaq	(%rax,%rdi,8), %rbp
	movsd	(%rax,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movl	$1024, %eax             # imm = 0x400
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB6_31:                               #   Parent Loop BB6_29 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	mulsd	-8(%rsi), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbp)
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	mulsd	(%rsi), %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%rbp)
	addq	$16, %rdx
	addq	$16, %rsi
	addq	$-2, %rax
	jne	.LBB6_31
# BB#32:                                #   in Loop: Header=BB6_30 Depth=2
	incq	%rdi
	addq	$8192, %rbx             # imm = 0x2000
	cmpq	$1024, %rdi             # imm = 0x400
	jne	.LBB6_30
# BB#33:                                #   in Loop: Header=BB6_29 Depth=1
	incq	%r9
	addq	$8192, %rcx             # imm = 0x2000
	cmpq	$1024, %r9              # imm = 0x400
	jne	.LBB6_29
# BB#34:                                # %.preheader.i57.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_3(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_4(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB6_35:                               # %.preheader.i57
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_36 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_36:                               #   Parent Loop BB6_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r14,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r15,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_37
# BB#39:                                #   in Loop: Header=BB6_36 Depth=2
	movsd	(%r14,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r15,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_38
# BB#40:                                #   in Loop: Header=BB6_36 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$1024, %rcx             # imm = 0x400
	movq	%rdi, %rcx
	jl	.LBB6_36
# BB#41:                                #   in Loop: Header=BB6_35 Depth=1
	incq	%rdx
	addq	$1024, %rax             # imm = 0x400
	cmpq	$1024, %rdx             # imm = 0x400
	jl	.LBB6_35
# BB#42:                                # %check_FP.exit
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movl	$16385, %edi            # imm = 0x4001
	callq	malloc
	movq	%rax, %r12
	movb	$0, 16384(%r12)
	xorl	%r13d, %r13d
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB6_43:                               # %.preheader.i60
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_44 Depth 2
	movq	%rbp, %rax
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_44:                               #   Parent Loop BB6_43 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movl	%edx, %ebx
	andb	$15, %bl
	orb	$48, %bl
	movb	%bl, -15(%r12,%rcx)
	movb	%bl, -14(%r12,%rcx)
	movq	%rdx, %rsi
	shrq	$8, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -13(%r12,%rcx)
	movb	%sil, -12(%r12,%rcx)
	movq	%rdx, %rsi
	shrq	$16, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -11(%r12,%rcx)
	movb	%sil, -10(%r12,%rcx)
	movl	%edx, %esi
	shrl	$24, %esi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -9(%r12,%rcx)
	movb	%sil, -8(%r12,%rcx)
	movq	%rdx, %rsi
	shrq	$32, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -7(%r12,%rcx)
	movb	%sil, -6(%r12,%rcx)
	movq	%rdx, %rsi
	shrq	$40, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -5(%r12,%rcx)
	movb	%sil, -4(%r12,%rcx)
	movq	%rdx, %rsi
	shrq	$48, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -3(%r12,%rcx)
	movb	%sil, -2(%r12,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r12,%rcx)
	movb	%dl, (%r12,%rcx)
	addq	$16, %rcx
	addq	$8, %rax
	cmpq	$16399, %rcx            # imm = 0x400F
	jne	.LBB6_44
# BB#45:                                #   in Loop: Header=BB6_43 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r12, %rdi
	callq	fputs
	incq	%r13
	addq	$8192, %rbp             # imm = 0x2000
	cmpq	$1024, %r13             # imm = 0x400
	jne	.LBB6_43
# BB#46:                                # %print_array.exit
	movq	%r12, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_47
.LBB6_37:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_38:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_4(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_47:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_48:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
