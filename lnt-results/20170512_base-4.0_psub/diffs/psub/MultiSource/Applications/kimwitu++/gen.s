	.text
	.file	"gen.bc"
	.globl	_ZN2kc15f_selofoperatorEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc15f_selofoperatorEPNS_7impl_IDE,@function
_ZN2kc15f_selofoperatorEPNS_7impl_IDE:  # @_ZN2kc15f_selofoperatorEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	Thephylumdeclarations(%rip), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	xorl	%r12d, %r12d
	cmpl	$12, %eax
	movl	$0, %r13d
	jne	.LBB0_24
# BB#1:                                 # %.lr.ph146
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_19 Depth 2
                                        #     Child Loop BB0_12 Depth 2
                                        #     Child Loop BB0_5 Depth 2
	movq	8(%r15), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB0_9
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$22, %eax
	jne	.LBB0_9
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	48(%rbx), %rax
	movq	8(%rax), %rbx
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_5 Depth=2
	movq	16(%rbx), %rbx
.LBB0_5:                                #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	jne	.LBB0_23
# BB#6:                                 # %.lr.ph141
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	8(%rbx), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_5 Depth=2
	movq	40(%rbp), %rsi
	incl	%r12d
	movq	%r14, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	cmovnel	%r12d, %r13d
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB0_16
# BB#10:                                #   in Loop: Header=BB0_2 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$23, %eax
	jne	.LBB0_16
# BB#11:                                #   in Loop: Header=BB0_2 Depth=1
	movq	48(%rbx), %rax
	movq	8(%rax), %rbx
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_12 Depth=2
	movq	16(%rbx), %rbx
.LBB0_12:                               #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	jne	.LBB0_23
# BB#13:                                # %.lr.ph134
                                        #   in Loop: Header=BB0_12 Depth=2
	movq	8(%rbx), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_12 Depth=2
	movq	40(%rbp), %rsi
	incl	%r12d
	movq	%r14, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	cmovnel	%r12d, %r13d
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB0_26
# BB#17:                                #   in Loop: Header=BB0_2 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$24, %eax
	jne	.LBB0_26
# BB#18:                                #   in Loop: Header=BB0_2 Depth=1
	movq	48(%rbx), %rax
	movq	8(%rax), %rbx
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_22:                               #   in Loop: Header=BB0_19 Depth=2
	movq	16(%rbx), %rbx
.LBB0_19:                               #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	jne	.LBB0_23
# BB#20:                                # %.lr.ph
                                        #   in Loop: Header=BB0_19 Depth=2
	movq	8(%rbx), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_19 Depth=2
	movq	40(%rbp), %rsi
	incl	%r12d
	movq	%r14, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	cmovnel	%r12d, %r13d
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_23:                               # %.thread
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	16(%r15), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$12, %eax
	je	.LBB0_2
.LBB0_24:                               # %.loopexit155
	incl	%r12d
	subl	%r13d, %r12d
	jmp	.LBB0_25
.LBB0_26:                               # %.loopexit
	movl	$.L.str, %edi
	movl	$150, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r12d, %r12d
.LBB0_25:
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN2kc15f_selofoperatorEPNS_7impl_IDE, .Lfunc_end0-_ZN2kc15f_selofoperatorEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc20freespineandelementsEPNS_21impl_unparseviewsinfoE
	.p2align	4, 0x90
	.type	_ZN2kc20freespineandelementsEPNS_21impl_unparseviewsinfoE,@function
_ZN2kc20freespineandelementsEPNS_21impl_unparseviewsinfoE: # @_ZN2kc20freespineandelementsEPNS_21impl_unparseviewsinfoE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB1_5
# BB#1:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	testq	%rax, %rax
	je	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	_ZN2kc20impl_abstract_phylum4freeEb
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	movq	(%rbx), %rax
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_2
.LBB1_5:                                # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc18impl_abstract_list8freelistEv # TAILCALL
.Lfunc_end1:
	.size	_ZN2kc20freespineandelementsEPNS_21impl_unparseviewsinfoE, .Lfunc_end1-_ZN2kc20freespineandelementsEPNS_21impl_unparseviewsinfoE
	.cfi_endproc

	.globl	_ZN2kc20freespineandelementsEPNS_21impl_rewriteviewsinfoE
	.p2align	4, 0x90
	.type	_ZN2kc20freespineandelementsEPNS_21impl_rewriteviewsinfoE,@function
_ZN2kc20freespineandelementsEPNS_21impl_rewriteviewsinfoE: # @_ZN2kc20freespineandelementsEPNS_21impl_rewriteviewsinfoE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB2_5
# BB#1:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	testq	%rax, %rax
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	_ZN2kc20impl_abstract_phylum4freeEb
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%rbx), %rax
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
.LBB2_5:                                # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc18impl_abstract_list8freelistEv # TAILCALL
.Lfunc_end2:
	.size	_ZN2kc20freespineandelementsEPNS_21impl_rewriteviewsinfoE, .Lfunc_end2-_ZN2kc20freespineandelementsEPNS_21impl_rewriteviewsinfoE
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"f_selofoperator"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/kimwitu++/gen.cc"
	.size	.L.str.1, 80


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
