	.text
	.file	"negamax.bc"
	.globl	search_for_move
	.p2align	4, 0x90
	.type	search_for_move,@function
search_for_move:                        # @search_for_move
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$3192, %rsp             # imm = 0xC78
.Lcfi6:
	.cfi_def_cfa_offset 3248
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movl	%edi, %ebx
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %eax
	movl	$1, %r12d
	cmpl	$86, %eax
	je	.LBB0_3
# BB#1:
	cmpl	$72, %eax
	jne	.LBB0_53
# BB#2:
	xorl	%r12d, %r12d
.LBB0_3:
	movl	$0, g_empty_squares(%rip)
	movslq	g_board_size(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_10
# BB#4:                                 # %.lr.ph341
	testb	$1, %al
	jne	.LBB0_6
# BB#5:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	jne	.LBB0_8
	jmp	.LBB0_9
.LBB0_6:
	movl	g_board+4(%rip), %edx
	notl	%edx
	movzwl	%dx, %ecx
	movl	countbits16(,%rcx,4), %ecx
	shrl	$16, %edx
	addl	countbits16(,%rdx,4), %ecx
	movl	$1, %edx
	cmpl	$1, %eax
	je	.LBB0_9
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movl	g_board+4(,%rdx,4), %esi
	notl	%esi
	movzwl	%si, %edi
	shrl	$16, %esi
	addl	countbits16(,%rdi,4), %ecx
	addl	countbits16(,%rsi,4), %ecx
	movl	g_board+8(,%rdx,4), %esi
	addq	$2, %rdx
	notl	%esi
	movzwl	%si, %edi
	shrl	$16, %esi
	addl	countbits16(,%rdi,4), %ecx
	addl	countbits16(,%rsi,4), %ecx
	cmpq	%rax, %rdx
	jl	.LBB0_8
.LBB0_9:                                # %._crit_edge342
	movl	%ecx, g_empty_squares(%rip)
.LBB0_10:                               # %.preheader.i
	xorps	%xmm0, %xmm0
	movaps	%xmm0, stat_cutoffs+144(%rip)
	movaps	%xmm0, stat_cutoffs+128(%rip)
	movaps	%xmm0, stat_cutoffs+112(%rip)
	movaps	%xmm0, stat_cutoffs+96(%rip)
	movaps	%xmm0, stat_cutoffs+80(%rip)
	movaps	%xmm0, stat_cutoffs+64(%rip)
	movaps	%xmm0, stat_cutoffs+48(%rip)
	movaps	%xmm0, stat_cutoffs+32(%rip)
	movaps	%xmm0, stat_cutoffs+16(%rip)
	movaps	%xmm0, stat_cutoffs(%rip)
	movaps	%xmm0, stat_nodes+144(%rip)
	movaps	%xmm0, stat_nodes+128(%rip)
	movaps	%xmm0, stat_nodes+112(%rip)
	movaps	%xmm0, stat_nodes+96(%rip)
	movaps	%xmm0, stat_nodes+80(%rip)
	movaps	%xmm0, stat_nodes+64(%rip)
	movaps	%xmm0, stat_nodes+48(%rip)
	movaps	%xmm0, stat_nodes+32(%rip)
	movaps	%xmm0, stat_nodes+16(%rip)
	movaps	%xmm0, stat_nodes(%rip)
	movaps	%xmm0, stat_nth_try(%rip)
	movq	$0, stat_nth_try+16(%rip)
	movups	%xmm0, stat_nth_try+40(%rip)
	movq	$0, stat_nth_try+56(%rip)
	movaps	%xmm0, stat_nth_try+80(%rip)
	movq	$0, stat_nth_try+96(%rip)
	movups	%xmm0, stat_nth_try+120(%rip)
	movq	$0, stat_nth_try+136(%rip)
	movaps	%xmm0, stat_nth_try+160(%rip)
	movq	$0, stat_nth_try+176(%rip)
	movups	%xmm0, stat_nth_try+200(%rip)
	movq	$0, stat_nth_try+216(%rip)
	movaps	%xmm0, stat_nth_try+240(%rip)
	movq	$0, stat_nth_try+256(%rip)
	movups	%xmm0, stat_nth_try+280(%rip)
	movq	$0, stat_nth_try+296(%rip)
	movaps	%xmm0, stat_nth_try+320(%rip)
	movq	$0, stat_nth_try+336(%rip)
	movups	%xmm0, stat_nth_try+360(%rip)
	movq	$0, stat_nth_try+376(%rip)
	movaps	%xmm0, stat_nth_try+400(%rip)
	movq	$0, stat_nth_try+416(%rip)
	movups	%xmm0, stat_nth_try+440(%rip)
	movq	$0, stat_nth_try+456(%rip)
	movaps	%xmm0, stat_nth_try+480(%rip)
	movq	$0, stat_nth_try+496(%rip)
	movups	%xmm0, stat_nth_try+520(%rip)
	movq	$0, stat_nth_try+536(%rip)
	movaps	%xmm0, stat_nth_try+560(%rip)
	movq	$0, stat_nth_try+576(%rip)
	movups	%xmm0, stat_nth_try+600(%rip)
	movq	$0, stat_nth_try+616(%rip)
	movaps	%xmm0, stat_nth_try+640(%rip)
	movq	$0, stat_nth_try+656(%rip)
	movups	%xmm0, stat_nth_try+680(%rip)
	movq	$0, stat_nth_try+696(%rip)
	movaps	%xmm0, stat_nth_try+720(%rip)
	movq	$0, stat_nth_try+736(%rip)
	movups	%xmm0, stat_nth_try+760(%rip)
	movq	$0, stat_nth_try+776(%rip)
	movaps	%xmm0, stat_nth_try+800(%rip)
	movq	$0, stat_nth_try+816(%rip)
	movups	%xmm0, stat_nth_try+840(%rip)
	movq	$0, stat_nth_try+856(%rip)
	movaps	%xmm0, stat_nth_try+880(%rip)
	movq	$0, stat_nth_try+896(%rip)
	movups	%xmm0, stat_nth_try+920(%rip)
	movq	$0, stat_nth_try+936(%rip)
	movaps	%xmm0, stat_nth_try+960(%rip)
	movq	$0, stat_nth_try+976(%rip)
	movups	%xmm0, stat_nth_try+1000(%rip)
	movq	$0, stat_nth_try+1016(%rip)
	movaps	%xmm0, stat_nth_try+1040(%rip)
	movq	$0, stat_nth_try+1056(%rip)
	movups	%xmm0, stat_nth_try+1080(%rip)
	movq	$0, stat_nth_try+1096(%rip)
	movaps	%xmm0, stat_nth_try+1120(%rip)
	movq	$0, stat_nth_try+1136(%rip)
	movups	%xmm0, stat_nth_try+1160(%rip)
	movq	$0, stat_nth_try+1176(%rip)
	movaps	%xmm0, stat_nth_try+1200(%rip)
	movq	$0, stat_nth_try+1216(%rip)
	movups	%xmm0, stat_nth_try+1240(%rip)
	movq	$0, stat_nth_try+1256(%rip)
	movaps	%xmm0, stat_nth_try+1280(%rip)
	movq	$0, stat_nth_try+1296(%rip)
	movups	%xmm0, stat_nth_try+1320(%rip)
	movq	$0, stat_nth_try+1336(%rip)
	movaps	%xmm0, stat_nth_try+1360(%rip)
	movq	$0, stat_nth_try+1376(%rip)
	movups	%xmm0, stat_nth_try+1400(%rip)
	movq	$0, stat_nth_try+1416(%rip)
	movaps	%xmm0, stat_nth_try+1440(%rip)
	movq	$0, stat_nth_try+1456(%rip)
	movups	%xmm0, stat_nth_try+1480(%rip)
	movq	$0, stat_nth_try+1496(%rip)
	movaps	%xmm0, stat_nth_try+1520(%rip)
	movq	$0, stat_nth_try+1536(%rip)
	movups	%xmm0, stat_nth_try+1560(%rip)
	movq	$0, stat_nth_try+1576(%rip)
	movl	%r12d, %eax
	xorl	$1, %eax
	leaq	(%r12,%r12,2), %rdx
	movl	g_info_totals(,%rdx,4), %ecx
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	(%rax,%rax,2), %rax
	movl	$5000, %esi             # imm = 0x1388
	cmpl	g_info_totals+4(,%rax,4), %ecx
	jg	.LBB0_12
# BB#11:
	movl	g_info_totals(,%rax,4), %eax
	movl	$-5000, %esi            # imm = 0xEC78
	cmpl	g_info_totals+4(,%rdx,4), %eax
	jge	.LBB0_12
# BB#13:
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	leaq	112(%rsp), %rbx
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	move_generator
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB0_15
# BB#14:
	movl	$.L.str, %edi
	movl	$159, %esi
	movl	$1, %edx
	movl	$.L.str.2, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB0_15:
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	score_and_get_first
	movl	$1, %r15d
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	sort_moves
	shlq	$14, 64(%rsp)           # 8-byte Folded Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader.i302
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_18 Depth 2
                                        #     Child Loop BB0_27 Depth 2
                                        #     Child Loop BB0_35 Depth 2
                                        #       Child Loop BB0_40 Depth 3
                                        #       Child Loop BB0_44 Depth 3
                                        #       Child Loop BB0_51 Depth 3
	movq	$0, g_num_nodes(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, stat_cutoffs+144(%rip)
	movaps	%xmm0, stat_cutoffs+128(%rip)
	movaps	%xmm0, stat_cutoffs+112(%rip)
	movaps	%xmm0, stat_cutoffs+96(%rip)
	movaps	%xmm0, stat_cutoffs+80(%rip)
	movaps	%xmm0, stat_cutoffs+64(%rip)
	movaps	%xmm0, stat_cutoffs+48(%rip)
	movaps	%xmm0, stat_cutoffs+32(%rip)
	movaps	%xmm0, stat_cutoffs+16(%rip)
	movaps	%xmm0, stat_cutoffs(%rip)
	movaps	%xmm0, stat_nodes+144(%rip)
	movaps	%xmm0, stat_nodes+128(%rip)
	movaps	%xmm0, stat_nodes+112(%rip)
	movaps	%xmm0, stat_nodes+96(%rip)
	movaps	%xmm0, stat_nodes+80(%rip)
	movaps	%xmm0, stat_nodes+64(%rip)
	movaps	%xmm0, stat_nodes+48(%rip)
	movaps	%xmm0, stat_nodes+32(%rip)
	movaps	%xmm0, stat_nodes+16(%rip)
	movaps	%xmm0, stat_nodes(%rip)
	movaps	%xmm0, stat_nth_try(%rip)
	movq	$0, stat_nth_try+16(%rip)
	movups	%xmm0, stat_nth_try+40(%rip)
	movq	$0, stat_nth_try+56(%rip)
	movaps	%xmm0, stat_nth_try+80(%rip)
	movq	$0, stat_nth_try+96(%rip)
	movups	%xmm0, stat_nth_try+120(%rip)
	movq	$0, stat_nth_try+136(%rip)
	movaps	%xmm0, stat_nth_try+160(%rip)
	movq	$0, stat_nth_try+176(%rip)
	movups	%xmm0, stat_nth_try+200(%rip)
	movq	$0, stat_nth_try+216(%rip)
	movaps	%xmm0, stat_nth_try+240(%rip)
	movq	$0, stat_nth_try+256(%rip)
	movups	%xmm0, stat_nth_try+280(%rip)
	movq	$0, stat_nth_try+296(%rip)
	movaps	%xmm0, stat_nth_try+320(%rip)
	movq	$0, stat_nth_try+336(%rip)
	movups	%xmm0, stat_nth_try+360(%rip)
	movq	$0, stat_nth_try+376(%rip)
	movaps	%xmm0, stat_nth_try+400(%rip)
	movq	$0, stat_nth_try+416(%rip)
	movups	%xmm0, stat_nth_try+440(%rip)
	movq	$0, stat_nth_try+456(%rip)
	movaps	%xmm0, stat_nth_try+480(%rip)
	movq	$0, stat_nth_try+496(%rip)
	movups	%xmm0, stat_nth_try+520(%rip)
	movq	$0, stat_nth_try+536(%rip)
	movaps	%xmm0, stat_nth_try+560(%rip)
	movq	$0, stat_nth_try+576(%rip)
	movups	%xmm0, stat_nth_try+600(%rip)
	movq	$0, stat_nth_try+616(%rip)
	movaps	%xmm0, stat_nth_try+640(%rip)
	movq	$0, stat_nth_try+656(%rip)
	movups	%xmm0, stat_nth_try+680(%rip)
	movq	$0, stat_nth_try+696(%rip)
	movaps	%xmm0, stat_nth_try+720(%rip)
	movq	$0, stat_nth_try+736(%rip)
	movups	%xmm0, stat_nth_try+760(%rip)
	movq	$0, stat_nth_try+776(%rip)
	movaps	%xmm0, stat_nth_try+800(%rip)
	movq	$0, stat_nth_try+816(%rip)
	movups	%xmm0, stat_nth_try+840(%rip)
	movq	$0, stat_nth_try+856(%rip)
	movaps	%xmm0, stat_nth_try+880(%rip)
	movq	$0, stat_nth_try+896(%rip)
	movups	%xmm0, stat_nth_try+920(%rip)
	movq	$0, stat_nth_try+936(%rip)
	movaps	%xmm0, stat_nth_try+960(%rip)
	movq	$0, stat_nth_try+976(%rip)
	movups	%xmm0, stat_nth_try+1000(%rip)
	movq	$0, stat_nth_try+1016(%rip)
	movaps	%xmm0, stat_nth_try+1040(%rip)
	movq	$0, stat_nth_try+1056(%rip)
	movups	%xmm0, stat_nth_try+1080(%rip)
	movq	$0, stat_nth_try+1096(%rip)
	movaps	%xmm0, stat_nth_try+1120(%rip)
	movq	$0, stat_nth_try+1136(%rip)
	movups	%xmm0, stat_nth_try+1160(%rip)
	movq	$0, stat_nth_try+1176(%rip)
	movaps	%xmm0, stat_nth_try+1200(%rip)
	movq	$0, stat_nth_try+1216(%rip)
	movups	%xmm0, stat_nth_try+1240(%rip)
	movq	$0, stat_nth_try+1256(%rip)
	movaps	%xmm0, stat_nth_try+1280(%rip)
	movq	$0, stat_nth_try+1296(%rip)
	movups	%xmm0, stat_nth_try+1320(%rip)
	movq	$0, stat_nth_try+1336(%rip)
	movaps	%xmm0, stat_nth_try+1360(%rip)
	movq	$0, stat_nth_try+1376(%rip)
	movups	%xmm0, stat_nth_try+1400(%rip)
	movq	$0, stat_nth_try+1416(%rip)
	movaps	%xmm0, stat_nth_try+1440(%rip)
	movq	$0, stat_nth_try+1456(%rip)
	movups	%xmm0, stat_nth_try+1480(%rip)
	movq	$0, stat_nth_try+1496(%rip)
	movaps	%xmm0, stat_nth_try+1520(%rip)
	movq	$0, stat_nth_try+1536(%rip)
	movups	%xmm0, stat_nth_try+1560(%rip)
	movq	$0, stat_nth_try+1576(%rip)
	movl	%r15d, starting_depth(%rip)
	testl	%r14d, %r14d
	movl	$0, %ebp
	movl	8(%rsp), %esi           # 4-byte Reload
	movq	%r15, 88(%rsp)          # 8-byte Spill
	jle	.LBB0_22
# BB#17:                                # %.lr.ph
                                        #   in Loop: Header=BB0_16 Depth=1
	leal	-1(%r15), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movslq	%r14d, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$-5000, %r15d           # imm = 0xEC78
	movq	%rbx, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_18:                               #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	callq	set_position_values
	movl	%ebp, g_move_number(%rip)
	addl	$-2, g_empty_squares(%rip)
	movq	(%r13), %rdi
	movl	8(%r13), %esi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %edx
	callq	toggle_move
	movslq	(%r13), %rax
	movslq	4(%r13), %rcx
	leaq	(%rax,%rax,2), %rax
	shlq	$9, %rax
	movq	64(%rsp), %rbx          # 8-byte Reload
	leaq	g_keyinfo(%rbx,%rax), %rdx
	shlq	$4, %rcx
	leaq	(%rcx,%rcx,2), %rax
	movl	(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_norm_hashkey(,%rcx,4)
	movl	4(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_norm_hashkey(,%rcx,4)
	movl	8(%rdx,%rax), %ecx
	xorl	%ecx, g_norm_hashkey+16(%rip)
	movl	12(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipV_hashkey(,%rcx,4)
	movl	16(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipV_hashkey(,%rcx,4)
	movl	20(%rdx,%rax), %ecx
	xorl	%ecx, g_flipV_hashkey+16(%rip)
	movl	24(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipH_hashkey(,%rcx,4)
	movl	28(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipH_hashkey(,%rcx,4)
	movl	32(%rdx,%rax), %ecx
	xorl	%ecx, g_flipH_hashkey+16(%rip)
	movl	36(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipVH_hashkey(,%rcx,4)
	movl	40(%rdx,%rax), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$27, %esi
	addl	%ecx, %esi
	sarl	$5, %esi
	movslq	%esi, %rcx
	xorl	%edx, g_flipVH_hashkey(,%rcx,4)
	movslq	(%r13), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$9, %rcx
	leaq	g_keyinfo(%rbx,%rcx), %rcx
	movl	44(%rcx,%rax), %eax
	xorl	%eax, g_flipVH_hashkey+16(%rip)
	xorl	%eax, %eax
	callq	check_hash_code_sanity
	movl	%r15d, %ecx
	negl	%ecx
	movl	$-5000, %edx            # imm = 0xEC78
	movl	8(%rsp), %edi           # 4-byte Reload
	movq	104(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	negamax
	movl	%eax, %r12d
	movl	%r12d, 56(%rsp)         # 4-byte Spill
	negl	%r12d
	addl	$2, g_empty_squares(%rip)
	movq	(%r13), %rdi
	movl	8(%r13), %esi
	movl	%ebp, %edx
	callq	toggle_move
	movslq	(%r13), %rax
	movslq	4(%r13), %rcx
	leaq	(%rax,%rax,2), %rax
	shlq	$9, %rax
	leaq	g_keyinfo(%rbx,%rax), %rax
	leaq	(%rcx,%rcx,2), %rdx
	shlq	$4, %rdx
	movl	(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_norm_hashkey(,%rcx,4)
	movl	4(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_norm_hashkey(,%rcx,4)
	movl	8(%rdx,%rax), %ecx
	xorl	%ecx, g_norm_hashkey+16(%rip)
	movl	12(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipV_hashkey(,%rcx,4)
	movl	16(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipV_hashkey(,%rcx,4)
	movl	20(%rdx,%rax), %ecx
	xorl	%ecx, g_flipV_hashkey+16(%rip)
	movl	24(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipH_hashkey(,%rcx,4)
	movl	28(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipH_hashkey(,%rcx,4)
	movl	32(%rdx,%rax), %ecx
	xorl	%ecx, g_flipH_hashkey+16(%rip)
	movl	36(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipVH_hashkey(,%rcx,4)
	movl	40(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipVH_hashkey(,%rcx,4)
	movl	44(%rdx,%rax), %eax
	xorl	%eax, g_flipVH_hashkey+16(%rip)
	xorl	%eax, %eax
	callq	check_hash_code_sanity
	movl	(%r13), %r14d
	movl	4(%r13), %ebx
	movq	g_num_nodes(%rip), %rdi
	callq	u64bit_to_string
	movq	%rax, %rbp
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	%ebx, %edx
	movl	%r12d, %ecx
	movq	%rbp, %r8
	callq	printf
	movl	$.L.str.4, %edi
	movl	$5000, %edx             # imm = 0x1388
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	printf
	movl	%r12d, %esi
	movl	%esi, 8(%r13)
	cmpl	$-4999, 56(%rsp)        # 4-byte Folded Reload
                                        # imm = 0xEC79
	jl	.LBB0_21
# BB#19:                                #   in Loop: Header=BB0_18 Depth=2
	cmpl	%esi, %r15d
	cmovll	%esi, %r15d
	movq	24(%rsp), %rax          # 8-byte Reload
	incq	%rax
	addq	$12, %r13
	cmpq	96(%rsp), %rax          # 8-byte Folded Reload
	movq	%rax, %rbp
	jl	.LBB0_18
# BB#20:                                # %._crit_edge.loopexitsplit
                                        #   in Loop: Header=BB0_16 Depth=1
	leaq	112(%rsp), %rbx
	movl	12(%rsp), %r14d         # 4-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	cmpl	$4999, %esi             # imm = 0x1387
	jle	.LBB0_23
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_21:                               # %.._crit_edge.loopexit_crit_edge
                                        #   in Loop: Header=BB0_16 Depth=1
	leaq	112(%rsp), %rbx
	movl	12(%rsp), %r14d         # 4-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB0_22:                               # %._crit_edge
                                        #   in Loop: Header=BB0_16 Depth=1
	cmpl	$4999, %esi             # imm = 0x1387
	jg	.LBB0_25
.LBB0_23:                               # %.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	testl	%r14d, %r14d
	jle	.LBB0_24
# BB#26:                                # %.lr.ph317.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	movl	%r14d, %eax
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_27:                               # %.lr.ph317
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-5000, 8(%rdx)         # imm = 0xEC78
	jg	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_27 Depth=2
	incl	%ebx
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_27 Depth=2
	testl	%ebx, %ebx
	jle	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_27 Depth=2
	movl	%ecx, %edi
	subl	%ebx, %edi
	movslq	%edi, %rdi
	leaq	(%rdi,%rdi,2), %rbp
	movl	8(%rdx), %edi
	movl	%edi, 120(%rsp,%rbp,4)
	movq	(%rdx), %rdi
	movq	%rdi, 112(%rsp,%rbp,4)
.LBB0_31:                               #   in Loop: Header=BB0_27 Depth=2
	incq	%rcx
	addq	$12, %rdx
	cmpq	%rcx, %rax
	jne	.LBB0_27
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_24:                               #   in Loop: Header=BB0_16 Depth=1
	xorl	%ebx, %ebx
.LBB0_32:                               # %._crit_edge318
                                        #   in Loop: Header=BB0_16 Depth=1
	movl	%esi, 8(%rsp)           # 4-byte Spill
	callq	print_stats
	subl	%ebx, %r14d
	je	.LBB0_48
# BB#33:                                #   in Loop: Header=BB0_16 Depth=1
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	testl	%r14d, %r14d
	jle	.LBB0_47
# BB#34:                                # %.lr.ph333.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	movslq	%eax, %r9
	movl	%eax, %r8d
	leaq	3(%r8), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	-2(%r8), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movb	%al, %r11b
	addb	$3, %r11b
	leaq	132(%rsp), %r15
	xorl	%edx, %edx
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB0_35:                               # %.lr.ph333
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_40 Depth 3
                                        #       Child Loop BB0_44 Depth 3
                                        #       Child Loop BB0_51 Depth 3
	andb	$3, %r11b
	movzbl	%r11b, %r11d
	leaq	(%rdx,%rdx,2), %r10
	leaq	1(%rdx), %r14
	cmpq	%r9, %r14
	jge	.LBB0_36
# BB#37:                                # %.lr.ph324.preheader
                                        #   in Loop: Header=BB0_35 Depth=2
	movq	24(%rsp), %r13          # 8-byte Reload
	subq	%rdx, %r13
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%edx, %ecx
	movl	120(%rsp,%r10,4), %eax
	testb	$3, %cl
	je	.LBB0_38
# BB#39:                                # %.lr.ph324.prol.preheader
                                        #   in Loop: Header=BB0_35 Depth=2
	movq	%r10, %rdi
	movq	%r9, %rsi
	movq	%r15, %rcx
	xorl	%ebx, %ebx
	movl	%edx, %ebp
	.p2align	4, 0x90
.LBB0_40:                               # %.lr.ph324.prol
                                        #   Parent Loop BB0_16 Depth=1
                                        #     Parent Loop BB0_35 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rcx), %r9d
	leal	(%r12,%rbx), %r10d
	cmpl	%eax, %r9d
	cmovgel	%r9d, %eax
	cmovgl	%r10d, %ebp
	incq	%rbx
	addq	$12, %rcx
	cmpq	%rbx, %r11
	jne	.LBB0_40
# BB#41:                                # %.lr.ph324.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB0_35 Depth=2
	addq	%r12, %rbx
	movq	%rsi, %r9
	movq	%rdi, %r10
	cmpq	$3, %r13
	jae	.LBB0_43
	jmp	.LBB0_45
	.p2align	4, 0x90
.LBB0_36:                               #   in Loop: Header=BB0_35 Depth=2
	movl	%edx, %ebp
	jmp	.LBB0_45
.LBB0_38:                               #   in Loop: Header=BB0_35 Depth=2
	movq	%r12, %rbx
	movl	%edx, %ebp
	cmpq	$3, %r13
	jb	.LBB0_45
.LBB0_43:                               # %.lr.ph324.preheader.new
                                        #   in Loop: Header=BB0_35 Depth=2
	leaq	(%rbx,%rbx,2), %rcx
	leaq	132(%rsp), %rsi
	leaq	24(%rsi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_44:                               # %.lr.ph324
                                        #   Parent Loop BB0_16 Depth=1
                                        #     Parent Loop BB0_35 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-36(%rcx), %esi
	movl	-24(%rcx), %edi
	cmpl	%eax, %esi
	cmovgel	%esi, %eax
	cmovgl	%ebx, %ebp
	leal	1(%rbx), %esi
	cmpl	%eax, %edi
	cmovgel	%edi, %eax
	cmovlel	%ebp, %esi
	leal	2(%rbx), %edi
	movl	-12(%rcx), %ebp
	cmpl	%eax, %ebp
	cmovgel	%ebp, %eax
	cmovlel	%esi, %edi
	leal	3(%rbx), %ebp
	movl	(%rcx), %esi
	cmpl	%eax, %esi
	cmovgel	%esi, %eax
	cmovlel	%edi, %ebp
	addq	$4, %rbx
	addq	$48, %rcx
	cmpq	%r8, %rbx
	jne	.LBB0_44
.LBB0_45:                               # %._crit_edge325
                                        #   in Loop: Header=BB0_35 Depth=2
	movl	%ebp, %eax
	cmpq	%rdx, %rax
	je	.LBB0_46
# BB#50:                                # %.lr.ph329
                                        #   in Loop: Header=BB0_35 Depth=2
	leaq	112(%rsp,%r10,4), %rax
	movslq	%ebp, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	leaq	112(%rsp,%rsi,4), %rbp
	movl	120(%rsp,%rsi,4), %edi
	movl	%edi, 80(%rsp)
	movq	112(%rsp,%rsi,4), %rsi
	movq	%rsi, 72(%rsp)
	decq	%rcx
	.p2align	4, 0x90
.LBB0_51:                               #   Parent Loop BB0_16 Depth=1
                                        #     Parent Loop BB0_35 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rbp), %esi
	movl	%esi, 8(%rbp)
	movq	-12(%rbp), %rsi
	movq	%rsi, (%rbp)
	leaq	-12(%rbp), %rbp
	movl	%ecx, %esi
	decq	%rcx
	cmpq	%rdx, %rsi
	jne	.LBB0_51
# BB#52:                                # %._crit_edge330
                                        #   in Loop: Header=BB0_35 Depth=2
	movl	80(%rsp), %ecx
	movl	%ecx, 8(%rax)
	movq	72(%rsp), %rcx
	movq	%rcx, (%rax)
.LBB0_46:                               # %.backedge
                                        #   in Loop: Header=BB0_35 Depth=2
	incq	%r12
	addq	$12, %r15
	addb	$3, %r11b
	cmpq	%r8, %r14
	movq	%r14, %rdx
	jne	.LBB0_35
.LBB0_47:                               # %._crit_edge334
                                        #   in Loop: Header=BB0_16 Depth=1
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	8(%rsp), %esi           # 4-byte Reload
	movq	88(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %edx
	callq	printf
	movl	g_num_nodes(%rip), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	addl	$44, %r15d
	cmpl	$50, %r15d
	movq	16(%rsp), %r12          # 8-byte Reload
	leaq	112(%rsp), %rbx
	movl	12(%rsp), %r14d         # 4-byte Reload
	jl	.LBB0_16
.LBB0_48:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	$-1, (%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$-1, (%rax)
	movq	g_num_nodes(%rip), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movl	8(%rsp), %esi           # 4-byte Reload
	jmp	.LBB0_49
.LBB0_12:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	$-1, (%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$-1, (%rax)
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax)
.LBB0_49:
	movl	%esi, %eax
	addq	$3192, %rsp             # imm = 0xC78
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_25:                               # %.thread
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%esi, %ebx
	callq	printf
	testl	%r12d, %r12d
	movslq	%ebp, %rax
	leaq	(%rax,%rax,2), %rax
	movl	112(%rsp,%rax,4), %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmoveq	%rsi, %rdx
	cmoveq	%rdi, %rsi
	movl	%ecx, (%rdx)
	movl	116(%rsp,%rax,4), %eax
	movl	%eax, (%rsi)
	movq	g_num_nodes(%rip), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	callq	print_stats
	movl	%ebx, %esi
	jmp	.LBB0_49
.LBB0_53:
	movl	$.L.str, %edi
	movl	$125, %esi
	movl	$1, %edx
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	search_for_move, .Lfunc_end0-search_for_move
	.cfi_endproc

	.p2align	4, 0x90
	.type	negamax,@function
negamax:                                # @negamax
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$3208, %rsp             # imm = 0xC88
.Lcfi19:
	.cfi_def_cfa_offset 3264
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r13d
	movl	%esi, %ebp
	movl	%edi, %r12d
	movl	%r13d, 16(%rsp)
	movl	%r14d, 24(%rsp)
	movl	%ebp, %ebx
	andl	$1, %ebx
	xorl	$1, %ebp
	movq	g_num_nodes(%rip), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, g_num_nodes(%rip)
	movslq	starting_depth(%rip), %rax
	movslq	%r12d, %r15
	subq	%r15, %rax
	incl	stat_nodes(,%rax,4)
	testl	%r15d, %r15d
	jle	.LBB1_3
# BB#1:
	leaq	(%rbx,%rbx,2), %rsi
	movl	g_info_totals(,%rsi,4), %ecx
	movslq	%ebp, %rax
	leaq	(%rax,%rax,2), %rax
	cmpl	g_info_totals+4(,%rax,4), %ecx
	jle	.LBB1_5
# BB#2:
	incl	cut1(%rip)
	movl	$5000, %eax             # imm = 0x1388
	jmp	.LBB1_10
.LBB1_3:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movl	%ebx, %edi
	callq	does_next_player_win
	movl	%eax, %ebx
	movl	$5000, %eax             # imm = 0x1388
	testl	%ebx, %ebx
	jg	.LBB1_10
# BB#4:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	does_who_just_moved_win
	subl	%eax, %ebx
	testl	%eax, %eax
	movl	$-5000, %eax            # imm = 0xEC78
	cmovsl	%ebx, %eax
	jmp	.LBB1_10
.LBB1_5:
	movl	g_info_totals(,%rax,4), %eax
	cmpl	g_info_totals+4(,%rsi,4), %eax
	jge	.LBB1_8
# BB#6:
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movl	$-1, 72(%rsp)
	leaq	20(%rsp), %rdi
	leaq	16(%rsp), %rsi
	leaq	24(%rsp), %rdx
	leaq	72(%rsp), %r8
	movl	%r12d, %ecx
	movl	%ebx, %r9d
	callq	hashlookup
	testl	%eax, %eax
	je	.LBB1_11
# BB#7:
	movl	20(%rsp), %eax
	jmp	.LBB1_10
.LBB1_8:
	incl	cut2(%rip)
.LBB1_9:
	movl	$-5000, %eax            # imm = 0xEC78
.LBB1_10:
	addq	$3208, %rsp             # imm = 0xC88
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_11:
	movl	$-1, 72(%rsp)
	xorl	%esi, %esi
	xorl	%eax, %eax
	movl	%ebx, %edi
	callq	does_next_player_win
	testl	%eax, %eax
	jle	.LBB1_13
# BB#12:
	incl	cut3(%rip)
	movl	$5000, %eax             # imm = 0x1388
	jmp	.LBB1_10
.LBB1_13:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	does_who_just_moved_win
	testl	%eax, %eax
	js	.LBB1_15
# BB#14:
	incl	cut4(%rip)
	jmp	.LBB1_9
.LBB1_15:
	leaq	128(%rsp), %rdi
	movl	%ebx, %esi
	callq	move_generator_stage1
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB1_17
# BB#16:
	movl	$2, 28(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_20
.LBB1_17:
	leaq	128(%rsp), %rdi
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	%ebx, %edx
	callq	move_generator_stage2
	movl	$3, 28(%rsp)            # 4-byte Folded Spill
	testl	%eax, %eax
	je	.LBB1_19
# BB#18:
	movl	%eax, %ebp
	jmp	.LBB1_20
.LBB1_19:
	movl	$.L.str, %edi
	movl	$446, %esi              # imm = 0x1BE
	movl	$1, %edx
	movl	$.L.str.13, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB1_20:
	movq	72(%rsp), %rcx
	movl	80(%rsp), %r8d
	leaq	128(%rsp), %rdi
	movl	%ebp, 60(%rsp)          # 4-byte Spill
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	score_and_get_first
	movl	136(%rsp), %eax
	movl	%eax, 56(%rsp)
	movq	128(%rsp), %rax
	movq	%rax, 48(%rsp)
	leal	-1(%r12), %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movl	%ebx, %eax
	xorl	$1, %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	xorl	%ebp, %ebp
	movl	$1, %eax
	xorl	%edx, %edx
	movl	%r14d, 36(%rsp)         # 4-byte Spill
	movq	%r12, 88(%rsp)          # 8-byte Spill
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	movq	%r15, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_21:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_27 Depth 2
	testl	%edx, %edx
	je	.LBB1_24
# BB#22:                                #   in Loop: Header=BB1_21 Depth=1
	cmpl	$1, %edx
	jne	.LBB1_25
# BB#23:                                #   in Loop: Header=BB1_21 Depth=1
	movl	$1, %esi
	leaq	128(%rsp), %rdi
	movl	60(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %edx
	callq	sort_moves
	movl	%r15d, %eax
	movq	96(%rsp), %r15          # 8-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	cmpl	%eax, %ebp
	jl	.LBB1_26
	jmp	.LBB1_31
.LBB1_24:                               #   in Loop: Header=BB1_21 Depth=1
	movl	$1, %edx
	cmpl	%eax, %ebp
	jl	.LBB1_26
	jmp	.LBB1_31
.LBB1_25:                               #   in Loop: Header=BB1_21 Depth=1
	leaq	128(%rsp), %rdi
	movl	%eax, %esi
	movl	%ebx, %edx
	callq	move_generator_stage2
	movl	$3, %edx
	cmpl	%eax, %ebp
	jge	.LBB1_31
.LBB1_26:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_21 Depth=1
	movl	%edx, 40(%rsp)          # 4-byte Spill
	movslq	%ebp, %rbp
	movl	%eax, 44(%rsp)          # 4-byte Spill
	cltq
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	stat_nth_try(,%rbp,4), %r13
	leaq	(%rbp,%rbp,2), %rax
	leaq	128(%rsp,%rax,4), %r12
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph
                                        #   Parent Loop BB1_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	starting_depth(%rip), %rax
	subq	%r15, %rax
	movl	%ebp, g_move_number(,%rax,4)
	addl	$-2, g_empty_squares(%rip)
	movq	(%r12), %rdi
	movl	8(%r12), %esi
	movl	%ebx, %edx
	callq	toggle_move
	movslq	(%r12), %rax
	movslq	4(%r12), %rcx
	movq	120(%rsp), %r14         # 8-byte Reload
	shlq	$14, %r14
	leaq	(%rax,%rax,2), %rax
	shlq	$9, %rax
	leaq	g_keyinfo(%r14,%rax), %rdx
	shlq	$4, %rcx
	leaq	(%rcx,%rcx,2), %rax
	movl	(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_norm_hashkey(,%rcx,4)
	movl	4(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_norm_hashkey(,%rcx,4)
	movl	8(%rdx,%rax), %ecx
	xorl	%ecx, g_norm_hashkey+16(%rip)
	movl	12(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipV_hashkey(,%rcx,4)
	movl	16(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipV_hashkey(,%rcx,4)
	movl	20(%rdx,%rax), %ecx
	xorl	%ecx, g_flipV_hashkey+16(%rip)
	movl	24(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipH_hashkey(,%rcx,4)
	movl	28(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipH_hashkey(,%rcx,4)
	movl	32(%rdx,%rax), %ecx
	xorl	%ecx, g_flipH_hashkey+16(%rip)
	movl	36(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipVH_hashkey(,%rcx,4)
	movl	40(%rdx,%rax), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$27, %esi
	addl	%ecx, %esi
	sarl	$5, %esi
	movslq	%esi, %rcx
	xorl	%edx, g_flipVH_hashkey(,%rcx,4)
	movslq	(%r12), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$9, %rcx
	leaq	g_keyinfo(%r14,%rcx), %rcx
	movl	44(%rcx,%rax), %eax
	xorl	%eax, g_flipVH_hashkey+16(%rip)
	xorl	%edx, %edx
	subl	24(%rsp), %edx
	xorl	%ecx, %ecx
	subl	16(%rsp), %ecx
	movl	68(%rsp), %edi          # 4-byte Reload
	movl	64(%rsp), %esi          # 4-byte Reload
	callq	negamax
	negl	%eax
	movl	%eax, 20(%rsp)
	addl	$2, g_empty_squares(%rip)
	movq	(%r12), %rdi
	movl	8(%r12), %esi
	movl	%ebx, %edx
	callq	toggle_move
	movslq	(%r12), %rax
	movslq	4(%r12), %rcx
	leaq	(%rax,%rax,2), %rax
	shlq	$9, %rax
	leaq	g_keyinfo(%r14,%rax), %rax
	leaq	(%rcx,%rcx,2), %rdx
	shlq	$4, %rdx
	movl	(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_norm_hashkey(,%rcx,4)
	movl	4(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_norm_hashkey(,%rcx,4)
	movl	8(%rdx,%rax), %ecx
	xorl	%ecx, g_norm_hashkey+16(%rip)
	movl	12(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipV_hashkey(,%rcx,4)
	movl	16(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipV_hashkey(,%rcx,4)
	movl	20(%rdx,%rax), %ecx
	xorl	%ecx, g_flipV_hashkey+16(%rip)
	movl	24(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipH_hashkey(,%rcx,4)
	movl	28(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipH_hashkey(,%rcx,4)
	movl	32(%rdx,%rax), %ecx
	xorl	%ecx, g_flipH_hashkey+16(%rip)
	movl	36(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipVH_hashkey(,%rcx,4)
	movl	40(%rdx,%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rcx
	xorl	%esi, g_flipVH_hashkey(,%rcx,4)
	movl	44(%rdx,%rax), %eax
	xorl	%eax, g_flipVH_hashkey+16(%rip)
	movl	20(%rsp), %edi
	movl	24(%rsp), %ecx
	cmpl	%ecx, %edi
	jge	.LBB1_32
# BB#28:                                #   in Loop: Header=BB1_27 Depth=2
	cmpl	16(%rsp), %edi
	jle	.LBB1_30
# BB#29:                                #   in Loop: Header=BB1_27 Depth=2
	movl	%edi, 16(%rsp)
	movl	8(%r12), %edx
	movl	%edx, 56(%rsp)
	movq	(%r12), %rdx
	movq	%rdx, 48(%rsp)
.LBB1_30:                               #   in Loop: Header=BB1_27 Depth=2
	incq	%rbp
	addq	$4, %r13
	addq	$12, %r12
	cmpq	112(%rsp), %rbp         # 8-byte Folded Reload
	jl	.LBB1_27
	jmp	.LBB1_33
.LBB1_31:                               # %.preheader..loopexit_crit_edge
                                        #   in Loop: Header=BB1_21 Depth=1
	movl	20(%rsp), %edi
	movl	24(%rsp), %ecx
	cmpl	$3, %edx
	jne	.LBB1_34
	jmp	.LBB1_35
.LBB1_32:                               #   in Loop: Header=BB1_21 Depth=1
	movl	%edi, 16(%rsp)
	movl	8(%r12), %edx
	movl	%edx, 56(%rsp)
	movq	(%r12), %rdx
	movq	%rdx, 48(%rsp)
	movslq	starting_depth(%rip), %rdx
	subq	%r15, %rdx
	incl	stat_cutoffs(,%rdx,4)
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rsi
	addq	%rsi, %r13
	cmpl	$5, %ebp
	leaq	stat_nth_try+20(%rdx,%rdx,4), %rdx
	cmovlq	%r13, %rdx
	incl	(%rdx)
.LBB1_33:                               #   in Loop: Header=BB1_21 Depth=1
	movl	36(%rsp), %r14d         # 4-byte Reload
	movq	88(%rsp), %r12          # 8-byte Reload
	movl	32(%rsp), %r13d         # 4-byte Reload
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	40(%rsp), %edx          # 4-byte Reload
	cmpl	$3, %edx
	je	.LBB1_35
.LBB1_34:                               # %.loopexit
                                        #   in Loop: Header=BB1_21 Depth=1
	cmpl	%ecx, %edi
	jl	.LBB1_21
.LBB1_35:
	movl	16(%rsp), %edi
	movq	g_num_nodes(%rip), %rcx
	movl	104(%rsp), %eax         # 4-byte Reload
	subq	%rax, %rcx
	shrq	$5, %rcx
	movl	56(%rsp), %eax
	movl	%eax, 8(%rsp)
	movq	48(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %esi
	movl	%r14d, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r12d, %r8d
	movl	%ebx, %r9d
	callq	hashstore
	movl	16(%rsp), %eax
	jmp	.LBB1_10
.Lfunc_end1:
	.size	negamax, .Lfunc_end1-negamax
	.cfi_endproc

	.p2align	4, 0x90
	.type	print_stats,@function
print_stats:                            # @print_stats
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movl	cut1(%rip), %esi
	movl	cut2(%rip), %edx
	movl	cut3(%rip), %ecx
	movl	cut4(%rip), %r8d
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$stat_nth_try+20, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movl	stat_cutoffs(,%rbx,4), %ecx
	movl	stat_nodes(,%rbx,4), %edx
	movl	%ecx, %eax
	orl	%edx, %eax
	je	.LBB2_3
# BB#2:                                 # %._crit_edge
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movl	-20(%r14), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movl	-16(%r14), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movl	-12(%r14), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movl	-8(%r14), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movl	-4(%r14), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movl	(%r14), %esi
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	printf
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	incq	%rbx
	addq	$40, %r14
	cmpq	$40, %rbx
	jne	.LBB2_1
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	print_stats, .Lfunc_end2-print_stats
	.cfi_endproc

	.type	g_empty_squares,@object # @g_empty_squares
	.bss
	.globl	g_empty_squares
	.p2align	2
g_empty_squares:
	.long	0                       # 0x0
	.size	g_empty_squares, 4

	.type	g_print,@object         # @g_print
	.globl	g_print
	.p2align	2
g_print:
	.long	0                       # 0x0
	.size	g_print, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/obsequi/negamax.c"
	.size	.L.str, 81

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Invalid player.\n"
	.size	.L.str.1, 17

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"No moves"
	.size	.L.str.2, 9

	.type	g_num_nodes,@object     # @g_num_nodes
	.comm	g_num_nodes,8,8
	.type	starting_depth,@object  # @starting_depth
	.local	starting_depth
	.comm	starting_depth,4,4
	.type	g_move_number,@object   # @g_move_number
	.comm	g_move_number,512,16
	.type	g_norm_hashkey,@object  # @g_norm_hashkey
	.comm	g_norm_hashkey,20,4
	.type	g_flipV_hashkey,@object # @g_flipV_hashkey
	.comm	g_flipV_hashkey,20,4
	.type	g_flipH_hashkey,@object # @g_flipH_hashkey
	.comm	g_flipH_hashkey,20,4
	.type	g_flipVH_hashkey,@object # @g_flipVH_hashkey
	.comm	g_flipVH_hashkey,20,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Move (%d,%d), value %d: %s.\n"
	.size	.L.str.3, 29

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"alpha %d, beta %d.\n"
	.size	.L.str.4, 20

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Winner found: %d.\n"
	.size	.L.str.5, 19

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"The value is %d at a depth of %d.\n"
	.size	.L.str.7, 35

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Nodes: %u.\n"
	.size	.L.str.8, 12

	.type	g_first_move,@object    # @g_first_move
	.comm	g_first_move,8192,16
	.type	stat_nth_try,@object    # @stat_nth_try
	.local	stat_nth_try
	.comm	stat_nth_try,1600,16
	.type	stat_cutoffs,@object    # @stat_cutoffs
	.local	stat_cutoffs
	.comm	stat_cutoffs,160,16
	.type	stat_nodes,@object      # @stat_nodes
	.local	stat_nodes
	.comm	stat_nodes,160,16
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%d %d %d %d.\n\n"
	.size	.L.str.9, 15

	.type	cut1,@object            # @cut1
	.local	cut1
	.comm	cut1,4,4
	.type	cut2,@object            # @cut2
	.local	cut2
	.comm	cut2,4,4
	.type	cut3,@object            # @cut3
	.local	cut3
	.comm	cut3,4,4
	.type	cut4,@object            # @cut4
	.local	cut4
	.comm	cut4,4,4
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cutoffs depth %d: (%d) %d -"
	.size	.L.str.10, 28

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	" %d"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	" >%d.\n"
	.size	.L.str.12, 7

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Should always have a move.\n"
	.size	.L.str.13, 28


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
