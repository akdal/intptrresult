	.text
	.file	"parse.bc"
	.globl	mk_leaf
	.p2align	4, 0x90
	.type	mk_leaf,@function
mk_leaf:                                # @mk_leaf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movl	%esi, %r13d
	movl	%edi, %r12d
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbp
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB0_6
# BB#1:
	testq	%rbp, %rbp
	je	.LBB0_6
# BB#2:
	movw	%r13w, 4(%rbx)
	movl	pos_cnt(%rip), %edi
	leal	1(%rdi), %eax
	movl	%eax, pos_cnt(%rip)
	movl	%edi, (%rbx)
	movzwl	%r13w, %eax
	cmpl	$1, %eax
	jne	.LBB0_4
# BB#3:
	movq	%r14, 8(%rbx)
	jmp	.LBB0_5
.LBB0_4:
	movb	%r15b, 8(%rbx)
.LBB0_5:
	movw	%r12w, (%rbp)
	movq	%rbx, 8(%rbp)
	movw	$0, 24(%rbp)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	create_pos
	movq	%rax, 32(%rbp)
	movq	%rax, 40(%rbp)
	movq	%rbp, %rax
.LBB0_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	mk_leaf, .Lfunc_end0-mk_leaf
	.cfi_endproc

	.globl	parse_cset
	.p2align	4, 0x90
	.type	parse_cset,@function
parse_cset:                             # @parse_cset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	(%r12), %rbx
	movb	(%rbx), %al
	xorl	%r15d, %r15d
	testb	%al, %al
	je	.LBB1_17
# BB#1:
	cmpb	$93, %al
	je	.LBB1_17
# BB#2:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	xorl	%r15d, %r15d
                                        # implicit-def: %RCX
	movq	%r14, %r13
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_13:                               #   in Loop: Header=BB1_3 Depth=1
	movb	%cl, 1(%rax)
	movl	$16, %edi
	callq	malloc
	movq	%rax, 8(%r13)
	movq	%rbp, %rbx
	movq	%r13, %rcx
	movq	%rax, %r13
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.LBB1_17
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	cmpb	$93, %al
	je	.LBB1_14
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=1
	movl	$2, %edi
	callq	malloc
	movq	%rax, (%r13)
	leaq	1(%rbx), %rbp
	movq	%rbp, (%r12)
	movzbl	(%rbx), %ecx
	cmpb	$45, %cl
	je	.LBB1_17
# BB#6:                                 #   in Loop: Header=BB1_3 Depth=1
	movb	%cl, (%rax)
	movzbl	(%rbp), %edx
	cmpb	$45, %dl
	je	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_3 Depth=1
	testb	%dl, %dl
	jne	.LBB1_13
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_3 Depth=1
	leaq	2(%rbx), %rdx
	movq	%rdx, (%r12)
	movzbl	2(%rbx), %edx
	testb	%dl, %dl
	je	.LBB1_17
# BB#9:                                 #   in Loop: Header=BB1_3 Depth=1
	cmpb	$45, %dl
	je	.LBB1_17
# BB#10:                                #   in Loop: Header=BB1_3 Depth=1
	cmpb	$93, %dl
	je	.LBB1_17
# BB#11:                                #   in Loop: Header=BB1_3 Depth=1
	cmpb	%cl, %dl
	jl	.LBB1_17
# BB#12:                                #   in Loop: Header=BB1_3 Depth=1
	leaq	3(%rbx), %rbp
	movq	%rbp, (%r12)
	movzbl	2(%rbx), %ecx
	jmp	.LBB1_13
.LBB1_14:
	movq	$0, 8(%rcx)
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r12
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	xorl	%r15d, %r15d
	testq	%r12, %r12
	je	.LBB1_17
# BB#15:
	testq	%rbx, %rbx
	je	.LBB1_17
# BB#16:
	movw	$1, 4(%r12)
	movl	pos_cnt(%rip), %edi
	leal	1(%rdi), %eax
	movl	%eax, pos_cnt(%rip)
	movl	%edi, (%r12)
	movq	%r14, 8(%r12)
	movw	$1, (%rbx)
	movq	%r12, 8(%rbx)
	movw	$0, 24(%rbx)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	create_pos
	movq	%rax, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rbx, %r15
.LBB1_17:                               # %mk_leaf.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	parse_cset, .Lfunc_end1-parse_cset
	.cfi_endproc

	.globl	parse_wildcard
	.p2align	4, 0x90
	.type	parse_wildcard,@function
parse_wildcard:                         # @parse_wildcard
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movl	$2, %edi
	callq	malloc
	movq	%rax, %rbx
	movw	$32513, (%rbx)          # imm = 0x7F01
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	movq	%rbx, (%r14)
	movq	$0, 8(%r14)
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r15
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.LBB2_3
# BB#1:
	testq	%rbx, %rbx
	je	.LBB2_3
# BB#2:
	movw	$1, 4(%r15)
	movl	pos_cnt(%rip), %edi
	leal	1(%rdi), %eax
	movl	%eax, pos_cnt(%rip)
	movl	%edi, (%r15)
	movq	%r14, 8(%r15)
	movw	$1, (%rbx)
	movq	%r15, 8(%rbx)
	movw	$0, 24(%rbx)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	create_pos
	movq	%rax, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rbx, %rax
.LBB2_3:                                # %mk_leaf.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	parse_wildcard, .Lfunc_end2-parse_wildcard
	.cfi_endproc

	.globl	parse_chlit
	.p2align	4, 0x90
	.type	parse_chlit,@function
parse_chlit:                            # @parse_chlit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	testb	%r14b, %r14b
	je	.LBB3_1
# BB#2:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbp
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB3_5
# BB#3:
	testq	%rbx, %rbx
	je	.LBB3_5
# BB#4:
	movw	$0, 4(%rbp)
	movl	pos_cnt(%rip), %edi
	leal	1(%rdi), %eax
	movl	%eax, pos_cnt(%rip)
	movl	%edi, (%rbp)
	movb	%r14b, 8(%rbp)
	movw	$1, (%rbx)
	movq	%rbp, 8(%rbx)
	movw	$0, 24(%rbx)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	create_pos
	movq	%rax, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rbx, %rax
	jmp	.LBB3_5
.LBB3_1:
	xorl	%eax, %eax
.LBB3_5:                                # %mk_leaf.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	parse_chlit, .Lfunc_end3-parse_chlit
	.cfi_endproc

	.globl	get_token
	.p2align	4, 0x90
	.type	get_token,@function
get_token:                              # @get_token
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 64
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB4_26
# BB#1:
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB4_26
# BB#2:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	movsbl	(%rbx), %r13d
	testl	%r13d, %r13d
	je	.LBB4_8
# BB#3:
	leal	-40(%r13), %eax
	cmpl	$52, %eax
	ja	.LBB4_9
# BB#4:
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_5:
	movw	$6, (%r14)
	jmp	.LBB4_24
.LBB4_8:
	movw	$0, (%r14)
	jmp	.LBB4_27
.LBB4_9:
	cmpl	$124, %r13d
	jne	.LBB4_11
# BB#10:
	movw	$3, (%r14)
	jmp	.LBB4_24
.LBB4_11:                               # %.thread
	movw	$1, (%r14)
	jmp	.LBB4_21
.LBB4_12:
	movw	$7, (%r14)
	jmp	.LBB4_24
.LBB4_13:
	movw	$2, (%r14)
	jmp	.LBB4_24
.LBB4_14:
	movw	$1, (%r14)
	movl	$2, %edi
	callq	malloc
	movq	%rax, %r12
	movw	$32513, (%r12)          # imm = 0x7F01
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r15
	movq	%r12, (%r15)
	movq	$0, 8(%r15)
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r13
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r12
	testq	%r13, %r13
	je	.LBB4_25
# BB#15:
	testq	%r12, %r12
	je	.LBB4_25
# BB#16:                                # %parse_wildcard.exit
	movw	$1, 4(%r13)
	movl	pos_cnt(%rip), %edi
	leal	1(%rdi), %eax
	movl	%eax, pos_cnt(%rip)
	movl	%edi, (%r13)
	movq	%r15, 8(%r13)
	movw	$1, (%r12)
	movq	%r13, 8(%r12)
	movw	$0, 24(%r12)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	create_pos
	movq	%rax, 32(%r12)
	movq	%rax, 40(%r12)
	movq	%r12, 8(%r14)
	jmp	.LBB4_24
.LBB4_17:
	movw	$4, (%r14)
	jmp	.LBB4_24
.LBB4_18:
	incq	%rbx
	movq	%rbx, (%rbp)
	movw	$1, (%r14)
	movq	%rbp, %rdi
	callq	parse_cset
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	jne	.LBB4_24
	jmp	.LBB4_26
.LBB4_20:
	leaq	1(%rbx), %rax
	movq	%rax, (%rbp)
	movb	1(%rbx), %r13b
	movw	$1, (%r14)
	testb	%r13b, %r13b
	je	.LBB4_25
.LBB4_21:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r12
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r15
	testq	%r12, %r12
	je	.LBB4_25
# BB#22:
	testq	%r15, %r15
	je	.LBB4_25
# BB#23:                                # %parse_chlit.exit
	movw	$0, 4(%r12)
	movl	pos_cnt(%rip), %edi
	leal	1(%rdi), %eax
	movl	%eax, pos_cnt(%rip)
	movl	%edi, (%r12)
	movb	%r13b, 8(%r12)
	movw	$1, (%r15)
	movq	%r12, 8(%r15)
	movw	$0, 24(%r15)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	create_pos
	movq	%rax, 32(%r15)
	movq	%rax, 40(%r15)
	movq	%r15, 8(%r14)
.LBB4_24:
	incq	(%rbp)
	jmp	.LBB4_27
.LBB4_25:                               # %parse_wildcard.exit.thread
	movq	$0, 8(%r14)
.LBB4_26:
	xorl	%r14d, %r14d
.LBB4_27:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	get_token, .Lfunc_end4-get_token
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_5
	.quad	.LBB4_12
	.quad	.LBB4_13
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_14
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_17
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_11
	.quad	.LBB4_18
	.quad	.LBB4_20

	.text
	.globl	cat2
	.p2align	4, 0x90
	.type	cat2,@function
cat2:                                   # @cat2
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 32
.Lcfi54:
	.cfi_offset %rbx, -24
.Lcfi55:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB5_8
# BB#1:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB5_8
# BB#2:
	cmpq	$0, 16(%rax)
	je	.LBB5_15
# BB#3:
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_8
# BB#4:
	movw	$5, (%r14)
	movq	%rbx, %rdi
	callq	Pop
	movq	%rax, 16(%r14)
	movq	%rbx, %rdi
	callq	Pop
	movq	%rax, 8(%r14)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	Push
	testq	%rax, %rax
	je	.LBB5_8
# BB#5:
	movq	8(%r14), %rax
	cmpw	$0, 24(%rax)
	je	.LBB5_9
# BB#6:
	movq	16(%r14), %rcx
	cmpw	$0, 24(%rcx)
	setne	%cl
	jmp	.LBB5_10
.LBB5_8:
	xorl	%eax, %eax
.LBB5_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_9:
	xorl	%ecx, %ecx
.LBB5_10:
	movzbl	%cl, %ecx
	movw	%cx, 24(%r14)
	cmpw	$0, 24(%rax)
	movq	32(%rax), %rax
	je	.LBB5_12
# BB#11:
	movq	16(%r14), %rcx
	movq	32(%rcx), %rsi
	movq	%rax, %rdi
	callq	pset_union
.LBB5_12:
	movq	%rax, 32(%r14)
	movq	16(%r14), %rax
	cmpw	$0, 24(%rax)
	movq	40(%rax), %rax
	je	.LBB5_14
# BB#13:
	movq	8(%r14), %rcx
	movq	40(%rcx), %rdi
	movq	%rax, %rsi
	callq	pset_union
.LBB5_14:
	movq	%rax, 40(%r14)
	movq	(%rbx), %rax
	jmp	.LBB5_15
.Lfunc_end5:
	.size	cat2, .Lfunc_end5-cat2
	.cfi_endproc

	.globl	wrap
	.p2align	4, 0x90
	.type	wrap,@function
wrap:                                   # @wrap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 32
.Lcfi59:
	.cfi_offset %rbx, -32
.Lcfi60:
	.cfi_offset %r14, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB6_8
# BB#1:
	cmpq	$0, (%rbx)
	je	.LBB6_8
# BB#2:
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB6_8
# BB#3:
	movw	%bp, (%r14)
	movq	%rbx, %rdi
	callq	Pop
	movq	%rax, 8(%r14)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	Push
	testq	%rax, %rax
	je	.LBB6_8
# BB#4:
	movw	$1, 24(%r14)
	movq	8(%r14), %rax
	movups	32(%rax), %xmm0
	movups	%xmm0, 32(%r14)
	movq	(%rbx), %rax
	jmp	.LBB6_9
.LBB6_8:
	xorl	%eax, %eax
.LBB6_9:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end6:
	.size	wrap, .Lfunc_end6-wrap
	.cfi_endproc

	.globl	mk_alt
	.p2align	4, 0x90
	.type	mk_alt,@function
mk_alt:                                 # @mk_alt
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 32
.Lcfi65:
	.cfi_offset %rbx, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.LBB7_10
# BB#1:
	testq	%rbx, %rbx
	je	.LBB7_10
# BB#2:
	movq	(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB7_10
# BB#3:
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB7_9
# BB#4:
	movw	$3, (%r14)
	movq	%r15, %rdi
	callq	Pop
	movq	%rax, 8(%r14)
	movq	%rbx, 16(%r14)
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	Push
	testq	%rax, %rax
	je	.LBB7_9
# BB#5:
	movq	8(%r14), %rax
	movq	16(%r14), %rcx
	movb	$1, %dl
	cmpw	$0, 24(%rax)
	jne	.LBB7_7
# BB#6:
	cmpw	$0, 24(%rcx)
	setne	%dl
.LBB7_7:                                # %._crit_edge
	movzbl	%dl, %edx
	movw	%dx, 24(%r14)
	movq	32(%rax), %rdi
	movq	32(%rcx), %rsi
	callq	pset_union
	movq	%rax, 32(%r14)
	movq	8(%r14), %rax
	movq	16(%r14), %rcx
	movq	40(%rax), %rdi
	movq	40(%rcx), %rsi
	callq	pset_union
	movq	%rax, 40(%r14)
	movq	(%r15), %rax
	jmp	.LBB7_10
.LBB7_9:
	xorl	%eax, %eax
.LBB7_10:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	mk_alt, .Lfunc_end7-mk_alt
	.cfi_endproc

	.globl	parse_re
	.p2align	4, 0x90
	.type	parse_re,@function
parse_re:                               # @parse_re
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 80
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbp
	movq	$0, 8(%rsp)
	testq	%rbp, %rbp
	je	.LBB8_28
# BB#1:
	cmpq	$0, (%rbp)
	je	.LBB8_28
# BB#2:                                 # %.preheader
	movq	%rbp, %rdi
	callq	get_token
	xorl	%r13d, %r13d
	testq	%rax, %rax
	je	.LBB8_29
# BB#3:                                 # %.lr.ph
	leaq	8(%rsp), %r15
	leaq	16(%rsp), %r12
	.p2align	4, 0x90
.LBB8_4:                                # =>This Inner Loop Header: Depth=1
	movswl	(%rax), %esi
	cmpl	$7, %esi
	ja	.LBB8_19
# BB#5:                                 #   in Loop: Header=BB8_4 Depth=1
	jmpq	*.LJTI8_0(,%rsi,8)
.LBB8_6:                                #   in Loop: Header=BB8_4 Depth=1
	movq	8(%rax), %rsi
	movq	%r15, %rdi
	callq	Push
	testq	%rax, %rax
	je	.LBB8_29
# BB#7:                                 #   in Loop: Header=BB8_4 Depth=1
	movq	8(%rsp), %rdi
	callq	Size
	cmpl	$2, %eax
	jg	.LBB8_24
	jmp	.LBB8_26
.LBB8_8:                                #   in Loop: Header=BB8_4 Depth=1
	cmpq	$0, 8(%rsp)
	je	.LBB8_29
# BB#9:                                 #   in Loop: Header=BB8_4 Depth=1
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB8_29
# BB#10:                                #   in Loop: Header=BB8_4 Depth=1
	movw	$2, (%rbx)
	jmp	.LBB8_17
.LBB8_11:                               #   in Loop: Header=BB8_4 Depth=1
	movq	%r15, %rdi
	callq	cat2
	testq	%rax, %rax
	je	.LBB8_29
# BB#12:                                #   in Loop: Header=BB8_4 Depth=1
	movswl	%r14w, %esi
	movq	%rbp, %rdi
	callq	parse_re
	testq	%rax, %rax
	je	.LBB8_29
# BB#13:                                #   in Loop: Header=BB8_4 Depth=1
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	mk_alt
	testq	%rax, %rax
	jne	.LBB8_26
	jmp	.LBB8_29
.LBB8_14:                               #   in Loop: Header=BB8_4 Depth=1
	cmpq	$0, 8(%rsp)
	je	.LBB8_29
# BB#15:                                #   in Loop: Header=BB8_4 Depth=1
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB8_29
# BB#16:                                #   in Loop: Header=BB8_4 Depth=1
	movw	$4, (%rbx)
.LBB8_17:                               #   in Loop: Header=BB8_4 Depth=1
	movq	%r15, %rdi
	callq	Pop
	movq	%rax, 8(%rbx)
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	Push
	testq	%rax, %rax
	je	.LBB8_29
# BB#18:                                # %wrap.exit
                                        #   in Loop: Header=BB8_4 Depth=1
	movw	$1, 24(%rbx)
	movq	8(%rbx), %rax
	movups	32(%rax), %xmm0
	movups	%xmm0, 32(%rbx)
	cmpq	$0, 8(%rsp)
	jne	.LBB8_26
	jmp	.LBB8_29
.LBB8_19:                               #   in Loop: Header=BB8_4 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	jmp	.LBB8_26
.LBB8_20:                               #   in Loop: Header=BB8_4 Depth=1
	movl	$7, %esi
	movq	%rbp, %rdi
	callq	parse_re
	movq	%rax, %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	Push
	testq	%rax, %rax
	je	.LBB8_29
# BB#21:                                #   in Loop: Header=BB8_4 Depth=1
	movq	%rbp, %rdi
	callq	get_token
	testq	%rbx, %rbx
	je	.LBB8_29
# BB#22:                                #   in Loop: Header=BB8_4 Depth=1
	movzwl	(%rax), %eax
	cmpl	$7, %eax
	jne	.LBB8_29
# BB#23:                                #   in Loop: Header=BB8_4 Depth=1
	movq	8(%rsp), %rdi
	callq	Size
	cmpl	$3, %eax
	jl	.LBB8_26
.LBB8_24:                               #   in Loop: Header=BB8_4 Depth=1
	movq	8(%rsp), %rax
	movq	16(%rax), %rax
	movq	%rax, 16(%rsp)
	movq	%r12, %rdi
	callq	cat2
	movq	8(%rsp), %rcx
	movq	%rax, 16(%rcx)
	testq	%rax, %rax
	je	.LBB8_29
# BB#25:                                #   in Loop: Header=BB8_4 Depth=1
	movl	8(%rax), %eax
	incl	%eax
	movl	%eax, 8(%rcx)
	.p2align	4, 0x90
.LBB8_26:                               # %.backedge
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	%rbp, %rdi
	callq	get_token
	testq	%rax, %rax
	jne	.LBB8_4
	jmp	.LBB8_29
.LBB8_30:
	decq	(%rbp)
.LBB8_31:                               # %.loopexit
	cmpw	%r14w, %si
	jne	.LBB8_28
# BB#32:
	leaq	8(%rsp), %rdi
	callq	cat2
	movq	%rax, %rdi
	callq	Top
	movq	%rax, %r13
	jmp	.LBB8_29
.LBB8_28:
	xorl	%r13d, %r13d
.LBB8_29:                               # %wrap.exit.thread
	movq	%r13, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	parse_re, .Lfunc_end8-parse_re
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_31
	.quad	.LBB8_6
	.quad	.LBB8_8
	.quad	.LBB8_11
	.quad	.LBB8_14
	.quad	.LBB8_19
	.quad	.LBB8_20
	.quad	.LBB8_30

	.text
	.globl	parse
	.p2align	4, 0x90
	.type	parse,@function
parse:                                  # @parse
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi84:
	.cfi_def_cfa_offset 48
.Lcfi85:
	.cfi_offset %rbx, -32
.Lcfi86:
	.cfi_offset %r14, -24
.Lcfi87:
	.cfi_offset %r15, -16
	movq	%rdi, 8(%rsp)
	movq	$0, (%rsp)
	leaq	8(%rsp), %rdi
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	callq	parse_re
	testq	%rax, %rax
	je	.LBB9_8
# BB#1:
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	Push
	testq	%rax, %rax
	je	.LBB9_7
# BB#2:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r15
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r14
	xorl	%ebx, %ebx
	testq	%r15, %r15
	je	.LBB9_8
# BB#3:
	testq	%r14, %r14
	je	.LBB9_8
# BB#4:
	movw	$0, 4(%r15)
	movl	pos_cnt(%rip), %edi
	leal	1(%rdi), %eax
	movl	%eax, pos_cnt(%rip)
	movl	%edi, (%r15)
	movb	$0, 8(%r15)
	movw	$0, (%r14)
	movq	%r15, 8(%r14)
	movw	$0, 24(%r14)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	create_pos
	movq	%rax, 32(%r14)
	movq	%rax, 40(%r14)
	movq	%rsp, %rdi
	movq	%r14, %rsi
	callq	Push
	testq	%rax, %rax
	je	.LBB9_7
# BB#5:
	movl	pos_cnt(%rip), %eax
	decl	%eax
	movl	%eax, pos_cnt(%rip)
	movl	%eax, final_pos(%rip)
	movq	%rsp, %rdi
	callq	cat2
	movq	%rax, %rdi
	callq	Top
	movq	%rax, %rbx
	jmp	.LBB9_8
.LBB9_7:
	xorl	%ebx, %ebx
.LBB9_8:                                # %mk_leaf.exit.thread
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	parse, .Lfunc_end9-parse
	.cfi_endproc

	.type	pos_cnt,@object         # @pos_cnt
	.bss
	.globl	pos_cnt
	.p2align	2
pos_cnt:
	.long	0                       # 0x0
	.size	pos_cnt, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"parse_re: unknown token type %d\n"
	.size	.L.str, 33

	.type	final_pos,@object       # @final_pos
	.comm	final_pos,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
