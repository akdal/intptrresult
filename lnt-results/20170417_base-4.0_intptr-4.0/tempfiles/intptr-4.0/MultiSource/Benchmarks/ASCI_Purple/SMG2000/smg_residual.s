	.text
	.file	"smg_residual.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.text
	.globl	hypre_SMGResidualCreate
	.p2align	4, 0x90
	.type	hypre_SMGResidualCreate,@function
hypre_SMGResidualCreate:                # @hypre_SMGResidualCreate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$80, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	$.L.str, %edi
	callq	hypre_InitializeTiming
	movl	%eax, 72(%rbx)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,0,0,1]
	movups	%xmm0, (%rbx)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 16(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	hypre_SMGResidualCreate, .Lfunc_end0-hypre_SMGResidualCreate
	.cfi_endproc

	.globl	hypre_SMGResidualSetup
	.p2align	4, 0x90
	.type	hypre_SMGResidualSetup,@function
hypre_SMGResidualSetup:                 # @hypre_SMGResidualSetup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 160
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	12(%rbx), %r12
	movq	8(%r14), %r13
	movq	24(%r14), %rbp
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 92(%rsp)
	movl	$1, 100(%rsp)
	movq	8(%r13), %rdi
	callq	hypre_BoxArrayDuplicate
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	hypre_ProjectBoxArray
	leaq	8(%rsp), %rax
	leaq	16(%rsp), %r10
	leaq	80(%rsp), %rdx
	leaq	72(%rsp), %rcx
	leaq	64(%rsp), %r8
	leaq	56(%rsp), %r9
	movq	%r13, %rdi
	movq	%rbp, %rsi
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CreateComputeInfo
	addq	$16, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -16
	movq	16(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	8(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	80(%rsp), %rdi
	movq	72(%rsp), %rsi
	movq	64(%rsp), %r8
	movq	56(%rsp), %r9
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rax
	leaq	100(%rsp), %rdx
	movq	%rdx, %rcx
	pushq	%rax
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	16(%r15)
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	callq	hypre_ComputePkgCreate
	addq	$64, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset -64
	movq	%r14, %rdi
	callq	hypre_StructMatrixRef
	movq	%rax, 24(%rbx)
	movq	%r15, %rdi
	callq	hypre_StructVectorRef
	movq	%rax, 32(%rbx)
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	hypre_StructVectorRef
	movq	%rax, 40(%rbx)
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	hypre_StructVectorRef
	movq	%rax, 48(%rbx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 56(%rbx)
	movq	48(%rsp), %rax
	movq	%rax, 64(%rbx)
	movl	72(%r15), %eax
	addl	112(%r14), %eax
	movl	16(%rbx), %ecx
	imull	12(%rbx), %ecx
	imull	20(%rbx), %ecx
	cltd
	idivl	%ecx
	movl	%eax, 76(%rbx)
	xorl	%eax, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_SMGResidualSetup, .Lfunc_end1-hypre_SMGResidualSetup
	.cfi_endproc

	.globl	hypre_SMGResidual
	.p2align	4, 0x90
	.type	hypre_SMGResidual,@function
hypre_SMGResidual:                      # @hypre_SMGResidual
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$456, %rsp              # imm = 0x1C8
.Lcfi33:
	.cfi_def_cfa_offset 512
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%r8, 312(%rsp)          # 8-byte Spill
	movq	%rcx, 376(%rsp)         # 8-byte Spill
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movq	%rsi, %rbx
	leaq	12(%rdi), %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movq	56(%rdi), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movq	64(%rdi), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	%rdi, 184(%rsp)         # 8-byte Spill
	movl	72(%rdi), %edi
	callq	hypre_BeginTiming
	movq	%rbx, 328(%rsp)         # 8-byte Spill
	movq	24(%rbx), %rax
	movq	(%rax), %rcx
	movq	%rcx, 400(%rsp)         # 8-byte Spill
	movl	8(%rax), %eax
	movq	%rax, 344(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
                                        #       Child Loop BB2_9 Depth 3
                                        #         Child Loop BB2_11 Depth 4
                                        #           Child Loop BB2_13 Depth 5
                                        #           Child Loop BB2_17 Depth 5
                                        #     Child Loop BB2_25 Depth 2
                                        #       Child Loop BB2_27 Depth 3
                                        #         Child Loop BB2_29 Depth 4
                                        #           Child Loop BB2_33 Depth 5
                                        #             Child Loop BB2_35 Depth 6
                                        #               Child Loop BB2_47 Depth 7
                                        #               Child Loop BB2_42 Depth 7
	cmpl	$1, %eax
	movl	%eax, 260(%rsp)         # 4-byte Spill
	je	.LBB2_22
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	testl	%eax, %eax
	jne	.LBB2_23
# BB#3:                                 #   in Loop: Header=BB2_1 Depth=1
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %rsi
	movq	296(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	leaq	448(%rsp), %rdx
	callq	hypre_InitializeIndtComputations
	movq	8(%rbx), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	304(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, 8(%rcx)
	jle	.LBB2_23
# BB#4:                                 # %.lr.ph737.preheader
                                        #   in Loop: Header=BB2_1 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph737
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_9 Depth 3
                                        #         Child Loop BB2_11 Depth 4
                                        #           Child Loop BB2_13 Depth 5
                                        #           Child Loop BB2_17 Depth 5
	movq	(%rcx), %r13
	leaq	(,%rsi,8), %rax
	leaq	(%rax,%rax,2), %rbx
	leaq	(%r13,%rbx), %rdi
	movq	376(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	(%rax), %r14
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	(%rax), %rbp
	movq	40(%rdx), %rax
	movslq	(%rax,%rsi,4), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	40(%rcx), %rax
	movq	%rsi, 208(%rsp)         # 8-byte Spill
	movslq	(%rax,%rsi,4), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	320(%rsp), %rsi         # 8-byte Reload
	leaq	164(%rsp), %rdx
	callq	hypre_BoxGetStrideSize
	movl	(%rbp,%rbx), %ecx
	movl	4(%rbp,%rbx), %r10d
	movl	12(%rbp,%rbx), %eax
	movl	%eax, %r15d
	subl	%ecx, %r15d
	incl	%r15d
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	cmpl	%ecx, %eax
	movl	16(%rbp,%rbx), %eax
	movl	$0, %edx
	cmovsl	%edx, %r15d
	movl	%eax, %r8d
	subl	%r10d, %r8d
	incl	%r8d
	cmpl	%r10d, %eax
	movl	(%r14,%rbx), %r12d
	movl	4(%r14,%rbx), %edi
	movl	12(%r14,%rbx), %eax
	cmovsl	%edx, %r8d
	movl	%eax, %r9d
	subl	%r12d, %r9d
	incl	%r9d
	cmpl	%r12d, %eax
	movl	16(%r14,%rbx), %ecx
	cmovsl	%edx, %r9d
	movl	%ecx, %eax
	subl	%edi, %eax
	incl	%eax
	cmpl	%edi, %ecx
	cmovsl	%edx, %eax
	movl	164(%rsp), %ecx
	movl	168(%rsp), %edx
	movl	172(%rsp), %esi
	cmpl	%ecx, %edx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	cmovgel	%edx, %ecx
	cmpl	%ecx, %esi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	cmovgel	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB2_21
# BB#6:                                 # %.lr.ph731
                                        #   in Loop: Header=BB2_5 Depth=2
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_21
# BB#7:                                 # %.lr.ph731
                                        #   in Loop: Header=BB2_5 Depth=2
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_21
# BB#8:                                 # %.preheader697.us.preheader
                                        #   in Loop: Header=BB2_5 Depth=2
	movl	8(%r13,%rbx), %ecx
	movl	%edi, 72(%rsp)          # 4-byte Spill
	movl	%ecx, %r11d
	subl	8(%rbp,%rbx), %r11d
	subl	8(%r14,%rbx), %ecx
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	movl	(%r13,%rbx), %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movl	4(%r13,%rbx), %r14d
	movl	%r15d, %ebx
	imull	%r8d, %ebx
	movq	184(%rsp), %rdx         # 8-byte Reload
	movl	20(%rdx), %esi
	imull	%esi, %ebx
	movl	%r9d, %ecx
	imull	%eax, %ecx
	imull	%esi, %ecx
	movl	16(%rdx), %r13d
	movl	%r10d, 104(%rsp)        # 4-byte Spill
	movl	%r12d, 112(%rsp)        # 4-byte Spill
	movl	%r15d, %r10d
	imull	%r13d, %r10d
	imull	%r9d, %r13d
	movslq	12(%rdx), %rsi
	movl	%esi, %ebp
	negl	%ebp
	imull	40(%rsp), %ebp          # 4-byte Folded Reload
	leal	(%rbp,%r13), %r12d
	addl	%r10d, %ebp
	movq	48(%rsp), %rdx          # 8-byte Reload
	leal	-1(%rdx), %edi
	imull	%edi, %r12d
	imull	%edi, %ebp
	addl	%r13d, %r12d
	movl	%esi, %edi
	imull	40(%rsp), %edi          # 4-byte Folded Reload
	subl	%edi, %r12d
	movl	%r12d, 176(%rsp)        # 4-byte Spill
	addl	%r10d, %ebp
	subl	%edi, %ebp
	movq	%rbp, 232(%rsp)         # 8-byte Spill
	movl	64(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edi
	subl	24(%rsp), %edi          # 4-byte Folded Reload
	movl	%r14d, %edx
	subl	104(%rsp), %edx         # 4-byte Folded Reload
	imull	%r8d, %r11d
	addl	%edx, %r11d
	movl	80(%rsp), %edx          # 4-byte Reload
	imull	%r15d, %r11d
	addl	%edi, %r11d
	movl	%r11d, %edi
	subl	112(%rsp), %ebp         # 4-byte Folded Reload
	subl	72(%rsp), %r14d         # 4-byte Folded Reload
	imull	%eax, %edx
	addl	%r14d, %edx
	movq	48(%rsp), %r8           # 8-byte Reload
	movl	%r8d, %eax
	movq	%r13, 64(%rsp)          # 8-byte Spill
	imull	%r13d, %eax
	movl	%eax, 224(%rsp)         # 4-byte Spill
	subl	%eax, %ecx
	movl	%ecx, 240(%rsp)         # 4-byte Spill
	imull	%r9d, %edx
	movl	%r8d, %eax
	movl	%r10d, 112(%rsp)        # 4-byte Spill
	imull	%r10d, %eax
	movl	%eax, 216(%rsp)         # 4-byte Spill
	subl	%eax, %ebx
	movl	%ebx, 248(%rsp)         # 4-byte Spill
	addl	%ebp, %edx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r14d
	andl	$3, %r14d
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbp
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	shlq	$3, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	shlq	$5, %r15
	movq	%rsi, %r10
	shlq	$4, %r10
	leaq	(,%rsi,8), %rbp
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rbp,2), %r8
	leaq	(%rcx,%rsi,8), %r11
	leaq	(%rcx,%r8), %rbp
	leaq	(%rcx,%r10), %r12
	leal	-1(%rbx), %ecx
	movl	%ecx, 104(%rsp)         # 4-byte Spill
	xorl	%ebx, %ebx
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_9:                                # %.preheader697.us
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_11 Depth 4
                                        #           Child Loop BB2_13 Depth 5
                                        #           Child Loop BB2_17 Depth 5
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movq	232(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	176(%rsp), %ecx         # 4-byte Reload
	jle	.LBB2_20
# BB#10:                                # %.preheader696.us.us.preheader
                                        #   in Loop: Header=BB2_9 Depth=3
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	xorl	%eax, %eax
	movl	%edx, 80(%rsp)          # 4-byte Spill
	movl	%edx, %r13d
	movl	%edi, 12(%rsp)          # 4-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	.p2align	4, 0x90
.LBB2_11:                               # %.preheader696.us.us
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_9 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_13 Depth 5
                                        #           Child Loop BB2_17 Depth 5
	movl	%eax, 56(%rsp)          # 4-byte Spill
	testl	%r14d, %r14d
	movslq	%edi, %rdi
	movslq	%r13d, %r13
	movq	%rdi, %rdx
	movq	%r13, %rcx
	movl	$0, %eax
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	je	.LBB2_15
# BB#12:                                # %.prol.preheader
                                        #   in Loop: Header=BB2_11 Depth=4
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,8), %rdx
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r13,8), %rbx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_13:                               #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_9 Depth=3
                                        #         Parent Loop BB2_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rdx,%rcx,8)
	incl	%eax
	addq	%rsi, %rcx
	cmpl	%eax, %r14d
	jne	.LBB2_13
# BB#14:                                # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB2_11 Depth=4
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx), %rdx
	addq	%r13, %rcx
.LBB2_15:                               # %.prol.loopexit
                                        #   in Loop: Header=BB2_11 Depth=4
	movq	%r13, 32(%rsp)          # 8-byte Spill
	cmpl	$3, 104(%rsp)           # 4-byte Folded Reload
	movq	%r11, %r9
	movq	%r10, %r11
	movq	%r8, %r10
	movq	%r12, %rsi
	jb	.LBB2_18
# BB#16:                                # %.preheader696.us.us.new
                                        #   in Loop: Header=BB2_11 Depth=4
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rdx,8), %r13
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rdx,8), %r8
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	movq	136(%rsp), %rdx         # 8-byte Reload
	leaq	(%rcx,%rdx), %r12
	movq	40(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%eax, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_17:                               #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        #       Parent Loop BB2_9 Depth=3
                                        #         Parent Loop BB2_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rcx,%rax), %rdi
	movq	%rdi, (%r13,%rax)
	movq	(%r12,%rax), %rdi
	leaq	(%r8,%rax), %rbx
	movq	%rdi, (%r9,%rbx)
	leaq	(%r11,%rax), %rdi
	movq	(%rcx,%rdi), %rdi
	movq	%rdi, (%rsi,%rbx)
	leaq	(%r10,%rax), %rdi
	movq	(%rcx,%rdi), %rdi
	movq	%rdi, (%rbp,%rbx)
	movq	%r15, %rdi
	addq	%rdi, %rax
	addl	$-4, %edx
	jne	.LBB2_17
.LBB2_18:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB2_11 Depth=4
	movq	32(%rsp), %r13          # 8-byte Reload
	addl	64(%rsp), %r13d         # 4-byte Folded Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	addl	112(%rsp), %edi         # 4-byte Folded Reload
	movl	56(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	48(%rsp), %eax          # 4-byte Folded Reload
	movq	%r10, %r8
	movq	%r11, %r10
	movq	%r9, %r11
	movq	%rsi, %r12
	movq	96(%rsp), %rsi          # 8-byte Reload
	jne	.LBB2_11
# BB#19:                                #   in Loop: Header=BB2_9 Depth=3
	movl	216(%rsp), %eax         # 4-byte Reload
	movl	224(%rsp), %ecx         # 4-byte Reload
	movl	80(%rsp), %edx          # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movl	8(%rsp), %ebx           # 4-byte Reload
.LBB2_20:                               # %._crit_edge705.us
                                        #   in Loop: Header=BB2_9 Depth=3
	addl	%edx, %ecx
	addl	%edi, %eax
	addl	240(%rsp), %ecx         # 4-byte Folded Reload
	addl	248(%rsp), %eax         # 4-byte Folded Reload
	incl	%ebx
	cmpl	4(%rsp), %ebx           # 4-byte Folded Reload
	movl	%ecx, %edx
	movl	%eax, %edi
	jne	.LBB2_9
.LBB2_21:                               # %._crit_edge732
                                        #   in Loop: Header=BB2_5 Depth=2
	movq	208(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	movq	304(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rsi
	jl	.LBB2_5
	jmp	.LBB2_23
	.p2align	4, 0x90
.LBB2_22:                               #   in Loop: Header=BB2_1 Depth=1
	movq	448(%rsp), %rdi
	callq	hypre_FinalizeIndtComputations
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
.LBB2_23:                               # %.loopexit
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	jle	.LBB2_57
# BB#24:                                # %.lr.ph808
                                        #   in Loop: Header=BB2_1 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_25:                               #   Parent Loop BB2_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_27 Depth 3
                                        #         Child Loop BB2_29 Depth 4
                                        #           Child Loop BB2_33 Depth 5
                                        #             Child Loop BB2_35 Depth 6
                                        #               Child Loop BB2_47 Depth 7
                                        #               Child Loop BB2_42 Depth 7
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax,%rdi,8), %rbp
	movl	8(%rbp), %eax
	testl	%eax, %eax
	jle	.LBB2_56
# BB#26:                                # %.lr.ph800
                                        #   in Loop: Header=BB2_25 Depth=2
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	40(%rcx), %rcx
	movq	(%rcx), %rbx
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	312(%rsp), %rsi         # 8-byte Reload
	movq	16(%rsi), %rdx
	movq	24(%rsi), %r8
	movq	(%rdx), %r9
	movq	40(%rsi), %rsi
	movslq	(%rsi,%rdi,4), %rsi
	leaq	(%r8,%rsi,8), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	(,%rdi,8), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	16(%rcx,%rdx), %rsi
	movq	%rsi, 200(%rsp)         # 8-byte Spill
	addq	%rdx, %rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	leaq	4(%rbx,%rdx), %rcx
	movq	%rcx, 360(%rsp)         # 8-byte Spill
	addq	%rdx, %rbx
	movq	%rbx, 416(%rsp)         # 8-byte Spill
	leaq	(%r9,%rdx), %rcx
	movq	%rcx, 408(%rsp)         # 8-byte Spill
	leaq	4(%r9,%rdx), %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%rdi, 424(%rsp)         # 8-byte Spill
	movq	%rbp, 384(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_27:                               #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_25 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_29 Depth 4
                                        #           Child Loop BB2_33 Depth 5
                                        #             Child Loop BB2_35 Depth 6
                                        #               Child Loop BB2_47 Depth 7
                                        #               Child Loop BB2_42 Depth 7
	cmpl	$0, 344(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_54
# BB#28:                                # %.lr.ph792
                                        #   in Loop: Header=BB2_27 Depth=3
	movq	(%rbp), %rax
	movq	%rdx, 392(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %esi
	movl	-12(%rax), %ecx
	movl	%ecx, 120(%rsp)         # 4-byte Spill
	movl	-4(%rax), %edx
	movq	336(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	movl	%eax, 124(%rsp)         # 4-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_29:                               #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_25 Depth=2
                                        #       Parent Loop BB2_27 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_33 Depth 5
                                        #             Child Loop BB2_35 Depth 6
                                        #               Child Loop BB2_47 Depth 7
                                        #               Child Loop BB2_42 Depth 7
	movl	%edx, %r12d
	movl	124(%rsp), %r15d        # 4-byte Reload
	movl	%esi, %ebp
	movl	120(%rsp), %eax         # 4-byte Reload
	subl	%eax, %ebp
	incl	%ebp
	cmpl	%eax, %esi
	movq	328(%rsp), %rax         # 8-byte Reload
	movq	48(%rax), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	64(%rax), %rax
	movq	(%rax,%rdi,8), %rax
	movslq	(%rax,%rcx,4), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	40(%rax), %rax
	movslq	(%rax,%rdi,4), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rcx, 432(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rax
	movq	400(%rsp), %rcx         # 8-byte Reload
	movslq	(%rcx,%rax,4), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	$0, %r13d
	cmovsl	%r13d, %ebp
	imull	8(%rcx,%rax,4), %ebp
	addl	4(%rcx,%rax,4), %ebp
	movq	368(%rsp), %rdi         # 8-byte Reload
	movq	320(%rsp), %rsi         # 8-byte Reload
	leaq	164(%rsp), %rdx
	callq	hypre_BoxGetStrideSize
	movq	408(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r9d
	movq	352(%rsp), %rax         # 8-byte Reload
	movl	8(%rax), %ecx
	movl	%ecx, %r8d
	subl	%r9d, %r8d
	incl	%r8d
	cmpl	%r9d, %ecx
	movl	(%rax), %r11d
	movl	12(%rax), %ecx
	cmovsl	%r13d, %r8d
	movl	%ecx, %esi
	subl	%r11d, %esi
	incl	%esi
	cmpl	%r11d, %ecx
	movq	336(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edx
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	-4(%rax), %edi
	cmovsl	%r13d, %esi
	movl	%edi, %r10d
	subl	%edx, %r10d
	incl	%r10d
	movl	%edx, 124(%rsp)         # 4-byte Spill
	movl	%edi, 268(%rsp)         # 4-byte Spill
	cmpl	%edx, %edi
	movl	-12(%rax), %edx
	movl	(%rax), %eax
	cmovsl	%r13d, %r10d
	movl	%eax, %ebx
	subl	%edx, %ebx
	incl	%ebx
	movl	%edx, 120(%rsp)         # 4-byte Spill
	movl	%eax, 264(%rsp)         # 4-byte Spill
	cmpl	%edx, %eax
	movq	416(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	movq	360(%rsp), %rax         # 8-byte Reload
	movl	8(%rax), %edi
	cmovsl	%r13d, %ebx
	movl	%edi, %edx
	subl	%ecx, %edx
	incl	%edx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	cmpl	%ecx, %edi
	movl	(%rax), %ecx
	cmovsl	%r13d, %edx
	movl	12(%rax), %edi
	movl	%edi, %r14d
	subl	%ecx, %r14d
	incl	%r14d
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	cmpl	%ecx, %edi
	cmovsl	%r13d, %r14d
	movl	%r12d, %edi
	subl	%r15d, %edi
	incl	%edi
	cmpl	%r15d, %r12d
	cmovsl	%r13d, %edi
	movl	164(%rsp), %eax
	movl	168(%rsp), %ecx
	cmpl	%eax, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	cmovgel	%ecx, %eax
	movl	172(%rsp), %ecx
	cmpl	%eax, %ecx
	movl	%ecx, 160(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %eax
	testl	%eax, %eax
	jle	.LBB2_52
# BB#30:                                # %.lr.ph786
                                        #   in Loop: Header=BB2_29 Depth=4
	cmpl	$0, 160(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_52
# BB#31:                                # %.lr.ph786
                                        #   in Loop: Header=BB2_29 Depth=4
	cmpl	$0, 112(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_52
# BB#32:                                # %.preheader695.us.preheader
                                        #   in Loop: Header=BB2_29 Depth=4
	imull	%edi, %ebp
	movslq	%ebp, %rax
	addq	24(%rsp), %rax          # 8-byte Folded Reload
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	%r8d, %ecx
	imull	%esi, %ecx
	movq	184(%rsp), %rdi         # 8-byte Reload
	movl	20(%rdi), %eax
	imull	%eax, %ecx
	movl	%ecx, 156(%rsp)         # 4-byte Spill
	movl	%r10d, %ecx
	imull	%ebx, %ecx
	imull	%eax, %ecx
	movl	%ecx, 152(%rsp)         # 4-byte Spill
	movl	%edx, %ecx
	imull	%r14d, %ecx
	imull	%eax, %ecx
	movl	%ecx, 148(%rsp)         # 4-byte Spill
	movl	%r11d, 136(%rsp)        # 4-byte Spill
	movl	16(%rdi), %r11d
	movl	%r11d, %ecx
	imull	%edx, %ecx
	movl	%r8d, %r12d
	imull	%r11d, %r12d
	imull	%r10d, %r11d
	movslq	12(%rdi), %rdi
	movq	%rdi, 88(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	negl	%edi
	imull	16(%rsp), %edi          # 4-byte Folded Reload
	leal	(%rdi,%rcx), %r15d
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leal	(%rdi,%r11), %ebp
	addl	%r12d, %edi
	movl	%r12d, 72(%rsp)         # 4-byte Spill
	movq	112(%rsp), %r13         # 8-byte Reload
	leal	-1(%r13), %eax
	imull	%eax, %r15d
	imull	%eax, %ebp
	imull	%eax, %edi
	addl	%ecx, %r15d
	movq	88(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	16(%rsp), %eax          # 4-byte Folded Reload
	subl	%eax, %r15d
	movl	%r15d, 288(%rsp)        # 4-byte Spill
	addl	%r11d, %ebp
	subl	%eax, %ebp
	movl	%ebp, 284(%rsp)         # 4-byte Spill
	addl	%r12d, %edi
	subl	%eax, %edi
	movq	%rdi, 440(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rdi         # 8-byte Reload
	movl	%r9d, %eax
	movl	(%rdi), %r9d
	movl	%r9d, %r15d
	subl	%eax, %r15d
	movl	4(%rdi), %r12d
	movl	%r12d, %ebp
	subl	136(%rsp), %ebp         # 4-byte Folded Reload
	movl	8(%rdi), %ecx
	movl	%ecx, %eax
	movq	352(%rsp), %rdi         # 8-byte Reload
	subl	4(%rdi), %eax
	imull	%esi, %eax
	addl	%ebp, %eax
	imull	%r8d, %eax
	addl	%r15d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%ecx, %esi
	movq	200(%rsp), %rax         # 8-byte Reload
	subl	-8(%rax), %esi
	imull	%ebx, %esi
	movl	%r12d, %eax
	subl	120(%rsp), %eax         # 4-byte Folded Reload
	addl	%eax, %esi
	imull	%r10d, %esi
	movl	%r9d, %eax
	subl	124(%rsp), %eax         # 4-byte Folded Reload
	addl	%eax, %esi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	subl	32(%rsp), %r9d          # 4-byte Folded Reload
	subl	40(%rsp), %r12d         # 4-byte Folded Reload
	movq	360(%rsp), %rax         # 8-byte Reload
	subl	4(%rax), %ecx
	imull	%r14d, %ecx
	addl	%r12d, %ecx
	imull	%edx, %ecx
	addl	%r9d, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	104(%rsp), %rsi         # 8-byte Reload
	leaq	(%rdx,%rsi,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %ecx
	addq	%rcx, %rsi
	leaq	8(%rdx,%rsi,8), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	leaq	(%rdx,%rsi,8), %rax
	movq	80(%rsp), %rdi          # 8-byte Reload
	leaq	(%rax,%rdi,8), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	addq	%rsi, %rdi
	leaq	(%rdx,%rdi,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	addq	%rcx, %rdi
	leaq	8(%rdx,%rdi,8), %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movl	%r13d, %eax
	imull	24(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 280(%rsp)         # 4-byte Spill
	subl	%eax, 148(%rsp)         # 4-byte Folded Spill
	movl	%r13d, %eax
	movq	%r11, 48(%rsp)          # 8-byte Spill
	imull	%r11d, %eax
	movl	%eax, 276(%rsp)         # 4-byte Spill
	subl	%eax, 152(%rsp)         # 4-byte Folded Spill
	movl	%r13d, %eax
	imull	72(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 272(%rsp)         # 4-byte Spill
	subl	%eax, 156(%rsp)         # 4-byte Folded Spill
	leaq	1(%rcx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rax, %rdx
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %rdx
	setne	%al
	movq	88(%rsp), %rdi          # 8-byte Reload
	cmpl	$1, %edi
	sete	%bl
	andb	%al, %bl
	movb	%bl, 40(%rsp)           # 1-byte Spill
	movq	%rdi, %rax
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	imulq	%rdx, %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movq	%rdi, %rsi
	shlq	$5, %rsi
	movq	%rdi, %r8
	shlq	$4, %r8
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	leaq	8(%rax,%rcx,8), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leaq	(,%rdi,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	movq	%rsi, 208(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_33:                               # %.preheader695.us
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_25 Depth=2
                                        #       Parent Loop BB2_27 Depth=3
                                        #         Parent Loop BB2_29 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_35 Depth 6
                                        #               Child Loop BB2_47 Depth 7
                                        #               Child Loop BB2_42 Depth 7
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movq	440(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	284(%rsp), %ecx         # 4-byte Reload
	movl	288(%rsp), %edx         # 4-byte Reload
	jle	.LBB2_51
# BB#34:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB2_33 Depth=5
	movl	%edi, 292(%rsp)         # 4-byte Spill
	xorl	%r14d, %r14d
	movl	12(%rsp), %r10d         # 4-byte Reload
	movl	4(%rsp), %r9d           # 4-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	.p2align	4, 0x90
.LBB2_35:                               # %.preheader.us.us
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_25 Depth=2
                                        #       Parent Loop BB2_27 Depth=3
                                        #         Parent Loop BB2_29 Depth=4
                                        #           Parent Loop BB2_33 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB2_47 Depth 7
                                        #               Child Loop BB2_42 Depth 7
	movslq	%r13d, %rbx
	movslq	%r9d, %r12
	movslq	%r10d, %r11
	cmpq	$3, 64(%rsp)            # 8-byte Folded Reload
	jbe	.LBB2_36
# BB#43:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_35 Depth=6
	cmpb	$0, 40(%rsp)            # 1-byte Folded Reload
	je	.LBB2_36
# BB#44:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_35 Depth=6
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r14d, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	movslq	%eax, %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r14d, %eax
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	movslq	%eax, %r15
	movl	72(%rsp), %eax          # 4-byte Reload
	imull	%r14d, %eax
	addl	12(%rsp), %eax          # 4-byte Folded Reload
	movslq	%eax, %rdx
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rax
	movq	224(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rbp
	movq	240(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rsi
	cmpq	%rsi, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rdi, 248(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rdi,8), %rsi
	sbbb	%dil, %dil
	cmpq	%rbp, %rsi
	sbbb	%cl, %cl
	andb	%dil, %cl
	movq	232(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r15,8), %rsi
	cmpq	%rsi, %rax
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r15,8), %rsi
	sbbb	%al, %al
	cmpq	%rbp, %rsi
	sbbb	%sil, %sil
	testb	$1, %cl
	jne	.LBB2_36
# BB#45:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_35 Depth=6
	andb	%sil, %al
	andb	$1, %al
	jne	.LBB2_36
# BB#46:                                # %vector.body.preheader
                                        #   in Loop: Header=BB2_35 Depth=6
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r12,8), %rsi
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r11,8), %rax
	movq	216(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, %rdx
	addq	%rcx, %r15
	movq	248(%rsp), %r12         # 8-byte Reload
	addq	%rcx, %r12
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	$16, %ebp
	movq	208(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_47:                               # %vector.body
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_25 Depth=2
                                        #       Parent Loop BB2_27 Depth=3
                                        #         Parent Loop BB2_29 Depth=4
                                        #           Parent Loop BB2_33 Depth=5
                                        #             Parent Loop BB2_35 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movupd	-16(%rbx,%rbp), %xmm0
	movupd	(%rbx,%rbp), %xmm1
	movupd	-16(%rsi,%rbp), %xmm2
	movupd	(%rsi,%rbp), %xmm3
	mulpd	%xmm0, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	-16(%rax,%rbp), %xmm0
	movupd	(%rax,%rbp), %xmm1
	subpd	%xmm2, %xmm0
	subpd	%xmm3, %xmm1
	movupd	%xmm0, -16(%rax,%rbp)
	movupd	%xmm1, (%rax,%rbp)
	addq	%rdi, %rbp
	addq	$-4, %rcx
	jne	.LBB2_47
# BB#48:                                # %middle.block
                                        #   in Loop: Header=BB2_35 Depth=6
	movq	176(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 64(%rsp)          # 8-byte Folded Reload
	movl	%eax, %ebx
	jne	.LBB2_37
	jmp	.LBB2_49
	.p2align	4, 0x90
.LBB2_36:                               #   in Loop: Header=BB2_35 Depth=6
	movq	%r11, %rdx
	movq	%r12, %r15
	movq	%rbx, %r12
	xorl	%ebx, %ebx
.LBB2_37:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_35 Depth=6
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%ebx, %eax
	testb	$1, %al
	jne	.LBB2_39
# BB#38:                                #   in Loop: Header=BB2_35 Depth=6
	movl	%ebx, %edi
	cmpl	%ebx, 104(%rsp)         # 4-byte Folded Reload
	jne	.LBB2_41
	jmp	.LBB2_49
	.p2align	4, 0x90
.LBB2_39:                               # %scalar.ph.prol
                                        #   in Loop: Header=BB2_35 Depth=6
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%r12,8), %xmm0    # xmm0 = mem[0],zero
	movq	136(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%r15,8), %xmm0
	movq	56(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax,%rdx,8)
	movq	88(%rsp), %rax          # 8-byte Reload
	addq	%rax, %r12
	addq	%rax, %r15
	addq	%rax, %rdx
	leal	1(%rbx), %edi
	cmpl	%ebx, 104(%rsp)         # 4-byte Folded Reload
	je	.LBB2_49
.LBB2_41:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB2_35 Depth=6
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rax
	movq	80(%rsp), %rbp          # 8-byte Reload
	leaq	(%rax,%rbp), %rdx
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r15,8), %rsi
	leaq	(%rsi,%rbp), %r11
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r12,8), %rbx
	leaq	(%rbx,%rbp), %rbp
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%edi, %ecx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_42:                               # %scalar.ph
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_25 Depth=2
                                        #       Parent Loop BB2_27 Depth=3
                                        #         Parent Loop BB2_29 Depth=4
                                        #           Parent Loop BB2_33 Depth=5
                                        #             Parent Loop BB2_35 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movsd	(%rbx,%rdi), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%rsi,%rdi), %xmm0
	movsd	(%rax,%rdi), %xmm1      # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax,%rdi)
	movsd	(%rbp,%rdi), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%r11,%rdi), %xmm0
	movsd	(%rdx,%rdi), %xmm1      # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx,%rdi)
	addq	%r8, %rdi
	addl	$-2, %ecx
	jne	.LBB2_42
.LBB2_49:                               # %._crit_edge745.us.us
                                        #   in Loop: Header=BB2_35 Depth=6
	addl	24(%rsp), %r13d         # 4-byte Folded Reload
	addl	48(%rsp), %r9d          # 4-byte Folded Reload
	addl	72(%rsp), %r10d         # 4-byte Folded Reload
	incl	%r14d
	cmpl	112(%rsp), %r14d        # 4-byte Folded Reload
	jne	.LBB2_35
# BB#50:                                #   in Loop: Header=BB2_33 Depth=5
	movl	272(%rsp), %eax         # 4-byte Reload
	movl	276(%rsp), %ecx         # 4-byte Reload
	movl	280(%rsp), %edx         # 4-byte Reload
	movl	292(%rsp), %edi         # 4-byte Reload
.LBB2_51:                               # %._crit_edge753.us
                                        #   in Loop: Header=BB2_33 Depth=5
	addl	8(%rsp), %edx           # 4-byte Folded Reload
	addl	4(%rsp), %ecx           # 4-byte Folded Reload
	addl	12(%rsp), %eax          # 4-byte Folded Reload
	addl	148(%rsp), %edx         # 4-byte Folded Reload
	addl	152(%rsp), %ecx         # 4-byte Folded Reload
	addl	156(%rsp), %eax         # 4-byte Folded Reload
	incl	%edi
	cmpl	160(%rsp), %edi         # 4-byte Folded Reload
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jne	.LBB2_33
.LBB2_52:                               # %._crit_edge787
                                        #   in Loop: Header=BB2_29 Depth=4
	movq	432(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	cmpq	344(%rsp), %rcx         # 8-byte Folded Reload
	movq	424(%rsp), %rdi         # 8-byte Reload
	movl	268(%rsp), %edx         # 4-byte Reload
	movl	264(%rsp), %esi         # 4-byte Reload
	jne	.LBB2_29
# BB#53:                                # %._crit_edge793.loopexit
                                        #   in Loop: Header=BB2_27 Depth=3
	movq	384(%rsp), %rbp         # 8-byte Reload
	movl	8(%rbp), %eax
	movq	392(%rsp), %rdx         # 8-byte Reload
.LBB2_54:                               # %._crit_edge793
                                        #   in Loop: Header=BB2_27 Depth=3
	incq	%rdx
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	jl	.LBB2_27
# BB#55:                                # %._crit_edge801.loopexit
                                        #   in Loop: Header=BB2_25 Depth=2
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	8(%rax), %ecx
.LBB2_56:                               # %._crit_edge801
                                        #   in Loop: Header=BB2_25 Depth=2
	incq	%rdi
	movslq	%ecx, %rax
	cmpq	%rax, %rdi
	jl	.LBB2_25
.LBB2_57:                               # %._crit_edge809
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	260(%rsp), %eax         # 4-byte Reload
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB2_1
# BB#58:
	movq	184(%rsp), %rbx         # 8-byte Reload
	movl	76(%rbx), %edi
	callq	hypre_IncFLOPCount
	movl	72(%rbx), %edi
	callq	hypre_EndTiming
	xorl	%eax, %eax
	addq	$456, %rsp              # imm = 0x1C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_SMGResidual, .Lfunc_end2-hypre_SMGResidual
	.cfi_endproc

	.globl	hypre_SMGResidualSetBase
	.p2align	4, 0x90
	.type	hypre_SMGResidualSetBase,@function
hypre_SMGResidualSetBase:               # @hypre_SMGResidualSetBase
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movl	(%rdx), %eax
	movl	%eax, 12(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	4(%rdx), %eax
	movl	%eax, 16(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	movl	8(%rdx), %eax
	movl	%eax, 20(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	hypre_SMGResidualSetBase, .Lfunc_end3-hypre_SMGResidualSetBase
	.cfi_endproc

	.globl	hypre_SMGResidualDestroy
	.p2align	4, 0x90
	.type	hypre_SMGResidualDestroy,@function
hypre_SMGResidualDestroy:               # @hypre_SMGResidualDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
.Lcfi41:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB4_2
# BB#1:
	movq	24(%rbx), %rdi
	callq	hypre_StructMatrixDestroy
	movq	32(%rbx), %rdi
	callq	hypre_StructVectorDestroy
	movq	40(%rbx), %rdi
	callq	hypre_StructVectorDestroy
	movq	48(%rbx), %rdi
	callq	hypre_StructVectorDestroy
	movq	56(%rbx), %rdi
	callq	hypre_BoxArrayDestroy
	movq	64(%rbx), %rdi
	callq	hypre_ComputePkgDestroy
	movl	72(%rbx), %edi
	callq	hypre_FinalizeTiming
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB4_2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	hypre_SMGResidualDestroy, .Lfunc_end4-hypre_SMGResidualDestroy
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SMGResidual"
	.size	.L.str, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
