	.text
	.file	"btGjkEpa2.bc"
	.globl	_ZN15btGjkEpaSolver220StackSizeRequirementEv
	.p2align	4, 0x90
	.type	_ZN15btGjkEpaSolver220StackSizeRequirementEv,@function
_ZN15btGjkEpaSolver220StackSizeRequirementEv: # @_ZN15btGjkEpaSolver220StackSizeRequirementEv
	.cfi_startproc
# BB#0:
	movl	$14928, %eax            # imm = 0x3A50
	retq
.Lfunc_end0:
	.size	_ZN15btGjkEpaSolver220StackSizeRequirementEv, .Lfunc_end0-_ZN15btGjkEpaSolver220StackSizeRequirementEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI1_3:
	.zero	16
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_1:
	.long	1065353216              # float 1
.LCPI1_2:
	.long	953267991               # float 9.99999974E-5
	.text
	.globl	_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE
	.p2align	4, 0x90
	.type	_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE,@function
_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE: # @_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$784, %rsp              # imm = 0x310
.Lcfi5:
	.cfi_def_cfa_offset 832
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%r9, %r14
	movq	%rsi, %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
	movl	$0, 32(%r14)
	movq	%rdi, 112(%rsp)
	movq	%rdx, 120(%rsp)
	movss	(%r13), %xmm15          # xmm15 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	%xmm9, 44(%rsp)         # 4-byte Spill
	movaps	%xmm1, %xmm0
	mulss	%xmm15, %xmm0
	movss	16(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	16(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	movss	32(%rcx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	32(%r13), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm10, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movaps	%xmm1, %xmm0
	mulss	%xmm3, %xmm0
	movss	20(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm3, %xmm7
	addss	%xmm0, %xmm7
	movss	36(%r13), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm13
	mulss	%xmm8, %xmm13
	addss	%xmm7, %xmm13
	movss	8(%r13), %xmm12         # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm1
	movss	24(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	addss	%xmm1, %xmm6
	movss	40(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm11
	addss	%xmm6, %xmm11
	movaps	%xmm15, %xmm6
	mulss	%xmm9, %xmm6
	movss	20(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	mulss	%xmm5, %xmm7
	movss	%xmm5, 48(%rsp)         # 4-byte Spill
	addss	%xmm6, %xmm7
	movss	36(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm14
	mulss	%xmm0, %xmm14
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	addss	%xmm7, %xmm14
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm7
	movaps	%xmm3, %xmm6
	mulss	%xmm5, %xmm6
	addss	%xmm7, %xmm6
	movaps	%xmm8, %xmm7
	mulss	%xmm0, %xmm7
	addss	%xmm6, %xmm7
	movaps	%xmm12, %xmm6
	mulss	%xmm9, %xmm6
	movaps	%xmm2, %xmm9
	mulss	%xmm5, %xmm9
	addss	%xmm6, %xmm9
	movaps	%xmm1, %xmm6
	mulss	%xmm0, %xmm6
	addss	%xmm9, %xmm6
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm15
	movaps	%xmm0, %xmm5
	movss	24(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm15, %xmm4
	movss	40(%rcx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm10
	addss	%xmm4, %xmm10
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm4
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm9, %xmm8
	addss	%xmm3, %xmm8
	mulss	%xmm5, %xmm12
	movaps	%xmm5, %xmm15
	movss	%xmm15, 80(%rsp)        # 4-byte Spill
	mulss	%xmm0, %xmm2
	movaps	%xmm0, %xmm4
	movss	%xmm4, 64(%rsp)         # 4-byte Spill
	addss	%xmm12, %xmm2
	mulss	%xmm9, %xmm1
	movaps	%xmm9, %xmm3
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	addss	%xmm2, %xmm1
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 128(%rsp)
	movss	%xmm13, 132(%rsp)
	movss	%xmm11, 136(%rsp)
	movl	$0, 140(%rsp)
	movss	%xmm14, 144(%rsp)
	movss	%xmm7, 148(%rsp)
	movss	%xmm6, 152(%rsp)
	movl	$0, 156(%rsp)
	movss	%xmm10, 160(%rsp)
	movss	%xmm8, 164(%rsp)
	movss	%xmm1, 168(%rsp)
	movl	$0, 172(%rsp)
	movsd	(%r13), %xmm7           # xmm7 = mem[0],zero
	movss	(%rcx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	movaps	%xmm7, %xmm0
	mulss	%xmm8, %xmm0
	movsd	16(%r13), %xmm2         # xmm2 = mem[0],zero
	movaps	%xmm2, %xmm5
	mulss	%xmm9, %xmm5
	addss	%xmm0, %xmm5
	movss	32(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 16(%rsp)         # 4-byte Spill
	movsd	32(%r13), %xmm11        # xmm11 = mem[0],zero
	movaps	%xmm11, %xmm0
	mulss	%xmm6, %xmm0
	addss	%xmm5, %xmm0
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movaps	%xmm2, %xmm5
	movss	44(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	mulss	48(%rsp), %xmm5         # 4-byte Folded Reload
	addss	%xmm1, %xmm5
	movaps	%xmm11, %xmm1
	movss	40(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm1
	addss	%xmm5, %xmm1
	movaps	%xmm1, 304(%rsp)        # 16-byte Spill
	movaps	%xmm7, %xmm1
	mulss	%xmm15, %xmm1
	movaps	%xmm2, %xmm5
	mulss	%xmm4, %xmm5
	addss	%xmm1, %xmm5
	movaps	%xmm11, %xmm1
	mulss	%xmm3, %xmm1
	addss	%xmm5, %xmm1
	movaps	%xmm1, 288(%rsp)        # 16-byte Spill
	movss	48(%rcx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	subss	48(%r13), %xmm14
	movaps	%xmm14, 272(%rsp)       # 16-byte Spill
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	mulps	%xmm7, %xmm14
	pshufd	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	movss	52(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	52(%r13), %xmm5
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	pshufd	$229, %xmm2, %xmm6      # xmm6 = xmm2[1,1,2,3]
	movaps	%xmm8, %xmm15
	movaps	%xmm15, %xmm2
	mulss	%xmm7, %xmm2
	movaps	%xmm9, %xmm13
	mulss	%xmm6, %xmm13
	addss	%xmm2, %xmm13
	movss	56(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	56(%r13), %xmm2
	movaps	%xmm2, %xmm8
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm11, %xmm8
	pshufd	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm12
	addss	%xmm13, %xmm12
	movaps	%xmm0, %xmm3
	movaps	%xmm3, %xmm13
	mulss	%xmm7, %xmm13
	movss	48(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	mulss	%xmm6, %xmm0
	addss	%xmm13, %xmm0
	movaps	%xmm10, %xmm13
	mulss	%xmm11, %xmm13
	addss	%xmm0, %xmm13
	mulss	80(%rsp), %xmm7         # 4-byte Folded Reload
	mulss	64(%rsp), %xmm6         # 4-byte Folded Reload
	addss	%xmm7, %xmm6
	mulss	12(%rsp), %xmm11        # 4-byte Folded Reload
	addss	%xmm6, %xmm11
	movss	8(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm7
	mulss	%xmm0, %xmm7
	movss	24(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm15
	mulss	%xmm6, %xmm15
	addss	%xmm7, %xmm15
	movss	40(%r13), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm7
	addss	%xmm15, %xmm7
	movaps	%xmm7, %xmm15
	mulss	%xmm0, %xmm3
	mulss	%xmm6, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm9, %xmm10
	addss	%xmm1, %xmm10
	movss	80(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	64(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm7
	addss	%xmm1, %xmm7
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	addss	%xmm7, %xmm1
	addps	%xmm14, %xmm4
	addps	%xmm4, %xmm8
	movaps	272(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm0, %xmm4
	mulss	%xmm6, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm9, %xmm2
	addss	%xmm5, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movaps	96(%rsp), %xmm2         # 16-byte Reload
	movss	%xmm2, 176(%rsp)
	movaps	304(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 180(%rsp)
	movaps	288(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 184(%rsp)
	movl	$0, 188(%rsp)
	movss	%xmm12, 192(%rsp)
	movss	%xmm13, 196(%rsp)
	movss	%xmm11, 200(%rsp)
	movl	$0, 204(%rsp)
	movss	%xmm15, 208(%rsp)
	movss	%xmm10, 212(%rsp)
	movss	%xmm1, 216(%rsp)
	movl	$0, 220(%rsp)
	movlps	%xmm8, 224(%rsp)
	movlps	%xmm0, 232(%rsp)
	movl	$_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3, %eax
	movd	%rax, %xmm0
	movdqu	%xmm0, 240(%rsp)
	movl	$0, 760(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 464(%rsp)
	movl	$2, 776(%rsp)
	movl	$0, 764(%rsp)
	movl	$0, 480(%rsp)
	leaq	320(%rsp), %rdi
	leaq	112(%rsp), %rsi
	movq	%r8, %rdx
	callq	_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3
	testl	%eax, %eax
	je	.LBB1_1
# BB#12:
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	setne	%cl
	incl	%ecx
	movl	%ecx, (%r14)
	xorl	%eax, %eax
	jmp	.LBB1_13
.LBB1_1:                                # %.preheader
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	768(%rsp), %rax
	cmpl	$0, 48(%rax)
	je	.LBB1_2
# BB#8:                                 # %.lr.ph
	xorps	%xmm1, %xmm1
	xorl	%ecx, %ecx
	leaq	112(%rsp), %r15
	leaq	256(%rsp), %r12
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 16(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebx
	movss	32(%rax,%rbx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movq	(%rax,%rbx,8), %rsi
	movq	112(%rsp), %rdi
	movq	240(%rsp), %rax
	addq	248(%rsp), %rdi
	testb	$1, %al
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	je	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_9 Depth=1
	movq	(%rdi), %rcx
	movq	-1(%rcx,%rax), %rax
.LBB1_11:                               # %_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j.exit
                                        #   in Loop: Header=BB1_9 Depth=1
	callq	*%rax
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	shufps	$16, %xmm0, %xmm1       # xmm1 = xmm1[0,0],xmm0[1,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	mulps	%xmm3, %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	addps	%xmm1, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	768(%rsp), %rax
	movq	(%rax,%rbx,8), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movaps	.LCPI1_0(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 256(%rsp)
	movlps	%xmm2, 264(%rsp)
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j
	movaps	48(%rsp), %xmm2         # 16-byte Reload
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	shufps	$32, %xmm0, %xmm1       # xmm1 = xmm1[0,0],xmm0[2,0]
	shufps	$36, %xmm1, %xmm0       # xmm0 = xmm0[0,1],xmm1[2,0]
	mulps	%xmm2, %xmm0
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	addps	%xmm0, %xmm1
	leal	1(%rbx), %ecx
	movq	768(%rsp), %rax
	cmpl	48(%rax), %ecx
	jb	.LBB1_9
	jmp	.LBB1_3
.LBB1_2:
	xorps	%xmm1, %xmm1
.LBB1_3:                                # %._crit_edge
	movaps	%xmm1, %xmm9
	shufps	$231, %xmm9, %xmm9      # xmm9 = xmm9[3,1,2,3]
	movaps	16(%rsp), %xmm10        # 16-byte Reload
	movaps	%xmm10, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm1, %xmm2
	shufps	$255, %xmm2, %xmm2      # xmm2 = xmm2[3,3,3,3]
	movss	16(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r13), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm2, %xmm4
	movaps	%xmm10, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	20(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulps	%xmm2, %xmm5
	addps	%xmm4, %xmm5
	movss	24(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	mulps	%xmm0, %xmm3
	addps	%xmm5, %xmm3
	movsd	48(%r13), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	movss	32(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	36(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm2, %xmm3
	movss	40(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm3, %xmm5
	addss	56(%r13), %xmm5
	xorps	%xmm8, %xmm8
	xorps	%xmm3, %xmm3
	movss	%xmm5, %xmm3            # xmm3 = xmm5[0],xmm3[1,2,3]
	movlps	%xmm4, 4(%r14)
	movlps	%xmm3, 12(%r14)
	movaps	%xmm1, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm1, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	16(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%r13), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	mulps	%xmm5, %xmm7
	movss	20(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	mulps	%xmm3, %xmm2
	addps	%xmm7, %xmm2
	movaps	%xmm1, %xmm5
	shufps	$170, %xmm5, %xmm5      # xmm5 = xmm5[2,2,2,2]
	movss	24(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	mulps	%xmm5, %xmm7
	addps	%xmm2, %xmm7
	movsd	48(%r13), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm7, %xmm2
	movss	32(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	movss	36(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm5, %xmm6
	movss	40(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	addss	%xmm6, %xmm5
	addss	56(%r13), %xmm5
	xorps	%xmm6, %xmm6
	movss	%xmm5, %xmm6            # xmm6 = xmm5[0],xmm6[1,2,3]
	movlps	%xmm2, 20(%r14)
	movlps	%xmm6, 28(%r14)
	subss	%xmm1, %xmm9
	subss	%xmm3, %xmm10
	subss	%xmm4, %xmm0
	movaps	%xmm9, %xmm2
	unpcklps	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1]
	movss	%xmm0, %xmm8            # xmm8 = xmm0[0],xmm8[1,2,3]
	movlps	%xmm2, 36(%r14)
	movlps	%xmm8, 44(%r14)
	mulss	%xmm9, %xmm9
	mulss	%xmm10, %xmm10
	addss	%xmm9, %xmm10
	mulss	%xmm0, %xmm0
	addss	%xmm10, %xmm0
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_5
# BB#4:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB1_5:                                # %._crit_edge.split
	movss	%xmm1, 52(%r14)
	movss	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI1_2(%rip), %xmm1
	jbe	.LBB1_7
# BB#6:                                 # %select.true.sink
	divss	%xmm1, %xmm0
.LBB1_7:                                # %select.end
	movss	36(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 36(%r14)
	movss	40(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 40(%r14)
	mulss	44(%r14), %xmm0
	movss	%xmm0, 44(%r14)
	movb	$1, %al
.LBB1_13:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$784, %rsp              # imm = 0x310
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE, .Lfunc_end1-_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI2_5:
	.zero	16
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_1:
	.long	953267991               # float 9.99999974E-5
.LCPI2_2:
	.long	3100751639              # float -9.99999974E-5
.LCPI2_3:
	.long	1065353216              # float 1
.LCPI2_4:
	.long	0                       # float 0
	.section	.text._ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3,"axG",@progbits,_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3,comdat
	.weak	_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3
	.p2align	4, 0x90
	.type	_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3,@function
_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3: # @_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 208
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdx, %rax
	movq	%rdi, %r14
	leaq	280(%r14), %rcx
	movq	%rcx, 408(%r14)
	leaq	312(%r14), %rcx
	movq	%rcx, 416(%r14)
	leaq	344(%r14), %rcx
	movq	%rcx, 424(%r14)
	leaq	376(%r14), %rdx
	movq	%rdx, 432(%r14)
	movq	$4, 440(%r14)
	movl	$0, 456(%r14)
	movups	(%rsi), %xmm0
	movups	%xmm0, (%r14)
	movups	16(%rsi), %xmm0
	movups	%xmm0, 16(%r14)
	movups	32(%rsi), %xmm0
	movups	%xmm0, 32(%r14)
	movups	48(%rsi), %xmm0
	movups	%xmm0, 48(%r14)
	movups	64(%rsi), %xmm0
	movups	%xmm0, 64(%r14)
	movups	80(%rsi), %xmm0
	movups	%xmm0, 80(%r14)
	movups	96(%rsi), %xmm0
	movups	%xmm0, 96(%r14)
	movups	112(%rsi), %xmm0
	movups	%xmm0, 112(%r14)
	movups	128(%rsi), %xmm0
	movups	%xmm0, 128(%r14)
	movl	$0, 160(%r14)
	movl	$0, 216(%r14)
	leaq	144(%r14), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movups	(%rax), %xmm0
	movups	%xmm0, 144(%r14)
	movq	144(%r14), %xmm0        # xmm0 = mem[0],zero
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	movdqa	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	152(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm3
	movaps	%xmm3, 128(%rsp)        # 16-byte Spill
	jbe	.LBB2_2
# BB#1:                                 # %.critedge98.critedge
	movdqa	.LCPI2_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm2, %xmm0
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movq	%xmm0, 48(%rsp)
	movlps	%xmm2, 56(%rsp)
	jmp	.LBB2_3
.LBB2_2:                                # %.critedge
	movl	$1065353216, 48(%rsp)   # imm = 0x3F800000
	movl	$0, 52(%rsp)
	movl	$0, 56(%rsp)
	movl	$0, 60(%rsp)
.LBB2_3:                                # %.critedge98
	movl	$0, 200(%r14)
	movl	$3, 440(%r14)
	movq	%rdx, 168(%r14)
	movl	$1, 216(%r14)
	leaq	48(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	movl	$1065353216, 200(%r14)  # imm = 0x3F800000
	leaq	168(%r14), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	168(%r14), %rax
	movups	16(%rax), %xmm0
	movq	112(%rsp), %rcx         # 8-byte Reload
	movups	%xmm0, (%rcx)
	movups	16(%rax), %xmm0
	movaps	%xmm0, 96(%rsp)
	movups	16(%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	16(%rax), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	16(%rax), %xmm0
	movaps	%xmm0, 48(%rsp)
	movss	144(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	148(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	152(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorl	%r12d, %r12d
	movss	.LCPI2_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	jmp	.LBB2_4
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_4 Depth=1
	movss	%xmm1, 44(%rsp)         # 4-byte Spill
	imulq	$56, %rbx, %r13
	movsd	144(%r14), %xmm0        # xmm0 = mem[0],zero
	movaps	.LCPI2_0(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	152(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	leaq	(%r14,%r13), %rbp
	movl	216(%r14,%r13), %eax
	movl	$0, 200(%rbp,%rax,4)
	movl	440(%r14), %eax
	decl	%eax
	movl	%eax, 440(%r14)
	movq	408(%r14,%rax,8), %rax
	movl	216(%r14,%r13), %ecx
	movq	%rax, 168(%rbp,%rcx,8)
	movl	216(%r14,%r13), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 216(%r14,%r13)
	movq	168(%rbp,%rax,8), %rdx
	movq	%r14, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	movss	.LCPI2_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movl	216(%r14,%r13), %eax
	leal	-1(%rax), %ecx
	movq	168(%rbp,%rcx,8), %rcx
	movss	16(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	subss	48(%rsp), %xmm2
	movaps	%xmm0, %xmm3
	subss	52(%rsp), %xmm3
	movaps	%xmm6, %xmm4
	subss	56(%rsp), %xmm4
	mulss	%xmm2, %xmm2
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	%xmm4, %xmm7
	ja	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_4 Depth=1
	movaps	%xmm1, %xmm2
	subss	64(%rsp), %xmm2
	movaps	%xmm0, %xmm3
	subss	68(%rsp), %xmm3
	movaps	%xmm6, %xmm4
	subss	72(%rsp), %xmm4
	mulss	%xmm2, %xmm2
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	%xmm4, %xmm7
	ja	.LBB2_10
# BB#44:                                #   in Loop: Header=BB2_4 Depth=1
	movaps	%xmm1, %xmm2
	subss	80(%rsp), %xmm2
	movaps	%xmm0, %xmm3
	subss	84(%rsp), %xmm3
	movaps	%xmm6, %xmm4
	subss	88(%rsp), %xmm4
	mulss	%xmm2, %xmm2
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	%xmm4, %xmm7
	ja	.LBB2_10
# BB#45:                                #   in Loop: Header=BB2_4 Depth=1
	movaps	%xmm1, %xmm2
	subss	96(%rsp), %xmm2
	movaps	%xmm0, %xmm3
	subss	100(%rsp), %xmm3
	movaps	%xmm6, %xmm4
	subss	104(%rsp), %xmm4
	mulss	%xmm2, %xmm2
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	%xmm4, %xmm7
	ja	.LBB2_10
# BB#46:                                # %.critedge100160
                                        #   in Loop: Header=BB2_4 Depth=1
	addq	$16, %rcx
	incl	%r12d
	andl	$3, %r12d
	movq	%r12, %rdx
	shlq	$4, %rdx
	movups	(%rcx), %xmm2
	movups	%xmm2, 48(%rsp,%rdx)
	mulss	144(%r14), %xmm1
	mulss	148(%r14), %xmm0
	addss	%xmm1, %xmm0
	mulss	152(%r14), %xmm6
	addss	%xmm0, %xmm6
	movss	44(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm6
	maxss	8(%rsp), %xmm6          # 4-byte Folded Reload
	movaps	%xmm1, %xmm0
	subss	%xmm6, %xmm0
	mulss	.LCPI2_2(%rip), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	ucomiss	%xmm1, %xmm0
	jae	.LBB2_10
# BB#11:                                #   in Loop: Header=BB2_4 Depth=1
	leaq	168(%r14,%r13), %rcx
	movl	$0, 12(%rsp)
	cmpl	$4, %eax
	je	.LBB2_21
# BB#12:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	$3, %eax
	je	.LBB2_20
# BB#13:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	$2, %eax
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	jne	.LBB2_23
# BB#14:                                #   in Loop: Header=BB2_4 Depth=1
	movq	(%rcx), %rax
	movq	176(%r14,%r13), %rcx
	movss	16(%rcx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	20(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm4
	subss	%xmm8, %xmm4
	movaps	%xmm13, %xmm3
	subss	%xmm9, %xmm3
	movss	24(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rax), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm12
	subss	%xmm10, %xmm0
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm3, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm5
	mulss	%xmm5, %xmm5
	addss	%xmm2, %xmm5
	ucomiss	.LCPI2_4, %xmm5
	jbe	.LBB2_10
# BB#15:                                #   in Loop: Header=BB2_4 Depth=1
	movaps	%xmm8, %xmm1
	mulss	%xmm4, %xmm1
	movaps	%xmm9, %xmm2
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm10, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	xorps	.LCPI2_0(%rip), %xmm1
	divss	%xmm5, %xmm1
	ucomiss	.LCPI2_3(%rip), %xmm1
	jae	.LBB2_16
# BB#17:                                #   in Loop: Header=BB2_4 Depth=1
	xorps	%xmm2, %xmm2
	ucomiss	%xmm1, %xmm2
	jae	.LBB2_18
# BB#19:                                #   in Loop: Header=BB2_4 Depth=1
	movss	%xmm1, 20(%rsp)
	movss	.LCPI2_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 16(%rsp)
	movl	$3, 12(%rsp)
	mulss	%xmm1, %xmm4
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm0
	addss	%xmm8, %xmm4
	addss	%xmm9, %xmm3
	addss	%xmm10, %xmm0
	mulss	%xmm4, %xmm4
	mulss	%xmm3, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	jmp	.LBB2_23
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_4 Depth=1
	movq	(%rcx), %rdi
	addq	$16, %rdi
	movq	176(%r14,%r13), %rsi
	movq	184(%r14,%r13), %rdx
	addq	$16, %rsi
	addq	$16, %rdx
	movq	192(%r14,%r13), %rcx
	addq	$16, %rcx
	leaq	16(%rsp), %r8
	leaq	12(%rsp), %r9
	movss	%xmm6, 8(%rsp)          # 4-byte Spill
	callq	_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj
	jmp	.LBB2_22
	.p2align	4, 0x90
.LBB2_20:                               #   in Loop: Header=BB2_4 Depth=1
	movq	(%rcx), %rdi
	addq	$16, %rdi
	movq	176(%r14,%r13), %rsi
	movq	184(%r14,%r13), %rdx
	addq	$16, %rsi
	addq	$16, %rdx
	leaq	16(%rsp), %rcx
	leaq	12(%rsp), %r8
	movss	%xmm6, 8(%rsp)          # 4-byte Spill
	callq	_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj
.LBB2_22:                               # %_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj.exit
                                        #   in Loop: Header=BB2_4 Depth=1
	movss	8(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	.LCPI2_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
.LBB2_23:                               # %_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj.exit
                                        #   in Loop: Header=BB2_4 Depth=1
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	ucomiss	.LCPI2_4, %xmm0
	jb	.LBB2_10
# BB#24:                                #   in Loop: Header=BB2_4 Depth=1
	movl	$1, %eax
	subl	%ebx, %eax
	leaq	216(%r14,%r13), %rdx
	imulq	$56, %rax, %r8
	movl	$0, 216(%r14,%r8)
	movq	112(%rsp), %rcx         # 8-byte Reload
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rcx)
	movl	%eax, 444(%r14)
	movl	(%rdx), %edx
	testq	%rdx, %rdx
	movl	12(%rsp), %r9d
	je	.LBB2_25
# BB#30:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	leaq	216(%r14,%r8), %rsi
	addq	120(%rsp), %r13         # 8-byte Folded Reload
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_31:                               # %.lr.ph
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	btl	%edi, %r9d
	movq	(%r13,%rdi,8), %rbp
	jae	.LBB2_33
# BB#32:                                #   in Loop: Header=BB2_31 Depth=2
	movl	(%rsi), %ecx
	leaq	(%r14,%r8), %rbx
	movq	%rbp, 168(%rbx,%rcx,8)
	movl	16(%rsp,%rdi,4), %ecx
	movl	(%rsi), %ebp
	leal	1(%rbp), %eax
	movl	%eax, (%rsi)
	movl	%ecx, 200(%rbx,%rbp,4)
	movq	(%r13,%rdi,8), %rax
	movd	%ecx, %xmm3
	movss	16(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	movss	20(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	mulss	24(%rax), %xmm3
	addss	%xmm4, %xmm2
	movss	%xmm2, 144(%r14)
	addss	%xmm5, %xmm1
	movss	%xmm1, 148(%r14)
	addss	%xmm3, %xmm0
	movss	%xmm0, 152(%r14)
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_33:                               #   in Loop: Header=BB2_31 Depth=2
	movl	440(%r14), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 440(%r14)
	movq	%rbp, 408(%r14,%rax,8)
.LBB2_34:                               #   in Loop: Header=BB2_31 Depth=2
	incq	%rdi
	cmpq	%rdi, %rdx
	jne	.LBB2_31
	jmp	.LBB2_26
	.p2align	4, 0x90
.LBB2_25:                               #   in Loop: Header=BB2_4 Depth=1
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
.LBB2_26:                               # %._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	cmpl	$15, %r9d
	jne	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_4 Depth=1
	movl	$1, 456(%r14)
.LBB2_28:                               #   in Loop: Header=BB2_4 Depth=1
	incl	%r15d
	cmpl	$128, %r15d
	jae	.LBB2_29
# BB#35:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	$0, 456(%r14)
	movss	%xmm6, 8(%rsp)          # 4-byte Spill
	je	.LBB2_4
	jmp	.LBB2_36
.LBB2_16:                               #   in Loop: Header=BB2_4 Depth=1
	movl	$0, 16(%rsp)
	movl	$1065353216, 20(%rsp)   # imm = 0x3F800000
	movl	$2, 12(%rsp)
	mulss	%xmm11, %xmm11
	mulss	%xmm13, %xmm13
	addss	%xmm11, %xmm13
	movaps	%xmm12, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm13, %xmm0
	jmp	.LBB2_23
.LBB2_18:                               #   in Loop: Header=BB2_4 Depth=1
	movl	$1065353216, 16(%rsp)   # imm = 0x3F800000
	movl	$0, 20(%rsp)
	movl	$1, 12(%rsp)
	mulss	%xmm8, %xmm8
	mulss	%xmm9, %xmm9
	addss	%xmm8, %xmm9
	mulss	%xmm10, %xmm10
	addss	%xmm9, %xmm10
	movaps	%xmm10, %xmm0
	jmp	.LBB2_23
	.p2align	4, 0x90
.LBB2_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_31 Depth 2
	movl	444(%r14), %ebx
	mulss	%xmm2, %xmm2
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB2_6
# BB#5:                                 # %call.sqrt
                                        #   in Loop: Header=BB2_4 Depth=1
	callq	sqrtf
	movss	.LCPI2_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB2_6:                                # %.split
                                        #   in Loop: Header=BB2_4 Depth=1
	ucomiss	%xmm1, %xmm7
	jbe	.LBB2_8
# BB#7:
	movl	$1, 456(%r14)
	jmp	.LBB2_36
.LBB2_10:
	movl	444(%r14), %eax
	imulq	$56, %rax, %rax
	leaq	(%r14,%rax), %rcx
	movl	216(%r14,%rax), %edx
	decl	%edx
	movl	%edx, 216(%r14,%rax)
	movq	168(%rcx,%rdx,8), %rax
	movl	440(%r14), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 440(%r14)
	movq	%rax, 408(%r14,%rcx,8)
.LBB2_36:                               # %.thread
	movl	444(%r14), %eax
	imulq	$56, %rax, %rax
	leaq	168(%r14,%rax), %rax
	movq	%rax, 448(%r14)
	movl	456(%r14), %eax
	cmpl	$1, %eax
	je	.LBB2_37
# BB#38:                                # %.thread
	testl	%eax, %eax
	jne	.LBB2_43
# BB#39:
	movss	144(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	148(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movss	152(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB2_41
# BB#40:                                # %call.sqrt182
	movaps	%xmm1, %xmm0
	callq	sqrtf
.LBB2_41:                               # %.split181
	movl	456(%r14), %eax
	jmp	.LBB2_42
.LBB2_37:
	xorps	%xmm0, %xmm0
	movl	$1, %eax
.LBB2_42:                               # %.sink.split
	movss	%xmm0, 160(%r14)
.LBB2_43:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_29:                               # %.thread166
	movl	$2, 456(%r14)
	jmp	.LBB2_36
.Lfunc_end2:
	.size	_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3, .Lfunc_end2-_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3
	.cfi_endproc

	.section	.text._ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j,"axG",@progbits,_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j,comdat
	.weak	_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j
	.p2align	4, 0x90
	.type	_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j,@function
_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j: # @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testl	%edx, %edx
	je	.LBB3_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	128(%rbx), %rax
	addq	136(%rbx), %rdi
	testb	$1, %al
	je	.LBB3_3
# BB#2:
	movq	(%rdi), %rcx
	movq	-1(%rcx,%rax), %rax
.LBB3_3:
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	36(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	mulss	48(%rbx), %xmm2
	mulss	52(%rbx), %xmm1
	addss	%xmm2, %xmm1
	mulss	56(%rbx), %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm3, (%rsp)
	movlps	%xmm1, 8(%rsp)
	movq	%rsp, %rsi
	callq	*%rax
	movaps	%xmm0, %xmm2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movaps	%xmm0, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	84(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm4, %xmm6
	addps	%xmm2, %xmm6
	movss	104(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	88(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	mulps	%xmm1, %xmm5
	addps	%xmm6, %xmm5
	movsd	112(%rbx), %xmm0        # xmm0 = mem[0],zero
	addps	%xmm5, %xmm0
	movss	100(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	addss	%xmm3, %xmm1
	addss	%xmm1, %xmm2
	addss	120(%rbx), %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB3_4:
	movq	(%rbx), %rdi
	movq	128(%rbx), %rax
	addq	136(%rbx), %rdi
	testb	$1, %al
	je	.LBB3_6
# BB#5:
	movq	(%rdi), %rcx
	movq	-1(%rcx,%rax), %rax
.LBB3_6:                                # %_ZNK12gjkepa2_impl13MinkowskiDiff8Support0ERK9btVector3.exit
	addq	$16, %rsp
	popq	%rbx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end3:
	.size	_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j, .Lfunc_end3-_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb
	.p2align	4, 0x90
	.type	_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb,@function
_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb: # @_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 40
	subq	$15240, %rsp            # imm = 0x3B88
.Lcfi31:
	.cfi_def_cfa_offset 15280
.Lcfi32:
	.cfi_offset %rbx, -40
.Lcfi33:
	.cfi_offset %r12, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %r15, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rsi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
	movl	$0, 32(%r14)
	movq	%rdi, 80(%rsp)
	movq	%rdx, 88(%rsp)
	movss	(%r12), %xmm14          # xmm14 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 16(%rsp)         # 4-byte Spill
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	%xmm9, 60(%rsp)         # 4-byte Spill
	movaps	%xmm1, %xmm0
	mulss	%xmm14, %xmm0
	movss	16(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	16(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm4, %xmm3
	addss	%xmm0, %xmm3
	movss	32(%rcx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	32(%r12), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	mulss	%xmm11, %xmm0
	addss	%xmm3, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movaps	%xmm1, %xmm0
	mulss	%xmm2, %xmm0
	movss	20(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm7
	mulss	%xmm3, %xmm7
	addss	%xmm0, %xmm7
	movss	36(%r12), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm13
	mulss	%xmm8, %xmm13
	addss	%xmm7, %xmm13
	movss	8(%r12), %xmm12         # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm1
	movss	24(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm1, %xmm5
	movss	40(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm10
	addss	%xmm5, %xmm10
	movaps	%xmm14, %xmm5
	mulss	%xmm9, %xmm5
	movss	20(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm7
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	addss	%xmm5, %xmm7
	movss	36(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 56(%rsp)         # 4-byte Spill
	movaps	%xmm11, %xmm15
	mulss	%xmm6, %xmm15
	addss	%xmm7, %xmm15
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm7
	movaps	%xmm3, %xmm5
	mulss	%xmm0, %xmm5
	addss	%xmm7, %xmm5
	movaps	%xmm8, %xmm7
	mulss	%xmm6, %xmm7
	addss	%xmm5, %xmm7
	movaps	%xmm12, %xmm5
	mulss	%xmm9, %xmm5
	movaps	%xmm2, %xmm9
	mulss	%xmm0, %xmm9
	addss	%xmm5, %xmm9
	movaps	%xmm1, %xmm5
	mulss	%xmm6, %xmm5
	addss	%xmm9, %xmm5
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm14
	movaps	%xmm0, %xmm6
	movss	24(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm14, %xmm4
	movss	40(%rcx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm11
	addss	%xmm4, %xmm11
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm9, %xmm8
	addss	%xmm3, %xmm8
	mulss	%xmm6, %xmm12
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm2
	movaps	%xmm0, %xmm14
	movss	%xmm14, 8(%rsp)         # 4-byte Spill
	addss	%xmm12, %xmm2
	mulss	%xmm9, %xmm1
	movss	%xmm9, 4(%rsp)          # 4-byte Spill
	addss	%xmm2, %xmm1
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 96(%rsp)
	movss	%xmm13, 100(%rsp)
	movss	%xmm10, 104(%rsp)
	movl	$0, 108(%rsp)
	movss	%xmm15, 112(%rsp)
	movss	%xmm7, 116(%rsp)
	movss	%xmm5, 120(%rsp)
	movl	$0, 124(%rsp)
	movss	%xmm11, 128(%rsp)
	movss	%xmm8, 132(%rsp)
	movss	%xmm1, 136(%rsp)
	movl	$0, 140(%rsp)
	movsd	(%r12), %xmm3           # xmm3 = mem[0],zero
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm0, %xmm8
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm4
	mulss	%xmm12, %xmm4
	movss	%xmm12, 32(%rsp)        # 4-byte Spill
	addss	%xmm1, %xmm4
	movss	32(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	%xmm5, 16(%rsp)         # 4-byte Spill
	movsd	32(%r12), %xmm10        # xmm10 = mem[0],zero
	movaps	%xmm10, %xmm1
	mulss	%xmm5, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm1, 288(%rsp)        # 16-byte Spill
	movaps	%xmm0, %xmm4
	movss	60(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	mulss	64(%rsp), %xmm4         # 4-byte Folded Reload
	addss	%xmm2, %xmm4
	movaps	%xmm10, %xmm2
	movss	56(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm2
	addss	%xmm4, %xmm2
	movaps	%xmm2, 272(%rsp)        # 16-byte Spill
	movaps	%xmm3, %xmm2
	mulss	%xmm6, %xmm2
	movaps	%xmm0, %xmm4
	mulss	%xmm14, %xmm4
	addss	%xmm2, %xmm4
	movaps	%xmm10, %xmm2
	mulss	%xmm9, %xmm2
	addss	%xmm4, %xmm2
	movaps	%xmm2, 256(%rsp)        # 16-byte Spill
	movss	48(%rcx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	subss	48(%r12), %xmm14
	movaps	%xmm14, 240(%rsp)       # 16-byte Spill
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	mulps	%xmm3, %xmm14
	pshufd	$229, %xmm3, %xmm6      # xmm6 = xmm3[1,1,2,3]
	movss	52(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	52(%r12), %xmm3
	movaps	%xmm3, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm0, %xmm5
	pshufd	$229, %xmm0, %xmm7      # xmm7 = xmm0[1,1,2,3]
	movaps	%xmm8, %xmm4
	movaps	%xmm4, %xmm0
	mulss	%xmm6, %xmm0
	mulss	%xmm7, %xmm12
	addss	%xmm0, %xmm12
	movss	56(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	56(%r12), %xmm2
	movaps	%xmm2, %xmm8
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm10, %xmm8
	pshufd	$229, %xmm10, %xmm10    # xmm10 = xmm10[1,1,2,3]
	movss	16(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm13
	addss	%xmm12, %xmm13
	movaps	%xmm1, %xmm9
	movaps	%xmm9, %xmm12
	mulss	%xmm6, %xmm12
	movss	64(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm12, %xmm0
	movaps	%xmm15, %xmm12
	mulss	%xmm10, %xmm12
	addss	%xmm0, %xmm12
	mulss	12(%rsp), %xmm6         # 4-byte Folded Reload
	mulss	8(%rsp), %xmm7          # 4-byte Folded Reload
	addss	%xmm6, %xmm7
	mulss	4(%rsp), %xmm10         # 4-byte Folded Reload
	addss	%xmm7, %xmm10
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm7
	movss	24(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	addss	%xmm7, %xmm4
	movss	%xmm4, 32(%rsp)         # 4-byte Spill
	movss	40(%r12), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm4
	addss	32(%rsp), %xmm4         # 4-byte Folded Reload
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm9
	mulss	%xmm6, %xmm1
	addss	%xmm9, %xmm1
	mulss	%xmm11, %xmm15
	addss	%xmm1, %xmm15
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	8(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm7
	addss	%xmm1, %xmm7
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	addss	%xmm7, %xmm1
	addps	%xmm14, %xmm5
	addps	%xmm5, %xmm8
	movaps	240(%rsp), %xmm5        # 16-byte Reload
	mulss	%xmm0, %xmm5
	mulss	%xmm6, %xmm3
	addss	%xmm5, %xmm3
	mulss	%xmm11, %xmm2
	addss	%xmm3, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movaps	288(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 144(%rsp)
	movaps	272(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 148(%rsp)
	movaps	256(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 152(%rsp)
	movl	$0, 156(%rsp)
	movss	%xmm13, 160(%rsp)
	movss	%xmm12, 164(%rsp)
	movss	%xmm10, 168(%rsp)
	movl	$0, 172(%rsp)
	movss	16(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 176(%rsp)
	movss	%xmm15, 180(%rsp)
	movss	%xmm1, 184(%rsp)
	movb	15280(%rsp), %al
	movl	$0, 188(%rsp)
	movlps	%xmm8, 192(%rsp)
	movlps	%xmm0, 200(%rsp)
	movl	$_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3, %ecx
	movl	$_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3, %edx
	testb	%al, %al
	cmovneq	%rcx, %rdx
	movq	%rdx, 208(%rsp)
	movq	$0, 216(%rsp)
	movl	$0, 752(%rsp)
	xorps	%xmm1, %xmm1
	movups	%xmm1, 456(%rsp)
	movl	$2, 768(%rsp)
	movl	$0, 756(%rsp)
	movl	$0, 472(%rsp)
	movaps	.LCPI4_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm0
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	xorps	%xmm2, %xmm0
	movlps	%xmm0, 776(%rsp)
	movlps	%xmm1, 784(%rsp)
	leaq	312(%rsp), %rdi
	leaq	80(%rsp), %rsi
	leaq	776(%rsp), %rdx
	callq	_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3
	cmpl	$2, %eax
	je	.LBB4_15
# BB#1:
	cmpl	$1, %eax
	jne	.LBB4_16
# BB#2:
	movq	$0, 15208(%rsp)
	movl	$0, 15216(%rsp)
	movq	$0, 15224(%rsp)
	movl	$0, 15232(%rsp)
	movl	$9, 776(%rsp)
	movl	$0, 15200(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 840(%rsp)
	movl	$0, 856(%rsp)
	xorl	%ecx, %ecx
	movl	$12288, %eax            # imm = 0x3000
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	leaq	2816(%rsp,%rax), %rdx
	movq	$0, 2888(%rsp,%rax)
	movq	%rcx, 2896(%rsp,%rax)
	testq	%rcx, %rcx
	je	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	%rdx, 72(%rcx)
.LBB4_5:                                # %_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE.exit.i.i.1
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%rdx, 15224(%rsp)
	leaq	2720(%rsp,%rax), %rcx
	movq	$0, 2792(%rsp,%rax)
	movq	%rdx, 2800(%rsp,%rax)
	movq	%rcx, 2888(%rsp,%rax)
	movq	%rcx, 15224(%rsp)
	addq	$-192, %rax
	jne	.LBB4_3
# BB#6:                                 # %_ZN12gjkepa2_impl3EPAC2Ev.exit
	movl	$128, 15232(%rsp)
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	movaps	.LCPI4_0(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	8(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 224(%rsp)
	movlps	%xmm2, 232(%rsp)
	leaq	776(%rsp), %rdi
	leaq	312(%rsp), %rsi
	leaq	224(%rsp), %rdx
	callq	_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3
	cmpl	$9, %eax
	jne	.LBB4_7
# BB#14:
	movl	$3, (%r14)
	jmp	.LBB4_16
.LBB4_15:
	movl	$2, (%r14)
.LBB4_16:
	xorl	%eax, %eax
.LBB4_17:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$15240, %rsp            # imm = 0x3B88
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB4_7:                                # %.preheader
	cmpl	$0, 832(%rsp)
	je	.LBB4_8
# BB#10:                                # %.lr.ph
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	xorps	%xmm4, %xmm4
	xorps	%xmm7, %xmm7
	.p2align	4, 0x90
.LBB4_11:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebx
	movq	784(%rsp,%rbx,8), %rsi
	movq	80(%rsp), %rdi
	movq	208(%rsp), %rax
	addq	216(%rsp), %rdi
	testb	$1, %al
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	je	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_11 Depth=1
	movq	(%rdi), %rcx
	movq	-1(%rcx,%rax), %rax
.LBB4_13:                               # %_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j.exit
                                        #   in Loop: Header=BB4_11 Depth=1
	callq	*%rax
	movss	816(%rsp,%rbx,4), %xmm2 # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm1
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	addss	%xmm3, %xmm7
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	addss	%xmm0, %xmm4
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	addss	%xmm1, %xmm0
	leal	1(%rbx), %eax
	cmpl	832(%rsp), %eax
	jb	.LBB4_11
	jmp	.LBB4_9
.LBB4_8:
	xorps	%xmm7, %xmm7
	xorps	%xmm4, %xmm4
	xorps	%xmm0, %xmm0
.LBB4_9:                                # %.critedge
	movl	$1, (%r14)
	movaps	%xmm7, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	16(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%r12), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	mulps	%xmm5, %xmm2
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	20(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	mulps	%xmm5, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm0, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	24(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	mulps	%xmm5, %xmm2
	addps	%xmm3, %xmm2
	movsd	48(%r12), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm2, %xmm1
	movss	32(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	movss	36(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm3, %xmm2
	movss	40(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	addss	56(%r12), %xmm3
	xorps	%xmm8, %xmm8
	xorps	%xmm2, %xmm2
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	movlps	%xmm1, 4(%r14)
	movlps	%xmm2, 12(%r14)
	movss	848(%rsp), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	856(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movsd	840(%rsp), %xmm10       # xmm10 = mem[0],zero
	movaps	%xmm10, %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$229, %xmm10, %xmm5     # xmm5 = xmm10[1,1,2,3]
	mulss	%xmm1, %xmm5
	movaps	%xmm1, %xmm6
	mulss	%xmm9, %xmm6
	subss	%xmm2, %xmm7
	subss	%xmm5, %xmm4
	subss	%xmm6, %xmm0
	movaps	%xmm7, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	16(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	(%r12), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm2, %xmm6
	movaps	%xmm4, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	20(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	mulps	%xmm2, %xmm3
	addps	%xmm6, %xmm3
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	24(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm2, %xmm6
	addps	%xmm3, %xmm6
	movsd	48(%r12), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm6, %xmm2
	mulss	32(%r12), %xmm7
	mulss	36(%r12), %xmm4
	addss	%xmm7, %xmm4
	mulss	40(%r12), %xmm0
	addss	%xmm4, %xmm0
	addss	56(%r12), %xmm0
	xorps	%xmm5, %xmm5
	movss	%xmm0, %xmm5            # xmm5 = xmm0[0],xmm5[1,2,3]
	movlps	%xmm2, 20(%r14)
	movlps	%xmm5, 28(%r14)
	movaps	.LCPI4_0(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm4, %xmm10
	xorps	%xmm4, %xmm9
	movss	%xmm9, %xmm8            # xmm8 = xmm9[0],xmm8[1,2,3]
	movlps	%xmm10, 36(%r14)
	movlps	%xmm8, 44(%r14)
	xorps	%xmm4, %xmm1
	movss	%xmm1, 52(%r14)
	movb	$1, %al
	jmp	.LBB4_17
.Lfunc_end4:
	.size	_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb, .Lfunc_end4-_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI5_3:
	.zero	16
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_1:
	.long	1065353216              # float 1
.LCPI5_2:
	.long	953267991               # float 9.99999974E-5
	.section	.text._ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3,"axG",@progbits,_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3,comdat
	.weak	_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3
	.p2align	4, 0x90
	.type	_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3,@function
_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3: # @_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 176
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	448(%r15), %r14
	cmpl	$2, 48(%r14)
	movq	%r12, %rbp
	jb	.LBB5_59
# BB#1:
	movq	%r15, %rdi
	callq	_ZN12gjkepa2_impl3GJK13EncloseOriginEv
	testb	%al, %al
	je	.LBB5_59
# BB#2:                                 # %.preheader377
	movq	14432(%r12), %rax
	testq	%rax, %rax
	je	.LBB5_14
# BB#3:                                 # %.lr.ph
	movl	14440(%r12), %edx
	movl	14456(%r12), %ecx
	.p2align	4, 0x90
.LBB5_4:                                # =>This Inner Loop Header: Depth=1
	movq	80(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
	movq	72(%rax), %rdi
	movq	%rdi, 72(%rsi)
.LBB5_6:                                #   in Loop: Header=BB5_4 Depth=1
	movq	72(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_4 Depth=1
	movq	80(%rax), %rdi
	movq	%rdi, 80(%rsi)
.LBB5_8:                                #   in Loop: Header=BB5_4 Depth=1
	movq	%rbp, %rdi
	cmpq	%rax, 14432(%rdi)
	jne	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_4 Depth=1
	movq	80(%rax), %rsi
	movq	%rsi, 14432(%rdi)
.LBB5_10:                               # %_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE.exit
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	$0, 72(%rax)
	movq	14448(%rdi), %rsi
	movq	%rsi, 80(%rax)
	movq	14448(%rdi), %rsi
	testq	%rsi, %rsi
	je	.LBB5_12
# BB#11:                                #   in Loop: Header=BB5_4 Depth=1
	movq	%rax, 72(%rsi)
.LBB5_12:                               # %_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE.exit
                                        #   in Loop: Header=BB5_4 Depth=1
	decl	%edx
	movq	%rbp, %rsi
	movq	%rax, 14448(%rsi)
	incl	%ecx
	movq	14432(%rsi), %rax
	testq	%rax, %rax
	jne	.LBB5_4
# BB#13:                                # %._crit_edge
	movq	%rbp, %r12
	movl	%edx, 14440(%r12)
	movl	%ecx, 14456(%r12)
.LBB5_14:
	movl	$0, (%r12)
	movl	$0, 14424(%r12)
	movq	(%r14), %rax
	movq	8(%r14), %rsi
	movq	24(%r14), %rcx
	movss	16(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm0
	subss	%xmm1, %xmm9
	movss	24(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm8
	movss	16(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm4
	subss	%xmm1, %xmm2
	movss	24(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm6
	movq	16(%r14), %rcx
	movss	16(%rcx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm10
	subss	%xmm1, %xmm5
	movss	24(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	movaps	%xmm8, %xmm3
	mulss	%xmm4, %xmm3
	mulss	%xmm9, %xmm4
	mulss	%xmm6, %xmm9
	mulss	%xmm10, %xmm9
	mulss	%xmm5, %xmm3
	addss	%xmm9, %xmm3
	mulss	%xmm0, %xmm6
	mulss	%xmm5, %xmm6
	subss	%xmm6, %xmm3
	mulss	%xmm1, %xmm4
	subss	%xmm4, %xmm3
	mulss	%xmm2, %xmm0
	mulss	%xmm1, %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm8, %xmm2
	mulss	%xmm10, %xmm2
	subss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB5_15
# BB#16:
	movq	%rsi, (%r14)
	movq	%rax, 8(%r14)
	movl	32(%r14), %edx
	movl	36(%r14), %edi
	movl	%edi, 32(%r14)
	movl	%edx, 36(%r14)
	movq	%rax, %rdx
	jmp	.LBB5_17
.LBB5_15:
	movq	%rsi, %rdx
	movq	%rax, %rsi
.LBB5_17:
	movl	$1, %r8d
	movq	%rbp, %r12
	movq	%r12, %rdi
	callq	_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	(%r14), %rdx
	movq	8(%r14), %rsi
	movq	24(%r14), %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	callq	_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%r14), %rdx
	movq	16(%r14), %rsi
	movq	24(%r14), %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	callq	_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b
	movq	%rax, %r13
	movq	(%r14), %rsi
	movq	16(%r14), %rdx
	movq	24(%r14), %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	callq	_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b
	cmpl	$4, 14440(%r12)
	jne	.LBB5_59
# BB#18:
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	14432(%r12), %r15
	movss	16(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movq	80(%r15), %rcx
	testq	%rcx, %rcx
	movq	%r12, %r14
	je	.LBB5_19
# BB#20:                                # %.lr.ph.preheader.i199
	movss	20(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB5_21:                               # %.lr.ph.i211
                                        # =>This Inner Loop Header: Depth=1
	movss	16(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm2
	setae	%sil
	ucomiss	%xmm1, %xmm3
	seta	%dl
	andb	%sil, %dl
	jne	.LBB5_23
# BB#22:                                # %.lr.ph.i211
                                        #   in Loop: Header=BB5_21 Depth=1
	movaps	%xmm3, %xmm1
	movaps	%xmm0, %xmm2
.LBB5_23:                               # %.lr.ph.i211
                                        #   in Loop: Header=BB5_21 Depth=1
	testb	%dl, %dl
	cmovneq	%rcx, %r15
	movq	80(%rcx), %rcx
	testq	%rcx, %rcx
	movaps	%xmm2, %xmm0
	movaps	%xmm1, %xmm3
	jne	.LBB5_21
# BB#24:                                # %_ZN12gjkepa2_impl3EPA8findbestEv.exit212.loopexit
	movss	16(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	jmp	.LBB5_25
.LBB5_59:                               # %.thread
	movl	$8, (%r12)
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movaps	.LCPI5_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm3
	xorps	%xmm2, %xmm3
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm2
	xorps	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm3, 64(%r12)
	movlps	%xmm4, 72(%r12)
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_61
# BB#60:                                # %call.sqrt524
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB5_61:                               # %.thread.split
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	jbe	.LBB5_63
# BB#62:
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movq	%rbp, %rcx
	movsd	64(%rcx), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	72(%rcx), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 64(%rcx)
	movlps	%xmm1, 72(%rcx)
	jmp	.LBB5_64
.LBB5_63:
	movq	%rbp, %rcx
	movq	$1065353216, 64(%rcx)   # imm = 0x3F800000
	movq	$0, 72(%rcx)
.LBB5_64:
	movl	$0, 80(%rcx)
	movl	$1, 56(%rcx)
	movq	(%r14), %rax
	movq	%rax, 8(%rcx)
	movl	$1065353216, 40(%rcx)   # imm = 0x3F800000
	movq	%rcx, %r14
.LBB5_65:
	movl	(%r14), %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_19:
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
.LBB5_25:                               # %_ZN12gjkepa2_impl3EPA8findbestEv.exit212
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movups	(%r15), %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movss	20(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movdqu	24(%r15), %xmm7
	movq	40(%r15), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movb	$0, 88(%rdx)
	movq	%rsi, 48(%rdx)
	movb	$0, 88(%rsi)
	movq	%rdx, 48(%rsi)
	movb	$0, 89(%rdx)
	movq	%r13, 56(%rdx)
	movb	$1, 88(%r13)
	movq	%rdx, 48(%r13)
	movb	$0, 90(%rdx)
	movq	%rax, 64(%rdx)
	movb	$2, 88(%rax)
	movq	%rdx, 48(%rax)
	movb	$2, 89(%rsi)
	movq	%rax, 56(%rsi)
	movb	$1, 90(%rax)
	movq	%rsi, 64(%rax)
	movb	$1, 90(%rsi)
	movq	%r13, 64(%rsi)
	movb	$2, 89(%r13)
	movq	%rsi, 56(%r13)
	movb	$1, 90(%r13)
	movq	%rax, 64(%r13)
	movb	$2, 89(%rax)
	movq	%r13, 56(%rax)
	movl	$0, (%r14)
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_26:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_57 Depth 2
                                        #     Child Loop BB5_50 Depth 2
	movq	%r14, %rbx
	movl	14424(%rbx), %r14d
	cmpq	$63, %r14
	ja	.LBB5_66
# BB#27:                                #   in Loop: Header=BB5_26 Depth=1
	movdqa	%xmm7, 32(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 96(%rsp)
	movl	$0, 112(%rsp)
	leal	1(%r14), %eax
	movl	%eax, 14424(%rbx)
	shlq	$5, %r14
	leaq	84(%rbx,%r14), %r12
	incl	%r13d
	movb	%r13b, 91(%r15)
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	100(%rbx,%r14), %xmm0
	mulss	104(%rbx,%r14), %xmm1
	addss	%xmm0, %xmm1
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	108(%rbx,%r14), %xmm0
	addss	%xmm1, %xmm0
	subss	16(%r15), %xmm0
	ucomiss	.LCPI5_2(%rip), %xmm0
	movq	%rbx, %r14
	jbe	.LBB5_28
# BB#56:                                # %.preheader.preheader
                                        #   in Loop: Header=BB5_26 Depth=1
	movb	$1, %al
	movl	$88, %ebp
	.p2align	4, 0x90
.LBB5_57:                               # %.preheader
                                        #   Parent Loop BB5_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebx
	movq	-656(%r15,%rbp,8), %rcx
	movzbl	(%r15,%rbp), %r8d
	movq	%r14, %rdi
	movl	%r13d, %esi
	movq	%r12, %rdx
	leaq	96(%rsp), %r9
	callq	_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE
	andb	%bl, %al
	leaq	-87(%rbp), %rcx
	cmpq	$2, %rcx
	ja	.LBB5_38
# BB#58:                                # %.preheader
                                        #   in Loop: Header=BB5_57 Depth=2
	incq	%rbp
	testb	%al, %al
	jne	.LBB5_57
.LBB5_38:                               #   in Loop: Header=BB5_26 Depth=1
	movl	$4, %ecx
	testb	%al, %al
	je	.LBB5_29
# BB#39:                                #   in Loop: Header=BB5_26 Depth=1
	cmpl	$2, 112(%rsp)
	movdqa	32(%rsp), %xmm7         # 16-byte Reload
	jbe	.LBB5_30
# BB#40:                                #   in Loop: Header=BB5_26 Depth=1
	movq	96(%rsp), %rax
	movq	104(%rsp), %rcx
	movb	$2, 89(%rax)
	movq	%rcx, 56(%rax)
	movb	$1, 90(%rcx)
	movq	%rax, 64(%rcx)
	movq	80(%r15), %rax
	testq	%rax, %rax
	je	.LBB5_42
# BB#41:                                #   in Loop: Header=BB5_26 Depth=1
	movq	72(%r15), %rcx
	movq	%rcx, 72(%rax)
.LBB5_42:                               #   in Loop: Header=BB5_26 Depth=1
	movq	72(%r15), %rax
	testq	%rax, %rax
	je	.LBB5_44
# BB#43:                                #   in Loop: Header=BB5_26 Depth=1
	movq	80(%r15), %rcx
	movq	%rcx, 80(%rax)
.LBB5_44:                               #   in Loop: Header=BB5_26 Depth=1
	cmpq	%r15, 14432(%r14)
	jne	.LBB5_46
# BB#45:                                #   in Loop: Header=BB5_26 Depth=1
	movq	80(%r15), %rax
	movq	%rax, 14432(%r14)
.LBB5_46:                               # %_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE.exit196
                                        #   in Loop: Header=BB5_26 Depth=1
	decl	14440(%r14)
	movq	$0, 72(%r15)
	movq	14448(%r14), %rax
	movq	%rax, 80(%r15)
	movq	14448(%r14), %rax
	testq	%rax, %rax
	je	.LBB5_48
# BB#47:                                #   in Loop: Header=BB5_26 Depth=1
	movq	%r15, 72(%rax)
.LBB5_48:                               # %_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE.exit195
                                        #   in Loop: Header=BB5_26 Depth=1
	movq	%r15, 14448(%r14)
	incl	14456(%r14)
	movq	14432(%r14), %r15
	movq	80(%r15), %rax
	testq	%rax, %rax
	je	.LBB5_53
# BB#49:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB5_26 Depth=1
	movss	16(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	20(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB5_50:                               # %.lr.ph.i
                                        #   Parent Loop BB5_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	20(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm2
	ucomiss	%xmm0, %xmm3
	setae	%dl
	ucomiss	%xmm2, %xmm1
	seta	%cl
	andb	%dl, %cl
	jne	.LBB5_52
# BB#51:                                # %.lr.ph.i
                                        #   in Loop: Header=BB5_50 Depth=2
	movaps	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
.LBB5_52:                               # %.lr.ph.i
                                        #   in Loop: Header=BB5_50 Depth=2
	testb	%cl, %cl
	cmovneq	%rax, %r15
	movq	80(%rax), %rax
	testq	%rax, %rax
	movaps	%xmm3, %xmm0
	movaps	%xmm2, %xmm1
	jne	.LBB5_50
.LBB5_53:                               # %_ZN12gjkepa2_impl3EPA8findbestEv.exit
                                        #   in Loop: Header=BB5_26 Depth=1
	movss	20(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rsp), %xmm0         # 4-byte Folded Reload
	jb	.LBB5_55
# BB#54:                                #   in Loop: Header=BB5_26 Depth=1
	movups	(%r15), %xmm1
	movaps	%xmm1, 48(%rsp)         # 16-byte Spill
	movss	16(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movdqu	24(%r15), %xmm7
	movq	40(%r15), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
.LBB5_55:                               #   in Loop: Header=BB5_26 Depth=1
	cmpl	$255, %r13d
	jb	.LBB5_26
	jmp	.LBB5_31
.LBB5_66:
	movl	$6, (%rbx)
	movq	%rbx, %r14
	jmp	.LBB5_31
.LBB5_28:
	movl	$7, %ecx
.LBB5_29:
	movdqa	32(%rsp), %xmm7         # 16-byte Reload
.LBB5_30:
	movl	%ecx, (%r14)
.LBB5_31:                               # %.loopexit
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm8
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm8
	movaps	%xmm1, %xmm9
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	mulss	%xmm0, %xmm9
	movaps	%xmm1, %xmm10
	movhlps	%xmm10, %xmm10          # xmm10 = xmm10[1,1]
	mulss	%xmm0, %xmm10
	movups	%xmm1, 64(%r14)
	movss	%xmm0, 80(%r14)
	movl	$3, 56(%r14)
	movdqu	%xmm7, 8(%r14)
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, 24(%r14)
	pshufd	$78, %xmm7, %xmm0       # xmm0 = xmm7[2,3,0,1]
	movd	%xmm0, %rbx
	movss	16(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm2
	subss	%xmm9, %xmm3
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm1
	movss	16(%rbp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm4
	subss	%xmm9, %xmm0
	movss	24(%rbp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm5
	movaps	%xmm1, %xmm6
	mulss	%xmm4, %xmm1
	mulss	%xmm3, %xmm4
	mulss	%xmm5, %xmm3
	mulss	%xmm0, %xmm6
	subss	%xmm6, %xmm3
	mulss	%xmm2, %xmm5
	subss	%xmm5, %xmm1
	mulss	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_33
# BB#32:                                # %call.sqrt
	movdqa	%xmm7, 32(%rsp)         # 16-byte Spill
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	movaps	%xmm10, 48(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm10        # 16-byte Reload
	movaps	64(%rsp), %xmm9         # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movdqa	32(%rsp), %xmm7         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB5_33:                               # %.loopexit.split
	movss	%xmm1, 40(%r14)
	movss	16(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	20(%rbp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm2
	subss	%xmm9, %xmm3
	movss	24(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm1
	movd	%xmm7, %rbp
	movss	16(%rbp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm4
	subss	%xmm9, %xmm0
	movss	24(%rbp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm5
	movaps	%xmm1, %xmm6
	mulss	%xmm4, %xmm1
	mulss	%xmm3, %xmm4
	mulss	%xmm5, %xmm3
	mulss	%xmm0, %xmm6
	subss	%xmm6, %xmm3
	mulss	%xmm2, %xmm5
	subss	%xmm5, %xmm1
	mulss	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_35
# BB#34:                                # %call.sqrt522
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	movaps	%xmm10, 48(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm10        # 16-byte Reload
	movaps	64(%rsp), %xmm9         # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB5_35:                               # %.loopexit.split.split
	movss	%xmm1, 44(%r14)
	movss	16(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	20(%rbp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm2
	subss	%xmm9, %xmm3
	movss	24(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm1
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm4
	subss	%xmm9, %xmm0
	movss	24(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm5
	movaps	%xmm1, %xmm6
	mulss	%xmm4, %xmm1
	mulss	%xmm3, %xmm4
	mulss	%xmm5, %xmm3
	mulss	%xmm0, %xmm6
	subss	%xmm6, %xmm3
	mulss	%xmm2, %xmm5
	subss	%xmm5, %xmm1
	mulss	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_37
# BB#36:                                # %call.sqrt523
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB5_37:                               # %.loopexit.split.split.split
	movss	40(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	44(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	addss	%xmm1, %xmm3
	divss	%xmm3, %xmm0
	movss	%xmm0, 40(%r14)
	divss	%xmm3, %xmm2
	movss	%xmm2, 44(%r14)
	divss	%xmm3, %xmm1
	movss	%xmm1, 48(%r14)
	jmp	.LBB5_65
.Lfunc_end5:
	.size	_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3, .Lfunc_end5-_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	2139095039              # float 3.40282347E+38
.LCPI6_1:
	.long	872415232               # float 1.1920929E-7
.LCPI6_2:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI6_4:
	.zero	16
	.text
	.globl	_ZN15btGjkEpaSolver214SignedDistanceERK9btVector3fPK13btConvexShapeRK11btTransformRNS_8sResultsE
	.p2align	4, 0x90
	.type	_ZN15btGjkEpaSolver214SignedDistanceERK9btVector3fPK13btConvexShapeRK11btTransformRNS_8sResultsE,@function
_ZN15btGjkEpaSolver214SignedDistanceERK9btVector3fPK13btConvexShapeRK11btTransformRNS_8sResultsE: # @_ZN15btGjkEpaSolver214SignedDistanceERK9btVector3fPK13btConvexShapeRK11btTransformRNS_8sResultsE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$968, %rsp              # imm = 0x3C8
.Lcfi55:
	.cfi_def_cfa_offset 1024
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	leaq	400(%rsp), %r12
	movq	%r12, %rdi
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV13btSphereShape+16, 400(%rsp)
	movl	$8, 408(%rsp)
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 440(%rsp)
	movss	%xmm0, 456(%rsp)
	movl	$1065353216, 128(%rsp)  # imm = 0x3F800000
	xorps	%xmm1, %xmm1
	movups	%xmm1, 132(%rsp)
	movl	$1065353216, 148(%rsp)  # imm = 0x3F800000
	movups	%xmm1, 152(%rsp)
	movq	$1065353216, 168(%rsp)  # imm = 0x3F800000
	movups	(%rbx), %xmm0
	movups	%xmm0, 176(%rsp)
	movups	%xmm1, 16(%r15)
	movups	%xmm1, (%r15)
	movl	$0, 32(%r15)
	movq	%r14, 256(%rsp)
	movq	%r12, 264(%rsp)
	movsd	16(%rbp), %xmm0         # xmm0 = mem[0],zero
	xorps	%xmm13, %xmm13
	movaps	%xmm0, %xmm12
	mulss	%xmm13, %xmm12
	movsd	(%rbp), %xmm10          # xmm10 = mem[0],zero
	movaps	%xmm10, %xmm15
	addss	%xmm12, %xmm15
	movsd	32(%rbp), %xmm11        # xmm11 = mem[0],zero
	movaps	%xmm11, %xmm1
	mulss	%xmm13, %xmm1
	addss	%xmm1, %xmm15
	pshufd	$229, %xmm0, %xmm14     # xmm14 = xmm0[1,1,2,3]
	movaps	%xmm0, %xmm2
	movaps	%xmm2, 480(%rsp)        # 16-byte Spill
	movdqa	%xmm14, 64(%rsp)        # 16-byte Spill
	mulss	%xmm13, %xmm14
	pshufd	$229, %xmm11, %xmm0     # xmm0 = xmm11[1,1,2,3]
	movss	8(%rbp), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	24(%rbp), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm6
	mulss	%xmm13, %xmm6
	movaps	%xmm10, %xmm4
	mulss	%xmm13, %xmm4
	movaps	%xmm4, %xmm8
	addss	%xmm2, %xmm8
	addss	%xmm1, %xmm8
	movaps	%xmm7, %xmm5
	addss	%xmm6, %xmm5
	mulss	%xmm13, %xmm7
	addss	%xmm7, %xmm9
	addss	%xmm6, %xmm7
	movdqa	%xmm0, %xmm1
	movdqa	%xmm1, 32(%rsp)         # 16-byte Spill
	movdqa	%xmm1, %xmm3
	mulss	%xmm13, %xmm3
	movss	40(%rbp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm7
	mulss	%xmm13, %xmm6
	pshufd	$229, %xmm10, %xmm0     # xmm0 = xmm10[1,1,2,3]
	movdqa	%xmm0, 240(%rsp)        # 16-byte Spill
	movdqa	%xmm0, %xmm2
	mulss	%xmm13, %xmm2
	movdqa	%xmm0, %xmm13
	addss	%xmm14, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm2, %xmm0
	addss	64(%rsp), %xmm0         # 16-byte Folded Reload
	addss	%xmm3, %xmm0
	addss	%xmm6, %xmm5
	addss	%xmm6, %xmm9
	addss	%xmm12, %xmm4
	addss	%xmm14, %xmm2
	addss	%xmm11, %xmm4
	addss	%xmm1, %xmm2
	movss	%xmm15, 272(%rsp)
	movss	%xmm13, 276(%rsp)
	movss	%xmm5, 280(%rsp)
	movl	$0, 284(%rsp)
	movss	%xmm8, 288(%rsp)
	movss	%xmm0, 292(%rsp)
	movss	%xmm9, 296(%rsp)
	movl	$0, 300(%rsp)
	movss	%xmm4, 304(%rsp)
	movss	%xmm2, 308(%rsp)
	movss	%xmm7, 312(%rsp)
	movl	$0, 316(%rsp)
	movss	128(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 192(%rsp)        # 4-byte Spill
	movss	132(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm1
	movaps	%xmm10, %xmm2
	movaps	%xmm10, %xmm6
	mulss	%xmm0, %xmm2
	movss	144(%rsp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	480(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm5
	movaps	%xmm10, %xmm4
	mulss	%xmm8, %xmm4
	movss	%xmm8, 88(%rsp)         # 4-byte Spill
	addss	%xmm2, %xmm4
	movss	160(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movaps	%xmm11, %xmm2
	movaps	%xmm11, %xmm3
	movaps	%xmm11, %xmm13
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm3
	movaps	%xmm3, 224(%rsp)        # 16-byte Spill
	mulss	%xmm7, %xmm1
	movaps	%xmm7, %xmm4
	movss	%xmm4, 92(%rsp)         # 4-byte Spill
	movss	148(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm5
	addss	%xmm1, %xmm5
	movss	164(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm2
	addss	%xmm5, %xmm2
	movaps	%xmm2, 208(%rsp)        # 16-byte Spill
	movss	176(%rsp), %xmm14       # xmm14 = mem[0],zero,zero,zero
	subss	48(%rbp), %xmm14
	movaps	%xmm14, 464(%rsp)       # 16-byte Spill
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	mulps	%xmm6, %xmm14
	movaps	%xmm6, %xmm2
	movss	180(%rsp), %xmm11       # xmm11 = mem[0],zero,zero,zero
	subss	52(%rbp), %xmm11
	movaps	%xmm11, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm10, %xmm6
	movaps	%xmm10, %xmm1
	movss	136(%rsp), %xmm12       # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm2
	movss	152(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	184(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	56(%rbp), %xmm2
	movaps	%xmm2, %xmm9
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm13, %xmm9
	movss	168(%rsp), %xmm10       # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm13
	addss	%xmm1, %xmm13
	movss	192(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	movaps	240(%rsp), %xmm7        # 16-byte Reload
	mulss	%xmm7, %xmm1
	movaps	64(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm3, %xmm8
	addss	%xmm1, %xmm8
	movss	16(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm1, %xmm15
	addss	%xmm8, %xmm15
	movaps	%xmm4, %xmm8
	mulss	%xmm7, %xmm8
	movaps	%xmm7, %xmm4
	movss	48(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	addss	%xmm8, %xmm7
	movss	96(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm8
	addss	%xmm7, %xmm8
	mulss	%xmm12, %xmm4
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm10, %xmm1
	addss	%xmm3, %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movss	8(%rbp), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm7, %xmm3
	movss	24(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm3, %xmm5
	movss	40(%rbp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm5, %xmm4
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	movss	92(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm4
	movss	48(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm4, %xmm5
	movss	96(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm5, %xmm4
	mulss	%xmm7, %xmm12
	mulss	%xmm1, %xmm0
	addss	%xmm12, %xmm0
	mulss	%xmm3, %xmm10
	addss	%xmm0, %xmm10
	addps	%xmm14, %xmm6
	addps	%xmm6, %xmm9
	movaps	464(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm7, %xmm0
	mulss	%xmm1, %xmm11
	addss	%xmm0, %xmm11
	mulss	%xmm3, %xmm2
	addss	%xmm11, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movaps	224(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 320(%rsp)
	movaps	208(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 324(%rsp)
	movss	%xmm13, 328(%rsp)
	movl	$0, 332(%rsp)
	movss	%xmm15, 336(%rsp)
	movss	%xmm8, 340(%rsp)
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	movss	%xmm1, 344(%rsp)
	movl	$0, 348(%rsp)
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 352(%rsp)
	movss	%xmm4, 356(%rsp)
	movss	%xmm10, 360(%rsp)
	movl	$0, 364(%rsp)
	movlps	%xmm9, 368(%rsp)
	movlps	%xmm0, 376(%rsp)
	movl	$_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3, %eax
	movd	%rax, %xmm0
	movdqu	%xmm0, 384(%rsp)
	movl	$0, 944(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 648(%rsp)
	movl	$2, 960(%rsp)
	movl	$0, 948(%rsp)
	movl	$0, 664(%rsp)
	movabsq	$4575657222473777152, %rax # imm = 0x3F8000003F800000
	movq	%rax, 112(%rsp)
	movq	$1065353216, 120(%rsp)  # imm = 0x3F800000
.Ltmp0:
	leaq	504(%rsp), %rdi
	leaq	256(%rsp), %rsi
	leaq	112(%rsp), %rdx
	callq	_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3
.Ltmp1:
# BB#1:
	movss	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	cmpl	$1, %eax
	je	.LBB6_19
# BB#2:
	testl	%eax, %eax
	jne	.LBB6_26
# BB#3:                                 # %.preheader
	movq	952(%rsp), %rcx
	cmpl	$0, 48(%rcx)
	je	.LBB6_4
# BB#5:                                 # %.lr.ph
	xorps	%xmm4, %xmm4
	xorl	%ebx, %ebx
	leaq	256(%rsp), %r12
	leaq	112(%rsp), %r13
	xorps	%xmm3, %xmm3
	xorps	%xmm7, %xmm7
	xorps	%xmm5, %xmm5
	xorps	%xmm6, %xmm6
	xorps	%xmm9, %xmm9
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	movl	%ebx, %ebx
	movd	32(%rcx,%rbx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movq	(%rcx,%rbx,8), %rsi
	movq	256(%rsp), %rdi
	movq	384(%rsp), %rax
	addq	392(%rsp), %rdi
	testb	$1, %al
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm9, 48(%rsp)         # 16-byte Spill
	movaps	%xmm3, 96(%rsp)         # 16-byte Spill
	movaps	%xmm7, 240(%rsp)        # 16-byte Spill
	movd	%xmm0, 224(%rsp)        # 4-byte Folded Spill
	je	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_6 Depth=1
	movq	(%rdi), %rcx
	movq	-1(%rcx,%rax), %rax
.LBB6_8:                                # %_ZNK12gjkepa2_impl13MinkowskiDiff8Support0ERK9btVector3.exit.i
                                        #   in Loop: Header=BB6_6 Depth=1
.Ltmp6:
	callq	*%rax
	movdqa	%xmm0, 192(%rsp)        # 16-byte Spill
	movaps	%xmm1, 208(%rsp)        # 16-byte Spill
.Ltmp7:
# BB#9:                                 #   in Loop: Header=BB6_6 Depth=1
	movq	952(%rsp), %rax
	movq	(%rax,%rbx,8), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movaps	.LCPI6_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 112(%rsp)
	movlps	%xmm2, 120(%rsp)
.Ltmp9:
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j
.Ltmp10:
# BB#10:                                #   in Loop: Header=BB6_6 Depth=1
	movss	224(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	movaps	192(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm4, %xmm2
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	%xmm5, %xmm4
	movaps	208(%rsp), %xmm6        # 16-byte Reload
	mulss	%xmm5, %xmm6
	movaps	240(%rsp), %xmm7        # 16-byte Reload
	addss	%xmm2, %xmm7
	movaps	96(%rsp), %xmm3         # 16-byte Reload
	addss	%xmm4, %xmm3
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	addss	%xmm6, %xmm4
	movaps	%xmm0, %xmm2
	mulss	%xmm5, %xmm2
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm5, %xmm0
	mulss	%xmm5, %xmm1
	movaps	48(%rsp), %xmm9         # 16-byte Reload
	addss	%xmm2, %xmm9
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	addss	%xmm0, %xmm6
	movaps	16(%rsp), %xmm5         # 16-byte Reload
	addss	%xmm1, %xmm5
	incl	%ebx
	movq	952(%rsp), %rcx
	cmpl	48(%rcx), %ebx
	jb	.LBB6_6
	jmp	.LBB6_11
.LBB6_19:
	leaq	648(%rsp), %r8
.Ltmp3:
	movl	$1, (%rsp)
	leaq	400(%rsp), %rdx
	leaq	128(%rsp), %rcx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%r15, %r9
	callq	_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb
.Ltmp4:
# BB#20:
	testb	%al, %al
	je	.LBB6_26
# BB#21:
	movsd	4(%r15), %xmm2          # xmm2 = mem[0],zero
	movsd	20(%r15), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm0, %xmm2
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	12(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	28(%r15), %xmm3
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB6_23
# BB#22:                                # %call.sqrt397
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	movss	%xmm3, 32(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	32(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB6_23:                               # %.split396
	ucomiss	.LCPI6_1(%rip), %xmm1
	jb	.LBB6_25
# BB#24:
	movss	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm2, 36(%r15)
	movlps	%xmm0, 44(%r15)
.LBB6_25:
	xorps	.LCPI6_3(%rip), %xmm1
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	jmp	.LBB6_26
.LBB6_4:
	xorps	%xmm9, %xmm9
	xorps	%xmm6, %xmm6
	xorps	%xmm5, %xmm5
	xorps	%xmm7, %xmm7
	xorps	%xmm3, %xmm3
	xorps	%xmm4, %xmm4
.LBB6_11:                               # %._crit_edge
	movaps	%xmm7, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	16(%rbp), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	(%rbp), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1]
	mulps	%xmm1, %xmm2
	movaps	%xmm3, %xmm8
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	movss	20(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	mulps	%xmm8, %xmm0
	addps	%xmm2, %xmm0
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	24(%rbp), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	8(%rbp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1]
	mulps	%xmm1, %xmm2
	addps	%xmm0, %xmm2
	movsd	48(%rbp), %xmm0         # xmm0 = mem[0],zero
	addps	%xmm2, %xmm0
	mulss	32(%rbp), %xmm7
	mulss	36(%rbp), %xmm3
	addss	%xmm7, %xmm3
	mulss	40(%rbp), %xmm4
	addss	%xmm3, %xmm4
	addss	56(%rbp), %xmm4
	xorps	%xmm7, %xmm7
	xorps	%xmm1, %xmm1
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movlps	%xmm0, 4(%r15)
	movlps	%xmm1, 12(%r15)
	movaps	%xmm9, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	16(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%rbp), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm4          # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	mulps	%xmm1, %xmm3
	movaps	%xmm6, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	20(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	mulps	%xmm1, %xmm4
	addps	%xmm3, %xmm4
	movaps	%xmm5, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	24(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	8(%rbp), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	mulps	%xmm1, %xmm3
	addps	%xmm4, %xmm3
	movsd	48(%rbp), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm3, %xmm1
	mulss	32(%rbp), %xmm9
	mulss	36(%rbp), %xmm6
	addss	%xmm9, %xmm6
	mulss	40(%rbp), %xmm5
	addss	%xmm6, %xmm5
	addss	56(%rbp), %xmm5
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	movss	%xmm5, %xmm7            # xmm7 = xmm5[0],xmm7[1,2,3]
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movlps	%xmm1, 20(%r15)
	movlps	%xmm7, 28(%r15)
.Ltmp12:
	movq	%r14, %rdi
	callq	_ZNK13btConvexShape19getMarginNonVirtualEv
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
.Ltmp13:
# BB#12:
.Ltmp14:
	leaq	400(%rsp), %rdi
	callq	_ZNK13btConvexShape19getMarginNonVirtualEv
.Ltmp15:
# BB#13:
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	subps	96(%rsp), %xmm4         # 16-byte Folded Reload
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	subss	32(%rsp), %xmm2         # 16-byte Folded Reload
	movss	48(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm5
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm4, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm3, %xmm3
	sqrtss	%xmm0, %xmm3
	ucomiss	%xmm3, %xmm3
	jnp	.LBB6_15
# BB#14:                                # %call.sqrt
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	movss	%xmm5, 48(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	48(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm0, %xmm3
.LBB6_15:                               # %.split
	movss	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm0
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm4
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm4, 36(%r15)
	movlps	%xmm1, 44(%r15)
	mulss	%xmm5, %xmm4
	mulss	%xmm5, %xmm0
	mulss	%xmm5, %xmm2
	addss	4(%r15), %xmm4
	movss	%xmm4, 4(%r15)
	addss	8(%r15), %xmm0
	movss	%xmm0, 8(%r15)
	addss	12(%r15), %xmm2
	movss	%xmm2, 12(%r15)
	subss	%xmm5, %xmm3
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
.LBB6_26:
	leaq	400(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	addq	$968, %rsp              # imm = 0x3C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_31:
.Ltmp5:
	jmp	.LBB6_28
.LBB6_18:
.Ltmp16:
	jmp	.LBB6_28
.LBB6_27:
.Ltmp2:
	jmp	.LBB6_28
.LBB6_17:
.Ltmp11:
	jmp	.LBB6_28
.LBB6_16:
.Ltmp8:
.LBB6_28:
	movq	%rax, %rbx
.Ltmp17:
	leaq	400(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp18:
# BB#29:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_30:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN15btGjkEpaSolver214SignedDistanceERK9btVector3fPK13btConvexShapeRK11btTransformRNS_8sResultsE, .Lfunc_end6-_ZN15btGjkEpaSolver214SignedDistanceERK9btVector3fPK13btConvexShapeRK11btTransformRNS_8sResultsE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp15-.Ltmp12         #   Call between .Ltmp12 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp17-.Ltmp15         #   Call between .Ltmp15 and .Ltmp17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Lfunc_end6-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end7:
	.size	__clang_call_terminate, .Lfunc_end7-__clang_call_terminate

	.text
	.globl	_ZN15btGjkEpaSolver214SignedDistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE
	.p2align	4, 0x90
	.type	_ZN15btGjkEpaSolver214SignedDistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE,@function
_ZN15btGjkEpaSolver214SignedDistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE: # @_ZN15btGjkEpaSolver214SignedDistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 64
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	callq	_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE
	testb	%al, %al
	je	.LBB8_1
# BB#2:
	movb	$1, %al
	jmp	.LBB8_3
.LBB8_1:
	movl	$0, (%rsp)
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	callq	_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb
.LBB8_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN15btGjkEpaSolver214SignedDistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE, .Lfunc_end8-_ZN15btGjkEpaSolver214SignedDistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	3212836864              # float -1
.LCPI9_2:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj,"axG",@progbits,_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj,comdat
	.weak	_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj
	.p2align	4, 0x90
	.type	_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj,@function
_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj: # @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi81:
	.cfi_def_cfa_offset 304
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, 112(%rsp)
	movq	%r15, 120(%rsp)
	movq	%r14, 128(%rsp)
	movsd	(%rdi), %xmm10          # xmm10 = mem[0],zero
	movsd	(%r15), %xmm2           # xmm2 = mem[0],zero
	movaps	%xmm10, %xmm5
	subps	%xmm2, %xmm5
	movss	8(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm6
	subss	%xmm4, %xmm6
	movaps	%xmm5, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	xorps	%xmm0, %xmm0
	xorps	%xmm3, %xmm3
	movss	%xmm6, %xmm3            # xmm3 = xmm6[0],xmm3[1,2,3]
	movlps	%xmm5, 144(%rsp)
	movlps	%xmm3, 152(%rsp)
	movsd	(%r14), %xmm8           # xmm8 = mem[0],zero
	subps	%xmm8, %xmm2
	movss	8(%r14), %xmm9          # xmm9 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm4
	movaps	%xmm2, %xmm11
	shufps	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm2, 160(%rsp)
	movlps	%xmm3, 168(%rsp)
	subps	%xmm10, %xmm8
	subss	%xmm1, %xmm9
	movss	%xmm9, %xmm0            # xmm0 = xmm9[0],xmm0[1,2,3]
	movlps	%xmm8, 176(%rsp)
	movlps	%xmm0, 184(%rsp)
	movaps	%xmm7, %xmm13
	mulss	%xmm4, %xmm13
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	subss	%xmm0, %xmm13
	movaps	%xmm6, %xmm14
	mulss	%xmm2, %xmm14
	movaps	%xmm4, %xmm0
	mulss	%xmm5, %xmm0
	subss	%xmm0, %xmm14
	movaps	%xmm11, %xmm12
	mulss	%xmm5, %xmm12
	movaps	%xmm7, %xmm0
	mulss	%xmm2, %xmm0
	subss	%xmm0, %xmm12
	movaps	%xmm13, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm14, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm12, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB9_1
# BB#2:                                 # %.preheader.preheader
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movaps	%xmm11, 192(%rsp)       # 16-byte Spill
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	movss	%xmm4, 64(%rsp)         # 4-byte Spill
	movss	%xmm9, 44(%rsp)         # 4-byte Spill
	movaps	%xmm8, 224(%rsp)        # 16-byte Spill
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	movss	.LCPI9_0(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	xorl	%r11d, %r11d
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	xorps	%xmm4, %xmm4
	movss	%xmm14, 12(%rsp)        # 4-byte Spill
	jmp	.LBB9_3
	.p2align	4, 0x90
.LBB9_19:                               # %..preheader_crit_edge
                                        #   in Loop: Header=BB9_3 Depth=1
	movss	164(%rsp,%rsi,4), %xmm7 # xmm7 = mem[0],zero,zero,zero
	movss	168(%rsp,%rsi,4), %xmm6 # xmm6 = mem[0],zero,zero,zero
	movss	160(%rsp,%rsi,4), %xmm5 # xmm5 = mem[0],zero,zero,zero
	addq	$4, %rsi
	incl	%r11d
	movaps	%xmm0, %xmm11
.LBB9_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	112(%rsp,%rsi,2), %rcx
	movaps	%xmm12, %xmm0
	mulss	%xmm7, %xmm0
	movaps	%xmm14, %xmm1
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm0
	mulss	%xmm13, %xmm6
	movaps	%xmm12, %xmm1
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm14, %xmm5
	mulss	%xmm13, %xmm7
	subss	%xmm7, %xmm5
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	mulss	%xmm3, %xmm6
	addss	%xmm0, %xmm6
	movss	8(%rcx), %xmm15         # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm5
	addss	%xmm6, %xmm5
	ucomiss	%xmm4, %xmm5
	jbe	.LBB9_4
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=1
	movaps	%xmm13, %xmm9
	movaps	%xmm12, %xmm8
	movl	_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3(%rsi), %edx
	movq	112(%rsp,%rdx,8), %rcx
	movss	(%rcx), %xmm13          # xmm13 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm5
	subss	%xmm2, %xmm5
	movaps	%xmm14, %xmm7
	subss	%xmm3, %xmm7
	movss	8(%rcx), %xmm12         # xmm12 = mem[0],zero,zero,zero
	xorps	%xmm10, %xmm10
	movaps	%xmm12, %xmm4
	subss	%xmm15, %xmm4
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm7, %xmm6
	mulss	%xmm6, %xmm6
	addss	%xmm0, %xmm6
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm6, %xmm1
	ucomiss	%xmm10, %xmm1
	movss	.LCPI9_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jbe	.LBB9_13
# BB#6:                                 #   in Loop: Header=BB9_3 Depth=1
	xorps	%xmm10, %xmm10
	movaps	%xmm2, %xmm0
	mulss	%xmm5, %xmm0
	movaps	%xmm3, %xmm6
	mulss	%xmm7, %xmm6
	addss	%xmm0, %xmm6
	movaps	%xmm15, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm6, %xmm0
	xorps	.LCPI9_1(%rip), %xmm0
	divss	%xmm1, %xmm0
	ucomiss	.LCPI9_2(%rip), %xmm0
	jae	.LBB9_7
# BB#8:                                 #   in Loop: Header=BB9_3 Depth=1
	ucomiss	%xmm0, %xmm10
	jae	.LBB9_9
# BB#10:                                #   in Loop: Header=BB9_3 Depth=1
	movd	%xmm0, %r8d
	movss	.LCPI9_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movd	%xmm1, %r9d
	mulss	%xmm0, %xmm5
	mulss	%xmm0, %xmm7
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm2
	addss	%xmm7, %xmm3
	addss	%xmm4, %xmm15
	mulss	%xmm2, %xmm2
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm15, %xmm15
	addss	%xmm3, %xmm15
	movl	$3, %r10d
	jmp	.LBB9_11
	.p2align	4, 0x90
.LBB9_4:                                #   in Loop: Header=BB9_3 Depth=1
	movaps	%xmm11, %xmm0
	cmpq	$8, %rsi
	jne	.LBB9_19
	jmp	.LBB9_20
.LBB9_7:                                #   in Loop: Header=BB9_3 Depth=1
	mulss	%xmm13, %xmm13
	mulss	%xmm14, %xmm14
	addss	%xmm13, %xmm14
	mulss	%xmm12, %xmm12
	addss	%xmm14, %xmm12
	movl	$1065353216, %r8d       # imm = 0x3F800000
	xorl	%r9d, %r9d
	movl	$2, %r10d
	movaps	%xmm12, %xmm0
	jmp	.LBB9_12
.LBB9_9:                                #   in Loop: Header=BB9_3 Depth=1
	mulss	%xmm2, %xmm2
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm15, %xmm15
	addss	%xmm3, %xmm15
	xorl	%r8d, %r8d
	movl	$1065353216, %r9d       # imm = 0x3F800000
	movl	$1, %r10d
.LBB9_11:                               # %_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	movaps	%xmm15, %xmm0
.LBB9_12:                               # %_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	xorps	%xmm10, %xmm10
.LBB9_13:                               # %_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	ucomiss	%xmm11, %xmm10
	xorps	%xmm4, %xmm4
	ja	.LBB9_16
# BB#14:                                # %_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	ucomiss	%xmm0, %xmm11
	ja	.LBB9_16
# BB#15:                                #   in Loop: Header=BB9_3 Depth=1
	movaps	%xmm11, %xmm0
	jmp	.LBB9_17
	.p2align	4, 0x90
.LBB9_16:                               #   in Loop: Header=BB9_3 Depth=1
	movl	%r10d, %ebx
	andl	$1, %ebx
	movl	$1, %eax
	movl	%r11d, %ecx
	shll	%cl, %eax
	testl	%ebx, %ebx
	cmovel	%ebx, %eax
	movl	%r10d, %ebx
	andl	$2, %ebx
	movl	$1, %ebp
	movl	%edx, %ecx
	shll	%cl, %ebp
	testl	%ebx, %ebx
	cmovel	%ebx, %ebp
	addl	%eax, %ebp
	movl	%ebp, (%r12)
	movl	%r9d, (%r13,%rsi)
	movl	%r8d, (%r13,%rdx,4)
	movl	_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3(,%rdx,4), %eax
	movl	$0, (%r13,%rax,4)
.LBB9_17:                               #   in Loop: Header=BB9_3 Depth=1
	movaps	%xmm8, %xmm12
	movaps	%xmm9, %xmm13
	movss	12(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	cmpq	$8, %rsi
	jne	.LBB9_19
.LBB9_20:
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_28
# BB#21:
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm0
	movss	4(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm2
	addss	%xmm1, %xmm2
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	xorps	%xmm8, %xmm8
	sqrtss	%xmm0, %xmm8
	ucomiss	%xmm8, %xmm8
	jnp	.LBB9_23
# BB#22:                                # %call.sqrt
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	movaps	%xmm13, 80(%rsp)        # 16-byte Spill
	movss	%xmm2, 40(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	40(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm13        # 16-byte Reload
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	%xmm0, %xmm8
	movaps	16(%rsp), %xmm0         # 16-byte Reload
.LBB9_23:                               # %.split
	divss	%xmm0, %xmm2
	mulss	%xmm2, %xmm13
	mulss	%xmm2, %xmm14
	mulss	%xmm2, %xmm12
	movaps	%xmm13, %xmm9
	mulss	%xmm9, %xmm9
	movaps	%xmm14, %xmm7
	mulss	%xmm7, %xmm7
	movl	$7, (%r12)
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	%xmm13, %xmm1
	subss	%xmm14, %xmm0
	movss	8(%r15), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	%xmm12, %xmm2
	movaps	192(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm6, %xmm3
	mulss	%xmm2, %xmm3
	movaps	%xmm0, %xmm4
	movss	64(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm4
	subss	%xmm4, %xmm3
	mulss	%xmm1, %xmm5
	movaps	96(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm4, %xmm2
	subss	%xmm2, %xmm5
	mulss	%xmm4, %xmm0
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm0
	mulss	%xmm3, %xmm3
	mulss	%xmm5, %xmm5
	addss	%xmm3, %xmm5
	mulss	%xmm0, %xmm0
	addss	%xmm5, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB9_25
# BB#24:                                # %call.sqrt195
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	movaps	%xmm13, 80(%rsp)        # 16-byte Spill
	movss	%xmm14, 12(%rsp)        # 4-byte Spill
	movss	%xmm8, 16(%rsp)         # 4-byte Spill
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	movss	%xmm7, 96(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	96(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm9         # 16-byte Reload
	movss	16(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm13        # 16-byte Reload
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB9_25:                               # %.split.split
	addss	%xmm7, %xmm9
	divss	%xmm8, %xmm1
	movss	%xmm1, (%r13)
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	%xmm13, %xmm1
	subss	%xmm14, %xmm0
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	%xmm12, %xmm2
	mulss	%xmm12, %xmm12
	movaps	208(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm6, %xmm3
	mulss	%xmm2, %xmm3
	movaps	%xmm0, %xmm4
	movss	44(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm4
	subss	%xmm4, %xmm3
	mulss	%xmm1, %xmm5
	movaps	224(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm4, %xmm2
	subss	%xmm2, %xmm5
	mulss	%xmm4, %xmm0
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm0
	mulss	%xmm3, %xmm3
	mulss	%xmm5, %xmm5
	addss	%xmm3, %xmm5
	mulss	%xmm0, %xmm0
	addss	%xmm5, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB9_27
# BB#26:                                # %call.sqrt196
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	movss	%xmm8, 16(%rsp)         # 4-byte Spill
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	64(%rsp), %xmm9         # 16-byte Reload
	movss	16(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB9_27:                               # %.split.split.split
	addss	%xmm9, %xmm12
	divss	%xmm8, %xmm1
	movss	%xmm1, 4(%r13)
	addss	(%r13), %xmm1
	movss	.LCPI9_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	%xmm0, 8(%r13)
	movaps	%xmm12, %xmm0
	jmp	.LBB9_28
.LBB9_1:
	movss	.LCPI9_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB9_28:
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj, .Lfunc_end9-_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	3212836864              # float -1
.LCPI10_1:
	.long	1065353216              # float 1
	.section	.text._ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj,"axG",@progbits,_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj,comdat
	.weak	_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj
	.p2align	4, 0x90
	.type	_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj,@function
_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj: # @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi94:
	.cfi_def_cfa_offset 224
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r8, %r14
	movq	%rcx, %rbx
	movq	%rdi, 112(%rsp)
	movq	%rsi, 120(%rsp)
	movq	%rdx, 128(%rsp)
	movq	%rbx, 136(%rsp)
	movsd	(%rdi), %xmm14          # xmm14 = mem[0],zero
	movsd	(%rbx), %xmm6           # xmm6 = mem[0],zero
	subps	%xmm6, %xmm14
	movss	8(%rdi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm12
	movaps	%xmm14, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm11, %xmm11
	xorps	%xmm0, %xmm0
	movss	%xmm12, %xmm0           # xmm0 = xmm12[0],xmm0[1,2,3]
	movlps	%xmm14, 32(%rsp)
	movlps	%xmm0, 40(%rsp)
	movsd	(%rsi), %xmm9           # xmm9 = mem[0],zero
	movaps	%xmm9, %xmm5
	subps	%xmm6, %xmm5
	movss	8(%rsi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm0
	subss	%xmm3, %xmm0
	movaps	%xmm5, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	xorps	%xmm4, %xmm4
	movss	%xmm0, %xmm4            # xmm4 = xmm0[0],xmm4[1,2,3]
	movlps	%xmm5, 48(%rsp)
	movlps	%xmm4, 56(%rsp)
	movsd	(%rdx), %xmm10          # xmm10 = mem[0],zero
	movaps	%xmm10, %xmm4
	subps	%xmm6, %xmm4
	movss	8(%rdx), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm6
	subss	%xmm3, %xmm6
	movlps	%xmm4, 64(%rsp)
	movaps	%xmm12, %xmm3
	mulss	%xmm5, %xmm3
	mulss	%xmm1, %xmm5
	mulss	%xmm0, %xmm1
	mulss	%xmm4, %xmm1
	mulss	%xmm14, %xmm0
	mulss	%xmm7, %xmm14
	mulss	%xmm12, %xmm7
	mulss	%xmm4, %xmm7
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movss	%xmm6, %xmm11           # xmm11 = xmm6[0],xmm11[1,2,3]
	movlps	%xmm11, 72(%rsp)
	mulss	%xmm4, %xmm3
	addss	%xmm1, %xmm3
	mulss	%xmm4, %xmm0
	subss	%xmm0, %xmm3
	mulss	%xmm6, %xmm5
	subss	%xmm5, %xmm3
	mulss	%xmm6, %xmm14
	addss	%xmm3, %xmm14
	subss	%xmm7, %xmm14
	xorps	%xmm11, %xmm11
	ucomiss	%xmm11, %xmm14
	movss	.LCPI10_0(%rip), %xmm12 # xmm12 = mem[0],zero,zero,zero
	je	.LBB10_10
# BB#1:
	movaps	%xmm9, %xmm3
	subss	%xmm10, %xmm3
	pshufd	$229, %xmm9, %xmm5      # xmm5 = xmm9[1,1,2,3]
	pshufd	$229, %xmm10, %xmm6     # xmm6 = xmm10[1,1,2,3]
	movss	(%rdi), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm1
	subss	%xmm5, %xmm1
	subss	%xmm6, %xmm5
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm6
	subss	%xmm13, %xmm6
	subss	%xmm8, %xmm13
	movaps	%xmm5, %xmm4
	mulss	%xmm6, %xmm4
	movaps	%xmm13, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm4
	mulss	%xmm7, %xmm4
	subss	%xmm9, %xmm7
	mulss	%xmm7, %xmm13
	mulss	%xmm3, %xmm6
	subss	%xmm6, %xmm13
	mulss	%xmm3, %xmm1
	mulss	%xmm7, %xmm5
	subss	%xmm5, %xmm1
	mulss	%xmm10, %xmm13
	addss	%xmm4, %xmm13
	mulss	%xmm0, %xmm1
	addss	%xmm13, %xmm1
	mulss	%xmm14, %xmm1
	ucomiss	%xmm1, %xmm11
	jb	.LBB10_10
# BB#2:
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movl	$0, 16(%rsp)
	movq	$0, 8(%rsp)
	movl	$0, 4(%rsp)
	movss	.LCPI10_0(%rip), %xmm12 # xmm12 = mem[0],zero,zero,zero
	xorl	%r12d, %r12d
	xorps	%xmm7, %xmm7
	leaq	4(%rsp), %rbp
	xorl	%r13d, %r13d
	movaps	%xmm14, 144(%rsp)       # 16-byte Spill
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	movl	_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3(%r13), %r15d
	movss	36(%rsp,%r13,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movq	%r15, %rax
	shlq	$4, %rax
	movss	40(%rsp,%rax), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	mulss	%xmm1, %xmm2
	movss	40(%rsp,%r13,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movss	32(%rsp,%rax), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movss	36(%rsp,%rax), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm6
	mulss	%xmm5, %xmm6
	subss	%xmm6, %xmm2
	mulss	%xmm4, %xmm3
	movss	32(%rsp,%r13,4), %xmm6  # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm3
	mulss	%xmm6, %xmm5
	mulss	%xmm4, %xmm0
	subss	%xmm0, %xmm5
	mulss	(%rbx), %xmm2
	mulss	4(%rbx), %xmm3
	addss	%xmm2, %xmm3
	mulss	8(%rbx), %xmm5
	addss	%xmm3, %xmm5
	mulss	%xmm14, %xmm5
	ucomiss	%xmm7, %xmm5
	jbe	.LBB10_7
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=1
	movq	112(%rsp,%r13,2), %rdi
	movq	112(%rsp,%r15,8), %rsi
	movq	%rbx, %rdx
	leaq	8(%rsp), %rcx
	movq	%rbp, %r8
	movss	%xmm12, 20(%rsp)        # 4-byte Spill
	callq	_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj
	xorps	%xmm7, %xmm7
	movss	20(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movaps	144(%rsp), %xmm14       # 16-byte Reload
	ucomiss	%xmm12, %xmm7
	ja	.LBB10_6
# BB#5:                                 #   in Loop: Header=BB10_3 Depth=1
	ucomiss	%xmm0, %xmm12
	jbe	.LBB10_7
.LBB10_6:                               #   in Loop: Header=BB10_3 Depth=1
	movl	4(%rsp), %eax
	movl	%eax, %edx
	andl	$1, %edx
	movl	$1, %esi
	movl	%r12d, %ecx
	shll	%cl, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	movl	%eax, %edx
	andl	$2, %edx
	movl	$1, %edi
	movl	%r15d, %ecx
	shll	%cl, %edi
	testl	%edx, %edx
	cmovel	%edx, %edi
	andl	$4, %eax
	leal	(%rdi,%rax,2), %eax
	addl	%esi, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	8(%rsp), %eax
	movl	%eax, (%r14,%r13)
	movl	12(%rsp), %eax
	movl	%eax, (%r14,%r15,4)
	movl	_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3(,%r15,4), %eax
	movl	$0, (%r14,%rax,4)
	movl	16(%rsp), %eax
	movl	%eax, 12(%r14)
	movaps	%xmm0, %xmm12
.LBB10_7:                               #   in Loop: Header=BB10_3 Depth=1
	addq	$4, %r13
	incl	%r12d
	cmpq	$12, %r13
	jne	.LBB10_3
# BB#8:
	xorps	%xmm0, %xmm0
	ucomiss	%xmm12, %xmm0
	jbe	.LBB10_10
# BB#9:
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	$15, (%rax)
	movq	88(%rsp), %rdx          # 8-byte Reload
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movq	96(%rsp), %rcx          # 8-byte Reload
	movss	8(%rcx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm5
	mulss	%xmm4, %xmm5
	movss	(%rbx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm5
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm7, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm5, %xmm2
	mulss	%xmm0, %xmm4
	mulss	%xmm6, %xmm4
	subss	%xmm4, %xmm2
	mulss	%xmm7, %xmm3
	movss	8(%rbx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	subss	%xmm3, %xmm2
	mulss	%xmm9, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm9, %xmm1
	mulss	%xmm8, %xmm1
	subss	%xmm1, %xmm0
	divss	%xmm14, %xmm0
	movss	%xmm0, (%r14)
	movq	104(%rsp), %rax         # 8-byte Reload
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm6
	mulss	%xmm5, %xmm6
	movss	(%rbx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm6
	movss	8(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	mulss	%xmm10, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm6, %xmm3
	mulss	%xmm1, %xmm5
	mulss	%xmm7, %xmm5
	subss	%xmm5, %xmm3
	mulss	%xmm10, %xmm4
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm4
	subss	%xmm4, %xmm3
	mulss	%xmm9, %xmm1
	mulss	%xmm5, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm9, %xmm2
	mulss	%xmm8, %xmm2
	subss	%xmm2, %xmm1
	divss	%xmm14, %xmm1
	movss	%xmm1, 4(%r14)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm7
	mulss	%xmm6, %xmm7
	movss	(%rbx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm11         # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm7
	movss	8(%rcx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	(%rax), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	mulss	%xmm9, %xmm4
	mulss	%xmm11, %xmm4
	addss	%xmm7, %xmm4
	mulss	%xmm2, %xmm6
	mulss	%xmm11, %xmm6
	subss	%xmm6, %xmm4
	mulss	%xmm9, %xmm5
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm5
	subss	%xmm5, %xmm4
	mulss	%xmm10, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm4, %xmm2
	mulss	%xmm10, %xmm3
	mulss	%xmm8, %xmm3
	subss	%xmm3, %xmm2
	divss	%xmm14, %xmm2
	movss	%xmm2, 8(%r14)
	addss	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI10_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	%xmm0, 12(%r14)
	xorps	%xmm12, %xmm12
.LBB10_10:
	movaps	%xmm12, %xmm0
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj, .Lfunc_end10-_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE,"axG",@progbits,_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE,comdat
	.weak	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	.p2align	4, 0x90
	.type	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE,@function
_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE: # @_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi104:
	.cfi_def_cfa_offset 80
.Lcfi105:
	.cfi_offset %rbx, -32
.Lcfi106:
	.cfi_offset %r14, -24
.Lcfi107:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB11_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB11_2:                               # %.split
	movss	.LCPI11_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movsd	(%r15), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	8(%r15), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, (%r14)
	movlps	%xmm1, 8(%r14)
	movq	(%rbx), %rdi
	movq	128(%rbx), %rax
	addq	136(%rbx), %rdi
	testb	$1, %al
	je	.LBB11_4
# BB#3:
	movq	(%rdi), %rcx
	movq	-1(%rcx,%rax), %rax
.LBB11_4:                               # %_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3.exit
	movq	%r14, %rsi
	callq	*%rax
	movss	(%r14), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	.LCPI11_1(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm3, %xmm7
	xorps	%xmm3, %xmm6
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm2
	movq	8(%rbx), %rdi
	movq	128(%rbx), %rax
	addq	136(%rbx), %rdi
	testb	$1, %al
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	je	.LBB11_6
# BB#5:
	movq	(%rdi), %rcx
	movq	-1(%rcx,%rax), %rax
.LBB11_6:                               # %_ZNK12gjkepa2_impl13MinkowskiDiff8Support1ERK9btVector3.exit
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	36(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm6, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	mulss	48(%rbx), %xmm7
	mulss	52(%rbx), %xmm6
	addss	%xmm7, %xmm6
	mulss	56(%rbx), %xmm2
	addss	%xmm6, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm3, (%rsp)
	movlps	%xmm0, 8(%rsp)
	movq	%rsp, %rsi
	callq	*%rax
	movaps	%xmm0, %xmm2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	80(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	84(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm0, %xmm6
	addps	%xmm2, %xmm6
	movss	104(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	88(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm1, %xmm5
	addps	%xmm6, %xmm5
	movsd	112(%rbx), %xmm1        # xmm1 = mem[0],zero
	addps	%xmm5, %xmm1
	movss	100(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm3, %xmm4
	addss	%xmm4, %xmm2
	addss	120(%rbx), %xmm2
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	subps	%xmm1, %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	subss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 16(%r14)
	movlps	%xmm2, 24(%r14)
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE, .Lfunc_end11-_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI12_1:
	.zero	16
	.section	.text._ZN12gjkepa2_impl3GJK13EncloseOriginEv,"axG",@progbits,_ZN12gjkepa2_impl3GJK13EncloseOriginEv,comdat
	.weak	_ZN12gjkepa2_impl3GJK13EncloseOriginEv
	.p2align	4, 0x90
	.type	_ZN12gjkepa2_impl3GJK13EncloseOriginEv,@function
_ZN12gjkepa2_impl3GJK13EncloseOriginEv: # @_ZN12gjkepa2_impl3GJK13EncloseOriginEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 40
	subq	$104, %rsp
.Lcfi112:
	.cfi_def_cfa_offset 144
.Lcfi113:
	.cfi_offset %rbx, -40
.Lcfi114:
	.cfi_offset %r12, -32
.Lcfi115:
	.cfi_offset %r14, -24
.Lcfi116:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	448(%rbx), %rax
	movl	48(%rax), %ecx
	decl	%ecx
	cmpl	$3, %ecx
	ja	.LBB12_18
# BB#1:
	jmpq	*.LJTI12_0(,%rcx,8)
.LBB12_2:                               # %.preheader
	movl	$1, %ecx
	movl	$1, %r12d
	movq	%rsp, %r14
	leaq	16(%rsp), %r15
	jmp	.LBB12_4
	.p2align	4, 0x90
.LBB12_3:                               # %._crit_edge
                                        #   in Loop: Header=BB12_4 Depth=1
	movq	448(%rbx), %rax
	movl	48(%rax), %ecx
	incq	%r12
.LBB12_4:                               # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$1065353216, -4(%rsp,%r12,4) # imm = 0x3F800000
	movl	%ecx, %ecx
	movl	$0, 32(%rax,%rcx,4)
	movl	440(%rbx), %ecx
	decl	%ecx
	movl	%ecx, 440(%rbx)
	movq	408(%rbx,%rcx,8), %rcx
	movl	48(%rax), %edx
	movq	%rcx, (%rax,%rdx,8)
	movl	48(%rax), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 48(%rax)
	movq	(%rax,%rcx,8), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	movq	%rbx, %rdi
	callq	_ZN12gjkepa2_impl3GJK13EncloseOriginEv
	testb	%al, %al
	jne	.LBB12_16
# BB#5:                                 #   in Loop: Header=BB12_4 Depth=1
	movq	448(%rbx), %rax
	movl	48(%rax), %ecx
	decl	%ecx
	movl	%ecx, 48(%rax)
	movq	(%rax,%rcx,8), %rax
	movl	440(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 440(%rbx)
	movq	%rax, 408(%rbx,%rcx,8)
	movq	448(%rbx), %rax
	movaps	(%rsp), %xmm0
	movaps	.LCPI12_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	8(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	movl	48(%rax), %ecx
	movl	$0, 32(%rax,%rcx,4)
	movl	440(%rbx), %ecx
	decl	%ecx
	movl	%ecx, 440(%rbx)
	movq	408(%rbx,%rcx,8), %rcx
	movl	48(%rax), %edx
	movq	%rcx, (%rax,%rdx,8)
	movl	48(%rax), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 48(%rax)
	movq	(%rax,%rcx,8), %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	movq	%rbx, %rdi
	callq	_ZN12gjkepa2_impl3GJK13EncloseOriginEv
	testb	%al, %al
	jne	.LBB12_16
# BB#6:                                 #   in Loop: Header=BB12_4 Depth=1
	movq	448(%rbx), %rax
	movl	48(%rax), %ecx
	decl	%ecx
	movl	%ecx, 48(%rax)
	movq	(%rax,%rcx,8), %rax
	movl	440(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 440(%rbx)
	movq	%rax, 408(%rbx,%rcx,8)
	cmpq	$2, %r12
	jbe	.LBB12_3
	jmp	.LBB12_18
.LBB12_7:
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movss	16(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	16(%rcx), %xmm4
	movsd	20(%rax), %xmm5         # xmm5 = mem[0],zero
	movsd	20(%rcx), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm0, %xmm5
	movaps	%xmm5, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm4, %xmm6
	shufps	$0, %xmm0, %xmm6        # xmm6 = xmm6[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm6      # xmm6 = xmm6[2,0],xmm0[2,3]
	xorl	%r12d, %r12d
	xorps	%xmm8, %xmm8
	xorps	%xmm7, %xmm7
	leaq	16(%rsp), %r14
	leaq	32(%rsp), %r15
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	movaps	%xmm6, 48(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB12_8:                               # =>This Inner Loop Header: Depth=1
	movaps	%xmm8, (%rsp)
	movl	$1065353216, (%rsp,%r12,4) # imm = 0x3F800000
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm5, %xmm1
	movsd	4(%rsp), %xmm2          # xmm2 = mem[0],zero
	mulps	%xmm6, %xmm2
	subps	%xmm2, %xmm1
	movss	4(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	mulss	%xmm5, %xmm0
	subss	%xmm0, %xmm2
	movaps	%xmm1, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm1, 16(%rsp)
	movlps	%xmm3, 24(%rsp)
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	ucomiss	%xmm7, %xmm2
	jbe	.LBB12_12
# BB#9:                                 #   in Loop: Header=BB12_8 Depth=1
	movq	448(%rbx), %rax
	movl	48(%rax), %ecx
	movl	$0, 32(%rax,%rcx,4)
	movl	440(%rbx), %ecx
	decl	%ecx
	movl	%ecx, 440(%rbx)
	movq	408(%rbx,%rcx,8), %rcx
	movl	48(%rax), %edx
	movq	%rcx, (%rax,%rdx,8)
	movl	48(%rax), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 48(%rax)
	movq	(%rax,%rcx,8), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	movq	%rbx, %rdi
	callq	_ZN12gjkepa2_impl3GJK13EncloseOriginEv
	testb	%al, %al
	jne	.LBB12_16
# BB#10:                                #   in Loop: Header=BB12_8 Depth=1
	movq	448(%rbx), %rax
	movl	48(%rax), %ecx
	decl	%ecx
	movl	%ecx, 48(%rax)
	movq	(%rax,%rcx,8), %rax
	movl	440(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 440(%rbx)
	movq	%rax, 408(%rbx,%rcx,8)
	movq	448(%rbx), %rax
	movsd	16(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	.LCPI12_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	24(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 32(%rsp)
	movlps	%xmm2, 40(%rsp)
	movl	48(%rax), %ecx
	movl	$0, 32(%rax,%rcx,4)
	movl	440(%rbx), %ecx
	decl	%ecx
	movl	%ecx, 440(%rbx)
	movq	408(%rbx,%rcx,8), %rcx
	movl	48(%rax), %edx
	movq	%rcx, (%rax,%rdx,8)
	movl	48(%rax), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 48(%rax)
	movq	(%rax,%rcx,8), %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	movq	%rbx, %rdi
	callq	_ZN12gjkepa2_impl3GJK13EncloseOriginEv
	testb	%al, %al
	jne	.LBB12_16
# BB#11:                                #   in Loop: Header=BB12_8 Depth=1
	movq	448(%rbx), %rax
	movl	48(%rax), %ecx
	decl	%ecx
	movl	%ecx, 48(%rax)
	movq	(%rax,%rcx,8), %rax
	movl	440(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 440(%rbx)
	movq	%rax, 408(%rbx,%rcx,8)
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	movaps	64(%rsp), %xmm5         # 16-byte Reload
	movaps	48(%rsp), %xmm6         # 16-byte Reload
	xorps	%xmm8, %xmm8
	xorps	%xmm7, %xmm7
.LBB12_12:                              #   in Loop: Header=BB12_8 Depth=1
	incq	%r12
	cmpq	$3, %r12
	jb	.LBB12_8
	jmp	.LBB12_18
.LBB12_13:
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movss	16(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movsd	20(%rdx), %xmm1         # xmm1 = mem[0],zero
	movsd	20(%rcx), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm1
	movq	16(%rax), %rcx
	movss	16(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	24(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm6
	movaps	%xmm1, %xmm3
	mulps	%xmm6, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm0
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm1, %xmm6
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	subss	%xmm6, %xmm0
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, (%rsp)
	movlps	%xmm2, 8(%rsp)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB12_18
# BB#14:
	movl	$0, 44(%rax)
	movl	440(%rbx), %ecx
	decl	%ecx
	movl	%ecx, 440(%rbx)
	movq	408(%rbx,%rcx,8), %rcx
	movl	48(%rax), %edx
	movq	%rcx, (%rax,%rdx,8)
	movl	48(%rax), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 48(%rax)
	movq	(%rax,%rcx,8), %rdx
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	movq	%rbx, %rdi
	callq	_ZN12gjkepa2_impl3GJK13EncloseOriginEv
	testb	%al, %al
	jne	.LBB12_16
# BB#15:
	movq	448(%rbx), %rax
	movl	48(%rax), %ecx
	decl	%ecx
	movl	%ecx, 48(%rax)
	movq	(%rax,%rcx,8), %rax
	movl	440(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 440(%rbx)
	movq	%rax, 408(%rbx,%rcx,8)
	movq	448(%rbx), %rax
	movsd	(%rsp), %xmm0           # xmm0 = mem[0],zero
	movaps	.LCPI12_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm0
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	movl	48(%rax), %ecx
	movl	$0, 32(%rax,%rcx,4)
	movl	440(%rbx), %ecx
	decl	%ecx
	movl	%ecx, 440(%rbx)
	movq	408(%rbx,%rcx,8), %rcx
	movl	48(%rax), %edx
	movq	%rcx, (%rax,%rdx,8)
	movl	48(%rax), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 48(%rax)
	movq	(%rax,%rcx,8), %rdx
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE
	movq	%rbx, %rdi
	callq	_ZN12gjkepa2_impl3GJK13EncloseOriginEv
	testb	%al, %al
	je	.LBB12_20
.LBB12_16:                              # %.critedge38
	movb	$1, %al
	jmp	.LBB12_19
.LBB12_17:
	movq	24(%rax), %rcx
	movq	(%rax), %rdx
	movq	8(%rax), %rsi
	movss	16(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rdx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm0
	subss	%xmm1, %xmm9
	movss	24(%rdx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm8
	movss	16(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm4
	subss	%xmm1, %xmm2
	movss	24(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm6
	movq	16(%rax), %rax
	movss	16(%rax), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	20(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm10
	subss	%xmm1, %xmm5
	movss	24(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	movaps	%xmm8, %xmm3
	mulss	%xmm4, %xmm3
	mulss	%xmm9, %xmm4
	mulss	%xmm6, %xmm9
	mulss	%xmm10, %xmm9
	mulss	%xmm5, %xmm3
	addss	%xmm9, %xmm3
	mulss	%xmm0, %xmm6
	mulss	%xmm5, %xmm6
	subss	%xmm6, %xmm3
	mulss	%xmm1, %xmm4
	subss	%xmm4, %xmm3
	mulss	%xmm2, %xmm0
	mulss	%xmm1, %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm8, %xmm2
	mulss	%xmm10, %xmm2
	subss	%xmm2, %xmm0
	movb	$1, %al
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB12_19
	jmp	.LBB12_18
.LBB12_20:
	movq	448(%rbx), %rax
	movl	48(%rax), %ecx
	decl	%ecx
	movl	%ecx, 48(%rax)
	movq	(%rax,%rcx,8), %rax
	movl	440(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 440(%rbx)
	movq	%rax, 408(%rbx,%rcx,8)
.LBB12_18:                              # %.thread126
	xorl	%eax, %eax
.LBB12_19:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	_ZN12gjkepa2_impl3GJK13EncloseOriginEv, .Lfunc_end12-_ZN12gjkepa2_impl3GJK13EncloseOriginEv
	.cfi_endproc
	.section	.rodata._ZN12gjkepa2_impl3GJK13EncloseOriginEv,"aG",@progbits,_ZN12gjkepa2_impl3GJK13EncloseOriginEv,comdat
	.p2align	3
.LJTI12_0:
	.quad	.LBB12_2
	.quad	.LBB12_7
	.quad	.LBB12_13
	.quad	.LBB12_17

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_0:
	.long	953267991               # float 9.99999974E-5
.LCPI13_1:
	.long	1065353216              # float 1
.LCPI13_2:
	.long	3156465418              # float -0.00999999977
.LCPI13_3:
	.long	3072837036              # float -9.99999974E-6
	.section	.text._ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b,"axG",@progbits,_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b,comdat
	.weak	_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b
	.p2align	4, 0x90
	.type	_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b,@function
_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b: # @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi121:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi123:
	.cfi_def_cfa_offset 112
.Lcfi124:
	.cfi_offset %rbx, -56
.Lcfi125:
	.cfi_offset %r12, -48
.Lcfi126:
	.cfi_offset %r13, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movq	%rcx, %rbp
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %r14
	movq	14448(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB13_23
# BB#1:
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.LBB13_3
# BB#2:
	movq	72(%rbx), %rcx
	movq	%rcx, 72(%rax)
.LBB13_3:
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.LBB13_5
# BB#4:
	movq	80(%rbx), %rcx
	movq	%rcx, 80(%rax)
.LBB13_5:
	cmpq	%rbx, 14448(%r14)
	jne	.LBB13_7
# BB#6:
	movq	80(%rbx), %rax
	movq	%rax, 14448(%r14)
.LBB13_7:                               # %_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE.exit
	decl	14456(%r14)
	movq	$0, 72(%rbx)
	movq	14432(%r14), %rax
	movq	%rax, 80(%rbx)
	movq	14432(%r14), %rax
	testq	%rax, %rax
	je	.LBB13_9
# BB#8:
	movq	%rbx, 72(%rax)
.LBB13_9:                               # %_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE.exit80
	movq	%rbx, 14432(%r14)
	incl	14440(%r14)
	movb	$0, 91(%rbx)
	movq	%r12, 24(%rbx)
	movq	%r13, 32(%rbx)
	movq	%rbp, 40(%rbx)
	movss	16(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	movsd	20(%r13), %xmm0         # xmm0 = mem[0],zero
	movsd	20(%r12), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm0
	movss	16(%rbp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	20(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	24(%rbp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm6
	movaps	%xmm0, %xmm3
	mulps	%xmm6, %xmm3
	movaps	%xmm1, %xmm4
	mulss	%xmm2, %xmm1
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm0, %xmm6
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	shufps	$0, %xmm0, %xmm4        # xmm4 = xmm4[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm4      # xmm4 = xmm4[2,0],xmm0[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	subss	%xmm6, %xmm1
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm3, (%rbx)
	movlps	%xmm2, 8(%rbx)
	mulss	%xmm3, %xmm3
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	sqrtss	%xmm1, %xmm12
	ucomiss	%xmm12, %xmm12
	jnp	.LBB13_11
# BB#10:                                # %call.sqrt
	movaps	%xmm1, %xmm0
	callq	sqrtf
	movaps	%xmm0, %xmm12
.LBB13_11:                              # %_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE.exit80.split
	movss	16(%r12), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	20(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	24(%r12), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, (%rsp)           # 16-byte Spill
	movss	8(%rbx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, 16(%rsp)         # 16-byte Spill
	movss	20(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	20(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movaps	%xmm1, %xmm4
	subss	%xmm7, %xmm4
	unpcklps	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	movaps	%xmm7, %xmm15
	subps	%xmm5, %xmm15
	movss	24(%rbp), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	24(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm2
	subss	%xmm10, %xmm2
	unpcklps	%xmm5, %xmm10   # xmm10 = xmm10[0],xmm5[0],xmm10[1],xmm5[1]
	unpcklps	%xmm14, %xmm5   # xmm5 = xmm5[0],xmm14[0],xmm5[1],xmm14[1]
	movaps	%xmm10, %xmm3
	subps	%xmm5, %xmm3
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movaps	%xmm6, %xmm1
	mulps	%xmm3, %xmm1
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	movaps	%xmm8, %xmm0
	movaps	%xmm0, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm5, %xmm3
	mulps	%xmm15, %xmm5
	mulps	%xmm9, %xmm15
	subps	%xmm15, %xmm1
	movss	16(%rbp), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm15
	subss	%xmm11, %xmm15
	movss	16(%r13), %xmm13        # xmm13 = mem[0],zero,zero,zero
	unpcklps	%xmm13, %xmm11  # xmm11 = xmm11[0],xmm13[0],xmm11[1],xmm13[1]
	unpcklps	%xmm8, %xmm13   # xmm13 = xmm13[0],xmm8[0],xmm13[1],xmm8[1]
	mulps	%xmm11, %xmm1
	subps	%xmm13, %xmm11
	mulps	%xmm11, %xmm9
	subps	%xmm3, %xmm9
	mulps	%xmm11, %xmm6
	subps	%xmm6, %xmm5
	mulps	%xmm7, %xmm9
	movaps	(%rsp), %xmm7           # 16-byte Reload
	addps	%xmm1, %xmm9
	mulps	%xmm10, %xmm5
	addps	%xmm9, %xmm5
	movaps	%xmm0, %xmm6
	movaps	16(%rsp), %xmm9         # 16-byte Reload
	movaps	%xmm7, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm9, %xmm3
	mulss	%xmm4, %xmm3
	subss	%xmm3, %xmm1
	movaps	%xmm9, %xmm3
	mulss	%xmm15, %xmm3
	mulss	%xmm6, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm6, %xmm4
	mulss	%xmm7, %xmm15
	subss	%xmm15, %xmm4
	mulss	%xmm8, %xmm1
	mulss	32(%rsp), %xmm3         # 16-byte Folded Reload
	addss	%xmm1, %xmm3
	mulss	%xmm14, %xmm4
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	%xmm3, %xmm4
	minss	%xmm1, %xmm5
	minss	%xmm4, %xmm5
	movss	.LCPI13_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm12
	movaps	%xmm1, %xmm2
	cmpltss	%xmm12, %xmm2
	movaps	%xmm2, %xmm3
	andps	%xmm12, %xmm3
	movss	.LCPI13_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	andnps	%xmm1, %xmm2
	orps	%xmm3, %xmm2
	divss	%xmm2, %xmm5
	movss	.LCPI13_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	cmpnless	%xmm5, %xmm2
	andps	%xmm5, %xmm2
	movss	%xmm2, 20(%rbx)
	movl	$2, %eax
	jbe	.LBB13_14
# BB#12:
	movss	16(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	movss	20(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	addss	%xmm2, %xmm3
	movss	24(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	addss	%xmm3, %xmm2
	divss	%xmm12, %xmm2
	movss	%xmm2, 16(%rbx)
	divss	%xmm12, %xmm1
	mulss	%xmm1, %xmm6
	movss	%xmm6, (%rbx)
	mulss	%xmm1, %xmm7
	movss	%xmm7, 4(%rbx)
	mulss	%xmm9, %xmm1
	movss	%xmm1, 8(%rbx)
	ucomiss	.LCPI13_3(%rip), %xmm2
	jae	.LBB13_25
# BB#13:
	xorb	$1, %r15b
	movl	$3, %eax
	je	.LBB13_25
.LBB13_14:
	movl	%eax, (%r14)
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.LBB13_16
# BB#15:
	movq	72(%rbx), %rcx
	movq	%rcx, 72(%rax)
.LBB13_16:
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.LBB13_18
# BB#17:
	movq	80(%rbx), %rcx
	movq	%rcx, 80(%rax)
.LBB13_18:
	cmpq	%rbx, 14432(%r14)
	jne	.LBB13_20
# BB#19:
	movq	80(%rbx), %rax
	movq	%rax, 14432(%r14)
.LBB13_20:                              # %_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE.exit55
	decl	14440(%r14)
	movq	$0, 72(%rbx)
	movq	14448(%r14), %rax
	movq	%rax, 80(%rbx)
	movq	14448(%r14), %rax
	testq	%rax, %rax
	je	.LBB13_22
# BB#21:
	movq	%rbx, 72(%rax)
.LBB13_22:                              # %_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE.exit
	movq	%rbx, 14448(%r14)
	incl	14456(%r14)
	jmp	.LBB13_24
.LBB13_23:
	movl	$5, (%r14)
.LBB13_24:
	xorl	%ebx, %ebx
.LBB13_25:
	movq	%rbx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b, .Lfunc_end13-_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	3072837036              # float -9.99999974E-6
	.section	.text._ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE,"axG",@progbits,_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE,comdat
	.weak	_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE
	.p2align	4, 0x90
	.type	_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE,@function
_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE: # @_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 64
.Lcfi137:
	.cfi_offset %rbx, -56
.Lcfi138:
	.cfi_offset %r12, -48
.Lcfi139:
	.cfi_offset %r13, -40
.Lcfi140:
	.cfi_offset %r14, -32
.Lcfi141:
	.cfi_offset %r15, -24
.Lcfi142:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movl	%r8d, %r13d
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movl	%esi, %ebp
	movzbl	91(%rbx), %eax
	cmpl	%ebp, %eax
	jne	.LBB14_2
# BB#1:
	xorl	%ebp, %ebp
	jmp	.LBB14_22
.LBB14_2:
	movl	%r13d, %r14d
	movl	_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3(,%r14,4), %eax
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	16(%r12), %xmm0
	mulss	20(%r12), %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	24(%r12), %xmm0
	addss	%xmm1, %xmm0
	subss	16(%rbx), %xmm0
	movss	.LCPI14_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB14_6
# BB#3:
	movq	24(%rbx,%rax,8), %rsi
	movq	24(%rbx,%r14,8), %rdx
	xorl	%ebp, %ebp
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	callq	_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b
	testq	%rax, %rax
	je	.LBB14_22
# BB#4:
	movb	%r13b, 88(%rax)
	movq	%rbx, 48(%rax)
	movb	$0, 88(%rbx,%r14)
	movq	%rax, 48(%rbx,%r14,8)
	movq	(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB14_19
# BB#5:
	movb	$2, 89(%rcx)
	movq	%rax, 56(%rcx)
	movb	$1, 90(%rax)
	movq	%rcx, 64(%rax)
	jmp	.LBB14_20
.LBB14_6:
	movb	%bpl, 91(%rbx)
	movq	48(%rbx,%rax,8), %rcx
	movzbl	88(%rbx,%rax), %r8d
	movq	%rdi, %r13
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	%r15, %r9
	callq	_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE
	testb	%al, %al
	je	.LBB14_18
# BB#7:
	movl	_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3(,%r14,4), %eax
	movq	48(%rbx,%rax,8), %rcx
	movzbl	88(%rbx,%rax), %r8d
	movq	%r13, %r14
	movq	%r13, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	%r15, %r9
	callq	_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE
	testb	%al, %al
	je	.LBB14_18
# BB#8:
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.LBB14_10
# BB#9:
	movq	72(%rbx), %rcx
	movq	%rcx, 72(%rax)
.LBB14_10:
	movq	72(%rbx), %rax
	testq	%rax, %rax
	movq	%r14, %rdx
	je	.LBB14_12
# BB#11:
	movq	80(%rbx), %rcx
	movq	%rcx, 80(%rax)
.LBB14_12:
	cmpq	%rbx, 14432(%rdx)
	jne	.LBB14_14
# BB#13:
	movq	80(%rbx), %rax
	movq	%rax, 14432(%rdx)
.LBB14_14:                              # %_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE.exit
	decl	14440(%rdx)
	movq	$0, 72(%rbx)
	movq	14448(%rdx), %rax
	movq	%rax, 80(%rbx)
	movq	14448(%rdx), %rax
	testq	%rax, %rax
	je	.LBB14_16
# BB#15:
	movq	%rbx, 72(%rax)
.LBB14_16:
	movq	%rbx, 14448(%rdx)
	incl	14456(%rdx)
	jmp	.LBB14_21
.LBB14_18:
	xorl	%ebp, %ebp
	jmp	.LBB14_22
.LBB14_19:
	movq	%rax, 8(%r15)
.LBB14_20:
	movq	%rax, (%r15)
	incl	16(%r15)
.LBB14_21:                              # %.thread59
	movb	$1, %bpl
.LBB14_22:                              # %.thread59
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE, .Lfunc_end14-_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE
	.cfi_endproc

	.type	_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3,@object # @_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3
	.section	.rodata._ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3,"aG",@progbits,_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3,comdat
	.weak	_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3
	.p2align	2
_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.size	_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3, 12

	.type	_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3,@object # @_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3
	.section	.rodata._ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3,"aG",@progbits,_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3,comdat
	.weak	_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3
	.p2align	2
_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.size	_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3, 12

	.type	_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3,@object # @_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3
	.section	.rodata._ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3,"aG",@progbits,_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3,comdat
	.weak	_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3
	.p2align	2
_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.size	_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3, 12

	.type	_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3,@object # @_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3
	.section	.rodata._ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3,"aG",@progbits,_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3,comdat
	.weak	_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3
	.p2align	2
_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3:
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.size	_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
