	.text
	.file	"main.bc"
	.globl	print_tree
	.p2align	4, 0x90
	.type	print_tree,@function
print_tree:                             # @print_tree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_3
	.p2align	4, 0x90
.LBB0_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%rbx), %xmm1         # xmm1 = mem[0],zero
	movl	$.L.str, %edi
	movb	$2, %al
	callq	printf
	movq	24(%rbx), %rdi
	movq	32(%rbx), %rbx
	callq	print_tree
	testq	%rbx, %rbx
	jne	.LBB0_1
.LBB0_3:                                # %tailrecurse._crit_edge
	popq	%rbx
	retq
.Lfunc_end0:
	.size	print_tree, .Lfunc_end0-print_tree
	.cfi_endproc

	.globl	print_list
	.p2align	4, 0x90
	.type	print_list,@function
print_list:                             # @print_list
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB1_4
# BB#1:
	movsd	8(%r14), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%r14), %xmm1         # xmm1 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$2, %al
	callq	printf
	movq	40(%r14), %rbx
	cmpq	%r14, %rbx
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%rbx), %xmm1         # xmm1 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$2, %al
	callq	printf
	movq	40(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB1_2
.LBB1_4:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	print_list, .Lfunc_end1-print_list
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	callq	dealwithargs
	movl	%eax, %ebx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movl	NumNodes(%rip), %ecx
	movsd	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movl	$0, %esi
	movl	$0, %edx
	xorps	%xmm0, %xmm0
	xorps	%xmm2, %xmm2
	movl	%ebx, %edi
	movaps	%xmm1, %xmm3
	callq	build_tree
	movq	%rax, %r14
	cmpl	$0, flag(%rip)
	jne	.LBB2_2
# BB#1:
	movl	$.Lstr, %edi
	callq	puts
	cmpl	$0, flag(%rip)
	je	.LBB2_4
.LBB2_2:                                # %.thread
	movl	$.Lstr.3, %edi
	callq	puts
	cmpl	$0, flag(%rip)
	je	.LBB2_4
# BB#3:
	movl	$.Lstr.2, %edi
	callq	puts
.LBB2_4:                                # %.thread12
	movl	NumNodes(%rip), %edx
	movl	$.L.str.6, %edi
	movl	$150, %esi
	xorl	%eax, %eax
	callq	printf
	movl	NumNodes(%rip), %edx
	movl	$150, %esi
	movq	%r14, %rdi
	callq	tsp
	cmpl	$0, flag(%rip)
	je	.LBB2_11
# BB#5:
	testq	%r14, %r14
	je	.LBB2_9
# BB#6:
	movsd	8(%r14), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%r14), %xmm1         # xmm1 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$2, %al
	callq	printf
	movq	40(%r14), %rbx
	cmpq	%r14, %rbx
	je	.LBB2_9
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%rbx), %xmm1         # xmm1 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$2, %al
	callq	printf
	movq	40(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB2_7
.LBB2_9:                                # %print_list.exit
	cmpl	$0, flag(%rip)
	je	.LBB2_11
# BB#10:
	movl	$.Lstr.1, %edi
	callq	puts
.LBB2_11:                               # %print_list.exit.thread
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"x=%f,y=%f\n"
	.size	.L.str, 11

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%f %f\n"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Building tree of size %d\n"
	.size	.L.str.2, 26

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Call tsp(t, %d, %d)\n"
	.size	.L.str.6, 21

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"Past build"
	.size	.Lstr, 11

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"linetype solid"
	.size	.Lstr.1, 15

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"newcurve pts"
	.size	.Lstr.2, 13

	.type	.Lstr.3,@object         # @str.3
.Lstr.3:
	.asciz	"newgraph"
	.size	.Lstr.3, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
