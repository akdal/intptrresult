	.text
	.file	"main.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4612811918334230528     # double 2.5
.LCPI0_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 288
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	8(%rbp), %r12
	movq	16(%rbp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	cmpl	$2, %edi
	jl	.LBB0_1
# BB#2:                                 # %.lr.ph.split.preheader
	movl	%edi, 80(%rsp)          # 4-byte Spill
	movl	$.L.str, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_44
# BB#3:                                 # %.lr.ph.preheader
	movslq	80(%rsp), %r13          # 4-byte Folded Reload
	xorl	%ebx, %ebx
	movl	$2, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.1, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	cmoveq	stdin(%rip), %rbx
	movl	$.L.str.1, %esi
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	cmoveq	stdout(%rip), %r14
	testq	%rbx, %rbx
	jne	.LBB0_9
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movl	$.L.str.2, %esi
	movq	%r12, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_6
.LBB0_9:                                # %.thread
                                        #   in Loop: Header=BB0_4 Depth=1
	testq	%r14, %r14
	jne	.LBB0_13
# BB#10:                                #   in Loop: Header=BB0_4 Depth=1
	movl	$.L.str.4, %esi
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_11
.LBB0_13:                               # %.thread135
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpq	%r13, %r15
	jge	.LBB0_14
# BB#43:                                # %.thread135..lr.ph.split_crit_edge
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	(%rbp,%r15,8), %rdi
	movl	$.L.str, %esi
	callq	strcmp
	incq	%r15
	testl	%eax, %eax
	jne	.LBB0_4
	jmp	.LBB0_44
.LBB0_1:
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	cmpl	$2, %edi
	jl	.LBB0_44
	jmp	.LBB0_16
.LBB0_14:
	movl	80(%rsp), %edi          # 4-byte Reload
	cmpl	$2, %edi
	jge	.LBB0_16
.LBB0_44:                               # %.critedge
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$10, %edi
	callq	putchar
	movl	$1, %ebx
	jmp	.LBB0_45
.LBB0_16:
	movl	$0, 108(%rsp)
	movl	$0, 104(%rsp)
	movl	$0, 100(%rsp)
	leaq	192(%rsp), %rsi
	leaq	100(%rsp), %rdx
	movq	%rbx, %rdi
	callq	_Z14GetInputFormatP8_IO_FILEP22AudioFormatDescriptionPj
	testl	%eax, %eax
	jne	.LBB0_17
# BB#18:
	movl	100(%rsp), %esi
	cmpl	$1463899717, %esi       # imm = 0x57415645
	je	.LBB0_21
# BB#19:
	cmpl	$1667327590, %esi       # imm = 0x63616666
	jne	.LBB0_20
.LBB0_21:
	movl	200(%rsp), %eax
	cmpl	$1634492771, %eax       # imm = 0x616C6163
	je	.LBB0_24
# BB#22:
	cmpl	$1819304813, %eax       # imm = 0x6C70636D
	jne	.LBB0_23
.LBB0_24:
	movq	192(%rsp), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movl	220(%rsp), %r12d
	cmpl	$1819304813, %eax       # imm = 0x6C70636D
	jne	.LBB0_28
# BB#25:
	movl	224(%rsp), %ecx
	addl	$-16, %ecx
	movl	%ecx, %eax
	roll	$30, %eax
	movl	$1634492771, %ebp       # imm = 0x616C6163
	cmpl	$4, %eax
	ja	.LBB0_29
# BB#26:                                # %switch.hole_check.i
	shrl	$2, %ecx
	movl	$23, %edx
	btl	%ecx, %edx
	jae	.LBB0_29
# BB#27:                                # %switch.lookup.i
	cltq
	movl	.Lswitch.table(,%rax,4), %r15d
	movl	$4096, 84(%rsp)         # 4-byte Folded Spill
                                        # imm = 0x1000
	movl	$0, 80(%rsp)            # 4-byte Folded Spill
	xorl	%r13d, %r13d
	jmp	.LBB0_34
.LBB0_28:
	movl	204(%rsp), %eax
	decl	%eax
	movl	$1819304813, %ebp       # imm = 0x6C70636D
	cmpl	$3, %eax
	ja	.LBB0_29
# BB#30:                                # %switch.lookup30.i
	movslq	%eax, %rcx
	movl	.Lswitch.table.5(,%rcx,4), %r13d
	cmpl	$1, %eax
	movl	%r13d, 80(%rsp)         # 4-byte Spill
	jne	.LBB0_31
# BB#32:
	movl	%r12d, %eax
	cvtsi2sdq	%rax, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_1(%rip), %xmm0
	cvttsd2si	%xmm0, %r13d
	jmp	.LBB0_33
.LBB0_29:
                                        # implicit-def: %EAX
	movl	%eax, 80(%rsp)          # 4-byte Spill
                                        # implicit-def: %R13D
                                        # implicit-def: %EAX
	movl	%eax, 84(%rsp)          # 4-byte Spill
                                        # implicit-def: %R15D
	jmp	.LBB0_34
.LBB0_31:
	shrl	$3, %r13d
	imull	%r12d, %r13d
.LBB0_33:                               # %_Z15SetOutputFormat22AudioFormatDescriptionPS_.exit
	xorl	%r15d, %r15d
	movl	$1, 84(%rsp)            # 4-byte Folded Spill
.LBB0_34:                               # %_Z15SetOutputFormat22AudioFormatDescriptionPS_.exit
	leaq	108(%rsp), %rdx
	leaq	104(%rsp), %rcx
	movq	%rbx, %rdi
	callq	_Z13FindDataStartP8_IO_FILEjPiS1_
	movslq	108(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	cmpl	$1634492771, %ebp       # imm = 0x616C6163
	jne	.LBB0_36
# BB#35:
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 152(%rsp)
	movl	$1634492771, 160(%rsp)  # imm = 0x616C6163
	movl	%r15d, 164(%rsp)
	movl	%r13d, 168(%rsp)
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, 172(%rsp)
	movl	%r13d, 176(%rsp)
	movl	%r12d, 180(%rsp)
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, 184(%rsp)
	movl	$0, 188(%rsp)
	movl	104(%rsp), %edx
	movq	184(%rsp), %rax
	movq	%rax, 72(%rsp)
	movups	152(%rsp), %xmm0
	movups	168(%rsp), %xmm1
	movups	%xmm1, 56(%rsp)
	movups	%xmm0, 40(%rsp)
	movq	224(%rsp), %rax
	movq	%rax, 32(%rsp)
	movups	192(%rsp), %xmm0
	movups	208(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z10EncodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_i
	testq	%rbx, %rbx
	jne	.LBB0_40
	jmp	.LBB0_41
.LBB0_36:
	movl	100(%rsp), %eax
	cmpl	$1463899717, %eax       # imm = 0x57415645
	setne	%cl
	cmpl	$3, %r12d
	jb	.LBB0_38
# BB#37:
	testb	%cl, %cl
	jne	.LBB0_46
.LBB0_38:
	cmpl	$1463899717, %eax       # imm = 0x57415645
	movl	$1667327590, %eax       # imm = 0x63616666
	movl	$1463899717, %ecx       # imm = 0x57415645
	cmovel	%eax, %ecx
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 112(%rsp)
	movl	%ebp, 120(%rsp)
	movl	%r15d, 124(%rsp)
	movl	%r13d, 128(%rsp)
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, 132(%rsp)
	movl	%r13d, 136(%rsp)
	movl	%r12d, 140(%rsp)
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, 144(%rsp)
	movl	$0, 148(%rsp)
	movq	144(%rsp), %rax
	movq	%rax, 72(%rsp)
	movups	112(%rsp), %xmm0
	movups	128(%rsp), %xmm1
	movups	%xmm1, 56(%rsp)
	movups	%xmm0, 40(%rsp)
	movq	224(%rsp), %rax
	movq	%rax, 32(%rsp)
	movups	192(%rsp), %xmm0
	movups	208(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z10DecodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_ij
	testq	%rbx, %rbx
	je	.LBB0_41
.LBB0_40:
	movq	%rbx, %rdi
	callq	fclose
.LBB0_41:
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	.LBB0_45
# BB#42:
	movq	%r14, %rdi
	callq	fclose
.LBB0_45:
	movl	%ebx, %eax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_6:                                # %.us-lcssa.us
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
.LBB0_7:                                # %.us-lcssa.us
	xorl	%eax, %eax
	movq	%r12, %rdx
	jmp	.LBB0_8
.LBB0_11:                               # %.us-lcssa144.us
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
.LBB0_12:                               # %.us-lcssa144.us
	xorl	%eax, %eax
	movq	88(%rsp), %rdx          # 8-byte Reload
.LBB0_8:                                # %.us-lcssa.us
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_17:
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	jmp	.LBB0_7
.LBB0_20:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	jmp	.LBB0_12
.LBB0_23:
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	jmp	.LBB0_12
.LBB0_46:
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	_Z14GetInputFormatP8_IO_FILEP22AudioFormatDescriptionPj
	.p2align	4, 0x90
	.type	_Z14GetInputFormatP8_IO_FILEP22AudioFormatDescriptionPj,@function
_Z14GetInputFormatP8_IO_FILEP22AudioFormatDescriptionPj: # @_Z14GetInputFormatP8_IO_FILEP22AudioFormatDescriptionPj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r12, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$4, %edx
	movq	%rbx, %rcx
	callq	fread
	movb	(%rsp), %al
	cmpb	$82, %al
	je	.LBB1_6
# BB#1:
	cmpb	$99, %al
	jne	.LBB1_19
# BB#2:
	cmpb	$97, 1(%rsp)
	jne	.LBB1_19
# BB#3:
	cmpb	$102, 2(%rsp)
	jne	.LBB1_19
# BB#4:
	cmpb	$102, 3(%rsp)
	jne	.LBB1_19
# BB#5:
	movl	$1667327590, (%r15)     # imm = 0x63616666
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z17GetCAFFdescFormatP8_IO_FILEP22AudioFormatDescription
	notb	%al
	movzbl	%al, %eax
	andl	$1, %eax
	negl	%eax
	jmp	.LBB1_20
.LBB1_6:
	cmpb	$73, 1(%rsp)
	jne	.LBB1_19
# BB#7:
	cmpb	$70, 2(%rsp)
	jne	.LBB1_19
# BB#8:
	cmpb	$70, 3(%rsp)
	jne	.LBB1_19
# BB#9:
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$8, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpb	$87, 4(%rsp)
	jne	.LBB1_19
# BB#10:
	cmpb	$65, 5(%rsp)
	jne	.LBB1_19
# BB#11:
	cmpb	$86, 6(%rsp)
	jne	.LBB1_19
# BB#12:
	cmpb	$69, 7(%rsp)
	jne	.LBB1_19
# BB#13:                                # %.lr.ph
	movl	$1463899717, (%r15)     # imm = 0x57415645
	movq	%rsp, %r12
	jmp	.LBB1_14
	.p2align	4, 0x90
.LBB1_15:                               # %.thread.us
                                        #   in Loop: Header=BB1_14 Depth=1
	movl	$1, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	callq	fread
	movzbl	3(%rsp), %eax
	shlq	$24, %rax
	movzbl	2(%rsp), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	1(%rsp), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	(%rsp), %esi
	orq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	fseek
.LBB1_14:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	callq	fread
	movl	(%rsp), %eax
	shll	$24, %eax
	movzbl	1(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	2(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	3(%rsp), %ecx
	orl	%eax, %ecx
	cmpl	$1718449184, %ecx       # imm = 0x666D7420
	jne	.LBB1_15
# BB#16:                                # %.us-lcssa.us
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$20, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpb	$1, 4(%rsp)
	jne	.LBB1_19
# BB#17:                                # %.us-lcssa.us
	movb	5(%rsp), %al
	testb	%al, %al
	jne	.LBB1_19
# BB#18:                                # %.thread.outer
	movl	$1819304813, 8(%r14)    # imm = 0x6C70636D
	movzbl	6(%rsp), %eax
	movl	%eax, 28(%r14)
	movzbl	11(%rsp), %ecx
	shll	$24, %ecx
	movzbl	10(%rsp), %edx
	shll	$16, %edx
	orl	%ecx, %edx
	movzbl	9(%rsp), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	movzbl	8(%rsp), %edx
	orl	%ecx, %edx
	cvtsi2sdq	%rdx, %xmm0
	movsd	%xmm0, (%r14)
	movzbl	18(%rsp), %ecx
	movl	%ecx, 32(%r14)
	movl	$12, 12(%r14)
	shrl	$3, %ecx
	imull	%eax, %ecx
	movl	%ecx, 24(%r14)
	movl	%ecx, 16(%r14)
	movl	$1, 20(%r14)
	movl	$0, 36(%r14)
	xorl	%eax, %eax
	jmp	.LBB1_20
.LBB1_19:                               # %thread-pre-split.thread
	movl	$0, (%r15)
	movl	$-1, %eax
.LBB1_20:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_Z14GetInputFormatP8_IO_FILEP22AudioFormatDescriptionPj, .Lfunc_end1-_Z14GetInputFormatP8_IO_FILEP22AudioFormatDescriptionPj
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4612811918334230528     # double 2.5
.LCPI2_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	_Z15SetOutputFormat22AudioFormatDescriptionPS_
	.p2align	4, 0x90
	.type	_Z15SetOutputFormat22AudioFormatDescriptionPS_,@function
_Z15SetOutputFormat22AudioFormatDescriptionPS_: # @_Z15SetOutputFormat22AudioFormatDescriptionPS_
	.cfi_startproc
# BB#0:
	leaq	8(%rsp), %r8
	cmpl	$1819304813, 16(%rsp)   # imm = 0x6C70636D
	jne	.LBB2_4
# BB#1:
	movl	$1634492771, 8(%rdi)    # imm = 0x616C6163
	movq	(%r8), %rax
	movq	%rax, (%rdi)
	movl	32(%r8), %esi
	addl	$-16, %esi
	movl	%esi, %edx
	roll	$30, %edx
	movl	$-1, %eax
	cmpl	$4, %edx
	ja	.LBB2_10
# BB#2:                                 # %switch.hole_check
	shrl	$2, %esi
	movl	$23, %ecx
	btl	%esi, %ecx
	jae	.LBB2_10
# BB#3:                                 # %switch.lookup
	movslq	%edx, %rax
	movl	.Lswitch.table(,%rax,4), %eax
	movl	%eax, 12(%rdi)
	movl	$4096, 20(%rdi)         # imm = 0x1000
	movl	28(%r8), %eax
	movl	%eax, 28(%rdi)
	movl	$0, 36(%rdi)
	movl	$0, 32(%rdi)
	movl	$0, 24(%rdi)
	movl	$0, 16(%rdi)
	jmp	.LBB2_9
.LBB2_4:
	movl	$1819304813, 8(%rdi)    # imm = 0x6C70636D
	movq	(%r8), %rax
	movq	%rax, (%rdi)
	movl	12(%r8), %edx
	decl	%edx
	movl	$-1, %eax
	cmpl	$3, %edx
	ja	.LBB2_10
# BB#5:                                 # %switch.lookup30
	movslq	%edx, %rax
	movl	.Lswitch.table.5(,%rax,4), %eax
	movl	%eax, 32(%rdi)
	movl	$1, 20(%rdi)
	movl	28(%r8), %ecx
	movl	%ecx, 28(%rdi)
	cmpl	$1, %edx
	jne	.LBB2_6
# BB#7:
	movl	%ecx, %eax
	cvtsi2sdq	%rax, %xmm0
	mulsd	.LCPI2_0(%rip), %xmm0
	addsd	.LCPI2_1(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	jmp	.LBB2_8
.LBB2_6:
	shrl	$3, %eax
	imull	%ecx, %eax
.LBB2_8:
	movl	%eax, 24(%rdi)
	movl	%eax, 16(%rdi)
	movl	$0, 12(%rdi)
	movl	$0, 36(%rdi)
.LBB2_9:
	xorl	%eax, %eax
.LBB2_10:
	retq
.Lfunc_end2:
	.size	_Z15SetOutputFormat22AudioFormatDescriptionPS_, .Lfunc_end2-_Z15SetOutputFormat22AudioFormatDescriptionPS_
	.cfi_endproc

	.globl	_Z13FindDataStartP8_IO_FILEjPiS1_
	.p2align	4, 0x90
	.type	_Z13FindDataStartP8_IO_FILEjPiS1_,@function
_Z13FindDataStartP8_IO_FILEjPiS1_:      # @_Z13FindDataStartP8_IO_FILEjPiS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 80
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	ftell
	movq	%rax, %r14
	cmpl	$1667327590, %ebp       # imm = 0x63616666
	je	.LBB3_7
# BB#1:
	movb	$1, %r13b
	cmpl	$1463899717, %ebp       # imm = 0x57415645
	jne	.LBB3_8
# BB#2:                                 # %.critedge29
	movq	%r15, 16(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	leaq	4(%rsp), %rdi
	movl	$1, %esi
	movl	$8, %edx
	movq	%rbx, %rcx
	callq	fread
	movzbl	11(%rsp), %eax
	shll	$24, %eax
	movzbl	10(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	9(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	8(%rsp), %r15d
	orl	%eax, %r15d
	movl	$12, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	movq	%rbx, %rdi
	callq	ftell
	cmpl	%r15d, %eax
	jae	.LBB3_8
# BB#3:                                 # %.lr.ph
	leaq	4(%rsp), %rbp
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	$8, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rcx
	callq	fread
	movzbl	4(%rsp), %eax
	shll	$24, %eax
	movzbl	5(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	6(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	7(%rsp), %ecx
	orl	%eax, %ecx
	cmpl	$1684108385, %ecx       # imm = 0x64617461
	je	.LBB3_5
# BB#6:                                 #   in Loop: Header=BB3_4 Depth=1
	movzbl	11(%rsp), %eax
	shlq	$24, %rax
	movzbl	10(%rsp), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	9(%rsp), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	8(%rsp), %esi
	orq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	fseek
	movq	%rbx, %rdi
	callq	ftell
	cmpl	%r15d, %eax
	jb	.LBB3_4
	jmp	.LBB3_8
.LBB3_7:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_Z17FindCAFFDataStartP8_IO_FILEPiS1_
	movb	%al, %r13b
	xorb	$1, %r13b
	jmp	.LBB3_8
.LBB3_5:                                # %.critedge.critedge
	movq	%rbx, %rdi
	callq	ftell
	movl	%eax, (%r12)
	movzbl	11(%rsp), %eax
	shll	$24, %eax
	movzbl	10(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	9(%rsp), %eax
	shll	$8, %eax
	orl	%ecx, %eax
	movzbl	8(%rsp), %ecx
	orl	%eax, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%ecx, (%rax)
	xorl	%r13d, %r13d
.LBB3_8:                                # %.critedge
	movslq	%r14d, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	movzbl	%r13b, %eax
	negl	%eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_Z13FindDataStartP8_IO_FILEjPiS1_, .Lfunc_end3-_Z13FindDataStartP8_IO_FILEjPiS1_
	.cfi_endproc

	.globl	_Z10EncodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_i
	.p2align	4, 0x90
	.type	_Z10EncodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_i,@function
_Z10EncodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_i: # @_Z10EncodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_i
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi41:
	.cfi_def_cfa_offset 384
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %r14
	movq	%rdi, 168(%rsp)         # 8-byte Spill
	movl	416(%rsp), %eax
	movl	%eax, 124(%rsp)         # 4-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shrl	$3, %eax
	imull	412(%rsp), %eax
	movl	444(%rsp), %r15d
	imull	%r15d, %eax
	leal	8(%rax), %ebp
	movl	$0, 100(%rsp)
	movl	$0, 96(%rsp)
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movslq	%eax, %rdi
	movl	$1, %esi
	movq	%rdi, 200(%rsp)         # 8-byte Spill
	callq	calloc
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movslq	%ebp, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movl	$0, 92(%rsp)
	movl	$0, 120(%rsp)
	movl	$8304, %edi             # imm = 0x2070
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp0:
	movq	%rbp, %rdi
	callq	_ZN11ALACEncoderC1Ev
.Ltmp1:
# BB#1:
	leaq	424(%rsp), %rbx
	leaq	384(%rsp), %r12
	movl	%r15d, 8284(%rbp)
	movq	(%rbp), %rax
	movq	32(%rbx), %rcx
	movq	%rcx, 32(%rsp)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%rbp, %rdi
	callq	*32(%rax)
	movq	%r14, %rdi
	callq	_Z18WriteCAFFcaffChunkP8_IO_FILE
	movq	32(%rbx), %rax
	movq	%rax, 32(%rsp)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	callq	_Z18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescription
	movl	28(%rbx), %r15d
	movq	%rbp, %rdi
	movl	%r15d, %esi
	callq	_ZN11ALACEncoder18GetMagicCookieSizeEj
	movl	%eax, 120(%rsp)
	movl	%eax, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	leaq	120(%rsp), %rdx
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	_ZN11ALACEncoder14GetMagicCookieEPvPj
	movl	120(%rsp), %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_Z18WriteCAFFkukiChunkP8_IO_FILEPvj
	movq	%rbx, %rdi
	callq	free
	cmpl	$3, %r15d
	jb	.LBB4_3
# BB#2:
	decl	%r15d
	movl	_ZL21ALACChannelLayoutTags(,%r15,4), %esi
	movq	%r14, %rdi
	callq	_Z18WriteCAFFchanChunkP8_IO_FILEj
.LBB4_3:
	movq	32(%r12), %rax
	movq	%rax, 32(%rsp)
	movups	(%r12), %xmm0
	movups	16(%r12), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	100(%rsp), %rsi
	leaq	296(%rsp), %r12
	movl	%r13d, %edi
	movq	%r12, %rdx
	callq	_Z20BuildBasePacketTable22AudioFormatDescriptioniPiP25port_CAFPacketTableHeader
	movl	100(%rsp), %eax
	movl	%eax, 116(%rsp)         # 4-byte Spill
	movslq	%eax, %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, %r15
	addl	$24, %ebx
	movl	%ebx, 100(%rsp)
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	%ebx, %edx
	callq	_Z24WriteCAFFpaktChunkHeaderP8_IO_FILEP25port_CAFPacketTableHeaderj
	movq	%r14, %rdi
	callq	ftell
	movq	%rax, %rbx
	movslq	100(%rsp), %rdx
	addq	$-24, %rdx
	movl	%edx, 100(%rsp)
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	fwrite
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	ftell
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	%r14, %rdi
	callq	_Z18WriteCAFFdataChunkP8_IO_FILE
	movq	%r14, %rdi
	callq	ftell
	movl	%r13d, %ecx
	movq	%rax, %r15
	cmpl	%ecx, 176(%rsp)         # 4-byte Folded Reload
	movq	%r14, %r13
	movq	%r13, 136(%rsp)         # 8-byte Spill
	movq	%rbx, 184(%rsp)         # 8-byte Spill
	jle	.LBB4_5
# BB#4:
	xorl	%eax, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	%ebx, %r12d
	movq	104(%rsp), %rbx         # 8-byte Reload
	testl	%ecx, %ecx
	jne	.LBB4_23
	jmp	.LBB4_22
.LBB4_5:                                # %.lr.ph209
	leaq	384(%rsp), %rax
	movl	12(%rax), %eax
	andl	$2, %eax
	movl	%eax, 160(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	124(%rsp), %eax         # 4-byte Reload
	movl	%eax, 112(%rsp)         # 4-byte Spill
	movl	%ebx, %r12d
	movq	104(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_11 Depth 2
                                        #     Child Loop BB4_18 Depth 2
                                        #     Child Loop BB4_15 Depth 2
	movl	%ecx, %r14d
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	200(%rsp), %rdx         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	callq	fread
	subl	%eax, %r14d
	cmpl	$0, 160(%rsp)           # 4-byte Folded Reload
	movl	%eax, 92(%rsp)
	movl	%r14d, 164(%rsp)        # 4-byte Spill
	je	.LBB4_20
# BB#7:                                 #   in Loop: Header=BB4_6 Depth=1
	movl	112(%rsp), %edx         # 4-byte Reload
	cmpl	$16, %edx
	je	.LBB4_13
# BB#8:                                 #   in Loop: Header=BB4_6 Depth=1
	cmpl	$32, %edx
	jne	.LBB4_9
# BB#16:                                #   in Loop: Header=BB4_6 Depth=1
	sarl	$2, %eax
	movl	$32, 112(%rsp)          # 4-byte Folded Spill
	testl	%eax, %eax
	jle	.LBB4_20
# BB#17:                                # %.lr.ph199.preheader
                                        #   in Loop: Header=BB4_6 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_18:                               # %.lr.ph199
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	Swap32
	incq	%rbp
	movl	92(%rsp), %eax
	sarl	$2, %eax
	cltq
	addq	$4, %rbx
	cmpq	%rax, %rbp
	jl	.LBB4_18
	jmp	.LBB4_19
.LBB4_13:                               #   in Loop: Header=BB4_6 Depth=1
	sarl	%eax
	movl	$16, 112(%rsp)          # 4-byte Folded Spill
	testl	%eax, %eax
	jle	.LBB4_20
# BB#14:                                # %.lr.ph201.preheader
                                        #   in Loop: Header=BB4_6 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_15:                               # %.lr.ph201
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	Swap16
	incq	%rbp
	movl	92(%rsp), %eax
	sarl	%eax
	cltq
	addq	$2, %rbx
	cmpq	%rax, %rbp
	jl	.LBB4_15
.LBB4_19:                               #   in Loop: Header=BB4_6 Depth=1
	movq	136(%rsp), %r13         # 8-byte Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB4_20
.LBB4_9:                                # %.preheader189
                                        #   in Loop: Header=BB4_6 Depth=1
	testl	%eax, %eax
	jle	.LBB4_20
# BB#10:                                # %.lr.ph203.preheader
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	%rbx, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph203
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r14,%rbx), %rdi
	callq	Swap24
	addq	$3, %rbx
	movslq	92(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB4_11
# BB#12:                                #   in Loop: Header=BB4_6 Depth=1
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB4_20:                               # %.loopexit190
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	leaq	384(%rsp), %rcx
	movq	%rcx, %rdx
	movq	32(%rdx), %rcx
	movq	%rcx, 288(%rsp)
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movaps	%xmm1, 272(%rsp)
	movaps	%xmm0, 256(%rsp)
	movq	32(%rdx), %rcx
	movq	%rcx, 72(%rsp)
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	%xmm1, 56(%rsp)
	movups	%xmm0, 40(%rsp)
	movq	288(%rsp), %rcx
	movq	%rcx, 32(%rsp)
	movaps	256(%rsp), %xmm0
	movaps	272(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%rbx, %rsi
	movq	152(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdx
	leaq	92(%rsp), %rcx
	callq	*%rax
	movl	92(%rsp), %edi
	movq	%rbx, %rsi
	leaq	96(%rsp), %rdx
	callq	_Z13GetBERIntegeriPhPi
	movslq	%r12d, %r12
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	fseek
	movslq	96(%rsp), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%r13, %rcx
	callq	fwrite
	movl	96(%rsp), %eax
	addl	%eax, %r12d
	subl	%eax, 116(%rsp)         # 4-byte Folded Spill
	movslq	%r15d, %r15
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	fseek
	movslq	92(%rsp), %rdx
	movl	$1, %esi
	movq	%rbp, %rdi
	movq	%r13, %rcx
	callq	fwrite
	movslq	92(%rsp), %rax
	addl	%eax, %r15d
	addq	%rax, 128(%rsp)         # 8-byte Folded Spill
	movl	164(%rsp), %ecx         # 4-byte Reload
	cmpl	%ecx, 176(%rsp)         # 4-byte Folded Reload
	jle	.LBB4_6
# BB#21:                                # %._crit_edge
	testl	%ecx, %ecx
	je	.LBB4_22
.LBB4_23:
	movslq	%ecx, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	168(%rsp), %rcx         # 8-byte Reload
	callq	fread
	movl	%eax, 92(%rsp)
	leaq	384(%rsp), %rcx
	testb	$2, 12(%rcx)
	jne	.LBB4_24
.LBB4_35:                               # %.loopexit
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	leaq	384(%rsp), %rcx
	movq	%rcx, %rdx
	movq	32(%rdx), %rcx
	movq	%rcx, 240(%rsp)
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movaps	%xmm1, 224(%rsp)
	movaps	%xmm0, 208(%rsp)
	movq	32(%rdx), %rcx
	movq	%rcx, 72(%rsp)
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	%xmm1, 56(%rsp)
	movups	%xmm0, 40(%rsp)
	movq	240(%rsp), %rcx
	movq	%rcx, 32(%rsp)
	movaps	208(%rsp), %xmm0
	movaps	224(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	92(%rsp), %rcx
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rsi
	movq	152(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdx
	callq	*%rax
	movl	92(%rsp), %edi
	leaq	96(%rsp), %rdx
	movq	%rbx, %rsi
	callq	_Z13GetBERIntegeriPhPi
	movslq	%r12d, %r12
	xorl	%edx, %edx
	movq	136(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	fseek
	movslq	96(%rsp), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%r13, %rcx
	callq	fwrite
	movl	96(%rsp), %eax
	addl	%eax, %r12d
	movl	116(%rsp), %ebp         # 4-byte Reload
	subl	%eax, %ebp
	movslq	%r15d, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	fseek
	movslq	92(%rsp), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r13, %rcx
	callq	fwrite
	movslq	92(%rsp), %rax
	addq	%rax, 128(%rsp)         # 8-byte Folded Spill
	jmp	.LBB4_36
.LBB4_22:
	movl	116(%rsp), %ebp         # 4-byte Reload
.LBB4_36:
	cmpl	$13, %ebp
	movl	%ebp, %r15d
	jb	.LBB4_38
# BB#37:
	movslq	%r12d, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	fseek
	movq	%r13, %rdi
	movl	%r15d, %esi
	callq	_Z18WriteCAFFfreeChunkP8_IO_FILEj
	movq	184(%rsp), %rax         # 8-byte Reload
	shlq	$32, %rax
	movabsq	$-137438953472, %rsi    # imm = 0xFFFFFFE000000000
	addq	%rax, %rsi
	sarq	$32, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	fseek
	movl	$24, %esi
	subl	%r15d, %esi
	addl	100(%rsp), %esi
	movq	%r13, %rdi
	callq	_Z18WriteCAFFChunkSizeP8_IO_FILEl
.LBB4_38:
	movq	192(%rsp), %rax         # 8-byte Reload
	shlq	$32, %rax
	movabsq	$17179869184, %rsi      # imm = 0x400000000
	addq	%rax, %rsi
	sarq	$32, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	fseek
	movq	128(%rsp), %rsi         # 8-byte Reload
	addq	$4, %rsi
	movq	%r13, %rdi
	callq	_Z18WriteCAFFChunkSizeP8_IO_FILEl
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	callq	*8(%rax)
	movq	%rbx, %rdi
	callq	free
	movq	152(%rsp), %rdi         # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_24:
	movl	124(%rsp), %ecx         # 4-byte Reload
	cmpl	$16, %ecx
	je	.LBB4_29
# BB#25:
	cmpl	$32, %ecx
	jne	.LBB4_26
# BB#32:
	sarl	$2, %eax
	testl	%eax, %eax
	jle	.LBB4_35
# BB#33:                                # %.lr.ph197.preheader
	xorl	%ebp, %ebp
	movq	104(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_34:                               # %.lr.ph197
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	Swap32
	incq	%rbp
	movl	92(%rsp), %eax
	sarl	$2, %eax
	cltq
	addq	$4, %rbx
	cmpq	%rax, %rbp
	jl	.LBB4_34
	jmp	.LBB4_35
.LBB4_29:
	sarl	%eax
	testl	%eax, %eax
	jle	.LBB4_35
# BB#30:                                # %.lr.ph195.preheader
	xorl	%ebx, %ebx
	movq	104(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_31:                               # %.lr.ph195
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	Swap16
	incq	%rbx
	movl	92(%rsp), %eax
	sarl	%eax
	cltq
	addq	$2, %r14
	cmpq	%rax, %rbx
	jl	.LBB4_31
	jmp	.LBB4_35
.LBB4_26:                               # %.preheader
	testl	%eax, %eax
	jle	.LBB4_35
# BB#27:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_28:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx), %rdi
	callq	Swap24
	addq	$3, %rbx
	movslq	92(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB4_28
	jmp	.LBB4_35
.LBB4_39:
.Ltmp2:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_Z10EncodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_i, .Lfunc_end4-_Z10EncodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end4-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z10DecodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_ij
	.p2align	4, 0x90
	.type	_Z10DecodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_ij,@function
_Z10DecodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_ij: # @_Z10DecodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_ij
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 224
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	%rdi, %r12
	movl	244(%rsp), %ecx
	movl	252(%rsp), %edx
	movl	296(%rsp), %eax
	movl	%eax, 96(%rsp)          # 4-byte Spill
	shrl	$3, %eax
	movl	%edx, 124(%rsp)         # 4-byte Spill
	movl	%edx, %ebx
	movl	%ecx, 128(%rsp)         # 4-byte Spill
	imull	%ecx, %ebx
	imull	%eax, %ebx
	leal	8(%rbx), %r15d
	movl	$0, 132(%rsp)
	movl	$0, 44(%rsp)
	movslq	%r15d, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %r14
	movslq	%ebx, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	$0, 40(%rsp)
	movl	$0, 92(%rsp)
	movl	$0, 76(%rsp)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp3:
	movq	%r13, %rdi
	callq	_ZN11ALACDecoderC1Ev
.Ltmp4:
# BB#1:
	movq	%r12, %rdi
	callq	_Z30GetMagicCookieSizeFromCAFFkukiP8_IO_FILE
	movl	%eax, 76(%rsp)
	movl	%eax, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	leaq	76(%rsp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	_Z26GetMagicCookieFromCAFFkukiP8_IO_FILEPhPj
	movl	76(%rsp), %edx
	movq	%r13, 112(%rsp)         # 8-byte Spill
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	_ZN11ALACDecoder4InitEPvj
	movq	%rbx, %rdi
	callq	free
	leaq	144(%rsp), %rdi
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	BitBufferInit
	movq	%r12, %rdi
	callq	ftell
	movq	%rax, %r13
	cmpl	$1463899717, %ebp       # imm = 0x57415645
	movl	%ebp, 120(%rsp)         # 4-byte Spill
	je	.LBB5_5
# BB#2:
	movq	104(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdi
	callq	_Z18WriteCAFFcaffChunkP8_IO_FILE
	leaq	264(%rsp), %rax
	movq	%rax, %r15
	movq	32(%r15), %rax
	movq	%rax, 32(%rsp)
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	callq	_Z18WriteCAFFdescChunkP8_IO_FILE22AudioFormatDescription
	movl	28(%r15), %eax
	cmpl	$3, %eax
	jb	.LBB5_4
# BB#3:
	decl	%eax
	movl	_ZL21ALACChannelLayoutTags(,%rax,4), %esi
	movq	%r14, %rdi
	callq	_Z18WriteCAFFchanChunkP8_IO_FILEj
.LBB5_4:
	movq	%r14, %rdi
	callq	ftell
	addq	$4, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	callq	_Z18WriteCAFFdataChunkP8_IO_FILE
	jmp	.LBB5_6
.LBB5_5:
	movl	$1163280727, 56(%rsp)   # imm = 0x45564157
	movq	$1179011410, 48(%rsp)   # imm = 0x46464952
	leaq	48(%rsp), %rdi
	movl	$1, %esi
	movl	$12, %edx
	movq	104(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rcx
	callq	fwrite
	leaq	264(%rsp), %rax
	movq	%rax, %rcx
	movl	24(%rcx), %eax
	movb	28(%rcx), %bl
	cvttsd2si	(%rcx), %rdx
	movaps	.L_ZZ17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescriptionE9theBuffer(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movq	$0, 64(%rsp)
	movl	%edx, %ecx
	imull	%eax, %ecx
	movb	%bl, 58(%rsp)
	movb	%dl, 60(%rsp)
	movb	%dh, 61(%rsp)  # NOREX
	movl	%edx, %esi
	shrl	$16, %esi
	movb	%sil, 62(%rsp)
	shrl	$24, %edx
	movb	%dl, 63(%rsp)
	movb	%cl, 64(%rsp)
	movb	%ch, 65(%rsp)  # NOREX
	movl	%ecx, %edx
	shrl	$16, %edx
	movb	%dl, 66(%rsp)
	shrl	$24, %ecx
	movb	%cl, 67(%rsp)
	movb	%al, 68(%rsp)
	movl	96(%rsp), %eax          # 4-byte Reload
	movb	%al, 70(%rsp)
	leaq	48(%rsp), %rdi
	movl	$1, %esi
	movl	$24, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	$1635017060, 48(%rsp)   # imm = 0x61746164
	leaq	48(%rsp), %rdi
	movl	$1, %esi
	movl	$8, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	%r15, %rdi
	callq	ftell
	movl	$4294967292, %ecx       # imm = 0xFFFFFFFC
	addq	%rax, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
.LBB5_6:
	movq	80(%rsp), %rbp          # 8-byte Reload
	leaq	44(%rsp), %rsi
	leaq	132(%rsp), %rdx
	movq	%r12, %rdi
	callq	_Z24FindCAFFPacketTableStartP8_IO_FILEPiS1_
	movslq	44(%rsp), %rsi
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	fseek
	movl	$1, %esi
	movl	$5, %edx
	movq	%rbp, %rdi
	movq	%r12, %rcx
	callq	fread
	movl	%eax, 40(%rsp)
	leaq	40(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_Z14ReadBERIntegerPhPi
	movl	%eax, %r15d
	movl	40(%rsp), %eax
	addl	%eax, 44(%rsp)
	movslq	%r13d, %rsi
	xorl	%edx, %edx
	movq	%r12, %rbx
	movq	%r12, %rdi
	callq	fseek
	testl	%r15d, %r15d
	jle	.LBB5_10
# BB#7:                                 # %.lr.ph
	addl	%r15d, %r13d
	xorl	%r14d, %r14d
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	%rbx, %r12
	.p2align	4, 0x90
.LBB5_8:                                # =>This Inner Loop Header: Depth=1
	movslq	%r15d, %rbx
	movl	$1, %esi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	callq	fread
	cmpq	%rax, %rbx
	movq	%r12, %rbx
	jne	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=1
	movq	112(%rsp), %rdi         # 8-byte Reload
	leaq	144(%rsp), %r12
	movq	%r12, %rsi
	movq	136(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdx
	movl	128(%rsp), %ecx         # 4-byte Reload
	movl	124(%rsp), %r8d         # 4-byte Reload
	leaq	92(%rsp), %r9
	callq	_ZN11ALACDecoder6DecodeEP9BitBufferPhjjPj
	leaq	264(%rsp), %rax
	movl	24(%rax), %eax
	imull	92(%rsp), %eax
	movl	%eax, 40(%rsp)
	movslq	%eax, %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	104(%rsp), %rcx         # 8-byte Reload
	callq	fwrite
	movslq	40(%rsp), %rax
	addq	%rax, %r14
	movslq	44(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	movl	$1, %esi
	movl	$5, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rcx
	callq	fread
	movl	%eax, 40(%rsp)
	movq	%rbp, %rdi
	leaq	40(%rsp), %rsi
	callq	_Z14ReadBERIntegerPhPi
	movq	%rbx, %r12
	movl	%eax, %r15d
	movl	40(%rsp), %eax
	addl	%eax, 44(%rsp)
	movslq	%r13d, %r13
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	fseek
	addl	%r15d, %r13d
	leaq	144(%rsp), %rdi
	callq	BitBufferReset
	testl	%r15d, %r15d
	jg	.LBB5_8
.LBB5_10:                               # %.critedge
	movslq	96(%rsp), %rsi          # 4-byte Folded Reload
	xorl	%edx, %edx
	movq	104(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdi
	callq	fseek
	cmpl	$1463899717, 120(%rsp)  # 4-byte Folded Reload
                                        # imm = 0x57415645
	je	.LBB5_12
# BB#11:
	addq	$4, %r14
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	_Z18WriteCAFFChunkSizeP8_IO_FILEl
	jmp	.LBB5_13
.LBB5_12:
	movq	%r14, %rbx
	movb	%bl, 48(%rsp)
	movb	%bh, 49(%rsp)  # NOREX
	movl	%ebx, %eax
	shrl	$16, %eax
	movb	%al, 50(%rsp)
	movl	%ebx, %eax
	shrl	$24, %eax
	movb	%al, 51(%rsp)
	leaq	48(%rsp), %r14
	movl	$1, %esi
	movl	$4, %edx
	movq	%r14, %rdi
	movq	%rbp, %rcx
	callq	fwrite
	movl	$4, %esi
	xorl	%edx, %edx
	movq	%rbp, %rdi
	callq	fseek
	addq	$36, %rbx
	movb	%bl, 48(%rsp)
	movb	%bh, 49(%rsp)  # NOREX
	movl	%ebx, %eax
	shrl	$16, %eax
	movb	%al, 50(%rsp)
	shrl	$24, %ebx
	movb	%bl, 51(%rsp)
	movl	$1, %esi
	movl	$4, %edx
	movq	%r14, %rdi
	movq	%rbp, %rcx
	callq	fwrite
.LBB5_13:
	movq	112(%rsp), %rbp         # 8-byte Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
.Ltmp6:
	movq	%rbp, %rdi
	callq	_ZN11ALACDecoderD1Ev
.Ltmp7:
# BB#14:
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	free
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_15:
.Ltmp8:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	jmp	.LBB5_16
.LBB5_17:
.Ltmp5:
	movq	%rax, %rbx
	movq	%r13, %rdi
.LBB5_16:
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_Z10DecodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_ij, .Lfunc_end5-_Z10DecodeALACP8_IO_FILES0_22AudioFormatDescriptionS1_ij
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp4           #   Call between .Ltmp4 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 5 <<
	.long	.Lfunc_end5-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z23GetALACChannelLayoutTagj
	.p2align	4, 0x90
	.type	_Z23GetALACChannelLayoutTagj,@function
_Z23GetALACChannelLayoutTagj:           # @_Z23GetALACChannelLayoutTagj
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	decl	%edi
	movl	_ZL21ALACChannelLayoutTags(,%rdi,4), %eax
	retq
.Lfunc_end6:
	.size	_Z23GetALACChannelLayoutTagj, .Lfunc_end6-_Z23GetALACChannelLayoutTagj
	.cfi_endproc

	.globl	_Z18WriteWAVERIFFChunkP8_IO_FILE
	.p2align	4, 0x90
	.type	_Z18WriteWAVERIFFChunkP8_IO_FILE,@function
_Z18WriteWAVERIFFChunkP8_IO_FILE:       # @_Z18WriteWAVERIFFChunkP8_IO_FILE
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 32
	movq	%rdi, %rax
	movl	$1163280727, 16(%rsp)   # imm = 0x45564157
	movq	$1179011410, 8(%rsp)    # imm = 0x46464952
	leaq	8(%rsp), %rdi
	movl	$1, %esi
	movl	$12, %edx
	movq	%rax, %rcx
	callq	fwrite
	addq	$24, %rsp
	retq
.Lfunc_end7:
	.size	_Z18WriteWAVERIFFChunkP8_IO_FILE, .Lfunc_end7-_Z18WriteWAVERIFFChunkP8_IO_FILE
	.cfi_endproc

	.globl	_Z17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescription
	.p2align	4, 0x90
	.type	_Z17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescription,@function
_Z17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescription: # @_Z17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescription
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 32
	movq	%rdi, %r8
	movaps	.L_ZZ17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescriptionE9theBuffer(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
	cvttsd2si	32(%rsp), %rdx
	movl	56(%rsp), %esi
	movl	%edx, %ecx
	imull	%esi, %ecx
	movb	60(%rsp), %al
	movb	%al, 10(%rsp)
	movb	%dl, 12(%rsp)
	movb	%dh, 13(%rsp)  # NOREX
	movl	%edx, %eax
	shrl	$16, %eax
	movb	%al, 14(%rsp)
	shrl	$24, %edx
	movb	%dl, 15(%rsp)
	movb	%cl, 16(%rsp)
	movb	%ch, 17(%rsp)  # NOREX
	movl	%ecx, %eax
	shrl	$16, %eax
	movb	%al, 18(%rsp)
	shrl	$24, %ecx
	movb	%cl, 19(%rsp)
	movb	%sil, 20(%rsp)
	movb	64(%rsp), %al
	movb	%al, 22(%rsp)
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$24, %edx
	movq	%r8, %rcx
	callq	fwrite
	addq	$24, %rsp
	retq
.Lfunc_end8:
	.size	_Z17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescription, .Lfunc_end8-_Z17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescription
	.cfi_endproc

	.globl	_Z18WriteWAVEdataChunkP8_IO_FILE
	.p2align	4, 0x90
	.type	_Z18WriteWAVEdataChunkP8_IO_FILE,@function
_Z18WriteWAVEdataChunkP8_IO_FILE:       # @_Z18WriteWAVEdataChunkP8_IO_FILE
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	movq	$1635017060, (%rsp)     # imm = 0x61746164
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$8, %edx
	movq	%rax, %rcx
	callq	fwrite
	popq	%rax
	retq
.Lfunc_end9:
	.size	_Z18WriteWAVEdataChunkP8_IO_FILE, .Lfunc_end9-_Z18WriteWAVEdataChunkP8_IO_FILE
	.cfi_endproc

	.globl	_Z18WriteWAVEChunkSizeP8_IO_FILEj
	.p2align	4, 0x90
	.type	_Z18WriteWAVEChunkSizeP8_IO_FILEj,@function
_Z18WriteWAVEChunkSizeP8_IO_FILEj:      # @_Z18WriteWAVEChunkSizeP8_IO_FILEj
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 16
	movl	%esi, %ecx
	movq	%rdi, %rax
	movb	%cl, 4(%rsp)
	movb	%ch, 5(%rsp)  # NOREX
	movl	%ecx, %edx
	shrl	$16, %edx
	movb	%dl, 6(%rsp)
	shrl	$24, %ecx
	movb	%cl, 7(%rsp)
	leaq	4(%rsp), %rdi
	movl	$1, %esi
	movl	$4, %edx
	movq	%rax, %rcx
	callq	fwrite
	popq	%rax
	retq
.Lfunc_end10:
	.size	_Z18WriteWAVEChunkSizeP8_IO_FILEj, .Lfunc_end10-_Z18WriteWAVEChunkSizeP8_IO_FILEj
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"-h"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"-"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"rb"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" Cannot open file \"%s\"\n"
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"w+b"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" Cannot determine what format file \"%s\" is\n"
	.size	.L.str.5, 44

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" File \"%s\" is of an unsupported type\n"
	.size	.L.str.6, 38

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" File \"%s's\" data format is of an unsupported type\n"
	.size	.L.str.7, 52

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" Cannot decode more than two channels to WAVE\n"
	.size	.L.str.8, 47

	.type	_ZL21ALACChannelLayoutTags,@object # @_ZL21ALACChannelLayoutTags
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
_ZL21ALACChannelLayoutTags:
	.long	6553601                 # 0x640001
	.long	6619138                 # 0x650002
	.long	7405571                 # 0x710003
	.long	7602180                 # 0x740004
	.long	7864325                 # 0x780005
	.long	8126470                 # 0x7c0006
	.long	9306119                 # 0x8e0007
	.long	8323080                 # 0x7f0008
	.size	_ZL21ALACChannelLayoutTags, 32

	.type	.L_ZZ18WriteWAVERIFFChunkP8_IO_FILEE13theReadBuffer,@object # @_ZZ18WriteWAVERIFFChunkP8_IO_FILEE13theReadBuffer
	.section	.rodata,"a",@progbits
.L_ZZ18WriteWAVERIFFChunkP8_IO_FILEE13theReadBuffer:
	.ascii	"RIFF\000\000\000\000WAVE"
	.size	.L_ZZ18WriteWAVERIFFChunkP8_IO_FILEE13theReadBuffer, 12

	.type	.L_ZZ17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescriptionE9theBuffer,@object # @_ZZ17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescriptionE9theBuffer
	.p2align	4
.L_ZZ17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescriptionE9theBuffer:
	.asciz	"fmt \020\000\000\000\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	.L_ZZ17WriteWAVEfmtChunkP8_IO_FILE22AudioFormatDescriptionE9theBuffer, 24

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr:
	.asciz	"Usage:"
	.size	.Lstr, 7

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"Encode:"
	.size	.Lstr.1, 8

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.2:
	.asciz	"        alacconvert <input wav or caf file> <output caf file>"
	.size	.Lstr.2, 62

	.type	.Lstr.3,@object         # @str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.3:
	.asciz	"Decode:"
	.size	.Lstr.3, 8

	.type	.Lstr.4,@object         # @str.4
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.4:
	.asciz	"        alacconvert <input caf file> <output wav or caf file>"
	.size	.Lstr.4, 62

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	4                       # 0x4
	.size	.Lswitch.table, 20

	.type	.Lswitch.table.5,@object # @switch.table.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	2
.Lswitch.table.5:
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	24                      # 0x18
	.long	32                      # 0x20
	.size	.Lswitch.table.5, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
