	.text
	.file	"btGjkEpaPenetrationDepthSolver.bc"
	.globl	_ZN30btGjkEpaPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc
	.p2align	4, 0x90
	.type	_ZN30btGjkEpaPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc,@function
_ZN30btGjkEpaPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc: # @_ZN30btGjkEpaPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %rbx
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	160(%rsp), %r13
	movq	152(%rsp), %rbp
	movsd	48(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	48(%r15), %xmm1         # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	56(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	56(%r15), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	movl	$1, (%rsp)
	leaq	16(%rsp), %r8
	leaq	32(%rsp), %r9
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb
	testb	%al, %al
	je	.LBB0_2
# BB#1:
	movups	36(%rsp), %xmm0
	movups	%xmm0, (%rbp)
	movups	52(%rsp), %xmm0
	movups	%xmm0, (%r13)
	movups	68(%rsp), %xmm0
	movq	144(%rsp), %rax
	movups	%xmm0, (%rax)
	movb	$1, %al
	jmp	.LBB0_5
.LBB0_2:
	leaq	16(%rsp), %r8
	leaq	32(%rsp), %r9
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE
	testb	%al, %al
	je	.LBB0_4
# BB#3:
	movups	36(%rsp), %xmm0
	movups	%xmm0, (%rbp)
	movups	52(%rsp), %xmm0
	movups	%xmm0, (%r13)
	movups	68(%rsp), %xmm0
	movq	144(%rsp), %rax
	movups	%xmm0, (%rax)
.LBB0_4:
	xorl	%eax, %eax
.LBB0_5:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN30btGjkEpaPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc, .Lfunc_end0-_ZN30btGjkEpaPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc
	.cfi_endproc

	.section	.text._ZN30btConvexPenetrationDepthSolverD2Ev,"axG",@progbits,_ZN30btConvexPenetrationDepthSolverD2Ev,comdat
	.weak	_ZN30btConvexPenetrationDepthSolverD2Ev
	.p2align	4, 0x90
	.type	_ZN30btConvexPenetrationDepthSolverD2Ev,@function
_ZN30btConvexPenetrationDepthSolverD2Ev: # @_ZN30btConvexPenetrationDepthSolverD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN30btConvexPenetrationDepthSolverD2Ev, .Lfunc_end1-_ZN30btConvexPenetrationDepthSolverD2Ev
	.cfi_endproc

	.section	.text._ZN30btGjkEpaPenetrationDepthSolverD0Ev,"axG",@progbits,_ZN30btGjkEpaPenetrationDepthSolverD0Ev,comdat
	.weak	_ZN30btGjkEpaPenetrationDepthSolverD0Ev
	.p2align	4, 0x90
	.type	_ZN30btGjkEpaPenetrationDepthSolverD0Ev,@function
_ZN30btGjkEpaPenetrationDepthSolverD0Ev: # @_ZN30btGjkEpaPenetrationDepthSolverD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN30btGjkEpaPenetrationDepthSolverD0Ev, .Lfunc_end2-_ZN30btGjkEpaPenetrationDepthSolverD0Ev
	.cfi_endproc

	.type	_ZTV30btGjkEpaPenetrationDepthSolver,@object # @_ZTV30btGjkEpaPenetrationDepthSolver
	.section	.rodata,"a",@progbits
	.globl	_ZTV30btGjkEpaPenetrationDepthSolver
	.p2align	3
_ZTV30btGjkEpaPenetrationDepthSolver:
	.quad	0
	.quad	_ZTI30btGjkEpaPenetrationDepthSolver
	.quad	_ZN30btConvexPenetrationDepthSolverD2Ev
	.quad	_ZN30btGjkEpaPenetrationDepthSolverD0Ev
	.quad	_ZN30btGjkEpaPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc
	.size	_ZTV30btGjkEpaPenetrationDepthSolver, 40

	.type	_ZTS30btGjkEpaPenetrationDepthSolver,@object # @_ZTS30btGjkEpaPenetrationDepthSolver
	.globl	_ZTS30btGjkEpaPenetrationDepthSolver
	.p2align	4
_ZTS30btGjkEpaPenetrationDepthSolver:
	.asciz	"30btGjkEpaPenetrationDepthSolver"
	.size	_ZTS30btGjkEpaPenetrationDepthSolver, 33

	.type	_ZTS30btConvexPenetrationDepthSolver,@object # @_ZTS30btConvexPenetrationDepthSolver
	.section	.rodata._ZTS30btConvexPenetrationDepthSolver,"aG",@progbits,_ZTS30btConvexPenetrationDepthSolver,comdat
	.weak	_ZTS30btConvexPenetrationDepthSolver
	.p2align	4
_ZTS30btConvexPenetrationDepthSolver:
	.asciz	"30btConvexPenetrationDepthSolver"
	.size	_ZTS30btConvexPenetrationDepthSolver, 33

	.type	_ZTI30btConvexPenetrationDepthSolver,@object # @_ZTI30btConvexPenetrationDepthSolver
	.section	.rodata._ZTI30btConvexPenetrationDepthSolver,"aG",@progbits,_ZTI30btConvexPenetrationDepthSolver,comdat
	.weak	_ZTI30btConvexPenetrationDepthSolver
	.p2align	3
_ZTI30btConvexPenetrationDepthSolver:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS30btConvexPenetrationDepthSolver
	.size	_ZTI30btConvexPenetrationDepthSolver, 16

	.type	_ZTI30btGjkEpaPenetrationDepthSolver,@object # @_ZTI30btGjkEpaPenetrationDepthSolver
	.section	.rodata,"a",@progbits
	.globl	_ZTI30btGjkEpaPenetrationDepthSolver
	.p2align	4
_ZTI30btGjkEpaPenetrationDepthSolver:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30btGjkEpaPenetrationDepthSolver
	.quad	_ZTI30btConvexPenetrationDepthSolver
	.size	_ZTI30btGjkEpaPenetrationDepthSolver, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
