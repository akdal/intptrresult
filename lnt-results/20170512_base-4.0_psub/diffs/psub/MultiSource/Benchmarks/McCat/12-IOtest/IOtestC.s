	.text
	.file	"IOtestC.bc"
	.globl	testC
	.p2align	4, 0x90
	.type	testC,@function
testC:                                  # @testC
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	initarray
	movq	%rsp, %rdx
	movl	$initminB, %edi
	movl	$stepminB, %esi
	callq	loop
	leaq	1(%rsp), %rdx
	movl	$initmaxB, %edi
	movl	$stepmaxB, %esi
	callq	loop
	leaq	2(%rsp), %rdx
	movl	$initaddB, %edi
	movl	$stepaddB, %esi
	callq	loop
	leaq	3(%rsp), %rdx
	movl	$initmultB, %edi
	movl	$stepmultB, %esi
	callq	loop
	movsbl	(%rsp), %esi
	movsbl	1(%rsp), %edx
	movsbl	2(%rsp), %ecx
	movsbl	3(%rsp), %r8d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	popq	%rax
	retq
.Lfunc_end0:
	.size	testC, .Lfunc_end0-testC
	.cfi_endproc

	.p2align	4, 0x90
	.type	initminB,@function
initminB:                               # @initminB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%edi, %edi
	callq	setac
	movb	$-1, (%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	initminB, .Lfunc_end1-initminB
	.cfi_endproc

	.p2align	4, 0x90
	.type	stepminB,@function
stepminB:                               # @stepminB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
.Lcfi4:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	getac
	movq	%rax, %rdi
	callq	array
	movsbl	(%rbx), %edi
	movsbl	%al, %esi
	callq	min
	movb	%al, (%rbx)
	callq	getac
	leaq	4097(%rax), %rdi
	popq	%rbx
	jmp	setac                   # TAILCALL
.Lfunc_end2:
	.size	stepminB, .Lfunc_end2-stepminB
	.cfi_endproc

	.p2align	4, 0x90
	.type	initmaxB,@function
initmaxB:                               # @initmaxB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%edi, %edi
	callq	setac
	movb	$0, (%rbx)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	initmaxB, .Lfunc_end3-initmaxB
	.cfi_endproc

	.p2align	4, 0x90
	.type	stepmaxB,@function
stepmaxB:                               # @stepmaxB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	getac
	movq	%rax, %rdi
	callq	array
	movsbl	(%rbx), %edi
	movsbl	%al, %esi
	callq	max
	movb	%al, (%rbx)
	callq	getac
	leaq	4097(%rax), %rdi
	popq	%rbx
	jmp	setac                   # TAILCALL
.Lfunc_end4:
	.size	stepmaxB, .Lfunc_end4-stepmaxB
	.cfi_endproc

	.p2align	4, 0x90
	.type	initaddB,@function
initaddB:                               # @initaddB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%edi, %edi
	callq	setac
	movb	$0, (%rbx)
	popq	%rbx
	retq
.Lfunc_end5:
	.size	initaddB, .Lfunc_end5-initaddB
	.cfi_endproc

	.p2align	4, 0x90
	.type	stepaddB,@function
stepaddB:                               # @stepaddB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	getac
	movq	%rax, %rdi
	callq	array
	movsbl	(%rbx), %edi
	movsbl	%al, %esi
	callq	add
	movb	%al, (%rbx)
	callq	getac
	leaq	4097(%rax), %rdi
	popq	%rbx
	jmp	setac                   # TAILCALL
.Lfunc_end6:
	.size	stepaddB, .Lfunc_end6-stepaddB
	.cfi_endproc

	.p2align	4, 0x90
	.type	initmultB,@function
initmultB:                              # @initmultB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%edi, %edi
	callq	setac
	movb	$1, (%rbx)
	popq	%rbx
	retq
.Lfunc_end7:
	.size	initmultB, .Lfunc_end7-initmultB
	.cfi_endproc

	.p2align	4, 0x90
	.type	stepmultB,@function
stepmultB:                              # @stepmultB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
.Lcfi16:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	getac
	movq	%rax, %rdi
	callq	array
	movsbl	(%rbx), %edi
	movsbl	%al, %esi
	callq	mult
	movb	%al, (%rbx)
	callq	getac
	leaq	4097(%rax), %rdi
	popq	%rbx
	jmp	setac                   # TAILCALL
.Lfunc_end8:
	.size	stepmultB, .Lfunc_end8-stepmultB
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"C %d min %d max %d add %d mult \n"
	.size	.L.str, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
