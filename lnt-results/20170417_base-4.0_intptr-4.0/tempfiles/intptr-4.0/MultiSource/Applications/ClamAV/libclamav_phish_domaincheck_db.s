	.text
	.file	"libclamav_phish_domaincheck_db.bc"
	.globl	domainlist_match
	.p2align	4, 0x90
	.type	domainlist_match,@function
domainlist_match:                       # @domainlist_match
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 64
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%r9, %r14
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB0_1
# BB#2:
	xorl	%eax, %eax
	testl	%r8d, %r8d
	cmovneq	%rcx, %rax
	movl	$0, (%rsp)
	leaq	16(%rsp), %r9
	movq	%rax, %rcx
	callq	regex_list_match
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB0_12
# BB#3:
	movq	16(%rsp), %r15
	testq	%r15, %r15
	je	.LBB0_12
# BB#4:
	movsbq	(%r15), %r12
	testq	%r12, %r12
	je	.LBB0_12
# BB#5:
	cmpb	$58, %r12b
	je	.LBB0_12
# BB#6:
	movq	%r15, %rdi
	callq	strlen
	cmpq	$3, %rax
	jne	.LBB0_11
# BB#7:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$16, 1(%rax,%r12,2)
	je	.LBB0_11
# BB#8:
	movsbq	1(%r15), %rcx
	testb	$16, 1(%rax,%rcx,2)
	je	.LBB0_11
# BB#9:
	movsbq	2(%r15), %rcx
	testb	$16, 1(%rax,%rcx,2)
	jne	.LBB0_10
.LBB0_11:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_12
.LBB0_1:
	xorl	%ebx, %ebx
.LBB0_12:                               # %.thread
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_10:
	movw	$0, 14(%rsp)
	leaq	14(%rsp), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sscanf
	movzwl	14(%rsp), %eax
	notl	%eax
	andw	%ax, (%r14)
	jmp	.LBB0_12
.Lfunc_end0:
	.size	domainlist_match, .Lfunc_end0-domainlist_match
	.cfi_endproc

	.globl	init_domainlist
	.p2align	4, 0x90
	.type	init_domainlist,@function
init_domainlist:                        # @init_domainlist
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	movl	$96, %edi
	callq	cli_malloc
	movq	%rax, 64(%rbx)
	testq	%rax, %rax
	je	.LBB1_3
# BB#5:
	movq	%rax, %rdi
	popq	%rbx
	jmp	init_regex_list         # TAILCALL
.LBB1_1:
	movl	$-111, %eax
	popq	%rbx
	retq
.LBB1_3:
	movl	$-114, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	init_domainlist, .Lfunc_end1-init_domainlist
	.cfi_endproc

	.globl	is_domainlist_ok
	.p2align	4, 0x90
	.type	is_domainlist_ok,@function
is_domainlist_ok:                       # @is_domainlist_ok
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#3:
	jmp	is_regex_ok             # TAILCALL
.LBB2_2:
	movl	$1, %eax
	retq
.Lfunc_end2:
	.size	is_domainlist_ok, .Lfunc_end2-is_domainlist_ok
	.cfi_endproc

	.globl	domainlist_cleanup
	.p2align	4, 0x90
	.type	domainlist_cleanup,@function
domainlist_cleanup:                     # @domainlist_cleanup
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB3_2
# BB#1:
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB3_2
# BB#3:
	jmp	regex_list_cleanup      # TAILCALL
.LBB3_2:
	retq
.Lfunc_end3:
	.size	domainlist_cleanup, .Lfunc_end3-domainlist_cleanup
	.cfi_endproc

	.globl	domainlist_done
	.p2align	4, 0x90
	.type	domainlist_done,@function
domainlist_done:                        # @domainlist_done
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB4_3
# BB#1:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_3
# BB#2:
	callq	regex_list_done
	movq	64(%rbx), %rdi
	callq	free
	movq	$0, 64(%rbx)
.LBB4_3:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	domainlist_done, .Lfunc_end4-domainlist_done
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%hx"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Phishcheck:Unknown flag format in domain-list, 3 hex digits expected"
	.size	.L.str.1, 69


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
