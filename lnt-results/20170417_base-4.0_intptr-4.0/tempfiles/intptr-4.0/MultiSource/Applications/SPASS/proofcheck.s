	.text
	.file	"proofcheck.bc"
	.globl	pcheck_ConvertParentsInSPASSProof
	.p2align	4, 0x90
	.type	pcheck_ConvertParentsInSPASSProof,@function
pcheck_ConvertParentsInSPASSProof:      # @pcheck_ConvertParentsInSPASSProof
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	96(%r14), %rdi
	callq	list_Copy
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	list_Copy
	testq	%rbp, %rbp
	je	.LBB0_1
# BB#2:
	testq	%rax, %rax
	je	.LBB0_6
# BB#3:                                 # %.preheader.i.preheader
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_4
# BB#5:
	movq	%rax, (%rcx)
	jmp	.LBB0_6
.LBB0_1:
	movq	%rax, %rbp
.LBB0_6:                                # %list_Nconc.exit
	movq	56(%r14), %rdi
	callq	list_Copy
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_7
# BB#8:
	testq	%rbp, %rbp
	je	.LBB0_12
# BB#9:                                 # %.preheader.i20.preheader
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB0_10:                               # %.preheader.i20
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_10
# BB#11:
	movq	%rbp, (%rax)
	jmp	.LBB0_12
.LBB0_7:
	movq	%rbp, %rbx
.LBB0_12:                               # %list_Nconc.exit22
	movq	40(%r14), %rdi
	callq	list_Copy
	testq	%rax, %rax
	je	.LBB0_13
# BB#14:
	testq	%rbx, %rbx
	je	.LBB0_18
# BB#15:                                # %.preheader.i26.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader.i26
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_16
# BB#17:
	movq	%rbx, (%rcx)
	jmp	.LBB0_18
.LBB0_13:
	movq	%rbx, %rax
.LBB0_18:                               # %list_Nconc.exit28
	movq	%rax, %rdi
	callq	clause_NumberSort
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	list_Length
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB0_19
# BB#20:
	movslq	%ebp, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leal	(,%rbp,8), %edi
	movl	%edi, 44(%rsp)          # 4-byte Spill
	callq	memory_Malloc
	movq	%rbx, %rcx
	movq	%rax, %rbx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	testq	%rcx, %rcx
	je	.LBB0_23
# BB#21:                                # %.lr.ph.i15.preheader
	movq	%rbx, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph.i15
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	(%rcx), %rcx
	addq	$8, %rax
	testq	%rcx, %rcx
	jne	.LBB0_22
.LBB0_23:                               # %._crit_edge.i
	movl	$8, %edx
	movl	$pcheck_CompareClauseNumber, %ecx
	movq	%rbx, %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	qsort
	testl	%ebp, %ebp
	jle	.LBB0_24
# BB#25:                                # %.lr.ph111.preheader.i.i
	movl	%ebp, %r15d
	movq	%rbx, %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph111.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	movl	48(%rdx), %esi
	testb	$64, %sil
	je	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_26 Depth=1
	addl	$-64, %esi
	movl	%esi, 48(%rdx)
	movq	(%rax), %rdx
	movl	48(%rdx), %esi
.LBB0_28:                               # %clause_RemoveFlag.exit.i.i
                                        #   in Loop: Header=BB0_26 Depth=1
	testb	%sil, %sil
	jns	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_26 Depth=1
	addl	$-128, %esi
	movl	%esi, 48(%rdx)
.LBB0_30:                               # %clause_RemoveFlag.exit82.i.i
                                        #   in Loop: Header=BB0_26 Depth=1
	addq	$8, %rax
	decq	%rcx
	jne	.LBB0_26
# BB#31:                                # %.lr.ph106.i.i.preheader
	xorl	%ebp, %ebp
	xorl	%r14d, %r14d
	movq	%r15, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_32:                               # %.lr.ph106.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_36 Depth 2
                                        #       Child Loop BB0_38 Depth 3
                                        #         Child Loop BB0_39 Depth 4
                                        #     Child Loop BB0_49 Depth 2
                                        #     Child Loop BB0_52 Depth 2
	movq	(%rbx,%rbp,8), %rcx
	movl	48(%rcx), %eax
	testb	$64, %al
	jne	.LBB0_54
# BB#33:                                #   in Loop: Header=BB0_32 Depth=1
	orl	$64, %eax
	movl	%eax, 48(%rcx)
	leaq	40(%rcx), %rdx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	32(%rcx), %r13
	testq	%r13, %r13
	je	.LBB0_34
# BB#35:                                # %.lr.ph.split.i.i.preheader
                                        #   in Loop: Header=BB0_32 Depth=1
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, %r15
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph.split.i.i
                                        #   Parent Loop BB0_32 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_38 Depth 3
                                        #         Child Loop BB0_39 Depth 4
	movq	(%r15), %r15
	movslq	8(%r13), %rbp
	movq	8(%r15), %rdi
	xorl	%ecx, %ecx
	movq	72(%rsp), %rax          # 8-byte Reload
.LBB0_38:                               # %.lr.ph.i83.i.i
                                        #   Parent Loop BB0_32 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_39 Depth 4
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB0_39:                               #   Parent Loop BB0_32 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        #       Parent Loop BB0_38 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	(%rcx,%rdx), %rax
	shrq	%rax
	movq	(%rbx,%rax,8), %r12
	movl	%ebp, %esi
	subl	(%r12), %esi
	js	.LBB0_37
# BB#40:                                #   in Loop: Header=BB0_39 Depth=4
	testl	%esi, %esi
	je	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_39 Depth=4
	incq	%rax
	cmpq	%rdx, %rax
	movq	%rax, %rcx
	jb	.LBB0_39
	jmp	.LBB0_43
	.p2align	4, 0x90
.LBB0_37:                               # %.outer.loopexit.i.i.i
                                        #   in Loop: Header=BB0_38 Depth=3
	cmpq	%rax, %rcx
	jb	.LBB0_38
	jmp	.LBB0_43
	.p2align	4, 0x90
.LBB0_42:                               # %bsearch.exit.i.i
                                        #   in Loop: Header=BB0_36 Depth=2
	leaq	(%rbx,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB0_43
# BB#44:                                #   in Loop: Header=BB0_36 Depth=2
	movq	%rdi, %rbp
	movq	%r14, 80(%rsp)          # 8-byte Spill
	cmpb	$0, 48(%r12)
	jns	.LBB0_46
# BB#45:                                #   in Loop: Header=BB0_36 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	orb	$-128, 48(%rcx)
	movq	(%rax), %r12
.LBB0_46:                               # %._crit_edge129.i.i
                                        #   in Loop: Header=BB0_36 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r12, 8(%r14)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, (%r14)
	movslq	%ebp, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	80(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_47
	.p2align	4, 0x90
.LBB0_43:                               # %bsearch.exit.thread.i.i
                                        #   in Loop: Header=BB0_36 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	24(%rsp), %rcx          # 8-byte Reload
	orb	$-128, 48(%rcx)
	movq	%rax, %r14
.LBB0_47:                               #   in Loop: Header=BB0_36 Depth=2
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB0_36
# BB#48:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	32(%r13), %rax
	testq	%rax, %rax
	je	.LBB0_50
	.p2align	4, 0x90
.LBB0_49:                               # %.lr.ph.i80.i.i
                                        #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB0_49
.LBB0_50:                               #   in Loop: Header=BB0_32 Depth=1
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB0_51
.LBB0_34:                               #   in Loop: Header=BB0_32 Depth=1
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB0_51:                               # %list_Delete.exit81.i.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movq	%r14, %r12
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.LBB0_53
	.p2align	4, 0x90
.LBB0_52:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB0_52
.LBB0_53:                               # %list_Delete.exit.i.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	list_NReverse
	movq	%rax, %r14
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	list_NReverse
	movq	%r14, 32(%r13)
	movq	%rax, 40(%r13)
	movq	%r12, %r14
.LBB0_54:                               #   in Loop: Header=BB0_32 Depth=1
	incq	%rbp
	cmpq	%r15, %rbp
	jne	.LBB0_32
	jmp	.LBB0_55
.LBB0_19:
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	jne	.LBB0_64
	jmp	.LBB0_65
.LBB0_24:
	xorl	%r14d, %r14d
.LBB0_55:                               # %pcheck_ForceParentNumbersToPointersInVector.exit.i
	movl	44(%rsp), %esi          # 4-byte Reload
	cmpl	$1024, %esi             # imm = 0x400
	jae	.LBB0_56
# BB#61:
	movl	%esi, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rbx, (%rax)
	jmp	.LBB0_62
.LBB0_56:
	movl	memory_ALIGN(%rip), %ecx
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%ecx
	subl	%edx, %ecx
	testl	%edx, %edx
	cmovel	%edx, %ecx
	addl	%esi, %ecx
	movl	memory_OFFSET(%rip), %eax
	movq	%rbx, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %rdi
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rsi, %rbp
	movq	%rdi, (%rbp)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB0_58
# BB#57:
	negq	%rax
	movq	-16(%rbx,%rax), %rax
	movq	%rax, (%rdx)
.LBB0_58:
	addl	memory_MARKSIZE(%rip), %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB0_60
# BB#59:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB0_60:
	addq	$-16, %rbx
	movq	%rbx, %rdi
	callq	free
.LBB0_62:                               # %pcheck_ConvertParentsInList.exit
	movq	32(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB0_65
	.p2align	4, 0x90
.LBB0_64:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB0_64
.LBB0_65:                               # %list_Delete.exit
	movq	%r14, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	pcheck_ConvertParentsInSPASSProof, .Lfunc_end0-pcheck_ConvertParentsInSPASSProof
	.cfi_endproc

	.globl	pcheck_ClauseNumberMergeSort
	.p2align	4, 0x90
	.type	pcheck_ClauseNumberMergeSort,@function
pcheck_ClauseNumberMergeSort:           # @pcheck_ClauseNumberMergeSort
	.cfi_startproc
# BB#0:
	jmp	clause_NumberSort       # TAILCALL
.Lfunc_end1:
	.size	pcheck_ClauseNumberMergeSort, .Lfunc_end1-pcheck_ClauseNumberMergeSort
	.cfi_endproc

	.globl	pcheck_ParentPointersToParentNumbers
	.p2align	4, 0x90
	.type	pcheck_ParentPointersToParentNumbers,@function
pcheck_ParentPointersToParentNumbers:   # @pcheck_ParentPointersToParentNumbers
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB2_13
# BB#1:                                 # %.lr.ph.i.preheader
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movl	48(%rcx), %edx
	testb	$64, %dl
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	addl	$-64, %edx
	movl	%edx, 48(%rcx)
.LBB2_4:                                # %clause_RemoveFlag.exit.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB2_2
# BB#5:                                 # %pcheck_ClauseListRemoveFlag.exit.preheader
	testq	%rdi, %rdi
	je	.LBB2_13
# BB#6:                                 # %.lr.ph28.preheader
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph28
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_9 Depth 2
	movq	8(%rax), %rcx
	movl	48(%rcx), %edx
	testb	$64, %dl
	jne	.LBB2_12
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	movq	32(%rcx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_11
	.p2align	4, 0x90
.LBB2_9:                                # %.lr.ph
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rcx
	movslq	(%rcx), %rcx
	movq	%rcx, 8(%rsi)
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB2_9
# BB#10:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	8(%rax), %rcx
	movl	48(%rcx), %edx
.LBB2_11:                               # %._crit_edge
                                        #   in Loop: Header=BB2_7 Depth=1
	orl	$64, %edx
	movl	%edx, 48(%rcx)
.LBB2_12:                               # %pcheck_ClauseListRemoveFlag.exit
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB2_7
.LBB2_13:                               # %pcheck_ClauseListRemoveFlag.exit._crit_edge
	movq	%rdi, %rax
	retq
.Lfunc_end2:
	.size	pcheck_ParentPointersToParentNumbers, .Lfunc_end2-pcheck_ParentPointersToParentNumbers
	.cfi_endproc

	.globl	pcheck_ClauseListRemoveFlag
	.p2align	4, 0x90
	.type	pcheck_ClauseListRemoveFlag,@function
pcheck_ClauseListRemoveFlag:            # @pcheck_ClauseListRemoveFlag
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	jne	.LBB3_2
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_4:                                # %clause_RemoveFlag.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB3_5
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rax
	movl	48(%rax), %ecx
	testl	%esi, %ecx
	je	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	subl	%esi, %ecx
	movl	%ecx, 48(%rax)
	jmp	.LBB3_4
.LBB3_5:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	pcheck_ClauseListRemoveFlag, .Lfunc_end3-pcheck_ClauseListRemoveFlag
	.cfi_endproc

	.globl	pcheck_ConvertTermListToClauseList
	.p2align	4, 0x90
	.type	pcheck_ConvertTermListToClauseList,@function
pcheck_ConvertTermListToClauseList:     # @pcheck_ConvertTermListToClauseList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 112
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB4_1
# BB#2:                                 # %.lr.ph117
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_7 Depth 2
                                        #     Child Loop BB4_14 Depth 2
                                        #     Child Loop BB4_17 Depth 2
                                        #     Child Loop BB4_34 Depth 2
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	8(%r12), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r14
	movq	8(%rcx), %rdi
	movq	$0, 8(%rcx)
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r15
	movl	8(%rcx), %ebx
	movq	(%rcx), %rax
	movl	8(%rax), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$1, %esi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	dfg_CreateClauseFromTerm
	movq	%rax, %r13
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r13)
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	24(%rsp), %rdx
	callq	string_StringToInt
	testl	%eax, %eax
	je	.LBB4_79
# BB#4:                                 # %pcheck_LabelToNumber.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	24(%rsp), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	testq	%r15, %r15
	je	.LBB4_5
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r15), %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	28(%rsp), %rdx
	callq	string_StringToInt
	testl	%eax, %eax
	je	.LBB4_80
# BB#8:                                 # %pcheck_LabelToNumber.exit76
                                        #   in Loop: Header=BB4_7 Depth=2
	movslq	28(%rsp), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	%r14, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	$0, 8(%rbp)
	movq	%r12, (%rbp)
	movq	(%r15), %r15
	testq	%r15, %r15
	movq	%rbp, %r12
	movq	%rbx, %r14
	jne	.LBB4_7
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_3 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
.LBB4_9:                                # %._crit_edge
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%r13)
	movq	%rbx, %rdi
	callq	list_NReverse
	movq	%rax, 32(%r13)
	movq	%rbp, 40(%r13)
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, 76(%r13)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 12(%r13)
	movl	24(%r13), %ebp
	testl	%ecx, %ecx
	jle	.LBB4_38
# BB#10:                                #   in Loop: Header=BB4_3 Depth=1
	movl	%ebp, %edx
	decl	%edx
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	js	.LBB4_18
# BB#11:                                # %.lr.ph.i77
                                        #   in Loop: Header=BB4_3 Depth=1
	movslq	%edx, %rax
	andl	$3, %ebp
	je	.LBB4_12
# BB#13:                                # %.prol.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB4_14:                               #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rsi
	movq	$0, (%rsi,%rax,8)
	decq	%rax
	incl	%ebp
	jne	.LBB4_14
# BB#15:                                #   in Loop: Header=BB4_3 Depth=1
	movl	%eax, %esi
	cmpl	$3, %edx
	jae	.LBB4_17
	jmp	.LBB4_18
	.p2align	4, 0x90
.LBB4_38:                               #   in Loop: Header=BB4_3 Depth=1
	testl	%ebp, %ebp
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	je	.LBB4_48
# BB#39:                                #   in Loop: Header=BB4_3 Depth=1
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB4_47
# BB#40:                                #   in Loop: Header=BB4_3 Depth=1
	shll	$3, %ebp
	cmpl	$1024, %ebp             # imm = 0x400
	jae	.LBB4_41
# BB#46:                                #   in Loop: Header=BB4_3 Depth=1
	movl	%ebp, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB4_47
.LBB4_12:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%edx, %esi
	cmpl	$3, %edx
	jb	.LBB4_18
	.p2align	4, 0x90
.LBB4_17:                               #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rdx
	movq	$0, (%rdx,%rax,8)
	movq	16(%r13), %rdx
	movq	$0, -8(%rdx,%rax,8)
	movq	16(%r13), %rdx
	movq	$0, -16(%rdx,%rax,8)
	movq	16(%r13), %rdx
	movq	$0, -24(%rdx,%rax,8)
	addq	$-4, %rax
	addl	$-4, %esi
	jns	.LBB4_17
.LBB4_18:                               # %clause_ClearSplitField.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%ebx, %ebx
	cmpl	$64, %ecx
	jb	.LBB4_20
# BB#19:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB4_3 Depth=1
	addl	$-64, %ecx
	movl	%ecx, %ebx
	shrl	$6, %ebx
	movl	%ebx, %eax
	shll	$6, %eax
	incl	%ebx
	subl	%eax, %ecx
.LBB4_20:                               # %clause_ComputeSplitFieldAddress.exit.i
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	24(%r13), %eax
	cmpl	%eax, %ebx
	jb	.LBB4_37
# BB#21:                                #   in Loop: Header=BB4_3 Depth=1
	leal	1(%rbx), %ebp
	cmpl	%ebp, %eax
	jae	.LBB4_37
# BB#22:                                #   in Loop: Header=BB4_3 Depth=1
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	leal	(,%rbp,8), %edi
	callq	memory_Malloc
	movq	%rax, %r14
	cmpl	$0, 24(%r13)
	je	.LBB4_23
# BB#33:                                # %.lr.ph27.i.i
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_34:                               #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rax
	movl	%ecx, %edx
	movq	(%rax,%rdx,8), %rax
	movq	%rax, (%r14,%rdx,8)
	incl	%ecx
	movl	24(%r13), %eax
	cmpl	%eax, %ecx
	jb	.LBB4_34
	jmp	.LBB4_24
.LBB4_41:                               #   in Loop: Header=BB4_3 Depth=1
	xorl	%edx, %edx
	movl	%ebp, %eax
	movl	memory_ALIGN(%rip), %esi
	divl	%esi
	movl	%esi, %eax
	subl	%edx, %eax
	testl	%edx, %edx
	cmovel	%edx, %eax
	addl	%ebp, %eax
	movl	memory_OFFSET(%rip), %ecx
	movq	%rdi, %rdx
	subq	%rcx, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %rbp
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rsi, %rbx
	movq	%rbp, (%rbx)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB4_43
# BB#42:                                #   in Loop: Header=BB4_3 Depth=1
	negq	%rcx
	movq	-16(%rdi,%rcx), %rcx
	movq	%rcx, (%rdx)
.LBB4_43:                               #   in Loop: Header=BB4_3 Depth=1
	addl	memory_MARKSIZE(%rip), %eax
	movq	memory_FREEDBYTES(%rip), %rcx
	leaq	16(%rcx,%rax), %rcx
	movq	%rcx, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rcx
	testq	%rcx, %rcx
	js	.LBB4_45
# BB#44:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB4_45:                               #   in Loop: Header=BB4_3 Depth=1
	addq	$-16, %rdi
	callq	free
.LBB4_47:                               # %memory_Free.exit.i
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	$0, 16(%r13)
	movl	$0, 24(%r13)
	jmp	.LBB4_48
.LBB4_23:                               #   in Loop: Header=BB4_3 Depth=1
	xorl	%eax, %eax
.LBB4_24:                               # %.preheader.i.i
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpl	%ebp, %eax
	jae	.LBB4_26
# BB#25:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	%eax, %ecx
	leaq	(%r14,%rcx,8), %rdi
	movl	%ebx, %ecx
	subl	%eax, %ecx
	leaq	8(,%rcx,8), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB4_26:                               # %._crit_edge.i.i75
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB4_36
# BB#27:                                #   in Loop: Header=BB4_3 Depth=1
	movl	24(%r13), %ecx
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB4_28
# BB#35:                                #   in Loop: Header=BB4_3 Depth=1
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB4_36
.LBB4_28:                               #   in Loop: Header=BB4_3 Depth=1
	xorl	%edx, %edx
	movl	%ecx, %eax
	movl	memory_ALIGN(%rip), %esi
	divl	%esi
	movl	%esi, %eax
	subl	%edx, %eax
	testl	%edx, %edx
	cmovel	%edx, %eax
	addl	%ecx, %eax
	movl	memory_OFFSET(%rip), %r9d
	movq	%rdi, %rdx
	subq	%r9, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r8
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ecx
	cmoveq	%rcx, %rsi
	movq	%r8, (%rsi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB4_30
# BB#29:                                #   in Loop: Header=BB4_3 Depth=1
	negq	%r9
	movq	-16(%rdi,%r9), %rcx
	movq	%rcx, (%rdx)
.LBB4_30:                               #   in Loop: Header=BB4_3 Depth=1
	addl	memory_MARKSIZE(%rip), %eax
	movq	memory_FREEDBYTES(%rip), %rcx
	leaq	16(%rcx,%rax), %rcx
	movq	%rcx, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rcx
	testq	%rcx, %rcx
	js	.LBB4_32
# BB#31:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB4_32:                               #   in Loop: Header=BB4_3 Depth=1
	addq	$-16, %rdi
	callq	free
.LBB4_36:                               # %memory_Free.exit.i.i
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%r14, 16(%r13)
	movl	%ebp, 24(%r13)
	movl	4(%rsp), %ecx           # 4-byte Reload
.LBB4_37:                               # %clause_SetSplitFieldBit.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	16(%r13), %rax
	movl	%ebx, %edx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	orq	%rsi, (%rax,%rdx,8)
.LBB4_48:                               # %clause_SetSplitField.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	48(%r13), %eax
	testb	$64, %al
	je	.LBB4_50
# BB#49:                                #   in Loop: Header=BB4_3 Depth=1
	addl	$-64, %eax
	movl	%eax, 48(%r13)
.LBB4_50:                               # %clause_RemoveFlag.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r15, (%rax)
	movq	(%r12), %r12
	testq	%r12, %r12
	movq	%rax, %rcx
	jne	.LBB4_3
	jmp	.LBB4_51
.LBB4_1:
	xorl	%eax, %eax
.LBB4_51:                               # %._crit_edge118
	movq	%rax, %rdi
	callq	list_NReverse
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB4_52
# BB#53:                                # %.lr.ph.i
	movq	%rbp, %rdi
	callq	list_Length
	movl	%eax, %ebx
	leal	(,%rbx,8), %edi
	movl	%edi, 4(%rsp)           # 4-byte Spill
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB4_54:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	(%rbp), %rbp
	addq	$8, %rax
	testq	%rbp, %rbp
	jne	.LBB4_54
# BB#55:                                # %._crit_edge.i
	movslq	%ebx, %r12
	movl	$8, %edx
	movl	$pcheck_CompareClauseNumber, %ecx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	qsort
	testl	%ebx, %ebx
	jle	.LBB4_71
# BB#56:                                # %.lr.ph54.i.i.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_57:                               # %.lr.ph54.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_59 Depth 2
                                        #       Child Loop BB4_61 Depth 3
                                        #         Child Loop BB4_62 Depth 4
                                        #     Child Loop BB4_69 Depth 2
	movq	(%r15,%r14,8), %rax
	movq	32(%rax), %r13
	movq	%r13, %rdi
	callq	list_Copy
	testq	%rax, %rax
	je	.LBB4_68
# BB#58:                                # %.lr.ph.split.i.i.preheader
                                        #   in Loop: Header=BB4_57 Depth=1
	movq	%rax, %r8
	.p2align	4, 0x90
.LBB4_59:                               # %.lr.ph.split.i.i
                                        #   Parent Loop BB4_57 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_61 Depth 3
                                        #         Child Loop BB4_62 Depth 4
	movq	8(%r8), %rbx
	xorl	%esi, %esi
	movq	%r12, %rdx
.LBB4_61:                               # %.lr.ph.i36.i.i
                                        #   Parent Loop BB4_57 Depth=1
                                        #     Parent Loop BB4_59 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_62 Depth 4
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB4_62:                               #   Parent Loop BB4_57 Depth=1
                                        #     Parent Loop BB4_59 Depth=2
                                        #       Parent Loop BB4_61 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	(%rsi,%rdi), %rdx
	shrq	%rdx
	movq	(%r15,%rdx,8), %rcx
	movl	%ebx, %ebp
	subl	(%rcx), %ebp
	js	.LBB4_60
# BB#63:                                #   in Loop: Header=BB4_62 Depth=4
	testl	%ebp, %ebp
	je	.LBB4_66
# BB#64:                                #   in Loop: Header=BB4_62 Depth=4
	incq	%rdx
	cmpq	%rdi, %rdx
	movq	%rdx, %rsi
	jb	.LBB4_62
	jmp	.LBB4_65
	.p2align	4, 0x90
.LBB4_60:                               # %.outer.loopexit.i.i.i
                                        #   in Loop: Header=BB4_61 Depth=3
	cmpq	%rdx, %rsi
	jb	.LBB4_61
	jmp	.LBB4_65
	.p2align	4, 0x90
.LBB4_66:                               # %bsearch.exit.i.i
                                        #   in Loop: Header=BB4_59 Depth=2
	leaq	(%r15,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.LBB4_65
# BB#67:                                #   in Loop: Header=BB4_59 Depth=2
	movq	%rcx, 8(%r8)
	movq	(%r8), %r8
	testq	%r8, %r8
	jne	.LBB4_59
.LBB4_68:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB4_57 Depth=1
	movq	(%r15,%r14,8), %rcx
	movq	%rax, 32(%rcx)
	testq	%r13, %r13
	je	.LBB4_70
	.p2align	4, 0x90
.LBB4_69:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB4_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB4_69
.LBB4_70:                               # %list_Delete.exit.i.i
                                        #   in Loop: Header=BB4_57 Depth=1
	incq	%r14
	cmpq	%r12, %r14
	jl	.LBB4_57
.LBB4_71:                               # %pcheck_ParentNumbersToPointersInVector.exit.i
	movl	4(%rsp), %esi           # 4-byte Reload
	cmpl	$1024, %esi             # imm = 0x400
	jae	.LBB4_72
# BB#77:
	movl	%esi, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%r15, (%rax)
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB4_78
.LBB4_52:
	xorl	%ebx, %ebx
	jmp	.LBB4_78
.LBB4_72:
	movl	memory_ALIGN(%rip), %ecx
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%ecx
	subl	%edx, %ecx
	testl	%edx, %edx
	cmovel	%edx, %ecx
	addl	%esi, %ecx
	movl	memory_OFFSET(%rip), %eax
	movq	%r15, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %rdi
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rsi, %rbp
	movq	%rdi, (%rbp)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB4_74
# BB#73:
	negq	%rax
	movq	-16(%r15,%rax), %rax
	movq	%rax, (%rdx)
.LBB4_74:
	addl	memory_MARKSIZE(%rip), %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB4_76
# BB#75:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB4_76:
	addq	$-16, %r15
	movq	%r15, %rdi
	callq	free
.LBB4_78:                               # %pcheck_ParentNumbersToParents.exit
	movq	%rbx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_65:                               # %bsearch.exit.thread.i.i
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%r15,%r14,8), %rax
	movl	(%rax), %edx
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB4_80:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB4_79:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end4:
	.size	pcheck_ConvertTermListToClauseList, .Lfunc_end4-pcheck_ConvertTermListToClauseList
	.cfi_endproc

	.globl	pcheck_BuildTableauFromProof
	.p2align	4, 0x90
	.type	pcheck_BuildTableauFromProof,@function
pcheck_BuildTableauFromProof:           # @pcheck_BuildTableauFromProof
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB5_36
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movl	12(%rcx), %ecx
	cmpl	%ebp, %ecx
	cmovgel	%ecx, %ebp
	testq	%rax, %rax
	jne	.LBB5_2
# BB#3:                                 # %.lr.ph
	movl	$56, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, (%r14)
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	tab_PathCreate
	movq	%rax, %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_10 Depth 2
                                        #     Child Loop BB5_21 Depth 2
	movl	8(%r15), %ecx
	movq	8(%rbx), %r14
	movslq	12(%r14), %rbp
	movq	32(%r14), %rax
	testq	%rax, %rax
	movl	$0, %edx
	je	.LBB5_12
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB5_9
# BB#6:                                 #   in Loop: Header=BB5_4 Depth=1
	cmpl	$0, 68(%rdx)
	jne	.LBB5_9
# BB#7:                                 #   in Loop: Header=BB5_4 Depth=1
	cmpl	$0, 72(%rdx)
	jne	.LBB5_9
# BB#8:                                 # %pcheck_ClauseIsFromRightSplit.exit.i
                                        #   in Loop: Header=BB5_4 Depth=1
	cmpl	$0, 64(%rdx)
	je	.LBB5_15
	.p2align	4, 0x90
.LBB5_9:                                # %.lr.ph.i.i.i.preheader.i
                                        #   in Loop: Header=BB5_4 Depth=1
	movl	12(%rdx), %edx
	testl	%edx, %edx
	cmovsl	%r12d, %edx
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB5_12
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph.i.i.i..lr.ph.i.i.i_crit_edge.i
                                        #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rdi
	movq	(%rsi), %rsi
	movl	12(%rdi), %edi
	cmpl	%edx, %edi
	cmovgel	%edi, %edx
	testq	%rsi, %rsi
	jne	.LBB5_10
.LBB5_12:                               # %pcheck_ClauseIsFromSplit.exit
                                        #   in Loop: Header=BB5_4 Depth=1
	cmpl	%ebp, %edx
	jae	.LBB5_31
.LBB5_13:                               # %pcheck_ClauseIsFromSplit.exit.thread.thread
                                        #   in Loop: Header=BB5_4 Depth=1
	incl	%ecx
	cmpl	%ecx, %ebp
	jg	.LBB5_14
# BB#18:                                #   in Loop: Header=BB5_4 Depth=1
	leaq	-1(%rbp), %rcx
	testq	%rax, %rax
	movq	(%r15), %rdx
	movq	-8(%rdx,%rbp,8), %r13
	je	.LBB5_19
# BB#20:                                # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB5_4 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_21:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movl	12(%rsi), %esi
	cmpl	%edx, %esi
	cmovgel	%esi, %edx
	testq	%rax, %rax
	jne	.LBB5_21
	jmp	.LBB5_22
.LBB5_19:                               #   in Loop: Header=BB5_4 Depth=1
	xorl	%edx, %edx
.LBB5_22:                               # %pcheck_ClauseIsFromLeftSplit.exit
                                        #   in Loop: Header=BB5_4 Depth=1
	cmpl	%ebp, %edx
	jae	.LBB5_26
# BB#23:                                #   in Loop: Header=BB5_4 Depth=1
	cmpq	$0, 32(%r13)
	jne	.LBB5_24
# BB#25:                                #   in Loop: Header=BB5_4 Depth=1
	movl	%ecx, 8(%r15)
	movq	32(%r14), %rax
	movq	8(%rax), %rax
	movq	%rax, 8(%r13)
	movq	%r14, 16(%r13)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	tab_AddSplitAtCursor
	cmpl	8(%r15), %ebp
	jle	.LBB5_33
	jmp	.LBB5_32
	.p2align	4, 0x90
.LBB5_26:                               #   in Loop: Header=BB5_4 Depth=1
	cmpq	$0, 40(%r13)
	jne	.LBB5_30
# BB#27:                                #   in Loop: Header=BB5_4 Depth=1
	cmpq	$0, 32(%r13)
	je	.LBB5_28
# BB#29:                                #   in Loop: Header=BB5_4 Depth=1
	movl	%ecx, 8(%r15)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	tab_AddSplitAtCursor
.LBB5_30:                               #   in Loop: Header=BB5_4 Depth=1
	movq	24(%r13), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, 24(%r13)
	xorl	%r12d, %r12d
.LBB5_31:                               #   in Loop: Header=BB5_4 Depth=1
	cmpl	8(%r15), %ebp
	jg	.LBB5_32
.LBB5_33:                               #   in Loop: Header=BB5_4 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	tab_AddClauseOnItsLevel
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_4
	jmp	.LBB5_34
.LBB5_15:                               # %pcheck_ClauseIsFromSplit.exit.thread
                                        #   in Loop: Header=BB5_4 Depth=1
	testl	%ebp, %ebp
	jne	.LBB5_13
# BB#16:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	(%r14), %esi
	movl	$.L.str, %edi
	jmp	.LBB5_17
.LBB5_34:                               # %._crit_edge
	movq	%r15, %rdi
	callq	tab_PathDelete
	jmp	.LBB5_35
.LBB5_36:
	movq	$0, (%r14)
.LBB5_35:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_32:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	(%r14), %esi
	movl	$.L.str.5, %edi
	jmp	.LBB5_17
.LBB5_14:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	(%r14), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB5_24:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	8(%r13), %rax
	movl	(%rax), %esi
	movl	$.L.str.3, %edi
	jmp	.LBB5_17
.LBB5_28:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	(%r14), %esi
	movl	$.L.str.4, %edi
.LBB5_17:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end5:
	.size	pcheck_BuildTableauFromProof, .Lfunc_end5-pcheck_BuildTableauFromProof
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	misc_Error, .Lfunc_end6-misc_Error
	.cfi_endproc

	.globl	pcheck_TableauProof
	.p2align	4, 0x90
	.type	pcheck_TableauProof,@function
pcheck_TableauProof:                    # @pcheck_TableauProof
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rdi
	callq	tab_LabelNodes
	cmpl	$0, pcheck_GenNamedCg(%rip)
	je	.LBB7_2
# BB#1:
	movq	(%r14), %rdi
	movq	pcheck_CgName(%rip), %rsi
	movl	pcheck_GraphFormat(%rip), %edx
	callq	tab_WriteTableau
.LBB7_2:
	movq	$0, 16(%rsp)
	cmpl	$0, pcheck_Quiet(%rip)
	jne	.LBB7_4
# BB#3:
	movq	stdout(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB7_4:
	movq	(%r14), %rdi
	leaq	16(%rsp), %rsi
	callq	tab_PruneClosedBranches
	movq	%rax, (%r14)
	cmpl	$0, pcheck_Quiet(%rip)
	jne	.LBB7_7
# BB#5:
	movl	$.L.str.7, %edi
	callq	puts
	cmpl	$0, pcheck_Quiet(%rip)
	jne	.LBB7_7
# BB#6:
	movq	stdout(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB7_7:                                # %.thread
	movq	(%r14), %rdi
	leaq	16(%rsp), %rsi
	callq	tab_RemoveIncompleteSplits
	movq	%rax, (%r14)
	cmpl	$0, pcheck_Quiet(%rip)
	jne	.LBB7_9
# BB#8:
	movl	$.L.str.7, %edi
	callq	puts
.LBB7_9:
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB7_11
	.p2align	4, 0x90
.LBB7_10:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB7_10
.LBB7_11:                               # %list_Delete.exit
	movq	$0, 8(%rsp)
	movq	(%r14), %rdi
	leaq	8(%rsp), %rsi
	callq	tab_GetEarliestEmptyClauses
	testq	%rbx, %rbx
	jne	.LBB7_13
	jmp	.LBB7_16
	.p2align	4, 0x90
.LBB7_15:                               # %clause_RemoveFlag.exit.i
                                        #   in Loop: Header=BB7_13 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB7_16
.LBB7_13:                               # %.lr.ph.i16
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movl	48(%rax), %ecx
	testb	$64, %cl
	je	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_13 Depth=1
	addl	$-64, %ecx
	movl	%ecx, 48(%rax)
	jmp	.LBB7_15
.LBB7_16:                               # %pcheck_ClauseListRemoveFlag.exit
	movq	8(%rsp), %rdi
	callq	pcheck_MarkRecursive
	movq	$0, (%rsp)
	movq	(%r14), %rdi
	movq	%rsp, %rsi
	callq	pcheck_CollectUnmarkedSplits
	movq	(%rsp), %rdi
	callq	pcheck_MarkRecursive
	movq	(%r14), %rdi
	callq	pcheck_RemoveUnmarkedFromTableau
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB7_18
	.p2align	4, 0x90
.LBB7_17:                               # %.lr.ph.i20
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB7_17
.LBB7_18:                               # %list_Delete.exit21
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB7_20
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph.i25
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB7_19
.LBB7_20:                               # %list_Delete.exit26
	cmpl	$0, pcheck_GenRedCg(%rip)
	je	.LBB7_22
# BB#21:
	movq	(%r14), %rdi
	movq	pcheck_RedCgName(%rip), %rsi
	movl	pcheck_GraphFormat(%rip), %edx
	callq	tab_WriteTableau
.LBB7_22:
	movq	(%r14), %rdi
	callq	tab_SetSplitLevels
	movq	(%r14), %rdi
	callq	pcheck_SplitLevels
	movq	(%r14), %rdi
	callq	tab_CheckEmpties
	movq	(%r14), %rdi
	callq	tab_IsClosed
	testl	%eax, %eax
	je	.LBB7_23
# BB#24:
	cmpl	$0, pcheck_Quiet(%rip)
	jne	.LBB7_26
# BB#25:
	movq	stdout(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB7_26:
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	tab_Depth
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	tab_PathCreate
	movq	%rax, %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	pcheck_TableauJustificationsRec
	movq	%r14, %rdi
	callq	tab_PathDelete
	movl	$1, %ebx
	cmpl	$0, pcheck_Quiet(%rip)
	jne	.LBB7_28
# BB#27:
	movl	$.L.str.7, %edi
	callq	puts
	jmp	.LBB7_28
.LBB7_23:
	movl	$.L.str.9, %edi
	callq	puts
	xorl	%ebx, %ebx
.LBB7_28:
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	pcheck_TableauProof, .Lfunc_end7-pcheck_TableauProof
	.cfi_endproc

	.globl	pcheck_MarkRecursive
	.p2align	4, 0x90
	.type	pcheck_MarkRecursive,@function
pcheck_MarkRecursive:                   # @pcheck_MarkRecursive
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 32
.Lcfi48:
	.cfi_offset %rbx, -24
.Lcfi49:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB8_2
	jmp	.LBB8_5
	.p2align	4, 0x90
.LBB8_4:                                #   in Loop: Header=BB8_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB8_5
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	testb	$64, 48(%r14)
	jne	.LBB8_4
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	32(%r14), %rdi
	callq	pcheck_MarkRecursive
	orb	$64, 48(%r14)
	jmp	.LBB8_4
.LBB8_5:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	pcheck_MarkRecursive, .Lfunc_end8-pcheck_MarkRecursive
	.cfi_endproc

	.p2align	4, 0x90
	.type	pcheck_CollectUnmarkedSplits,@function
pcheck_CollectUnmarkedSplits:           # @pcheck_CollectUnmarkedSplits
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 48
.Lcfi55:
	.cfi_offset %rbx, -48
.Lcfi56:
	.cfi_offset %r12, -40
.Lcfi57:
	.cfi_offset %r13, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	jne	.LBB9_2
	jmp	.LBB9_9
	.p2align	4, 0x90
.LBB9_8:                                # %tailrecurse
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	32(%r14), %rdi
	movq	%r15, %rsi
	callq	pcheck_CollectUnmarkedSplits
	movq	40(%r14), %r14
	testq	%r14, %r14
	je	.LBB9_9
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_4 Depth 2
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_4
	jmp	.LBB9_8
	.p2align	4, 0x90
.LBB9_7:                                #   in Loop: Header=BB9_4 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB9_8
.LBB9_4:                                # %.lr.ph
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %r12
	testb	$64, 48(%r12)
	jne	.LBB9_7
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=2
	cmpl	$15, 76(%r12)
	jne	.LBB9_7
# BB#6:                                 #   in Loop: Header=BB9_4 Depth=2
	movq	(%r15), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, (%r15)
	jmp	.LBB9_7
.LBB9_9:                                # %tailrecurse._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	pcheck_CollectUnmarkedSplits, .Lfunc_end9-pcheck_CollectUnmarkedSplits
	.cfi_endproc

	.p2align	4, 0x90
	.type	pcheck_RemoveUnmarkedFromTableau,@function
pcheck_RemoveUnmarkedFromTableau:       # @pcheck_RemoveUnmarkedFromTableau
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 16
.Lcfi61:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB10_3
	.p2align	4, 0x90
.LBB10_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movl	$pcheck_ClauseIsUnmarked, %esi
	callq	list_DeleteElementIf
	movq	%rax, (%rbx)
	movq	32(%rbx), %rdi
	callq	pcheck_RemoveUnmarkedFromTableau
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB10_1
.LBB10_3:                               # %tailrecurse._crit_edge
	popq	%rbx
	retq
.Lfunc_end10:
	.size	pcheck_RemoveUnmarkedFromTableau, .Lfunc_end10-pcheck_RemoveUnmarkedFromTableau
	.cfi_endproc

	.p2align	4, 0x90
	.type	pcheck_SplitLevels,@function
pcheck_SplitLevels:                     # @pcheck_SplitLevels
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 32
.Lcfi65:
	.cfi_offset %rbx, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	jne	.LBB11_2
	jmp	.LBB11_11
	.p2align	4, 0x90
.LBB11_10:                              # %tailrecurse
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	40(%r14), %rdi
	callq	pcheck_SplitLevels
	movq	32(%r14), %r14
	testq	%r14, %r14
	je	.LBB11_11
.LBB11_2:                               # %.lr.ph31
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_4 Depth 2
                                        #       Child Loop BB11_7 Depth 3
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.LBB11_4
	jmp	.LBB11_10
	.p2align	4, 0x90
.LBB11_9:                               #   in Loop: Header=BB11_4 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB11_10
.LBB11_4:                               # %.lr.ph
                                        #   Parent Loop BB11_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_7 Depth 3
	movq	8(%rax), %rbx
	movq	32(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB11_9
# BB#5:                                 #   in Loop: Header=BB11_4 Depth=2
	cmpl	$15, 76(%rbx)
	je	.LBB11_9
# BB#6:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB11_4 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_7:                               # %.lr.ph.i.i
                                        #   Parent Loop BB11_2 Depth=1
                                        #     Parent Loop BB11_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %rdx
	movq	(%rcx), %rcx
	movl	12(%rdx), %edx
	cmpl	%ebp, %edx
	cmovgel	%edx, %ebp
	testq	%rcx, %rcx
	jne	.LBB11_7
# BB#8:                                 # %pcheck_MaxParentSplitLevel.exit
                                        #   in Loop: Header=BB11_4 Depth=2
	cmpl	%ebp, 12(%rbx)
	je	.LBB11_9
# BB#12:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	(%rbx), %esi
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB11_11:                              # %tailrecurse._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	pcheck_SplitLevels, .Lfunc_end11-pcheck_SplitLevels
	.cfi_endproc

	.globl	pcheck_TableauToProofTask
	.p2align	4, 0x90
	.type	pcheck_TableauToProofTask,@function
pcheck_TableauToProofTask:              # @pcheck_TableauToProofTask
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 96
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB12_2
	jmp	.LBB12_39
	.p2align	4, 0x90
.LBB12_38:                              # %tailrecurse
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	40(%rbx), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	pcheck_TableauToProofTask
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB12_39
.LBB12_2:                               # %.lr.ph61
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_11 Depth 2
                                        #     Child Loop BB12_18 Depth 2
                                        #     Child Loop BB12_22 Depth 2
                                        #     Child Loop BB12_28 Depth 2
                                        #       Child Loop BB12_33 Depth 3
                                        #       Child Loop BB12_35 Depth 3
	cmpq	$0, 40(%rbx)
	movq	%rbx, %rbp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jne	.LBB12_4
# BB#3:                                 # %tab_IsLeaf.exit
                                        #   in Loop: Header=BB12_2 Depth=1
	cmpq	$0, 32(%rbp)
	je	.LBB12_26
.LBB12_4:                               # %tab_IsLeaf.exit.thread
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	8(%rbp), %rdi
	callq	pcheck_ClauseToTerm
	movq	%rax, %rbx
	movq	16(%rbp), %rdi
	callq	pcheck_ClauseToTerm
	movq	%rax, %r15
	movq	24(%rbp), %r13
	testq	%r13, %r13
	je	.LBB12_26
# BB#5:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	8(%r13), %r14
	movq	32(%r14), %rdi
	callq	list_Copy
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	list_Length
	cmpl	$2, %eax
	jne	.LBB12_15
# BB#6:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	8(%rbp), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB12_11
# BB#7:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpl	$0, 68(%rcx)
	jne	.LBB12_11
# BB#8:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpl	$0, 72(%rcx)
	jne	.LBB12_11
# BB#9:                                 # %clause_IsEmptyClause.exit.i.i
                                        #   in Loop: Header=BB12_2 Depth=1
	cmpl	$0, 64(%rcx)
	jne	.LBB12_11
# BB#10:                                # %.thread.i.i
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	(%rbp), %rax
	movq	8(%rax), %rsi
	movl	$-1, %edx
	movl	$-1, %ecx
	movq	%r14, %rdi
	callq	subs_Subsumes
	jmp	.LBB12_11
	.p2align	4, 0x90
.LBB12_15:                              # %clause_IsEmptyClause.exit.thread.i.i
                                        #   in Loop: Header=BB12_2 Depth=1
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB12_16
	.p2align	4, 0x90
.LBB12_11:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbp, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbp
	jne	.LBB12_11
# BB#12:                                # %pcheck_IsRightSplitHalf.exit.i
                                        #   in Loop: Header=BB12_2 Depth=1
	testl	%eax, %eax
	je	.LBB12_14
# BB#13:                                #   in Loop: Header=BB12_2 Depth=1
	movq	(%r13), %r13
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	24(%rbx), %rax
	movq	8(%rax), %rdi
	callq	pcheck_ClauseToTerm
	movq	%rax, %rbp
	movl	fol_OR(%rip), %r12d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbp, 8(%r14)
	movq	$0, (%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, (%rax)
	movl	%r12d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r15
	movl	fol_EQUIV(%rip), %r14d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbp, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbp, 8(%r12)
	movq	$0, (%r12)
	movq	16(%rbx), %rax
	movl	(%rax), %edi
	callq	string_IntToString
	movq	%rax, %r14
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	callq	pcheck_GenericFilename
	movq	%rax, %rbx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	string_Conc
	movq	%rax, %rbp
	movl	$.L.str.19, %esi
	movq	%rbp, %rdi
	callq	misc_OpenFile
	movq	%rax, %r15
	movq	%r12, (%rsp)
	movl	$.L.str.20, %esi
	movl	$.L.str.21, %edx
	movl	$.L.str.22, %ecx
	movl	$.L.str.23, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	callq	fol_FPrintDFGProblem
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	misc_CloseFile
	movq	%r14, %rdi
	callq	string_StringFree
	movq	%rbx, %rdi
	callq	string_StringFree
	movq	%rbp, %rdi
	callq	string_StringFree
	movl	$term_Delete, %esi
	movq	%r12, %rdi
	callq	list_DeleteWithElement
.LBB12_14:                              #   in Loop: Header=BB12_2 Depth=1
	testq	%r13, %r13
	movq	8(%rsp), %rbp           # 8-byte Reload
	jne	.LBB12_17
	jmp	.LBB12_26
.LBB12_16:                              #   in Loop: Header=BB12_2 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB12_17:                              # %.lr.ph.i.sink.split.i
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	16(%rbp), %rdi
	callq	pcheck_ClauseToTerm
	movq	%rax, %r14
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB12_18:                              # %.lr.ph.i.i
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rbx
	movq	8(%r13), %rdi
	callq	pcheck_ClauseToTerm
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB12_18
# BB#19:                                # %pcheck_ClauseListToTermList.exit.i
                                        #   in Loop: Header=BB12_2 Depth=1
	testq	%rbx, %rbx
	je	.LBB12_20
# BB#23:                                #   in Loop: Header=BB12_2 Depth=1
	movl	fol_AND(%rip), %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
.LBB12_24:                              # %list_Delete.exit.i
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB12_25
.LBB12_20:                              #   in Loop: Header=BB12_2 Depth=1
	testq	%rax, %rax
	je	.LBB12_24
# BB#21:                                # %.lr.ph.i67.i.preheader
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB12_22:                              # %.lr.ph.i67.i
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB12_22
.LBB12_25:                              # %list_Delete.exit.i
                                        #   in Loop: Header=BB12_2 Depth=1
	movl	fol_NOT(%rip), %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movl	fol_IMPLIES(%rip), %r15d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r14, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movl	%r15d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbp, 8(%r15)
	movq	$0, (%r15)
	movq	24(%r12), %rax
	movq	8(%rax), %rax
	movl	(%rax), %edi
	callq	string_IntToString
	movq	%rax, %r14
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	callq	pcheck_GenericFilename
	movq	%r12, %rbp
	movq	%rax, %r13
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	string_Conc
	movq	%rax, %r12
	movl	$.L.str.19, %esi
	movq	%r12, %rdi
	callq	misc_OpenFile
	movq	%rax, %rbx
	movq	%r15, (%rsp)
	movl	$.L.str.20, %esi
	movl	$.L.str.21, %edx
	movl	$.L.str.22, %ecx
	movl	$.L.str.23, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rdi
	callq	fol_FPrintDFGProblem
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	misc_CloseFile
	movq	%r14, %rdi
	callq	string_StringFree
	movq	%r13, %rdi
	callq	string_StringFree
	movq	%r12, %rdi
	callq	string_StringFree
	movl	$term_Delete, %esi
	movq	%r15, %rdi
	callq	list_DeleteWithElement
.LBB12_26:                              # %pcheck_SplitToProblems.exit
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	(%rbp), %r13
	testq	%r13, %r13
	jne	.LBB12_28
	jmp	.LBB12_38
	.p2align	4, 0x90
.LBB12_37:                              #   in Loop: Header=BB12_28 Depth=2
	movq	(%r13), %r13
	testq	%r13, %r13
	je	.LBB12_38
.LBB12_28:                              # %.lr.ph
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_33 Depth 3
                                        #       Child Loop BB12_35 Depth 3
	movq	8(%r13), %rbp
	cmpl	$15, 76(%rbp)
	je	.LBB12_37
# BB#29:                                #   in Loop: Header=BB12_28 Depth=2
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_37
# BB#30:                                #   in Loop: Header=BB12_28 Depth=2
	callq	list_Copy
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB12_31
# BB#32:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB12_28 Depth=2
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB12_33:                              # %.lr.ph.i
                                        #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rdi
	callq	pcheck_ClauseToTerm
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r12, 8(%r14)
	movq	%r15, (%r14)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%r14, %r15
	jne	.LBB12_33
# BB#34:                                # %.lr.ph.i49.preheader
                                        #   in Loop: Header=BB12_28 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_35:                              # %.lr.ph.i49
                                        #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB12_35
	jmp	.LBB12_36
.LBB12_31:                              #   in Loop: Header=BB12_28 Depth=2
	xorl	%r14d, %r14d
.LBB12_36:                              # %list_Delete.exit
                                        #   in Loop: Header=BB12_28 Depth=2
	movq	%rbp, %rdi
	callq	pcheck_ClauseToTerm
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	$0, (%r15)
	movl	(%rbp), %edi
	callq	string_IntToString
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	pcheck_GenericFilename
	movq	%rax, %rbx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	string_Conc
	movq	%rax, %rbp
	movl	$.L.str.19, %esi
	movq	%rbp, %rdi
	callq	misc_OpenFile
	movq	%rax, %r12
	movq	%r15, (%rsp)
	movl	$.L.str.20, %esi
	movl	$.L.str.21, %edx
	movl	$.L.str.22, %ecx
	movl	$.L.str.23, %r8d
	movq	%r12, %rdi
	movq	%r14, %r9
	callq	fol_FPrintDFGProblem
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	misc_CloseFile
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	string_StringFree
	movq	%rbx, %rdi
	callq	string_StringFree
	movq	%rbp, %rdi
	callq	string_StringFree
	movl	$term_Delete, %esi
	movq	%r14, %rdi
	callq	list_DeleteWithElement
	movl	$term_Delete, %esi
	movq	%r15, %rdi
	callq	list_DeleteWithElement
	jmp	.LBB12_37
.LBB12_39:                              # %tailrecurse._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	pcheck_TableauToProofTask, .Lfunc_end12-pcheck_TableauToProofTask
	.cfi_endproc

	.p2align	4, 0x90
	.type	pcheck_ClauseToTerm,@function
pcheck_ClauseToTerm:                    # @pcheck_ClauseToTerm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 48
.Lcfi86:
	.cfi_offset %rbx, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	64(%r14), %eax
	addl	68(%r14), %eax
	addl	72(%r14), %eax
	decl	%eax
	js	.LBB13_4
# BB#1:                                 # %.lr.ph
	movq	$-1, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%rbx, (%rax)
	movl	64(%r14), %ecx
	movl	72(%r14), %edx
	addl	68(%r14), %ecx
	leal	-1(%rdx,%rcx), %ecx
	movslq	%ecx, %rcx
	incq	%rbp
	cmpq	%rcx, %rbp
	movq	%rax, %rbx
	jl	.LBB13_2
# BB#3:                                 # %._crit_edge
	testq	%rax, %rax
	jne	.LBB13_5
.LBB13_4:                               # %._crit_edge.thread
	movl	fol_FALSE(%rip), %edi
	xorl	%esi, %esi
	callq	term_Create
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
.LBB13_5:
	cmpq	$0, (%rax)
	je	.LBB13_6
# BB#13:
	movl	fol_OR(%rip), %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r14
	jmp	.LBB13_8
.LBB13_6:
	movq	8(%rax), %r14
	testq	%rax, %rax
	je	.LBB13_8
# BB#7:                                 # %list_Delete.exit.loopexit
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
.LBB13_8:                               # %list_Delete.exit
	movq	%r14, %rdi
	callq	term_VariableSymbols
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB13_12
# BB#9:                                 # %.lr.ph.i.i.preheader
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB13_10:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %edi
	xorl	%esi, %esi
	callq	term_Create
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_10
# BB#11:                                # %pcheck_CollectTermVariables.exit.i
	movl	fol_ALL(%rip), %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fol_CreateQuantifier    # TAILCALL
.LBB13_12:                              # %pcheck_UnivClosure.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	pcheck_ClauseToTerm, .Lfunc_end13-pcheck_ClauseToTerm
	.cfi_endproc

	.globl	pcheck_SeqProofDepth
	.p2align	4, 0x90
	.type	pcheck_SeqProofDepth,@function
pcheck_SeqProofDepth:                   # @pcheck_SeqProofDepth
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB14_3
	.p2align	4, 0x90
.LBB14_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdi
	movl	8(%rcx), %ecx
	cmpl	%eax, %ecx
	cmoval	%ecx, %eax
	testq	%rdi, %rdi
	jne	.LBB14_1
.LBB14_3:                               # %._crit_edge
	retq
.Lfunc_end14:
	.size	pcheck_SeqProofDepth, .Lfunc_end14-pcheck_SeqProofDepth
	.cfi_endproc

	.globl	pcheck_ReduceSPASSProof
	.p2align	4, 0x90
	.type	pcheck_ReduceSPASSProof,@function
pcheck_ReduceSPASSProof:                # @pcheck_ReduceSPASSProof
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi92:
	.cfi_def_cfa_offset 64
.Lcfi93:
	.cfi_offset %rbx, -24
.Lcfi94:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	24(%rsp), %rsi
	callq	pcheck_BuildTableauFromProof
	movq	$0, 32(%rsp)
	movq	24(%rsp), %rdi
	leaq	32(%rsp), %r14
	movq	%r14, %rsi
	callq	tab_PruneClosedBranches
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	tab_RemoveIncompleteSplits
	movq	%rax, %r14
	movq	%r14, 24(%rsp)
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.LBB15_2
	.p2align	4, 0x90
.LBB15_1:                               # %.lr.ph.i5
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_1
.LBB15_2:                               # %list_Delete.exit6
	movq	%r14, %rdi
	callq	tab_SetSplitLevels
	movq	$0, 16(%rsp)
	leaq	16(%rsp), %rsi
	movq	%r14, %rdi
	callq	tab_GetEarliestEmptyClauses
	testq	%rbx, %rbx
	jne	.LBB15_4
	jmp	.LBB15_7
	.p2align	4, 0x90
.LBB15_6:                               # %clause_RemoveFlag.exit.i
                                        #   in Loop: Header=BB15_4 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB15_7
.LBB15_4:                               # %.lr.ph.i12
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movl	48(%rax), %ecx
	testb	$64, %cl
	je	.LBB15_6
# BB#5:                                 #   in Loop: Header=BB15_4 Depth=1
	addl	$-64, %ecx
	movl	%ecx, 48(%rax)
	jmp	.LBB15_6
.LBB15_7:                               # %pcheck_ClauseListRemoveFlag.exit
	movq	16(%rsp), %rdi
	callq	pcheck_MarkRecursive
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	pcheck_CollectUnmarkedSplits
	movq	8(%rsp), %rdi
	callq	pcheck_MarkRecursive
	movq	%r14, %rdi
	callq	pcheck_RemoveUnmarkedFromTableau
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB15_9
	.p2align	4, 0x90
.LBB15_8:                               # %.lr.ph.i10
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_8
.LBB15_9:                               # %list_Delete.exit11
	movq	$0, (%rsp)
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	tab_ToClauseList
	movq	(%rsp), %rdi
	callq	clause_NumberSort
	movq	%rax, (%rsp)
	movq	%r14, %rdi
	callq	tab_Delete
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB15_11
	.p2align	4, 0x90
.LBB15_10:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_10
.LBB15_11:                              # %list_Delete.exit
	movq	(%rsp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	pcheck_ReduceSPASSProof, .Lfunc_end15-pcheck_ReduceSPASSProof
	.cfi_endproc

	.globl	pcheck_DeleteProof
	.p2align	4, 0x90
	.type	pcheck_DeleteProof,@function
pcheck_DeleteProof:                     # @pcheck_DeleteProof
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 48
.Lcfi100:
	.cfi_offset %rbx, -40
.Lcfi101:
	.cfi_offset %r12, -32
.Lcfi102:
	.cfi_offset %r14, -24
.Lcfi103:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB16_12
# BB#1:                                 # %.lr.ph43.preheader
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB16_2:                               # %.lr.ph43
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_5 Depth 2
                                        #     Child Loop BB16_7 Depth 2
                                        #     Child Loop BB16_9 Depth 2
	movq	8(%r15), %r12
	movq	8(%r12), %rdi
	callq	string_StringFree
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB16_4
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	callq	term_Delete
	movq	(%r12), %rax
.LBB16_4:                               #   in Loop: Header=BB16_2 Depth=1
	movq	(%rax), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB16_9
	.p2align	4, 0x90
.LBB16_5:                               # %.lr.ph
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	string_StringFree
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB16_5
# BB#6:                                 # %._crit_edge
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.LBB16_9
	.p2align	4, 0x90
.LBB16_7:                               # %.lr.ph.i34
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB16_7
# BB#8:                                 # %list_Delete.exit35
                                        #   in Loop: Header=BB16_2 Depth=1
	testq	%r12, %r12
	je	.LBB16_10
	.p2align	4, 0x90
.LBB16_9:                               # %.lr.ph.i29
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB16_9
.LBB16_10:                              # %list_Delete.exit30
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB16_2
	.p2align	4, 0x90
.LBB16_11:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB16_11
.LBB16_12:                              # %list_Delete.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	pcheck_DeleteProof, .Lfunc_end16-pcheck_DeleteProof
	.cfi_endproc

	.globl	pcheck_GenericFilename
	.p2align	4, 0x90
	.type	pcheck_GenericFilename,@function
pcheck_GenericFilename:                 # @pcheck_GenericFilename
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -32
.Lcfi108:
	.cfi_offset %r14, -24
.Lcfi109:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$.L.str.12, %edi
	callq	string_Conc
	movq	%rax, %r15
	movq	pcheck_ProofFileSuffix(%rip), %rsi
	movq	%r15, %rdi
	callq	string_Conc
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	string_StringFree
	xorl	%esi, %esi
	cmpb	$46, (%rbx)
	je	.LBB17_5
# BB#1:                                 # %.lr.ph
	movq	%rbx, %rdi
	callq	strlen
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB17_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %rcx
	jae	.LBB17_4
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	leaq	1(%rcx), %rsi
	cmpb	$46, 1(%rbx,%rcx)
	movq	%rsi, %rcx
	jne	.LBB17_2
	jmp	.LBB17_5
.LBB17_4:                               # %..critedge.loopexit_crit_edge
	movl	%ecx, %esi
.LBB17_5:                               # %.critedge
	movq	%rbx, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	string_Prefix
	movq	%rax, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	string_Nconc            # TAILCALL
.Lfunc_end17:
	.size	pcheck_GenericFilename, .Lfunc_end17-pcheck_GenericFilename
	.cfi_endproc

	.p2align	4, 0x90
	.type	pcheck_CompareClauseNumber,@function
pcheck_CompareClauseNumber:             # @pcheck_CompareClauseNumber
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	(%rax), %eax
	movq	(%rsi), %rcx
	subl	(%rcx), %eax
	retq
.Lfunc_end18:
	.size	pcheck_CompareClauseNumber, .Lfunc_end18-pcheck_CompareClauseNumber
	.cfi_endproc

	.p2align	4, 0x90
	.type	pcheck_ClauseIsUnmarked,@function
pcheck_ClauseIsUnmarked:                # @pcheck_ClauseIsUnmarked
	.cfi_startproc
# BB#0:
	movl	48(%rdi), %eax
	shrl	$6, %eax
	notl	%eax
	andl	$1, %eax
	retq
.Lfunc_end19:
	.size	pcheck_ClauseIsUnmarked, .Lfunc_end19-pcheck_ClauseIsUnmarked
	.cfi_endproc

	.p2align	4, 0x90
	.type	pcheck_TableauJustificationsRec,@function
pcheck_TableauJustificationsRec:        # @pcheck_TableauJustificationsRec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 64
.Lcfi117:
	.cfi_offset %rbx, -56
.Lcfi118:
	.cfi_offset %r12, -48
.Lcfi119:
	.cfi_offset %r13, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB20_26
# BB#1:
	movq	(%r15), %rax
	movslq	8(%r15), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	jne	.LBB20_3
	jmp	.LBB20_22
	.p2align	4, 0x90
.LBB20_10:                              # %.lr.ph.split
                                        #   Parent Loop BB20_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB20_15 Depth 3
	movq	8(%rbx), %r13
	testq	%r13, %r13
	je	.LBB20_14
# BB#11:                                #   in Loop: Header=BB20_10 Depth=2
	cmpl	$0, 68(%r13)
	jne	.LBB20_14
# BB#12:                                #   in Loop: Header=BB20_10 Depth=2
	cmpl	$0, 72(%r13)
	jne	.LBB20_14
# BB#13:                                # %clause_IsEmptyClause.exit
                                        #   in Loop: Header=BB20_10 Depth=2
	cmpl	$0, 64(%r13)
	je	.LBB20_18
	.p2align	4, 0x90
.LBB20_14:                              # %clause_IsEmptyClause.exit.thread
                                        #   in Loop: Header=BB20_10 Depth=2
	movl	12(%r13), %eax
	movq	32(%r13), %rdx
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB20_17
	.p2align	4, 0x90
.LBB20_15:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB20_3 Depth=1
                                        #     Parent Loop BB20_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdx
	movl	12(%rsi), %esi
	cmpl	%ecx, %esi
	cmovgel	%esi, %ecx
	testq	%rdx, %rdx
	jne	.LBB20_15
.LBB20_17:                              # %pcheck_ClauseIsFromLeftSplit.exit
                                        #   in Loop: Header=BB20_10 Depth=2
	cmpl	%eax, %ecx
	jae	.LBB20_19
.LBB20_18:                              #   in Loop: Header=BB20_10 Depth=2
	movl	(%r13), %eax
	cmpl	(%rbp), %eax
	jle	.LBB20_20
.LBB20_19:                              # %.critedge
                                        #   in Loop: Header=BB20_10 Depth=2
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	tab_PathContainsClause
	testl	%eax, %eax
	je	.LBB20_27
.LBB20_20:                              #   in Loop: Header=BB20_10 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB20_10
	.p2align	4, 0x90
.LBB20_21:                              # %._crit_edge
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.LBB20_22
.LBB20_3:                               # %.lr.ph25
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_10 Depth 2
                                        #       Child Loop BB20_15 Depth 3
                                        #     Child Loop BB20_8 Depth 2
	movq	8(%r12), %rbp
	movq	32(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB20_21
# BB#4:                                 #   in Loop: Header=BB20_3 Depth=1
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB20_8
# BB#5:                                 #   in Loop: Header=BB20_3 Depth=1
	cmpl	$0, 68(%rax)
	jne	.LBB20_8
# BB#6:                                 #   in Loop: Header=BB20_3 Depth=1
	cmpl	$0, 72(%rax)
	jne	.LBB20_8
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB20_3 Depth=1
	cmpl	$0, 64(%rax)
	je	.LBB20_10
	.p2align	4, 0x90
.LBB20_8:                               # %.lr.ph.split.us
                                        #   Parent Loop BB20_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %r13
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	tab_PathContainsClause
	testl	%eax, %eax
	je	.LBB20_27
# BB#9:                                 #   in Loop: Header=BB20_8 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB20_8
	jmp	.LBB20_21
.LBB20_22:                              # %._crit_edge26
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB20_24
# BB#23:
	movslq	8(%r15), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r15)
	movq	(%r15), %rdx
	movq	%rax, 8(%rdx,%rcx,8)
	movq	32(%r14), %rdi
	movq	%r15, %rsi
	callq	pcheck_TableauJustificationsRec
	movq	(%r15), %rax
	movslq	8(%r15), %rcx
	leal	-1(%rcx), %edx
	movl	%edx, 8(%r15)
	movq	$0, (%rax,%rcx,8)
.LBB20_24:
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.LBB20_26
# BB#25:
	movslq	8(%r15), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r15)
	movq	(%r15), %rdx
	movq	%rax, 8(%rdx,%rcx,8)
	movq	40(%r14), %rdi
	movq	%r15, %rsi
	callq	pcheck_TableauJustificationsRec
	movq	(%r15), %rax
	movslq	8(%r15), %rcx
	leal	-1(%rcx), %edx
	movl	%edx, 8(%r15)
	movq	$0, (%rax,%rcx,8)
.LBB20_26:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_27:                              # %.us-lcssa.us
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	(%r13), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end20:
	.size	pcheck_TableauJustificationsRec, .Lfunc_end20-pcheck_TableauJustificationsRec
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n Error: Split level of split clause %d is 0.\n"
	.size	.L.str, 47

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n Error: Split level of split clause %d"
	.size	.L.str.1, 40

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" is not increment of current split level.\n"
	.size	.L.str.2, 43

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n Error: Multiple left splits for clause %d.\n"
	.size	.L.str.3, 46

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n Error: Right split with incorrect split level, clause %d.\n"
	.size	.L.str.4, 61

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n Error: Split level of clause %d greater than current level.\n"
	.size	.L.str.5, 63

	.type	pcheck_GenNamedCg,@object # @pcheck_GenNamedCg
	.comm	pcheck_GenNamedCg,4,4
	.type	pcheck_CgName,@object   # @pcheck_CgName
	.comm	pcheck_CgName,8,8
	.type	pcheck_GraphFormat,@object # @pcheck_GraphFormat
	.comm	pcheck_GraphFormat,4,4
	.type	pcheck_Quiet,@object    # @pcheck_Quiet
	.comm	pcheck_Quiet,4,4
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"pruning closed branches..."
	.size	.L.str.6, 27

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"finished."
	.size	.L.str.7, 10

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"removing incomplete splits..."
	.size	.L.str.8, 30

	.type	pcheck_GenRedCg,@object # @pcheck_GenRedCg
	.comm	pcheck_GenRedCg,4,4
	.type	pcheck_RedCgName,@object # @pcheck_RedCgName
	.comm	pcheck_RedCgName,8,8
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\nerror: tableau is not closed."
	.size	.L.str.9, 31

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"checking justifications..."
	.size	.L.str.10, 27

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"_"
	.size	.L.str.12, 2

	.type	pcheck_ProofFileSuffix,@object # @pcheck_ProofFileSuffix
	.comm	pcheck_ProofFileSuffix,8,8
	.type	pcheck_Timelimit,@object # @pcheck_Timelimit
	.comm	pcheck_Timelimit,4,4
	.type	pcheck_ClauseCg,@object # @pcheck_ClauseCg
	.comm	pcheck_ClauseCg,4,4
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\n In pcheck_LabelToNumber:"
	.size	.L.str.13, 27

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	" Could not convert clause"
	.size	.L.str.14, 26

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	" label %s to a number.\n"
	.size	.L.str.15, 24

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"\n Error: Missing parent clause %d of clause %d.\n"
	.size	.L.str.16, 49

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\n Error: Split level of clause %d should be %d.\n"
	.size	.L.str.17, 49

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\n Error: Parent clause with number %d is not yet justified.\n"
	.size	.L.str.18, 61

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"w"
	.size	.L.str.19, 2

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"{*Sub Proof*}"
	.size	.L.str.20, 14

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"{* Proof Checker *}"
	.size	.L.str.21, 20

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"unsatisfiable"
	.size	.L.str.22, 14

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"{* The problem is the correctness test for a single proof line *}"
	.size	.L.str.23, 66


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
