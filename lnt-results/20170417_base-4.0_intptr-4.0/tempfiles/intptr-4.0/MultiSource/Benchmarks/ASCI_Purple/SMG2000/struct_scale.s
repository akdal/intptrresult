	.text
	.file	"struct_scale.bc"
	.globl	hypre_StructScale
	.p2align	4, 0x90
	.type	hypre_StructScale,@function
hypre_StructScale:                      # @hypre_StructScale
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 176
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rax
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB0_14
# BB#1:                                 # %.lr.ph265
	movaps	%xmm0, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	xorl	%edx, %edx
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movaps	%xmm3, 80(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_21 Depth 4
                                        #         Child Loop BB0_10 Depth 4
	movq	(%rcx), %rcx
	leaq	(,%rdx,8), %rax
	leaq	(%rax,%rax,2), %rbp
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rbp), %r15
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rax), %r13
	movq	40(%rcx), %rax
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movslq	(%rax,%rdx,4), %rbx
	movq	%r15, %rdi
	leaq	44(%rsp), %rsi
	callq	hypre_BoxGetSize
	movapd	80(%rsp), %xmm3         # 16-byte Reload
	movapd	96(%rsp), %xmm0         # 16-byte Reload
	movl	(%r13,%rbp), %edi
	movl	4(%r13,%rbp), %ecx
	movl	12(%r13,%rbp), %eax
	movl	%eax, %r12d
	subl	%edi, %r12d
	incl	%r12d
	cmpl	%edi, %eax
	movl	16(%r13,%rbp), %eax
	movl	$0, %edx
	cmovsl	%edx, %r12d
	movl	%eax, %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	%ecx, %eax
	cmovsl	%edx, %esi
	movl	%esi, %r8d
	movl	44(%rsp), %r14d
	movl	48(%rsp), %esi
	movl	52(%rsp), %edx
	cmpl	%r14d, %esi
	movl	%r14d, %eax
	cmovgel	%esi, %eax
	cmpl	%eax, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	cmovgel	%edx, %eax
	testl	%eax, %eax
	jle	.LBB0_13
# BB#3:                                 # %.lr.ph227
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_13
# BB#4:                                 # %.lr.ph227.split.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%esi, %esi
	jle	.LBB0_13
# BB#5:                                 # %.lr.ph227.split.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%r14d, %r14d
	jle	.LBB0_13
# BB#6:                                 # %.preheader203.us.us.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	movl	(%r15), %eax
	subl	%edi, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	4(%rdx,%rbp), %edi
	movl	8(%rdx,%rbp), %r10d
	subl	%ecx, %edi
	subl	8(%r13,%rbp), %r10d
	imull	%r8d, %r10d
	addl	%edi, %r10d
	imull	%r12d, %r10d
	addl	%eax, %r10d
	leal	-1(%r14), %ecx
	incq	%rcx
	movq	%rcx, %rdi
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %rdi
	leaq	-4(%rdi), %r11
	shrq	$2, %r11
	movq	%r11, 24(%rsp)          # 8-byte Spill
                                        # kill: %R11D<def> %R11D<kill> %R11<kill> %R11<def>
	andl	$1, %r11d
	imull	%r12d, %r8d
	movl	%r8d, 36(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader203.us.us.us
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_21 Depth 4
                                        #         Child Loop BB0_10 Depth 4
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%r10d, 16(%rsp)         # 4-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader.us.us.us.us
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_21 Depth 4
                                        #         Child Loop BB0_10 Depth 4
	movslq	%r10d, %r13
	xorl	%edx, %edx
	cmpq	$3, %rcx
	jbe	.LBB0_9
# BB#15:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_8 Depth=3
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#16:                                # %vector.ph
                                        #   in Loop: Header=BB0_8 Depth=3
	testq	%r11, %r11
	jne	.LBB0_17
# BB#18:                                # %vector.body.prol
                                        #   in Loop: Header=BB0_8 Depth=3
	movupd	(%rbx,%r13,8), %xmm2
	movupd	16(%rbx,%r13,8), %xmm1
	mulpd	%xmm3, %xmm2
	mulpd	%xmm3, %xmm1
	movupd	%xmm2, (%rbx,%r13,8)
	movupd	%xmm1, 16(%rbx,%r13,8)
	movl	$4, %r8d
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_20
	jmp	.LBB0_22
.LBB0_17:                               #   in Loop: Header=BB0_8 Depth=3
	xorl	%r8d, %r8d
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB0_22
.LBB0_20:                               # %vector.ph.new
                                        #   in Loop: Header=BB0_8 Depth=3
	leaq	(%rbx,%r13,8), %r9
	.p2align	4, 0x90
.LBB0_21:                               # %vector.body
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	(%r9,%r8,8), %xmm2
	movupd	16(%r9,%r8,8), %xmm1
	mulpd	%xmm3, %xmm2
	mulpd	%xmm3, %xmm1
	movupd	%xmm2, (%r9,%r8,8)
	movupd	%xmm1, 16(%r9,%r8,8)
	movupd	32(%r9,%r8,8), %xmm2
	movupd	48(%r9,%r8,8), %xmm1
	mulpd	%xmm3, %xmm2
	mulpd	%xmm3, %xmm1
	movupd	%xmm2, 32(%r9,%r8,8)
	movupd	%xmm1, 48(%r9,%r8,8)
	addq	$8, %r8
	cmpq	%r8, %rdi
	jne	.LBB0_21
.LBB0_22:                               # %middle.block
                                        #   in Loop: Header=BB0_8 Depth=3
	cmpq	%rdi, %rcx
	je	.LBB0_11
# BB#23:                                #   in Loop: Header=BB0_8 Depth=3
	addq	%rdi, %r13
	movl	%edi, %edx
	.p2align	4, 0x90
.LBB0_9:                                # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_8 Depth=3
	leaq	(%rbx,%r13,8), %rbp
	movl	%r14d, %eax
	subl	%edx, %eax
	.p2align	4, 0x90
.LBB0_10:                               # %scalar.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbp)
	addq	$8, %rbp
	decl	%eax
	jne	.LBB0_10
.LBB0_11:                               # %._crit_edge.us.us.us.us
                                        #   in Loop: Header=BB0_8 Depth=3
	addl	%r12d, %r10d
	incl	%r15d
	cmpl	%esi, %r15d
	jne	.LBB0_8
# BB#12:                                # %._crit_edge208.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB0_7 Depth=2
	movl	40(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	16(%rsp), %r10d         # 4-byte Reload
	addl	36(%rsp), %r10d         # 4-byte Folded Reload
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB0_7
	.p2align	4, 0x90
.LBB0_13:                               # %._crit_edge228
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rdx
	jl	.LBB0_2
.LBB0_14:                               # %._crit_edge266
	xorl	%eax, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_StructScale, .Lfunc_end0-hypre_StructScale
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
