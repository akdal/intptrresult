	.text
	.file	"ExtractingFilePath.bc"
	.globl	_Z15MakeCorrectPathR13CObjectVectorI11CStringBaseIwEE
	.p2align	4, 0x90
	.type	_Z15MakeCorrectPathR13CObjectVectorI11CStringBaseIwEE,@function
_Z15MakeCorrectPathR13CObjectVectorI11CStringBaseIwEE: # @_Z15MakeCorrectPathR13CObjectVectorI11CStringBaseIwEE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	cmpl	$0, 12(%r13)
	jle	.LBB0_23
# BB#1:                                 # %.lr.ph.lr.ph
	xorl	%ebp, %ebp
	movq	%r13, 16(%rsp)          # 8-byte Spill
.LBB0_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
                                        #       Child Loop BB0_7 Depth 3
                                        #       Child Loop BB0_18 Depth 3
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_3:                                #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_7 Depth 3
                                        #       Child Loop BB0_18 Depth 3
	movq	16(%r13), %rax
	movq	(%rax,%rbp,8), %rbp
	movq	(%rbp), %rdi
	movl	$.L.str, %esi
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	je	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=2
	movq	(%rbp), %rdi
	movl	$.L.str.1, %esi
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	je	.LBB0_5
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=2
	movq	(%rbp), %rbx
	leaq	8(%rbp), %r14
	movslq	8(%rbp), %rax
	movq	%rax, %r15
	incq	%rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	movl	$0, (%r12)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_7:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx,%rax), %ecx
	movl	%ecx, (%r12,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_3 Depth=2
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%r15, %rdx
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=2
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
	movl	$0, (%r12)
	leaq	8(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%edx, %edx
.LBB0_9:                                # %_ZL18GetCorrectFileNameRK11CStringBaseIwE.exit
                                        #   in Loop: Header=BB0_3 Depth=2
	movl	$0, 8(%rbp)
	movq	(%rbp), %rbx
	movl	$0, (%rbx)
	leal	1(%rdx), %r14d
	movl	12(%rbp), %r15d
	cmpl	%r15d, %r14d
	jne	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_3 Depth=2
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_11:                               #   in Loop: Header=BB0_3 Depth=2
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp0:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp1:
# BB#12:                                # %.noexc
                                        #   in Loop: Header=BB0_3 Depth=2
	testq	%rbx, %rbx
	je	.LBB0_13
# BB#14:                                # %.noexc
                                        #   in Loop: Header=BB0_3 Depth=2
	testl	%r15d, %r15d
	movl	$0, %eax
	movq	8(%rsp), %r15           # 8-byte Reload
	jle	.LBB0_16
# BB#15:                                # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB0_3 Depth=2
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	(%r15), %rax
	jmp	.LBB0_16
.LBB0_13:                               #   in Loop: Header=BB0_3 Depth=2
	xorl	%eax, %eax
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB0_16:                               # %._crit_edge16.i.i
                                        #   in Loop: Header=BB0_3 Depth=2
	movq	%r13, (%rbp)
	movl	$0, (%r13,%rax,4)
	movl	%r14d, 12(%rbp)
	movq	%r13, %rbx
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB0_17:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=2
	xorl	%eax, %eax
	movq	32(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_18:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r12,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_18
# BB#19:                                # %_ZN11CStringBaseIwED2Ev.exit16
                                        #   in Loop: Header=BB0_3 Depth=2
	movl	%edx, (%r15)
	movq	%r12, %rdi
	callq	_ZdaPv
	cmpl	$0, (%r15)
	jne	.LBB0_22
# BB#20:                                #   in Loop: Header=BB0_3 Depth=2
	movq	(%r13), %rax
	movl	$1, %edx
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	*16(%rax)
	movslq	12(%r13), %rax
	cmpq	%rax, %rbp
	jl	.LBB0_3
	jmp	.LBB0_23
	.p2align	4, 0x90
.LBB0_22:                               # %.outer
                                        #   in Loop: Header=BB0_2 Depth=1
	incq	%rbp
	movslq	12(%r13), %rax
	cmpq	%rax, %rbp
	jl	.LBB0_2
.LBB0_23:                               # %.outer._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_21:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp2:
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z15MakeCorrectPathR13CObjectVectorI11CStringBaseIwEE, .Lfunc_end0-_Z15MakeCorrectPathR13CObjectVectorI11CStringBaseIwEE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_Z21MakePathNameFromPartsRK13CObjectVectorI11CStringBaseIwEE
	.p2align	4, 0x90
	.type	_Z21MakePathNameFromPartsRK13CObjectVectorI11CStringBaseIwEE,@function
_Z21MakePathNameFromPartsRK13CObjectVectorI11CStringBaseIwEE: # @_Z21MakePathNameFromPartsRK13CObjectVectorI11CStringBaseIwEE
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r12, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%r15)
	movl	$0, (%rax)
	movl	$4, 12(%r15)
	cmpl	$0, 12(%r14)
	jle	.LBB2_9
# BB#1:                                 # %.lr.ph
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_7 Depth 2
	testq	%r12, %r12
	je	.LBB2_5
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
.Ltmp3:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp4:
# BB#4:                                 # %_ZN11CStringBaseIwEpLEw.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	(%r15), %rax
	movslq	8(%r15), %rcx
	movl	$47, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r15)
	movl	$0, 4(%rax,%rcx,4)
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%r12,8), %rbx
	movl	8(%rbx), %esi
.Ltmp5:
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp6:
# BB#6:                                 # %.noexc
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	8(%r15), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r15), %rcx
	movq	(%rbx), %rdx
	.p2align	4, 0x90
.LBB2_7:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rcx)
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB2_7
# BB#8:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	8(%rbx), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%r15)
	incq	%r12
	movslq	12(%r14), %rax
	cmpq	%rax, %r12
	jl	.LBB2_2
.LBB2_9:                                # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB2_10:
.Ltmp7:
	movq	%rax, %r14
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB2_12
# BB#11:
	callq	_ZdaPv
.LBB2_12:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_Z21MakePathNameFromPartsRK13CObjectVectorI11CStringBaseIwEE, .Lfunc_end2-_Z21MakePathNameFromPartsRK13CObjectVectorI11CStringBaseIwEE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin1    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z16GetCorrectFsPathRK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_Z16GetCorrectFsPathRK11CStringBaseIwE,@function
_Z16GetCorrectFsPathRK11CStringBaseIwE: # @_Z16GetCorrectFsPathRK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -40
.Lcfi28:
	.cfi_offset %r12, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	(%r15), %rdi
	movl	$.L.str, %esi
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	je	.LBB3_2
# BB#1:
	movq	(%r15), %rdi
	movl	$.L.str.1, %esi
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	je	.LBB3_2
# BB#3:
	movq	(%r15), %rbx
	movslq	8(%r15), %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movq	%r15, %r12
	incq	%r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r12d, 12(%r14)
	.p2align	4, 0x90
.LBB3_4:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB3_4
# BB#5:                                 # %_ZL21ReplaceIncorrectCharsRK11CStringBaseIwE.exit.i
	movl	%r15d, 8(%r14)
	jmp	.LBB3_6
.LBB3_2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	$4, 12(%r14)
.LBB3_6:                                # %_ZL18GetCorrectFileNameRK11CStringBaseIwE.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_Z16GetCorrectFsPathRK11CStringBaseIwE, .Lfunc_end3-_Z16GetCorrectFsPathRK11CStringBaseIwE
	.cfi_endproc

	.globl	_Z20GetCorrectFullFsPathRK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_Z20GetCorrectFullFsPathRK11CStringBaseIwE,@function
_Z20GetCorrectFullFsPathRK11CStringBaseIwE: # @_Z20GetCorrectFullFsPathRK11CStringBaseIwE
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rsp)
	movq	$8, 24(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rsp)
.Ltmp8:
	movq	%rsp, %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E
.Ltmp9:
# BB#1:                                 # %.preheader
.Ltmp10:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	_Z21MakePathNameFromPartsRK13CObjectVectorI11CStringBaseIwEE
.Ltmp11:
# BB#2:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rsp)
.Ltmp22:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp23:
# BB#3:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit12
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	retq
.LBB4_4:
.Ltmp24:
	movq	%rax, %rbx
.Ltmp25:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp26:
	jmp	.LBB4_5
.LBB4_6:
.Ltmp27:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_7:
.Ltmp12:
	movq	%rax, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rsp)
.Ltmp13:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp14:
# BB#8:
.Ltmp19:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp20:
.LBB4_5:                                # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_11:
.Ltmp21:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB4_9:
.Ltmp15:
	movq	%rax, %rbx
.Ltmp16:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp17:
# BB#12:                                # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB4_10:
.Ltmp18:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_Z20GetCorrectFullFsPathRK11CStringBaseIwE, .Lfunc_end4-_Z20GetCorrectFullFsPathRK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp8-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin2   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin2   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp25-.Ltmp23         #   Call between .Ltmp23 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin2   #     jumps to .Ltmp27
	.byte	1                       #   On action: 1
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin2   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin2   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp16-.Ltmp20         #   Call between .Ltmp20 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin2   #     jumps to .Ltmp18
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 32
.Lcfi37:
	.cfi_offset %rbx, -24
.Lcfi38:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp28:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp29:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB5_2:
.Ltmp30:
	movq	%rax, %r14
.Ltmp31:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp32:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_4:
.Ltmp33:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end5-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp28-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin3   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp29         #   Call between .Ltmp29 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin3   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp32-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp32     #   Call between .Ltmp32 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -48
.Lcfi45:
	.cfi_offset %r12, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB6_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB6_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB6_3:                                # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB6_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB6_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB6_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB6_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB6_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB6_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB6_17
.LBB6_7:
	xorl	%ecx, %ecx
.LBB6_8:                                # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB6_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB6_10:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB6_10
.LBB6_11:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB6_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB6_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB6_13
	jmp	.LBB6_26
.LBB6_25:                               # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB6_27
.LBB6_26:                               # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB6_27:                               # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB6_28:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_17:                               # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB6_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB6_20
	jmp	.LBB6_21
.LBB6_18:
	xorl	%ebx, %ebx
.LBB6_21:                               # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB6_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB6_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB6_23
.LBB6_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB6_8
	jmp	.LBB6_26
.Lfunc_end6:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end6-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp34:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp35:
# BB#1:
.Ltmp40:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp41:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB7_5:
.Ltmp42:
	movq	%rax, %r14
	jmp	.LBB7_6
.LBB7_3:
.Ltmp36:
	movq	%rax, %r14
.Ltmp37:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp38:
.LBB7_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_4:
.Ltmp39:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end7-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp34-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin4   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin4   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin4   #     jumps to .Ltmp39
	.byte	1                       #   On action: 1
	.long	.Ltmp38-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp38     #   Call between .Ltmp38 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 64
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB8_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB8_6
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_5
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	callq	_ZdaPv
.LBB8_5:                                # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB8_2
.LBB8_7:                                # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end8:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end8-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	46                      # 0x2e
	.long	46                      # 0x2e
	.long	0                       # 0x0
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	46                      # 0x2e
	.long	0                       # 0x0
	.size	.L.str.1, 8

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
