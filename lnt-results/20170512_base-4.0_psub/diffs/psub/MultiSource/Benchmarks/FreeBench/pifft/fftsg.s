	.text
	.file	"fftsg.bc"
	.globl	cdft
	.p2align	4, 0x90
	.type	cdft,@function
cdft:                                   # @cdft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %rbx
	movq	%rdx, %r15
	movl	%esi, %r13d
	movl	%edi, %ebp
	movl	(%rbx), %r12d
	leal	(,%r12,4), %eax
	cmpl	%ebp, %eax
	jge	.LBB0_2
# BB#1:
	movl	%ebp, %r12d
	sarl	$2, %r12d
	movl	%r12d, %edi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	makewt
.LBB0_2:
	addq	$8, %rbx
	movl	%ebp, %edi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movl	%r12d, %ecx
	movq	%r14, %r8
	addq	$8, %rsp
	testl	%r13d, %r13d
	js	.LBB0_4
# BB#3:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cftfsub                 # TAILCALL
.LBB0_4:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cftbsub                 # TAILCALL
.Lfunc_end0:
	.size	cdft, .Lfunc_end0-cdft
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
.LCPI1_1:
	.quad	4605249457297304856     # double 0.78539816339744828
.LCPI1_2:
	.quad	4602678819172646912     # double 0.5
.LCPI1_3:
	.quad	4618441417868443648     # double 6
.LCPI1_4:
	.quad	4613937818241073152     # double 3
	.text
	.globl	makewt
	.p2align	4, 0x90
	.type	makewt,@function
makewt:                                 # @makewt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%edi, %r14d
	movl	%r14d, (%rsi)
	movl	$1, 4(%rsi)
	cmpl	$3, %r14d
	jl	.LBB1_7
# BB#1:
	movl	%r14d, %r13d
	shrl	%r13d
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	movsd	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	mulsd	%xmm1, %xmm0
	callq	cos
	movabsq	$4607182418800017408, %r15 # imm = 0x3FF0000000000000
	movq	%r15, (%rbx)
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	%xmm0, 8(%rbx)
	cmpl	$8, %r14d
	jb	.LBB1_6
# BB#2:                                 # %.preheader109
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	callq	cos
	movsd	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rbx)
	movsd	.LCPI1_3(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
	callq	cos
	movsd	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 24(%rbx)
	cmpl	$9, %r14d
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	jbe	.LBB1_3
# BB#4:                                 # %.lr.ph115
	movsd	.LCPI1_4(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movl	%r13d, %r12d
	movl	$4, %ebp
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	callq	cos
	movsd	%xmm0, (%rbx,%rbp,8)
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	%xmm0, 8(%rbx,%rbp,8)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	cos
	movsd	%xmm0, 16(%rbx,%rbp,8)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	%xmm0, 24(%rbx,%rbp,8)
	addq	$4, %rbp
	cmpq	%r12, %rbp
	jl	.LBB1_5
.LBB1_6:                                # %.preheader107
	cmpl	$5, %r14d
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	jbe	.LBB1_7
.LBB1_3:                                # %.lr.ph113.preheader
	leaq	80(%rbx), %r8
	leaq	48(%rbx), %r9
	xorl	%edx, %edx
	movsd	.LCPI1_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph113
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_16 Depth 2
	movl	%edx, %ecx
	movl	%r13d, %esi
	leal	(%rsi,%rcx), %edx
	shrl	%r13d
	movslq	%edx, %rax
	movq	%r15, (%rbx,%rax,8)
	movsd	%xmm3, 8(%rbx,%rax,8)
	cmpl	$8, %esi
	jb	.LBB1_8
# BB#10:                                # %.preheader
                                        #   in Loop: Header=BB1_9 Depth=1
	movslq	%ecx, %rcx
	movapd	%xmm0, %xmm1
	divsd	32(%rbx,%rcx,8), %xmm1
	movapd	%xmm0, %xmm2
	divsd	48(%rbx,%rcx,8), %xmm2
	movsd	%xmm1, 16(%rbx,%rax,8)
	movsd	%xmm2, 24(%rbx,%rax,8)
	cmpl	$10, %esi
	jb	.LBB1_9
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	%r13d, %edi
	cmpl	$8, %r13d
	movl	$8, %ebp
	cmovaq	%rdi, %rbp
	addq	$-5, %rbp
	movq	%rbp, %r10
	shrq	$2, %r10
	btl	$2, %ebp
	jb	.LBB1_12
# BB#13:                                # %.lr.ph.prol
                                        #   in Loop: Header=BB1_9 Depth=1
	movupd	64(%rbx,%rcx,8), %xmm1
	movupd	80(%rbx,%rcx,8), %xmm2
	movupd	%xmm1, 32(%rbx,%rax,8)
	movupd	%xmm2, 48(%rbx,%rax,8)
	movl	$8, %ebp
	testq	%r10, %r10
	jne	.LBB1_15
	jmp	.LBB1_8
.LBB1_12:                               #   in Loop: Header=BB1_9 Depth=1
	movl	$4, %ebp
	testq	%r10, %r10
	je	.LBB1_8
.LBB1_15:                               # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB1_9 Depth=1
	leaq	(%rcx,%rbp,2), %rcx
	leaq	(%r8,%rcx,8), %rcx
	leaq	(%r9,%rax,8), %rax
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph
                                        #   Parent Loop BB1_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-80(%rcx), %xmm1
	movups	-64(%rcx), %xmm2
	movups	%xmm1, -48(%rax,%rbp,8)
	movups	%xmm2, -32(%rax,%rbp,8)
	movupd	-16(%rcx), %xmm1
	movupd	(%rcx), %xmm2
	movupd	%xmm1, -16(%rax,%rbp,8)
	movupd	%xmm2, (%rax,%rbp,8)
	addq	$8, %rbp
	subq	$-128, %rcx
	cmpq	%rdi, %rbp
	jl	.LBB1_16
.LBB1_8:                                # %.loopexit
                                        #   in Loop: Header=BB1_9 Depth=1
	cmpl	$6, %esi
	jae	.LBB1_9
.LBB1_7:                                # %.loopexit108
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	makewt, .Lfunc_end1-makewt
	.cfi_endproc

	.globl	cftfsub
	.p2align	4, 0x90
	.type	cftfsub,@function
cftfsub:                                # @cftfsub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movl	%ecx, %r12d
	movq	%rdx, %r15
	movq	%rsi, %r13
	movl	%edi, %ebp
	cmpl	$33, %ebp
	jl	.LBB2_7
# BB#1:
	movl	%ebp, %ebx
	shrl	$2, %ebx
	movl	%r12d, %eax
	subl	%ebx, %eax
	cltq
	leaq	(%r14,%rax,8), %rdx
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	cftf1st
	cmpl	$513, %ebp              # imm = 0x201
	jl	.LBB2_3
# BB#2:
	movl	%ebx, %edi
	movq	%r13, %rsi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	cftrec1
	movl	%ebx, %eax
	leaq	(%r13,%rax,8), %rsi
	movl	%ebx, %edi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	cftrec2
	leal	(%rbx,%rbx), %eax
	leaq	(%r13,%rax,8), %rsi
	movl	%ebx, %edi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	cftrec1
	leal	(%rbx,%rbx,2), %eax
	leaq	(%r13,%rax,8), %rsi
	movl	%ebx, %edi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	cftrec1
	jmp	.LBB2_6
.LBB2_7:
	cmpl	$9, %ebp
	jl	.LBB2_11
# BB#8:
	cmpl	$32, %ebp
	jne	.LBB2_10
# BB#9:
	movslq	%r12d, %rax
	leaq	-64(%r14,%rax,8), %rsi
	movq	%r13, %rdi
	callq	cftf161
	movups	16(%r13), %xmm9
	movupd	32(%r13), %xmm1
	movups	48(%r13), %xmm8
	movupd	64(%r13), %xmm3
	movups	80(%r13), %xmm11
	movups	112(%r13), %xmm10
	movups	128(%r13), %xmm6
	movups	160(%r13), %xmm7
	movupd	176(%r13), %xmm2
	movupd	192(%r13), %xmm0
	movups	208(%r13), %xmm5
	movupd	224(%r13), %xmm4
	movups	%xmm6, 16(%r13)
	movupd	%xmm3, 32(%r13)
	movupd	%xmm0, 48(%r13)
	movupd	%xmm1, 64(%r13)
	movups	%xmm7, 80(%r13)
	movupd	%xmm4, 112(%r13)
	movups	%xmm9, 128(%r13)
	movups	%xmm11, 160(%r13)
	movups	%xmm5, 176(%r13)
	movups	%xmm8, 192(%r13)
	movupd	%xmm2, 208(%r13)
	movups	%xmm10, 224(%r13)
	jmp	.LBB2_15
.LBB2_3:
	movl	%ebp, %edi
	movq	%r13, %rsi
	movl	%r12d, %edx
	movq	%r14, %rcx
	cmpl	$132, %ebp
	jb	.LBB2_5
# BB#4:
	callq	cftexp1
	jmp	.LBB2_6
.LBB2_11:
	cmpl	$4, %ebp
	je	.LBB2_14
# BB#12:
	cmpl	$8, %ebp
	jne	.LBB2_15
# BB#13:
	movupd	(%r13), %xmm0
	movupd	16(%r13), %xmm1
	movupd	32(%r13), %xmm2
	movupd	48(%r13), %xmm3
	movapd	%xmm0, %xmm4
	addpd	%xmm2, %xmm4
	subpd	%xmm2, %xmm0
	movapd	%xmm1, %xmm2
	addpd	%xmm3, %xmm2
	subpd	%xmm3, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movapd	%xmm4, %xmm3
	addpd	%xmm2, %xmm3
	movupd	%xmm3, (%r13)
	subpd	%xmm2, %xmm4
	movupd	%xmm4, 32(%r13)
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1]
	movupd	%xmm0, 16(%r13)
	movsd	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1]
	movupd	%xmm2, 48(%r13)
	jmp	.LBB2_15
.LBB2_10:
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	cftf081
	movupd	16(%r13), %xmm0
	movupd	48(%r13), %xmm1
	movupd	64(%r13), %xmm2
	movupd	96(%r13), %xmm3
	movupd	%xmm2, 16(%r13)
	movupd	%xmm3, 48(%r13)
	movupd	%xmm0, 64(%r13)
	movupd	%xmm1, 96(%r13)
	jmp	.LBB2_15
.LBB2_5:
	callq	cftfx41
.LBB2_6:
	movl	%ebp, %edi
	movq	%r15, %rsi
	movq	%r13, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	bitrv2                  # TAILCALL
.LBB2_14:
	movupd	(%r13), %xmm0
	movupd	16(%r13), %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%r13)
	movupd	%xmm2, 16(%r13)
.LBB2_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cftfsub, .Lfunc_end2-cftfsub
	.cfi_endproc

	.globl	cftbsub
	.p2align	4, 0x90
	.type	cftbsub,@function
cftbsub:                                # @cftbsub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 64
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movl	%ecx, %r12d
	movq	%rdx, %r15
	movq	%rsi, %r13
	movl	%edi, %ebp
	cmpl	$33, %ebp
	jl	.LBB3_7
# BB#1:
	movl	%ebp, %ebx
	shrl	$2, %ebx
	movl	%r12d, %eax
	subl	%ebx, %eax
	cltq
	leaq	(%r14,%rax,8), %rdx
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	cftb1st
	cmpl	$513, %ebp              # imm = 0x201
	jl	.LBB3_3
# BB#2:
	movl	%ebx, %edi
	movq	%r13, %rsi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	cftrec1
	movl	%ebx, %eax
	leaq	(%r13,%rax,8), %rsi
	movl	%ebx, %edi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	cftrec2
	leal	(%rbx,%rbx), %eax
	leaq	(%r13,%rax,8), %rsi
	movl	%ebx, %edi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	cftrec1
	leal	(%rbx,%rbx,2), %eax
	leaq	(%r13,%rax,8), %rsi
	movl	%ebx, %edi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	cftrec1
	jmp	.LBB3_6
.LBB3_7:
	cmpl	$9, %ebp
	jl	.LBB3_11
# BB#8:
	cmpl	$32, %ebp
	jne	.LBB3_10
# BB#9:
	movslq	%r12d, %rax
	leaq	-64(%r14,%rax,8), %rsi
	movq	%r13, %rdi
	callq	cftf161
	movups	16(%r13), %xmm10
	movups	32(%r13), %xmm8
	movups	48(%r13), %xmm13
	movups	64(%r13), %xmm9
	movupd	80(%r13), %xmm4
	movups	96(%r13), %xmm12
	movups	112(%r13), %xmm6
	movups	128(%r13), %xmm11
	movupd	144(%r13), %xmm1
	movups	160(%r13), %xmm14
	movupd	176(%r13), %xmm0
	movups	192(%r13), %xmm7
	movups	208(%r13), %xmm5
	movupd	224(%r13), %xmm2
	movupd	240(%r13), %xmm3
	movupd	%xmm3, 16(%r13)
	movups	%xmm6, 32(%r13)
	movupd	%xmm0, 48(%r13)
	movups	%xmm13, 64(%r13)
	movups	%xmm5, 80(%r13)
	movupd	%xmm4, 96(%r13)
	movupd	%xmm1, 112(%r13)
	movups	%xmm10, 128(%r13)
	movupd	%xmm2, 144(%r13)
	movups	%xmm12, 160(%r13)
	movups	%xmm14, 176(%r13)
	movups	%xmm8, 192(%r13)
	movups	%xmm7, 208(%r13)
	movups	%xmm9, 224(%r13)
	movups	%xmm11, 240(%r13)
	jmp	.LBB3_15
.LBB3_3:
	movl	%ebp, %edi
	movq	%r13, %rsi
	movl	%r12d, %edx
	movq	%r14, %rcx
	cmpl	$132, %ebp
	jb	.LBB3_5
# BB#4:
	callq	cftexp1
	jmp	.LBB3_6
.LBB3_11:
	cmpl	$4, %ebp
	je	.LBB3_14
# BB#12:
	cmpl	$8, %ebp
	jne	.LBB3_15
# BB#13:
	movupd	(%r13), %xmm0
	movupd	16(%r13), %xmm1
	movupd	32(%r13), %xmm2
	movupd	48(%r13), %xmm3
	movapd	%xmm0, %xmm4
	addpd	%xmm2, %xmm4
	subpd	%xmm2, %xmm0
	movapd	%xmm1, %xmm2
	addpd	%xmm3, %xmm2
	subpd	%xmm3, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movapd	%xmm4, %xmm3
	addpd	%xmm2, %xmm3
	movupd	%xmm3, (%r13)
	subpd	%xmm2, %xmm4
	movupd	%xmm4, 32(%r13)
	movapd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	subpd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	movupd	%xmm1, 16(%r13)
	movsd	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1]
	movupd	%xmm2, 48(%r13)
	jmp	.LBB3_15
.LBB3_10:
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	cftf081
	movupd	16(%r13), %xmm0
	movupd	32(%r13), %xmm1
	movupd	48(%r13), %xmm2
	movupd	64(%r13), %xmm3
	movupd	80(%r13), %xmm4
	movups	96(%r13), %xmm5
	movups	112(%r13), %xmm6
	movups	%xmm6, 16(%r13)
	movupd	%xmm2, 32(%r13)
	movupd	%xmm4, 48(%r13)
	movupd	%xmm0, 64(%r13)
	movups	%xmm5, 80(%r13)
	movupd	%xmm1, 96(%r13)
	movupd	%xmm3, 112(%r13)
	jmp	.LBB3_15
.LBB3_5:
	callq	cftfx41
.LBB3_6:
	movl	%ebp, %edi
	movq	%r15, %rsi
	movq	%r13, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	bitrv2conj              # TAILCALL
.LBB3_14:
	movupd	(%r13), %xmm0
	movupd	16(%r13), %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%r13)
	movupd	%xmm2, 16(%r13)
.LBB3_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	cftbsub, .Lfunc_end3-cftbsub
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4607182418800017408     # double 1
.LCPI4_1:
	.quad	4605249457297304856     # double 0.78539816339744828
.LCPI4_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	rdft
	.p2align	4, 0x90
	.type	rdft,@function
rdft:                                   # @rdft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 112
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movl	%edi, %ebp
	movl	(%r12), %r14d
	leal	(,%r14,4), %eax
	cmpl	%ebp, %eax
	jge	.LBB4_2
# BB#1:
	movl	%ebp, %r14d
	sarl	$2, %r14d
	movl	%r14d, %edi
	movl	%esi, %r15d
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	makewt
	movl	%r15d, %esi
.LBB4_2:
	movl	4(%r12), %r15d
	leal	(,%r15,4), %eax
	cmpl	%ebp, %eax
	jge	.LBB4_9
# BB#3:
	movl	%ebp, %r15d
	sarl	$2, %r15d
	movl	%r15d, 4(%r12)
	cmpl	$2, %r15d
	jl	.LBB4_9
# BB#4:
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movslq	%r14d, %r12
	leaq	(%r13,%r12,8), %rbp
	movl	%r15d, %r14d
	shrl	%r14d
	movsd	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	mulsd	%xmm1, %xmm0
	callq	cos
	movsd	%xmm0, (%rbp)
	mulsd	.LCPI4_2(%rip), %xmm0
	movsd	%xmm0, (%rbp,%r14,8)
	cmpl	$4, %r15d
	jb	.LBB4_8
# BB#5:                                 # %.lr.ph.preheader.i
	movslq	%r15d, %rax
	addq	%rax, %r12
	movq	%r13, 32(%rsp)          # 8-byte Spill
	leaq	-8(%r13,%r12,8), %r12
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	mulsd	40(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	callq	cos
	movsd	.LCPI4_2(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rbp,%r13,8)
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	.LCPI4_2(%rip), %xmm0
	movsd	%xmm0, (%r12)
	incq	%r13
	addq	$-8, %r12
	cmpq	%r14, %r13
	jl	.LBB4_6
# BB#7:
	movq	32(%rsp), %r13          # 8-byte Reload
.LBB4_8:                                # %makect.exit
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
.LBB4_9:                                # %makect.exit
	testl	%esi, %esi
	js	.LBB4_18
# BB#10:
	cmpl	$5, %ebp
	jl	.LBB4_14
# BB#11:
	addq	$8, %r12
	movl	%ebp, %edi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movl	%r14d, %ecx
	movq	%r13, %r8
	callq	cftfsub
	cmpl	$5, %ebp
	je	.LBB4_16
# BB#12:                                # %.lr.ph.preheader.i64
	movslq	%r14d, %rcx
	movl	%ebp, %esi
	shrl	%esi
	leal	(%r15,%r15), %eax
	cltd
	idivl	%esi
	movl	%esi, %r8d
	movslq	%ebp, %rdx
	movslq	%eax, %rbp
	movslq	%r15d, %rax
	leaq	(%rcx,%rbp), %rsi
	leaq	(%r13,%rsi,8), %rsi
	addq	%rcx, %rax
	subq	%rbp, %rax
	shlq	$3, %rbp
	leaq	(%r13,%rax,8), %rdi
	movq	%rbp, %rax
	negq	%rax
	addq	$-2, %rdx
	movl	$2, %ecx
	movsd	.LCPI4_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB4_13:                               # %.lr.ph.i67
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm2
	subsd	(%rdi), %xmm2
	movupd	(%rbx,%rcx,8), %xmm3
	movsd	(%rbx,%rdx,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm3, %xmm1
	subsd	%xmm4, %xmm1
	movapd	%xmm3, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	8(%rbx,%rdx,8), %xmm4
	movsd	(%rsi), %xmm5           # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm6, %xmm4
	unpcklpd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	subpd	%xmm4, %xmm2
	addpd	%xmm4, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	subpd	%xmm1, %xmm3
	movupd	%xmm3, (%rbx,%rcx,8)
	movupd	(%rbx,%rdx,8), %xmm2
	movapd	%xmm2, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%rbx,%rdx,8)
	addq	$2, %rcx
	addq	%rbp, %rsi
	addq	%rax, %rdi
	addq	$-2, %rdx
	cmpq	%r8, %rcx
	jl	.LBB4_13
	jmp	.LBB4_16
.LBB4_18:
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	subsd	8(%rbx), %xmm1
	mulsd	.LCPI4_2(%rip), %xmm1
	movsd	%xmm1, 8(%rbx)
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rbx)
	cmpl	$5, %ebp
	jl	.LBB4_24
# BB#19:
	movl	%ebp, %edi
	cmpl	$5, %ebp
	je	.LBB4_22
# BB#20:                                # %.lr.ph.preheader.i68
	movslq	%r14d, %rcx
	movl	%edi, %ebp
	movl	%ebp, %esi
	shrl	%esi
	leal	(%r15,%r15), %eax
	cltd
	idivl	%esi
	movl	%esi, %r8d
	movslq	%ebp, %rdx
	movslq	%eax, %r9
	movslq	%r15d, %rax
	leaq	(%rcx,%r9), %rsi
	leaq	(%r13,%rsi,8), %rsi
	addq	%rcx, %rax
	subq	%r9, %rax
	shlq	$3, %r9
	leaq	(%r13,%rax,8), %rbp
	movq	%r9, %rax
	negq	%rax
	addq	$-2, %rdx
	movl	$2, %ecx
	movsd	.LCPI4_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB4_21:                               # %.lr.ph.i73
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm1
	subsd	(%rbp), %xmm1
	movupd	(%rbx,%rcx,8), %xmm2
	movupd	(%rbx,%rdx,8), %xmm3
	movapd	%xmm2, %xmm4
	subpd	%xmm3, %xmm4
	addpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm5
	movsd	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm5, %xmm1
	movsd	(%rsi), %xmm5           # xmm5 = mem[0],zero
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	shufpd	$1, %xmm4, %xmm3        # xmm3 = xmm3[1],xmm4[0]
	mulpd	%xmm5, %xmm3
	movapd	%xmm1, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm1
	movsd	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1]
	subpd	%xmm1, %xmm2
	movupd	%xmm2, (%rbx,%rcx,8)
	movupd	(%rbx,%rdx,8), %xmm2
	movapd	%xmm2, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%rbx,%rdx,8)
	addq	$2, %rcx
	addq	%r9, %rsi
	addq	%rax, %rbp
	addq	$-2, %rdx
	cmpq	%r8, %rcx
	jl	.LBB4_21
.LBB4_22:                               # %rftbsub.exit
	addq	$8, %r12
	jmp	.LBB4_23
.LBB4_14:
	cmpl	$4, %ebp
	jne	.LBB4_16
# BB#15:
	addq	$8, %r12
	movl	$4, %edi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movl	%r14d, %ecx
	movq	%r13, %r8
	callq	cftfsub
.LBB4_16:                               # %rftfsub.exit
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rbx)
	movsd	%xmm2, 8(%rbx)
.LBB4_17:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_24:
	cmpl	$4, %ebp
	jne	.LBB4_17
# BB#25:
	addq	$8, %r12
	movl	$4, %edi
.LBB4_23:                               # %rftbsub.exit
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movl	%r14d, %ecx
	movq	%r13, %r8
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cftbsub                 # TAILCALL
.Lfunc_end4:
	.size	rdft, .Lfunc_end4-rdft
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4607182418800017408     # double 1
.LCPI5_1:
	.quad	4605249457297304856     # double 0.78539816339744828
.LCPI5_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	makect
	.p2align	4, 0x90
	.type	makect,@function
makect:                                 # @makect
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 64
.Lcfi71:
	.cfi_offset %rbx, -48
.Lcfi72:
	.cfi_offset %r12, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movl	%edi, %r14d
	movl	%r14d, 4(%rsi)
	cmpl	$2, %r14d
	jl	.LBB5_4
# BB#1:
	movl	%r14d, %ebx
	shrl	%ebx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI5_1(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	mulsd	%xmm1, %xmm0
	callq	cos
	movsd	%xmm0, (%r12)
	mulsd	.LCPI5_2(%rip), %xmm0
	movsd	%xmm0, (%r12,%rbx,8)
	cmpl	$4, %r14d
	jb	.LBB5_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%ebx, %r15d
	movslq	%r14d, %rbp
	decq	%rbp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	cos
	movsd	.LCPI5_2(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r12,%rbx,8)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	.LCPI5_2(%rip), %xmm0
	movsd	%xmm0, (%r12,%rbp,8)
	incq	%rbx
	decq	%rbp
	cmpq	%r15, %rbx
	jl	.LBB5_3
.LBB5_4:                                # %.loopexit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	makect, .Lfunc_end5-makect
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	rftfsub
	.p2align	4, 0x90
	.type	rftfsub,@function
rftfsub:                                # @rftfsub
	.cfi_startproc
# BB#0:
	movl	%edx, %r8d
	movl	%edi, %r9d
	sarl	%r9d
	cmpl	$3, %r9d
	jl	.LBB6_3
# BB#1:                                 # %.lr.ph.preheader
	leal	(%r8,%r8), %eax
	cltd
	idivl	%r9d
	movslq	%r9d, %r9
	movslq	%edi, %rdx
	movslq	%eax, %r10
	movslq	%r8d, %rax
	leaq	(%rcx,%r10,8), %rdi
	subq	%r10, %rax
	shlq	$3, %r10
	leaq	(%rcx,%rax,8), %rcx
	movq	%r10, %r8
	negq	%r8
	leaq	-16(%rsi,%rdx,8), %rdx
	movl	$2, %eax
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm2
	subsd	(%rcx), %xmm2
	movupd	(%rsi,%rax,8), %xmm3
	movsd	(%rdx), %xmm4           # xmm4 = mem[0],zero
	movapd	%xmm3, %xmm1
	subsd	%xmm4, %xmm1
	movapd	%xmm3, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	8(%rdx), %xmm4
	movsd	(%rdi), %xmm5           # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm6, %xmm4
	unpcklpd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	subpd	%xmm4, %xmm2
	addpd	%xmm4, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	subpd	%xmm1, %xmm3
	movupd	%xmm3, (%rsi,%rax,8)
	movupd	(%rdx), %xmm2
	movapd	%xmm2, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%rdx)
	addq	$2, %rax
	addq	%r10, %rdi
	addq	%r8, %rcx
	addq	$-16, %rdx
	cmpq	%r9, %rax
	jl	.LBB6_2
.LBB6_3:                                # %._crit_edge
	retq
.Lfunc_end6:
	.size	rftfsub, .Lfunc_end6-rftfsub
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	rftbsub
	.p2align	4, 0x90
	.type	rftbsub,@function
rftbsub:                                # @rftbsub
	.cfi_startproc
# BB#0:
	movl	%edx, %r8d
	movl	%edi, %r9d
	sarl	%r9d
	cmpl	$3, %r9d
	jl	.LBB7_3
# BB#1:                                 # %.lr.ph.preheader
	leal	(%r8,%r8), %eax
	cltd
	idivl	%r9d
	movslq	%r9d, %r9
	movslq	%edi, %rdx
	movslq	%eax, %r10
	movslq	%r8d, %rax
	leaq	(%rcx,%r10,8), %rdi
	subq	%r10, %rax
	shlq	$3, %r10
	leaq	(%rcx,%rax,8), %rcx
	movq	%r10, %r8
	negq	%r8
	leaq	-16(%rsi,%rdx,8), %rdx
	movl	$2, %eax
	movsd	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm1
	subsd	(%rcx), %xmm1
	movupd	(%rsi,%rax,8), %xmm2
	movupd	(%rdx), %xmm3
	movapd	%xmm2, %xmm4
	subpd	%xmm3, %xmm4
	addpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm5
	movsd	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm5, %xmm1
	movsd	(%rdi), %xmm5           # xmm5 = mem[0],zero
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	shufpd	$1, %xmm4, %xmm3        # xmm3 = xmm3[1],xmm4[0]
	mulpd	%xmm5, %xmm3
	movapd	%xmm1, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm1
	movsd	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1]
	subpd	%xmm1, %xmm2
	movupd	%xmm2, (%rsi,%rax,8)
	movupd	(%rdx), %xmm2
	movapd	%xmm2, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%rdx)
	addq	$2, %rax
	addq	%r10, %rdi
	addq	%r8, %rcx
	addq	$-16, %rdx
	cmpq	%r9, %rax
	jl	.LBB7_2
.LBB7_3:                                # %._crit_edge
	retq
.Lfunc_end7:
	.size	rftbsub, .Lfunc_end7-rftbsub
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	4607182418800017408     # double 1
.LCPI8_1:
	.quad	4605249457297304856     # double 0.78539816339744828
.LCPI8_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	ddct
	.p2align	4, 0x90
	.type	ddct,@function
ddct:                                   # @ddct
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi82:
	.cfi_def_cfa_offset 112
.Lcfi83:
	.cfi_offset %rbx, -56
.Lcfi84:
	.cfi_offset %r12, -48
.Lcfi85:
	.cfi_offset %r13, -40
.Lcfi86:
	.cfi_offset %r14, -32
.Lcfi87:
	.cfi_offset %r15, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %r14
	movl	%esi, 20(%rsp)          # 4-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	(%rbp), %ecx
	leal	(,%rcx,4), %eax
	cmpl	%edi, %eax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	jge	.LBB8_2
# BB#1:
	movl	%edi, %ebx
	sarl	$2, %ebx
	movl	%ebx, %edi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	makewt
	movq	%rbx, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB8_2:
	movl	4(%rbp), %r11d
	cmpl	%edi, %r11d
	jge	.LBB8_8
# BB#3:
	movl	%edi, 4(%rbp)
	cmpl	$2, %edi
	movl	%edi, %r11d
	jl	.LBB8_8
# BB#4:
	movslq	%ecx, %rbx
	leaq	(%r13,%rbx,8), %r15
	movl	%edi, %r12d
	shrl	%r12d
	movsd	.LCPI8_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	callq	atan
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	movsd	.LCPI8_1(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	mulsd	%xmm1, %xmm0
	callq	cos
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movsd	%xmm0, (%r15)
	mulsd	.LCPI8_2(%rip), %xmm0
	movsd	%xmm0, (%r15,%r12,8)
	cmpl	$4, %edi
	movl	%edi, %r11d
	jb	.LBB8_8
# BB#5:                                 # %.lr.ph.preheader.i
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movslq	%edi, %rax
	addq	%rax, %rbx
	leaq	-8(%r13,%rbx,8), %rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB8_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	40(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	callq	cos
	movsd	.LCPI8_2(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r15,%rbp,8)
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	.LCPI8_2(%rip), %xmm0
	movsd	%xmm0, (%rbx)
	incq	%rbp
	addq	$-8, %rbx
	cmpq	%r12, %rbp
	jl	.LBB8_6
# BB#7:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%edi, %r11d
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
.LBB8_8:                                # %makect.exit
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jns	.LBB8_21
# BB#9:
	movq	%rbp, %r9
	movslq	%edi, %rbp
	movsd	-8(%r14,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	leal	-2(%rdi), %edx
	cmpl	$2, %edx
	jl	.LBB8_16
# BB#10:                                # %.lr.ph120.preheader
	movslq	%edx, %rbx
	leaq	-2(%rbx), %rax
	movq	%rax, %rsi
	shrq	%rsi
	btl	$1, %eax
	jb	.LBB8_11
# BB#12:                                # %.lr.ph120.prol
	movsd	(%r14,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	movsd	-24(%r14,%rbp,8), %xmm2 # xmm2 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, -8(%r14,%rbp,8)
	addsd	(%r14,%rbx,8), %xmm2
	movsd	%xmm2, (%r14,%rbx,8)
	testq	%rsi, %rsi
	jne	.LBB8_14
	jmp	.LBB8_16
.LBB8_11:
	movq	%rbx, %rax
	movl	%edi, %edx
	testq	%rsi, %rsi
	je	.LBB8_16
.LBB8_14:                               # %.lr.ph120.preheader.new
	movq	%rax, %r8
	shlq	$32, %r8
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	addq	%r8, %rsi
	movabsq	$-12884901888, %rbx     # imm = 0xFFFFFFFD00000000
	addq	%r8, %rbx
	movabsq	$-17179869184, %r8      # imm = 0xFFFFFFFC00000000
	.p2align	4, 0x90
.LBB8_15:                               # %.lr.ph120
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r14,%rax,8), %xmm1    # xmm1 = mem[0],zero
	movslq	%edx, %rdx
	movsd	-24(%r14,%rdx,8), %xmm2 # xmm2 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, -8(%r14,%rdx,8)
	addsd	(%r14,%rax,8), %xmm2
	movsd	%xmm2, (%r14,%rax,8)
	movsd	-16(%r14,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movq	%rbx, %rdx
	sarq	$29, %rdx
	movsd	(%r14,%rdx), %xmm2      # xmm2 = mem[0],zero
	subsd	%xmm2, %xmm1
	movq	%rsi, %rdx
	sarq	$29, %rdx
	movsd	%xmm1, (%r14,%rdx)
	addsd	-16(%r14,%rax,8), %xmm2
	movsd	%xmm2, -16(%r14,%rax,8)
	addq	%r8, %rsi
	addq	%r8, %rbx
	leal	-2(%rax), %edx
	leaq	-4(%rax), %rax
	cmpq	$1, %rax
	jg	.LBB8_15
.LBB8_16:                               # %._crit_edge121
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	subsd	%xmm0, %xmm2
	movsd	%xmm2, 8(%r14)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%r14)
	cmpl	$5, %edi
	jl	.LBB8_22
# BB#17:
	cmpl	$5, %edi
	je	.LBB8_20
# BB#18:                                # %.lr.ph.preheader.i101
	movslq	%ecx, %rbx
	movl	%edi, %esi
	shrl	%esi
	leal	(%r11,%r11), %eax
	cltd
	idivl	%esi
	movl	%esi, %r8d
	movslq	%eax, %r10
	movslq	%r11d, %rax
	leaq	(%rbx,%r10), %rdx
	leaq	(%r13,%rdx,8), %rsi
	addq	%rbx, %rax
	subq	%r10, %rax
	shlq	$3, %r10
	leaq	(%r13,%rax,8), %rbx
	movq	%r10, %rax
	negq	%rax
	addq	$-2, %rbp
	movl	$2, %edx
	movsd	.LCPI8_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB8_19:                               # %.lr.ph.i104
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm1
	subsd	(%rbx), %xmm1
	movupd	(%r14,%rdx,8), %xmm2
	movupd	(%r14,%rbp,8), %xmm3
	movapd	%xmm2, %xmm4
	subpd	%xmm3, %xmm4
	addpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm5
	movsd	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm5, %xmm1
	movsd	(%rsi), %xmm5           # xmm5 = mem[0],zero
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	shufpd	$1, %xmm4, %xmm3        # xmm3 = xmm3[1],xmm4[0]
	mulpd	%xmm5, %xmm3
	movapd	%xmm1, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm1
	movsd	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1]
	subpd	%xmm1, %xmm2
	movupd	%xmm2, (%r14,%rdx,8)
	movupd	(%r14,%rbp,8), %xmm2
	movapd	%xmm2, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%r14,%rbp,8)
	addq	$2, %rdx
	addq	%r10, %rsi
	addq	%rax, %rbx
	addq	$-2, %rbp
	cmpq	%r8, %rdx
	jl	.LBB8_19
.LBB8_20:                               # %rftbsub.exit
	movq	%r9, %rbp
	leaq	8(%rbp), %rdx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rsi
	movq	%r13, %r8
	movq	%rcx, %rbx
	movq	%r11, %r15
	callq	cftbsub
	movq	%r15, %r11
	movq	%rbx, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB8_21
.LBB8_22:
	cmpl	$4, %edi
	movq	%r9, %rbp
	jne	.LBB8_21
# BB#23:                                # %.thread
	leaq	8(%rbp), %rdx
	movl	$4, %edi
	movq	%r14, %rsi
	movq	%r13, %r8
	movq	%rcx, %rbx
	movq	%r11, %r15
	callq	cftbsub
	movq	%r15, %r11
	movq	%rbx, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movslq	%ecx, %rax
	movq	%r13, %r8
	leaq	(%r13,%rax,8), %r15
	movl	$2, %r13d
	jmp	.LBB8_24
.LBB8_21:
	movslq	%ecx, %rax
	movq	%r13, %r8
	leaq	(%r13,%rax,8), %r15
	movl	%edi, %r13d
	sarl	%r13d
	cmpl	$2, %r13d
	jl	.LBB8_27
.LBB8_24:                               # %.lr.ph.preheader.i105
	movq	%rbp, %r10
	movl	%r11d, %eax
	cltd
	idivl	%edi
	movslq	%edi, %rbx
	movslq	%eax, %rdx
	movq	%r11, %r12
	movslq	%r11d, %rsi
	movl	%r13d, %eax
	subq	%rdx, %rsi
	leaq	(%r15,%rsi,8), %rsi
	leaq	(,%rdx,8), %r9
	movq	%r9, %r11
	negq	%r11
	leaq	(%r15,%rdx,8), %rdx
	decq	%rax
	leaq	8(%r14), %rbp
	leaq	-8(%r14,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB8_25:                               # %.lr.ph.i108
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	movsd	(%rbx), %xmm4           # xmm4 = mem[0],zero
	movapd	%xmm2, %xmm5
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rbp)
	movsd	%xmm3, (%rbx)
	addq	%r11, %rsi
	addq	%r9, %rdx
	addq	$8, %rbp
	addq	$-8, %rbx
	decq	%rax
	jne	.LBB8_25
# BB#26:
	movq	%r10, %rbp
	movq	%r12, %r11
.LBB8_27:                               # %dctsub.exit
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	movslq	%r13d, %r12
	mulsd	(%r14,%r12,8), %xmm0
	movsd	%xmm0, (%r14,%r12,8)
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	js	.LBB8_44
# BB#28:
	cmpl	$5, %edi
	jl	.LBB8_32
# BB#29:
	movq	%r11, %rbx
	addq	$8, %rbp
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rsi
	movq	%rbp, %rdx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	cftfsub
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$3, %r13d
	jl	.LBB8_34
# BB#30:                                # %.lr.ph.preheader.i109
	leal	(%rbx,%rbx), %eax
	cltd
	idivl	%r13d
	movslq	%edi, %rcx
	cltq
	movslq	%ebx, %rsi
	leaq	(%r15,%rax,8), %rdx
	subq	%rax, %rsi
	shlq	$3, %rax
	leaq	(%r15,%rsi,8), %rsi
	movq	%rax, %rbx
	negq	%rbx
	addq	$-2, %rcx
	movl	$2, %ebp
	movsd	.LCPI8_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB8_31:                               # %.lr.ph.i114
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm2
	subsd	(%rsi), %xmm2
	movupd	(%r14,%rbp,8), %xmm3
	movsd	(%r14,%rcx,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm3, %xmm1
	subsd	%xmm4, %xmm1
	movapd	%xmm3, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	8(%r14,%rcx,8), %xmm4
	movsd	(%rdx), %xmm5           # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm6, %xmm4
	unpcklpd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	subpd	%xmm4, %xmm2
	addpd	%xmm4, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	subpd	%xmm1, %xmm3
	movupd	%xmm3, (%r14,%rbp,8)
	movupd	(%r14,%rcx,8), %xmm2
	movapd	%xmm2, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%r14,%rcx,8)
	addq	$2, %rbp
	addq	%rax, %rdx
	addq	%rbx, %rsi
	addq	$-2, %rcx
	cmpq	%r12, %rbp
	jl	.LBB8_31
	jmp	.LBB8_34
.LBB8_32:
	cmpl	$4, %edi
	jne	.LBB8_34
# BB#33:
	addq	$8, %rbp
	movl	$4, %edi
	movq	%r14, %rsi
	movq	%rbp, %rdx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	cftfsub
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB8_34:                               # %rftfsub.exit
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	movsd	8(%r14), %xmm2          # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%r14)
	cmpl	$3, %edi
	jl	.LBB8_43
# BB#35:                                # %.lr.ph.preheader
	movslq	%edi, %rax
	leaq	-3(%rax), %rcx
	shrq	%rcx
	leaq	1(%rcx), %rdx
	cmpq	$2, %rdx
	jae	.LBB8_37
# BB#36:
	movl	$2, %ecx
	jmp	.LBB8_42
.LBB8_37:                               # %min.iters.checked
	movl	%edx, %esi
	andl	$1, %esi
	subq	%rsi, %rdx
	je	.LBB8_38
# BB#39:                                # %vector.body.preheader
	leaq	4(%rcx,%rcx), %rcx
	leaq	(%rsi,%rsi), %rbp
	subq	%rbp, %rcx
	leaq	16(%r14), %rbp
	.p2align	4, 0x90
.LBB8_40:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbp), %xmm1
	movupd	16(%rbp), %xmm2
	movapd	%xmm1, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	unpckhpd	%xmm2, %xmm1    # xmm1 = xmm1[1],xmm2[1]
	movapd	%xmm3, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm3, %xmm1
	movapd	%xmm2, %xmm3
	unpcklpd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0]
	movhlps	%xmm2, %xmm1            # xmm1 = xmm2[1],xmm1[1]
	movups	%xmm1, 8(%rbp)
	movupd	%xmm3, -8(%rbp)
	addq	$32, %rbp
	addq	$-2, %rdx
	jne	.LBB8_40
# BB#41:                                # %middle.block
	testq	%rsi, %rsi
	jne	.LBB8_42
	jmp	.LBB8_43
.LBB8_38:
	movl	$2, %ecx
	.p2align	4, 0x90
.LBB8_42:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r14,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	%rcx, %rdx
	orq	$1, %rdx
	movsd	(%r14,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	subsd	%xmm2, %xmm3
	movsd	%xmm3, -8(%r14,%rcx,8)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%r14,%rcx,8)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jl	.LBB8_42
.LBB8_43:                               # %._crit_edge
	movslq	%edi, %rax
	movsd	%xmm0, -8(%r14,%rax,8)
.LBB8_44:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	ddct, .Lfunc_end8-ddct
	.cfi_endproc

	.globl	dctsub
	.p2align	4, 0x90
	.type	dctsub,@function
dctsub:                                 # @dctsub
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 16
.Lcfi90:
	.cfi_offset %rbx, -16
	movl	%edx, %r8d
	movl	%edi, %r9d
	sarl	%r9d
	cmpl	$2, %r9d
	jl	.LBB9_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%r8d, %eax
	cltd
	idivl	%edi
	movslq	%edi, %rbx
	cltq
	movslq	%r8d, %rdx
	movl	%r9d, %r11d
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,8), %rdx
	leaq	(,%rax,8), %r8
	movq	%r8, %r10
	negq	%r10
	leaq	(%rcx,%rax,8), %rdi
	leaq	-8(%rsi,%rbx,8), %rax
	leaq	8(%rsi), %rbx
	decq	%r11
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	movsd	(%rax), %xmm4           # xmm4 = mem[0],zero
	movapd	%xmm2, %xmm5
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rbx)
	movsd	%xmm3, (%rax)
	addq	%r10, %rdx
	addq	%r8, %rdi
	addq	$-8, %rax
	addq	$8, %rbx
	decq	%r11
	jne	.LBB9_2
.LBB9_3:                                # %._crit_edge
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movslq	%r9d, %rax
	mulsd	(%rsi,%rax,8), %xmm0
	movsd	%xmm0, (%rsi,%rax,8)
	popq	%rbx
	retq
.Lfunc_end9:
	.size	dctsub, .Lfunc_end9-dctsub
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	4607182418800017408     # double 1
.LCPI10_1:
	.quad	4605249457297304856     # double 0.78539816339744828
.LCPI10_2:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_3:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	ddst
	.p2align	4, 0x90
	.type	ddst,@function
ddst:                                   # @ddst
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi97:
	.cfi_def_cfa_offset 112
.Lcfi98:
	.cfi_offset %rbx, -56
.Lcfi99:
	.cfi_offset %r12, -48
.Lcfi100:
	.cfi_offset %r13, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %r14
	movl	%esi, 20(%rsp)          # 4-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	(%rbp), %ecx
	leal	(,%rcx,4), %eax
	cmpl	%edi, %eax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	jge	.LBB10_2
# BB#1:
	movl	%edi, %ebx
	sarl	$2, %ebx
	movl	%ebx, %edi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	makewt
	movq	%rbx, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB10_2:
	movl	4(%rbp), %r11d
	cmpl	%edi, %r11d
	jge	.LBB10_8
# BB#3:
	movl	%edi, 4(%rbp)
	cmpl	$2, %edi
	movl	%edi, %r11d
	jl	.LBB10_8
# BB#4:
	movslq	%ecx, %rbx
	leaq	(%r13,%rbx,8), %r15
	movl	%edi, %r12d
	shrl	%r12d
	movsd	.LCPI10_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	callq	atan
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	movsd	.LCPI10_1(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	mulsd	%xmm1, %xmm0
	callq	cos
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movsd	%xmm0, (%r15)
	mulsd	.LCPI10_2(%rip), %xmm0
	movsd	%xmm0, (%r15,%r12,8)
	cmpl	$4, %edi
	movl	%edi, %r11d
	jb	.LBB10_8
# BB#5:                                 # %.lr.ph.preheader.i
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movslq	%edi, %rax
	addq	%rax, %rbx
	leaq	-8(%r13,%rbx,8), %rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB10_6:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	40(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	callq	cos
	movsd	.LCPI10_2(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r15,%rbp,8)
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	.LCPI10_2(%rip), %xmm0
	movsd	%xmm0, (%rbx)
	incq	%rbp
	addq	$-8, %rbx
	cmpq	%r12, %rbp
	jl	.LBB10_6
# BB#7:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%edi, %r11d
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
.LBB10_8:                               # %makect.exit
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jns	.LBB10_22
# BB#9:
	movq	%rbp, %r9
	movslq	%edi, %rbp
	movsd	-8(%r14,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	leal	-2(%rdi), %edx
	cmpl	$2, %edx
	jl	.LBB10_15
# BB#10:                                # %.lr.ph120.preheader
	movslq	%edx, %rbx
	leaq	-2(%rbx), %rax
	movq	%rax, %rsi
	shrq	%rsi
	btl	$1, %eax
	jb	.LBB10_12
# BB#11:                                # %.lr.ph120.prol
	movsd	(%r14,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	xorpd	.LCPI10_3(%rip), %xmm1
	movsd	-24(%r14,%rbp,8), %xmm2 # xmm2 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, -8(%r14,%rbp,8)
	movsd	(%r14,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movsd	%xmm1, (%r14,%rbx,8)
	testq	%rsi, %rsi
	jne	.LBB10_13
	jmp	.LBB10_15
.LBB10_12:
	movq	%rbx, %rax
	movl	%edi, %edx
	testq	%rsi, %rsi
	je	.LBB10_15
.LBB10_13:                              # %.lr.ph120.preheader.new
	movq	%rax, %r8
	shlq	$32, %r8
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	addq	%r8, %rsi
	movabsq	$-12884901888, %rbx     # imm = 0xFFFFFFFD00000000
	addq	%r8, %rbx
	movabsq	$-17179869184, %r8      # imm = 0xFFFFFFFC00000000
	movapd	.LCPI10_3(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB10_14:                              # %.lr.ph120
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r14,%rax,8), %xmm2    # xmm2 = mem[0],zero
	xorpd	%xmm1, %xmm2
	movslq	%edx, %rdx
	movsd	-24(%r14,%rdx,8), %xmm3 # xmm3 = mem[0],zero
	subsd	%xmm3, %xmm2
	movsd	%xmm2, -8(%r14,%rdx,8)
	movsd	(%r14,%rax,8), %xmm2    # xmm2 = mem[0],zero
	subsd	%xmm3, %xmm2
	movsd	%xmm2, (%r14,%rax,8)
	movsd	-16(%r14,%rax,8), %xmm2 # xmm2 = mem[0],zero
	xorpd	%xmm1, %xmm2
	movq	%rbx, %rdx
	sarq	$29, %rdx
	movsd	(%r14,%rdx), %xmm3      # xmm3 = mem[0],zero
	subsd	%xmm3, %xmm2
	movq	%rsi, %rdx
	sarq	$29, %rdx
	movsd	%xmm2, (%r14,%rdx)
	movsd	-16(%r14,%rax,8), %xmm2 # xmm2 = mem[0],zero
	subsd	%xmm3, %xmm2
	movsd	%xmm2, -16(%r14,%rax,8)
	addq	%r8, %rsi
	addq	%r8, %rbx
	leal	-2(%rax), %edx
	leaq	-4(%rax), %rax
	cmpq	$1, %rax
	jg	.LBB10_14
.LBB10_15:                              # %._crit_edge121
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 8(%r14)
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%r14)
	cmpl	$5, %edi
	jl	.LBB10_20
# BB#16:
	cmpl	$5, %edi
	je	.LBB10_19
# BB#17:                                # %.lr.ph.preheader.i101
	movslq	%ecx, %rbx
	movl	%edi, %esi
	shrl	%esi
	leal	(%r11,%r11), %eax
	cltd
	idivl	%esi
	movl	%esi, %r8d
	movslq	%eax, %r10
	movslq	%r11d, %rax
	leaq	(%rbx,%r10), %rdx
	leaq	(%r13,%rdx,8), %rsi
	addq	%rbx, %rax
	subq	%r10, %rax
	shlq	$3, %r10
	leaq	(%r13,%rax,8), %rbx
	movq	%r10, %rax
	negq	%rax
	addq	$-2, %rbp
	movl	$2, %edx
	movsd	.LCPI10_2(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB10_18:                              # %.lr.ph.i104
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm1
	subsd	(%rbx), %xmm1
	movupd	(%r14,%rdx,8), %xmm2
	movupd	(%r14,%rbp,8), %xmm3
	movapd	%xmm2, %xmm4
	subpd	%xmm3, %xmm4
	addpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm5
	movsd	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm5, %xmm1
	movsd	(%rsi), %xmm5           # xmm5 = mem[0],zero
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	shufpd	$1, %xmm4, %xmm3        # xmm3 = xmm3[1],xmm4[0]
	mulpd	%xmm5, %xmm3
	movapd	%xmm1, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm1
	movsd	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1]
	subpd	%xmm1, %xmm2
	movupd	%xmm2, (%r14,%rdx,8)
	movupd	(%r14,%rbp,8), %xmm2
	movapd	%xmm2, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%r14,%rbp,8)
	addq	$2, %rdx
	addq	%r10, %rsi
	addq	%rax, %rbx
	addq	$-2, %rbp
	cmpq	%r8, %rdx
	jl	.LBB10_18
.LBB10_19:                              # %rftbsub.exit
	movq	%r9, %rbp
	leaq	8(%rbp), %rdx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rsi
	movq	%r13, %r8
	movq	%rcx, %rbx
	movq	%r11, %r15
	callq	cftbsub
	movq	%r15, %r11
	movq	%rbx, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB10_22
.LBB10_20:
	cmpl	$4, %edi
	movq	%r9, %rbp
	jne	.LBB10_22
# BB#21:                                # %.thread
	leaq	8(%rbp), %rdx
	movl	$4, %edi
	movq	%r14, %rsi
	movq	%r13, %r8
	movq	%rcx, %rbx
	movq	%r11, %r15
	callq	cftbsub
	movq	%r15, %r11
	movq	%rbx, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movslq	%ecx, %rax
	movq	%r13, %r8
	leaq	(%r13,%rax,8), %r15
	movl	$2, %r13d
	jmp	.LBB10_23
.LBB10_22:
	movslq	%ecx, %rax
	movq	%r13, %r8
	leaq	(%r13,%rax,8), %r15
	movl	%edi, %r13d
	sarl	%r13d
	cmpl	$2, %r13d
	jl	.LBB10_26
.LBB10_23:                              # %.lr.ph.preheader.i105
	movq	%rbp, %r10
	movl	%r11d, %eax
	cltd
	idivl	%edi
	movslq	%edi, %rbx
	movslq	%eax, %rdx
	movq	%r11, %r12
	movslq	%r11d, %rsi
	movl	%r13d, %eax
	subq	%rdx, %rsi
	leaq	(%r15,%rsi,8), %rsi
	leaq	(,%rdx,8), %r9
	movq	%r9, %r11
	negq	%r11
	leaq	(%r15,%rdx,8), %rdx
	decq	%rax
	leaq	8(%r14), %rbp
	leaq	-8(%r14,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB10_24:                              # %.lr.ph.i108
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	movsd	(%rbp), %xmm4           # xmm4 = mem[0],zero
	movapd	%xmm2, %xmm5
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rbx)
	movsd	%xmm3, (%rbp)
	addq	%r11, %rsi
	addq	%r9, %rdx
	addq	$8, %rbp
	addq	$-8, %rbx
	decq	%rax
	jne	.LBB10_24
# BB#25:
	movq	%r10, %rbp
	movq	%r12, %r11
.LBB10_26:                              # %dstsub.exit
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	movslq	%r13d, %r12
	mulsd	(%r14,%r12,8), %xmm0
	movsd	%xmm0, (%r14,%r12,8)
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	js	.LBB10_44
# BB#27:
	cmpl	$5, %edi
	jl	.LBB10_31
# BB#28:
	movq	%r11, %rbx
	addq	$8, %rbp
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rsi
	movq	%rbp, %rdx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	cftfsub
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$3, %r13d
	jl	.LBB10_33
# BB#29:                                # %.lr.ph.preheader.i109
	leal	(%rbx,%rbx), %eax
	cltd
	idivl	%r13d
	movslq	%edi, %rcx
	cltq
	movslq	%ebx, %rsi
	leaq	(%r15,%rax,8), %rdx
	subq	%rax, %rsi
	shlq	$3, %rax
	leaq	(%r15,%rsi,8), %rsi
	movq	%rax, %rbx
	negq	%rbx
	addq	$-2, %rcx
	movl	$2, %ebp
	movsd	.LCPI10_2(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB10_30:                              # %.lr.ph.i114
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm2
	subsd	(%rsi), %xmm2
	movupd	(%r14,%rbp,8), %xmm3
	movsd	(%r14,%rcx,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm3, %xmm1
	subsd	%xmm4, %xmm1
	movapd	%xmm3, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	8(%r14,%rcx,8), %xmm4
	movsd	(%rdx), %xmm5           # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm6, %xmm4
	unpcklpd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	subpd	%xmm4, %xmm2
	addpd	%xmm4, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	subpd	%xmm1, %xmm3
	movupd	%xmm3, (%r14,%rbp,8)
	movupd	(%r14,%rcx,8), %xmm2
	movapd	%xmm2, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%r14,%rcx,8)
	addq	$2, %rbp
	addq	%rax, %rdx
	addq	%rbx, %rsi
	addq	$-2, %rcx
	cmpq	%r12, %rbp
	jl	.LBB10_30
	jmp	.LBB10_33
.LBB10_31:
	cmpl	$4, %edi
	jne	.LBB10_33
# BB#32:
	addq	$8, %rbp
	movl	$4, %edi
	movq	%r14, %rsi
	movq	%rbp, %rdx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	cftfsub
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB10_33:                              # %rftfsub.exit
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	movsd	8(%r14), %xmm2          # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%r14)
	cmpl	$3, %edi
	jl	.LBB10_43
# BB#34:                                # %.lr.ph.preheader
	movslq	%edi, %rax
	leaq	-3(%rax), %rcx
	shrq	%rcx
	leaq	1(%rcx), %rdx
	cmpq	$2, %rdx
	jb	.LBB10_40
# BB#36:                                # %min.iters.checked
	movl	%edx, %esi
	andl	$1, %esi
	subq	%rsi, %rdx
	je	.LBB10_40
# BB#37:                                # %vector.body.preheader
	leaq	4(%rcx,%rcx), %rcx
	leaq	(%rsi,%rsi), %rbp
	subq	%rbp, %rcx
	leaq	16(%r14), %rbp
	movapd	.LCPI10_3(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB10_38:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbp), %xmm2
	movupd	16(%rbp), %xmm3
	movapd	%xmm2, %xmm4
	unpcklpd	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0]
	unpckhpd	%xmm3, %xmm2    # xmm2 = xmm2[1],xmm3[1]
	movapd	%xmm4, %xmm3
	xorpd	%xmm1, %xmm3
	subpd	%xmm2, %xmm3
	subpd	%xmm2, %xmm4
	movapd	%xmm3, %xmm2
	unpcklpd	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0]
	movhlps	%xmm3, %xmm4            # xmm4 = xmm3[1],xmm4[1]
	movups	%xmm4, 8(%rbp)
	movupd	%xmm2, -8(%rbp)
	addq	$32, %rbp
	addq	$-2, %rdx
	jne	.LBB10_38
# BB#39:                                # %middle.block
	testq	%rsi, %rsi
	jne	.LBB10_41
	jmp	.LBB10_43
.LBB10_40:
	movl	$2, %ecx
.LBB10_41:                              # %.lr.ph.preheader125
	movapd	.LCPI10_3(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB10_42:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r14,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm3
	xorpd	%xmm1, %xmm3
	movq	%rcx, %rdx
	orq	$1, %rdx
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movsd	(%r14,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	subpd	%xmm2, %xmm3
	movupd	%xmm3, -8(%r14,%rcx,8)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jl	.LBB10_42
.LBB10_43:                              # %._crit_edge
	xorpd	.LCPI10_3(%rip), %xmm0
	movslq	%edi, %rax
	movlpd	%xmm0, -8(%r14,%rax,8)
.LBB10_44:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	ddst, .Lfunc_end10-ddst
	.cfi_endproc

	.globl	dstsub
	.p2align	4, 0x90
	.type	dstsub,@function
dstsub:                                 # @dstsub
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 16
.Lcfi105:
	.cfi_offset %rbx, -16
	movl	%edx, %r8d
	movl	%edi, %r9d
	sarl	%r9d
	cmpl	$2, %r9d
	jl	.LBB11_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%r8d, %eax
	cltd
	idivl	%edi
	movslq	%edi, %rbx
	cltq
	movslq	%r8d, %rdx
	movl	%r9d, %r11d
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,8), %rdx
	leaq	(,%rax,8), %r8
	movq	%r8, %r10
	negq	%r10
	leaq	(%rcx,%rax,8), %rdi
	leaq	-8(%rsi,%rbx,8), %rax
	leaq	8(%rsi), %rbx
	decq	%r11
	.p2align	4, 0x90
.LBB11_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	movsd	(%rbx), %xmm4           # xmm4 = mem[0],zero
	movapd	%xmm2, %xmm5
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rax)
	movsd	%xmm3, (%rbx)
	addq	%r10, %rdx
	addq	%r8, %rdi
	addq	$-8, %rax
	addq	$8, %rbx
	decq	%r11
	jne	.LBB11_2
.LBB11_3:                               # %._crit_edge
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movslq	%r9d, %rax
	mulsd	(%rsi,%rax,8), %xmm0
	movsd	%xmm0, (%rsi,%rax,8)
	popq	%rbx
	retq
.Lfunc_end11:
	.size	dstsub, .Lfunc_end11-dstsub
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_0:
	.quad	4607182418800017408     # double 1
.LCPI12_1:
	.quad	4605249457297304856     # double 0.78539816339744828
.LCPI12_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	dfct
	.p2align	4, 0x90
	.type	dfct,@function
dfct:                                   # @dfct
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi112:
	.cfi_def_cfa_offset 160
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movl	%edi, %ebp
	movl	(%r14), %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	leal	(,%rax,8), %eax
	cmpl	%ebp, %eax
	movq	%r8, 40(%rsp)           # 8-byte Spill
	jge	.LBB12_2
# BB#1:
	movl	%ebp, %edi
	sarl	$3, %edi
	movq	%rdi, (%rsp)            # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rsi
	movq	%r8, %rdx
	callq	makewt
	movq	40(%rsp), %r8           # 8-byte Reload
.LBB12_2:
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movl	4(%r14), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leal	(%rax,%rax), %eax
	movl	%ebp, %r13d
	sarl	%r13d
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	cmpl	%ebp, %eax
	jge	.LBB12_8
# BB#3:
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%r13d, 4(%rax)
	cmpl	$2, %r13d
	movl	%r13d, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jl	.LBB12_8
# BB#4:
	movslq	(%rsp), %rbp            # 4-byte Folded Reload
	leaq	(%r8,%rbp,8), %r15
	movl	%r13d, %r14d
	shrl	%r14d
	movsd	.LCPI12_0(%rip), %xmm0  # xmm0 = mem[0],zero
	callq	atan
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	movsd	.LCPI12_1(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	mulsd	%xmm1, %xmm0
	callq	cos
	movq	40(%rsp), %r8           # 8-byte Reload
	movsd	%xmm0, (%r15)
	mulsd	.LCPI12_2(%rip), %xmm0
	movsd	%xmm0, (%r15,%r14,8)
	cmpl	$4, %r13d
	movl	%r13d, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jb	.LBB12_8
# BB#5:                                 # %.lr.ph.preheader.i
	movslq	%r13d, %rax
	addq	%rax, %rbp
	leaq	-8(%r8,%rbp,8), %r12
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB12_6:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	64(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	callq	cos
	movsd	.LCPI12_2(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r15,%rbp,8)
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	.LCPI12_2(%rip), %xmm0
	movsd	%xmm0, (%r12)
	incq	%rbp
	addq	$-8, %r12
	cmpq	%r14, %rbp
	jl	.LBB12_6
# BB#7:
	movl	%r13d, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	40(%rsp), %r8           # 8-byte Reload
.LBB12_8:                               # %makect.exit
	movslq	%r13d, %rbp
	movq	56(%rsp), %r15          # 8-byte Reload
	movsd	(%r15,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r15), %xmm1           # xmm1 = mem[0],zero
	movl	20(%rsp), %r9d          # 4-byte Reload
	movslq	%r9d, %r10
	movsd	(%r15,%r10,8), %xmm2    # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	subsd	%xmm2, %xmm1
	movsd	%xmm1, (%r15)
	movapd	%xmm3, %xmm1
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx)
	addsd	%xmm0, %xmm3
	movsd	%xmm3, (%rbx,%rbp,8)
	cmpl	$3, %r9d
	jl	.LBB12_56
# BB#9:
	movl	%r9d, %r11d
	shrl	$2, %r11d
	cmpl	$8, %r9d
	jb	.LBB12_12
# BB#10:                                # %.lr.ph291.preheader
	movl	%r11d, %eax
	leaq	(,%r10,8), %rcx
	leaq	(,%rbp,8), %rdx
	subq	%rdx, %rcx
	addq	%r15, %rcx
	leaq	-1(%rbp), %rdx
	leaq	-8(,%r10,8), %rsi
	movl	$1, %edi
	.p2align	4, 0x90
.LBB12_11:                              # %.lr.ph291
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r15,%rsi), %xmm1      # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	movsd	(%r15,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movsd	(%rcx,%rdi,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	addsd	%xmm3, %xmm1
	movsd	%xmm2, (%r15,%rdi,8)
	movsd	%xmm4, (%r15,%rdx,8)
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx,%rdi,8)
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx,%rdx,8)
	incq	%rdi
	decq	%rdx
	addq	$-8, %rsi
	cmpq	%rax, %rdi
	jl	.LBB12_11
.LBB12_12:                              # %._crit_edge292
	movq	%r10, 72(%rsp)          # 8-byte Spill
	movl	%r11d, %eax
	movsd	(%r15,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movl	%r9d, %ecx
	subl	%r11d, %ecx
	movslq	%ecx, %rcx
	addsd	(%r15,%rcx,8), %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	movsd	(%r15,%rax,8), %xmm0    # xmm0 = mem[0],zero
	subsd	(%r15,%rcx,8), %xmm0
	movsd	%xmm0, (%r15,%rax,8)
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r9d, %r12d
	sarl	$2, %r12d
	cmpl	$2, %r12d
	jl	.LBB12_15
# BB#13:                                # %.lr.ph.preheader.i249
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, %eax
	cltd
	idivl	%r13d
	movslq	%eax, %rdx
	movslq	%ecx, %rcx
	movl	%r12d, %eax
	movq	32(%rsp), %rsi          # 8-byte Reload
	addq	%rsi, %rcx
	subq	%rdx, %rcx
	leaq	(%r8,%rcx,8), %rcx
	leaq	(,%rdx,8), %r10
	movq	%r10, %r9
	negq	%r9
	addq	%rsi, %rdx
	leaq	(%r8,%rdx,8), %rdi
	decq	%rax
	leaq	8(%r15), %rdx
	leaq	-8(%r15,%rbp,8), %rsi
	.p2align	4, 0x90
.LBB12_14:                              # %.lr.ph.i252
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	movsd	(%rsi), %xmm4           # xmm4 = mem[0],zero
	movapd	%xmm2, %xmm5
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rdx)
	movsd	%xmm3, (%rsi)
	addq	%r9, %rcx
	addq	%r10, %rdi
	addq	$8, %rdx
	addq	$-8, %rsi
	decq	%rax
	jne	.LBB12_14
.LBB12_15:                              # %dctsub.exit
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	(%r8,%rax,8), %xmm0     # xmm0 = mem[0],zero
	movq	%r15, %r14
	movslq	%r12d, %r15
	mulsd	(%r14,%r15,8), %xmm0
	movsd	%xmm0, (%r14,%r15,8)
	cmpl	$5, %r13d
	jl	.LBB12_19
# BB#16:
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rdx
	movl	%r13d, %edi
	movq	%r14, %rsi
	movq	(%rsp), %rcx            # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%r11, 48(%rsp)          # 8-byte Spill
	callq	cftfsub
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	cmpl	$3, %r12d
	jl	.LBB12_21
# BB#17:                                # %.lr.ph.preheader.i253
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	(%rcx,%rcx), %eax
	cltd
	idivl	%r12d
	movslq	%eax, %r9
	movslq	%ecx, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rdx
	leaq	(%rdx,%r9), %rcx
	leaq	(%r8,%rcx,8), %rcx
	addq	%rdx, %rax
	subq	%r9, %rax
	shlq	$3, %r9
	leaq	(%r8,%rax,8), %rdx
	movq	%r9, %rsi
	negq	%rsi
	leaq	-2(%rbp), %rdi
	movl	$2, %eax
	movsd	.LCPI12_2(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB12_18:                              # %.lr.ph.i256
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm2
	subsd	(%rdx), %xmm2
	movupd	(%r14,%rax,8), %xmm3
	movsd	(%r14,%rdi,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm3, %xmm1
	subsd	%xmm4, %xmm1
	movapd	%xmm3, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	8(%r14,%rdi,8), %xmm4
	movsd	(%rcx), %xmm5           # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm6, %xmm4
	unpcklpd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	subpd	%xmm4, %xmm2
	addpd	%xmm4, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	subpd	%xmm1, %xmm3
	movupd	%xmm3, (%r14,%rax,8)
	movupd	(%r14,%rdi,8), %xmm2
	movapd	%xmm2, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%r14,%rdi,8)
	addq	$2, %rax
	addq	%r9, %rcx
	addq	%rsi, %rdx
	addq	$-2, %rdi
	cmpq	%r15, %rax
	jl	.LBB12_18
	jmp	.LBB12_21
.LBB12_56:
	movq	(%r15), %rax
	movq	%rax, 8(%r15)
	movq	(%rbx), %rax
	movq	%rax, 16(%r15)
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	jmp	.LBB12_57
.LBB12_19:
	cmpl	$4, %r13d
	jne	.LBB12_21
# BB#20:
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rdx
	movl	$4, %edi
	movq	%r14, %rsi
	movq	(%rsp), %rcx            # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%r11, %r15
	callq	cftfsub
	movq	%r15, %r11
	movq	40(%rsp), %r8           # 8-byte Reload
.LBB12_21:                              # %rftfsub.exit
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	subsd	8(%r14), %xmm0
	movq	72(%rsp), %rax          # 8-byte Reload
	movsd	%xmm0, -8(%r14,%rax,8)
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	addsd	8(%r14), %xmm0
	movsd	%xmm0, 8(%r14)
	leal	-2(%r13), %eax
	cmpl	$2, %eax
	movq	%r14, %r15
	jl	.LBB12_24
# BB#22:                                # %.lr.ph288.preheader
	addq	$-2, %rbp
	leal	-5(%r13,%r13), %eax
	cltq
	leaq	(%r15,%rax,8), %rax
	leal	-3(%r13,%r13), %ecx
	movslq	%ecx, %rcx
	leaq	(%r15,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB12_23:                              # %.lr.ph288
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	addsd	8(%r15,%rbp,8), %xmm0
	movsd	%xmm0, (%rcx)
	movsd	(%r15,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	subsd	8(%r15,%rbp,8), %xmm0
	movsd	%xmm0, (%rax)
	addq	$-2, %rbp
	addq	$-32, %rax
	addq	$-32, %rcx
	cmpq	$1, %rbp
	jg	.LBB12_23
.LBB12_24:                              # %.preheader
	cmpl	$7, 20(%rsp)            # 4-byte Folded Reload
	jbe	.LBB12_25
# BB#26:                                # %.lr.ph283
	movq	8(%rsp), %rcx           # 8-byte Reload
	movslq	%ecx, %rax
	addq	$8, 24(%rsp)            # 8-byte Folded Spill
	leal	(%rcx,%rcx), %ecx
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	addq	%rcx, %rax
	leaq	(%r8,%rax,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	(%r8,%rcx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	$2, %r13d
	jmp	.LBB12_27
.LBB12_34:                              # %.lr.ph.preheader.i266
                                        #   in Loop: Header=BB12_27 Depth=1
	movl	84(%rsp), %eax          # 4-byte Reload
	cltd
	idivl	48(%rsp)                # 4-byte Folded Reload
	cltq
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	shlq	$3, %rax
	movq	96(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movq	%rax, %rsi
	negq	%rsi
	addq	$-2, %r15
	movl	$2, %edi
	movsd	.LCPI12_2(%rip), %xmm6  # xmm6 = mem[0],zero
	.p2align	4, 0x90
.LBB12_35:                              # %.lr.ph.i271
                                        #   Parent Loop BB12_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm6, %xmm1
	subsd	(%rdx), %xmm1
	movupd	(%rbx,%rdi,8), %xmm2
	movsd	(%rbx,%r15,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm2, %xmm0
	subsd	%xmm3, %xmm0
	movapd	%xmm2, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	addsd	8(%rbx,%r15,8), %xmm3
	movsd	(%rcx), %xmm4           # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm5
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm5, %xmm3
	unpcklpd	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0]
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	subpd	%xmm3, %xmm1
	addpd	%xmm3, %xmm0
	movsd	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1]
	subpd	%xmm0, %xmm2
	movupd	%xmm2, (%rbx,%rdi,8)
	movupd	(%rbx,%r15,8), %xmm1
	movapd	%xmm1, %xmm2
	addpd	%xmm0, %xmm2
	subpd	%xmm0, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	movupd	%xmm1, (%rbx,%r15,8)
	addq	$2, %rdi
	addq	%rax, %rcx
	addq	%rsi, %rdx
	addq	$-2, %r15
	cmpq	%r12, %rdi
	jl	.LBB12_35
# BB#36:                                #   in Loop: Header=BB12_27 Depth=1
	movq	56(%rsp), %r15          # 8-byte Reload
	jmp	.LBB12_39
	.p2align	4, 0x90
.LBB12_27:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_30 Depth 2
                                        #     Child Loop BB12_35 Depth 2
                                        #     Child Loop BB12_45 Depth 2
                                        #     Child Loop BB12_52 Depth 2
	movl	%r11d, %r14d
	shrl	%r11d
	cmpl	$3, %r14d
	movq	%r11, 48(%rsp)          # 8-byte Spill
	jbe	.LBB12_28
# BB#29:                                # %.lr.ph.preheader.i258
                                        #   in Loop: Header=BB12_27 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	%r14d
	movl	%r14d, %r15d
	movslq	%eax, %rsi
	movl	%r11d, %r12d
	leaq	(,%rsi,8), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	movq	%rax, %rdx
	negq	%rdx
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rsi,8), %rsi
	leaq	-1(%r15), %rdi
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB12_30:                              # %.lr.ph.i264
                                        #   Parent Loop BB12_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	movsd	(%rbx,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	movsd	(%rbx,%rdi,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm2, %xmm5
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rbx,%rbp,8)
	movsd	%xmm3, (%rbx,%rdi,8)
	incq	%rbp
	addq	%rdx, %rcx
	addq	%rax, %rsi
	decq	%rdi
	cmpq	%rbp, %r12
	jne	.LBB12_30
# BB#31:                                # %dctsub.exit265
                                        #   in Loop: Header=BB12_27 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	(%r8,%rax,8), %xmm0     # xmm0 = mem[0],zero
	leaq	(%rbx,%r12,8), %rax
	mulsd	(%rbx,%r12,8), %xmm0
	movsd	%xmm0, (%rbx,%r12,8)
	cmpl	$5, %r14d
	movq	%rax, 64(%rsp)          # 8-byte Spill
	jb	.LBB12_37
# BB#32:                                #   in Loop: Header=BB12_27 Depth=1
	movl	%r14d, %edi
	movq	%rbx, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	cftfsub
	cmpl	$5, %r14d
	jne	.LBB12_34
# BB#33:                                #   in Loop: Header=BB12_27 Depth=1
	movq	56(%rsp), %r15          # 8-byte Reload
	jmp	.LBB12_39
	.p2align	4, 0x90
.LBB12_28:                              # %.thread
                                        #   in Loop: Header=BB12_27 Depth=1
	movl	%r11d, %r12d
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	(%r8,%rax,8), %xmm0     # xmm0 = mem[0],zero
	leaq	(%rbx,%r12,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	mulsd	(%rbx,%r12,8), %xmm0
	movsd	%xmm0, (%rbx,%r12,8)
.LBB12_39:                              # %rftfsub.exit272
                                        #   in Loop: Header=BB12_27 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	subsd	8(%rbx), %xmm0
	movl	20(%rsp), %eax          # 4-byte Reload
	subl	%r13d, %eax
	cltq
	movsd	%xmm0, (%r15,%rax,8)
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	addsd	8(%rbx), %xmm0
	movslq	%r13d, %r9
	movsd	%xmm0, (%r15,%r9,8)
	cmpl	$3, %r14d
	jb	.LBB12_46
# BB#40:                                # %.lr.ph
                                        #   in Loop: Header=BB12_27 Depth=1
	leal	(,%r13,4), %eax
	movl	%r14d, %r10d
	movslq	%eax, %r8
	leaq	-3(%r10), %rcx
	movq	%rcx, %rax
	shrq	%rax
	btl	$1, %ecx
	jb	.LBB12_41
# BB#42:                                #   in Loop: Header=BB12_27 Depth=1
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	subsd	24(%rbx), %xmm0
	movq	%r8, %rcx
	subq	%r9, %rcx
	movsd	%xmm0, (%r15,%rcx,8)
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	addsd	24(%rbx), %xmm0
	leaq	(%r8,%r9), %rcx
	movsd	%xmm0, (%r15,%rcx,8)
	movq	%r8, %r11
	movl	$4, %esi
	testq	%rax, %rax
	jne	.LBB12_44
	jmp	.LBB12_46
	.p2align	4, 0x90
.LBB12_37:                              #   in Loop: Header=BB12_27 Depth=1
	cmpl	$4, %r14d
	movq	56(%rsp), %r15          # 8-byte Reload
	jne	.LBB12_39
# BB#38:                                #   in Loop: Header=BB12_27 Depth=1
	movl	$4, %edi
	movq	%rbx, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	cftfsub
	jmp	.LBB12_39
	.p2align	4, 0x90
.LBB12_41:                              #   in Loop: Header=BB12_27 Depth=1
	xorl	%r11d, %r11d
	movl	$2, %esi
	testq	%rax, %rax
	je	.LBB12_46
.LBB12_44:                              # %.lr.ph.new
                                        #   in Loop: Header=BB12_27 Depth=1
	leaq	(%r11,%r8,2), %rbp
	movq	%r8, %rdi
	shlq	$4, %rdi
	leaq	(%rbp,%r9), %rcx
	subq	%r9, %rbp
	leaq	(%r11,%r9), %rax
	addq	%r8, %rax
	addq	%r8, %r11
	subq	%r9, %r11
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB12_45:                              #   Parent Loop BB12_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	subsd	8(%rbx,%rsi,8), %xmm0
	movsd	%xmm0, (%rdx,%r11,8)
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addsd	8(%rbx,%rsi,8), %xmm0
	movsd	%xmm0, (%rdx,%rax,8)
	movsd	16(%rbx,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	subsd	24(%rbx,%rsi,8), %xmm0
	movsd	%xmm0, (%rdx,%rbp,8)
	movsd	16(%rbx,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	addsd	24(%rbx,%rsi,8), %xmm0
	movsd	%xmm0, (%rdx,%rcx,8)
	addq	$4, %rsi
	addq	%rdi, %rdx
	cmpq	%r10, %rsi
	jl	.LBB12_45
.LBB12_46:                              # %._crit_edge
                                        #   in Loop: Header=BB12_27 Depth=1
	movq	48(%rsp), %r11          # 8-byte Reload
	testl	%r11d, %r11d
	movq	40(%rsp), %r8           # 8-byte Reload
	je	.LBB12_53
# BB#47:                                # %.lr.ph277.preheader
                                        #   in Loop: Header=BB12_27 Depth=1
	movl	%r14d, %edx
	testb	$1, %r12b
	jne	.LBB12_49
# BB#48:                                #   in Loop: Header=BB12_27 Depth=1
	xorl	%eax, %eax
	cmpq	$1, %r12
	jne	.LBB12_51
	jmp	.LBB12_53
	.p2align	4, 0x90
.LBB12_49:                              # %.lr.ph277.prol
                                        #   in Loop: Header=BB12_27 Depth=1
	leal	(%r14,%r14), %eax
	movsd	(%rbx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	subsd	(%rbx,%rdx,8), %xmm0
	movsd	%xmm0, (%rbx)
	movsd	(%rbx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rbx,%rdx,8), %xmm0
	movsd	%xmm0, (%rbx,%rdx,8)
	movl	$1, %eax
	cmpq	$1, %r12
	je	.LBB12_53
.LBB12_51:                              # %.lr.ph277.preheader.new
                                        #   in Loop: Header=BB12_27 Depth=1
	movq	%rax, %rcx
	subq	%r12, %rcx
	movq	%rdx, %rsi
	shlq	$4, %rsi
	leaq	(,%rax,8), %rbp
	subq	%rbp, %rsi
	addq	%rbx, %rsi
	leaq	(,%rdx,8), %rdi
	subq	%rbp, %rdi
	addq	%rbx, %rdi
	incq	%rax
	leaq	(%rbx,%rdx,8), %rdx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_52:                              # %.lr.ph277
                                        #   Parent Loop BB12_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	subsd	-8(%rdx,%rax,8), %xmm0
	movsd	%xmm0, -8(%rbx,%rax,8)
	movsd	(%rsi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	addsd	-8(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rdi,%rbp,8)
	movsd	-8(%rsi,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	subsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	movsd	-8(%rsi,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	addsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, -8(%rdi,%rbp,8)
	addq	$-2, %rbp
	addq	$2, %rax
	cmpq	%rbp, %rcx
	jne	.LBB12_52
.LBB12_53:                              # %._crit_edge278
                                        #   in Loop: Header=BB12_27 Depth=1
	addl	%r13d, %r13d
	leal	(%r11,%r14), %eax
	cmpl	$3, %r14d
	movq	(%rbx,%rax,8), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	ja	.LBB12_27
# BB#54:                                # %._crit_edge284.loopexit
	movslq	%r13d, %rax
	jmp	.LBB12_55
.LBB12_25:                              # %.preheader.._crit_edge284_crit_edge
	movl	$2, %eax
.LBB12_55:                              # %._crit_edge284
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	(%rbx), %rcx
	movq	%rcx, (%r15,%rax,8)
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%rbx), %xmm0
	movsd	%xmm0, (%r15,%rdx,8)
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	addsd	8(%rbx), %xmm0
.LBB12_57:
	movsd	%xmm0, (%r15)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	dfct, .Lfunc_end12-dfct
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4607182418800017408     # double 1
.LCPI13_1:
	.quad	4605249457297304856     # double 0.78539816339744828
.LCPI13_2:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_3:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	dfst
	.p2align	4, 0x90
	.type	dfst,@function
dfst:                                   # @dfst
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi125:
	.cfi_def_cfa_offset 160
.Lcfi126:
	.cfi_offset %rbx, -56
.Lcfi127:
	.cfi_offset %r12, -48
.Lcfi128:
	.cfi_offset %r13, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movq	%rcx, %r10
	movq	%rdx, %r14
	movl	(%r10), %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leal	(,%rax,8), %eax
	cmpl	%edi, %eax
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	jge	.LBB13_2
# BB#1:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	sarl	$3, %edi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r10, %rsi
	movq	%r8, %rdx
	movq	%r10, %rbp
	callq	makewt
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%rbp, %r10
	movq	40(%rsp), %r8           # 8-byte Reload
.LBB13_2:
	movl	4(%r10), %r9d
	leal	(%r9,%r9), %eax
	cmpl	%edi, %eax
	jge	.LBB13_9
# BB#3:
	movl	%edi, %r9d
	sarl	%r9d
	movl	%r9d, 4(%r10)
	cmpl	$2, %r9d
	jl	.LBB13_9
# BB#4:
	movq	%r10, 48(%rsp)          # 8-byte Spill
	movslq	24(%rsp), %rbp          # 4-byte Folded Reload
	leaq	(%r8,%rbp,8), %r15
	movl	%r9d, %r12d
	shrl	%r12d
	movsd	.LCPI13_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movq	%r8, %r13
	movq	%r9, %rbx
	callq	atan
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	movsd	.LCPI13_1(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	mulsd	%xmm1, %xmm0
	callq	cos
	movq	%rbx, %r9
	movsd	%xmm0, (%r15)
	mulsd	.LCPI13_2(%rip), %xmm0
	movsd	%xmm0, (%r15,%r12,8)
	cmpl	$4, %r9d
	jb	.LBB13_5
# BB#6:                                 # %.lr.ph.preheader.i
	movslq	%r9d, %rax
	addq	%rax, %rbp
	leaq	-8(%r13,%rbp,8), %r13
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB13_7:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	64(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	callq	cos
	movsd	.LCPI13_2(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r15,%rbp,8)
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	.LCPI13_2(%rip), %xmm0
	movsd	%xmm0, (%r13)
	incq	%rbp
	addq	$-8, %r13
	cmpq	%r12, %rbp
	jl	.LBB13_7
# BB#8:
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %r10          # 8-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %r9
	cmpl	$3, %edi
	jge	.LBB13_10
	jmp	.LBB13_49
.LBB13_5:
	movq	%r13, %r8
	movq	48(%rsp), %r10          # 8-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB13_9:                               # %makect.exit
	cmpl	$3, %edi
	jl	.LBB13_49
.LBB13_10:
	movl	%edi, %r11d
	shrl	%r11d
	movl	%edi, %r12d
	shrl	$2, %r12d
	cmpl	$8, %edi
	jb	.LBB13_13
# BB#11:                                # %.lr.ph263.preheader
	movl	%r12d, %eax
	movslq	%edi, %rbx
	movl	%r11d, %ecx
	leaq	(,%rbx,8), %rdx
	leaq	(,%rcx,8), %rbp
	subq	%rbp, %rdx
	addq	%rsi, %rdx
	decq	%rcx
	leaq	-8(,%rbx,8), %rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB13_12:                              # %.lr.ph263
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rsi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rsi,%rbx), %xmm1      # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	subsd	%xmm1, %xmm0
	movsd	(%rsi,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movsd	(%rdx,%rbp,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm4
	addsd	%xmm3, %xmm4
	subsd	%xmm3, %xmm1
	movsd	%xmm2, (%rsi,%rbp,8)
	movsd	%xmm4, (%rsi,%rcx,8)
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%r14,%rbp,8)
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r14,%rcx,8)
	incq	%rbp
	decq	%rcx
	addq	$-8, %rbx
	cmpq	%rax, %rbp
	jl	.LBB13_12
.LBB13_13:                              # %._crit_edge264
	movl	%edi, %eax
	subl	%r12d, %eax
	cmpl	$7, %edi
	movl	%r12d, %r13d
	movsd	(%rsi,%r13,8), %xmm0    # xmm0 = mem[0],zero
	cltq
	subsd	(%rsi,%rax,8), %xmm0
	movsd	%xmm0, (%r14)
	movsd	(%rsi,%rax,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rsi,%r13,8), %xmm0
	movsd	%xmm0, (%rsi,%r13,8)
	movl	%r11d, %ebp
	movq	(%rsi,%rbp,8), %rax
	movq	%rax, (%rsi)
	movslq	24(%rsp), %rbx          # 4-byte Folded Reload
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	%r11, 32(%rsp)          # 8-byte Spill
	jbe	.LBB13_14
# BB#17:                                # %.lr.ph.preheader.i221
	movl	%r9d, %eax
	cltd
	idivl	%r11d
	movslq	%eax, %rcx
	movslq	%r9d, %r15
	addq	%rbx, %r15
	movq	%r15, %rax
	subq	%rcx, %rax
	leaq	(%r8,%rax,8), %rax
	leaq	(,%rcx,8), %r9
	movq	%r9, %r11
	negq	%r11
	addq	%rbx, %rcx
	leaq	(%r8,%rcx,8), %rdx
	leaq	-1(%rbp), %rbx
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB13_18:                              # %.lr.ph.i224
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	movsd	(%rsi,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	movsd	(%rsi,%rcx,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm2, %xmm5
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rsi,%rbx,8)
	movsd	%xmm3, (%rsi,%rcx,8)
	incq	%rcx
	addq	%r11, %rax
	addq	%r9, %rdx
	decq	%rbx
	cmpq	%rcx, %r13
	jne	.LBB13_18
# BB#19:                                # %dstsub.exit
	movq	56(%rsp), %rax          # 8-byte Reload
	movsd	(%r8,%rax,8), %xmm0     # xmm0 = mem[0],zero
	mulsd	(%rsi,%r13,8), %xmm0
	movsd	%xmm0, (%rsi,%r13,8)
	cmpl	$10, %edi
	movq	32(%rsp), %r11          # 8-byte Reload
	jb	.LBB13_15
# BB#20:
	leaq	8(%r10), %rdx
	movl	%r11d, %edi
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%r12, %rbx
	movq	%r10, %r12
	callq	cftfsub
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%r12, %r10
	movq	%rbx, %r12
	movq	40(%rsp), %r8           # 8-byte Reload
	cmpl	$12, %edi
	jb	.LBB13_23
# BB#21:                                # %.lr.ph.preheader.i225
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rax), %eax
	cltd
	idivl	%r12d
	movslq	%eax, %r11
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r11), %rcx
	leaq	(%r8,%rcx,8), %rcx
	subq	%r11, %r15
	shlq	$3, %r11
	leaq	(%r8,%r15,8), %rdx
	movq	%r11, %r9
	negq	%r9
	leaq	-2(%rbp), %rbx
	movl	$2, %eax
	movsd	.LCPI13_2(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB13_22:                              # %.lr.ph.i228
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm2
	subsd	(%rdx), %xmm2
	movupd	(%rsi,%rax,8), %xmm3
	movsd	(%rsi,%rbx,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm3, %xmm1
	subsd	%xmm4, %xmm1
	movapd	%xmm3, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	8(%rsi,%rbx,8), %xmm4
	movsd	(%rcx), %xmm5           # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm6, %xmm4
	unpcklpd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	subpd	%xmm4, %xmm2
	addpd	%xmm4, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	subpd	%xmm1, %xmm3
	movupd	%xmm3, (%rsi,%rax,8)
	movupd	(%rsi,%rbx,8), %xmm2
	movapd	%xmm2, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%rsi,%rbx,8)
	addq	$2, %rax
	addq	%r11, %rcx
	addq	%r9, %rdx
	addq	$-2, %rbx
	cmpq	%r13, %rax
	jl	.LBB13_22
	jmp	.LBB13_23
.LBB13_14:                              # %dstsub.exit.thread
	movsd	(%r8,%rbx,8), %xmm0     # xmm0 = mem[0],zero
	mulsd	(%rsi,%r13,8), %xmm0
	movsd	%xmm0, (%rsi,%r13,8)
.LBB13_15:
	cmpl	$4, %r11d
	jne	.LBB13_23
# BB#16:
	leaq	8(%r10), %rdx
	movl	$4, %edi
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%r10, %r15
	callq	cftfsub
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%r15, %r10
	movq	40(%rsp), %r8           # 8-byte Reload
.LBB13_23:                              # %rftfsub.exit
	movsd	8(%rsi), %xmm0          # xmm0 = mem[0],zero
	subsd	(%rsi), %xmm0
	movslq	%edi, %rax
	movsd	%xmm0, -8(%rsi,%rax,8)
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	addsd	8(%rsi), %xmm0
	movsd	%xmm0, 8(%rsi)
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	-2(%rcx), %eax
	cmpl	$2, %eax
	jl	.LBB13_26
# BB#24:                                # %.lr.ph260.preheader
	addq	$-2, %rbp
	leal	-5(%rcx,%rcx), %eax
	cltq
	leaq	(%rsi,%rax,8), %rax
	leal	-3(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	leaq	(%rsi,%rcx,8), %rcx
	movapd	.LCPI13_3(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB13_25:                              # %.lr.ph260
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rsi,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	subsd	8(%rsi,%rbp,8), %xmm1
	movsd	%xmm1, (%rcx)
	movsd	(%rsi,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	subsd	8(%rsi,%rbp,8), %xmm1
	movsd	%xmm1, (%rax)
	addq	$-2, %rbp
	addq	$-32, %rax
	addq	$-32, %rcx
	cmpq	$1, %rbp
	jg	.LBB13_25
.LBB13_26:                              # %.preheader
	cmpl	$7, %edi
	movq	72(%rsp), %r11          # 8-byte Reload
	jbe	.LBB13_27
# BB#28:                                # %.lr.ph255
	movslq	%r11d, %rax
	addq	$8, %r10
	leal	(%r11,%r11), %ecx
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	%rcx, %rax
	leaq	(%r8,%rax,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	(%r8,%rcx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	$2, %r13d
	movapd	.LCPI13_3(%rip), %xmm7  # xmm7 = [-0.000000e+00,-0.000000e+00]
	movq	%r10, 48(%rsp)          # 8-byte Spill
	jmp	.LBB13_29
	.p2align	4, 0x90
.LBB13_44:                              # %.lr.ph249.preheader
                                        #   in Loop: Header=BB13_29 Depth=1
	movl	%r15d, %eax
	movq	64(%rsp), %r9           # 8-byte Reload
	movl	%r9d, %ecx
	leaq	(%r14,%rax,8), %rdx
	shlq	$4, %rax
	addq	%r14, %rax
	movq	$-1, %rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB13_45:                              # %.lr.ph249
                                        #   Parent Loop BB13_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rdx,%rbp,8), %xmm0
	movsd	%xmm0, (%r14,%rbp,8)
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	subsd	(%rdx,%rbp,8), %xmm0
	movsd	%xmm0, (%rdx,%rbx,8)
	incq	%rbp
	decq	%rbx
	cmpq	%rcx, %rbp
	jl	.LBB13_45
# BB#46:                                # %._crit_edge250
                                        #   in Loop: Header=BB13_29 Depth=1
	leal	(%r9,%r15), %eax
	cmpl	$3, %r15d
	movq	(%r14,%rax,8), %rax
	movq	%rax, (%r14)
	movl	%r9d, %r12d
	ja	.LBB13_29
	jmp	.LBB13_47
	.p2align	4, 0x90
.LBB13_37:                              #   in Loop: Header=BB13_29 Depth=1
	cmpl	$4, %ebx
	jne	.LBB13_39
# BB#38:                                #   in Loop: Header=BB13_29 Depth=1
	movl	$4, %edi
	movq	%r14, %rsi
	movq	%r10, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	cftfsub
	movapd	.LCPI13_3(%rip), %xmm7  # xmm7 = [-0.000000e+00,-0.000000e+00]
	movq	72(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	jmp	.LBB13_39
	.p2align	4, 0x90
.LBB13_29:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_32 Depth 2
                                        #     Child Loop BB13_36 Depth 2
                                        #     Child Loop BB13_41 Depth 2
                                        #     Child Loop BB13_45 Depth 2
	movl	%r12d, %ecx
	shrl	%ecx
	cmpl	$3, %r12d
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	jbe	.LBB13_30
# BB#31:                                # %.lr.ph.preheader.i230
                                        #   in Loop: Header=BB13_29 Depth=1
	movl	%r11d, %eax
	cltd
	idivl	%r12d
	movl	%r12d, %r15d
	cltq
	movl	%ecx, %r12d
	leaq	(,%rax,8), %r9
	movq	96(%rsp), %rcx          # 8-byte Reload
	subq	%r9, %rcx
	movq	%r9, %rdx
	negq	%rdx
	movq	88(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rax,8), %rax
	leaq	-1(%r15), %rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB13_32:                              # %.lr.ph.i236
                                        #   Parent Loop BB13_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	movsd	(%r14,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm3
	mulsd	%xmm1, %xmm3
	movsd	(%r14,%rbp,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm2, %xmm5
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%r14,%rbx,8)
	movsd	%xmm3, (%r14,%rbp,8)
	incq	%rbp
	addq	%rdx, %rcx
	addq	%r9, %rax
	decq	%rbx
	cmpq	%rbp, %r12
	jne	.LBB13_32
# BB#33:                                # %dstsub.exit237
                                        #   in Loop: Header=BB13_29 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movsd	(%r8,%rax,8), %xmm0     # xmm0 = mem[0],zero
	mulsd	(%r14,%r12,8), %xmm0
	movsd	%xmm0, (%r14,%r12,8)
	movq	32(%rsp), %rbx          # 8-byte Reload
	cmpl	$5, %ebx
	jb	.LBB13_37
# BB#34:                                #   in Loop: Header=BB13_29 Depth=1
	movl	%ebx, %edi
	movq	%r14, %rsi
	movq	%r10, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	cftfsub
	movapd	.LCPI13_3(%rip), %xmm7  # xmm7 = [-0.000000e+00,-0.000000e+00]
	movsd	.LCPI13_2(%rip), %xmm6  # xmm6 = mem[0],zero
	movq	72(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	cmpl	$5, %ebx
	je	.LBB13_39
# BB#35:                                # %.lr.ph.preheader.i238
                                        #   in Loop: Header=BB13_29 Depth=1
	movl	84(%rsp), %eax          # 4-byte Reload
	cltd
	idivl	64(%rsp)                # 4-byte Folded Reload
	cltq
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	shlq	$3, %rax
	movq	96(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movq	%rax, %rbx
	negq	%rbx
	addq	$-2, %r15
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB13_36:                              # %.lr.ph.i243
                                        #   Parent Loop BB13_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm6, %xmm1
	subsd	(%rdx), %xmm1
	movupd	(%r14,%rbp,8), %xmm2
	movsd	(%r14,%r15,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm2, %xmm0
	subsd	%xmm3, %xmm0
	movapd	%xmm2, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	addsd	8(%r14,%r15,8), %xmm3
	movsd	(%rcx), %xmm4           # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm5
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm5, %xmm3
	unpcklpd	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0]
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	subpd	%xmm3, %xmm1
	addpd	%xmm3, %xmm0
	movsd	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1]
	subpd	%xmm0, %xmm2
	movupd	%xmm2, (%r14,%rbp,8)
	movupd	(%r14,%r15,8), %xmm1
	movapd	%xmm1, %xmm2
	addpd	%xmm0, %xmm2
	subpd	%xmm0, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	movupd	%xmm1, (%r14,%r15,8)
	addq	$2, %rbp
	addq	%rax, %rcx
	addq	%rbx, %rdx
	addq	$-2, %r15
	cmpq	%r12, %rbp
	jl	.LBB13_36
	jmp	.LBB13_39
	.p2align	4, 0x90
.LBB13_30:                              # %.thread
                                        #   in Loop: Header=BB13_29 Depth=1
	movl	%ecx, %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movsd	(%r8,%rcx,8), %xmm0     # xmm0 = mem[0],zero
	mulsd	(%r14,%rax,8), %xmm0
	movsd	%xmm0, (%r14,%rax,8)
.LBB13_39:                              # %rftfsub.exit244
                                        #   in Loop: Header=BB13_29 Depth=1
	movsd	8(%r14), %xmm0          # xmm0 = mem[0],zero
	subsd	(%r14), %xmm0
	movl	%edi, %eax
	subl	%r13d, %eax
	cltq
	movsd	%xmm0, (%rsi,%rax,8)
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	addsd	8(%r14), %xmm0
	movslq	%r13d, %rbp
	movsd	%xmm0, (%rsi,%rbp,8)
	movq	32(%rsp), %r15          # 8-byte Reload
	cmpl	$3, %r15d
	jb	.LBB13_42
# BB#40:                                # %.lr.ph
                                        #   in Loop: Header=BB13_29 Depth=1
	leal	(,%rbp,4), %ecx
	movl	%r15d, %r9d
	movslq	%ecx, %rax
	leaq	(%rbp,%rax), %rdx
	shlq	$3, %rax
	shlq	$3, %rbp
	movq	%rax, %rcx
	subq	%rbp, %rcx
	movq	%rsi, %rbx
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB13_41:                              #   Parent Loop BB13_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r14,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	xorpd	%xmm7, %xmm0
	subsd	8(%r14,%rbp,8), %xmm0
	movsd	%xmm0, (%rcx,%rbx)
	movsd	(%r14,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	subsd	8(%r14,%rbp,8), %xmm0
	movsd	%xmm0, (%rbx,%rdx,8)
	addq	$2, %rbp
	addq	%rax, %rbx
	cmpq	%r9, %rbp
	jl	.LBB13_41
.LBB13_42:                              # %._crit_edge
                                        #   in Loop: Header=BB13_29 Depth=1
	addl	%r13d, %r13d
	cmpl	$3, %r15d
	ja	.LBB13_44
# BB#43:                                # %._crit_edge250.thread
	movq	64(%rsp), %rax          # 8-byte Reload
	addl	%r15d, %eax
	movq	(%r14,%rax,8), %rax
	movq	%rax, (%r14)
.LBB13_47:                              # %._crit_edge256.loopexit
	movslq	%r13d, %rcx
	jmp	.LBB13_48
.LBB13_27:                              # %.preheader.._crit_edge256_crit_edge
	movq	(%r14), %rax
	movl	$2, %ecx
.LBB13_48:                              # %._crit_edge256
	movq	%rax, (%rsi,%rcx,8)
.LBB13_49:
	movq	$0, (%rsi)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	dfst, .Lfunc_end13-dfst
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI14_0:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_1:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	cftf1st
	.p2align	4, 0x90
	.type	cftf1st,@function
cftf1st:                                # @cftf1st
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi138:
	.cfi_def_cfa_offset 160
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	sarl	$3, %edi
	leal	(%rdi,%rdi), %ebx
	leal	(,%rdi,4), %ecx
	leal	(%rbx,%rbx,2), %eax
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	movslq	%ecx, %rcx
	movupd	(%rsi), %xmm0
	movupd	(%rsi,%rcx,8), %xmm1
	movapd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	subpd	%xmm1, %xmm0
	movslq	%ebx, %rbp
	cltq
	movupd	(%rsi,%rbp,8), %xmm1
	movupd	(%rsi,%rax,8), %xmm3
	movapd	%xmm1, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movapd	%xmm2, %xmm3
	addpd	%xmm4, %xmm3
	movupd	%xmm3, (%rsi)
	subpd	%xmm4, %xmm2
	movupd	%xmm2, (%rsi,%rbp,8)
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1]
	movupd	%xmm0, (%rsi,%rcx,8)
	movsd	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1]
	movupd	%xmm2, (%rsi,%rax,8)
	movsd	8(%rdx), %xmm8          # xmm8 = mem[0],zero
	movsd	16(%rdx), %xmm3         # xmm3 = mem[0],zero
	movsd	24(%rdx), %xmm0         # xmm0 = mem[0],zero
	movapd	%xmm0, -32(%rsp)        # 16-byte Spill
	leal	-2(%rdi), %eax
	movslq	%eax, %r14
	cmpl	$3, %r14d
	jl	.LBB14_1
# BB#2:                                 # %.lr.ph.preheader
	movapd	%xmm8, (%rsp)           # 16-byte Spill
	movq	%rbp, %rax
	shlq	$5, %rax
	leaq	-16(%rsi,%rax), %r10
	leaq	(%rbp,%rbp,2), %rax
	leaq	(%rsi,%rax,8), %r13
	movq	%rbp, %rax
	shlq	$4, %rax
	leaq	32(%rsi,%rax), %r11
	leaq	32(%rsi,%rbp,8), %r12
	movq	%rdi, -80(%rsp)         # 8-byte Spill
	leal	-1(,%rdi,8), %eax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	leal	(%rbx,%rbx,2), %eax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	leaq	-1(%rax), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	-96(%rsp), %rax         # 8-byte Reload
	leaq	-1(%rax), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movl	%ebp, %r9d
	leaq	-1(%r9), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movsd	.LCPI14_0(%rip), %xmm2  # xmm2 = mem[0],zero
	xorpd	%xmm14, %xmm14
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	xorpd	%xmm11, %xmm11
	movapd	%xmm2, %xmm10
	movsd	%xmm3, -88(%rsp)        # 8-byte Spill
	.p2align	4, 0x90
.LBB14_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm10, -112(%rsp)      # 8-byte Spill
	movsd	%xmm11, -104(%rsp)      # 8-byte Spill
	movsd	%xmm2, -120(%rsp)       # 8-byte Spill
	movsd	32(%rdx,%rdi,8), %xmm9  # xmm9 = mem[0],zero
	movsd	-16(%r11,%rdi,8), %xmm6 # xmm6 = mem[0],zero
	movupd	16(%rsi,%rdi,8), %xmm5
	movq	-96(%rsp), %rax         # 8-byte Reload
	leal	3(%rax,%rdi), %eax
	movslq	%eax, %r15
	movupd	32(%rsi,%rdi,8), %xmm1
	movupd	(%r11,%rdi,8), %xmm2
	movsd	(%rsi,%r15,8), %xmm12   # xmm12 = mem[0],zero
	movapd	%xmm1, %xmm0
	movsd	-16(%r12,%rdi,8), %xmm13 # xmm13 = mem[0],zero
	movsd	16(%r13,%rdi,8), %xmm4  # xmm4 = mem[0],zero
	leal	3(%r9,%rdi), %eax
	addpd	%xmm2, %xmm0
	movslq	%eax, %rcx
	movsd	(%rsi,%rcx,8), %xmm7    # xmm7 = mem[0],zero
	movq	-48(%rsp), %rax         # 8-byte Reload
	leal	3(%rax,%rdi), %eax
	subpd	%xmm2, %xmm1
	movq	%r14, %rbx
	movslq	%eax, %r14
	movsd	(%rsi,%r14,8), %xmm2    # xmm2 = mem[0],zero
	movapd	%xmm13, %xmm3
	subsd	%xmm4, %xmm13
	unpcklpd	%xmm7, %xmm3    # xmm3 = xmm3[0],xmm7[0]
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	subsd	%xmm2, %xmm7
	movupd	(%r12,%rdi,8), %xmm10
	addpd	%xmm3, %xmm4
	movupd	32(%r13,%rdi,8), %xmm2
	movapd	%xmm10, %xmm3
	addpd	%xmm2, %xmm3
	subpd	%xmm2, %xmm10
	movapd	%xmm5, %xmm11
	subsd	%xmm6, %xmm11
	unpcklpd	%xmm12, %xmm6   # xmm6 = xmm6[0],xmm12[0]
	addpd	%xmm5, %xmm6
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	subsd	%xmm12, %xmm5
	movapd	%xmm6, %xmm2
	addpd	%xmm4, %xmm2
	movsd	40(%rdx,%rdi,8), %xmm8  # xmm8 = mem[0],zero
	movsd	48(%rdx,%rdi,8), %xmm12 # xmm12 = mem[0],zero
	movsd	56(%rdx,%rdi,8), %xmm15 # xmm15 = mem[0],zero
	movaps	%xmm15, -16(%rsp)       # 16-byte Spill
	movupd	%xmm2, 16(%rsi,%rdi,8)
	movapd	%xmm0, %xmm2
	addpd	%xmm3, %xmm2
	movupd	%xmm2, 32(%rsi,%rdi,8)
	movapd	%xmm6, %xmm2
	subsd	%xmm4, %xmm2
	movhlps	%xmm6, %xmm6            # xmm6 = xmm6[1,1]
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	subsd	%xmm4, %xmm6
	movsd	%xmm2, -16(%r12,%rdi,8)
	movsd	%xmm6, (%rsi,%rcx,8)
	movapd	%xmm9, 64(%rsp)         # 16-byte Spill
	movsd	-112(%rsp), %xmm15      # 8-byte Reload
                                        # xmm15 = mem[0],zero
	addsd	%xmm9, %xmm15
	movsd	-104(%rsp), %xmm6       # 8-byte Reload
                                        # xmm6 = mem[0],zero
	addsd	%xmm8, %xmm6
	movapd	%xmm8, 80(%rsp)         # 16-byte Spill
	movsd	-88(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm15
	mulsd	%xmm2, %xmm6
	subpd	%xmm3, %xmm0
	movapd	%xmm11, %xmm2
	subsd	%xmm7, %xmm2
	movupd	%xmm0, (%r12,%rdi,8)
	movapd	%xmm5, %xmm0
	addsd	%xmm13, %xmm0
	movapd	%xmm15, %xmm3
	movsd	%xmm15, -112(%rsp)      # 8-byte Spill
	mulsd	%xmm2, %xmm3
	movapd	%xmm6, %xmm4
	movsd	%xmm6, -104(%rsp)       # 8-byte Spill
	mulsd	%xmm0, %xmm4
	subsd	%xmm4, %xmm3
	mulsd	%xmm15, %xmm0
	mulsd	%xmm6, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm3, -16(%r11,%rdi,8)
	movapd	%xmm10, %xmm4
	shufpd	$1, %xmm4, %xmm4        # xmm4 = xmm4[1,0]
	movapd	%xmm1, %xmm0
	subpd	%xmm4, %xmm0
	addpd	%xmm1, %xmm4
	movapd	%xmm4, %xmm3
	movsd	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1]
	shufpd	$1, %xmm0, %xmm4        # xmm4 = xmm4[1],xmm0[0]
	movlhps	%xmm9, %xmm9            # xmm9 = xmm9[0,0]
	movaps	%xmm9, 32(%rsp)         # 16-byte Spill
	movlhps	%xmm8, %xmm8            # xmm8 = xmm8[0,0]
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	mulpd	%xmm9, %xmm3
	mulpd	%xmm8, %xmm4
	movapd	%xmm3, %xmm0
	subpd	%xmm4, %xmm0
	addpd	%xmm3, %xmm4
	movsd	%xmm2, (%rsi,%r15,8)
	movsd	%xmm0, %xmm4            # xmm4 = xmm0[0],xmm4[1]
	addsd	%xmm11, %xmm7
	movsd	-120(%rsp), %xmm15      # 8-byte Reload
                                        # xmm15 = mem[0],zero
	addsd	%xmm12, %xmm15
	movapd	-32(%rsp), %xmm0        # 16-byte Reload
	mulsd	%xmm0, %xmm15
	movapd	-16(%rsp), %xmm6        # 16-byte Reload
	subsd	%xmm6, %xmm14
	mulsd	%xmm0, %xmm14
	subsd	%xmm13, %xmm5
	movapd	%xmm15, %xmm2
	movapd	%xmm15, %xmm0
	movsd	%xmm0, -120(%rsp)       # 8-byte Spill
	mulsd	%xmm7, %xmm2
	movapd	%xmm14, %xmm3
	mulsd	%xmm5, %xmm3
	addsd	%xmm2, %xmm3
	mulsd	%xmm14, %xmm7
	movapd	%xmm14, %xmm15
	mulsd	%xmm0, %xmm5
	subsd	%xmm7, %xmm5
	movapd	%xmm10, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	addsd	%xmm1, %xmm2
	movupd	%xmm4, (%r11,%rdi,8)
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm10, %xmm1
	movapd	%xmm12, 48(%rsp)        # 16-byte Spill
	movapd	%xmm12, %xmm4
	mulsd	%xmm2, %xmm4
	movapd	%xmm12, %xmm7
	movsd	%xmm3, 16(%r13,%rdi,8)
	mulsd	%xmm1, %xmm7
	movapd	%xmm6, %xmm0
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm4
	movsd	%xmm5, (%rsi,%r14,8)
	movq	%rbx, %r14
	movsd	%xmm4, 32(%r13,%rdi,8)
	mulsd	%xmm0, %xmm2
	addsd	%xmm7, %xmm2
	movsd	%xmm2, 40(%r13,%rdi,8)
	movsd	-48(%r12,%r8,8), %xmm13 # xmm13 = mem[0],zero
	movq	-72(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r8), %eax
	movslq	%eax, %rcx
	movsd	-16(%r13,%r8,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm13, %xmm12
	movsd	(%rsi,%rcx,8), %xmm5    # xmm5 = mem[0],zero
	movq	-56(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r8), %eax
	movslq	%eax, %rbx
	addsd	%xmm1, %xmm12
	movsd	(%rsi,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	movapd	%xmm5, %xmm6
	addsd	%xmm2, %xmm6
	subsd	%xmm1, %xmm13
	subsd	%xmm2, %xmm5
	movupd	-64(%r12,%r8,8), %xmm1
	movupd	-32(%r13,%r8,8), %xmm2
	movapd	%xmm1, %xmm14
	addpd	%xmm2, %xmm14
	subpd	%xmm2, %xmm1
	movq	-64(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r8), %eax
	movsd	-48(%r11,%r8,8), %xmm10 # xmm10 = mem[0],zero
	movsd	(%r10,%r8,8), %xmm9     # xmm9 = mem[0],zero
	movapd	%xmm10, %xmm3
	addsd	%xmm9, %xmm3
	cltq
	movq	-40(%rsp), %rbp         # 8-byte Reload
	leal	(%rbp,%r8), %ebp
	movslq	%ebp, %rbp
	movapd	%xmm12, %xmm8
	addsd	%xmm3, %xmm8
	movsd	(%rsi,%rax,8), %xmm11   # xmm11 = mem[0],zero
	movupd	-64(%r11,%r8,8), %xmm7
	movupd	-16(%r10,%r8,8), %xmm2
	movsd	(%rsi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm8, -48(%r12,%r8,8)
	movapd	%xmm11, %xmm8
	addsd	%xmm0, %xmm8
	movapd	%xmm6, %xmm4
	addsd	%xmm8, %xmm4
	movsd	%xmm4, (%rsi,%rcx,8)
	subsd	%xmm9, %xmm10
	subsd	%xmm0, %xmm11
	movapd	%xmm7, %xmm0
	addpd	%xmm2, %xmm0
	movapd	%xmm14, %xmm4
	addpd	%xmm0, %xmm4
	movupd	%xmm4, -64(%r12,%r8,8)
	subsd	%xmm3, %xmm12
	subsd	%xmm8, %xmm6
	movsd	%xmm12, -48(%r11,%r8,8)
	movsd	%xmm6, (%rsi,%rax,8)
	subpd	%xmm2, %xmm7
	subpd	%xmm0, %xmm14
	movupd	%xmm14, -64(%r11,%r8,8)
	movapd	%xmm13, %xmm0
	subsd	%xmm11, %xmm0
	movapd	%xmm5, %xmm2
	addsd	%xmm10, %xmm2
	movsd	-112(%rsp), %xmm6       # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movapd	%xmm6, %xmm3
	mulsd	%xmm2, %xmm3
	movsd	-104(%rsp), %xmm4       # 8-byte Reload
                                        # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm2
	mulsd	%xmm0, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm6, %xmm0
	movsd	%xmm4, -16(%r13,%r8,8)
	addsd	%xmm2, %xmm0
	movapd	%xmm7, %xmm2
	shufpd	$1, %xmm2, %xmm2        # xmm2 = xmm2[1,0]
	movapd	%xmm1, %xmm3
	subpd	%xmm2, %xmm3
	addpd	%xmm1, %xmm2
	movapd	%xmm2, %xmm4
	movsd	%xmm3, %xmm4            # xmm4 = xmm3[0],xmm4[1]
	mulpd	16(%rsp), %xmm4         # 16-byte Folded Reload
	shufpd	$1, %xmm3, %xmm2        # xmm2 = xmm2[1],xmm3[0]
	movsd	%xmm0, (%rsi,%rbx,8)
	mulpd	32(%rsp), %xmm2         # 16-byte Folded Reload
	movapd	%xmm4, %xmm0
	subpd	%xmm2, %xmm0
	addpd	%xmm4, %xmm2
	movsd	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1]
	movupd	%xmm2, -32(%r13,%r8,8)
	addsd	%xmm13, %xmm11
	movsd	-88(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	subsd	%xmm10, %xmm5
	movdqa	64(%rsp), %xmm10        # 16-byte Reload
	movsd	-120(%rsp), %xmm2       # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm0
	mulsd	%xmm5, %xmm0
	mulsd	%xmm15, %xmm5
	mulsd	%xmm11, %xmm15
	addsd	%xmm15, %xmm0
	movsd	%xmm0, (%r10,%r8,8)
	mulsd	%xmm2, %xmm11
	subsd	%xmm11, %xmm5
	movapd	80(%rsp), %xmm11        # 16-byte Reload
	movapd	%xmm7, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movsd	%xmm5, (%rsi,%rbp,8)
	movapd	-16(%rsp), %xmm5        # 16-byte Reload
	movapd	%xmm5, %xmm14
	addsd	%xmm1, %xmm0
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm7, %xmm1
	movapd	48(%rsp), %xmm4         # 16-byte Reload
	movapd	%xmm4, %xmm2
	mulsd	%xmm0, %xmm5
	mulsd	%xmm1, %xmm2
	subsd	%xmm5, %xmm2
	movsd	%xmm2, -16(%r10,%r8,8)
	movapd	%xmm4, %xmm2
	xorpd	.LCPI14_1(%rip), %xmm14
	mulsd	%xmm14, %xmm1
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -8(%r10,%r8,8)
	leaq	4(%rdi), %rax
	addq	$-4, %r8
	addq	$6, %rdi
	cmpq	%r14, %rdi
	movq	%rax, %rdi
	jl	.LBB14_3
# BB#4:                                 # %._crit_edge.loopexit
	unpcklpd	%xmm2, %xmm14   # xmm14 = xmm14[0],xmm2[0]
	movapd	(%rsp), %xmm8           # 16-byte Reload
	movq	-80(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB14_5
.LBB14_1:
	movq	.LCPI14_0(%rip), %xmm10 # xmm10 = mem[0],zero
	movdqa	%xmm10, %xmm14
	pslldq	$8, %xmm14              # xmm14 = zero,zero,zero,zero,zero,zero,zero,zero,xmm14[0,1,2,3,4,5,6,7]
	xorpd	%xmm11, %xmm11
.LBB14_5:                               # %._crit_edge
	addsd	%xmm8, %xmm10
	mulsd	%xmm3, %xmm10
	addsd	%xmm8, %xmm11
	mulsd	%xmm3, %xmm11
	movapd	%xmm8, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	subpd	%xmm1, %xmm14
	movaps	-32(%rsp), %xmm9        # 16-byte Reload
	movlhps	%xmm9, %xmm9            # xmm9 = xmm9[0,0]
	mulpd	%xmm14, %xmm9
	leal	(%rdi,%rdi,2), %eax
	leal	(%rdi,%rdi,4), %ecx
	leal	(,%rdi,8), %ebp
	subl	%edi, %ebp
	movupd	(%rsi,%r14,8), %xmm2
	movslq	%ecx, %rcx
	movupd	-16(%rsi,%rcx,8), %xmm0
	movapd	%xmm2, %xmm3
	addpd	%xmm0, %xmm3
	subpd	%xmm0, %xmm2
	movslq	%eax, %rdx
	movupd	-16(%rsi,%rdx,8), %xmm0
	movslq	%ebp, %rax
	movupd	-16(%rsi,%rax,8), %xmm4
	movapd	%xmm0, %xmm5
	addpd	%xmm4, %xmm5
	subpd	%xmm4, %xmm0
	movapd	%xmm0, %xmm4
	shufpd	$1, %xmm4, %xmm4        # xmm4 = xmm4[1,0]
	movapd	%xmm3, %xmm6
	addpd	%xmm5, %xmm6
	movupd	%xmm6, (%rsi,%r14,8)
	subpd	%xmm5, %xmm3
	movupd	%xmm3, -16(%rsi,%rdx,8)
	movapd	%xmm2, %xmm3
	subpd	%xmm4, %xmm3
	addpd	%xmm2, %xmm4
	movapd	%xmm4, %xmm5
	movsd	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1]
	movlhps	%xmm10, %xmm10          # xmm10 = xmm10[0,0]
	mulpd	%xmm10, %xmm5
	movlhps	%xmm11, %xmm11          # xmm11 = xmm11[0,0]
	shufpd	$1, %xmm3, %xmm4        # xmm4 = xmm4[1],xmm3[0]
	mulpd	%xmm11, %xmm4
	movapd	%xmm5, %xmm3
	subpd	%xmm4, %xmm3
	addpd	%xmm5, %xmm4
	movsd	%xmm3, %xmm4            # xmm4 = xmm3[0],xmm4[1]
	movupd	%xmm4, -16(%rsi,%rcx,8)
	movapd	%xmm0, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	addsd	%xmm2, %xmm3
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	subsd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm9, %xmm2
	movapd	%xmm9, %xmm0
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm0, %xmm3
	movapd	%xmm2, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm2
	movsd	%xmm4, %xmm2            # xmm2 = xmm4[0],xmm2[1]
	movupd	%xmm2, -16(%rsi,%rax,8)
	movslq	%edi, %rdi
	movupd	(%rsi,%rdi,8), %xmm2
	movupd	(%rsi,%rcx,8), %xmm3
	movapd	%xmm2, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm2
	movupd	(%rsi,%rdx,8), %xmm3
	movupd	(%rsi,%rax,8), %xmm5
	movapd	%xmm3, %xmm6
	addpd	%xmm5, %xmm6
	subpd	%xmm5, %xmm3
	movapd	%xmm3, %xmm5
	shufpd	$1, %xmm5, %xmm5        # xmm5 = xmm5[1,0]
	movapd	%xmm4, %xmm7
	addpd	%xmm6, %xmm7
	movupd	%xmm7, (%rsi,%rdi,8)
	subpd	%xmm6, %xmm4
	movupd	%xmm4, (%rsi,%rdx,8)
	movapd	%xmm2, %xmm4
	subpd	%xmm5, %xmm4
	addpd	%xmm2, %xmm5
	movapd	%xmm5, %xmm6
	movsd	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1]
	shufpd	$1, %xmm4, %xmm5        # xmm5 = xmm5[1],xmm4[0]
	movapd	%xmm6, %xmm4
	subpd	%xmm5, %xmm4
	addpd	%xmm6, %xmm5
	movsd	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1]
	mulpd	%xmm1, %xmm5
	movupd	%xmm5, (%rsi,%rcx,8)
	movapd	%xmm3, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	addsd	%xmm2, %xmm1
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	subsd	%xmm3, %xmm2
	xorpd	.LCPI14_1(%rip), %xmm8
	movapd	%xmm2, %xmm3
	addsd	%xmm1, %xmm3
	mulsd	%xmm8, %xmm3
	movsd	%xmm3, (%rsi,%rax,8)
	subsd	%xmm1, %xmm2
	mulsd	%xmm8, %xmm2
	movsd	%xmm2, 8(%rsi,%rax,8)
	movupd	16(%rsi,%rdi,8), %xmm1
	movupd	16(%rsi,%rcx,8), %xmm2
	movapd	%xmm1, %xmm3
	addpd	%xmm2, %xmm3
	subpd	%xmm2, %xmm1
	movupd	16(%rsi,%rdx,8), %xmm2
	movupd	16(%rsi,%rax,8), %xmm4
	movapd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	subpd	%xmm4, %xmm2
	movapd	%xmm2, %xmm4
	shufpd	$1, %xmm4, %xmm4        # xmm4 = xmm4[1,0]
	movapd	%xmm3, %xmm6
	addpd	%xmm5, %xmm6
	movupd	%xmm6, 16(%rsi,%rdi,8)
	subpd	%xmm5, %xmm3
	movupd	%xmm3, 16(%rsi,%rdx,8)
	movapd	%xmm1, %xmm3
	subpd	%xmm4, %xmm3
	addpd	%xmm1, %xmm4
	movapd	%xmm4, %xmm5
	movsd	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1]
	mulpd	%xmm11, %xmm5
	shufpd	$1, %xmm3, %xmm4        # xmm4 = xmm4[1],xmm3[0]
	mulpd	%xmm10, %xmm4
	movapd	%xmm5, %xmm3
	subpd	%xmm4, %xmm3
	addpd	%xmm5, %xmm4
	movsd	%xmm3, %xmm4            # xmm4 = xmm3[0],xmm4[1]
	movupd	%xmm4, 16(%rsi,%rcx,8)
	movapd	%xmm2, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	addsd	%xmm1, %xmm3
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm9, %xmm3
	movapd	%xmm1, %xmm0
	addpd	%xmm3, %xmm0
	subpd	%xmm3, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	%xmm1, 16(%rsi,%rax,8)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	cftf1st, .Lfunc_end14-cftf1st
	.cfi_endproc

	.globl	cftrec1
	.p2align	4, 0x90
	.type	cftrec1,@function
cftrec1:                                # @cftrec1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi151:
	.cfi_def_cfa_offset 64
.Lcfi152:
	.cfi_offset %rbx, -56
.Lcfi153:
	.cfi_offset %r12, -48
.Lcfi154:
	.cfi_offset %r13, -40
.Lcfi155:
	.cfi_offset %r14, -32
.Lcfi156:
	.cfi_offset %r15, -24
.Lcfi157:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movl	%edi, %r12d
	movl	%r12d, %ebp
	sarl	$2, %ebp
	leal	(%rbp,%rbp), %r13d
	movl	%r15d, %eax
	subl	%r13d, %eax
	cltq
	leaq	(%r14,%rax,8), %rdx
	callq	cftmdl1
	cmpl	$513, %r12d             # imm = 0x201
	jl	.LBB15_1
# BB#2:                                 # %tailrecurse.preheader
	movl	%r13d, %r12d
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB15_3:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %r13d
	movl	%r13d, %edi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	movq	%r14, %rcx
	callq	cftrec1
	movq	%r14, %r15
	movslq	%r13d, %r14
	leaq	(%rbx,%r14,8), %rsi
	movl	%r14d, %edi
	movl	4(%rsp), %edx           # 4-byte Reload
	movq	%r15, %rcx
	callq	cftrec2
	movslq	%r12d, %rax
	leaq	(%rbx,%rax,8), %rsi
	movl	%r14d, %edi
	movl	4(%rsp), %edx           # 4-byte Reload
	movq	%r15, %rcx
	callq	cftrec1
	leal	(%r13,%r13,2), %eax
	cltq
	leaq	(%rbx,%rax,8), %rbx
	movl	%r14d, %ebp
	sarl	$2, %ebp
	leal	(%rbp,%rbp), %r12d
	movl	4(%rsp), %eax           # 4-byte Reload
	subl	%r12d, %eax
	cltq
	leaq	(%r15,%rax,8), %rdx
	movl	%r14d, %edi
	movq	%rbx, %rsi
	callq	cftmdl1
	cmpl	$512, %r14d             # imm = 0x200
	movq	%r15, %r14
	movl	4(%rsp), %r15d          # 4-byte Reload
	jg	.LBB15_3
	jmp	.LBB15_4
.LBB15_1:
	movq	%r12, %r13
.LBB15_4:                               # %tailrecurse._crit_edge
	movl	%r13d, %edi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cftexp1                 # TAILCALL
.Lfunc_end15:
	.size	cftrec1, .Lfunc_end15-cftrec1
	.cfi_endproc

	.globl	cftrec2
	.p2align	4, 0x90
	.type	cftrec2,@function
cftrec2:                                # @cftrec2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi161:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 48
.Lcfi163:
	.cfi_offset %rbx, -48
.Lcfi164:
	.cfi_offset %r12, -40
.Lcfi165:
	.cfi_offset %r14, -32
.Lcfi166:
	.cfi_offset %r15, -24
.Lcfi167:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movl	%edi, %r12d
	movl	%r15d, %eax
	subl	%r12d, %eax
	cltq
	leaq	(%r14,%rax,8), %rdx
	callq	cftmdl2
	cmpl	$513, %r12d             # imm = 0x201
	jl	.LBB16_2
	.p2align	4, 0x90
.LBB16_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	sarl	$2, %r12d
	movl	%r12d, %edi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	movq	%r14, %rcx
	callq	cftrec1
	movslq	%r12d, %rbp
	leaq	(%rbx,%rbp,8), %rsi
	movl	%ebp, %edi
	movl	%r15d, %edx
	movq	%r14, %rcx
	callq	cftrec2
	leal	(%rbp,%rbp), %eax
	cltq
	leaq	(%rbx,%rax,8), %rsi
	movl	%ebp, %edi
	movl	%r15d, %edx
	movq	%r14, %rcx
	callq	cftrec1
	leal	(%r12,%r12,2), %eax
	cltq
	leaq	(%rbx,%rax,8), %rbx
	movl	%r15d, %eax
	subl	%ebp, %eax
	cltq
	leaq	(%r14,%rax,8), %rdx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cftmdl2
	cmpl	$512, %ebp              # imm = 0x200
	jg	.LBB16_1
.LBB16_2:                               # %tailrecurse._crit_edge
	movl	%r12d, %edi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	movq	%r14, %rcx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cftexp2                 # TAILCALL
.Lfunc_end16:
	.size	cftrec2, .Lfunc_end16-cftrec2
	.cfi_endproc

	.globl	cftexp1
	.p2align	4, 0x90
	.type	cftexp1,@function
cftexp1:                                # @cftexp1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi171:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi172:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi174:
	.cfi_def_cfa_offset 160
.Lcfi175:
	.cfi_offset %rbx, -56
.Lcfi176:
	.cfi_offset %r12, -48
.Lcfi177:
	.cfi_offset %r13, -40
.Lcfi178:
	.cfi_offset %r14, -32
.Lcfi179:
	.cfi_offset %r15, -24
.Lcfi180:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movl	%edi, %r14d
	movl	%r14d, %ebx
	sarl	$2, %ebx
	cmpl	$129, %ebx
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%r14d, 28(%rsp)         # 4-byte Spill
	jl	.LBB17_1
# BB#6:                                 # %.preheader106.preheader
	movslq	%r14d, %r15
	movl	%r14d, %ebp
	movq	%r15, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_7:                               # %.preheader106
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_9 Depth 2
                                        #       Child Loop BB17_12 Depth 3
	sarl	$3, %ebp
	movl	%edx, %eax
	subl	%ebp, %eax
	movl	%ebx, %ebp
	cltq
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rax,8), %r13
	cmpl	%r14d, %ebp
	jge	.LBB17_15
# BB#8:                                 # %.lr.ph118
                                        #   in Loop: Header=BB17_7 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
	subl	%ebp, %eax
	cltq
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rax,8), %rdx
	movl	%ebp, %eax
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_9:                               #   Parent Loop BB17_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_12 Depth 3
	movl	%eax, %ecx
	subl	%ebp, %ecx
	cmpl	%r14d, %ecx
	jge	.LBB17_10
# BB#11:                                # %.lr.ph114
                                        #   in Loop: Header=BB17_9 Depth=2
	leal	(%rax,%rax), %edx
	leal	(,%rax,4), %esi
	movslq	%ecx, %rbx
	movl	%esi, 40(%rsp)          # 4-byte Spill
	movslq	%esi, %rsi
	movslq	%edx, %rcx
	cltq
	leaq	(,%rbx,8), %r15
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	leaq	(,%rsi,8), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	leaq	(%r15,%rax,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	(%r15,%rcx,8), %r14
	movq	32(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB17_12:                              #   Parent Loop BB17_7 Depth=1
                                        #     Parent Loop BB17_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r12,%r15), %rsi
	movl	%ebp, %edi
	movq	%r13, %rdx
	callq	cftmdl1
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%r12,%rax), %rsi
	movl	%ebp, %edi
	movq	56(%rsp), %rdx          # 8-byte Reload
	callq	cftmdl2
	leaq	(%r12,%r14), %rsi
	movl	%ebp, %edi
	movq	%r13, %rdx
	callq	cftmdl1
	addq	16(%rsp), %rbx          # 8-byte Folded Reload
	addq	72(%rsp), %r12          # 8-byte Folded Reload
	cmpq	48(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB17_12
# BB#13:                                #   in Loop: Header=BB17_9 Depth=2
	movl	28(%rsp), %r14d         # 4-byte Reload
	movl	40(%rsp), %eax          # 4-byte Reload
	jmp	.LBB17_14
	.p2align	4, 0x90
.LBB17_10:                              # %.._crit_edge115_crit_edge
                                        #   in Loop: Header=BB17_9 Depth=2
	shll	$2, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
.LBB17_14:                              # %._crit_edge115
                                        #   in Loop: Header=BB17_9 Depth=2
	cmpl	%r14d, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jl	.LBB17_9
.LBB17_15:                              # %._crit_edge119
                                        #   in Loop: Header=BB17_7 Depth=1
	movl	%r14d, %eax
	subl	%ebp, %eax
	cltq
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	movl	%ebp, %edi
	movq	%r13, %rdx
	callq	cftmdl1
	movl	%ebp, %ebx
	sarl	$2, %ebx
	cmpl	$128, %ebx
	movl	4(%rsp), %edx           # 4-byte Reload
	jg	.LBB17_7
	jmp	.LBB17_2
.LBB17_1:
	movl	%r14d, %ebp
.LBB17_2:                               # %.preheader
	sarl	$3, %ebp
	movl	%edx, %eax
	subl	%ebp, %eax
	cltq
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	(%rbp,%rax,8), %rdx
	cmpl	%r14d, %ebx
	jge	.LBB17_20
# BB#3:                                 # %.lr.ph110
	movl	4(%rsp), %eax           # 4-byte Reload
	subl	%ebx, %eax
	cltq
	leaq	(%rbp,%rax,8), %rsi
	movslq	%r14d, %rdi
	movl	%ebx, %eax
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_17 Depth 2
	movl	%eax, %ecx
	subl	%ebx, %ecx
	cmpl	%r14d, %ecx
	jge	.LBB17_5
# BB#16:                                # %.lr.ph
                                        #   in Loop: Header=BB17_4 Depth=1
	leal	(%rax,%rax), %r8d
	leal	(,%rax,4), %esi
	movl	%ebx, %r15d
	movslq	%ecx, %r12
	movl	%esi, 84(%rsp)          # 4-byte Spill
	movslq	%esi, %rsi
	movslq	%r8d, %r8
	cltq
	leaq	(,%r12,8), %rcx
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	leaq	(,%rsi,8), %rsi
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rcx, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	(%rcx,%r8,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB17_17:                              #   Parent Loop BB17_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%r13,%rax), %r14
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	cftmdl1
	movl	%r15d, %edi
	movq	%r14, %rsi
	movl	4(%rsp), %ebx           # 4-byte Reload
	movl	%ebx, %edx
	movq	%rbp, %rcx
	callq	cftfx41
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%r13,%rax), %rbp
	movl	%r15d, %edi
	movq	%rbp, %rsi
	movq	88(%rsp), %rdx          # 8-byte Reload
	callq	cftmdl2
	movl	%r15d, %edi
	movq	%rbp, %rsi
	movl	%ebx, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	cftfx42
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%r13,%rax), %rbp
	movl	%r15d, %edi
	movq	%rbp, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	cftmdl1
	movl	%r15d, %edi
	movq	%rbp, %rsi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ebx, %edx
	movq	%rbp, %rcx
	callq	cftfx41
	movq	96(%rsp), %rdi          # 8-byte Reload
	addq	72(%rsp), %r12          # 8-byte Folded Reload
	addq	56(%rsp), %r13          # 8-byte Folded Reload
	cmpq	%rdi, %r12
	jl	.LBB17_17
# BB#18:                                #   in Loop: Header=BB17_4 Depth=1
	movl	28(%rsp), %r14d         # 4-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%r15d, %ebx
	jmp	.LBB17_19
	.p2align	4, 0x90
.LBB17_5:                               # %.._crit_edge_crit_edge
                                        #   in Loop: Header=BB17_4 Depth=1
	shll	$2, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
.LBB17_19:                              # %._crit_edge
                                        #   in Loop: Header=BB17_4 Depth=1
	cmpl	%r14d, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jl	.LBB17_4
.LBB17_20:                              # %._crit_edge111
	subl	%ebx, %r14d
	movslq	%r14d, %rax
	movq	%rbp, %r14
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbp
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	cftmdl1
	movl	%ebx, %edi
	movq	%rbp, %rsi
	movl	4(%rsp), %edx           # 4-byte Reload
	movq	%r14, %rcx
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cftfx41                 # TAILCALL
.Lfunc_end17:
	.size	cftexp1, .Lfunc_end17-cftexp1
	.cfi_endproc

	.globl	cftfx41
	.p2align	4, 0x90
	.type	cftfx41,@function
cftfx41:                                # @cftfx41
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi181:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi182:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi183:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi184:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi185:
	.cfi_def_cfa_offset 48
.Lcfi186:
	.cfi_offset %rbx, -40
.Lcfi187:
	.cfi_offset %r12, -32
.Lcfi188:
	.cfi_offset %r14, -24
.Lcfi189:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rsi, %rbx
	cmpl	$128, %edi
	jne	.LBB18_2
# BB#1:
	movslq	%edx, %r12
	leaq	-64(%r14,%r12,8), %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	cftf161
	leaq	256(%rbx), %rdi
	leaq	-256(%r14,%r12,8), %rsi
	callq	cftf162
	leaq	512(%rbx), %rdi
	movq	%r15, %rsi
	callq	cftf161
	addq	$768, %rbx              # imm = 0x300
	movq	%rbx, %rdi
	movq	%r15, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	cftf161                 # TAILCALL
.LBB18_2:
	movslq	%edx, %rax
	leaq	-128(%r14,%rax,8), %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	cftf081
	movq	%rbx, %rdi
	subq	$-128, %rdi
	movq	%r14, %rsi
	callq	cftf082
	leaq	256(%rbx), %rdi
	movq	%r14, %rsi
	callq	cftf081
	addq	$384, %rbx              # imm = 0x180
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	cftf081                 # TAILCALL
.Lfunc_end18:
	.size	cftfx41, .Lfunc_end18-cftfx41
	.cfi_endproc

	.globl	bitrv2
	.p2align	4, 0x90
	.type	bitrv2,@function
bitrv2:                                 # @bitrv2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi190:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi191:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi193:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi194:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 56
.Lcfi196:
	.cfi_offset %rbx, -56
.Lcfi197:
	.cfi_offset %r12, -48
.Lcfi198:
	.cfi_offset %r13, -40
.Lcfi199:
	.cfi_offset %r14, -32
.Lcfi200:
	.cfi_offset %r15, -24
.Lcfi201:
	.cfi_offset %rbp, -16
	movl	$0, (%rsi)
	cmpl	$9, %edi
	jl	.LBB19_20
# BB#1:                                 # %.lr.ph246.preheader
	leaq	16(%rsi), %r8
	leaq	12(%rsi), %r9
	movl	$1, %r10d
	.p2align	4, 0x90
.LBB19_2:                               # %.lr.ph246
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_11 Depth 2
                                        #     Child Loop BB19_15 Depth 2
                                        #     Child Loop BB19_18 Depth 2
	sarl	%edi
	testl	%r10d, %r10d
	jle	.LBB19_19
# BB#3:                                 # %.lr.ph240.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	movslq	%r10d, %r11
	movl	%r10d, %ebx
	cmpl	$8, %r10d
	jae	.LBB19_5
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB19_13
	.p2align	4, 0x90
.LBB19_5:                               # %min.iters.checked
                                        #   in Loop: Header=BB19_2 Depth=1
	movl	%r10d, %r14d
	andl	$7, %r14d
	movq	%rbx, %rax
	subq	%r14, %rax
	je	.LBB19_9
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB19_2 Depth=1
	leaq	(%rsi,%r11,4), %rcx
	leaq	(%rsi,%rbx,4), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB19_10
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB19_2 Depth=1
	leaq	(%r11,%rbx), %rcx
	leaq	(%rsi,%rcx,4), %rcx
	cmpq	%rsi, %rcx
	jbe	.LBB19_10
.LBB19_9:                               #   in Loop: Header=BB19_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB19_13
.LBB19_10:                              # %vector.ph
                                        #   in Loop: Header=BB19_2 Depth=1
	movd	%edi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	%rax, %rbp
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB19_11:                              # %vector.body
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rcx), %xmm1
	movdqu	(%rcx), %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm2
	movdqu	%xmm1, -16(%rcx,%r11,4)
	movdqu	%xmm2, (%rcx,%r11,4)
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB19_11
# BB#12:                                # %middle.block
                                        #   in Loop: Header=BB19_2 Depth=1
	testl	%r14d, %r14d
	je	.LBB19_19
	.p2align	4, 0x90
.LBB19_13:                              # %.lr.ph240.preheader296
                                        #   in Loop: Header=BB19_2 Depth=1
	movl	%ebx, %ecx
	subl	%eax, %ecx
	leaq	-1(%rbx), %r14
	subq	%rax, %r14
	andq	$3, %rcx
	je	.LBB19_16
# BB#14:                                # %.lr.ph240.prol.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	leaq	(%rsi,%r11,4), %r15
	negq	%rcx
	.p2align	4, 0x90
.LBB19_15:                              # %.lr.ph240.prol
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi,%rax,4), %ebp
	addl	%edi, %ebp
	movl	%ebp, (%r15,%rax,4)
	incq	%rax
	incq	%rcx
	jne	.LBB19_15
.LBB19_16:                              # %.lr.ph240.prol.loopexit
                                        #   in Loop: Header=BB19_2 Depth=1
	cmpq	$3, %r14
	jb	.LBB19_19
# BB#17:                                # %.lr.ph240.preheader296.new
                                        #   in Loop: Header=BB19_2 Depth=1
	subq	%rax, %rbx
	addq	%rax, %r11
	leaq	(%r9,%r11,4), %rbp
	leaq	(%r9,%rax,4), %rax
	.p2align	4, 0x90
.LBB19_18:                              # %.lr.ph240
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rax), %ecx
	addl	%edi, %ecx
	movl	%ecx, -12(%rbp)
	movl	-8(%rax), %ecx
	addl	%edi, %ecx
	movl	%ecx, -8(%rbp)
	movl	-4(%rax), %ecx
	addl	%edi, %ecx
	movl	%ecx, -4(%rbp)
	movl	(%rax), %ecx
	addl	%edi, %ecx
	movl	%ecx, (%rbp)
	addq	$16, %rbp
	addq	$16, %rax
	addq	$-4, %rbx
	jne	.LBB19_18
.LBB19_19:                              # %._crit_edge241
                                        #   in Loop: Header=BB19_2 Depth=1
	leal	(%r10,%r10), %r14d
	movl	%r10d, %eax
	shll	$4, %eax
	cmpl	%edi, %eax
	movl	%r14d, %r10d
	jl	.LBB19_2
	jmp	.LBB19_21
.LBB19_20:
	movl	$8, %eax
	movl	$1, %r14d
.LBB19_21:                              # %._crit_edge247
	leal	(%r14,%r14), %r9d
	cmpl	%edi, %eax
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movl	%r9d, -28(%rsp)         # 4-byte Spill
	jne	.LBB19_29
# BB#22:                                # %.preheader228
	testl	%r14d, %r14d
	jle	.LBB19_34
# BB#23:                                # %.preheader.lr.ph
	movslq	%r9d, %r10
	movl	%r14d, %eax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	shll	$2, %r14d
	leaq	(,%r10,8), %r12
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB19_24:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_26 Depth 2
	testq	%r11, %r11
	movslq	(%rsi,%r11,4), %rax
	leal	(%r11,%r11), %r15d
	jle	.LBB19_28
# BB#25:                                # %.lr.ph
                                        #   in Loop: Header=BB19_24 Depth=1
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rax,8), %rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_26:                              #   Parent Loop BB19_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rsi,%rbx,4), %rbp
	movslq	%r15d, %rcx
	addq	%rbp, %rcx
	movq	(%rax), %rbp
	movq	8(%rax), %r13
	movq	(%rdx,%rcx,8), %r8
	movq	%r11, %rdi
	movslq	%ecx, %r11
	movq	8(%rdx,%r11,8), %r9
	movq	%r8, (%rax)
	movq	%r9, 8(%rax)
	movq	%rbp, (%rdx,%rcx,8)
	movq	%r13, 8(%rdx,%r11,8)
	movl	-28(%rsp), %r9d         # 4-byte Reload
	movq	-16(%rsp), %rsi         # 8-byte Reload
	addl	%r14d, %r11d
	leaq	(%rax,%r12), %rcx
	movups	(%rax,%r10,8), %xmm0
	movslq	%r11d, %rbp
	movq	%rdi, %r11
	movq	(%rdx,%rbp,8), %r8
	movq	8(%rdx,%rbp,8), %rdi
	movq	%r8, (%rax,%r10,8)
	movq	%rdi, 8(%rax,%r10,8)
	movups	%xmm0, (%rdx,%rbp,8)
	subl	%r9d, %ebp
	movups	(%rcx,%r10,8), %xmm0
	movslq	%ebp, %rdi
	movq	(%rdx,%rdi,8), %r8
	movq	8(%rdx,%rdi,8), %rbp
	movq	%r8, (%rcx,%r10,8)
	movq	%rbp, 8(%rcx,%r10,8)
	leaq	(%rcx,%r12), %rcx
	movups	%xmm0, (%rdx,%rdi,8)
	addl	%r14d, %edi
	movdqu	(%rcx,%r10,8), %xmm0
	movslq	%edi, %rdi
	movq	(%rdx,%rdi,8), %r8
	movq	8(%rdx,%rdi,8), %rbp
	movq	%r8, (%rcx,%r10,8)
	movq	%rbp, 8(%rcx,%r10,8)
	movdqu	%xmm0, (%rdx,%rdi,8)
	incq	%rbx
	addq	$16, %rax
	cmpq	%rbx, %r11
	jne	.LBB19_26
# BB#27:                                #   in Loop: Header=BB19_24 Depth=1
	movq	-24(%rsp), %rax         # 8-byte Reload
.LBB19_28:                              # %._crit_edge
                                        #   in Loop: Header=BB19_24 Depth=1
	addl	%r9d, %r15d
	addl	%eax, %r15d
	movslq	%r15d, %rax
	addl	%r9d, %r15d
	movq	(%rdx,%rax,8), %rcx
	movq	8(%rdx,%rax,8), %r8
	movslq	%r15d, %rdi
	movq	(%rdx,%rdi,8), %rbp
	movq	8(%rdx,%rdi,8), %rbx
	movq	%rbp, (%rdx,%rax,8)
	movq	%rbx, 8(%rdx,%rax,8)
	movq	%rcx, (%rdx,%rdi,8)
	movq	%r8, 8(%rdx,%rdi,8)
	incq	%r11
	cmpq	-8(%rsp), %r11          # 8-byte Folded Reload
	jne	.LBB19_24
	jmp	.LBB19_34
.LBB19_29:                              # %.preheader230
	cmpl	$2, %r14d
	jl	.LBB19_34
# BB#30:                                # %.preheader229.preheader
	movslq	%r9d, %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movl	%r14d, %r10d
	leaq	8(%rdx), %r11
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB19_31:                              # %.lr.ph235
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_32 Depth 2
	movslq	(%rsi,%r9,4), %rax
	leal	(%r9,%r9), %r14d
	movq	-24(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax), %rcx
	leaq	(%r11,%rcx,8), %rcx
	leaq	(%r11,%rax,8), %rax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB19_32:                              #   Parent Loop BB19_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rsi,%rdi,4), %rsi
	movslq	%r14d, %rbx
	addq	%rsi, %rbx
	movq	-8(%rax), %r15
	movq	(%rax), %r12
	movq	(%rdx,%rbx,8), %r13
	movslq	%ebx, %rbp
	movq	8(%rdx,%rbp,8), %r8
	movq	%r13, -8(%rax)
	movq	-16(%rsp), %rsi         # 8-byte Reload
	movq	%r8, (%rax)
	movq	%r15, (%rdx,%rbx,8)
	movq	%r12, 8(%rdx,%rbp,8)
	addl	-28(%rsp), %ebp         # 4-byte Folded Reload
	movdqu	-8(%rcx), %xmm0
	movslq	%ebp, %rbx
	movq	(%rdx,%rbx,8), %r8
	movq	8(%rdx,%rbx,8), %rbp
	movq	%r8, -8(%rcx)
	movq	%rbp, (%rcx)
	movdqu	%xmm0, (%rdx,%rbx,8)
	incq	%rdi
	addq	$16, %rcx
	addq	$16, %rax
	cmpq	%rdi, %r9
	jne	.LBB19_32
# BB#33:                                # %._crit_edge236
                                        #   in Loop: Header=BB19_31 Depth=1
	incq	%r9
	cmpq	%r10, %r9
	jne	.LBB19_31
.LBB19_34:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	bitrv2, .Lfunc_end19-bitrv2
	.cfi_endproc

	.globl	cftf161
	.p2align	4, 0x90
	.type	cftf161,@function
cftf161:                                # @cftf161
	.cfi_startproc
# BB#0:
	subq	$104, %rsp
.Lcfi202:
	.cfi_def_cfa_offset 112
	movsd	8(%rsi), %xmm8          # xmm8 = mem[0],zero
	movapd	%xmm8, 32(%rsp)         # 16-byte Spill
	movsd	16(%rsi), %xmm7         # xmm7 = mem[0],zero
	mulsd	%xmm7, %xmm8
	movupd	128(%rdi), %xmm1
	movupd	(%rdi), %xmm5
	movupd	16(%rdi), %xmm0
	movapd	%xmm5, %xmm3
	addpd	%xmm1, %xmm3
	subpd	%xmm1, %xmm5
	movapd	%xmm5, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movapd	%xmm1, %xmm6
	movupd	64(%rdi), %xmm1
	movupd	192(%rdi), %xmm2
	movapd	%xmm1, %xmm4
	addpd	%xmm2, %xmm4
	subpd	%xmm2, %xmm1
	movapd	%xmm3, %xmm2
	addpd	%xmm4, %xmm2
	movapd	%xmm2, 80(%rsp)         # 16-byte Spill
	subpd	%xmm4, %xmm3
	movapd	%xmm3, 64(%rsp)         # 16-byte Spill
	movapd	%xmm5, %xmm2
	addpd	%xmm1, %xmm6
	movapd	%xmm6, (%rsp)           # 16-byte Spill
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	subsd	%xmm1, %xmm5
	movapd	%xmm5, -16(%rsp)        # 16-byte Spill
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm1, %xmm2
	movapd	%xmm2, 16(%rsp)         # 16-byte Spill
	movupd	144(%rdi), %xmm1
	movapd	%xmm0, %xmm13
	addpd	%xmm1, %xmm13
	movapd	%xmm0, %xmm10
	subsd	%xmm1, %xmm10
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm1, %xmm0
	movupd	80(%rdi), %xmm1
	movupd	208(%rdi), %xmm2
	movapd	%xmm1, %xmm6
	addpd	%xmm2, %xmm6
	movapd	%xmm1, %xmm3
	subsd	%xmm2, %xmm3
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	subsd	%xmm2, %xmm1
	movapd	%xmm13, %xmm2
	addpd	%xmm6, %xmm2
	movapd	%xmm2, 48(%rsp)         # 16-byte Spill
	subpd	%xmm6, %xmm13
	movapd	%xmm0, %xmm2
	addsd	%xmm3, %xmm2
	unpcklpd	%xmm10, %xmm7   # xmm7 = xmm7[0],xmm10[0]
	subsd	%xmm1, %xmm10
	movapd	%xmm8, %xmm9
	unpcklpd	%xmm1, %xmm9    # xmm9 = xmm9[0],xmm1[0]
	addpd	%xmm7, %xmm9
	movapd	%xmm9, %xmm1
	mulsd	%xmm2, %xmm1
	movapd	%xmm8, %xmm4
	mulsd	%xmm10, %xmm4
	addsd	%xmm1, %xmm4
	movapd	%xmm4, -32(%rsp)        # 16-byte Spill
	movupd	32(%rdi), %xmm7
	subsd	%xmm3, %xmm0
	unpcklpd	%xmm8, %xmm10   # xmm10 = xmm10[0],xmm8[0]
	mulpd	%xmm9, %xmm10
	movapd	%xmm8, %xmm1
	unpcklpd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	unpcklpd	%xmm9, %xmm2    # xmm2 = xmm2[0],xmm9[0]
	mulpd	%xmm1, %xmm2
	subpd	%xmm2, %xmm10
	mulsd	%xmm8, %xmm0
	movapd	%xmm9, %xmm14
	movhlps	%xmm14, %xmm14          # xmm14 = xmm14[1,1]
	mulsd	%xmm9, %xmm14
	addsd	%xmm0, %xmm14
	movupd	160(%rdi), %xmm0
	movapd	%xmm7, %xmm3
	addpd	%xmm0, %xmm3
	subpd	%xmm0, %xmm7
	movupd	96(%rdi), %xmm0
	movupd	224(%rdi), %xmm1
	movapd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	subpd	%xmm1, %xmm0
	movapd	%xmm3, %xmm1
	addpd	%xmm2, %xmm1
	movapd	%xmm1, -48(%rsp)        # 16-byte Spill
	subpd	%xmm2, %xmm3
	movapd	%xmm3, -64(%rsp)        # 16-byte Spill
	movapd	%xmm7, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	addpd	%xmm0, %xmm1
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	subpd	%xmm0, %xmm7
	movapd	%xmm7, %xmm0
	subsd	%xmm1, %xmm0
	movapd	%xmm0, -96(%rsp)        # 16-byte Spill
	movapd	%xmm1, %xmm4
	addpd	%xmm7, %xmm4
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	movhlps	%xmm7, %xmm7            # xmm7 = xmm7[1,1]
	subsd	%xmm1, %xmm7
	movupd	48(%rdi), %xmm3
	movupd	176(%rdi), %xmm0
	movapd	%xmm3, %xmm12
	addpd	%xmm0, %xmm12
	movapd	%xmm3, %xmm15
	subsd	%xmm0, %xmm15
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	subsd	%xmm0, %xmm3
	movupd	112(%rdi), %xmm6
	movupd	240(%rdi), %xmm0
	movapd	%xmm6, %xmm2
	addpd	%xmm0, %xmm2
	movapd	%xmm6, %xmm1
	subsd	%xmm0, %xmm1
	movhlps	%xmm6, %xmm6            # xmm6 = xmm6[1,1]
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	subsd	%xmm0, %xmm6
	movapd	%xmm12, %xmm0
	addpd	%xmm2, %xmm0
	movapd	%xmm0, -80(%rsp)        # 16-byte Spill
	subpd	%xmm2, %xmm12
	movapd	%xmm3, %xmm0
	addpd	%xmm1, %xmm0
	subpd	%xmm1, %xmm3
	movapd	%xmm15, %xmm1
	subpd	%xmm6, %xmm1
	addpd	%xmm15, %xmm6
	movapd	%xmm8, %xmm2
	mulsd	%xmm0, %xmm2
	movapd	%xmm9, %xmm5
	mulsd	%xmm1, %xmm5
	addsd	%xmm2, %xmm5
	movapd	%xmm5, -112(%rsp)       # 16-byte Spill
	movapd	%xmm9, %xmm11
	unpcklpd	%xmm8, %xmm11   # xmm11 = xmm11[0],xmm8[0]
	unpcklpd	%xmm6, %xmm1    # xmm1 = xmm1[0],xmm6[0]
	mulsd	%xmm8, %xmm6
	unpcklpd	%xmm9, %xmm8    # xmm8 = xmm8[0],xmm9[0]
	mulpd	%xmm1, %xmm8
	unpcklpd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0]
	mulpd	%xmm0, %xmm11
	subpd	%xmm11, %xmm8
	mulsd	%xmm9, %xmm3
	addsd	%xmm3, %xmm6
	movapd	%xmm12, %xmm11
	shufpd	$1, %xmm11, %xmm11      # xmm11 = xmm11[1,0]
	movapd	%xmm13, %xmm0
	subpd	%xmm11, %xmm0
	movapd	%xmm0, -128(%rsp)       # 16-byte Spill
	addpd	%xmm13, %xmm11
	movapd	%xmm12, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	addsd	%xmm13, %xmm0
	movhlps	%xmm13, %xmm13          # xmm13 = xmm13[1,1]
	subsd	%xmm12, %xmm13
	movapd	%xmm0, %xmm5
	subsd	%xmm13, %xmm5
	addsd	%xmm0, %xmm13
	movapd	32(%rsp), %xmm12        # 16-byte Reload
	movapd	-96(%rsp), %xmm9        # 16-byte Reload
	mulsd	%xmm12, %xmm9
	mulsd	%xmm12, %xmm7
	mulsd	%xmm12, %xmm5
	mulsd	%xmm12, %xmm13
	movlhps	%xmm12, %xmm12          # xmm12 = xmm12[0,0]
	mulpd	%xmm12, %xmm4
	movapd	(%rsp), %xmm0           # 16-byte Reload
	movapd	%xmm0, %xmm1
	subpd	%xmm4, %xmm1
	movapd	%xmm4, %xmm3
	addsd	%xmm0, %xmm4
	movapd	-16(%rsp), %xmm15       # 16-byte Reload
	movsd	%xmm15, %xmm0           # xmm0 = xmm15[0],xmm0[1]
	subsd	%xmm7, %xmm15
	movsd	%xmm7, %xmm3            # xmm3 = xmm7[0],xmm3[1]
	addpd	%xmm0, %xmm3
	movapd	%xmm10, %xmm0
	subpd	%xmm8, %xmm0
	movapd	%xmm8, %xmm7
	addsd	%xmm10, %xmm8
	shufpd	$1, %xmm14, %xmm10      # xmm10 = xmm10[1],xmm14[0]
	subsd	%xmm6, %xmm14
	shufpd	$1, %xmm6, %xmm7        # xmm7 = xmm7[1],xmm6[0]
	addpd	%xmm10, %xmm7
	movapd	%xmm1, %xmm6
	addsd	%xmm0, %xmm6
	movapd	%xmm1, %xmm10
	subpd	%xmm0, %xmm10
	movapd	%xmm0, %xmm2
	addpd	%xmm1, %xmm0
	shufpd	$1, %xmm15, %xmm1       # xmm1 = xmm1[1],xmm15[0]
	addsd	%xmm14, %xmm15
	movsd	%xmm15, 200(%rdi)
	shufpd	$1, %xmm14, %xmm2       # xmm2 = xmm2[1],xmm14[0]
	subpd	%xmm2, %xmm1
	movupd	%xmm1, 208(%rdi)
	movapd	%xmm3, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	movapd	%xmm3, %xmm2
	addpd	%xmm7, %xmm2
	subsd	%xmm7, %xmm3
	movhlps	%xmm7, %xmm7            # xmm7 = xmm7[1,1]
	subsd	%xmm7, %xmm1
	movsd	%xmm1, 224(%rdi)
	movupd	%xmm2, 232(%rdi)
	movsd	%xmm3, 248(%rdi)
	movapd	16(%rsp), %xmm7         # 16-byte Reload
	movapd	%xmm7, %xmm1
	addsd	%xmm9, %xmm1
	subsd	%xmm9, %xmm7
	movapd	-32(%rsp), %xmm2        # 16-byte Reload
	movapd	%xmm2, %xmm14
	movapd	-112(%rsp), %xmm3       # 16-byte Reload
	addsd	%xmm3, %xmm14
	subsd	%xmm3, %xmm2
	movapd	%xmm1, %xmm3
	addsd	%xmm8, %xmm3
	movsd	%xmm3, 128(%rdi)
	movapd	%xmm4, %xmm3
	addsd	%xmm14, %xmm3
	movsd	%xmm3, 136(%rdi)
	subsd	%xmm8, %xmm1
	movsd	%xmm1, 144(%rdi)
	subsd	%xmm14, %xmm4
	movsd	%xmm4, 152(%rdi)
	movapd	%xmm7, %xmm1
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 160(%rdi)
	movsd	%xmm6, 168(%rdi)
	addsd	%xmm7, %xmm2
	movsd	%xmm2, 176(%rdi)
	movsd	%xmm10, %xmm0           # xmm0 = xmm10[0],xmm0[1]
	movupd	%xmm0, 184(%rdi)
	movapd	%xmm11, %xmm0
	movapd	-128(%rsp), %xmm1       # 16-byte Reload
	movsd	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1]
	shufpd	$1, %xmm1, %xmm11       # xmm11 = xmm11[1],xmm1[0]
	movapd	%xmm0, %xmm1
	subpd	%xmm11, %xmm1
	addpd	%xmm0, %xmm11
	movsd	%xmm1, %xmm11           # xmm11 = xmm1[0],xmm11[1]
	movapd	-64(%rsp), %xmm2        # 16-byte Reload
	movapd	%xmm2, %xmm0
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	mulpd	%xmm12, %xmm11
	movapd	64(%rsp), %xmm3         # 16-byte Reload
	movapd	%xmm3, %xmm1
	subpd	%xmm0, %xmm1
	addpd	%xmm3, %xmm0
	movsd	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1]
	movapd	%xmm2, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	addsd	%xmm3, %xmm1
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	subsd	%xmm2, %xmm3
	movapd	%xmm0, %xmm2
	addpd	%xmm11, %xmm2
	movupd	%xmm2, 64(%rdi)
	subpd	%xmm11, %xmm0
	movupd	%xmm0, 80(%rdi)
	movapd	%xmm1, %xmm0
	subsd	%xmm13, %xmm0
	movsd	%xmm0, 96(%rdi)
	movapd	%xmm3, %xmm0
	addsd	%xmm5, %xmm0
	movsd	%xmm0, 104(%rdi)
	addsd	%xmm13, %xmm1
	movsd	%xmm1, 112(%rdi)
	subsd	%xmm5, %xmm3
	movsd	%xmm3, 120(%rdi)
	movapd	80(%rsp), %xmm3         # 16-byte Reload
	movapd	%xmm3, %xmm0
	movapd	-48(%rsp), %xmm1        # 16-byte Reload
	addpd	%xmm1, %xmm0
	subpd	%xmm1, %xmm3
	movapd	48(%rsp), %xmm4         # 16-byte Reload
	movapd	%xmm4, %xmm1
	movapd	-80(%rsp), %xmm2        # 16-byte Reload
	addpd	%xmm2, %xmm1
	subpd	%xmm2, %xmm4
	movapd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	movupd	%xmm2, (%rdi)
	subpd	%xmm1, %xmm0
	shufpd	$1, %xmm4, %xmm4        # xmm4 = xmm4[1,0]
	movupd	%xmm0, 16(%rdi)
	movapd	%xmm3, %xmm0
	subpd	%xmm4, %xmm0
	addpd	%xmm3, %xmm4
	movapd	%xmm4, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	%xmm1, 32(%rdi)
	movsd	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1]
	movupd	%xmm0, 48(%rdi)
	addq	$104, %rsp
	retq
.Lfunc_end20:
	.size	cftf161, .Lfunc_end20-cftf161
	.cfi_endproc

	.globl	bitrv216
	.p2align	4, 0x90
	.type	bitrv216,@function
bitrv216:                               # @bitrv216
	.cfi_startproc
# BB#0:
	movups	16(%rdi), %xmm9
	movups	32(%rdi), %xmm1
	movups	48(%rdi), %xmm8
	movups	64(%rdi), %xmm3
	movups	80(%rdi), %xmm11
	movups	112(%rdi), %xmm10
	movups	128(%rdi), %xmm6
	movups	160(%rdi), %xmm7
	movups	176(%rdi), %xmm2
	movups	192(%rdi), %xmm0
	movups	208(%rdi), %xmm5
	movups	224(%rdi), %xmm4
	movups	%xmm6, 16(%rdi)
	movups	%xmm3, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm1, 64(%rdi)
	movups	%xmm7, 80(%rdi)
	movups	%xmm4, 112(%rdi)
	movups	%xmm9, 128(%rdi)
	movups	%xmm11, 160(%rdi)
	movups	%xmm5, 176(%rdi)
	movups	%xmm8, 192(%rdi)
	movups	%xmm2, 208(%rdi)
	movups	%xmm10, 224(%rdi)
	retq
.Lfunc_end21:
	.size	bitrv216, .Lfunc_end21-bitrv216
	.cfi_endproc

	.globl	cftf081
	.p2align	4, 0x90
	.type	cftf081,@function
cftf081:                                # @cftf081
	.cfi_startproc
# BB#0:
	movsd	8(%rsi), %xmm8          # xmm8 = mem[0],zero
	movupd	64(%rdi), %xmm4
	movupd	(%rdi), %xmm1
	movupd	16(%rdi), %xmm2
	movupd	32(%rdi), %xmm5
	movupd	48(%rdi), %xmm13
	movapd	%xmm1, %xmm11
	addpd	%xmm4, %xmm11
	subpd	%xmm4, %xmm1
	movupd	96(%rdi), %xmm4
	movapd	%xmm5, %xmm7
	addpd	%xmm4, %xmm7
	subpd	%xmm4, %xmm5
	movapd	%xmm5, %xmm6
	shufpd	$1, %xmm6, %xmm6        # xmm6 = xmm6[1,0]
	movapd	%xmm11, %xmm10
	addpd	%xmm7, %xmm10
	subpd	%xmm7, %xmm11
	movapd	%xmm1, %xmm4
	subpd	%xmm6, %xmm4
	addpd	%xmm1, %xmm6
	movsd	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1]
	movapd	%xmm5, %xmm12
	movhlps	%xmm12, %xmm12          # xmm12 = xmm12[1,1]
	addsd	%xmm1, %xmm12
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm5, %xmm1
	movupd	80(%rdi), %xmm4
	movapd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	subpd	%xmm4, %xmm2
	movupd	112(%rdi), %xmm4
	movapd	%xmm13, %xmm3
	addpd	%xmm4, %xmm3
	subpd	%xmm4, %xmm13
	movapd	%xmm13, %xmm4
	shufpd	$1, %xmm4, %xmm4        # xmm4 = xmm4[1,0]
	movapd	%xmm5, %xmm9
	addpd	%xmm3, %xmm9
	subpd	%xmm3, %xmm5
	shufpd	$1, %xmm5, %xmm5        # xmm5 = xmm5[1,0]
	movapd	%xmm2, %xmm3
	subpd	%xmm4, %xmm3
	addpd	%xmm2, %xmm4
	movapd	%xmm4, %xmm7
	movsd	%xmm3, %xmm7            # xmm7 = xmm3[0],xmm7[1]
	movapd	%xmm13, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	addsd	%xmm2, %xmm0
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	subsd	%xmm13, %xmm2
	shufpd	$1, %xmm3, %xmm4        # xmm4 = xmm4[1],xmm3[0]
	movapd	%xmm7, %xmm3
	subpd	%xmm4, %xmm3
	addpd	%xmm7, %xmm4
	movsd	%xmm3, %xmm4            # xmm4 = xmm3[0],xmm4[1]
	movapd	%xmm0, %xmm3
	subsd	%xmm2, %xmm3
	mulsd	%xmm8, %xmm3
	addsd	%xmm0, %xmm2
	mulsd	%xmm8, %xmm2
	movlhps	%xmm8, %xmm8            # xmm8 = xmm8[0,0]
	mulpd	%xmm4, %xmm8
	movapd	%xmm6, %xmm0
	addpd	%xmm8, %xmm0
	movupd	%xmm0, 64(%rdi)
	subpd	%xmm8, %xmm6
	movupd	%xmm6, 80(%rdi)
	movapd	%xmm12, %xmm0
	subsd	%xmm2, %xmm0
	movsd	%xmm0, 96(%rdi)
	movapd	%xmm1, %xmm0
	addsd	%xmm3, %xmm0
	movsd	%xmm0, 104(%rdi)
	addsd	%xmm12, %xmm2
	movsd	%xmm2, 112(%rdi)
	subsd	%xmm3, %xmm1
	movsd	%xmm1, 120(%rdi)
	movapd	%xmm10, %xmm0
	addpd	%xmm9, %xmm0
	movupd	%xmm0, (%rdi)
	subpd	%xmm9, %xmm10
	movupd	%xmm10, 16(%rdi)
	movapd	%xmm11, %xmm0
	subpd	%xmm5, %xmm0
	addpd	%xmm11, %xmm5
	movapd	%xmm5, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movupd	%xmm1, 32(%rdi)
	movsd	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1]
	movupd	%xmm0, 48(%rdi)
	retq
.Lfunc_end22:
	.size	cftf081, .Lfunc_end22-cftf081
	.cfi_endproc

	.globl	bitrv208
	.p2align	4, 0x90
	.type	bitrv208,@function
bitrv208:                               # @bitrv208
	.cfi_startproc
# BB#0:
	movups	16(%rdi), %xmm0
	movups	48(%rdi), %xmm1
	movups	64(%rdi), %xmm2
	movups	96(%rdi), %xmm3
	movups	%xmm2, 16(%rdi)
	movups	%xmm3, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm1, 96(%rdi)
	retq
.Lfunc_end23:
	.size	bitrv208, .Lfunc_end23-bitrv208
	.cfi_endproc

	.globl	cftf040
	.p2align	4, 0x90
	.type	cftf040,@function
cftf040:                                # @cftf040
	.cfi_startproc
# BB#0:
	movupd	(%rdi), %xmm0
	movupd	16(%rdi), %xmm1
	movupd	32(%rdi), %xmm2
	movupd	48(%rdi), %xmm3
	movapd	%xmm0, %xmm4
	addpd	%xmm2, %xmm4
	subpd	%xmm2, %xmm0
	movapd	%xmm1, %xmm2
	addpd	%xmm3, %xmm2
	subpd	%xmm3, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movapd	%xmm4, %xmm3
	addpd	%xmm2, %xmm3
	movupd	%xmm3, (%rdi)
	subpd	%xmm2, %xmm4
	movupd	%xmm4, 32(%rdi)
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1]
	movupd	%xmm0, 16(%rdi)
	movsd	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1]
	movupd	%xmm2, 48(%rdi)
	retq
.Lfunc_end24:
	.size	cftf040, .Lfunc_end24-cftf040
	.cfi_endproc

	.globl	cftx020
	.p2align	4, 0x90
	.type	cftx020,@function
cftx020:                                # @cftx020
	.cfi_startproc
# BB#0:
	movupd	(%rdi), %xmm0
	movupd	16(%rdi), %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdi)
	movupd	%xmm2, 16(%rdi)
	retq
.Lfunc_end25:
	.size	cftx020, .Lfunc_end25-cftx020
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI26_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI26_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	cftb1st
	.p2align	4, 0x90
	.type	cftb1st,@function
cftb1st:                                # @cftb1st
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi203:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi204:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi205:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi206:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi207:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi208:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi209:
	.cfi_def_cfa_offset 192
.Lcfi210:
	.cfi_offset %rbx, -56
.Lcfi211:
	.cfi_offset %r12, -48
.Lcfi212:
	.cfi_offset %r13, -40
.Lcfi213:
	.cfi_offset %r14, -32
.Lcfi214:
	.cfi_offset %r15, -24
.Lcfi215:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	sarl	$3, %edi
	leal	(%rdi,%rdi), %r8d
	leal	(,%rdi,4), %eax
	leal	(%r8,%r8,2), %ecx
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movslq	%eax, %r9
	movsd	(%rsi,%r9,8), %xmm3     # xmm3 = mem[0],zero
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rsi), %xmm1          # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	addsd	%xmm3, %xmm2
	unpcklpd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0]
	xorpd	.LCPI26_0(%rip), %xmm1
	leal	1(,%rdi,4), %ebp
	movslq	%ebp, %rbp
	movsd	(%rsi,%rbp,8), %xmm4    # xmm4 = mem[0],zero
	subsd	%xmm4, %xmm1
	unpcklpd	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0]
	subpd	%xmm3, %xmm0
	movslq	%r8d, %rbp
	movsd	(%rsi,%rbp,8), %xmm3    # xmm3 = mem[0],zero
	movslq	%ecx, %rcx
	movsd	(%rsi,%rcx,8), %xmm4    # xmm4 = mem[0],zero
	leal	1(%rdi,%rdi), %ebx
	movslq	%ebx, %rbx
	movsd	(%rsi,%rbx,8), %xmm5    # xmm5 = mem[0],zero
	movl	%ecx, %eax
	orl	$1, %eax
	cltq
	movsd	(%rsi,%rax,8), %xmm6    # xmm6 = mem[0],zero
	movapd	%xmm5, %xmm7
	addsd	%xmm6, %xmm7
	unpcklpd	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0]
	addsd	%xmm4, %xmm3
	unpcklpd	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0]
	subpd	%xmm6, %xmm5
	movapd	%xmm2, %xmm4
	addsd	%xmm3, %xmm4
	movsd	%xmm4, (%rsi)
	movapd	%xmm1, %xmm4
	subsd	%xmm7, %xmm4
	movsd	%xmm4, 8(%rsi)
	subsd	%xmm3, %xmm2
	movsd	%xmm2, (%rsi,%rbp,8)
	addsd	%xmm1, %xmm7
	movsd	%xmm7, (%rsi,%rbx,8)
	movapd	%xmm0, %xmm1
	addpd	%xmm5, %xmm1
	movupd	%xmm1, (%rsi,%r9,8)
	subpd	%xmm5, %xmm0
	movupd	%xmm0, (%rsi,%rcx,8)
	movsd	8(%rdx), %xmm15         # xmm15 = mem[0],zero
	movsd	16(%rdx), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%rdx), %xmm12        # xmm12 = mem[0],zero
	leal	-2(%rdi), %eax
	movslq	%eax, %r13
	cmpl	$3, %r13d
	jl	.LBB26_1
# BB#2:                                 # %.lr.ph.preheader
	movapd	%xmm15, 80(%rsp)        # 16-byte Spill
	movq	%rbp, %rax
	shlq	$5, %rax
	leaq	-16(%rsi,%rax), %r10
	leaq	(%rbp,%rbp,2), %rax
	leaq	(%rsi,%rax,8), %r11
	movq	%rbp, %r15
	shlq	$4, %r15
	addq	%rsi, %r15
	leaq	(%rsi,%rbp,8), %rax
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	leal	-1(,%rdi,8), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leal	(%r8,%r8,2), %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	-1(%rcx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	-72(%rsp), %rcx         # 8-byte Reload
	leaq	-1(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%ebp, %r12d
	leaq	-1(%r12), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movsd	.LCPI26_1(%rip), %xmm1  # xmm1 = mem[0],zero
	xorpd	%xmm5, %xmm5
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	xorpd	%xmm2, %xmm2
	movapd	%xmm2, -64(%rsp)        # 16-byte Spill
	movapd	%xmm1, -96(%rsp)        # 16-byte Spill
	movapd	%xmm1, -32(%rsp)        # 16-byte Spill
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	movsd	%xmm0, -80(%rsp)        # 8-byte Spill
	.p2align	4, 0x90
.LBB26_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm5, -48(%rsp)        # 16-byte Spill
	movsd	16(%rsi,%rdi,8), %xmm2  # xmm2 = mem[0],zero
	movsd	16(%r15,%rdi,8), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm2, %xmm11
	addsd	%xmm0, %xmm11
	movsd	32(%rsi,%rdi,8), %xmm4  # xmm4 = mem[0],zero
	subsd	%xmm0, %xmm2
	movsd	%xmm2, -128(%rsp)       # 8-byte Spill
	movsd	32(%r15,%rdi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm4, %xmm8
	addsd	%xmm1, %xmm8
	movsd	40(%rsi,%rdi,8), %xmm0  # xmm0 = mem[0],zero
	unpcklpd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movapd	.LCPI26_0(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm2, %xmm0
	movapd	%xmm2, %xmm6
	movsd	40(%r15,%rdi,8), %xmm5  # xmm5 = mem[0],zero
	movsd	16(%rax,%rdi,8), %xmm2  # xmm2 = mem[0],zero
	unpcklpd	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0]
	movsd	16(%r11,%rdi,8), %xmm7  # xmm7 = mem[0],zero
	movapd	%xmm2, %xmm14
	addsd	%xmm7, %xmm14
	subsd	%xmm7, %xmm2
	movsd	%xmm2, 40(%rsp)         # 8-byte Spill
	subsd	%xmm5, %xmm0
	movsd	32(%rax,%rdi,8), %xmm5  # xmm5 = mem[0],zero
	movsd	32(%r11,%rdi,8), %xmm7  # xmm7 = mem[0],zero
	movsd	40(%rax,%rdi,8), %xmm9  # xmm9 = mem[0],zero
	subpd	%xmm1, %xmm4
	movsd	40(%r11,%rdi,8), %xmm2  # xmm2 = mem[0],zero
	movapd	%xmm9, %xmm1
	addsd	%xmm2, %xmm1
	unpcklpd	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0]
	addsd	%xmm7, %xmm5
	unpcklpd	%xmm7, %xmm2    # xmm2 = xmm2[0],xmm7[0]
	movsd	24(%rsi,%rdi,8), %xmm3  # xmm3 = mem[0],zero
	movq	-72(%rsp), %rcx         # 8-byte Reload
	leal	3(%rcx,%rdi), %ebp
	movapd	%xmm3, %xmm7
	xorpd	%xmm6, %xmm7
	movslq	%ebp, %rbx
	movsd	(%rsi,%rbx,8), %xmm10   # xmm10 = mem[0],zero
	subsd	%xmm10, %xmm7
	subsd	%xmm3, %xmm10
	leal	3(%r12,%rdi), %ebp
	movslq	%ebp, %rcx
	movq	24(%rsp), %rbp          # 8-byte Reload
	leal	3(%rbp,%rdi), %ebp
	movslq	%ebp, %rbp
	movsd	(%rsi,%rcx,8), %xmm13   # xmm13 = mem[0],zero
	movsd	(%rsi,%rbp,8), %xmm15   # xmm15 = mem[0],zero
	movapd	%xmm13, %xmm3
	addsd	%xmm15, %xmm3
	subsd	%xmm15, %xmm13
	subpd	%xmm2, %xmm9
	movapd	-32(%rsp), %xmm2        # 16-byte Reload
	movsd	%xmm2, -112(%rsp)       # 8-byte Spill
	movapd	-64(%rsp), %xmm2        # 16-byte Reload
	movsd	%xmm2, -104(%rsp)       # 8-byte Spill
	movapd	-96(%rsp), %xmm2        # 16-byte Reload
	movsd	%xmm2, -120(%rsp)       # 8-byte Spill
	movapd	%xmm11, %xmm2
	addsd	%xmm14, %xmm2
	movsd	32(%rdx,%rdi,8), %xmm12 # xmm12 = mem[0],zero
	movsd	40(%rdx,%rdi,8), %xmm6  # xmm6 = mem[0],zero
	movaps	%xmm6, -64(%rsp)        # 16-byte Spill
	movsd	48(%rdx,%rdi,8), %xmm6  # xmm6 = mem[0],zero
	movaps	%xmm6, -96(%rsp)        # 16-byte Spill
	movsd	56(%rdx,%rdi,8), %xmm15 # xmm15 = mem[0],zero
	movaps	%xmm15, 64(%rsp)        # 16-byte Spill
	movsd	%xmm2, 16(%rsi,%rdi,8)
	movapd	%xmm7, %xmm2
	subsd	%xmm3, %xmm2
	movsd	%xmm2, 24(%rsi,%rdi,8)
	movapd	%xmm8, %xmm2
	addsd	%xmm5, %xmm2
	movsd	%xmm2, 32(%rsi,%rdi,8)
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	movsd	%xmm2, 40(%rsi,%rdi,8)
	subsd	%xmm14, %xmm11
	addsd	%xmm7, %xmm3
	movsd	%xmm11, 16(%rax,%rdi,8)
	movsd	%xmm3, (%rsi,%rcx,8)
	subsd	%xmm5, %xmm8
	addsd	%xmm0, %xmm1
	movsd	-112(%rsp), %xmm3       # 8-byte Reload
                                        # xmm3 = mem[0],zero
	addsd	%xmm12, %xmm3
	movsd	-80(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm3
	movsd	-104(%rsp), %xmm5       # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movapd	-64(%rsp), %xmm7        # 16-byte Reload
	addsd	%xmm7, %xmm5
	mulsd	%xmm0, %xmm5
	movsd	%xmm8, 32(%rax,%rdi,8)
	movsd	%xmm1, 40(%rax,%rdi,8)
	movsd	-128(%rsp), %xmm11      # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movapd	%xmm11, %xmm0
	addsd	%xmm13, %xmm0
	movapd	%xmm10, %xmm1
	movsd	40(%rsp), %xmm14        # 8-byte Reload
                                        # xmm14 = mem[0],zero
	addsd	%xmm14, %xmm1
	movapd	%xmm3, %xmm2
	movapd	%xmm3, %xmm6
	movsd	%xmm6, -112(%rsp)       # 8-byte Spill
	mulsd	%xmm0, %xmm2
	movapd	%xmm5, %xmm3
	movsd	%xmm5, -104(%rsp)       # 8-byte Spill
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm2
	movsd	%xmm2, 16(%r15,%rdi,8)
	mulsd	%xmm6, %xmm1
	mulsd	%xmm5, %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi,%rbx,8)
	movapd	%xmm4, %xmm0
	movapd	%xmm12, -32(%rsp)       # 16-byte Spill
	addpd	%xmm9, %xmm0
	movlhps	%xmm12, %xmm12          # xmm12 = xmm12[0,0]
	movaps	%xmm12, 112(%rsp)       # 16-byte Spill
	mulpd	%xmm0, %xmm12
	movapd	%xmm7, %xmm2
	movapd	%xmm2, %xmm15
	movlhps	%xmm15, %xmm15          # xmm15 = xmm15[0,0]
	movaps	%xmm15, 96(%rsp)        # 16-byte Spill
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	mulpd	%xmm15, %xmm0
	movapd	%xmm12, %xmm2
	subpd	%xmm0, %xmm2
	addpd	%xmm12, %xmm0
	movsd	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1]
	movapd	%xmm11, %xmm7
	subsd	%xmm13, %xmm7
	movsd	-120(%rsp), %xmm2       # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	-96(%rsp), %xmm6        # 16-byte Reload
	addsd	%xmm6, %xmm2
	movapd	-48(%rsp), %xmm3        # 16-byte Reload
	movapd	64(%rsp), %xmm5         # 16-byte Reload
	subsd	%xmm5, %xmm3
	movapd	48(%rsp), %xmm1         # 16-byte Reload
	mulsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm11
	mulsd	%xmm1, %xmm11
	subsd	%xmm14, %xmm10
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm3
	movsd	%xmm3, -120(%rsp)       # 8-byte Spill
	mulsd	%xmm7, %xmm1
	movapd	%xmm11, %xmm2
	movsd	%xmm11, -48(%rsp)       # 8-byte Spill
	mulsd	%xmm10, %xmm2
	addsd	%xmm1, %xmm2
	mulsd	%xmm3, %xmm10
	mulsd	%xmm11, %xmm7
	movupd	%xmm0, 32(%r15,%rdi,8)
	subsd	%xmm7, %xmm10
	movapd	%xmm4, %xmm0
	subsd	%xmm9, %xmm0
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	movsd	%xmm2, 16(%r11,%rdi,8)
	movhlps	%xmm9, %xmm9            # xmm9 = xmm9[1,1]
	subsd	%xmm9, %xmm4
	movapd	%xmm6, %xmm2
	movapd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm10, (%rsi,%rbp,8)
	mulsd	%xmm4, %xmm2
	movapd	%xmm5, %xmm3
	mulsd	%xmm3, %xmm4
	subsd	%xmm4, %xmm1
	movsd	%xmm1, 32(%r11,%rdi,8)
	mulsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, 40(%r11,%rdi,8)
	movsd	-16(%rax,%r8,8), %xmm10 # xmm10 = mem[0],zero
	movsd	-16(%r11,%r8,8), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm10, %xmm15
	movq	(%rsp), %rcx            # 8-byte Reload
	leal	(%rcx,%r8), %ecx
	movslq	%ecx, %rbp
	addsd	%xmm0, %xmm15
	movsd	(%rsi,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm7
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%r8), %ecx
	movslq	%ecx, %r9
	movapd	.LCPI26_0(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm2, %xmm7
	movsd	(%rsi,%r9,8), %xmm8     # xmm8 = mem[0],zero
	subsd	%xmm8, %xmm7
	subsd	%xmm0, %xmm10
	subsd	%xmm1, %xmm8
	movsd	-32(%rax,%r8,8), %xmm3  # xmm3 = mem[0],zero
	movsd	-32(%r11,%r8,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm3, %xmm14
	addsd	%xmm1, %xmm14
	movsd	-24(%rax,%r8,8), %xmm13 # xmm13 = mem[0],zero
	unpcklpd	%xmm13, %xmm1   # xmm1 = xmm1[0],xmm13[0]
	xorpd	%xmm2, %xmm13
	movsd	-24(%r11,%r8,8), %xmm2  # xmm2 = mem[0],zero
	subsd	%xmm2, %xmm13
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	subpd	%xmm1, %xmm3
	movsd	-16(%r15,%r8,8), %xmm0  # xmm0 = mem[0],zero
	movsd	(%r10,%r8,8), %xmm1     # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm9
	addsd	%xmm1, %xmm9
	subsd	%xmm1, %xmm0
	movsd	%xmm0, -128(%rsp)       # 8-byte Spill
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	(%rcx,%r8), %ecx
	movslq	%ecx, %rbx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%r8), %ecx
	movslq	%ecx, %r14
	movapd	%xmm15, %xmm2
	addsd	%xmm9, %xmm2
	movsd	(%rsi,%rbx,8), %xmm6    # xmm6 = mem[0],zero
	movsd	(%rsi,%r14,8), %xmm1    # xmm1 = mem[0],zero
	movsd	-32(%r15,%r8,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-16(%r10,%r8,8), %xmm4  # xmm4 = mem[0],zero
	movsd	-24(%r15,%r8,8), %xmm12 # xmm12 = mem[0],zero
	movsd	-8(%r10,%r8,8), %xmm5   # xmm5 = mem[0],zero
	movsd	%xmm2, -16(%rax,%r8,8)
	movapd	%xmm6, %xmm2
	addsd	%xmm1, %xmm2
	movapd	%xmm7, %xmm11
	subsd	%xmm2, %xmm11
	movsd	%xmm11, (%rsi,%rbp,8)
	subsd	%xmm1, %xmm6
	movapd	%xmm12, %xmm1
	addsd	%xmm5, %xmm1
	unpcklpd	%xmm0, %xmm12   # xmm12 = xmm12[0],xmm0[0]
	addsd	%xmm4, %xmm0
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movapd	%xmm14, %xmm4
	addsd	%xmm0, %xmm4
	movsd	%xmm4, -32(%rax,%r8,8)
	movapd	%xmm13, %xmm4
	subsd	%xmm1, %xmm4
	movsd	%xmm4, -24(%rax,%r8,8)
	subsd	%xmm9, %xmm15
	movsd	%xmm15, -16(%r15,%r8,8)
	addsd	%xmm7, %xmm2
	movsd	%xmm2, (%rsi,%rbx,8)
	subsd	%xmm0, %xmm14
	movsd	%xmm14, -32(%r15,%r8,8)
	addsd	%xmm13, %xmm1
	movsd	%xmm1, -24(%r15,%r8,8)
	movapd	%xmm10, %xmm0
	addsd	%xmm6, %xmm0
	movapd	%xmm8, %xmm1
	movsd	-128(%rsp), %xmm9       # 8-byte Reload
                                        # xmm9 = mem[0],zero
	addsd	%xmm9, %xmm1
	movsd	-112(%rsp), %xmm7       # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movapd	%xmm7, %xmm2
	mulsd	%xmm1, %xmm2
	movsd	-104(%rsp), %xmm4       # 8-byte Reload
                                        # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm1
	mulsd	%xmm0, %xmm4
	subsd	%xmm2, %xmm4
	movsd	%xmm4, -16(%r11,%r8,8)
	mulsd	%xmm7, %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi,%r9,8)
	subpd	%xmm5, %xmm12
	movapd	%xmm3, %xmm0
	addpd	%xmm12, %xmm0
	movapd	96(%rsp), %xmm2         # 16-byte Reload
	mulpd	%xmm0, %xmm2
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	mulpd	112(%rsp), %xmm0        # 16-byte Folded Reload
	movapd	%xmm2, %xmm1
	subpd	%xmm0, %xmm1
	addpd	%xmm2, %xmm0
	movsd	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1]
	movupd	%xmm0, -32(%r11,%r8,8)
	subsd	%xmm6, %xmm10
	subsd	%xmm9, %xmm8
	movsd	-120(%rsp), %xmm2       # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm0
	mulsd	%xmm8, %xmm0
	movsd	-48(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm8
	mulsd	%xmm10, %xmm1
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%r10,%r8,8)
	mulsd	%xmm2, %xmm10
	subsd	%xmm10, %xmm8
	movapd	%xmm3, %xmm0
	subsd	%xmm12, %xmm0
	movsd	%xmm8, (%rsi,%r14,8)
	movapd	64(%rsp), %xmm4         # 16-byte Reload
	movapd	%xmm4, %xmm5
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	movhlps	%xmm12, %xmm12          # xmm12 = xmm12[1,1]
	subsd	%xmm12, %xmm3
	movapd	-96(%rsp), %xmm2        # 16-byte Reload
	movapd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm4
	mulsd	%xmm3, %xmm1
	subsd	%xmm4, %xmm1
	movsd	%xmm1, -16(%r10,%r8,8)
	xorpd	.LCPI26_0(%rip), %xmm5
	mulsd	%xmm5, %xmm3
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm3
	movsd	%xmm3, -8(%r10,%r8,8)
	leaq	4(%rdi), %rcx
	addq	$-4, %r8
	addq	$6, %rdi
	cmpq	%r13, %rdi
	movq	%rcx, %rdi
	jl	.LBB26_3
# BB#4:                                 # %._crit_edge.loopexit
	unpcklpd	-96(%rsp), %xmm5 # 16-byte Folded Reload
                                        # xmm5 = xmm5[0],mem[0]
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movapd	-64(%rsp), %xmm13       # 16-byte Reload
	movdqa	-32(%rsp), %xmm14       # 16-byte Reload
	movapd	80(%rsp), %xmm15        # 16-byte Reload
	movq	-8(%rsp), %rdi          # 8-byte Reload
	movsd	-80(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jmp	.LBB26_5
.LBB26_1:
	movq	.LCPI26_1(%rip), %xmm14 # xmm14 = mem[0],zero
	movdqa	%xmm14, %xmm5
	pslldq	$8, %xmm5               # xmm5 = zero,zero,zero,zero,zero,zero,zero,zero,xmm5[0,1,2,3,4,5,6,7]
	xorpd	%xmm13, %xmm13
.LBB26_5:                               # %._crit_edge
	addsd	%xmm15, %xmm14
	mulsd	%xmm0, %xmm14
	addsd	%xmm15, %xmm13
	mulsd	%xmm0, %xmm13
	movapd	%xmm15, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	subpd	%xmm0, %xmm5
	movlhps	%xmm12, %xmm12          # xmm12 = xmm12[0,0]
	mulpd	%xmm5, %xmm12
	leal	(%rdi,%rdi,2), %eax
	leal	(%rdi,%rdi,4), %ecx
	leal	(,%rdi,8), %ebp
	subl	%edi, %ebp
	movsd	(%rsi,%r13,8), %xmm2    # xmm2 = mem[0],zero
	movslq	%ecx, %rcx
	movsd	-16(%rsi,%rcx,8), %xmm1 # xmm1 = mem[0],zero
	movapd	%xmm2, %xmm3
	addsd	%xmm1, %xmm3
	movslq	%edi, %rdx
	movsd	-8(%rsi,%rdx,8), %xmm4  # xmm4 = mem[0],zero
	movapd	.LCPI26_0(%rip), %xmm8  # xmm8 = [-0.000000e+00,-0.000000e+00]
	unpcklpd	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0]
	xorpd	%xmm8, %xmm4
	movsd	-8(%rsi,%rcx,8), %xmm5  # xmm5 = mem[0],zero
	subsd	%xmm5, %xmm4
	unpcklpd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0]
	subpd	%xmm1, %xmm2
	movslq	%eax, %rdi
	movsd	-16(%rsi,%rdi,8), %xmm5 # xmm5 = mem[0],zero
	movslq	%ebp, %rax
	movsd	-16(%rsi,%rax,8), %xmm6 # xmm6 = mem[0],zero
	movsd	-8(%rsi,%rdi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%rsi,%rax,8), %xmm7  # xmm7 = mem[0],zero
	movapd	%xmm0, %xmm1
	addsd	%xmm7, %xmm1
	unpcklpd	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0]
	addsd	%xmm6, %xmm5
	unpcklpd	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0]
	subpd	%xmm7, %xmm0
	movapd	%xmm3, %xmm6
	addsd	%xmm5, %xmm6
	movsd	%xmm6, (%rsi,%r13,8)
	movapd	%xmm4, %xmm6
	subsd	%xmm1, %xmm6
	movsd	%xmm6, -8(%rsi,%rdx,8)
	subsd	%xmm5, %xmm3
	movsd	%xmm3, -16(%rsi,%rdi,8)
	addsd	%xmm4, %xmm1
	movsd	%xmm1, -8(%rsi,%rdi,8)
	movapd	%xmm2, %xmm1
	addpd	%xmm0, %xmm1
	movlhps	%xmm14, %xmm14          # xmm14 = xmm14[0,0]
	movaps	%xmm14, %xmm3
	mulpd	%xmm1, %xmm3
	movlhps	%xmm13, %xmm13          # xmm13 = xmm13[0,0]
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	mulpd	%xmm13, %xmm1
	movapd	%xmm3, %xmm4
	subpd	%xmm1, %xmm4
	addpd	%xmm3, %xmm1
	movsd	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1]
	movupd	%xmm1, -16(%rsi,%rcx,8)
	movapd	%xmm2, %xmm3
	subsd	%xmm0, %xmm3
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	subsd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm12, %xmm2
	movapd	%xmm12, %xmm9
	shufpd	$1, %xmm9, %xmm9        # xmm9 = xmm9[1,0]
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm9, %xmm3
	movapd	%xmm2, %xmm0
	addpd	%xmm3, %xmm0
	subpd	%xmm3, %xmm2
	movsd	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1]
	movupd	%xmm2, -16(%rsi,%rax,8)
	movsd	(%rsi,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	movsd	(%rsi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm2, %xmm6
	addsd	%xmm0, %xmm6
	movsd	8(%rsi,%rdx,8), %xmm4   # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm7
	xorpd	%xmm8, %xmm7
	movsd	8(%rsi,%rcx,8), %xmm3   # xmm3 = mem[0],zero
	subsd	%xmm3, %xmm7
	subsd	%xmm0, %xmm2
	subsd	%xmm4, %xmm3
	movsd	(%rsi,%rdi,8), %xmm11   # xmm11 = mem[0],zero
	movsd	(%rsi,%rax,8), %xmm10   # xmm10 = mem[0],zero
	movapd	%xmm11, %xmm4
	addsd	%xmm10, %xmm4
	movsd	8(%rsi,%rdi,8), %xmm1   # xmm1 = mem[0],zero
	movsd	8(%rsi,%rax,8), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm1, %xmm5
	addsd	%xmm0, %xmm5
	subsd	%xmm10, %xmm11
	subsd	%xmm0, %xmm1
	movapd	%xmm6, %xmm0
	addsd	%xmm4, %xmm0
	movsd	%xmm0, (%rsi,%rdx,8)
	movapd	%xmm7, %xmm0
	subsd	%xmm5, %xmm0
	movsd	%xmm0, 8(%rsi,%rdx,8)
	subsd	%xmm4, %xmm6
	movsd	%xmm6, (%rsi,%rdi,8)
	addsd	%xmm7, %xmm5
	movsd	%xmm5, 8(%rsi,%rdi,8)
	movapd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movapd	%xmm3, %xmm4
	addsd	%xmm11, %xmm4
	movapd	%xmm0, %xmm5
	subsd	%xmm4, %xmm5
	mulsd	%xmm15, %xmm5
	movsd	%xmm5, (%rsi,%rcx,8)
	addsd	%xmm0, %xmm4
	mulsd	%xmm15, %xmm4
	movsd	%xmm4, 8(%rsi,%rcx,8)
	subsd	%xmm1, %xmm2
	subsd	%xmm11, %xmm3
	xorpd	%xmm8, %xmm15
	movapd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	mulsd	%xmm15, %xmm0
	movsd	%xmm0, (%rsi,%rax,8)
	subsd	%xmm2, %xmm3
	mulsd	%xmm15, %xmm3
	movsd	%xmm3, 8(%rsi,%rax,8)
	movsd	16(%rsi,%rdx,8), %xmm2  # xmm2 = mem[0],zero
	movsd	16(%rsi,%rcx,8), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm2, %xmm4
	addsd	%xmm0, %xmm4
	movsd	24(%rsi,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	xorpd	%xmm1, %xmm8
	movsd	24(%rsi,%rcx,8), %xmm3  # xmm3 = mem[0],zero
	subsd	%xmm3, %xmm8
	unpcklpd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0]
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	subpd	%xmm0, %xmm2
	movsd	16(%rsi,%rdi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	16(%rsi,%rax,8), %xmm1  # xmm1 = mem[0],zero
	movsd	24(%rsi,%rdi,8), %xmm3  # xmm3 = mem[0],zero
	movsd	24(%rsi,%rax,8), %xmm5  # xmm5 = mem[0],zero
	movapd	%xmm3, %xmm6
	addsd	%xmm5, %xmm6
	unpcklpd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0]
	addsd	%xmm1, %xmm0
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	subpd	%xmm5, %xmm3
	movapd	%xmm4, %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rsi,%rdx,8)
	movapd	%xmm8, %xmm1
	subsd	%xmm6, %xmm1
	movsd	%xmm1, 24(%rsi,%rdx,8)
	subsd	%xmm0, %xmm4
	movsd	%xmm4, 16(%rsi,%rdi,8)
	addsd	%xmm8, %xmm6
	movsd	%xmm6, 24(%rsi,%rdi,8)
	movapd	%xmm2, %xmm0
	addpd	%xmm3, %xmm0
	mulpd	%xmm0, %xmm13
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	mulpd	%xmm14, %xmm0
	movapd	%xmm13, %xmm1
	subpd	%xmm0, %xmm1
	addpd	%xmm13, %xmm0
	movsd	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1]
	movupd	%xmm0, 16(%rsi,%rcx,8)
	movapd	%xmm2, %xmm0
	subsd	%xmm3, %xmm0
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	subsd	%xmm3, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm9, %xmm2
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm12, %xmm0
	movapd	%xmm2, %xmm1
	addpd	%xmm0, %xmm1
	subpd	%xmm0, %xmm2
	movsd	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1]
	movupd	%xmm2, 16(%rsi,%rax,8)
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	cftb1st, .Lfunc_end26-cftb1st
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI27_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	bitrv2conj
	.p2align	4, 0x90
	.type	bitrv2conj,@function
bitrv2conj:                             # @bitrv2conj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi216:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi217:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi218:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi219:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi220:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi221:
	.cfi_def_cfa_offset 56
.Lcfi222:
	.cfi_offset %rbx, -56
.Lcfi223:
	.cfi_offset %r12, -48
.Lcfi224:
	.cfi_offset %r13, -40
.Lcfi225:
	.cfi_offset %r14, -32
.Lcfi226:
	.cfi_offset %r15, -24
.Lcfi227:
	.cfi_offset %rbp, -16
	movl	$0, (%rsi)
	cmpl	$9, %edi
	jl	.LBB27_20
# BB#1:                                 # %.lr.ph275.preheader
	leaq	16(%rsi), %r8
	leaq	12(%rsi), %r9
	movl	$1, %r10d
	.p2align	4, 0x90
.LBB27_2:                               # %.lr.ph275
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_11 Depth 2
                                        #     Child Loop BB27_15 Depth 2
                                        #     Child Loop BB27_18 Depth 2
	sarl	%edi
	testl	%r10d, %r10d
	jle	.LBB27_19
# BB#3:                                 # %.lr.ph269.preheader
                                        #   in Loop: Header=BB27_2 Depth=1
	movslq	%r10d, %r14
	movl	%r10d, %ebp
	cmpl	$8, %r10d
	jae	.LBB27_5
# BB#4:                                 #   in Loop: Header=BB27_2 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB27_13
	.p2align	4, 0x90
.LBB27_5:                               # %min.iters.checked
                                        #   in Loop: Header=BB27_2 Depth=1
	movl	%r10d, %r11d
	andl	$7, %r11d
	movq	%rbp, %rcx
	subq	%r11, %rcx
	je	.LBB27_9
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB27_2 Depth=1
	leaq	(%rsi,%r14,4), %rax
	leaq	(%rsi,%rbp,4), %rbx
	cmpq	%rbx, %rax
	jae	.LBB27_10
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB27_2 Depth=1
	leaq	(%r14,%rbp), %rax
	leaq	(%rsi,%rax,4), %rax
	cmpq	%rsi, %rax
	jbe	.LBB27_10
.LBB27_9:                               #   in Loop: Header=BB27_2 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB27_13
.LBB27_10:                              # %vector.ph
                                        #   in Loop: Header=BB27_2 Depth=1
	movd	%edi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	%rcx, %rax
	movq	%r8, %rbx
	.p2align	4, 0x90
.LBB27_11:                              # %vector.body
                                        #   Parent Loop BB27_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rbx), %xmm1
	movdqu	(%rbx), %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm2
	movdqu	%xmm1, -16(%rbx,%r14,4)
	movdqu	%xmm2, (%rbx,%r14,4)
	addq	$32, %rbx
	addq	$-8, %rax
	jne	.LBB27_11
# BB#12:                                # %middle.block
                                        #   in Loop: Header=BB27_2 Depth=1
	testl	%r11d, %r11d
	je	.LBB27_19
	.p2align	4, 0x90
.LBB27_13:                              # %.lr.ph269.preheader333
                                        #   in Loop: Header=BB27_2 Depth=1
	movl	%ebp, %ebx
	subl	%ecx, %ebx
	leaq	-1(%rbp), %r11
	subq	%rcx, %r11
	andq	$3, %rbx
	je	.LBB27_16
# BB#14:                                # %.lr.ph269.prol.preheader
                                        #   in Loop: Header=BB27_2 Depth=1
	leaq	(%rsi,%r14,4), %r15
	negq	%rbx
	.p2align	4, 0x90
.LBB27_15:                              # %.lr.ph269.prol
                                        #   Parent Loop BB27_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi,%rcx,4), %eax
	addl	%edi, %eax
	movl	%eax, (%r15,%rcx,4)
	incq	%rcx
	incq	%rbx
	jne	.LBB27_15
.LBB27_16:                              # %.lr.ph269.prol.loopexit
                                        #   in Loop: Header=BB27_2 Depth=1
	cmpq	$3, %r11
	jb	.LBB27_19
# BB#17:                                # %.lr.ph269.preheader333.new
                                        #   in Loop: Header=BB27_2 Depth=1
	subq	%rcx, %rbp
	addq	%rcx, %r14
	leaq	(%r9,%r14,4), %rax
	leaq	(%r9,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB27_18:                              # %.lr.ph269
                                        #   Parent Loop BB27_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rcx), %ebx
	addl	%edi, %ebx
	movl	%ebx, -12(%rax)
	movl	-8(%rcx), %ebx
	addl	%edi, %ebx
	movl	%ebx, -8(%rax)
	movl	-4(%rcx), %ebx
	addl	%edi, %ebx
	movl	%ebx, -4(%rax)
	movl	(%rcx), %ebx
	addl	%edi, %ebx
	movl	%ebx, (%rax)
	addq	$16, %rax
	addq	$16, %rcx
	addq	$-4, %rbp
	jne	.LBB27_18
.LBB27_19:                              # %._crit_edge270
                                        #   in Loop: Header=BB27_2 Depth=1
	leal	(%r10,%r10), %r12d
	movl	%r10d, %eax
	shll	$4, %eax
	cmpl	%edi, %eax
	movl	%r12d, %r10d
	jl	.LBB27_2
	jmp	.LBB27_21
.LBB27_20:
	movl	$8, %eax
	movl	$1, %r12d
.LBB27_21:                              # %._crit_edge276
	leal	(%r12,%r12), %r10d
	cmpl	%edi, %eax
	jne	.LBB27_29
# BB#22:                                # %.preheader258
	testl	%r12d, %r12d
	jle	.LBB27_34
# BB#23:                                # %.preheader.lr.ph
	movl	%r10d, %eax
	orl	$1, %eax
	movl	%eax, -24(%rsp)         # 4-byte Spill
	movslq	%r10d, %r14
	movl	%r12d, %eax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	shll	$2, %r12d
	leaq	(,%r14,8), %r13
	xorl	%r11d, %r11d
	movdqa	.LCPI27_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB27_24:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_26 Depth 2
	testq	%r11, %r11
	movslq	(%rsi,%r11,4), %rax
	leal	(%r11,%r11), %r15d
	jle	.LBB27_28
# BB#25:                                # %.lr.ph
                                        #   in Loop: Header=BB27_24 Depth=1
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rax,8), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB27_26:                              #   Parent Loop BB27_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rsi,%rbp,4), %rcx
	movslq	%r15d, %rax
	addq	%rcx, %rax
	movq	(%rbx), %rcx
	movq	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	(%rdx,%rax,8), %r9
	movslq	%eax, %r8
	movq	8(%rdx,%r8,8), %xmm2    # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%r9, (%rbx)
	movq	%xmm2, 8(%rbx)
	movq	%rcx, (%rdx,%rax,8)
	movq	%xmm1, 8(%rdx,%r8,8)
	addl	%r12d, %r8d
	leaq	(%rbx,%r13), %rax
	movq	(%rbx,%r14,8), %r9
	movq	8(%rbx,%r14,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movslq	%r8d, %rcx
	movq	(%rdx,%rcx,8), %rdi
	movq	8(%rdx,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rdi, (%rbx,%r14,8)
	movq	%xmm2, 8(%rbx,%r14,8)
	movq	%r9, (%rdx,%rcx,8)
	movq	%xmm1, 8(%rdx,%rcx,8)
	subl	%r10d, %ecx
	movq	(%rax,%r14,8), %r8
	movq	8(%rax,%r14,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movslq	%ecx, %rcx
	movq	(%rdx,%rcx,8), %rdi
	movq	8(%rdx,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rdi, (%rax,%r14,8)
	movq	%xmm2, 8(%rax,%r14,8)
	leaq	(%rax,%r13), %rax
	movq	%r8, (%rdx,%rcx,8)
	movq	%xmm1, 8(%rdx,%rcx,8)
	addl	%r12d, %ecx
	movq	(%rax,%r14,8), %r8
	movq	8(%rax,%r14,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movslq	%ecx, %rcx
	movq	(%rdx,%rcx,8), %rdi
	movq	8(%rdx,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rdi, (%rax,%r14,8)
	movq	%xmm2, 8(%rax,%r14,8)
	movq	%r8, (%rdx,%rcx,8)
	movq	%xmm1, 8(%rdx,%rcx,8)
	incq	%rbp
	addq	$16, %rbx
	cmpq	%rbp, %r11
	jne	.LBB27_26
# BB#27:                                #   in Loop: Header=BB27_24 Depth=1
	movq	-16(%rsp), %rax         # 8-byte Reload
.LBB27_28:                              # %._crit_edge
                                        #   in Loop: Header=BB27_24 Depth=1
	addl	%r15d, %eax
	cltq
	movq	8(%rdx,%rax,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	%xmm1, 8(%rdx,%rax,8)
	addl	%r10d, %eax
	movslq	%eax, %rcx
	addl	%r10d, %eax
	movq	(%rdx,%rcx,8), %rdi
	movq	8(%rdx,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	cltq
	movq	(%rdx,%rax,8), %rbp
	movq	8(%rdx,%rax,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rbp, (%rdx,%rcx,8)
	movq	%xmm2, 8(%rdx,%rcx,8)
	movq	%rdi, (%rdx,%rax,8)
	movq	%xmm1, 8(%rdx,%rax,8)
	addl	-24(%rsp), %eax         # 4-byte Folded Reload
	cltq
	movq	(%rdx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	%xmm1, (%rdx,%rax,8)
	incq	%r11
	cmpq	-32(%rsp), %r11         # 8-byte Folded Reload
	jne	.LBB27_24
	jmp	.LBB27_34
.LBB27_29:
	movq	8(%rdx), %xmm1          # xmm1 = mem[0],zero
	movdqa	.LCPI27_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm1
	movq	%xmm1, 8(%rdx)
	movl	%r10d, %eax
	orl	$1, %eax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	cltq
	movq	(%rdx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	%xmm1, (%rdx,%rax,8)
	cmpl	$2, %r12d
	jl	.LBB27_34
# BB#30:                                # %.preheader259.lr.ph
	movslq	%r10d, %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movl	%r12d, %eax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leaq	8(%rdx), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB27_31:                              # %.lr.ph264
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_32 Depth 2
	movslq	(%rsi,%r12,4), %r15
	leal	(%r12,%r12), %r13d
	movq	-24(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r15), %rax
	movq	-8(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	leaq	(%rcx,%r15,8), %rbp
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB27_32:                              #   Parent Loop BB27_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rsi,%rdi,4), %rcx
	movslq	%r13d, %r14
	addq	%r14, %rcx
	movq	-8(%rbp), %r8
	movq	(%rbp), %xmm1           # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	(%rdx,%rcx,8), %r9
	movslq	%ecx, %r11
	movq	8(%rdx,%r11,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%r9, -8(%rbp)
	movq	%xmm2, (%rbp)
	movq	%r8, (%rdx,%rcx,8)
	movq	%xmm1, 8(%rdx,%r11,8)
	addl	%r10d, %r11d
	movq	-8(%rax), %r8
	movq	(%rax), %xmm1           # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movslq	%r11d, %rcx
	movq	(%rdx,%rcx,8), %rbx
	movq	8(%rdx,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rbx, -8(%rax)
	movq	%xmm2, (%rax)
	movq	%r8, (%rdx,%rcx,8)
	movq	%xmm1, 8(%rdx,%rcx,8)
	incq	%rdi
	addq	$16, %rax
	addq	$16, %rbp
	cmpq	%rdi, %r12
	jne	.LBB27_32
# BB#33:                                # %._crit_edge265
                                        #   in Loop: Header=BB27_31 Depth=1
	addq	%r14, %r15
	movq	8(%rdx,%r15,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	%xmm1, 8(%rdx,%r15,8)
	movq	-16(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	cltq
	movq	(%rdx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	%xmm1, (%rdx,%rax,8)
	incq	%r12
	cmpq	-32(%rsp), %r12         # 8-byte Folded Reload
	jne	.LBB27_31
.LBB27_34:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	bitrv2conj, .Lfunc_end27-bitrv2conj
	.cfi_endproc

	.globl	bitrv216neg
	.p2align	4, 0x90
	.type	bitrv216neg,@function
bitrv216neg:                            # @bitrv216neg
	.cfi_startproc
# BB#0:
	movups	16(%rdi), %xmm10
	movups	32(%rdi), %xmm8
	movups	48(%rdi), %xmm13
	movups	64(%rdi), %xmm9
	movups	80(%rdi), %xmm4
	movups	96(%rdi), %xmm12
	movups	112(%rdi), %xmm6
	movups	128(%rdi), %xmm11
	movups	144(%rdi), %xmm1
	movups	160(%rdi), %xmm14
	movups	176(%rdi), %xmm0
	movups	192(%rdi), %xmm7
	movups	208(%rdi), %xmm5
	movups	224(%rdi), %xmm2
	movups	240(%rdi), %xmm3
	movups	%xmm3, 16(%rdi)
	movups	%xmm6, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm13, 64(%rdi)
	movups	%xmm5, 80(%rdi)
	movups	%xmm4, 96(%rdi)
	movups	%xmm1, 112(%rdi)
	movups	%xmm10, 128(%rdi)
	movups	%xmm2, 144(%rdi)
	movups	%xmm12, 160(%rdi)
	movups	%xmm14, 176(%rdi)
	movups	%xmm8, 192(%rdi)
	movups	%xmm7, 208(%rdi)
	movups	%xmm9, 224(%rdi)
	movups	%xmm11, 240(%rdi)
	retq
.Lfunc_end28:
	.size	bitrv216neg, .Lfunc_end28-bitrv216neg
	.cfi_endproc

	.globl	bitrv208neg
	.p2align	4, 0x90
	.type	bitrv208neg,@function
bitrv208neg:                            # @bitrv208neg
	.cfi_startproc
# BB#0:
	movups	16(%rdi), %xmm0
	movups	32(%rdi), %xmm1
	movups	48(%rdi), %xmm2
	movups	64(%rdi), %xmm3
	movups	80(%rdi), %xmm4
	movups	96(%rdi), %xmm5
	movups	112(%rdi), %xmm6
	movups	%xmm6, 16(%rdi)
	movups	%xmm2, 32(%rdi)
	movups	%xmm4, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm5, 80(%rdi)
	movups	%xmm1, 96(%rdi)
	movups	%xmm3, 112(%rdi)
	retq
.Lfunc_end29:
	.size	bitrv208neg, .Lfunc_end29-bitrv208neg
	.cfi_endproc

	.globl	cftb040
	.p2align	4, 0x90
	.type	cftb040,@function
cftb040:                                # @cftb040
	.cfi_startproc
# BB#0:
	movupd	(%rdi), %xmm0
	movupd	16(%rdi), %xmm1
	movupd	32(%rdi), %xmm2
	movupd	48(%rdi), %xmm3
	movapd	%xmm0, %xmm4
	addpd	%xmm2, %xmm4
	subpd	%xmm2, %xmm0
	movapd	%xmm1, %xmm2
	addpd	%xmm3, %xmm2
	subpd	%xmm3, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movapd	%xmm4, %xmm3
	addpd	%xmm2, %xmm3
	movupd	%xmm3, (%rdi)
	subpd	%xmm2, %xmm4
	movupd	%xmm4, 32(%rdi)
	movapd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	subpd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	movupd	%xmm1, 16(%rdi)
	movsd	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1]
	movupd	%xmm2, 48(%rdi)
	retq
.Lfunc_end30:
	.size	cftb040, .Lfunc_end30-cftb040
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI31_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	cftmdl1
	.p2align	4, 0x90
	.type	cftmdl1,@function
cftmdl1:                                # @cftmdl1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi228:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi229:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi230:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi231:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi232:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi233:
	.cfi_def_cfa_offset 56
.Lcfi234:
	.cfi_offset %rbx, -56
.Lcfi235:
	.cfi_offset %r12, -48
.Lcfi236:
	.cfi_offset %r13, -40
.Lcfi237:
	.cfi_offset %r14, -32
.Lcfi238:
	.cfi_offset %r15, -24
.Lcfi239:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	sarl	$3, %edi
	leal	(%rdi,%rdi), %ebx
	leal	(,%rdi,4), %ecx
	leal	(%rbx,%rbx,2), %eax
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	movslq	%ecx, %rcx
	movupd	(%rsi), %xmm0
	movupd	(%rsi,%rcx,8), %xmm1
	movapd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	subpd	%xmm1, %xmm0
	movslq	%ebx, %rbp
	cltq
	movupd	(%rsi,%rbp,8), %xmm1
	movupd	(%rsi,%rax,8), %xmm3
	movapd	%xmm1, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movapd	%xmm2, %xmm3
	addpd	%xmm4, %xmm3
	movupd	%xmm3, (%rsi)
	subpd	%xmm4, %xmm2
	movupd	%xmm2, (%rsi,%rbp,8)
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1]
	movupd	%xmm0, (%rsi,%rcx,8)
	movsd	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1]
	movupd	%xmm2, (%rsi,%rax,8)
	movsd	8(%rdx), %xmm0          # xmm0 = mem[0],zero
	movapd	%xmm0, -24(%rsp)        # 16-byte Spill
	movq	%rdi, -88(%rsp)         # 8-byte Spill
	movslq	%edi, %r9
	cmpl	$3, %r9d
	jl	.LBB31_3
# BB#1:                                 # %.lr.ph.preheader
	addq	$56, %rdx
	movq	%rbp, %rax
	shlq	$5, %rax
	leaq	-16(%rsi,%rax), %r10
	leaq	(%rbp,%rbp,2), %rax
	leaq	(%rsi,%rax,8), %r11
	movq	%rbp, %r15
	shlq	$4, %r15
	addq	%rsi, %r15
	leaq	(%rsi,%rbp,8), %r13
	movq	-88(%rsp), %rax         # 8-byte Reload
	leal	-1(,%rax,8), %eax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	leal	(%rbx,%rbx,2), %eax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	leaq	-1(%rax), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	-80(%rsp), %rax         # 8-byte Reload
	leaq	-1(%rax), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movl	%ebp, %r12d
	leaq	-1(%r12), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movl	$2, %r8d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB31_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	-24(%rdx), %xmm11       # xmm11 = mem[0],zero
	movsd	-16(%rdx), %xmm12       # xmm12 = mem[0],zero
	movsd	-8(%rdx), %xmm8         # xmm8 = mem[0],zero
	movsd	(%rdx), %xmm13          # xmm13 = mem[0],zero
	movsd	(%r15,%r8,8), %xmm2     # xmm2 = mem[0],zero
	movupd	(%rsi,%r8,8), %xmm6
	movq	-80(%rsp), %rax         # 8-byte Reload
	leal	1(%rax,%r8), %ecx
	movslq	%ecx, %r14
	movsd	(%rsi,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm6, %xmm7
	subsd	%xmm2, %xmm7
	unpcklpd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	addpd	%xmm6, %xmm2
	movhlps	%xmm6, %xmm6            # xmm6 = xmm6[1,1]
	subsd	%xmm0, %xmm6
	movsd	(%r13,%r8,8), %xmm1     # xmm1 = mem[0],zero
	movsd	(%r11,%r8,8), %xmm3     # xmm3 = mem[0],zero
	leal	1(%r12,%r8), %ecx
	movslq	%ecx, %rcx
	movsd	(%rsi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	-48(%rsp), %rax         # 8-byte Reload
	leal	1(%rax,%r8), %ebp
	movq	%r9, %rax
	movslq	%ebp, %r9
	movsd	(%rsi,%r9,8), %xmm4     # xmm4 = mem[0],zero
	movapd	%xmm1, %xmm5
	unpcklpd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0]
	subsd	%xmm3, %xmm1
	unpcklpd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0]
	addpd	%xmm5, %xmm3
	subsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm4
	addpd	%xmm3, %xmm4
	movupd	%xmm4, (%rsi,%r8,8)
	movapd	%xmm2, %xmm4
	subsd	%xmm3, %xmm4
	movsd	%xmm4, (%r13,%r8,8)
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	subsd	%xmm3, %xmm2
	movsd	%xmm2, (%rsi,%rcx,8)
	movapd	%xmm7, %xmm2
	subsd	%xmm0, %xmm2
	movapd	%xmm6, %xmm3
	addsd	%xmm1, %xmm3
	movapd	%xmm11, %xmm4
	mulsd	%xmm2, %xmm4
	movapd	%xmm12, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%r15,%r8,8)
	mulsd	%xmm11, %xmm3
	mulsd	%xmm12, %xmm2
	addsd	%xmm3, %xmm2
	movsd	%xmm2, (%rsi,%r14,8)
	addsd	%xmm7, %xmm0
	subsd	%xmm1, %xmm6
	movsd	%xmm8, -32(%rsp)        # 8-byte Spill
	movapd	%xmm8, %xmm1
	mulsd	%xmm0, %xmm1
	movapd	%xmm8, %xmm2
	mulsd	%xmm6, %xmm2
	mulsd	%xmm13, %xmm6
	subsd	%xmm6, %xmm1
	movsd	%xmm1, (%r11,%r8,8)
	mulsd	%xmm13, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rsi,%r9,8)
	movq	%rax, %r9
	movsd	-16(%r13,%rdi,8), %xmm9 # xmm9 = mem[0],zero
	movsd	-16(%r11,%rdi,8), %xmm14 # xmm14 = mem[0],zero
	movapd	%xmm9, %xmm4
	addsd	%xmm14, %xmm4
	movq	-72(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rdi), %ecx
	movslq	%ecx, %rcx
	movsd	(%rsi,%rcx,8), %xmm6    # xmm6 = mem[0],zero
	movq	-56(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rdi), %ebp
	movslq	%ebp, %rbp
	movsd	(%rsi,%rbp,8), %xmm15   # xmm15 = mem[0],zero
	movapd	%xmm6, %xmm1
	addsd	%xmm15, %xmm1
	movsd	-16(%r15,%rdi,8), %xmm2 # xmm2 = mem[0],zero
	movsd	(%r10,%rdi,8), %xmm10   # xmm10 = mem[0],zero
	movapd	%xmm2, %xmm5
	addsd	%xmm10, %xmm5
	movq	-64(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rdi), %ebx
	movslq	%ebx, %rax
	movsd	(%rsi,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movq	-40(%rsp), %rbx         # 8-byte Reload
	leal	(%rbx,%rdi), %ebx
	movslq	%ebx, %rbx
	movsd	(%rsi,%rbx,8), %xmm8    # xmm8 = mem[0],zero
	movapd	%xmm4, %xmm7
	addsd	%xmm5, %xmm7
	movsd	%xmm7, -16(%r13,%rdi,8)
	movapd	%xmm0, %xmm7
	addsd	%xmm8, %xmm7
	movapd	%xmm1, %xmm3
	addsd	%xmm7, %xmm3
	movsd	%xmm3, (%rsi,%rcx,8)
	subsd	%xmm5, %xmm4
	movsd	%xmm4, -16(%r15,%rdi,8)
	subsd	%xmm7, %xmm1
	movsd	%xmm1, (%rsi,%rax,8)
	subsd	%xmm14, %xmm9
	subsd	%xmm15, %xmm6
	subsd	%xmm10, %xmm2
	subsd	%xmm8, %xmm0
	movapd	%xmm9, %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm6, %xmm3
	addsd	%xmm2, %xmm3
	movapd	%xmm11, %xmm4
	mulsd	%xmm3, %xmm4
	mulsd	%xmm12, %xmm3
	mulsd	%xmm1, %xmm12
	subsd	%xmm4, %xmm12
	movsd	%xmm12, -16(%r11,%rdi,8)
	mulsd	%xmm11, %xmm1
	addsd	%xmm3, %xmm1
	movsd	%xmm1, (%rsi,%rbp,8)
	movapd	%xmm13, %xmm1
	xorpd	.LCPI31_0(%rip), %xmm1
	addsd	%xmm9, %xmm0
	subsd	%xmm2, %xmm6
	mulsd	%xmm0, %xmm13
	movsd	-32(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm0
	mulsd	%xmm6, %xmm2
	subsd	%xmm13, %xmm2
	movsd	%xmm2, (%r10,%rdi,8)
	mulsd	%xmm1, %xmm6
	subsd	%xmm0, %xmm6
	movsd	%xmm6, (%rsi,%rbx,8)
	addq	$2, %r8
	addq	$32, %rdx
	addq	$-2, %rdi
	cmpq	%r9, %r8
	jl	.LBB31_2
.LBB31_3:                               # %._crit_edge
	movq	-88(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%rdi,2), %eax
	leal	(%rdi,%rdi,4), %ecx
	leal	(,%rdi,8), %edx
	subl	%edi, %edx
	movslq	%ecx, %rcx
	movupd	(%rsi,%r9,8), %xmm0
	movupd	(%rsi,%rcx,8), %xmm1
	movapd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	subpd	%xmm1, %xmm0
	movslq	%eax, %rdi
	movslq	%edx, %rax
	movupd	(%rsi,%rdi,8), %xmm1
	movupd	(%rsi,%rax,8), %xmm3
	movapd	%xmm1, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm1
	movapd	%xmm1, %xmm3
	shufpd	$1, %xmm3, %xmm3        # xmm3 = xmm3[1,0]
	movapd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, (%rsi,%r9,8)
	subpd	%xmm4, %xmm2
	movupd	%xmm2, (%rsi,%rdi,8)
	movapd	%xmm0, %xmm2
	subpd	%xmm3, %xmm2
	addpd	%xmm0, %xmm3
	movapd	%xmm3, %xmm4
	movsd	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1]
	shufpd	$1, %xmm2, %xmm3        # xmm3 = xmm3[1],xmm2[0]
	movapd	%xmm4, %xmm2
	subpd	%xmm3, %xmm2
	addpd	%xmm4, %xmm3
	movsd	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1]
	movaps	-24(%rsp), %xmm4        # 16-byte Reload
	movaps	%xmm4, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm3, %xmm2
	movupd	%xmm2, (%rsi,%rcx,8)
	movapd	%xmm1, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	addsd	%xmm0, %xmm2
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	subsd	%xmm1, %xmm0
	xorps	.LCPI31_0(%rip), %xmm4
	movapd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	movsd	%xmm1, (%rsi,%rax,8)
	subsd	%xmm2, %xmm0
	mulsd	%xmm4, %xmm0
	movsd	%xmm0, 8(%rsi,%rax,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	cftmdl1, .Lfunc_end31-cftmdl1
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI32_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	cftmdl2
	.p2align	4, 0x90
	.type	cftmdl2,@function
cftmdl2:                                # @cftmdl2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi240:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi241:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi242:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi243:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi244:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi245:
	.cfi_def_cfa_offset 56
.Lcfi246:
	.cfi_offset %rbx, -56
.Lcfi247:
	.cfi_offset %r12, -48
.Lcfi248:
	.cfi_offset %r13, -40
.Lcfi249:
	.cfi_offset %r14, -32
.Lcfi250:
	.cfi_offset %r15, -24
.Lcfi251:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	sarl	$3, %edi
	leal	(%rdi,%rdi), %r8d
	movsd	8(%rdx), %xmm8          # xmm8 = mem[0],zero
	leal	(,%rdi,4), %ecx
	leal	(%r8,%r8,2), %eax
	leal	1(,%rdi,4), %ebp
	movslq	%ebp, %r9
	movsd	(%rsi,%r9,8), %xmm5     # xmm5 = mem[0],zero
	movsd	(%rsi), %xmm9           # xmm9 = mem[0],zero
	movsd	8(%rsi), %xmm0          # xmm0 = mem[0],zero
	movapd	%xmm9, %xmm3
	subsd	%xmm5, %xmm3
	movslq	%ecx, %r14
	movsd	(%rsi,%r14,8), %xmm6    # xmm6 = mem[0],zero
	movapd	%xmm0, %xmm4
	addsd	%xmm6, %xmm4
	addsd	%xmm5, %xmm9
	subsd	%xmm6, %xmm0
	movslq	%r8d, %rbp
	movsd	(%rsi,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	movslq	%eax, %rbx
	orl	$1, %eax
	movslq	%eax, %rcx
	movsd	(%rsi,%rcx,8), %xmm10   # xmm10 = mem[0],zero
	movapd	%xmm1, %xmm6
	subsd	%xmm10, %xmm6
	leal	1(%rdi,%rdi), %eax
	cltq
	movsd	(%rsi,%rax,8), %xmm7    # xmm7 = mem[0],zero
	movsd	(%rsi,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	movapd	%xmm7, %xmm5
	addsd	%xmm2, %xmm5
	addsd	%xmm10, %xmm1
	subsd	%xmm2, %xmm7
	movapd	%xmm6, %xmm2
	subsd	%xmm5, %xmm2
	mulsd	%xmm8, %xmm2
	addsd	%xmm6, %xmm5
	mulsd	%xmm8, %xmm5
	movapd	%xmm3, %xmm6
	addsd	%xmm2, %xmm6
	movsd	%xmm6, (%rsi)
	movapd	%xmm4, %xmm6
	addsd	%xmm5, %xmm6
	movsd	%xmm6, 8(%rsi)
	subsd	%xmm2, %xmm3
	movq	%rbp, -72(%rsp)         # 8-byte Spill
	movsd	%xmm3, (%rsi,%rbp,8)
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%rsi,%rax,8)
	movapd	%xmm1, %xmm2
	subsd	%xmm7, %xmm2
	mulsd	%xmm8, %xmm2
	addsd	%xmm1, %xmm7
	mulsd	%xmm8, %xmm7
	movapd	%xmm9, %xmm1
	subsd	%xmm7, %xmm1
	movsd	%xmm1, (%rsi,%r14,8)
	movapd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%rsi,%r9,8)
	addsd	%xmm9, %xmm7
	movsd	%xmm7, (%rsi,%rbx,8)
	subsd	%xmm2, %xmm0
	movsd	%xmm0, (%rsi,%rcx,8)
	movq	%rdi, %rcx
	movslq	%edi, %r10
	cmpl	$3, %r10d
	jl	.LBB32_3
# BB#1:                                 # %.lr.ph.preheader
	movq	-72(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rax
	shlq	$5, %rax
	leaq	-16(%rsi,%rax), %rdi
	leaq	(%rbp,%rbp,2), %rax
	leaq	(%rsi,%rax,8), %r11
	movq	%rbp, %r9
	shlq	$4, %r9
	addq	%rsi, %r9
	leaq	(%rsi,%rbp,8), %r12
	shll	$3, %ecx
	decq	%rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	leal	(%r8,%r8,2), %eax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	decq	%rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	-1(%r14), %rbx
	movl	%r14d, %eax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leaq	-1(%rax), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movl	%ebp, %eax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	leaq	-1(%rax), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movl	$2, %r14d
	movl	$28, %r13d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB32_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	-24(%rdx,%r13,2), %xmm9 # xmm9 = mem[0],zero
	movsd	-16(%rdx,%r13,2), %xmm11 # xmm11 = mem[0],zero
	movsd	-8(%rdx,%r13,2), %xmm0  # xmm0 = mem[0],zero
	movsd	%xmm0, -64(%rsp)        # 8-byte Spill
	movsd	(%rdx,%r13,2), %xmm14   # xmm14 = mem[0],zero
	movsd	-24(%rdx,%rbx,8), %xmm13 # xmm13 = mem[0],zero
	movsd	-16(%rdx,%rbx,8), %xmm12 # xmm12 = mem[0],zero
	movsd	(%rsi,%r14,8), %xmm1    # xmm1 = mem[0],zero
	movq	-32(%rsp), %rax         # 8-byte Reload
	leal	1(%rax,%r14), %eax
	movslq	%eax, %rbp
	movsd	(%rsi,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm6
	subsd	%xmm2, %xmm6
	movsd	8(%rsi,%r14,8), %xmm5   # xmm5 = mem[0],zero
	movsd	(%r9,%r14,8), %xmm3     # xmm3 = mem[0],zero
	movapd	%xmm5, %xmm0
	addsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm1
	subsd	%xmm3, %xmm5
	movsd	(%r12,%r14,8), %xmm2    # xmm2 = mem[0],zero
	movq	-16(%rsp), %rax         # 8-byte Reload
	leal	1(%rax,%r14), %eax
	cltq
	movsd	(%rsi,%rax,8), %xmm15   # xmm15 = mem[0],zero
	movapd	%xmm2, %xmm4
	subsd	%xmm15, %xmm4
	movq	-48(%rsp), %rcx         # 8-byte Reload
	leal	1(%rcx,%r14), %ecx
	movslq	%ecx, %rcx
	movsd	(%rsi,%rcx,8), %xmm8    # xmm8 = mem[0],zero
	movsd	(%r11,%r14,8), %xmm7    # xmm7 = mem[0],zero
	movapd	%xmm8, %xmm3
	addsd	%xmm7, %xmm3
	addsd	%xmm15, %xmm2
	subsd	%xmm7, %xmm8
	movapd	%xmm9, %xmm10
	mulsd	%xmm6, %xmm10
	movapd	%xmm11, %xmm7
	mulsd	%xmm0, %xmm7
	subsd	%xmm7, %xmm10
	mulsd	%xmm9, %xmm0
	mulsd	%xmm11, %xmm6
	addsd	%xmm0, %xmm6
	movapd	%xmm12, %xmm0
	mulsd	%xmm4, %xmm0
	movapd	%xmm13, %xmm7
	mulsd	%xmm3, %xmm7
	subsd	%xmm7, %xmm0
	movsd	-8(%rdx,%rbx,8), %xmm7  # xmm7 = mem[0],zero
	mulsd	%xmm12, %xmm3
	mulsd	%xmm13, %xmm4
	addsd	%xmm3, %xmm4
	movapd	%xmm10, %xmm3
	addsd	%xmm0, %xmm3
	movsd	(%rdx,%rbx,8), %xmm15   # xmm15 = mem[0],zero
	movsd	%xmm3, (%rsi,%r14,8)
	movapd	%xmm6, %xmm3
	addsd	%xmm4, %xmm3
	movsd	%xmm3, 8(%rsi,%r14,8)
	subsd	%xmm0, %xmm10
	movapd	%xmm15, %xmm0
	movapd	.LCPI32_0(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm3, %xmm0
	movsd	%xmm10, (%r12,%r14,8)
	subsd	%xmm4, %xmm6
	movsd	%xmm6, (%rsi,%rcx,8)
	movsd	-64(%rsp), %xmm4        # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm3
	mulsd	%xmm1, %xmm3
	mulsd	%xmm5, %xmm4
	mulsd	%xmm14, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm14, %xmm1
	addsd	%xmm4, %xmm1
	movapd	%xmm7, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm15, %xmm2
	movapd	%xmm7, %xmm5
	mulsd	%xmm8, %xmm5
	subsd	%xmm2, %xmm5
	mulsd	%xmm0, %xmm8
	subsd	%xmm4, %xmm8
	movapd	%xmm3, %xmm0
	addsd	%xmm5, %xmm0
	movsd	%xmm0, (%r9,%r14,8)
	movapd	%xmm1, %xmm0
	addsd	%xmm8, %xmm0
	movsd	%xmm0, (%rsi,%rbp,8)
	subsd	%xmm5, %xmm3
	movsd	%xmm3, (%r11,%r14,8)
	subsd	%xmm8, %xmm1
	movsd	%xmm1, (%rsi,%rax,8)
	movsd	-16(%r12,%r15,8), %xmm6 # xmm6 = mem[0],zero
	movq	-24(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %r8
	movsd	(%rsi,%r8,8), %xmm2     # xmm2 = mem[0],zero
	movapd	%xmm6, %xmm1
	subsd	%xmm2, %xmm1
	movq	-56(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %rbp
	movsd	(%rsi,%rbp,8), %xmm10   # xmm10 = mem[0],zero
	movsd	-16(%r11,%r15,8), %xmm3 # xmm3 = mem[0],zero
	movapd	%xmm10, %xmm5
	addsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm6
	subsd	%xmm3, %xmm10
	movsd	-16(%r9,%r15,8), %xmm8  # xmm8 = mem[0],zero
	movq	-8(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %eax
	cltq
	movsd	(%rsi,%rax,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm8, %xmm2
	subsd	%xmm3, %xmm2
	movq	-40(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r15), %ecx
	movslq	%ecx, %rcx
	movsd	(%rsi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	%xmm3, %xmm8
	movapd	%xmm12, %xmm3
	mulsd	%xmm5, %xmm3
	mulsd	%xmm13, %xmm5
	mulsd	%xmm1, %xmm13
	subsd	%xmm3, %xmm13
	movsd	(%rdi,%r15,8), %xmm3    # xmm3 = mem[0],zero
	mulsd	%xmm12, %xmm1
	movapd	%xmm0, %xmm4
	addsd	%xmm3, %xmm4
	addsd	%xmm5, %xmm1
	movapd	%xmm9, %xmm5
	mulsd	%xmm4, %xmm5
	mulsd	%xmm11, %xmm4
	mulsd	%xmm2, %xmm11
	subsd	%xmm5, %xmm11
	mulsd	%xmm9, %xmm2
	addsd	%xmm4, %xmm2
	movapd	%xmm13, %xmm4
	addsd	%xmm11, %xmm4
	movsd	%xmm4, -16(%r12,%r15,8)
	movapd	%xmm1, %xmm4
	addsd	%xmm2, %xmm4
	movsd	%xmm4, (%rsi,%rbp,8)
	subsd	%xmm11, %xmm13
	movsd	%xmm13, -16(%r9,%r15,8)
	subsd	%xmm2, %xmm1
	movsd	%xmm1, (%rsi,%rcx,8)
	subsd	%xmm3, %xmm0
	movapd	%xmm7, %xmm1
	mulsd	%xmm6, %xmm1
	mulsd	%xmm10, %xmm7
	mulsd	%xmm15, %xmm10
	subsd	%xmm10, %xmm1
	mulsd	%xmm15, %xmm6
	addsd	%xmm7, %xmm6
	movapd	%xmm14, %xmm2
	mulsd	%xmm8, %xmm14
	movsd	-64(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm8
	mulsd	%xmm0, %xmm3
	subsd	%xmm14, %xmm3
	xorpd	.LCPI32_0(%rip), %xmm2
	mulsd	%xmm2, %xmm0
	subsd	%xmm8, %xmm0
	movapd	%xmm1, %xmm2
	addsd	%xmm3, %xmm2
	movsd	%xmm2, -16(%r11,%r15,8)
	movapd	%xmm6, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, (%rsi,%r8,8)
	subsd	%xmm3, %xmm1
	movsd	%xmm1, (%rdi,%r15,8)
	subsd	%xmm0, %xmm6
	movsd	%xmm6, (%rsi,%rax,8)
	addq	$2, %r14
	addq	$-4, %rbx
	addq	$16, %r13
	addq	$-2, %r15
	cmpq	%r10, %r14
	jl	.LBB32_2
.LBB32_3:                               # %._crit_edge
	movq	-72(%rsp), %rax         # 8-byte Reload
	movupd	(%rdx,%rax,8), %xmm8
	leal	(%r10,%r10,2), %ecx
	leal	(%r10,%r10,4), %eax
	leal	(,%r10,8), %edi
	subl	%r10d, %edi
	movsd	(%rsi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	cltq
	movsd	8(%rsi,%rax,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm3
	subsd	%xmm1, %xmm3
	movsd	8(%rsi,%r10,8), %xmm10  # xmm10 = mem[0],zero
	movsd	(%rsi,%rax,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm10, %xmm9
	addsd	%xmm4, %xmm9
	addsd	%xmm1, %xmm0
	subsd	%xmm4, %xmm10
	movslq	%ecx, %rdx
	movsd	(%rsi,%rdx,8), %xmm4    # xmm4 = mem[0],zero
	movslq	%edi, %rcx
	movsd	8(%rsi,%rcx,8), %xmm5   # xmm5 = mem[0],zero
	movapd	%xmm4, %xmm7
	subsd	%xmm5, %xmm7
	movsd	8(%rsi,%rdx,8), %xmm1   # xmm1 = mem[0],zero
	movsd	(%rsi,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm6
	addsd	%xmm2, %xmm6
	addsd	%xmm5, %xmm4
	subsd	%xmm2, %xmm1
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm8, %xmm3
	movlhps	%xmm6, %xmm6            # xmm6 = xmm6[0,0]
	mulpd	%xmm8, %xmm6
	movlhps	%xmm10, %xmm10          # xmm10 = xmm10[0,0]
	mulpd	%xmm8, %xmm10
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm8, %xmm4
	shufpd	$1, %xmm8, %xmm8        # xmm8 = xmm8[1,0]
	movlhps	%xmm9, %xmm9            # xmm9 = xmm9[0,0]
	mulpd	%xmm8, %xmm9
	movapd	%xmm3, %xmm2
	subpd	%xmm9, %xmm2
	addpd	%xmm9, %xmm3
	movsd	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1]
	movlhps	%xmm7, %xmm7            # xmm7 = xmm7[0,0]
	mulpd	%xmm8, %xmm7
	movapd	%xmm7, %xmm2
	subpd	%xmm6, %xmm2
	addpd	%xmm6, %xmm7
	movsd	%xmm2, %xmm7            # xmm7 = xmm2[0],xmm7[1]
	movapd	%xmm3, %xmm2
	addpd	%xmm7, %xmm2
	movupd	%xmm2, (%rsi,%r10,8)
	subpd	%xmm7, %xmm3
	movupd	%xmm3, (%rsi,%rdx,8)
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm8, %xmm0
	movapd	%xmm0, %xmm2
	subpd	%xmm10, %xmm2
	addpd	%xmm10, %xmm0
	movsd	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm8, %xmm1
	movapd	%xmm4, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm1, %xmm4
	movsd	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1]
	movapd	%xmm0, %xmm1
	subpd	%xmm4, %xmm1
	movupd	%xmm1, (%rsi,%rax,8)
	addpd	%xmm0, %xmm4
	movupd	%xmm4, (%rsi,%rcx,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end32:
	.size	cftmdl2, .Lfunc_end32-cftmdl2
	.cfi_endproc

	.globl	cftexp2
	.p2align	4, 0x90
	.type	cftexp2,@function
cftexp2:                                # @cftexp2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi254:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi255:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi256:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi257:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi258:
	.cfi_def_cfa_offset 160
.Lcfi259:
	.cfi_offset %rbx, -56
.Lcfi260:
	.cfi_offset %r12, -48
.Lcfi261:
	.cfi_offset %r13, -40
.Lcfi262:
	.cfi_offset %r14, -32
.Lcfi263:
	.cfi_offset %r15, -24
.Lcfi264:
	.cfi_offset %rbp, -16
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movl	%edi, %r14d
	movl	%r14d, %ebp
	sarl	%ebp
	movl	%r14d, %ebx
	sarl	$2, %ebx
	cmpl	$129, %ebx
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	jl	.LBB33_12
# BB#1:                                 # %.preheader118.preheader
	movslq	%ebp, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(,%rax,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	88(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB33_3:                               # %.preheader118
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_5 Depth 2
                                        #       Child Loop BB33_7 Depth 3
                                        #       Child Loop BB33_10 Depth 3
	movl	%r14d, %eax
	movl	%ebx, %r14d
	cmpl	%ebp, %r14d
	jge	.LBB33_2
# BB#4:                                 # %.lr.ph136
                                        #   in Loop: Header=BB33_3 Depth=1
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	%esi, %ecx
	subl	%r14d, %ecx
	movslq	%ecx, %rcx
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	sarl	$3, %eax
	movl	%esi, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rax
	leaq	(%rdx,%rax,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB33_5:                               #   Parent Loop BB33_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_7 Depth 3
                                        #       Child Loop BB33_10 Depth 3
	movl	%ecx, %eax
	subl	%r14d, %eax
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leal	(%rcx,%rcx), %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	cmpl	%ebp, %eax
	movq	72(%rsp), %rbx          # 8-byte Reload
	jge	.LBB33_8
# BB#6:                                 # %.lr.ph129
                                        #   in Loop: Header=BB33_5 Depth=2
	movslq	%eax, %r12
	movslq	40(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	(,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	80(%rsp), %r13          # 8-byte Reload
	leaq	(,%r12,8), %rbp
	.p2align	4, 0x90
.LBB33_7:                               #   Parent Loop BB33_3 Depth=1
                                        #     Parent Loop BB33_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r13,%rbp), %rsi
	movl	%r14d, %edi
	movq	%rbx, %rdx
	callq	cftmdl1
	leaq	(%r13,%r15), %rsi
	addq	%rbp, %rsi
	movl	%r14d, %edi
	movq	%rbx, %rdx
	callq	cftmdl1
	addq	64(%rsp), %r12          # 8-byte Folded Reload
	addq	56(%rsp), %r13          # 8-byte Folded Reload
	cmpq	24(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB33_7
.LBB33_8:                               # %._crit_edge130
                                        #   in Loop: Header=BB33_5 Depth=2
	movl	40(%rsp), %ecx          # 4-byte Reload
	subl	%r14d, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	shll	$2, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpl	12(%rsp), %ecx          # 4-byte Folded Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jge	.LBB33_11
# BB#9:                                 # %.lr.ph133
                                        #   in Loop: Header=BB33_5 Depth=2
	movslq	%ecx, %rbx
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	(,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rbp          # 8-byte Reload
	leaq	(,%rbx,8), %r13
	.p2align	4, 0x90
.LBB33_10:                              #   Parent Loop BB33_3 Depth=1
                                        #     Parent Loop BB33_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%rbp,%r13), %rsi
	movl	%r14d, %edi
	movq	%r12, %rdx
	callq	cftmdl2
	leaq	(%rbp,%r15), %rsi
	addq	%r13, %rsi
	movl	%r14d, %edi
	movq	%r12, %rdx
	callq	cftmdl2
	addq	64(%rsp), %rbx          # 8-byte Folded Reload
	addq	56(%rsp), %rbp          # 8-byte Folded Reload
	cmpq	24(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB33_10
.LBB33_11:                              # %._crit_edge134
                                        #   in Loop: Header=BB33_5 Depth=2
	movl	12(%rsp), %ebp          # 4-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	%ebp, %ecx
	jl	.LBB33_5
.LBB33_2:                               # %.loopexit
                                        #   in Loop: Header=BB33_3 Depth=1
	movl	%r14d, %ebx
	sarl	$2, %ebx
	cmpl	$129, %ebx
	jge	.LBB33_3
.LBB33_12:                              # %.preheader
	cmpl	%ebp, %ebx
	jge	.LBB33_21
# BB#13:                                # %.lr.ph125
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%edx, %eax
	subl	%ebx, %eax
	cltq
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	sarl	$3, %r14d
	movl	%edx, %eax
	subl	%r14d, %eax
	cltq
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movslq	%ebp, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	(,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ebx, %ecx
	.p2align	4, 0x90
.LBB33_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_16 Depth 2
                                        #     Child Loop BB33_19 Depth 2
	movl	%ecx, %eax
	subl	%ebx, %eax
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leal	(%rcx,%rcx), %ecx
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	cmpl	%ebp, %eax
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	96(%rsp), %r13          # 8-byte Reload
	jge	.LBB33_17
# BB#15:                                # %.lr.ph
                                        #   in Loop: Header=BB33_14 Depth=1
	movslq	%eax, %r15
	movslq	72(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	(,%rax,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	80(%rsp), %r12          # 8-byte Reload
	leaq	(,%r15,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB33_16:                              #   Parent Loop BB33_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%r12,%rax), %r14
	movl	%ebx, %edi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	cftmdl1
	movl	%ebx, %edi
	movq	%r14, %rsi
	movl	8(%rsp), %r14d          # 4-byte Reload
	movl	%r14d, %edx
	movq	%rbp, %rcx
	callq	cftfx41
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%r12,%rax), %rbp
	addq	24(%rsp), %rbp          # 8-byte Folded Reload
	movl	%ebx, %edi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	cftmdl1
	movl	%ebx, %edi
	movq	%rbp, %rsi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	%r14d, %edx
	movq	%rbp, %rcx
	callq	cftfx41
	addq	16(%rsp), %r15          # 8-byte Folded Reload
	addq	40(%rsp), %r12          # 8-byte Folded Reload
	cmpq	64(%rsp), %r15          # 8-byte Folded Reload
	jl	.LBB33_16
.LBB33_17:                              # %._crit_edge
                                        #   in Loop: Header=BB33_14 Depth=1
	movl	72(%rsp), %ecx          # 4-byte Reload
	subl	%ebx, %ecx
	movq	32(%rsp), %rax          # 8-byte Reload
	shll	$2, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cmpl	12(%rsp), %ecx          # 4-byte Folded Reload
	movq	88(%rsp), %r15          # 8-byte Reload
	movl	%ebx, %r14d
	jge	.LBB33_20
# BB#18:                                # %.lr.ph122
                                        #   in Loop: Header=BB33_14 Depth=1
	movslq	%ecx, %r12
	movslq	32(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(,%rax,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rbx          # 8-byte Reload
	leaq	(,%r12,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB33_19:                              #   Parent Loop BB33_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%rsp), %r13          # 8-byte Reload
	leaq	(%rbx,%r13), %rbp
	movl	%r14d, %edi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	cftmdl2
	movl	%r14d, %edi
	movq	%rbp, %rsi
	movl	8(%rsp), %edx           # 4-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	cftfx42
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rbx,%rax), %rbp
	addq	%r13, %rbp
	movl	%r14d, %edi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	cftmdl2
	movl	%r14d, %edi
	movq	%rbp, %rsi
	movl	8(%rsp), %edx           # 4-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	cftfx42
	addq	24(%rsp), %r12          # 8-byte Folded Reload
	addq	16(%rsp), %rbx          # 8-byte Folded Reload
	cmpq	64(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB33_19
.LBB33_20:                              # %._crit_edge123
                                        #   in Loop: Header=BB33_14 Depth=1
	movl	12(%rsp), %ebp          # 4-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	%ebp, %ecx
	movl	%r14d, %ebx
	jl	.LBB33_14
.LBB33_21:                              # %._crit_edge126
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end33:
	.size	cftexp2, .Lfunc_end33-cftexp2
	.cfi_endproc

	.globl	cftfx42
	.p2align	4, 0x90
	.type	cftfx42,@function
cftfx42:                                # @cftfx42
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi265:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi266:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi267:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi268:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi269:
	.cfi_def_cfa_offset 48
.Lcfi270:
	.cfi_offset %rbx, -40
.Lcfi271:
	.cfi_offset %r12, -32
.Lcfi272:
	.cfi_offset %r14, -24
.Lcfi273:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rsi, %rbx
	cmpl	$128, %edi
	jne	.LBB34_2
# BB#1:
	movslq	%edx, %r12
	leaq	-64(%r14,%r12,8), %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	cftf161
	leaq	256(%rbx), %rdi
	leaq	-256(%r14,%r12,8), %r14
	movq	%r14, %rsi
	callq	cftf162
	leaq	512(%rbx), %rdi
	movq	%r15, %rsi
	callq	cftf161
	addq	$768, %rbx              # imm = 0x300
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	cftf162                 # TAILCALL
.LBB34_2:
	movslq	%edx, %rax
	leaq	-128(%r14,%rax,8), %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	cftf081
	movq	%rbx, %rdi
	subq	$-128, %rdi
	movq	%r14, %rsi
	callq	cftf082
	leaq	256(%rbx), %rdi
	movq	%r14, %rsi
	callq	cftf081
	addq	$384, %rbx              # imm = 0x180
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	cftf082                 # TAILCALL
.Lfunc_end34:
	.size	cftfx42, .Lfunc_end34-cftfx42
	.cfi_endproc

	.globl	cftf162
	.p2align	4, 0x90
	.type	cftf162,@function
cftf162:                                # @cftf162
	.cfi_startproc
# BB#0:
	subq	$120, %rsp
.Lcfi274:
	.cfi_def_cfa_offset 128
	movsd	8(%rsi), %xmm10         # xmm10 = mem[0],zero
	movsd	32(%rsi), %xmm15        # xmm15 = mem[0],zero
	movsd	136(%rdi), %xmm8        # xmm8 = mem[0],zero
	movsd	(%rdi), %xmm13          # xmm13 = mem[0],zero
	movsd	8(%rdi), %xmm6          # xmm6 = mem[0],zero
	movapd	%xmm13, %xmm0
	subsd	%xmm8, %xmm0
	movsd	128(%rdi), %xmm9        # xmm9 = mem[0],zero
	movapd	%xmm6, %xmm3
	addsd	%xmm9, %xmm3
	movsd	64(%rdi), %xmm2         # xmm2 = mem[0],zero
	movsd	200(%rdi), %xmm11       # xmm11 = mem[0],zero
	movapd	%xmm2, %xmm1
	subsd	%xmm11, %xmm1
	movsd	72(%rdi), %xmm4         # xmm4 = mem[0],zero
	movsd	192(%rdi), %xmm12       # xmm12 = mem[0],zero
	movapd	%xmm4, %xmm7
	addsd	%xmm12, %xmm7
	movapd	%xmm1, %xmm5
	subsd	%xmm7, %xmm5
	mulsd	%xmm10, %xmm5
	addsd	%xmm1, %xmm7
	mulsd	%xmm10, %xmm7
	movapd	%xmm0, %xmm1
	addsd	%xmm5, %xmm1
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movapd	%xmm3, %xmm1
	addsd	%xmm7, %xmm1
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	subsd	%xmm5, %xmm0
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	subsd	%xmm7, %xmm3
	movsd	%xmm3, 80(%rsp)         # 8-byte Spill
	addsd	%xmm8, %xmm13
	subsd	%xmm9, %xmm6
	addsd	%xmm11, %xmm2
	subsd	%xmm12, %xmm4
	movapd	%xmm2, %xmm1
	subsd	%xmm4, %xmm1
	movsd	%xmm10, 64(%rsp)        # 8-byte Spill
	mulsd	%xmm10, %xmm1
	addsd	%xmm2, %xmm4
	mulsd	%xmm10, %xmm4
	movapd	%xmm13, %xmm0
	subsd	%xmm4, %xmm0
	movsd	%xmm0, 88(%rsp)         # 8-byte Spill
	movapd	%xmm6, %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 96(%rsp)         # 8-byte Spill
	addsd	%xmm13, %xmm4
	movsd	%xmm4, 112(%rsp)        # 8-byte Spill
	subsd	%xmm1, %xmm6
	movsd	%xmm6, 104(%rsp)        # 8-byte Spill
	movsd	16(%rdi), %xmm0         # xmm0 = mem[0],zero
	movsd	152(%rdi), %xmm14       # xmm14 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm14, %xmm4
	movsd	24(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	144(%rdi), %xmm9        # xmm9 = mem[0],zero
	movapd	%xmm1, %xmm2
	addsd	%xmm9, %xmm2
	movsd	%xmm15, -120(%rsp)      # 8-byte Spill
	movapd	%xmm15, %xmm3
	mulsd	%xmm4, %xmm3
	movsd	40(%rsi), %xmm6         # xmm6 = mem[0],zero
	movsd	%xmm6, -128(%rsp)       # 8-byte Spill
	movapd	%xmm6, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movapd	%xmm3, %xmm13
	mulsd	%xmm15, %xmm2
	mulsd	%xmm6, %xmm4
	addsd	%xmm2, %xmm4
	movapd	%xmm4, %xmm11
	movsd	80(%rdi), %xmm4         # xmm4 = mem[0],zero
	movsd	216(%rdi), %xmm10       # xmm10 = mem[0],zero
	movapd	%xmm4, %xmm2
	subsd	%xmm10, %xmm2
	movsd	88(%rdi), %xmm12        # xmm12 = mem[0],zero
	movsd	208(%rdi), %xmm15       # xmm15 = mem[0],zero
	movapd	%xmm12, %xmm3
	addsd	%xmm15, %xmm3
	movsd	56(%rsi), %xmm8         # xmm8 = mem[0],zero
	movapd	%xmm8, %xmm5
	mulsd	%xmm2, %xmm5
	movsd	48(%rsi), %xmm7         # xmm7 = mem[0],zero
	movapd	%xmm7, %xmm6
	mulsd	%xmm3, %xmm6
	subsd	%xmm6, %xmm5
	mulsd	%xmm8, %xmm3
	mulsd	%xmm7, %xmm2
	addsd	%xmm3, %xmm2
	movapd	%xmm13, %xmm3
	addsd	%xmm5, %xmm3
	movsd	%xmm3, -24(%rsp)        # 8-byte Spill
	subsd	%xmm5, %xmm13
	movsd	%xmm13, 8(%rsp)         # 8-byte Spill
	movapd	%xmm11, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, -16(%rsp)        # 8-byte Spill
	subsd	%xmm2, %xmm11
	movsd	%xmm11, (%rsp)          # 8-byte Spill
	addsd	%xmm14, %xmm0
	subsd	%xmm9, %xmm1
	movsd	%xmm7, -112(%rsp)       # 8-byte Spill
	movapd	%xmm7, %xmm3
	mulsd	%xmm0, %xmm3
	movsd	%xmm8, -88(%rsp)        # 8-byte Spill
	movapd	%xmm8, %xmm2
	mulsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm3
	mulsd	%xmm8, %xmm0
	mulsd	%xmm7, %xmm1
	addsd	%xmm1, %xmm0
	addsd	%xmm10, %xmm4
	subsd	%xmm15, %xmm12
	movsd	-120(%rsp), %xmm6       # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movapd	%xmm6, %xmm1
	mulsd	%xmm4, %xmm1
	movsd	-128(%rsp), %xmm5       # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm2
	mulsd	%xmm12, %xmm2
	addsd	%xmm1, %xmm2
	mulsd	%xmm6, %xmm12
	mulsd	%xmm5, %xmm4
	subsd	%xmm4, %xmm12
	movapd	%xmm3, %xmm1
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	addsd	%xmm3, %xmm2
	movsd	%xmm2, 56(%rsp)         # 8-byte Spill
	movapd	%xmm0, %xmm1
	subsd	%xmm12, %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	addsd	%xmm0, %xmm12
	movsd	%xmm12, 48(%rsp)        # 8-byte Spill
	movsd	32(%rdi), %xmm0         # xmm0 = mem[0],zero
	movsd	168(%rdi), %xmm9        # xmm9 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm9, %xmm4
	movsd	40(%rdi), %xmm14        # xmm14 = mem[0],zero
	movsd	160(%rdi), %xmm10       # xmm10 = mem[0],zero
	movapd	%xmm14, %xmm3
	addsd	%xmm10, %xmm3
	movsd	64(%rsi), %xmm15        # xmm15 = mem[0],zero
	movapd	%xmm15, %xmm1
	mulsd	%xmm4, %xmm1
	movsd	72(%rsi), %xmm2         # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm11
	mulsd	%xmm15, %xmm3
	mulsd	%xmm2, %xmm4
	addsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm13
	movsd	96(%rdi), %xmm12        # xmm12 = mem[0],zero
	movsd	232(%rdi), %xmm8        # xmm8 = mem[0],zero
	movapd	%xmm12, %xmm3
	subsd	%xmm8, %xmm3
	movsd	104(%rdi), %xmm6        # xmm6 = mem[0],zero
	movsd	224(%rdi), %xmm7        # xmm7 = mem[0],zero
	movapd	%xmm6, %xmm5
	addsd	%xmm7, %xmm5
	movapd	%xmm2, %xmm4
	mulsd	%xmm3, %xmm4
	movapd	%xmm15, %xmm1
	mulsd	%xmm5, %xmm1
	subsd	%xmm1, %xmm4
	mulsd	%xmm15, %xmm3
	mulsd	%xmm2, %xmm5
	addsd	%xmm5, %xmm3
	movapd	%xmm11, %xmm1
	addsd	%xmm4, %xmm1
	movsd	%xmm1, -80(%rsp)        # 8-byte Spill
	subsd	%xmm4, %xmm11
	movsd	%xmm11, -56(%rsp)       # 8-byte Spill
	movapd	%xmm13, %xmm1
	addsd	%xmm3, %xmm1
	movsd	%xmm1, -72(%rsp)        # 8-byte Spill
	subsd	%xmm3, %xmm13
	movsd	%xmm13, -64(%rsp)       # 8-byte Spill
	addsd	%xmm9, %xmm0
	subsd	%xmm10, %xmm14
	movapd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	movapd	%xmm15, %xmm1
	mulsd	%xmm14, %xmm1
	subsd	%xmm1, %xmm3
	mulsd	%xmm15, %xmm0
	mulsd	%xmm2, %xmm14
	addsd	%xmm14, %xmm0
	addsd	%xmm8, %xmm12
	subsd	%xmm7, %xmm6
	movapd	%xmm2, %xmm1
	mulsd	%xmm6, %xmm1
	mulsd	%xmm15, %xmm6
	mulsd	%xmm12, %xmm15
	subsd	%xmm1, %xmm15
	mulsd	%xmm2, %xmm12
	addsd	%xmm6, %xmm12
	movapd	%xmm3, %xmm1
	subsd	%xmm15, %xmm1
	movsd	%xmm1, -48(%rsp)        # 8-byte Spill
	addsd	%xmm3, %xmm15
	movsd	%xmm15, -8(%rsp)        # 8-byte Spill
	movapd	%xmm0, %xmm1
	subsd	%xmm12, %xmm1
	movsd	%xmm1, -40(%rsp)        # 8-byte Spill
	addsd	%xmm0, %xmm12
	movsd	%xmm12, -32(%rsp)       # 8-byte Spill
	movsd	48(%rdi), %xmm2         # xmm2 = mem[0],zero
	movsd	184(%rdi), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, -96(%rsp)        # 8-byte Spill
	movapd	%xmm2, %xmm4
	subsd	%xmm0, %xmm4
	movsd	56(%rdi), %xmm7         # xmm7 = mem[0],zero
	movsd	176(%rdi), %xmm8        # xmm8 = mem[0],zero
	movapd	%xmm7, %xmm0
	addsd	%xmm8, %xmm0
	movsd	-112(%rsp), %xmm3       # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movapd	%xmm3, %xmm13
	mulsd	%xmm4, %xmm13
	movsd	-88(%rsp), %xmm11       # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movapd	%xmm11, %xmm1
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm13
	mulsd	%xmm11, %xmm4
	mulsd	%xmm3, %xmm0
	addsd	%xmm0, %xmm4
	movsd	112(%rdi), %xmm5        # xmm5 = mem[0],zero
	movsd	248(%rdi), %xmm12       # xmm12 = mem[0],zero
	movapd	%xmm5, %xmm0
	subsd	%xmm12, %xmm0
	movsd	120(%rdi), %xmm3        # xmm3 = mem[0],zero
	movsd	240(%rdi), %xmm9        # xmm9 = mem[0],zero
	movapd	%xmm3, %xmm10
	addsd	%xmm9, %xmm10
	movsd	-128(%rsp), %xmm6       # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movapd	%xmm6, %xmm15
	mulsd	%xmm0, %xmm15
	movsd	-120(%rsp), %xmm1       # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm14
	mulsd	%xmm10, %xmm14
	subsd	%xmm14, %xmm15
	mulsd	%xmm1, %xmm0
	movapd	%xmm6, %xmm14
	mulsd	%xmm14, %xmm10
	addsd	%xmm10, %xmm0
	movapd	%xmm13, %xmm6
	addsd	%xmm15, %xmm6
	movsd	%xmm6, -104(%rsp)       # 8-byte Spill
	subsd	%xmm15, %xmm13
	movapd	%xmm4, %xmm15
	addsd	%xmm0, %xmm15
	subsd	%xmm0, %xmm4
	addsd	-96(%rsp), %xmm2        # 8-byte Folded Reload
	subsd	%xmm8, %xmm7
	movapd	%xmm14, %xmm0
	movapd	%xmm14, %xmm8
	mulsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm2
	movapd	%xmm1, %xmm14
	mulsd	%xmm7, %xmm14
	addsd	%xmm0, %xmm14
	mulsd	%xmm8, %xmm7
	subsd	%xmm2, %xmm7
	addsd	%xmm12, %xmm5
	subsd	%xmm9, %xmm3
	movsd	-112(%rsp), %xmm1       # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm0
	mulsd	%xmm3, %xmm0
	mulsd	%xmm11, %xmm3
	mulsd	%xmm5, %xmm11
	subsd	%xmm0, %xmm11
	mulsd	%xmm1, %xmm5
	addsd	%xmm3, %xmm5
	movapd	%xmm14, %xmm0
	addsd	%xmm11, %xmm0
	movsd	%xmm0, -128(%rsp)       # 8-byte Spill
	subsd	%xmm11, %xmm14
	movapd	%xmm7, %xmm12
	addsd	%xmm5, %xmm12
	subsd	%xmm5, %xmm7
	movsd	%xmm7, -120(%rsp)       # 8-byte Spill
	movsd	32(%rsp), %xmm11        # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movapd	%xmm11, %xmm0
	movsd	-80(%rsp), %xmm9        # 8-byte Reload
                                        # xmm9 = mem[0],zero
	addsd	%xmm9, %xmm0
	movsd	-24(%rsp), %xmm10       # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movapd	%xmm10, %xmm1
	addsd	%xmm6, %xmm1
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%rdi)
	movsd	40(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movapd	%xmm7, %xmm3
	movsd	-72(%rsp), %xmm6        # 8-byte Reload
                                        # xmm6 = mem[0],zero
	addsd	%xmm6, %xmm3
	movsd	-16(%rsp), %xmm8        # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movapd	%xmm8, %xmm5
	addsd	%xmm15, %xmm5
	movapd	%xmm3, %xmm2
	addsd	%xmm5, %xmm2
	movsd	%xmm2, 8(%rdi)
	subsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rdi)
	subsd	%xmm5, %xmm3
	movsd	%xmm3, 24(%rdi)
	subsd	%xmm9, %xmm11
	subsd	%xmm6, %xmm7
	subsd	-104(%rsp), %xmm10      # 8-byte Folded Reload
	subsd	%xmm15, %xmm8
	movapd	%xmm11, %xmm0
	subsd	%xmm8, %xmm0
	movsd	%xmm0, 32(%rdi)
	movapd	%xmm7, %xmm0
	addsd	%xmm10, %xmm0
	movsd	%xmm0, 40(%rdi)
	addsd	%xmm11, %xmm8
	movsd	%xmm8, 48(%rdi)
	subsd	%xmm10, %xmm7
	movsd	%xmm7, 56(%rdi)
	movsd	8(%rsp), %xmm8          # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movapd	%xmm8, %xmm0
	subsd	%xmm4, %xmm0
	movsd	(%rsp), %xmm9           # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movapd	%xmm9, %xmm1
	addsd	%xmm13, %xmm1
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm0, %xmm1
	movsd	72(%rsp), %xmm15        # 8-byte Reload
                                        # xmm15 = mem[0],zero
	movapd	%xmm15, %xmm0
	movsd	-64(%rsp), %xmm7        # 8-byte Reload
                                        # xmm7 = mem[0],zero
	subsd	%xmm7, %xmm0
	movsd	64(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm2
	movapd	%xmm0, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, 64(%rdi)
	movsd	80(%rsp), %xmm10        # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movapd	%xmm10, %xmm3
	movsd	-56(%rsp), %xmm6        # 8-byte Reload
                                        # xmm6 = mem[0],zero
	addsd	%xmm6, %xmm3
	mulsd	%xmm5, %xmm1
	movapd	%xmm5, %xmm11
	movapd	%xmm3, %xmm5
	addsd	%xmm1, %xmm5
	movsd	%xmm5, 72(%rdi)
	subsd	%xmm2, %xmm0
	movsd	%xmm0, 80(%rdi)
	subsd	%xmm1, %xmm3
	movsd	%xmm3, 88(%rdi)
	addsd	%xmm15, %xmm7
	subsd	%xmm6, %xmm10
	addsd	%xmm8, %xmm4
	movapd	%xmm9, %xmm3
	subsd	%xmm13, %xmm3
	movapd	%xmm4, %xmm0
	subsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm3
	movapd	%xmm11, %xmm2
	mulsd	%xmm2, %xmm3
	movapd	%xmm7, %xmm1
	subsd	%xmm3, %xmm1
	movsd	%xmm1, 96(%rdi)
	mulsd	%xmm2, %xmm0
	movapd	%xmm2, %xmm15
	movapd	%xmm10, %xmm2
	movapd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 104(%rdi)
	addsd	%xmm7, %xmm3
	movsd	%xmm3, 112(%rdi)
	subsd	%xmm0, %xmm2
	movsd	%xmm2, 120(%rdi)
	movsd	88(%rsp), %xmm9         # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movapd	%xmm9, %xmm0
	movsd	-48(%rsp), %xmm10       # 8-byte Reload
                                        # xmm10 = mem[0],zero
	addsd	%xmm10, %xmm0
	movsd	16(%rsp), %xmm8         # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movapd	%xmm8, %xmm1
	movsd	-128(%rsp), %xmm11      # 8-byte Reload
                                        # xmm11 = mem[0],zero
	subsd	%xmm11, %xmm1
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 128(%rdi)
	movsd	96(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movapd	%xmm7, %xmm2
	movsd	-40(%rsp), %xmm6        # 8-byte Reload
                                        # xmm6 = mem[0],zero
	addsd	%xmm6, %xmm2
	movsd	24(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm3
	subsd	%xmm12, %xmm3
	movapd	%xmm2, %xmm4
	addsd	%xmm3, %xmm4
	movsd	%xmm4, 136(%rdi)
	subsd	%xmm1, %xmm0
	movsd	%xmm0, 144(%rdi)
	subsd	%xmm3, %xmm2
	movsd	%xmm2, 152(%rdi)
	movapd	%xmm9, %xmm2
	subsd	%xmm10, %xmm2
	movapd	%xmm7, %xmm1
	subsd	%xmm6, %xmm1
	movapd	%xmm11, %xmm3
	addsd	%xmm8, %xmm3
	addsd	%xmm5, %xmm12
	movapd	%xmm2, %xmm0
	subsd	%xmm12, %xmm0
	movsd	%xmm0, 160(%rdi)
	movapd	%xmm1, %xmm0
	addsd	%xmm3, %xmm0
	movsd	%xmm0, 168(%rdi)
	addsd	%xmm2, %xmm12
	movsd	%xmm12, 176(%rdi)
	subsd	%xmm3, %xmm1
	movsd	%xmm1, 184(%rdi)
	movsd	112(%rsp), %xmm9        # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movapd	%xmm9, %xmm0
	movsd	-32(%rsp), %xmm7        # 8-byte Reload
                                        # xmm7 = mem[0],zero
	subsd	%xmm7, %xmm0
	movsd	104(%rsp), %xmm13       # 8-byte Reload
                                        # xmm13 = mem[0],zero
	movapd	%xmm13, %xmm1
	movsd	-8(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	addsd	%xmm6, %xmm1
	movsd	56(%rsp), %xmm10        # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movapd	%xmm10, %xmm2
	movsd	-120(%rsp), %xmm11      # 8-byte Reload
                                        # xmm11 = mem[0],zero
	addsd	%xmm11, %xmm2
	movsd	48(%rsp), %xmm8         # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movapd	%xmm8, %xmm3
	subsd	%xmm14, %xmm3
	movapd	%xmm2, %xmm4
	subsd	%xmm3, %xmm4
	movapd	%xmm15, %xmm5
	mulsd	%xmm5, %xmm4
	addsd	%xmm2, %xmm3
	mulsd	%xmm5, %xmm3
	movapd	%xmm0, %xmm2
	addsd	%xmm4, %xmm2
	movsd	%xmm2, 192(%rdi)
	movapd	%xmm1, %xmm2
	addsd	%xmm3, %xmm2
	movsd	%xmm2, 200(%rdi)
	subsd	%xmm4, %xmm0
	movsd	%xmm0, 208(%rdi)
	subsd	%xmm3, %xmm1
	movsd	%xmm1, 216(%rdi)
	movapd	%xmm7, %xmm3
	addsd	%xmm9, %xmm3
	movapd	%xmm13, %xmm2
	subsd	%xmm6, %xmm2
	movapd	%xmm10, %xmm1
	subsd	%xmm11, %xmm1
	addsd	%xmm8, %xmm14
	movapd	%xmm1, %xmm0
	subsd	%xmm14, %xmm0
	addsd	%xmm1, %xmm14
	mulsd	%xmm5, %xmm0
	mulsd	%xmm5, %xmm14
	movapd	%xmm3, %xmm1
	subsd	%xmm14, %xmm1
	movsd	%xmm1, 224(%rdi)
	movapd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 232(%rdi)
	addsd	%xmm3, %xmm14
	movsd	%xmm14, 240(%rdi)
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 248(%rdi)
	addq	$120, %rsp
	retq
.Lfunc_end35:
	.size	cftf162, .Lfunc_end35-cftf162
	.cfi_endproc

	.globl	cftf082
	.p2align	4, 0x90
	.type	cftf082,@function
cftf082:                                # @cftf082
	.cfi_startproc
# BB#0:
	movsd	8(%rsi), %xmm0          # xmm0 = mem[0],zero
	movsd	32(%rsi), %xmm12        # xmm12 = mem[0],zero
	movsd	72(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	(%rdi), %xmm4           # xmm4 = mem[0],zero
	movsd	8(%rdi), %xmm3          # xmm3 = mem[0],zero
	movapd	%xmm4, %xmm2
	subsd	%xmm1, %xmm2
	movsd	%xmm2, -48(%rsp)        # 8-byte Spill
	movsd	64(%rdi), %xmm2         # xmm2 = mem[0],zero
	movapd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	movsd	%xmm5, -32(%rsp)        # 8-byte Spill
	addsd	%xmm1, %xmm4
	movsd	%xmm4, -8(%rsp)         # 8-byte Spill
	subsd	%xmm2, %xmm3
	movsd	%xmm3, -16(%rsp)        # 8-byte Spill
	movsd	32(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	104(%rdi), %xmm2        # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	subsd	%xmm2, %xmm3
	movsd	40(%rdi), %xmm9         # xmm9 = mem[0],zero
	movsd	96(%rdi), %xmm5         # xmm5 = mem[0],zero
	movapd	%xmm9, %xmm13
	addsd	%xmm5, %xmm13
	movapd	%xmm3, %xmm14
	subsd	%xmm13, %xmm14
	mulsd	%xmm0, %xmm14
	addsd	%xmm3, %xmm13
	mulsd	%xmm0, %xmm13
	addsd	%xmm2, %xmm1
	subsd	%xmm5, %xmm9
	movapd	%xmm1, %xmm2
	subsd	%xmm9, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -24(%rsp)        # 8-byte Spill
	addsd	%xmm1, %xmm9
	mulsd	%xmm0, %xmm9
	movsd	16(%rdi), %xmm15        # xmm15 = mem[0],zero
	movsd	88(%rdi), %xmm1         # xmm1 = mem[0],zero
	movapd	%xmm15, %xmm6
	subsd	%xmm1, %xmm6
	movsd	24(%rdi), %xmm3         # xmm3 = mem[0],zero
	movsd	80(%rdi), %xmm4         # xmm4 = mem[0],zero
	movapd	%xmm3, %xmm5
	addsd	%xmm4, %xmm5
	movapd	%xmm12, %xmm10
	mulsd	%xmm6, %xmm10
	movsd	40(%rsi), %xmm2         # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm0
	mulsd	%xmm5, %xmm0
	subsd	%xmm0, %xmm10
	mulsd	%xmm12, %xmm5
	mulsd	%xmm2, %xmm6
	addsd	%xmm5, %xmm6
	addsd	%xmm1, %xmm15
	movapd	%xmm12, %xmm0
	subsd	%xmm4, %xmm3
	movapd	%xmm2, %xmm1
	mulsd	%xmm15, %xmm1
	mulsd	%xmm12, %xmm15
	mulsd	%xmm3, %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -40(%rsp)        # 8-byte Spill
	mulsd	%xmm2, %xmm3
	addsd	%xmm3, %xmm15
	movsd	48(%rdi), %xmm5         # xmm5 = mem[0],zero
	movsd	120(%rdi), %xmm8        # xmm8 = mem[0],zero
	movapd	%xmm5, %xmm3
	subsd	%xmm8, %xmm3
	movsd	56(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	112(%rdi), %xmm4        # xmm4 = mem[0],zero
	movapd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm3, %xmm7
	movapd	%xmm12, %xmm11
	mulsd	%xmm0, %xmm11
	subsd	%xmm11, %xmm7
	mulsd	%xmm12, %xmm3
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm3
	addsd	%xmm8, %xmm5
	subsd	%xmm4, %xmm1
	movapd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm12, %xmm1
	mulsd	%xmm5, %xmm12
	subsd	%xmm0, %xmm12
	mulsd	%xmm2, %xmm5
	addsd	%xmm1, %xmm5
	movsd	-48(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm14, %xmm0
	movapd	%xmm10, %xmm11
	addsd	%xmm7, %xmm11
	movapd	%xmm0, %xmm2
	addsd	%xmm11, %xmm2
	movsd	%xmm2, (%rdi)
	movsd	-32(%rsp), %xmm8        # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movapd	%xmm8, %xmm1
	addsd	%xmm13, %xmm1
	movapd	%xmm6, %xmm4
	addsd	%xmm3, %xmm4
	movapd	%xmm1, %xmm2
	addsd	%xmm4, %xmm2
	movsd	%xmm2, 8(%rdi)
	subsd	%xmm11, %xmm0
	movsd	%xmm0, 16(%rdi)
	subsd	%xmm4, %xmm1
	movsd	%xmm1, 24(%rdi)
	movsd	-48(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm14, %xmm1
	subsd	%xmm13, %xmm8
	subsd	%xmm7, %xmm10
	subsd	%xmm3, %xmm6
	movapd	%xmm1, %xmm0
	subsd	%xmm6, %xmm0
	movsd	%xmm0, 32(%rdi)
	movapd	%xmm8, %xmm0
	addsd	%xmm10, %xmm0
	movsd	%xmm0, 40(%rdi)
	addsd	%xmm1, %xmm6
	movsd	%xmm6, 48(%rdi)
	subsd	%xmm10, %xmm8
	movsd	%xmm8, 56(%rdi)
	movsd	-8(%rsp), %xmm8         # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movapd	%xmm8, %xmm0
	subsd	%xmm9, %xmm0
	movsd	-16(%rsp), %xmm7        # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movapd	%xmm7, %xmm1
	movsd	-24(%rsp), %xmm6        # 8-byte Reload
                                        # xmm6 = mem[0],zero
	addsd	%xmm6, %xmm1
	movsd	-40(%rsp), %xmm10       # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movapd	%xmm10, %xmm2
	subsd	%xmm12, %xmm2
	movapd	%xmm15, %xmm3
	subsd	%xmm5, %xmm3
	movapd	%xmm0, %xmm4
	addsd	%xmm2, %xmm4
	movsd	%xmm4, 64(%rdi)
	movapd	%xmm1, %xmm4
	addsd	%xmm3, %xmm4
	movsd	%xmm4, 72(%rdi)
	subsd	%xmm2, %xmm0
	movsd	%xmm0, 80(%rdi)
	subsd	%xmm3, %xmm1
	movsd	%xmm1, 88(%rdi)
	addsd	%xmm8, %xmm9
	movapd	%xmm7, %xmm1
	subsd	%xmm6, %xmm1
	addsd	%xmm10, %xmm12
	addsd	%xmm15, %xmm5
	movapd	%xmm9, %xmm0
	subsd	%xmm5, %xmm0
	movsd	%xmm0, 96(%rdi)
	movapd	%xmm1, %xmm0
	addsd	%xmm12, %xmm0
	movsd	%xmm0, 104(%rdi)
	addsd	%xmm9, %xmm5
	movsd	%xmm5, 112(%rdi)
	subsd	%xmm12, %xmm1
	movsd	%xmm1, 120(%rdi)
	retq
.Lfunc_end36:
	.size	cftf082, .Lfunc_end36-cftf082
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
