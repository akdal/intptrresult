	.text
	.file	"doitgen.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4575657221408423936     # double 0.0078125
.LCPI6_2:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 176
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$16777216, %edx         # imm = 0x1000000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_64
# BB#1:
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_64
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$16777216, %edx         # imm = 0x1000000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_64
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_64
# BB#4:                                 # %polybench_alloc_data.exit45
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$16777216, %edx         # imm = 0x1000000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_64
# BB#5:                                 # %polybench_alloc_data.exit45
	movq	8(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_64
# BB#6:                                 # %polybench_alloc_data.exit47
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$131072, %edx           # imm = 0x20000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_64
# BB#7:                                 # %polybench_alloc_data.exit47
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_64
# BB#8:                                 # %polybench_alloc_data.exit49
	xorl	%eax, %eax
	movl	$1, %r8d
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_9:                                # %.preheader3.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_10 Depth 2
                                        #       Child Loop BB6_11 Depth 3
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movq	%r8, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_10:                               # %.preheader2.i
                                        #   Parent Loop BB6_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_11 Depth 3
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm1, %xmm2
	movq	%rdx, %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_11:                               #   Parent Loop BB6_9 Depth=1
                                        #     Parent Loop BB6_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ebp, %xmm3
	addsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	movsd	%xmm3, -8(%r14,%rdi,8)
	movsd	%xmm3, -8(%r15,%rdi,8)
	movl	%ebp, %ecx
	orl	$1, %ecx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	addsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	movsd	%xmm3, (%r14,%rdi,8)
	movsd	%xmm3, (%r15,%rdi,8)
	addq	$2, %rbp
	addq	$2, %rdi
	cmpq	$128, %rbp
	jne	.LBB6_11
# BB#12:                                #   in Loop: Header=BB6_10 Depth=2
	incq	%rsi
	subq	$-128, %rdx
	cmpq	$128, %rsi
	jne	.LBB6_10
# BB#13:                                #   in Loop: Header=BB6_9 Depth=1
	incq	%rax
	addq	$16384, %r8             # imm = 0x4000
	cmpq	$128, %rax
	jne	.LBB6_9
# BB#14:                                # %.preheader.i.preheader
	leaq	24(%rbx), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_15:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_16 Depth 2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movq	%rax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_16:                               #   Parent Loop BB6_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -24(%rdx)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -16(%rdx)
	movl	%esi, %edi
	orl	$2, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%rdx)
	movl	%esi, %edi
	orl	$3, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%rdx)
	addq	$4, %rsi
	addq	$32, %rdx
	cmpq	$128, %rsi
	jne	.LBB6_16
# BB#17:                                #   in Loop: Header=BB6_15 Depth=1
	incq	%rcx
	addq	$1024, %rax             # imm = 0x400
	cmpq	$128, %rcx
	jne	.LBB6_15
# BB#18:                                # %init_array.exit
	leaq	8(%r14), %r8
	leaq	48(%r12), %rdx
	leaq	48(%r14), %rsi
	leaq	24(%r12), %rdi
	leaq	24(%r14), %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_19:                               # %.preheader2.i50
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_20 Depth 2
                                        #       Child Loop BB6_21 Depth 3
                                        #         Child Loop BB6_22 Depth 4
                                        #       Child Loop BB6_27 Depth 3
                                        #       Child Loop BB6_29 Depth 3
	movq	%rax, 48(%rsp)          # 8-byte Spill
	shlq	$17, %rax
	leaq	1024(%rax), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%rbp, %r9
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, %r10
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%rsi, %r11
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%r8, %rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_20:                               # %.preheader1.i
                                        #   Parent Loop BB6_19 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_21 Depth 3
                                        #         Child Loop BB6_22 Depth 4
                                        #       Child Loop BB6_27 Depth 3
                                        #       Child Loop BB6_29 Depth 3
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r11, 40(%rsp)          # 8-byte Spill
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rcx, %r10
	shlq	$10, %r10
	leaq	(%rax,%r10), %r11
	leaq	(%r14,%r11), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r10), %r13
	leaq	(%r14,%r13), %rcx
	addq	%r12, %r11
	addq	%r12, %r13
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_21:                               #   Parent Loop BB6_19 Depth=1
                                        #     Parent Loop BB6_20 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_22 Depth 4
	leaq	(%r12,%rax), %rdi
	addq	%r10, %rdi
	leaq	(%rdi,%rdx,8), %rsi
	movq	$0, (%rdi,%rdx,8)
	xorpd	%xmm0, %xmm0
	movl	$1024, %edi             # imm = 0x400
	movq	%rbp, %r9
	.p2align	4, 0x90
.LBB6_22:                               #   Parent Loop BB6_19 Depth=1
                                        #     Parent Loop BB6_20 Depth=2
                                        #       Parent Loop BB6_21 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-8(%r9), %xmm1          # xmm1 = mem[0],zero
	leaq	(%rbx,%rdi), %r8
	mulsd	-1024(%r8,%rdx,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsi)
	movsd	(%r9), %xmm0            # xmm0 = mem[0],zero
	mulsd	(%r8,%rdx,8), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi)
	addq	$16, %r9
	addq	$2048, %rdi             # imm = 0x800
	cmpq	$132096, %rdi           # imm = 0x20400
	jne	.LBB6_22
# BB#23:                                #   in Loop: Header=BB6_21 Depth=3
	incq	%rdx
	cmpq	$128, %rdx
	jne	.LBB6_21
# BB#24:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_20 Depth=2
	cmpq	%r13, 104(%rsp)         # 8-byte Folded Reload
	jae	.LBB6_26
# BB#25:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_20 Depth=2
	cmpq	%rcx, %r11
	jae	.LBB6_26
# BB#28:                                # %.preheader.i55.preheader
                                        #   in Loop: Header=BB6_20 Depth=2
	movl	$128, %edx
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	%r9, %rsi
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	%r10, %rdi
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_29:                               # %.preheader.i55
                                        #   Parent Loop BB6_19 Depth=1
                                        #     Parent Loop BB6_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rcx
	movq	%rcx, -24(%rsi)
	movq	-16(%rdi), %rcx
	movq	%rcx, -16(%rsi)
	movq	-8(%rdi), %rcx
	movq	%rcx, -8(%rsi)
	movq	(%rdi), %rcx
	movq	%rcx, (%rsi)
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB6_29
	jmp	.LBB6_30
	.p2align	4, 0x90
.LBB6_26:                               # %vector.body.preheader
                                        #   in Loop: Header=BB6_20 Depth=2
	movl	$128, %edx
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	%r11, %rsi
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdi
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_27:                               # %vector.body
                                        #   Parent Loop BB6_19 Depth=1
                                        #     Parent Loop BB6_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movupd	-16(%rdi), %xmm0
	movupd	(%rdi), %xmm1
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	addq	$64, %rdi
	addq	$64, %rsi
	addq	$-8, %rdx
	jne	.LBB6_27
.LBB6_30:                               # %middle.block
                                        #   in Loop: Header=BB6_20 Depth=2
	movq	112(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	addq	$1024, %rbp             # imm = 0x400
	addq	$1024, %r13             # imm = 0x400
	addq	$1024, %r11             # imm = 0x400
	addq	$1024, %r10             # imm = 0x400
	addq	$1024, %r9              # imm = 0x400
	cmpq	$128, %rcx
	jne	.LBB6_20
# BB#31:                                #   in Loop: Header=BB6_19 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	incq	%rax
	movq	88(%rsp), %r8           # 8-byte Reload
	addq	$131072, %r8            # imm = 0x20000
	movq	80(%rsp), %rdx          # 8-byte Reload
	addq	$131072, %rdx           # imm = 0x20000
	movq	72(%rsp), %rsi          # 8-byte Reload
	addq	$131072, %rsi           # imm = 0x20000
	movq	64(%rsp), %rdi          # 8-byte Reload
	addq	$131072, %rdi           # imm = 0x20000
	movq	56(%rsp), %rbp          # 8-byte Reload
	addq	$131072, %rbp           # imm = 0x20000
	cmpq	$128, %rax
	jne	.LBB6_19
# BB#32:                                # %.preheader2.i59.preheader
	leaq	8(%r15), %r8
	leaq	48(%r12), %rdx
	leaq	48(%r15), %rsi
	leaq	24(%r12), %rdi
	leaq	24(%r15), %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_33:                               # %.preheader2.i59
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_34 Depth 2
                                        #       Child Loop BB6_35 Depth 3
                                        #         Child Loop BB6_36 Depth 4
                                        #       Child Loop BB6_41 Depth 3
                                        #       Child Loop BB6_43 Depth 3
	movq	%rax, 48(%rsp)          # 8-byte Spill
	shlq	$17, %rax
	leaq	1024(%rax), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%rbp, %r9
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, %r10
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%rsi, %r11
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%r8, %rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_34:                               # %.preheader1.i61
                                        #   Parent Loop BB6_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_35 Depth 3
                                        #         Child Loop BB6_36 Depth 4
                                        #       Child Loop BB6_41 Depth 3
                                        #       Child Loop BB6_43 Depth 3
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r11, 40(%rsp)          # 8-byte Spill
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rcx, %r10
	shlq	$10, %r10
	leaq	(%rax,%r10), %r11
	leaq	(%r15,%r11), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r10), %r13
	leaq	(%r15,%r13), %rcx
	addq	%r12, %r11
	addq	%r12, %r13
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_35:                               #   Parent Loop BB6_33 Depth=1
                                        #     Parent Loop BB6_34 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_36 Depth 4
	leaq	(%r12,%rax), %rdi
	addq	%r10, %rdi
	leaq	(%rdi,%rdx,8), %rsi
	movq	$0, (%rdi,%rdx,8)
	xorpd	%xmm0, %xmm0
	movl	$1024, %edi             # imm = 0x400
	movq	%rbp, %r9
	.p2align	4, 0x90
.LBB6_36:                               #   Parent Loop BB6_33 Depth=1
                                        #     Parent Loop BB6_34 Depth=2
                                        #       Parent Loop BB6_35 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-8(%r9), %xmm1          # xmm1 = mem[0],zero
	leaq	(%rbx,%rdi), %r8
	mulsd	-1024(%r8,%rdx,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsi)
	movsd	(%r9), %xmm0            # xmm0 = mem[0],zero
	mulsd	(%r8,%rdx,8), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi)
	addq	$16, %r9
	addq	$2048, %rdi             # imm = 0x800
	cmpq	$132096, %rdi           # imm = 0x20400
	jne	.LBB6_36
# BB#37:                                #   in Loop: Header=BB6_35 Depth=3
	incq	%rdx
	cmpq	$128, %rdx
	jne	.LBB6_35
# BB#38:                                # %vector.memcheck132
                                        #   in Loop: Header=BB6_34 Depth=2
	cmpq	%r13, 104(%rsp)         # 8-byte Folded Reload
	jae	.LBB6_40
# BB#39:                                # %vector.memcheck132
                                        #   in Loop: Header=BB6_34 Depth=2
	cmpq	%rcx, %r11
	jae	.LBB6_40
# BB#42:                                # %.preheader.i71.preheader
                                        #   in Loop: Header=BB6_34 Depth=2
	movl	$128, %edx
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	%r9, %rsi
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	%r10, %rdi
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_43:                               # %.preheader.i71
                                        #   Parent Loop BB6_33 Depth=1
                                        #     Parent Loop BB6_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rcx
	movq	%rcx, -24(%rsi)
	movq	-16(%rdi), %rcx
	movq	%rcx, -16(%rsi)
	movq	-8(%rdi), %rcx
	movq	%rcx, -8(%rsi)
	movq	(%rdi), %rcx
	movq	%rcx, (%rsi)
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB6_43
	jmp	.LBB6_44
	.p2align	4, 0x90
.LBB6_40:                               # %vector.body119.preheader
                                        #   in Loop: Header=BB6_34 Depth=2
	movl	$128, %edx
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	%r11, %rsi
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdi
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_41:                               # %vector.body119
                                        #   Parent Loop BB6_33 Depth=1
                                        #     Parent Loop BB6_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movupd	-16(%rdi), %xmm0
	movupd	(%rdi), %xmm1
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	addq	$64, %rdi
	addq	$64, %rsi
	addq	$-8, %rdx
	jne	.LBB6_41
.LBB6_44:                               # %middle.block120
                                        #   in Loop: Header=BB6_34 Depth=2
	movq	112(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	addq	$1024, %rbp             # imm = 0x400
	addq	$1024, %r13             # imm = 0x400
	addq	$1024, %r11             # imm = 0x400
	addq	$1024, %r10             # imm = 0x400
	addq	$1024, %r9              # imm = 0x400
	cmpq	$128, %rcx
	jne	.LBB6_34
# BB#45:                                #   in Loop: Header=BB6_33 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	incq	%rax
	movq	88(%rsp), %r8           # 8-byte Reload
	addq	$131072, %r8            # imm = 0x20000
	movq	80(%rsp), %rdx          # 8-byte Reload
	addq	$131072, %rdx           # imm = 0x20000
	movq	72(%rsp), %rsi          # 8-byte Reload
	addq	$131072, %rsi           # imm = 0x20000
	movq	64(%rsp), %rdi          # 8-byte Reload
	addq	$131072, %rdi           # imm = 0x20000
	movq	56(%rsp), %rbp          # 8-byte Reload
	addq	$131072, %rbp           # imm = 0x20000
	cmpq	$128, %rax
	jne	.LBB6_33
# BB#46:                                # %.preheader1.i76.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_1(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_2(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB6_47:                               # %.preheader1.i76
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_48 Depth 2
                                        #       Child Loop BB6_49 Depth 3
	movq	%rax, %rsi
	xorl	%ecx, %ecx
.LBB6_48:                               # %.preheader.i78
                                        #   Parent Loop BB6_47 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_49 Depth 3
	movq	%rsi, %rdi
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB6_49:                               #   Parent Loop BB6_47 Depth=1
                                        #     Parent Loop BB6_48 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%r14,%rdi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r15,%rdi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_50
# BB#52:                                #   in Loop: Header=BB6_49 Depth=3
	movsd	(%r14,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r15,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_51
# BB#53:                                #   in Loop: Header=BB6_49 Depth=3
	addq	$2, %rdi
	leaq	2(%r8), %rbp
	incq	%r8
	cmpq	$128, %r8
	movq	%rbp, %r8
	jl	.LBB6_49
# BB#54:                                #   in Loop: Header=BB6_48 Depth=2
	incq	%rcx
	subq	$-128, %rsi
	cmpq	$128, %rcx
	jl	.LBB6_48
# BB#55:                                #   in Loop: Header=BB6_47 Depth=1
	incq	%rdx
	addq	$16384, %rax            # imm = 0x4000
	cmpq	$128, %rdx
	jl	.LBB6_47
# BB#56:                                # %check_FP.exit
	movl	$2049, %edi             # imm = 0x801
	callq	malloc
	movq	%rax, %r13
	movb	$0, 2048(%r13)
	xorl	%eax, %eax
	movq	%r15, %rcx
.LBB6_57:                               # %.preheader1.i83
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_58 Depth 2
                                        #       Child Loop BB6_59 Depth 3
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, %rax
	xorl	%ebp, %ebp
.LBB6_58:                               # %.preheader.i84
                                        #   Parent Loop BB6_57 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_59 Depth 3
	movq	%rax, %rdx
	movl	$15, %esi
	.p2align	4, 0x90
.LBB6_59:                               #   Parent Loop BB6_57 Depth=1
                                        #     Parent Loop BB6_58 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdx), %rdi
	movl	%edi, %ecx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, -15(%r13,%rsi)
	movb	%cl, -14(%r13,%rsi)
	movq	%rdi, %rcx
	shrq	$8, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, -13(%r13,%rsi)
	movb	%cl, -12(%r13,%rsi)
	movq	%rdi, %rcx
	shrq	$16, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, -11(%r13,%rsi)
	movb	%cl, -10(%r13,%rsi)
	movl	%edi, %ecx
	shrl	$24, %ecx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, -9(%r13,%rsi)
	movb	%cl, -8(%r13,%rsi)
	movq	%rdi, %rcx
	shrq	$32, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, -7(%r13,%rsi)
	movb	%cl, -6(%r13,%rsi)
	movq	%rdi, %rcx
	shrq	$40, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, -5(%r13,%rsi)
	movb	%cl, -4(%r13,%rsi)
	movq	%rdi, %rcx
	shrq	$48, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, -3(%r13,%rsi)
	movb	%cl, -2(%r13,%rsi)
	shrq	$56, %rdi
	andb	$15, %dil
	orb	$48, %dil
	movb	%dil, -1(%r13,%rsi)
	movb	%dil, (%r13,%rsi)
	addq	$16, %rsi
	addq	$8, %rdx
	cmpq	$2063, %rsi             # imm = 0x80F
	jne	.LBB6_59
# BB#60:                                #   in Loop: Header=BB6_58 Depth=2
	incq	%rbp
	addq	$1024, %rax             # imm = 0x400
	cmpq	$128, %rbp
	jne	.LBB6_58
# BB#61:                                #   in Loop: Header=BB6_57 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r13, %rdi
	callq	fputs
	movq	24(%rsp), %rax          # 8-byte Reload
	incq	%rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	addq	$131072, %rcx           # imm = 0x20000
	cmpq	$128, %rax
	jne	.LBB6_57
# BB#62:                                # %print_array.exit
	movq	%r13, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_63
.LBB6_50:                               # %check_FP.exit.threadsplit
	decq	%r8
.LBB6_51:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r9d
	pushq	%r8
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%rcx
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi16:
	.cfi_adjust_cfa_offset -16
	movl	$1, %eax
.LBB6_63:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_64:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d][%d] = %lf and B[%d][%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 84


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
