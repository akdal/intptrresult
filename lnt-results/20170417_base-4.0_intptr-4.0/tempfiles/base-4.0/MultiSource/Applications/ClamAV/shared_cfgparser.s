	.text
	.file	"shared_cfgparser.bc"
	.globl	getcfg
	.p2align	4, 0x90
	.type	getcfg,@function
getcfg:                                 # @getcfg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1096, %rsp             # imm = 0x448
.Lcfi6:
	.cfi_def_cfa_offset 1152
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r13
	movq	$0, 8(%rsp)
	movq	cfg_options(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB0_8
# BB#1:                                 # %.lr.ph420.preheader
	movl	$cfg_options+32, %ebx
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph420
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	callq	__strdup
	movq	%rax, %rdx
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_3 Depth=1
	xorl	%edx, %edx
.LBB0_6:                                #   in Loop: Header=BB0_3 Depth=1
	movl	-20(%rbx), %ecx
	movswl	-8(%rbx), %r8d
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	regcfg
	testl	%eax, %eax
	js	.LBB0_7
# BB#2:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	(%rbx), %rbp
	addq	$32, %rbx
	testq	%rbp, %rbp
	jne	.LBB0_3
.LBB0_8:                                # %._crit_edge421
	movl	$.L.str.90, %esi
	movq	%r13, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_31
# BB#9:                                 # %.preheader298
	leaq	64(%rsp), %rdi
	movl	$1024, %esi             # imm = 0x400
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB0_13
# BB#10:                                # %.lr.ph.preheader
	leaq	64(%rsp), %r12
	xorl	%ebp, %ebp
	incl	%ebp
	cmpb	$35, 64(%rsp)
	jne	.LBB0_15
	jmp	.LBB0_12
.LBB0_7:
	movq	stderr(%rip), %rcx
	movl	$.L.str.89, %edi
	movl	$54, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_31:
	movq	8(%rsp), %rdi
	callq	freecfg
	xorl	%eax, %eax
	jmp	.LBB0_14
.LBB0_102:                              # %._crit_edge413
	testl	%ebx, %ebx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	20(%rsp), %r15d         # 4-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	je	.LBB0_103
# BB#106:                               # %.outer
	movq	%r14, %rdi
	callq	free
	movl	$1024, %esi             # imm = 0x400
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB0_13
# BB#107:                               # %.lr.ph
	incl	%ebp
	.p2align	4, 0x90
.LBB0_11:
	cmpb	$35, 64(%rsp)
	je	.LBB0_12
.LBB0_15:
	movl	$.L.str.91, %edi
	movl	$7, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_16
# BB#19:
	xorl	%esi, %esi
	movl	$.L.str.93, %edx
	movq	%r12, %rdi
	callq	cli_strtok
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB0_20
.LBB0_12:                               # %.backedge
	movl	$1024, %esi             # imm = 0x400
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	incl	%ebp
	testq	%rax, %rax
	jne	.LBB0_11
	jmp	.LBB0_13
.LBB0_20:
	movl	$1, %esi
	movl	$.L.str.93, %edx
	movq	%r12, %rdi
	callq	cli_strtok
	movq	cfg_options(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB0_103
# BB#21:                                # %.lr.ph412.preheader
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movl	%r15d, 20(%rsp)         # 4-byte Spill
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph412
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_40 Depth 2
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_101
# BB#23:                                #   in Loop: Header=BB0_22 Depth=1
	movq	%r13, %r15
	shlq	$5, %r15
	movzwl	cfg_options+8(%r15), %r8d
	leal	-1(%r8), %eax
	movzwl	%ax, %eax
	cmpl	$5, %eax
	ja	.LBB0_97
# BB#24:                                #   in Loop: Header=BB0_22 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_25:                               #   in Loop: Header=BB0_22 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_26
# BB#32:                                #   in Loop: Header=BB0_22 Depth=1
	movswl	cfg_options+24(%r15), %r8d
	movl	$-1, %ecx
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	jmp	.LBB0_33
.LBB0_57:                               #   in Loop: Header=BB0_22 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_58
# BB#59:                                #   in Loop: Header=BB0_22 Depth=1
	movq	%rbx, %rdi
	callq	isnumb
	testl	%eax, %eax
	je	.LBB0_60
# BB#64:                                #   in Loop: Header=BB0_22 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movswl	cfg_options+24(%r15), %r8d
	xorl	%edx, %edx
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	movl	%eax, %ecx
	callq	regcfg
	testl	%eax, %eax
	js	.LBB0_65
# BB#99:                                #   in Loop: Header=BB0_22 Depth=1
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB0_100
.LBB0_66:                               #   in Loop: Header=BB0_22 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_67
# BB#69:                                #   in Loop: Header=BB0_22 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	strlen
	movsbq	-1(%rbx,%rax), %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx,%rcx,4), %ecx
	cmpl	$109, %ecx
	je	.LBB0_71
# BB#70:                                #   in Loop: Header=BB0_22 Depth=1
	cmpl	$107, %ecx
	jne	.LBB0_78
.LBB0_71:                               #   in Loop: Header=BB0_22 Depth=1
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movl	$1, %esi
	movq	%rax, %rdi
	callq	calloc
	movq	%rax, %rbx
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	leaq	-1(%rax), %rdx
	movq	%rbx, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	strncpy
	movq	%rbx, %rdi
	callq	isnumb
	testl	%eax, %eax
	je	.LBB0_72
# BB#75:                                # %.thread
                                        #   in Loop: Header=BB0_22 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	strtol
	cmpl	$109, 40(%rsp)          # 4-byte Folded Reload
	movb	$20, %cl
	je	.LBB0_77
# BB#76:                                # %.thread
                                        #   in Loop: Header=BB0_22 Depth=1
	movb	$10, %cl
.LBB0_77:                               # %.thread
                                        #   in Loop: Header=BB0_22 Depth=1
	shll	%cl, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_82
.LBB0_83:                               #   in Loop: Header=BB0_22 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_84
# BB#86:                                #   in Loop: Header=BB0_22 Depth=1
	movl	$.L.str.102, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	movl	$1, %ecx
	je	.LBB0_96
# BB#87:                                #   in Loop: Header=BB0_22 Depth=1
	movb	(%rbx), %al
	movb	%al, 40(%rsp)           # 1-byte Spill
	cmpb	$49, %al
	jne	.LBB0_89
# BB#88:                                #   in Loop: Header=BB0_22 Depth=1
	cmpb	$0, 1(%rbx)
	movl	$1, %ecx
	je	.LBB0_96
.LBB0_89:                               # %.thread505
                                        #   in Loop: Header=BB0_22 Depth=1
	movl	$.L.str.104, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	movl	$1, %ecx
	je	.LBB0_96
# BB#90:                                #   in Loop: Header=BB0_22 Depth=1
	movl	$.L.str.105, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB0_96
# BB#91:                                #   in Loop: Header=BB0_22 Depth=1
	cmpb	$48, 40(%rsp)           # 1-byte Folded Reload
	jne	.LBB0_93
# BB#92:                                #   in Loop: Header=BB0_22 Depth=1
	cmpb	$0, 1(%rbx)
	je	.LBB0_96
.LBB0_93:                               # %.thread506
                                        #   in Loop: Header=BB0_22 Depth=1
	movl	$.L.str.107, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jne	.LBB0_94
.LBB0_96:                               #   in Loop: Header=BB0_22 Depth=1
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movq	%rbx, %rdi
	callq	free
	movswl	cfg_options+24(%r15), %r8d
	xorl	%edx, %edx
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	movl	40(%rsp), %ecx          # 4-byte Reload
	callq	regcfg
	testl	%eax, %eax
	js	.LBB0_65
.LBB0_100:                              #   in Loop: Header=BB0_22 Depth=1
	movl	$1, %ebx
	jmp	.LBB0_101
.LBB0_36:                               #   in Loop: Header=BB0_22 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_26
# BB#37:                                #   in Loop: Header=BB0_22 Depth=1
	movq	%rbx, %rdi
	callq	free
	movl	$32, %esi
	movq	%r12, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	callq	__strdup
	testq	%rax, %rax
	je	.LBB0_38
# BB#39:                                # %.preheader.preheader
                                        #   in Loop: Header=BB0_22 Depth=1
	movq	%rax, %r9
	.p2align	4, 0x90
.LBB0_40:                               # %.preheader
                                        #   Parent Loop BB0_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	setne	%dl
	cmpb	$10, %cl
	setne	%bl
	incq	%rax
	cmpb	$13, %cl
	je	.LBB0_42
# BB#41:                                # %.preheader
                                        #   in Loop: Header=BB0_40 Depth=2
	andb	%bl, %dl
	jne	.LBB0_40
.LBB0_42:                               # %__strpbrk_c2.exit
                                        #   in Loop: Header=BB0_22 Depth=1
	cmpq	$1, %rax
	je	.LBB0_45
# BB#43:                                # %__strpbrk_c2.exit
                                        #   in Loop: Header=BB0_22 Depth=1
	testb	%cl, %cl
	je	.LBB0_45
# BB#44:                                #   in Loop: Header=BB0_22 Depth=1
	movb	$0, -1(%rax)
.LBB0_45:                               #   in Loop: Header=BB0_22 Depth=1
	movswl	cfg_options+24(%r15), %r8d
	movl	$-1, %ecx
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	movq	%r9, %rdx
	movq	%rdx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB0_34
.LBB0_46:                               #   in Loop: Header=BB0_22 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_26
# BB#47:                                #   in Loop: Header=BB0_22 Depth=1
	movb	(%rbx), %al
	cmpb	$39, %al
	je	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_22 Depth=1
	cmpb	$34, %al
	jne	.LBB0_52
.LBB0_49:                               #   in Loop: Header=BB0_22 Depth=1
	movq	%rbx, %rdi
	callq	free
	movl	$32, %esi
	movq	%r12, %rdi
	callq	strchr
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	2(%rax), %rdi
	callq	__strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_38
# BB#50:                                #   in Loop: Header=BB0_22 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movsbl	1(%rax), %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB0_53
# BB#51:                                #   in Loop: Header=BB0_22 Depth=1
	movb	$0, (%rax)
.LBB0_52:                               #   in Loop: Header=BB0_22 Depth=1
	movswl	cfg_options+24(%r15), %r8d
	movl	$-1, %ecx
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	movq	%rbx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB0_33:                               #   in Loop: Header=BB0_22 Depth=1
	movq	%rbx, %rdx
	jmp	.LBB0_34
.LBB0_78:                               #   in Loop: Header=BB0_22 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	isnumb
	testl	%eax, %eax
	je	.LBB0_79
# BB#81:                                #   in Loop: Header=BB0_22 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB0_82:                               #   in Loop: Header=BB0_22 Depth=1
	movq	%rbx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	free
	movswl	cfg_options+24(%r15), %r8d
	xorl	%edx, %edx
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	movq	40(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
.LBB0_34:                               #   in Loop: Header=BB0_22 Depth=1
	callq	regcfg
	movl	$1, %ebx
	testl	%eax, %eax
	js	.LBB0_35
.LBB0_101:                              #   in Loop: Header=BB0_22 Depth=1
	incq	%r13
	movq	%r13, %rax
	shlq	$5, %rax
	movq	cfg_options(%rax), %rsi
	testq	%rsi, %rsi
	jne	.LBB0_22
	jmp	.LBB0_102
.LBB0_13:                               # %.outer._crit_edge
	movq	%rbx, %rdi
	callq	fclose
	movq	8(%rsp), %rax
.LBB0_14:
	addq	$1096, %rsp             # imm = 0x448
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_16:
	testl	%r15d, %r15d
	jne	.LBB0_17
.LBB0_18:
	movq	%rbx, %rdi
	callq	fclose
	jmp	.LBB0_31
.LBB0_97:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_56
# BB#98:
	movq	stderr(%rip), %rdi
	movl	$.L.str.108, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movq	%r14, %rcx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	fprintf
	jmp	.LBB0_56
.LBB0_65:
	movq	stderr(%rip), %rcx
	movl	$.L.str.89, %edi
	movl	$54, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_56:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	%r14, %rdi
	callq	free
	movq	%rbx, %rdi
	jmp	.LBB0_30
.LBB0_38:
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB0_35:
	movq	stderr(%rip), %rcx
	movl	$.L.str.89, %edi
	movl	$54, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_74:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	%r14, %rdi
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_30
.LBB0_26:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB0_29
# BB#27:
	movq	stderr(%rip), %rdi
	movl	$.L.str.94, %esi
	jmp	.LBB0_28
.LBB0_17:
	movq	stderr(%rip), %rdi
	movl	$.L.str.92, %esi
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	fprintf
	jmp	.LBB0_18
.LBB0_103:                              # %._crit_edge413.thread
	testl	%r15d, %r15d
	jne	.LBB0_104
.LBB0_105:
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_18
.LBB0_58:
	xorl	%r15d, %r15d
	jmp	.LBB0_61
.LBB0_67:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB0_29
# BB#68:
	movq	stderr(%rip), %rdi
	movl	$.L.str.99, %esi
	jmp	.LBB0_28
.LBB0_84:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB0_29
# BB#85:
	movq	stderr(%rip), %rdi
	movl	$.L.str.101, %esi
.LBB0_28:
	xorl	%eax, %eax
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	fprintf
.LBB0_29:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	%r14, %rdi
	jmp	.LBB0_30
.LBB0_60:
	movq	%rbx, %r15
.LBB0_61:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_62
.LBB0_63:
	movq	%rbx, %rdi
	callq	fclose
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
.LBB0_30:
	callq	free
	jmp	.LBB0_31
.LBB0_72:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB0_74
# BB#73:
	movq	stderr(%rip), %rdi
	movl	$.L.str.100, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	fprintf
	jmp	.LBB0_74
.LBB0_53:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB0_56
# BB#54:
	movq	stderr(%rip), %rdi
	movl	$.L.str.97, %esi
	jmp	.LBB0_55
.LBB0_79:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB0_56
# BB#80:
	movq	stderr(%rip), %rdi
	movl	$.L.str.100, %esi
	jmp	.LBB0_55
.LBB0_94:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB0_56
# BB#95:
	movq	stderr(%rip), %rdi
	movl	$.L.str.101, %esi
.LBB0_55:
	xorl	%eax, %eax
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	fprintf
	jmp	.LBB0_56
.LBB0_62:
	movq	stderr(%rip), %rdi
	movl	$.L.str.98, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	fprintf
	jmp	.LBB0_63
.LBB0_104:
	movq	stderr(%rip), %rdi
	movl	$.L.str.109, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	fprintf
	jmp	.LBB0_105
.Lfunc_end0:
	.size	getcfg, .Lfunc_end0-getcfg
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_25
	.quad	.LBB0_57
	.quad	.LBB0_66
	.quad	.LBB0_83
	.quad	.LBB0_36
	.quad	.LBB0_46

	.text
	.p2align	4, 0x90
	.type	regcfg,@function
regcfg:                                 # @regcfg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r8d, %r12d
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movq	%rsi, %rbp
	movq	%rdi, %r13
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	testq	%rbp, %rbp
	je	.LBB1_3
# BB#4:
	movq	%rbp, %rdi
	callq	__strdup
	jmp	.LBB1_5
.LBB1_1:
	movl	$-1, %eax
	jmp	.LBB1_30
.LBB1_3:
	xorl	%eax, %eax
.LBB1_5:
	movq	%rax, (%rbx)
	movw	$0, 20(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movw	%r12w, 22(%rbx)
	testq	%r15, %r15
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB1_7
# BB#6:
	movq	%r15, 8(%rbx)
	movw	$1, 20(%rbx)
	movw	$1, %cx
	jmp	.LBB1_8
.LBB1_7:
	movq	$0, 8(%rbx)
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.LBB1_8:
	movl	%r14d, 16(%rbx)
	movq	%r14, 16(%rsp)          # 8-byte Spill
	leal	1(%r14), %eax
	cmpl	$2, %eax
	jb	.LBB1_10
# BB#9:
	movw	$1, 20(%rbx)
	movw	$1, %cx
.LBB1_10:
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	(%r13), %r14
	testq	%r14, %r14
	je	.LBB1_15
# BB#11:                                # %.lr.ph.i.preheader
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_12 Depth=1
	movq	%rbp, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_16
.LBB1_14:                               #   in Loop: Header=BB1_12 Depth=1
	movq	32(%r12), %r12
	testq	%r12, %r12
	jne	.LBB1_12
.LBB1_15:                               # %.loopexit
	movq	%r14, 32(%rbx)
	movq	%rbx, (%r13)
	movl	$1, %eax
.LBB1_30:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_16:                               # %cfgopt_i.exit
	cmpw	$0, 22(%r12)
	je	.LBB1_25
# BB#17:
	cmpw	$0, 20(%r12)
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB1_20
	.p2align	4, 0x90
.LBB1_18:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rax
	movq	24(%rax), %r12
	testq	%r12, %r12
	jne	.LBB1_18
# BB#19:
	movq	%rbx, 24(%rax)
	movl	$3, %eax
	jmp	.LBB1_30
.LBB1_25:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB1_27
# BB#26:
	callq	free
	movq	(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rbx), %r15
	movl	16(%rbx), %eax
	movw	20(%rbx), %cx
	movq	%rcx, (%rsp)            # 8-byte Spill
.LBB1_27:                               # %._crit_edge67
	movq	%r15, 8(%r12)
	movl	%eax, 16(%r12)
	movq	(%rsp), %rax            # 8-byte Reload
	movw	%ax, 20(%r12)
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB1_29
# BB#28:
	callq	free
.LBB1_29:
	movq	%rbx, %rdi
	callq	free
	movl	$2, %eax
	jmp	.LBB1_30
.LBB1_20:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB1_22
# BB#21:
	callq	free
	movq	(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rbx), %r15
	movl	16(%rbx), %eax
	movw	20(%rbx), %cx
	movq	%rcx, (%rsp)            # 8-byte Spill
.LBB1_22:                               # %._crit_edge
	movq	%r15, 8(%r12)
	movl	%eax, 16(%r12)
	movq	(%rsp), %rax            # 8-byte Reload
	movw	%ax, 20(%r12)
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB1_24
# BB#23:
	callq	free
.LBB1_24:
	movq	%rbx, %rdi
	callq	free
	movl	$3, %eax
	jmp	.LBB1_30
.Lfunc_end1:
	.size	regcfg, .Lfunc_end1-regcfg
	.cfi_endproc

	.globl	freecfg
	.p2align	4, 0x90
	.type	freecfg,@function
freecfg:                                # @freecfg
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB2_11
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph24
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_3
	jmp	.LBB2_6
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_3 Depth=2
	movq	(%rbx), %rdi
	callq	free
	movq	8(%rbx), %rdi
	callq	free
	movq	24(%rbx), %r15
	movq	%rbx, %rdi
	callq	free
	movq	%r15, %rbx
	testq	%rbx, %rbx
	je	.LBB2_6
.LBB2_3:                                # %.lr.ph
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, 8(%rbx)
	jne	.LBB2_4
# BB#5:                                 #   in Loop: Header=BB2_3 Depth=2
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_3
.LBB2_6:                                # %._crit_edge
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_1 Depth=1
	callq	free
.LBB2_8:                                #   in Loop: Header=BB2_1 Depth=1
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_1 Depth=1
	callq	free
.LBB2_10:                               #   in Loop: Header=BB2_1 Depth=1
	movq	32(%r14), %rbx
	movq	%r14, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %r14
	jne	.LBB2_1
.LBB2_11:                               # %._crit_edge25
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	freecfg, .Lfunc_end2-freecfg
	.cfi_endproc

	.globl	cfgopt
	.p2align	4, 0x90
	.type	cfgopt,@function
cfgopt:                                 # @cfgopt
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB3_2
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB3_5
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	%r14, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_4
	jmp	.LBB3_6
.LBB3_5:
	xorl	%ebx, %ebx
.LBB3_6:                                # %._crit_edge
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	cfgopt, .Lfunc_end3-cfgopt
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"LogFile"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"LogFileUnlock"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"LogFileMaxSize"
	.size	.L.str.2, 15

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"LogTime"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"LogClean"
	.size	.L.str.4, 9

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"LogVerbose"
	.size	.L.str.5, 11

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"LogSyslog"
	.size	.L.str.6, 10

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"LogFacility"
	.size	.L.str.7, 12

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"LOG_LOCAL6"
	.size	.L.str.8, 11

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"PidFile"
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"TemporaryDirectory"
	.size	.L.str.10, 19

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"ScanPE"
	.size	.L.str.11, 7

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"ScanELF"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"DetectBrokenExecutables"
	.size	.L.str.13, 24

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"ScanMail"
	.size	.L.str.14, 9

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"MailFollowURLs"
	.size	.L.str.15, 15

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"MailMaxRecursion"
	.size	.L.str.16, 17

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"PhishingSignatures"
	.size	.L.str.17, 19

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"PhishingScanURLs"
	.size	.L.str.18, 17

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"PhishingAlwaysBlockCloak"
	.size	.L.str.19, 25

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"PhishingAlwaysBlockSSLMismatch"
	.size	.L.str.20, 31

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"PhishingRestrictedScan"
	.size	.L.str.21, 23

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"DetectPUA"
	.size	.L.str.22, 10

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"AlgorithmicDetection"
	.size	.L.str.23, 21

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"ScanHTML"
	.size	.L.str.24, 9

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"ScanOLE2"
	.size	.L.str.25, 9

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"ScanPDF"
	.size	.L.str.26, 8

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"ScanArchive"
	.size	.L.str.27, 12

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"ArchiveMaxFileSize"
	.size	.L.str.28, 19

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"ArchiveMaxRecursion"
	.size	.L.str.29, 20

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"ArchiveMaxFiles"
	.size	.L.str.30, 16

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"ArchiveMaxCompressionRatio"
	.size	.L.str.31, 27

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"ArchiveLimitMemoryUsage"
	.size	.L.str.32, 24

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"ArchiveBlockEncrypted"
	.size	.L.str.33, 22

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"ArchiveBlockMax"
	.size	.L.str.34, 16

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"DatabaseDirectory"
	.size	.L.str.35, 18

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"/usr/local/share/clamav"
	.size	.L.str.36, 24

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"TCPAddr"
	.size	.L.str.37, 8

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"TCPSocket"
	.size	.L.str.38, 10

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"LocalSocket"
	.size	.L.str.39, 12

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"MaxConnectionQueueLength"
	.size	.L.str.40, 25

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"StreamMaxLength"
	.size	.L.str.41, 16

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"StreamMinPort"
	.size	.L.str.42, 14

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"StreamMaxPort"
	.size	.L.str.43, 14

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"MaxThreads"
	.size	.L.str.44, 11

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"ReadTimeout"
	.size	.L.str.45, 12

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"IdleTimeout"
	.size	.L.str.46, 12

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"MaxDirectoryRecursion"
	.size	.L.str.47, 22

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"FollowDirectorySymlinks"
	.size	.L.str.48, 24

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"FollowFileSymlinks"
	.size	.L.str.49, 19

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"ExitOnOOM"
	.size	.L.str.50, 10

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Foreground"
	.size	.L.str.51, 11

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"Debug"
	.size	.L.str.52, 6

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"LeaveTemporaryFiles"
	.size	.L.str.53, 20

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"FixStaleSocket"
	.size	.L.str.54, 15

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"User"
	.size	.L.str.55, 5

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"AllowSupplementaryGroups"
	.size	.L.str.56, 25

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"SelfCheck"
	.size	.L.str.57, 10

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"VirusEvent"
	.size	.L.str.58, 11

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"ClamukoScanOnAccess"
	.size	.L.str.59, 20

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"ClamukoScanOnOpen"
	.size	.L.str.60, 18

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"ClamukoScanOnClose"
	.size	.L.str.61, 19

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"ClamukoScanOnExec"
	.size	.L.str.62, 18

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"ClamukoIncludePath"
	.size	.L.str.63, 19

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"ClamukoExcludePath"
	.size	.L.str.64, 19

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"ClamukoMaxFileSize"
	.size	.L.str.65, 19

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"DatabaseOwner"
	.size	.L.str.66, 14

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"clamav"
	.size	.L.str.67, 7

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"Checks"
	.size	.L.str.68, 7

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"UpdateLogFile"
	.size	.L.str.69, 14

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"DNSDatabaseInfo"
	.size	.L.str.70, 16

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"current.cvd.clamav.net"
	.size	.L.str.71, 23

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"DatabaseMirror"
	.size	.L.str.72, 15

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"MaxAttempts"
	.size	.L.str.73, 12

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"ScriptedUpdates"
	.size	.L.str.74, 16

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"HTTPProxyServer"
	.size	.L.str.75, 16

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"HTTPProxyPort"
	.size	.L.str.76, 14

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"HTTPProxyUsername"
	.size	.L.str.77, 18

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"HTTPProxyPassword"
	.size	.L.str.78, 18

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"HTTPUserAgent"
	.size	.L.str.79, 14

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"NotifyClamd"
	.size	.L.str.80, 12

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"OnUpdateExecute"
	.size	.L.str.81, 16

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"OnErrorExecute"
	.size	.L.str.82, 15

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"OnOutdatedExecute"
	.size	.L.str.83, 18

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"LocalIPAddress"
	.size	.L.str.84, 15

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"ConnectTimeout"
	.size	.L.str.85, 15

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"ReceiveTimeout"
	.size	.L.str.86, 15

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"DevACOnly"
	.size	.L.str.87, 10

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"DevACDepth"
	.size	.L.str.88, 11

	.type	cfg_options,@object     # @cfg_options
	.data
	.globl	cfg_options
	.p2align	4
cfg_options:
	.quad	.L.str
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.1
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.2
	.short	3                       # 0x3
	.zero	2
	.long	1048576                 # 0x100000
	.quad	0
	.short	0                       # 0x0
	.short	3                       # 0x3
	.zero	4
	.quad	.L.str.3
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	3                       # 0x3
	.zero	4
	.quad	.L.str.4
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.5
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	3                       # 0x3
	.zero	4
	.quad	.L.str.6
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	3                       # 0x3
	.zero	4
	.quad	.L.str.7
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	.L.str.8
	.short	0                       # 0x0
	.short	3                       # 0x3
	.zero	4
	.quad	.L.str.9
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	3                       # 0x3
	.zero	4
	.quad	.L.str.10
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.11
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.12
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.13
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.14
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.15
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.16
	.short	2                       # 0x2
	.zero	2
	.long	64                      # 0x40
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.17
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.18
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.19
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.20
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.21
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.22
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.23
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.24
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.25
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.26
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.27
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.28
	.short	3                       # 0x3
	.zero	2
	.long	10485760                # 0xa00000
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.29
	.short	2                       # 0x2
	.zero	2
	.long	8                       # 0x8
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.30
	.short	2                       # 0x2
	.zero	2
	.long	1000                    # 0x3e8
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.31
	.short	2                       # 0x2
	.zero	2
	.long	250                     # 0xfa
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.32
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.33
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.34
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.35
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	.L.str.36
	.short	0                       # 0x0
	.short	3                       # 0x3
	.zero	4
	.quad	.L.str.37
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.38
	.short	2                       # 0x2
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.39
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.40
	.short	2                       # 0x2
	.zero	2
	.long	15                      # 0xf
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.41
	.short	3                       # 0x3
	.zero	2
	.long	10485760                # 0xa00000
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.42
	.short	2                       # 0x2
	.zero	2
	.long	1024                    # 0x400
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.43
	.short	2                       # 0x2
	.zero	2
	.long	2048                    # 0x800
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.44
	.short	2                       # 0x2
	.zero	2
	.long	10                      # 0xa
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.45
	.short	2                       # 0x2
	.zero	2
	.long	120                     # 0x78
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.46
	.short	2                       # 0x2
	.zero	2
	.long	30                      # 0x1e
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.47
	.short	2                       # 0x2
	.zero	2
	.long	15                      # 0xf
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.48
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.49
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.50
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.51
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	3                       # 0x3
	.zero	4
	.quad	.L.str.52
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	3                       # 0x3
	.zero	4
	.quad	.L.str.53
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.54
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.55
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.56
	.short	4                       # 0x4
	.zero	2
	.long	0                       # 0x0
	.quad	0
	.short	0                       # 0x0
	.short	3                       # 0x3
	.zero	4
	.quad	.L.str.57
	.short	2                       # 0x2
	.zero	2
	.long	1800                    # 0x708
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.58
	.short	5                       # 0x5
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.59
	.short	4                       # 0x4
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.60
	.short	4                       # 0x4
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.61
	.short	4                       # 0x4
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.62
	.short	4                       # 0x4
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.63
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.64
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.65
	.short	3                       # 0x3
	.zero	2
	.long	5242880                 # 0x500000
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.66
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	.L.str.67
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.68
	.short	2                       # 0x2
	.zero	2
	.long	12                      # 0xc
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.69
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.70
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	.L.str.71
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.72
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	1                       # 0x1
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.73
	.short	2                       # 0x2
	.zero	2
	.long	3                       # 0x3
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.74
	.short	4                       # 0x4
	.zero	2
	.long	1                       # 0x1
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.75
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.76
	.short	2                       # 0x2
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.77
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.78
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.79
	.short	5                       # 0x5
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.80
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.81
	.short	5                       # 0x5
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.82
	.short	5                       # 0x5
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.83
	.short	5                       # 0x5
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.84
	.short	6                       # 0x6
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.85
	.short	2                       # 0x2
	.zero	2
	.long	30                      # 0x1e
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.86
	.short	2                       # 0x2
	.zero	2
	.long	30                      # 0x1e
	.quad	0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.zero	4
	.quad	.L.str.87
	.short	4                       # 0x4
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.quad	.L.str.88
	.short	2                       # 0x2
	.zero	2
	.long	4294967295              # 0xffffffff
	.quad	0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.zero	4
	.zero	32
	.size	cfg_options, 2752

	.type	.L.str.89,@object       # @.str.89
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.89:
	.asciz	"ERROR: Can't register new options (not enough memory)\n"
	.size	.L.str.89, 55

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"rb"
	.size	.L.str.90, 3

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"Example"
	.size	.L.str.91, 8

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"ERROR: Please edit the example config file %s.\n"
	.size	.L.str.92, 48

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	" \r\n"
	.size	.L.str.93, 4

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"ERROR: Parse error at line %d: Option %s requires string argument.\n"
	.size	.L.str.94, 68

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"ERROR: Parse error at line %d: Option %s missing closing quote.\n"
	.size	.L.str.97, 65

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"ERROR: Parse error at line %d: Option %s requires numerical argument.\n"
	.size	.L.str.98, 71

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"ERROR: Parse error at line %d: Option %s requires argument.\n"
	.size	.L.str.99, 61

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"ERROR: Parse error at line %d: Option %s requires numerical (raw/K/M) argument.\n"
	.size	.L.str.100, 81

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"ERROR: Parse error at line %d: Option %s requires boolean argument.\n"
	.size	.L.str.101, 69

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"yes"
	.size	.L.str.102, 4

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"true"
	.size	.L.str.104, 5

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"no"
	.size	.L.str.105, 3

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"false"
	.size	.L.str.107, 6

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"ERROR: Parse error at line %d: Option %s is of unknown type %d\n"
	.size	.L.str.108, 64

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"ERROR: Parse error at line %d: Unknown option %s.\n"
	.size	.L.str.109, 51


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
