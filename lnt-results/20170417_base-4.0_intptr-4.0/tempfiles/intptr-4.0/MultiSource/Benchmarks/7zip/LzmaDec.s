	.text
	.file	"LzmaDec.bc"
	.globl	LzmaDec_InitDicAndState
	.p2align	4, 0x90
	.type	LzmaDec_InitDicAndState,@function
LzmaDec_InitDicAndState:                # @LzmaDec_InitDicAndState
	.cfi_startproc
# BB#0:
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, 92(%rdi)
	movl	$0, 108(%rdi)
	testl	%esi, %esi
	je	.LBB0_2
# BB#1:
	movq	$0, 64(%rdi)
	movl	$1, 100(%rdi)
.LBB0_2:
	testl	%edx, %edx
	je	.LBB0_4
# BB#3:
	movl	$1, 100(%rdi)
.LBB0_4:
	retq
.Lfunc_end0:
	.size	LzmaDec_InitDicAndState, .Lfunc_end0-LzmaDec_InitDicAndState
	.cfi_endproc

	.globl	LzmaDec_Init
	.p2align	4, 0x90
	.type	LzmaDec_Init,@function
LzmaDec_Init:                           # @LzmaDec_Init
	.cfi_startproc
# BB#0:
	movq	$0, 48(%rdi)
	movl	$1, 96(%rdi)
	movl	$0, 92(%rdi)
	movl	$0, 108(%rdi)
	movq	$0, 64(%rdi)
	movl	$1, 100(%rdi)
	retq
.Lfunc_end1:
	.size	LzmaDec_Init, .Lfunc_end1-LzmaDec_Init
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
.LCPI2_1:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	LzmaDec_DecodeToDic
	.p2align	4, 0x90
	.type	LzmaDec_DecodeToDic,@function
LzmaDec_DecodeToDic:                    # @LzmaDec_DecodeToDic
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	movq	%rcx, %r11
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	movq	(%r11), %r10
	movq	$0, (%r11)
	movl	92(%r12), %eax
	leal	-1(%rax), %ecx
	cmpl	$272, %ecx              # imm = 0x110
	movq	%r11, 32(%rsp)          # 8-byte Spill
	ja	.LBB2_13
# BB#1:
	movq	%r9, %r13
	movq	24(%r12), %rsi
	movq	48(%r12), %r9
	movq	56(%r12), %r14
	movl	76(%r12), %r15d
	movq	16(%rsp), %r11          # 8-byte Reload
	subq	%r9, %r11
	cmpq	%rax, %r11
	cmovael	%eax, %r11d
	cmpl	$0, 68(%r12)
	je	.LBB2_3
# BB#2:                                 # %._crit_edge45.i
	leaq	64(%r12), %rcx
	movl	64(%r12), %edi
	jmp	.LBB2_5
.LBB2_3:
	movl	12(%r12), %r8d
	movl	64(%r12), %edi
	leaq	64(%r12), %rcx
	movl	%r8d, %ebp
	subl	%edi, %ebp
	cmpl	%r11d, %ebp
	ja	.LBB2_5
# BB#4:
	movl	%r8d, 68(%r12)
.LBB2_5:
	addl	%r11d, %edi
	movl	%edi, (%rcx)
	subl	%r11d, %eax
	movl	%eax, 92(%r12)
	testl	%r11d, %r11d
	je	.LBB2_12
# BB#6:                                 # %.lr.ph.i
	leal	-1(%r11), %r8d
	testb	$1, %r11b
	movq	%r9, %rax
	je	.LBB2_8
# BB#7:
	xorl	%eax, %eax
	movq	%r9, %rcx
	subq	%r15, %rcx
	cmovbq	%r14, %rax
	addq	%rcx, %rax
	movb	(%rsi,%rax), %al
	movb	%al, (%rsi,%r9)
	leaq	1(%r9), %rax
	movl	%r8d, %r11d
.LBB2_8:                                # %.prol.loopexit
	testl	%r8d, %r8d
	je	.LBB2_11
# BB#9:                                 # %.lr.ph.i.new
	movq	%r15, %rbp
	negq	%rbp
	addq	%rax, %rsi
	movl	%r11d, %r11d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_10:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rax,%rcx), %rbx
	cmpq	%r15, %rbx
	movl	$0, %ebx
	cmovbq	%r14, %rbx
	addq	%rbp, %rbx
	addq	%rcx, %rbx
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, (%rsi,%rcx)
	leaq	1(%rax,%rcx), %rbx
	cmpq	%r15, %rbx
	movl	$0, %ebx
	cmovbq	%r14, %rbx
	leaq	(%rbp,%rcx), %rdi
	addq	%rbx, %rdi
	movzbl	1(%rsi,%rdi), %ebx
	movb	%bl, 1(%rsi,%rcx)
	addq	$2, %rcx
	cmpl	%ecx, %r11d
	jne	.LBB2_10
.LBB2_11:                               # %._crit_edge.loopexit.i
	movl	%r8d, %eax
	leaq	1(%r9,%rax), %r9
.LBB2_12:                               # %._crit_edge.i
	movq	%r9, 48(%r12)
	movq	%r13, %r9
	movq	32(%rsp), %r11          # 8-byte Reload
.LBB2_13:                               # %LzmaDec_WriteRem.exit
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movl	$0, (%r9)
	movl	92(%r12), %ecx
	cmpl	$274, %ecx              # imm = 0x112
	jne	.LBB2_19
# BB#14:                                # %LzmaDec_WriteRem.exit.._crit_edge_crit_edge
	addq	$44, %r12
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB2_15:                               # %._crit_edge
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jne	.LBB2_17
# BB#16:
	movl	$1, (%rax)
	movl	(%r12), %ecx
.LBB2_17:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setne	%al
.LBB2_18:                               # %.thread180
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_19:                               # %.lr.ph222
	leaq	112(%r12), %r8
	leaq	44(%r12), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024,1024,1024,1024,1024]
	movq	%r8, 24(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB2_20:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_53 Depth 2
                                        #     Child Loop BB2_57 Depth 2
                                        #     Child Loop BB2_62 Depth 2
	cmpl	$0, 96(%r12)
	je	.LBB2_34
# BB#21:                                # %.preheader182
                                        #   in Loop: Header=BB2_20 Depth=1
	testq	%r10, %r10
	movl	108(%r12), %esi
	je	.LBB2_35
# BB#22:                                # %.lr.ph
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpl	$4, %esi
	ja	.LBB2_36
# BB#23:                                #   in Loop: Header=BB2_20 Depth=1
	leaq	1(%rdx), %r14
	movb	(%rdx), %al
	leal	1(%rsi), %edi
	movl	%edi, 108(%r12)
	movb	%al, 112(%r12,%rsi)
	incq	(%r11)
	movq	%r10, %rbx
	decq	%rbx
	je	.LBB2_37
# BB#24:                                # %.lr.ph.1
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpl	$4, %edi
	ja	.LBB2_40
# BB#25:                                #   in Loop: Header=BB2_20 Depth=1
	leaq	2(%rdx), %r14
	movb	1(%rdx), %bl
	leal	2(%rsi), %eax
	movl	%eax, 108(%r12)
	movl	%edi, %edi
	movb	%bl, 112(%r12,%rdi)
	incq	(%r11)
	movq	%r10, %rbx
	addq	$-2, %rbx
	je	.LBB2_38
# BB#26:                                # %.lr.ph.2
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpl	$4, %eax
	ja	.LBB2_40
# BB#27:                                #   in Loop: Header=BB2_20 Depth=1
	leaq	3(%rdx), %r14
	movb	2(%rdx), %bl
	leal	3(%rsi), %edi
	movl	%edi, 108(%r12)
	movl	%eax, %eax
	movb	%bl, 112(%r12,%rax)
	incq	(%r11)
	movq	%r10, %rbx
	addq	$-3, %rbx
	je	.LBB2_37
# BB#28:                                # %.lr.ph.3
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpl	$4, %edi
	ja	.LBB2_40
# BB#29:                                #   in Loop: Header=BB2_20 Depth=1
	leaq	4(%rdx), %r14
	movb	3(%rdx), %al
	leal	4(%rsi), %ebp
	movl	%ebp, 108(%r12)
	movl	%edi, %edi
	movb	%al, 112(%r12,%rdi)
	incq	(%r11)
	movq	%r10, %rbx
	addq	$-4, %rbx
	je	.LBB2_80
# BB#30:                                # %.lr.ph.4
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpl	$4, %ebp
	ja	.LBB2_40
# BB#31:                                #   in Loop: Header=BB2_20 Depth=1
	leaq	5(%rdx), %r14
	movb	4(%rdx), %bl
	leal	5(%rsi), %eax
	movl	%eax, 108(%r12)
	movl	%ebp, %edi
	movb	%bl, 112(%r12,%rdi)
	incq	(%r11)
	addq	$-5, %r10
	je	.LBB2_38
# BB#32:                                # %.lr.ph.5
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpl	$-5, %esi
	movq	%r10, %rbx
	jb	.LBB2_40
# BB#33:                                #   in Loop: Header=BB2_20 Depth=1
	movb	5(%rdx), %bl
	addq	$6, %rdx
	addl	$6, %esi
	movl	%esi, 108(%r12)
	movl	%eax, %eax
	movb	%bl, 112(%r12,%rax)
	incq	(%r11)
	.p2align	4, 0x90
.LBB2_35:                               #   in Loop: Header=BB2_20 Depth=1
	movq	%rdx, %r14
	cmpl	$4, %esi
	ja	.LBB2_39
	jmp	.LBB2_82
	.p2align	4, 0x90
.LBB2_34:                               #   in Loop: Header=BB2_20 Depth=1
	movq	%r10, %rbx
	movq	%rdx, %r14
	jmp	.LBB2_42
.LBB2_36:                               #   in Loop: Header=BB2_20 Depth=1
	movq	%rdx, %r14
	movq	%r10, %rbx
	cmpb	$0, (%r8)
	je	.LBB2_41
	jmp	.LBB2_84
.LBB2_37:                               #   in Loop: Header=BB2_20 Depth=1
	movl	%edi, %esi
	cmpl	$4, %esi
	ja	.LBB2_39
	jmp	.LBB2_82
.LBB2_38:                               #   in Loop: Header=BB2_20 Depth=1
	movl	%eax, %esi
	cmpl	$4, %esi
	jbe	.LBB2_82
	.p2align	4, 0x90
.LBB2_39:                               #   in Loop: Header=BB2_20 Depth=1
	xorl	%ebx, %ebx
.LBB2_40:                               # %.critedge.thread
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpb	$0, (%r8)
	jne	.LBB2_84
.LBB2_41:                               #   in Loop: Header=BB2_20 Depth=1
	movzbl	113(%r12), %eax
	shll	$24, %eax
	movzbl	114(%r12), %edx
	shll	$16, %edx
	orl	%eax, %edx
	movzbl	115(%r12), %eax
	shll	$8, %eax
	orl	%edx, %eax
	movzbl	116(%r12), %edx
	orl	%eax, %edx
	movl	%edx, 44(%r12)
	movl	$-1, 40(%r12)
	movl	$0, 96(%r12)
	movl	$0, 108(%r12)
.LBB2_42:                               #   in Loop: Header=BB2_20 Depth=1
	xorl	%r9d, %r9d
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 48(%r12)
	jb	.LBB2_47
# BB#43:                                #   in Loop: Header=BB2_20 Depth=1
	testl	%ecx, %ecx
	jne	.LBB2_45
# BB#44:                                #   in Loop: Header=BB2_20 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB2_91
.LBB2_45:                               #   in Loop: Header=BB2_20 Depth=1
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	je	.LBB2_87
# BB#46:                                #   in Loop: Header=BB2_20 Depth=1
	movl	$1, %r9d
	testl	%ecx, %ecx
	jne	.LBB2_88
.LBB2_47:                               #   in Loop: Header=BB2_20 Depth=1
	cmpl	$0, 100(%r12)
	je	.LBB2_59
# BB#48:                                #   in Loop: Header=BB2_20 Depth=1
	movl	4(%r12), %ecx
	addl	(%r12), %ecx
	movl	$768, %edx              # imm = 0x300
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	addl	$1846, %edx             # imm = 0x736
	je	.LBB2_58
# BB#49:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB2_20 Depth=1
	movq	16(%r12), %rcx
	movl	%edx, %eax
	cmpl	$16, %edx
	jb	.LBB2_55
# BB#51:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_20 Depth=1
	andl	$15, %edx
	movq	%rax, %rbp
	subq	%rdx, %rbp
	je	.LBB2_55
# BB#52:                                # %vector.body.preheader
                                        #   in Loop: Header=BB2_20 Depth=1
	leaq	16(%rcx), %rdi
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB2_53:                               # %vector.body
                                        #   Parent Loop BB2_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm0, -16(%rdi)
	movups	%xmm0, (%rdi)
	addq	$32, %rdi
	addq	$-16, %rsi
	jne	.LBB2_53
# BB#54:                                # %middle.block
                                        #   in Loop: Header=BB2_20 Depth=1
	testl	%edx, %edx
	jne	.LBB2_56
	jmp	.LBB2_58
	.p2align	4, 0x90
.LBB2_55:                               #   in Loop: Header=BB2_20 Depth=1
	xorl	%ebp, %ebp
.LBB2_56:                               # %.lr.ph.i168.preheader
                                        #   in Loop: Header=BB2_20 Depth=1
	subq	%rbp, %rax
	leaq	(%rcx,%rbp,2), %rcx
	.p2align	4, 0x90
.LBB2_57:                               # %.lr.ph.i168
                                        #   Parent Loop BB2_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	$1024, (%rcx)           # imm = 0x400
	addq	$2, %rcx
	decq	%rax
	jne	.LBB2_57
.LBB2_58:                               # %LzmaDec_InitStateReal.exit
                                        #   in Loop: Header=BB2_20 Depth=1
	movl	$1, 88(%r12)
	movaps	.LCPI2_1(%rip), %xmm0   # xmm0 = [0,1,1,1]
	movups	%xmm0, 72(%r12)
	movl	$0, 100(%r12)
.LBB2_59:                               #   in Loop: Header=BB2_20 Depth=1
	movl	108(%r12), %ebp
	testq	%rbp, %rbp
	je	.LBB2_66
# BB#60:                                # %.preheader
                                        #   in Loop: Header=BB2_20 Depth=1
	xorl	%r13d, %r13d
	cmpl	$19, %ebp
	ja	.LBB2_65
# BB#61:                                # %.lr.ph216.preheader
                                        #   in Loop: Header=BB2_20 Depth=1
	leaq	(%r8,%rbp), %rax
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_62:                               # %.lr.ph216
                                        #   Parent Loop BB2_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, %r13
	jae	.LBB2_74
# BB#63:                                #   in Loop: Header=BB2_62 Depth=2
	movzbl	(%r14,%r13), %ecx
	movb	%cl, (%rax,%r13)
	leaq	1(%rbp,%r13), %rcx
	incq	%r13
	cmpq	$20, %rcx
	jb	.LBB2_62
# BB#64:                                # %.critedge8.loopexit
                                        #   in Loop: Header=BB2_20 Depth=1
	addl	%r13d, %ebp
.LBB2_65:                               # %.critedge8
                                        #   in Loop: Header=BB2_20 Depth=1
	movl	%ebp, 108(%r12)
	movb	$1, %r15b
	testl	%r9d, %r9d
	jne	.LBB2_75
	jmp	.LBB2_77
	.p2align	4, 0x90
.LBB2_66:                               #   in Loop: Header=BB2_20 Depth=1
	movq	%r11, %r15
	cmpq	$20, %rbx
	jb	.LBB2_69
# BB#67:                                #   in Loop: Header=BB2_20 Depth=1
	testl	%r9d, %r9d
	jne	.LBB2_69
# BB#68:                                #   in Loop: Header=BB2_20 Depth=1
	leaq	-20(%r14,%rbx), %rdx
	jmp	.LBB2_72
.LBB2_69:                               #   in Loop: Header=BB2_20 Depth=1
	movl	%r9d, %ebp
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	LzmaDec_TryDummy
	testl	%eax, %eax
	je	.LBB2_93
# BB#70:                                #   in Loop: Header=BB2_20 Depth=1
	testl	%ebp, %ebp
	movq	%r14, %rdx
	je	.LBB2_72
# BB#71:                                #   in Loop: Header=BB2_20 Depth=1
	cmpl	$2, %eax
	movq	%r14, %rdx
	jne	.LBB2_85
.LBB2_72:                               # %.thread
                                        #   in Loop: Header=BB2_20 Depth=1
	movq	%r14, 32(%r12)
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	LzmaDec_DecodeReal2
	testl	%eax, %eax
	jne	.LBB2_90
# BB#73:                                #   in Loop: Header=BB2_20 Depth=1
	movq	32(%r12), %rdx
	movq	%rdx, %r13
	subq	%r14, %r13
	movq	%r15, %r11
	addq	%r13, (%r11)
	movq	24(%rsp), %r8           # 8-byte Reload
	jmp	.LBB2_79
.LBB2_74:                               # %.critedge8.thread
                                        #   in Loop: Header=BB2_20 Depth=1
	addl	%r13d, %ebp
	movl	%ebp, 108(%r12)
	testl	%r9d, %r9d
	setne	%r15b
.LBB2_75:                               #   in Loop: Header=BB2_20 Depth=1
	movl	%ebp, %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	callq	LzmaDec_TryDummy
	testl	%eax, %eax
	je	.LBB2_89
# BB#76:                                #   in Loop: Header=BB2_20 Depth=1
	cmpl	$2, %eax
	setne	%al
	andb	%al, %r15b
	cmpb	$1, %r15b
	movq	24(%rsp), %r8           # 8-byte Reload
	je	.LBB2_85
.LBB2_77:                               # %.thread174
                                        #   in Loop: Header=BB2_20 Depth=1
	movq	%r8, 32(%r12)
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r8, %rdx
	movq	%r8, %r15
	callq	LzmaDec_DecodeReal2
	testl	%eax, %eax
	jne	.LBB2_86
# BB#78:                                #   in Loop: Header=BB2_20 Depth=1
	movl	32(%r12), %eax
	subl	%r15d, %eax
	subl	%ebp, %r13d
	addl	%eax, %r13d
	movq	32(%rsp), %r11          # 8-byte Reload
	addq	%r13, (%r11)
	addq	%r13, %r14
	movl	$0, 108(%r12)
	movq	%r14, %rdx
	movq	%r15, %r8
.LBB2_79:                               # %.backedge
                                        #   in Loop: Header=BB2_20 Depth=1
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024,1024,1024,1024,1024]
	subq	%r13, %rbx
	movl	92(%r12), %ecx
	cmpl	$274, %ecx              # imm = 0x112
	movq	%rbx, %r10
	jne	.LBB2_20
	jmp	.LBB2_83
.LBB2_80:                               #   in Loop: Header=BB2_20 Depth=1
	movl	%ebp, %esi
	cmpl	$4, %esi
	ja	.LBB2_39
.LBB2_82:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$3, (%rax)
	jmp	.LBB2_92
.LBB2_83:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	jmp	.LBB2_15
.LBB2_84:
	movl	$1, %eax
	jmp	.LBB2_18
.LBB2_85:
	movl	$1, %eax
	movl	$2, %ecx
	jmp	.LBB2_95
.LBB2_86:
	movl	$1, %eax
	jmp	.LBB2_18
.LBB2_87:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$2, (%rax)
	jmp	.LBB2_92
.LBB2_88:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$2, (%rax)
	movl	$1, %eax
	jmp	.LBB2_18
.LBB2_89:
	movl	%r13d, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	addq	%rax, (%rcx)
	jmp	.LBB2_94
.LBB2_90:
	movl	$1, %eax
	jmp	.LBB2_18
.LBB2_91:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$4, (%rax)
.LBB2_92:                               # %.thread180
	xorl	%eax, %eax
	jmp	.LBB2_18
.LBB2_93:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movl	%ebx, 108(%r12)
	addq	%rbx, (%r15)
.LBB2_94:                               # %.loopexit183
	xorl	%eax, %eax
	movl	$3, %ecx
.LBB2_95:                               # %.loopexit183
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%ecx, (%rdx)
	jmp	.LBB2_18
.Lfunc_end2:
	.size	LzmaDec_DecodeToDic, .Lfunc_end2-LzmaDec_DecodeToDic
	.cfi_endproc

	.p2align	4, 0x90
	.type	LzmaDec_TryDummy,@function
LzmaDec_TryDummy:                       # @LzmaDec_TryDummy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	40(%rdi), %ebp
	movl	44(%rdi), %r12d
	movq	16(%rdi), %r9
	movl	72(%rdi), %r14d
	movl	64(%rdi), %eax
	movb	8(%rdi), %cl
	movl	$1, %r11d
	shll	%cl, %r11d
	decl	%r11d
	andl	%eax, %r11d
	movl	%r14d, %ecx
	shll	$4, %ecx
	leaq	(%r9,%rcx,2), %r15
	movzwl	(%r15,%r11,2), %ecx
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB3_3
# BB#1:
	testq	%rdx, %rdx
	jle	.LBB3_28
# BB#2:
	shll	$8, %ebp
	shll	$8, %r12d
	leaq	1(%rsi), %r8
	movzbl	(%rsi), %ebx
	orl	%ebx, %r12d
	jmp	.LBB3_4
.LBB3_3:
	movq	%rsi, %r8
.LBB3_4:
	addq	%rdx, %rsi
	movl	%ebp, %r13d
	shrl	$11, %r13d
	imull	%ecx, %r13d
	movl	%r12d, %r10d
	subl	%r13d, %r10d
	jae	.LBB3_16
# BB#5:
	addq	$3692, %r9              # imm = 0xE6C
	movl	68(%rdi), %ecx
	orl	%eax, %ecx
	je	.LBB3_7
# BB#6:
	movb	4(%rdi), %cl
	movl	$1, %edx
	shll	%cl, %edx
	decl	%edx
	andl	%eax, %edx
	movl	(%rdi), %ecx
	shll	%cl, %edx
	movq	24(%rdi), %rax
	leaq	48(%rdi), %rbp
	leaq	56(%rdi), %rbx
	cmpq	$0, 48(%rdi)
	cmoveq	%rbx, %rbp
	movq	(%rbp), %rbp
	movzbl	-1(%rax,%rbp), %ebp
	movl	$8, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	shrl	%cl, %ebp
	addl	%edx, %ebp
	shll	$8, %ebp
	leal	(%rbp,%rbp,2), %eax
	leaq	(%r9,%rax,2), %r9
.LBB3_7:
	cmpl	$6, %r14d
	ja	.LBB3_26
# BB#8:                                 # %.preheader.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB3_9:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ecx
	movzwl	(%r9,%rcx,2), %ecx
	cmpl	$16777215, %r13d        # imm = 0xFFFFFF
	ja	.LBB3_12
# BB#10:                                #   in Loop: Header=BB3_9 Depth=1
	cmpq	%rsi, %r8
	jae	.LBB3_53
# BB#11:                                #   in Loop: Header=BB3_9 Depth=1
	shll	$8, %r13d
	shll	$8, %r12d
	movzbl	(%r8), %edx
	incq	%r8
	orl	%edx, %r12d
.LBB3_12:                               #   in Loop: Header=BB3_9 Depth=1
	movl	%r13d, %edi
	shrl	$11, %r13d
	imull	%ecx, %r13d
	movl	%r12d, %ecx
	subl	%r13d, %ecx
	jae	.LBB3_14
# BB#13:                                #   in Loop: Header=BB3_9 Depth=1
	addl	%eax, %eax
	cmpl	$256, %eax              # imm = 0x100
	jb	.LBB3_9
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_14:                               #   in Loop: Header=BB3_9 Depth=1
	subl	%r13d, %edi
	leal	1(%rax,%rax), %eax
	movl	%ecx, %r12d
	movl	%edi, %r13d
	cmpl	$256, %eax              # imm = 0x100
	jb	.LBB3_9
.LBB3_15:
	movl	$1, %r12d
	jmp	.LBB3_105
.LBB3_16:
	subl	%r13d, %ebp
	movzwl	384(%r9,%r14,2), %edx
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB3_19
# BB#17:
	xorl	%eax, %eax
	cmpq	%rsi, %r8
	jae	.LBB3_106
# BB#18:
	shll	$8, %ebp
	shll	$8, %r10d
	movzbl	(%r8), %eax
	incq	%r8
	orl	%eax, %r10d
.LBB3_19:
	movl	%ebp, %edi
	shrl	$11, %edi
	imull	%edx, %edi
	movl	%r10d, %edx
	subl	%edi, %edx
	jae	.LBB3_29
# BB#20:
	movl	$2, %r12d
	xorl	%r15d, %r15d
	movl	$818, %ecx              # imm = 0x332
.LBB3_21:
	movzwl	(%r9,%rcx,2), %edx
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB3_24
# BB#22:
	xorl	%eax, %eax
	cmpq	%rsi, %r8
	jae	.LBB3_106
# BB#23:
	shll	$8, %edi
	shll	$8, %r10d
	movzbl	(%r8), %eax
	incq	%r8
	orl	%eax, %r10d
.LBB3_24:
	leaq	(%r9,%rcx,2), %rbp
	movl	%edi, %r13d
	shrl	$11, %r13d
	imull	%edx, %r13d
	movl	%r10d, %edx
	subl	%r13d, %edx
	jae	.LBB3_49
# BB#25:
	shll	$3, %r11d
	leaq	4(%rbp,%r11,2), %rbp
	xorl	%eax, %eax
	movl	$8, %ecx
	jmp	.LBB3_63
.LBB3_26:
	movq	24(%rdi), %rax
	movq	48(%rdi), %rcx
	movl	76(%rdi), %edx
	subq	%rdx, %rcx
	jae	.LBB3_39
# BB#27:
	movq	56(%rdi), %rdx
	jmp	.LBB3_40
.LBB3_28:
	xorl	%eax, %eax
	jmp	.LBB3_106
.LBB3_29:
	subl	%edi, %ebp
	movzwl	408(%r9,%r14,2), %ecx
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB3_32
# BB#30:
	xorl	%eax, %eax
	cmpq	%rsi, %r8
	jae	.LBB3_106
# BB#31:
	shll	$8, %ebp
	shll	$8, %edx
	movzbl	(%r8), %eax
	incq	%r8
	orl	%eax, %edx
.LBB3_32:
	movl	%ebp, %edi
	shrl	$11, %edi
	imull	%ecx, %edi
	movl	%edx, %r10d
	subl	%edi, %r10d
	jae	.LBB3_54
# BB#33:
	movzwl	480(%r15,%r11,2), %ecx
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB3_36
# BB#34:
	xorl	%eax, %eax
	cmpq	%rsi, %r8
	jae	.LBB3_106
# BB#35:
	shll	$8, %edi
	shll	$8, %edx
	movzbl	(%r8), %eax
	incq	%r8
	orl	%eax, %edx
.LBB3_36:
	movl	%edi, %ebx
	shrl	$11, %ebx
	imull	%ecx, %ebx
	movl	%edx, %r10d
	subl	%ebx, %r10d
	jae	.LBB3_83
# BB#37:
	movl	$3, %eax
	cmpl	$16777215, %ebx         # imm = 0xFFFFFF
	ja	.LBB3_106
# BB#38:
	cmpq	%rsi, %r8
	sbbl	%eax, %eax
	andl	$1, %eax
	leal	(%rax,%rax,2), %eax
	jmp	.LBB3_106
.LBB3_39:
	xorl	%edx, %edx
.LBB3_40:
	addq	%rdx, %rcx
	movzbl	(%rax,%rcx), %r10d
	movl	$1, %ecx
	movl	$256, %ebp              # imm = 0x100
	.p2align	4, 0x90
.LBB3_41:                               # =>This Inner Loop Header: Depth=1
	addl	%r10d, %r10d
	movl	%r10d, %edi
	andl	%ebp, %edi
	movl	%ebp, %eax
	leaq	(%r9,%rax,2), %rax
	leaq	(%rax,%rdi,2), %rax
	movl	%ecx, %edx
	movzwl	(%rax,%rdx,2), %eax
	cmpl	$16777215, %r13d        # imm = 0xFFFFFF
	ja	.LBB3_44
# BB#42:                                #   in Loop: Header=BB3_41 Depth=1
	cmpq	%rsi, %r8
	jae	.LBB3_53
# BB#43:                                #   in Loop: Header=BB3_41 Depth=1
	shll	$8, %r13d
	shll	$8, %r12d
	movzbl	(%r8), %edx
	incq	%r8
	orl	%edx, %r12d
.LBB3_44:                               #   in Loop: Header=BB3_41 Depth=1
	movl	%r13d, %edx
	shrl	$11, %r13d
	imull	%eax, %r13d
	movl	%r12d, %eax
	subl	%r13d, %eax
	jae	.LBB3_46
# BB#45:                                #   in Loop: Header=BB3_41 Depth=1
	addl	%ecx, %ecx
	xorl	%ebp, %edi
	jmp	.LBB3_47
	.p2align	4, 0x90
.LBB3_46:                               #   in Loop: Header=BB3_41 Depth=1
	subl	%r13d, %edx
	leal	1(%rcx,%rcx), %ecx
	movl	%eax, %r12d
	movl	%edx, %r13d
.LBB3_47:                               #   in Loop: Header=BB3_41 Depth=1
	cmpl	$256, %ecx              # imm = 0x100
	movl	%edi, %ebp
	jb	.LBB3_41
# BB#48:
	movl	$1, %r12d
	jmp	.LBB3_105
.LBB3_53:
	xorl	%eax, %eax
	jmp	.LBB3_106
.LBB3_49:
	subl	%r13d, %edi
	movzwl	2(%rbp), %ecx
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB3_59
# BB#50:
	xorl	%eax, %eax
	cmpq	%rsi, %r8
	jae	.LBB3_106
# BB#51:
	shll	$8, %edi
	shll	$8, %edx
	movzbl	(%r8), %r10d
	incq	%r8
	orl	%edx, %r10d
	jmp	.LBB3_60
.LBB3_54:
	subl	%edi, %ebp
	movzwl	432(%r9,%r14,2), %ecx
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB3_57
# BB#55:
	xorl	%eax, %eax
	cmpq	%rsi, %r8
	jae	.LBB3_106
# BB#56:
	shll	$8, %ebp
	shll	$8, %r10d
	movzbl	(%r8), %eax
	incq	%r8
	orl	%eax, %r10d
.LBB3_57:
	movl	%ebp, %edi
	shrl	$11, %edi
	imull	%ecx, %edi
	movl	$3, %r12d
	movl	$12, %r15d
	movl	%r10d, %edx
	subl	%edi, %edx
	jae	.LBB3_84
# BB#58:
	movl	$1332, %ecx             # imm = 0x534
	jmp	.LBB3_21
.LBB3_59:
	movl	%edx, %r10d
.LBB3_60:
	movl	%edi, %r13d
	shrl	$11, %r13d
	imull	%ecx, %r13d
	movl	%r10d, %edx
	subl	%r13d, %edx
	jae	.LBB3_62
# BB#61:
	shll	$3, %r11d
	leaq	260(%rbp,%r11,2), %rbp
	movl	$8, %ecx
	movl	$8, %eax
	jmp	.LBB3_63
.LBB3_62:
	subl	%r13d, %edi
	addq	$516, %rbp              # imm = 0x204
	movl	$16, %eax
	movl	$256, %ecx              # imm = 0x100
	movl	%edx, %r10d
	movl	%edi, %r13d
.LBB3_63:
	movl	$1, %edx
	.p2align	4, 0x90
.LBB3_64:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %edi
	movzwl	(%rbp,%rdi,2), %edi
	cmpl	$16777215, %r13d        # imm = 0xFFFFFF
	ja	.LBB3_67
# BB#65:                                #   in Loop: Header=BB3_64 Depth=1
	cmpq	%rsi, %r8
	jae	.LBB3_82
# BB#66:                                #   in Loop: Header=BB3_64 Depth=1
	shll	$8, %r13d
	shll	$8, %r10d
	movzbl	(%r8), %ebx
	incq	%r8
	orl	%ebx, %r10d
.LBB3_67:                               #   in Loop: Header=BB3_64 Depth=1
	movl	%r13d, %ebx
	shrl	$11, %r13d
	imull	%edi, %r13d
	movl	%r10d, %edi
	subl	%r13d, %edi
	jae	.LBB3_69
# BB#68:                                #   in Loop: Header=BB3_64 Depth=1
	addl	%edx, %edx
	jmp	.LBB3_70
	.p2align	4, 0x90
.LBB3_69:                               #   in Loop: Header=BB3_64 Depth=1
	subl	%r13d, %ebx
	leal	1(%rdx,%rdx), %edx
	movl	%edi, %r10d
	movl	%ebx, %r13d
.LBB3_70:                               #   in Loop: Header=BB3_64 Depth=1
	movl	%edx, %edi
	subl	%ecx, %edi
	jb	.LBB3_64
# BB#71:
	cmpl	$3, %r15d
	ja	.LBB3_105
# BB#72:
	addl	%eax, %edi
	cmpl	$3, %edi
	movl	$3, %eax
	cmovbl	%edi, %eax
	shll	$6, %eax
	leaq	864(%r9,%rax,2), %rcx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB3_73:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	movzwl	(%rcx,%rdx,2), %edi
	cmpl	$16777215, %r13d        # imm = 0xFFFFFF
	ja	.LBB3_76
# BB#74:                                #   in Loop: Header=BB3_73 Depth=1
	cmpq	%rsi, %r8
	jae	.LBB3_87
# BB#75:                                #   in Loop: Header=BB3_73 Depth=1
	shll	$8, %r13d
	shll	$8, %r10d
	movzbl	(%r8), %edx
	incq	%r8
	orl	%edx, %r10d
.LBB3_76:                               #   in Loop: Header=BB3_73 Depth=1
	movl	%r13d, %edx
	shrl	$11, %r13d
	imull	%edi, %r13d
	movl	%r10d, %edi
	subl	%r13d, %edi
	jae	.LBB3_78
# BB#77:                                #   in Loop: Header=BB3_73 Depth=1
	addl	%eax, %eax
	cmpl	$64, %eax
	jb	.LBB3_73
	jmp	.LBB3_79
	.p2align	4, 0x90
.LBB3_78:                               #   in Loop: Header=BB3_73 Depth=1
	subl	%r13d, %edx
	leal	1(%rax,%rax), %eax
	movl	%edi, %r10d
	movl	%edx, %r13d
	cmpl	$64, %eax
	jb	.LBB3_73
.LBB3_79:
	addl	$-64, %eax
	cmpl	$4, %eax
	jb	.LBB3_105
# BB#80:
	movl	%eax, %ecx
	shrl	%ecx
	cmpl	$13, %eax
	ja	.LBB3_92
# BB#81:
	decl	%ecx
	movl	%eax, %edx
	andl	$1, %eax
	orl	$2, %eax
	shll	%cl, %eax
	leaq	1376(%r9,%rax,2), %r9
	addq	%rdx, %rdx
	subq	%rdx, %r9
	addq	$-2, %r9
	jmp	.LBB3_98
.LBB3_82:
	xorl	%eax, %eax
	jmp	.LBB3_106
.LBB3_83:
	subl	%ebx, %edi
	movl	$3, %r12d
	movl	$12, %r15d
	movl	$1332, %ecx             # imm = 0x534
	jmp	.LBB3_21
.LBB3_84:
	subl	%edi, %ebp
	movzwl	456(%r9,%r14,2), %ecx
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB3_88
# BB#85:
	xorl	%eax, %eax
	cmpq	%rsi, %r8
	jae	.LBB3_106
# BB#86:
	shll	$8, %ebp
	shll	$8, %edx
	movzbl	(%r8), %r10d
	incq	%r8
	orl	%edx, %r10d
	jmp	.LBB3_89
.LBB3_87:
	xorl	%eax, %eax
	jmp	.LBB3_106
.LBB3_88:
	movl	%edx, %r10d
.LBB3_89:
	movl	%ebp, %edi
	shrl	$11, %edi
	imull	%ecx, %edi
	movl	%r10d, %eax
	subl	%edi, %eax
	jae	.LBB3_91
# BB#90:
	movl	$1332, %ecx             # imm = 0x534
	jmp	.LBB3_21
.LBB3_91:
	subl	%edi, %ebp
	movl	$1332, %ecx             # imm = 0x534
	movl	%eax, %r10d
	movl	%ebp, %edi
	jmp	.LBB3_21
.LBB3_92:
	movl	$5, %eax
	subl	%ecx, %eax
.LBB3_93:                               # =>This Inner Loop Header: Depth=1
	cmpl	$16777215, %r13d        # imm = 0xFFFFFF
	ja	.LBB3_96
# BB#94:                                #   in Loop: Header=BB3_93 Depth=1
	cmpq	%rsi, %r8
	jae	.LBB3_108
# BB#95:                                #   in Loop: Header=BB3_93 Depth=1
	shll	$8, %r13d
	shll	$8, %r10d
	movzbl	(%r8), %ecx
	incq	%r8
	orl	%ecx, %r10d
.LBB3_96:                               #   in Loop: Header=BB3_93 Depth=1
	shrl	%r13d
	movl	%r10d, %ecx
	subl	%r13d, %ecx
	shrl	$31, %ecx
	addl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	andl	%r13d, %ecx
	subl	%ecx, %r10d
	incl	%eax
	jne	.LBB3_93
# BB#97:
	addq	$1604, %r9              # imm = 0x644
	movl	$4, %ecx
.LBB3_98:
	movl	$1, %eax
	.p2align	4, 0x90
.LBB3_99:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	movzwl	(%r9,%rdx,2), %edi
	cmpl	$16777215, %r13d        # imm = 0xFFFFFF
	ja	.LBB3_102
# BB#100:                               #   in Loop: Header=BB3_99 Depth=1
	cmpq	%rsi, %r8
	jae	.LBB3_108
# BB#101:                               #   in Loop: Header=BB3_99 Depth=1
	shll	$8, %r13d
	shll	$8, %r10d
	movzbl	(%r8), %edx
	incq	%r8
	orl	%edx, %r10d
.LBB3_102:                              #   in Loop: Header=BB3_99 Depth=1
	movl	%r13d, %edx
	shrl	$11, %r13d
	imull	%edi, %r13d
	movl	%r10d, %edi
	subl	%r13d, %edi
	jae	.LBB3_104
# BB#103:                               #   in Loop: Header=BB3_99 Depth=1
	addl	%eax, %eax
	decl	%ecx
	jne	.LBB3_99
	jmp	.LBB3_105
.LBB3_104:                              #   in Loop: Header=BB3_99 Depth=1
	subl	%r13d, %edx
	leal	1(%rax,%rax), %eax
	movl	%edi, %r10d
	movl	%edx, %r13d
	decl	%ecx
	jne	.LBB3_99
.LBB3_105:                              # %.thread
	xorl	%eax, %eax
	cmpq	%rsi, %r8
	cmovbl	%r12d, %eax
	cmpl	$16777215, %r13d        # imm = 0xFFFFFF
	cmoval	%r12d, %eax
.LBB3_106:                              # %.thread547
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_108:
	xorl	%eax, %eax
	jmp	.LBB3_106
.Lfunc_end3:
	.size	LzmaDec_TryDummy, .Lfunc_end3-LzmaDec_TryDummy
	.cfi_endproc

	.p2align	4, 0x90
	.type	LzmaDec_DecodeReal2,@function
LzmaDec_DecodeReal2:                    # @LzmaDec_DecodeReal2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 80
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	movq	%rdi, -72(%rsp)         # 8-byte Spill
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	jmp	.LBB4_4
	.p2align	4, 0x90
.LBB4_1:                                #   in Loop: Header=BB4_4 Depth=1
	movq	-32(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 32(%rdi)
	jae	.LBB4_181
# BB#2:                                 #   in Loop: Header=BB4_4 Depth=1
	movl	92(%rdi), %ecx
	cmpl	$274, %ecx              # imm = 0x112
	jb	.LBB4_4
	jmp	.LBB4_182
.LBB4_3:                                #   in Loop: Header=BB4_4 Depth=1
	addl	$274, %r13d             # imm = 0x112
	addl	$-12, %r8d
	movl	-116(%rsp), %ebp        # 4-byte Reload
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	jbe	.LBB4_164
	jmp	.LBB4_165
	.p2align	4, 0x90
.LBB4_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
                                        #       Child Loop BB4_65 Depth 3
                                        #       Child Loop BB4_114 Depth 3
                                        #       Child Loop BB4_106 Depth 3
                                        #       Child Loop BB4_158 Depth 3
                                        #       Child Loop BB4_160 Depth 3
                                        #       Child Loop BB4_154 Depth 3
                                        #       Child Loop BB4_26 Depth 3
                                        #       Child Loop BB4_15 Depth 3
                                        #     Child Loop BB4_176 Depth 2
	movl	68(%rdi), %eax
	movl	%eax, -76(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	je	.LBB4_6
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	48(%rdi), %r9
	movl	64(%rdi), %ebp
	movq	%rsi, -64(%rsp)         # 8-byte Spill
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_6:                                #   in Loop: Header=BB4_4 Depth=1
	movl	12(%rdi), %eax
	movl	64(%rdi), %ebp
	subl	%ebp, %eax
	movq	48(%rdi), %r9
	movq	%rsi, %rcx
	subq	%r9, %rcx
	cmpq	%rax, %rcx
	leaq	(%rax,%r9), %rax
	cmovbeq	%rsi, %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
.LBB4_7:                                #   in Loop: Header=BB4_4 Depth=1
	movq	16(%rdi), %rdx
	movl	72(%rdi), %r8d
	movl	76(%rdi), %eax
	movl	%eax, -120(%rsp)        # 4-byte Spill
	movl	80(%rdi), %eax
	movl	%eax, -96(%rsp)         # 4-byte Spill
	movl	84(%rdi), %eax
	movl	%eax, -100(%rsp)        # 4-byte Spill
	movl	88(%rdi), %eax
	movl	%eax, -92(%rsp)         # 4-byte Spill
	movb	8(%rdi), %cl
	movl	$1, %eax
	shll	%cl, %eax
	decl	%eax
	movl	%eax, -44(%rsp)         # 4-byte Spill
	movb	4(%rdi), %cl
	movl	$1, %eax
	shll	%cl, %eax
	decl	%eax
	movl	%eax, -48(%rsp)         # 4-byte Spill
	movl	(%rdi), %eax
	movq	24(%rdi), %r14
	movq	56(%rdi), %r12
	movq	32(%rdi), %r15
	movl	40(%rdi), %r11d
	movl	44(%rdi), %r10d
	movl	$8, %ecx
	movl	%eax, -52(%rsp)         # 4-byte Spill
	subl	%eax, %ecx
	movl	%ecx, -56(%rsp)         # 4-byte Spill
	leaq	3692(%rdx), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	leaq	864(%rdx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	1376(%rdx), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	leaq	480(%rdx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	16(%r14), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	1(%r14), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_8:                                #   Parent Loop BB4_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_65 Depth 3
                                        #       Child Loop BB4_114 Depth 3
                                        #       Child Loop BB4_106 Depth 3
                                        #       Child Loop BB4_158 Depth 3
                                        #       Child Loop BB4_160 Depth 3
                                        #       Child Loop BB4_154 Depth 3
                                        #       Child Loop BB4_26 Depth 3
                                        #       Child Loop BB4_15 Depth 3
	movq	%r9, -88(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	andl	-44(%rsp), %r9d         # 4-byte Folded Reload
	movl	%r8d, %edi
	shll	$4, %edi
	movq	-112(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rdi,2), %rax
	movzwl	(%rax,%r9,2), %edx
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	movl	%r8d, -124(%rsp)        # 4-byte Spill
	ja	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %r11d
	shll	$8, %r10d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%r10d, %ecx
	movl	%ecx, %r10d
.LBB4_10:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%r11d, %esi
	shrl	$11, %esi
	imull	%edx, %esi
	movl	%r10d, %r8d
	subl	%esi, %r8d
	jae	.LBB4_21
# BB#11:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%edx, %ecx
	shrl	$5, %ecx
	addl	%edx, %ecx
	movw	%cx, (%rax,%r9,2)
	movl	%ebp, %eax
	orl	-76(%rsp), %eax         # 4-byte Folded Reload
	movq	-40(%rsp), %r8          # 8-byte Reload
	movq	-88(%rsp), %r9          # 8-byte Reload
	je	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_8 Depth=2
	movl	%ebp, %eax
	andl	-48(%rsp), %eax         # 4-byte Folded Reload
	movl	-52(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	testq	%r9, %r9
	movq	%r9, %rcx
	cmoveq	%r12, %rcx
	movzbl	-1(%r14,%rcx), %edx
	movl	-56(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	addl	%eax, %edx
	shll	$8, %edx
	leal	(%rdx,%rdx,2), %eax
	movq	-40(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %r8
.LBB4_13:                               #   in Loop: Header=BB4_8 Depth=2
	cmpl	$6, -124(%rsp)          # 4-byte Folded Reload
	ja	.LBB4_25
# BB#14:                                # %.preheader.preheader
                                        #   in Loop: Header=BB4_8 Depth=2
	movl	$1, %eax
	.p2align	4, 0x90
.LBB4_15:                               # %.preheader
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %eax
	movzwl	(%r8,%rax,2), %ecx
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB4_17
# BB#16:                                #   in Loop: Header=BB4_15 Depth=3
	shll	$8, %esi
	shll	$8, %r10d
	movzbl	(%r15), %edx
	incq	%r15
	orl	%edx, %r10d
.LBB4_17:                               #   in Loop: Header=BB4_15 Depth=3
	movl	%esi, %edx
	shrl	$11, %esi
	imull	%ecx, %esi
	movl	%r10d, %edi
	subl	%esi, %edi
	jae	.LBB4_19
# BB#18:                                #   in Loop: Header=BB4_15 Depth=3
	movl	$2048, %edx             # imm = 0x800
	subl	%ecx, %edx
	shrl	$5, %edx
	addl	%ecx, %edx
	movw	%dx, (%r8,%rax,2)
	leal	(%rax,%rax), %eax
	cmpl	$256, %eax              # imm = 0x100
	jb	.LBB4_15
	jmp	.LBB4_20
	.p2align	4, 0x90
.LBB4_19:                               #   in Loop: Header=BB4_15 Depth=3
	subl	%esi, %edx
	movl	%ecx, %esi
	shrl	$5, %esi
	subl	%esi, %ecx
	movw	%cx, (%r8,%rax,2)
	leal	1(%rax,%rax), %eax
	movl	%edx, %esi
	movl	%edi, %r10d
	cmpl	$256, %eax              # imm = 0x100
	jb	.LBB4_15
.LBB4_20:                               # %.loopexit.loopexit.i
                                        #   in Loop: Header=BB4_8 Depth=2
	movl	-124(%rsp), %r8d        # 4-byte Reload
	cmpl	$3, %r8d
	movl	$3, %ecx
	cmovbl	%r8d, %ecx
	jmp	.LBB4_33
	.p2align	4, 0x90
.LBB4_21:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%ebp, -116(%rsp)        # 4-byte Spill
	subl	%esi, %r11d
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movw	%dx, (%rax,%r9,2)
	movl	-124(%rsp), %ebp        # 4-byte Reload
	movq	-112(%rsp), %rax        # 8-byte Reload
	movzwl	384(%rax,%rbp,2), %eax
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	ja	.LBB4_23
# BB#22:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %r11d
	shll	$8, %r8d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%ecx, %r8d
.LBB4_23:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%r11d, %esi
	shrl	$11, %esi
	imull	%eax, %esi
	movl	%r8d, %r10d
	subl	%esi, %r10d
	jae	.LBB4_34
# BB#24:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%eax, %ecx
	shrl	$5, %ecx
	addl	%eax, %ecx
	movq	-112(%rsp), %rdi        # 8-byte Reload
	movw	%cx, 384(%rdi,%rbp,2)
	addl	$12, -124(%rsp)         # 4-byte Folded Spill
	movl	$818, %eax              # imm = 0x332
	jmp	.LBB4_54
	.p2align	4, 0x90
.LBB4_25:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%ebp, -116(%rsp)        # 4-byte Spill
	movq	-72(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %rax
	movl	-120(%rsp), %ecx        # 4-byte Reload
	movq	%r9, %rdx
	subq	%rcx, %rdx
	movl	$0, %ecx
	cmovbq	%r12, %rcx
	addq	%rdx, %rcx
	movzbl	(%rax,%rcx), %r9d
	movl	$256, %ebx              # imm = 0x100
	movl	$1, %eax
	.p2align	4, 0x90
.LBB4_26:                               #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addl	%r9d, %r9d
	movl	%r9d, %ebp
	andl	%ebx, %ebp
	movl	%ebx, %ecx
	leaq	(%r8,%rcx,2), %rcx
	leaq	(%rcx,%rbp,2), %r11
	movl	%eax, %eax
	movzwl	(%r11,%rax,2), %ecx
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB4_28
# BB#27:                                #   in Loop: Header=BB4_26 Depth=3
	shll	$8, %esi
	shll	$8, %r10d
	movzbl	(%r15), %edx
	incq	%r15
	orl	%edx, %r10d
.LBB4_28:                               #   in Loop: Header=BB4_26 Depth=3
	movl	%esi, %edi
	shrl	$11, %esi
	imull	%ecx, %esi
	movl	%r10d, %edx
	subl	%esi, %edx
	jae	.LBB4_30
# BB#29:                                #   in Loop: Header=BB4_26 Depth=3
	movl	$2048, %edx             # imm = 0x800
	subl	%ecx, %edx
	shrl	$5, %edx
	addl	%ecx, %edx
	movw	%dx, (%r11,%rax,2)
	leal	(%rax,%rax), %eax
	xorl	%ebx, %ebp
	jmp	.LBB4_31
	.p2align	4, 0x90
.LBB4_30:                               #   in Loop: Header=BB4_26 Depth=3
	subl	%esi, %edi
	movl	%ecx, %esi
	shrl	$5, %esi
	subl	%esi, %ecx
	movw	%cx, (%r11,%rax,2)
	leal	1(%rax,%rax), %eax
	movl	%edi, %esi
	movl	%edx, %r10d
.LBB4_31:                               #   in Loop: Header=BB4_26 Depth=3
	cmpl	$256, %eax              # imm = 0x100
	movl	%ebp, %ebx
	jb	.LBB4_26
# BB#32:                                # %.loopexit.loopexit1094.i
                                        #   in Loop: Header=BB4_8 Depth=2
	xorl	%ecx, %ecx
	movl	-124(%rsp), %r8d        # 4-byte Reload
	cmpl	$9, %r8d
	seta	%cl
	leal	3(%rcx,%rcx,2), %ecx
	movq	-88(%rsp), %r9          # 8-byte Reload
	movl	-116(%rsp), %ebp        # 4-byte Reload
.LBB4_33:                               # %.loopexit.i
                                        #   in Loop: Header=BB4_8 Depth=2
	subl	%ecx, %r8d
	movb	%al, (%r14,%r9)
	incq	%r9
	incl	%ebp
	movl	%esi, %r11d
	cmpq	-32(%rsp), %r15         # 8-byte Folded Reload
	jb	.LBB4_162
	jmp	.LBB4_163
	.p2align	4, 0x90
.LBB4_34:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%eax, %ecx
	shrl	$5, %ecx
	subl	%ecx, %eax
	movq	-112(%rsp), %rcx        # 8-byte Reload
	movw	%ax, 384(%rcx,%rbp,2)
	movl	-116(%rsp), %eax        # 4-byte Reload
	orl	-76(%rsp), %eax         # 4-byte Folded Reload
	je	.LBB4_187
# BB#35:                                #   in Loop: Header=BB4_8 Depth=2
	subl	%esi, %r11d
	movzwl	408(%rcx,%rbp,2), %eax
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	ja	.LBB4_37
# BB#36:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %r11d
	shll	$8, %r10d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%r10d, %ecx
	movl	%ecx, %r10d
.LBB4_37:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%r11d, %esi
	shrl	$11, %esi
	imull	%eax, %esi
	movl	%r10d, %r8d
	subl	%esi, %r8d
	jae	.LBB4_42
# BB#38:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%eax, %ecx
	shrl	$5, %ecx
	addl	%eax, %ecx
	movq	-112(%rsp), %rax        # 8-byte Reload
	movw	%cx, 408(%rax,%rbp,2)
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%rdi,2), %rax
	movzwl	(%rax,%r9,2), %edx
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %esi
	shll	$8, %r10d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%r10d, %ecx
	movl	%ecx, %r10d
.LBB4_40:                               #   in Loop: Header=BB4_8 Depth=2
	movl	-116(%rsp), %ebp        # 4-byte Reload
	movl	%esi, %r11d
	shrl	$11, %r11d
	imull	%edx, %r11d
	movl	%r10d, %r8d
	subl	%r11d, %r8d
	jae	.LBB4_46
# BB#41:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%edx, %ecx
	shrl	$5, %ecx
	addl	%edx, %ecx
	movw	%cx, (%rax,%r9,2)
	movl	-120(%rsp), %eax        # 4-byte Reload
	movq	-88(%rsp), %r9          # 8-byte Reload
	movq	%r9, %rcx
	subq	%rax, %rcx
	movl	$0, %eax
	cmovbq	%r12, %rax
	addq	%rcx, %rax
	movb	(%r14,%rax), %al
	movb	%al, (%r14,%r9)
	incq	%r9
	incl	%ebp
	xorl	%eax, %eax
	cmpl	$6, -124(%rsp)          # 4-byte Folded Reload
	seta	%al
	leal	9(%rax,%rax), %r8d
	cmpq	-32(%rsp), %r15         # 8-byte Folded Reload
	jb	.LBB4_162
	jmp	.LBB4_163
.LBB4_42:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%esi, %r11d
	movl	%eax, %ecx
	shrl	$5, %ecx
	subl	%ecx, %eax
	movq	-112(%rsp), %rdi        # 8-byte Reload
	movw	%ax, 408(%rdi,%rbp,2)
	movzwl	432(%rdi,%rbp,2), %edx
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	ja	.LBB4_44
# BB#43:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %r11d
	shll	$8, %r8d
	movzbl	(%r15), %eax
	incq	%r15
	orl	%eax, %r8d
.LBB4_44:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%r11d, %esi
	shrl	$11, %esi
	imull	%edx, %esi
	movl	%r8d, %eax
	subl	%esi, %eax
	jae	.LBB4_47
# BB#45:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %eax             # imm = 0x800
	subl	%edx, %eax
	shrl	$5, %eax
	addl	%edx, %eax
	movw	%ax, 432(%rdi,%rbp,2)
	movl	-120(%rsp), %ecx        # 4-byte Reload
	movl	-96(%rsp), %eax         # 4-byte Reload
	movl	%eax, -120(%rsp)        # 4-byte Spill
	jmp	.LBB4_53
.LBB4_46:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%r11d, %esi
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movw	%dx, (%rax,%r9,2)
	movl	-96(%rsp), %ecx         # 4-byte Reload
	movq	-112(%rsp), %rdi        # 8-byte Reload
	jmp	.LBB4_53
.LBB4_47:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%esi, %r11d
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movw	%dx, 432(%rdi,%rbp,2)
	movzwl	456(%rdi,%rbp,2), %ebx
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	ja	.LBB4_49
# BB#48:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %r11d
	shll	$8, %eax
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%ecx, %eax
.LBB4_49:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%r11d, %esi
	shrl	$11, %esi
	imull	%ebx, %esi
	movl	%eax, %ecx
	subl	%esi, %ecx
	jae	.LBB4_51
# BB#50:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %edx             # imm = 0x800
	subl	%ebx, %edx
	shrl	$5, %edx
	addl	%ebx, %edx
	movl	-92(%rsp), %r10d        # 4-byte Reload
	movl	-100(%rsp), %ebx        # 4-byte Reload
	jmp	.LBB4_52
.LBB4_51:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%esi, %r11d
	movl	%ebx, %eax
	shrl	$5, %eax
	subl	%eax, %ebx
	movw	%bx, %dx
	movl	%r11d, %esi
	movl	%ecx, %eax
	movl	-100(%rsp), %r10d       # 4-byte Reload
	movl	-92(%rsp), %ebx         # 4-byte Reload
.LBB4_52:                               #   in Loop: Header=BB4_8 Depth=2
	movl	-120(%rsp), %ecx        # 4-byte Reload
	movw	%dx, 456(%rdi,%rbp,2)
	movl	%eax, %r8d
	movl	%r10d, -92(%rsp)        # 4-byte Spill
	movl	%ebx, -120(%rsp)        # 4-byte Spill
	movl	-96(%rsp), %eax         # 4-byte Reload
	movl	%eax, -100(%rsp)        # 4-byte Spill
.LBB4_53:                               #   in Loop: Header=BB4_8 Depth=2
	xorl	%eax, %eax
	cmpl	$6, -124(%rsp)          # 4-byte Folded Reload
	seta	%al
	leal	8(%rax,%rax,2), %eax
	movl	%eax, -124(%rsp)        # 4-byte Spill
	movl	$1332, %eax             # imm = 0x534
	movl	%ecx, -96(%rsp)         # 4-byte Spill
.LBB4_54:                               #   in Loop: Header=BB4_8 Depth=2
	movzwl	(%rdi,%rax,2), %edx
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB4_56
# BB#55:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %esi
	shll	$8, %r8d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%ecx, %r8d
.LBB4_56:                               #   in Loop: Header=BB4_8 Depth=2
	leaq	(%rdi,%rax,2), %rbp
	movl	%esi, %r11d
	shrl	$11, %r11d
	imull	%edx, %r11d
	movl	%r8d, %ecx
	subl	%r11d, %ecx
	jae	.LBB4_58
# BB#57:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %eax             # imm = 0x800
	subl	%edx, %eax
	shrl	$5, %eax
	addl	%edx, %eax
	movw	%ax, (%rbp)
	shll	$3, %r9d
	leaq	4(%rbp,%r9,2), %rbp
	xorl	%r9d, %r9d
	movl	$8, %eax
	jmp	.LBB4_64
	.p2align	4, 0x90
.LBB4_58:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%r11d, %esi
	movl	%edx, %eax
	shrl	$5, %eax
	subl	%eax, %edx
	movw	%dx, (%rbp)
	movzwl	2(%rbp), %eax
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB4_60
# BB#59:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %esi
	shll	$8, %ecx
	movzbl	(%r15), %r8d
	incq	%r15
	orl	%ecx, %r8d
	jmp	.LBB4_61
.LBB4_60:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%ecx, %r8d
.LBB4_61:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%esi, %r11d
	shrl	$11, %r11d
	imull	%eax, %r11d
	movl	%r8d, %ecx
	subl	%r11d, %ecx
	jae	.LBB4_63
# BB#62:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%eax, %ecx
	shrl	$5, %ecx
	addl	%eax, %ecx
	movw	%cx, 2(%rbp)
	shll	$3, %r9d
	leaq	260(%rbp,%r9,2), %rbp
	movl	$8, %eax
	movl	$8, %r9d
	jmp	.LBB4_64
.LBB4_63:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%r11d, %esi
	movl	%eax, %edx
	shrl	$5, %edx
	subl	%edx, %eax
	movw	%ax, 2(%rbp)
	addq	$516, %rbp              # imm = 0x204
	movl	$16, %r9d
	movl	$256, %eax              # imm = 0x100
	movl	%esi, %r11d
	movl	%ecx, %r8d
.LBB4_64:                               #   in Loop: Header=BB4_8 Depth=2
	movl	$1, %edx
	movl	%r8d, %r10d
	movl	-124(%rsp), %r8d        # 4-byte Reload
	.p2align	4, 0x90
.LBB4_65:                               #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %esi
	movzwl	(%rbp,%rsi,2), %edi
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	ja	.LBB4_67
# BB#66:                                #   in Loop: Header=BB4_65 Depth=3
	shll	$8, %r11d
	shll	$8, %r10d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%ecx, %r10d
.LBB4_67:                               #   in Loop: Header=BB4_65 Depth=3
	movl	%r11d, %ecx
	shrl	$11, %r11d
	imull	%edi, %r11d
	movl	%r10d, %ebx
	subl	%r11d, %ebx
	jae	.LBB4_69
# BB#68:                                #   in Loop: Header=BB4_65 Depth=3
	movl	$2048, %ecx             # imm = 0x800
	subl	%edi, %ecx
	shrl	$5, %ecx
	addl	%edi, %ecx
	movw	%cx, (%rbp,%rsi,2)
	leal	(%rsi,%rsi), %edx
	jmp	.LBB4_70
	.p2align	4, 0x90
.LBB4_69:                               #   in Loop: Header=BB4_65 Depth=3
	subl	%r11d, %ecx
	movl	%edi, %edx
	shrl	$5, %edx
	subl	%edx, %edi
	movw	%di, (%rbp,%rsi,2)
	leal	1(%rsi,%rsi), %edx
	movl	%ecx, %r11d
	movl	%ebx, %r10d
.LBB4_70:                               #   in Loop: Header=BB4_65 Depth=3
	movl	%edx, %r13d
	subl	%eax, %r13d
	jb	.LBB4_65
# BB#71:                                #   in Loop: Header=BB4_8 Depth=2
	addl	%r9d, %r13d
	cmpl	$12, %r8d
	jb	.LBB4_76
# BB#72:                                #   in Loop: Header=BB4_8 Depth=2
	cmpl	$3, %r13d
	movl	$3, %edx
	cmovbl	%r13d, %edx
	shll	$6, %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movzwl	2(%rsi,%rdx,2), %eax
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	ja	.LBB4_74
# BB#73:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %r11d
	shll	$8, %r10d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%r10d, %ecx
	movl	%ecx, %r10d
.LBB4_74:                               #   in Loop: Header=BB4_8 Depth=2
	leaq	(%rsi,%rdx,2), %rsi
	movl	%r11d, %edi
	shrl	$11, %edi
	imull	%eax, %edi
	movl	%r10d, %ebp
	subl	%edi, %ebp
	jae	.LBB4_77
# BB#75:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%eax, %ecx
	shrl	$5, %ecx
	addl	%eax, %ecx
	movl	$2, %edx
	jmp	.LBB4_78
	.p2align	4, 0x90
.LBB4_76:                               #   in Loop: Header=BB4_8 Depth=2
	xorl	%edx, %edx
	movq	-88(%rsp), %r9          # 8-byte Reload
	movl	-116(%rsp), %ebx        # 4-byte Reload
	jmp	.LBB4_143
.LBB4_77:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%edi, %r11d
	movl	%eax, %ecx
	shrl	$5, %ecx
	subl	%ecx, %eax
	movl	$3, %edx
	movw	%ax, %cx
	movl	%r11d, %edi
	movl	%ebp, %r10d
.LBB4_78:                               #   in Loop: Header=BB4_8 Depth=2
	movw	%cx, 2(%rsi)
	movl	%edx, %ecx
	movzwl	(%rsi,%rcx,2), %edx
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB4_80
# BB#79:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %edi
	shll	$8, %r10d
	movzbl	(%r15), %eax
	incq	%r15
	orl	%r10d, %eax
	movl	%eax, %r10d
.LBB4_80:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%edi, %eax
	shrl	$11, %eax
	imull	%edx, %eax
	movl	%r10d, %ebp
	subl	%eax, %ebp
	jae	.LBB4_82
# BB#81:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %edi             # imm = 0x800
	subl	%edx, %edi
	shrl	$5, %edi
	addl	%edx, %edi
	movw	%di, (%rsi,%rcx,2)
	leal	(%rcx,%rcx), %ecx
	jmp	.LBB4_83
.LBB4_82:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%eax, %edi
	movl	%edx, %eax
	shrl	$5, %eax
	subl	%eax, %edx
	movw	%dx, (%rsi,%rcx,2)
	leal	1(%rcx,%rcx), %ecx
	movl	%edi, %eax
	movl	%ebp, %r10d
.LBB4_83:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%ecx, %edi
	movzwl	(%rsi,%rdi,2), %edx
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB4_85
# BB#84:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %eax
	shll	$8, %r10d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%r10d, %ecx
	movl	%ecx, %r10d
.LBB4_85:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%eax, %ecx
	shrl	$11, %ecx
	imull	%edx, %ecx
	movl	%r10d, %ebp
	subl	%ecx, %ebp
	jae	.LBB4_87
# BB#86:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %eax             # imm = 0x800
	subl	%edx, %eax
	shrl	$5, %eax
	addl	%edx, %eax
	movw	%ax, (%rsi,%rdi,2)
	leal	(%rdi,%rdi), %edx
	jmp	.LBB4_88
.LBB4_87:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%ecx, %eax
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movw	%dx, (%rsi,%rdi,2)
	leal	1(%rdi,%rdi), %edx
	movl	%eax, %ecx
	movl	%ebp, %r10d
.LBB4_88:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%edx, %edi
	movzwl	(%rsi,%rdi,2), %edx
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB4_90
# BB#89:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %ecx
	shll	$8, %r10d
	movzbl	(%r15), %eax
	incq	%r15
	orl	%r10d, %eax
	movl	%eax, %r10d
.LBB4_90:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%ecx, %eax
	shrl	$11, %eax
	imull	%edx, %eax
	movl	%r10d, %ebp
	subl	%eax, %ebp
	jae	.LBB4_92
# BB#91:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%edx, %ecx
	shrl	$5, %ecx
	addl	%edx, %ecx
	movw	%cx, (%rsi,%rdi,2)
	leal	(%rdi,%rdi), %edx
	jmp	.LBB4_93
.LBB4_92:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%eax, %ecx
	movl	%edx, %eax
	shrl	$5, %eax
	subl	%eax, %edx
	movw	%dx, (%rsi,%rdi,2)
	leal	1(%rdi,%rdi), %edx
	movl	%ecx, %eax
	movl	%ebp, %r10d
.LBB4_93:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%edx, %edi
	movzwl	(%rsi,%rdi,2), %edx
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB4_95
# BB#94:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %eax
	shll	$8, %r10d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%r10d, %ecx
	movl	%ecx, %r10d
.LBB4_95:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%eax, %ecx
	shrl	$11, %ecx
	imull	%edx, %ecx
	movl	%r10d, %ebp
	subl	%ecx, %ebp
	jae	.LBB4_97
# BB#96:                                #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %eax             # imm = 0x800
	subl	%edx, %eax
	shrl	$5, %eax
	addl	%edx, %eax
	movw	%ax, (%rsi,%rdi,2)
	leal	(%rdi,%rdi), %edx
	jmp	.LBB4_98
.LBB4_97:                               #   in Loop: Header=BB4_8 Depth=2
	subl	%ecx, %eax
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movw	%dx, (%rsi,%rdi,2)
	leal	1(%rdi,%rdi), %edx
	movl	%eax, %ecx
	movl	%ebp, %r10d
.LBB4_98:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%edx, %eax
	movzwl	(%rsi,%rax,2), %ebx
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB4_100
# BB#99:                                #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %ecx
	shll	$8, %r10d
	movzbl	(%r15), %edi
	incq	%r15
	orl	%r10d, %edi
	movl	%edi, %r10d
.LBB4_100:                              #   in Loop: Header=BB4_8 Depth=2
	movl	%ecx, %ebp
	shrl	$11, %ebp
	imull	%ebx, %ebp
	movl	%r10d, %edi
	subl	%ebp, %edi
	jae	.LBB4_102
# BB#101:                               #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%ebx, %ecx
	shrl	$5, %ecx
	addl	%ebx, %ecx
	movw	%cx, (%rsi,%rax,2)
	leal	(%rax,%rax), %r8d
	jmp	.LBB4_103
.LBB4_102:                              #   in Loop: Header=BB4_8 Depth=2
	subl	%ebp, %ecx
	movl	%ebx, %edx
	shrl	$5, %edx
	subl	%edx, %ebx
	movw	%bx, (%rsi,%rax,2)
	leal	1(%rax,%rax), %r8d
	movl	%ecx, %ebp
	movl	%edi, %r10d
.LBB4_103:                              #   in Loop: Header=BB4_8 Depth=2
	addl	$-64, %r8d
	cmpl	$4, %r8d
	jb	.LBB4_112
# BB#104:                               #   in Loop: Header=BB4_8 Depth=2
	movl	%r8d, %edx
	shrl	%edx
	movl	%r8d, %esi
	andl	$1, %esi
	orl	$2, %esi
	cmpl	$13, %r8d
	ja	.LBB4_113
# BB#105:                               #   in Loop: Header=BB4_8 Depth=2
	leal	-1(%rdx), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movq	-8(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,2), %r9
	movl	%r8d, %ecx
	addq	%rcx, %rcx
	subq	%rcx, %r9
	addq	$-2, %r9
	movl	$1, %edi
	movl	$1, %ebx
	subl	%edx, %ebx
	movl	%esi, %r8d
	movl	$1, %edx
	.p2align	4, 0x90
.LBB4_106:                              #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %esi
	movzwl	(%r9,%rsi,2), %eax
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB4_108
# BB#107:                               #   in Loop: Header=BB4_106 Depth=3
	shll	$8, %ebp
	shll	$8, %r10d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%ecx, %r10d
.LBB4_108:                              #   in Loop: Header=BB4_106 Depth=3
	movl	%ebp, %r11d
	shrl	$11, %r11d
	imull	%eax, %r11d
	movl	%r10d, %ecx
	subl	%r11d, %ecx
	jae	.LBB4_110
# BB#109:                               #   in Loop: Header=BB4_106 Depth=3
	movl	$2048, %ecx             # imm = 0x800
	subl	%eax, %ecx
	shrl	$5, %ecx
	addl	%eax, %ecx
	movw	%cx, (%r9,%rsi,2)
	leal	(%rsi,%rsi), %edx
	jmp	.LBB4_111
	.p2align	4, 0x90
.LBB4_110:                              #   in Loop: Header=BB4_106 Depth=3
	subl	%r11d, %ebp
	movl	%eax, %edx
	shrl	$5, %edx
	subl	%edx, %eax
	movw	%ax, (%r9,%rsi,2)
	leal	1(%rsi,%rsi), %edx
	orl	%edi, %r8d
	movl	%ebp, %r11d
	movl	%ecx, %r10d
.LBB4_111:                              #   in Loop: Header=BB4_106 Depth=3
	addl	%edi, %edi
	incl	%ebx
	movl	%r11d, %ebp
	jne	.LBB4_106
	jmp	.LBB4_139
.LBB4_112:                              #   in Loop: Header=BB4_8 Depth=2
	movl	%ebp, %r11d
	jmp	.LBB4_139
.LBB4_113:                              #   in Loop: Header=BB4_8 Depth=2
	movl	$5, %eax
	subl	%edx, %eax
	movq	-88(%rsp), %r9          # 8-byte Reload
	movl	-124(%rsp), %r8d        # 4-byte Reload
	.p2align	4, 0x90
.LBB4_114:                              #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB4_116
# BB#115:                               #   in Loop: Header=BB4_114 Depth=3
	shll	$8, %ebp
	shll	$8, %r10d
	movzbl	(%r15), %edx
	incq	%r15
	orl	%r10d, %edx
	movl	%ebp, %ecx
	movl	%edx, %r10d
	jmp	.LBB4_117
	.p2align	4, 0x90
.LBB4_116:                              #   in Loop: Header=BB4_114 Depth=3
	movl	%ebp, %ecx
.LBB4_117:                              #   in Loop: Header=BB4_114 Depth=3
	movl	%ecx, %ebp
	shrl	%ebp
	movl	%r10d, %edx
	subl	%ebp, %edx
	movl	%edx, %r10d
	sarl	$31, %r10d
	leal	1(%r10,%rsi,2), %esi
	andl	%ebp, %r10d
	addl	%edx, %r10d
	incl	%eax
	jne	.LBB4_114
# BB#118:                               #   in Loop: Header=BB4_8 Depth=2
	movq	-112(%rsp), %rax        # 8-byte Reload
	movzwl	1606(%rax), %eax
	cmpl	$33554431, %ecx         # imm = 0x1FFFFFF
	ja	.LBB4_120
# BB#119:                               #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %ebp
	shll	$8, %r10d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%r10d, %ecx
	movl	%ecx, %r10d
.LBB4_120:                              #   in Loop: Header=BB4_8 Depth=2
	shll	$4, %esi
	movl	%ebp, %ecx
	shrl	$11, %ecx
	imull	%eax, %ecx
	movl	%r10d, %edx
	subl	%ecx, %edx
	jae	.LBB4_122
# BB#121:                               #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %edx             # imm = 0x800
	subl	%eax, %edx
	shrl	$5, %edx
	addl	%eax, %edx
	movq	-112(%rsp), %rbx        # 8-byte Reload
	movw	%dx, 1606(%rbx)
	movl	$2, %eax
	jmp	.LBB4_123
.LBB4_122:                              #   in Loop: Header=BB4_8 Depth=2
	subl	%ecx, %ebp
	movl	%eax, %ecx
	shrl	$5, %ecx
	subl	%ecx, %eax
	movq	-112(%rsp), %rbx        # 8-byte Reload
	movw	%ax, 1606(%rbx)
	orl	$1, %esi
	movl	$3, %eax
	movl	%ebp, %ecx
	movl	%edx, %r10d
.LBB4_123:                              #   in Loop: Header=BB4_8 Depth=2
	movl	%eax, %edi
	movzwl	1604(%rbx,%rdi,2), %edx
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB4_125
# BB#124:                               #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %ecx
	shll	$8, %r10d
	movzbl	(%r15), %eax
	incq	%r15
	orl	%r10d, %eax
	movl	%eax, %r10d
.LBB4_125:                              #   in Loop: Header=BB4_8 Depth=2
	movl	%ecx, %eax
	shrl	$11, %eax
	imull	%edx, %eax
	movl	%r10d, %ebp
	subl	%eax, %ebp
	jae	.LBB4_127
# BB#126:                               #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%edx, %ecx
	shrl	$5, %ecx
	addl	%edx, %ecx
	movq	-112(%rsp), %rbx        # 8-byte Reload
	movw	%cx, 1604(%rbx,%rdi,2)
	leal	(%rdi,%rdi), %edx
	jmp	.LBB4_128
.LBB4_127:                              #   in Loop: Header=BB4_8 Depth=2
	subl	%eax, %ecx
	movl	%edx, %eax
	shrl	$5, %eax
	subl	%eax, %edx
	movq	-112(%rsp), %rbx        # 8-byte Reload
	movw	%dx, 1604(%rbx,%rdi,2)
	leal	1(%rdi,%rdi), %edx
	orl	$2, %esi
	movl	%ecx, %eax
	movl	%ebp, %r10d
.LBB4_128:                              #   in Loop: Header=BB4_8 Depth=2
	movl	%edx, %ecx
	movzwl	1604(%rbx,%rcx,2), %edx
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB4_130
# BB#129:                               #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %eax
	shll	$8, %r10d
	movzbl	(%r15), %edi
	incq	%r15
	orl	%r10d, %edi
	movl	%edi, %r10d
.LBB4_130:                              #   in Loop: Header=BB4_8 Depth=2
	movl	%eax, %edi
	shrl	$11, %edi
	imull	%edx, %edi
	movl	%r10d, %ebp
	subl	%edi, %ebp
	jae	.LBB4_132
# BB#131:                               #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %eax             # imm = 0x800
	subl	%edx, %eax
	shrl	$5, %eax
	addl	%edx, %eax
	movq	-112(%rsp), %rbx        # 8-byte Reload
	movw	%ax, 1604(%rbx,%rcx,2)
	leal	(%rcx,%rcx), %ecx
	jmp	.LBB4_133
.LBB4_132:                              #   in Loop: Header=BB4_8 Depth=2
	subl	%edi, %eax
	movl	%edx, %edi
	shrl	$5, %edi
	subl	%edi, %edx
	movq	-112(%rsp), %rbx        # 8-byte Reload
	movw	%dx, 1604(%rbx,%rcx,2)
	leal	1(%rcx,%rcx), %ecx
	orl	$4, %esi
	movl	%eax, %edi
	movl	%ebp, %r10d
.LBB4_133:                              #   in Loop: Header=BB4_8 Depth=2
	movl	%ecx, %eax
	movzwl	1604(%rbx,%rax,2), %ebp
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB4_135
# BB#134:                               #   in Loop: Header=BB4_8 Depth=2
	shll	$8, %edi
	shll	$8, %r10d
	movzbl	(%r15), %ecx
	incq	%r15
	orl	%r10d, %ecx
	movl	%ecx, %r10d
.LBB4_135:                              #   in Loop: Header=BB4_8 Depth=2
	movl	%edi, %r11d
	shrl	$11, %r11d
	imull	%ebp, %r11d
	movl	%r10d, %ecx
	subl	%r11d, %ecx
	jae	.LBB4_137
# BB#136:                               #   in Loop: Header=BB4_8 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%ebp, %ecx
	shrl	$5, %ecx
	addl	%ebp, %ecx
	movq	-112(%rsp), %rdx        # 8-byte Reload
	movw	%cx, 1604(%rdx,%rax,2)
	cmpl	$-1, %esi
	jne	.LBB4_138
	jmp	.LBB4_3
.LBB4_137:                              #   in Loop: Header=BB4_8 Depth=2
	subl	%r11d, %edi
	movl	%ebp, %edx
	shrl	$5, %edx
	subl	%edx, %ebp
	movq	-112(%rsp), %rdx        # 8-byte Reload
	movw	%bp, 1604(%rdx,%rax,2)
	orl	$8, %esi
	movl	%edi, %r11d
	movl	%ecx, %r10d
	cmpl	$-1, %esi
	je	.LBB4_3
.LBB4_138:                              #   in Loop: Header=BB4_8 Depth=2
	movl	%esi, %r8d
.LBB4_139:                              # %.thread.i
                                        #   in Loop: Header=BB4_8 Depth=2
	movl	-76(%rsp), %eax         # 4-byte Reload
	testl	%eax, %eax
	je	.LBB4_141
# BB#140:                               #   in Loop: Header=BB4_8 Depth=2
	cmpl	%eax, %r8d
	movq	-88(%rsp), %r9          # 8-byte Reload
	movl	-116(%rsp), %ebx        # 4-byte Reload
	jb	.LBB4_142
	jmp	.LBB4_187
.LBB4_141:                              #   in Loop: Header=BB4_8 Depth=2
	movl	-116(%rsp), %ebx        # 4-byte Reload
	cmpl	%ebx, %r8d
	movq	-88(%rsp), %r9          # 8-byte Reload
	jae	.LBB4_187
.LBB4_142:                              #   in Loop: Header=BB4_8 Depth=2
	incl	%r8d
	xorl	%eax, %eax
	cmpl	$18, -124(%rsp)         # 4-byte Folded Reload
	seta	%al
	leal	7(%rax,%rax,2), %eax
	movl	-100(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, -92(%rsp)         # 4-byte Spill
	movl	-96(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, -100(%rsp)        # 4-byte Spill
	movl	-120(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, -96(%rsp)         # 4-byte Spill
	movl	%r8d, -120(%rsp)        # 4-byte Spill
	movl	%eax, %r8d
	xorl	%edx, %edx
.LBB4_143:                              #   in Loop: Header=BB4_8 Depth=2
	movq	-64(%rsp), %rsi         # 8-byte Reload
	subq	%r9, %rsi
	je	.LBB4_187
# BB#144:                               #   in Loop: Header=BB4_8 Depth=2
	addl	$2, %r13d
	movl	%r13d, %eax
	cmpq	%rax, %rsi
	cmovael	%r13d, %esi
	movl	-120(%rsp), %eax        # 4-byte Reload
	movq	%r9, %rcx
	subq	%rax, %rcx
	movl	$0, %eax
	cmovbq	%r12, %rax
	addq	%rcx, %rax
	addl	%esi, %ebx
	subl	%esi, %r13d
	leaq	(%rax,%rsi), %rcx
	cmpq	%r12, %rcx
	jbe	.LBB4_147
# BB#145:                               # %.preheader.preheader.i
                                        #   in Loop: Header=BB4_8 Depth=2
	leal	-1(%rsi), %edi
	testb	$1, %sil
	jne	.LBB4_152
# BB#146:                               #   in Loop: Header=BB4_8 Depth=2
	movq	%r9, %rbp
	testl	%edi, %edi
	jne	.LBB4_153
	jmp	.LBB4_155
	.p2align	4, 0x90
.LBB4_147:                              #   in Loop: Header=BB4_8 Depth=2
	movl	%ebx, -116(%rsp)        # 4-byte Spill
	leaq	(%r14,%r9), %rdi
	movq	%rax, %rbp
	subq	%r9, %rbp
	leaq	(%rdi,%rsi), %rbx
	cmpl	$32, %esi
	jb	.LBB4_160
# BB#148:                               # %min.iters.checked
                                        #   in Loop: Header=BB4_8 Depth=2
	movl	%r8d, -124(%rsp)        # 4-byte Spill
	movl	%esi, %r8d
	andl	$31, %r8d
	movq	%rsi, %rdx
	subq	%r8, %rdx
	je	.LBB4_156
# BB#149:                               # %vector.memcheck
                                        #   in Loop: Header=BB4_8 Depth=2
	addq	%r14, %rcx
	cmpq	%rcx, %rdi
	jae	.LBB4_157
# BB#150:                               # %vector.memcheck
                                        #   in Loop: Header=BB4_8 Depth=2
	addq	%r14, %rax
	cmpq	%rbx, %rax
	jae	.LBB4_157
# BB#151:                               #   in Loop: Header=BB4_8 Depth=2
	movl	-124(%rsp), %r8d        # 4-byte Reload
	jmp	.LBB4_160
.LBB4_152:                              # %.preheader.i.prol
                                        #   in Loop: Header=BB4_8 Depth=2
	movb	(%r14,%rax), %cl
	leaq	1(%r9), %rbp
	movb	%cl, (%r14,%r9)
	incq	%rax
	cmpq	%r12, %rax
	movl	$0, %ecx
	cmoveq	%rcx, %rax
	movl	%edi, %esi
	testl	%edi, %edi
	je	.LBB4_155
.LBB4_153:                              # %.preheader.preheader.i.new
                                        #   in Loop: Header=BB4_8 Depth=2
	addq	8(%rsp), %rbp           # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB4_154:                              # %.preheader.i
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r14,%rax), %ecx
	movb	%cl, -1(%rbp)
	incq	%rax
	cmpq	%r12, %rax
	cmoveq	%rdx, %rax
	movzbl	(%r14,%rax), %ecx
	movb	%cl, (%rbp)
	incq	%rax
	cmpq	%r12, %rax
	cmoveq	%rdx, %rax
	addq	$2, %rbp
	addl	$-2, %esi
	jne	.LBB4_154
.LBB4_155:                              # %.loopexit1084.loopexit.i
                                        #   in Loop: Header=BB4_8 Depth=2
	movl	%edi, %eax
	leaq	1(%r9,%rax), %r9
	movl	%ebx, %ebp
	cmpq	-32(%rsp), %r15         # 8-byte Folded Reload
	jb	.LBB4_162
	jmp	.LBB4_163
.LBB4_156:                              #   in Loop: Header=BB4_8 Depth=2
	movl	-124(%rsp), %r8d        # 4-byte Reload
	jmp	.LBB4_160
.LBB4_157:                              # %vector.body.preheader
                                        #   in Loop: Header=BB4_8 Depth=2
	addq	%rdx, %rdi
	movq	-16(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rax
	.p2align	4, 0x90
.LBB4_158:                              # %vector.body
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%rbp,%rax), %xmm0
	movups	(%rbp,%rax), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %rax
	addq	$-32, %rdx
	jne	.LBB4_158
# BB#159:                               # %middle.block
                                        #   in Loop: Header=BB4_8 Depth=2
	testl	%r8d, %r8d
	movl	-124(%rsp), %r8d        # 4-byte Reload
	je	.LBB4_161
	.p2align	4, 0x90
.LBB4_160:                              # %scalar.ph
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbp,%rdi), %eax
	movb	%al, (%rdi)
	incq	%rdi
	cmpq	%rdi, %rbx
	jne	.LBB4_160
.LBB4_161:                              # %.loopexit1084.loopexit1095.i
                                        #   in Loop: Header=BB4_8 Depth=2
	addq	%rsi, %r9
	movl	-116(%rsp), %ebp        # 4-byte Reload
	cmpq	-32(%rsp), %r15         # 8-byte Folded Reload
	jae	.LBB4_163
.LBB4_162:                              # %.loopexit1084.i
                                        #   in Loop: Header=BB4_8 Depth=2
	cmpq	-64(%rsp), %r9          # 8-byte Folded Reload
	jb	.LBB4_8
.LBB4_163:                              # %.critedge.i
                                        #   in Loop: Header=BB4_4 Depth=1
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	ja	.LBB4_165
.LBB4_164:                              #   in Loop: Header=BB4_4 Depth=1
	shll	$8, %r11d
	shll	$8, %r10d
	movzbl	(%r15), %eax
	incq	%r15
	orl	%r10d, %eax
	movl	%eax, %r10d
.LBB4_165:                              #   in Loop: Header=BB4_4 Depth=1
	movl	-100(%rsp), %ecx        # 4-byte Reload
	movq	-72(%rsp), %rax         # 8-byte Reload
	movq	%r15, 32(%rax)
	movl	%r11d, 40(%rax)
	movl	%r10d, 44(%rax)
	movq	%rax, %rdi
	movl	%r13d, 92(%rdi)
	movq	%r9, 48(%rdi)
	movl	%ebp, 64(%rdi)
	movl	-120(%rsp), %eax        # 4-byte Reload
	movl	%eax, 76(%rdi)
	movl	-96(%rsp), %eax         # 4-byte Reload
	movl	%eax, 80(%rdi)
	movl	%ecx, 84(%rdi)
	movl	-92(%rsp), %eax         # 4-byte Reload
	movl	%eax, 88(%rdi)
	movl	%r8d, 72(%rdi)
	movl	12(%rdi), %ebx
	movl	%ebx, %edx
	subl	%ebp, %edx
	ja	.LBB4_167
# BB#166:                               #   in Loop: Header=BB4_4 Depth=1
	movl	%ebx, 68(%rdi)
.LBB4_167:                              #   in Loop: Header=BB4_4 Depth=1
	leal	-1(%r13), %eax
	cmpl	$272, %eax              # imm = 0x110
	movq	-24(%rsp), %rsi         # 8-byte Reload
	ja	.LBB4_180
# BB#168:                               #   in Loop: Header=BB4_4 Depth=1
	movl	%ebp, %r8d
	movq	24(%rdi), %rax
	movq	%r9, %rcx
	movq	56(%rdi), %r9
	movq	%rsi, %rbp
	movq	%rcx, %r14
	subq	%rcx, %rbp
	movl	%r13d, %ecx
	cmpq	%rcx, %rbp
	cmovael	%r13d, %ebp
	cmpl	$0, 68(%rdi)
	jne	.LBB4_171
# BB#169:                               #   in Loop: Header=BB4_4 Depth=1
	cmpl	%ebp, %edx
	ja	.LBB4_171
# BB#170:                               #   in Loop: Header=BB4_4 Depth=1
	movl	%ebx, 68(%rdi)
.LBB4_171:                              # %._crit_edge45.i
                                        #   in Loop: Header=BB4_4 Depth=1
	addl	%ebp, %r8d
	movl	%r8d, 64(%rdi)
	subl	%ebp, %r13d
	movl	%r13d, 92(%rdi)
	testl	%ebp, %ebp
	je	.LBB4_178
# BB#172:                               # %.lr.ph.i
                                        #   in Loop: Header=BB4_4 Depth=1
	movl	-120(%rsp), %r11d       # 4-byte Reload
	leal	-1(%rbp), %r8d
	testb	$1, %bpl
	movq	%r14, %rdi
	je	.LBB4_174
# BB#173:                               #   in Loop: Header=BB4_4 Depth=1
	movq	%r14, %rdi
	movq	%rdi, %rdx
	subq	%r11, %rdx
	movl	$0, %ecx
	cmovbq	%r9, %rcx
	addq	%rdx, %rcx
	movb	(%rax,%rcx), %dl
	movb	%dl, (%rax,%rdi)
	incq	%rdi
	movl	%r8d, %ebp
.LBB4_174:                              # %.prol.loopexit
                                        #   in Loop: Header=BB4_4 Depth=1
	testl	%r8d, %r8d
	je	.LBB4_177
# BB#175:                               # %.lr.ph.i.new
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	%r11, %rdx
	negq	%rdx
	addq	%rdi, %rax
	movl	%ebp, %r10d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_176:                              #   Parent Loop BB4_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rdi,%rbx), %rcx
	cmpq	%r11, %rcx
	movl	$0, %ecx
	cmovbq	%r9, %rcx
	addq	%rdx, %rcx
	addq	%rbx, %rcx
	movzbl	(%rax,%rcx), %ecx
	movb	%cl, (%rax,%rbx)
	leaq	1(%rdi,%rbx), %rcx
	cmpq	%r11, %rcx
	movl	$0, %ecx
	cmovbq	%r9, %rcx
	leaq	(%rdx,%rbx), %rbp
	addq	%rcx, %rbp
	movzbl	1(%rax,%rbp), %ecx
	movb	%cl, 1(%rax,%rbx)
	addq	$2, %rbx
	cmpl	%ebx, %r10d
	jne	.LBB4_176
.LBB4_177:                              # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB4_4 Depth=1
	movl	%r8d, %eax
	movq	%r14, %r9
	leaq	1(%r9,%rax), %r9
	movq	-72(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB4_179
.LBB4_178:                              #   in Loop: Header=BB4_4 Depth=1
	movq	%r14, %r9
.LBB4_179:                              # %._crit_edge.i
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	%r9, 48(%rdi)
.LBB4_180:                              #   in Loop: Header=BB4_4 Depth=1
	cmpq	%rsi, %r9
	jb	.LBB4_1
.LBB4_181:                              # %.critedgethread-pre-split
	movl	92(%rdi), %ecx
.LBB4_182:                              # %.critedge
	xorl	%eax, %eax
	cmpl	$275, %ecx              # imm = 0x113
	jb	.LBB4_188
# BB#183:
	movl	$274, 92(%rdi)          # imm = 0x112
	jmp	.LBB4_188
.LBB4_187:
	movl	$1, %eax
.LBB4_188:                              # %LzmaDec_WriteRem.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	LzmaDec_DecodeReal2, .Lfunc_end4-LzmaDec_DecodeReal2
	.cfi_endproc

	.globl	LzmaDec_DecodeToBuf
	.p2align	4, 0x90
	.type	LzmaDec_DecodeToBuf,@function
LzmaDec_DecodeToBuf:                    # @LzmaDec_DecodeToBuf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 128
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movq	%rcx, %rbp
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r13
	movq	(%rdx), %rax
	movq	(%r8), %rcx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	$0, (%rdx)
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	$0, (%r8)
	movq	%r13, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 16(%rsp)
	movq	48(%r13), %r15
	movq	56(%r13), %rdx
	cmpq	%rdx, %r15
	jne	.LBB5_3
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	movq	$0, 48(%r13)
	xorl	%r15d, %r15d
.LBB5_3:                                #   in Loop: Header=BB5_1 Depth=1
	movq	%rdx, %rcx
	subq	%r15, %rcx
	leaq	(%r15,%rax), %rsi
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpq	%rcx, %rax
	cmovaq	%rdx, %rsi
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	$0, %eax
	cmoval	%eax, %r8d
	movq	%r13, %rdi
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rbp, %rdx
	leaq	16(%rsp), %rcx
	movq	128(%rsp), %r9
	callq	LzmaDec_DecodeToDic
	movq	16(%rsp), %rbx
	movq	40(%rsp), %rcx          # 8-byte Reload
	addq	%rbx, (%rcx)
	movl	%eax, %r14d
	movq	48(%r13), %r12
	subq	%r15, %r12
	addq	24(%r13), %r15
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	%r12, (%rax)
	testl	%r14d, %r14d
	jne	.LBB5_6
# BB#4:                                 #   in Loop: Header=BB5_1 Depth=1
	xorl	%r14d, %r14d
	testq	%r12, %r12
	je	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_1 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	subq	%r12, %rax
	addq	%r12, %rbp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	subq	%rbx, %rcx
	movq	64(%rsp), %rbp          # 8-byte Reload
	addq	%rbx, %rbp
	testq	%rax, %rax
	movq	24(%rsp), %r13          # 8-byte Reload
	jne	.LBB5_1
.LBB5_6:                                # %.loopexit
	movl	%r14d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	LzmaDec_DecodeToBuf, .Lfunc_end5-LzmaDec_DecodeToBuf
	.cfi_endproc

	.globl	LzmaDec_FreeProbs
	.p2align	4, 0x90
	.type	LzmaDec_FreeProbs,@function
LzmaDec_FreeProbs:                      # @LzmaDec_FreeProbs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 16
.Lcfi52:
	.cfi_offset %rbx, -16
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	16(%rbx), %rsi
	movq	%rax, %rdi
	callq	*8(%rax)
	movq	$0, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	LzmaDec_FreeProbs, .Lfunc_end6-LzmaDec_FreeProbs
	.cfi_endproc

	.globl	LzmaDec_Free
	.p2align	4, 0x90
	.type	LzmaDec_Free,@function
LzmaDec_Free:                           # @LzmaDec_Free
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -24
.Lcfi57:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	16(%r14), %rsi
	movq	%rbx, %rdi
	callq	*8(%rbx)
	movq	$0, 16(%r14)
	movq	24(%r14), %rsi
	movq	%rbx, %rdi
	callq	*8(%rbx)
	movq	$0, 24(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	LzmaDec_Free, .Lfunc_end7-LzmaDec_Free
	.cfi_endproc

	.globl	LzmaProps_Decode
	.p2align	4, 0x90
	.type	LzmaProps_Decode,@function
LzmaProps_Decode:                       # @LzmaProps_Decode
	.cfi_startproc
# BB#0:
	movl	$4, %eax
	cmpl	$5, %edx
	jb	.LBB8_3
# BB#1:
	movzbl	1(%rsi), %ecx
	movzbl	2(%rsi), %edx
	shll	$8, %edx
	orl	%ecx, %edx
	movzbl	3(%rsi), %ecx
	shll	$16, %ecx
	orl	%edx, %ecx
	movzbl	4(%rsi), %edx
	shll	$24, %edx
	orl	%ecx, %edx
	cmpl	$4096, %edx             # imm = 0x1000
	movl	$4096, %ecx             # imm = 0x1000
	cmoval	%edx, %ecx
	movl	%ecx, 12(%rdi)
	movb	(%rsi), %cl
	cmpb	$-32, %cl
	ja	.LBB8_3
# BB#2:
	movzbl	%cl, %r9d
	imull	$57, %r9d, %esi
	andl	$15872, %esi            # imm = 0x3E00
	shrl	$9, %esi
	movb	$9, %r8b
	movl	%esi, %eax
	mulb	%r8b
	movl	%ecx, %edx
	subb	%al, %dl
	movzbl	%dl, %eax
	movl	%eax, (%rdi)
	imull	$109, %r9d, %eax
	shrl	$8, %eax
	subb	%al, %cl
	shrb	%cl
	addb	%al, %cl
	shrb	$5, %cl
	movzbl	%cl, %eax
	movl	%eax, 8(%rdi)
	imull	$205, %esi, %eax
	andl	$7168, %eax             # imm = 0x1C00
	shrl	$10, %eax
	movb	$5, %cl
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	mulb	%cl
	subb	%al, %sil
	movzbl	%sil, %eax
	movl	%eax, 4(%rdi)
	xorl	%eax, %eax
.LBB8_3:
	retq
.Lfunc_end8:
	.size	LzmaProps_Decode, .Lfunc_end8-LzmaProps_Decode
	.cfi_endproc

	.globl	LzmaDec_AllocateProbs
	.p2align	4, 0x90
	.type	LzmaDec_AllocateProbs,@function
LzmaDec_AllocateProbs:                  # @LzmaDec_AllocateProbs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi64:
	.cfi_def_cfa_offset 80
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdi, %rbp
	movl	$4, %eax
	cmpl	$5, %edx
	jb	.LBB9_7
# BB#1:
	movzbl	1(%rsi), %ecx
	movzbl	2(%rsi), %edx
	shll	$8, %edx
	orl	%ecx, %edx
	movzbl	3(%rsi), %ecx
	shll	$16, %ecx
	orl	%edx, %ecx
	movzbl	4(%rsi), %edx
	shll	$24, %edx
	orl	%ecx, %edx
	cmpl	$4096, %edx             # imm = 0x1000
	movl	$4096, %r15d            # imm = 0x1000
	cmoval	%edx, %r15d
	movb	(%rsi), %bl
	cmpb	$-32, %bl
	ja	.LBB9_7
# BB#2:                                 # %LzmaProps_Decode.exit
	movzbl	%bl, %edi
	imull	$57, %edi, %ecx
	andl	$15872, %ecx            # imm = 0x3E00
	shrl	$9, %ecx
	movb	$9, %dl
	movl	%ecx, %eax
	mulb	%dl
	movl	%ebx, %edx
	subb	%al, %dl
	movzbl	%dl, %r12d
	imull	$205, %ecx, %eax
	andl	$7168, %eax             # imm = 0x1C00
	shrl	$10, %eax
	movb	$5, %sil
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	mulb	%sil
	subb	%al, %cl
	movzbl	%cl, %edx
	leal	(%rdx,%r12), %ecx
	movl	$768, %r13d             # imm = 0x300
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r13d
	addl	$1846, %r13d            # imm = 0x736
	movq	16(%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB9_4
# BB#3:
	cmpl	104(%rbp), %r13d
	je	.LBB9_6
.LBB9_4:                                # %._crit_edge.i
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	%r14, %rdi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	callq	*8(%r14)
	movq	$0, 16(%rbp)
	movl	%r13d, %esi
	addq	%rsi, %rsi
	movq	%r14, %rdi
	callq	*(%r14)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%rax, 16(%rbp)
	movl	%r13d, 104(%rbp)
	testq	%rax, %rax
	je	.LBB9_5
.LBB9_6:
	imull	$109, %edi, %eax
	shrl	$8, %eax
	subb	%al, %bl
	shrb	%bl
	addb	%al, %bl
	shrb	$5, %bl
	movzbl	%bl, %eax
	movl	%r12d, (%rbp)
	movl	%edx, 4(%rbp)
	movl	%eax, 8(%rbp)
	movl	%r15d, 12(%rbp)
	xorl	%eax, %eax
	jmp	.LBB9_7
.LBB9_5:
	movl	$2, %eax
.LBB9_7:                                # %LzmaProps_Decode.exit.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	LzmaDec_AllocateProbs, .Lfunc_end9-LzmaDec_AllocateProbs
	.cfi_endproc

	.globl	LzmaDec_Allocate
	.p2align	4, 0x90
	.type	LzmaDec_Allocate,@function
LzmaDec_Allocate:                       # @LzmaDec_Allocate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi77:
	.cfi_def_cfa_offset 80
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdi, %r13
	movl	$4, %eax
	cmpl	$5, %edx
	jb	.LBB10_12
# BB#1:
	movzbl	1(%rsi), %ecx
	movzbl	2(%rsi), %edx
	shll	$8, %edx
	orl	%ecx, %edx
	movzbl	3(%rsi), %ecx
	shll	$16, %ecx
	orl	%edx, %ecx
	movzbl	4(%rsi), %edx
	shll	$24, %edx
	orl	%ecx, %edx
	cmpl	$4096, %edx             # imm = 0x1000
	movl	$4096, %r12d            # imm = 0x1000
	cmoval	%edx, %r12d
	movb	(%rsi), %bl
	cmpb	$-32, %bl
	ja	.LBB10_12
# BB#2:                                 # %LzmaProps_Decode.exit
	movzbl	%bl, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	imull	$57, %eax, %ecx
	andl	$15872, %ecx            # imm = 0x3E00
	shrl	$9, %ecx
	movb	$9, %dl
	movl	%ecx, %eax
	mulb	%dl
	movl	%ebx, %edx
	subb	%al, %dl
	movzbl	%dl, %r15d
	imull	$205, %ecx, %eax
	andl	$7168, %eax             # imm = 0x1C00
	shrl	$10, %eax
	movb	$5, %dl
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	mulb	%dl
	subb	%al, %cl
	movzbl	%cl, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leal	(%rax,%r15), %ecx
	movl	$768, %ebp              # imm = 0x300
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	$1846, %ebp             # imm = 0x736
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB10_4
# BB#3:
	cmpl	104(%r13), %ebp
	je	.LBB10_5
.LBB10_4:                               # %._crit_edge.i
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 16(%r13)
	movl	%ebp, %esi
	addq	%rsi, %rsi
	movq	%r14, %rdi
	callq	*(%r14)
	movq	%rax, 16(%r13)
	movl	%ebp, 104(%r13)
	testq	%rax, %rax
	je	.LBB10_11
.LBB10_5:
	movq	%r15, %rbp
	movl	%r12d, %r15d
	movq	24(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB10_7
# BB#6:
	cmpq	56(%r13), %r15
	je	.LBB10_8
.LBB10_7:
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 24(%r13)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*(%r14)
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	je	.LBB10_10
.LBB10_8:
	imull	$109, 12(%rsp), %eax    # 4-byte Folded Reload
	shrl	$8, %eax
	subb	%al, %bl
	shrb	%bl
	addb	%al, %bl
	shrb	$5, %bl
	movzbl	%bl, %eax
	movq	%r15, 56(%r13)
	movl	%ebp, (%r13)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 4(%r13)
	movl	%eax, 8(%r13)
	movl	%r12d, 12(%r13)
	xorl	%eax, %eax
	jmp	.LBB10_12
.LBB10_10:
	movq	16(%r13), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 16(%r13)
.LBB10_11:                              # %LzmaProps_Decode.exit.thread
	movl	$2, %eax
.LBB10_12:                              # %LzmaProps_Decode.exit.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	LzmaDec_Allocate, .Lfunc_end10-LzmaDec_Allocate
	.cfi_endproc

	.globl	LzmaDecode
	.p2align	4, 0x90
	.type	LzmaDecode,@function
LzmaDecode:                             # @LzmaDecode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 240
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rsi, %r14
	movq	(%rbx), %r15
	movq	(%r14), %rsi
	movq	$0, (%r14)
	movq	$0, (%rbx)
	movl	$6, %ebp
	cmpq	$5, %r15
	jb	.LBB11_8
# BB#1:
	cmpl	$5, %r9d
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rsp)
	movl	$4, %ebp
	jb	.LBB11_8
# BB#2:
	movzbl	1(%r8), %eax
	movzbl	2(%r8), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movzbl	3(%r8), %eax
	shll	$16, %eax
	orl	%ecx, %eax
	movzbl	4(%r8), %ecx
	shll	$24, %ecx
	orl	%eax, %ecx
	cmpl	$4096, %ecx             # imm = 0x1000
	movl	$4096, %eax             # imm = 0x1000
	cmoval	%ecx, %eax
	movb	(%r8), %r12b
	cmpb	$-32, %r12b
	ja	.LBB11_8
# BB#3:                                 # %._crit_edge.i.i
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	256(%rsp), %r13
	movzbl	%r12b, %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	imull	$57, %eax, %ecx
	andl	$15872, %ecx            # imm = 0x3E00
	shrl	$9, %ecx
	movb	$9, %dl
	movl	%ecx, %eax
	mulb	%dl
	movl	%r12d, %edx
	subb	%al, %dl
	movzbl	%dl, %esi
	imull	$205, %ecx, %eax
	andl	$7168, %eax             # imm = 0x1C00
	shrl	$10, %eax
	movb	$5, %dl
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	mulb	%dl
	subb	%al, %cl
	movzbl	%cl, %eax
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leal	(%rax,%rsi), %ecx
	movl	$768, %ebp              # imm = 0x300
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	$1846, %ebp             # imm = 0x736
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	*8(%r13)
	leaq	(%rbp,%rbp), %rsi
	movq	%r13, %rdi
	callq	*(%r13)
	movq	%rax, 64(%rsp)
	movl	%ebp, 152(%rsp)
	testq	%rax, %rax
	je	.LBB11_4
# BB#5:
	movq	248(%rsp), %rbp
	movl	240(%rsp), %r8d
	imull	$109, (%rsp), %eax      # 4-byte Folded Reload
	shrl	$8, %eax
	subb	%al, %r12b
	shrb	%r12b
	addb	%al, %r12b
	shrb	$5, %r12b
	movzbl	%r12b, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 48(%rsp)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 52(%rsp)
	movl	%eax, 56(%rsp)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 60(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 72(%rsp)
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, 104(%rsp)
	movq	$0, 96(%rsp)
	movl	$1, 144(%rsp)
	movl	$0, 140(%rsp)
	movl	$0, 156(%rsp)
	movl	$0, 112(%rsp)
	movl	$0, 116(%rsp)
	movl	$1, 148(%rsp)
	movq	%r15, (%rbx)
	leaq	48(%rsp), %rdi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	%rbp, %rbx
	movq	%rbx, %r9
	callq	LzmaDec_DecodeToDic
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB11_7
# BB#6:
	xorl	%eax, %eax
	cmpl	$3, (%rbx)
	movl	$6, %ebp
	cmovnel	%eax, %ebp
.LBB11_7:
	movq	96(%rsp), %rax
	movq	%rax, (%r14)
	movq	64(%rsp), %rsi
	movq	256(%rsp), %rdi
	callq	*8(%rdi)
	movq	$0, 64(%rsp)
	jmp	.LBB11_8
.LBB11_4:
	movl	$2, %ebp
.LBB11_8:                               # %LzmaDec_AllocateProbs.exit.thread
	movl	%ebp, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	LzmaDecode, .Lfunc_end11-LzmaDecode
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
