	.text
	.file	"follow.bc"
	.globl	extend_re
	.p2align	4, 0x90
	.type	extend_re,@function
extend_re:                              # @extend_re
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	strlen
	leal	5(%rax), %edi
	callq	malloc
	movl	$2632238, (%rax)        # imm = 0x282A2E
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcat
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	strlen
	movw	$41, (%rbx,%rax)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	extend_re, .Lfunc_end0-extend_re
	.cfi_endproc

	.globl	mk_followpos_1
	.p2align	4, 0x90
	.type	mk_followpos_1,@function
mk_followpos_1:                         # @mk_followpos_1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -48
.Lcfi8:
	.cfi_offset %r12, -40
.Lcfi9:
	.cfi_offset %r13, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_6:                                # %._crit_edge42
                                        #   in Loop: Header=BB1_1 Depth=1
	addq	$8, %r14
	movq	(%r14), %r14
.LBB1_1:                                # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
                                        #     Child Loop BB1_4 Depth 2
	movswl	(%r14), %esi
	cmpl	$5, %esi
	ja	.LBB1_12
# BB#2:                                 # %tailrecurse
                                        #   in Loop: Header=BB1_1 Depth=1
	jmpq	*.LJTI1_0(,%rsi,8)
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	movq	40(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB1_6
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%r14), %rdi
	movslq	(%rbx), %r15
	movq	(%r12,%r15,8), %rsi
	callq	pset_union
	movq	%rax, (%r12,%r15,8)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_4
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	callq	mk_followpos_1
	addq	$16, %r14
	movq	(%r14), %r14
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_1 Depth=1
	movq	8(%r14), %rdi
	movq	40(%rdi), %rbx
	leaq	16(%r14), %r15
	testq	%rbx, %rbx
	je	.LBB1_10
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rax
	movq	32(%rax), %rdi
	movslq	(%rbx), %r13
	movq	(%r12,%r13,8), %rsi
	callq	pset_union
	movq	%rax, (%r12,%r13,8)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_8
# BB#9:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	8(%r14), %rdi
.LBB1_10:                               # %._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	%r12, %rsi
	callq	mk_followpos_1
	movq	(%r15), %r14
	jmp	.LBB1_1
.LBB1_13:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB1_12:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	printf                  # TAILCALL
.Lfunc_end1:
	.size	mk_followpos_1, .Lfunc_end1-mk_followpos_1
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_13
	.quad	.LBB1_13
	.quad	.LBB1_3
	.quad	.LBB1_11
	.quad	.LBB1_6
	.quad	.LBB1_7

	.text
	.globl	mk_followpos
	.p2align	4, 0x90
	.type	mk_followpos,@function
mk_followpos:                           # @mk_followpos
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	.LBB2_5
# BB#1:
	testl	%r15d, %r15d
	js	.LBB2_5
# BB#2:
	leal	1(%r15), %edi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_3
# BB#4:                                 # %._crit_edge
	movl	%r15d, %eax
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	mk_followpos_1
	jmp	.LBB2_5
.LBB2_3:
	xorl	%ebx, %ebx
.LBB2_5:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	mk_followpos, .Lfunc_end2-mk_followpos
	.cfi_endproc

	.globl	init
	.p2align	4, 0x90
	.type	init,@function
init:                                   # @init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -48
.Lcfi24:
	.cfi_offset %r12, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	callq	strlen
	leal	5(%rax), %edi
	callq	malloc
	movl	$2632238, (%rax)        # imm = 0x282A2E
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcat
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	strlen
	movw	$41, (%rbp,%rax)
	movq	%rbp, %rdi
	callq	parse
	movq	%rax, %r15
	movl	$-1, %ebp
	testq	%r15, %r15
	je	.LBB3_10
# BB#1:
	movl	pos_cnt(%rip), %r12d
	testl	%r12d, %r12d
	js	.LBB3_10
# BB#2:
	leal	1(%r12), %edi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB3_10
# BB#3:                                 # %mk_followpos.exit
	leaq	8(,%r12,8), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	mk_followpos_1
	movl	pos_cnt(%rip), %ebp
	testl	%ebp, %ebp
	js	.LBB3_10
# BB#4:                                 # %.lr.ph29.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph29
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
	movq	(%r14,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB3_9
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	movq	8(%rcx), %rcx
	addq	$4, %rdx
	testq	%rcx, %rcx
	jne	.LBB3_7
# BB#8:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	pos_cnt(%rip), %ebp
.LBB3_9:                                # %._crit_edge
                                        #   in Loop: Header=BB3_5 Depth=1
	movslq	%ebp, %rcx
	subq	$-128, %rbx
	cmpq	%rcx, %rax
	leaq	1(%rax), %rax
	jl	.LBB3_5
.LBB3_10:                               # %mk_followpos.exit.thread
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	init, .Lfunc_end3-init
	.cfi_endproc

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"mk_followpos: unknown node type %d\n"
	.size	.L.str.2, 36

	.type	lpos,@object            # @lpos
	.comm	lpos,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
