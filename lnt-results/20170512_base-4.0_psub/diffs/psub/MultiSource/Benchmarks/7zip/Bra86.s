	.text
	.file	"Bra86.bc"
	.globl	x86_Convert
	.p2align	4, 0x90
	.type	x86_Convert,@function
x86_Convert:                            # @x86_Convert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rcx, %r9
	movl	%edx, %r12d
	cmpq	$5, %rsi
	jae	.LBB0_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB0_48
.LBB0_2:
	movl	(%r9), %ebx
	andl	$7, %ebx
	leaq	-4(%rdi,%rsi), %r10
	movq	$-1, %r11
	cmpq	%rdi, %r10
	jbe	.LBB0_23
# BB#3:                                 # %.lr.ph.lr.ph
	addl	$5, %r12d
	movq	$-1, %r11
	testl	%r8d, %r8d
	je	.LBB0_25
# BB#4:                                 # %.lr.ph.preheader
	movl	$23, %r8d
	movq	%rdi, %rax
.LBB0_5:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #     Child Loop BB0_18 Depth 2
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB0_6:                                #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	andb	$-2, %cl
	cmpb	$-24, %cl
	je	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=2
	incq	%rax
	cmpq	%r10, %rax
	jb	.LBB0_6
	jmp	.LBB0_44
.LBB0_8:                                #   in Loop: Header=BB0_5 Depth=1
	movq	%rax, %r11
	subq	%rdi, %r11
	movq	%r11, %rcx
	subq	%rsi, %rcx
	xorl	%r14d, %r14d
	cmpq	$3, %rcx
	ja	.LBB0_14
# BB#9:                                 #   in Loop: Header=BB0_5 Depth=1
	decl	%ecx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebx
	andl	$7, %ebx
	je	.LBB0_14
# BB#10:                                #   in Loop: Header=BB0_5 Depth=1
	btq	%rbx, %r8
	jae	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_5 Depth=1
	movl	%ebx, %ecx
	movzbl	kMaskToBitNumber(%rcx), %ecx
	movl	$4, %edx
	subq	%rcx, %rdx
	movb	(%rdx,%rax), %cl
	incb	%cl
	cmpb	$1, %cl
	ja	.LBB0_13
.LBB0_12:                               #   in Loop: Header=BB0_5 Depth=1
	andl	$3, %ebx
	leal	1(%rbx,%rbx), %ebx
	leaq	1(%r11), %rax
	jmp	.LBB0_22
.LBB0_13:                               #   in Loop: Header=BB0_5 Depth=1
	movl	%ebx, %r14d
.LBB0_14:                               # %.thread
                                        #   in Loop: Header=BB0_5 Depth=1
	movzbl	4(%rax), %ecx
	movl	%ecx, %edx
	incb	%dl
	cmpb	$1, %dl
	ja	.LBB0_20
# BB#15:                                #   in Loop: Header=BB0_5 Depth=1
	shll	$24, %ecx
	movzbl	3(%rax), %edx
	shll	$16, %edx
	orl	%ecx, %edx
	movzbl	2(%rax), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	movzbl	1(%rax), %ebx
	orl	%ecx, %ebx
	leal	(%r12,%r11), %r15d
	addl	%r15d, %ebx
	testl	%r14d, %r14d
	je	.LBB0_19
# BB#16:                                # %.lr.ph119
                                        #   in Loop: Header=BB0_5 Depth=1
	movl	%r14d, %ecx
	movzbl	kMaskToBitNumber(%rcx), %edx
	shll	$3, %edx
	movl	$24, %esi
	subl	%edx, %esi
	movl	$32, %ecx
	subl	%edx, %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	decl	%ebp
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_18 Depth=2
	xorl	%ebp, %ebx
	addl	%r15d, %ebx
.LBB0_18:                               #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %edx
	movl	%esi, %ecx
	shrl	%cl, %edx
	incb	%dl
	cmpb	$1, %dl
	jbe	.LBB0_17
.LBB0_19:                               # %._crit_edge122
                                        #   in Loop: Header=BB0_5 Depth=1
	movl	%ebx, %ecx
	shrl	$24, %ecx
	andl	$1, %ecx
	negl	%ecx
	movb	%cl, 4(%rax)
	movl	%ebx, %ecx
	shrl	$16, %ecx
	movb	%cl, 3(%rax)
	movb	%bh, 2(%rax)  # NOREX
	movb	%bl, 1(%rax)
	movl	$5, %eax
	jmp	.LBB0_21
.LBB0_20:                               #   in Loop: Header=BB0_5 Depth=1
	andl	$3, %r14d
	leal	1(%r14,%r14), %r14d
	movl	$1, %eax
.LBB0_21:                               #   in Loop: Header=BB0_5 Depth=1
	addq	%r11, %rax
	movl	%r14d, %ebx
.LBB0_22:                               # %.thread107.backedge
                                        #   in Loop: Header=BB0_5 Depth=1
	addq	%rdi, %rax
	cmpq	%r10, %rax
	jb	.LBB0_5
	jmp	.LBB0_45
.LBB0_23:
	movq	%rdi, %rax
	jmp	.LBB0_45
.LBB0_25:                               # %.lr.ph.us.preheader
	movl	$23, %r8d
	movq	%rdi, %rax
.LBB0_26:                               # %.lr.ph.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_27 Depth 2
                                        #     Child Loop BB0_39 Depth 2
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB0_27:                               #   Parent Loop BB0_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	andb	$-2, %cl
	cmpb	$-24, %cl
	je	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_27 Depth=2
	incq	%rax
	cmpq	%r10, %rax
	jb	.LBB0_27
	jmp	.LBB0_44
.LBB0_29:                               #   in Loop: Header=BB0_26 Depth=1
	movq	%rax, %r11
	subq	%rdi, %r11
	movq	%r11, %rcx
	subq	%rsi, %rcx
	xorl	%r14d, %r14d
	cmpq	$3, %rcx
	ja	.LBB0_35
# BB#30:                                #   in Loop: Header=BB0_26 Depth=1
	decl	%ecx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebx
	andl	$7, %ebx
	je	.LBB0_35
# BB#31:                                #   in Loop: Header=BB0_26 Depth=1
	btq	%rbx, %r8
	jae	.LBB0_33
# BB#32:                                #   in Loop: Header=BB0_26 Depth=1
	movl	%ebx, %ecx
	movzbl	kMaskToBitNumber(%rcx), %ecx
	movl	$4, %esi
	subq	%rcx, %rsi
	movb	(%rsi,%rax), %cl
	incb	%cl
	cmpb	$1, %cl
	ja	.LBB0_34
.LBB0_33:                               #   in Loop: Header=BB0_26 Depth=1
	andl	$3, %ebx
	leal	1(%rbx,%rbx), %ebx
	leaq	1(%r11), %rax
	jmp	.LBB0_43
.LBB0_34:                               #   in Loop: Header=BB0_26 Depth=1
	movl	%ebx, %r14d
.LBB0_35:                               # %.thread.us
                                        #   in Loop: Header=BB0_26 Depth=1
	movzbl	4(%rax), %ecx
	movl	%ecx, %ebx
	incb	%bl
	cmpb	$1, %bl
	ja	.LBB0_41
# BB#36:                                #   in Loop: Header=BB0_26 Depth=1
	shll	$24, %ecx
	movzbl	3(%rax), %esi
	shll	$16, %esi
	orl	%ecx, %esi
	movzbl	2(%rax), %ecx
	shll	$8, %ecx
	orl	%esi, %ecx
	movzbl	1(%rax), %edx
	orl	%ecx, %edx
	leal	(%r12,%r11), %r15d
	subl	%r15d, %edx
	testl	%r14d, %r14d
	je	.LBB0_40
# BB#37:                                # %.lr.ph119.us
                                        #   in Loop: Header=BB0_26 Depth=1
	movl	%r14d, %ecx
	movzbl	kMaskToBitNumber(%rcx), %ebx
	shll	$3, %ebx
	movl	$24, %esi
	subl	%ebx, %esi
	movl	$32, %ecx
	subl	%ebx, %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	decl	%ebp
	jmp	.LBB0_39
	.p2align	4, 0x90
.LBB0_38:                               #   in Loop: Header=BB0_39 Depth=2
	xorl	%ebp, %edx
	subl	%r15d, %edx
.LBB0_39:                               #   Parent Loop BB0_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %ebx
	movl	%esi, %ecx
	shrl	%cl, %ebx
	incb	%bl
	cmpb	$1, %bl
	jbe	.LBB0_38
.LBB0_40:                               # %._crit_edge122.us
                                        #   in Loop: Header=BB0_26 Depth=1
	movl	%edx, %ecx
	shrl	$24, %ecx
	andl	$1, %ecx
	negl	%ecx
	movb	%cl, 4(%rax)
	movl	%edx, %ecx
	shrl	$16, %ecx
	movb	%cl, 3(%rax)
	movb	%dh, 2(%rax)  # NOREX
	movb	%dl, 1(%rax)
	movl	$5, %eax
	jmp	.LBB0_42
.LBB0_41:                               #   in Loop: Header=BB0_26 Depth=1
	andl	$3, %r14d
	leal	1(%r14,%r14), %r14d
	movl	$1, %eax
.LBB0_42:                               #   in Loop: Header=BB0_26 Depth=1
	addq	%r11, %rax
	movl	%r14d, %ebx
.LBB0_43:                               # %.thread107.backedge.us
                                        #   in Loop: Header=BB0_26 Depth=1
	addq	%rdi, %rax
	cmpq	%r10, %rax
	jb	.LBB0_26
	jmp	.LBB0_45
.LBB0_44:
	movq	%rsi, %r11
.LBB0_45:                               # %.thread107._crit_edge
	subq	%rdi, %rax
	movq	%rax, %rcx
	subq	%r11, %rcx
	xorl	%edx, %edx
	cmpq	$3, %rcx
	ja	.LBB0_47
# BB#46:
	decl	%ecx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebx
	andl	$7, %ebx
	movl	%ebx, %edx
.LBB0_47:
	movl	%edx, (%r9)
.LBB0_48:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	x86_Convert, .Lfunc_end0-x86_Convert
	.cfi_endproc

	.type	kMaskToAllowedStatus,@object # @kMaskToAllowedStatus
	.section	.rodata,"a",@progbits
	.globl	kMaskToAllowedStatus
kMaskToAllowedStatus:
	.asciz	"\001\001\001\000\001\000\000"
	.size	kMaskToAllowedStatus, 8

	.type	kMaskToBitNumber,@object # @kMaskToBitNumber
	.globl	kMaskToBitNumber
kMaskToBitNumber:
	.ascii	"\000\001\002\002\003\003\003\003"
	.size	kMaskToBitNumber, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
