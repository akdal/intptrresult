	.text
	.file	"essen.bc"
	.globl	essential
	.p2align	4, 0x90
	.type	essential,@function
essential:                              # @essential
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	(%rdi), %r12
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_active
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB0_13
# BB#1:                                 # %.lr.ph
	movq	24(%r12), %rbp
	leaq	(%rbp,%rax,4), %rcx
	movl	$17408, %edx            # imm = 0x4400
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %eax
	andl	%edx, %eax
	cmpl	$1024, %eax             # imm = 0x400
	jne	.LBB0_12
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rsi
	callq	sf_join
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	cb_consensus
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	cube2list
	movq	%rax, %r13
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	cube_is_covered
	movl	%eax, %r15d
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:                                 # %.thread.i
                                        #   in Loop: Header=BB0_2 Depth=1
	callq	free
	jmp	.LBB0_6
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%r13, %r13
	je	.LBB0_7
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %rdi
	callq	free
.LBB0_7:                                # %essen_cube.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	testl	%r15d, %r15d
	jne	.LBB0_11
# BB#8:                                 #   in Loop: Header=BB0_2 Depth=1
	testb	$2, debug(%rip)
	je	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
.LBB0_10:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbp, %rsi
	callq	sf_addset
	movq	%rax, (%rsp)            # 8-byte Spill
	andb	$-33, 1(%rbp)
	decl	16(%r12)
.LBB0_11:                               #   in Loop: Header=BB0_2 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	$17408, %edx            # imm = 0x4400
.LBB0_12:                               #   in Loop: Header=BB0_2 Depth=1
	movslq	(%r12), %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%rcx, %rbp
	jb	.LBB0_2
.LBB0_13:                               # %._crit_edge
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_inactive
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rsi
	callq	sf_join
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	movq	%rbp, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	essential, .Lfunc_end0-essential
	.cfi_endproc

	.globl	essen_cube
	.p2align	4, 0x90
	.type	essen_cube,@function
essen_cube:                             # @essen_cube
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	xorl	%eax, %eax
	callq	sf_join
	movq	%rax, %rbp
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	cb_consensus
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	cube2list
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	cube_is_covered
	xorl	%ebp, %ebp
	testl	%eax, %eax
	sete	%bpl
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:                                 # %.thread
	callq	free
	jmp	.LBB1_3
.LBB1_2:
	testq	%rbx, %rbx
	je	.LBB1_4
.LBB1_3:
	movq	%rbx, %rdi
	callq	free
.LBB1_4:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	essen_cube, .Lfunc_end1-essen_cube
	.cfi_endproc

	.globl	cb_consensus
	.p2align	4, 0x90
	.type	cb_consensus,@function
cb_consensus:                           # @cb_consensus
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 64
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	12(%r14), %edi
	addl	%edi, %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB2_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB2_3
.LBB2_2:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB2_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r12
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB2_12
# BB#4:                                 # %.lr.ph.preheader
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %rbp
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r13, %rbx
	je	.LBB2_11
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	cdist01
	cmpl	$1, %eax
	je	.LBB2_9
# BB#7:                                 #   in Loop: Header=BB2_5 Depth=1
	testl	%eax, %eax
	jne	.LBB2_11
# BB#8:                                 #   in Loop: Header=BB2_5 Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	cb_consensus_dist0
	jmp	.LBB2_10
	.p2align	4, 0x90
.LBB2_9:                                #   in Loop: Header=BB2_5 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	consensus
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	sf_addset
.LBB2_10:                               #   in Loop: Header=BB2_5 Depth=1
	movq	%rax, %r15
.LBB2_11:                               #   in Loop: Header=BB2_5 Depth=1
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB2_5
.LBB2_12:                               # %._crit_edge
	testq	%r12, %r12
	je	.LBB2_14
# BB#13:
	movq	%r12, %rdi
	callq	free
.LBB2_14:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cb_consensus, .Lfunc_end2-cb_consensus
	.cfi_endproc

	.globl	cb_consensus_dist0
	.p2align	4, 0x90
	.type	cb_consensus_dist0,@function
cb_consensus_dist0:                     # @cb_consensus_dist0
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 160
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movq	cube+80(%rip), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %r14
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	setp_implies
	testl	%eax, %eax
	jne	.LBB3_75
# BB#1:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB3_3
# BB#2:
	movl	$8, %edi
	jmp	.LBB3_4
.LBB3_3:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB3_4:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r12
	movl	(%rbx), %eax
	movl	%eax, %ecx
	notl	%ecx
	movl	$-1024, %edx            # imm = 0xFC00
	movq	16(%rsp), %rdi          # 8-byte Reload
	andl	(%rdi), %edx
	orq	$-1024, %rcx            # imm = 0xFC00
	andl	$1023, %eax             # imm = 0x3FF
	movq	$-2, %rsi
	cmoveq	%rcx, %rsi
	orl	%eax, %edx
	movl	%edx, (%rdi)
	leaq	2(%rsi,%rax), %r10
	cmpq	$8, %r10
	movq	%rbx, (%rsp)            # 8-byte Spill
	jb	.LBB3_17
# BB#5:                                 # %min.iters.checked
	movq	%r10, %r8
	andq	$-8, %r8
	je	.LBB3_17
# BB#6:                                 # %vector.memcheck
	testl	%eax, %eax
	movl	$1, %edx
	cmoveq	%rax, %rdx
	leaq	(%rdi,%rdx,4), %rsi
	leaq	4(%rdi,%rax,4), %rbp
	leaq	(%rbx,%rdx,4), %rcx
	leaq	4(%rbx,%rax,4), %rbx
	leaq	(%r15,%rdx,4), %r9
	leaq	4(%r15,%rax,4), %rdx
	cmpq	%rbx, %rsi
	sbbb	%bl, %bl
	cmpq	%rbp, %rcx
	sbbb	%cl, %cl
	andb	%bl, %cl
	cmpq	%rdx, %rsi
	sbbb	%dl, %dl
	cmpq	%rbp, %r9
	sbbb	%bl, %bl
	testb	$1, %cl
	jne	.LBB3_11
# BB#7:                                 # %vector.memcheck
	andb	%bl, %dl
	andb	$1, %dl
	jne	.LBB3_11
# BB#8:                                 # %vector.body.preheader
	leaq	-8(%r8), %rcx
	movq	%rcx, %rdx
	shrq	$3, %rdx
	btl	$3, %ecx
	movq	(%rsp), %rcx            # 8-byte Reload
	jb	.LBB3_12
# BB#9:                                 # %vector.body.prol
	movups	-12(%rcx,%rax,4), %xmm0
	movups	-28(%rcx,%rax,4), %xmm1
	movups	-12(%r15,%rax,4), %xmm2
	movups	-28(%r15,%rax,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%rax,4)
	movups	%xmm3, -28(%rdi,%rax,4)
	movl	$8, %esi
	testq	%rdx, %rdx
	jne	.LBB3_13
	jmp	.LBB3_15
.LBB3_11:
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB3_17
.LBB3_12:
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB3_15
.LBB3_13:                               # %vector.body.preheader.new
	movq	%r8, %r9
	negq	%r9
	negq	%rsi
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	-12(%rcx,%rax,4), %rbp
	leaq	-12(%r15,%rax,4), %rbx
	leaq	-12(%rdi,%rax,4), %rdx
	.p2align	4, 0x90
.LBB3_14:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rsi,4), %xmm0
	movups	-16(%rbp,%rsi,4), %xmm1
	movups	(%rbx,%rsi,4), %xmm2
	movups	-16(%rbx,%rsi,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rsi,4)
	movups	%xmm3, -16(%rdx,%rsi,4)
	movups	-32(%rbp,%rsi,4), %xmm0
	movups	-48(%rbp,%rsi,4), %xmm1
	movups	-32(%rbx,%rsi,4), %xmm2
	movups	-48(%rbx,%rsi,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdx,%rsi,4)
	movups	%xmm3, -48(%rdx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %r9
	jne	.LBB3_14
.LBB3_15:                               # %middle.block
	cmpq	%r8, %r10
	movq	(%rsp), %rbx            # 8-byte Reload
	je	.LBB3_19
# BB#16:
	subq	%r8, %rax
.LBB3_17:                               # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB3_18:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15,%rax,4), %ecx
	notl	%ecx
	andl	-4(%rbx,%rax,4), %ecx
	movl	%ecx, -4(%rdi,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB3_18
.LBB3_19:                               # %.loopexit310
	movl	(%rbx), %eax
	movl	%eax, %ecx
	notl	%ecx
	movl	$-1024, %edx            # imm = 0xFC00
	andl	(%r14), %edx
	orq	$-1024, %rcx            # imm = 0xFC00
	andl	$1023, %eax             # imm = 0x3FF
	movq	$-2, %rsi
	cmoveq	%rcx, %rsi
	orl	%eax, %edx
	movl	%edx, (%r14)
	leaq	2(%rsi,%rax), %r10
	cmpq	$8, %r10
	jb	.LBB3_32
# BB#20:                                # %min.iters.checked135
	movq	%r10, %r8
	andq	$-8, %r8
	je	.LBB3_32
# BB#21:                                # %vector.memcheck161
	testl	%eax, %eax
	movl	$1, %edx
	cmoveq	%rax, %rdx
	leaq	(%r14,%rdx,4), %rsi
	leaq	4(%r14,%rax,4), %rbp
	leaq	(%rbx,%rdx,4), %rcx
	leaq	4(%rbx,%rax,4), %rbx
	leaq	(%r15,%rdx,4), %r9
	leaq	4(%r15,%rax,4), %rdx
	cmpq	%rbx, %rsi
	sbbb	%bl, %bl
	cmpq	%rbp, %rcx
	sbbb	%cl, %cl
	andb	%bl, %cl
	cmpq	%rdx, %rsi
	sbbb	%dl, %dl
	cmpq	%rbp, %r9
	sbbb	%bl, %bl
	testb	$1, %cl
	jne	.LBB3_26
# BB#22:                                # %vector.memcheck161
	andb	%bl, %dl
	andb	$1, %dl
	jne	.LBB3_26
# BB#23:                                # %vector.body130.preheader
	leaq	-8(%r8), %rcx
	movq	%rcx, %rdx
	shrq	$3, %rdx
	btl	$3, %ecx
	movq	(%rsp), %rcx            # 8-byte Reload
	jb	.LBB3_27
# BB#24:                                # %vector.body130.prol
	movups	-12(%rcx,%rax,4), %xmm0
	movups	-28(%rcx,%rax,4), %xmm1
	movups	-12(%r15,%rax,4), %xmm2
	movups	-28(%r15,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r14,%rax,4)
	movups	%xmm3, -28(%r14,%rax,4)
	movl	$8, %esi
	testq	%rdx, %rdx
	jne	.LBB3_28
	jmp	.LBB3_30
.LBB3_26:
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB3_32
.LBB3_27:
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB3_30
.LBB3_28:                               # %vector.body130.preheader.new
	movq	%r8, %r9
	negq	%r9
	negq	%rsi
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	-12(%rcx,%rax,4), %rbp
	leaq	-12(%r15,%rax,4), %rbx
	leaq	-12(%r14,%rax,4), %rdx
	.p2align	4, 0x90
.LBB3_29:                               # %vector.body130
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rsi,4), %xmm0
	movups	-16(%rbp,%rsi,4), %xmm1
	movups	(%rbx,%rsi,4), %xmm2
	movups	-16(%rbx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rsi,4)
	movups	%xmm3, -16(%rdx,%rsi,4)
	movups	-32(%rbp,%rsi,4), %xmm0
	movups	-48(%rbp,%rsi,4), %xmm1
	movups	-32(%rbx,%rsi,4), %xmm2
	movups	-48(%rbx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdx,%rsi,4)
	movups	%xmm3, -48(%rdx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %r9
	jne	.LBB3_29
.LBB3_30:                               # %middle.block131
	cmpq	%r8, %r10
	movq	(%rsp), %rbx            # 8-byte Reload
	je	.LBB3_34
# BB#31:
	subq	%r8, %rax
.LBB3_32:                               # %scalar.ph132.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB3_33:                               # %scalar.ph132
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15,%rax,4), %ecx
	andl	-4(%rbx,%rax,4), %ecx
	movl	%ecx, -4(%r14,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB3_33
.LBB3_34:                               # %.loopexit309
	movl	cube+8(%rip), %eax
	xorl	%ebx, %ebx
	cmpl	cube+4(%rip), %eax
	jge	.LBB3_55
# BB#35:                                # %.lr.ph.preheader
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movslq	%eax, %r13
	leaq	-4(%r12), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	4(%r12), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	-4(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	4(%r15), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	-4(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	4(%r14), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	-12(%r15), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	-12(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-12(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_36:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_44 Depth 2
                                        #     Child Loop BB3_51 Depth 2
	movq	cube+72(%rip), %rax
	movq	(%rax,%r13,8), %rbp
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	setp_disjoint
	testl	%eax, %eax
	jne	.LBB3_53
# BB#37:                                #   in Loop: Header=BB3_36 Depth=1
	movl	(%r15), %esi
	movl	(%r12), %eax
	movl	$-1024, %ecx            # imm = 0xFC00
	andl	%ecx, %eax
	andl	$1023, %esi             # imm = 0x3FF
	movq	%rsi, %r9
	movl	$1, %ecx
	cmoveq	%rcx, %r9
	movl	%esi, %ecx
	orl	%eax, %ecx
	movl	%ecx, (%r12)
	cmpq	$8, %r9
	jb	.LBB3_49
# BB#39:                                # %min.iters.checked188
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	%r9, %rax
	andq	$1016, %rax             # imm = 0x3F8
	je	.LBB3_49
# BB#40:                                # %vector.memcheck230
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	1(%rsi), %rax
	testl	%esi, %esi
	movl	$2, %ecx
	cmovneq	%rcx, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rdx
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,4), %r8
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %r10
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rbx
	cmpq	%rbx, %rdx
	leaq	-4(%rbp,%rax,4), %r11
	sbbb	%cl, %cl
	cmpq	%r8, %r10
	sbbb	%bl, %bl
	andb	%cl, %bl
	leaq	4(%rbp,%rsi,4), %rcx
	cmpq	%rcx, %rdx
	sbbb	%cl, %cl
	cmpq	%r8, %r11
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rsi,4), %r11
	sbbb	%r10b, %r10b
	cmpq	%r11, %rdx
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	sbbb	%al, %al
	cmpq	%r8, %rdx
	sbbb	%dl, %dl
	testb	$1, %bl
	jne	.LBB3_49
# BB#41:                                # %vector.memcheck230
                                        #   in Loop: Header=BB3_36 Depth=1
	andb	%r10b, %cl
	andb	$1, %cl
	jne	.LBB3_49
# BB#42:                                # %vector.memcheck230
                                        #   in Loop: Header=BB3_36 Depth=1
	andb	%dl, %al
	andb	$1, %al
	jne	.LBB3_49
# BB#43:                                # %vector.body183.preheader
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	%rsi, %rdx
	movq	96(%rsp), %r8           # 8-byte Reload
	subq	%r8, %rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,4), %rdi
	leaq	-12(%rbp,%rsi,4), %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,4), %rax
	movq	24(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rsi,4), %rsi
	movq	%r9, %r10
	andq	$-8, %r10
	negq	%r10
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_44:                               # %vector.body183
                                        #   Parent Loop BB3_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbx,4), %xmm0
	movups	-16(%rdi,%rbx,4), %xmm1
	movups	(%rcx,%rbx,4), %xmm2
	movups	-16(%rcx,%rbx,4), %xmm3
	andps	%xmm2, %xmm0
	andps	%xmm3, %xmm1
	movups	(%rax,%rbx,4), %xmm4
	movups	-16(%rax,%rbx,4), %xmm5
	andnps	%xmm4, %xmm2
	andnps	%xmm5, %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rsi,%rbx,4)
	movups	%xmm3, -16(%rsi,%rbx,4)
	addq	$-8, %rbx
	cmpq	%rbx, %r10
	jne	.LBB3_44
# BB#45:                                # %middle.block184
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpq	%r8, %r9
	jne	.LBB3_50
	jmp	.LBB3_52
	.p2align	4, 0x90
.LBB3_49:                               #   in Loop: Header=BB3_36 Depth=1
	movq	%rsi, %rdx
.LBB3_50:                               # %scalar.ph185.preheader
                                        #   in Loop: Header=BB3_36 Depth=1
	incq	%rdx
	.p2align	4, 0x90
.LBB3_51:                               # %scalar.ph185
                                        #   Parent Loop BB3_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbp,%rdx,4), %eax
	movl	-4(%r15,%rdx,4), %ecx
	andl	%eax, %ecx
	notl	%eax
	andl	-4(%r14,%rdx,4), %eax
	orl	%ecx, %eax
	movl	%eax, -4(%r12,%rdx,4)
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB3_51
.LBB3_52:                               # %.loopexit
                                        #   in Loop: Header=BB3_36 Depth=1
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r12, %rsi
	callq	sf_addset
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %ebx
.LBB3_53:                               #   in Loop: Header=BB3_36 Depth=1
	incq	%r13
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %r13
	movq	16(%rsp), %rdi          # 8-byte Reload
	jl	.LBB3_36
# BB#54:                                # %._crit_edge.loopexit
	movl	cube+8(%rip), %eax
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB3_55:                               # %._crit_edge
	testl	%ebx, %ebx
	jne	.LBB3_63
# BB#56:                                # %._crit_edge
	testl	%eax, %eax
	jle	.LBB3_63
# BB#57:
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	(%rbx), %eax
	movl	%eax, %ecx
	notl	%ecx
	movl	$-1024, %edx            # imm = 0xFC00
	andl	(%r12), %edx
	orq	$-1024, %rcx            # imm = 0xFC00
	andl	$1023, %eax             # imm = 0x3FF
	movq	$-2, %rsi
	cmoveq	%rcx, %rsi
	orl	%eax, %edx
	movl	%edx, (%r12)
	leaq	2(%rsi,%rax), %r10
	cmpq	$8, %r10
	jb	.LBB3_71
# BB#58:                                # %min.iters.checked261
	movq	%r10, %r8
	andq	$-8, %r8
	je	.LBB3_71
# BB#59:                                # %vector.memcheck287
	testl	%eax, %eax
	movl	$1, %edx
	cmoveq	%rax, %rdx
	leaq	(%r12,%rdx,4), %rsi
	leaq	4(%r12,%rax,4), %rdi
	leaq	(%rbx,%rdx,4), %rbp
	leaq	4(%rbx,%rax,4), %rbx
	leaq	(%r15,%rdx,4), %r9
	leaq	4(%r15,%rax,4), %rdx
	cmpq	%rbx, %rsi
	sbbb	%bl, %bl
	cmpq	%rdi, %rbp
	sbbb	%cl, %cl
	andb	%bl, %cl
	cmpq	%rdx, %rsi
	sbbb	%dl, %dl
	cmpq	%rdi, %r9
	sbbb	%bl, %bl
	testb	$1, %cl
	jne	.LBB3_65
# BB#60:                                # %vector.memcheck287
	andb	%bl, %dl
	andb	$1, %dl
	jne	.LBB3_65
# BB#61:                                # %vector.body256.preheader
	leaq	-8(%r8), %rcx
	movq	%rcx, %rdx
	shrq	$3, %rdx
	btl	$3, %ecx
	jb	.LBB3_66
# BB#62:                                # %vector.body256.prol
	movq	(%rsp), %rcx            # 8-byte Reload
	movups	-12(%rcx,%rax,4), %xmm0
	movups	-28(%rcx,%rax,4), %xmm1
	movups	-12(%r15,%rax,4), %xmm2
	movups	-28(%r15,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r12,%rax,4)
	movups	%xmm3, -28(%r12,%rax,4)
	movl	$8, %esi
	testq	%rdx, %rdx
	jne	.LBB3_67
	jmp	.LBB3_69
.LBB3_63:
	testq	%r12, %r12
	jne	.LBB3_74
	jmp	.LBB3_75
.LBB3_65:
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB3_71
.LBB3_66:
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB3_69
.LBB3_67:                               # %vector.body256.preheader.new
	movq	%r8, %rdi
	negq	%rdi
	negq	%rsi
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	-12(%rcx,%rax,4), %rbp
	leaq	-12(%r15,%rax,4), %rbx
	leaq	-12(%r12,%rax,4), %rdx
	.p2align	4, 0x90
.LBB3_68:                               # %vector.body256
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rsi,4), %xmm0
	movups	-16(%rbp,%rsi,4), %xmm1
	movups	(%rbx,%rsi,4), %xmm2
	movups	-16(%rbx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rsi,4)
	movups	%xmm3, -16(%rdx,%rsi,4)
	movups	-32(%rbp,%rsi,4), %xmm0
	movups	-48(%rbp,%rsi,4), %xmm1
	movups	-32(%rbx,%rsi,4), %xmm2
	movups	-48(%rbx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdx,%rsi,4)
	movups	%xmm3, -48(%rdx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB3_68
.LBB3_69:                               # %middle.block257
	cmpq	%r8, %r10
	movq	(%rsp), %rbx            # 8-byte Reload
	je	.LBB3_73
# BB#70:
	subq	%r8, %rax
.LBB3_71:                               # %scalar.ph258.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB3_72:                               # %scalar.ph258
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15,%rax,4), %ecx
	andl	-4(%rbx,%rax,4), %ecx
	movl	%ecx, -4(%r12,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB3_72
.LBB3_73:                               # %.thread
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	sf_addset
	movq	%rax, %r13
.LBB3_74:
	movq	%r12, %rdi
	callq	free
.LBB3_75:
	movq	%r13, %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	cb_consensus_dist0, .Lfunc_end3-cb_consensus_dist0
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ESSENTIAL: %s\n"
	.size	.L.str, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
