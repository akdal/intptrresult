	.text
	.file	"btConvex2dConvex2dAlgorithm.bc"
	.globl	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver,@function
_ZN27btConvex2dConvex2dAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver: # @_ZN27btConvex2dConvex2dAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.cfi_startproc
# BB#0:
	movb	$0, 8(%rdi)
	movq	$_ZTVN27btConvex2dConvex2dAlgorithm10CreateFuncE+16, (%rdi)
	movl	$0, 32(%rdi)
	movl	$3, 36(%rdi)
	movq	%rsi, 24(%rdi)
	movq	%rdx, 16(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver, .Lfunc_end0-_ZN27btConvex2dConvex2dAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.cfi_endproc

	.globl	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD2Ev
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD2Ev,@function
_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD2Ev: # @_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD2Ev, .Lfunc_end1-_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD2Ev
	.cfi_endproc

	.globl	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD0Ev,@function
_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD0Ev: # @_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD0Ev, .Lfunc_end2-_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.globl	_ZN27btConvex2dConvex2dAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii,@function
_ZN27btConvex2dConvex2dAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii: # @_ZN27btConvex2dConvex2dAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	80(%rsp), %r12d
	movl	72(%rsp), %r13d
	movq	64(%rsp), %rbp
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV27btConvex2dConvex2dAlgorithm+16, (%rbx)
	movq	%r14, 16(%rbx)
	movq	%rbp, 24(%rbx)
	movb	$0, 32(%rbx)
	movq	%r15, 40(%rbx)
	movb	$0, 48(%rbx)
	movl	%r13d, 52(%rbx)
	movl	%r12d, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN27btConvex2dConvex2dAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii, .Lfunc_end3-_ZN27btConvex2dConvex2dAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii
	.cfi_endproc

	.globl	_ZN27btConvex2dConvex2dAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithmD2Ev,@function
_ZN27btConvex2dConvex2dAlgorithmD2Ev:   # @_ZN27btConvex2dConvex2dAlgorithmD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV27btConvex2dConvex2dAlgorithm+16, (%rbx)
	cmpb	$0, 32(%rbx)
	je	.LBB4_3
# BB#1:
	movq	40(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB4_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	callq	*32(%rax)
.Ltmp1:
.LBB4_3:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN30btActivatingCollisionAlgorithmD2Ev # TAILCALL
.LBB4_4:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp4:
# BB#5:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_6:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN27btConvex2dConvex2dAlgorithmD2Ev, .Lfunc_end4-_ZN27btConvex2dConvex2dAlgorithmD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end5:
	.size	__clang_call_terminate, .Lfunc_end5-__clang_call_terminate

	.text
	.globl	_ZN27btConvex2dConvex2dAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithmD0Ev,@function
_ZN27btConvex2dConvex2dAlgorithmD0Ev:   # @_ZN27btConvex2dConvex2dAlgorithmD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV27btConvex2dConvex2dAlgorithm+16, (%rbx)
	cmpb	$0, 32(%rbx)
	je	.LBB6_3
# BB#1:
	movq	40(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB6_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp6:
	callq	*32(%rax)
.Ltmp7:
.LBB6_3:
.Ltmp12:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp13:
# BB#4:                                 # %_ZN27btConvex2dConvex2dAlgorithmD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_5:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp10:
	jmp	.LBB6_8
.LBB6_6:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_7:
.Ltmp14:
	movq	%rax, %r14
.LBB6_8:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN27btConvex2dConvex2dAlgorithmD0Ev, .Lfunc_end6-_ZN27btConvex2dConvex2dAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN27btConvex2dConvex2dAlgorithm19setLowLevelOfDetailEb
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithm19setLowLevelOfDetailEb,@function
_ZN27btConvex2dConvex2dAlgorithm19setLowLevelOfDetailEb: # @_ZN27btConvex2dConvex2dAlgorithm19setLowLevelOfDetailEb
	.cfi_startproc
# BB#0:
	movb	%sil, 48(%rdi)
	retq
.Lfunc_end7:
	.size	_ZN27btConvex2dConvex2dAlgorithm19setLowLevelOfDetailEb, .Lfunc_end7-_ZN27btConvex2dConvex2dAlgorithm19setLowLevelOfDetailEb
	.cfi_endproc

	.globl	_ZN27btConvex2dConvex2dAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN27btConvex2dConvex2dAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN27btConvex2dConvex2dAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi29:
	.cfi_def_cfa_offset 320
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	40(%r12), %rax
	testq	%rax, %rax
	jne	.LBB8_2
# BB#1:
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	*24(%rax)
	movq	%rax, 40(%r12)
	movb	$1, 32(%r12)
.LBB8_2:
	movq	%rax, 8(%r14)
	movq	200(%rbx), %r15
	movq	200(%r13), %rbp
	movl	$1566444395, 144(%rsp)  # imm = 0x5D5E0B6B
	movq	$0, 152(%rsp)
	movq	16(%r12), %rcx
	movq	24(%r12), %r8
	leaq	168(%rsp), %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	movq	%r15, 208(%rsp)
	movq	%rbp, 216(%rsp)
	movq	(%r15), %rax
.Ltmp15:
	movq	%r15, %rdi
	callq	*88(%rax)
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
.Ltmp16:
# BB#3:
	movq	(%rbp), %rax
.Ltmp17:
	movq	%rbp, %rdi
	callq	*88(%rax)
	movss	%xmm0, (%rsp)           # 4-byte Spill
.Ltmp18:
# BB#4:
	movq	40(%r12), %rdi
.Ltmp19:
	callq	_ZNK20btPersistentManifold27getContactBreakingThresholdEv
.Ltmp20:
# BB#5:
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	(%rsp), %xmm1           # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	movss	%xmm1, 144(%rsp)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	48(%rcx), %rax
	movq	%rax, 152(%rsp)
	movups	8(%rbx), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	24(%rbx), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	40(%rbx), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	56(%rbx), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	8(%r13), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	24(%r13), %xmm0
	movaps	%xmm0, 96(%rsp)
	movups	40(%r13), %xmm0
	movaps	%xmm0, 112(%rsp)
	movups	56(%r13), %xmm0
	movaps	%xmm0, 128(%rsp)
	movq	24(%rcx), %rcx
.Ltmp21:
	leaq	168(%rsp), %rdi
	leaq	16(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	callq	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
.Ltmp22:
# BB#6:
	cmpb	$0, 32(%r12)
	je	.LBB8_11
# BB#7:
	movq	8(%r14), %rdi
	cmpl	$0, 728(%rdi)
	je	.LBB8_11
# BB#8:
	movq	712(%rdi), %rax
	cmpq	144(%r14), %rax
	je	.LBB8_12
# BB#9:
	leaq	80(%r14), %rsi
	addq	$16, %r14
	jmp	.LBB8_10
.LBB8_12:
	leaq	16(%r14), %rsi
	addq	$80, %r14
.LBB8_10:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit
	movq	%r14, %rdx
	callq	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
.LBB8_11:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_13:
.Ltmp23:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN27btConvex2dConvex2dAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end8-_ZN27btConvex2dConvex2dAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp15-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp22-.Ltmp15         #   Call between .Ltmp15 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end8-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN27btConvex2dConvex2dAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN27btConvex2dConvex2dAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN27btConvex2dConvex2dAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$1336, %rsp             # imm = 0x538
.Lcfi42:
	.cfi_def_cfa_offset 1392
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movss	120(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	124(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	56(%rbx), %xmm0
	subss	60(%rbx), %xmm1
	movss	128(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	64(%rbx), %xmm2
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	268(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	ucomiss	%xmm2, %xmm0
	jbe	.LBB9_3
# BB#1:
	movss	120(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	124(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	56(%r14), %xmm0
	subss	60(%r14), %xmm1
	movss	128(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	64(%r14), %xmm2
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	268(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	ucomiss	%xmm2, %xmm0
	jbe	.LBB9_3
# BB#2:
	movss	.LCPI9_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB9_25
.LBB9_3:
	movq	200(%rbx), %r12
	movl	264(%r14), %ebp
	leaq	168(%rsp), %r15
	movq	%r15, %rdi
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV13btSphereShape+16, 168(%rsp)
	movl	$8, 176(%rsp)
	movl	%ebp, 208(%rsp)
	movl	%ebp, 224(%rsp)
	movq	$_ZTVN12btConvexCast10CastResultE+16, 424(%rsp)
	movl	$1566444395, 592(%rsp)  # imm = 0x5D5E0B6B
	movq	$0, 600(%rsp)
	movl	$0, 608(%rsp)
	andb	$-16, 1304(%rsp)
.Ltmp24:
	leaq	72(%rsp), %rdi
	leaq	976(%rsp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
.Ltmp25:
# BB#4:
	leaq	72(%rbx), %r15
	leaq	8(%rbx), %r12
	leaq	72(%r14), %r13
	leaq	8(%r14), %rbp
.Ltmp27:
	leaq	72(%rsp), %rdi
	leaq	424(%rsp), %r9
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movq	%r13, %r8
	callq	_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
.Ltmp28:
# BB#5:
	testb	%al, %al
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB9_6
# BB#7:
	movss	260(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	592(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_9
# BB#8:
	movss	%xmm0, 260(%rbx)
.LBB9_9:
	movss	260(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_11
# BB#10:
	movss	%xmm0, 260(%r14)
.LBB9_11:
	movss	.LCPI9_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_13
# BB#12:
	movaps	%xmm0, %xmm1
	jmp	.LBB9_13
.LBB9_6:
	movss	.LCPI9_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
.LBB9_13:
.Ltmp32:
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	leaq	72(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp33:
# BB#14:
	leaq	168(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
	movq	200(%r14), %r15
	movl	264(%rbx), %ebp
	leaq	104(%rsp), %r12
	movq	%r12, %rdi
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV13btSphereShape+16, 104(%rsp)
	movl	$8, 112(%rsp)
	movl	%ebp, 144(%rsp)
	movl	%ebp, 160(%rsp)
	movq	$_ZTVN12btConvexCast10CastResultE+16, 232(%rsp)
	movl	$1566444395, 400(%rsp)  # imm = 0x5D5E0B6B
	movq	$0, 408(%rsp)
	movl	$0, 416(%rsp)
	movb	$0, 944(%rsp)
.Ltmp37:
	leaq	40(%rsp), %rdi
	leaq	616(%rsp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
.Ltmp38:
# BB#15:
.Ltmp40:
	leaq	40(%rsp), %rdi
	leaq	232(%rsp), %r9
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	callq	_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
.Ltmp41:
# BB#16:
	testb	%al, %al
	je	.LBB9_23
# BB#17:
	movss	260(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	400(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_19
# BB#18:
	movss	%xmm0, 260(%rbx)
.LBB9_19:
	movss	260(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_21
# BB#20:
	movss	%xmm0, 260(%r14)
.LBB9_21:
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_23
# BB#22:
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
.LBB9_23:
.Ltmp45:
	leaq	40(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp46:
# BB#24:
	leaq	104(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB9_25:
	addq	$1336, %rsp             # imm = 0x538
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_33:
.Ltmp47:
	jmp	.LBB9_34
.LBB9_32:
.Ltmp42:
	movq	%rax, %rbx
.Ltmp43:
	leaq	40(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp44:
	jmp	.LBB9_35
.LBB9_31:
.Ltmp39:
.LBB9_34:
	movq	%rax, %rbx
.LBB9_35:
.Ltmp48:
	leaq	104(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp49:
	jmp	.LBB9_36
.LBB9_28:
.Ltmp34:
	jmp	.LBB9_29
.LBB9_27:
.Ltmp29:
	movq	%rax, %rbx
.Ltmp30:
	leaq	72(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp31:
	jmp	.LBB9_30
.LBB9_26:
.Ltmp26:
.LBB9_29:
	movq	%rax, %rbx
.LBB9_30:
.Ltmp35:
	leaq	168(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp36:
.LBB9_36:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_37:
.Ltmp50:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN27btConvex2dConvex2dAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end9-_ZN27btConvex2dConvex2dAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp24-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin3   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin3   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin3   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp37-.Ltmp33         #   Call between .Ltmp33 and .Ltmp37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin3   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin3   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin3   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp43-.Ltmp46         #   Call between .Ltmp46 and .Ltmp43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp36-.Ltmp43         #   Call between .Ltmp43 and .Ltmp36
	.long	.Ltmp50-.Lfunc_begin3   #     jumps to .Ltmp50
	.byte	1                       #   On action: 1
	.long	.Ltmp36-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Lfunc_end9-.Ltmp36     #   Call between .Ltmp36 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12btConvexCast10CastResultD2Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD2Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD2Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD2Ev,@function
_ZN12btConvexCast10CastResultD2Ev:      # @_ZN12btConvexCast10CastResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZN12btConvexCast10CastResultD2Ev, .Lfunc_end10-_ZN12btConvexCast10CastResultD2Ev
	.cfi_endproc

	.section	.text._ZN27btConvex2dConvex2dAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN27btConvex2dConvex2dAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN27btConvex2dConvex2dAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN27btConvex2dConvex2dAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN27btConvex2dConvex2dAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 80
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$64, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	movq	8(%r12), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	16(%r13), %r14
	movq	24(%r13), %r15
	movq	32(%r13), %rbp
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV27btConvex2dConvex2dAlgorithm+16, (%rbx)
	movq	%r15, 16(%rbx)
	movq	%r14, 24(%rbx)
	movb	$0, 32(%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 40(%rbx)
	movb	$0, 48(%rbx)
	movq	%rbp, 52(%rbx)
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN27btConvex2dConvex2dAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end11-_ZN27btConvex2dConvex2dAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN27btConvex2dConvex2dAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN27btConvex2dConvex2dAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN27btConvex2dConvex2dAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN27btConvex2dConvex2dAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN27btConvex2dConvex2dAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN27btConvex2dConvex2dAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 48
.Lcfi67:
	.cfi_offset %rbx, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	40(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB12_18
# BB#1:
	cmpb	$0, 32(%r14)
	je	.LBB12_18
# BB#2:
	movl	4(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB12_17
# BB#3:
	leal	(%rax,%rax), %edx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%edx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB12_17
# BB#4:
	testl	%ebp, %ebp
	je	.LBB12_5
# BB#6:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB12_8
	jmp	.LBB12_12
.LBB12_5:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB12_12
.LBB12_8:                               # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB12_10
	.p2align	4, 0x90
.LBB12_9:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB12_9
.LBB12_10:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB12_12
	.p2align	4, 0x90
.LBB12_11:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB12_11
.LBB12_12:                              # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_16
# BB#13:
	cmpb	$0, 24(%rbx)
	je	.LBB12_15
# BB#14:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%rbx), %eax
.LBB12_15:
	movq	$0, 16(%rbx)
.LBB12_16:                              # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
	movb	$1, 24(%rbx)
	movq	%r15, 16(%rbx)
	movl	%ebp, 8(%rbx)
	movq	40(%r14), %rcx
.LBB12_17:                              # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
	movq	16(%rbx), %rdx
	cltq
	movq	%rcx, (%rdx,%rax,8)
	incl	%eax
	movl	%eax, 4(%rbx)
.LBB12_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN27btConvex2dConvex2dAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end12-_ZN27btConvex2dConvex2dAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult9DebugDrawEf,"axG",@progbits,_ZN12btConvexCast10CastResult9DebugDrawEf,comdat
	.weak	_ZN12btConvexCast10CastResult9DebugDrawEf
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult9DebugDrawEf,@function
_ZN12btConvexCast10CastResult9DebugDrawEf: # @_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end13:
	.size	_ZN12btConvexCast10CastResult9DebugDrawEf, .Lfunc_end13-_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,"axG",@progbits,_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,comdat
	.weak	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,@function
_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform: # @_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end14:
	.size	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform, .Lfunc_end14-_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResultD0Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD0Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD0Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD0Ev,@function
_ZN12btConvexCast10CastResultD0Ev:      # @_ZN12btConvexCast10CastResultD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end15:
	.size	_ZN12btConvexCast10CastResultD0Ev, .Lfunc_end15-_ZN12btConvexCast10CastResultD0Ev
	.cfi_endproc

	.type	_ZTVN27btConvex2dConvex2dAlgorithm10CreateFuncE,@object # @_ZTVN27btConvex2dConvex2dAlgorithm10CreateFuncE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN27btConvex2dConvex2dAlgorithm10CreateFuncE
	.p2align	3
_ZTVN27btConvex2dConvex2dAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN27btConvex2dConvex2dAlgorithm10CreateFuncE
	.quad	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD2Ev
	.quad	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD0Ev
	.quad	_ZN27btConvex2dConvex2dAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN27btConvex2dConvex2dAlgorithm10CreateFuncE, 40

	.type	_ZTV27btConvex2dConvex2dAlgorithm,@object # @_ZTV27btConvex2dConvex2dAlgorithm
	.globl	_ZTV27btConvex2dConvex2dAlgorithm
	.p2align	3
_ZTV27btConvex2dConvex2dAlgorithm:
	.quad	0
	.quad	_ZTI27btConvex2dConvex2dAlgorithm
	.quad	_ZN27btConvex2dConvex2dAlgorithmD2Ev
	.quad	_ZN27btConvex2dConvex2dAlgorithmD0Ev
	.quad	_ZN27btConvex2dConvex2dAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN27btConvex2dConvex2dAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN27btConvex2dConvex2dAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV27btConvex2dConvex2dAlgorithm, 56

	.type	_ZTSN27btConvex2dConvex2dAlgorithm10CreateFuncE,@object # @_ZTSN27btConvex2dConvex2dAlgorithm10CreateFuncE
	.globl	_ZTSN27btConvex2dConvex2dAlgorithm10CreateFuncE
	.p2align	4
_ZTSN27btConvex2dConvex2dAlgorithm10CreateFuncE:
	.asciz	"N27btConvex2dConvex2dAlgorithm10CreateFuncE"
	.size	_ZTSN27btConvex2dConvex2dAlgorithm10CreateFuncE, 44

	.type	_ZTS30btCollisionAlgorithmCreateFunc,@object # @_ZTS30btCollisionAlgorithmCreateFunc
	.section	.rodata._ZTS30btCollisionAlgorithmCreateFunc,"aG",@progbits,_ZTS30btCollisionAlgorithmCreateFunc,comdat
	.weak	_ZTS30btCollisionAlgorithmCreateFunc
	.p2align	4
_ZTS30btCollisionAlgorithmCreateFunc:
	.asciz	"30btCollisionAlgorithmCreateFunc"
	.size	_ZTS30btCollisionAlgorithmCreateFunc, 33

	.type	_ZTI30btCollisionAlgorithmCreateFunc,@object # @_ZTI30btCollisionAlgorithmCreateFunc
	.section	.rodata._ZTI30btCollisionAlgorithmCreateFunc,"aG",@progbits,_ZTI30btCollisionAlgorithmCreateFunc,comdat
	.weak	_ZTI30btCollisionAlgorithmCreateFunc
	.p2align	3
_ZTI30btCollisionAlgorithmCreateFunc:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS30btCollisionAlgorithmCreateFunc
	.size	_ZTI30btCollisionAlgorithmCreateFunc, 16

	.type	_ZTIN27btConvex2dConvex2dAlgorithm10CreateFuncE,@object # @_ZTIN27btConvex2dConvex2dAlgorithm10CreateFuncE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN27btConvex2dConvex2dAlgorithm10CreateFuncE
	.p2align	4
_ZTIN27btConvex2dConvex2dAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN27btConvex2dConvex2dAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN27btConvex2dConvex2dAlgorithm10CreateFuncE, 24

	.type	_ZTS27btConvex2dConvex2dAlgorithm,@object # @_ZTS27btConvex2dConvex2dAlgorithm
	.globl	_ZTS27btConvex2dConvex2dAlgorithm
	.p2align	4
_ZTS27btConvex2dConvex2dAlgorithm:
	.asciz	"27btConvex2dConvex2dAlgorithm"
	.size	_ZTS27btConvex2dConvex2dAlgorithm, 30

	.type	_ZTI27btConvex2dConvex2dAlgorithm,@object # @_ZTI27btConvex2dConvex2dAlgorithm
	.globl	_ZTI27btConvex2dConvex2dAlgorithm
	.p2align	4
_ZTI27btConvex2dConvex2dAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27btConvex2dConvex2dAlgorithm
	.quad	_ZTI30btActivatingCollisionAlgorithm
	.size	_ZTI27btConvex2dConvex2dAlgorithm, 24

	.type	_ZTVN12btConvexCast10CastResultE,@object # @_ZTVN12btConvexCast10CastResultE
	.section	.rodata._ZTVN12btConvexCast10CastResultE,"aG",@progbits,_ZTVN12btConvexCast10CastResultE,comdat
	.weak	_ZTVN12btConvexCast10CastResultE
	.p2align	3
_ZTVN12btConvexCast10CastResultE:
	.quad	0
	.quad	_ZTIN12btConvexCast10CastResultE
	.quad	_ZN12btConvexCast10CastResult9DebugDrawEf
	.quad	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.quad	_ZN12btConvexCast10CastResultD2Ev
	.quad	_ZN12btConvexCast10CastResultD0Ev
	.size	_ZTVN12btConvexCast10CastResultE, 48

	.type	_ZTSN12btConvexCast10CastResultE,@object # @_ZTSN12btConvexCast10CastResultE
	.section	.rodata._ZTSN12btConvexCast10CastResultE,"aG",@progbits,_ZTSN12btConvexCast10CastResultE,comdat
	.weak	_ZTSN12btConvexCast10CastResultE
	.p2align	4
_ZTSN12btConvexCast10CastResultE:
	.asciz	"N12btConvexCast10CastResultE"
	.size	_ZTSN12btConvexCast10CastResultE, 29

	.type	_ZTIN12btConvexCast10CastResultE,@object # @_ZTIN12btConvexCast10CastResultE
	.section	.rodata._ZTIN12btConvexCast10CastResultE,"aG",@progbits,_ZTIN12btConvexCast10CastResultE,comdat
	.weak	_ZTIN12btConvexCast10CastResultE
	.p2align	3
_ZTIN12btConvexCast10CastResultE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN12btConvexCast10CastResultE
	.size	_ZTIN12btConvexCast10CastResultE, 16


	.globl	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncC1EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.type	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncC1EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver,@function
_ZN27btConvex2dConvex2dAlgorithm10CreateFuncC1EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver = _ZN27btConvex2dConvex2dAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.globl	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD1Ev
	.type	_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD1Ev,@function
_ZN27btConvex2dConvex2dAlgorithm10CreateFuncD1Ev = _ZN27btConvex2dConvex2dAlgorithm10CreateFuncD2Ev
	.globl	_ZN27btConvex2dConvex2dAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii
	.type	_ZN27btConvex2dConvex2dAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii,@function
_ZN27btConvex2dConvex2dAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii = _ZN27btConvex2dConvex2dAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii
	.globl	_ZN27btConvex2dConvex2dAlgorithmD1Ev
	.type	_ZN27btConvex2dConvex2dAlgorithmD1Ev,@function
_ZN27btConvex2dConvex2dAlgorithmD1Ev = _ZN27btConvex2dConvex2dAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
