	.text
	.file	"annexb.bc"
	.globl	GetAnnexbNALU
	.p2align	4, 0x90
	.type	GetAnnexbNALU,@function
GetAnnexbNALU:                          # @GetAnnexbNALU
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_2:                                # %.preheader122.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader122
                                        # =>This Inner Loop Header: Depth=1
	movq	bits(%rip), %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	bits(%rip), %rdi
	callq	fgetc
	movb	%al, (%r13,%rbp)
	incq	%rbp
	testb	%al, %al
	je	.LBB0_3
.LBB0_5:                                # %.critedge
	movq	bits(%rip), %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB0_10
# BB#6:
	testl	%ebp, %ebp
	je	.LBB0_7
# BB#8:
	movl	$.Lstr.4, %edi
	jmp	.LBB0_9
.LBB0_10:
	movslq	%ebp, %rax
	cmpb	$1, -1(%r13,%rax)
	jne	.LBB0_11
# BB#12:
	cmpl	$2, %ebp
	jg	.LBB0_13
.LBB0_11:
	movl	$.Lstr.3, %edi
.LBB0_9:
	callq	puts
	movq	%r13, %rdi
	callq	free
	movl	$-1, %r15d
	jmp	.LBB0_33
.LBB0_7:
	movq	%r13, %rdi
	callq	free
	xorl	%r15d, %r15d
.LBB0_33:
	movl	%r15d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_13:
	leal	-4(%rbp), %ecx
	xorl	%esi, %esi
	xorl	%edx, %edx
	cmpl	$3, %ebp
	setne	%dl
	cmovnel	%ecx, %esi
	addl	$3, %edx
	movl	%edx, (%rbx)
	testl	%esi, %esi
	jle	.LBB0_16
# BB#14:
	movl	IsFirstByteStreamNALU(%rip), %ecx
	testl	%ecx, %ecx
	jne	.LBB0_16
# BB#15:
	movl	$.Lstr.1, %edi
	jmp	.LBB0_9
.LBB0_16:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	$0, IsFirstByteStreamNALU(%rip)
	leaq	(%r13,%rax), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	shlq	$32, %rax
	movabsq	$-17179869184, %r15     # imm = 0xFFFFFFFC00000000
	addq	%rax, %r15
	movl	%ebp, %ecx
	movl	%esi, 12(%rsp)          # 4-byte Spill
	subl	%esi, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movabsq	$-8589934592, %r14      # imm = 0xFFFFFFFE00000000
	addq	%rax, %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_17:                               # =>This Inner Loop Header: Depth=1
	movq	bits(%rip), %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB0_18
# BB#21:                                # %.lr.ph.i
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	bits(%rip), %rdi
	callq	fgetc
	movq	32(%rsp), %rcx          # 8-byte Reload
	movb	%al, (%rcx,%r12)
	movzbl	-2(%rcx,%r12), %edx
	movzbl	-1(%rcx,%r12), %ebx
	movzbl	-3(%rcx,%r12), %ecx
	orb	%dl, %cl
	orb	%bl, %cl
	sete	%sil
	cmpb	$1, %al
	sete	%cl
	andb	%sil, %cl
	jne	.LBB0_25
# BB#22:                                #   in Loop: Header=BB0_17 Depth=1
	cmpb	$1, %al
	sete	%sil
	orb	%dl, %bl
	sete	%al
	andb	%sil, %al
	orb	%al, %cl
	movabsq	$4294967296, %rdx       # imm = 0x100000000
	addq	%rdx, %r15
	addq	%rdx, %r14
	incq	%r12
	testb	$1, %cl
	je	.LBB0_17
# BB#23:                                # %.thread
	movl	%ebp, %r14d
	addq	%r12, %r14
	testb	%al, %al
	je	.LBB0_28
# BB#24:                                # %.thread._crit_edge
	movl	$-3, %r15d
	xorl	%ebp, %ebp
	jmp	.LBB0_29
.LBB0_18:                               # %.preheader
	movq	24(%rsp), %rdx          # 8-byte Reload
	addl	%r12d, %edx
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB0_19:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rax
	sarq	$32, %rax
	decl	%edx
	addq	%rcx, %r14
	cmpb	$0, (%r13,%rax)
	je	.LBB0_19
# BB#20:
	leal	-1(%rbp,%r12), %r15d
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %eax
	subl	%eax, %edx
	movl	%edx, 4(%rbx)
	movq	24(%rbx), %rdi
	movl	12(%rsp), %ecx          # 4-byte Reload
	addl	%eax, %ecx
	movslq	%ecx, %rax
	jmp	.LBB0_32
.LBB0_25:                               # %.preheader121
	movl	%ebp, %eax
	leaq	1(%rax,%r12), %r14
	movl	$-1, %ebp
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB0_26:                               # =>This Inner Loop Header: Depth=1
	movq	%r15, %rax
	sarq	$32, %rax
	incl	%ebp
	addq	%rcx, %r15
	cmpb	$0, (%r13,%rax)
	je	.LBB0_26
# BB#27:
	movl	$-4, %r15d
	jmp	.LBB0_29
.LBB0_28:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
.LBB0_29:
	movq	bits(%rip), %rdi
	movslq	%r15d, %rsi
	movl	$1, %edx
	callq	fseek
	testl	%eax, %eax
	je	.LBB0_31
# BB#30:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	movl	%r15d, %ecx
	callq	snprintf
	movq	%r13, %rdi
	callq	free
	movl	$errortext, %edi
	movl	$600, %esi              # imm = 0x258
	callq	error
.LBB0_31:
	addl	%r14d, %r15d
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %eax
	movl	12(%rsp), %ecx          # 4-byte Reload
	addl	%ecx, %ebp
	movl	%r15d, %edx
	subl	%ebp, %edx
	subl	%eax, %edx
	movl	%edx, 4(%rbx)
	movq	24(%rbx), %rdi
	addl	%ecx, %eax
	cltq
.LBB0_32:
	movq	%r13, %rsi
	addq	%rax, %rsi
	callq	memcpy
	movq	24(%rbx), %rax
	movzbl	(%rax), %ecx
	shrl	$7, %ecx
	movl	%ecx, 20(%rbx)
	movzbl	(%rax), %ecx
	shrl	$5, %ecx
	andl	$3, %ecx
	movl	%ecx, 16(%rbx)
	movzbl	(%rax), %eax
	andl	$31, %eax
	movl	%eax, 12(%rbx)
	movq	%r13, %rdi
	callq	free
	jmp	.LBB0_33
.Lfunc_end0:
	.size	GetAnnexbNALU, .Lfunc_end0-GetAnnexbNALU
	.cfi_endproc

	.globl	OpenBitstreamFile
	.p2align	4, 0x90
	.type	OpenBitstreamFile,@function
OpenBitstreamFile:                      # @OpenBitstreamFile
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$.L.str.6, %esi
	callq	fopen64
	movq	%rax, bits(%rip)
	testq	%rax, %rax
	je	.LBB1_2
# BB#1:
	popq	%rax
	retq
.LBB1_2:
	movq	input(%rip), %rcx
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.7, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	popq	%rax
	jmp	error                   # TAILCALL
.Lfunc_end1:
	.size	OpenBitstreamFile, .Lfunc_end1-OpenBitstreamFile
	.cfi_endproc

	.globl	CloseBitstreamFile
	.p2align	4, 0x90
	.type	CloseBitstreamFile,@function
CloseBitstreamFile:                     # @CloseBitstreamFile
	.cfi_startproc
# BB#0:
	movq	bits(%rip), %rdi
	jmp	fclose                  # TAILCALL
.Lfunc_end2:
	.size	CloseBitstreamFile, .Lfunc_end2-CloseBitstreamFile
	.cfi_endproc

	.globl	CheckZeroByteNonVCL
	.p2align	4, 0x90
	.type	CheckZeroByteNonVCL,@function
CheckZeroByteNonVCL:                    # @CheckZeroByteNonVCL
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %ecx
	leal	-1(%rcx), %eax
	cmpl	$5, %eax
	jb	.LBB3_10
# BB#1:
	leal	-7(%rcx), %eax
	leal	-6(%rcx), %edx
	cmpl	$4, %edx
	jae	.LBB3_2
# BB#5:
	cmpl	$0, LastAccessUnitExists(%rip)
	jne	.LBB3_6
	jmp	.LBB3_7
.LBB3_2:
	cmpl	$13, %ecx
	jl	.LBB3_7
# BB#3:
	cmpl	$18, %ecx
	jg	.LBB3_7
# BB#4:
	movl	LastAccessUnitExists(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB3_7
.LBB3_6:
	movl	$0, LastAccessUnitExists(%rip)
	movl	$0, NALUCount(%rip)
.LBB3_7:
	movl	NALUCount(%rip), %ecx
	leal	1(%rcx), %edx
	cmpl	$2, %eax
	movl	%edx, NALUCount(%rip)
	jb	.LBB3_9
# BB#8:
	testl	%ecx, %ecx
	jne	.LBB3_10
.LBB3_9:
	cmpl	$3, (%rdi)
	jne	.LBB3_10
# BB#11:
	movl	$.Lstr.6, %edi
	jmp	puts                    # TAILCALL
.LBB3_10:
	retq
.Lfunc_end3:
	.size	CheckZeroByteNonVCL, .Lfunc_end3-CheckZeroByteNonVCL
	.cfi_endproc

	.globl	CheckZeroByteVCL
	.p2align	4, 0x90
	.type	CheckZeroByteVCL,@function
CheckZeroByteVCL:                       # @CheckZeroByteVCL
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %eax
	decl	%eax
	cmpl	$4, %eax
	ja	.LBB4_5
# BB#1:
	cmpl	$0, LastAccessUnitExists(%rip)
	je	.LBB4_3
# BB#2:                                 # %.thread
	movl	$1, NALUCount(%rip)
	movl	$1, LastAccessUnitExists(%rip)
	cmpl	$3, (%rdi)
	je	.LBB4_6
	jmp	.LBB4_5
.LBB4_3:
	movl	NALUCount(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, NALUCount(%rip)
	movl	$1, LastAccessUnitExists(%rip)
	testl	%eax, %eax
	jne	.LBB4_5
# BB#4:
	cmpl	$3, (%rdi)
	jne	.LBB4_5
.LBB4_6:
	movl	$.Lstr.6, %edi
	jmp	puts                    # TAILCALL
.LBB4_5:
	retq
.Lfunc_end4:
	.size	CheckZeroByteVCL, .Lfunc_end4-CheckZeroByteVCL
	.cfi_endproc

	.type	bits,@object            # @bits
	.bss
	.globl	bits
	.p2align	3
bits:
	.quad	0
	.size	bits, 8

	.type	IsFirstByteStreamNALU,@object # @IsFirstByteStreamNALU
	.data
	.globl	IsFirstByteStreamNALU
	.p2align	2
IsFirstByteStreamNALU:
	.long	1                       # 0x1
	.size	IsFirstByteStreamNALU, 4

	.type	LastAccessUnitExists,@object # @LastAccessUnitExists
	.bss
	.globl	LastAccessUnitExists
	.p2align	2
LastAccessUnitExists:
	.long	0                       # 0x0
	.size	LastAccessUnitExists, 4

	.type	NALUCount,@object       # @NALUCount
	.globl	NALUCount
	.p2align	2
NALUCount:
	.long	0                       # 0x0
	.size	NALUCount, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"GetAnnexbNALU: Buf"
	.size	.L.str, 19

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"GetAnnexbNALU: Cannot fseek %d in the bit stream file"
	.size	.L.str.5, 54

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"rb"
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Cannot open Annex B ByteStream file '%s'"
	.size	.L.str.7, 41

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	" Panic: Error in next start code search "
	.size	.Lstr, 41

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"GetAnnexbNALU: The leading_zero_8bits syntax can only be present in the first byte stream NAL unit, return -1"
	.size	.Lstr.1, 110

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"GetAnnexbNALU: no Start Code at the begin of the NALU, return -1"
	.size	.Lstr.3, 65

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"GetAnnexbNALU can't read start code"
	.size	.Lstr.4, 36

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"warning: zero_byte shall exist"
	.size	.Lstr.6, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
