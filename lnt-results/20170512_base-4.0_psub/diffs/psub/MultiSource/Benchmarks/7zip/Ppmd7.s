	.text
	.file	"Ppmd7.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16,6
.LCPI0_1:
	.zero	16,8
	.text
	.globl	Ppmd7_Construct
	.p2align	4, 0x90
	.type	Ppmd7_Construct,@function
Ppmd7_Construct:                        # @Ppmd7_Construct
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
.Lcfi2:
	.cfi_offset %rbx, -24
.Lcfi3:
	.cfi_offset %r14, -16
	movq	$0, 64(%rdi)
	xorl	%r14d, %r14d
	movl	$4, %r8d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_14 Depth 2
                                        #     Child Loop BB0_17 Depth 2
	movl	%r14d, %r10d
	shrl	$2, %r10d
	incl	%r10d
	cmpq	$11, %r14
	cmoval	%r8d, %r10d
	cmpl	$32, %r10d
	movl	%r11d, %edx
	movl	%r10d, %ecx
	jb	.LBB0_12
# BB#2:                                 # %min.iters.checked
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	%r10d, %r9d
	andl	$2147483616, %r9d       # imm = 0x7FFFFFE0
	movl	%r11d, %edx
	movl	%r10d, %ecx
	je	.LBB0_12
# BB#3:                                 # %vector.scevcheck
                                        #   in Loop: Header=BB0_1 Depth=1
	leal	-1(%r10), %ecx
	addl	%r11d, %ecx
	movl	%r11d, %edx
	movl	%r10d, %ecx
	jb	.LBB0_12
# BB#4:                                 # %vector.ph
                                        #   in Loop: Header=BB0_1 Depth=1
	movzbl	%r14b, %ecx
	movd	%ecx, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leal	-32(%r9), %edx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andl	$3, %esi
	je	.LBB0_5
# BB#6:                                 # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	negl	%esi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_7:                                # %vector.body.prol
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r11,%rcx), %ebx
	movdqu	%xmm0, 146(%rdi,%rbx)
	movdqu	%xmm0, 162(%rdi,%rbx)
	addl	$32, %ecx
	incl	%esi
	jne	.LBB0_7
	jmp	.LBB0_8
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%ecx, %ecx
.LBB0_8:                                # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$96, %edx
	jb	.LBB0_10
	.p2align	4, 0x90
.LBB0_9:                                # %vector.body
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r11,%rcx), %edx
	movdqu	%xmm0, 146(%rdi,%rdx)
	movdqu	%xmm0, 162(%rdi,%rdx)
	leal	32(%r11,%rcx), %edx
	movdqu	%xmm0, 146(%rdi,%rdx)
	movdqu	%xmm0, 162(%rdi,%rdx)
	leal	64(%r11,%rcx), %edx
	movdqu	%xmm0, 146(%rdi,%rdx)
	movdqu	%xmm0, 162(%rdi,%rdx)
	leal	96(%r11,%rcx), %edx
	movdqu	%xmm0, 146(%rdi,%rdx)
	movdqu	%xmm0, 162(%rdi,%rdx)
	subl	$-128, %ecx
	cmpl	%ecx, %r9d
	jne	.LBB0_9
.LBB0_10:                               # %middle.block
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	%r9d, %r10d
	je	.LBB0_18
# BB#11:                                #   in Loop: Header=BB0_1 Depth=1
	leal	(%r11,%r9), %edx
	movl	%r10d, %ecx
	subl	%r9d, %ecx
	.p2align	4, 0x90
.LBB0_12:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	leal	-1(%rcx), %r9d
	movl	%ecx, %esi
	andl	$7, %esi
	je	.LBB0_15
# BB#13:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	negl	%esi
	.p2align	4, 0x90
.LBB0_14:                               # %scalar.ph.prol
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %ebx
	incl	%edx
	movb	%r14b, 146(%rdi,%rbx)
	decl	%ecx
	incl	%esi
	jne	.LBB0_14
.LBB0_15:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$7, %r9d
	jb	.LBB0_18
# BB#16:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_1 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_17:                               # %scalar.ph
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rdx,%rsi), %ebx
	leal	1(%rdx,%rsi), %eax
	movb	%r14b, 146(%rdi,%rbx)
	leal	2(%rdx,%rsi), %ebx
	movb	%r14b, 146(%rdi,%rax)
	leal	3(%rdx,%rsi), %eax
	movb	%r14b, 146(%rdi,%rbx)
	leal	4(%rdx,%rsi), %ebx
	movb	%r14b, 146(%rdi,%rax)
	leal	5(%rdx,%rsi), %eax
	movb	%r14b, 146(%rdi,%rbx)
	leal	6(%rdx,%rsi), %ebx
	movb	%r14b, 146(%rdi,%rax)
	leal	7(%rdx,%rsi), %eax
	movb	%r14b, 146(%rdi,%rbx)
	movb	%r14b, 146(%rdi,%rax)
	addl	$8, %esi
	cmpl	%esi, %ecx
	jne	.LBB0_17
.LBB0_18:                               # %.loopexit
                                        #   in Loop: Header=BB0_1 Depth=1
	addl	%r11d, %r10d
	movb	%r10b, 108(%rdi,%r14)
	incq	%r14
	cmpq	$38, %r14
	movl	%r10d, %r11d
	jne	.LBB0_1
# BB#19:                                # %.lr.ph.preheader
	movw	$512, 684(%rdi)         # imm = 0x200
	movabsq	$289360691352306692, %rax # imm = 0x404040404040404
	movq	%rax, 686(%rdi)
	movb	$4, 694(%rdi)
	movabsq	$434041037028460038, %rax # imm = 0x606060606060606
	movq	%rax, 932(%rdi)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6]
	movups	%xmm0, 919(%rdi)
	movups	%xmm0, 903(%rdi)
	movups	%xmm0, 887(%rdi)
	movups	%xmm0, 871(%rdi)
	movups	%xmm0, 855(%rdi)
	movups	%xmm0, 839(%rdi)
	movups	%xmm0, 823(%rdi)
	movups	%xmm0, 807(%rdi)
	movups	%xmm0, 791(%rdi)
	movups	%xmm0, 775(%rdi)
	movups	%xmm0, 759(%rdi)
	movups	%xmm0, 743(%rdi)
	movups	%xmm0, 727(%rdi)
	movups	%xmm0, 711(%rdi)
	movups	%xmm0, 695(%rdi)
	movw	$256, 428(%rdi)         # imm = 0x100
	movb	$2, 430(%rdi)
	movl	$1, %ecx
	movl	$3, %edx
	xorl	%r8d, %r8d
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph.1
                                        #   in Loop: Header=BB0_20 Depth=1
	movb	%al, %sil
	addl	%edx, %esi
	decl	%edx
	testl	%ecx, %ecx
	cmovnel	%ecx, %edx
	movb	%sil, 432(%rdi,%r8)
	decl	%edx
	leal	-1(%rsi), %ecx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	cmovnel	%edx, %ecx
	addl	%esi, %eax
	addq	$2, %r8
	movl	%eax, %edx
.LBB0_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movb	%dl, 431(%rdi,%r8)
	xorl	%esi, %esi
	decl	%ecx
	sete	%al
	cmpq	$252, %r8
	jne	.LBB0_22
# BB#21:                                # %._crit_edge
	xorps	%xmm0, %xmm0
	movups	%xmm0, 988(%rdi)
	movups	%xmm0, 972(%rdi)
	movups	%xmm0, 956(%rdi)
	movups	%xmm0, 940(%rdi)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]
	movups	%xmm0, 1180(%rdi)
	movups	%xmm0, 1164(%rdi)
	movups	%xmm0, 1148(%rdi)
	movups	%xmm0, 1132(%rdi)
	movups	%xmm0, 1116(%rdi)
	movups	%xmm0, 1100(%rdi)
	movups	%xmm0, 1084(%rdi)
	movups	%xmm0, 1068(%rdi)
	movups	%xmm0, 1052(%rdi)
	movups	%xmm0, 1036(%rdi)
	movups	%xmm0, 1020(%rdi)
	movups	%xmm0, 1004(%rdi)
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	Ppmd7_Construct, .Lfunc_end0-Ppmd7_Construct
	.cfi_endproc

	.globl	Ppmd7_Free
	.p2align	4, 0x90
	.type	Ppmd7_Free,@function
Ppmd7_Free:                             # @Ppmd7_Free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	64(%rbx), %rsi
	movq	%rax, %rdi
	callq	*8(%rax)
	movl	$0, 52(%rbx)
	movq	$0, 64(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	Ppmd7_Free, .Lfunc_end1-Ppmd7_Free
	.cfi_endproc

	.globl	Ppmd7_Alloc
	.p2align	4, 0x90
	.type	Ppmd7_Alloc,@function
Ppmd7_Alloc:                            # @Ppmd7_Alloc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	64(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_2
# BB#1:
	movl	$1, %eax
	cmpl	%r14d, 52(%rbx)
	je	.LBB2_5
.LBB2_2:                                # %._crit_edge
	movq	%r15, %rdi
	callq	*8(%r15)
	movl	$0, 52(%rbx)
	movq	$0, 64(%rbx)
	movl	%r14d, %eax
	andl	$3, %eax
	movl	$4, %ecx
	subl	%eax, %ecx
	negl	%eax
	movl	%ecx, 104(%rbx)
	leal	16(%r14,%rax), %esi
	movq	%r15, %rdi
	callq	*(%r15)
	movq	%rax, 64(%rbx)
	testq	%rax, %rax
	je	.LBB2_3
# BB#4:
	movl	%r14d, 52(%rbx)
	movl	$1, %eax
	jmp	.LBB2_5
.LBB2_3:
	xorl	%eax, %eax
.LBB2_5:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	Ppmd7_Alloc, .Lfunc_end2-Ppmd7_Alloc
	.cfi_endproc

	.globl	Ppmd7_Init
	.p2align	4, 0x90
	.type	Ppmd7_Init,@function
Ppmd7_Init:                             # @Ppmd7_Init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	%esi, 36(%rbx)
	callq	RestartModel
	movb	$7, 1198(%rbx)
	movw	$0, 1196(%rbx)
	movb	$64, 1199(%rbx)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	Ppmd7_Init, .Lfunc_end3-Ppmd7_Init
	.cfi_endproc

	.p2align	4, 0x90
	.type	RestartModel,@function
RestartModel:                           # @RestartModel
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 404(%rdi)
	movups	%xmm0, 388(%rdi)
	movups	%xmm0, 372(%rdi)
	movups	%xmm0, 356(%rdi)
	movups	%xmm0, 340(%rdi)
	movups	%xmm0, 324(%rdi)
	movups	%xmm0, 308(%rdi)
	movups	%xmm0, 292(%rdi)
	movups	%xmm0, 276(%rdi)
	movq	$0, 420(%rdi)
	movq	64(%rdi), %r10
	movl	104(%rdi), %r8d
	leaq	(%r10,%r8), %r11
	movq	%r11, 88(%rdi)
	movl	52(%rdi), %esi
	leaq	(%r11,%rsi), %rcx
	movl	$2863311531, %eax       # imm = 0xAAAAAAAB
	imulq	%rsi, %rax
	shrq	$38, %rax
	imull	$84, %eax, %r9d
	subq	%r9, %rcx
	movq	%rcx, 96(%rdi)
	movl	$0, 56(%rdi)
	movl	36(%rdi), %eax
	movl	%eax, 24(%rdi)
	cmpl	$12, %eax
	notl	%eax
	movl	$-13, %edx
	cmovbl	%eax, %edx
	movl	%edx, 48(%rdi)
	movl	%edx, 44(%rdi)
	movl	$0, 32(%rdi)
	leaq	-12(%rsi,%r11), %rax
	movq	%rax, 80(%rdi)
	movq	%rax, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$0, -4(%rsi,%r11)
	movw	$256, -12(%rsi,%r11)    # imm = 0x100
	movw	$257, -10(%rsi,%r11)    # imm = 0x101
	movq	%rcx, 16(%rdi)
	leaq	1536(%rcx), %rax
	movq	%rax, 72(%rdi)
	subl	%r10d, %ecx
	movl	%ecx, -8(%rsi,%r11)
	leaq	10(%rsi,%r8), %rcx
	subq	%r9, %rcx
	addq	%r10, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movb	%al, -10(%rcx)
	movb	$1, -9(%rcx)
	movw	$0, -8(%rcx)
	movw	$0, -6(%rcx)
	leal	1(%rax), %edx
	movb	%dl, -4(%rcx)
	movb	$1, -3(%rcx)
	movw	$0, -2(%rcx)
	movw	$0, (%rcx)
	addq	$2, %rax
	addq	$12, %rcx
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB4_1
# BB#2:                                 # %.preheader81.preheader
	leaq	2926(%rdi), %rcx
	movq	$-128, %r8
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader81
                                        # =>This Inner Loop Header: Depth=1
	leal	130(%r8), %esi
	movl	$15581, %eax            # imm = 0x3CDD
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, -126(%rcx)
	movw	%dx, -110(%rcx)
	movw	%dx, -94(%rcx)
	movw	%dx, -78(%rcx)
	movw	%dx, -62(%rcx)
	movw	%dx, -46(%rcx)
	movw	%dx, -30(%rcx)
	movw	%dx, -14(%rcx)
	movl	$7999, %eax             # imm = 0x1F3F
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, -124(%rcx)
	movw	%dx, -108(%rcx)
	movw	%dx, -92(%rcx)
	movw	%dx, -76(%rcx)
	movw	%dx, -60(%rcx)
	movw	%dx, -44(%rcx)
	movw	%dx, -28(%rcx)
	movw	%dx, -12(%rcx)
	movl	$22975, %eax            # imm = 0x59BF
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, -122(%rcx)
	movw	%dx, -106(%rcx)
	movw	%dx, -90(%rcx)
	movw	%dx, -74(%rcx)
	movw	%dx, -58(%rcx)
	movw	%dx, -42(%rcx)
	movw	%dx, -26(%rcx)
	movw	%dx, -10(%rcx)
	movl	$18675, %eax            # imm = 0x48F3
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, -120(%rcx)
	movw	%dx, -104(%rcx)
	movw	%dx, -88(%rcx)
	movw	%dx, -72(%rcx)
	movw	%dx, -56(%rcx)
	movw	%dx, -40(%rcx)
	movw	%dx, -24(%rcx)
	movw	%dx, -8(%rcx)
	movl	$25761, %eax            # imm = 0x64A1
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, -118(%rcx)
	movw	%dx, -102(%rcx)
	movw	%dx, -86(%rcx)
	movw	%dx, -70(%rcx)
	movw	%dx, -54(%rcx)
	movw	%dx, -38(%rcx)
	movw	%dx, -22(%rcx)
	movw	%dx, -6(%rcx)
	movl	$23228, %eax            # imm = 0x5ABC
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, -116(%rcx)
	movw	%dx, -100(%rcx)
	movw	%dx, -84(%rcx)
	movw	%dx, -68(%rcx)
	movw	%dx, -52(%rcx)
	movw	%dx, -36(%rcx)
	movw	%dx, -20(%rcx)
	movw	%dx, -4(%rcx)
	movl	$26162, %eax            # imm = 0x6632
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, -114(%rcx)
	movw	%dx, -98(%rcx)
	movw	%dx, -82(%rcx)
	movw	%dx, -66(%rcx)
	movw	%dx, -50(%rcx)
	movw	%dx, -34(%rcx)
	movw	%dx, -18(%rcx)
	movw	%dx, -2(%rcx)
	movl	$24657, %eax            # imm = 0x6051
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, -112(%rcx)
	movw	%dx, -96(%rcx)
	movw	%dx, -80(%rcx)
	movw	%dx, -64(%rcx)
	movw	%dx, -48(%rcx)
	movw	%dx, -32(%rcx)
	movw	%dx, -16(%rcx)
	movw	%dx, (%rcx)
	subq	$-128, %rcx
	incq	%r8
	jne	.LBB4_3
# BB#4:                                 # %.preheader.preheader
	addq	$1263, %rdi             # imm = 0x4EF
	movq	$-1000, %rax            # imm = 0xFC18
	.p2align	4, 0x90
.LBB4_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leal	1080(%rax), %ecx
	movb	$3, -61(%rdi)
	movw	%cx, -63(%rdi)
	movb	$4, -60(%rdi)
	movb	$3, -57(%rdi)
	movw	%cx, -59(%rdi)
	movb	$4, -56(%rdi)
	movb	$3, -53(%rdi)
	movw	%cx, -55(%rdi)
	movb	$4, -52(%rdi)
	movb	$3, -49(%rdi)
	movw	%cx, -51(%rdi)
	movb	$4, -48(%rdi)
	movb	$3, -45(%rdi)
	movw	%cx, -47(%rdi)
	movb	$4, -44(%rdi)
	movb	$3, -41(%rdi)
	movw	%cx, -43(%rdi)
	movb	$4, -40(%rdi)
	movb	$3, -37(%rdi)
	movw	%cx, -39(%rdi)
	movb	$4, -36(%rdi)
	movb	$3, -33(%rdi)
	movw	%cx, -35(%rdi)
	movb	$4, -32(%rdi)
	movb	$3, -29(%rdi)
	movw	%cx, -31(%rdi)
	movb	$4, -28(%rdi)
	movb	$3, -25(%rdi)
	movw	%cx, -27(%rdi)
	movb	$4, -24(%rdi)
	movb	$3, -21(%rdi)
	movw	%cx, -23(%rdi)
	movb	$4, -20(%rdi)
	movb	$3, -17(%rdi)
	movw	%cx, -19(%rdi)
	movb	$4, -16(%rdi)
	movb	$3, -13(%rdi)
	movw	%cx, -15(%rdi)
	movb	$4, -12(%rdi)
	movb	$3, -9(%rdi)
	movw	%cx, -11(%rdi)
	movb	$4, -8(%rdi)
	movb	$3, -5(%rdi)
	movw	%cx, -7(%rdi)
	movb	$4, -4(%rdi)
	movb	$3, -1(%rdi)
	movw	%cx, -3(%rdi)
	movb	$4, (%rdi)
	addq	$64, %rdi
	addq	$40, %rax
	jne	.LBB4_5
# BB#6:
	retq
.Lfunc_end4:
	.size	RestartModel, .Lfunc_end4-RestartModel
	.cfi_endproc

	.globl	Ppmd7_MakeEscFreq
	.p2align	4, 0x90
	.type	Ppmd7_MakeEscFreq,@function
Ppmd7_MakeEscFreq:                      # @Ppmd7_MakeEscFreq
	.cfi_startproc
# BB#0:
	movq	(%rdi), %r9
	movzwl	(%r9), %r11d
	cmpl	$256, %r11d             # imm = 0x100
	jne	.LBB5_1
# BB#2:
	addq	$1196, %rdi             # imm = 0x4AC
	movl	$1, %eax
	jmp	.LBB5_3
.LBB5_1:
	movl	%r11d, %r8d
	subl	%esi, %r8d
	leal	-1(%r8), %ecx
	movzbl	428(%rdi,%rcx), %ecx
	movq	64(%rdi), %r10
	movl	8(%r9), %eax
	movzwl	(%r10,%rax), %eax
	subl	%r11d, %eax
	cmpl	%eax, %r8d
	sbbq	%rax, %rax
	shlq	$6, %rcx
	addq	%rdi, %rcx
	andl	$4, %eax
	addq	%rcx, %rax
	movzwl	2(%r9), %r9d
	imull	$11, %r11d, %ecx
	cmpl	%ecx, %r9d
	sbbq	%rcx, %rcx
	andl	$8, %ecx
	addq	%rax, %rcx
	cmpl	%esi, %r8d
	sbbq	%rsi, %rsi
	andl	$16, %esi
	addq	%rcx, %rsi
	movl	40(%rdi), %r8d
	leaq	1200(%rsi,%r8,4), %rdi
	movzwl	1200(%rsi,%r8,4), %r9d
	movb	1202(%rsi,%r8,4), %cl
	movl	%r9d, %eax
	shrl	%cl, %eax
	subl	%eax, %r9d
	movw	%r9w, 1200(%rsi,%r8,4)
	cmpl	$1, %eax
	adcl	$0, %eax
.LBB5_3:
	movl	%eax, (%rdx)
	movq	%rdi, %rax
	retq
.Lfunc_end5:
	.size	Ppmd7_MakeEscFreq, .Lfunc_end5-Ppmd7_MakeEscFreq
	.cfi_endproc

	.globl	Ppmd7_Update1
	.p2align	4, 0x90
	.type	Ppmd7_Update1,@function
Ppmd7_Update1:                          # @Ppmd7_Update1
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rax
	movb	1(%rax), %cl
	addb	$4, %cl
	movb	%cl, 1(%rax)
	movq	(%rbx), %rdx
	addw	$4, 2(%rdx)
	cmpb	-5(%rax), %cl
	jbe	.LBB6_3
# BB#1:
	leaq	-6(%rax), %rcx
	movzwl	4(%rax), %edx
	movw	%dx, 12(%rsp)
	movl	(%rax), %edx
	movl	%edx, 8(%rsp)
	movzwl	-2(%rax), %edx
	movw	%dx, 4(%rax)
	movl	-6(%rax), %edx
	movl	%edx, (%rax)
	movzwl	12(%rsp), %edx
	movw	%dx, -2(%rax)
	movl	8(%rsp), %edx
	movl	%edx, -6(%rax)
	movq	%rcx, 16(%rbx)
	cmpb	$125, -5(%rax)
	movq	%rcx, %rax
	jb	.LBB6_3
# BB#2:
	movq	%rbx, %rdi
	callq	Rescale
	movq	16(%rbx), %rax
.LBB6_3:
	cmpl	$0, 24(%rbx)
	jne	.LBB6_6
# BB#4:
	movzwl	2(%rax), %ecx
	movzwl	4(%rax), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	addq	64(%rbx), %rax
	cmpq	88(%rbx), %rax
	jbe	.LBB6_6
# BB#5:                                 # %NextContext.exit
	movq	%rax, 8(%rbx)
	movq	%rax, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB6_6:
	movq	%rbx, %rdi
	addq	$16, %rsp
	popq	%rbx
	jmp	UpdateModel             # TAILCALL
.Lfunc_end6:
	.size	Ppmd7_Update1, .Lfunc_end6-Ppmd7_Update1
	.cfi_endproc

	.p2align	4, 0x90
	.type	Rescale,@function
Rescale:                                # @Rescale
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 56
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	64(%rdi), %rsi
	movq	(%rdi), %rax
	movq	16(%rdi), %rcx
	movl	4(%rax), %ebp
	leaq	(%rsi,%rbp), %rax
	movzwl	4(%rcx), %edx
	movw	%dx, -36(%rsp)
	movl	(%rcx), %edx
	movl	%edx, -40(%rsp)
	cmpq	%rax, %rcx
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	movq	%rbp, -32(%rsp)         # 8-byte Spill
	je	.LBB7_2
	.p2align	4, 0x90
.LBB7_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%rcx), %edx
	movw	%dx, 4(%rcx)
	movl	-6(%rcx), %edx
	movl	%edx, (%rcx)
	leaq	-6(%rcx), %rcx
	cmpq	%rcx, %rax
	jne	.LBB7_1
.LBB7_2:                                # %._crit_edge
	movzwl	-36(%rsp), %edx
	movw	%dx, 4(%rcx)
	movl	-40(%rsp), %edx
	movl	%edx, (%rcx)
	movq	(%rdi), %rcx
	movzwl	2(%rcx), %r15d
	movzbl	1(%rax), %edx
	subl	%edx, %r15d
	addb	$4, %dl
	xorl	%r10d, %r10d
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	cmpl	$0, 24(%rdi)
	setne	%r10b
	movzbl	%dl, %edx
	addl	%r10d, %edx
	shrl	%edx
	movb	%dl, 1(%rax)
	movzwl	(%rcx), %edi
	leal	-1(%rdi), %ebx
	addl	$-2, %edi
	leaq	1(%rdi), %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	movq	%rax, %rbp
	movl	%edx, %r12d
	jmp	.LBB7_3
	.p2align	4, 0x90
.LBB7_8:                                # %..critedge_crit_edge
                                        #   in Loop: Header=BB7_3 Depth=1
	leaq	6(%rax,%r14), %rsi
	jmp	.LBB7_9
	.p2align	4, 0x90
.LBB7_6:                                #   in Loop: Header=BB7_3 Depth=1
	movq	%rax, %rsi
.LBB7_9:                                # %.critedge
                                        #   in Loop: Header=BB7_3 Depth=1
	movb	%dl, (%rsi)
	movb	%r8b, 1(%rax,%r9)
	movl	%r11d, 2(%rax,%r9)
.LBB7_10:                               #   in Loop: Header=BB7_3 Depth=1
	movl	%r15d, %esi
	subl	%ecx, %esi
	addl	%r8d, %r12d
	decl	%ebx
	je	.LBB7_12
# BB#11:                                # %._crit_edge153
                                        #   in Loop: Header=BB7_3 Depth=1
	movb	7(%rbp), %dl
	addq	$6, %rbp
	addq	$6, %r13
	movl	%esi, %r15d
.LBB7_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
	movzbl	7(%rbp), %ecx
	leal	(%rcx,%r10), %r8d
	shrl	%r8d
	movb	%r8b, 7(%rbp)
	movzbl	%dl, %edx
	cmpl	%edx, %r8d
	jbe	.LBB7_10
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movb	6(%rbp), %dl
	movl	8(%rbp), %r11d
	movq	%r13, %r14
	.p2align	4, 0x90
.LBB7_5:                                #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %r9
	movzwl	4(%rax,%r9), %esi
	movw	%si, 10(%rax,%r9)
	movl	(%rax,%r9), %esi
	movl	%esi, 6(%rax,%r9)
	testq	%r9, %r9
	je	.LBB7_6
# BB#7:                                 #   in Loop: Header=BB7_5 Depth=2
	leaq	-6(%r9), %r14
	cmpb	-5(%rax,%r9), %r8b
	ja	.LBB7_5
	jmp	.LBB7_8
.LBB7_12:
	movq	-16(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdx,2), %rdx
	movq	-8(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %r10
	cmpb	$0, 1(%rax,%rdx,2)
	je	.LBB7_13
.LBB7_33:                               # %.thread
	addl	%esi, %r12d
	shrl	%esi
	subl	%esi, %r12d
	movw	%r12w, 2(%r10)
	movl	4(%r10), %eax
	addq	64(%r14), %rax
	movq	%rax, 16(%r14)
.LBB7_34:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_13:
	leaq	(%rdi,%rdi,2), %rdx
	movq	-32(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,2), %rdx
	movq	-24(%rsp), %rsi         # 8-byte Reload
	leaq	1(%rsi,%rdx), %rbp
	subl	%ecx, %r15d
	movzwl	(%r10), %edx
	movl	%edx, %ecx
	movl	%r15d, %esi
	.p2align	4, 0x90
.LBB7_14:                               # =>This Inner Loop Header: Depth=1
	incl	%esi
	decl	%ecx
	cmpb	$0, (%rbp)
	leaq	-6(%rbp), %rbp
	je	.LBB7_14
# BB#15:
	movw	%cx, (%r10)
	movzwl	%cx, %edi
	cmpl	$1, %edi
	jne	.LBB7_19
# BB#16:
	movb	(%rax), %dil
	movb	1(%rax), %cl
	movl	2(%rax), %ebp
	.p2align	4, 0x90
.LBB7_17:                               # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebx
	shrb	%bl
	subb	%bl, %cl
	movl	%esi, %ebx
	shrl	%ebx
	cmpl	$3, %esi
	movl	%ebx, %esi
	ja	.LBB7_17
# BB#18:
	incl	%edx
	shrl	%edx
	decl	%edx
	movzbl	146(%r14,%rdx), %edx
	movl	276(%r14,%rdx,4), %esi
	movl	%esi, (%rax)
	subl	64(%r14), %eax
	movl	%eax, 276(%r14,%rdx,4)
	leaq	2(%r10), %rax
	movq	%rax, 16(%r14)
	movb	%dil, 2(%r10)
	movb	%cl, 3(%r10)
	movl	%ebp, 4(%r10)
	jmp	.LBB7_34
.LBB7_19:
	movzwl	%cx, %ebx
	incl	%edx
	shrl	%edx
	incl	%ebx
	shrl	%ebx
	cmpl	%ebx, %edx
	je	.LBB7_33
# BB#20:
	decl	%edx
	movzbl	146(%r14,%rdx), %r11d
	leal	-1(%rbx), %ebp
	movzbl	146(%r14,%rbp), %ecx
	cmpb	%cl, %r11b
	je	.LBB7_32
# BB#21:
	movl	276(%r14,%rcx,4), %edx
	testq	%rdx, %rdx
	je	.LBB7_28
# BB#22:
	movq	64(%r14), %r8
	leaq	(%r8,%rdx), %r9
	movl	(%r8,%rdx), %edx
	movl	%edx, 276(%r14,%rcx,4)
	testb	$1, %bl
	jne	.LBB7_24
# BB#23:
	movq	%r9, %rcx
	movq	%rax, %rdx
	movl	%ebx, %ebp
	cmpl	$1, %ebx
	jne	.LBB7_26
	jmp	.LBB7_27
.LBB7_28:
	movzbl	108(%r14,%r11), %edx
	movzbl	108(%r14,%rcx), %ecx
	subl	%ecx, %edx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,4), %rcx
	leal	-1(%rdx), %ebx
	movzbl	146(%r14,%rbx), %ebp
	movzbl	108(%r14,%rbp), %edi
	cmpl	%edx, %edi
	movq	64(%r14), %r8
	je	.LBB7_30
# BB#29:
	leal	-1(%rbp), %ebp
	movzbl	108(%r14,%rbp), %edi
	leaq	(%rdi,%rdi,2), %r9
	leaq	(%rcx,%r9,4), %rdx
	subl	%edi, %ebx
	movl	276(%r14,%rbx,4), %edi
	movl	%edi, (%rcx,%r9,4)
	subl	%r8d, %edx
	movl	%edx, 276(%r14,%rbx,4)
.LBB7_30:                               # %SplitBlock.exit.i
	movl	%ebp, %edx
	leaq	276(%r14,%rdx,4), %rbx
	movl	276(%r14,%rdx,4), %edx
	movl	%edx, (%rcx)
	subq	%r8, %rcx
	movq	%rax, %r9
	jmp	.LBB7_31
.LBB7_24:
	movl	(%rax), %ecx
	movl	%ecx, (%r9)
	movl	4(%rax), %ecx
	movl	%ecx, 4(%r9)
	movl	8(%rax), %ecx
	movl	%ecx, 8(%r9)
	leaq	12(%rax), %rdx
	movq	%r9, %rcx
	addq	$12, %rcx
	cmpl	$1, %ebx
	je	.LBB7_27
	.p2align	4, 0x90
.LBB7_26:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %edi
	movl	%edi, (%rcx)
	movl	4(%rdx), %edi
	movl	%edi, 4(%rcx)
	movl	8(%rdx), %edi
	movl	%edi, 8(%rcx)
	movl	12(%rdx), %edi
	movl	%edi, 12(%rcx)
	movl	16(%rdx), %edi
	movl	%edi, 16(%rcx)
	movl	20(%rdx), %edi
	movl	%edi, 20(%rcx)
	addq	$24, %rdx
	addq	$24, %rcx
	addl	$-2, %ebp
	jne	.LBB7_26
.LBB7_27:
	leaq	276(%r14,%r11,4), %rbx
	movl	276(%r14,%r11,4), %ecx
	movl	%ecx, (%rax)
	subq	%r8, %rax
	movq	%rax, %rcx
.LBB7_31:                               # %.sink.split.i
	movl	%ecx, (%rbx)
	movq	%r9, %rax
.LBB7_32:                               # %ShrinkUnits.exit
	subl	64(%r14), %eax
	movl	%eax, 4(%r10)
	jmp	.LBB7_33
.Lfunc_end7:
	.size	Rescale, .Lfunc_end7-Rescale
	.cfi_endproc

	.globl	Ppmd7_Update1_0
	.p2align	4, 0x90
	.type	Ppmd7_Update1_0,@function
Ppmd7_Update1_0:                        # @Ppmd7_Update1_0
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
.Lcfi30:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rcx
	movq	16(%rbx), %rax
	movzbl	1(%rax), %edx
	movl	%edx, %esi
	addl	%esi, %esi
	movzwl	2(%rcx), %edi
	cmpl	%esi, %edi
	sbbl	%esi, %esi
	andl	$1, %esi
	movl	%esi, 32(%rbx)
	addl	%esi, 44(%rbx)
	addl	$4, %edi
	movw	%di, 2(%rcx)
	addb	$4, %dl
	movb	%dl, 1(%rax)
	cmpb	$125, %dl
	jb	.LBB8_2
# BB#1:
	movq	%rbx, %rdi
	callq	Rescale
	movq	16(%rbx), %rax
.LBB8_2:
	cmpl	$0, 24(%rbx)
	jne	.LBB8_5
# BB#3:
	movzwl	2(%rax), %ecx
	movzwl	4(%rax), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	addq	64(%rbx), %rax
	cmpq	88(%rbx), %rax
	jbe	.LBB8_5
# BB#4:                                 # %NextContext.exit
	movq	%rax, 8(%rbx)
	movq	%rax, (%rbx)
	popq	%rbx
	retq
.LBB8_5:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	UpdateModel             # TAILCALL
.Lfunc_end8:
	.size	Ppmd7_Update1_0, .Lfunc_end8-Ppmd7_Update1_0
	.cfi_endproc

	.globl	Ppmd7_UpdateBin
	.p2align	4, 0x90
	.type	Ppmd7_UpdateBin,@function
Ppmd7_UpdateBin:                        # @Ppmd7_UpdateBin
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movb	1(%rax), %cl
	movl	%ecx, %edx
	shrb	$7, %dl
	xorb	$1, %dl
	addb	%cl, %dl
	movb	%dl, 1(%rax)
	movl	$1, 32(%rdi)
	incl	44(%rdi)
	cmpl	$0, 24(%rdi)
	jne	.LBB9_3
# BB#1:
	movzwl	2(%rax), %ecx
	movzwl	4(%rax), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	addq	64(%rdi), %rax
	cmpq	88(%rdi), %rax
	jbe	.LBB9_3
# BB#2:                                 # %NextContext.exit
	movq	%rax, 8(%rdi)
	movq	%rax, (%rdi)
	retq
.LBB9_3:
	jmp	UpdateModel             # TAILCALL
.Lfunc_end9:
	.size	Ppmd7_UpdateBin, .Lfunc_end9-Ppmd7_UpdateBin
	.cfi_endproc

	.globl	Ppmd7_Update2
	.p2align	4, 0x90
	.type	Ppmd7_Update2,@function
Ppmd7_Update2:                          # @Ppmd7_Update2
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
.Lcfi32:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	addw	$4, 2(%rax)
	movq	16(%rbx), %rax
	movb	1(%rax), %cl
	addb	$4, %cl
	movb	%cl, 1(%rax)
	cmpb	$125, %cl
	jb	.LBB10_2
# BB#1:
	movq	%rbx, %rdi
	callq	Rescale
.LBB10_2:
	movl	48(%rbx), %eax
	movl	%eax, 44(%rbx)
	movq	%rbx, %rdi
	popq	%rbx
	jmp	UpdateModel             # TAILCALL
.Lfunc_end10:
	.size	Ppmd7_Update2, .Lfunc_end10-Ppmd7_Update2
	.cfi_endproc

	.p2align	4, 0x90
	.type	UpdateModel,@function
UpdateModel:                            # @UpdateModel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 112
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	16(%r12), %rdi
	movzwl	2(%rdi), %r8d
	movzwl	4(%rdi), %eax
	cmpb	$30, 1(%rdi)
	ja	.LBB11_11
# BB#1:
	movq	(%r12), %rcx
	movl	8(%rcx), %ecx
	testq	%rcx, %rcx
	je	.LBB11_11
# BB#2:
	movq	64(%r12), %rdx
	movzwl	(%rdx,%rcx), %esi
	cmpl	$1, %esi
	jne	.LBB11_5
# BB#3:
	movb	3(%rdx,%rcx), %bl
	cmpb	$31, %bl
	ja	.LBB11_11
# BB#4:
	incb	%bl
	movb	%bl, 3(%rdx,%rcx)
	cmpl	$0, 24(%r12)
	jne	.LBB11_14
	jmp	.LBB11_12
.LBB11_5:
	movl	4(%rdx,%rcx), %ebp
	leaq	(%rdx,%rbp), %rsi
	movb	(%rdi), %bl
	cmpb	%bl, (%rdx,%rbp)
	je	.LBB11_9
	.p2align	4, 0x90
.LBB11_6:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpb	%bl, 6(%rsi)
	leaq	6(%rsi), %rsi
	jne	.LBB11_6
# BB#7:
	movb	1(%rsi), %bl
	cmpb	-5(%rsi), %bl
	jb	.LBB11_9
# BB#8:
	movzwl	4(%rsi), %edi
	movw	%di, 20(%rsp)
	movl	(%rsi), %edi
	movl	%edi, 16(%rsp)
	movzwl	-2(%rsi), %edi
	movw	%di, 4(%rsi)
	movl	-6(%rsi), %edi
	movl	%edi, (%rsi)
	movzwl	20(%rsp), %edi
	movw	%di, -2(%rsi)
	movl	16(%rsp), %edi
	movl	%edi, -6(%rsi)
	leaq	-6(%rsi), %rsi
.LBB11_9:
	movb	1(%rsi), %bl
	cmpb	$114, %bl
	ja	.LBB11_11
# BB#10:
	addb	$2, %bl
	movb	%bl, 1(%rsi)
	addw	$2, 2(%rdx,%rcx)
.LBB11_11:
	cmpl	$0, 24(%r12)
	je	.LBB11_12
.LBB11_14:
	movq	16(%r12), %rcx
	movq	88(%r12), %rdx
	movb	(%rcx), %cl
	leaq	1(%rdx), %rsi
	movq	%rsi, 88(%r12)
	movb	%cl, (%rdx)
	movq	88(%r12), %r11
	cmpq	96(%r12), %r11
	jae	.LBB11_58
# BB#15:
	shll	$16, %eax
	orl	%r8d, %eax
	movq	64(%r12), %r10
	subq	%r10, %r11
	testl	%eax, %eax
	je	.LBB11_22
# BB#16:
	cmpl	%r11d, %eax
	ja	.LBB11_19
# BB#17:
	movq	%r11, %rbx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	CreateSuccessors
	testq	%rax, %rax
	je	.LBB11_58
# BB#18:                                # %.thread
	movq	64(%r12), %r10
	subl	%r10d, %eax
	movq	%rbx, %r11
.LBB11_19:
	decl	24(%r12)
	je	.LBB11_21
# BB#20:                                # %._crit_edge226
	movq	(%r12), %rcx
	jmp	.LBB11_23
.LBB11_12:
	movl	$1, %esi
	movq	%r12, %rdi
	callq	CreateSuccessors
	movq	%rax, 8(%r12)
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.LBB11_58
# BB#13:
	movq	16(%r12), %rcx
	subq	64(%r12), %rax
	movw	%ax, 2(%rcx)
	shrl	$16, %eax
	movw	%ax, 4(%rcx)
	jmp	.LBB11_57
.LBB11_22:
	movq	16(%r12), %rax
	movw	%r11w, 2(%rax)
	movl	%r11d, %edx
	shrl	$16, %edx
	movw	%dx, 4(%rax)
	movq	(%r12), %rcx
	movl	%ecx, %eax
	subl	%r10d, %eax
	jmp	.LBB11_23
.LBB11_21:
	movq	(%r12), %rcx
	xorl	%edx, %edx
	cmpq	%rcx, 8(%r12)
	movq	$-1, %rsi
	cmoveq	%rdx, %rsi
	addq	%rsi, 88(%r12)
	movl	%eax, %r11d
.LBB11_23:
	movq	8(%r12), %rbx
	cmpq	%rcx, %rbx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	je	.LBB11_56
# BB#24:                                # %.lr.ph
	movzwl	(%rcx), %r9d
	movq	16(%r12), %rdx
	movzbl	1(%rdx), %edx
	movzwl	2(%rcx), %r14d
	xorl	%r15d, %r15d
	cmpl	$3, %r9d
	seta	%r15b
	incl	%r14d
	subl	%r9d, %r14d
	subl	%edx, %r14d
	movl	%r11d, %eax
	shrl	$16, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB11_25:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_40 Depth 2
	movzwl	(%rbx), %r13d
	cmpl	$1, %r13d
	jne	.LBB11_26
# BB#42:                                #   in Loop: Header=BB11_25 Depth=1
	movl	276(%r12), %edx
	testq	%rdx, %rdx
	je	.LBB11_44
# BB#43:                                #   in Loop: Header=BB11_25 Depth=1
	leaq	(%r10,%rdx), %rax
	movl	(%r10,%rdx), %ecx
	movl	%ecx, 276(%r12)
	testq	%rax, %rax
	jne	.LBB11_48
	jmp	.LBB11_58
	.p2align	4, 0x90
.LBB11_26:                              #   in Loop: Header=BB11_25 Depth=1
	testb	$1, %r13b
	jne	.LBB11_29
# BB#27:                                #   in Loop: Header=BB11_25 Depth=1
	movl	%r13d, %r15d
	shrl	%r15d
	leal	-1(%r15), %ebp
	movzbl	146(%r12,%rbp), %r14d
	cmpb	146(%r12,%r15), %r14b
	je	.LBB11_28
# BB#30:                                #   in Loop: Header=BB11_25 Depth=1
	movl	280(%r12,%r14,4), %edx
	testq	%rdx, %rdx
	je	.LBB11_32
# BB#31:                                #   in Loop: Header=BB11_25 Depth=1
	leaq	(%r10,%rdx), %rax
	movl	(%r10,%rdx), %ecx
	movl	%ecx, 280(%r12,%r14,4)
	testq	%rax, %rax
	jne	.LBB11_36
	jmp	.LBB11_58
.LBB11_44:                              #   in Loop: Header=BB11_25 Depth=1
	movzbl	108(%r12), %eax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rcx
	movl	80(%r12), %edx
	movq	72(%r12), %rax
	subl	%eax, %edx
	cmpl	%edx, %ecx
	jbe	.LBB11_45
# BB#46:                                #   in Loop: Header=BB11_25 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r11, %rbp
	callq	AllocUnitsRare
	movl	8(%rsp), %r9d           # 4-byte Reload
	movq	%rbp, %r11
	testq	%rax, %rax
	jne	.LBB11_48
	jmp	.LBB11_58
.LBB11_45:                              #   in Loop: Header=BB11_25 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 72(%r12)
	testq	%rax, %rax
	je	.LBB11_58
	.p2align	4, 0x90
.LBB11_48:                              # %.thread215
                                        #   in Loop: Header=BB11_25 Depth=1
	movzwl	6(%rbx), %ecx
	movw	%cx, 4(%rax)
	movl	2(%rbx), %ecx
	movl	%ecx, (%rax)
	movq	64(%r12), %r10
	movl	%eax, %ecx
	subl	%r10d, %ecx
	movl	%ecx, 4(%rbx)
	movb	1(%rax), %dl
	cmpb	$30, %dl
	jb	.LBB11_49
# BB#50:                                # %.thread215
                                        #   in Loop: Header=BB11_25 Depth=1
	movb	$120, %dl
	jmp	.LBB11_51
	.p2align	4, 0x90
.LBB11_49:                              #   in Loop: Header=BB11_25 Depth=1
	addb	%dl, %dl
.LBB11_51:                              # %.thread215
                                        #   in Loop: Header=BB11_25 Depth=1
	movb	%dl, 1(%rax)
	movzbl	%dl, %eax
	movl	28(%r12), %edx
	addl	%r15d, %edx
	addl	%eax, %edx
	jmp	.LBB11_52
.LBB11_32:                              #   in Loop: Header=BB11_25 Depth=1
	leaq	1(%r14), %rsi
	movzbl	108(%r12,%rsi), %eax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rcx
	movl	80(%r12), %edx
	movq	72(%r12), %rax
	subl	%eax, %edx
	cmpl	%edx, %ecx
	jbe	.LBB11_33
# BB#34:                                #   in Loop: Header=BB11_25 Depth=1
	movq	%r12, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r11, 40(%rsp)          # 8-byte Spill
	callq	AllocUnitsRare
	movq	40(%rsp), %r11          # 8-byte Reload
	testq	%rax, %rax
	jne	.LBB11_36
	jmp	.LBB11_58
.LBB11_33:                              #   in Loop: Header=BB11_25 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 72(%r12)
	testq	%rax, %rax
	je	.LBB11_58
.LBB11_36:                              #   in Loop: Header=BB11_25 Depth=1
	movq	64(%r12), %r10
	movl	4(%rbx), %r8d
	leaq	(%r10,%r8), %r9
	testb	$1, %r15b
	jne	.LBB11_38
# BB#37:                                #   in Loop: Header=BB11_25 Depth=1
	movq	%rax, %rdx
	movq	%r9, %rdi
	movl	%r15d, %ebp
	cmpl	$1, %r15d
	jne	.LBB11_40
	jmp	.LBB11_41
.LBB11_38:                              #   in Loop: Header=BB11_25 Depth=1
	movl	(%r9), %edx
	movl	%edx, (%rax)
	movl	4(%r9), %edx
	movl	%edx, 4(%rax)
	movl	8(%r9), %edx
	movl	%edx, 8(%rax)
	movq	%r9, %rdi
	addq	$12, %rdi
	leaq	12(%rax), %rdx
	cmpl	$1, %r15d
	je	.LBB11_41
	.p2align	4, 0x90
.LBB11_40:                              #   Parent Loop BB11_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %esi
	movl	%esi, (%rdx)
	movl	4(%rdi), %esi
	movl	%esi, 4(%rdx)
	movl	8(%rdi), %esi
	movl	%esi, 8(%rdx)
	movl	12(%rdi), %esi
	movl	%esi, 12(%rdx)
	movl	16(%rdi), %esi
	movl	%esi, 16(%rdx)
	movl	20(%rdi), %esi
	movl	%esi, 20(%rdx)
	addq	$24, %rdi
	addq	$24, %rdx
	addl	$-2, %ebp
	jne	.LBB11_40
.LBB11_41:                              # %.thread211
                                        #   in Loop: Header=BB11_25 Depth=1
	movl	276(%r12,%r14,4), %edx
	movl	%edx, (%r9)
	movl	%r8d, 276(%r12,%r14,4)
	subl	%r10d, %eax
	movl	%eax, 4(%rbx)
	movl	8(%rsp), %r9d           # 4-byte Reload
.LBB11_28:                              #   in Loop: Header=BB11_25 Depth=1
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	12(%rsp), %r15d         # 4-byte Reload
.LBB11_29:                              # %.thread213
                                        #   in Loop: Header=BB11_25 Depth=1
	movzwl	2(%rbx), %eax
	leal	(%r13,%r13), %edx
	cmpl	%r9d, %edx
	movl	%eax, %edx
	adcl	$0, %edx
	leal	(,%r13,4), %esi
	cmpl	%r9d, %esi
	setbe	%cl
	leal	(,%r13,8), %esi
	cmpl	%esi, %eax
	setbe	%al
	andb	%cl, %al
	movzbl	%al, %eax
	leal	(%rdx,%rax,2), %edx
.LBB11_52:                              #   in Loop: Header=BB11_25 Depth=1
	movw	%dx, 2(%rbx)
	movq	16(%r12), %r8
	movzbl	1(%r8), %ecx
	movzwl	%dx, %edx
	leal	6(%rdx), %esi
	imull	%ecx, %esi
	addl	%esi, %esi
	leal	(%r14,%rdx), %edi
	leal	(%rdi,%rdi), %ecx
	leal	(%rcx,%rcx,2), %ecx
	cmpl	%ecx, %esi
	jae	.LBB11_54
# BB#53:                                #   in Loop: Header=BB11_25 Depth=1
	cmpl	%esi, %edi
	sbbl	%ecx, %ecx
	andl	$1, %ecx
	shll	$2, %edi
	xorl	%ebp, %ebp
	cmpl	%edi, %esi
	setae	%bpl
	leal	1(%rcx,%rbp), %esi
	movl	$3, %edi
	jmp	.LBB11_55
	.p2align	4, 0x90
.LBB11_54:                              #   in Loop: Header=BB11_25 Depth=1
	leal	(%rdi,%rdi,8), %ecx
	xorl	%ebp, %ebp
	cmpl	%ecx, %esi
	setae	%bpl
	movl	%edi, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,2), %ecx
	xorl	%eax, %eax
	cmpl	%ecx, %esi
	setae	%al
	addl	%ebp, %eax
	leal	(%rdi,%rdi,4), %ecx
	leal	(%rcx,%rcx,2), %ecx
	xorl	%edi, %edi
	cmpl	%ecx, %esi
	setae	%dil
	leal	4(%rdi,%rax), %edi
	movl	%edi, %esi
.LBB11_55:                              #   in Loop: Header=BB11_25 Depth=1
	addl	%edx, %edi
	movw	%di, 2(%rbx)
	movl	4(%rbx), %eax
	addq	%r10, %rax
	leaq	(%r13,%r13,2), %rcx
	movw	%r11w, 2(%rax,%rcx,2)
	movl	28(%rsp), %edx          # 4-byte Reload
	movw	%dx, 4(%rax,%rcx,2)
	movb	(%r8), %dl
	movb	%dl, (%rax,%rcx,2)
	movb	%sil, 1(%rax,%rcx,2)
	incl	%r13d
	movw	%r13w, (%rbx)
	movl	8(%rbx), %ebx
	addq	%r10, %rbx
	cmpq	(%r12), %rbx
	jne	.LBB11_25
.LBB11_56:                              # %._crit_edge
	movl	48(%rsp), %eax          # 4-byte Reload
	addq	%r10, %rax
	movq	%rax, (%r12)
	movq	%rax, 8(%r12)
.LBB11_57:                              # %.thread218
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_58:
	movq	%r12, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	RestartModel            # TAILCALL
.Lfunc_end11:
	.size	UpdateModel, .Lfunc_end11-UpdateModel
	.cfi_endproc

	.p2align	4, 0x90
	.type	CreateSuccessors,@function
CreateSuccessors:                       # @CreateSuccessors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi52:
	.cfi_def_cfa_offset 592
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	(%r12), %rbx
	movq	16(%r12), %rax
	movzwl	2(%rax), %ecx
	movzwl	4(%rax), %r15d
	shll	$16, %r15d
	xorl	%ebp, %ebp
	testl	%esi, %esi
	jne	.LBB12_2
# BB#1:
	movq	%rax, 16(%rsp)
	movl	$1, %ebp
.LBB12_2:                               # %.preheader
	orl	%ecx, %r15d
	movl	8(%rbx), %esi
	testl	%esi, %esi
	movq	64(%r12), %rcx
	je	.LBB12_10
# BB#3:                                 # %.lr.ph
	leaq	-6(%rcx), %r8
	.p2align	4, 0x90
.LBB12_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_6 Depth 2
	movl	%esi, %edx
	leaq	(%rcx,%rdx), %rbx
	movzwl	(%rcx,%rdx), %edx
	cmpl	$1, %edx
	jne	.LBB12_5
# BB#7:                                 #   in Loop: Header=BB12_4 Depth=1
	leaq	2(%rbx), %rsi
	jmp	.LBB12_8
	.p2align	4, 0x90
.LBB12_5:                               #   in Loop: Header=BB12_4 Depth=1
	movl	4(%rbx), %esi
	movb	(%rax), %dl
	addq	%r8, %rsi
	.p2align	4, 0x90
.LBB12_6:                               #   Parent Loop BB12_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	%dl, 6(%rsi)
	leaq	6(%rsi), %rsi
	jne	.LBB12_6
.LBB12_8:                               # %.loopexit102
                                        #   in Loop: Header=BB12_4 Depth=1
	movzwl	2(%rsi), %edi
	movzwl	4(%rsi), %edx
	shll	$16, %edx
	orl	%edi, %edx
	cmpl	%r15d, %edx
	jne	.LBB12_27
# BB#9:                                 # %.thread
                                        #   in Loop: Header=BB12_4 Depth=1
	movl	%ebp, %edx
	incl	%ebp
	movq	%rsi, 16(%rsp,%rdx,8)
	movl	8(%rbx), %esi
	testl	%esi, %esi
	jne	.LBB12_4
	jmp	.LBB12_10
.LBB12_27:
	movl	%edx, %ebx
	addq	%rcx, %rbx
	testl	%ebp, %ebp
	movq	%rbx, %rax
	je	.LBB12_26
.LBB12_10:                              # %.loopexit103
	movl	%r15d, %eax
	movb	(%rcx,%rax), %r8b
	incl	%r15d
	movl	%r15d, %edi
	shrl	$16, %edi
	movzwl	(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB12_12
# BB#11:
	movb	3(%rbx), %r13b
	jmp	.LBB12_18
.LBB12_12:
	movl	4(%rbx), %edx
	addq	%rcx, %rdx
	.p2align	4, 0x90
.LBB12_13:                              # =>This Inner Loop Header: Depth=1
	cmpb	%r8b, (%rdx)
	leaq	6(%rdx), %rdx
	jne	.LBB12_13
# BB#14:
	movzbl	-5(%rdx), %edx
	movzwl	2(%rbx), %esi
	incl	%esi
	addl	%edx, %eax
	subl	%eax, %esi
	leal	-2(%rdx,%rdx), %eax
	cmpl	%esi, %eax
	jbe	.LBB12_15
# BB#16:
	leal	(%rsi,%rsi,2), %edx
	leal	-1(%rax,%rdx), %eax
	addl	%esi, %esi
	xorl	%edx, %edx
	divl	%esi
	movl	%eax, %r13d
	jmp	.LBB12_17
.LBB12_15:
	decl	%edx
	leal	(%rdx,%rdx,4), %eax
	cmpl	%eax, %esi
	sbbl	%r13d, %r13d
	andl	$1, %r13d
.LBB12_17:
	incl	%r13d
.LBB12_18:                              # %.sink.split
	leal	-1(%rbp), %eax
	leaq	16(%rsp,%rax,8), %r14
	movb	%r8b, 11(%rsp)          # 1-byte Spill
	movl	%edi, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB12_19:                              # =>This Inner Loop Header: Depth=1
	movq	80(%r12), %rax
	cmpq	72(%r12), %rax
	je	.LBB12_22
# BB#20:                                #   in Loop: Header=BB12_19 Depth=1
	addq	$-12, %rax
	movq	%rax, 80(%r12)
	jmp	.LBB12_21
	.p2align	4, 0x90
.LBB12_22:                              #   in Loop: Header=BB12_19 Depth=1
	movl	276(%r12), %edx
	testq	%rdx, %rdx
	je	.LBB12_24
# BB#23:                                #   in Loop: Header=BB12_19 Depth=1
	leaq	(%rcx,%rdx), %rax
	movl	(%rcx,%rdx), %ecx
	movl	%ecx, 276(%r12)
	jmp	.LBB12_21
.LBB12_24:                              #   in Loop: Header=BB12_19 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	AllocUnitsRare
	movzbl	11(%rsp), %r8d          # 1-byte Folded Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	testq	%rax, %rax
	je	.LBB12_25
	.p2align	4, 0x90
.LBB12_21:                              #   in Loop: Header=BB12_19 Depth=1
	movw	$1, (%rax)
	movb	%r8b, 2(%rax)
	movb	%r13b, 3(%rax)
	movw	%r15w, 4(%rax)
	movw	%di, 6(%rax)
	movq	64(%r12), %rcx
	subl	%ecx, %ebx
	movl	%ebx, 8(%rax)
	movq	%rax, %rdx
	subq	%rcx, %rdx
	movq	(%r14), %rsi
	movw	%dx, 2(%rsi)
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	shrl	$16, %edx
	decl	%ebp
	leaq	-8(%r14), %r14
	movw	%dx, 4(%rsi)
	movq	%rax, %rbx
	jne	.LBB12_19
.LBB12_26:                              # %.loopexit
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_25:
	xorl	%eax, %eax
	jmp	.LBB12_26
.Lfunc_end12:
	.size	CreateSuccessors, .Lfunc_end12-CreateSuccessors
	.cfi_endproc

	.p2align	4, 0x90
	.type	AllocUnitsRare,@function
AllocUnitsRare:                         # @AllocUnitsRare
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$0, 56(%rdi)
	je	.LBB13_5
.LBB13_1:                               # %.preheader.preheader
	leal	1(%rsi), %eax
	.p2align	4, 0x90
.LBB13_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$38, %eax
	je	.LBB13_3
# BB#34:                                #   in Loop: Header=BB13_2 Depth=1
	movl	%eax, %edx
	movl	276(%rdi,%rdx,4), %ebp
	incl	%eax
	testq	%rbp, %rbp
	je	.LBB13_2
# BB#35:
	movq	64(%rdi), %r8
	leaq	(%r8,%rbp), %rax
	movl	(%r8,%rbp), %ecx
	movl	%ecx, 276(%rdi,%rdx,4)
	movzbl	108(%rdi,%rdx), %ebx
	movl	%esi, %edx
	movzbl	108(%rdi,%rdx), %edx
	subl	%edx, %ebx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,4), %rdx
	leal	-1(%rbx), %ebp
	movzbl	146(%rdi,%rbp), %esi
	movzbl	108(%rdi,%rsi), %ecx
	cmpl	%ebx, %ecx
	je	.LBB13_37
# BB#36:
	leal	-1(%rsi), %esi
	movzbl	108(%rdi,%rsi), %ecx
	leaq	(%rcx,%rcx,2), %r9
	leaq	(%rdx,%r9,4), %rbx
	subl	%ecx, %ebp
	movl	276(%rdi,%rbp,4), %ecx
	movl	%ecx, (%rdx,%r9,4)
	subl	%r8d, %ebx
	movl	%ebx, 276(%rdi,%rbp,4)
.LBB13_37:                              # %SplitBlock.exit
	movl	%esi, %ecx
	movl	276(%rdi,%rcx,4), %esi
	movl	%esi, (%rdx)
	subl	%r8d, %edx
	movl	%edx, 276(%rdi,%rcx,4)
	jmp	.LBB13_38
.LBB13_5:
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movl	52(%rdi), %r9d
	addl	104(%rdi), %r9d
	movl	$255, 56(%rdi)
	xorl	%r8d, %r8d
	movl	%r9d, %eax
	.p2align	4, 0x90
.LBB13_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_8 Depth 2
	movzbl	108(%rdi,%r8), %r10d
	movl	276(%rdi,%r8,4), %edx
	movl	$0, 276(%rdi,%r8,4)
	testl	%edx, %edx
	je	.LBB13_9
# BB#7:                                 # %.lr.ph132.i
                                        #   in Loop: Header=BB13_6 Depth=1
	movq	64(%rdi), %rbp
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB13_8:                               #   Parent Loop BB13_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %eax
	movl	%eax, %ecx
	movl	%ebx, 4(%rbp,%rcx)
	movl	%ebx, %edx
	movl	%eax, 8(%rbp,%rdx)
	movl	(%rbp,%rcx), %edx
	movw	$0, (%rbp,%rcx)
	movw	%r10w, 2(%rbp,%rcx)
	testl	%edx, %edx
	movl	%eax, %ebx
	jne	.LBB13_8
.LBB13_9:                               # %._crit_edge133.i
                                        #   in Loop: Header=BB13_6 Depth=1
	incq	%r8
	cmpq	$38, %r8
	jne	.LBB13_6
# BB#10:
	movq	64(%rdi), %r8
	movl	%r9d, %r10d
	movw	$1, (%r8,%r10)
	movl	%eax, 4(%r8,%r10)
	movl	%eax, %edx
	movl	%r9d, 8(%r8,%rdx)
	movq	72(%rdi), %rdx
	cmpq	80(%rdi), %rdx
	je	.LBB13_12
# BB#11:
	movw	$1, (%rdx)
	cmpl	%r9d, %eax
	jne	.LBB13_13
	jmp	.LBB13_18
	.p2align	4, 0x90
.LBB13_17:                              # %._crit_edge124.i
	movl	4(%rdx), %eax
.LBB13_12:                              # %.preheader.i
	cmpl	%r9d, %eax
	je	.LBB13_18
.LBB13_13:                              # %.lr.ph126.i
	movl	%eax, %eax
	leaq	(%r8,%rax), %rdx
	movzwl	2(%r8,%rax), %ebx
	leaq	(%rbx,%rbx,2), %rax
	cmpw	$0, (%rdx,%rax,4)
	jne	.LBB13_17
# BB#14:                                # %.lr.ph126.i
	movzwl	2(%rdx,%rax,4), %ebp
	addl	%ebx, %ebp
	cmpl	$65535, %ebp            # imm = 0xFFFF
	ja	.LBB13_17
	.p2align	4, 0x90
.LBB13_15:                              # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rbx,%rbx,2), %rax
	movl	4(%rdx,%rax,4), %ebx
	movl	8(%rdx,%rax,4), %ecx
	movl	%ebx, 4(%r8,%rcx)
	movl	4(%rdx,%rax,4), %eax
	movl	%ecx, 8(%r8,%rax)
	movw	%bp, 2(%rdx)
	movl	%ebp, %ebx
	leaq	(%rbx,%rbx,2), %rax
	cmpw	$0, (%rdx,%rax,4)
	jne	.LBB13_17
# BB#16:                                # %.critedge.i
                                        #   in Loop: Header=BB13_15 Depth=1
	movzwl	2(%rdx,%rax,4), %eax
	addl	%eax, %ebp
	cmpl	$65536, %ebp            # imm = 0x10000
	jb	.LBB13_15
	jmp	.LBB13_17
.LBB13_3:
	movl	%esi, %eax
	movzbl	108(%rdi,%rax), %eax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rcx
	decl	56(%rdi)
	movq	96(%rdi), %rax
	movl	88(%rdi), %edx
	movl	%eax, %esi
	subl	%edx, %esi
	cmpl	%ecx, %esi
	jbe	.LBB13_4
# BB#33:
	subq	%rcx, %rax
	movq	%rax, 96(%rdi)
	jmp	.LBB13_38
.LBB13_18:                              # %._crit_edge127.i
	movl	4(%r8,%r10), %r10d
	cmpl	%r9d, %r10d
	je	.LBB13_31
# BB#19:                                # %.lr.ph121.i
	leaq	1536(%r8), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_20:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_24 Depth 2
                                        #     Child Loop BB13_26 Depth 2
	movl	%r10d, %r13d
	leaq	(%r8,%r13), %rbx
	movl	4(%r8,%r13), %r10d
	movzwl	2(%r8,%r13), %r15d
	cmpl	$129, %r15d
	jb	.LBB13_28
# BB#21:                                # %.lr.ph.i
                                        #   in Loop: Header=BB13_20 Depth=1
	leal	-129(%r15), %r12d
	movl	%r12d, %r11d
	shrl	$7, %r11d
	leaq	(%r11,%r11,2), %r14
	shlq	$9, %r14
	movl	424(%rdi), %eax
	leal	1(%r11), %edx
	andl	$3, %edx
	je	.LBB13_22
# BB#23:                                # %.prol.preheader
                                        #   in Loop: Header=BB13_20 Depth=1
	negl	%edx
	movl	%r15d, %ebp
	.p2align	4, 0x90
.LBB13_24:                              #   Parent Loop BB13_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rbx)
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, 424(%rdi)
	addl	$-128, %ebp
	addq	$1536, %rbx             # imm = 0x600
	incl	%edx
	jne	.LBB13_24
	jmp	.LBB13_25
.LBB13_22:                              #   in Loop: Header=BB13_20 Depth=1
	movl	%r15d, %ebp
.LBB13_25:                              # %.prol.loopexit
                                        #   in Loop: Header=BB13_20 Depth=1
	addq	%r13, %r14
	cmpl	$384, %r12d             # imm = 0x180
	jb	.LBB13_27
	.p2align	4, 0x90
.LBB13_26:                              #   Parent Loop BB13_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, (%rbx)
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, 424(%rdi)
	leaq	1536(%rbx), %rdx
	movl	%eax, 1536(%rbx)
	subl	%r8d, %edx
	movl	%edx, 424(%rdi)
	leaq	3072(%rbx), %rcx
	movl	%edx, 3072(%rbx)
	subl	%r8d, %ecx
	movl	%ecx, 424(%rdi)
	leaq	4608(%rbx), %rax
	movl	%ecx, 4608(%rbx)
	subl	%r8d, %eax
	movl	%eax, 424(%rdi)
	addl	$-512, %ebp             # imm = 0xFE00
	addq	$6144, %rbx             # imm = 0x1800
	cmpl	$128, %ebp
	ja	.LBB13_26
.LBB13_27:                              # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB13_20 Depth=1
	shll	$7, %r11d
	addq	-8(%rsp), %r14          # 8-byte Folded Reload
	addl	$-128, %r15d
	subl	%r11d, %r15d
	movq	%r14, %rbx
.LBB13_28:                              # %._crit_edge.i
                                        #   in Loop: Header=BB13_20 Depth=1
	leal	-1(%r15), %edx
	movzbl	146(%rdi,%rdx), %eax
	movzbl	108(%rdi,%rax), %ecx
	cmpl	%r15d, %ecx
	je	.LBB13_30
# BB#29:                                #   in Loop: Header=BB13_20 Depth=1
	leal	-1(%rax), %eax
	movzbl	108(%rdi,%rax), %ecx
	leaq	(%rcx,%rcx,2), %rbp
	leaq	(%rbx,%rbp,4), %rsi
	subl	%ecx, %edx
	movl	276(%rdi,%rdx,4), %ecx
	movl	%ecx, (%rbx,%rbp,4)
	subl	%r8d, %esi
	movl	%esi, 276(%rdi,%rdx,4)
.LBB13_30:                              #   in Loop: Header=BB13_20 Depth=1
	movl	%eax, %eax
	movl	276(%rdi,%rax,4), %ecx
	movl	%ecx, (%rbx)
	subl	%r8d, %ebx
	movl	%ebx, 276(%rdi,%rax,4)
	cmpl	%r9d, %r10d
	jne	.LBB13_20
.LBB13_31:                              # %GlueFreeBlocks.exit
	movq	-16(%rsp), %rsi         # 8-byte Reload
	movl	%esi, %edx
	movl	276(%rdi,%rdx,4), %ebp
	testq	%rbp, %rbp
	je	.LBB13_1
# BB#32:
	leaq	(%r8,%rbp), %rax
	movl	(%r8,%rbp), %ecx
	movl	%ecx, 276(%rdi,%rdx,4)
	jmp	.LBB13_38
.LBB13_4:
	xorl	%eax, %eax
.LBB13_38:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	AllocUnitsRare, .Lfunc_end13-AllocUnitsRare
	.cfi_endproc

	.type	PPMD7_kExpEscape,@object # @PPMD7_kExpEscape
	.section	.rodata,"a",@progbits
	.globl	PPMD7_kExpEscape
	.p2align	4
PPMD7_kExpEscape:
	.ascii	"\031\016\t\007\005\005\004\004\004\003\003\003\002\002\002\002"
	.size	PPMD7_kExpEscape, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
