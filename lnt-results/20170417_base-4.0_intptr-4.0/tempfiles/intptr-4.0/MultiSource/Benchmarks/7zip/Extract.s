	.text
	.file	"Extract.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	67                      # 0x43
	.long	97                      # 0x61
	.long	110                     # 0x6e
	.long	32                      # 0x20
.LCPI0_1:
	.long	110                     # 0x6e
	.long	111                     # 0x6f
	.long	116                     # 0x74
	.long	32                      # 0x20
.LCPI0_2:
	.long	99                      # 0x63
	.long	114                     # 0x72
	.long	101                     # 0x65
	.long	97                      # 0x61
.LCPI0_3:
	.long	116                     # 0x74
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	111                     # 0x6f
.LCPI0_4:
	.long	117                     # 0x75
	.long	116                     # 0x74
	.long	112                     # 0x70
	.long	117                     # 0x75
.LCPI0_5:
	.long	116                     # 0x74
	.long	32                      # 0x20
	.long	100                     # 0x64
	.long	105                     # 0x69
.LCPI0_6:
	.long	114                     # 0x72
	.long	101                     # 0x65
	.long	99                      # 0x63
	.long	116                     # 0x74
.LCPI0_7:
	.long	111                     # 0x6f
	.long	114                     # 0x72
	.long	121                     # 0x79
	.long	32                      # 0x20
.LCPI0_8:
	.zero	16
	.text
	.globl	_Z18DecompressArchivesP7CCodecsRK13CRecordVectorIiER13CObjectVectorI11CStringBaseIwEES9_RKN9NWildcard11CCensorNodeERK15CExtractOptionsP15IOpenCallbackUIP18IExtractCallbackUIRS7_R15CDecompressStat
	.p2align	4, 0x90
	.type	_Z18DecompressArchivesP7CCodecsRK13CRecordVectorIiER13CObjectVectorI11CStringBaseIwEES9_RKN9NWildcard11CCensorNodeERK15CExtractOptionsP15IOpenCallbackUIP18IExtractCallbackUIRS7_R15CDecompressStat,@function
_Z18DecompressArchivesP7CCodecsRK13CRecordVectorIiER13CObjectVectorI11CStringBaseIwEES9_RKN9NWildcard11CCensorNodeERK15CExtractOptionsP15IOpenCallbackUIP18IExtractCallbackUIRS7_R15CDecompressStat: # @_Z18DecompressArchivesP7CCodecsRK13CRecordVectorIiER13CObjectVectorI11CStringBaseIwEES9_RKN9NWildcard11CCensorNodeERK15CExtractOptionsP15IOpenCallbackUIP18IExtractCallbackUIRS7_R15CDecompressStat
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$520, %rsp              # imm = 0x208
.Lcfi6:
	.cfi_def_cfa_offset 576
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	600(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 28(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movups	%xmm0, 368(%rsp)
	movq	$8, 384(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 360(%rsp)
	movq	%r9, 16(%rsp)           # 8-byte Spill
	cmpb	$0, (%r9)
	movq	%r8, 288(%rsp)          # 8-byte Spill
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	%rsi, 408(%rsp)         # 8-byte Spill
	movq	%rdi, 208(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	cmovel	12(%rdx), %eax
	testl	%eax, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	jle	.LBB0_13
# BB#1:                                 # %.lr.ph675
	leaq	176(%rsp), %r13
	movslq	%eax, %rbp
	xorl	%r15d, %r15d
	leaq	136(%rsp), %r14
	leaq	360(%rsp), %r12
	xorl	%eax, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
.Ltmp0:
.Lcfi13:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp1:
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, 176(%rsp)
	movl	$0, (%rax)
	movl	$4, 188(%rsp)
	movq	$0, 136(%rsp)
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$0, (%rax)
	je	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax), %rsi
.Ltmp3:
.Lcfi14:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
.Ltmp4:
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	testb	%al, %al
	je	.LBB0_227
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	testb	$16, 168(%rsp)
	jne	.LBB0_228
# BB#8:                                 # %._crit_edge705
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	136(%rsp), %rbx
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
.Ltmp11:
.Lcfi15:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp12:
# BB#10:                                #   in Loop: Header=BB0_2 Depth=1
	movq	376(%rsp), %rax
	movslq	372(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 372(%rsp)
	movq	136(%rsp), %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_2 Depth=1
.Lcfi16:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_12:                               # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit300
                                        #   in Loop: Header=BB0_2 Depth=1
	addq	%rbx, 112(%rsp)         # 8-byte Folded Spill
	incq	%r15
	cmpq	%rbp, %r15
	jl	.LBB0_2
	jmp	.LBB0_14
.LBB0_13:
	xorl	%eax, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
.LBB0_14:                               # %._crit_edge676
.Ltmp14:
.Lcfi17:
	.cfi_escape 0x2e, 0x00
	movl	$336, %edi              # imm = 0x150
	callq	_Znwm
	movq	%rax, %r12
.Ltmp15:
# BB#15:
.Ltmp17:
.Lcfi18:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN23CArchiveExtractCallbackC2Ev
.Ltmp18:
	movl	8(%rsp), %r15d          # 4-byte Reload
# BB#16:
	movq	(%r12), %rax
.Ltmp20:
.Lcfi19:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp21:
# BB#17:                                # %_ZN9CMyComPtrI23IArchiveExtractCallbackEC2EPS0_.exit
	cmpl	$1, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	setg	267(%r12)
	movq	%rax, 88(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 316(%r12)
	movups	%xmm0, 304(%r12)
	movq	%r12, 104(%rsp)         # 8-byte Spill
	jle	.LBB0_20
# BB#18:
	movq	584(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp23:
.Lcfi20:
	.cfi_escape 0x2e, 0x00
	movq	112(%rsp), %rsi         # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %r13d
.Ltmp24:
# BB#19:
	testl	%r13d, %r13d
	jne	.LBB0_225
.LBB0_20:                               # %.preheader359
	testl	%r15d, %r15d
	jle	.LBB0_224
# BB#21:                                # %.lr.ph670
	leaq	272(%rsp), %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	32(%rax), %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	xorps	%xmm0, %xmm0
	movl	$8, %eax
	movd	%rax, %xmm1
	movdqa	%xmm1, 496(%rsp)        # 16-byte Spill
	xorl	%r13d, %r13d
	jmp	.LBB0_197
	.p2align	4, 0x90
.LBB0_22:                               #   in Loop: Header=BB0_197 Depth=1
	incq	%rbp
	movslq	%r15d, %rax
	cmpq	%rax, %rbp
	jl	.LBB0_197
	jmp	.LBB0_224
.LBB0_23:                               #   in Loop: Header=BB0_197 Depth=1
	movslq	8(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB0_209
# BB#24:                                #   in Loop: Header=BB0_197 Depth=1
	movq	(%r14), %rax
	leaq	(,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB0_25:                               #   Parent Loop BB0_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$46, -4(%rax,%rdx)
	je	.LBB0_29
# BB#26:                                #   in Loop: Header=BB0_25 Depth=2
	addq	$-4, %rdx
	jne	.LBB0_25
	jmp	.LBB0_209
.LBB0_27:                               #   in Loop: Header=BB0_197 Depth=1
	testl	%ebx, %ebx
	movq	104(%rsp), %r12         # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_37
# BB#28:                                #   in Loop: Header=BB0_197 Depth=1
	movl	$7, %r14d
	leaq	272(%rsp), %rbx
	jmp	.LBB0_218
.LBB0_29:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
                                        #   in Loop: Header=BB0_197 Depth=1
	leaq	-4(%rax,%rdx), %rbx
	subq	%rax, %rbx
	shrq	$2, %rbx
	testl	%ebx, %ebx
	js	.LBB0_209
# BB#30:                                #   in Loop: Header=BB0_197 Depth=1
	leal	1(%rbx), %edx
	subl	%edx, %ecx
.Ltmp42:
.Lcfi21:
	.cfi_escape 0x2e, 0x00
	leaq	32(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%r14, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp43:
# BB#31:                                # %_ZNK11CStringBaseIwE3MidEi.exit
                                        #   in Loop: Header=BB0_197 Depth=1
.Ltmp45:
.Lcfi22:
	.cfi_escape 0x2e, 0x00
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	%rbp, %rsi
	callq	_ZNK7CCodecs22FindFormatForExtensionERK11CStringBaseIwE
	movl	%eax, %ebp
.Ltmp46:
# BB#32:                                #   in Loop: Header=BB0_197 Depth=1
	testl	%ebp, %ebp
	js	.LBB0_35
# BB#33:                                #   in Loop: Header=BB0_197 Depth=1
	movq	32(%rsp), %rdi
.Ltmp48:
.Lcfi23:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.2, %esi
	callq	_Z15MyStringComparePKwS0_
.Ltmp49:
# BB#34:                                #   in Loop: Header=BB0_197 Depth=1
	testl	%eax, %eax
	je	.LBB0_66
.LBB0_35:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit316.thread
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_209
# BB#36:                                #   in Loop: Header=BB0_197 Depth=1
.Lcfi24:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_209
.LBB0_37:                               #   in Loop: Header=BB0_197 Depth=1
	movl	%ecx, 228(%rsp)         # 4-byte Spill
	movl	460(%rsp), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpb	$0, (%rcx)
	jne	.LBB0_55
# BB#38:                                #   in Loop: Header=BB0_197 Depth=1
	testl	%eax, %eax
	jle	.LBB0_55
# BB#39:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_197 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_40:                               # %.lr.ph
                                        #   Parent Loop BB0_197 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_42 Depth 3
	movq	216(%rsp), %rax         # 8-byte Reload
	movl	12(%rax), %ebp
	testl	%ebp, %ebp
	je	.LBB0_54
# BB#41:                                # %.lr.ph.i328
                                        #   in Loop: Header=BB0_40 Depth=2
	movl	%r15d, 8(%rsp)          # 4-byte Spill
	movq	464(%rsp), %rax
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_42:                               #   Parent Loop BB0_197 Depth=1
                                        #     Parent Loop BB0_40 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rbp,%r13), %r14d
	movl	%r14d, %ebx
	shrl	$31, %ebx
	addl	%r14d, %ebx
	sarl	%ebx
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movslq	%ebx, %r12
	movq	(%rax,%r12,8), %r15
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	(%r15), %rsi
.Ltmp77:
.Lcfi25:
	.cfi_escape 0x2e, 0x00
	callq	_Z15MyStringComparePKwS0_
.Ltmp78:
# BB#43:                                # %.noexc331
                                        #   in Loop: Header=BB0_42 Depth=3
	testl	%eax, %eax
	je	.LBB0_46
# BB#44:                                # %.thread.i329
                                        #   in Loop: Header=BB0_42 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	(%r15), %rsi
.Ltmp79:
.Lcfi26:
	.cfi_escape 0x2e, 0x00
	callq	_Z15MyStringComparePKwS0_
.Ltmp80:
# BB#45:                                # %.noexc332
                                        #   in Loop: Header=BB0_42 Depth=3
	leal	1(%rbx), %ecx
	testl	%eax, %eax
	cmovsl	%ebx, %ebp
	cmovsl	%r13d, %ecx
	cmpl	%ebp, %ecx
	movl	%ecx, %r13d
	jne	.LBB0_42
	jmp	.LBB0_52
	.p2align	4, 0x90
.LBB0_46:                               # %_ZNK13CObjectVectorI11CStringBaseIwEE12FindInSortedERKS1_.exit
                                        #   in Loop: Header=BB0_40 Depth=2
	cmpl	$-1, %r14d
	jl	.LBB0_52
# BB#47:                                # %_ZNK13CObjectVectorI11CStringBaseIwEE12FindInSortedERKS1_.exit
                                        #   in Loop: Header=BB0_40 Depth=2
	cmpq	56(%rsp), %r12          # 8-byte Folded Reload
	movl	8(%rsp), %r15d          # 4-byte Reload
	jle	.LBB0_53
# BB#48:                                #   in Loop: Header=BB0_40 Depth=2
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp82:
.Lcfi27:
	.cfi_escape 0x2e, 0x00
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp83:
# BB#49:                                #   in Loop: Header=BB0_40 Depth=2
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp84:
.Lcfi28:
	.cfi_escape 0x2e, 0x00
	movl	$1, %edx
	movl	%ebx, %esi
	callq	*16(%rax)
.Ltmp85:
# BB#50:                                #   in Loop: Header=BB0_40 Depth=2
	movq	376(%rsp), %rax
	movq	(%rax,%r12,8), %rbp
.Ltmp86:
.Lcfi29:
	.cfi_escape 0x2e, 0x00
	movl	$1, %edx
	leaq	360(%rsp), %rdi
	movl	%ebx, %esi
	callq	_ZN17CBaseRecordVector6DeleteEii
.Ltmp87:
# BB#51:                                #   in Loop: Header=BB0_40 Depth=2
	subq	%rbp, 112(%rsp)         # 8-byte Folded Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	12(%rax), %r15d
	jmp	.LBB0_53
	.p2align	4, 0x90
.LBB0_52:                               #   in Loop: Header=BB0_40 Depth=2
	movl	8(%rsp), %r15d          # 4-byte Reload
.LBB0_53:                               # %_ZNK13CObjectVectorI11CStringBaseIwEE12FindInSortedERKS1_.exit.thread
                                        #   in Loop: Header=BB0_40 Depth=2
	movq	48(%rsp), %rdx          # 8-byte Reload
.LBB0_54:                               # %_ZNK13CObjectVectorI11CStringBaseIwEE12FindInSortedERKS1_.exit.thread
                                        #   in Loop: Header=BB0_40 Depth=2
	incq	%rdx
	movslq	460(%rsp), %rax
	cmpq	%rax, %rdx
	jl	.LBB0_40
.LBB0_55:                               # %.loopexit358
                                        #   in Loop: Header=BB0_197 Depth=1
	testl	%eax, %eax
	je	.LBB0_59
# BB#56:                                #   in Loop: Header=BB0_197 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	addq	480(%rsp), %rsi
	movq	584(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp89:
.Lcfi30:
	.cfi_escape 0x2e, 0x00
	movq	%rsi, %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	callq	*40(%rax)
	movl	%eax, %r13d
.Ltmp90:
# BB#57:                                #   in Loop: Header=BB0_197 Depth=1
	testl	%r13d, %r13d
	jne	.LBB0_216
.LBB0_59:                               #   in Loop: Header=BB0_197 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 192(%rsp)
.Ltmp91:
.Lcfi31:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp92:
# BB#60:                                #   in Loop: Header=BB0_197 Depth=1
	movq	%rax, 192(%rsp)
	movl	$0, (%rax)
	movl	$4, 204(%rsp)
	movq	576(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp94:
.Lcfi32:
	.cfi_escape 0x2e, 0x00
	leaq	192(%rsp), %rsi
	movq	104(%rsp), %r12         # 8-byte Reload
	callq	*32(%rax)
	movl	%eax, %r13d
.Ltmp95:
	leaq	272(%rsp), %rbx
# BB#61:                                #   in Loop: Header=BB0_197 Depth=1
	testl	%r13d, %r13d
	je	.LBB0_72
.LBB0_62:                               #   in Loop: Header=BB0_197 Depth=1
	movl	$1, 24(%rsp)            # 4-byte Folded Spill
.LBB0_63:                               # %.loopexit
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_65
# BB#64:                                #   in Loop: Header=BB0_197 Depth=1
.Lcfi33:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_65:                               # %_ZN11CStringBaseIwED2Ev.exit306
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	56(%rsp), %rbp          # 8-byte Reload
	movl	24(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB0_218
.LBB0_66:                               #   in Loop: Header=BB0_197 Depth=1
.Ltmp51:
.Lcfi34:
	.cfi_escape 0x2e, 0x00
	xorl	%edx, %edx
	leaq	136(%rsp), %rdi
	movq	%r14, %rsi
	movl	%ebx, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp52:
# BB#67:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
                                        #   in Loop: Header=BB0_197 Depth=1
	movl	$0, 40(%rsp)
	movq	32(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	144(%rsp), %rax
	incq	%rax
	movl	44(%rsp), %ecx
	cmpl	%ecx, %eax
	je	.LBB0_94
# BB#68:                                #   in Loop: Header=BB0_197 Depth=1
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp54:
.Lcfi35:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rcx
.Ltmp55:
# BB#69:                                # %.noexc312
                                        #   in Loop: Header=BB0_197 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_92
# BB#70:                                # %.noexc312
                                        #   in Loop: Header=BB0_197 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movl	$0, %eax
	jle	.LBB0_93
# BB#71:                                # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB0_197 Depth=1
.Lcfi36:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	movq	%rcx, %rbx
	callq	_ZdaPv
	movq	%rbx, %rcx
	movslq	40(%rsp), %rax
	jmp	.LBB0_93
.LBB0_72:                               #   in Loop: Header=BB0_197 Depth=1
	cmpl	$0, 200(%rsp)
	je	.LBB0_76
# BB#73:                                #   in Loop: Header=BB0_197 Depth=1
	movq	584(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp96:
.Lcfi37:
	.cfi_escape 0x2e, 0x00
	leaq	192(%rsp), %rsi
	callq	*120(%rax)
	movl	%eax, %r13d
.Ltmp97:
# BB#74:                                #   in Loop: Header=BB0_197 Depth=1
	testl	%r13d, %r13d
	jne	.LBB0_62
.LBB0_76:                               # %.preheader
                                        #   in Loop: Header=BB0_197 Depth=1
	movl	428(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB0_83
# BB#77:                                # %.lr.ph657.preheader
                                        #   in Loop: Header=BB0_197 Depth=1
	xorl	%ebx, %ebx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_78:                               # %.lr.ph657
                                        #   Parent Loop BB0_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	432(%rsp), %rcx
	movq	(%rcx,%rbx,8), %rcx
	cmpl	$0, 72(%rcx)
	je	.LBB0_82
# BB#79:                                #   in Loop: Header=BB0_78 Depth=2
	movl	$1, 24(%rsp)            # 4-byte Folded Spill
	movq	584(%rsp), %rdi
	movq	(%rdi), %rax
	movq	64(%rcx), %rsi
.Ltmp99:
.Lcfi38:
	.cfi_escape 0x2e, 0x00
	callq	*72(%rax)
	movl	%eax, %r13d
.Ltmp100:
# BB#80:                                #   in Loop: Header=BB0_78 Depth=2
	testl	%r13d, %r13d
	jne	.LBB0_91
# BB#81:                                # %..thread_crit_edge
                                        #   in Loop: Header=BB0_78 Depth=2
	movl	428(%rsp), %eax
	xorps	%xmm0, %xmm0
.LBB0_82:                               # %.thread
                                        #   in Loop: Header=BB0_78 Depth=2
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB0_78
	jmp	.LBB0_84
.LBB0_83:                               #   in Loop: Header=BB0_197 Depth=1
	xorps	%xmm0, %xmm0
.LBB0_84:                               # %._crit_edge
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	432(%rsp), %rcx
	cltq
	movq	-8(%rcx,%rax,8), %rdx
	movb	268(%rsp), %al
	movq	16(%rsp), %rcx          # 8-byte Reload
	orb	(%rcx), %al
	sete	56(%rdx)
	movq	256(%rsp), %rax
	movq	%rax, 48(%rdx)
	movq	480(%rsp), %rax
	addq	232(%rsp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	(%rdx), %rbp
	leaq	336(%rsp), %rax
	movups	%xmm0, (%rax)
	movq	$4, 352(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 328(%rsp)
	cmpb	$0, (%rcx)
	je	.LBB0_88
.LBB0_85:                               #   in Loop: Header=BB0_197 Depth=1
	movq	%rbp, %r13
	movl	$1, 24(%rsp)            # 4-byte Folded Spill
	leaq	144(%rsp), %rax
	movups	%xmm0, (%rax)
	movq	$8, 160(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 136(%rsp)
	movaps	%xmm0, 64(%rsp)
	movslq	24(%rcx), %r14
	leaq	1(%r14), %rbx
	testl	%ebx, %ebx
	je	.LBB0_113
# BB#86:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp117:
.Lcfi39:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp118:
	leaq	32(%rsp), %rbp
# BB#87:                                # %.noexc.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	%rax, 64(%rsp)
	movl	$0, (%rax)
	movl	%ebx, 76(%rsp)
	xorps	%xmm0, %xmm0
	jmp	.LBB0_114
.LBB0_88:                               #   in Loop: Header=BB0_197 Depth=1
	movq	(%rbp), %rax
.Ltmp102:
.Lcfi40:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	leaq	136(%rsp), %rsi
	callq	*56(%rax)
	movl	%eax, %r13d
.Ltmp103:
	leaq	272(%rsp), %rbx
# BB#89:                                #   in Loop: Header=BB0_197 Depth=1
	movl	$1, 24(%rsp)            # 4-byte Folded Spill
	testl	%r13d, %r13d
	je	.LBB0_139
# BB#90:                                # %.thread178.thread.i
                                        #   in Loop: Header=BB0_197 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB0_192
.LBB0_91:                               #   in Loop: Header=BB0_197 Depth=1
	leaq	272(%rsp), %rbx
	jmp	.LBB0_63
.LBB0_92:                               #   in Loop: Header=BB0_197 Depth=1
	xorl	%eax, %eax
.LBB0_93:                               # %._crit_edge16.i.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	%rcx, 32(%rsp)
	movl	$0, (%rcx,%rax,4)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 44(%rsp)
	movq	%rcx, %rbx
.LBB0_94:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	136(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_95:                               #   Parent Loop BB0_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_95
# BB#96:                                #   in Loop: Header=BB0_197 Depth=1
	movl	144(%rsp), %ecx
	movl	%ecx, 40(%rsp)
	testq	%rdi, %rdi
	je	.LBB0_98
# BB#97:                                #   in Loop: Header=BB0_197 Depth=1
.Lcfi41:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	movl	40(%rsp), %ecx
.LBB0_98:                               # %_ZN11CStringBaseIwED2Ev.exit313
                                        #   in Loop: Header=BB0_197 Depth=1
	testl	%ecx, %ecx
	je	.LBB0_35
# BB#99:                                #   in Loop: Header=BB0_197 Depth=1
	movq	32(%rsp), %rax
	movslq	%ecx, %rdx
	shlq	$2, %rdx
	.p2align	4, 0x90
.LBB0_100:                              #   Parent Loop BB0_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$46, -4(%rax,%rdx)
	je	.LBB0_102
# BB#101:                               #   in Loop: Header=BB0_100 Depth=2
	addq	$-4, %rdx
	jne	.LBB0_100
	jmp	.LBB0_35
.LBB0_102:                              # %_ZNK11CStringBaseIwE11ReverseFindEw.exit316
                                        #   in Loop: Header=BB0_197 Depth=1
	leaq	-4(%rax,%rdx), %rdx
	subq	%rax, %rdx
	shrq	$2, %rdx
	testl	%edx, %edx
	js	.LBB0_35
# BB#103:                               #   in Loop: Header=BB0_197 Depth=1
	incl	%edx
	subl	%edx, %ecx
.Ltmp57:
.Lcfi42:
	.cfi_escape 0x2e, 0x00
	leaq	136(%rsp), %rbx
	movq	%rbx, %rdi
	leaq	32(%rsp), %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp58:
# BB#104:                               # %_ZNK11CStringBaseIwE3MidEi.exit318
                                        #   in Loop: Header=BB0_197 Depth=1
.Ltmp60:
.Lcfi43:
	.cfi_escape 0x2e, 0x00
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, %rsi
	callq	_ZNK7CCodecs22FindFormatForExtensionERK11CStringBaseIwE
	movl	%eax, %ebx
.Ltmp61:
# BB#105:                               #   in Loop: Header=BB0_197 Depth=1
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_107
# BB#106:                               #   in Loop: Header=BB0_197 Depth=1
.Lcfi44:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_107:                              # %_ZN11CStringBaseIwED2Ev.exit319
                                        #   in Loop: Header=BB0_197 Depth=1
	testl	%ebx, %ebx
	js	.LBB0_35
# BB#108:                               #   in Loop: Header=BB0_197 Depth=1
	movq	32(%rsp), %rdi
.Ltmp63:
.Lcfi45:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.3, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp64:
# BB#109:                               # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit
                                        #   in Loop: Header=BB0_197 Depth=1
	testl	%eax, %eax
	je	.LBB0_35
# BB#110:                               #   in Loop: Header=BB0_197 Depth=1
.Ltmp65:
.Lcfi46:
	.cfi_escape 0x2e, 0x00
	leaq	296(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp66:
# BB#111:                               #   in Loop: Header=BB0_197 Depth=1
	movq	312(%rsp), %rax
	movslq	308(%rsp), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	308(%rsp)
.Ltmp67:
.Lcfi47:
	.cfi_escape 0x2e, 0x00
	leaq	296(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp68:
# BB#112:                               # %_ZN13CRecordVectorIiE3AddEi.exit323
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	312(%rsp), %rax
	movslq	308(%rsp), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	308(%rsp)
	jmp	.LBB0_35
.LBB0_113:                              #   in Loop: Header=BB0_197 Depth=1
	xorl	%eax, %eax
	leaq	32(%rsp), %rbp
.LBB0_114:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rcx
	leaq	272(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_115:                              #   Parent Loop BB0_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB0_115
# BB#116:                               # %_Z11MyStringLenIwEiPKT_.exit.i.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movl	%r14d, 72(%rsp)
	movaps	%xmm0, 80(%rsp)
.Ltmp120:
.Lcfi48:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	_Znam
.Ltmp121:
# BB#117:                               # %.noexc127.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	%rax, 80(%rsp)
	movl	$2, 92(%rsp)
	movl	$42, (%rax)
	movl	$0, 4(%rax)
	movl	$1, 88(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	24(%rax), %rsi
.Ltmp123:
.Lcfi49:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movq	104(%rsp), %r12         # 8-byte Reload
	callq	_Z16GetCorrectFsPathRK11CStringBaseIwE
.Ltmp124:
# BB#118:                               #   in Loop: Header=BB0_197 Depth=1
.Ltmp126:
.Lcfi50:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	leaq	80(%rsp), %rsi
	movq	%rbp, %rdx
	callq	_ZN11CStringBaseIwE7ReplaceERKS0_S2_
.Ltmp127:
# BB#119:                               #   in Loop: Header=BB0_197 Depth=1
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_121
# BB#120:                               #   in Loop: Header=BB0_197 Depth=1
.Lcfi51:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_121:                              # %_ZN11CStringBaseIwED2Ev.exit128.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_123
# BB#122:                               #   in Loop: Header=BB0_197 Depth=1
.Lcfi52:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_123:                              # %_ZN11CStringBaseIwED2Ev.exit129.i
                                        #   in Loop: Header=BB0_197 Depth=1
	cmpl	$0, 72(%rsp)
	je	.LBB0_126
# BB#124:                               #   in Loop: Header=BB0_197 Depth=1
	movq	64(%rsp), %rdi
.Ltmp129:
.Lcfi53:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NWindows5NFile10NDirectory22CreateComplexDirectoryEPKw
.Ltmp130:
# BB#125:                               #   in Loop: Header=BB0_197 Depth=1
	testb	%al, %al
	je	.LBB0_130
.LBB0_126:                              #   in Loop: Header=BB0_197 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$0, (%rax)
	movl	$0, %esi
	cmovneq	288(%rsp), %rsi         # 8-byte Folded Reload
	movzbl	3(%rax), %r9d
	movzbl	1(%rax), %r8d
	movzbl	4(%rax), %eax
.Ltmp143:
.Lcfi54:
	.cfi_escape 0x2e, 0x20
	movq	%r12, %rdi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	584(%rsp), %rcx
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	leaq	144(%rsp), %rbp
	pushq	%rbp
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	leaq	80(%rsp), %rbp
	pushq	%rbp
	movq	%r13, %rbp
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	callq	_ZN23CArchiveExtractCallback4InitEPKN9NWildcard11CCensorNodeEPK4CArcP29IFolderArchiveExtractCallbackbbbRK11CStringBaseIwERK13CObjectVectorISA_Ey
	addq	$32, %rsp
.Lcfi59:
	.cfi_adjust_cfa_offset -32
.Ltmp144:
# BB#127:                               #   in Loop: Header=BB0_197 Depth=1
.Ltmp145:
.Lcfi60:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movq	400(%rsp), %rsi         # 8-byte Reload
	callq	_Z13SetPropertiesP8IUnknownRK13CObjectVectorI9CPropertyE
	movl	%eax, %r13d
.Ltmp146:
# BB#128:                               #   in Loop: Header=BB0_197 Depth=1
	testl	%r13d, %r13d
	je	.LBB0_137
# BB#129:                               #   in Loop: Header=BB0_197 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB0_188
.LBB0_130:                              # %_Z11MyStringLenIwEiPKT_.exit.i133.i
                                        #   in Loop: Header=BB0_197 Depth=1
.Lcfi61:
	.cfi_escape 0x2e, 0x00
	callq	__errno_location
	movl	(%rax), %r13d
	testl	%r13d, %r13d
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovel	%eax, %r13d
.Ltmp131:
.Lcfi62:
	.cfi_escape 0x2e, 0x00
	movl	$132, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp132:
# BB#131:                               # %.noexc137.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [67,97,110,32]
	movups	%xmm0, (%rbx)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [110,111,116,32]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [99,114,101,97]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [116,101,32,111]
	movups	%xmm0, 48(%rbx)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [117,116,112,117]
	movups	%xmm0, 64(%rbx)
	movaps	.LCPI0_5(%rip), %xmm0   # xmm0 = [116,32,100,105]
	movups	%xmm0, 80(%rbx)
	movaps	.LCPI0_6(%rip), %xmm0   # xmm0 = [114,101,99,116]
	movups	%xmm0, 96(%rbx)
	movaps	.LCPI0_7(%rip), %xmm0   # xmm0 = [111,114,121,32]
	movups	%xmm0, 112(%rbx)
	movl	$0, 128(%rbx)
.Ltmp134:
.Lcfi63:
	.cfi_escape 0x2e, 0x00
	movl	$132, %edi
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	callq	_Znam
	movq	%rax, %rbp
.Ltmp135:
# BB#132:                               # %.noexc141.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movl	$67, (%rbp)
	movl	$4, %eax
.LBB0_133:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i._ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i_crit_edge
                                        #   Parent Loop BB0_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_133
# BB#134:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movl	72(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB0_157
# BB#135:                               #   in Loop: Header=BB0_197 Depth=1
	cmpl	$15, %eax
	movl	$16, %ecx
	cmovlel	%ecx, %eax
	addl	$34, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp137:
.Lcfi64:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp138:
# BB#136:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movups	112(%rbp), %xmm0
	movups	%xmm0, 112(%r14)
	movups	96(%rbp), %xmm0
	movups	%xmm0, 96(%r14)
	movups	80(%rbp), %xmm0
	movups	%xmm0, 80(%r14)
	movups	64(%rbp), %xmm0
	movups	%xmm0, 64(%r14)
	movups	(%rbp), %xmm0
	movdqu	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%r14)
	movups	%xmm2, 32(%r14)
	movdqu	%xmm1, 16(%r14)
	movups	%xmm0, (%r14)
.Lcfi65:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdaPv
	movl	$0, 128(%r14)
	movq	%r14, %rbp
	jmp	.LBB0_158
.LBB0_137:                              #   in Loop: Header=BB0_197 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 3(%rax)
	je	.LBB0_166
# BB#138:                               #   in Loop: Header=BB0_197 Depth=1
	movzbl	4(%rax), %ecx
	xorl	$1, %ecx
	jmp	.LBB0_167
.LBB0_139:                              # %.preheader.i
                                        #   in Loop: Header=BB0_197 Depth=1
	cmpl	$0, 136(%rsp)
	je	.LBB0_155
# BB#140:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_197 Depth=1
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
.LBB0_141:                              # %.lr.ph.i
                                        #   Parent Loop BB0_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
.Ltmp104:
.Lcfi66:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp105:
# BB#142:                               #   in Loop: Header=BB0_141 Depth=2
	movq	%rax, 80(%rsp)
	movl	$0, (%rax)
	movl	$4, 92(%rsp)
.Ltmp107:
.Lcfi67:
	.cfi_escape 0x2e, 0x00
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r14d, %esi
	leaq	80(%rsp), %rdx
	callq	_ZNK4CArc11GetItemPathEjR11CStringBaseIwE
.Ltmp108:
# BB#143:                               #   in Loop: Header=BB0_141 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	je	.LBB0_145
# BB#144:                               #   in Loop: Header=BB0_141 Depth=2
	movl	$1, %ebx
	movl	%eax, %r13d
	jmp	.LBB0_151
.LBB0_145:                              #   in Loop: Header=BB0_141 Depth=2
.Ltmp110:
.Lcfi68:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movl	%r14d, %esi
	leaq	64(%rsp), %rdx
	callq	_Z19IsArchiveItemFolderP10IInArchivejRb
.Ltmp111:
# BB#146:                               #   in Loop: Header=BB0_141 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	movl	$1, %ebx
	jne	.LBB0_151
# BB#147:                               #   in Loop: Header=BB0_141 Depth=2
	xorl	%edx, %edx
	cmpb	$0, 64(%rsp)
	sete	%dl
.Ltmp112:
.Lcfi69:
	.cfi_escape 0x2e, 0x00
	movq	288(%rsp), %rdi         # 8-byte Reload
	leaq	80(%rsp), %rsi
	callq	_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEb
.Ltmp113:
# BB#148:                               #   in Loop: Header=BB0_141 Depth=2
	movl	$4, %ebx
	testb	%al, %al
	je	.LBB0_151
# BB#149:                               #   in Loop: Header=BB0_141 Depth=2
.Ltmp114:
.Lcfi70:
	.cfi_escape 0x2e, 0x00
	leaq	328(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp115:
# BB#150:                               # %_ZN13CRecordVectorIjE3AddEj.exit.i
                                        #   in Loop: Header=BB0_141 Depth=2
	movq	344(%rsp), %rax
	movslq	340(%rsp), %rcx
	movl	%r14d, (%rax,%rcx,4)
	incl	340(%rsp)
	xorl	%ebx, %ebx
.LBB0_151:                              #   in Loop: Header=BB0_141 Depth=2
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_153
# BB#152:                               #   in Loop: Header=BB0_141 Depth=2
.Lcfi71:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_153:                              # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB0_141 Depth=2
	orl	$4, %ebx
	cmpl	$4, %ebx
	jne	.LBB0_174
# BB#154:                               #   in Loop: Header=BB0_141 Depth=2
	incl	%r14d
	cmpl	136(%rsp), %r14d
	leaq	272(%rsp), %rbx
	jb	.LBB0_141
.LBB0_155:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_197 Depth=1
	cmpl	$0, 340(%rsp)
	xorps	%xmm0, %xmm0
	je	.LBB0_180
# BB#156:                               # %.thread.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB0_85
.LBB0_157:                              #   in Loop: Header=BB0_197 Depth=1
	movq	%rbp, %r14
.LBB0_158:                              # %.noexc.i.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	movl	%r15d, 8(%rsp)          # 4-byte Spill
	movq	%r14, %rcx
	subq	$-128, %rcx
	movq	64(%rsp), %rsi
.LBB0_159:                              #   Parent Loop BB0_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB0_159
# BB#160:                               #   in Loop: Header=BB0_197 Depth=1
	movslq	72(%rsp), %r15
	movq	592(%rsp), %rax
	movq	%rax, %rcx
	movl	$0, 8(%rcx)
	movq	(%rcx), %rbx
	movl	$0, (%rbx)
	leaq	33(%r15), %rax
	movl	12(%rcx), %ebp
	cmpl	%ebp, %eax
	jne	.LBB0_162
# BB#161:                               #   in Loop: Header=BB0_197 Depth=1
	movq	128(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB0_184
.LBB0_162:                              #   in Loop: Header=BB0_197 Depth=1
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp140:
.Lcfi72:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rdx
.Ltmp141:
# BB#163:                               # %.noexc148.i
                                        #   in Loop: Header=BB0_197 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_182
# BB#164:                               # %.noexc148.i
                                        #   in Loop: Header=BB0_197 Depth=1
	testl	%ebp, %ebp
	movl	$0, %eax
	movq	128(%rsp), %rbp         # 8-byte Reload
	jle	.LBB0_183
# BB#165:                               # %._crit_edge.thread.i.i.i
                                        #   in Loop: Header=BB0_197 Depth=1
.Lcfi73:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	movq	%rdx, %rbx
	callq	_ZdaPv
	movq	%rbx, %rdx
	movq	592(%rsp), %rax
	movslq	8(%rax), %rax
	jmp	.LBB0_183
.LBB0_166:                              #   in Loop: Header=BB0_197 Depth=1
	xorl	%ecx, %ecx
.LBB0_167:                              #   in Loop: Header=BB0_197 Depth=1
	cmpb	$0, (%rax)
	movq	(%rbp), %rax
	movq	72(%rax), %rax
	je	.LBB0_172
# BB#168:                               #   in Loop: Header=BB0_197 Depth=1
.Ltmp148:
.Lcfi74:
	.cfi_escape 0x2e, 0x00
	xorl	%esi, %esi
	movl	$-1, %edx
	movq	%rbp, %rdi
	movq	%r12, %r8
	callq	*%rax
	movl	%eax, %ebx
.Ltmp149:
# BB#169:                               #   in Loop: Header=BB0_197 Depth=1
	movl	$0, 32(%rsp)
	movq	(%rbp), %rax
.Ltmp150:
.Lcfi75:
	.cfi_escape 0x2e, 0x00
	movl	$44, %esi
	movq	%rbp, %rdi
	leaq	32(%rsp), %rdx
	callq	*80(%rax)
.Ltmp151:
# BB#170:                               #   in Loop: Header=BB0_197 Depth=1
	testl	%eax, %eax
	je	.LBB0_175
.LBB0_171:                              #   in Loop: Header=BB0_197 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB0_178
.LBB0_172:                              #   in Loop: Header=BB0_197 Depth=1
	movq	344(%rsp), %rsi
	movl	340(%rsp), %edx
.Ltmp160:
.Lcfi76:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movq	%r12, %r8
	callq	*%rax
	movl	%eax, %ebx
.Ltmp161:
# BB#173:                               #   in Loop: Header=BB0_197 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB0_179
.LBB0_174:                              # %.thread178.i
                                        #   in Loop: Header=BB0_197 Depth=1
	xorl	%r14d, %r14d
	leaq	272(%rsp), %rbx
	jmp	.LBB0_192
.LBB0_175:                              #   in Loop: Header=BB0_197 Depth=1
	movzwl	32(%rsp), %eax
	cmpl	$21, %eax
	je	.LBB0_177
# BB#176:                               #   in Loop: Header=BB0_197 Depth=1
	movzwl	%ax, %eax
	cmpl	$19, %eax
	jne	.LBB0_171
.LBB0_177:                              #   in Loop: Header=BB0_197 Depth=1
.Ltmp152:
.Lcfi77:
	.cfi_escape 0x2e, 0x00
	leaq	32(%rsp), %rdi
	callq	_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT
	movq	%rax, %r14
.Ltmp153:
.LBB0_178:                              #   in Loop: Header=BB0_197 Depth=1
.Ltmp157:
.Lcfi78:
	.cfi_escape 0x2e, 0x00
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp158:
.LBB0_179:                              #   in Loop: Header=BB0_197 Depth=1
	movq	584(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp162:
.Lcfi79:
	.cfi_escape 0x2e, 0x00
	movl	%ebx, %esi
	callq	*112(%rax)
	movl	%eax, %r13d
.Ltmp163:
	jmp	.LBB0_187
.LBB0_180:                              #   in Loop: Header=BB0_197 Depth=1
	movq	584(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp182:
.Lcfi80:
	.cfi_escape 0x2e, 0x00
	callq	*104(%rax)
.Ltmp183:
# BB#181:                               # %.thread181.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movl	$1, 24(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.LBB0_192
.LBB0_182:                              #   in Loop: Header=BB0_197 Depth=1
	xorl	%eax, %eax
	movq	128(%rsp), %rbp         # 8-byte Reload
.LBB0_183:                              # %._crit_edge16.i.i144.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	592(%rsp), %rcx
	movq	%rdx, (%rcx)
	movl	$0, (%rdx,%rax,4)
	movq	392(%rsp), %rax         # 8-byte Reload
	movl	%eax, 12(%rcx)
	movq	%rdx, %rbx
.LBB0_184:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i145.i.preheader
                                        #   in Loop: Header=BB0_197 Depth=1
	addl	$32, %r15d
.LBB0_185:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i145.i
                                        #   Parent Loop BB0_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14), %eax
	addq	$4, %r14
	movl	%eax, (%rbx)
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB0_185
# BB#186:                               # %_ZN11CStringBaseIwED2Ev.exit150.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	592(%rsp), %rax
	movl	%r15d, 8(%rax)
.Lcfi81:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdaPv
.Lcfi82:
	.cfi_escape 0x2e, 0x00
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	xorl	%r14d, %r14d
	movl	8(%rsp), %r15d          # 4-byte Reload
.LBB0_187:                              #   in Loop: Header=BB0_197 Depth=1
	leaq	272(%rsp), %rbx
.LBB0_188:                              #   in Loop: Header=BB0_197 Depth=1
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_190
# BB#189:                               #   in Loop: Header=BB0_197 Depth=1
.Lcfi83:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_190:                              # %_ZN11CStringBaseIwED2Ev.exit163.i
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 136(%rsp)
.Ltmp173:
.Lcfi84:
	.cfi_escape 0x2e, 0x00
	leaq	136(%rsp), %rbp
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp174:
# BB#191:                               #   in Loop: Header=BB0_197 Depth=1
.Ltmp179:
.Lcfi85:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp180:
.LBB0_192:                              #   in Loop: Header=BB0_197 Depth=1
.Ltmp188:
.Lcfi86:
	.cfi_escape 0x2e, 0x00
	leaq	328(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp189:
# BB#193:                               #   in Loop: Header=BB0_197 Depth=1
	testl	%r13d, %r13d
	movl	228(%rsp), %ecx         # 4-byte Reload
	cmovnel	%r13d, %ecx
	jne	.LBB0_63
# BB#194:                               #   in Loop: Header=BB0_197 Depth=1
	movl	%ecx, %r13d
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$0, (%rax)
	jne	.LBB0_196
# BB#195:                               #   in Loop: Header=BB0_197 Depth=1
	movq	480(%rsp), %r14
	addq	232(%rsp), %r14
.LBB0_196:                              #   in Loop: Header=BB0_197 Depth=1
	movq	296(%r12), %rax
	addq	%r14, 48(%rax)
	movq	320(%r12), %rcx
	movq	%rcx, 56(%rax)
	xorl	%ecx, %ecx
	movq	592(%rsp), %rax
	cmpl	$0, 8(%rax)
	setne	%cl
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%eax, %r13d
	jmp	.LBB0_63
	.p2align	4, 0x90
.LBB0_197:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_25 Depth 2
                                        #     Child Loop BB0_95 Depth 2
                                        #     Child Loop BB0_100 Depth 2
                                        #     Child Loop BB0_40 Depth 2
                                        #       Child Loop BB0_42 Depth 3
                                        #     Child Loop BB0_78 Depth 2
                                        #     Child Loop BB0_141 Depth 2
                                        #     Child Loop BB0_115 Depth 2
                                        #     Child Loop BB0_133 Depth 2
                                        #     Child Loop BB0_159 Depth 2
                                        #     Child Loop BB0_185 Depth 2
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	(%rax,%rbp,8), %r14
	movups	%xmm0, (%rbx)
.Ltmp26:
.Lcfi87:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp27:
# BB#198:                               #   in Loop: Header=BB0_197 Depth=1
	movq	%rax, 272(%rsp)
	movl	$0, (%rax)
	movl	$4, 284(%rsp)
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$0, (%rax)
	je	.LBB0_200
# BB#199:                               #   in Loop: Header=BB0_197 Depth=1
	movq	$0, 232(%rsp)
	movl	$0, 264(%rsp)
	jmp	.LBB0_203
	.p2align	4, 0x90
.LBB0_200:                              #   in Loop: Header=BB0_197 Depth=1
	movq	(%r14), %rsi
.Ltmp29:
.Lcfi88:
	.cfi_escape 0x2e, 0x00
	leaq	232(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
.Ltmp30:
# BB#201:                               #   in Loop: Header=BB0_197 Depth=1
	testb	%al, %al
	je	.LBB0_229
# BB#202:                               #   in Loop: Header=BB0_197 Depth=1
	testb	$16, 264(%rsp)
	jne	.LBB0_229
.LBB0_203:                              #   in Loop: Header=BB0_197 Depth=1
	movq	576(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp34:
.Lcfi89:
	.cfi_escape 0x2e, 0x00
	callq	*48(%rax)
.Ltmp35:
# BB#204:                               #   in Loop: Header=BB0_197 Depth=1
	movq	584(%rsp), %rdi
	movq	(%rdi), %rax
	movq	(%r14), %rsi
.Ltmp36:
.Lcfi90:
	.cfi_escape 0x2e, 0x00
	callq	*88(%rax)
.Ltmp37:
# BB#205:                               #   in Loop: Header=BB0_197 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	je	.LBB0_207
# BB#206:                               #   in Loop: Header=BB0_197 Depth=1
	movl	$1, %r14d
	movl	%eax, %r13d
	movq	56(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_220
	.p2align	4, 0x90
.LBB0_207:                              #   in Loop: Header=BB0_197 Depth=1
	leaq	424(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$8, 440(%rsp)
	movq	$_ZTV13CObjectVectorI4CArcE+16, 416(%rsp)
	movups	%xmm0, 32(%rax)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 448(%rsp)
	movaps	496(%rsp), %xmm0        # 16-byte Reload
	movups	%xmm0, 472(%rsp)
	movb	$0, 488(%rsp)
.Ltmp39:
.Lcfi91:
	.cfi_escape 0x2e, 0x00
	leaq	296(%rsp), %rdi
	movq	408(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rsi
	callq	_ZN13CRecordVectorIiEC2ERKS0_
.Ltmp40:
# BB#208:                               #   in Loop: Header=BB0_197 Depth=1
	cmpl	$0, 12(%rbx)
	je	.LBB0_23
.LBB0_209:                              # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.thread
                                        #   in Loop: Header=BB0_197 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %ecx
.Ltmp70:
.Lcfi92:
	.cfi_escape 0x2e, 0x10
	subq	$8, %rsp
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	xorl	%r8d, %r8d
	leaq	424(%rsp), %rdi
	movq	216(%rsp), %rsi         # 8-byte Reload
	leaq	304(%rsp), %rdx
	movq	%r14, %r9
	pushq	584(%rsp)
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	callq	_ZN12CArchiveLink5Open2EP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP15IOpenCallbackUI
	addq	$16, %rsp
.Lcfi95:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebx
.Ltmp71:
# BB#210:                               #   in Loop: Header=BB0_197 Depth=1
	cmpl	$-2147467260, %ebx      # imm = 0x80004004
	jne	.LBB0_212
# BB#211:                               #   in Loop: Header=BB0_197 Depth=1
	movl	$-2147467260, %r13d     # imm = 0x80004004
	jmp	.LBB0_217
.LBB0_212:                              #   in Loop: Header=BB0_197 Depth=1
	movq	576(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp73:
.Lcfi96:
	.cfi_escape 0x2e, 0x00
	callq	*40(%rax)
.Ltmp74:
# BB#213:                               #   in Loop: Header=BB0_197 Depth=1
	movq	584(%rsp), %rdi
	movq	(%rdi), %rbp
	movq	(%r14), %rsi
.Ltmp75:
.Lcfi97:
	.cfi_escape 0x2e, 0x00
	movzbl	%al, %ecx
	movl	%ebx, %edx
	callq	*96(%rbp)
.Ltmp76:
# BB#214:                               #   in Loop: Header=BB0_197 Depth=1
	testl	%eax, %eax
	movl	%eax, %ecx
	cmovel	%r13d, %ecx
	je	.LBB0_27
# BB#215:                               #   in Loop: Header=BB0_197 Depth=1
	movl	%eax, %r13d
.LBB0_216:                              #   in Loop: Header=BB0_197 Depth=1
	movq	104(%rsp), %r12         # 8-byte Reload
.LBB0_217:                              #   in Loop: Header=BB0_197 Depth=1
	leaq	272(%rsp), %rbx
	movq	56(%rsp), %rbp          # 8-byte Reload
	movl	$1, %r14d
.LBB0_218:                              #   in Loop: Header=BB0_197 Depth=1
.Ltmp193:
.Lcfi98:
	.cfi_escape 0x2e, 0x00
	leaq	296(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp194:
# BB#219:                               #   in Loop: Header=BB0_197 Depth=1
.Ltmp198:
.Lcfi99:
	.cfi_escape 0x2e, 0x00
	leaq	416(%rsp), %rdi
	callq	_ZN12CArchiveLinkD2Ev
.Ltmp199:
.LBB0_220:                              #   in Loop: Header=BB0_197 Depth=1
	xorps	%xmm0, %xmm0
	movq	272(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_222
# BB#221:                               #   in Loop: Header=BB0_197 Depth=1
.Lcfi100:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	xorps	%xmm0, %xmm0
.LBB0_222:                              # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit303
                                        #   in Loop: Header=BB0_197 Depth=1
	cmpl	$7, %r14d
	je	.LBB0_22
# BB#223:                               # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit303
                                        #   in Loop: Header=BB0_197 Depth=1
	testl	%r14d, %r14d
	je	.LBB0_22
	jmp	.LBB0_225
.LBB0_224:                              # %._crit_edge671
	movups	304(%r12), %xmm0
	movq	600(%rsp), %rax
	movq	%rax, %rcx
	movups	%xmm0, 24(%rcx)
	movq	320(%r12), %rax
	movq	%rax, 8(%rcx)
	movl	328(%r12), %eax
	movl	%eax, 40(%rcx)
	movq	120(%rsp), %rax         # 8-byte Reload
	movslq	12(%rax), %rax
	movq	%rax, (%rcx)
	movq	296(%r12), %rax
	movq	48(%rax), %rax
	movq	%rax, 16(%rcx)
	xorl	%r13d, %r13d
.LBB0_225:                              # %.loopexit361
	movq	(%r12), %rax
.Ltmp203:
.Lcfi101:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp204:
# BB#226:                               # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit297
.Lcfi102:
	.cfi_escape 0x2e, 0x00
	leaq	360(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movl	%r13d, %eax
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_227:
.Lcfi103:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
.Ltmp6:
.Lcfi104:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp7:
	jmp	.LBB0_230
.LBB0_228:
.Lcfi105:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.1, (%rax)
.Ltmp8:
.Lcfi106:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp9:
	jmp	.LBB0_230
.LBB0_229:
.Lcfi107:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
.Ltmp31:
.Lcfi108:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp32:
.LBB0_230:
.LBB0_231:                              # %.loopexit.split-lp
.Ltmp33:
	movq	%rax, %rbx
	jmp	.LBB0_314
.LBB0_232:                              # %.loopexit.split-lp368
.Ltmp10:
	jmp	.LBB0_306
.LBB0_233:
.Ltmp159:
	jmp	.LBB0_262
.LBB0_234:
.Ltmp142:
	movq	%rax, %rbx
.Lcfi109:
	.cfi_escape 0x2e, 0x00
	movq	128(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB0_236
.LBB0_235:
.Ltmp139:
	movq	%rax, %rbx
.Lcfi110:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
.LBB0_236:                              # %_ZN11CStringBaseIwED2Ev.exit152.i
	callq	_ZdaPv
	jmp	.LBB0_239
.LBB0_237:
.Ltmp154:
	movq	%rax, %rbx
.Ltmp155:
.Lcfi111:
	.cfi_escape 0x2e, 0x00
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp156:
	jmp	.LBB0_263
.LBB0_238:
.Ltmp136:
	movq	%rax, %rbx
.LBB0_239:                              # %_ZN11CStringBaseIwED2Ev.exit152.i
.Lcfi112:
	.cfi_escape 0x2e, 0x00
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	jmp	.LBB0_263
.LBB0_240:
.Ltmp133:
	jmp	.LBB0_262
.LBB0_241:
.Ltmp164:
	jmp	.LBB0_262
.LBB0_242:
.Ltmp184:
	jmp	.LBB0_273
.LBB0_243:
.Ltmp119:
	movq	%rax, %rbx
	jmp	.LBB0_265
.LBB0_244:
.Ltmp62:
	movq	%rax, %rbx
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_282
# BB#245:
.Lcfi113:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_282
.LBB0_246:
.Ltmp59:
	jmp	.LBB0_281
.LBB0_247:
.Ltmp69:
	jmp	.LBB0_281
.LBB0_248:
.Ltmp181:
	jmp	.LBB0_273
.LBB0_249:
.Ltmp175:
	movq	%rax, %rbx
.Ltmp176:
.Lcfi114:
	.cfi_escape 0x2e, 0x00
	leaq	136(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp177:
	jmp	.LBB0_274
.LBB0_250:
.Ltmp178:
.Lcfi115:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_251:
.Ltmp128:
	movq	%rax, %rbx
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_254
# BB#252:
.Lcfi116:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_254
.LBB0_253:
.Ltmp125:
	movq	%rax, %rbx
.LBB0_254:                              # %_ZN11CStringBaseIwED2Ev.exit139.i
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_263
# BB#255:
.Lcfi117:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_263
.LBB0_256:
.Ltmp122:
	jmp	.LBB0_262
.LBB0_257:
.Ltmp116:
	jmp	.LBB0_270
.LBB0_258:
.Ltmp190:
	jmp	.LBB0_286
.LBB0_259:
.Ltmp56:
	movq	%rax, %rbx
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_282
# BB#260:
.Lcfi118:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_282
.LBB0_261:
.Ltmp147:
.LBB0_262:
	movq	%rax, %rbx
.LBB0_263:
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_265
# BB#264:
.Lcfi119:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_265:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 136(%rsp)
.Ltmp165:
.Lcfi120:
	.cfi_escape 0x2e, 0x00
	leaq	136(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp166:
# BB#266:
.Ltmp171:
.Lcfi121:
	.cfi_escape 0x2e, 0x00
	leaq	136(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp172:
	jmp	.LBB0_274
.LBB0_267:
.Ltmp167:
	movq	%rax, %rbx
.Ltmp168:
.Lcfi122:
	.cfi_escape 0x2e, 0x00
	leaq	136(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp169:
	jmp	.LBB0_276
.LBB0_268:
.Ltmp170:
.Lcfi123:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_269:
.Ltmp109:
.LBB0_270:
	movq	%rax, %rbx
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_274
# BB#271:
.Lcfi124:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_274
.LBB0_272:
.Ltmp106:
.LBB0_273:
	movq	%rax, %rbx
.LBB0_274:
.Ltmp185:
.Lcfi125:
	.cfi_escape 0x2e, 0x00
	leaq	328(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp186:
	jmp	.LBB0_287
.LBB0_275:
.Ltmp187:
	movq	%rax, %rbx
.LBB0_276:                              # %.body.i
.Lcfi126:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_277:
.Ltmp53:
	jmp	.LBB0_281
.LBB0_278:
.Ltmp50:
	jmp	.LBB0_281
.LBB0_279:
.Ltmp98:
	jmp	.LBB0_286
.LBB0_280:
.Ltmp47:
.LBB0_281:
	movq	%rax, %rbx
.LBB0_282:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_312
# BB#283:
.Lcfi127:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_312
.LBB0_284:
.Ltmp44:
	jmp	.LBB0_311
.LBB0_285:
.Ltmp101:
.LBB0_286:                              # %.body
	movq	%rax, %rbx
.LBB0_287:                              # %.body
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_312
# BB#288:
.Lcfi128:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_312
.LBB0_289:
.Ltmp25:
	jmp	.LBB0_302
.LBB0_290:
.Ltmp205:
	jmp	.LBB0_309
.LBB0_291:
.Ltmp22:
	jmp	.LBB0_309
.LBB0_292:
.Ltmp19:
	movq	%rax, %rbx
.Lcfi129:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZdlPv
	jmp	.LBB0_317
.LBB0_293:                              # %.loopexit.split-lp363
.Ltmp16:
	jmp	.LBB0_309
.LBB0_294:
.Ltmp88:
	jmp	.LBB0_311
.LBB0_295:
.Ltmp72:
	jmp	.LBB0_311
.LBB0_296:
.Ltmp41:
	jmp	.LBB0_299
.LBB0_297:
.Ltmp200:
	movq	%rax, %rbx
	jmp	.LBB0_314
.LBB0_298:
.Ltmp195:
.LBB0_299:
	movq	%rax, %rbx
	jmp	.LBB0_313
.LBB0_300:
.Ltmp93:
	jmp	.LBB0_311
.LBB0_301:
.Ltmp28:
.LBB0_302:
	movq	%rax, %rbx
	jmp	.LBB0_316
.LBB0_303:                              # %.loopexit367
.Ltmp5:
	jmp	.LBB0_306
.LBB0_304:                              # %.loopexit360
.Ltmp38:
	movq	%rax, %rbx
	jmp	.LBB0_314
.LBB0_305:
.Ltmp13:
.LBB0_306:
	movq	%rax, %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_317
# BB#307:
.Lcfi130:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_317
.LBB0_308:                              # %.loopexit362
.Ltmp2:
.LBB0_309:                              # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit
	movq	%rax, %rbx
	jmp	.LBB0_317
.LBB0_310:
.Ltmp81:
.LBB0_311:
	movq	%rax, %rbx
.LBB0_312:
.Ltmp191:
.Lcfi131:
	.cfi_escape 0x2e, 0x00
	leaq	296(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp192:
.LBB0_313:
.Ltmp196:
.Lcfi132:
	.cfi_escape 0x2e, 0x00
	leaq	416(%rsp), %rdi
	callq	_ZN12CArchiveLinkD2Ev
.Ltmp197:
.LBB0_314:
	movq	272(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_316
# BB#315:
.Lcfi133:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_316:
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp201:
.Lcfi134:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp202:
.LBB0_317:                              # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit
.Ltmp206:
.Lcfi135:
	.cfi_escape 0x2e, 0x00
	leaq	360(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp207:
# BB#318:
.Lcfi136:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_319:
.Ltmp208:
.Lcfi137:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_Z18DecompressArchivesP7CCodecsRK13CRecordVectorIiER13CObjectVectorI11CStringBaseIwEES9_RKN9NWildcard11CCensorNodeERK15CExtractOptionsP15IOpenCallbackUIP18IExtractCallbackUIRS7_R15CDecompressStat, .Lfunc_end0-_Z18DecompressArchivesP7CCodecsRK13CRecordVectorIiER13CObjectVectorI11CStringBaseIwEES9_RKN9NWildcard11CCensorNodeERK15CExtractOptionsP15IOpenCallbackUIP18IExtractCallbackUIRS7_R15CDecompressStat
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\206"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\332\006"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin0   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin0   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin0   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp80-.Ltmp77         #   Call between .Ltmp77 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin0   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp87-.Ltmp82         #   Call between .Ltmp82 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin0   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp92-.Ltmp89         #   Call between .Ltmp89 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin0   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp98-.Lfunc_begin0   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin0   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin0   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin0  #     jumps to .Ltmp101
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin0  # >> Call Site 19 <<
	.long	.Ltmp118-.Ltmp117       #   Call between .Ltmp117 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin0  #     jumps to .Ltmp119
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin0  # >> Call Site 20 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp184-.Lfunc_begin0  #     jumps to .Ltmp184
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin0   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin0   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp68-.Ltmp63         #   Call between .Ltmp63 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin0   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp120-.Lfunc_begin0  # >> Call Site 24 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin0  #     jumps to .Ltmp122
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin0  # >> Call Site 25 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin0  #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin0  # >> Call Site 26 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin0  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin0  # >> Call Site 27 <<
	.long	.Ltmp146-.Ltmp129       #   Call between .Ltmp129 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin0  #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin0  # >> Call Site 28 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin0  #     jumps to .Ltmp133
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin0  # >> Call Site 29 <<
	.long	.Ltmp135-.Ltmp134       #   Call between .Ltmp134 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin0  #     jumps to .Ltmp136
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin0  # >> Call Site 30 <<
	.long	.Ltmp138-.Ltmp137       #   Call between .Ltmp137 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin0  #     jumps to .Ltmp139
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin0  # >> Call Site 31 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin0  #     jumps to .Ltmp106
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin0  # >> Call Site 32 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin0  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin0  # >> Call Site 33 <<
	.long	.Ltmp115-.Ltmp110       #   Call between .Ltmp110 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin0  #     jumps to .Ltmp116
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin0  # >> Call Site 34 <<
	.long	.Ltmp141-.Ltmp140       #   Call between .Ltmp140 and .Ltmp141
	.long	.Ltmp142-.Lfunc_begin0  #     jumps to .Ltmp142
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin0  # >> Call Site 35 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp164-.Lfunc_begin0  #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin0  # >> Call Site 36 <<
	.long	.Ltmp151-.Ltmp150       #   Call between .Ltmp150 and .Ltmp151
	.long	.Ltmp154-.Lfunc_begin0  #     jumps to .Ltmp154
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin0  # >> Call Site 37 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp164-.Lfunc_begin0  #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp152-.Lfunc_begin0  # >> Call Site 38 <<
	.long	.Ltmp153-.Ltmp152       #   Call between .Ltmp152 and .Ltmp153
	.long	.Ltmp154-.Lfunc_begin0  #     jumps to .Ltmp154
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin0  # >> Call Site 39 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin0  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin0  # >> Call Site 40 <<
	.long	.Ltmp163-.Ltmp162       #   Call between .Ltmp162 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin0  #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin0  # >> Call Site 41 <<
	.long	.Ltmp183-.Ltmp182       #   Call between .Ltmp182 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin0  #     jumps to .Ltmp184
	.byte	0                       #   On action: cleanup
	.long	.Ltmp173-.Lfunc_begin0  # >> Call Site 42 <<
	.long	.Ltmp174-.Ltmp173       #   Call between .Ltmp173 and .Ltmp174
	.long	.Ltmp175-.Lfunc_begin0  #     jumps to .Ltmp175
	.byte	0                       #   On action: cleanup
	.long	.Ltmp179-.Lfunc_begin0  # >> Call Site 43 <<
	.long	.Ltmp180-.Ltmp179       #   Call between .Ltmp179 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin0  #     jumps to .Ltmp181
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin0  # >> Call Site 44 <<
	.long	.Ltmp189-.Ltmp188       #   Call between .Ltmp188 and .Ltmp189
	.long	.Ltmp190-.Lfunc_begin0  #     jumps to .Ltmp190
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 45 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 46 <<
	.long	.Ltmp37-.Ltmp29         #   Call between .Ltmp29 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin0   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 47 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin0   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin0   # >> Call Site 48 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin0   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin0   # >> Call Site 49 <<
	.long	.Ltmp76-.Ltmp73         #   Call between .Ltmp73 and .Ltmp76
	.long	.Ltmp93-.Lfunc_begin0   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp193-.Lfunc_begin0  # >> Call Site 50 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin0  #     jumps to .Ltmp195
	.byte	0                       #   On action: cleanup
	.long	.Ltmp198-.Lfunc_begin0  # >> Call Site 51 <<
	.long	.Ltmp199-.Ltmp198       #   Call between .Ltmp198 and .Ltmp199
	.long	.Ltmp200-.Lfunc_begin0  #     jumps to .Ltmp200
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin0  # >> Call Site 52 <<
	.long	.Ltmp204-.Ltmp203       #   Call between .Ltmp203 and .Ltmp204
	.long	.Ltmp205-.Lfunc_begin0  #     jumps to .Ltmp205
	.byte	0                       #   On action: cleanup
	.long	.Ltmp204-.Lfunc_begin0  # >> Call Site 53 <<
	.long	.Ltmp6-.Ltmp204         #   Call between .Ltmp204 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 54 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 55 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 56 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 57 <<
	.long	.Ltmp31-.Ltmp9          #   Call between .Ltmp9 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 58 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp155-.Lfunc_begin0  # >> Call Site 59 <<
	.long	.Ltmp156-.Ltmp155       #   Call between .Ltmp155 and .Ltmp156
	.long	.Ltmp187-.Lfunc_begin0  #     jumps to .Ltmp187
	.byte	1                       #   On action: 1
	.long	.Ltmp176-.Lfunc_begin0  # >> Call Site 60 <<
	.long	.Ltmp177-.Ltmp176       #   Call between .Ltmp176 and .Ltmp177
	.long	.Ltmp178-.Lfunc_begin0  #     jumps to .Ltmp178
	.byte	1                       #   On action: 1
	.long	.Ltmp165-.Lfunc_begin0  # >> Call Site 61 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin0  #     jumps to .Ltmp167
	.byte	1                       #   On action: 1
	.long	.Ltmp171-.Lfunc_begin0  # >> Call Site 62 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp187-.Lfunc_begin0  #     jumps to .Ltmp187
	.byte	1                       #   On action: 1
	.long	.Ltmp168-.Lfunc_begin0  # >> Call Site 63 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin0  #     jumps to .Ltmp170
	.byte	1                       #   On action: 1
	.long	.Ltmp185-.Lfunc_begin0  # >> Call Site 64 <<
	.long	.Ltmp186-.Ltmp185       #   Call between .Ltmp185 and .Ltmp186
	.long	.Ltmp187-.Lfunc_begin0  #     jumps to .Ltmp187
	.byte	1                       #   On action: 1
	.long	.Ltmp191-.Lfunc_begin0  # >> Call Site 65 <<
	.long	.Ltmp207-.Ltmp191       #   Call between .Ltmp191 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin0  #     jumps to .Ltmp208
	.byte	1                       #   On action: 1
	.long	.Ltmp207-.Lfunc_begin0  # >> Call Site 66 <<
	.long	.Lfunc_end0-.Ltmp207    #   Call between .Ltmp207 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.zero	16
	.section	.text._ZN23CArchiveExtractCallbackC2Ev,"axG",@progbits,_ZN23CArchiveExtractCallbackC2Ev,comdat
	.weak	_ZN23CArchiveExtractCallbackC2Ev
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallbackC2Ev,@function
_ZN23CArchiveExtractCallbackC2Ev:       # @_ZN23CArchiveExtractCallbackC2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 32
.Lcfi141:
	.cfi_offset %rbx, -32
.Lcfi142:
	.cfi_offset %r14, -24
.Lcfi143:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 24(%rbx)
	movl	$_ZTV23CArchiveExtractCallback+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTV23CArchiveExtractCallback+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTV23CArchiveExtractCallback+192, 16(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 64(%rbx)
	movdqu	%xmm0, 48(%rbx)
	movq	$0, 80(%rbx)
.Ltmp209:
	movl	$16, %edi
	callq	_Znam
.Ltmp210:
# BB#1:
	movq	%rax, 72(%rbx)
	movl	$0, (%rax)
	movl	$4, 84(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 96(%rbx)
.Ltmp212:
	movl	$16, %edi
	callq	_Znam
.Ltmp213:
# BB#2:
	movq	%rax, 96(%rbx)
	movl	$0, (%rax)
	movl	$4, 108(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 112(%rbx)
.Ltmp215:
	movl	$16, %edi
	callq	_Znam
.Ltmp216:
# BB#3:
	movq	%rax, 112(%rbx)
	movl	$0, (%rax)
	movl	$4, 124(%rbx)
	movb	$1, 138(%rbx)
	movb	$1, 139(%rbx)
	movb	$1, 140(%rbx)
	movq	$0, 208(%rbx)
	movq	$0, 224(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 240(%rbx)
	movq	$8, 256(%rbx)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 232(%rbx)
	movb	$0, 267(%rbx)
	movq	$0, 272(%rbx)
.Ltmp218:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp219:
# BB#4:
.Ltmp220:
	movq	%r15, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp221:
# BB#5:
	movq	%r15, 296(%rbx)
	movq	(%r15), %rax
.Ltmp223:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp224:
# BB#6:                                 # %.noexc20
	movq	272(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#7:
	movq	(%rdi), %rax
.Ltmp225:
	callq	*16(%rax)
.Ltmp226:
.LBB2_8:
	movq	%r15, 272(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB2_24:
.Ltmp222:
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB2_25
.LBB2_20:
.Ltmp217:
	movq	%rax, %r14
	jmp	.LBB2_21
.LBB2_17:
.Ltmp214:
	movq	%rax, %r14
	jmp	.LBB2_18
.LBB2_9:
.Ltmp211:
	movq	%rax, %r14
	jmp	.LBB2_10
.LBB2_23:
.Ltmp227:
	movq	%rax, %r14
.LBB2_25:
	movq	272(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_27
# BB#26:
	movq	(%rdi), %rax
.Ltmp228:
	callq	*16(%rax)
.Ltmp229:
.LBB2_27:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit23
	leaq	232(%rbx), %r15
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%r15)
.Ltmp230:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp231:
# BB#28:
.Ltmp236:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp237:
# BB#29:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	224(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_31
# BB#30:
	movq	(%rdi), %rax
.Ltmp238:
	callq	*16(%rax)
.Ltmp239:
.LBB2_31:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit18
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_33
# BB#32:
	movq	(%rdi), %rax
.Ltmp240:
	callq	*16(%rax)
.Ltmp241:
.LBB2_33:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	leaq	112(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB2_21
# BB#34:
	callq	_ZdaPv
.LBB2_21:                               # %_ZN11CStringBaseIwED2Ev.exit10
	leaq	96(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB2_18
# BB#22:
	callq	_ZdaPv
.LBB2_18:                               # %_ZN11CStringBaseIwED2Ev.exit9
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_10
# BB#19:
	callq	_ZdaPv
.LBB2_10:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_12
# BB#11:
	movq	(%rdi), %rax
.Ltmp242:
	callq	*16(%rax)
.Ltmp243:
.LBB2_12:                               # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_14
# BB#13:
	movq	(%rdi), %rax
.Ltmp244:
	callq	*16(%rax)
.Ltmp245:
.LBB2_14:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	addq	$48, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_16
# BB#15:
	movq	(%rdi), %rax
.Ltmp246:
	callq	*16(%rax)
.Ltmp247:
.LBB2_16:                               # %_ZN9CMyComPtrI29IFolderArchiveExtractCallbackED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_35:
.Ltmp232:
	movq	%rax, %rbx
.Ltmp233:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp234:
	jmp	.LBB2_38
.LBB2_36:
.Ltmp235:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_37:
.Ltmp248:
	movq	%rax, %rbx
.LBB2_38:                               # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN23CArchiveExtractCallbackC2Ev, .Lfunc_end2-_ZN23CArchiveExtractCallbackC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp209-.Lfunc_begin1  # >> Call Site 1 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin1  #     jumps to .Ltmp211
	.byte	0                       #   On action: cleanup
	.long	.Ltmp212-.Lfunc_begin1  # >> Call Site 2 <<
	.long	.Ltmp213-.Ltmp212       #   Call between .Ltmp212 and .Ltmp213
	.long	.Ltmp214-.Lfunc_begin1  #     jumps to .Ltmp214
	.byte	0                       #   On action: cleanup
	.long	.Ltmp215-.Lfunc_begin1  # >> Call Site 3 <<
	.long	.Ltmp216-.Ltmp215       #   Call between .Ltmp215 and .Ltmp216
	.long	.Ltmp217-.Lfunc_begin1  #     jumps to .Ltmp217
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin1  # >> Call Site 4 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp227-.Lfunc_begin1  #     jumps to .Ltmp227
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin1  # >> Call Site 5 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin1  #     jumps to .Ltmp222
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin1  # >> Call Site 6 <<
	.long	.Ltmp226-.Ltmp223       #   Call between .Ltmp223 and .Ltmp226
	.long	.Ltmp227-.Lfunc_begin1  #     jumps to .Ltmp227
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin1  # >> Call Site 7 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp248-.Lfunc_begin1  #     jumps to .Ltmp248
	.byte	1                       #   On action: 1
	.long	.Ltmp230-.Lfunc_begin1  # >> Call Site 8 <<
	.long	.Ltmp231-.Ltmp230       #   Call between .Ltmp230 and .Ltmp231
	.long	.Ltmp232-.Lfunc_begin1  #     jumps to .Ltmp232
	.byte	1                       #   On action: 1
	.long	.Ltmp236-.Lfunc_begin1  # >> Call Site 9 <<
	.long	.Ltmp247-.Ltmp236       #   Call between .Ltmp236 and .Ltmp247
	.long	.Ltmp248-.Lfunc_begin1  #     jumps to .Ltmp248
	.byte	1                       #   On action: 1
	.long	.Ltmp247-.Lfunc_begin1  # >> Call Site 10 <<
	.long	.Ltmp233-.Ltmp247       #   Call between .Ltmp247 and .Ltmp233
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp233-.Lfunc_begin1  # >> Call Site 11 <<
	.long	.Ltmp234-.Ltmp233       #   Call between .Ltmp233 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin1  #     jumps to .Ltmp235
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIiEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIiEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIiEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiEC2ERKS0_,@function
_ZN13CRecordVectorIiEC2ERKS0_:          # @_ZN13CRecordVectorIiEC2ERKS0_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi144:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi145:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi147:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 48
.Lcfi149:
	.cfi_offset %rbx, -48
.Lcfi150:
	.cfi_offset %r12, -40
.Lcfi151:
	.cfi_offset %r14, -32
.Lcfi152:
	.cfi_offset %r15, -24
.Lcfi153:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$4, 24(%r12)
	movq	$_ZTV13CRecordVectorIiE+16, (%r12)
.Ltmp249:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp250:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp251:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp252:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB3_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movl	(%rax,%rbx,4), %ebp
.Ltmp254:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp255:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB3_4
.LBB3_6:                                # %_ZN13CRecordVectorIiEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_8:                                # %.loopexit.split-lp
.Ltmp253:
	jmp	.LBB3_9
.LBB3_7:                                # %.loopexit
.Ltmp256:
.LBB3_9:
	movq	%rax, %r14
.Ltmp257:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp258:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_11:
.Ltmp259:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN13CRecordVectorIiEC2ERKS0_, .Lfunc_end3-_ZN13CRecordVectorIiEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp249-.Lfunc_begin2  # >> Call Site 1 <<
	.long	.Ltmp252-.Ltmp249       #   Call between .Ltmp249 and .Ltmp252
	.long	.Ltmp253-.Lfunc_begin2  #     jumps to .Ltmp253
	.byte	0                       #   On action: cleanup
	.long	.Ltmp254-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp255-.Ltmp254       #   Call between .Ltmp254 and .Ltmp255
	.long	.Ltmp256-.Lfunc_begin2  #     jumps to .Ltmp256
	.byte	0                       #   On action: cleanup
	.long	.Ltmp257-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp259-.Lfunc_begin2  #     jumps to .Ltmp259
	.byte	1                       #   On action: 1
	.long	.Ltmp258-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp258    #   Call between .Ltmp258 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12CArchiveLinkD2Ev,"axG",@progbits,_ZN12CArchiveLinkD2Ev,comdat
	.weak	_ZN12CArchiveLinkD2Ev
	.p2align	4, 0x90
	.type	_ZN12CArchiveLinkD2Ev,@function
_ZN12CArchiveLinkD2Ev:                  # @_ZN12CArchiveLinkD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 32
.Lcfi157:
	.cfi_offset %rbx, -32
.Lcfi158:
	.cfi_offset %r14, -24
.Lcfi159:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
.Ltmp260:
	callq	_ZN12CArchiveLink7ReleaseEv
.Ltmp261:
# BB#1:
	leaq	32(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 32(%r15)
.Ltmp271:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp272:
# BB#2:
.Ltmp277:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp278:
# BB#3:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	$_ZTV13CObjectVectorI4CArcE+16, (%r15)
.Ltmp289:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp290:
# BB#4:                                 # %_ZN13CObjectVectorI4CArcED2Ev.exit
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB4_7:
.Ltmp291:
	movq	%rax, %r14
.Ltmp292:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp293:
	jmp	.LBB4_8
.LBB4_9:
.Ltmp294:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_14:
.Ltmp279:
	movq	%rax, %r14
	jmp	.LBB4_15
.LBB4_5:
.Ltmp273:
	movq	%rax, %r14
.Ltmp274:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp275:
	jmp	.LBB4_15
.LBB4_6:
.Ltmp276:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_10:
.Ltmp262:
	movq	%rax, %r14
	leaq	32(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 32(%r15)
.Ltmp263:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp264:
# BB#11:
.Ltmp269:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp270:
.LBB4_15:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit5
	movq	$_ZTV13CObjectVectorI4CArcE+16, (%r15)
.Ltmp280:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp281:
# BB#16:
.Ltmp286:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp287:
.LBB4_8:                                # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_12:
.Ltmp265:
	movq	%rax, %r14
.Ltmp266:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp267:
	jmp	.LBB4_20
.LBB4_13:
.Ltmp268:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_17:
.Ltmp282:
	movq	%rax, %r14
.Ltmp283:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp284:
	jmp	.LBB4_20
.LBB4_18:
.Ltmp285:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_19:
.Ltmp288:
	movq	%rax, %r14
.LBB4_20:                               # %.body3
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN12CArchiveLinkD2Ev, .Lfunc_end4-_ZN12CArchiveLinkD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\277\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Ltmp260-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin3  #     jumps to .Ltmp262
	.byte	0                       #   On action: cleanup
	.long	.Ltmp271-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp272-.Ltmp271       #   Call between .Ltmp271 and .Ltmp272
	.long	.Ltmp273-.Lfunc_begin3  #     jumps to .Ltmp273
	.byte	0                       #   On action: cleanup
	.long	.Ltmp277-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp278-.Ltmp277       #   Call between .Ltmp277 and .Ltmp278
	.long	.Ltmp279-.Lfunc_begin3  #     jumps to .Ltmp279
	.byte	0                       #   On action: cleanup
	.long	.Ltmp289-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp290-.Ltmp289       #   Call between .Ltmp289 and .Ltmp290
	.long	.Ltmp291-.Lfunc_begin3  #     jumps to .Ltmp291
	.byte	0                       #   On action: cleanup
	.long	.Ltmp290-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp292-.Ltmp290       #   Call between .Ltmp290 and .Ltmp292
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp292-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp293-.Ltmp292       #   Call between .Ltmp292 and .Ltmp293
	.long	.Ltmp294-.Lfunc_begin3  #     jumps to .Ltmp294
	.byte	1                       #   On action: 1
	.long	.Ltmp274-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp275-.Ltmp274       #   Call between .Ltmp274 and .Ltmp275
	.long	.Ltmp276-.Lfunc_begin3  #     jumps to .Ltmp276
	.byte	1                       #   On action: 1
	.long	.Ltmp263-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin3  #     jumps to .Ltmp265
	.byte	1                       #   On action: 1
	.long	.Ltmp269-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp270-.Ltmp269       #   Call between .Ltmp269 and .Ltmp270
	.long	.Ltmp288-.Lfunc_begin3  #     jumps to .Ltmp288
	.byte	1                       #   On action: 1
	.long	.Ltmp280-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Ltmp281-.Ltmp280       #   Call between .Ltmp280 and .Ltmp281
	.long	.Ltmp282-.Lfunc_begin3  #     jumps to .Ltmp282
	.byte	1                       #   On action: 1
	.long	.Ltmp286-.Lfunc_begin3  # >> Call Site 11 <<
	.long	.Ltmp287-.Ltmp286       #   Call between .Ltmp286 and .Ltmp287
	.long	.Ltmp288-.Lfunc_begin3  #     jumps to .Ltmp288
	.byte	1                       #   On action: 1
	.long	.Ltmp287-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp266-.Ltmp287       #   Call between .Ltmp287 and .Ltmp266
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp266-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp268-.Lfunc_begin3  #     jumps to .Ltmp268
	.byte	1                       #   On action: 1
	.long	.Ltmp283-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp284-.Ltmp283       #   Call between .Ltmp283 and .Ltmp284
	.long	.Ltmp285-.Lfunc_begin3  #     jumps to .Ltmp285
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi162:
	.cfi_def_cfa_offset 32
.Lcfi163:
	.cfi_offset %rbx, -24
.Lcfi164:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp295:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp296:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB5_2:
.Ltmp297:
	movq	%rax, %r14
.Ltmp298:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp299:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_4:
.Ltmp300:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end5-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp295-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp296-.Ltmp295       #   Call between .Ltmp295 and .Ltmp296
	.long	.Ltmp297-.Lfunc_begin4  #     jumps to .Ltmp297
	.byte	0                       #   On action: cleanup
	.long	.Ltmp296-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp298-.Ltmp296       #   Call between .Ltmp296 and .Ltmp298
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp298-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp299-.Ltmp298       #   Call between .Ltmp298 and .Ltmp299
	.long	.Ltmp300-.Lfunc_begin4  #     jumps to .Ltmp300
	.byte	1                       #   On action: 1
	.long	.Ltmp299-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp299    #   Call between .Ltmp299 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi167:
	.cfi_def_cfa_offset 32
.Lcfi168:
	.cfi_offset %rbx, -24
.Lcfi169:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp301:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp302:
# BB#1:
.Ltmp307:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp308:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_5:
.Ltmp309:
	movq	%rax, %r14
	jmp	.LBB6_6
.LBB6_3:
.Ltmp303:
	movq	%rax, %r14
.Ltmp304:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp305:
.LBB6_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_4:
.Ltmp306:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end6-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp301-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp303-.Lfunc_begin5  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp308-.Ltmp307       #   Call between .Ltmp307 and .Ltmp308
	.long	.Ltmp309-.Lfunc_begin5  #     jumps to .Ltmp309
	.byte	0                       #   On action: cleanup
	.long	.Ltmp304-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp305-.Ltmp304       #   Call between .Ltmp304 and .Ltmp305
	.long	.Ltmp306-.Lfunc_begin5  #     jumps to .Ltmp306
	.byte	1                       #   On action: 1
	.long	.Ltmp305-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp305    #   Call between .Ltmp305 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi173:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi174:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi175:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi176:
	.cfi_def_cfa_offset 64
.Lcfi177:
	.cfi_offset %rbx, -56
.Lcfi178:
	.cfi_offset %r12, -48
.Lcfi179:
	.cfi_offset %r13, -40
.Lcfi180:
	.cfi_offset %r14, -32
.Lcfi181:
	.cfi_offset %r15, -24
.Lcfi182:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB7_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB7_6
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	callq	_ZdaPv
.LBB7_5:                                # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB7_6:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB7_2
.LBB7_7:                                # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end7:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end7-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI4CArcED2Ev,"axG",@progbits,_ZN13CObjectVectorI4CArcED2Ev,comdat
	.weak	_ZN13CObjectVectorI4CArcED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI4CArcED2Ev,@function
_ZN13CObjectVectorI4CArcED2Ev:          # @_ZN13CObjectVectorI4CArcED2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi183:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi184:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi185:
	.cfi_def_cfa_offset 32
.Lcfi186:
	.cfi_offset %rbx, -24
.Lcfi187:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI4CArcE+16, (%rbx)
.Ltmp310:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp311:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB8_2:
.Ltmp312:
	movq	%rax, %r14
.Ltmp313:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp314:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_4:
.Ltmp315:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN13CObjectVectorI4CArcED2Ev, .Lfunc_end8-_ZN13CObjectVectorI4CArcED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp310-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp311-.Ltmp310       #   Call between .Ltmp310 and .Ltmp311
	.long	.Ltmp312-.Lfunc_begin6  #     jumps to .Ltmp312
	.byte	0                       #   On action: cleanup
	.long	.Ltmp311-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp313-.Ltmp311       #   Call between .Ltmp311 and .Ltmp313
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp313-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp314-.Ltmp313       #   Call between .Ltmp313 and .Ltmp314
	.long	.Ltmp315-.Lfunc_begin6  #     jumps to .Ltmp315
	.byte	1                       #   On action: 1
	.long	.Ltmp314-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp314    #   Call between .Ltmp314 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI4CArcED0Ev,"axG",@progbits,_ZN13CObjectVectorI4CArcED0Ev,comdat
	.weak	_ZN13CObjectVectorI4CArcED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI4CArcED0Ev,@function
_ZN13CObjectVectorI4CArcED0Ev:          # @_ZN13CObjectVectorI4CArcED0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi188:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi189:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi190:
	.cfi_def_cfa_offset 32
.Lcfi191:
	.cfi_offset %rbx, -24
.Lcfi192:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI4CArcE+16, (%rbx)
.Ltmp316:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp317:
# BB#1:
.Ltmp322:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp323:
# BB#2:                                 # %_ZN13CObjectVectorI4CArcED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB9_5:
.Ltmp324:
	movq	%rax, %r14
	jmp	.LBB9_6
.LBB9_3:
.Ltmp318:
	movq	%rax, %r14
.Ltmp319:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp320:
.LBB9_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_4:
.Ltmp321:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN13CObjectVectorI4CArcED0Ev, .Lfunc_end9-_ZN13CObjectVectorI4CArcED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp316-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp317-.Ltmp316       #   Call between .Ltmp316 and .Ltmp317
	.long	.Ltmp318-.Lfunc_begin7  #     jumps to .Ltmp318
	.byte	0                       #   On action: cleanup
	.long	.Ltmp322-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp323-.Ltmp322       #   Call between .Ltmp322 and .Ltmp323
	.long	.Ltmp324-.Lfunc_begin7  #     jumps to .Ltmp324
	.byte	0                       #   On action: cleanup
	.long	.Ltmp319-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp320-.Ltmp319       #   Call between .Ltmp319 and .Ltmp320
	.long	.Ltmp321-.Lfunc_begin7  #     jumps to .Ltmp321
	.byte	1                       #   On action: 1
	.long	.Ltmp320-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp320    #   Call between .Ltmp320 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI4CArcE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI4CArcE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI4CArcE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI4CArcE6DeleteEii,@function
_ZN13CObjectVectorI4CArcE6DeleteEii:    # @_ZN13CObjectVectorI4CArcE6DeleteEii
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi195:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi196:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi197:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi198:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi199:
	.cfi_def_cfa_offset 64
.Lcfi200:
	.cfi_offset %rbx, -56
.Lcfi201:
	.cfi_offset %r12, -48
.Lcfi202:
	.cfi_offset %r13, -40
.Lcfi203:
	.cfi_offset %r14, -32
.Lcfi204:
	.cfi_offset %r15, -24
.Lcfi205:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB10_13
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB10_12
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	callq	_ZdaPv
.LBB10_5:                               # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_7
# BB#6:                                 #   in Loop: Header=BB10_2 Depth=1
	callq	_ZdaPv
.LBB10_7:                               # %_ZN11CStringBaseIwED2Ev.exit1.i
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	8(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_9
# BB#8:                                 #   in Loop: Header=BB10_2 Depth=1
	callq	_ZdaPv
.LBB10_9:                               # %_ZN11CStringBaseIwED2Ev.exit2.i
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_11
# BB#10:                                #   in Loop: Header=BB10_2 Depth=1
	movq	(%rdi), %rax
.Ltmp325:
	callq	*16(%rax)
.Ltmp326:
.LBB10_11:                              # %_ZN4CArcD2Ev.exit
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB10_12:                              #   in Loop: Header=BB10_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB10_2
.LBB10_13:                              # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB10_14:
.Ltmp327:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN13CObjectVectorI4CArcE6DeleteEii, .Lfunc_end10-_ZN13CObjectVectorI4CArcE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp325-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp326-.Ltmp325       #   Call between .Ltmp325 and .Ltmp326
	.long	.Ltmp327-.Lfunc_begin8  #     jumps to .Ltmp327
	.byte	0                       #   On action: cleanup
	.long	.Ltmp326-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end10-.Ltmp326   #   Call between .Ltmp326 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIwE7ReplaceERKS0_S2_,"axG",@progbits,_ZN11CStringBaseIwE7ReplaceERKS0_S2_,comdat
	.weak	_ZN11CStringBaseIwE7ReplaceERKS0_S2_
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE7ReplaceERKS0_S2_,@function
_ZN11CStringBaseIwE7ReplaceERKS0_S2_:   # @_ZN11CStringBaseIwE7ReplaceERKS0_S2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi206:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi207:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi208:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi209:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi210:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi211:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi212:
	.cfi_def_cfa_offset 80
.Lcfi213:
	.cfi_offset %rbx, -56
.Lcfi214:
	.cfi_offset %r12, -48
.Lcfi215:
	.cfi_offset %r13, -40
.Lcfi216:
	.cfi_offset %r14, -32
.Lcfi217:
	.cfi_offset %r15, -24
.Lcfi218:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorl	%r12d, %r12d
	cmpl	$0, 8(%r15)
	je	.LBB11_12
# BB#1:
	movq	(%r15), %rdi
	movq	(%rbp), %rsi
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	je	.LBB11_12
# BB#2:
	movl	8(%rbx), %r14d
	testl	%r14d, %r14d
	jle	.LBB11_12
# BB#3:                                 # %.lr.ph
	movl	8(%r15), %eax
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	8(%rbp), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	xorl	%r13d, %r13d
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%eax, %r8d
	xorl	%r12d, %r12d
	testl	%r8d, %r8d
	jne	.LBB11_5
	jmp	.LBB11_14
	.p2align	4, 0x90
.LBB11_35:                              # %_ZN11CStringBaseIwE6InsertEiRKS0_.exit._crit_edge
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	8(%r15), %r8d
	testl	%r8d, %r8d
	je	.LBB11_14
.LBB11_5:                               # %.preheader.lr.ph.i
	jle	.LBB11_12
# BB#6:                                 # %.preheader.us.preheader.i
	movq	(%r15), %rcx
	movslq	%r8d, %rdx
	movslq	%r13d, %r13
	movslq	%r14d, %rsi
	.p2align	4, 0x90
.LBB11_7:                               # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_8 Depth 2
	movq	%r13, %rbp
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB11_8:                               #   Parent Loop BB11_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %rbp
	jge	.LBB11_10
# BB#9:                                 #   in Loop: Header=BB11_8 Depth=2
	movq	(%rbx), %rax
	movl	(%rax,%rbp,4), %eax
	cmpl	(%rcx,%rdi,4), %eax
	jne	.LBB11_10
# BB#13:                                #   in Loop: Header=BB11_8 Depth=2
	incq	%rdi
	incq	%rbp
	cmpq	%rdx, %rdi
	jl	.LBB11_8
.LBB11_10:                              # %.critedge.us.i
                                        #   in Loop: Header=BB11_7 Depth=1
	cmpl	%r8d, %edi
	je	.LBB11_14
# BB#11:                                #   in Loop: Header=BB11_7 Depth=1
	incq	%r13
	cmpq	%rsi, %r13
	jl	.LBB11_7
	jmp	.LBB11_12
	.p2align	4, 0x90
.LBB11_14:                              # %_ZNK11CStringBaseIwE4FindERKS0_i.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_31 Depth 2
                                        #     Child Loop BB11_23 Depth 2
                                        #     Child Loop BB11_26 Depth 2
	testl	%r13d, %r13d
	js	.LBB11_12
# BB#15:                                #   in Loop: Header=BB11_14 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	(%r13,%rcx), %eax
	movl	%r14d, %ebp
	subl	%r13d, %ebp
	cmpl	%r14d, %eax
	cmovlel	%ecx, %ebp
	testl	%ebp, %ebp
	jle	.LBB11_17
# BB#16:                                #   in Loop: Header=BB11_14 Depth=1
	leal	(%rbp,%r13), %eax
	movq	(%rbx), %rcx
	movslq	%r13d, %rdx
	leaq	(%rcx,%rdx,4), %rdi
	cltq
	leaq	(%rcx,%rax,4), %rsi
	incl	%r14d
	subl	%eax, %r14d
	movslq	%r14d, %rdx
	shlq	$2, %rdx
	callq	memmove
	movl	8(%rbx), %r14d
	subl	%ebp, %r14d
	movl	%r14d, 8(%rbx)
.LBB11_17:                              # %_ZN11CStringBaseIwE6DeleteEii.exit
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	8(%rax), %ebp
	testq	%rbp, %rbp
	je	.LBB11_34
# BB#18:                                # %_ZN11CStringBaseIwE11InsertSpaceERii.exit.i
                                        #   in Loop: Header=BB11_14 Depth=1
	cmpl	%r13d, %r14d
	cmovgl	%r13d, %r14d
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
	leal	(%r14,%rbp), %eax
	movq	(%rbx), %rcx
	cltq
	leaq	(%rcx,%rax,4), %rdi
	movslq	%r14d, %r14
	leaq	(%rcx,%r14,4), %rsi
	movl	$1, %eax
	subl	%r14d, %eax
	addl	8(%rbx), %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memmove
	testl	%ebp, %ebp
	jle	.LBB11_33
# BB#19:                                # %.lr.ph.i
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rbx), %r8
	cmpl	$7, %ebp
	jbe	.LBB11_20
# BB#27:                                # %min.iters.checked
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	%ebp, %r9d
	andl	$7, %r9d
	movq	%rbp, %rdx
	subq	%r9, %rdx
	je	.LBB11_20
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB11_14 Depth=1
	leaq	(%r8,%r14,4), %rcx
	leaq	(%rax,%rbp,4), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB11_30
# BB#29:                                # %vector.memcheck
                                        #   in Loop: Header=BB11_14 Depth=1
	leaq	(%r14,%rbp), %rcx
	leaq	(%r8,%rcx,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB11_30
.LBB11_20:                              #   in Loop: Header=BB11_14 Depth=1
	xorl	%edx, %edx
.LBB11_21:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	%ebp, %edi
	subl	%edx, %edi
	leaq	-1(%rbp), %r9
	subq	%rdx, %r9
	andq	$3, %rdi
	je	.LBB11_24
# BB#22:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB11_14 Depth=1
	leaq	(%r8,%r14,4), %rcx
	negq	%rdi
	.p2align	4, 0x90
.LBB11_23:                              # %scalar.ph.prol
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rdx,4), %esi
	movl	%esi, (%rcx,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB11_23
.LBB11_24:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB11_14 Depth=1
	cmpq	$3, %r9
	jb	.LBB11_33
# BB#25:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	addq	%rdx, %r14
	leaq	12(%r8,%r14,4), %rcx
	leaq	12(%rax,%rdx,4), %rax
	.p2align	4, 0x90
.LBB11_26:                              # %scalar.ph
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rax), %edx
	movl	%edx, -12(%rcx)
	movl	-8(%rax), %edx
	movl	%edx, -8(%rcx)
	movl	-4(%rax), %edx
	movl	%edx, -4(%rcx)
	movl	(%rax), %edx
	movl	%edx, (%rcx)
	addq	$16, %rcx
	addq	$16, %rax
	addq	$-4, %rsi
	jne	.LBB11_26
.LBB11_33:                              # %._crit_edge.i
                                        #   in Loop: Header=BB11_14 Depth=1
	addl	8(%rbx), %ebp
	movl	%ebp, 8(%rbx)
	movl	%ebp, %r14d
.LBB11_34:                              # %_ZN11CStringBaseIwE6InsertEiRKS0_.exit
                                        #   in Loop: Header=BB11_14 Depth=1
	addl	4(%rsp), %r13d          # 4-byte Folded Reload
	incl	%r12d
	cmpl	%r14d, %r13d
	jl	.LBB11_35
	jmp	.LBB11_12
.LBB11_30:                              # %vector.body.preheader
                                        #   in Loop: Header=BB11_14 Depth=1
	leaq	16(%rax), %rdi
	leaq	16(%r8,%r14,4), %rcx
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB11_31:                              # %vector.body
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	addq	$32, %rdi
	addq	$32, %rcx
	addq	$-8, %rsi
	jne	.LBB11_31
# BB#32:                                # %middle.block
                                        #   in Loop: Header=BB11_14 Depth=1
	testl	%r9d, %r9d
	jne	.LBB11_21
	jmp	.LBB11_33
.LBB11_12:                              # %_ZNK11CStringBaseIwE4FindERKS0_i.exit.thread
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN11CStringBaseIwE7ReplaceERKS0_S2_, .Lfunc_end11-_ZN11CStringBaseIwE7ReplaceERKS0_S2_
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi219:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi220:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi221:
	.cfi_def_cfa_offset 32
.Lcfi222:
	.cfi_offset %rbx, -24
.Lcfi223:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp328:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp329:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB12_2:
.Ltmp330:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end12-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp328-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp329-.Ltmp328       #   Call between .Ltmp328 and .Ltmp329
	.long	.Ltmp330-.Lfunc_begin9  #     jumps to .Ltmp330
	.byte	0                       #   On action: cleanup
	.long	.Ltmp329-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp329   #   Call between .Ltmp329 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi224:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi225:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi227:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi228:
	.cfi_def_cfa_offset 48
.Lcfi229:
	.cfi_offset %rbx, -48
.Lcfi230:
	.cfi_offset %r12, -40
.Lcfi231:
	.cfi_offset %r14, -32
.Lcfi232:
	.cfi_offset %r15, -24
.Lcfi233:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB13_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB13_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB13_3:                               # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB13_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB13_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB13_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB13_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB13_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB13_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB13_17
.LBB13_7:
	xorl	%ecx, %ecx
.LBB13_8:                               # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB13_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB13_10:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB13_10
.LBB13_11:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB13_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB13_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB13_13
	jmp	.LBB13_26
.LBB13_25:                              # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB13_27
.LBB13_26:                              # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB13_27:                              # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB13_28:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_17:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB13_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB13_20
	jmp	.LBB13_21
.LBB13_18:
	xorl	%ebx, %ebx
.LBB13_21:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB13_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB13_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB13_23
.LBB13_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB13_8
	jmp	.LBB13_26
.Lfunc_end13:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end13-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi234:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi236:
	.cfi_def_cfa_offset 32
.Lcfi237:
	.cfi_offset %rbx, -24
.Lcfi238:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp331:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp332:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB14_2:
.Ltmp333:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end14-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp331-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp332-.Ltmp331       #   Call between .Ltmp331 and .Ltmp332
	.long	.Ltmp333-.Lfunc_begin10 #     jumps to .Ltmp333
	.byte	0                       #   On action: cleanup
	.long	.Ltmp332-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end14-.Ltmp332   #   Call between .Ltmp332 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIiED0Ev,"axG",@progbits,_ZN13CRecordVectorIiED0Ev,comdat
	.weak	_ZN13CRecordVectorIiED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiED0Ev,@function
_ZN13CRecordVectorIiED0Ev:              # @_ZN13CRecordVectorIiED0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi239:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi240:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi241:
	.cfi_def_cfa_offset 32
.Lcfi242:
	.cfi_offset %rbx, -24
.Lcfi243:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp334:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp335:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB15_2:
.Ltmp336:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZN13CRecordVectorIiED0Ev, .Lfunc_end15-_ZN13CRecordVectorIiED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp334-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp335-.Ltmp334       #   Call between .Ltmp334 and .Ltmp335
	.long	.Ltmp336-.Lfunc_begin11 #     jumps to .Ltmp336
	.byte	0                       #   On action: cleanup
	.long	.Ltmp335-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Lfunc_end15-.Ltmp335   #   Call between .Ltmp335 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%rbp
.Lcfi244:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi245:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi246:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi247:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi248:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi249:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi250:
	.cfi_def_cfa_offset 64
.Lcfi251:
	.cfi_offset %rbx, -56
.Lcfi252:
	.cfi_offset %r12, -48
.Lcfi253:
	.cfi_offset %r13, -40
.Lcfi254:
	.cfi_offset %r14, -32
.Lcfi255:
	.cfi_offset %r15, -24
.Lcfi256:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB16_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB16_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB16_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB16_5
.LBB16_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB16_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp337:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp338:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB16_35
# BB#12:
	movq	%rbx, %r13
.LBB16_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB16_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB16_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB16_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB16_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB16_15
.LBB16_14:
	xorl	%esi, %esi
.LBB16_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB16_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB16_16
.LBB16_29:
	movq	%r13, %rbx
.LBB16_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp339:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp340:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB16_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB16_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB16_8
.LBB16_3:
	xorl	%eax, %eax
.LBB16_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB16_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB16_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB16_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB16_30
.LBB16_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB16_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB16_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB16_24
	jmp	.LBB16_25
.LBB16_22:
	xorl	%ecx, %ecx
.LBB16_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB16_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB16_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB16_27
.LBB16_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB16_15
	jmp	.LBB16_29
.LBB16_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp341:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end16-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin12-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp337-.Lfunc_begin12 #   Call between .Lfunc_begin12 and .Ltmp337
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp337-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp340-.Ltmp337       #   Call between .Ltmp337 and .Ltmp340
	.long	.Ltmp341-.Lfunc_begin12 #     jumps to .Ltmp341
	.byte	0                       #   On action: cleanup
	.long	.Ltmp340-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Lfunc_end16-.Ltmp340   #   Call between .Ltmp340 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"there is no such archive"
	.size	.L.str, 25

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"can't decompress folder"
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.2:
	.long	48                      # 0x30
	.long	48                      # 0x30
	.long	49                      # 0x31
	.long	0                       # 0x0
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
	.p2align	2
.L.str.3:
	.long	114                     # 0x72
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	0                       # 0x0
	.size	.L.str.3, 16

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24

	.type	_ZTV13CObjectVectorI4CArcE,@object # @_ZTV13CObjectVectorI4CArcE
	.section	.rodata._ZTV13CObjectVectorI4CArcE,"aG",@progbits,_ZTV13CObjectVectorI4CArcE,comdat
	.weak	_ZTV13CObjectVectorI4CArcE
	.p2align	3
_ZTV13CObjectVectorI4CArcE:
	.quad	0
	.quad	_ZTI13CObjectVectorI4CArcE
	.quad	_ZN13CObjectVectorI4CArcED2Ev
	.quad	_ZN13CObjectVectorI4CArcED0Ev
	.quad	_ZN13CObjectVectorI4CArcE6DeleteEii
	.size	_ZTV13CObjectVectorI4CArcE, 40

	.type	_ZTS13CObjectVectorI4CArcE,@object # @_ZTS13CObjectVectorI4CArcE
	.section	.rodata._ZTS13CObjectVectorI4CArcE,"aG",@progbits,_ZTS13CObjectVectorI4CArcE,comdat
	.weak	_ZTS13CObjectVectorI4CArcE
	.p2align	4
_ZTS13CObjectVectorI4CArcE:
	.asciz	"13CObjectVectorI4CArcE"
	.size	_ZTS13CObjectVectorI4CArcE, 23

	.type	_ZTI13CObjectVectorI4CArcE,@object # @_ZTI13CObjectVectorI4CArcE
	.section	.rodata._ZTI13CObjectVectorI4CArcE,"aG",@progbits,_ZTI13CObjectVectorI4CArcE,comdat
	.weak	_ZTI13CObjectVectorI4CArcE
	.p2align	4
_ZTI13CObjectVectorI4CArcE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI4CArcE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI4CArcE, 24

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV13CRecordVectorIiE,@object # @_ZTV13CRecordVectorIiE
	.section	.rodata._ZTV13CRecordVectorIiE,"aG",@progbits,_ZTV13CRecordVectorIiE,comdat
	.weak	_ZTV13CRecordVectorIiE
	.p2align	3
_ZTV13CRecordVectorIiE:
	.quad	0
	.quad	_ZTI13CRecordVectorIiE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIiED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIiE, 40

	.type	_ZTS13CRecordVectorIiE,@object # @_ZTS13CRecordVectorIiE
	.section	.rodata._ZTS13CRecordVectorIiE,"aG",@progbits,_ZTS13CRecordVectorIiE,comdat
	.weak	_ZTS13CRecordVectorIiE
	.p2align	4
_ZTS13CRecordVectorIiE:
	.asciz	"13CRecordVectorIiE"
	.size	_ZTS13CRecordVectorIiE, 19

	.type	_ZTI13CRecordVectorIiE,@object # @_ZTI13CRecordVectorIiE
	.section	.rodata._ZTI13CRecordVectorIiE,"aG",@progbits,_ZTI13CRecordVectorIiE,comdat
	.weak	_ZTI13CRecordVectorIiE
	.p2align	4
_ZTI13CRecordVectorIiE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIiE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIiE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
