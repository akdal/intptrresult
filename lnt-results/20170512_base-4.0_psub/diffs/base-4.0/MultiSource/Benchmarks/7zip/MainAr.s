	.text
	.file	"MainAr.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 64
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movq	$g_StdOut, g_StdStream(%rip)
	leaq	16(%rsp), %rdi
	callq	_ZN13NConsoleClose18CCtrlHandlerSetterC1Ev
.Ltmp0:
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	_Z5Main2iPPKc
	movl	%eax, %ebx
.Ltmp1:
.LBB0_43:
	leaq	16(%rsp), %rdi
	callq	_ZN13NConsoleClose18CCtrlHandlerSetterD1Ev
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB0_1:
.Ltmp2:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	cmpl	$10, %ebx
	jne	.LBB0_4
# BB#2:
	callq	__cxa_begin_catch
	movq	g_StdStream(%rip), %rdi
.Ltmp101:
	movl	$.L.str.4, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp102:
# BB#3:
	movl	$8, %ebx
.Ltmp106:
	callq	__cxa_end_catch
.Ltmp107:
	jmp	.LBB0_43
.LBB0_4:
	cmpl	$9, %ebx
	jne	.LBB0_8
# BB#5:
	callq	__cxa_begin_catch
	movq	g_StdStream(%rip), %rdi
.Ltmp92:
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp93:
# BB#6:
.Ltmp94:
	movl	$.L.str.5, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp95:
# BB#7:
	movl	$255, %ebx
.Ltmp99:
	callq	__cxa_end_catch
.Ltmp100:
	jmp	.LBB0_43
.LBB0_8:
	cmpl	$8, %ebx
	jne	.LBB0_13
# BB#9:
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	movq	g_StdStream(%rip), %rdi
.Ltmp81:
	movl	$.L.str.3, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp82:
# BB#10:
	movq	(%rbx), %rsi
.Ltmp83:
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp84:
# BB#11:
.Ltmp85:
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp86:
# BB#12:
	movl	$7, %ebx
.Ltmp90:
	callq	__cxa_end_catch
.Ltmp91:
	jmp	.LBB0_43
.LBB0_13:
	cmpl	$7, %ebx
	jne	.LBB0_17
# BB#14:
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	movl	(%rbx), %eax
	cmpl	$-2147467260, %eax      # imm = 0x80004004
	je	.LBB0_51
# BB#15:
	cmpl	$-2147024882, %eax      # imm = 0x8007000E
	jne	.LBB0_54
# BB#16:
	movl	$8, %ebx
	movq	g_StdStream(%rip), %rdi
.Ltmp57:
	movl	$.L.str.4, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp58:
	jmp	.LBB0_53
.LBB0_17:
	cmpl	$6, %ebx
	jne	.LBB0_22
# BB#18:
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	movq	g_StdStream(%rip), %rdi
.Ltmp46:
	movl	$.L.str.2, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp47:
# BB#19:
	movl	(%rbx), %esi
.Ltmp48:
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEi
.Ltmp49:
# BB#20:
.Ltmp50:
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp51:
# BB#21:
	movl	(%rbx), %ebx
	callq	__cxa_end_catch
	jmp	.LBB0_43
.LBB0_22:
	cmpl	$5, %ebx
	jne	.LBB0_27
# BB#23:
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	movq	g_StdStream(%rip), %rdi
.Ltmp35:
	movl	$.L.str.3, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp36:
# BB#24:
	movq	(%rbx), %rsi
.Ltmp37:
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp38:
# BB#25:
.Ltmp39:
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp40:
# BB#26:
	movl	$2, %ebx
.Ltmp44:
	callq	__cxa_end_catch
.Ltmp45:
	jmp	.LBB0_43
.LBB0_51:
	movq	g_StdStream(%rip), %rdi
.Ltmp53:
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp54:
# BB#52:
	movl	$255, %ebx
.Ltmp55:
	movl	$.L.str.5, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp56:
	jmp	.LBB0_53
.LBB0_54:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp59:
	movl	$16, %edi
	callq	_Znam
.Ltmp60:
# BB#55:
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	$4, 12(%rsp)
	movl	(%rbx), %edi
.Ltmp62:
	movq	%rsp, %rsi
	callq	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
.Ltmp63:
# BB#56:
	movq	g_StdStream(%rip), %rdi
.Ltmp64:
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp65:
# BB#57:
.Ltmp66:
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp67:
# BB#58:
.Ltmp68:
	movl	$.L.str, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp69:
# BB#59:
.Ltmp70:
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp71:
# BB#60:
	movq	(%rsp), %rsi
.Ltmp72:
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp73:
# BB#61:
.Ltmp74:
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp75:
# BB#62:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_64
# BB#63:
	callq	_ZdaPv
.LBB0_64:                               # %_ZN11CStringBaseIwED2Ev.exit59
	movl	$2, %ebx
.LBB0_53:
.Ltmp79:
	callq	__cxa_end_catch
.Ltmp80:
	jmp	.LBB0_43
.LBB0_27:
	cmpl	$4, %ebx
	jne	.LBB0_32
# BB#28:
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	movq	g_StdStream(%rip), %rdi
.Ltmp24:
	movl	$.L.str.3, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp25:
# BB#29:
	movq	(%rbx), %rsi
.Ltmp26:
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp27:
# BB#30:
.Ltmp28:
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp29:
# BB#31:
	movl	$2, %ebx
.Ltmp33:
	callq	__cxa_end_catch
.Ltmp34:
	jmp	.LBB0_43
.LBB0_32:
	cmpl	$3, %ebx
	jne	.LBB0_37
# BB#33:
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	movq	g_StdStream(%rip), %rdi
.Ltmp17:
	movl	$.L.str.3, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp18:
# BB#34:
.Ltmp19:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp20:
# BB#35:
.Ltmp21:
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp22:
	jmp	.LBB0_36
.LBB0_37:
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB0_41
# BB#38:
	movl	(%rax), %ebx
	movq	g_StdStream(%rip), %rdi
.Ltmp10:
	movl	$.L.str.2, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp11:
# BB#39:
.Ltmp12:
	movq	%rax, %rdi
	movl	%ebx, %esi
	callq	_ZN13CStdOutStreamlsEi
.Ltmp13:
# BB#40:
.Ltmp14:
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp15:
.LBB0_36:
	callq	__cxa_end_catch
	movl	$2, %ebx
	jmp	.LBB0_43
.LBB0_41:
	movq	g_StdStream(%rip), %rdi
.Ltmp3:
	movl	$.L.str.1, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp4:
# BB#42:
	movl	$2, %ebx
.Ltmp8:
	callq	__cxa_end_catch
.Ltmp9:
	jmp	.LBB0_43
.LBB0_44:
.Ltmp5:
	movq	%rax, %rbx
.Ltmp6:
	callq	__cxa_end_catch
.Ltmp7:
	jmp	.LBB0_73
.LBB0_45:
.Ltmp16:
	jmp	.LBB0_46
.LBB0_47:
.Ltmp23:
	jmp	.LBB0_46
.LBB0_48:
.Ltmp30:
	movq	%rax, %rbx
.Ltmp31:
	callq	__cxa_end_catch
.Ltmp32:
	jmp	.LBB0_73
.LBB0_49:
.Ltmp41:
	movq	%rax, %rbx
.Ltmp42:
	callq	__cxa_end_catch
.Ltmp43:
	jmp	.LBB0_73
.LBB0_67:
.Ltmp61:
	movq	%rax, %rbx
	jmp	.LBB0_68
.LBB0_50:
.Ltmp52:
.LBB0_46:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	jmp	.LBB0_73
.LBB0_65:
.Ltmp76:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_68
# BB#66:
	callq	_ZdaPv
.LBB0_68:
.Ltmp77:
	callq	__cxa_end_catch
.Ltmp78:
	jmp	.LBB0_73
.LBB0_69:
.Ltmp87:
	movq	%rax, %rbx
.Ltmp88:
	callq	__cxa_end_catch
.Ltmp89:
	jmp	.LBB0_73
.LBB0_70:
.Ltmp96:
	movq	%rax, %rbx
.Ltmp97:
	callq	__cxa_end_catch
.Ltmp98:
	jmp	.LBB0_73
.LBB0_71:
.Ltmp103:
	movq	%rax, %rbx
.Ltmp104:
	callq	__cxa_end_catch
.Ltmp105:
	jmp	.LBB0_73
.LBB0_72:
.Ltmp108:
	movq	%rax, %rbx
.LBB0_73:
.Ltmp109:
	leaq	16(%rsp), %rdi
	callq	_ZN13NConsoleClose18CCtrlHandlerSetterD1Ev
.Ltmp110:
# BB#74:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_75:
.Ltmp111:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\206\204\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\307\003"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	19                      #   On action: 10
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp101-.Ltmp1         #   Call between .Ltmp1 and .Ltmp101
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin0  # >> Call Site 4 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin0  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp106-.Lfunc_begin0  # >> Call Site 5 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin0  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin0  # >> Call Site 6 <<
	.long	.Ltmp92-.Ltmp107        #   Call between .Ltmp107 and .Ltmp92
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp95-.Ltmp92         #   Call between .Ltmp92 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin0   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp108-.Lfunc_begin0  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin0  # >> Call Site 9 <<
	.long	.Ltmp81-.Ltmp100        #   Call between .Ltmp100 and .Ltmp81
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp86-.Ltmp81         #   Call between .Ltmp81 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin0   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp108-.Lfunc_begin0  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp57-.Ltmp91         #   Call between .Ltmp91 and .Ltmp57
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp61-.Lfunc_begin0   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp46-.Ltmp58         #   Call between .Ltmp58 and .Ltmp46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp51-.Ltmp46         #   Call between .Ltmp46 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin0   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp35-.Ltmp51         #   Call between .Ltmp51 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp40-.Ltmp35         #   Call between .Ltmp35 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin0   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp108-.Lfunc_begin0  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp60-.Ltmp53         #   Call between .Ltmp53 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin0   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp75-.Ltmp62         #   Call between .Ltmp62 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin0   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp108-.Lfunc_begin0  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp24-.Ltmp80         #   Call between .Ltmp80 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp29-.Ltmp24         #   Call between .Ltmp24 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp108-.Lfunc_begin0  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp17-.Ltmp34         #   Call between .Ltmp34 and .Ltmp17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp22-.Ltmp17         #   Call between .Ltmp17 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin0   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp10-.Ltmp22         #   Call between .Ltmp22 and .Ltmp10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp15-.Ltmp10         #   Call between .Ltmp10 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 29 <<
	.long	.Ltmp3-.Ltmp15          #   Call between .Ltmp15 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 30 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 31 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp108-.Lfunc_begin0  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 32 <<
	.long	.Ltmp43-.Ltmp6          #   Call between .Ltmp6 and .Ltmp43
	.long	.Ltmp111-.Lfunc_begin0  #     jumps to .Ltmp111
	.byte	1                       #   On action: 1
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 33 <<
	.long	.Ltmp77-.Ltmp43         #   Call between .Ltmp43 and .Ltmp77
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin0   # >> Call Site 34 <<
	.long	.Ltmp110-.Ltmp77        #   Call between .Ltmp77 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin0  #     jumps to .Ltmp111
	.byte	1                       #   On action: 1
	.long	.Ltmp110-.Lfunc_begin0  # >> Call Site 35 <<
	.long	.Lfunc_end0-.Ltmp110    #   Call between .Ltmp110 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	125                     #   Continue to action 2
	.byte	4                       # >> Action Record 4 <<
                                        #   Catch TypeInfo 4
	.byte	125                     #   Continue to action 3
	.byte	5                       # >> Action Record 5 <<
                                        #   Catch TypeInfo 5
	.byte	125                     #   Continue to action 4
	.byte	6                       # >> Action Record 6 <<
                                        #   Catch TypeInfo 6
	.byte	125                     #   Continue to action 5
	.byte	7                       # >> Action Record 7 <<
                                        #   Catch TypeInfo 7
	.byte	125                     #   Continue to action 6
	.byte	8                       # >> Action Record 8 <<
                                        #   Catch TypeInfo 8
	.byte	125                     #   Continue to action 7
	.byte	9                       # >> Action Record 9 <<
                                        #   Catch TypeInfo 9
	.byte	125                     #   Continue to action 8
	.byte	10                      # >> Action Record 10 <<
                                        #   Catch TypeInfo 10
	.byte	125                     #   Continue to action 9
                                        # >> Catch TypeInfos <<
	.long	_ZTI13CNewException     # TypeInfo 10
	.long	_ZTIN13NConsoleClose19CCtrlBreakExceptionE # TypeInfo 9
	.long	_ZTI28CArchiveCommandLineException # TypeInfo 8
	.long	_ZTI16CSystemException  # TypeInfo 7
	.long	_ZTIN9NExitCode5EEnumE  # TypeInfo 6
	.long	_ZTI11CStringBaseIwE    # TypeInfo 5
	.long	_ZTI11CStringBaseIcE    # TypeInfo 4
	.long	_ZTIPKc                 # TypeInfo 3
	.long	_ZTIi                   # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.type	g_StdStream,@object     # @g_StdStream
	.bss
	.globl	g_StdStream
	.p2align	3
g_StdStream:
	.quad	0
	.size	g_StdStream, 8

	.type	_ZTS13CNewException,@object # @_ZTS13CNewException
	.section	.rodata._ZTS13CNewException,"aG",@progbits,_ZTS13CNewException,comdat
	.weak	_ZTS13CNewException
_ZTS13CNewException:
	.asciz	"13CNewException"
	.size	_ZTS13CNewException, 16

	.type	_ZTI13CNewException,@object # @_ZTI13CNewException
	.section	.rodata._ZTI13CNewException,"aG",@progbits,_ZTI13CNewException,comdat
	.weak	_ZTI13CNewException
	.p2align	3
_ZTI13CNewException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CNewException
	.size	_ZTI13CNewException, 16

	.type	_ZTSN13NConsoleClose19CCtrlBreakExceptionE,@object # @_ZTSN13NConsoleClose19CCtrlBreakExceptionE
	.section	.rodata._ZTSN13NConsoleClose19CCtrlBreakExceptionE,"aG",@progbits,_ZTSN13NConsoleClose19CCtrlBreakExceptionE,comdat
	.weak	_ZTSN13NConsoleClose19CCtrlBreakExceptionE
	.p2align	4
_ZTSN13NConsoleClose19CCtrlBreakExceptionE:
	.asciz	"N13NConsoleClose19CCtrlBreakExceptionE"
	.size	_ZTSN13NConsoleClose19CCtrlBreakExceptionE, 39

	.type	_ZTIN13NConsoleClose19CCtrlBreakExceptionE,@object # @_ZTIN13NConsoleClose19CCtrlBreakExceptionE
	.section	.rodata._ZTIN13NConsoleClose19CCtrlBreakExceptionE,"aG",@progbits,_ZTIN13NConsoleClose19CCtrlBreakExceptionE,comdat
	.weak	_ZTIN13NConsoleClose19CCtrlBreakExceptionE
	.p2align	3
_ZTIN13NConsoleClose19CCtrlBreakExceptionE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN13NConsoleClose19CCtrlBreakExceptionE
	.size	_ZTIN13NConsoleClose19CCtrlBreakExceptionE, 16

	.type	_ZTS28CArchiveCommandLineException,@object # @_ZTS28CArchiveCommandLineException
	.section	.rodata._ZTS28CArchiveCommandLineException,"aG",@progbits,_ZTS28CArchiveCommandLineException,comdat
	.weak	_ZTS28CArchiveCommandLineException
	.p2align	4
_ZTS28CArchiveCommandLineException:
	.asciz	"28CArchiveCommandLineException"
	.size	_ZTS28CArchiveCommandLineException, 31

	.type	_ZTS11CStringBaseIcE,@object # @_ZTS11CStringBaseIcE
	.section	.rodata._ZTS11CStringBaseIcE,"aG",@progbits,_ZTS11CStringBaseIcE,comdat
	.weak	_ZTS11CStringBaseIcE
	.p2align	4
_ZTS11CStringBaseIcE:
	.asciz	"11CStringBaseIcE"
	.size	_ZTS11CStringBaseIcE, 17

	.type	_ZTI11CStringBaseIcE,@object # @_ZTI11CStringBaseIcE
	.section	.rodata._ZTI11CStringBaseIcE,"aG",@progbits,_ZTI11CStringBaseIcE,comdat
	.weak	_ZTI11CStringBaseIcE
	.p2align	3
_ZTI11CStringBaseIcE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS11CStringBaseIcE
	.size	_ZTI11CStringBaseIcE, 16

	.type	_ZTI28CArchiveCommandLineException,@object # @_ZTI28CArchiveCommandLineException
	.section	.rodata._ZTI28CArchiveCommandLineException,"aG",@progbits,_ZTI28CArchiveCommandLineException,comdat
	.weak	_ZTI28CArchiveCommandLineException
	.p2align	4
_ZTI28CArchiveCommandLineException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS28CArchiveCommandLineException
	.quad	_ZTI11CStringBaseIcE
	.size	_ZTI28CArchiveCommandLineException, 24

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTSN9NExitCode5EEnumE,@object # @_ZTSN9NExitCode5EEnumE
	.section	.rodata._ZTSN9NExitCode5EEnumE,"aG",@progbits,_ZTSN9NExitCode5EEnumE,comdat
	.weak	_ZTSN9NExitCode5EEnumE
	.p2align	4
_ZTSN9NExitCode5EEnumE:
	.asciz	"N9NExitCode5EEnumE"
	.size	_ZTSN9NExitCode5EEnumE, 19

	.type	_ZTIN9NExitCode5EEnumE,@object # @_ZTIN9NExitCode5EEnumE
	.section	.rodata._ZTIN9NExitCode5EEnumE,"aG",@progbits,_ZTIN9NExitCode5EEnumE,comdat
	.weak	_ZTIN9NExitCode5EEnumE
	.p2align	3
_ZTIN9NExitCode5EEnumE:
	.quad	_ZTVN10__cxxabiv116__enum_type_infoE+16
	.quad	_ZTSN9NExitCode5EEnumE
	.size	_ZTIN9NExitCode5EEnumE, 16

	.type	_ZTS11CStringBaseIwE,@object # @_ZTS11CStringBaseIwE
	.section	.rodata._ZTS11CStringBaseIwE,"aG",@progbits,_ZTS11CStringBaseIwE,comdat
	.weak	_ZTS11CStringBaseIwE
	.p2align	4
_ZTS11CStringBaseIwE:
	.asciz	"11CStringBaseIwE"
	.size	_ZTS11CStringBaseIwE, 17

	.type	_ZTI11CStringBaseIwE,@object # @_ZTI11CStringBaseIwE
	.section	.rodata._ZTI11CStringBaseIwE,"aG",@progbits,_ZTI11CStringBaseIwE,comdat
	.weak	_ZTI11CStringBaseIwE
	.p2align	3
_ZTI11CStringBaseIwE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS11CStringBaseIwE
	.size	_ZTI11CStringBaseIwE, 16

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"System error:"
	.size	.L.str, 14

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n\nUnknown Error\n"
	.size	.L.str.1, 17

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n\nInternal Error #"
	.size	.L.str.2, 19

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n\nError:\n"
	.size	.L.str.3, 10

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n\nERROR: Can't allocate required memory!\n"
	.size	.L.str.4, 42

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\nBreak signaled\n"
	.size	.L.str.5, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
