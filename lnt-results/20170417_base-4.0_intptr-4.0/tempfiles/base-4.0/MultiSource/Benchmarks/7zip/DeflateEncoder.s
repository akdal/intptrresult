	.text
	.file	"DeflateEncoder.bc"
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoderC2Eb
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoderC2Eb,@function
_ZN9NCompress8NDeflate8NEncoder6CCoderC2Eb: # @_ZN9NCompress8NDeflate8NEncoder6CCoderC2Eb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	1168(%rbx), %r14
	movq	$0, 1168(%rbx)
	movl	$0, 1176(%rbx)
	movq	$0, 1192(%rbx)
	movq	$0, 1208(%rbx)
	leaq	1240(%rbx), %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 1240(%rbx)
	movl	$32, 1264(%rbx)
	movb	$0, 1268(%rbx)
	movb	$1, 1269(%rbx)
	movups	%xmm0, 1272(%rbx)
	movl	$1, 1292(%rbx)
	movl	$1, 1296(%rbx)
	movb	$0, 1336(%rbx)
	movb	%sil, 1337(%rbx)
	movq	$0, 4920(%rbx)
	movl	$0, 39760(%rbx)
	xorl	%eax, %eax
	testb	%sil, %sil
	sete	%al
	movl	$_ZN9NCompress8NDeflateL11kLenStart64E, %ecx
	movl	$_ZN9NCompress8NDeflateL11kLenStart32E, %edx
	cmovneq	%rcx, %rdx
	movl	$_ZN9NCompress8NDeflateL16kLenDirectBits64E, %ecx
	movl	$_ZN9NCompress8NDeflateL16kLenDirectBits32E, %esi
	cmovneq	%rcx, %rsi
	leal	257(%rax), %ecx
	movl	%ecx, 1312(%rbx)
	addl	$255, %eax
	movl	%eax, 1308(%rbx)
	movq	%rdx, 1320(%rbx)
	movq	%rsi, 1328(%rbx)
.Ltmp0:
	callq	MatchFinder_Construct
.Ltmp1:
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r15
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp3:
	callq	*16(%rax)
.Ltmp4:
.LBB0_4:                                # %_ZN9NCompress8NDeflate8NEncoder13_CSeqInStreamD2Ev.exit
.Ltmp5:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer4FreeEv
.Ltmp6:
# BB#5:
	movq	1192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp11:
	callq	*16(%rax)
.Ltmp12:
.LBB0_7:                                # %_ZN12CBitlEncoderD2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_11:
.Ltmp13:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB0_8:
.Ltmp7:
	movq	%rax, %r14
	movq	1192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_12
# BB#9:
	movq	(%rdi), %rax
.Ltmp8:
	callq	*16(%rax)
.Ltmp9:
.LBB0_12:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB0_10:
.Ltmp10:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoderC2Eb, .Lfunc_end0-_ZN9NCompress8NDeflate8NEncoder6CCoderC2Eb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp8-.Ltmp12          #   Call between .Ltmp12 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder6CreateEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder6CreateEv,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder6CreateEv: # @_ZN9NCompress8NDeflate8NEncoder6CCoder6CreateEv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpq	$0, 1248(%rbx)
	jne	.LBB2_3
# BB#1:
.Ltmp14:
	movl	$262140, %edi           # imm = 0x3FFFC
	callq	MyAlloc
.Ltmp15:
# BB#2:
	movq	%rax, 1248(%rbx)
	testq	%rax, %rax
	je	.LBB2_26
.LBB2_3:
	cmpq	$0, 4920(%rbx)
	jne	.LBB2_6
# BB#4:
.Ltmp16:
	movl	$339968, %edi           # imm = 0x53000
	callq	MyAlloc
.Ltmp17:
# BB#5:
	movq	%rax, 4920(%rbx)
	testq	%rax, %rax
	je	.LBB2_26
.LBB2_6:
	cmpb	$0, 1301(%rbx)
	je	.LBB2_12
# BB#7:
	cmpq	$0, 1272(%rbx)
	jne	.LBB2_16
# BB#8:
.Ltmp18:
	movl	$1310700, %edi          # imm = 0x13FFEC
	callq	MidAlloc
.Ltmp19:
# BB#9:
	movq	%rax, 1272(%rbx)
	testq	%rax, %rax
	jne	.LBB2_16
	jmp	.LBB2_26
.LBB2_12:
	cmpq	$0, 1280(%rbx)
	jne	.LBB2_16
# BB#13:
.Ltmp20:
	movl	$1040, %edi             # imm = 0x410
	callq	MyAlloc
.Ltmp21:
# BB#14:
	movq	%rax, 1280(%rbx)
	testq	%rax, %rax
	je	.LBB2_26
# BB#15:
	movq	%rax, 1256(%rbx)
.LBB2_16:
	cmpb	$0, 1336(%rbx)
	je	.LBB2_17
.LBB2_21:
	movl	39760(%rbx), %eax
	testl	%eax, %eax
	je	.LBB2_23
# BB#22:
	movl	%eax, 60(%rbx)
.LBB2_23:
	movb	$1, 1336(%rbx)
	xorl	%ebp, %ebp
	jmp	.LBB2_27
.LBB2_17:
	movzbl	1269(%rbx), %eax
	movl	%eax, 112(%rbx)
	movl	$3, 96(%rbx)
	cmpb	$0, 1337(%rbx)
	movl	$65536, %eax            # imm = 0x10000
	movl	$32768, %esi            # imm = 0x8000
	cmovnel	%eax, %esi
	movl	1264(%rbx), %ecx
	movl	1312(%rbx), %r8d
	subl	%ecx, %r8d
.Ltmp22:
	movl	$69889, %edx            # imm = 0x11101
	movl	$_ZN9NCompress8NDeflate8NEncoderL7g_AllocE, %r9d
	movq	%rbx, %rdi
	callq	MatchFinder_Create
.Ltmp23:
# BB#18:
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	testl	%eax, %eax
	je	.LBB2_27
# BB#19:
	leaq	1168(%rbx), %rdi
.Ltmp24:
	movl	$1048576, %esi          # imm = 0x100000
	callq	_ZN10COutBuffer6CreateEj
.Ltmp25:
# BB#20:                                # %_ZN12CBitlEncoder6CreateEj.exit
	testb	%al, %al
	jne	.LBB2_21
	jmp	.LBB2_27
.LBB2_10:
.Ltmp26:
	movq	%rdx, %rbp
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %ebp
	je	.LBB2_11
# BB#25:
	callq	__cxa_end_catch
.LBB2_26:
	movl	$-2147024882, %ebp      # imm = 0x8007000E
.LBB2_27:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB2_11:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp27:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp28:
# BB#28:
.LBB2_24:
.Ltmp29:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder6CreateEv, .Lfunc_end2-_ZN9NCompress8NDeflate8NEncoder6CCoder6CreateEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp25-.Ltmp14         #   Call between .Ltmp14 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin1   #     jumps to .Ltmp26
	.byte	3                       #   On action: 2
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp27-.Ltmp25         #   Call between .Ltmp25 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin1   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp28     #   Call between .Ltmp28 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder25BaseSetEncoderProperties2EPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder25BaseSetEncoderProperties2EPKjPK14tagPROPVARIANTj,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder25BaseSetEncoderProperties2EPKjPK14tagPROPVARIANTj: # @_ZN9NCompress8NDeflate8NEncoder6CCoder25BaseSetEncoderProperties2EPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB3_18
# BB#1:                                 # %.lr.ph
	movl	%ecx, %r8d
	addq	$8, %rdx
	xorl	%r10d, %r10d
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%r10,4), %ecx
	movl	$-2147024809, %eax      # imm = 0x80070057
	addl	$-8, %ecx
	cmpl	$4, %ecx
	ja	.LBB3_19
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	jmpq	*.LJTI3_0(,%rcx,8)
.LBB3_10:                               #   in Loop: Header=BB3_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB3_19
# BB#11:                                #   in Loop: Header=BB3_2 Depth=1
	movl	(%rdx), %ecx
	movl	%ecx, 1264(%rdi)
	cmpl	$3, %ecx
	jb	.LBB3_19
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	1312(%rdi), %ecx
	jbe	.LBB3_17
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_13:                               #   in Loop: Header=BB3_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB3_19
# BB#14:                                #   in Loop: Header=BB3_2 Depth=1
	movl	(%rdx), %eax
	movl	%eax, 39760(%rdi)
	jmp	.LBB3_17
	.p2align	4, 0x90
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB3_19
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	(%rdx), %eax
	testl	%eax, %eax
	cmovel	%r9d, %eax
	movl	%eax, 1296(%rdi)
	cmpl	$1, %eax
	jne	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$1, 1292(%rdi)
	jmp	.LBB3_17
	.p2align	4, 0x90
.LBB3_15:                               #   in Loop: Header=BB3_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB3_19
# BB#16:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, (%rdx)
	sete	1268(%rdi)
	setne	1269(%rdi)
	jmp	.LBB3_17
.LBB3_7:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$10, %eax
	jg	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$2, 1292(%rdi)
	jmp	.LBB3_17
.LBB3_9:                                #   in Loop: Header=BB3_2 Depth=1
	addl	$-8, %eax
	movl	%eax, 1292(%rdi)
	movl	$10, 1296(%rdi)
	.p2align	4, 0x90
.LBB3_17:                               #   in Loop: Header=BB3_2 Depth=1
	incq	%r10
	addq	$16, %rdx
	cmpq	%r8, %r10
	jb	.LBB3_2
.LBB3_18:
	xorl	%eax, %eax
.LBB3_19:                               # %.thread
	retq
.Lfunc_end3:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder25BaseSetEncoderProperties2EPKjPK14tagPROPVARIANTj, .Lfunc_end3-_ZN9NCompress8NDeflate8NEncoder6CCoder25BaseSetEncoderProperties2EPKjPK14tagPROPVARIANTj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_10
	.quad	.LBB3_19
	.quad	.LBB3_13
	.quad	.LBB3_4
	.quad	.LBB3_15

	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder4FreeEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder4FreeEv,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder4FreeEv: # @_ZN9NCompress8NDeflate8NEncoder6CCoder4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	1272(%rbx), %rdi
	callq	MidFree
	movq	$0, 1272(%rbx)
	movq	1280(%rbx), %rdi
	callq	MyFree
	movq	$0, 1280(%rbx)
	movq	1248(%rbx), %rdi
	callq	MyFree
	movq	$0, 1248(%rbx)
	movq	4920(%rbx), %rdi
	callq	MyFree
	movq	$0, 4920(%rbx)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder4FreeEv, .Lfunc_end4-_ZN9NCompress8NDeflate8NEncoder6CCoder4FreeEv
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev,@function
_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev: # @_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	1272(%rbx), %rdi
.Ltmp30:
	callq	MidFree
.Ltmp31:
# BB#1:                                 # %.noexc
	movq	$0, 1272(%rbx)
	movq	1280(%rbx), %rdi
.Ltmp32:
	callq	MyFree
.Ltmp33:
# BB#2:                                 # %.noexc3
	movq	$0, 1280(%rbx)
	movq	1248(%rbx), %rdi
.Ltmp34:
	callq	MyFree
.Ltmp35:
# BB#3:                                 # %.noexc4
	movq	$0, 1248(%rbx)
	movq	4920(%rbx), %rdi
.Ltmp36:
	callq	MyFree
.Ltmp37:
# BB#4:
	movq	$0, 4920(%rbx)
.Ltmp38:
	movl	$_ZN9NCompress8NDeflate8NEncoderL7g_AllocE, %esi
	movq	%rbx, %rdi
	callq	MatchFinder_Free
.Ltmp39:
# BB#5:
	movq	1240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp43:
	callq	*16(%rax)
.Ltmp44:
.LBB5_7:                                # %_ZN9NCompress8NDeflate8NEncoder13_CSeqInStreamD2Ev.exit
	leaq	1168(%rbx), %rdi
.Ltmp55:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp56:
# BB#8:
	movq	1192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_14
# BB#9:
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*16(%rax)               # TAILCALL
.LBB5_14:                               # %_ZN12CBitlEncoderD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_17:
.Ltmp45:
	movq	%rax, %r14
	jmp	.LBB5_18
.LBB5_10:
.Ltmp57:
	movq	%rax, %r14
	movq	1192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_12
# BB#11:
	movq	(%rdi), %rax
.Ltmp58:
	callq	*16(%rax)
.Ltmp59:
	jmp	.LBB5_12
.LBB5_13:
.Ltmp60:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_15:
.Ltmp40:
	movq	%rax, %r14
	movq	1240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_18
# BB#16:
	movq	(%rdi), %rax
.Ltmp41:
	callq	*16(%rax)
.Ltmp42:
.LBB5_18:                               # %_ZN9NCompress8NDeflate8NEncoder13_CSeqInStreamD2Ev.exit8
	leaq	1168(%rbx), %rdi
.Ltmp46:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp47:
# BB#19:
	movq	1192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_12
# BB#20:
	movq	(%rdi), %rax
.Ltmp52:
	callq	*16(%rax)
.Ltmp53:
.LBB5_12:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_24:
.Ltmp54:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB5_21:
.Ltmp48:
	movq	%rax, %r14
	movq	1192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_25
# BB#22:
	movq	(%rdi), %rax
.Ltmp49:
	callq	*16(%rax)
.Ltmp50:
.LBB5_25:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB5_23:
.Ltmp51:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev, .Lfunc_end5-_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp39-.Ltmp30         #   Call between .Ltmp30 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin2   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin2   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin2   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp58-.Ltmp56         #   Call between .Ltmp56 and .Ltmp58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	1                       #   On action: 1
	.long	.Ltmp41-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp54-.Lfunc_begin2   #     jumps to .Ltmp54
	.byte	1                       #   On action: 1
	.long	.Ltmp46-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin2   #     jumps to .Ltmp48
	.byte	1                       #   On action: 1
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin2   #     jumps to .Ltmp54
	.byte	1                       #   On action: 1
	.long	.Ltmp53-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp49-.Ltmp53         #   Call between .Ltmp53 and .Ltmp49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin2   #     jumps to .Ltmp51
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetMatchesEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetMatchesEv,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder10GetMatchesEv: # @_ZN9NCompress8NDeflate8NEncoder6CCoder10GetMatchesEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	subq	$2088, %rsp             # imm = 0x828
.Lcfi23:
	.cfi_def_cfa_offset 2112
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpb	$0, 1301(%r14)
	je	.LBB6_3
# BB#1:
	movl	1288(%r14), %eax
	leaq	(%rax,%rax), %rcx
	addq	1272(%r14), %rcx
	movq	%rcx, 1256(%r14)
	cmpb	$0, 1376(%r14)
	je	.LBB6_3
# BB#2:
	movzwl	(%rcx), %ecx
	leal	1(%rcx,%rax), %eax
	movl	%eax, 1288(%r14)
	jmp	.LBB6_20
.LBB6_3:
	cmpb	$0, 1269(%r14)
	je	.LBB6_5
# BB#4:
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	Bt3Zip_MatchFinder_GetMatches
	jmp	.LBB6_6
.LBB6_5:
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	Hc3Zip_MatchFinder_GetMatches
.LBB6_6:
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	1256(%r14), %rcx
	movw	%ax, (%rcx)
	testl	%eax, %eax
	je	.LBB6_16
# BB#7:                                 # %.lr.ph41.preheader
	movl	$-1, %esi
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph41
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %edx
	leal	1(%rdx), %esi
	movzwl	(%rsp,%rsi,4), %edi
	leal	2(%rdx), %esi
	movw	%di, (%rcx,%rsi,2)
	movzwl	(%rsp,%rsi,4), %edi
	addl	$3, %edx
	movw	%di, (%rcx,%rdx,2)
	cmpl	%eax, %edx
	jb	.LBB6_8
# BB#9:                                 # %._crit_edge
	leal	-2(%rax), %edx
	movl	(%rsp,%rdx,4), %edx
	cmpl	1264(%r14), %edx
	jne	.LBB6_16
# BB#10:
	movl	1312(%r14), %r9d
	cmpl	%r9d, %edx
	je	.LBB6_16
# BB#11:
	movl	%esi, %r8d
	movl	16(%r14), %edi
	subl	8(%r14), %edi
	incl	%edi
	cmpl	%r9d, %edi
	cmoval	%r9d, %edi
	cmpl	%edi, %edx
	jae	.LBB6_15
# BB#12:                                # %.lr.ph.preheader
	movq	(%r14), %r9
	decq	%r9
	leal	-1(%rax), %esi
	movl	(%rsp,%rsi,4), %r10d
	incl	%r10d
	movq	%r9, %rsi
	subq	%r10, %rsi
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r9,%rdx), %ebx
	cmpb	(%rsi,%rdx), %bl
	jne	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_13 Depth=1
	incq	%rdx
	cmpl	%edi, %edx
	jb	.LBB6_13
.LBB6_15:                               # %.critedge
	movw	%dx, (%rcx,%r8,2)
.LBB6_16:
	cmpb	$0, 1301(%r14)
	je	.LBB6_18
# BB#17:
	movl	1288(%r14), %ecx
	leal	1(%rax,%rcx), %eax
	movl	%eax, 1288(%r14)
.LBB6_18:
	cmpb	$0, 1376(%r14)
	jne	.LBB6_20
# BB#19:
	incl	1380(%r14)
.LBB6_20:
	addq	$2088, %rsp             # imm = 0x828
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetMatchesEv, .Lfunc_end6-_ZN9NCompress8NDeflate8NEncoder6CCoder10GetMatchesEv
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj: # @_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	testl	%ebp, %ebp
	je	.LBB7_6
# BB#1:
	movb	1376(%rbx), %al
	testb	%al, %al
	jne	.LBB7_6
# BB#2:
	cmpb	$0, 1269(%rbx)
	je	.LBB7_4
# BB#3:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	Bt3Zip_MatchFinder_Skip
	jmp	.LBB7_5
.LBB7_4:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	Hc3Zip_MatchFinder_Skip
.LBB7_5:
	addl	%ebp, 1380(%rbx)
.LBB7_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj, .Lfunc_end7-_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder8BackwardERjj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder8BackwardERjj,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder8BackwardERjj: # @_ZN9NCompress8NDeflate8NEncoder6CCoder8BackwardERjj
	.cfi_startproc
# BB#0:
	movl	%edx, 1384(%rdi)
	movl	%edx, %eax
	movzwl	4932(%rdi,%rax,8), %ecx
	movzwl	4934(%rdi,%rax,8), %r8d
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	movzwl	%cx, %eax
	movzwl	4934(%rdi,%rax,8), %r9d
	testw	%cx, %cx
	movzwl	4932(%rdi,%rax,8), %ecx
	movw	%r8w, 4934(%rdi,%rax,8)
	movw	%dx, 4932(%rdi,%rax,8)
	movl	%r9d, %r8d
	movl	%eax, %edx
	jne	.LBB8_1
# BB#2:
	movzwl	4934(%rdi), %eax
	movl	%eax, (%rsi)
	movzwl	4932(%rdi), %eax
	movl	%eax, 1388(%rdi)
	retq
.Lfunc_end8:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder8BackwardERjj, .Lfunc_end8-_ZN9NCompress8NDeflate8NEncoder6CCoder8BackwardERjj
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetOptimalERj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetOptimalERj,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder10GetOptimalERj: # @_ZN9NCompress8NDeflate8NEncoder6CCoder10GetOptimalERj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 80
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	1388(%rbx), %eax
	cmpl	%eax, 1384(%rbx)
	jne	.LBB9_1
# BB#2:
	movq	$0, 1384(%rbx)
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetMatchesEv
	movq	1256(%rbx), %rax
	movzwl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB9_3
# BB#4:
	leal	-2(%rcx), %edx
	movzwl	2(%rax,%rdx,2), %r15d
	cmpl	1264(%rbx), %r15d
	jbe	.LBB9_9
# BB#5:
	decl	%ecx
	movzwl	2(%rax,%rcx,2), %eax
	movl	%eax, (%r14)
	movl	%r15d, %ebp
	decl	%ebp
	je	.LBB9_63
# BB#6:
	movb	1376(%rbx), %al
	testb	%al, %al
	jne	.LBB9_63
# BB#7:
	cmpb	$0, 1269(%rbx)
	je	.LBB9_61
# BB#8:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	Bt3Zip_MatchFinder_Skip
	jmp	.LBB9_62
.LBB9_1:
	movzwl	4932(%rbx,%rax,8), %r15d
	subl	%eax, %r15d
	movzwl	4934(%rbx,%rax,8), %eax
	movl	%eax, (%r14)
	movl	1388(%rbx), %eax
	movzwl	4932(%rbx,%rax,8), %eax
	movl	%eax, 1388(%rbx)
	jmp	.LBB9_63
.LBB9_3:
	movl	$1, %r15d
	jmp	.LBB9_63
.LBB9_9:
	movq	(%rbx), %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	subl	1380(%rbx), %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %edx
	movzbl	1392(%rbx,%rdx), %edx
	movl	%edx, 4936(%rbx)
	movw	$0, 4940(%rbx)
	movl	$268435455, 4944(%rbx)  # imm = 0xFFFFFFF
	movw	$1, 4948(%rbx)
	cmpl	$3, %r15d
	jb	.LBB9_15
# BB#10:                                # %.lr.ph188.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_11:                               # %.lr.ph188
                                        # =>This Inner Loop Header: Depth=1
	leaq	3(%rdx), %rsi
	leal	1(%rcx), %edi
	movzwl	2(%rax,%rdi,2), %ebp
	cmpq	$511, %rbp              # imm = 0x1FF
	movw	$0, 4956(%rbx,%rdx,8)
	movw	%bp, 4958(%rbx,%rdx,8)
	movzbl	1648(%rbx,%rdx), %edi
	ja	.LBB9_13
# BB#12:                                #   in Loop: Header=BB9_11 Depth=1
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rbp), %ebp
	jmp	.LBB9_14
	.p2align	4, 0x90
.LBB9_13:                               #   in Loop: Header=BB9_11 Depth=1
	shrq	$8, %rbp
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rbp), %ebp
	addq	$16, %rbp
.LBB9_14:                               # %_ZN9NCompress8NDeflate8NEncoder10GetPosSlotEj.exit
                                        #   in Loop: Header=BB9_11 Depth=1
	movl	%ebp, %ebp
	movzbl	1904(%rbx,%rbp), %ebp
	addl	%edi, %ebp
	movl	%ebp, 4952(%rbx,%rdx,8)
	movl	%ecx, %edi
	movzwl	2(%rax,%rdi,2), %edi
	leal	2(%rcx), %ebp
	cmpq	%rdi, %rsi
	cmovnel	%ecx, %ebp
	leaq	1(%rdx), %rcx
	addq	$4, %rdx
	cmpq	%r15, %rdx
	movq	%rcx, %rdx
	movl	%ebp, %ecx
	jbe	.LBB9_11
.LBB9_15:                               # %_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj.exit163.thread173.preheader
	movq	%r14, 8(%rsp)           # 8-byte Spill
	leaq	4992(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
.LBB9_16:                               # %_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj.exit163.thread173.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_17 Depth 2
                                        #     Child Loop BB9_31 Depth 2
                                        #     Child Loop BB9_34 Depth 2
                                        #     Child Loop BB9_48 Depth 2
	leal	1(%r14), %eax
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB9_17:                               # %_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj.exit163.thread173
                                        #   Parent Loop BB9_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %r13d
	leal	1(%rbp), %r14d
	cmpl	%r15d, %r14d
	je	.LBB9_20
# BB#18:                                # %_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj.exit163.thread173
                                        #   in Loop: Header=BB9_17 Depth=2
	cmpl	$4096, %r14d            # imm = 0x1000
	je	.LBB9_20
# BB#19:                                #   in Loop: Header=BB9_17 Depth=2
	cmpl	$653286, 1288(%rbx)     # imm = 0x9F7E6
	jae	.LBB9_20
# BB#23:                                #   in Loop: Header=BB9_17 Depth=2
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetMatchesEv
	movq	1256(%rbx), %r9
	movzwl	(%r9), %r8d
	testl	%r8d, %r8d
	je	.LBB9_24
# BB#38:                                #   in Loop: Header=BB9_17 Depth=2
	leal	-2(%r8), %eax
	movzwl	2(%r9,%rax,2), %r12d
	cmpl	1264(%rbx), %r12d
	jbe	.LBB9_25
	jmp	.LBB9_39
	.p2align	4, 0x90
.LBB9_24:                               #   in Loop: Header=BB9_17 Depth=2
	xorl	%r12d, %r12d
.LBB9_25:                               #   in Loop: Header=BB9_17 Depth=2
	movl	%r14d, %eax
	movl	4928(%rbx,%rax,8), %edx
	movq	(%rbx), %rax
	movl	%r14d, %ecx
	subl	1380(%rbx), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %eax
	movzbl	1392(%rbx,%rax), %eax
	addl	%edx, %eax
	addl	$2, %ebp
	cmpl	4928(%rbx,%rbp,8), %eax
	jae	.LBB9_27
# BB#26:                                #   in Loop: Header=BB9_17 Depth=2
	movl	%eax, 4928(%rbx,%rbp,8)
	movw	%r14w, 4932(%rbx,%rbp,8)
.LBB9_27:                               #   in Loop: Header=BB9_17 Depth=2
	leal	1(%r13), %eax
	testw	%r8w, %r8w
	movl	%r14d, %ebp
	je	.LBB9_17
# BB#28:                                # %.preheader
                                        #   in Loop: Header=BB9_16 Depth=1
	leal	(%r12,%r14), %esi
	cmpl	%esi, %r15d
	jae	.LBB9_36
# BB#29:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_16 Depth=1
	movl	%r15d, %eax
	movl	%esi, %ecx
	movl	%esi, %edi
	subl	%r15d, %edi
	decq	%rcx
	subq	%rax, %rcx
	testb	$7, %dil
	je	.LBB9_32
# BB#30:                                # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB9_16 Depth=1
	leal	(%r12,%r13), %edi
	subb	%r15b, %dil
	movzbl	%dil, %edi
	andl	$7, %edi
	negq	%rdi
	.p2align	4, 0x90
.LBB9_31:                               # %.lr.ph.prol
                                        #   Parent Loop BB9_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$268435455, 4936(%rbx,%rax,8) # imm = 0xFFFFFFF
	incq	%rax
	incq	%rdi
	jne	.LBB9_31
.LBB9_32:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB9_16 Depth=1
	cmpq	$7, %rcx
	jb	.LBB9_35
# BB#33:                                # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB9_16 Depth=1
	addl	%r13d, %r12d
	subq	%rax, %r12
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	.p2align	4, 0x90
.LBB9_34:                               # %.lr.ph
                                        #   Parent Loop BB9_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$268435455, -56(%rax)   # imm = 0xFFFFFFF
	movl	$268435455, -48(%rax)   # imm = 0xFFFFFFF
	movl	$268435455, -40(%rax)   # imm = 0xFFFFFFF
	movl	$268435455, -32(%rax)   # imm = 0xFFFFFFF
	movl	$268435455, -24(%rax)   # imm = 0xFFFFFFF
	movl	$268435455, -16(%rax)   # imm = 0xFFFFFFF
	movl	$268435455, -8(%rax)    # imm = 0xFFFFFFF
	movl	$268435455, (%rax)      # imm = 0xFFFFFFF
	addq	$64, %rax
	addq	$-8, %r12
	jne	.LBB9_34
.LBB9_35:                               #   in Loop: Header=BB9_16 Depth=1
	movl	%esi, %r15d
.LBB9_36:                               # %._crit_edge
                                        #   in Loop: Header=BB9_16 Depth=1
	movzwl	4(%r9), %esi
	cmpl	$511, %esi              # imm = 0x1FF
	ja	.LBB9_46
# BB#37:                                #   in Loop: Header=BB9_16 Depth=1
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rsi), %eax
	jmp	.LBB9_47
.LBB9_46:                               #   in Loop: Header=BB9_16 Depth=1
	movl	%esi, %eax
	shrl	$8, %eax
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rax), %eax
	addq	$16, %rax
.LBB9_47:                               # %_ZN9NCompress8NDeflate8NEncoder10GetPosSlotEj.exit160
                                        #   in Loop: Header=BB9_16 Depth=1
	movl	%eax, %eax
	movzbl	1904(%rbx,%rax), %edi
	addl	%edx, %edi
	movl	$3, %edx
	xorl	%ecx, %ecx
	jmp	.LBB9_48
	.p2align	4, 0x90
.LBB9_60:                               #   in Loop: Header=BB9_48 Depth=2
	incl	%edx
	movl	%eax, %ecx
.LBB9_48:                               #   Parent Loop BB9_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-3(%rdx), %eax
	movzbl	1648(%rbx,%rax), %eax
	addl	%edi, %eax
	leal	(%r14,%rdx), %ebp
	cmpl	4928(%rbx,%rbp,8), %eax
	jae	.LBB9_50
# BB#49:                                #   in Loop: Header=BB9_48 Depth=2
	movl	%eax, 4928(%rbx,%rbp,8)
	movw	%r14w, 4932(%rbx,%rbp,8)
	movw	%si, 4934(%rbx,%rbp,8)
.LBB9_50:                               #   in Loop: Header=BB9_48 Depth=2
	movl	%ecx, %eax
	movzwl	2(%r9,%rax,2), %eax
	cmpl	%eax, %edx
	jne	.LBB9_51
# BB#52:                                #   in Loop: Header=BB9_48 Depth=2
	leal	2(%rcx), %eax
	cmpl	%r8d, %eax
	je	.LBB9_16
# BB#53:                                #   in Loop: Header=BB9_48 Depth=2
	cmpl	$511, %esi              # imm = 0x1FF
	ja	.LBB9_55
# BB#54:                                #   in Loop: Header=BB9_48 Depth=2
	movl	%esi, %esi
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rsi), %esi
	jmp	.LBB9_56
	.p2align	4, 0x90
.LBB9_51:                               #   in Loop: Header=BB9_48 Depth=2
	movl	%ecx, %eax
	jmp	.LBB9_60
	.p2align	4, 0x90
.LBB9_55:                               #   in Loop: Header=BB9_48 Depth=2
	shrl	$8, %esi
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rsi), %esi
	addq	$16, %rsi
.LBB9_56:                               # %_ZN9NCompress8NDeflate8NEncoder10GetPosSlotEj.exit158
                                        #   in Loop: Header=BB9_48 Depth=2
	movl	%esi, %esi
	movzbl	1904(%rbx,%rsi), %esi
	subl	%esi, %edi
	addl	$3, %ecx
	movzwl	2(%r9,%rcx,2), %esi
	cmpl	$511, %esi              # imm = 0x1FF
	ja	.LBB9_58
# BB#57:                                #   in Loop: Header=BB9_48 Depth=2
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rsi), %ecx
	jmp	.LBB9_59
	.p2align	4, 0x90
.LBB9_58:                               #   in Loop: Header=BB9_48 Depth=2
	movl	%esi, %ecx
	shrl	$8, %ecx
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rcx), %ecx
	addq	$16, %rcx
.LBB9_59:                               # %_ZN9NCompress8NDeflate8NEncoder10GetPosSlotEj.exit156
                                        #   in Loop: Header=BB9_48 Depth=2
	movl	%ecx, %ecx
	movzbl	1904(%rbx,%rcx), %ecx
	addl	%ecx, %edi
	jmp	.LBB9_60
.LBB9_20:
	movl	%r14d, 1384(%rbx)
	movl	%r14d, %eax
	movzwl	4932(%rbx,%rax,8), %ecx
	movzwl	4934(%rbx,%rax,8), %eax
	.p2align	4, 0x90
.LBB9_21:                               # =>This Inner Loop Header: Depth=1
	movzwl	%cx, %edx
	movzwl	4934(%rbx,%rdx,8), %esi
	testw	%cx, %cx
	movzwl	4932(%rbx,%rdx,8), %ecx
	movw	%ax, 4934(%rbx,%rdx,8)
	movw	%r14w, 4932(%rbx,%rdx,8)
	movl	%esi, %eax
	movl	%edx, %r14d
	jne	.LBB9_21
# BB#22:                                # %_ZN9NCompress8NDeflate8NEncoder6CCoder8BackwardERjj.exit
	movzwl	4934(%rbx), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, (%rcx)
	movzwl	4932(%rbx), %r15d
	movl	%r15d, 1388(%rbx)
	jmp	.LBB9_63
.LBB9_39:
	movl	%r14d, 1384(%rbx)
	movl	%r14d, %eax
	movzwl	4932(%rbx,%rax,8), %edx
	movzwl	4934(%rbx,%rax,8), %ecx
	movl	%r14d, %esi
	.p2align	4, 0x90
.LBB9_40:                               # =>This Inner Loop Header: Depth=1
	movzwl	%dx, %edi
	movzwl	4934(%rbx,%rdi,8), %ebp
	testw	%dx, %dx
	movzwl	4932(%rbx,%rdi,8), %edx
	movw	%cx, 4934(%rbx,%rdi,8)
	movw	%si, 4932(%rbx,%rdi,8)
	movl	%ebp, %ecx
	movl	%edi, %esi
	jne	.LBB9_40
# BB#41:                                # %_ZN9NCompress8NDeflate8NEncoder6CCoder8BackwardERjj.exit168
	movzwl	4934(%rbx), %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%ecx, (%rdx)
	movzwl	4932(%rbx), %r15d
	movl	%r15d, 1388(%rbx)
	decl	%r8d
	movzwl	2(%r9,%r8,2), %ecx
	movw	%cx, 4934(%rbx,%rax,8)
	addl	%r12d, %r14d
	movl	%r14d, 1384(%rbx)
	movw	%r14w, 4932(%rbx,%rax,8)
	decl	%r12d
	je	.LBB9_63
# BB#42:                                # %_ZN9NCompress8NDeflate8NEncoder6CCoder8BackwardERjj.exit168
	movb	1376(%rbx), %al
	testb	%al, %al
	jne	.LBB9_63
# BB#43:
	cmpb	$0, 1269(%rbx)
	je	.LBB9_45
# BB#44:
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	Bt3Zip_MatchFinder_Skip
	addl	%r12d, 1380(%rbx)
	jmp	.LBB9_63
.LBB9_61:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	Hc3Zip_MatchFinder_Skip
.LBB9_62:                               # %_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj.exit
	addl	%ebp, 1380(%rbx)
.LBB9_63:                               # %_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj.exit
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_45:
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	Hc3Zip_MatchFinder_Skip
	addl	%r12d, 1380(%rbx)
	jmp	.LBB9_63
.Lfunc_end9:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetOptimalERj, .Lfunc_end9-_ZN9NCompress8NDeflate8NEncoder6CCoder10GetOptimalERj
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder14GetOptimalFastERj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder14GetOptimalFastERj,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder14GetOptimalFastERj: # @_ZN9NCompress8NDeflate8NEncoder6CCoder14GetOptimalFastERj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetMatchesEv
	movq	1256(%rbx), %rax
	movzwl	(%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB10_1
# BB#2:
	leal	-1(%rcx), %edx
	movzwl	(%rax,%rdx,2), %r14d
	movzwl	(%rax,%rcx,2), %eax
	movl	%eax, (%rbp)
	movl	%r14d, %ebp
	decl	%ebp
	je	.LBB10_8
# BB#3:
	movb	1376(%rbx), %al
	testb	%al, %al
	jne	.LBB10_8
# BB#4:
	cmpb	$0, 1269(%rbx)
	je	.LBB10_6
# BB#5:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	Bt3Zip_MatchFinder_Skip
	jmp	.LBB10_7
.LBB10_1:
	movl	$1, %r14d
	jmp	.LBB10_8
.LBB10_6:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	Hc3Zip_MatchFinder_Skip
.LBB10_7:
	addl	%ebp, 1380(%rbx)
.LBB10_8:                               # %_ZN9NCompress8NDeflate8NEncoder6CCoder7MovePosEj.exit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder14GetOptimalFastERj, .Lfunc_end10-_ZN9NCompress8NDeflate8NEncoder6CCoder14GetOptimalFastERj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.zero	16,8
.LCPI11_1:
	.zero	16,5
	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder7CTables14InitStructuresEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder7CTables14InitStructuresEv,@function
_ZN9NCompress8NDeflate8NEncoder7CTables14InitStructuresEv: # @_ZN9NCompress8NDeflate8NEncoder7CTables14InitStructuresEv
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader
	movaps	.LCPI11_0(%rip), %xmm0  # xmm0 = [8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]
	movups	%xmm0, 240(%rdi)
	movups	%xmm0, 224(%rdi)
	movups	%xmm0, 208(%rdi)
	movups	%xmm0, 192(%rdi)
	movups	%xmm0, 176(%rdi)
	movups	%xmm0, 160(%rdi)
	movups	%xmm0, 144(%rdi)
	movups	%xmm0, 128(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movb	$13, 256(%rdi)
	movaps	.LCPI11_1(%rip), %xmm0  # xmm0 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	movups	%xmm0, 272(%rdi)
	movups	%xmm0, 257(%rdi)
	movups	%xmm0, 304(%rdi)
	movups	%xmm0, 288(%rdi)
	retq
.Lfunc_end11:
	.size	_ZN9NCompress8NDeflate8NEncoder7CTables14InitStructuresEv, .Lfunc_end11-_ZN9NCompress8NDeflate8NEncoder7CTables14InitStructuresEv
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder15LevelTableDummyEPKhiPj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder15LevelTableDummyEPKhiPj,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder15LevelTableDummyEPKhiPj: # @_ZN9NCompress8NDeflate8NEncoder6CCoder15LevelTableDummyEPKhiPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 48
.Lcfi55:
	.cfi_offset %rbx, -48
.Lcfi56:
	.cfi_offset %r12, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	jle	.LBB12_19
# BB#1:                                 # %.lr.ph
	movzbl	(%rsi), %eax
	xorl	%r14d, %r14d
	testb	%al, %al
	setne	%r14b
	movl	$138, %edi
	movl	$7, %r15d
	cmovel	%edi, %r15d
	addl	$3, %r14d
	leal	-1(%rdx), %edi
	leaq	72(%rcx), %r9
	leaq	68(%rcx), %r8
	leaq	64(%rcx), %r10
	movslq	%edi, %r11
	movl	%edx, %r12d
	xorl	%edi, %edi
	movl	$255, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	cmpq	%r11, %rdi
	movl	$255, %eax
	jge	.LBB12_4
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	movzbl	1(%rsi,%rdi), %eax
.LBB12_4:                               #   in Loop: Header=BB12_2 Depth=1
	incl	%ebx
	cmpl	%r15d, %ebx
	jge	.LBB12_7
# BB#5:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpl	%eax, %edx
	jne	.LBB12_7
# BB#6:                                 #   in Loop: Header=BB12_2 Depth=1
	movl	%ebp, %edx
	jmp	.LBB12_18
	.p2align	4, 0x90
.LBB12_7:                               #   in Loop: Header=BB12_2 Depth=1
	cmpl	%r14d, %ebx
	jge	.LBB12_9
# BB#8:                                 #   in Loop: Header=BB12_2 Depth=1
	movslq	%edx, %rbp
	leaq	(%rcx,%rbp,4), %rbp
	jmp	.LBB12_15
	.p2align	4, 0x90
.LBB12_9:                               #   in Loop: Header=BB12_2 Depth=1
	testl	%edx, %edx
	je	.LBB12_12
# BB#10:                                #   in Loop: Header=BB12_2 Depth=1
	movl	$1, %ebx
	cmpl	%ebp, %edx
	je	.LBB12_14
# BB#13:                                #   in Loop: Header=BB12_2 Depth=1
	movslq	%edx, %rbp
	incl	(%rcx,%rbp,4)
.LBB12_14:                              #   in Loop: Header=BB12_2 Depth=1
	movq	%r10, %rbp
	jmp	.LBB12_15
.LBB12_12:                              #   in Loop: Header=BB12_2 Depth=1
	cmpl	$11, %ebx
	movq	%r9, %rbp
	cmovlq	%r8, %rbp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB12_15:                              #   in Loop: Header=BB12_2 Depth=1
	addl	%ebx, (%rbp)
	xorl	%ebx, %ebx
	testl	%eax, %eax
	je	.LBB12_17
# BB#16:                                #   in Loop: Header=BB12_2 Depth=1
	xorl	%r14d, %r14d
	cmpl	%eax, %edx
	setne	%r14b
	leal	6(%r14), %r15d
	addl	$3, %r14d
	jmp	.LBB12_18
.LBB12_17:                              #   in Loop: Header=BB12_2 Depth=1
	movl	$3, %r14d
	movl	$138, %r15d
	.p2align	4, 0x90
.LBB12_18:                              #   in Loop: Header=BB12_2 Depth=1
	incq	%rdi
	cmpq	%rdi, %r12
	movl	%edx, %ebp
	jne	.LBB12_2
.LBB12_19:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder15LevelTableDummyEPKhiPj, .Lfunc_end12-_ZN9NCompress8NDeflate8NEncoder6CCoder15LevelTableDummyEPKhiPj
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji: # @_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %rbx
	testl	%edx, %edx
	je	.LBB13_7
# BB#1:                                 # %.lr.ph.i
	leaq	1168(%rbx), %r14
	movl	1224(%rbx), %eax
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %ebp
	subl	%eax, %ebp
	jb	.LBB13_3
# BB#4:                                 #   in Loop: Header=BB13_2 Depth=1
	movl	$8, %ecx
	subl	%eax, %ecx
	movl	%r15d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%rbx), %eax
	movq	1168(%rbx), %rcx
	movl	1176(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	1176(%rbx), %eax
	cmpl	1180(%rbx), %eax
	jne	.LBB13_6
# BB#5:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB13_6:                               # %_ZN10COutBuffer9WriteByteEh.exit.i
                                        #   in Loop: Header=BB13_2 Depth=1
	movzbl	1224(%rbx), %ecx
	shrl	%cl, %r15d
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
	movl	$8, %eax
	testl	%ebp, %ebp
	movl	%ebp, %edx
	jne	.LBB13_2
	jmp	.LBB13_7
.LBB13_3:
	movl	$1, %esi
	movl	%edx, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r15d, %esi
	movl	$8, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%rbx), %esi
	movb	%sil, 1228(%rbx)
	subl	%edx, %eax
	movl	%eax, 1224(%rbx)
.LBB13_7:                               # %_ZN12CBitlEncoder9WriteBitsEjj.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji, .Lfunc_end13-_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder14LevelTableCodeEPKhiS4_PKj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder14LevelTableCodeEPKhiS4_PKj,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder14LevelTableCodeEPKhiS4_PKj: # @_ZN9NCompress8NDeflate8NEncoder6CCoder14LevelTableCodeEPKhiS4_PKj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi75:
	.cfi_def_cfa_offset 128
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	testl	%edx, %edx
	jle	.LBB14_74
# BB#1:                                 # %.lr.ph160
	movq	56(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %r15d
	xorl	%eax, %eax
	testb	%r15b, %r15b
	setne	%al
	movl	$138, %esi
	movl	$7, %r8d
	cmovel	%esi, %r8d
	addl	$3, %eax
	leal	-1(%rdx), %esi
	leaq	1168(%rbx), %rdi
	movq	%rdi, (%rsp)            # 8-byte Spill
	movslq	%esi, %rdi
	movl	%edx, %ebp
	xorl	%r13d, %r13d
	movl	$255, %edx
	xorl	%r12d, %r12d
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB14_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_21 Depth 2
                                        #     Child Loop BB14_30 Depth 2
                                        #     Child Loop BB14_36 Depth 2
                                        #     Child Loop BB14_60 Depth 2
                                        #     Child Loop BB14_66 Depth 2
                                        #     Child Loop BB14_46 Depth 2
                                        #     Child Loop BB14_52 Depth 2
                                        #     Child Loop BB14_9 Depth 2
                                        #       Child Loop BB14_11 Depth 3
	movl	%r15d, %esi
	cmpq	%rdi, %r13
	movl	$255, %r15d
	jge	.LBB14_4
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	movl	%esi, %ecx
	movq	56(%rsp), %rsi          # 8-byte Reload
	movzbl	1(%rsi,%r13), %r15d
	movl	%ecx, %esi
.LBB14_4:                               #   in Loop: Header=BB14_2 Depth=1
	leal	1(%r12), %r14d
	cmpl	%r8d, %r14d
	jge	.LBB14_6
# BB#5:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpl	%r15d, %esi
	je	.LBB14_73
.LBB14_6:                               #   in Loop: Header=BB14_2 Depth=1
	cmpl	%eax, %r14d
	movl	%r15d, 36(%rsp)         # 4-byte Spill
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	jge	.LBB14_17
# BB#7:                                 # %.preheader
                                        #   in Loop: Header=BB14_2 Depth=1
	testl	%r12d, %r12d
	js	.LBB14_70
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB14_2 Depth=1
	movslq	12(%rsp), %rbp          # 4-byte Folded Reload
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB14_9:                               #   Parent Loop BB14_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_11 Depth 3
	movq	24(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax,%rbp), %eax
	testl	%eax, %eax
	je	.LBB14_16
# BB#10:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB14_9 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rbp,4), %r15d
	movl	1224(%rbx), %edx
	.p2align	4, 0x90
.LBB14_11:                              #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %r13d
	subl	%edx, %r13d
	jb	.LBB14_12
# BB#13:                                #   in Loop: Header=BB14_11 Depth=3
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r15d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%rbx), %eax
	movq	1168(%rbx), %rcx
	movl	1176(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	1176(%rbx), %eax
	cmpl	1180(%rbx), %eax
	jne	.LBB14_15
# BB#14:                                #   in Loop: Header=BB14_11 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_15:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB14_11 Depth=3
	movzbl	1224(%rbx), %ecx
	shrl	%cl, %r15d
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
	movl	$8, %edx
	testl	%r13d, %r13d
	movl	%r13d, %eax
	jne	.LBB14_11
	jmp	.LBB14_16
	.p2align	4, 0x90
.LBB14_12:                              #   in Loop: Header=BB14_9 Depth=2
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r15d, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%rbx), %esi
	movb	%sil, 1228(%rbx)
	subl	%eax, %edx
	movl	%edx, 1224(%rbx)
.LBB14_16:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit
                                        #   in Loop: Header=BB14_9 Depth=2
	incl	%r12d
	cmpl	%r14d, %r12d
	jne	.LBB14_9
	jmp	.LBB14_70
	.p2align	4, 0x90
.LBB14_17:                              #   in Loop: Header=BB14_2 Depth=1
	testl	%esi, %esi
	je	.LBB14_42
# BB#18:                                #   in Loop: Header=BB14_2 Depth=1
	cmpl	%edx, %esi
	je	.LBB14_27
# BB#19:                                #   in Loop: Header=BB14_2 Depth=1
	movslq	%esi, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax,%rcx), %eax
	testl	%eax, %eax
	je	.LBB14_26
# BB#20:                                # %.lr.ph.i.i78
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx,%rcx,4), %r14d
	movl	1224(%rbx), %edx
	.p2align	4, 0x90
.LBB14_21:                              #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB14_22
# BB#23:                                #   in Loop: Header=BB14_21 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r14d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%rbx), %eax
	movq	1168(%rbx), %rcx
	movl	1176(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	1176(%rbx), %eax
	cmpl	1180(%rbx), %eax
	jne	.LBB14_25
# BB#24:                                #   in Loop: Header=BB14_21 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_25:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i81
                                        #   in Loop: Header=BB14_21 Depth=2
	movzbl	1224(%rbx), %ecx
	shrl	%cl, %r14d
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
	movl	$8, %edx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB14_21
	jmp	.LBB14_26
.LBB14_42:                              #   in Loop: Header=BB14_2 Depth=1
	cmpl	$10, %r14d
	jg	.LBB14_57
# BB#43:                                #   in Loop: Header=BB14_2 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movzbl	17(%rax), %eax
	testl	%eax, %eax
	je	.LBB14_44
# BB#45:                                # %.lr.ph.i.i96
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	68(%rcx), %r14d
	movl	1224(%rbx), %edx
	.p2align	4, 0x90
.LBB14_46:                              #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB14_47
# BB#48:                                #   in Loop: Header=BB14_46 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r14d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%rbx), %eax
	movq	1168(%rbx), %rcx
	movl	1176(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	1176(%rbx), %eax
	cmpl	1180(%rbx), %eax
	jne	.LBB14_50
# BB#49:                                #   in Loop: Header=BB14_46 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_50:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i99
                                        #   in Loop: Header=BB14_46 Depth=2
	movzbl	1224(%rbx), %ecx
	shrl	%cl, %r14d
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
	movl	$8, %edx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB14_46
	jmp	.LBB14_51
.LBB14_57:                              #   in Loop: Header=BB14_2 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movzbl	18(%rax), %eax
	testl	%eax, %eax
	je	.LBB14_58
# BB#59:                                # %.lr.ph.i.i108
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	72(%rcx), %r14d
	movl	1224(%rbx), %edx
	.p2align	4, 0x90
.LBB14_60:                              #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB14_61
# BB#62:                                #   in Loop: Header=BB14_60 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r14d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%rbx), %eax
	movq	1168(%rbx), %rcx
	movl	1176(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	1176(%rbx), %eax
	cmpl	1180(%rbx), %eax
	jne	.LBB14_64
# BB#63:                                #   in Loop: Header=BB14_60 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_64:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i111
                                        #   in Loop: Header=BB14_60 Depth=2
	movzbl	1224(%rbx), %ecx
	shrl	%cl, %r14d
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
	movl	$8, %edx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB14_60
	jmp	.LBB14_65
.LBB14_22:                              #   in Loop: Header=BB14_2 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r14d, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%rbx), %esi
	movb	%sil, 1228(%rbx)
	subl	%eax, %edx
	movl	%edx, 1224(%rbx)
.LBB14_26:                              #   in Loop: Header=BB14_2 Depth=1
	movl	%r12d, %r14d
.LBB14_27:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit82
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movzbl	16(%rax), %eax
	testl	%eax, %eax
	je	.LBB14_28
# BB#29:                                # %.lr.ph.i.i84
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	64(%rcx), %r15d
	movl	1224(%rbx), %edx
	.p2align	4, 0x90
.LBB14_30:                              #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB14_31
# BB#32:                                #   in Loop: Header=BB14_30 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r15d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%rbx), %eax
	movq	1168(%rbx), %rcx
	movl	1176(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	1176(%rbx), %eax
	cmpl	1180(%rbx), %eax
	jne	.LBB14_34
# BB#33:                                #   in Loop: Header=BB14_30 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_34:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i87
                                        #   in Loop: Header=BB14_30 Depth=2
	movzbl	1224(%rbx), %ecx
	shrl	%cl, %r15d
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
	movl	$8, %edx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB14_30
	jmp	.LBB14_35
.LBB14_28:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit82._ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit88_crit_edge
                                        #   in Loop: Header=BB14_2 Depth=1
	movl	1224(%rbx), %edx
	jmp	.LBB14_35
.LBB14_31:                              #   in Loop: Header=BB14_2 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r15d, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%rbx), %esi
	movb	%sil, 1228(%rbx)
	subl	%eax, %edx
	movl	%edx, 1224(%rbx)
.LBB14_35:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit88
                                        #   in Loop: Header=BB14_2 Depth=1
	addl	$-3, %r14d
	movl	$2, %eax
	.p2align	4, 0x90
.LBB14_36:                              #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB14_37
# BB#39:                                #   in Loop: Header=BB14_36 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r14d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%rbx), %eax
	movq	1168(%rbx), %rcx
	movl	1176(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	1176(%rbx), %eax
	cmpl	1180(%rbx), %eax
	jne	.LBB14_41
# BB#40:                                #   in Loop: Header=BB14_36 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_41:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i93
                                        #   in Loop: Header=BB14_36 Depth=2
	movzbl	1224(%rbx), %ecx
	shrl	%cl, %r14d
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
	movl	$8, %edx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB14_36
	jmp	.LBB14_70
.LBB14_37:                              #   in Loop: Header=BB14_2 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r14d, %esi
	jmp	.LBB14_38
.LBB14_44:                              # %._ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit100_crit_edge
                                        #   in Loop: Header=BB14_2 Depth=1
	movl	1224(%rbx), %edx
	jmp	.LBB14_51
.LBB14_58:                              # %._ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit112_crit_edge
                                        #   in Loop: Header=BB14_2 Depth=1
	movl	1224(%rbx), %edx
	jmp	.LBB14_65
.LBB14_47:                              #   in Loop: Header=BB14_2 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r14d, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%rbx), %esi
	movb	%sil, 1228(%rbx)
	subl	%eax, %edx
	movl	%edx, 1224(%rbx)
.LBB14_51:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit100
                                        #   in Loop: Header=BB14_2 Depth=1
	addl	$-2, %r12d
	movl	$3, %eax
	.p2align	4, 0x90
.LBB14_52:                              #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB14_53
# BB#54:                                #   in Loop: Header=BB14_52 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r12d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%rbx), %eax
	movq	1168(%rbx), %rcx
	movl	1176(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	1176(%rbx), %eax
	cmpl	1180(%rbx), %eax
	jne	.LBB14_56
# BB#55:                                #   in Loop: Header=BB14_52 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_56:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i105
                                        #   in Loop: Header=BB14_52 Depth=2
	movzbl	1224(%rbx), %ecx
	shrl	%cl, %r12d
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
	movl	$8, %edx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB14_52
	jmp	.LBB14_70
.LBB14_61:                              #   in Loop: Header=BB14_2 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r14d, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%rbx), %esi
	movb	%sil, 1228(%rbx)
	subl	%eax, %edx
	movl	%edx, 1224(%rbx)
.LBB14_65:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit112
                                        #   in Loop: Header=BB14_2 Depth=1
	addl	$-10, %r12d
	movl	$7, %eax
	.p2align	4, 0x90
.LBB14_66:                              #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB14_53
# BB#67:                                #   in Loop: Header=BB14_66 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r12d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%rbx), %eax
	movq	1168(%rbx), %rcx
	movl	1176(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	1176(%rbx), %eax
	cmpl	1180(%rbx), %eax
	jne	.LBB14_69
# BB#68:                                #   in Loop: Header=BB14_66 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB14_69:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i117
                                        #   in Loop: Header=BB14_66 Depth=2
	movzbl	1224(%rbx), %ecx
	shrl	%cl, %r12d
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
	movl	$8, %edx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB14_66
	jmp	.LBB14_70
.LBB14_53:                              #   in Loop: Header=BB14_2 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r12d, %esi
.LBB14_38:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit94
                                        #   in Loop: Header=BB14_2 Depth=1
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%rbx), %esi
	movb	%sil, 1228(%rbx)
	subl	%eax, %edx
	movl	%edx, 1224(%rbx)
.LBB14_70:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit94
                                        #   in Loop: Header=BB14_2 Depth=1
	xorl	%r14d, %r14d
	movl	36(%rsp), %r15d         # 4-byte Reload
	testl	%r15d, %r15d
	je	.LBB14_71
# BB#72:                                #   in Loop: Header=BB14_2 Depth=1
	xorl	%eax, %eax
	movl	12(%rsp), %edx          # 4-byte Reload
	cmpl	%r15d, %edx
	setne	%al
	leal	6(%rax), %r8d
	addl	$3, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	jmp	.LBB14_73
.LBB14_71:                              #   in Loop: Header=BB14_2 Depth=1
	movl	$3, %eax
	movl	$138, %r8d
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	.p2align	4, 0x90
.LBB14_73:                              #   in Loop: Header=BB14_2 Depth=1
	incq	%r13
	cmpq	%rbp, %r13
	movl	%r14d, %r12d
	jne	.LBB14_2
.LBB14_74:                              # %._crit_edge
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder14LevelTableCodeEPKhiS4_PKj, .Lfunc_end14-_ZN9NCompress8NDeflate8NEncoder6CCoder14LevelTableCodeEPKhiS4_PKj
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder10MakeTablesEj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder10MakeTablesEj,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder10MakeTablesEj: # @_ZN9NCompress8NDeflate8NEncoder6CCoder10MakeTablesEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 32
.Lcfi85:
	.cfi_offset %rbx, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	2256(%rbx), %rdi
	leaq	3536(%rbx), %rsi
	leaq	1936(%rbx), %rdx
	movl	$288, %ecx              # imm = 0x120
	movl	%ebp, %r8d
	callq	Huffman_Generate
	leaq	3408(%rbx), %rdi
	leaq	4688(%rbx), %rsi
	leaq	2224(%rbx), %rdx
	movl	$32, %ecx
	movl	%ebp, %r8d
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	Huffman_Generate        # TAILCALL
.Lfunc_end15:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder10MakeTablesEj, .Lfunc_end15-_ZN9NCompress8NDeflate8NEncoder6CCoder10MakeTablesEj
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder16Huffman_GetPriceEPKjPKhj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder16Huffman_GetPriceEPKjPKhj,@function
_ZN9NCompress8NDeflate8NEncoder16Huffman_GetPriceEPKjPKhj: # @_ZN9NCompress8NDeflate8NEncoder16Huffman_GetPriceEPKjPKhj
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	je	.LBB16_1
# BB#2:                                 # %.lr.ph.preheader
	movl	%edx, %ecx
	cmpl	$8, %edx
	jb	.LBB16_3
# BB#4:                                 # %min.iters.checked
	andl	$7, %edx
	movq	%rcx, %r8
	subq	%rdx, %r8
	je	.LBB16_3
# BB#5:                                 # %vector.body.preheader
	leaq	4(%rsi), %r9
	leaq	16(%rdi), %rax
	pxor	%xmm1, %xmm1
	movq	%r8, %r10
	pxor	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB16_6:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%r9), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movd	(%r9), %xmm4            # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3],xmm3[4],xmm1[4],xmm3[5],xmm1[5],xmm3[6],xmm1[6],xmm3[7],xmm1[7]
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	movdqu	-16(%rax), %xmm5
	movdqu	(%rax), %xmm6
	pshufd	$245, %xmm3, %xmm7      # xmm7 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm7, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm6, %xmm6      # xmm6 = xmm6[1,1,3,3]
	pmuludq	%xmm5, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	paddd	%xmm3, %xmm2
	paddd	%xmm4, %xmm0
	addq	$8, %r9
	addq	$32, %rax
	addq	$-8, %r10
	jne	.LBB16_6
# BB#7:                                 # %middle.block
	paddd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	testl	%edx, %edx
	jne	.LBB16_8
	jmp	.LBB16_10
.LBB16_3:
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.LBB16_8:                               # %.lr.ph.preheader22
	addq	%r8, %rsi
	leaq	(%rdi,%r8,4), %rdx
	subq	%r8, %rcx
	.p2align	4, 0x90
.LBB16_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %edi
	imull	(%rdx), %edi
	addl	%edi, %eax
	incq	%rsi
	addq	$4, %rdx
	decq	%rcx
	jne	.LBB16_9
.LBB16_10:                              # %._crit_edge
	retq
.LBB16_1:
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	_ZN9NCompress8NDeflate8NEncoder16Huffman_GetPriceEPKjPKhj, .Lfunc_end16-_ZN9NCompress8NDeflate8NEncoder16Huffman_GetPriceEPKjPKhj
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder21Huffman_GetPrice_SpecEPKjPKhjS5_j
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder21Huffman_GetPrice_SpecEPKjPKhjS5_j,@function
_ZN9NCompress8NDeflate8NEncoder21Huffman_GetPrice_SpecEPKjPKhjS5_j: # @_ZN9NCompress8NDeflate8NEncoder21Huffman_GetPrice_SpecEPKjPKhjS5_j
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 32
.Lcfi90:
	.cfi_offset %rbx, -32
.Lcfi91:
	.cfi_offset %r14, -24
.Lcfi92:
	.cfi_offset %r15, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	xorl	%eax, %eax
	testl	%edx, %edx
	movl	$0, %r9d
	je	.LBB17_9
# BB#1:                                 # %.lr.ph.preheader.i
	movl	%edx, %r10d
	cmpl	$8, %edx
	jb	.LBB17_2
# BB#3:                                 # %min.iters.checked
	movl	%edx, %r14d
	andl	$7, %r14d
	movq	%r10, %r11
	subq	%r14, %r11
	je	.LBB17_2
# BB#4:                                 # %vector.body.preheader
	leaq	4(%rsi), %r9
	leaq	16(%rdi), %rbx
	pxor	%xmm1, %xmm1
	movq	%r11, %r15
	pxor	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB17_5:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%r9), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movd	(%r9), %xmm4            # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3],xmm3[4],xmm1[4],xmm3[5],xmm1[5],xmm3[6],xmm1[6],xmm3[7],xmm1[7]
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	movdqu	-16(%rbx), %xmm5
	movdqu	(%rbx), %xmm6
	pshufd	$245, %xmm3, %xmm7      # xmm7 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm7, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm6, %xmm6      # xmm6 = xmm6[1,1,3,3]
	pmuludq	%xmm5, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	paddd	%xmm3, %xmm2
	paddd	%xmm4, %xmm0
	addq	$8, %r9
	addq	$32, %rbx
	addq	$-8, %r15
	jne	.LBB17_5
# BB#6:                                 # %middle.block
	paddd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %r9d
	testl	%r14d, %r14d
	jne	.LBB17_7
	jmp	.LBB17_9
.LBB17_2:
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
.LBB17_7:                               # %.lr.ph.i.preheader
	subq	%r11, %r10
	leaq	(%rdi,%r11,4), %r14
	addq	%r11, %rsi
	.p2align	4, 0x90
.LBB17_8:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %ebx
	imull	(%r14), %ebx
	addl	%ebx, %r9d
	addq	$4, %r14
	incq	%rsi
	decq	%r10
	jne	.LBB17_8
.LBB17_9:                               # %_ZN9NCompress8NDeflate8NEncoder16Huffman_GetPriceEPKjPKhj.exit
	subl	%r8d, %edx
	je	.LBB17_18
# BB#10:                                # %.lr.ph.preheader.i8
	movl	%r8d, %r8d
	movl	%edx, %esi
	cmpl	$8, %edx
	jb	.LBB17_11
# BB#12:                                # %min.iters.checked32
	andl	$7, %edx
	movq	%rsi, %r10
	subq	%rdx, %r10
	je	.LBB17_11
# BB#13:                                # %vector.body28.preheader
	leaq	4(%rcx), %rax
	leaq	16(%rdi,%r8,4), %rbx
	pxor	%xmm1, %xmm1
	movq	%r10, %r11
	pxor	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB17_14:                              # %vector.body28
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movd	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3],xmm3[4],xmm1[4],xmm3[5],xmm1[5],xmm3[6],xmm1[6],xmm3[7],xmm1[7]
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	movdqu	-16(%rbx), %xmm5
	movdqu	(%rbx), %xmm6
	pshufd	$245, %xmm3, %xmm7      # xmm7 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm7, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm6, %xmm6      # xmm6 = xmm6[1,1,3,3]
	pmuludq	%xmm5, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	paddd	%xmm3, %xmm2
	paddd	%xmm4, %xmm0
	addq	$8, %rax
	addq	$32, %rbx
	addq	$-8, %r11
	jne	.LBB17_14
# BB#15:                                # %middle.block29
	paddd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	testl	%edx, %edx
	jne	.LBB17_16
	jmp	.LBB17_18
.LBB17_11:
	xorl	%r10d, %r10d
	xorl	%eax, %eax
.LBB17_16:                              # %.lr.ph.i13.preheader
	subq	%r10, %rsi
	addq	%r10, %r8
	leaq	(%rdi,%r8,4), %rdx
	addq	%r10, %rcx
	.p2align	4, 0x90
.LBB17_17:                              # %.lr.ph.i13
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edi
	imull	(%rdx), %edi
	addl	%edi, %eax
	addq	$4, %rdx
	incq	%rcx
	decq	%rsi
	jne	.LBB17_17
.LBB17_18:                              # %_ZN9NCompress8NDeflate8NEncoder16Huffman_GetPriceEPKjPKhj.exit15
	addl	%r9d, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	_ZN9NCompress8NDeflate8NEncoder21Huffman_GetPrice_SpecEPKjPKhjS5_j, .Lfunc_end17-_ZN9NCompress8NDeflate8NEncoder21Huffman_GetPrice_SpecEPKjPKhjS5_j
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
.LCPI18_1:
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI18_2:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
.LCPI18_3:
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI18_4:
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
.LCPI18_5:
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
.LCPI18_6:
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.text
	.globl	_ZNK9NCompress8NDeflate8NEncoder6CCoder15GetLzBlockPriceEv
	.p2align	4, 0x90
	.type	_ZNK9NCompress8NDeflate8NEncoder6CCoder15GetLzBlockPriceEv,@function
_ZNK9NCompress8NDeflate8NEncoder6CCoder15GetLzBlockPriceEv: # @_ZNK9NCompress8NDeflate8NEncoder6CCoder15GetLzBlockPriceEv
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movq	1328(%rdi), %rax
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB18_1:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	1936(%rdi,%rcx), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movd	1940(%rdi,%rcx), %xmm4  # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	movdqu	2256(%rdi,%rcx,4), %xmm5
	movdqu	2272(%rdi,%rcx,4), %xmm6
	pshufd	$245, %xmm3, %xmm7      # xmm7 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm7, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm6, %xmm6      # xmm6 = xmm6[1,1,3,3]
	pmuludq	%xmm5, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	paddd	%xmm1, %xmm3
	paddd	%xmm2, %xmm4
	movd	1944(%rdi,%rcx), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movd	1948(%rdi,%rcx), %xmm2  # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movdqu	2288(%rdi,%rcx,4), %xmm5
	movdqu	2304(%rdi,%rcx,4), %xmm6
	pshufd	$245, %xmm1, %xmm7      # xmm7 = xmm1[1,1,3,3]
	pmuludq	%xmm5, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm7, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	pshufd	$245, %xmm2, %xmm5      # xmm5 = xmm2[1,1,3,3]
	pmuludq	%xmm6, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm6, %xmm6      # xmm6 = xmm6[1,1,3,3]
	pmuludq	%xmm5, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	punpckldq	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	paddd	%xmm3, %xmm1
	paddd	%xmm4, %xmm2
	addq	$16, %rcx
	cmpq	$288, %rcx              # imm = 0x120
	jne	.LBB18_1
# BB#2:                                 # %vector.body73
	paddd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r8d
	movd	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	pxor	%xmm8, %xmm8
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	movdqu	3284(%rdi), %xmm4
	movdqu	3300(%rdi), %xmm5
	movdqu	3316(%rdi), %xmm1
	movdqu	3332(%rdi), %xmm2
	pshufd	$245, %xmm3, %xmm6      # xmm6 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm7      # xmm7 = xmm3[0,2,2,3]
	pshufd	$245, %xmm4, %xmm3      # xmm3 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	movd	4(%rax), %xmm3          # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	pshufd	$245, %xmm3, %xmm4      # xmm4 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm4, %xmm5
	pshufd	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddd	%xmm7, %xmm3
	movd	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3],xmm4[4],xmm8[4],xmm4[5],xmm8[5],xmm4[6],xmm8[6],xmm4[7],xmm8[7]
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm5, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movd	12(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	pshufd	$245, %xmm1, %xmm5      # xmm5 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm2, %xmm2      # xmm2 = xmm2[1,1,3,3]
	pmuludq	%xmm5, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	paddd	%xmm4, %xmm1
	paddd	%xmm3, %xmm1
	movd	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3],xmm2[4],xmm8[4],xmm2[5],xmm8[5],xmm2[6],xmm8[6],xmm2[7],xmm8[7]
	punpcklwd	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3]
	movdqu	3348(%rdi), %xmm3
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movd	20(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	movdqu	3364(%rdi), %xmm4
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddd	%xmm2, %xmm3
	movd	24(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3],xmm2[4],xmm8[4],xmm2[5],xmm8[5],xmm2[6],xmm8[6],xmm2[7],xmm8[7]
	punpcklwd	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3]
	movdqu	3380(%rdi), %xmm4
	pshufd	$245, %xmm2, %xmm5      # xmm5 = xmm2[1,1,3,3]
	pmuludq	%xmm4, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	paddd	%xmm3, %xmm2
	paddd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm1       # xmm1 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm1
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm2
	movd	%xmm2, %r9d
	movd	2224(%rdi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	movdqu	3408(%rdi), %xmm2
	pshufd	$245, %xmm1, %xmm3      # xmm3 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm4      # xmm4 = xmm1[0,2,2,3]
	pshufd	$245, %xmm2, %xmm1      # xmm1 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movd	2228(%rdi), %xmm2       # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3],xmm2[4],xmm8[4],xmm2[5],xmm8[5],xmm2[6],xmm8[6],xmm2[7],xmm8[7]
	punpcklwd	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3]
	movdqu	3424(%rdi), %xmm14
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm14, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm14, %xmm0     # xmm0 = xmm14[1,1,3,3]
	movdqa	%xmm0, -24(%rsp)        # 16-byte Spill
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	paddd	%xmm4, %xmm2
	movd	2232(%rdi), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3],xmm4[4],xmm8[4],xmm4[5],xmm8[5],xmm4[6],xmm8[6],xmm4[7],xmm8[7]
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	movdqu	3440(%rdi), %xmm15
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm15, %xmm4
	pshufd	$232, %xmm4, %xmm7      # xmm7 = xmm4[0,2,2,3]
	pshufd	$245, %xmm15, %xmm9     # xmm9 = xmm15[1,1,3,3]
	pmuludq	%xmm9, %xmm5
	pshufd	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3]
	punpckldq	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	movd	2236(%rdi), %xmm5       # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	punpcklwd	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3]
	movdqu	3456(%rdi), %xmm4
	pshufd	$245, %xmm5, %xmm1      # xmm1 = xmm5[1,1,3,3]
	pmuludq	%xmm4, %xmm5
	pshufd	$232, %xmm5, %xmm6      # xmm6 = xmm5[0,2,2,3]
	pshufd	$245, %xmm4, %xmm10     # xmm10 = xmm4[1,1,3,3]
	pmuludq	%xmm10, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	paddd	%xmm7, %xmm6
	paddd	%xmm2, %xmm6
	movd	2240(%rdi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	movdqu	3472(%rdi), %xmm7
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm7, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm7, %xmm11     # xmm11 = xmm7[1,1,3,3]
	pmuludq	%xmm11, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movd	2244(%rdi), %xmm5       # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	punpcklwd	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3]
	movdqu	3488(%rdi), %xmm2
	pshufd	$245, %xmm5, %xmm3      # xmm3 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm13     # xmm13 = xmm5[0,2,2,3]
	pshufd	$245, %xmm2, %xmm12     # xmm12 = xmm2[1,1,3,3]
	pmuludq	%xmm12, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm13   # xmm13 = xmm13[0],xmm3[0],xmm13[1],xmm3[1]
	paddd	%xmm1, %xmm13
	movd	2248(%rdi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	movdqu	3504(%rdi), %xmm5
	pshufd	$245, %xmm1, %xmm0      # xmm0 = xmm1[1,1,3,3]
	pmuludq	%xmm5, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm5, %xmm3      # xmm3 = xmm5[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	paddd	%xmm13, %xmm1
	paddd	%xmm6, %xmm1
	movd	2252(%rdi), %xmm6       # xmm6 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3],xmm6[4],xmm8[4],xmm6[5],xmm8[5],xmm6[6],xmm8[6],xmm6[7],xmm8[7]
	punpcklwd	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3]
	movdqu	3520(%rdi), %xmm8
	pshufd	$245, %xmm6, %xmm0      # xmm0 = xmm6[1,1,3,3]
	pmuludq	%xmm8, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm8, %xmm13     # xmm13 = xmm8[1,1,3,3]
	pmuludq	%xmm13, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm1, %xmm6
	pshufd	$78, %xmm6, %xmm0       # xmm0 = xmm6[2,3,0,1]
	paddd	%xmm6, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r10d
	movdqa	.LCPI18_0(%rip), %xmm0  # xmm0 = [1,1,2,2]
	pmuludq	%xmm0, %xmm14
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	-24(%rsp), %xmm0        # 16-byte Folded Reload
	pshufd	$232, %xmm14, %xmm1     # xmm1 = xmm14[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movdqa	.LCPI18_1(%rip), %xmm0  # xmm0 = [3,3,4,4]
	pmuludq	%xmm0, %xmm15
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm9, %xmm0
	pshufd	$232, %xmm15, %xmm6     # xmm6 = xmm15[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm1, %xmm6
	movdqa	.LCPI18_2(%rip), %xmm0  # xmm0 = [5,5,6,6]
	pmuludq	%xmm0, %xmm4
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm10, %xmm0
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	movdqa	.LCPI18_3(%rip), %xmm0  # xmm0 = [7,7,8,8]
	pmuludq	%xmm0, %xmm7
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm11, %xmm0
	pshufd	$232, %xmm7, %xmm1      # xmm1 = xmm7[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	paddd	%xmm4, %xmm1
	paddd	%xmm6, %xmm1
	movdqa	.LCPI18_4(%rip), %xmm0  # xmm0 = [9,9,10,10]
	pmuludq	%xmm0, %xmm2
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm12, %xmm0
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movdqa	.LCPI18_5(%rip), %xmm0  # xmm0 = [11,11,12,12]
	pmuludq	%xmm0, %xmm5
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm5, %xmm3      # xmm3 = xmm5[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	paddd	%xmm2, %xmm3
	movdqa	.LCPI18_6(%rip), %xmm0  # xmm0 = [13,13,14,14]
	pmuludq	%xmm0, %xmm8
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm13, %xmm0
	pshufd	$232, %xmm8, %xmm2      # xmm2 = xmm8[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	paddd	%xmm3, %xmm2
	paddd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %ecx
	movzbl	28(%rax), %edx
	imull	3396(%rdi), %edx
	addl	%r9d, %edx
	movzbl	29(%rax), %esi
	imull	3400(%rdi), %esi
	addl	%edx, %esi
	movzbl	30(%rax), %eax
	imull	3404(%rdi), %eax
	addl	%esi, %eax
	addl	%r8d, %eax
	addl	%r10d, %eax
	addl	%ecx, %eax
	retq
.Lfunc_end18:
	.size	_ZNK9NCompress8NDeflate8NEncoder6CCoder15GetLzBlockPriceEv, .Lfunc_end18-_ZNK9NCompress8NDeflate8NEncoder6CCoder15GetLzBlockPriceEv
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder8TryBlockEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder8TryBlockEv,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder8TryBlockEv: # @_ZN9NCompress8NDeflate8NEncoder6CCoder8TryBlockEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi98:
	.cfi_def_cfa_offset 64
.Lcfi99:
	.cfi_offset %rbx, -48
.Lcfi100:
	.cfi_offset %r12, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	2256(%rbx), %rdi
	movl	$0, 1372(%rbx)
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$1280, %edx             # imm = 0x500
	callq	memset
	movl	4912(%rbx), %r12d
	movl	$0, 4912(%rbx)
	leaq	12(%rsp), %r14
	xorl	%r15d, %r15d
	jmp	.LBB19_2
	.p2align	4, 0x90
.LBB19_1:                               #   in Loop: Header=BB19_2 Depth=1
	subl	%r15d, %edx
	movl	%edx, 1380(%rbx)
	addl	4912(%rbx), %r15d
	movl	%r15d, 4912(%rbx)
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movl	1388(%rbx), %eax
	cmpl	1384(%rbx), %eax
	jne	.LBB19_8
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	cmpl	$653285, 1288(%rbx)     # imm = 0x9F7E5
	ja	.LBB19_24
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=1
	cmpl	%r12d, %r15d
	jae	.LBB19_24
# BB#5:                                 #   in Loop: Header=BB19_2 Depth=1
	cmpb	$0, 1376(%rbx)
	jne	.LBB19_8
# BB#6:                                 #   in Loop: Header=BB19_2 Depth=1
	movl	16(%rbx), %eax
	cmpl	8(%rbx), %eax
	je	.LBB19_24
# BB#7:                                 #   in Loop: Header=BB19_2 Depth=1
	cmpl	1304(%rbx), %ebp
	jae	.LBB19_24
	.p2align	4, 0x90
.LBB19_8:                               #   in Loop: Header=BB19_2 Depth=1
	cmpb	$0, 1268(%rbx)
	je	.LBB19_14
# BB#9:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetMatchesEv
	movq	1256(%rbx), %rax
	movzwl	(%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB19_15
# BB#10:                                #   in Loop: Header=BB19_2 Depth=1
	leal	-1(%rcx), %edx
	movzwl	(%rax,%rdx,2), %r15d
	movzwl	(%rax,%rcx,2), %eax
	movl	%eax, 12(%rsp)
	movl	%r15d, %ebp
	decl	%ebp
	je	.LBB19_18
# BB#11:                                #   in Loop: Header=BB19_2 Depth=1
	movzbl	1376(%rbx), %eax
	testb	%al, %al
	jne	.LBB19_18
# BB#12:                                #   in Loop: Header=BB19_2 Depth=1
	cmpb	$0, 1269(%rbx)
	je	.LBB19_16
# BB#13:                                #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	Bt3Zip_MatchFinder_Skip
	jmp	.LBB19_17
	.p2align	4, 0x90
.LBB19_14:                              #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder10GetOptimalERj
	movl	%eax, %r15d
	jmp	.LBB19_18
	.p2align	4, 0x90
.LBB19_15:                              #   in Loop: Header=BB19_2 Depth=1
	movl	$1, %r15d
	jmp	.LBB19_18
.LBB19_16:                              #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	Hc3Zip_MatchFinder_Skip
.LBB19_17:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder14GetOptimalFastERj.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	addl	%ebp, 1380(%rbx)
	.p2align	4, 0x90
.LBB19_18:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder14GetOptimalFastERj.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	1248(%rbx), %rax
	movl	1372(%rbx), %ecx
	leal	1(%rcx), %ebp
	movl	%ebp, 1372(%rbx)
	cmpl	$3, %r15d
	jb	.LBB19_21
# BB#19:                                #   in Loop: Header=BB19_2 Depth=1
	leal	-3(%r15), %edx
	movw	%dx, (%rax,%rcx,4)
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%rdx), %edx
	incl	3284(%rbx,%rdx,4)
	movl	12(%rsp), %edx
	cmpq	$511, %rdx              # imm = 0x1FF
	movw	%dx, 2(%rax,%rcx,4)
	ja	.LBB19_22
# BB#20:                                #   in Loop: Header=BB19_2 Depth=1
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rdx), %eax
	jmp	.LBB19_23
	.p2align	4, 0x90
.LBB19_21:                              #   in Loop: Header=BB19_2 Depth=1
	movq	(%rbx), %rsi
	movl	1380(%rbx), %edx
	movl	%edx, %edi
	negl	%edi
	movslq	%edi, %rdi
	movzbl	(%rsi,%rdi), %esi
	movzwl	%si, %esi
	incl	2256(%rbx,%rsi,4)
	movw	$-32768, (%rax,%rcx,4)  # imm = 0x8000
	movw	%si, 2(%rax,%rcx,4)
	jmp	.LBB19_1
	.p2align	4, 0x90
.LBB19_22:                              #   in Loop: Header=BB19_2 Depth=1
	shrl	$8, %edx
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rdx), %eax
	addq	$16, %rax
.LBB19_23:                              # %_ZN9NCompress8NDeflate8NEncoder10GetPosSlotEj.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movl	%eax, %eax
	incl	3408(%rbx,%rax,4)
	movl	1380(%rbx), %edx
	jmp	.LBB19_1
.LBB19_24:
	incl	3280(%rbx)
	addl	%r15d, 1380(%rbx)
	movb	$1, 1376(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder8TryBlockEv, .Lfunc_end19-_ZN9NCompress8NDeflate8NEncoder6CCoder8TryBlockEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI20_0:
	.zero	16,11
.LCPI20_1:
	.zero	16,6
.LCPI20_2:
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	6                       # 0x6
.LCPI20_3:
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	14                      # 0xe
	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE: # @_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE
	.cfi_startproc
# BB#0:
	cmpb	$0, 1268(%rdi)
	jne	.LBB20_33
# BB#1:                                 # %vector.memcheck
	leaq	1392(%rdi), %rax
	leaq	256(%rsi), %rcx
	cmpq	%rcx, %rax
	jae	.LBB20_13
# BB#2:                                 # %vector.memcheck
	leaq	1648(%rdi), %rax
	cmpq	%rax, %rsi
	jae	.LBB20_13
# BB#3:                                 # %.preheader28.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB20_4:                               # %.preheader28
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi,%rax), %ecx
	testb	%cl, %cl
	jne	.LBB20_6
# BB#5:                                 # %.preheader28
                                        #   in Loop: Header=BB20_4 Depth=1
	movb	$11, %cl
.LBB20_6:                               # %.preheader28
                                        #   in Loop: Header=BB20_4 Depth=1
	movb	%cl, 1392(%rdi,%rax)
	movzbl	1(%rsi,%rax), %ecx
	testb	%cl, %cl
	jne	.LBB20_8
# BB#7:                                 # %.preheader28
                                        #   in Loop: Header=BB20_4 Depth=1
	movb	$11, %cl
.LBB20_8:                               # %.preheader28
                                        #   in Loop: Header=BB20_4 Depth=1
	movb	%cl, 1393(%rdi,%rax)
	movzbl	2(%rsi,%rax), %ecx
	testb	%cl, %cl
	jne	.LBB20_10
# BB#9:                                 # %.preheader28
                                        #   in Loop: Header=BB20_4 Depth=1
	movb	$11, %cl
.LBB20_10:                              # %.preheader28
                                        #   in Loop: Header=BB20_4 Depth=1
	movb	%cl, 1394(%rdi,%rax)
	movzbl	3(%rsi,%rax), %ecx
	testb	%cl, %cl
	jne	.LBB20_12
# BB#11:                                # %.preheader28
                                        #   in Loop: Header=BB20_4 Depth=1
	movb	$11, %cl
.LBB20_12:                              # %.preheader28
                                        #   in Loop: Header=BB20_4 Depth=1
	movb	%cl, 1395(%rdi,%rax)
	addq	$4, %rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB20_4
	jmp	.LBB20_14
.LBB20_13:                              # %vector.body
	movdqu	(%rsi), %xmm2
	movdqu	16(%rsi), %xmm3
	pxor	%xmm0, %xmm0
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	pcmpeqd	%xmm1, %xmm1
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	movdqa	.LCPI20_0(%rip), %xmm8  # xmm8 = [11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11]
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1392(%rdi)
	movdqu	%xmm7, 1408(%rdi)
	movdqu	32(%rsi), %xmm3
	movdqu	48(%rsi), %xmm2
	movdqa	%xmm3, %xmm5
	pcmpeqb	%xmm0, %xmm5
	movdqa	%xmm5, %xmm6
	pxor	%xmm1, %xmm6
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm3, %xmm5
	pandn	%xmm8, %xmm6
	por	%xmm5, %xmm6
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm7
	por	%xmm4, %xmm7
	movdqu	%xmm6, 1424(%rdi)
	movdqu	%xmm7, 1440(%rdi)
	movdqu	64(%rsi), %xmm2
	movdqu	80(%rsi), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1456(%rdi)
	movdqu	%xmm7, 1472(%rdi)
	movdqu	96(%rsi), %xmm2
	movdqu	112(%rsi), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1488(%rdi)
	movdqu	%xmm7, 1504(%rdi)
	movdqu	128(%rsi), %xmm2
	movdqu	144(%rsi), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1520(%rdi)
	movdqu	%xmm7, 1536(%rdi)
	movdqu	160(%rsi), %xmm2
	movdqu	176(%rsi), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1552(%rdi)
	movdqu	%xmm7, 1568(%rdi)
	movdqu	192(%rsi), %xmm2
	movdqu	208(%rsi), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1584(%rdi)
	movdqu	%xmm7, 1600(%rdi)
	movdqu	224(%rsi), %xmm2
	movdqu	240(%rsi), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	pcmpeqb	%xmm3, %xmm0
	pxor	%xmm0, %xmm1
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm0
	pandn	%xmm8, %xmm1
	por	%xmm0, %xmm1
	movdqu	%xmm5, 1616(%rdi)
	movdqu	%xmm1, 1632(%rdi)
.LBB20_14:                              # %.preheader27
	movl	1308(%rdi), %r8d
	testl	%r8d, %r8d
	je	.LBB20_25
# BB#15:                                # %.lr.ph
	movq	1328(%rdi), %r9
	xorl	%edx, %edx
	testb	$1, %r8b
	je	.LBB20_19
# BB#16:
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%rip), %eax
	movb	257(%rsi,%rax), %cl
	testb	%cl, %cl
	jne	.LBB20_18
# BB#17:
	movb	$11, %cl
.LBB20_18:
	addb	(%r9,%rax), %cl
	movb	%cl, 1648(%rdi)
	movl	$1, %edx
.LBB20_19:                              # %.prol.loopexit
	cmpl	$1, %r8d
	je	.LBB20_25
	.p2align	4, 0x90
.LBB20_20:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %r10d
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%r10), %ecx
	movzbl	257(%rsi,%rcx), %eax
	testb	%al, %al
	jne	.LBB20_22
# BB#21:                                #   in Loop: Header=BB20_20 Depth=1
	movb	$11, %al
.LBB20_22:                              #   in Loop: Header=BB20_20 Depth=1
	addb	(%r9,%rcx), %al
	movb	%al, 1648(%rdi,%r10)
	leal	1(%rdx), %r10d
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%r10), %ecx
	movzbl	257(%rsi,%rcx), %eax
	testb	%al, %al
	jne	.LBB20_24
# BB#23:                                #   in Loop: Header=BB20_20 Depth=1
	movb	$11, %al
.LBB20_24:                              #   in Loop: Header=BB20_20 Depth=1
	addb	(%r9,%rcx), %al
	movb	%al, 1648(%rdi,%r10)
	addl	$2, %edx
	cmpl	%r8d, %edx
	jb	.LBB20_20
.LBB20_25:                              # %vector.memcheck53
	leaq	1904(%rdi), %rax
	leaq	320(%rsi), %rcx
	cmpq	%rcx, %rax
	jae	.LBB20_34
# BB#26:                                # %vector.memcheck53
	leaq	1936(%rdi), %rax
	leaq	288(%rsi), %rcx
	cmpq	%rax, %rcx
	jae	.LBB20_34
# BB#27:                                # %.preheader.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB20_28:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	288(%rsi,%rax), %ecx
	testb	%cl, %cl
	jne	.LBB20_30
# BB#29:                                # %.preheader
                                        #   in Loop: Header=BB20_28 Depth=1
	movb	$6, %cl
.LBB20_30:                              # %.preheader
                                        #   in Loop: Header=BB20_28 Depth=1
	addb	_ZN9NCompress8NDeflateL15kDistDirectBitsE(%rax), %cl
	movb	%cl, 1904(%rdi,%rax)
	movzbl	289(%rsi,%rax), %ecx
	testb	%cl, %cl
	jne	.LBB20_32
# BB#31:                                # %.preheader
                                        #   in Loop: Header=BB20_28 Depth=1
	movb	$6, %cl
.LBB20_32:                              # %.preheader
                                        #   in Loop: Header=BB20_28 Depth=1
	addb	_ZN9NCompress8NDeflateL15kDistDirectBitsE+1(%rax), %cl
	movb	%cl, 1905(%rdi,%rax)
	addq	$2, %rax
	cmpq	$32, %rax
	jne	.LBB20_28
.LBB20_33:                              # %.loopexit
	retq
.LBB20_34:                              # %vector.body39
	movdqu	288(%rsi), %xmm0
	pxor	%xmm1, %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqb	%xmm1, %xmm2
	pcmpeqd	%xmm3, %xmm3
	movdqa	%xmm2, %xmm4
	pxor	%xmm3, %xmm4
	pandn	%xmm0, %xmm2
	movdqa	.LCPI20_1(%rip), %xmm0  # xmm0 = [6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6]
	pandn	%xmm0, %xmm4
	por	%xmm2, %xmm4
	paddb	.LCPI20_2(%rip), %xmm4
	movdqu	%xmm4, 1904(%rdi)
	movdqu	304(%rsi), %xmm2
	pcmpeqb	%xmm2, %xmm1
	pxor	%xmm1, %xmm3
	pandn	%xmm2, %xmm1
	pandn	%xmm0, %xmm3
	por	%xmm1, %xmm3
	paddb	.LCPI20_3(%rip), %xmm3
	movdqu	%xmm3, 1920(%rdi)
	retq
.Lfunc_end20:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE, .Lfunc_end20-_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI21_0:
	.long	43690                   # 0xaaaa
	.long	43690                   # 0xaaaa
	.long	43690                   # 0xaaaa
	.long	43690                   # 0xaaaa
.LCPI21_1:
	.long	21845                   # 0x5555
	.long	21845                   # 0x5555
	.long	21845                   # 0x5555
	.long	21845                   # 0x5555
.LCPI21_2:
	.long	52428                   # 0xcccc
	.long	52428                   # 0xcccc
	.long	52428                   # 0xcccc
	.long	52428                   # 0xcccc
.LCPI21_3:
	.long	13107                   # 0x3333
	.long	13107                   # 0x3333
	.long	13107                   # 0x3333
	.long	13107                   # 0x3333
.LCPI21_4:
	.long	61680                   # 0xf0f0
	.long	61680                   # 0xf0f0
	.long	61680                   # 0xf0f0
	.long	61680                   # 0xf0f0
.LCPI21_5:
	.long	3855                    # 0xf0f
	.long	3855                    # 0xf0f
	.long	3855                    # 0xf0f
	.long	3855                    # 0xf0f
.LCPI21_6:
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI21_7:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder19Huffman_ReverseBitsEPjPKhj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder19Huffman_ReverseBitsEPjPKhj,@function
_ZN9NCompress8NDeflate8NEncoder19Huffman_ReverseBitsEPjPKhj: # @_ZN9NCompress8NDeflate8NEncoder19Huffman_ReverseBitsEPjPKhj
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	je	.LBB21_11
# BB#1:                                 # %.lr.ph.preheader
	movl	%edx, %r11d
	cmpl	$3, %edx
	jbe	.LBB21_2
# BB#5:                                 # %min.iters.checked
	andl	$3, %edx
	movq	%r11, %r8
	subq	%rdx, %r8
	je	.LBB21_2
# BB#6:                                 # %vector.memcheck
	leaq	(%rsi,%r11), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB21_8
# BB#7:                                 # %vector.memcheck
	leaq	(%rdi,%r11,4), %rcx
	cmpq	%rsi, %rcx
	jbe	.LBB21_8
.LBB21_2:
	xorl	%r8d, %r8d
.LBB21_3:                               # %.lr.ph.preheader23
	leaq	(%rdi,%r8,4), %rdx
	addq	%r8, %rsi
	subq	%r8, %r11
	.p2align	4, 0x90
.LBB21_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %ecx
	movl	%ecx, %edi
	andl	$21845, %edi            # imm = 0x5555
	shrl	%ecx
	andl	$21845, %ecx            # imm = 0x5555
	leal	(%rcx,%rdi,2), %ecx
	movl	%ecx, %edi
	andl	$13107, %edi            # imm = 0x3333
	shrl	$2, %ecx
	andl	$13107, %ecx            # imm = 0x3333
	leal	(%rcx,%rdi,4), %edi
	movl	%edi, %ecx
	shll	$4, %ecx
	andl	$61680, %ecx            # imm = 0xF0F0
	shrl	$4, %edi
	andl	$3855, %edi             # imm = 0xF0F
	orl	%ecx, %edi
	bswapl	%edi
	shrl	$16, %edi
	movzbl	(%rsi), %eax
	movl	$16, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	movl	%edi, (%rdx)
	addq	$4, %rdx
	incq	%rsi
	decq	%r11
	jne	.LBB21_4
.LBB21_11:                              # %._crit_edge
	retq
.LBB21_8:                               # %vector.body.preheader
	movdqa	.LCPI21_0(%rip), %xmm8  # xmm8 = [43690,43690,43690,43690]
	movdqa	.LCPI21_1(%rip), %xmm9  # xmm9 = [21845,21845,21845,21845]
	movdqa	.LCPI21_2(%rip), %xmm10 # xmm10 = [52428,52428,52428,52428]
	movdqa	.LCPI21_3(%rip), %xmm11 # xmm11 = [13107,13107,13107,13107]
	movdqa	.LCPI21_4(%rip), %xmm12 # xmm12 = [61680,61680,61680,61680]
	movdqa	.LCPI21_5(%rip), %xmm13 # xmm13 = [3855,3855,3855,3855]
	movdqa	.LCPI21_6(%rip), %xmm6  # xmm6 = [0,255,0,0,0,255,0,0,0,255,0,0,0,255,0,0]
	pxor	%xmm7, %xmm7
	movdqa	.LCPI21_7(%rip), %xmm3  # xmm3 = [16,16,16,16]
	movq	%r8, %r9
	movq	%rsi, %r10
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB21_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rcx), %xmm2
	movdqa	%xmm2, %xmm0
	paddd	%xmm0, %xmm0
	pand	%xmm8, %xmm0
	psrld	$1, %xmm2
	pand	%xmm9, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	pslld	$2, %xmm0
	pand	%xmm10, %xmm0
	psrld	$2, %xmm2
	pand	%xmm11, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	pslld	$4, %xmm0
	pand	%xmm12, %xmm0
	psrld	$4, %xmm2
	pand	%xmm13, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	pslld	$8, %xmm0
	pand	%xmm6, %xmm0
	psrld	$8, %xmm2
	por	%xmm0, %xmm2
	movd	(%r10), %xmm1           # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1],xmm1[2],xmm7[2],xmm1[3],xmm7[3],xmm1[4],xmm7[4],xmm1[5],xmm7[5],xmm1[6],xmm7[6],xmm1[7],xmm7[7]
	punpcklwd	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1],xmm1[2],xmm7[2],xmm1[3],xmm7[3]
	movdqa	%xmm3, %xmm0
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm2, %xmm4
	psrld	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	psrlq	$32, %xmm5
	movdqa	%xmm2, %xmm1
	psrld	%xmm5, %xmm1
	movsd	%xmm1, %xmm4            # xmm4 = xmm1[0],xmm4[1]
	pshufd	$237, %xmm4, %xmm1      # xmm1 = xmm4[1,3,2,3]
	movdqa	%xmm0, %xmm4
	punpckhdq	%xmm7, %xmm4    # xmm4 = xmm4[2],xmm7[2],xmm4[3],xmm7[3]
	movdqa	%xmm2, %xmm5
	psrld	%xmm4, %xmm5
	punpckldq	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0],xmm0[1],xmm7[1]
	psrld	%xmm0, %xmm2
	movsd	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1]
	pshufd	$232, %xmm5, %xmm0      # xmm0 = xmm5[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movdqu	%xmm0, (%rcx)
	addq	$16, %rcx
	addq	$4, %r10
	addq	$-4, %r9
	jne	.LBB21_9
# BB#10:                                # %middle.block
	testl	%edx, %edx
	jne	.LBB21_3
	jmp	.LBB21_11
.Lfunc_end21:
	.size	_ZN9NCompress8NDeflate8NEncoder19Huffman_ReverseBitsEPjPKhj, .Lfunc_end21-_ZN9NCompress8NDeflate8NEncoder19Huffman_ReverseBitsEPjPKhj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI22_0:
	.long	43690                   # 0xaaaa
	.long	43690                   # 0xaaaa
	.long	43690                   # 0xaaaa
	.long	43690                   # 0xaaaa
.LCPI22_1:
	.long	21845                   # 0x5555
	.long	21845                   # 0x5555
	.long	21845                   # 0x5555
	.long	21845                   # 0x5555
.LCPI22_2:
	.long	52428                   # 0xcccc
	.long	52428                   # 0xcccc
	.long	52428                   # 0xcccc
	.long	52428                   # 0xcccc
.LCPI22_3:
	.long	13107                   # 0x3333
	.long	13107                   # 0x3333
	.long	13107                   # 0x3333
	.long	13107                   # 0x3333
.LCPI22_4:
	.long	61680                   # 0xf0f0
	.long	61680                   # 0xf0f0
	.long	61680                   # 0xf0f0
	.long	61680                   # 0xf0f0
.LCPI22_5:
	.long	3855                    # 0xf0f
	.long	3855                    # 0xf0f
	.long	3855                    # 0xf0f
	.long	3855                    # 0xf0f
.LCPI22_6:
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI22_7:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder10WriteBlockEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder10WriteBlockEv,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder10WriteBlockEv: # @_ZN9NCompress8NDeflate8NEncoder6CCoder10WriteBlockEv
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi108:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi110:
	.cfi_def_cfa_offset 80
.Lcfi111:
	.cfi_offset %rbx, -56
.Lcfi112:
	.cfi_offset %r12, -48
.Lcfi113:
	.cfi_offset %r13, -40
.Lcfi114:
	.cfi_offset %r14, -32
.Lcfi115:
	.cfi_offset %r15, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	leaq	3536(%r14), %rcx
	leaq	4688(%r14), %rdx
	leaq	2224(%r14), %rax
	cmpq	%rax, %rcx
	jae	.LBB22_2
# BB#1:                                 # %min.iters.checked
	leaq	1936(%r14), %rcx
	cmpq	%rdx, %rcx
	jae	.LBB22_2
# BB#4:                                 # %.lr.ph.i.preheader
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB22_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	3536(%r14,%rsi,4), %ecx
	movl	%ecx, %edi
	andl	$21845, %edi            # imm = 0x5555
	shrl	%ecx
	andl	$21845, %ecx            # imm = 0x5555
	leal	(%rcx,%rdi,2), %ecx
	movl	%ecx, %edi
	andl	$13107, %edi            # imm = 0x3333
	shrl	$2, %ecx
	andl	$13107, %ecx            # imm = 0x3333
	leal	(%rcx,%rdi,4), %edi
	movl	%edi, %ecx
	shll	$4, %ecx
	andl	$61680, %ecx            # imm = 0xF0F0
	shrl	$4, %edi
	andl	$3855, %edi             # imm = 0xF0F
	orl	%ecx, %edi
	bswapl	%edi
	shrl	$16, %edi
	movzbl	1936(%r14,%rsi), %ebp
	movl	$16, %ecx
	subl	%ebp, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	movl	%edi, 3536(%r14,%rsi,4)
	incq	%rsi
	cmpq	$288, %rsi              # imm = 0x120
	jne	.LBB22_5
	jmp	.LBB22_6
.LBB22_2:                               # %vector.body.preheader
	xorl	%ecx, %ecx
	movdqa	.LCPI22_0(%rip), %xmm8  # xmm8 = [43690,43690,43690,43690]
	movdqa	.LCPI22_1(%rip), %xmm9  # xmm9 = [21845,21845,21845,21845]
	movdqa	.LCPI22_2(%rip), %xmm10 # xmm10 = [52428,52428,52428,52428]
	movdqa	.LCPI22_3(%rip), %xmm11 # xmm11 = [13107,13107,13107,13107]
	movdqa	.LCPI22_4(%rip), %xmm12 # xmm12 = [61680,61680,61680,61680]
	movdqa	.LCPI22_5(%rip), %xmm13 # xmm13 = [3855,3855,3855,3855]
	movdqa	.LCPI22_6(%rip), %xmm6  # xmm6 = [0,255,0,0,0,255,0,0,0,255,0,0,0,255,0,0]
	pxor	%xmm7, %xmm7
	movdqa	.LCPI22_7(%rip), %xmm3  # xmm3 = [16,16,16,16]
	.p2align	4, 0x90
.LBB22_3:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	3536(%r14,%rcx,4), %xmm2
	movdqa	%xmm2, %xmm0
	paddd	%xmm0, %xmm0
	pand	%xmm8, %xmm0
	psrld	$1, %xmm2
	pand	%xmm9, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	pslld	$2, %xmm0
	pand	%xmm10, %xmm0
	psrld	$2, %xmm2
	pand	%xmm11, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	pslld	$4, %xmm0
	pand	%xmm12, %xmm0
	psrld	$4, %xmm2
	pand	%xmm13, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	pslld	$8, %xmm0
	pand	%xmm6, %xmm0
	psrld	$8, %xmm2
	por	%xmm0, %xmm2
	movd	1936(%r14,%rcx), %xmm1  # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1],xmm1[2],xmm7[2],xmm1[3],xmm7[3],xmm1[4],xmm7[4],xmm1[5],xmm7[5],xmm1[6],xmm7[6],xmm1[7],xmm7[7]
	punpcklwd	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1],xmm1[2],xmm7[2],xmm1[3],xmm7[3]
	movdqa	%xmm3, %xmm0
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm2, %xmm4
	psrld	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	psrlq	$32, %xmm5
	movdqa	%xmm2, %xmm1
	psrld	%xmm5, %xmm1
	movsd	%xmm1, %xmm4            # xmm4 = xmm1[0],xmm4[1]
	pshufd	$237, %xmm4, %xmm1      # xmm1 = xmm4[1,3,2,3]
	movdqa	%xmm0, %xmm4
	punpckhdq	%xmm7, %xmm4    # xmm4 = xmm4[2],xmm7[2],xmm4[3],xmm7[3]
	movdqa	%xmm2, %xmm5
	psrld	%xmm4, %xmm5
	punpckldq	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0],xmm0[1],xmm7[1]
	psrld	%xmm0, %xmm2
	movsd	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1]
	pshufd	$232, %xmm5, %xmm0      # xmm0 = xmm5[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movdqu	%xmm0, 3536(%r14,%rcx,4)
	addq	$4, %rcx
	cmpq	$288, %rcx              # imm = 0x120
	jne	.LBB22_3
.LBB22_6:                               # %vector.memcheck186
	leaq	2256(%r14), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB22_8
# BB#7:                                 # %vector.memcheck186
	leaq	4816(%r14), %rcx
	cmpq	%rcx, %rax
	jae	.LBB22_8
# BB#10:                                # %.lr.ph.i35.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_11:                              # %.lr.ph.i35
                                        # =>This Inner Loop Header: Depth=1
	movl	4688(%r14,%rax,4), %ecx
	movl	%ecx, %edx
	andl	$21845, %edx            # imm = 0x5555
	shrl	%ecx
	andl	$21845, %ecx            # imm = 0x5555
	leal	(%rcx,%rdx,2), %ecx
	movl	%ecx, %edx
	andl	$13107, %edx            # imm = 0x3333
	shrl	$2, %ecx
	andl	$13107, %ecx            # imm = 0x3333
	leal	(%rcx,%rdx,4), %edx
	movl	%edx, %ecx
	shll	$4, %ecx
	andl	$61680, %ecx            # imm = 0xF0F0
	shrl	$4, %edx
	andl	$3855, %edx             # imm = 0xF0F
	orl	%ecx, %edx
	bswapl	%edx
	shrl	$16, %edx
	movzbl	2224(%r14,%rax), %esi
	movl	$16, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movl	%edx, 4688(%r14,%rax,4)
	incq	%rax
	cmpq	$32, %rax
	jne	.LBB22_11
	jmp	.LBB22_12
.LBB22_8:                               # %vector.body170.preheader
	xorl	%eax, %eax
	movdqa	.LCPI22_0(%rip), %xmm8  # xmm8 = [43690,43690,43690,43690]
	movdqa	.LCPI22_1(%rip), %xmm9  # xmm9 = [21845,21845,21845,21845]
	movdqa	.LCPI22_2(%rip), %xmm10 # xmm10 = [52428,52428,52428,52428]
	movdqa	.LCPI22_3(%rip), %xmm11 # xmm11 = [13107,13107,13107,13107]
	movdqa	.LCPI22_4(%rip), %xmm12 # xmm12 = [61680,61680,61680,61680]
	movdqa	.LCPI22_5(%rip), %xmm13 # xmm13 = [3855,3855,3855,3855]
	movdqa	.LCPI22_6(%rip), %xmm6  # xmm6 = [0,255,0,0,0,255,0,0,0,255,0,0,0,255,0,0]
	pxor	%xmm7, %xmm7
	movdqa	.LCPI22_7(%rip), %xmm3  # xmm3 = [16,16,16,16]
	.p2align	4, 0x90
.LBB22_9:                               # %vector.body170
                                        # =>This Inner Loop Header: Depth=1
	movdqu	4688(%r14,%rax,4), %xmm2
	movdqa	%xmm2, %xmm0
	paddd	%xmm0, %xmm0
	pand	%xmm8, %xmm0
	psrld	$1, %xmm2
	pand	%xmm9, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	pslld	$2, %xmm0
	pand	%xmm10, %xmm0
	psrld	$2, %xmm2
	pand	%xmm11, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	pslld	$4, %xmm0
	pand	%xmm12, %xmm0
	psrld	$4, %xmm2
	pand	%xmm13, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	pslld	$8, %xmm0
	pand	%xmm6, %xmm0
	psrld	$8, %xmm2
	por	%xmm0, %xmm2
	movd	2224(%r14,%rax), %xmm1  # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1],xmm1[2],xmm7[2],xmm1[3],xmm7[3],xmm1[4],xmm7[4],xmm1[5],xmm7[5],xmm1[6],xmm7[6],xmm1[7],xmm7[7]
	punpcklwd	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1],xmm1[2],xmm7[2],xmm1[3],xmm7[3]
	movdqa	%xmm3, %xmm0
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm2, %xmm4
	psrld	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	psrlq	$32, %xmm5
	movdqa	%xmm2, %xmm1
	psrld	%xmm5, %xmm1
	movsd	%xmm1, %xmm4            # xmm4 = xmm1[0],xmm4[1]
	pshufd	$237, %xmm4, %xmm1      # xmm1 = xmm4[1,3,2,3]
	movdqa	%xmm0, %xmm4
	punpckhdq	%xmm7, %xmm4    # xmm4 = xmm4[2],xmm7[2],xmm4[3],xmm7[3]
	movdqa	%xmm2, %xmm5
	psrld	%xmm4, %xmm5
	punpckldq	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0],xmm0[1],xmm7[1]
	psrld	%xmm0, %xmm2
	movsd	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1]
	pshufd	$232, %xmm5, %xmm0      # xmm0 = xmm5[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movdqu	%xmm0, 4688(%r14,%rax,4)
	addq	$4, %rax
	cmpq	$32, %rax
	jne	.LBB22_9
.LBB22_12:                              # %_ZN9NCompress8NDeflate8NEncoder19Huffman_ReverseBitsEPjPKhj.exit36.preheader
	cmpl	$0, 1372(%r14)
	je	.LBB22_20
# BB#13:                                # %.lr.ph
	leaq	1168(%r14), %r15
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_33 Depth 2
                                        #     Child Loop BB22_40 Depth 2
                                        #     Child Loop BB22_50 Depth 2
                                        #     Child Loop BB22_57 Depth 2
                                        #     Child Loop BB22_17 Depth 2
	movq	1248(%r14), %rcx
	movl	%eax, %eax
	movzwl	(%rcx,%rax,4), %r13d
	testw	%r13w, %r13w
	movq	%rax, 8(%rsp)           # 8-byte Spill
	js	.LBB22_15
# BB#31:                                #   in Loop: Header=BB22_14 Depth=1
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r15, %rbx
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%r13), %ebp
	movzbl	2193(%r14,%rbp), %eax
	testl	%eax, %eax
	je	.LBB22_38
# BB#32:                                # %.lr.ph.i62
                                        #   in Loop: Header=BB22_14 Depth=1
	movl	4564(%r14,%rbp,4), %r12d
	movl	1224(%r14), %edx
	.p2align	4, 0x90
.LBB22_33:                              #   Parent Loop BB22_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %r15d
	subl	%edx, %r15d
	jb	.LBB22_34
# BB#35:                                #   in Loop: Header=BB22_33 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r12d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%r14), %eax
	movq	1168(%r14), %rcx
	movl	1176(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r14)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r14), %eax
	cmpl	1180(%r14), %eax
	jne	.LBB22_37
# BB#36:                                #   in Loop: Header=BB22_33 Depth=2
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB22_37:                              # %_ZN10COutBuffer9WriteByteEh.exit.i65
                                        #   in Loop: Header=BB22_33 Depth=2
	movzbl	1224(%r14), %ecx
	shrl	%cl, %r12d
	movl	$8, 1224(%r14)
	movb	$0, 1228(%r14)
	movl	$8, %edx
	testl	%r15d, %r15d
	movl	%r15d, %eax
	jne	.LBB22_33
	jmp	.LBB22_38
	.p2align	4, 0x90
.LBB22_15:                              #   in Loop: Header=BB22_14 Depth=1
	movzwl	2(%rcx,%rax,4), %ecx
	movzbl	1936(%r14,%rcx), %eax
	testl	%eax, %eax
	je	.LBB22_62
# BB#16:                                # %.lr.ph.i56
                                        #   in Loop: Header=BB22_14 Depth=1
	movl	3536(%r14,%rcx,4), %ebp
	movl	1224(%r14), %edx
	.p2align	4, 0x90
.LBB22_17:                              #   Parent Loop BB22_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebx
	subl	%edx, %ebx
	jb	.LBB22_18
# BB#28:                                #   in Loop: Header=BB22_17 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%r14), %eax
	movq	1168(%r14), %rcx
	movl	1176(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r14)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r14), %eax
	cmpl	1180(%r14), %eax
	jne	.LBB22_30
# BB#29:                                #   in Loop: Header=BB22_17 Depth=2
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB22_30:                              # %_ZN10COutBuffer9WriteByteEh.exit.i59
                                        #   in Loop: Header=BB22_17 Depth=2
	movzbl	1224(%r14), %ecx
	shrl	%cl, %ebp
	movl	$8, 1224(%r14)
	movb	$0, 1228(%r14)
	movl	$8, %edx
	testl	%ebx, %ebx
	movl	%ebx, %eax
	jne	.LBB22_17
	jmp	.LBB22_62
.LBB22_34:                              #   in Loop: Header=BB22_14 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r12d, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%r14), %esi
	movb	%sil, 1228(%r14)
	subl	%eax, %edx
	movl	%edx, 1224(%r14)
.LBB22_38:                              # %_ZN12CBitlEncoder9WriteBitsEjj.exit66
                                        #   in Loop: Header=BB22_14 Depth=1
	movq	1328(%r14), %rax
	movzbl	(%rax,%rbp), %eax
	testl	%eax, %eax
	movq	%rbx, %r15
	je	.LBB22_45
# BB#39:                                # %.lr.ph.i50
                                        #   in Loop: Header=BB22_14 Depth=1
	movq	1320(%r14), %rcx
	movzbl	(%rcx,%rbp), %ecx
	subl	%ecx, %r13d
	movl	1224(%r14), %edx
	.p2align	4, 0x90
.LBB22_40:                              #   Parent Loop BB22_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB22_41
# BB#42:                                #   in Loop: Header=BB22_40 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r13d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%r14), %eax
	movq	1168(%r14), %rcx
	movl	1176(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r14)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r14), %eax
	cmpl	1180(%r14), %eax
	jne	.LBB22_44
# BB#43:                                #   in Loop: Header=BB22_40 Depth=2
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB22_44:                              # %_ZN10COutBuffer9WriteByteEh.exit.i53
                                        #   in Loop: Header=BB22_40 Depth=2
	movzbl	1224(%r14), %ecx
	shrl	%cl, %r13d
	movl	$8, 1224(%r14)
	movb	$0, 1228(%r14)
	movl	$8, %edx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB22_40
	jmp	.LBB22_45
.LBB22_41:                              #   in Loop: Header=BB22_14 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r13d, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%r14), %esi
	movb	%sil, 1228(%r14)
	subl	%eax, %edx
	movl	%edx, 1224(%r14)
.LBB22_45:                              # %_ZN12CBitlEncoder9WriteBitsEjj.exit54
                                        #   in Loop: Header=BB22_14 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movzwl	2(%rcx,%rax,4), %r12d
	cmpl	$511, %r12d             # imm = 0x1FF
	ja	.LBB22_47
# BB#46:                                #   in Loop: Header=BB22_14 Depth=1
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%r12), %eax
	jmp	.LBB22_48
	.p2align	4, 0x90
.LBB22_47:                              #   in Loop: Header=BB22_14 Depth=1
	movl	%r12d, %eax
	shrl	$8, %eax
	movzbl	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%rax), %eax
	addq	$16, %rax
.LBB22_48:                              # %_ZN9NCompress8NDeflate8NEncoder10GetPosSlotEj.exit
                                        #   in Loop: Header=BB22_14 Depth=1
	movl	%eax, %r13d
	movzbl	2224(%r14,%r13), %eax
	testl	%eax, %eax
	je	.LBB22_55
# BB#49:                                # %.lr.ph.i38
                                        #   in Loop: Header=BB22_14 Depth=1
	movl	4688(%r14,%r13,4), %ebp
	movl	1224(%r14), %edx
	.p2align	4, 0x90
.LBB22_50:                              #   Parent Loop BB22_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebx
	subl	%edx, %ebx
	jb	.LBB22_51
# BB#52:                                #   in Loop: Header=BB22_50 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%r14), %eax
	movq	1168(%r14), %rcx
	movl	1176(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r14)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r14), %eax
	cmpl	1180(%r14), %eax
	jne	.LBB22_54
# BB#53:                                #   in Loop: Header=BB22_50 Depth=2
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB22_54:                              # %_ZN10COutBuffer9WriteByteEh.exit.i41
                                        #   in Loop: Header=BB22_50 Depth=2
	movzbl	1224(%r14), %ecx
	shrl	%cl, %ebp
	movl	$8, 1224(%r14)
	movb	$0, 1228(%r14)
	movl	$8, %edx
	testl	%ebx, %ebx
	movl	%ebx, %eax
	jne	.LBB22_50
	jmp	.LBB22_55
.LBB22_51:                              #   in Loop: Header=BB22_14 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%ebp, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%r14), %esi
	movb	%sil, 1228(%r14)
	subl	%eax, %edx
	movl	%edx, 1224(%r14)
.LBB22_55:                              # %_ZN12CBitlEncoder9WriteBitsEjj.exit42
                                        #   in Loop: Header=BB22_14 Depth=1
	cmpq	$4, %r13
	jb	.LBB22_62
# BB#56:                                # %.lr.ph.i31
                                        #   in Loop: Header=BB22_14 Depth=1
	movzbl	_ZN9NCompress8NDeflateL15kDistDirectBitsE(%r13), %eax
	subl	_ZN9NCompress8NDeflateL10kDistStartE(,%r13,4), %r12d
	movl	1224(%r14), %edx
	.p2align	4, 0x90
.LBB22_57:                              #   Parent Loop BB22_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebx
	subl	%edx, %ebx
	jb	.LBB22_58
# BB#59:                                #   in Loop: Header=BB22_57 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r12d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%r14), %eax
	movq	1168(%r14), %rcx
	movl	1176(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r14)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r14), %eax
	cmpl	1180(%r14), %eax
	jne	.LBB22_61
# BB#60:                                #   in Loop: Header=BB22_57 Depth=2
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB22_61:                              # %_ZN10COutBuffer9WriteByteEh.exit.i
                                        #   in Loop: Header=BB22_57 Depth=2
	movzbl	1224(%r14), %ecx
	shrl	%cl, %r12d
	movl	$8, 1224(%r14)
	movb	$0, 1228(%r14)
	movl	$8, %edx
	testl	%ebx, %ebx
	movl	%ebx, %eax
	jne	.LBB22_57
	jmp	.LBB22_62
.LBB22_58:                              #   in Loop: Header=BB22_14 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r12d, %esi
	jmp	.LBB22_19
.LBB22_18:                              #   in Loop: Header=BB22_14 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%ebp, %esi
.LBB22_19:                              # %_ZN12CBitlEncoder9WriteBitsEjj.exit60
                                        #   in Loop: Header=BB22_14 Depth=1
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%r14), %esi
	movb	%sil, 1228(%r14)
	subl	%eax, %edx
	movl	%edx, 1224(%r14)
.LBB22_62:                              # %_ZN12CBitlEncoder9WriteBitsEjj.exit60
                                        #   in Loop: Header=BB22_14 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	incl	%eax
	cmpl	1372(%r14), %eax
	jb	.LBB22_14
.LBB22_20:                              # %_ZN9NCompress8NDeflate8NEncoder19Huffman_ReverseBitsEPjPKhj.exit36._crit_edge
	movzbl	2192(%r14), %eax
	testl	%eax, %eax
	je	.LBB22_27
# BB#21:                                # %.lr.ph.i44
	movl	4560(%r14), %ebp
	leaq	1168(%r14), %r15
	movl	1224(%r14), %edx
	.p2align	4, 0x90
.LBB22_22:                              # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebx
	subl	%edx, %ebx
	jb	.LBB22_23
# BB#24:                                #   in Loop: Header=BB22_22 Depth=1
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%r14), %eax
	movq	1168(%r14), %rcx
	movl	1176(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r14)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r14), %eax
	cmpl	1180(%r14), %eax
	jne	.LBB22_26
# BB#25:                                #   in Loop: Header=BB22_22 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB22_26:                              # %_ZN10COutBuffer9WriteByteEh.exit.i47
                                        #   in Loop: Header=BB22_22 Depth=1
	movzbl	1224(%r14), %ecx
	shrl	%cl, %ebp
	movl	$8, 1224(%r14)
	movb	$0, 1228(%r14)
	movl	$8, %edx
	testl	%ebx, %ebx
	movl	%ebx, %eax
	jne	.LBB22_22
	jmp	.LBB22_27
.LBB22_23:
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%ebp, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%r14), %esi
	movb	%sil, 1228(%r14)
	subl	%eax, %edx
	movl	%edx, 1224(%r14)
.LBB22_27:                              # %_ZN12CBitlEncoder9WriteBitsEjj.exit48
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder10WriteBlockEv, .Lfunc_end22-_ZN9NCompress8NDeflate8NEncoder6CCoder10WriteBlockEv
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder15WriteStoreBlockEjjb
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder15WriteStoreBlockEjjb,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder15WriteStoreBlockEjjb: # @_ZN9NCompress8NDeflate8NEncoder6CCoder15WriteStoreBlockEjjb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi121:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi123:
	.cfi_def_cfa_offset 80
.Lcfi124:
	.cfi_offset %rbx, -56
.Lcfi125:
	.cfi_offset %r12, -48
.Lcfi126:
	.cfi_offset %r13, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%rdi, %r12
	leaq	1168(%r12), %r15
	.p2align	4, 0x90
.LBB23_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_2 Depth 2
                                        #     Child Loop BB23_9 Depth 2
                                        #     Child Loop BB23_24 Depth 2
	cmpl	$65535, %esi            # imm = 0xFFFF
	movl	$65535, %r13d           # imm = 0xFFFF
	cmovbl	%esi, %r13d
	subl	%r13d, %esi
	movl	%esi, 20(%rsp)          # 4-byte Spill
	sete	%al
	andb	16(%rsp), %al           # 1-byte Folded Reload
	movzbl	%al, %ebp
	movl	1224(%r12), %edx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB23_2:                               #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebx
	subl	%edx, %ebx
	jb	.LBB23_3
# BB#4:                                 #   in Loop: Header=BB23_2 Depth=2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%r12), %eax
	movq	1168(%r12), %rcx
	movl	1176(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r12)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB23_6
# BB#5:                                 #   in Loop: Header=BB23_2 Depth=2
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB23_6:                               # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB23_2 Depth=2
	movzbl	1224(%r12), %ecx
	shrl	%cl, %ebp
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	$8, %edx
	testl	%ebx, %ebx
	movl	%ebx, %eax
	jne	.LBB23_2
# BB#7:                                 #   in Loop: Header=BB23_1 Depth=1
	xorl	%esi, %esi
	jmp	.LBB23_8
	.p2align	4, 0x90
.LBB23_3:                               #   in Loop: Header=BB23_1 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%ebp, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%r12), %esi
	movb	%sil, 1228(%r12)
	subl	%eax, %edx
	movl	%edx, 1224(%r12)
.LBB23_8:                               # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit.preheader
                                        #   in Loop: Header=BB23_1 Depth=1
	xorl	%r14d, %r14d
	movl	$2, %eax
	.p2align	4, 0x90
.LBB23_9:                               # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit
                                        #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB23_13
# BB#10:                                #   in Loop: Header=BB23_9 Depth=2
	movzbl	%sil, %eax
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r14d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%eax, %edx
	movq	1168(%r12), %rax
	movl	1176(%r12), %ecx
	leal	1(%rcx), %esi
	movl	%esi, 1176(%r12)
	movb	%dl, (%rax,%rcx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB23_12
# BB#11:                                #   in Loop: Header=BB23_9 Depth=2
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB23_12:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i27
                                        #   in Loop: Header=BB23_9 Depth=2
	movzbl	1224(%r12), %ecx
	shrl	%cl, %r14d
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	$8, %edx
	xorl	%esi, %esi
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB23_9
	jmp	.LBB23_16
	.p2align	4, 0x90
.LBB23_13:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit28
                                        #   in Loop: Header=BB23_1 Depth=1
	movl	$1, %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	decl	%edi
	andl	%r14d, %edi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movzbl	%sil, %ecx
	orl	%edi, %ecx
	movb	%cl, 1228(%r12)
	subl	%eax, %edx
	movl	%edx, 1224(%r12)
	cmpl	$7, %edx
	ja	.LBB23_16
# BB#14:                                #   in Loop: Header=BB23_1 Depth=1
	movq	1168(%r12), %rax
	movl	1176(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r12)
	movb	%cl, (%rax,%rdx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB23_16
# BB#15:                                #   in Loop: Header=BB23_1 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	.p2align	4, 0x90
.LBB23_16:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit28.thread
                                        #   in Loop: Header=BB23_1 Depth=1
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movq	1168(%r12), %rax
	movl	1176(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 1176(%r12)
	movb	%r13b, (%rax,%rcx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB23_18
# BB#17:                                #   in Loop: Header=BB23_1 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB23_18:                              #   in Loop: Header=BB23_1 Depth=1
	movb	1224(%r12), %cl
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	%r13d, %eax
	shrl	%cl, %eax
	movq	1168(%r12), %rcx
	movl	1176(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r12)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB23_19
# BB#29:                                #   in Loop: Header=BB23_1 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB23_19:                              #   in Loop: Header=BB23_1 Depth=1
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	%r13d, %ebx
	xorl	$65535, %ebx            # imm = 0xFFFF
	movq	1168(%r12), %rax
	movl	1176(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 1176(%r12)
	movb	%bl, (%rax,%rcx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB23_21
# BB#20:                                #   in Loop: Header=BB23_1 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB23_21:                              #   in Loop: Header=BB23_1 Depth=1
	movb	1224(%r12), %cl
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	shrl	%cl, %ebx
	movq	1168(%r12), %rax
	movl	1176(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 1176(%r12)
	movb	%bl, (%rax,%rcx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB23_22
# BB#30:                                #   in Loop: Header=BB23_1 Depth=1
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB23_22:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit38
                                        #   in Loop: Header=BB23_1 Depth=1
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	testl	%r13d, %r13d
	je	.LBB23_27
# BB#23:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB23_1 Depth=1
	movq	(%r12), %r14
	movl	12(%rsp), %eax          # 4-byte Reload
	subq	%rax, %r14
	movl	%r13d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB23_24:                              # %.lr.ph
                                        #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r14,%rbx), %eax
	movq	1168(%r12), %rcx
	movl	1176(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r12)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB23_26
# BB#25:                                #   in Loop: Header=BB23_24 Depth=2
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB23_26:                              # %_ZN12CBitlEncoder9WriteByteEh.exit
                                        #   in Loop: Header=BB23_24 Depth=2
	incq	%rbx
	cmpq	%rbp, %rbx
	jb	.LBB23_24
.LBB23_27:                              # %._crit_edge
                                        #   in Loop: Header=BB23_1 Depth=1
	subl	%r13d, 12(%rsp)         # 4-byte Folded Spill
	movl	20(%rsp), %esi          # 4-byte Reload
	testl	%esi, %esi
	jne	.LBB23_1
# BB#28:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder15WriteStoreBlockEjjb, .Lfunc_end23-_ZN9NCompress8NDeflate8NEncoder6CCoder15WriteStoreBlockEjjb
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI24_0:
	.zero	16,11
.LCPI24_1:
	.zero	16,6
.LCPI24_2:
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	6                       # 0x6
.LCPI24_3:
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	14                      # 0xe
.LCPI24_4:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
.LCPI24_5:
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI24_6:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
.LCPI24_7:
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI24_8:
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
.LCPI24_9:
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
.LCPI24_10:
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder11TryDynBlockEij
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder11TryDynBlockEij,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder11TryDynBlockEij: # @_ZN9NCompress8NDeflate8NEncoder6CCoder11TryDynBlockEij
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi136:
	.cfi_def_cfa_offset 208
.Lcfi137:
	.cfi_offset %rbx, -56
.Lcfi138:
	.cfi_offset %r12, -48
.Lcfi139:
	.cfi_offset %r13, -40
.Lcfi140:
	.cfi_offset %r14, -32
.Lcfi141:
	.cfi_offset %r15, -24
.Lcfi142:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rdi, %r14
	movq	4920(%r14), %rdi
	movslq	%esi, %rax
	imulq	$332, %rax, %r8         # imm = 0x14C
	movl	324(%rdi,%r8), %eax
	movl	%eax, 4912(%r14)
	movl	328(%rdi,%r8), %eax
	movl	%eax, 80(%rsp)          # 4-byte Spill
	cmpb	$0, 1268(%r14)
	jne	.LBB24_34
# BB#1:                                 # %vector.memcheck
	leaq	1392(%r14), %rax
	leaq	(%rdi,%r8), %rbp
	leaq	256(%rdi,%r8), %rcx
	cmpq	%rcx, %rax
	jae	.LBB24_13
# BB#2:                                 # %vector.memcheck
	leaq	1648(%r14), %rax
	cmpq	%rax, %rbp
	jae	.LBB24_13
# BB#3:                                 # %.preheader28.i.preheader
	leaq	3(%rdi,%r8), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB24_4:                               # %.preheader28.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rcx,%rdx), %eax
	testb	%al, %al
	jne	.LBB24_6
# BB#5:                                 # %.preheader28.i
                                        #   in Loop: Header=BB24_4 Depth=1
	movb	$11, %al
.LBB24_6:                               # %.preheader28.i
                                        #   in Loop: Header=BB24_4 Depth=1
	movb	%al, 1392(%r14,%rdx)
	movzbl	-2(%rcx,%rdx), %eax
	testb	%al, %al
	jne	.LBB24_8
# BB#7:                                 # %.preheader28.i
                                        #   in Loop: Header=BB24_4 Depth=1
	movb	$11, %al
.LBB24_8:                               # %.preheader28.i
                                        #   in Loop: Header=BB24_4 Depth=1
	movb	%al, 1393(%r14,%rdx)
	movzbl	-1(%rcx,%rdx), %eax
	testb	%al, %al
	jne	.LBB24_10
# BB#9:                                 # %.preheader28.i
                                        #   in Loop: Header=BB24_4 Depth=1
	movb	$11, %al
.LBB24_10:                              # %.preheader28.i
                                        #   in Loop: Header=BB24_4 Depth=1
	movb	%al, 1394(%r14,%rdx)
	movzbl	(%rcx,%rdx), %eax
	testb	%al, %al
	jne	.LBB24_12
# BB#11:                                # %.preheader28.i
                                        #   in Loop: Header=BB24_4 Depth=1
	movb	$11, %al
.LBB24_12:                              # %.preheader28.i
                                        #   in Loop: Header=BB24_4 Depth=1
	movb	%al, 1395(%r14,%rdx)
	addq	$4, %rdx
	cmpq	$256, %rdx              # imm = 0x100
	jne	.LBB24_4
	jmp	.LBB24_14
.LBB24_13:                              # %vector.body
	movdqu	(%rdi,%r8), %xmm2
	movdqu	16(%rdi,%r8), %xmm3
	pxor	%xmm0, %xmm0
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	pcmpeqd	%xmm1, %xmm1
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	movdqa	.LCPI24_0(%rip), %xmm8  # xmm8 = [11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11]
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1392(%r14)
	movdqu	%xmm7, 1408(%r14)
	movdqu	32(%rdi,%r8), %xmm3
	movdqu	48(%rdi,%r8), %xmm2
	movdqa	%xmm3, %xmm5
	pcmpeqb	%xmm0, %xmm5
	movdqa	%xmm5, %xmm6
	pxor	%xmm1, %xmm6
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm3, %xmm5
	pandn	%xmm8, %xmm6
	por	%xmm5, %xmm6
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm7
	por	%xmm4, %xmm7
	movdqu	%xmm6, 1424(%r14)
	movdqu	%xmm7, 1440(%r14)
	movdqu	64(%rdi,%r8), %xmm2
	movdqu	80(%rdi,%r8), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1456(%r14)
	movdqu	%xmm7, 1472(%r14)
	movdqu	96(%rdi,%r8), %xmm2
	movdqu	112(%rdi,%r8), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1488(%r14)
	movdqu	%xmm7, 1504(%r14)
	movdqu	128(%rdi,%r8), %xmm2
	movdqu	144(%rdi,%r8), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1520(%r14)
	movdqu	%xmm7, 1536(%r14)
	movdqu	160(%rdi,%r8), %xmm2
	movdqu	176(%rdi,%r8), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1552(%r14)
	movdqu	%xmm7, 1568(%r14)
	movdqu	192(%rdi,%r8), %xmm2
	movdqu	208(%rdi,%r8), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1584(%r14)
	movdqu	%xmm7, 1600(%r14)
	movdqu	224(%rdi,%r8), %xmm2
	movdqu	240(%rdi,%r8), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	pcmpeqb	%xmm3, %xmm0
	pxor	%xmm0, %xmm1
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm0
	pandn	%xmm8, %xmm1
	por	%xmm0, %xmm1
	movdqu	%xmm5, 1616(%r14)
	movdqu	%xmm1, 1632(%r14)
.LBB24_14:                              # %.preheader27.i
	movl	%ebx, %r9d
	movl	1308(%r14), %ecx
	testq	%rcx, %rcx
	je	.LBB24_25
# BB#15:                                # %.lr.ph.i
	movq	1328(%r14), %rdx
	testb	$1, %cl
	jne	.LBB24_17
# BB#16:
	xorl	%esi, %esi
	cmpl	$1, %ecx
	jne	.LBB24_20
	jmp	.LBB24_25
.LBB24_17:
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%rip), %esi
	movb	257(%rsi,%rbp), %al
	testb	%al, %al
	jne	.LBB24_19
# BB#18:
	movb	$11, %al
.LBB24_19:
	addb	(%rdx,%rsi), %al
	movb	%al, 1648(%r14)
	movl	$1, %esi
	cmpl	$1, %ecx
	je	.LBB24_25
	.p2align	4, 0x90
.LBB24_20:                              # =>This Inner Loop Header: Depth=1
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%rsi), %ebx
	movzbl	257(%rbx,%rbp), %eax
	testb	%al, %al
	jne	.LBB24_22
# BB#21:                                #   in Loop: Header=BB24_20 Depth=1
	movb	$11, %al
.LBB24_22:                              #   in Loop: Header=BB24_20 Depth=1
	addb	(%rdx,%rbx), %al
	movb	%al, 1648(%r14,%rsi)
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE+1(%rsi), %ebx
	movzbl	257(%rbx,%rbp), %eax
	testb	%al, %al
	jne	.LBB24_24
# BB#23:                                #   in Loop: Header=BB24_20 Depth=1
	movb	$11, %al
.LBB24_24:                              #   in Loop: Header=BB24_20 Depth=1
	addb	(%rdx,%rbx), %al
	movb	%al, 1649(%r14,%rsi)
	addq	$2, %rsi
	cmpq	%rsi, %rcx
	jne	.LBB24_20
.LBB24_25:                              # %vector.memcheck124
	leaq	1904(%r14), %rax
	leaq	320(%rdi,%r8), %rcx
	cmpq	%rcx, %rax
	jae	.LBB24_33
# BB#26:                                # %vector.memcheck124
	leaq	1936(%r14), %rax
	leaq	288(%rdi,%r8), %rcx
	cmpq	%rax, %rcx
	jae	.LBB24_33
# BB#27:                                # %.preheader.i.preheader
	leaq	289(%rdi,%r8), %rax
	xorl	%ecx, %ecx
	movl	%r9d, %ebx
	.p2align	4, 0x90
.LBB24_28:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rax,%rcx), %edx
	testb	%dl, %dl
	jne	.LBB24_30
# BB#29:                                # %.preheader.i
                                        #   in Loop: Header=BB24_28 Depth=1
	movb	$6, %dl
.LBB24_30:                              # %.preheader.i
                                        #   in Loop: Header=BB24_28 Depth=1
	addb	_ZN9NCompress8NDeflateL15kDistDirectBitsE(%rcx), %dl
	movb	%dl, 1904(%r14,%rcx)
	movzbl	(%rax,%rcx), %edx
	testb	%dl, %dl
	jne	.LBB24_32
# BB#31:                                # %.preheader.i
                                        #   in Loop: Header=BB24_28 Depth=1
	movb	$6, %dl
.LBB24_32:                              # %.preheader.i
                                        #   in Loop: Header=BB24_28 Depth=1
	addb	_ZN9NCompress8NDeflateL15kDistDirectBitsE+1(%rcx), %dl
	movb	%dl, 1905(%r14,%rcx)
	addq	$2, %rcx
	cmpq	$32, %rcx
	jne	.LBB24_28
	jmp	.LBB24_34
.LBB24_33:                              # %vector.body111
	movdqu	288(%rdi,%r8), %xmm0
	pxor	%xmm1, %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqb	%xmm1, %xmm2
	pcmpeqd	%xmm3, %xmm3
	movdqa	%xmm2, %xmm4
	pxor	%xmm3, %xmm4
	pandn	%xmm0, %xmm2
	movdqa	.LCPI24_1(%rip), %xmm0  # xmm0 = [6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6]
	pandn	%xmm0, %xmm4
	por	%xmm2, %xmm4
	paddb	.LCPI24_2(%rip), %xmm4
	movdqu	%xmm4, 1904(%r14)
	movdqu	304(%rdi,%r8), %xmm2
	pcmpeqb	%xmm2, %xmm1
	pxor	%xmm1, %xmm3
	pandn	%xmm2, %xmm1
	pandn	%xmm0, %xmm3
	por	%xmm1, %xmm3
	paddb	.LCPI24_3(%rip), %xmm3
	movdqu	%xmm3, 1920(%r14)
	movl	%r9d, %ebx
.LBB24_34:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE.exit.preheader
	testl	%ebx, %ebx
	je	.LBB24_54
# BB#35:                                # %.lr.ph
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	leaq	2256(%r14), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	3536(%r14), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	1936(%r14), %r15
	leaq	3408(%r14), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	4688(%r14), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	2224(%r14), %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB24_36:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_46 Depth 2
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, 1288(%r14)
	movq	%r14, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder8TryBlockEv
	movl	1372(%r14), %eax
	movl	$12, %r13d
	cmpl	$18000, %eax            # imm = 0x4650
	ja	.LBB24_39
# BB#37:                                #   in Loop: Header=BB24_36 Depth=1
	movl	$11, %r13d
	cmpl	$7000, %eax             # imm = 0x1B58
	ja	.LBB24_39
# BB#38:                                #   in Loop: Header=BB24_36 Depth=1
	xorl	%r13d, %r13d
	cmpl	$2000, %eax             # imm = 0x7D0
	seta	%r13b
	addl	$9, %r13d
.LBB24_39:                              #   in Loop: Header=BB24_36 Depth=1
	movl	$288, %ecx              # imm = 0x120
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	136(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movl	%r13d, %r8d
	callq	Huffman_Generate
	movl	$32, %ecx
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	%r12, %rdx
	movl	%r13d, %r8d
	callq	Huffman_Generate
	cmpb	$0, 1268(%r14)
	jne	.LBB24_52
# BB#40:                                # %vector.body149
                                        #   in Loop: Header=BB24_36 Depth=1
	movl	%ebx, %edi
	movdqu	1936(%r14), %xmm0
	movdqu	1952(%r14), %xmm1
	movdqa	%xmm0, %xmm2
	pxor	%xmm6, %xmm6
	pcmpeqb	%xmm6, %xmm2
	movdqa	%xmm2, %xmm3
	pcmpeqd	%xmm7, %xmm7
	pxor	%xmm7, %xmm3
	movdqa	%xmm1, %xmm4
	pcmpeqb	%xmm6, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm7, %xmm5
	pandn	%xmm0, %xmm2
	movdqa	.LCPI24_0(%rip), %xmm8  # xmm8 = [11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11]
	pandn	%xmm8, %xmm3
	por	%xmm2, %xmm3
	pandn	%xmm1, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	movdqu	%xmm3, 1392(%r14)
	movdqu	%xmm5, 1408(%r14)
	movdqu	1968(%r14), %xmm0
	movdqu	1984(%r14), %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqb	%xmm6, %xmm2
	movdqa	%xmm2, %xmm3
	pxor	%xmm7, %xmm3
	movdqa	%xmm1, %xmm4
	pcmpeqb	%xmm6, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm7, %xmm5
	pandn	%xmm0, %xmm2
	pandn	%xmm8, %xmm3
	por	%xmm2, %xmm3
	pandn	%xmm1, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	movdqu	%xmm3, 1424(%r14)
	movdqu	%xmm5, 1440(%r14)
	movdqu	2000(%r14), %xmm0
	movdqu	2016(%r14), %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqb	%xmm6, %xmm2
	movdqa	%xmm2, %xmm3
	pxor	%xmm7, %xmm3
	movdqa	%xmm1, %xmm4
	pcmpeqb	%xmm6, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm7, %xmm5
	pandn	%xmm0, %xmm2
	pandn	%xmm8, %xmm3
	por	%xmm2, %xmm3
	pandn	%xmm1, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	movdqu	%xmm3, 1456(%r14)
	movdqu	%xmm5, 1472(%r14)
	movdqu	2032(%r14), %xmm0
	movdqu	2048(%r14), %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqb	%xmm6, %xmm2
	movdqa	%xmm2, %xmm3
	pxor	%xmm7, %xmm3
	movdqa	%xmm1, %xmm4
	pcmpeqb	%xmm6, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm7, %xmm5
	pandn	%xmm0, %xmm2
	pandn	%xmm8, %xmm3
	por	%xmm2, %xmm3
	pandn	%xmm1, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	movdqu	%xmm3, 1488(%r14)
	movdqu	%xmm5, 1504(%r14)
	movdqu	2064(%r14), %xmm0
	movdqu	2080(%r14), %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqb	%xmm6, %xmm2
	movdqa	%xmm2, %xmm3
	pxor	%xmm7, %xmm3
	movdqa	%xmm1, %xmm4
	pcmpeqb	%xmm6, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm7, %xmm5
	pandn	%xmm0, %xmm2
	pandn	%xmm8, %xmm3
	por	%xmm2, %xmm3
	pandn	%xmm1, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	movdqu	%xmm3, 1520(%r14)
	movdqu	%xmm5, 1536(%r14)
	movdqu	2096(%r14), %xmm0
	movdqu	2112(%r14), %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqb	%xmm6, %xmm2
	movdqa	%xmm2, %xmm3
	pxor	%xmm7, %xmm3
	movdqa	%xmm1, %xmm4
	pcmpeqb	%xmm6, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm7, %xmm5
	pandn	%xmm0, %xmm2
	pandn	%xmm8, %xmm3
	por	%xmm2, %xmm3
	pandn	%xmm1, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	movdqu	%xmm3, 1552(%r14)
	movdqu	%xmm5, 1568(%r14)
	movdqu	2128(%r14), %xmm0
	movdqu	2144(%r14), %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqb	%xmm6, %xmm2
	movdqa	%xmm2, %xmm3
	pxor	%xmm7, %xmm3
	movdqa	%xmm1, %xmm4
	pcmpeqb	%xmm6, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm7, %xmm5
	pandn	%xmm0, %xmm2
	pandn	%xmm8, %xmm3
	por	%xmm2, %xmm3
	pandn	%xmm1, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	movdqu	%xmm3, 1584(%r14)
	movdqu	%xmm5, 1600(%r14)
	movdqu	2160(%r14), %xmm0
	movdqu	2176(%r14), %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqb	%xmm6, %xmm2
	movdqa	%xmm2, %xmm3
	pxor	%xmm7, %xmm3
	movdqa	%xmm1, %xmm4
	pcmpeqb	%xmm6, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm7, %xmm5
	pandn	%xmm0, %xmm2
	pandn	%xmm8, %xmm3
	por	%xmm2, %xmm3
	pandn	%xmm1, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	movdqu	%xmm3, 1616(%r14)
	movdqu	%xmm5, 1632(%r14)
	movl	1308(%r14), %eax
	testq	%rax, %rax
	je	.LBB24_51
# BB#41:                                # %.lr.ph.i59
                                        #   in Loop: Header=BB24_36 Depth=1
	movq	1328(%r14), %rcx
	testb	$1, %al
	jne	.LBB24_43
# BB#42:                                #   in Loop: Header=BB24_36 Depth=1
	xorl	%edx, %edx
	cmpl	$1, %eax
	jne	.LBB24_46
	jmp	.LBB24_51
.LBB24_43:                              #   in Loop: Header=BB24_36 Depth=1
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%rip), %esi
	movb	2193(%r14,%rsi), %dl
	testb	%dl, %dl
	jne	.LBB24_45
# BB#44:                                #   in Loop: Header=BB24_36 Depth=1
	movb	$11, %dl
.LBB24_45:                              #   in Loop: Header=BB24_36 Depth=1
	addb	(%rcx,%rsi), %dl
	movb	%dl, 1648(%r14)
	movl	$1, %edx
	cmpl	$1, %eax
	je	.LBB24_51
	.p2align	4, 0x90
.LBB24_46:                              #   Parent Loop BB24_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%rdx), %esi
	movzbl	2193(%r14,%rsi), %ebx
	testb	%bl, %bl
	jne	.LBB24_48
# BB#47:                                #   in Loop: Header=BB24_46 Depth=2
	movb	$11, %bl
.LBB24_48:                              #   in Loop: Header=BB24_46 Depth=2
	addb	(%rcx,%rsi), %bl
	movb	%bl, 1648(%r14,%rdx)
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE+1(%rdx), %esi
	movzbl	2193(%r14,%rsi), %ebx
	testb	%bl, %bl
	jne	.LBB24_50
# BB#49:                                #   in Loop: Header=BB24_46 Depth=2
	movb	$11, %bl
.LBB24_50:                              #   in Loop: Header=BB24_46 Depth=2
	addb	(%rcx,%rsi), %bl
	movb	%bl, 1649(%r14,%rdx)
	addq	$2, %rdx
	cmpq	%rdx, %rax
	jne	.LBB24_46
.LBB24_51:                              # %vector.body135
                                        #   in Loop: Header=BB24_36 Depth=1
	movdqu	2224(%r14), %xmm0
	movdqa	%xmm0, %xmm1
	pxor	%xmm3, %xmm3
	pcmpeqb	%xmm3, %xmm1
	movdqa	%xmm1, %xmm2
	pcmpeqd	%xmm4, %xmm4
	pxor	%xmm4, %xmm2
	pandn	%xmm0, %xmm1
	movdqa	.LCPI24_1(%rip), %xmm0  # xmm0 = [6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6]
	movdqa	%xmm0, %xmm5
	pandn	%xmm5, %xmm2
	por	%xmm1, %xmm2
	paddb	.LCPI24_2(%rip), %xmm2
	movdqu	%xmm2, 1904(%r14)
	movdqu	2240(%r14), %xmm0
	movdqa	%xmm0, %xmm1
	pcmpeqb	%xmm3, %xmm1
	movdqa	%xmm1, %xmm2
	pxor	%xmm4, %xmm2
	pandn	%xmm0, %xmm1
	pandn	%xmm5, %xmm2
	por	%xmm1, %xmm2
	paddb	.LCPI24_3(%rip), %xmm2
	movdqu	%xmm2, 1920(%r14)
	movl	%edi, %ebx
.LBB24_52:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE.exit69
                                        #   in Loop: Header=BB24_36 Depth=1
	incl	%ebp
	cmpl	%ebx, %ebp
	jne	.LBB24_36
# BB#53:
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	%r15, %rbp
	jmp	.LBB24_55
.LBB24_54:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE.exit.preheader._ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE.exit._crit_edge_crit_edge
	leaq	1936(%r14), %rbp
.LBB24_55:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE.exit._crit_edge
	addq	%r8, %rdi
	movl	$320, %edx              # imm = 0x140
	movq	%rbp, %rsi
	callq	memcpy
	movabsq	$1224065679360, %rsi    # imm = 0x11D00000000
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB24_56:                              # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rdx
	movq	%rsi, %r15
	leaq	286(%rdx), %rcx
	movl	%ecx, 1360(%r14)
	cmpq	$258, %rcx              # imm = 0x102
	jb	.LBB24_58
# BB#57:                                #   in Loop: Header=BB24_56 Depth=1
	leaq	-1(%rdx), %rdi
	leaq	(%r15,%rax), %rsi
	cmpb	$0, 2221(%r14,%rdx)
	je	.LBB24_56
.LBB24_58:                              # %.critedge
	movabsq	$133143986176, %rsi     # imm = 0x1F00000000
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB24_59:                              # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rbx
	movq	%rsi, %r11
	leaq	32(%rbx), %r12
	movl	%r12d, 1364(%r14)
	cmpq	$2, %r12
	jb	.LBB24_61
# BB#60:                                #   in Loop: Header=BB24_59 Depth=1
	leaq	-1(%rbx), %rdi
	leaq	(%r11,%rax), %rsi
	cmpb	$0, 2255(%r14,%rbx)
	je	.LBB24_59
.LBB24_61:                              # %.critedge1
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 60(%rsp)
	movdqa	%xmm0, 48(%rsp)
	movdqa	%xmm0, 32(%rsp)
	movdqa	%xmm0, 16(%rsp)
	movdqa	%xmm0, (%rsp)
	testl	%ecx, %ecx
	jle	.LBB24_80
# BB#62:                                # %.lr.ph.i37
	movzbl	(%rbp), %ebp
	xorl	%edi, %edi
	testb	%bpl, %bpl
	setne	%dil
	movl	$138, %eax
	movl	$7, %r13d
	cmovel	%eax, %r13d
	addl	$3, %edi
	leaq	72(%rsp), %r8
	leaq	64(%rsp), %r10
	sarq	$32, %r15
	xorl	%ecx, %ecx
	movl	$255, %esi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB24_63:                              # =>This Inner Loop Header: Depth=1
	movl	%ebp, %r9d
	cmpq	%r15, %rcx
	movl	$255, %ebp
	jge	.LBB24_65
# BB#64:                                #   in Loop: Header=BB24_63 Depth=1
	movzbl	1937(%r14,%rcx), %ebp
.LBB24_65:                              #   in Loop: Header=BB24_63 Depth=1
	incl	%eax
	cmpl	%r13d, %eax
	jge	.LBB24_68
# BB#66:                                #   in Loop: Header=BB24_63 Depth=1
	cmpl	%ebp, %r9d
	jne	.LBB24_68
# BB#67:                                #   in Loop: Header=BB24_63 Depth=1
	movl	%esi, %r9d
	jmp	.LBB24_79
	.p2align	4, 0x90
.LBB24_68:                              #   in Loop: Header=BB24_63 Depth=1
	cmpl	%edi, %eax
	jge	.LBB24_70
# BB#69:                                #   in Loop: Header=BB24_63 Depth=1
	movslq	%r9d, %rsi
	leaq	(%rsp,%rsi,4), %rsi
	jmp	.LBB24_76
	.p2align	4, 0x90
.LBB24_70:                              #   in Loop: Header=BB24_63 Depth=1
	testl	%r9d, %r9d
	je	.LBB24_73
# BB#71:                                #   in Loop: Header=BB24_63 Depth=1
	movl	$1, %eax
	cmpl	%esi, %r9d
	je	.LBB24_75
# BB#74:                                #   in Loop: Header=BB24_63 Depth=1
	movslq	%r9d, %rsi
	incl	(%rsp,%rsi,4)
.LBB24_75:                              #   in Loop: Header=BB24_63 Depth=1
	movq	%r10, %rsi
	jmp	.LBB24_76
.LBB24_73:                              #   in Loop: Header=BB24_63 Depth=1
	cmpl	$11, %eax
	movq	%r8, %rsi
	leaq	68(%rsp), %rax
	cmovlq	%rax, %rsi
	movl	$1, %eax
	.p2align	4, 0x90
.LBB24_76:                              #   in Loop: Header=BB24_63 Depth=1
	addl	%eax, (%rsi)
	xorl	%eax, %eax
	testl	%ebp, %ebp
	je	.LBB24_78
# BB#77:                                #   in Loop: Header=BB24_63 Depth=1
	xorl	%edi, %edi
	cmpl	%ebp, %r9d
	setne	%dil
	leal	6(%rdi), %r13d
	addl	$3, %edi
	jmp	.LBB24_79
.LBB24_78:                              #   in Loop: Header=BB24_63 Depth=1
	movl	$3, %edi
	movl	$138, %r13d
	.p2align	4, 0x90
.LBB24_79:                              #   in Loop: Header=BB24_63 Depth=1
	leaq	1(%rcx), %rsi
	addq	$-285, %rcx             # imm = 0xFEE3
	cmpq	%rdx, %rcx
	movq	%rsi, %rcx
	movl	%r9d, %esi
	jne	.LBB24_63
.LBB24_80:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder15LevelTableDummyEPKhiPj.exit56
	testl	%r12d, %r12d
	jle	.LBB24_99
# BB#81:                                # %.lr.ph.i30
	movzbl	2224(%r14), %edx
	xorl	%esi, %esi
	testb	%dl, %dl
	setne	%sil
	movl	$138, %eax
	movl	$7, %r15d
	cmovel	%eax, %r15d
	addl	$3, %esi
	leaq	72(%rsp), %r9
	leaq	68(%rsp), %r8
	leaq	64(%rsp), %r10
	sarq	$32, %r11
	xorl	%ecx, %ecx
	movl	$255, %edi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB24_82:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ebp
	cmpq	%r11, %rcx
	movl	$255, %edx
	jge	.LBB24_84
# BB#83:                                #   in Loop: Header=BB24_82 Depth=1
	movzbl	2225(%r14,%rcx), %edx
.LBB24_84:                              #   in Loop: Header=BB24_82 Depth=1
	incl	%eax
	cmpl	%r15d, %eax
	jge	.LBB24_87
# BB#85:                                #   in Loop: Header=BB24_82 Depth=1
	cmpl	%edx, %ebp
	jne	.LBB24_87
# BB#86:                                #   in Loop: Header=BB24_82 Depth=1
	movl	%edi, %ebp
	jmp	.LBB24_98
	.p2align	4, 0x90
.LBB24_87:                              #   in Loop: Header=BB24_82 Depth=1
	cmpl	%esi, %eax
	jge	.LBB24_89
# BB#88:                                #   in Loop: Header=BB24_82 Depth=1
	movslq	%ebp, %rsi
	leaq	(%rsp,%rsi,4), %rsi
	jmp	.LBB24_95
	.p2align	4, 0x90
.LBB24_89:                              #   in Loop: Header=BB24_82 Depth=1
	testl	%ebp, %ebp
	je	.LBB24_92
# BB#90:                                #   in Loop: Header=BB24_82 Depth=1
	movl	$1, %eax
	cmpl	%edi, %ebp
	je	.LBB24_94
# BB#93:                                #   in Loop: Header=BB24_82 Depth=1
	movslq	%ebp, %rsi
	incl	(%rsp,%rsi,4)
.LBB24_94:                              #   in Loop: Header=BB24_82 Depth=1
	movq	%r10, %rsi
	jmp	.LBB24_95
.LBB24_92:                              #   in Loop: Header=BB24_82 Depth=1
	cmpl	$11, %eax
	movq	%r9, %rsi
	cmovlq	%r8, %rsi
	movl	$1, %eax
	.p2align	4, 0x90
.LBB24_95:                              #   in Loop: Header=BB24_82 Depth=1
	addl	%eax, (%rsi)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.LBB24_97
# BB#96:                                #   in Loop: Header=BB24_82 Depth=1
	xorl	%esi, %esi
	cmpl	%edx, %ebp
	setne	%sil
	leal	6(%rsi), %r15d
	addl	$3, %esi
	jmp	.LBB24_98
.LBB24_97:                              #   in Loop: Header=BB24_82 Depth=1
	movl	$3, %esi
	movl	$138, %r15d
	.p2align	4, 0x90
.LBB24_98:                              #   in Loop: Header=BB24_82 Depth=1
	leaq	1(%rcx), %rdi
	addq	$-31, %rcx
	cmpq	%rbx, %rcx
	movq	%rdi, %rcx
	movl	%ebp, %edi
	jne	.LBB24_82
.LBB24_99:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder15LevelTableDummyEPKhiPj.exit
	leaq	4816(%r14), %rsi
	leaq	4892(%r14), %rdx
	movq	%rsp, %rdi
	movl	$19, %ecx
	movl	$7, %r8d
	callq	Huffman_Generate
	movl	$4, 1368(%r14)
	xorl	%eax, %eax
	jmp	.LBB24_101
	.p2align	4, 0x90
.LBB24_100:                             #   in Loop: Header=BB24_101 Depth=1
	movb	%cl, 1339(%r14,%rax)
	addq	$2, %rax
.LBB24_101:                             # =>This Inner Loop Header: Depth=1
	movzbl	_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE(%rax), %ecx
	movzbl	4892(%r14,%rcx), %ecx
	testb	%cl, %cl
	je	.LBB24_104
# BB#102:                               #   in Loop: Header=BB24_101 Depth=1
	movl	1368(%r14), %edx
	cmpq	%rdx, %rax
	jb	.LBB24_104
# BB#103:                               #   in Loop: Header=BB24_101 Depth=1
	leal	1(%rax), %edx
	movl	%edx, 1368(%r14)
.LBB24_104:                             #   in Loop: Header=BB24_101 Depth=1
	movb	%cl, 1338(%r14,%rax)
	cmpq	$18, %rax
	je	.LBB24_108
# BB#105:                               #   in Loop: Header=BB24_101 Depth=1
	movzbl	_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE+1(%rax), %ecx
	movzbl	4892(%r14,%rcx), %ecx
	testb	%cl, %cl
	je	.LBB24_100
# BB#106:                               #   in Loop: Header=BB24_101 Depth=1
	leaq	1(%rax), %rdx
	movl	1368(%r14), %esi
	cmpq	%rsi, %rdx
	jb	.LBB24_100
# BB#107:                               #   in Loop: Header=BB24_101 Depth=1
	leal	2(%rax), %edx
	movl	%edx, 1368(%r14)
	jmp	.LBB24_100
.LBB24_108:                             # %min.iters.checked167
	movq	1328(%r14), %rcx
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB24_109:                             # %vector.body164
                                        # =>This Inner Loop Header: Depth=1
	movd	1936(%r14,%rax), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movd	1940(%r14,%rax), %xmm4  # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	movdqu	2256(%r14,%rax,4), %xmm5
	movdqu	2272(%r14,%rax,4), %xmm6
	pshufd	$245, %xmm3, %xmm7      # xmm7 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm7, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm6, %xmm6      # xmm6 = xmm6[1,1,3,3]
	pmuludq	%xmm5, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	paddd	%xmm1, %xmm3
	paddd	%xmm2, %xmm4
	movd	1944(%r14,%rax), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movd	1948(%r14,%rax), %xmm2  # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movdqu	2288(%r14,%rax,4), %xmm5
	movdqu	2304(%r14,%rax,4), %xmm6
	pshufd	$245, %xmm1, %xmm7      # xmm7 = xmm1[1,1,3,3]
	pmuludq	%xmm5, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm7, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	pshufd	$245, %xmm2, %xmm5      # xmm5 = xmm2[1,1,3,3]
	pmuludq	%xmm6, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm6, %xmm6      # xmm6 = xmm6[1,1,3,3]
	pmuludq	%xmm5, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	punpckldq	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	paddd	%xmm3, %xmm1
	paddd	%xmm4, %xmm2
	addq	$16, %rax
	cmpq	$288, %rax              # imm = 0x120
	jne	.LBB24_109
# BB#110:                               # %vector.body225
	paddd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r8d
	movd	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	pxor	%xmm8, %xmm8
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	movdqu	3284(%r14), %xmm4
	movdqu	3300(%r14), %xmm5
	movdqu	3316(%r14), %xmm1
	movdqu	3332(%r14), %xmm2
	pshufd	$245, %xmm3, %xmm6      # xmm6 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm7      # xmm7 = xmm3[0,2,2,3]
	pshufd	$245, %xmm4, %xmm3      # xmm3 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	movd	4(%rcx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	pshufd	$245, %xmm3, %xmm4      # xmm4 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm4, %xmm5
	pshufd	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddd	%xmm7, %xmm3
	movd	8(%rcx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3],xmm4[4],xmm8[4],xmm4[5],xmm8[5],xmm4[6],xmm8[6],xmm4[7],xmm8[7]
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm5, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movd	12(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	pshufd	$245, %xmm1, %xmm5      # xmm5 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm2, %xmm2      # xmm2 = xmm2[1,1,3,3]
	pmuludq	%xmm5, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	paddd	%xmm4, %xmm1
	paddd	%xmm3, %xmm1
	movd	16(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3],xmm2[4],xmm8[4],xmm2[5],xmm8[5],xmm2[6],xmm8[6],xmm2[7],xmm8[7]
	punpcklwd	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3]
	movdqu	3348(%r14), %xmm3
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movd	20(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	movdqu	3364(%r14), %xmm4
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddd	%xmm2, %xmm3
	movd	24(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3],xmm2[4],xmm8[4],xmm2[5],xmm8[5],xmm2[6],xmm8[6],xmm2[7],xmm8[7]
	punpcklwd	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3]
	movdqu	3380(%r14), %xmm4
	pshufd	$245, %xmm2, %xmm5      # xmm5 = xmm2[1,1,3,3]
	pmuludq	%xmm4, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	paddd	%xmm3, %xmm2
	paddd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm1       # xmm1 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm1
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm2
	movd	%xmm2, %edx
	movd	2224(%r14), %xmm1       # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	movdqu	3408(%r14), %xmm2
	pshufd	$245, %xmm1, %xmm3      # xmm3 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm4      # xmm4 = xmm1[0,2,2,3]
	pshufd	$245, %xmm2, %xmm1      # xmm1 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movd	2228(%r14), %xmm2       # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3],xmm2[4],xmm8[4],xmm2[5],xmm8[5],xmm2[6],xmm8[6],xmm2[7],xmm8[7]
	punpcklwd	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3]
	movdqu	3424(%r14), %xmm14
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm14, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm14, %xmm0     # xmm0 = xmm14[1,1,3,3]
	movdqa	%xmm0, 80(%rsp)         # 16-byte Spill
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	paddd	%xmm4, %xmm2
	movd	2232(%r14), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3],xmm4[4],xmm8[4],xmm4[5],xmm8[5],xmm4[6],xmm8[6],xmm4[7],xmm8[7]
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	movdqu	3440(%r14), %xmm15
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm15, %xmm4
	pshufd	$232, %xmm4, %xmm7      # xmm7 = xmm4[0,2,2,3]
	pshufd	$245, %xmm15, %xmm9     # xmm9 = xmm15[1,1,3,3]
	pmuludq	%xmm9, %xmm5
	pshufd	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3]
	punpckldq	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	movd	2236(%r14), %xmm5       # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	punpcklwd	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3]
	movdqu	3456(%r14), %xmm4
	pshufd	$245, %xmm5, %xmm1      # xmm1 = xmm5[1,1,3,3]
	pmuludq	%xmm4, %xmm5
	pshufd	$232, %xmm5, %xmm6      # xmm6 = xmm5[0,2,2,3]
	pshufd	$245, %xmm4, %xmm10     # xmm10 = xmm4[1,1,3,3]
	pmuludq	%xmm10, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	paddd	%xmm7, %xmm6
	paddd	%xmm2, %xmm6
	movd	2240(%r14), %xmm1       # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	movdqu	3472(%r14), %xmm7
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm7, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm7, %xmm11     # xmm11 = xmm7[1,1,3,3]
	pmuludq	%xmm11, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movd	2244(%r14), %xmm5       # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	punpcklwd	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3]
	movdqu	3488(%r14), %xmm2
	pshufd	$245, %xmm5, %xmm3      # xmm3 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm13     # xmm13 = xmm5[0,2,2,3]
	pshufd	$245, %xmm2, %xmm12     # xmm12 = xmm2[1,1,3,3]
	pmuludq	%xmm12, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm13   # xmm13 = xmm13[0],xmm3[0],xmm13[1],xmm3[1]
	paddd	%xmm1, %xmm13
	movd	2248(%r14), %xmm1       # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	movdqu	3504(%r14), %xmm5
	pshufd	$245, %xmm1, %xmm0      # xmm0 = xmm1[1,1,3,3]
	pmuludq	%xmm5, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm5, %xmm3      # xmm3 = xmm5[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	paddd	%xmm13, %xmm1
	paddd	%xmm6, %xmm1
	movd	2252(%r14), %xmm6       # xmm6 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3],xmm6[4],xmm8[4],xmm6[5],xmm8[5],xmm6[6],xmm8[6],xmm6[7],xmm8[7]
	punpcklwd	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3]
	movdqu	3520(%r14), %xmm8
	pshufd	$245, %xmm6, %xmm0      # xmm0 = xmm6[1,1,3,3]
	pmuludq	%xmm8, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm8, %xmm13     # xmm13 = xmm8[1,1,3,3]
	pmuludq	%xmm13, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm1, %xmm6
	pshufd	$78, %xmm6, %xmm0       # xmm0 = xmm6[2,3,0,1]
	paddd	%xmm6, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r9d
	movdqa	.LCPI24_4(%rip), %xmm0  # xmm0 = [1,1,2,2]
	pmuludq	%xmm0, %xmm14
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	80(%rsp), %xmm0         # 16-byte Folded Reload
	pshufd	$232, %xmm14, %xmm1     # xmm1 = xmm14[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movdqa	.LCPI24_5(%rip), %xmm0  # xmm0 = [3,3,4,4]
	pmuludq	%xmm0, %xmm15
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm9, %xmm0
	pshufd	$232, %xmm15, %xmm6     # xmm6 = xmm15[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm1, %xmm6
	movdqa	.LCPI24_6(%rip), %xmm0  # xmm0 = [5,5,6,6]
	pmuludq	%xmm0, %xmm4
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm10, %xmm0
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	movdqa	.LCPI24_7(%rip), %xmm0  # xmm0 = [7,7,8,8]
	pmuludq	%xmm0, %xmm7
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm11, %xmm0
	pshufd	$232, %xmm7, %xmm1      # xmm1 = xmm7[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	paddd	%xmm4, %xmm1
	paddd	%xmm6, %xmm1
	movdqa	.LCPI24_8(%rip), %xmm0  # xmm0 = [9,9,10,10]
	pmuludq	%xmm0, %xmm2
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm12, %xmm0
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movdqa	.LCPI24_9(%rip), %xmm0  # xmm0 = [11,11,12,12]
	pmuludq	%xmm0, %xmm5
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm5, %xmm3      # xmm3 = xmm5[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	paddd	%xmm2, %xmm3
	movdqa	.LCPI24_10(%rip), %xmm0 # xmm0 = [13,13,14,14]
	pmuludq	%xmm0, %xmm8
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm13, %xmm0
	pshufd	$232, %xmm8, %xmm2      # xmm2 = xmm8[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	paddd	%xmm3, %xmm2
	paddd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edi
	movzbl	28(%rcx), %ebp
	imull	3396(%r14), %ebp
	addl	%edx, %ebp
	movzbl	29(%rcx), %edx
	imull	3400(%r14), %edx
	addl	%ebp, %edx
	movzbl	30(%rcx), %ecx
	imull	3404(%r14), %ecx
	addl	%edx, %ecx
	movzbl	4892(%r14), %edx
	imull	(%rsp), %edx
	movzbl	4893(%r14), %ebp
	imull	4(%rsp), %ebp
	addl	%edx, %ebp
	movzbl	4894(%r14), %edx
	imull	8(%rsp), %edx
	addl	%ebp, %edx
	movzbl	4895(%r14), %ebp
	imull	12(%rsp), %ebp
	addl	%edx, %ebp
	movzbl	4896(%r14), %edx
	imull	16(%rsp), %edx
	addl	%ebp, %edx
	movzbl	4897(%r14), %ebp
	imull	20(%rsp), %ebp
	addl	%edx, %ebp
	movzbl	4898(%r14), %edx
	imull	24(%rsp), %edx
	addl	%ebp, %edx
	movzbl	4899(%r14), %ebp
	imull	28(%rsp), %ebp
	addl	%edx, %ebp
	movzbl	4900(%r14), %edx
	imull	32(%rsp), %edx
	addl	%ebp, %edx
	movzbl	4901(%r14), %ebp
	imull	36(%rsp), %ebp
	addl	%edx, %ebp
	movzbl	4902(%r14), %edx
	imull	40(%rsp), %edx
	addl	%ebp, %edx
	movzbl	4903(%r14), %ebp
	imull	44(%rsp), %ebp
	addl	%edx, %ebp
	movzbl	4904(%r14), %edx
	imull	48(%rsp), %edx
	addl	%ebp, %edx
	movzbl	4905(%r14), %ebp
	imull	52(%rsp), %ebp
	addl	%edx, %ebp
	movzbl	4906(%r14), %edx
	imull	56(%rsp), %edx
	addl	%ebp, %edx
	movzbl	4907(%r14), %ebp
	imull	60(%rsp), %ebp
	addl	%edx, %ebp
	movzbl	4908(%r14), %edx
	movl	64(%rsp), %ebx
	movl	68(%rsp), %eax
	imull	%ebx, %edx
	addl	%ebp, %edx
	movzbl	4909(%r14), %ebp
	imull	%eax, %ebp
	addl	%edx, %ebp
	movzbl	4910(%r14), %edx
	movl	72(%rsp), %esi
	imull	%esi, %edx
	addl	%ebp, %edx
	leal	(%rax,%rax,2), %eax
	leal	(%rax,%rbx,2), %eax
	leal	(,%rsi,8), %ebp
	subl	%esi, %ebp
	addl	%eax, %ebp
	imull	$3, 1368(%r14), %eax
	addl	%r8d, %ecx
	addl	%r9d, %ecx
	addl	%edi, %ecx
	addl	%edx, %ecx
	addl	%ebp, %ecx
	leal	17(%rcx,%rax), %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder11TryDynBlockEij, .Lfunc_end24-_ZN9NCompress8NDeflate8NEncoder6CCoder11TryDynBlockEij
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI25_0:
	.zero	16,8
.LCPI25_1:
	.zero	16,9
.LCPI25_2:
	.zero	16,7
.LCPI25_3:
	.zero	16,5
.LCPI25_4:
	.zero	16,11
.LCPI25_5:
	.zero	16,6
.LCPI25_6:
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	6                       # 0x6
.LCPI25_7:
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	14                      # 0xe
.LCPI25_8:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
.LCPI25_9:
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI25_10:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
.LCPI25_11:
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI25_12:
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
.LCPI25_13:
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
.LCPI25_14:
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder13TryFixedBlockEi
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder13TryFixedBlockEi,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder13TryFixedBlockEi: # @_ZN9NCompress8NDeflate8NEncoder6CCoder13TryFixedBlockEi
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi144:
	.cfi_def_cfa_offset 32
.Lcfi145:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	4920(%rbx), %rax
	movslq	%esi, %rcx
	imulq	$332, %rcx, %rcx        # imm = 0x14C
	movl	324(%rax,%rcx), %edx
	movl	%edx, 4912(%rbx)
	movl	328(%rax,%rcx), %eax
	movl	%eax, 1288(%rbx)
	movaps	.LCPI25_0(%rip), %xmm0  # xmm0 = [8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]
	movups	%xmm0, 2064(%rbx)
	movups	%xmm0, 2048(%rbx)
	movups	%xmm0, 2032(%rbx)
	movups	%xmm0, 2016(%rbx)
	movups	%xmm0, 2000(%rbx)
	movups	%xmm0, 1984(%rbx)
	movups	%xmm0, 1968(%rbx)
	movups	%xmm0, 1952(%rbx)
	movups	%xmm0, 1936(%rbx)
	movaps	.LCPI25_1(%rip), %xmm0  # xmm0 = [9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9]
	movups	%xmm0, 2176(%rbx)
	movups	%xmm0, 2160(%rbx)
	movups	%xmm0, 2144(%rbx)
	movups	%xmm0, 2128(%rbx)
	movups	%xmm0, 2112(%rbx)
	movups	%xmm0, 2096(%rbx)
	movups	%xmm0, 2080(%rbx)
	movabsq	$506381209866536711, %rax # imm = 0x707070707070707
	movq	%rax, 2208(%rbx)
	movaps	.LCPI25_2(%rip), %xmm0  # xmm0 = [7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7]
	movups	%xmm0, 2192(%rbx)
	movabsq	$578721382704613384, %rax # imm = 0x808080808080808
	movq	%rax, 2216(%rbx)
	movdqa	.LCPI25_3(%rip), %xmm0  # xmm0 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	movdqu	%xmm0, 2240(%rbx)
	movdqu	%xmm0, 2224(%rbx)
	cmpb	$0, 1268(%rbx)
	jne	.LBB25_22
# BB#1:                                 # %vector.body
	movb	$8, 1392(%rbx)
	movdqu	1937(%rbx), %xmm2
	movdqu	1953(%rbx), %xmm3
	pxor	%xmm0, %xmm0
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	pcmpeqd	%xmm1, %xmm1
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	movdqa	.LCPI25_4(%rip), %xmm8  # xmm8 = [11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11]
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1393(%rbx)
	movdqu	%xmm7, 1409(%rbx)
	movdqu	1969(%rbx), %xmm3
	movdqu	1985(%rbx), %xmm2
	movdqa	%xmm3, %xmm5
	pcmpeqb	%xmm0, %xmm5
	movdqa	%xmm5, %xmm6
	pxor	%xmm1, %xmm6
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm3, %xmm5
	pandn	%xmm8, %xmm6
	por	%xmm5, %xmm6
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm7
	por	%xmm4, %xmm7
	movdqu	%xmm6, 1425(%rbx)
	movdqu	%xmm7, 1441(%rbx)
	movdqu	2001(%rbx), %xmm2
	movdqu	2017(%rbx), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1457(%rbx)
	movdqu	%xmm7, 1473(%rbx)
	movdqu	2033(%rbx), %xmm2
	movdqu	2049(%rbx), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1489(%rbx)
	movdqu	%xmm7, 1505(%rbx)
	movdqu	2065(%rbx), %xmm2
	movdqu	2081(%rbx), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1521(%rbx)
	movdqu	%xmm7, 1537(%rbx)
	movdqu	2097(%rbx), %xmm2
	movdqu	2113(%rbx), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm3, %xmm6
	pcmpeqb	%xmm0, %xmm6
	movdqa	%xmm6, %xmm7
	pxor	%xmm1, %xmm7
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm6
	pandn	%xmm8, %xmm7
	por	%xmm6, %xmm7
	movdqu	%xmm5, 1553(%rbx)
	movdqu	%xmm7, 1569(%rbx)
	movdqu	2129(%rbx), %xmm2
	movdqu	2145(%rbx), %xmm3
	movdqa	%xmm2, %xmm4
	pcmpeqb	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm1, %xmm5
	pcmpeqb	%xmm3, %xmm0
	pxor	%xmm0, %xmm1
	pandn	%xmm2, %xmm4
	pandn	%xmm8, %xmm5
	por	%xmm4, %xmm5
	pandn	%xmm3, %xmm0
	pandn	%xmm8, %xmm1
	por	%xmm0, %xmm1
	movdqu	%xmm5, 1585(%rbx)
	movdqu	%xmm1, 1601(%rbx)
	xorl	%eax, %eax
	jmp	.LBB25_2
	.p2align	4, 0x90
.LBB25_27:                              # %.preheader28.i..preheader28.i_crit_edge.3
                                        #   in Loop: Header=BB25_2 Depth=1
	movb	%cl, 1620(%rbx,%rax)
	addq	$4, %rax
.LBB25_2:                               # %.preheader28.i..preheader28.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movzbl	2161(%rbx,%rax), %ecx
	testb	%cl, %cl
	jne	.LBB25_4
# BB#3:                                 # %.preheader28.i..preheader28.i_crit_edge
                                        #   in Loop: Header=BB25_2 Depth=1
	movb	$11, %cl
.LBB25_4:                               # %.preheader28.i..preheader28.i_crit_edge
                                        #   in Loop: Header=BB25_2 Depth=1
	movb	%cl, 1617(%rbx,%rax)
	movzbl	2162(%rbx,%rax), %ecx
	testb	%cl, %cl
	jne	.LBB25_6
# BB#5:                                 # %.preheader28.i..preheader28.i_crit_edge
                                        #   in Loop: Header=BB25_2 Depth=1
	movb	$11, %cl
.LBB25_6:                               # %.preheader28.i..preheader28.i_crit_edge
                                        #   in Loop: Header=BB25_2 Depth=1
	movb	%cl, 1618(%rbx,%rax)
	movzbl	2163(%rbx,%rax), %ecx
	testb	%cl, %cl
	jne	.LBB25_8
# BB#7:                                 # %.preheader28.i..preheader28.i_crit_edge
                                        #   in Loop: Header=BB25_2 Depth=1
	movb	$11, %cl
.LBB25_8:                               # %.preheader28.i..preheader28.i_crit_edge
                                        #   in Loop: Header=BB25_2 Depth=1
	movb	%cl, 1619(%rbx,%rax)
	cmpq	$28, %rax
	je	.LBB25_9
# BB#25:                                # %.preheader28.i..preheader28.i_crit_edge.3
                                        #   in Loop: Header=BB25_2 Depth=1
	movzbl	2164(%rbx,%rax), %ecx
	testb	%cl, %cl
	jne	.LBB25_27
# BB#26:                                # %.preheader28.i..preheader28.i_crit_edge.3
                                        #   in Loop: Header=BB25_2 Depth=1
	movb	$11, %cl
	jmp	.LBB25_27
.LBB25_9:                               # %.preheader27.i
	movl	1308(%rbx), %edi
	testq	%rdi, %rdi
	je	.LBB25_21
# BB#10:                                # %.lr.ph.i
	movq	1328(%rbx), %rcx
	testb	$1, %dil
	jne	.LBB25_12
# BB#11:
	xorl	%edx, %edx
	cmpl	$1, %edi
	jne	.LBB25_16
	jmp	.LBB25_21
.LBB25_12:
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%rip), %esi
	movb	2193(%rbx,%rsi), %dl
	testb	%dl, %dl
	jne	.LBB25_14
# BB#13:
	movb	$11, %dl
.LBB25_14:
	addb	(%rcx,%rsi), %dl
	movb	%dl, 1648(%rbx)
	movl	$1, %edx
	cmpl	$1, %edi
	je	.LBB25_21
	.p2align	4, 0x90
.LBB25_16:                              # =>This Inner Loop Header: Depth=1
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%rdx), %esi
	movzbl	2193(%rbx,%rsi), %eax
	testb	%al, %al
	jne	.LBB25_18
# BB#17:                                #   in Loop: Header=BB25_16 Depth=1
	movb	$11, %al
.LBB25_18:                              #   in Loop: Header=BB25_16 Depth=1
	addb	(%rcx,%rsi), %al
	movb	%al, 1648(%rbx,%rdx)
	movzbl	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE+1(%rdx), %esi
	movzbl	2193(%rbx,%rsi), %eax
	testb	%al, %al
	jne	.LBB25_20
# BB#19:                                #   in Loop: Header=BB25_16 Depth=1
	movb	$11, %al
.LBB25_20:                              #   in Loop: Header=BB25_16 Depth=1
	addb	(%rcx,%rsi), %al
	movb	%al, 1649(%rbx,%rdx)
	addq	$2, %rdx
	cmpq	%rdx, %rdi
	jne	.LBB25_16
.LBB25_21:                              # %vector.body19
	movdqu	2224(%rbx), %xmm0
	pxor	%xmm1, %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqb	%xmm1, %xmm2
	pcmpeqd	%xmm3, %xmm3
	movdqa	%xmm2, %xmm4
	pxor	%xmm3, %xmm4
	pandn	%xmm0, %xmm2
	movdqa	.LCPI25_5(%rip), %xmm0  # xmm0 = [6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6]
	pandn	%xmm0, %xmm4
	por	%xmm2, %xmm4
	paddb	.LCPI25_6(%rip), %xmm4
	movdqu	%xmm4, 1904(%rbx)
	movdqu	2240(%rbx), %xmm2
	pcmpeqb	%xmm2, %xmm1
	pxor	%xmm1, %xmm3
	pandn	%xmm2, %xmm1
	pandn	%xmm0, %xmm3
	por	%xmm1, %xmm3
	paddb	.LCPI25_7(%rip), %xmm3
	movdqu	%xmm3, 1920(%rbx)
.LBB25_22:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9SetPricesERKNS0_7CLevelsE.exit
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder8TryBlockEv
	movq	1328(%rbx), %rax
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB25_23:                              # %vector.body33
                                        # =>This Inner Loop Header: Depth=1
	movd	1936(%rbx,%rcx), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movd	1940(%rbx,%rcx), %xmm4  # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	movdqu	2256(%rbx,%rcx,4), %xmm5
	movdqu	2272(%rbx,%rcx,4), %xmm6
	pshufd	$245, %xmm3, %xmm7      # xmm7 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm7, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm6, %xmm6      # xmm6 = xmm6[1,1,3,3]
	pmuludq	%xmm5, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	paddd	%xmm1, %xmm3
	paddd	%xmm2, %xmm4
	movd	1944(%rbx,%rcx), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movd	1948(%rbx,%rcx), %xmm2  # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movdqu	2288(%rbx,%rcx,4), %xmm5
	movdqu	2304(%rbx,%rcx,4), %xmm6
	pshufd	$245, %xmm1, %xmm7      # xmm7 = xmm1[1,1,3,3]
	pmuludq	%xmm5, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm7, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	pshufd	$245, %xmm2, %xmm5      # xmm5 = xmm2[1,1,3,3]
	pmuludq	%xmm6, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm6, %xmm6      # xmm6 = xmm6[1,1,3,3]
	pmuludq	%xmm5, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	punpckldq	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	paddd	%xmm3, %xmm1
	paddd	%xmm4, %xmm2
	addq	$16, %rcx
	cmpq	$288, %rcx              # imm = 0x120
	jne	.LBB25_23
# BB#24:                                # %vector.body94
	paddd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r8d
	movd	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	pxor	%xmm8, %xmm8
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	movdqu	3284(%rbx), %xmm4
	movdqu	3300(%rbx), %xmm5
	movdqu	3316(%rbx), %xmm1
	movdqu	3332(%rbx), %xmm2
	pshufd	$245, %xmm3, %xmm6      # xmm6 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm7      # xmm7 = xmm3[0,2,2,3]
	pshufd	$245, %xmm4, %xmm3      # xmm3 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	movd	4(%rax), %xmm3          # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	pshufd	$245, %xmm3, %xmm4      # xmm4 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm4, %xmm5
	pshufd	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddd	%xmm7, %xmm3
	movd	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3],xmm4[4],xmm8[4],xmm4[5],xmm8[5],xmm4[6],xmm8[6],xmm4[7],xmm8[7]
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm5, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movd	12(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	pshufd	$245, %xmm1, %xmm5      # xmm5 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm2, %xmm2      # xmm2 = xmm2[1,1,3,3]
	pmuludq	%xmm5, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	paddd	%xmm4, %xmm1
	paddd	%xmm3, %xmm1
	movd	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3],xmm2[4],xmm8[4],xmm2[5],xmm8[5],xmm2[6],xmm8[6],xmm2[7],xmm8[7]
	punpcklwd	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3]
	movdqu	3348(%rbx), %xmm3
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movd	20(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	movdqu	3364(%rbx), %xmm4
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddd	%xmm2, %xmm3
	movd	24(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3],xmm2[4],xmm8[4],xmm2[5],xmm8[5],xmm2[6],xmm8[6],xmm2[7],xmm8[7]
	punpcklwd	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3]
	movdqu	3380(%rbx), %xmm4
	pshufd	$245, %xmm2, %xmm5      # xmm5 = xmm2[1,1,3,3]
	pmuludq	%xmm4, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	paddd	%xmm3, %xmm2
	paddd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm1       # xmm1 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm1
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm2
	movd	%xmm2, %edx
	movd	2224(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	movdqu	3408(%rbx), %xmm2
	pshufd	$245, %xmm1, %xmm3      # xmm3 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm4      # xmm4 = xmm1[0,2,2,3]
	pshufd	$245, %xmm2, %xmm1      # xmm1 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movd	2228(%rbx), %xmm2       # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3],xmm2[4],xmm8[4],xmm2[5],xmm8[5],xmm2[6],xmm8[6],xmm2[7],xmm8[7]
	punpcklwd	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3]
	movdqu	3424(%rbx), %xmm14
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm14, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm14, %xmm0     # xmm0 = xmm14[1,1,3,3]
	movdqa	%xmm0, (%rsp)           # 16-byte Spill
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	paddd	%xmm4, %xmm2
	movd	2232(%rbx), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3],xmm4[4],xmm8[4],xmm4[5],xmm8[5],xmm4[6],xmm8[6],xmm4[7],xmm8[7]
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	movdqu	3440(%rbx), %xmm15
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm15, %xmm4
	pshufd	$232, %xmm4, %xmm7      # xmm7 = xmm4[0,2,2,3]
	pshufd	$245, %xmm15, %xmm9     # xmm9 = xmm15[1,1,3,3]
	pmuludq	%xmm9, %xmm5
	pshufd	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3]
	punpckldq	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	movd	2236(%rbx), %xmm5       # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	punpcklwd	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3]
	movdqu	3456(%rbx), %xmm4
	pshufd	$245, %xmm5, %xmm1      # xmm1 = xmm5[1,1,3,3]
	pmuludq	%xmm4, %xmm5
	pshufd	$232, %xmm5, %xmm6      # xmm6 = xmm5[0,2,2,3]
	pshufd	$245, %xmm4, %xmm10     # xmm10 = xmm4[1,1,3,3]
	pmuludq	%xmm10, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	paddd	%xmm7, %xmm6
	paddd	%xmm2, %xmm6
	movd	2240(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	movdqu	3472(%rbx), %xmm7
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm7, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm7, %xmm11     # xmm11 = xmm7[1,1,3,3]
	pmuludq	%xmm11, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movd	2244(%rbx), %xmm5       # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	punpcklwd	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3]
	movdqu	3488(%rbx), %xmm2
	pshufd	$245, %xmm5, %xmm3      # xmm3 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm13     # xmm13 = xmm5[0,2,2,3]
	pshufd	$245, %xmm2, %xmm12     # xmm12 = xmm2[1,1,3,3]
	pmuludq	%xmm12, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm13   # xmm13 = xmm13[0],xmm3[0],xmm13[1],xmm3[1]
	paddd	%xmm1, %xmm13
	movd	2248(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3],xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	punpcklwd	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1],xmm1[2],xmm8[2],xmm1[3],xmm8[3]
	movdqu	3504(%rbx), %xmm5
	pshufd	$245, %xmm1, %xmm0      # xmm0 = xmm1[1,1,3,3]
	pmuludq	%xmm5, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm5, %xmm3      # xmm3 = xmm5[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	paddd	%xmm13, %xmm1
	paddd	%xmm6, %xmm1
	movd	2252(%rbx), %xmm6       # xmm6 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3],xmm6[4],xmm8[4],xmm6[5],xmm8[5],xmm6[6],xmm8[6],xmm6[7],xmm8[7]
	punpcklwd	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3]
	movdqu	3520(%rbx), %xmm8
	pshufd	$245, %xmm6, %xmm0      # xmm0 = xmm6[1,1,3,3]
	pmuludq	%xmm8, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm8, %xmm13     # xmm13 = xmm8[1,1,3,3]
	pmuludq	%xmm13, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm1, %xmm6
	pshufd	$78, %xmm6, %xmm0       # xmm0 = xmm6[2,3,0,1]
	paddd	%xmm6, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %esi
	movdqa	.LCPI25_8(%rip), %xmm0  # xmm0 = [1,1,2,2]
	pmuludq	%xmm0, %xmm14
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	(%rsp), %xmm0           # 16-byte Folded Reload
	pshufd	$232, %xmm14, %xmm1     # xmm1 = xmm14[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movdqa	.LCPI25_9(%rip), %xmm0  # xmm0 = [3,3,4,4]
	pmuludq	%xmm0, %xmm15
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm9, %xmm0
	pshufd	$232, %xmm15, %xmm6     # xmm6 = xmm15[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm1, %xmm6
	movdqa	.LCPI25_10(%rip), %xmm0 # xmm0 = [5,5,6,6]
	pmuludq	%xmm0, %xmm4
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm10, %xmm0
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	movdqa	.LCPI25_11(%rip), %xmm0 # xmm0 = [7,7,8,8]
	pmuludq	%xmm0, %xmm7
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm11, %xmm0
	pshufd	$232, %xmm7, %xmm1      # xmm1 = xmm7[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	paddd	%xmm4, %xmm1
	paddd	%xmm6, %xmm1
	movdqa	.LCPI25_12(%rip), %xmm0 # xmm0 = [9,9,10,10]
	pmuludq	%xmm0, %xmm2
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm12, %xmm0
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movdqa	.LCPI25_13(%rip), %xmm0 # xmm0 = [11,11,12,12]
	pmuludq	%xmm0, %xmm5
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm5, %xmm3      # xmm3 = xmm5[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	paddd	%xmm2, %xmm3
	movdqa	.LCPI25_14(%rip), %xmm0 # xmm0 = [13,13,14,14]
	pmuludq	%xmm0, %xmm8
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm13, %xmm0
	pshufd	$232, %xmm8, %xmm2      # xmm2 = xmm8[0,2,2,3]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	paddd	%xmm3, %xmm2
	paddd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edi
	movzbl	28(%rax), %ecx
	imull	3396(%rbx), %ecx
	addl	%edx, %ecx
	movzbl	29(%rax), %edx
	imull	3400(%rbx), %edx
	addl	%ecx, %edx
	movzbl	30(%rax), %eax
	imull	3404(%rbx), %eax
	addl	%edx, %eax
	addl	%r8d, %eax
	addl	%esi, %eax
	leal	3(%rdi,%rax), %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end25:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder13TryFixedBlockEi, .Lfunc_end25-_ZN9NCompress8NDeflate8NEncoder6CCoder13TryFixedBlockEi
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder13GetBlockPriceEii
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder13GetBlockPriceEii,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder13GetBlockPriceEii: # @_ZN9NCompress8NDeflate8NEncoder6CCoder13GetBlockPriceEii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi149:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi150:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi152:
	.cfi_def_cfa_offset 112
.Lcfi153:
	.cfi_offset %rbx, -56
.Lcfi154:
	.cfi_offset %r12, -48
.Lcfi155:
	.cfi_offset %r13, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	4920(%rbx), %r12
	movslq	%r15d, %rsi
	imulq	$332, %rsi, %r14        # imm = 0x14C
	movb	$0, 322(%r12,%r14)
	movl	1292(%rbx), %edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder11TryDynBlockEij
	movl	%eax, %ebp
	movl	4912(%rbx), %eax
	movl	%eax, 324(%r12,%r14)
	movl	1372(%rbx), %r9d
	movl	1288(%rbx), %esi
	movl	1380(%rbx), %edi
	cmpl	$256, %r9d              # imm = 0x100
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%edi, 8(%rsp)           # 4-byte Spill
	movl	%r13d, 16(%rsp)         # 4-byte Spill
	ja	.LBB26_3
# BB#1:
	movb	1300(%rbx), %cl
	testb	%cl, %cl
	je	.LBB26_3
# BB#2:
	leaq	322(%r12,%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movl	%r15d, %r13d
	movl	%r13d, %esi
	movl	%r9d, %r15d
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder13TryFixedBlockEi
	movl	%r15d, %r9d
	movl	8(%rsp), %edi           # 4-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %ecx          # 4-byte Reload
	movl	%r13d, %r15d
	movl	%ecx, %r13d
	cmpl	%ebp, %eax
	setb	%cl
	movq	24(%rsp), %rdx          # 8-byte Reload
	movb	%cl, (%rdx)
	cmovbl	%eax, %ebp
	movl	4912(%rbx), %eax
.LBB26_3:
	leaq	324(%r12,%r14), %r8
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB26_4:                               # =>This Inner Loop Header: Depth=1
	cmpl	$65535, %eax            # imm = 0xFFFF
	movl	$65535, %edx            # imm = 0xFFFF
	cmovbl	%eax, %edx
	leal	40(%rcx,%rdx,8), %ecx
	subl	%edx, %eax
	jne	.LBB26_4
# BB#5:                                 # %_ZN9NCompress8NDeflate8NEncoderL13GetStorePriceEji.exit
	cmpl	%ebp, %ecx
	setbe	321(%r12,%r14)
	cmovbel	%ecx, %ebp
	movb	$0, 320(%r12,%r14)
	cmpl	$2, %r13d
	jl	.LBB26_11
# BB#6:                                 # %_ZN9NCompress8NDeflate8NEncoderL13GetStorePriceEji.exit
	cmpl	$128, %r9d
	jb	.LBB26_11
# BB#7:
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movq	4920(%rbx), %r13
	addl	%r15d, %r15d
	movl	%r15d, 44(%rsp)         # 4-byte Spill
	movslq	%r15d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	imulq	$332, %rax, %rbp        # imm = 0x14C
	leaq	(%r13,%rbp), %rdi
	leaq	(%r12,%r14), %rsi
	movl	$320, %edx              # imm = 0x140
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%r8, %r15
	callq	memcpy
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movl	(%r15), %eax
	shrl	%eax
	movl	%eax, 324(%r13,%rbp)
	movl	328(%r12,%r14), %eax
	movl	%eax, 328(%r13,%rbp)
	movl	16(%rsp), %edx          # 4-byte Reload
	decl	%edx
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%edx, 16(%rsp)          # 4-byte Spill
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder13GetBlockPriceEii
	movq	%rbp, %rsi
	movl	%eax, %ebp
	movl	324(%r13,%rsi), %eax
	cmpl	$64, %eax
	jb	.LBB26_8
# BB#9:
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	subl	%eax, %ecx
	cmpl	$64, %ecx
	jb	.LBB26_8
# BB#10:
	leaq	320(%r12,%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	324(%r13,%rsi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	4920(%rbx), %r13
	movl	44(%rsp), %eax          # 4-byte Reload
	orl	$1, %eax
	movslq	%eax, %r15
	imulq	$332, %r15, %r14        # imm = 0x14C
	leaq	(%r13,%r14), %rdi
	movl	$320, %edx              # imm = 0x140
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%ecx, %r12d
	callq	memcpy
	movl	%r12d, 324(%r13,%r14)
	movl	1288(%rbx), %eax
	movl	%eax, 328(%r13,%r14)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	subl	%eax, 1380(%rbx)
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movl	16(%rsp), %edx          # 4-byte Reload
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder13GetBlockPriceEii
	addl	%ebp, %eax
	movl	20(%rsp), %ebp          # 4-byte Reload
	cmpl	%ebp, %eax
	setb	%cl
	movq	24(%rsp), %rdx          # 8-byte Reload
	movb	%cl, (%rdx)
	cmovbl	%eax, %ebp
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	8(%rsp), %edi           # 4-byte Reload
	jmp	.LBB26_11
.LBB26_8:
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	8(%rsp), %edi           # 4-byte Reload
	movl	20(%rsp), %ebp          # 4-byte Reload
.LBB26_11:
	movl	%edi, 1380(%rbx)
	movl	%esi, 1288(%rbx)
	movl	%ebp, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder13GetBlockPriceEii, .Lfunc_end26-_ZN9NCompress8NDeflate8NEncoder6CCoder13GetBlockPriceEii
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI27_0:
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
.LCPI27_1:
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
.LCPI27_2:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI27_3:
	.long	43690                   # 0xaaaa
	.long	43690                   # 0xaaaa
	.long	43690                   # 0xaaaa
	.long	43690                   # 0xaaaa
.LCPI27_4:
	.long	21845                   # 0x5555
	.long	21845                   # 0x5555
	.long	21845                   # 0x5555
	.long	21845                   # 0x5555
.LCPI27_5:
	.long	52428                   # 0xcccc
	.long	52428                   # 0xcccc
	.long	52428                   # 0xcccc
	.long	52428                   # 0xcccc
.LCPI27_6:
	.long	13107                   # 0x3333
	.long	13107                   # 0x3333
	.long	13107                   # 0x3333
	.long	13107                   # 0x3333
.LCPI27_7:
	.long	61680                   # 0xf0f0
	.long	61680                   # 0xf0f0
	.long	61680                   # 0xf0f0
	.long	61680                   # 0xf0f0
.LCPI27_8:
	.long	3855                    # 0xf0f
	.long	3855                    # 0xf0f
	.long	3855                    # 0xf0f
	.long	3855                    # 0xf0f
.LCPI27_9:
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI27_10:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder9CodeBlockEib
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder9CodeBlockEib,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder9CodeBlockEib: # @_ZN9NCompress8NDeflate8NEncoder6CCoder9CodeBlockEib
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi162:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi163:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi165:
	.cfi_def_cfa_offset 64
.Lcfi166:
	.cfi_offset %rbx, -56
.Lcfi167:
	.cfi_offset %r12, -48
.Lcfi168:
	.cfi_offset %r13, -40
.Lcfi169:
	.cfi_offset %r14, -32
.Lcfi170:
	.cfi_offset %r15, -24
.Lcfi171:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebx
	movq	%rdi, %r12
	jmp	.LBB27_2
	.p2align	4, 0x90
.LBB27_1:                               # %tailrecurse
                                        #   in Loop: Header=BB27_2 Depth=1
	leal	(%rbx,%rbx), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder9CodeBlockEib
	leal	1(%rbx,%rbx), %ebx
.LBB27_2:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	4920(%r12), %r15
	movslq	%ebx, %rax
	imulq	$332, %rax, %rcx        # imm = 0x14C
	cmpb	$0, 320(%r15,%rcx)
	jne	.LBB27_1
# BB#3:                                 # %tailrecurse._crit_edge
	imulq	$332, %rax, %rax        # imm = 0x14C
	cmpb	$0, 321(%r15,%rax)
	je	.LBB27_5
# BB#4:
	leaq	324(%r15,%rax), %rbx
	movl	324(%r15,%rax), %esi
	movl	1380(%r12), %edx
	movzbl	%r14b, %ecx
	movq	%r12, %rdi
	leaq	1380(%r12), %r12
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder15WriteStoreBlockEjjb
	jmp	.LBB27_69
.LBB27_5:
	movq	%rax, (%rsp)            # 8-byte Spill
	movzbl	%r14b, %r13d
	leaq	1168(%r12), %r14
	movl	1224(%r12), %edx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB27_6:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB27_7
# BB#8:                                 #   in Loop: Header=BB27_6 Depth=1
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r13d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%r12), %eax
	movq	1168(%r12), %rcx
	movl	1176(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r12)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB27_10
# BB#9:                                 #   in Loop: Header=BB27_6 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB27_10:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i35
                                        #   in Loop: Header=BB27_6 Depth=1
	movzbl	1224(%r12), %ecx
	shrl	%cl, %r13d
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	$8, %edx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB27_6
# BB#11:
	xorl	%esi, %esi
	jmp	.LBB27_12
.LBB27_7:
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%r13d, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%r12), %esi
	movb	%sil, 1228(%r12)
	subl	%eax, %edx
	movl	%edx, 1224(%r12)
.LBB27_12:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit36
	movq	(%rsp), %rax            # 8-byte Reload
	cmpb	$0, 322(%r15,%rax)
	je	.LBB27_30
# BB#13:                                # %.preheader144.preheader
	movl	$1, %ebp
	movl	$2, %eax
	.p2align	4, 0x90
.LBB27_14:                              # %.preheader144
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %r13d
	subl	%edx, %r13d
	jb	.LBB27_15
# BB#16:                                #   in Loop: Header=BB27_14 Depth=1
	movzbl	%sil, %eax
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%eax, %edx
	movq	1168(%r12), %rax
	movl	1176(%r12), %ecx
	leal	1(%rcx), %esi
	movl	%esi, 1176(%r12)
	movb	%dl, (%rax,%rcx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB27_18
# BB#17:                                #   in Loop: Header=BB27_14 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB27_18:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i40
                                        #   in Loop: Header=BB27_14 Depth=1
	movzbl	1224(%r12), %ecx
	shrl	%cl, %ebp
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	$8, %edx
	xorl	%esi, %esi
	testl	%r13d, %r13d
	movl	%r13d, %eax
	jne	.LBB27_14
	jmp	.LBB27_19
.LBB27_30:
	cmpl	$1, 1296(%r12)
	jg	.LBB27_32
# BB#31:
	cmpb	$0, 1300(%r12)
	je	.LBB27_33
.LBB27_32:
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder11TryDynBlockEij
	movl	1224(%r12), %edx
.LBB27_33:                              # %.preheader143.preheader
	movl	$2, %eax
	movl	$2, %ebx
	.p2align	4, 0x90
.LBB27_34:                              # %.preheader143
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB27_35
# BB#36:                                #   in Loop: Header=BB27_34 Depth=1
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebx, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	1228(%r12), %eax
	movq	1168(%r12), %rcx
	movl	1176(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%r12)
	movb	%al, (%rcx,%rdx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB27_38
# BB#37:                                #   in Loop: Header=BB27_34 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB27_38:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i45
                                        #   in Loop: Header=BB27_34 Depth=1
	movzbl	1224(%r12), %ecx
	shrl	%cl, %ebx
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	$8, %edx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB27_34
# BB#39:
	xorl	%esi, %esi
	jmp	.LBB27_40
.LBB27_15:
	movl	$1, %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	decl	%edi
	andl	%ebp, %edi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movzbl	%sil, %ecx
	orl	%edi, %ecx
	movb	%cl, 1228(%r12)
	subl	%eax, %edx
	movl	%edx, 1224(%r12)
.LBB27_19:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit41
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder13TryFixedBlockEi
	leaq	2256(%r12), %rdi
	leaq	3408(%r12), %r14
	leaq	1936(%r12), %rdx
	leaq	2224(%r12), %rbx
	cmpq	%rbx, %rdi
	jae	.LBB27_21
# BB#20:                                # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit41
	cmpq	%r14, %rdx
	jae	.LBB27_21
# BB#23:                                # %scalar.ph.preheader
	xorl	%eax, %eax
	movq	(%rsp), %r13            # 8-byte Reload
	.p2align	4, 0x90
.LBB27_24:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1936(%r12,%rax), %esi
	movl	$9, %ecx
	subl	%esi, %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movl	%esi, 2256(%r12,%rax,4)
	movzbl	1937(%r12,%rax), %esi
	movl	$9, %ecx
	subl	%esi, %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movl	%esi, 2260(%r12,%rax,4)
	movzbl	1938(%r12,%rax), %esi
	movl	$9, %ecx
	subl	%esi, %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movl	%esi, 2264(%r12,%rax,4)
	movzbl	1939(%r12,%rax), %esi
	movl	$9, %ecx
	subl	%esi, %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movl	%esi, 2268(%r12,%rax,4)
	addq	$4, %rax
	cmpq	$288, %rax              # imm = 0x120
	jne	.LBB27_24
	jmp	.LBB27_25
.LBB27_21:                              # %vector.body.preheader
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movdqa	.LCPI27_0(%rip), %xmm1  # xmm1 = [9,9,9,9]
	movdqa	.LCPI27_1(%rip), %xmm2  # xmm2 = [1065353216,1065353216,1065353216,1065353216]
	movdqa	.LCPI27_2(%rip), %xmm3  # xmm3 = [1,1,1,1]
	movq	(%rsp), %r13            # 8-byte Reload
	.p2align	4, 0x90
.LBB27_22:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	1936(%r12,%rax), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movd	1940(%r12,%rax), %xmm5  # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	punpcklbw	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3],xmm5[4],xmm0[4],xmm5[5],xmm0[5],xmm5[6],xmm0[6],xmm5[7],xmm0[7]
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	movdqa	%xmm1, %xmm6
	psubd	%xmm4, %xmm6
	movdqa	%xmm1, %xmm4
	psubd	%xmm5, %xmm4
	pslld	$23, %xmm6
	paddd	%xmm2, %xmm6
	cvttps2dq	%xmm6, %xmm5
	pshufd	$245, %xmm5, %xmm6      # xmm6 = xmm5[1,1,3,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pmuludq	%xmm3, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	pslld	$23, %xmm4
	paddd	%xmm2, %xmm4
	cvttps2dq	%xmm4, %xmm4
	pshufd	$245, %xmm4, %xmm6      # xmm6 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm3, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm4    # xmm4 = xmm4[0],xmm6[0],xmm4[1],xmm6[1]
	movdqu	%xmm5, 2256(%r12,%rax,4)
	movdqu	%xmm4, 2272(%r12,%rax,4)
	movd	1944(%r12,%rax), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movd	1948(%r12,%rax), %xmm5  # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	punpcklbw	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3],xmm5[4],xmm0[4],xmm5[5],xmm0[5],xmm5[6],xmm0[6],xmm5[7],xmm0[7]
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	movdqa	%xmm1, %xmm6
	psubd	%xmm4, %xmm6
	movdqa	%xmm1, %xmm4
	psubd	%xmm5, %xmm4
	pslld	$23, %xmm6
	paddd	%xmm2, %xmm6
	cvttps2dq	%xmm6, %xmm5
	pshufd	$245, %xmm5, %xmm6      # xmm6 = xmm5[1,1,3,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pmuludq	%xmm3, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	pslld	$23, %xmm4
	paddd	%xmm2, %xmm4
	cvttps2dq	%xmm4, %xmm4
	pshufd	$245, %xmm4, %xmm6      # xmm6 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm3, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm4    # xmm4 = xmm4[0],xmm6[0],xmm4[1],xmm6[1]
	movdqu	%xmm5, 2288(%r12,%rax,4)
	movdqu	%xmm4, 2304(%r12,%rax,4)
	addq	$16, %rax
	cmpq	$288, %rax              # imm = 0x120
	jne	.LBB27_22
.LBB27_25:                              # %vector.memcheck248
	leaq	3536(%r12), %rsi
	cmpq	%rdi, %r14
	jae	.LBB27_70
# BB#26:                                # %vector.memcheck248
	cmpq	%rsi, %rbx
	jae	.LBB27_70
# BB#27:                                # %.preheader.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB27_28:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	2224(%r12,%rax), %ebp
	movl	$9, %ecx
	subl	%ebp, %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movl	%ebp, 3408(%r12,%rax,4)
	movzbl	2225(%r12,%rax), %ebp
	movl	$9, %ecx
	subl	%ebp, %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movl	%ebp, 3412(%r12,%rax,4)
	movzbl	2226(%r12,%rax), %ebp
	movl	$9, %ecx
	subl	%ebp, %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movl	%ebp, 3416(%r12,%rax,4)
	movzbl	2227(%r12,%rax), %ebp
	movl	$9, %ecx
	subl	%ebp, %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movl	%ebp, 3420(%r12,%rax,4)
	addq	$4, %rax
	cmpq	$32, %rax
	jne	.LBB27_28
	jmp	.LBB27_29
.LBB27_70:                              # %vector.body232
	movd	2224(%r12), %xmm2       # xmm2 = mem[0],zero,zero,zero
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1],xmm2[2],xmm1[2],xmm2[3],xmm1[3],xmm2[4],xmm1[4],xmm2[5],xmm1[5],xmm2[6],xmm1[6],xmm2[7],xmm1[7]
	punpcklwd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1],xmm2[2],xmm1[2],xmm2[3],xmm1[3]
	movdqa	.LCPI27_0(%rip), %xmm0  # xmm0 = [9,9,9,9]
	movdqa	%xmm0, %xmm4
	psubd	%xmm2, %xmm4
	pslld	$23, %xmm4
	movdqa	.LCPI27_1(%rip), %xmm3  # xmm3 = [1065353216,1065353216,1065353216,1065353216]
	paddd	%xmm3, %xmm4
	cvttps2dq	%xmm4, %xmm4
	movdqa	.LCPI27_2(%rip), %xmm2  # xmm2 = [1,1,1,1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movdqu	%xmm4, 3408(%r12)
	movd	2228(%r12), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	movdqa	%xmm0, %xmm5
	psubd	%xmm4, %xmm5
	pslld	$23, %xmm5
	paddd	%xmm3, %xmm5
	cvttps2dq	%xmm5, %xmm4
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movdqu	%xmm4, 3424(%r12)
	movd	2232(%r12), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	movdqa	%xmm0, %xmm5
	psubd	%xmm4, %xmm5
	pslld	$23, %xmm5
	paddd	%xmm3, %xmm5
	cvttps2dq	%xmm5, %xmm4
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movdqu	%xmm4, 3440(%r12)
	movd	2236(%r12), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	movdqa	%xmm0, %xmm5
	psubd	%xmm4, %xmm5
	pslld	$23, %xmm5
	paddd	%xmm3, %xmm5
	cvttps2dq	%xmm5, %xmm4
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movdqu	%xmm4, 3456(%r12)
	movd	2240(%r12), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	movdqa	%xmm0, %xmm5
	psubd	%xmm4, %xmm5
	pslld	$23, %xmm5
	paddd	%xmm3, %xmm5
	cvttps2dq	%xmm5, %xmm4
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movdqu	%xmm4, 3472(%r12)
	movd	2244(%r12), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	movdqa	%xmm0, %xmm5
	psubd	%xmm4, %xmm5
	pslld	$23, %xmm5
	paddd	%xmm3, %xmm5
	cvttps2dq	%xmm5, %xmm4
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movdqu	%xmm4, 3488(%r12)
	movd	2248(%r12), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	movdqa	%xmm0, %xmm5
	psubd	%xmm4, %xmm5
	pslld	$23, %xmm5
	paddd	%xmm3, %xmm5
	cvttps2dq	%xmm5, %xmm4
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movdqu	%xmm4, 3504(%r12)
	movd	2252(%r12), %xmm4       # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	psubd	%xmm4, %xmm0
	pslld	$23, %xmm0
	paddd	%xmm3, %xmm0
	cvttps2dq	%xmm0, %xmm0
	pshufd	$245, %xmm0, %xmm1      # xmm1 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movdqu	%xmm0, 3520(%r12)
.LBB27_29:                              # %middle.block233
	movl	$288, %ecx              # imm = 0x120
	movl	$9, %r8d
	callq	Huffman_Generate
	leaq	4688(%r12), %rsi
	movl	$32, %ecx
	movl	$9, %r8d
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	Huffman_Generate
	jmp	.LBB27_68
.LBB27_35:
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%ebx, %esi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	1228(%r12), %esi
	movb	%sil, 1228(%r12)
	subl	%eax, %edx
	movl	%edx, 1224(%r12)
.LBB27_40:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit46
	movl	$-257, %ebx             # imm = 0xFEFF
	addl	1360(%r12), %ebx
	movl	$5, %eax
	.p2align	4, 0x90
.LBB27_41:                              # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB27_42
# BB#43:                                #   in Loop: Header=BB27_41 Depth=1
	movzbl	%sil, %eax
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebx, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%eax, %edx
	movq	1168(%r12), %rax
	movl	1176(%r12), %ecx
	leal	1(%rcx), %esi
	movl	%esi, 1176(%r12)
	movb	%dl, (%rax,%rcx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB27_45
# BB#44:                                #   in Loop: Header=BB27_41 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB27_45:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i50
                                        #   in Loop: Header=BB27_41 Depth=1
	movzbl	1224(%r12), %ecx
	shrl	%cl, %ebx
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	$8, %edx
	xorl	%esi, %esi
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB27_41
	jmp	.LBB27_46
.LBB27_42:
	movl	$1, %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	decl	%edi
	andl	%ebx, %edi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movzbl	%sil, %esi
	orl	%edi, %esi
	movb	%sil, 1228(%r12)
	subl	%eax, %edx
	movl	%edx, 1224(%r12)
.LBB27_46:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit51
	movl	1364(%r12), %ebx
	decl	%ebx
	movl	$5, %eax
	.p2align	4, 0x90
.LBB27_47:                              # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB27_48
# BB#49:                                #   in Loop: Header=BB27_47 Depth=1
	movzbl	%sil, %eax
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebx, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%eax, %edx
	movq	1168(%r12), %rax
	movl	1176(%r12), %ecx
	leal	1(%rcx), %esi
	movl	%esi, 1176(%r12)
	movb	%dl, (%rax,%rcx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB27_51
# BB#50:                                #   in Loop: Header=BB27_47 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB27_51:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i55
                                        #   in Loop: Header=BB27_47 Depth=1
	movzbl	1224(%r12), %ecx
	shrl	%cl, %ebx
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	$8, %edx
	xorl	%esi, %esi
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB27_47
	jmp	.LBB27_52
.LBB27_48:
	movl	$1, %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	decl	%edi
	andl	%ebx, %edi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movzbl	%sil, %esi
	orl	%edi, %esi
	movb	%sil, 1228(%r12)
	subl	%eax, %edx
	movl	%edx, 1224(%r12)
.LBB27_52:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit56
	movl	1368(%r12), %ebx
	addl	$-4, %ebx
	movl	$4, %eax
	.p2align	4, 0x90
.LBB27_53:                              # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	subl	%edx, %ebp
	jb	.LBB27_54
# BB#55:                                #   in Loop: Header=BB27_53 Depth=1
	movzbl	%sil, %eax
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebx, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%eax, %edx
	movq	1168(%r12), %rax
	movl	1176(%r12), %ecx
	leal	1(%rcx), %esi
	movl	%esi, 1176(%r12)
	movb	%dl, (%rax,%rcx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB27_57
# BB#56:                                #   in Loop: Header=BB27_53 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB27_57:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i60
                                        #   in Loop: Header=BB27_53 Depth=1
	movzbl	1224(%r12), %ecx
	shrl	%cl, %ebx
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	$8, %edx
	xorl	%esi, %esi
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB27_53
	jmp	.LBB27_58
.LBB27_54:
	movl	$1, %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	decl	%edi
	andl	%ebx, %edi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movzbl	%sil, %esi
	orl	%edi, %esi
	movb	%sil, 1228(%r12)
	subl	%eax, %edx
	movl	%edx, 1224(%r12)
.LBB27_58:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit61.preheader
	cmpl	$0, 1368(%r12)
	je	.LBB27_67
# BB#59:                                # %.lr.ph.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB27_60:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_61 Depth 2
	movl	%r13d, %eax
	movzbl	1338(%r12,%rax), %ebp
	movl	$3, %eax
	.p2align	4, 0x90
.LBB27_61:                              #   Parent Loop BB27_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebx
	subl	%edx, %ebx
	jb	.LBB27_62
# BB#63:                                #   in Loop: Header=BB27_61 Depth=2
	movzbl	%sil, %eax
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%eax, %edx
	movq	1168(%r12), %rax
	movl	1176(%r12), %ecx
	leal	1(%rcx), %esi
	movl	%esi, 1176(%r12)
	movb	%dl, (%rax,%rcx)
	movl	1176(%r12), %eax
	cmpl	1180(%r12), %eax
	jne	.LBB27_65
# BB#64:                                #   in Loop: Header=BB27_61 Depth=2
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB27_65:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB27_61 Depth=2
	movzbl	1224(%r12), %ecx
	shrl	%cl, %ebp
	movl	$8, 1224(%r12)
	movb	$0, 1228(%r12)
	movl	$8, %edx
	xorl	%esi, %esi
	testl	%ebx, %ebx
	movl	%ebx, %eax
	jne	.LBB27_61
	jmp	.LBB27_66
	.p2align	4, 0x90
.LBB27_62:                              #   in Loop: Header=BB27_60 Depth=1
	movl	$1, %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	decl	%edi
	andl	%ebp, %edi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movzbl	%sil, %esi
	orl	%edi, %esi
	movb	%sil, 1228(%r12)
	subl	%eax, %edx
	movl	%edx, 1224(%r12)
.LBB27_66:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder9WriteBitsEji.exit
                                        #   in Loop: Header=BB27_60 Depth=1
	incl	%r13d
	cmpl	1368(%r12), %r13d
	jb	.LBB27_60
.LBB27_67:                              # %.lr.ph.i
	leaq	4816(%r12), %r14
	movdqu	4816(%r12), %xmm0
	movdqa	%xmm0, %xmm1
	paddd	%xmm1, %xmm1
	movdqa	.LCPI27_3(%rip), %xmm8  # xmm8 = [43690,43690,43690,43690]
	pand	%xmm8, %xmm1
	psrld	$1, %xmm0
	movdqa	.LCPI27_4(%rip), %xmm9  # xmm9 = [21845,21845,21845,21845]
	pand	%xmm9, %xmm0
	por	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	pslld	$2, %xmm1
	movdqa	.LCPI27_5(%rip), %xmm10 # xmm10 = [52428,52428,52428,52428]
	pand	%xmm10, %xmm1
	psrld	$2, %xmm0
	movdqa	.LCPI27_6(%rip), %xmm11 # xmm11 = [13107,13107,13107,13107]
	pand	%xmm11, %xmm0
	por	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	pslld	$4, %xmm1
	movdqa	.LCPI27_7(%rip), %xmm12 # xmm12 = [61680,61680,61680,61680]
	pand	%xmm12, %xmm1
	psrld	$4, %xmm0
	movdqa	.LCPI27_8(%rip), %xmm13 # xmm13 = [3855,3855,3855,3855]
	pand	%xmm13, %xmm0
	por	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	pslld	$8, %xmm1
	movdqa	.LCPI27_9(%rip), %xmm2  # xmm2 = [0,255,0,0,0,255,0,0,0,255,0,0,0,255,0,0]
	pand	%xmm2, %xmm1
	psrld	$8, %xmm0
	por	%xmm1, %xmm0
	leaq	4892(%r12), %rbx
	movd	4892(%r12), %xmm4       # xmm4 = mem[0],zero,zero,zero
	pxor	%xmm7, %xmm7
	punpcklbw	%xmm7, %xmm4    # xmm4 = xmm4[0],xmm7[0],xmm4[1],xmm7[1],xmm4[2],xmm7[2],xmm4[3],xmm7[3],xmm4[4],xmm7[4],xmm4[5],xmm7[5],xmm4[6],xmm7[6],xmm4[7],xmm7[7]
	punpcklwd	%xmm7, %xmm4    # xmm4 = xmm4[0],xmm7[0],xmm4[1],xmm7[1],xmm4[2],xmm7[2],xmm4[3],xmm7[3]
	movdqa	.LCPI27_10(%rip), %xmm5 # xmm5 = [16,16,16,16]
	movdqa	%xmm5, %xmm1
	psubd	%xmm4, %xmm1
	movdqa	%xmm1, %xmm4
	psrldq	$12, %xmm4              # xmm4 = xmm4[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm0, %xmm6
	psrld	%xmm4, %xmm6
	movdqa	%xmm1, %xmm4
	psrlq	$32, %xmm4
	movdqa	%xmm0, %xmm3
	psrld	%xmm4, %xmm3
	movsd	%xmm3, %xmm6            # xmm6 = xmm3[0],xmm6[1]
	pshufd	$237, %xmm6, %xmm3      # xmm3 = xmm6[1,3,2,3]
	movdqa	%xmm1, %xmm4
	punpckhdq	%xmm7, %xmm4    # xmm4 = xmm4[2],xmm7[2],xmm4[3],xmm7[3]
	movdqa	%xmm0, %xmm6
	psrld	%xmm4, %xmm6
	punpckldq	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1]
	psrld	%xmm1, %xmm0
	movsd	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1]
	pshufd	$232, %xmm6, %xmm0      # xmm0 = xmm6[0,2,2,3]
	punpckldq	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	movdqu	%xmm0, 4816(%r12)
	movdqu	4832(%r12), %xmm3
	movdqa	%xmm3, %xmm0
	paddd	%xmm0, %xmm0
	pand	%xmm8, %xmm0
	psrld	$1, %xmm3
	pand	%xmm9, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm3, %xmm0
	pslld	$2, %xmm0
	pand	%xmm10, %xmm0
	psrld	$2, %xmm3
	pand	%xmm11, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm3, %xmm0
	pslld	$4, %xmm0
	pand	%xmm12, %xmm0
	psrld	$4, %xmm3
	pand	%xmm13, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm3, %xmm0
	pslld	$8, %xmm0
	pand	%xmm2, %xmm0
	psrld	$8, %xmm3
	por	%xmm0, %xmm3
	movd	4896(%r12), %xmm0       # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0],xmm0[1],xmm7[1],xmm0[2],xmm7[2],xmm0[3],xmm7[3],xmm0[4],xmm7[4],xmm0[5],xmm7[5],xmm0[6],xmm7[6],xmm0[7],xmm7[7]
	punpcklwd	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0],xmm0[1],xmm7[1],xmm0[2],xmm7[2],xmm0[3],xmm7[3]
	movdqa	%xmm5, %xmm1
	psubd	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	psrldq	$12, %xmm0              # xmm0 = xmm0[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm3, %xmm4
	psrld	%xmm0, %xmm4
	movdqa	%xmm1, %xmm0
	psrlq	$32, %xmm0
	movdqa	%xmm3, %xmm6
	psrld	%xmm0, %xmm6
	movsd	%xmm6, %xmm4            # xmm4 = xmm6[0],xmm4[1]
	pshufd	$237, %xmm4, %xmm0      # xmm0 = xmm4[1,3,2,3]
	movdqa	%xmm1, %xmm4
	punpckhdq	%xmm7, %xmm4    # xmm4 = xmm4[2],xmm7[2],xmm4[3],xmm7[3]
	movdqa	%xmm3, %xmm6
	psrld	%xmm4, %xmm6
	punpckldq	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1]
	psrld	%xmm1, %xmm3
	movsd	%xmm3, %xmm6            # xmm6 = xmm3[0],xmm6[1]
	pshufd	$232, %xmm6, %xmm1      # xmm1 = xmm6[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movdqu	%xmm1, 4832(%r12)
	movdqu	4848(%r12), %xmm3
	movdqa	%xmm3, %xmm0
	paddd	%xmm0, %xmm0
	pand	%xmm8, %xmm0
	psrld	$1, %xmm3
	pand	%xmm9, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm3, %xmm0
	pslld	$2, %xmm0
	pand	%xmm10, %xmm0
	psrld	$2, %xmm3
	pand	%xmm11, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm3, %xmm0
	pslld	$4, %xmm0
	pand	%xmm12, %xmm0
	psrld	$4, %xmm3
	pand	%xmm13, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm3, %xmm0
	pslld	$8, %xmm0
	pand	%xmm2, %xmm0
	psrld	$8, %xmm3
	por	%xmm0, %xmm3
	movd	4900(%r12), %xmm0       # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0],xmm0[1],xmm7[1],xmm0[2],xmm7[2],xmm0[3],xmm7[3],xmm0[4],xmm7[4],xmm0[5],xmm7[5],xmm0[6],xmm7[6],xmm0[7],xmm7[7]
	punpcklwd	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0],xmm0[1],xmm7[1],xmm0[2],xmm7[2],xmm0[3],xmm7[3]
	movdqa	%xmm5, %xmm1
	psubd	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	psrldq	$12, %xmm0              # xmm0 = xmm0[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm3, %xmm4
	psrld	%xmm0, %xmm4
	movdqa	%xmm1, %xmm0
	psrlq	$32, %xmm0
	movdqa	%xmm3, %xmm6
	psrld	%xmm0, %xmm6
	movsd	%xmm6, %xmm4            # xmm4 = xmm6[0],xmm4[1]
	pshufd	$237, %xmm4, %xmm0      # xmm0 = xmm4[1,3,2,3]
	movdqa	%xmm1, %xmm4
	punpckhdq	%xmm7, %xmm4    # xmm4 = xmm4[2],xmm7[2],xmm4[3],xmm7[3]
	movdqa	%xmm3, %xmm6
	psrld	%xmm4, %xmm6
	punpckldq	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1]
	psrld	%xmm1, %xmm3
	movsd	%xmm3, %xmm6            # xmm6 = xmm3[0],xmm6[1]
	pshufd	$232, %xmm6, %xmm1      # xmm1 = xmm6[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movdqu	%xmm1, 4848(%r12)
	movdqu	4864(%r12), %xmm3
	movdqa	%xmm3, %xmm0
	paddd	%xmm0, %xmm0
	pand	%xmm8, %xmm0
	psrld	$1, %xmm3
	pand	%xmm9, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm3, %xmm0
	pslld	$2, %xmm0
	pand	%xmm10, %xmm0
	psrld	$2, %xmm3
	pand	%xmm11, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm3, %xmm0
	pslld	$4, %xmm0
	pand	%xmm12, %xmm0
	psrld	$4, %xmm3
	pand	%xmm13, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm3, %xmm0
	pslld	$8, %xmm0
	pand	%xmm2, %xmm0
	psrld	$8, %xmm3
	por	%xmm0, %xmm3
	movd	4904(%r12), %xmm0       # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0],xmm0[1],xmm7[1],xmm0[2],xmm7[2],xmm0[3],xmm7[3],xmm0[4],xmm7[4],xmm0[5],xmm7[5],xmm0[6],xmm7[6],xmm0[7],xmm7[7]
	punpcklwd	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0],xmm0[1],xmm7[1],xmm0[2],xmm7[2],xmm0[3],xmm7[3]
	psubd	%xmm0, %xmm5
	movdqa	%xmm5, %xmm0
	psrldq	$12, %xmm0              # xmm0 = xmm0[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm3, %xmm1
	psrld	%xmm0, %xmm1
	movdqa	%xmm5, %xmm0
	psrlq	$32, %xmm0
	movdqa	%xmm3, %xmm2
	psrld	%xmm0, %xmm2
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	pshufd	$237, %xmm1, %xmm0      # xmm0 = xmm1[1,3,2,3]
	movdqa	%xmm5, %xmm1
	punpckhdq	%xmm7, %xmm1    # xmm1 = xmm1[2],xmm7[2],xmm1[3],xmm7[3]
	movdqa	%xmm3, %xmm2
	psrld	%xmm1, %xmm2
	punpckldq	%xmm7, %xmm5    # xmm5 = xmm5[0],xmm7[0],xmm5[1],xmm7[1]
	psrld	%xmm5, %xmm3
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	pshufd	$232, %xmm2, %xmm1      # xmm1 = xmm2[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movdqu	%xmm1, 4864(%r12)
	movl	4880(%r12), %eax
	movl	%eax, %ecx
	andl	$21845, %ecx            # imm = 0x5555
	shrl	%eax
	andl	$21845, %eax            # imm = 0x5555
	leal	(%rax,%rcx,2), %eax
	movl	%eax, %ecx
	andl	$13107, %ecx            # imm = 0x3333
	shrl	$2, %eax
	andl	$13107, %eax            # imm = 0x3333
	leal	(%rax,%rcx,4), %edx
	movl	%edx, %eax
	shll	$4, %eax
	andl	$61680, %eax            # imm = 0xF0F0
	shrl	$4, %edx
	andl	$3855, %edx             # imm = 0xF0F
	orl	%eax, %edx
	bswapl	%edx
	shrl	$16, %edx
	movzbl	4908(%r12), %esi
	movl	$16, %eax
	movl	$16, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movl	%edx, 4880(%r12)
	movl	4884(%r12), %ecx
	movl	%ecx, %edx
	andl	$21845, %edx            # imm = 0x5555
	shrl	%ecx
	andl	$21845, %ecx            # imm = 0x5555
	leal	(%rcx,%rdx,2), %ecx
	movl	%ecx, %edx
	andl	$13107, %edx            # imm = 0x3333
	shrl	$2, %ecx
	andl	$13107, %ecx            # imm = 0x3333
	leal	(%rcx,%rdx,4), %edx
	movl	%edx, %ecx
	shll	$4, %ecx
	andl	$61680, %ecx            # imm = 0xF0F0
	shrl	$4, %edx
	andl	$3855, %edx             # imm = 0xF0F
	orl	%ecx, %edx
	bswapl	%edx
	shrl	$16, %edx
	movzbl	4909(%r12), %esi
	movl	$16, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movl	%edx, 4884(%r12)
	movl	4888(%r12), %ecx
	movl	%ecx, %edx
	andl	$21845, %edx            # imm = 0x5555
	shrl	%ecx
	andl	$21845, %ecx            # imm = 0x5555
	leal	(%rcx,%rdx,2), %ecx
	movl	%ecx, %edx
	andl	$13107, %edx            # imm = 0x3333
	shrl	$2, %ecx
	andl	$13107, %ecx            # imm = 0x3333
	leal	(%rcx,%rdx,4), %edx
	movl	%edx, %ecx
	shll	$4, %ecx
	andl	$61680, %ecx            # imm = 0xF0F0
	shrl	$4, %edx
	andl	$3855, %edx             # imm = 0xF0F
	orl	%ecx, %edx
	bswapl	%edx
	shrl	$16, %edx
	movzbl	4910(%r12), %ecx
	subl	%ecx, %eax
	movl	%eax, %ecx
	shrl	%cl, %edx
	movl	%edx, 4888(%r12)
	leaq	1936(%r12), %rsi
	movl	1360(%r12), %edx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movq	%r14, %r8
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder14LevelTableCodeEPKhiS4_PKj
	leaq	2224(%r12), %rsi
	movl	1364(%r12), %edx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movq	%r14, %r8
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder14LevelTableCodeEPKhiS4_PKj
	movq	(%rsp), %r13            # 8-byte Reload
.LBB27_68:
	movq	%r12, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder10WriteBlockEv
	leaq	324(%r15,%r13), %rbx
	addq	$1380, %r12             # imm = 0x564
.LBB27_69:
	movl	(%rbx), %eax
	subl	%eax, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder9CodeBlockEib, .Lfunc_end27-_ZN9NCompress8NDeflate8NEncoder6CCoder9CodeBlockEib
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NEncoder4ReadEPvS2_Pm
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder4ReadEPvS2_Pm,@function
_ZN9NCompress8NDeflate8NEncoder4ReadEPvS2_Pm: # @_ZN9NCompress8NDeflate8NEncoder4ReadEPvS2_Pm
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi173:
	.cfi_def_cfa_offset 32
.Lcfi174:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	movq	(%rbx), %rax
	testq	$-2147483648, %rax      # imm = 0x80000000
	movl	$-2147483648, %edx      # imm = 0x80000000
	cmovel	%eax, %edx
	movl	%edx, 12(%rsp)
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	leaq	12(%rsp), %rcx
	callq	*40(%rax)
	movl	12(%rsp), %ecx
	movq	%rcx, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end28:
	.size	_ZN9NCompress8NDeflate8NEncoder4ReadEPvS2_Pm, .Lfunc_end28-_ZN9NCompress8NDeflate8NEncoder4ReadEPvS2_Pm
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI29_0:
	.zero	16,8
.LCPI29_1:
	.zero	16,5
	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo: # @_ZN9NCompress8NDeflate8NEncoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi175:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi176:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi177:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi178:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi179:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi180:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi181:
	.cfi_def_cfa_offset 80
.Lcfi182:
	.cfi_offset %rbx, -56
.Lcfi183:
	.cfi_offset %r12, -48
.Lcfi184:
	.cfi_offset %r13, -40
.Lcfi185:
	.cfi_offset %r14, -32
.Lcfi186:
	.cfi_offset %r15, -24
.Lcfi187:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpl	$1, 1292(%rbx)
	jne	.LBB29_1
# BB#2:
	cmpl	$1, 1296(%rbx)
	setne	%al
	setne	1300(%rbx)
	jmp	.LBB29_3
.LBB29_1:                               # %.thread
	movb	$1, 1300(%rbx)
	movb	$1, %al
.LBB29_3:
	movb	%al, 1301(%rbx)
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder6CreateEv
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB29_25
# BB#4:
	movl	1296(%rbx), %eax
	shll	$12, %eax
	addl	$7168, %eax             # imm = 0x1C00
	movl	%eax, 1304(%rbx)
	movq	$0, 8(%rsp)
	testq	%r14, %r14
	je	.LBB29_6
# BB#5:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB29_6:
	movq	1240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_8
# BB#7:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB29_8:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%r14, 1240(%rbx)
	leaq	1232(%rbx), %rax
	movq	$_ZN9NCompress8NDeflate8NEncoder4ReadEPvS2_Pm, 1232(%rbx)
	movq	%rax, 72(%rbx)
	movq	%rbx, %rdi
	callq	MatchFinder_Init
	leaq	1168(%rbx), %r14
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
	movq	%r14, %rdi
	callq	_ZN10COutBuffer4InitEv
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
	movq	$0, 1384(%rbx)
	movq	4920(%rbx), %r12
	movl	$0, 660(%r12)
	movaps	.LCPI29_0(%rip), %xmm0  # xmm0 = [8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]
	movups	%xmm0, 572(%r12)
	movups	%xmm0, 556(%r12)
	movups	%xmm0, 540(%r12)
	movups	%xmm0, 524(%r12)
	movups	%xmm0, 508(%r12)
	movups	%xmm0, 492(%r12)
	movups	%xmm0, 476(%r12)
	movups	%xmm0, 460(%r12)
	movups	%xmm0, 444(%r12)
	movups	%xmm0, 428(%r12)
	movups	%xmm0, 412(%r12)
	movups	%xmm0, 396(%r12)
	movups	%xmm0, 380(%r12)
	movups	%xmm0, 364(%r12)
	movups	%xmm0, 348(%r12)
	movups	%xmm0, 332(%r12)
	movb	$13, 588(%r12)
	movaps	.LCPI29_1(%rip), %xmm0  # xmm0 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	movups	%xmm0, 636(%r12)
	movups	%xmm0, 621(%r12)
	movups	%xmm0, 605(%r12)
	movups	%xmm0, 589(%r12)
	movl	$0, 1380(%rbx)
	testq	%r15, %r15
	je	.LBB29_26
# BB#9:
	leaq	16(%rsp), %r13
	.p2align	4, 0x90
.LBB29_10:                              # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$60923, 656(%r12)       # imm = 0xEDFB
	movb	$0, 1376(%rbx)
	movl	1296(%rbx), %edx
.Ltmp61:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder13GetBlockPriceEii
.Ltmp62:
# BB#11:                                #   in Loop: Header=BB29_10 Depth=1
	movl	16(%rbx), %eax
	xorl	%edx, %edx
	cmpl	8(%rbx), %eax
	sete	%dl
.Ltmp63:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder9CodeBlockEib
.Ltmp64:
# BB#12:                                #   in Loop: Header=BB29_10 Depth=1
	movq	4920(%rbx), %rax
	movl	656(%rax), %eax
	addq	%rax, 8(%rsp)
.Ltmp65:
	movq	%r14, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp66:
# BB#13:                                #   in Loop: Header=BB29_10 Depth=1
	movl	$15, %ecx
	subl	1224(%rbx), %ecx
	shrl	$3, %ecx
	addq	%rax, %rcx
	movq	%rcx, 16(%rsp)
	movq	(%r15), %rax
.Ltmp68:
	movq	%r15, %rdi
	leaq	8(%rsp), %rsi
	movq	%r13, %rdx
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp69:
# BB#14:                                #   in Loop: Header=BB29_10 Depth=1
	testl	%ebp, %ebp
	jne	.LBB29_21
# BB#15:                                #   in Loop: Header=BB29_10 Depth=1
	movl	16(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB29_10
	jmp	.LBB29_16
	.p2align	4, 0x90
.LBB29_26:                              # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$60923, 656(%r12)       # imm = 0xEDFB
	movb	$0, 1376(%rbx)
	movl	1296(%rbx), %edx
.Ltmp71:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder13GetBlockPriceEii
.Ltmp72:
# BB#27:                                #   in Loop: Header=BB29_26 Depth=1
	movl	16(%rbx), %eax
	xorl	%edx, %edx
	cmpl	8(%rbx), %eax
	sete	%dl
.Ltmp73:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder9CodeBlockEib
.Ltmp74:
# BB#28:                                #   in Loop: Header=BB29_26 Depth=1
	movq	4920(%rbx), %rax
	movl	656(%rax), %eax
	addq	%rax, 8(%rsp)
	movl	16(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB29_26
.LBB29_16:                              # %.us-lcssa41.us
	movl	136(%rbx), %ebp
	testl	%ebp, %ebp
	jne	.LBB29_21
# BB#17:
	cmpl	$7, 1224(%rbx)
	ja	.LBB29_20
# BB#18:
	movb	1228(%rbx), %al
	movq	1168(%rbx), %rcx
	movl	1176(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 1176(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	1176(%rbx), %eax
	cmpl	1180(%rbx), %eax
	jne	.LBB29_20
# BB#19:
.Ltmp76:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp77:
.LBB29_20:                              # %_ZN12CBitlEncoder9FlushByteEv.exit.i
	movl	$8, 1224(%rbx)
	movb	$0, 1228(%rbx)
.Ltmp78:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer5FlushEv
	movl	%eax, %ebp
.Ltmp79:
.LBB29_21:                              # %_ZN12CBitlEncoder5FlushEv.exit
	movq	1240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_23
# BB#22:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 1240(%rbx)
.LBB29_23:                              # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit.i.i
	movq	1192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_25
# BB#24:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 1192(%rbx)
.LBB29_25:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_31:                              # %.loopexit.split-lp
.Ltmp80:
	jmp	.LBB29_33
.LBB29_32:                              # %.us-lcssa
.Ltmp70:
	jmp	.LBB29_33
.LBB29_29:                              # %.loopexit.us-lcssa.us
.Ltmp75:
	jmp	.LBB29_33
.LBB29_30:                              # %.loopexit.us-lcssa
.Ltmp67:
.LBB29_33:
	movq	%rax, %rbp
	movq	1240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_36
# BB#34:
	movq	(%rdi), %rax
.Ltmp81:
	callq	*16(%rax)
.Ltmp82:
# BB#35:                                # %.noexc32
	movq	$0, 1240(%rbx)
.LBB29_36:                              # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit.i.i31
	movq	1192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_39
# BB#37:
	movq	(%rdi), %rax
.Ltmp83:
	callq	*16(%rax)
.Ltmp84:
# BB#38:                                # %.noexc33
	movq	$0, 1192(%rbx)
.LBB29_39:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder14CCoderReleaserD2Ev.exit34
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB29_40:
.Ltmp85:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end29:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo, .Lfunc_end29-_ZN9NCompress8NDeflate8NEncoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp61-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp66-.Ltmp61         #   Call between .Ltmp61 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin3   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin3   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp74-.Ltmp71         #   Call between .Ltmp71 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin3   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp79-.Ltmp76         #   Call between .Ltmp76 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin3   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp81-.Ltmp79         #   Call between .Ltmp79 and .Ltmp81
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp84-.Ltmp81         #   Call between .Ltmp81 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin3   #     jumps to .Ltmp85
	.byte	1                       #   On action: 1
	.long	.Ltmp84-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Lfunc_end29-.Ltmp84    #   Call between .Ltmp84 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoder8BaseCodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoder8BaseCodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo,@function
_ZN9NCompress8NDeflate8NEncoder6CCoder8BaseCodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo: # @_ZN9NCompress8NDeflate8NEncoder6CCoder8BaseCodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbx
.Lcfi188:
	.cfi_def_cfa_offset 16
.Lcfi189:
	.cfi_offset %rbx, -16
.Ltmp86:
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	movl	%eax, %ebx
.Ltmp87:
.LBB30_4:
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB30_1:
.Ltmp88:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB30_3
# BB#2:
	movl	(%rax), %ebx
	callq	__cxa_end_catch
	jmp	.LBB30_4
.LBB30_3:
	callq	__cxa_end_catch
	movl	$-2147467259, %ebx      # imm = 0x80004005
	jmp	.LBB30_4
.Lfunc_end30:
	.size	_ZN9NCompress8NDeflate8NEncoder6CCoder8BaseCodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo, .Lfunc_end30-_ZN9NCompress8NDeflate8NEncoder6CCoder8BaseCodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\250"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp86-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin4   #     jumps to .Ltmp88
	.byte	3                       #   On action: 2
	.long	.Ltmp87-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end30-.Ltmp87    #   Call between .Ltmp87 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo,@function
_ZN9NCompress8NDeflate8NEncoder9CCOMCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo: # @_ZN9NCompress8NDeflate8NEncoder9CCOMCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbx
.Lcfi190:
	.cfi_def_cfa_offset 16
.Lcfi191:
	.cfi_offset %rbx, -16
	addq	$24, %rdi
.Ltmp89:
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	movl	%eax, %ebx
.Ltmp90:
.LBB31_4:                               # %_ZN9NCompress8NDeflate8NEncoder6CCoder8BaseCodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo.exit
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB31_1:
.Ltmp91:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB31_3
# BB#2:
	movl	(%rax), %ebx
	callq	__cxa_end_catch
	jmp	.LBB31_4
.LBB31_3:
	callq	__cxa_end_catch
	movl	$-2147467259, %ebx      # imm = 0x80004005
	jmp	.LBB31_4
.Lfunc_end31:
	.size	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo, .Lfunc_end31-_ZN9NCompress8NDeflate8NEncoder9CCOMCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\250"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp89-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin5   #     jumps to .Ltmp91
	.byte	3                       #   On action: 2
	.long	.Ltmp90-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end31-.Ltmp90    #   Call between .Ltmp90 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZN9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZN9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB32_18
# BB#1:                                 # %.lr.ph.i
	movl	%ecx, %r8d
	addq	$8, %rdx
	xorl	%r10d, %r10d
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB32_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%r10,4), %ecx
	movl	$-2147024809, %eax      # imm = 0x80070057
	addl	$-8, %ecx
	cmpl	$4, %ecx
	ja	.LBB32_19
# BB#3:                                 #   in Loop: Header=BB32_2 Depth=1
	jmpq	*.LJTI32_0(,%rcx,8)
.LBB32_10:                              #   in Loop: Header=BB32_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB32_19
# BB#11:                                #   in Loop: Header=BB32_2 Depth=1
	movl	(%rdx), %ecx
	movl	%ecx, 1288(%rdi)
	cmpl	$3, %ecx
	jb	.LBB32_19
# BB#12:                                #   in Loop: Header=BB32_2 Depth=1
	cmpl	1336(%rdi), %ecx
	jbe	.LBB32_17
	jmp	.LBB32_19
	.p2align	4, 0x90
.LBB32_13:                              #   in Loop: Header=BB32_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB32_19
# BB#14:                                #   in Loop: Header=BB32_2 Depth=1
	movl	(%rdx), %eax
	movl	%eax, 39784(%rdi)
	jmp	.LBB32_17
	.p2align	4, 0x90
.LBB32_4:                               #   in Loop: Header=BB32_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB32_19
# BB#5:                                 #   in Loop: Header=BB32_2 Depth=1
	movl	(%rdx), %eax
	testl	%eax, %eax
	cmovel	%r9d, %eax
	movl	%eax, 1320(%rdi)
	cmpl	$1, %eax
	jne	.LBB32_7
# BB#6:                                 #   in Loop: Header=BB32_2 Depth=1
	movl	$1, 1316(%rdi)
	jmp	.LBB32_17
	.p2align	4, 0x90
.LBB32_15:                              #   in Loop: Header=BB32_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB32_19
# BB#16:                                #   in Loop: Header=BB32_2 Depth=1
	cmpl	$0, (%rdx)
	sete	1292(%rdi)
	setne	1293(%rdi)
	jmp	.LBB32_17
.LBB32_7:                               #   in Loop: Header=BB32_2 Depth=1
	cmpl	$10, %eax
	jg	.LBB32_9
# BB#8:                                 #   in Loop: Header=BB32_2 Depth=1
	movl	$2, 1316(%rdi)
	jmp	.LBB32_17
.LBB32_9:                               #   in Loop: Header=BB32_2 Depth=1
	addl	$-8, %eax
	movl	%eax, 1316(%rdi)
	movl	$10, 1320(%rdi)
	.p2align	4, 0x90
.LBB32_17:                              #   in Loop: Header=BB32_2 Depth=1
	incq	%r10
	addq	$16, %rdx
	cmpq	%r8, %r10
	jb	.LBB32_2
.LBB32_18:
	xorl	%eax, %eax
.LBB32_19:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder25BaseSetEncoderProperties2EPKjPK14tagPROPVARIANTj.exit
	retq
.Lfunc_end32:
	.size	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end32-_ZN9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI32_0:
	.quad	.LBB32_10
	.quad	.LBB32_19
	.quad	.LBB32_13
	.quad	.LBB32_4
	.quad	.LBB32_15

	.text
	.globl	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB33_18
# BB#1:                                 # %.lr.ph.i.i
	movl	%ecx, %r8d
	addq	$8, %rdx
	xorl	%r10d, %r10d
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB33_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%r10,4), %ecx
	movl	$-2147024809, %eax      # imm = 0x80070057
	addl	$-8, %ecx
	cmpl	$4, %ecx
	ja	.LBB33_19
# BB#3:                                 #   in Loop: Header=BB33_2 Depth=1
	jmpq	*.LJTI33_0(,%rcx,8)
.LBB33_10:                              #   in Loop: Header=BB33_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB33_19
# BB#11:                                #   in Loop: Header=BB33_2 Depth=1
	movl	(%rdx), %ecx
	movl	%ecx, 1280(%rdi)
	cmpl	$3, %ecx
	jb	.LBB33_19
# BB#12:                                #   in Loop: Header=BB33_2 Depth=1
	cmpl	1328(%rdi), %ecx
	jbe	.LBB33_17
	jmp	.LBB33_19
	.p2align	4, 0x90
.LBB33_13:                              #   in Loop: Header=BB33_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB33_19
# BB#14:                                #   in Loop: Header=BB33_2 Depth=1
	movl	(%rdx), %eax
	movl	%eax, 39776(%rdi)
	jmp	.LBB33_17
	.p2align	4, 0x90
.LBB33_4:                               #   in Loop: Header=BB33_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB33_19
# BB#5:                                 #   in Loop: Header=BB33_2 Depth=1
	movl	(%rdx), %eax
	testl	%eax, %eax
	cmovel	%r9d, %eax
	movl	%eax, 1312(%rdi)
	cmpl	$1, %eax
	jne	.LBB33_7
# BB#6:                                 #   in Loop: Header=BB33_2 Depth=1
	movl	$1, 1308(%rdi)
	jmp	.LBB33_17
	.p2align	4, 0x90
.LBB33_15:                              #   in Loop: Header=BB33_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB33_19
# BB#16:                                #   in Loop: Header=BB33_2 Depth=1
	cmpl	$0, (%rdx)
	sete	1284(%rdi)
	setne	1285(%rdi)
	jmp	.LBB33_17
.LBB33_7:                               #   in Loop: Header=BB33_2 Depth=1
	cmpl	$10, %eax
	jg	.LBB33_9
# BB#8:                                 #   in Loop: Header=BB33_2 Depth=1
	movl	$2, 1308(%rdi)
	jmp	.LBB33_17
.LBB33_9:                               #   in Loop: Header=BB33_2 Depth=1
	addl	$-8, %eax
	movl	%eax, 1308(%rdi)
	movl	$10, 1312(%rdi)
	.p2align	4, 0x90
.LBB33_17:                              #   in Loop: Header=BB33_2 Depth=1
	incq	%r10
	addq	$16, %rdx
	cmpq	%r8, %r10
	jb	.LBB33_2
.LBB33_18:
	xorl	%eax, %eax
.LBB33_19:                              # %_ZN9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj.exit
	retq
.Lfunc_end33:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end33-_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI33_0:
	.quad	.LBB33_10
	.quad	.LBB33_19
	.quad	.LBB33_13
	.quad	.LBB33_4
	.quad	.LBB33_15

	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder644CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder644CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo,@function
_ZN9NCompress8NDeflate8NEncoder11CCOMCoder644CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo: # @_ZN9NCompress8NDeflate8NEncoder11CCOMCoder644CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 16
.Lcfi193:
	.cfi_offset %rbx, -16
	addq	$24, %rdi
.Ltmp92:
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	movl	%eax, %ebx
.Ltmp93:
.LBB34_4:                               # %_ZN9NCompress8NDeflate8NEncoder6CCoder8BaseCodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo.exit
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB34_1:
.Ltmp94:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB34_3
# BB#2:
	movl	(%rax), %ebx
	callq	__cxa_end_catch
	jmp	.LBB34_4
.LBB34_3:
	callq	__cxa_end_catch
	movl	$-2147467259, %ebx      # imm = 0x80004005
	jmp	.LBB34_4
.Lfunc_end34:
	.size	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder644CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo, .Lfunc_end34-_ZN9NCompress8NDeflate8NEncoder11CCOMCoder644CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\250"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp92-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin6   #     jumps to .Ltmp94
	.byte	3                       #   On action: 2
	.long	.Ltmp93-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Lfunc_end34-.Ltmp93    #   Call between .Ltmp93 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB35_18
# BB#1:                                 # %.lr.ph.i
	movl	%ecx, %r8d
	addq	$8, %rdx
	xorl	%r10d, %r10d
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB35_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%r10,4), %ecx
	movl	$-2147024809, %eax      # imm = 0x80070057
	addl	$-8, %ecx
	cmpl	$4, %ecx
	ja	.LBB35_19
# BB#3:                                 #   in Loop: Header=BB35_2 Depth=1
	jmpq	*.LJTI35_0(,%rcx,8)
.LBB35_10:                              #   in Loop: Header=BB35_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB35_19
# BB#11:                                #   in Loop: Header=BB35_2 Depth=1
	movl	(%rdx), %ecx
	movl	%ecx, 1288(%rdi)
	cmpl	$3, %ecx
	jb	.LBB35_19
# BB#12:                                #   in Loop: Header=BB35_2 Depth=1
	cmpl	1336(%rdi), %ecx
	jbe	.LBB35_17
	jmp	.LBB35_19
	.p2align	4, 0x90
.LBB35_13:                              #   in Loop: Header=BB35_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB35_19
# BB#14:                                #   in Loop: Header=BB35_2 Depth=1
	movl	(%rdx), %eax
	movl	%eax, 39784(%rdi)
	jmp	.LBB35_17
	.p2align	4, 0x90
.LBB35_4:                               #   in Loop: Header=BB35_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB35_19
# BB#5:                                 #   in Loop: Header=BB35_2 Depth=1
	movl	(%rdx), %eax
	testl	%eax, %eax
	cmovel	%r9d, %eax
	movl	%eax, 1320(%rdi)
	cmpl	$1, %eax
	jne	.LBB35_7
# BB#6:                                 #   in Loop: Header=BB35_2 Depth=1
	movl	$1, 1316(%rdi)
	jmp	.LBB35_17
	.p2align	4, 0x90
.LBB35_15:                              #   in Loop: Header=BB35_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB35_19
# BB#16:                                #   in Loop: Header=BB35_2 Depth=1
	cmpl	$0, (%rdx)
	sete	1292(%rdi)
	setne	1293(%rdi)
	jmp	.LBB35_17
.LBB35_7:                               #   in Loop: Header=BB35_2 Depth=1
	cmpl	$10, %eax
	jg	.LBB35_9
# BB#8:                                 #   in Loop: Header=BB35_2 Depth=1
	movl	$2, 1316(%rdi)
	jmp	.LBB35_17
.LBB35_9:                               #   in Loop: Header=BB35_2 Depth=1
	addl	$-8, %eax
	movl	%eax, 1316(%rdi)
	movl	$10, 1320(%rdi)
	.p2align	4, 0x90
.LBB35_17:                              #   in Loop: Header=BB35_2 Depth=1
	incq	%r10
	addq	$16, %rdx
	cmpq	%r8, %r10
	jb	.LBB35_2
.LBB35_18:
	xorl	%eax, %eax
.LBB35_19:                              # %_ZN9NCompress8NDeflate8NEncoder6CCoder25BaseSetEncoderProperties2EPKjPK14tagPROPVARIANTj.exit
	retq
.Lfunc_end35:
	.size	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end35-_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI35_0:
	.quad	.LBB35_10
	.quad	.LBB35_19
	.quad	.LBB35_13
	.quad	.LBB35_4
	.quad	.LBB35_15

	.text
	.globl	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB36_18
# BB#1:                                 # %.lr.ph.i.i
	movl	%ecx, %r8d
	addq	$8, %rdx
	xorl	%r10d, %r10d
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB36_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%r10,4), %ecx
	movl	$-2147024809, %eax      # imm = 0x80070057
	addl	$-8, %ecx
	cmpl	$4, %ecx
	ja	.LBB36_19
# BB#3:                                 #   in Loop: Header=BB36_2 Depth=1
	jmpq	*.LJTI36_0(,%rcx,8)
.LBB36_10:                              #   in Loop: Header=BB36_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB36_19
# BB#11:                                #   in Loop: Header=BB36_2 Depth=1
	movl	(%rdx), %ecx
	movl	%ecx, 1280(%rdi)
	cmpl	$3, %ecx
	jb	.LBB36_19
# BB#12:                                #   in Loop: Header=BB36_2 Depth=1
	cmpl	1328(%rdi), %ecx
	jbe	.LBB36_17
	jmp	.LBB36_19
	.p2align	4, 0x90
.LBB36_13:                              #   in Loop: Header=BB36_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB36_19
# BB#14:                                #   in Loop: Header=BB36_2 Depth=1
	movl	(%rdx), %eax
	movl	%eax, 39776(%rdi)
	jmp	.LBB36_17
	.p2align	4, 0x90
.LBB36_4:                               #   in Loop: Header=BB36_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB36_19
# BB#5:                                 #   in Loop: Header=BB36_2 Depth=1
	movl	(%rdx), %eax
	testl	%eax, %eax
	cmovel	%r9d, %eax
	movl	%eax, 1312(%rdi)
	cmpl	$1, %eax
	jne	.LBB36_7
# BB#6:                                 #   in Loop: Header=BB36_2 Depth=1
	movl	$1, 1308(%rdi)
	jmp	.LBB36_17
	.p2align	4, 0x90
.LBB36_15:                              #   in Loop: Header=BB36_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB36_19
# BB#16:                                #   in Loop: Header=BB36_2 Depth=1
	cmpl	$0, (%rdx)
	sete	1284(%rdi)
	setne	1285(%rdi)
	jmp	.LBB36_17
.LBB36_7:                               #   in Loop: Header=BB36_2 Depth=1
	cmpl	$10, %eax
	jg	.LBB36_9
# BB#8:                                 #   in Loop: Header=BB36_2 Depth=1
	movl	$2, 1308(%rdi)
	jmp	.LBB36_17
.LBB36_9:                               #   in Loop: Header=BB36_2 Depth=1
	addl	$-8, %eax
	movl	%eax, 1308(%rdi)
	movl	$10, 1312(%rdi)
	.p2align	4, 0x90
.LBB36_17:                              #   in Loop: Header=BB36_2 Depth=1
	incq	%r10
	addq	$16, %rdx
	cmpq	%r8, %r10
	jb	.LBB36_2
.LBB36_18:
	xorl	%eax, %eax
.LBB36_19:                              # %_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj.exit
	retq
.Lfunc_end36:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end36-_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI36_0:
	.quad	.LBB36_10
	.quad	.LBB36_19
	.quad	.LBB36_13
	.quad	.LBB36_4
	.quad	.LBB36_15

	.section	.text._ZN9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi194:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi195:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi196:
	.cfi_def_cfa_offset 32
.Lcfi197:
	.cfi_offset %rbx, -32
.Lcfi198:
	.cfi_offset %r14, -24
.Lcfi199:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB37_3
# BB#1:
	movl	$IID_ICompressSetCoderProperties, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB37_2
.LBB37_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB37_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB37_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB37_4
.Lfunc_end37:
	.size	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end37-_ZN9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv,"axG",@progbits,_ZN9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv,comdat
	.weak	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv,@function
_ZN9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv: # @_ZN9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end38:
	.size	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv, .Lfunc_end38-_ZN9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv,"axG",@progbits,_ZN9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv,comdat
	.weak	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv,@function
_ZN9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv: # @_ZN9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi200:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB39_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB39_2:
	popq	%rcx
	retq
.Lfunc_end39:
	.size	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv, .Lfunc_end39-_ZN9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NEncoder9CCOMCoderD2Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD2Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD2Ev,@function
_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD2Ev: # @_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi201:
	.cfi_def_cfa_offset 16
	addq	$24, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev
	popq	%rax
	retq
.Lfunc_end40:
	.size	_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD2Ev, .Lfunc_end40-_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD2Ev
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev,@function
_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev: # @_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi202:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi203:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi204:
	.cfi_def_cfa_offset 32
.Lcfi205:
	.cfi_offset %rbx, -24
.Lcfi206:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
.Ltmp95:
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev
.Ltmp96:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB41_2:
.Ltmp97:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end41:
	.size	_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev, .Lfunc_end41-_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp95-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin7   #     jumps to .Ltmp97
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Lfunc_end41-.Ltmp96    #   Call between .Ltmp96 and .Lfunc_end41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi207:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB42_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB42_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB42_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB42_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB42_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB42_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB42_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB42_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB42_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB42_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB42_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB42_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB42_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB42_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB42_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB42_32
.LBB42_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetCoderProperties(%rip), %cl
	jne	.LBB42_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+1(%rip), %cl
	jne	.LBB42_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+2(%rip), %cl
	jne	.LBB42_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+3(%rip), %cl
	jne	.LBB42_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+4(%rip), %cl
	jne	.LBB42_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+5(%rip), %cl
	jne	.LBB42_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+6(%rip), %cl
	jne	.LBB42_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+7(%rip), %cl
	jne	.LBB42_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+8(%rip), %cl
	jne	.LBB42_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+9(%rip), %cl
	jne	.LBB42_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+10(%rip), %cl
	jne	.LBB42_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+11(%rip), %cl
	jne	.LBB42_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+12(%rip), %cl
	jne	.LBB42_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+13(%rip), %cl
	jne	.LBB42_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+14(%rip), %cl
	jne	.LBB42_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+15(%rip), %cl
	jne	.LBB42_33
.LBB42_32:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB42_33:                              # %_ZN9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end42:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end42-_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv,@function
_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv: # @_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end43:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv, .Lfunc_end43-_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv,@function
_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv: # @_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi208:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB44_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB44_2:                               # %_ZN9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end44:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv, .Lfunc_end44-_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD1Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD1Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD1Ev,@function
_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD1Ev: # @_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$16, %rdi
	jmp	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev # TAILCALL
.Lfunc_end45:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD1Ev, .Lfunc_end45-_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev,@function
_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev: # @_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi210:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi211:
	.cfi_def_cfa_offset 32
.Lcfi212:
	.cfi_offset %rbx, -24
.Lcfi213:
	.cfi_offset %r14, -16
	leaq	-8(%rdi), %rbx
	addq	$16, %rdi
.Ltmp98:
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev
.Ltmp99:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB46_2:
.Ltmp100:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end46:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev, .Lfunc_end46-_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table46:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp98-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin8  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Lfunc_end46-.Ltmp99    #   Call between .Ltmp99 and .Lfunc_end46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi214:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi215:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi216:
	.cfi_def_cfa_offset 32
.Lcfi217:
	.cfi_offset %rbx, -32
.Lcfi218:
	.cfi_offset %r14, -24
.Lcfi219:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB47_3
# BB#1:
	movl	$IID_ICompressSetCoderProperties, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB47_2
.LBB47_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB47_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB47_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB47_4
.Lfunc_end47:
	.size	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv, .Lfunc_end47-_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv,"axG",@progbits,_ZN9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv,comdat
	.weak	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv,@function
_ZN9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv: # @_ZN9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end48:
	.size	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv, .Lfunc_end48-_ZN9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv,"axG",@progbits,_ZN9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv,comdat
	.weak	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv,@function
_ZN9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv: # @_ZN9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi220:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB49_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB49_2:
	popq	%rcx
	retq
.Lfunc_end49:
	.size	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv, .Lfunc_end49-_ZN9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D2Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D2Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D2Ev,@function
_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D2Ev: # @_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D2Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi221:
	.cfi_def_cfa_offset 16
	addq	$24, %rdi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev
	popq	%rax
	retq
.Lfunc_end50:
	.size	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D2Ev, .Lfunc_end50-_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D2Ev
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev,@function
_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev: # @_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi222:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi223:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi224:
	.cfi_def_cfa_offset 32
.Lcfi225:
	.cfi_offset %rbx, -24
.Lcfi226:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
.Ltmp101:
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev
.Ltmp102:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB51_2:
.Ltmp103:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end51:
	.size	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev, .Lfunc_end51-_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table51:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp101-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin9  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end51-.Ltmp102   #   Call between .Ltmp102 and .Lfunc_end51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi227:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB52_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB52_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB52_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB52_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB52_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB52_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB52_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB52_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB52_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB52_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB52_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB52_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB52_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB52_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB52_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB52_32
.LBB52_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetCoderProperties(%rip), %cl
	jne	.LBB52_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+1(%rip), %cl
	jne	.LBB52_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+2(%rip), %cl
	jne	.LBB52_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+3(%rip), %cl
	jne	.LBB52_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+4(%rip), %cl
	jne	.LBB52_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+5(%rip), %cl
	jne	.LBB52_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+6(%rip), %cl
	jne	.LBB52_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+7(%rip), %cl
	jne	.LBB52_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+8(%rip), %cl
	jne	.LBB52_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+9(%rip), %cl
	jne	.LBB52_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+10(%rip), %cl
	jne	.LBB52_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+11(%rip), %cl
	jne	.LBB52_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+12(%rip), %cl
	jne	.LBB52_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+13(%rip), %cl
	jne	.LBB52_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+14(%rip), %cl
	jne	.LBB52_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+15(%rip), %cl
	jne	.LBB52_33
.LBB52_32:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB52_33:                              # %_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end52:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv, .Lfunc_end52-_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv,@function
_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv: # @_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end53:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv, .Lfunc_end53-_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv,@function
_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv: # @_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi228:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB54_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB54_2:                               # %_ZN9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end54:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv, .Lfunc_end54-_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D1Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D1Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D1Ev,@function
_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D1Ev: # @_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D1Ev
	.cfi_startproc
# BB#0:
	addq	$16, %rdi
	jmp	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev # TAILCALL
.Lfunc_end55:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D1Ev, .Lfunc_end55-_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D1Ev
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev,@function
_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev: # @_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi229:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi230:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi231:
	.cfi_def_cfa_offset 32
.Lcfi232:
	.cfi_offset %rbx, -24
.Lcfi233:
	.cfi_offset %r14, -16
	leaq	-8(%rdi), %rbx
	addq	$16, %rdi
.Ltmp104:
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev
.Ltmp105:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB56_2:
.Ltmp106:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end56:
	.size	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev, .Lfunc_end56-_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table56:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp104-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin10 #     jumps to .Ltmp106
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end56-.Ltmp105   #   Call between .Ltmp105 and .Lfunc_end56
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoderL7SzAllocEPvm,@function
_ZN9NCompress8NDeflate8NEncoderL7SzAllocEPvm: # @_ZN9NCompress8NDeflate8NEncoderL7SzAllocEPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyAlloc                 # TAILCALL
.Lfunc_end57:
	.size	_ZN9NCompress8NDeflate8NEncoderL7SzAllocEPvm, .Lfunc_end57-_ZN9NCompress8NDeflate8NEncoderL7SzAllocEPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NEncoderL6SzFreeEPvS2_,@function
_ZN9NCompress8NDeflate8NEncoderL6SzFreeEPvS2_: # @_ZN9NCompress8NDeflate8NEncoderL6SzFreeEPvS2_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyFree                  # TAILCALL
.Lfunc_end58:
	.size	_ZN9NCompress8NDeflate8NEncoderL6SzFreeEPvS2_, .Lfunc_end58-_ZN9NCompress8NDeflate8NEncoderL6SzFreeEPvS2_
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB59_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB59_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB59_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB59_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB59_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB59_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB59_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB59_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB59_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB59_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB59_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB59_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB59_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB59_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB59_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB59_16:
	xorl	%eax, %eax
	retq
.Lfunc_end59:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end59-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_DeflateEncoder.ii,@function
_GLOBAL__sub_I_DeflateEncoder.ii:       # @_GLOBAL__sub_I_DeflateEncoder.ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi234:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi235:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi236:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi237:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi238:
	.cfi_def_cfa_offset 48
.Lcfi239:
	.cfi_offset %rbx, -40
.Lcfi240:
	.cfi_offset %r14, -32
.Lcfi241:
	.cfi_offset %r15, -24
.Lcfi242:
	.cfi_offset %rbp, -16
	xorl	%ebx, %ebx
	movl	$1, %ebp
	jmp	.LBB60_1
	.p2align	4, 0x90
.LBB60_5:                               # %._crit_edge.i.i.1
                                        #   in Loop: Header=BB60_1 Depth=1
	movzbl	_ZN9NCompress8NDeflateL16kLenDirectBits32E+1(%rbx), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	leal	1(%rbx), %esi
	movzbl	_ZN9NCompress8NDeflateL11kLenStart32E+1(%rbx), %eax
	leaq	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%rax), %rdi
	testl	%edx, %edx
	cmovlel	%ebp, %edx
	decl	%edx
	incq	%rdx
	callq	memset
	addq	$2, %rbx
.LBB60_1:                               # %._crit_edge.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	_ZN9NCompress8NDeflateL16kLenDirectBits32E(%rbx), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	movzbl	_ZN9NCompress8NDeflateL11kLenStart32E(%rbx), %eax
	leaq	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE(%rax), %rdi
	testl	%edx, %edx
	cmovlel	%ebp, %edx
	decl	%edx
	incq	%rdx
	movl	%ebx, %esi
	callq	memset
	cmpq	$28, %rbx
	jne	.LBB60_5
# BB#2:                                 # %.preheader.i.i.preheader
	xorl	%ebx, %ebx
	movl	$1, %r14d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB60_3:                               # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	_ZN9NCompress8NDeflateL15kDistDirectBitsE(%rbx), %ecx
	movl	$1, %ebp
	shll	%cl, %ebp
	movslq	%eax, %r15
	leaq	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%r15), %rdi
	cmpl	$1, %ebp
	cmovbel	%r14d, %ebp
	leal	-1(%rbp), %edx
	incq	%rdx
	movl	%ebx, %esi
	callq	memset
	leal	-1(%r15,%rbp), %eax
	incl	%eax
	movzbl	_ZN9NCompress8NDeflateL15kDistDirectBitsE+1(%rbx), %ecx
	movl	$1, %ebp
	shll	%cl, %ebp
	movslq	%eax, %r15
	leaq	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE(%r15), %rdi
	cmpl	$1, %ebp
	cmovbel	%r14d, %ebp
	decl	%ebp
	leaq	1(%rbp), %rdx
	leal	1(%rbx), %esi
	callq	memset
	leaq	1(%r15,%rbp), %rax
	addq	$2, %rbx
	cmpq	$18, %rbx
	jne	.LBB60_3
# BB#4:                                 # %__cxx_global_var_init.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end60:
	.size	_GLOBAL__sub_I_DeflateEncoder.ii, .Lfunc_end60-_GLOBAL__sub_I_DeflateEncoder.ii
	.cfi_endproc

	.type	_ZN9NCompress8NDeflateL11kLenStart64E,@object # @_ZN9NCompress8NDeflateL11kLenStart64E
	.section	.rodata,"a",@progbits
	.p2align	4
_ZN9NCompress8NDeflateL11kLenStart64E:
	.asciz	"\000\001\002\003\004\005\006\007\b\n\f\016\020\024\030\034 (08@P`p\200\240\300\340\000\000"
	.size	_ZN9NCompress8NDeflateL11kLenStart64E, 31

	.type	_ZN9NCompress8NDeflateL11kLenStart32E,@object # @_ZN9NCompress8NDeflateL11kLenStart32E
	.p2align	4
_ZN9NCompress8NDeflateL11kLenStart32E:
	.asciz	"\000\001\002\003\004\005\006\007\b\n\f\016\020\024\030\034 (08@P`p\200\240\300\340\377\000"
	.size	_ZN9NCompress8NDeflateL11kLenStart32E, 31

	.type	_ZN9NCompress8NDeflateL16kLenDirectBits64E,@object # @_ZN9NCompress8NDeflateL16kLenDirectBits64E
	.p2align	4
_ZN9NCompress8NDeflateL16kLenDirectBits64E:
	.asciz	"\000\000\000\000\000\000\000\000\001\001\001\001\002\002\002\002\003\003\003\003\004\004\004\004\005\005\005\005\020\000"
	.size	_ZN9NCompress8NDeflateL16kLenDirectBits64E, 31

	.type	_ZN9NCompress8NDeflateL16kLenDirectBits32E,@object # @_ZN9NCompress8NDeflateL16kLenDirectBits32E
	.p2align	4
_ZN9NCompress8NDeflateL16kLenDirectBits32E:
	.asciz	"\000\000\000\000\000\000\000\000\001\001\001\001\002\002\002\002\003\003\003\003\004\004\004\004\005\005\005\005\000\000"
	.size	_ZN9NCompress8NDeflateL16kLenDirectBits32E, 31

	.type	_ZN9NCompress8NDeflate8NEncoderL7g_AllocE,@object # @_ZN9NCompress8NDeflate8NEncoderL7g_AllocE
	.data
	.p2align	3
_ZN9NCompress8NDeflate8NEncoderL7g_AllocE:
	.quad	_ZN9NCompress8NDeflate8NEncoderL7SzAllocEPvm
	.quad	_ZN9NCompress8NDeflate8NEncoderL6SzFreeEPvS2_
	.size	_ZN9NCompress8NDeflate8NEncoderL7g_AllocE, 16

	.type	_ZN9NCompress8NDeflateL15kDistDirectBitsE,@object # @_ZN9NCompress8NDeflateL15kDistDirectBitsE
	.section	.rodata,"a",@progbits
	.p2align	4
_ZN9NCompress8NDeflateL15kDistDirectBitsE:
	.ascii	"\000\000\000\000\001\001\002\002\003\003\004\004\005\005\006\006\007\007\b\b\t\t\n\n\013\013\f\f\r\r\016\016"
	.size	_ZN9NCompress8NDeflateL15kDistDirectBitsE, 32

	.type	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE,@object # @_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE
	.local	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE
	.comm	_ZN9NCompress8NDeflate8NEncoderL10g_LenSlotsE,256,16
	.type	_ZN9NCompress8NDeflateL10kDistStartE,@object # @_ZN9NCompress8NDeflateL10kDistStartE
	.p2align	4
_ZN9NCompress8NDeflateL10kDistStartE:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	16                      # 0x10
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	48                      # 0x30
	.long	64                      # 0x40
	.long	96                      # 0x60
	.long	128                     # 0x80
	.long	192                     # 0xc0
	.long	256                     # 0x100
	.long	384                     # 0x180
	.long	512                     # 0x200
	.long	768                     # 0x300
	.long	1024                    # 0x400
	.long	1536                    # 0x600
	.long	2048                    # 0x800
	.long	3072                    # 0xc00
	.long	4096                    # 0x1000
	.long	6144                    # 0x1800
	.long	8192                    # 0x2000
	.long	12288                   # 0x3000
	.long	16384                   # 0x4000
	.long	24576                   # 0x6000
	.long	32768                   # 0x8000
	.long	49152                   # 0xc000
	.size	_ZN9NCompress8NDeflateL10kDistStartE, 128

	.type	_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE,@object # @_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE
	.p2align	4
_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE:
	.ascii	"\020\021\022\000\b\007\t\006\n\005\013\004\f\003\r\002\016\001\017"
	.size	_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE, 19

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24

	.type	_ZTVN9NCompress8NDeflate8NEncoder9CCOMCoderE,@object # @_ZTVN9NCompress8NDeflate8NEncoder9CCOMCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress8NDeflate8NEncoder9CCOMCoderE
	.p2align	3
_ZTVN9NCompress8NDeflate8NEncoder9CCOMCoderE:
	.quad	0
	.quad	_ZTIN9NCompress8NDeflate8NEncoder9CCOMCoderE
	.quad	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv
	.quad	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv
	.quad	_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD2Ev
	.quad	_ZN9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev
	.quad	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.quad	_ZN9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	-8
	.quad	_ZTIN9NCompress8NDeflate8NEncoder9CCOMCoderE
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder6AddRefEv
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder7ReleaseEv
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD1Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoderD0Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder9CCOMCoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.size	_ZTVN9NCompress8NDeflate8NEncoder9CCOMCoderE, 136

	.type	_ZTSN9NCompress8NDeflate8NEncoder9CCOMCoderE,@object # @_ZTSN9NCompress8NDeflate8NEncoder9CCOMCoderE
	.globl	_ZTSN9NCompress8NDeflate8NEncoder9CCOMCoderE
	.p2align	4
_ZTSN9NCompress8NDeflate8NEncoder9CCOMCoderE:
	.asciz	"N9NCompress8NDeflate8NEncoder9CCOMCoderE"
	.size	_ZTSN9NCompress8NDeflate8NEncoder9CCOMCoderE, 41

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS27ICompressSetCoderProperties,@object # @_ZTS27ICompressSetCoderProperties
	.section	.rodata._ZTS27ICompressSetCoderProperties,"aG",@progbits,_ZTS27ICompressSetCoderProperties,comdat
	.weak	_ZTS27ICompressSetCoderProperties
	.p2align	4
_ZTS27ICompressSetCoderProperties:
	.asciz	"27ICompressSetCoderProperties"
	.size	_ZTS27ICompressSetCoderProperties, 30

	.type	_ZTI27ICompressSetCoderProperties,@object # @_ZTI27ICompressSetCoderProperties
	.section	.rodata._ZTI27ICompressSetCoderProperties,"aG",@progbits,_ZTI27ICompressSetCoderProperties,comdat
	.weak	_ZTI27ICompressSetCoderProperties
	.p2align	4
_ZTI27ICompressSetCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27ICompressSetCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI27ICompressSetCoderProperties, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTSN9NCompress8NDeflate8NEncoder6CCoderE,@object # @_ZTSN9NCompress8NDeflate8NEncoder6CCoderE
	.section	.rodata._ZTSN9NCompress8NDeflate8NEncoder6CCoderE,"aG",@progbits,_ZTSN9NCompress8NDeflate8NEncoder6CCoderE,comdat
	.weak	_ZTSN9NCompress8NDeflate8NEncoder6CCoderE
	.p2align	4
_ZTSN9NCompress8NDeflate8NEncoder6CCoderE:
	.asciz	"N9NCompress8NDeflate8NEncoder6CCoderE"
	.size	_ZTSN9NCompress8NDeflate8NEncoder6CCoderE, 38

	.type	_ZTIN9NCompress8NDeflate8NEncoder6CCoderE,@object # @_ZTIN9NCompress8NDeflate8NEncoder6CCoderE
	.section	.rodata._ZTIN9NCompress8NDeflate8NEncoder6CCoderE,"aG",@progbits,_ZTIN9NCompress8NDeflate8NEncoder6CCoderE,comdat
	.weak	_ZTIN9NCompress8NDeflate8NEncoder6CCoderE
	.p2align	3
_ZTIN9NCompress8NDeflate8NEncoder6CCoderE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN9NCompress8NDeflate8NEncoder6CCoderE
	.size	_ZTIN9NCompress8NDeflate8NEncoder6CCoderE, 16

	.type	_ZTIN9NCompress8NDeflate8NEncoder9CCOMCoderE,@object # @_ZTIN9NCompress8NDeflate8NEncoder9CCOMCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress8NDeflate8NEncoder9CCOMCoderE
	.p2align	4
_ZTIN9NCompress8NDeflate8NEncoder9CCOMCoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress8NDeflate8NEncoder9CCOMCoderE
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI27ICompressSetCoderProperties
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.quad	_ZTIN9NCompress8NDeflate8NEncoder6CCoderE
	.quad	6146                    # 0x1802
	.size	_ZTIN9NCompress8NDeflate8NEncoder9CCOMCoderE, 88

	.type	_ZTVN9NCompress8NDeflate8NEncoder11CCOMCoder64E,@object # @_ZTVN9NCompress8NDeflate8NEncoder11CCOMCoder64E
	.globl	_ZTVN9NCompress8NDeflate8NEncoder11CCOMCoder64E
	.p2align	3
_ZTVN9NCompress8NDeflate8NEncoder11CCOMCoder64E:
	.quad	0
	.quad	_ZTIN9NCompress8NDeflate8NEncoder11CCOMCoder64E
	.quad	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv
	.quad	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv
	.quad	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D2Ev
	.quad	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev
	.quad	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder644CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.quad	_ZN9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	-8
	.quad	_ZTIN9NCompress8NDeflate8NEncoder11CCOMCoder64E
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6414QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder646AddRefEv
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder647ReleaseEv
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D1Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder64D0Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NEncoder11CCOMCoder6418SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.size	_ZTVN9NCompress8NDeflate8NEncoder11CCOMCoder64E, 136

	.type	_ZTSN9NCompress8NDeflate8NEncoder11CCOMCoder64E,@object # @_ZTSN9NCompress8NDeflate8NEncoder11CCOMCoder64E
	.globl	_ZTSN9NCompress8NDeflate8NEncoder11CCOMCoder64E
	.p2align	4
_ZTSN9NCompress8NDeflate8NEncoder11CCOMCoder64E:
	.asciz	"N9NCompress8NDeflate8NEncoder11CCOMCoder64E"
	.size	_ZTSN9NCompress8NDeflate8NEncoder11CCOMCoder64E, 44

	.type	_ZTIN9NCompress8NDeflate8NEncoder11CCOMCoder64E,@object # @_ZTIN9NCompress8NDeflate8NEncoder11CCOMCoder64E
	.globl	_ZTIN9NCompress8NDeflate8NEncoder11CCOMCoder64E
	.p2align	4
_ZTIN9NCompress8NDeflate8NEncoder11CCOMCoder64E:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress8NDeflate8NEncoder11CCOMCoder64E
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI27ICompressSetCoderProperties
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.quad	_ZTIN9NCompress8NDeflate8NEncoder6CCoderE
	.quad	6146                    # 0x1802
	.size	_ZTIN9NCompress8NDeflate8NEncoder11CCOMCoder64E, 88

	.type	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE,@object # @_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE
	.local	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE
	.comm	_ZN9NCompress8NDeflate8NEncoderL9g_FastPosE,512,16
	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_DeflateEncoder.ii

	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoderC1Eb
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoderC1Eb,@function
_ZN9NCompress8NDeflate8NEncoder6CCoderC1Eb = _ZN9NCompress8NDeflate8NEncoder6CCoderC2Eb
	.globl	_ZN9NCompress8NDeflate8NEncoder6CCoderD1Ev
	.type	_ZN9NCompress8NDeflate8NEncoder6CCoderD1Ev,@function
_ZN9NCompress8NDeflate8NEncoder6CCoderD1Ev = _ZN9NCompress8NDeflate8NEncoder6CCoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
