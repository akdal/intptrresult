	.text
	.file	"dt.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182419250377371     # double 1.0000001000000001
.LCPI0_1:
	.quad	4457293557087583675     # double 1.0E-10
.LCPI0_2:
	.quad	4607182418800017408     # double 1
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 48
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	leaq	8(%rsp), %rdi
	movl	$16, %esi
	movl	$16384, %edx            # imm = 0x4000
	callq	posix_memalign
	leaq	16(%rsp), %rdi
	movl	$16, %esi
	movl	$16384, %edx            # imm = 0x4000
	callq	posix_memalign
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	movl	$131072, %esi           # imm = 0x20000
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$2048, %esi             # imm = 0x800
	xorl	%eax, %eax
	callq	printf
	movl	$2048, %r14d            # imm = 0x800
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%r14, %xmm0
	callq	cosf
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	movq	8(%rsp), %rax
	movsd	%xmm0, (%rax,%rbx,8)
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rbx, %xmm0
	callq	sinf
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	addsd	.LCPI0_2(%rip), %xmm0
	movq	16(%rsp), %rax
	movsd	%xmm0, (%rax,%rbx,8)
	incq	%rbx
	decq	%r14
	jne	.LBB0_1
# BB#2:
	movq	8(%rsp), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	movl	$2, %esi
	.p2align	4, 0x90
.LBB0_4:                                # %vector.body
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rax,%rsi,8), %xmm0
	movupd	-16(%rcx,%rsi,8), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, -16(%rcx,%rsi,8)
	movupd	(%rax,%rsi,8), %xmm0
	movupd	(%rcx,%rsi,8), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, (%rcx,%rsi,8)
	addq	$4, %rsi
	cmpq	$2050, %rsi             # imm = 0x802
	jne	.LBB0_4
# BB#5:                                 # %middle.block
                                        #   in Loop: Header=BB0_3 Depth=1
	incq	%rdx
	cmpq	$131072, %rdx           # imm = 0x20000
	jne	.LBB0_3
# BB#6:                                 # %double_array_divs_variable.exit
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" %i iterations of each test. "
	.size	.L.str, 30

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" inner loop / array size %i.\n"
	.size	.L.str.1, 30

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%f\n"
	.size	.L.str.2, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
