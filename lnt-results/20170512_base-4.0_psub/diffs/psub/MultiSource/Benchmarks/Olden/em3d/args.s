	.text
	.file	"args.bc"
	.globl	dealwithargs
	.p2align	4, 0x90
	.type	dealwithargs,@function
dealwithargs:                           # @dealwithargs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	cmpl	$5, %ebp
	jl	.LBB0_5
# BB#1:                                 # %.thread
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, NumNodes(%rip)
	jmp	.LBB0_2
.LBB0_5:
	movl	$1, NumNodes(%rip)
	cmpl	$1, %ebp
	jle	.LBB0_6
.LBB0_2:
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, n_nodes(%rip)
	cmpl	$2, %ebp
	jle	.LBB0_7
# BB#3:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, d_nodes(%rip)
	movl	$75, %eax
	cmpl	$3, %ebp
	je	.LBB0_8
# BB#4:
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	jmp	.LBB0_8
.LBB0_6:                                # %.thread10
	movl	$64, n_nodes(%rip)
.LBB0_7:                                # %.thread11
	movl	$3, d_nodes(%rip)
	movl	$75, %eax
.LBB0_8:
	movl	%eax, local_p(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	dealwithargs, .Lfunc_end0-dealwithargs
	.cfi_endproc

	.type	NumNodes,@object        # @NumNodes
	.comm	NumNodes,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
