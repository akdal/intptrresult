	.text
	.file	"Lzma2Encoder.bc"
	.globl	_ZN9NCompress6NLzma28CEncoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CEncoderC2Ev,@function
_ZN9NCompress6NLzma28CEncoderC2Ev:      # @_ZN9NCompress6NLzma28CEncoderC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$0, 24(%rbx)
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress6NLzma28CEncoderE+160, 16(%rbx)
	movq	$0, 32(%rbx)
	movl	$_ZN9NCompress6NLzma2L7g_AllocE, %edi
	movl	$_ZN9NCompress6NLzma2L10g_BigAllocE, %esi
	callq	Lzma2Enc_Create
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	popq	%rbx
	retq
.LBB0_2:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end0:
	.size	_ZN9NCompress6NLzma28CEncoderC2Ev, .Lfunc_end0-_ZN9NCompress6NLzma28CEncoderC2Ev
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CEncoderD2Ev,@function
_ZN9NCompress6NLzma28CEncoderD2Ev:      # @_ZN9NCompress6NLzma28CEncoderD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	movq	$_ZTVN9NCompress6NLzma28CEncoderE+160, 16(%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	callq	Lzma2Enc_Destroy
.LBB1_2:
	popq	%rax
	retq
.Lfunc_end1:
	.size	_ZN9NCompress6NLzma28CEncoderD2Ev, .Lfunc_end1-_ZN9NCompress6NLzma28CEncoderD2Ev
	.cfi_endproc

	.globl	_ZThn8_N9NCompress6NLzma28CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CEncoderD1Ev,@function
_ZThn8_N9NCompress6NLzma28CEncoderD1Ev: # @_ZThn8_N9NCompress6NLzma28CEncoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	$_ZTVN9NCompress6NLzma28CEncoderE+160, 8(%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	jmp	Lzma2Enc_Destroy        # TAILCALL
.LBB2_1:                                # %_ZN9NCompress6NLzma28CEncoderD2Ev.exit
	retq
.Lfunc_end2:
	.size	_ZThn8_N9NCompress6NLzma28CEncoderD1Ev, .Lfunc_end2-_ZThn8_N9NCompress6NLzma28CEncoderD1Ev
	.cfi_endproc

	.globl	_ZThn16_N9NCompress6NLzma28CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CEncoderD1Ev,@function
_ZThn16_N9NCompress6NLzma28CEncoderD1Ev: # @_ZThn16_N9NCompress6NLzma28CEncoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -16(%rdi)
	movq	$_ZTVN9NCompress6NLzma28CEncoderE+160, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB3_1
# BB#2:
	jmp	Lzma2Enc_Destroy        # TAILCALL
.LBB3_1:                                # %_ZN9NCompress6NLzma28CEncoderD2Ev.exit
	retq
.Lfunc_end3:
	.size	_ZThn16_N9NCompress6NLzma28CEncoderD1Ev, .Lfunc_end3-_ZThn16_N9NCompress6NLzma28CEncoderD1Ev
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CEncoderD0Ev,@function
_ZN9NCompress6NLzma28CEncoderD0Ev:      # @_ZN9NCompress6NLzma28CEncoderD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress6NLzma28CEncoderE+160, 16(%rbx)
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_2
# BB#1:
.Ltmp0:
	callq	Lzma2Enc_Destroy
.Ltmp1:
.LBB4_2:                                # %_ZN9NCompress6NLzma28CEncoderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_3:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN9NCompress6NLzma28CEncoderD0Ev, .Lfunc_end4-_ZN9NCompress6NLzma28CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress6NLzma28CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CEncoderD0Ev,@function
_ZThn8_N9NCompress6NLzma28CEncoderD0Ev: # @_ZThn8_N9NCompress6NLzma28CEncoderD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+96, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	$_ZTVN9NCompress6NLzma28CEncoderE+160, 8(%rax)
	movq	24(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
.Ltmp3:
	callq	Lzma2Enc_Destroy
.Ltmp4:
.LBB5_2:                                # %_ZN9NCompress6NLzma28CEncoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_3:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZThn8_N9NCompress6NLzma28CEncoderD0Ev, .Lfunc_end5-_ZThn8_N9NCompress6NLzma28CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn16_N9NCompress6NLzma28CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CEncoderD0Ev,@function
_ZThn16_N9NCompress6NLzma28CEncoderD0Ev: # @_ZThn16_N9NCompress6NLzma28CEncoderD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+96, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CEncoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -16(%rax)
	movq	$_ZTVN9NCompress6NLzma28CEncoderE+160, (%rax)
	movq	16(%rax), %rdi
	leaq	-16(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB6_2
# BB#1:
.Ltmp6:
	callq	Lzma2Enc_Destroy
.Ltmp7:
.LBB6_2:                                # %_ZN9NCompress6NLzma28CEncoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_3:
.Ltmp8:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZThn16_N9NCompress6NLzma28CEncoderD0Ev, .Lfunc_end6-_ZThn16_N9NCompress6NLzma28CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin2    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NCompress6NLzma212SetLzma2PropEjRK14tagPROPVARIANTR14CLzma2EncProps
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma212SetLzma2PropEjRK14tagPROPVARIANTR14CLzma2EncProps,@function
_ZN9NCompress6NLzma212SetLzma2PropEjRK14tagPROPVARIANTR14CLzma2EncProps: # @_ZN9NCompress6NLzma212SetLzma2PropEjRK14tagPROPVARIANTR14CLzma2EncProps
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 16
	cmpl	$13, %edi
	je	.LBB7_4
# BB#1:
	cmpl	$4, %edi
	jne	.LBB7_6
# BB#2:
	movl	$-2147024809, %eax      # imm = 0x80070057
	movzwl	(%rsi), %ecx
	cmpl	$19, %ecx
	jne	.LBB7_8
# BB#3:
	movl	8(%rsi), %eax
	movq	%rax, 48(%rdx)
	jmp	.LBB7_7
.LBB7_4:
	movl	$-2147024809, %eax      # imm = 0x80070057
	movzwl	(%rsi), %ecx
	cmpl	$19, %ecx
	jne	.LBB7_8
# BB#5:
	movl	8(%rsi), %eax
	movl	%eax, 60(%rdx)
	jmp	.LBB7_7
.LBB7_6:
	callq	_ZN9NCompress5NLzma11SetLzmaPropEjRK14tagPROPVARIANTR14_CLzmaEncProps
	testl	%eax, %eax
	jne	.LBB7_8
.LBB7_7:
	xorl	%eax, %eax
.LBB7_8:
	popq	%rcx
	retq
.Lfunc_end7:
	.size	_ZN9NCompress6NLzma212SetLzma2PropEjRK14tagPROPVARIANTR14CLzma2EncProps, .Lfunc_end7-_ZN9NCompress6NLzma212SetLzma2PropEjRK14tagPROPVARIANTR14CLzma2EncProps
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZN9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZN9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 128
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	leaq	8(%rsp), %rdi
	callq	Lzma2EncProps_Init
	testl	%ebp, %ebp
	je	.LBB8_10
# BB#1:                                 # %.lr.ph
	movl	%ebp, %r13d
	xorl	%ebp, %ebp
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rbp,4), %edi
	cmpl	$13, %edi
	je	.LBB8_6
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpl	$4, %edi
	jne	.LBB8_8
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	movzwl	(%rbx), %eax
	cmpl	$19, %eax
	jne	.LBB8_13
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	8(%rbx), %eax
	movq	%rax, 56(%rsp)
	jmp	.LBB8_9
	.p2align	4, 0x90
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=1
	movzwl	(%rbx), %eax
	cmpl	$19, %eax
	jne	.LBB8_13
# BB#7:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	8(%rbx), %eax
	movl	%eax, 68(%rsp)
	jmp	.LBB8_9
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_2 Depth=1
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	_ZN9NCompress5NLzma11SetLzmaPropEjRK14tagPROPVARIANTR14_CLzmaEncProps
	testl	%eax, %eax
	jne	.LBB8_11
.LBB8_9:                                #   in Loop: Header=BB8_2 Depth=1
	incq	%rbp
	addq	$16, %rbx
	cmpq	%r13, %rbp
	jb	.LBB8_2
.LBB8_10:                               # %._crit_edge
	movq	32(%r14), %rdi
	leaq	8(%rsp), %rsi
	callq	Lzma2Enc_SetProps
	movl	%eax, %edi
	callq	_Z13SResToHRESULTi
	jmp	.LBB8_11
.LBB8_13:
	movl	$-2147024809, %eax      # imm = 0x80070057
.LBB8_11:                               # %.thread22
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end8-_ZN9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZThn8_N9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZThn8_N9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZThn8_N9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 128
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	leaq	8(%rsp), %rdi
	callq	Lzma2EncProps_Init
	testl	%ebp, %ebp
	je	.LBB9_10
# BB#1:                                 # %.lr.ph.i
	movl	%ebp, %r13d
	xorl	%ebp, %ebp
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rbp,4), %edi
	cmpl	$13, %edi
	je	.LBB9_6
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	cmpl	$4, %edi
	jne	.LBB9_8
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	movzwl	(%rbx), %eax
	cmpl	$19, %eax
	jne	.LBB9_13
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	8(%rbx), %eax
	movq	%rax, 56(%rsp)
	jmp	.LBB9_9
	.p2align	4, 0x90
.LBB9_6:                                #   in Loop: Header=BB9_2 Depth=1
	movzwl	(%rbx), %eax
	cmpl	$19, %eax
	jne	.LBB9_13
# BB#7:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	8(%rbx), %eax
	movl	%eax, 68(%rsp)
	jmp	.LBB9_9
	.p2align	4, 0x90
.LBB9_8:                                #   in Loop: Header=BB9_2 Depth=1
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	_ZN9NCompress5NLzma11SetLzmaPropEjRK14tagPROPVARIANTR14_CLzmaEncProps
	testl	%eax, %eax
	jne	.LBB9_11
.LBB9_9:                                #   in Loop: Header=BB9_2 Depth=1
	incq	%rbp
	addq	$16, %rbx
	cmpq	%r13, %rbp
	jb	.LBB9_2
.LBB9_10:                               # %._crit_edge.i
	movq	24(%r14), %rdi
	leaq	8(%rsp), %rsi
	callq	Lzma2Enc_SetProps
	movl	%eax, %edi
	callq	_Z13SResToHRESULTi
	jmp	.LBB9_11
.LBB9_13:
	movl	$-2147024809, %eax      # imm = 0x80070057
.LBB9_11:                               # %_ZN9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj.exit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZThn8_N9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end9-_ZThn8_N9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZN9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZN9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	32(%rdi), %rdi
	callq	Lzma2Enc_WriteProperties
	movb	%al, 15(%rsp)
	leaq	15(%rsp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end10:
	.size	_ZN9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end10-_ZN9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZThn16_N9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZThn16_N9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZThn16_N9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	16(%rdi), %rdi
	callq	Lzma2Enc_WriteProperties
	movb	%al, 15(%rsp)
	leaq	15(%rsp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end11:
	.size	_ZThn16_N9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end11-_ZThn16_N9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress6NLzma28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress6NLzma28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 144
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movq	%rdx, %r14
	movq	%rdi, %r15
	leaq	32(%rsp), %r12
	movq	%r12, %rdi
	callq	_ZN16CSeqInStreamWrapC1EP19ISequentialInStream
	leaq	56(%rsp), %r13
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN17CSeqOutStreamWrapC1EP20ISequentialOutStream
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN21CCompressProgressWrapC1EP21ICompressProgressInfo
	movq	32(%r15), %rdi
	testq	%rbp, %rbp
	cmoveq	%rbp, %rbx
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	Lzma2Enc_Encode
	cmpl	$10, %eax
	je	.LBB12_5
# BB#1:
	cmpl	$9, %eax
	je	.LBB12_4
# BB#2:
	cmpl	$8, %eax
	jne	.LBB12_7
# BB#3:
	movl	48(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB12_8
	jmp	.LBB12_7
.LBB12_5:
	movl	24(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB12_8
	jmp	.LBB12_7
.LBB12_4:
	movl	72(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB12_8
.LBB12_7:                               # %.thread
	movl	%eax, %edi
	callq	_Z13SResToHRESULTi
	movl	%eax, %ecx
.LBB12_8:
	movl	%ecx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN9NCompress6NLzma28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end12-_ZN9NCompress6NLzma28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc

	.section	.text._ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB13_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB13_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB13_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB13_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB13_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB13_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB13_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB13_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB13_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB13_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB13_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB13_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB13_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB13_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB13_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB13_16
.LBB13_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressSetCoderProperties(%rip), %cl
	jne	.LBB13_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+1(%rip), %al
	jne	.LBB13_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+2(%rip), %al
	jne	.LBB13_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+3(%rip), %al
	jne	.LBB13_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+4(%rip), %al
	jne	.LBB13_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+5(%rip), %al
	jne	.LBB13_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+6(%rip), %al
	jne	.LBB13_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+7(%rip), %al
	jne	.LBB13_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+8(%rip), %al
	jne	.LBB13_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+9(%rip), %al
	jne	.LBB13_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+10(%rip), %al
	jne	.LBB13_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+11(%rip), %al
	jne	.LBB13_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+12(%rip), %al
	jne	.LBB13_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+13(%rip), %al
	jne	.LBB13_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+14(%rip), %al
	jne	.LBB13_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit8
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+15(%rip), %al
	jne	.LBB13_33
.LBB13_16:
	leaq	8(%rdi), %rax
	jmp	.LBB13_50
.LBB13_33:                              # %_ZeqRK4GUIDS1_.exit8.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressWriteCoderProperties(%rip), %cl
	jne	.LBB13_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+1(%rip), %cl
	jne	.LBB13_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+2(%rip), %cl
	jne	.LBB13_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+3(%rip), %cl
	jne	.LBB13_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+4(%rip), %cl
	jne	.LBB13_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+5(%rip), %cl
	jne	.LBB13_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+6(%rip), %cl
	jne	.LBB13_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+7(%rip), %cl
	jne	.LBB13_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+8(%rip), %cl
	jne	.LBB13_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+9(%rip), %cl
	jne	.LBB13_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+10(%rip), %cl
	jne	.LBB13_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+11(%rip), %cl
	jne	.LBB13_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+12(%rip), %cl
	jne	.LBB13_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+13(%rip), %cl
	jne	.LBB13_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+14(%rip), %cl
	jne	.LBB13_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+15(%rip), %cl
	jne	.LBB13_51
# BB#49:
	leaq	16(%rdi), %rax
.LBB13_50:                              # %_ZeqRK4GUIDS1_.exit10.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB13_51:                              # %_ZeqRK4GUIDS1_.exit10.thread
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end13-_ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress6NLzma28CEncoder6AddRefEv,"axG",@progbits,_ZN9NCompress6NLzma28CEncoder6AddRefEv,comdat
	.weak	_ZN9NCompress6NLzma28CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CEncoder6AddRefEv,@function
_ZN9NCompress6NLzma28CEncoder6AddRefEv: # @_ZN9NCompress6NLzma28CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN9NCompress6NLzma28CEncoder6AddRefEv, .Lfunc_end14-_ZN9NCompress6NLzma28CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress6NLzma28CEncoder7ReleaseEv,"axG",@progbits,_ZN9NCompress6NLzma28CEncoder7ReleaseEv,comdat
	.weak	_ZN9NCompress6NLzma28CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CEncoder7ReleaseEv,@function
_ZN9NCompress6NLzma28CEncoder7ReleaseEv: # @_ZN9NCompress6NLzma28CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB15_2:
	popq	%rcx
	retq
.Lfunc_end15:
	.size	_ZN9NCompress6NLzma28CEncoder7ReleaseEv, .Lfunc_end15-_ZN9NCompress6NLzma28CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end16:
	.size	_ZThn8_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end16-_ZThn8_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NLzma28CEncoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress6NLzma28CEncoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress6NLzma28CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CEncoder6AddRefEv,@function
_ZThn8_N9NCompress6NLzma28CEncoder6AddRefEv: # @_ZThn8_N9NCompress6NLzma28CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end17:
	.size	_ZThn8_N9NCompress6NLzma28CEncoder6AddRefEv, .Lfunc_end17-_ZThn8_N9NCompress6NLzma28CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NLzma28CEncoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress6NLzma28CEncoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress6NLzma28CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CEncoder7ReleaseEv,@function
_ZThn8_N9NCompress6NLzma28CEncoder7ReleaseEv: # @_ZThn8_N9NCompress6NLzma28CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB18_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB18_2:                               # %_ZN9NCompress6NLzma28CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZThn8_N9NCompress6NLzma28CEncoder7ReleaseEv, .Lfunc_end18-_ZThn8_N9NCompress6NLzma28CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end19:
	.size	_ZThn16_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end19-_ZThn16_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NLzma28CEncoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress6NLzma28CEncoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress6NLzma28CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CEncoder6AddRefEv,@function
_ZThn16_N9NCompress6NLzma28CEncoder6AddRefEv: # @_ZThn16_N9NCompress6NLzma28CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end20:
	.size	_ZThn16_N9NCompress6NLzma28CEncoder6AddRefEv, .Lfunc_end20-_ZThn16_N9NCompress6NLzma28CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NLzma28CEncoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress6NLzma28CEncoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress6NLzma28CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CEncoder7ReleaseEv,@function
_ZThn16_N9NCompress6NLzma28CEncoder7ReleaseEv: # @_ZThn16_N9NCompress6NLzma28CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB21_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB21_2:                               # %_ZN9NCompress6NLzma28CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZThn16_N9NCompress6NLzma28CEncoder7ReleaseEv, .Lfunc_end21-_ZThn16_N9NCompress6NLzma28CEncoder7ReleaseEv
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma2L7SzAllocEPvm,@function
_ZN9NCompress6NLzma2L7SzAllocEPvm:      # @_ZN9NCompress6NLzma2L7SzAllocEPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyAlloc                 # TAILCALL
.Lfunc_end22:
	.size	_ZN9NCompress6NLzma2L7SzAllocEPvm, .Lfunc_end22-_ZN9NCompress6NLzma2L7SzAllocEPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma2L6SzFreeEPvS1_,@function
_ZN9NCompress6NLzma2L6SzFreeEPvS1_:     # @_ZN9NCompress6NLzma2L6SzFreeEPvS1_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyFree                  # TAILCALL
.Lfunc_end23:
	.size	_ZN9NCompress6NLzma2L6SzFreeEPvS1_, .Lfunc_end23-_ZN9NCompress6NLzma2L6SzFreeEPvS1_
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma2L10SzBigAllocEPvm,@function
_ZN9NCompress6NLzma2L10SzBigAllocEPvm:  # @_ZN9NCompress6NLzma2L10SzBigAllocEPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigAlloc                # TAILCALL
.Lfunc_end24:
	.size	_ZN9NCompress6NLzma2L10SzBigAllocEPvm, .Lfunc_end24-_ZN9NCompress6NLzma2L10SzBigAllocEPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma2L9SzBigFreeEPvS1_,@function
_ZN9NCompress6NLzma2L9SzBigFreeEPvS1_:  # @_ZN9NCompress6NLzma2L9SzBigFreeEPvS1_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigFree                 # TAILCALL
.Lfunc_end25:
	.size	_ZN9NCompress6NLzma2L9SzBigFreeEPvS1_, .Lfunc_end25-_ZN9NCompress6NLzma2L9SzBigFreeEPvS1_
	.cfi_endproc

	.type	_ZTVN9NCompress6NLzma28CEncoderE,@object # @_ZTVN9NCompress6NLzma28CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress6NLzma28CEncoderE
	.p2align	3
_ZTVN9NCompress6NLzma28CEncoderE:
	.quad	0
	.quad	_ZTIN9NCompress6NLzma28CEncoderE
	.quad	_ZN9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress6NLzma28CEncoder6AddRefEv
	.quad	_ZN9NCompress6NLzma28CEncoder7ReleaseEv
	.quad	_ZN9NCompress6NLzma28CEncoderD2Ev
	.quad	_ZN9NCompress6NLzma28CEncoderD0Ev
	.quad	_ZN9NCompress6NLzma28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.quad	_ZN9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	_ZN9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.quad	-8
	.quad	_ZTIN9NCompress6NLzma28CEncoderE
	.quad	_ZThn8_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress6NLzma28CEncoder6AddRefEv
	.quad	_ZThn8_N9NCompress6NLzma28CEncoder7ReleaseEv
	.quad	_ZThn8_N9NCompress6NLzma28CEncoderD1Ev
	.quad	_ZThn8_N9NCompress6NLzma28CEncoderD0Ev
	.quad	_ZThn8_N9NCompress6NLzma28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	-16
	.quad	_ZTIN9NCompress6NLzma28CEncoderE
	.quad	_ZThn16_N9NCompress6NLzma28CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress6NLzma28CEncoder6AddRefEv
	.quad	_ZThn16_N9NCompress6NLzma28CEncoder7ReleaseEv
	.quad	_ZThn16_N9NCompress6NLzma28CEncoderD1Ev
	.quad	_ZThn16_N9NCompress6NLzma28CEncoderD0Ev
	.quad	_ZThn16_N9NCompress6NLzma28CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.size	_ZTVN9NCompress6NLzma28CEncoderE, 208

	.type	_ZN9NCompress6NLzma2L7g_AllocE,@object # @_ZN9NCompress6NLzma2L7g_AllocE
	.data
	.p2align	3
_ZN9NCompress6NLzma2L7g_AllocE:
	.quad	_ZN9NCompress6NLzma2L7SzAllocEPvm
	.quad	_ZN9NCompress6NLzma2L6SzFreeEPvS1_
	.size	_ZN9NCompress6NLzma2L7g_AllocE, 16

	.type	_ZN9NCompress6NLzma2L10g_BigAllocE,@object # @_ZN9NCompress6NLzma2L10g_BigAllocE
	.p2align	3
_ZN9NCompress6NLzma2L10g_BigAllocE:
	.quad	_ZN9NCompress6NLzma2L10SzBigAllocEPvm
	.quad	_ZN9NCompress6NLzma2L9SzBigFreeEPvS1_
	.size	_ZN9NCompress6NLzma2L10g_BigAllocE, 16

	.type	_ZTSN9NCompress6NLzma28CEncoderE,@object # @_ZTSN9NCompress6NLzma28CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN9NCompress6NLzma28CEncoderE
	.p2align	4
_ZTSN9NCompress6NLzma28CEncoderE:
	.asciz	"N9NCompress6NLzma28CEncoderE"
	.size	_ZTSN9NCompress6NLzma28CEncoderE, 29

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS27ICompressSetCoderProperties,@object # @_ZTS27ICompressSetCoderProperties
	.section	.rodata._ZTS27ICompressSetCoderProperties,"aG",@progbits,_ZTS27ICompressSetCoderProperties,comdat
	.weak	_ZTS27ICompressSetCoderProperties
	.p2align	4
_ZTS27ICompressSetCoderProperties:
	.asciz	"27ICompressSetCoderProperties"
	.size	_ZTS27ICompressSetCoderProperties, 30

	.type	_ZTI27ICompressSetCoderProperties,@object # @_ZTI27ICompressSetCoderProperties
	.section	.rodata._ZTI27ICompressSetCoderProperties,"aG",@progbits,_ZTI27ICompressSetCoderProperties,comdat
	.weak	_ZTI27ICompressSetCoderProperties
	.p2align	4
_ZTI27ICompressSetCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27ICompressSetCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI27ICompressSetCoderProperties, 24

	.type	_ZTS29ICompressWriteCoderProperties,@object # @_ZTS29ICompressWriteCoderProperties
	.section	.rodata._ZTS29ICompressWriteCoderProperties,"aG",@progbits,_ZTS29ICompressWriteCoderProperties,comdat
	.weak	_ZTS29ICompressWriteCoderProperties
	.p2align	4
_ZTS29ICompressWriteCoderProperties:
	.asciz	"29ICompressWriteCoderProperties"
	.size	_ZTS29ICompressWriteCoderProperties, 32

	.type	_ZTI29ICompressWriteCoderProperties,@object # @_ZTI29ICompressWriteCoderProperties
	.section	.rodata._ZTI29ICompressWriteCoderProperties,"aG",@progbits,_ZTI29ICompressWriteCoderProperties,comdat
	.weak	_ZTI29ICompressWriteCoderProperties
	.p2align	4
_ZTI29ICompressWriteCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29ICompressWriteCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI29ICompressWriteCoderProperties, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress6NLzma28CEncoderE,@object # @_ZTIN9NCompress6NLzma28CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress6NLzma28CEncoderE
	.p2align	4
_ZTIN9NCompress6NLzma28CEncoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress6NLzma28CEncoderE
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI27ICompressSetCoderProperties
	.quad	2050                    # 0x802
	.quad	_ZTI29ICompressWriteCoderProperties
	.quad	4098                    # 0x1002
	.quad	_ZTI13CMyUnknownImp
	.quad	6146                    # 0x1802
	.size	_ZTIN9NCompress6NLzma28CEncoderE, 88


	.globl	_ZN9NCompress6NLzma28CEncoderC1Ev
	.type	_ZN9NCompress6NLzma28CEncoderC1Ev,@function
_ZN9NCompress6NLzma28CEncoderC1Ev = _ZN9NCompress6NLzma28CEncoderC2Ev
	.globl	_ZN9NCompress6NLzma28CEncoderD1Ev
	.type	_ZN9NCompress6NLzma28CEncoderD1Ev,@function
_ZN9NCompress6NLzma28CEncoderD1Ev = _ZN9NCompress6NLzma28CEncoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
