	.text
	.file	"configfile.bc"
	.globl	JMHelpExit
	.p2align	4, 0x90
	.type	JMHelpExit,@function
JMHelpExit:                             # @JMHelpExit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.224, %edi
	movl	$955, %esi              # imm = 0x3BB
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	JMHelpExit, .Lfunc_end0-JMHelpExit
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI1_1:
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	4                       # 0x4
.LCPI1_2:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
.LCPI1_3:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI1_4:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
.LCPI1_5:
	.long	16                      # 0x10
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	16                      # 0x10
.LCPI1_6:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	4                       # 0x4
	.text
	.globl	Configure
	.p2align	4, 0x90
	.type	Configure,@function
Configure:                              # @Configure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %r14d
	movl	$configinput, %edi
	xorl	%esi, %esi
	movl	$5800, %edx             # imm = 0x16A8
	callq	memset
	movl	$.Lstr, %edi
	callq	puts
	cmpq	$0, Map(%rip)
	je	.LBB1_7
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$Map+8, %eax
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB1_5
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB1_2 Depth=1
	testl	%ecx, %ecx
	jne	.LBB1_6
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	cvttsd2si	16(%rax), %ecx
	movq	(%rax), %rdx
	movl	%ecx, (%rdx)
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	movq	(%rax), %rcx
	movq	16(%rax), %rdx
	movq	%rdx, (%rcx)
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	cmpq	$0, 48(%rax)
	leaq	56(%rax), %rax
	jne	.LBB1_2
.LBB1_7:                                # %InitEncoderParams.exit
	cmpl	$2, %r14d
	jne	.LBB1_11
# BB#8:
	movq	8(%r15), %rdi
	movl	$.L.str.227, %esi
	movl	$2, %edx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB1_9
# BB#10:
	callq	JMHelpExit
.LBB1_11:
	cmpl	$3, %r14d
	jl	.LBB1_9
# BB#12:
	movq	8(%r15), %rbx
	movl	$.L.str.228, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_14
# BB#13:
	movl	$.L.str.225, %ebp
	movl	$1, %r12d
	jmp	.LBB1_15
.LBB1_9:
	movl	$.L.str.225, %ebp
	movl	$1, %r12d
	jmp	.LBB1_16
.LBB1_14:
	movq	16(%r15), %rbp
	movl	$3, %r12d
.LBB1_15:
	movl	$.L.str.227, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_323
.LBB1_16:                               # %.thread
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbp, %rsi
	movl	$.L.str.229, %edi
	xorl	%eax, %eax
	callq	printf
	movq	%rbp, %rdi
	callq	GetConfigFileContent
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_18
# BB#17:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB1_18:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	ParseContent
	movl	$10, %edi
	callq	putchar
	movq	%rbx, %rdi
	callq	free
	cmpl	%r14d, %r12d
	jge	.LBB1_30
# BB#19:                                # %.lr.ph.preheader
	movslq	%r14d, %rbx
	movl	%r14d, (%rsp)           # 4-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph.split.us.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_21 Depth 2
                                        #     Child Loop BB1_40 Depth 2
                                        #     Child Loop BB1_46 Depth 2
                                        #       Child Loop BB1_47 Depth 3
	movslq	%r12d, %rbp
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph.split.us
                                        #   in Loop: Header=BB1_21 Depth=2
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.236, %edx
	xorl	%eax, %eax
	movl	%ebp, %ecx
	movq	%rbx, %r8
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB1_21:                               # %.lr.ph.split.us.preheader
                                        #   Parent Loop BB1_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%rbp,8), %rbx
	movl	$.L.str.227, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_25
# BB#22:                                # %.lr.ph
                                        #   in Loop: Header=BB1_21 Depth=2
	movl	$.L.str.231, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_26
# BB#23:                                #   in Loop: Header=BB1_21 Depth=2
	movl	$.L.str.232, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB1_24
# BB#38:                                # %.us-lcssa90.us
                                        #   in Loop: Header=BB1_20 Depth=1
	incl	%r12d
	xorl	%ebp, %ebp
	cmpl	%r14d, %r12d
	movl	%r12d, %r13d
	movq	16(%rsp), %rbx          # 8-byte Reload
	jge	.LBB1_42
# BB#39:                                # %.lr.ph94.preheader
                                        #   in Loop: Header=BB1_20 Depth=1
	movslq	%r12d, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_40:                               # %.lr.ph94
                                        #   Parent Loop BB1_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r13,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB1_42
# BB#41:                                #   in Loop: Header=BB1_40 Depth=2
	incq	%r13
	callq	strlen
	addl	%eax, %ebp
	cmpq	%rbx, %r13
	jl	.LBB1_40
.LBB1_42:                               # %.critedge
                                        #   in Loop: Header=BB1_20 Depth=1
	addl	$1000, %ebp             # imm = 0x3E8
	movslq	%ebp, %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB1_44
# BB#43:                                #   in Loop: Header=BB1_20 Depth=1
	movl	$.L.str.233, %edi
	callq	no_mem_exit
.LBB1_44:                               #   in Loop: Header=BB1_20 Depth=1
	movb	$0, (%r14)
	cmpl	%r13d, %r12d
	jge	.LBB1_53
# BB#45:                                # %.lr.ph101.preheader
                                        #   in Loop: Header=BB1_20 Depth=1
	movslq	%r12d, %rbp
	.p2align	4, 0x90
.LBB1_46:                               # %.lr.ph101
                                        #   Parent Loop BB1_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_47 Depth 3
	movq	(%r15,%rbp,8), %rbx
	movq	%r14, %rdi
	callq	strlen
	addq	%r14, %rax
	jmp	.LBB1_47
	.p2align	4, 0x90
.LBB1_50:                               #   in Loop: Header=BB1_47 Depth=3
	incq	%rax
	incq	%rbx
.LBB1_47:                               #   Parent Loop BB1_20 Depth=1
                                        #     Parent Loop BB1_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx), %ecx
	cmpb	$61, %cl
	je	.LBB1_324
# BB#48:                                #   in Loop: Header=BB1_47 Depth=3
	testb	%cl, %cl
	je	.LBB1_51
# BB#49:                                #   in Loop: Header=BB1_47 Depth=3
	movb	%cl, (%rax)
	jmp	.LBB1_50
	.p2align	4, 0x90
.LBB1_324:                              #   in Loop: Header=BB1_47 Depth=3
	movw	$15648, (%rax)          # imm = 0x3D20
	movb	$32, 2(%rax)
	addq	$2, %rax
	jmp	.LBB1_50
	.p2align	4, 0x90
.LBB1_51:                               #   in Loop: Header=BB1_46 Depth=2
	movb	$0, (%rax)
	incq	%rbp
	cmpl	%r13d, %ebp
	jne	.LBB1_46
# BB#52:                                #   in Loop: Header=BB1_20 Depth=1
	movl	%r13d, %r12d
.LBB1_53:                               # %._crit_edge102
                                        #   in Loop: Header=BB1_20 Depth=1
	movl	$.L.str.234, %edi
	movl	$.L.str.235, %esi
	xorl	%eax, %eax
	callq	printf
	movq	%r14, %rdi
	callq	strlen
	movq	%r14, %rdi
	movl	%eax, %esi
	callq	ParseContent
	movq	%r14, %rdi
	callq	free
	movl	$10, %edi
	callq	putchar
	movl	(%rsp), %r14d           # 4-byte Reload
	cmpl	%r14d, %r12d
	jl	.LBB1_20
	jmp	.LBB1_30
	.p2align	4, 0x90
.LBB1_26:                               # %.us-lcssa89.us
                                        #   in Loop: Header=BB1_20 Depth=1
	movq	8(%r15,%rbp,8), %rdi
	incq	%rbp
	callq	GetConfigFileContent
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_20 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB1_28:                               #   in Loop: Header=BB1_20 Depth=1
	movq	(%r15,%rbp,8), %rsi
	movl	$.L.str.229, %edi
	xorl	%eax, %eax
	callq	printf
	movq	%rbx, %rdi
	callq	strlen
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	ParseContent
	movl	$10, %edi
	callq	putchar
	movq	%rbx, %rdi
	callq	free
	addl	$2, %r12d
	cmpl	%r14d, %r12d
	jl	.LBB1_20
.LBB1_30:                               # %.outer._crit_edge
	movl	$10, %edi
	callq	putchar
	movq	Map(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB1_70
# BB#31:                                # %.lr.ph.i.i
	movq	input(%rip), %rax
	movl	5256(%rax), %eax
	leal	(%rax,%rax,2), %eax
	leal	-48(%rax,%rax), %eax
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movl	$Map+8, %ebx
	.p2align	4, 0x90
.LBB1_32:                               # =>This Inner Loop Header: Depth=1
	movl	24(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB1_63
# BB#33:                                #   in Loop: Header=BB1_32 Depth=1
	cmpl	$2, %eax
	je	.LBB1_57
# BB#34:                                #   in Loop: Header=BB1_32 Depth=1
	cmpl	$1, %eax
	jne	.LBB1_69
# BB#35:                                #   in Loop: Header=BB1_32 Depth=1
	movl	8(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB1_54
# BB#36:                                #   in Loop: Header=BB1_32 Depth=1
	testl	%eax, %eax
	jne	.LBB1_69
# BB#37:                                #   in Loop: Header=BB1_32 Depth=1
	movq	(%rbx), %rax
	movl	(%rax), %eax
	cvttsd2si	32(%rbx), %r8d
	jmp	.LBB1_65
	.p2align	4, 0x90
.LBB1_63:                               #   in Loop: Header=BB1_32 Depth=1
	cmpl	$0, 8(%rbx)
	jne	.LBB1_69
# BB#64:                                #   in Loop: Header=BB1_32 Depth=1
	movq	(%rbx), %rax
	movl	(%rax), %eax
	movsd	32(%rbx), %xmm0         # xmm0 = mem[0],zero
	subsd	(%rsp), %xmm0           # 8-byte Folded Reload
	cvttsd2si	%xmm0, %r8d
.LBB1_65:                               #   in Loop: Header=BB1_32 Depth=1
	cmpl	%r8d, %eax
	cvttsd2si	40(%rbx), %r9d
	jl	.LBB1_67
# BB#66:                                #   in Loop: Header=BB1_32 Depth=1
	cmpl	%r9d, %eax
	jle	.LBB1_69
.LBB1_67:                               # %._crit_edge2.i.i
                                        #   in Loop: Header=BB1_32 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.311, %edx
	xorl	%eax, %eax
	callq	snprintf
	jmp	.LBB1_68
	.p2align	4, 0x90
.LBB1_57:                               #   in Loop: Header=BB1_32 Depth=1
	movl	8(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB1_61
# BB#58:                                #   in Loop: Header=BB1_32 Depth=1
	testl	%eax, %eax
	jne	.LBB1_69
# BB#59:                                #   in Loop: Header=BB1_32 Depth=1
	movq	(%rbx), %rax
	cvttsd2si	32(%rbx), %r8d
	cmpl	%r8d, (%rax)
	jge	.LBB1_69
# BB#60:                                #   in Loop: Header=BB1_32 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.313, %edx
	xorl	%eax, %eax
	callq	snprintf
	jmp	.LBB1_68
.LBB1_54:                               #   in Loop: Header=BB1_32 Depth=1
	movq	(%rbx), %rax
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	movsd	32(%rbx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	movsd	40(%rbx), %xmm1         # xmm1 = mem[0],zero
	ja	.LBB1_56
# BB#55:                                #   in Loop: Header=BB1_32 Depth=1
	ucomisd	%xmm1, %xmm2
	jbe	.LBB1_69
.LBB1_56:                               # %._crit_edge3.i.i
                                        #   in Loop: Header=BB1_32 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.312, %edx
	movb	$2, %al
	callq	snprintf
	jmp	.LBB1_68
.LBB1_61:                               #   in Loop: Header=BB1_32 Depth=1
	movq	(%rbx), %rax
	movsd	32(%rbx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	jbe	.LBB1_69
# BB#62:                                #   in Loop: Header=BB1_32 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.314, %edx
	movb	$1, %al
	callq	snprintf
	.p2align	4, 0x90
.LBB1_68:                               #   in Loop: Header=BB1_32 Depth=1
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
.LBB1_69:                               #   in Loop: Header=BB1_32 Depth=1
	movq	48(%rbx), %rcx
	addq	$56, %rbx
	testq	%rcx, %rcx
	jne	.LBB1_32
.LBB1_70:                               # %TestEncoderParams.exit.i
	movq	input(%rip), %rdi
	movsd	4080(%rdi), %xmm0       # xmm0 = mem[0],zero
	xorps	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB1_72
	jp	.LBB1_72
# BB#71:
	movabsq	$4629137466983448576, %rax # imm = 0x403E000000000000
	movq	%rax, 4080(%rdi)
.LBB1_72:                               # %.preheader97.i
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [4,4,4,4]
	movups	%xmm0, 136(%rdi)
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [4,2,2,4]
	movups	%xmm0, 152(%rdi)
	movaps	.LCPI1_2(%rip), %xmm0   # xmm0 = [2,2,2,1]
	movups	%xmm0, 168(%rdi)
	movaps	.LCPI1_3(%rip), %xmm0   # xmm0 = [1,2,1,1]
	movups	%xmm0, 184(%rdi)
	movl	$7, 200(%rdi)
	movl	$6, 204(%rdi)
	movl	$5, 216(%rdi)
	movl	$4, 220(%rdi)
	movl	$3, 228(%rdi)
	movl	$2, 252(%rdi)
	movl	$1, 260(%rdi)
	movaps	.LCPI1_4(%rip), %xmm0   # xmm0 = [16,16,16,16]
	movups	%xmm0, 72(%rdi)
	movaps	.LCPI1_5(%rip), %xmm0   # xmm0 = [16,8,8,16]
	movups	%xmm0, 88(%rdi)
	movl	$8, 104(%rdi)
	movaps	.LCPI1_6(%rip), %xmm0   # xmm0 = [8,8,4,4]
	movups	%xmm0, 108(%rdi)
	movabsq	$17179869192, %rax      # imm = 0x400000008
	movq	%rax, 124(%rdi)
	movl	$4, 132(%rdi)
	cmpl	$0, 2964(%rdi)
	je	.LBB1_73
# BB#74:
	movl	2096(%rdi), %eax
	incl	%eax
	jmp	.LBB1_75
.LBB1_73:
	movl	$1, %eax
.LBB1_75:
	movl	48(%rdi), %r8d
	cmpl	$-1, %r8d
	movl	%r8d, %edx
	je	.LBB1_76
.LBB1_81:                               # %select.unfold.i
	movl	%edx, log2_max_frame_num_minus4(%rip)
	testl	%edx, %edx
	jne	.LBB1_85
	jmp	.LBB1_83
.LBB1_76:
	imull	8(%rdi), %eax
	decl	%eax
	je	.LBB1_82
# BB#77:                                # %.lr.ph.i80.i.preheader
	movl	$-4, %ecx
	.p2align	4, 0x90
.LBB1_78:                               # %.lr.ph.i80.i
                                        # =>This Inner Loop Header: Depth=1
	shrl	%eax
	incl	%ecx
	testl	%eax, %eax
	jne	.LBB1_78
# BB#79:                                # %CeilLog2.exit.i
	xorl	%edx, %edx
	testl	%ecx, %ecx
	cmovnsl	%ecx, %edx
	cmpl	$12, %edx
	jl	.LBB1_81
# BB#80:                                # %.thread.i
	movl	$12, log2_max_frame_num_minus4(%rip)
	jmp	.LBB1_85
.LBB1_82:                               # %select.unfold.thread.i
	movl	$0, log2_max_frame_num_minus4(%rip)
.LBB1_83:
	cmpl	$16, 32(%rdi)
	jne	.LBB1_85
# BB#84:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.255, %edx
	movl	$16, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rdi
.LBB1_85:
	movl	52(%rdi), %edx
	cmpl	$-1, %edx
	je	.LBB1_87
# BB#86:                                # %._crit_edge123.i
	movl	20(%rdi), %eax
	movl	%edx, %esi
	jmp	.LBB1_90
.LBB1_87:
	movl	20(%rdi), %eax
	leal	1(%rax), %ecx
	imull	8(%rdi), %ecx
	leal	-1(%rcx,%rcx), %esi
	movl	$-4, %ecx
	.p2align	4, 0x90
.LBB1_88:                               # %.lr.ph.i83.i
                                        # =>This Inner Loop Header: Depth=1
	shrl	%esi
	incl	%ecx
	testl	%esi, %esi
	jne	.LBB1_88
# BB#89:                                # %CeilLog2.exit85.i
	xorl	%ebp, %ebp
	testl	%ecx, %ecx
	cmovnsl	%ecx, %ebp
	cmpl	$13, %ebp
	movl	$12, %esi
	cmovll	%ebp, %esi
.LBB1_90:
	leal	3(%rsi), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	cmpl	$-1, %edx
	movl	%esi, log2_max_pic_order_cnt_lsb_minus4(%rip)
	je	.LBB1_93
# BB#91:
	leal	(,%rax,4), %ecx
	cmpl	%ecx, %ebp
	jge	.LBB1_93
# BB#92:
	movl	$.L.str.256, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	movq	input(%rip), %rdi
	movl	20(%rdi), %eax
.LBB1_93:
	movl	2096(%rdi), %ecx
	cmpl	%eax, %ecx
	jle	.LBB1_95
# BB#94:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.257, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	movq	input(%rip), %rdi
	movl	2096(%rdi), %ecx
.LBB1_95:
	testl	%ecx, %ecx
	je	.LBB1_98
# BB#96:
	movl	2112(%rdi), %ecx
	cmpl	$2, %ecx
	jb	.LBB1_98
# BB#97:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.258, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	movq	input(%rip), %rdi
.LBB1_98:
	movl	4704(%rdi), %eax
	testl	%eax, %eax
	jg	.LBB1_100
# BB#99:
	cmpl	$0, 4708(%rdi)
	jle	.LBB1_105
.LBB1_100:
	cmpl	$0, 2116(%rdi)
	jne	.LBB1_102
# BB#101:
	movl	$.L.str.259, %edi
	xorl	%eax, %eax
	callq	printf
	movq	input(%rip), %rdi
	movl	4704(%rdi), %eax
.LBB1_102:
	movl	$1, 2116(%rdi)
	testl	%eax, %eax
	jle	.LBB1_105
# BB#103:
	movl	4712(%rdi), %ecx
	cmpl	$2, %ecx
	jb	.LBB1_105
# BB#104:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.260, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	movq	input(%rip), %rdi
.LBB1_105:                              # %.thread205.i
	movl	4008(%rdi), %ecx
	cmpl	$2, %ecx
	jb	.LBB1_107
# BB#106:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.261, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	movq	input(%rip), %rdi
.LBB1_107:
	addq	$280, %rdi              # imm = 0x118
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open64
	movl	%eax, p_in(%rip)
	cmpl	$-1, %eax
	jne	.LBB1_109
# BB#108:
	movq	input(%rip), %rcx
	addq	$280, %rcx              # imm = 0x118
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.262, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB1_109:
	movq	input(%rip), %rbx
	addq	$792, %rbx              # imm = 0x318
	movq	%rbx, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB1_112
# BB#110:
	movl	$577, %esi              # imm = 0x241
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open64
	movl	%eax, p_dec(%rip)
	cmpl	$-1, %eax
	jne	.LBB1_112
# BB#111:
	movq	input(%rip), %rcx
	addq	$792, %rcx              # imm = 0x318
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.263, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB1_112:
	movq	input(%rip), %r12
	movl	56(%r12), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	addl	%eax, %ecx
	andl	$-16, %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	movl	$16, %esi
	subl	%edx, %esi
	xorl	%edx, %edx
	cmpl	%ecx, %eax
	cmovnel	%esi, %edx
	movq	img(%rip), %rax
	movl	%edx, 15584(%rax)
	cmpl	$0, 4704(%r12)
	jne	.LBB1_114
# BB#113:
	cmpl	$0, 4708(%r12)
	je	.LBB1_117
.LBB1_114:
	movl	60(%r12), %esi
	testb	$1, %sil
	je	.LBB1_116
# BB#115:
	movl	$.L.str.264, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %r12
	movl	60(%r12), %esi
	movq	img(%rip), %rax
	movl	15584(%rax), %edx
.LBB1_116:
	movl	%esi, %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	addl	%esi, %ecx
	andl	$-32, %ecx
	movl	%esi, %edi
	subl	%ecx, %edi
	movl	$32, %ecx
.LBB1_118:
	subl	%edi, %ecx
	testl	%edi, %edi
	cmovel	%edi, %ecx
	movl	%ecx, 15588(%rax)
	movl	%ecx, %eax
	orl	%edx, %eax
	jne	.LBB1_119
# BB#120:
	cmpl	$1, 264(%r12)
	jne	.LBB1_124
.LBB1_121:
	cmpl	$0, 4708(%r12)
	je	.LBB1_124
# BB#122:
	testb	$1, 268(%r12)
	jne	.LBB1_123
.LBB1_124:
	cmpl	$0, 5032(%r12)
	je	.LBB1_156
.LBB1_125:
	movl	5036(%r12), %ebx
	cmpl	$6, %ebx
	ja	.LBB1_156
# BB#126:
	movl	$69, %eax
	btl	%ebx, %eax
	jae	.LBB1_156
# BB#127:
	leaq	4776(%r12), %rbp
	movq	%rbp, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB1_128
# BB#129:
	movl	$.L.str.237, %esi
	movq	%rbp, %rdi
	callq	fopen64
	movq	%rax, %r15
	testq	%r15, %r15
	movq	input(%rip), %r12
	je	.LBB1_325
# BB#130:                               # %._crit_edge146.i
	movl	5036(%r12), %ebx
	cmpl	$6, %ebx
	jne	.LBB1_132
	jmp	.LBB1_146
.LBB1_128:
	xorl	%r15d, %r15d
	cmpl	$6, %ebx
	je	.LBB1_146
.LBB1_132:
	cmpl	$2, %ebx
	je	.LBB1_139
# BB#133:
	testl	%ebx, %ebx
	jne	.LBB1_155
# BB#134:
	movslq	5032(%r12), %rax
	leaq	4(,%rax,4), %rdi
	callq	malloc
	movq	%rax, 5064(%r12)
	testq	%rax, %rax
	jne	.LBB1_136
# BB#135:
	movl	$.L.str.268, %edi
	callq	no_mem_exit
	movq	input(%rip), %r12
.LBB1_136:                              # %.preheader.i
	cmpl	$0, 5032(%r12)
	js	.LBB1_155
# BB#137:                               # %.lr.ph.i87.preheader
	movq	$-1, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_138:                              # %.lr.ph.i87
                                        # =>This Inner Loop Header: Depth=1
	movq	5064(%r12), %rdx
	addq	%rbp, %rdx
	movl	$.L.str.245, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	movl	$.L.str.269, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	movq	input(%rip), %r12
	movslq	5032(%r12), %rax
	incq	%rbx
	addq	$4, %rbp
	cmpq	%rax, %rbx
	jl	.LBB1_138
	jmp	.LBB1_155
.LBB1_117:
	movl	60(%r12), %esi
	movl	%esi, %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	addl	%esi, %ecx
	andl	$-16, %ecx
	movl	%esi, %edi
	subl	%ecx, %edi
	movl	$16, %ecx
	jmp	.LBB1_118
.LBB1_325:
	addq	$4776, %r12             # imm = 0x12A8
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.263, %edx
	xorl	%eax, %eax
	movq	%r12, %rcx
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	jmp	.LBB1_156
.LBB1_146:
	movb	$1, %al
	cmpl	$0, 4704(%r12)
	jne	.LBB1_148
# BB#147:
	cmpl	$0, 4708(%r12)
	setne	%al
.LBB1_148:
	xorb	$1, %al
	movzbl	%al, %ecx
	movq	img(%rip), %rax
	movl	15584(%rax), %ebx
	movl	15588(%rax), %eax
	addl	56(%r12), %ebx
	sarl	$4, %ebx
	addl	60(%r12), %eax
	sarl	$4, %eax
	movl	$2, %esi
	subl	%ecx, %esi
	cltd
	idivl	%esi
	movl	%eax, %r14d
	movslq	%r14d, %rax
	movslq	%ebx, %rdi
	imulq	%rax, %rdi
	callq	malloc
	movq	%rax, 5056(%r12)
	testq	%rax, %rax
	jne	.LBB1_150
# BB#149:
	movl	$.L.str.272, %edi
	callq	no_mem_exit
.LBB1_150:                              # %.preheader96.i
	imull	%ebx, %r14d
	testl	%r14d, %r14d
	jle	.LBB1_155
# BB#151:                               # %.lr.ph102.preheader.i
	movl	%r14d, %ebx
	leaq	12(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_152:                              # %.lr.ph102.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.245, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	fscanf
	movzbl	12(%rsp), %eax
	movq	input(%rip), %rcx
	movq	5056(%rcx), %rcx
	movb	%al, (%rcx,%rbp)
	movq	input(%rip), %rcx
	movq	5056(%rcx), %rax
	movzbl	(%rax,%rbp), %eax
	cmpl	5032(%rcx), %eax
	jle	.LBB1_154
# BB#153:                               #   in Loop: Header=BB1_152 Depth=1
	addq	$4776, %rcx             # imm = 0x12A8
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.273, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB1_154:                              #   in Loop: Header=BB1_152 Depth=1
	movl	$.L.str.269, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_152
	jmp	.LBB1_155
.LBB1_139:
	movslq	5032(%r12), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, 5040(%r12)
	movslq	5032(%r12), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, 5048(%r12)
	cmpq	$0, 5040(%r12)
	jne	.LBB1_141
# BB#140:
	movl	$.L.str.270, %edi
	callq	no_mem_exit
	movq	input(%rip), %r12
	movq	5048(%r12), %rax
.LBB1_141:
	testq	%rax, %rax
	jne	.LBB1_143
# BB#142:
	movl	$.L.str.271, %edi
	callq	no_mem_exit
	movq	input(%rip), %r12
.LBB1_143:                              # %.preheader94.i
	cmpl	$0, 5032(%r12)
	jle	.LBB1_155
# BB#144:                               # %.lr.ph100.i.preheader
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_145:                              # %.lr.ph100.i
                                        # =>This Inner Loop Header: Depth=1
	movq	5040(%r12), %rdx
	addq	%rbp, %rdx
	movl	$.L.str.245, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	movl	$.L.str.269, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	movq	input(%rip), %rax
	movq	5048(%rax), %rdx
	addq	%rbp, %rdx
	movl	$.L.str.245, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	movl	$.L.str.269, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	incq	%rbx
	movq	input(%rip), %r12
	movslq	5032(%r12), %rax
	addq	$4, %rbp
	cmpq	%rax, %rbx
	jl	.LBB1_145
.LBB1_155:                              # %.loopexit.i
	movq	%r15, %rdi
	callq	fclose
.LBB1_156:
	movq	input(%rip), %rsi
	cmpl	$0, 4000(%rsi)
	je	.LBB1_160
# BB#157:
	cmpl	$0, 4704(%rsi)
	jne	.LBB1_159
# BB#158:
	cmpl	$0, 4708(%rsi)
	je	.LBB1_160
.LBB1_159:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.274, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	movq	input(%rip), %rsi
.LBB1_160:
	cmpl	$0, 4004(%rsi)
	je	.LBB1_164
# BB#161:
	cmpl	$0, 4704(%rsi)
	jne	.LBB1_163
# BB#162:
	cmpl	$0, 4708(%rsi)
	je	.LBB1_164
.LBB1_163:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.275, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	movq	input(%rip), %rsi
.LBB1_164:
	movl	4704(%rsi), %ecx
	cmpl	$3, %ecx
	jb	.LBB1_166
# BB#165:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.276, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	movq	input(%rip), %rsi
.LBB1_166:
	movl	4708(%rsi), %ecx
	cmpl	$4, %ecx
	jb	.LBB1_168
# BB#167:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.277, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
	movq	input(%rip), %rsi
.LBB1_168:
	movl	4168(%rsi), %eax
	testl	%eax, %eax
	jne	.LBB1_171
# BB#169:
	cmpl	$0, 4708(%rsi)
	je	.LBB1_174
# BB#170:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.278, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
	movl	4168(%rsi), %eax
.LBB1_171:
	cmpl	$2, %eax
	jne	.LBB1_174
# BB#172:
	cmpl	$100, (%rsi)
	jl	.LBB1_174
# BB#173:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.279, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_174:                              # %.thread206.i
	cmpl	$2, 5788(%rsi)
	jne	.LBB1_179
# BB#175:
	cmpl	$0, 5784(%rsi)
	jne	.LBB1_179
# BB#176:
	cmpl	$0, 5780(%rsi)
	jne	.LBB1_179
# BB#177:
	movl	5244(%rsi), %eax
	decl	%eax
	cmpl	$1, %eax
	ja	.LBB1_179
# BB#178:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.280, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_179:
	movl	4736(%rsi), %ecx
	movl	32(%rsi), %r8d
	testl	%ecx, %ecx
	js	.LBB1_181
# BB#180:
	cmpl	%r8d, %ecx
	jl	.LBB1_182
.LBB1_181:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.281, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
	movl	4736(%rsi), %ecx
.LBB1_182:
	testl	%ecx, %ecx
	jle	.LBB1_185
# BB#183:
	cmpl	$0, 4012(%rsi)
	jne	.LBB1_185
# BB#184:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.282, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_185:
	cmpl	$0, 4704(%rsi)
	jne	.LBB1_187
# BB#186:
	cmpl	$0, 4708(%rsi)
	je	.LBB1_189
.LBB1_187:
	cmpl	$1, 4764(%rsi)
	jne	.LBB1_189
# BB#188:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.283, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_189:
	cmpl	$1, 4012(%rsi)
	je	.LBB1_192
# BB#190:
	cmpl	$1, 4764(%rsi)
	jne	.LBB1_192
# BB#191:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.284, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_192:
	cmpl	$0, 2928(%rsi)
	jg	.LBB1_194
# BB#193:
	cmpl	$0, 2932(%rsi)
	jle	.LBB1_196
.LBB1_194:
	cmpl	$0, 4708(%rsi)
	je	.LBB1_196
# BB#195:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.285, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_196:
	cmpl	$0, 4736(%rsi)
	jle	.LBB1_199
# BB#197:
	cmpl	$0, 2928(%rsi)
	jle	.LBB1_199
# BB#198:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.286, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_199:
	cmpl	$0, 5032(%rsi)
	jle	.LBB1_202
# BB#200:
	movl	5036(%rsi), %eax
	addl	$-3, %eax
	cmpl	$2, %eax
	ja	.LBB1_202
# BB#201:
	movl	$1, 5032(%rsi)
.LBB1_202:
	cmpl	$0, 5116(%rsi)
	je	.LBB1_209
# BB#203:
	movq	img(%rip), %rax
	movl	15584(%rax), %ecx
	movl	15588(%rax), %eax
	addl	60(%rsi), %eax
	addl	56(%rsi), %ecx
	imull	%eax, %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$24, %eax
	addl	%ecx, %eax
	sarl	$8, %eax
	xorl	%edx, %edx
	divl	5128(%rsi)
	testl	%edx, %edx
	je	.LBB1_205
# BB#204:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.287, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_205:
	cmpl	$0, 2096(%rsi)
	jne	.LBB1_207
# BB#206:
	cmpl	$0, 20(%rsi)
	je	.LBB1_209
.LBB1_207:
	cmpl	$1, 5136(%rsi)
	jne	.LBB1_209
# BB#208:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.288, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_209:
	cmpl	$0, 2096(%rsi)
	je	.LBB1_215
# BB#210:
	cmpl	$0, 2964(%rsi)
	je	.LBB1_215
# BB#211:
	cmpl	$0, 1568(%rsi)
	je	.LBB1_215
# BB#212:
	cmpl	$0, 1560(%rsi)
	je	.LBB1_215
# BB#213:
	cmpl	$0, 5088(%rsi)
	je	.LBB1_215
# BB#214:
	movl	$.L.str.289, %edi
	movl	$-1000, %esi            # imm = 0xFC18
	callq	error
	movq	input(%rip), %rsi
.LBB1_215:
	cmpl	$0, 2112(%rsi)
	jne	.LBB1_219
# BB#216:
	cmpl	$1, 32(%rsi)
	jg	.LBB1_219
# BB#217:
	cmpl	$0, 2096(%rsi)
	jle	.LBB1_219
# BB#218:
	movl	$.L.str.290, %edi
	movl	$-1000, %esi            # imm = 0xFC18
	callq	error
	movq	input(%rip), %rsi
.LBB1_219:
	cmpl	$0, 5100(%rsi)
	je	.LBB1_224
# BB#220:
	cmpl	$0, 2136(%rsi)
	je	.LBB1_222
# BB#221:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.291, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
	cmpl	$0, 5100(%rsi)
	je	.LBB1_224
.LBB1_222:                              # %.thread208.i
	movl	(%rsi), %eax
	addl	$-100, %eax
	cmpl	$45, %eax
	jb	.LBB1_224
# BB#223:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.292, %edx
	movl	$100, %ecx
	movl	$144, %r8d
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_224:                              # %.thread207.i
	cmpl	$0, 5208(%rsi)
	je	.LBB1_227
# BB#225:
	movl	(%rsi), %eax
	addl	$-100, %eax
	cmpl	$45, %eax
	jb	.LBB1_227
# BB#226:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.293, %edx
	movl	$100, %ecx
	movl	$144, %r8d
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_227:
	movl	64(%rsi), %eax
	cmpl	$2, %eax
	jne	.LBB1_230
# BB#228:
	cmpl	$121, (%rsi)
	jg	.LBB1_233
# BB#229:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.294, %edx
	movl	$122, %ecx
	movl	$144, %r8d
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
	movl	64(%rsi), %eax
.LBB1_230:
	cmpl	$3, %eax
	jne	.LBB1_233
# BB#231:
	cmpl	$143, (%rsi)
	jg	.LBB1_233
# BB#232:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.295, %edx
	movl	$144, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_233:                              # %.thread209.i
	cmpl	$0, 2096(%rsi)
	je	.LBB1_237
# BB#234:
	cmpl	$0, 2120(%rsi)
	je	.LBB1_237
# BB#235:
	movl	28(%rsi), %eax
	cmpl	2128(%rsi), %eax
	jge	.LBB1_237
# BB#236:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.296, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_237:
	cmpl	$0, 5776(%rsi)
	je	.LBB1_242
# BB#238:
	cmpl	$0, 5772(%rsi)
	jne	.LBB1_240
# BB#239:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.297, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
	cmpl	$0, 5776(%rsi)
	je	.LBB1_242
.LBB1_240:                              # %.thread211.i
	cmpl	$0, 64(%rsi)
	jne	.LBB1_242
# BB#241:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.298, %edx
	xorl	%eax, %eax
	callq	snprintf
	movq	input(%rip), %rsi
	movl	$0, 5776(%rsi)
.LBB1_242:                              # %.thread210.i
	cmpl	$0, 1564(%rsi)
	je	.LBB1_246
# BB#243:
	cmpl	$0, 4704(%rsi)
	je	.LBB1_245
# BB#244:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.299, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
	cmpl	$0, 1564(%rsi)
	je	.LBB1_246
.LBB1_245:                              # %.thread213.i
	movl	$1, 4000(%rsi)
.LBB1_246:                              # %.thread212.i
	cmpl	$0, 5084(%rsi)
	je	.LBB1_258
# BB#247:
	cmpl	$0, 4704(%rsi)
	jne	.LBB1_249
# BB#248:
	cmpl	$0, 4708(%rsi)
	je	.LBB1_250
.LBB1_249:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.300, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_250:
	cmpl	$0, 2940(%rsi)
	je	.LBB1_252
# BB#251:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.301, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_252:
	cmpl	$0, 2096(%rsi)
	je	.LBB1_254
# BB#253:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.302, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_254:
	movl	5764(%rsi), %eax
	movb	5760(%rsi), %cl
	movl	$1, %edx
	shll	%cl, %edx
	cmpl	%edx, %eax
	jge	.LBB1_256
# BB#255:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.303, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
	movl	5764(%rsi), %eax
.LBB1_256:
	cmpl	%eax, 32(%rsi)
	jge	.LBB1_258
# BB#257:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.304, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_258:
	cmpl	$1, 32(%rsi)
	jne	.LBB1_261
# BB#259:
	cmpl	$0, 2096(%rsi)
	jne	.LBB1_260
.LBB1_261:
	cmpl	$0, 2968(%rsi)
	jne	.LBB1_263
.LBB1_262:
	cmpl	$0, 2964(%rsi)
	je	.LBB1_265
.LBB1_263:
	cmpl	$0, 2096(%rsi)
	jne	.LBB1_264
.LBB1_265:
	movl	(%rsi), %eax
	leal	-66(%rax), %ecx
	cmpl	$56, %ecx
	ja	.LBB1_266
# BB#268:
	movabsq	$72075203408037889, %rdx # imm = 0x100100400400801
	btq	%rcx, %rdx
	jae	.LBB1_266
.LBB1_269:
	cmpl	$0, 4016(%rsi)
	je	.LBB1_272
.LBB1_270:
	cmpl	$1, 4008(%rsi)
	jne	.LBB1_272
# BB#271:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.316, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_272:
	cmpl	$0, 5084(%rsi)
	je	.LBB1_275
# BB#273:
	cmpl	$66, (%rsi)
	je	.LBB1_275
# BB#274:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.317, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_275:
	cmpl	$0, 4016(%rsi)
	je	.LBB1_278
# BB#276:
	cmpl	$88, (%rsi)
	je	.LBB1_278
# BB#277:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.318, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_278:
	cmpl	$0, 4072(%rsi)
	je	.LBB1_281
# BB#279:
	cmpl	$0, 4176(%rsi)
	jne	.LBB1_280
.LBB1_281:
	cmpl	$66, (%rsi)
	jne	.LBB1_294
.LBB1_282:
	cmpl	$0, 2096(%rsi)
	jne	.LBB1_284
# BB#283:
	cmpl	$2, 2964(%rsi)
	jne	.LBB1_286
.LBB1_284:
	cmpl	$0, 2100(%rsi)
	jne	.LBB1_286
# BB#285:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.321, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_286:
	cmpl	$0, 2136(%rsi)
	je	.LBB1_288
# BB#287:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.322, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_288:
	cmpl	$0, 2928(%rsi)
	je	.LBB1_290
# BB#289:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.323, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_290:
	cmpl	$0, 2932(%rsi)
	je	.LBB1_292
# BB#291:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.323, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_292:
	cmpl	$1, 4008(%rsi)
	jne	.LBB1_294
# BB#293:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.324, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_294:
	cmpl	$77, (%rsi)
	jne	.LBB1_299
# BB#295:
	cmpl	$0, 2136(%rsi)
	je	.LBB1_297
# BB#296:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.325, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_297:
	cmpl	$0, 5032(%rsi)
	je	.LBB1_299
# BB#298:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.326, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_299:
	cmpl	$88, (%rsi)
	jne	.LBB1_304
# BB#300:
	cmpl	$0, 2116(%rsi)
	jne	.LBB1_302
# BB#301:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.327, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_302:
	cmpl	$1, 4008(%rsi)
	jne	.LBB1_304
# BB#303:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.328, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_304:                              # %ProfileCheck.exit.i
	movl	4(%rsi), %eax
	cmpl	$30, %eax
	jl	.LBB1_307
# BB#305:
	cmpl	$0, 2116(%rsi)
	je	.LBB1_306
.LBB1_307:
	addl	$-21, %eax
	cmpl	$21, %eax
	jb	.LBB1_311
# BB#308:
	cmpl	$0, 4704(%rsi)
	jg	.LBB1_310
# BB#309:
	cmpl	$0, 4708(%rsi)
	jle	.LBB1_311
.LBB1_310:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.330, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
.LBB1_311:                              # %PatchInp.exit
	cmpl	$0, 5108(%rsi)
	je	.LBB1_322
# BB#312:
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movq	Map(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB1_321
# BB#313:                               # %.lr.ph.i83.preheader
	movl	$Map+8, %ebx
	.p2align	4, 0x90
.LBB1_314:                              # %.lr.ph.i83
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB1_319
# BB#315:                               # %.lr.ph.i83
                                        #   in Loop: Header=BB1_314 Depth=1
	cmpl	$1, %eax
	je	.LBB1_318
# BB#316:                               # %.lr.ph.i83
                                        #   in Loop: Header=BB1_314 Depth=1
	testl	%eax, %eax
	jne	.LBB1_320
# BB#317:                               #   in Loop: Header=BB1_314 Depth=1
	movq	(%rbx), %rax
	movl	(%rax), %edx
	movl	$.L.str.252, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB1_320
	.p2align	4, 0x90
.LBB1_319:                              #   in Loop: Header=BB1_314 Depth=1
	movq	(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.254, %edi
	movb	$1, %al
	callq	printf
	jmp	.LBB1_320
	.p2align	4, 0x90
.LBB1_318:                              #   in Loop: Header=BB1_314 Depth=1
	movq	(%rbx), %rdx
	movl	$.L.str.253, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_320:                              #   in Loop: Header=BB1_314 Depth=1
	movq	48(%rbx), %rsi
	addq	$56, %rbx
	testq	%rsi, %rsi
	jne	.LBB1_314
.LBB1_321:                              # %DisplayEncoderParams.exit
	movl	$.Lstr.4, %edi
	callq	puts
.LBB1_322:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_266:
	cmpl	$144, %eax
	je	.LBB1_269
# BB#267:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.315, %edx
	movl	$100, %ecx
	movl	$144, %r8d
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	input(%rip), %rsi
	cmpl	$0, 4016(%rsi)
	jne	.LBB1_270
	jmp	.LBB1_272
.LBB1_119:                              # %._crit_edge141.i
	movq	stderr(%rip), %rdi
	addl	56(%r12), %edx
	addl	%esi, %ecx
	movl	$.L.str.265, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %r12
	cmpl	$1, 264(%r12)
	je	.LBB1_121
	jmp	.LBB1_124
.LBB1_264:
	movq	stderr(%rip), %rcx
	movl	$.L.str.308, %edi
	movl	$59, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.309, %edi
	movl	$61, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.310, %edi
	movl	$58, %esi
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rsi
	jmp	.LBB1_265
.LBB1_280:
	movq	stderr(%rip), %rcx
	movl	$.L.str.319, %edi
	movl	$84, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.320, %edi
	movl	$117, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.319, %edi
	movl	$84, %esi
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rsi
	movl	$0, 4176(%rsi)
	cmpl	$66, (%rsi)
	je	.LBB1_282
	jmp	.LBB1_294
.LBB1_260:
	movq	stderr(%rip), %rcx
	movl	$.L.str.305, %edi
	movl	$82, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.306, %edi
	movl	$55, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.307, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rsi
	cmpl	$0, 2968(%rsi)
	jne	.LBB1_263
	jmp	.LBB1_262
.LBB1_306:
	movq	stderr(%rip), %rcx
	movl	$.L.str.329, %edi
	movl	$106, %esi
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rsi
	movl	$1, 2116(%rsi)
	movl	4(%rsi), %eax
	jmp	.LBB1_307
.LBB1_123:
	movq	stderr(%rip), %rcx
	movl	$.L.str.266, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movq	input(%rip), %rax
	movl	268(%rax), %ecx
	cmpl	$1, %ecx
	movl	$-1, %esi
	movl	$1, %edx
	cmovgl	%esi, %edx
	addl	%ecx, %edx
	movl	%edx, 268(%rax)
	movq	stderr(%rip), %rdi
	movl	$.L.str.267, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %r12
	cmpl	$0, 5032(%r12)
	jne	.LBB1_125
	jmp	.LBB1_156
.LBB1_25:                               # %.us-lcssa.us
	callq	JMHelpExit
.LBB1_323:
	callq	JMHelpExit
.Lfunc_end1:
	.size	Configure, .Lfunc_end1-Configure
	.cfi_endproc

	.globl	GetConfigFileContent
	.p2align	4, 0x90
	.type	GetConfigFileContent,@function
GetConfigFileContent:                   # @GetConfigFileContent
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -40
.Lcfi20:
	.cfi_offset %r12, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$.L.str.237, %esi
	callq	fopen64
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_1
# BB#3:
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	fseek
	testl	%eax, %eax
	je	.LBB2_5
.LBB2_4:
	xorl	%r14d, %r14d
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.239, %edx
	jmp	.LBB2_2
.LBB2_1:
	xorl	%r14d, %r14d
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.238, %edx
.LBB2_2:
	xorl	%eax, %eax
	movq	%r15, %rcx
	callq	snprintf
.LBB2_11:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB2_5:
	movq	%rbx, %rdi
	callq	ftell
	movq	%rax, %r12
	cmpq	$60001, %r12            # imm = 0xEA61
	jb	.LBB2_7
# BB#6:
	xorl	%r14d, %r14d
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.240, %edx
	xorl	%eax, %eax
	movq	%r12, %rcx
	movq	%r15, %r8
	callq	snprintf
	jmp	.LBB2_11
.LBB2_7:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	testl	%eax, %eax
	jne	.LBB2_4
# BB#8:
	movq	%r12, %rdi
	incq	%rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB2_10
# BB#9:
	movl	$.L.str.241, %edi
	callq	no_mem_exit
.LBB2_10:
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	fread
	movb	$0, (%r14,%rax)
	movq	%rbx, %rdi
	callq	fclose
	jmp	.LBB2_11
.Lfunc_end2:
	.size	GetConfigFileContent, .Lfunc_end2-GetConfigFileContent
	.cfi_endproc

	.p2align	4, 0x90
	.type	ParseContent,@function
ParseContent:                           # @ParseContent
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$80024, %rsp            # imm = 0x13898
.Lcfi29:
	.cfi_def_cfa_offset 80080
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	testl	%esi, %esi
	jle	.LBB3_49
# BB#1:                                 # %.lr.ph87.lr.ph.preheader
	movslq	%esi, %rcx
	addq	%rdi, %rcx
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	xorl	%edx, %edx
.LBB3_2:                                # %.lr.ph87.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
                                        #     Child Loop BB3_8 Depth 2
                                        #     Child Loop BB3_19 Depth 2
	testl	%edx, %edx
	je	.LBB3_8
	.p2align	4, 0x90
.LBB3_3:                                #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rdi), %esi
	addl	$-9, %esi
	cmpl	$26, %esi
	ja	.LBB3_6
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=2
	jmpq	*.LJTI3_0(,%rsi,8)
.LBB3_5:                                # %.critedge.backedge
                                        #   in Loop: Header=BB3_3 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB3_3
	jmp	.LBB3_24
.LBB3_6:                                # %.us-lcssa
                                        #   in Loop: Header=BB3_3 Depth=2
	testl	%ebp, %ebp
	jne	.LBB3_5
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=2
	movslq	%eax, %rsi
	incl	%eax
	movq	%rdi, 16(%rsp,%rsi,8)
	movl	$-1, %ebp
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_8:                                #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rdi), %esi
	addl	$-9, %esi
	cmpl	$26, %esi
	ja	.LBB3_11
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=2
	jmpq	*.LJTI3_1(,%rsi,8)
.LBB3_10:                               # %.critedge.outer67.us
                                        #   in Loop: Header=BB3_8 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB3_8
	jmp	.LBB3_24
.LBB3_11:                               # %.us-lcssa.us.us
                                        #   in Loop: Header=BB3_8 Depth=2
	testl	%ebp, %ebp
	jne	.LBB3_10
# BB#12:                                #   in Loop: Header=BB3_8 Depth=2
	movslq	%eax, %rsi
	incl	%eax
	movq	%rdi, 16(%rsp,%rsi,8)
	movl	$-1, %ebp
	jmp	.LBB3_10
.LBB3_13:                               # %.us-lcssa92.us.us-lcssa.us
                                        #   in Loop: Header=BB3_2 Depth=1
	movb	$0, (%rdi)
	incq	%rdi
	xorl	%edx, %edx
	jmp	.LBB3_22
.LBB3_14:                               # %.us-lcssa91.us
                                        #   in Loop: Header=BB3_2 Depth=1
	movb	$0, (%rdi)
	incq	%rdi
	xorl	%esi, %esi
	testl	%edx, %edx
	jne	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_2 Depth=1
	movslq	%eax, %rsi
	incl	%eax
	movq	%rdi, 16(%rsp,%rsi,8)
	notl	%ebp
	movl	%ebp, %esi
.LBB3_16:                               #   in Loop: Header=BB3_2 Depth=1
	notl	%edx
	movl	%esi, %ebp
	cmpq	%rcx, %rdi
	jb	.LBB3_2
	jmp	.LBB3_24
.LBB3_17:                               # %.us-lcssa89.us
                                        #   in Loop: Header=BB3_2 Depth=1
	movb	$0, (%rdi)
	xorl	%edx, %edx
	cmpq	%rcx, %rdi
	jae	.LBB3_22
# BB#18:                                # %._crit_edge146.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	incq	%rdi
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB3_19:                               # %._crit_edge146
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rsi), %rdi
	cmpq	%rcx, %rsi
	jae	.LBB3_21
# BB#20:                                # %._crit_edge146
                                        #   in Loop: Header=BB3_19 Depth=2
	cmpb	$10, (%rsi)
	movq	%rdi, %rsi
	jne	.LBB3_19
.LBB3_21:                               # %.critedge.outer.backedge.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	decq	%rdi
	.p2align	4, 0x90
.LBB3_22:                               # %.critedge.outer.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	xorl	%ebp, %ebp
	cmpq	%rcx, %rdi
	jb	.LBB3_2
.LBB3_24:                               # %.critedge.outer67._crit_edge
	cmpl	$2, %eax
	jl	.LBB3_49
# BB#25:                                # %.lr.ph
	decl	%eax
	movslq	%eax, %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_26:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_28 Depth 2
	movq	16(%rsp,%r14,8), %r15
	movq	Map(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_31
# BB#27:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_26 Depth=1
	movl	$Map+56, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_28:                               # %.lr.ph.i
                                        #   Parent Loop BB3_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_32
# BB#29:                                #   in Loop: Header=BB3_28 Depth=2
	incq	%rbp
	movq	(%rbx), %rdi
	addq	$56, %rbx
	testq	%rdi, %rdi
	jne	.LBB3_28
.LBB3_31:                               #   in Loop: Header=BB3_26 Depth=1
	movq	$-1, %rbp
.LBB3_33:                               # %ParameterNameToMapIndex.exit.thread
                                        #   in Loop: Header=BB3_26 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.242, %edx
	xorl	%eax, %eax
	movq	%r15, %rcx
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB3_34:                               #   in Loop: Header=BB3_26 Depth=1
	movq	24(%rsp,%r14,8), %rsi
	movl	$.L.str.243, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_36
# BB#35:                                #   in Loop: Header=BB3_26 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.244, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB3_36:                               #   in Loop: Header=BB3_26 Depth=1
	movslq	%ebp, %rax
	imulq	$56, %rax, %rbp
	movl	Map+16(%rbp), %eax
	cmpl	$2, %eax
	je	.LBB3_42
# BB#37:                                #   in Loop: Header=BB3_26 Depth=1
	cmpl	$1, %eax
	je	.LBB3_45
# BB#38:                                #   in Loop: Header=BB3_26 Depth=1
	testl	%eax, %eax
	jne	.LBB3_47
# BB#39:                                #   in Loop: Header=BB3_26 Depth=1
	movq	32(%rsp,%r14,8), %r12
	movl	$.L.str.245, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	4(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	je	.LBB3_41
# BB#40:                                #   in Loop: Header=BB3_26 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.246, %edx
	xorl	%eax, %eax
	movq	%r15, %rcx
	movq	%r12, %r8
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB3_41:                               #   in Loop: Header=BB3_26 Depth=1
	movl	4(%rsp), %eax
	movq	Map+8(%rbp), %rcx
	movl	%eax, (%rcx)
	jmp	.LBB3_46
	.p2align	4, 0x90
.LBB3_32:                               # %ParameterNameToMapIndex.exit
                                        #   in Loop: Header=BB3_26 Depth=1
	testl	%ebp, %ebp
	jns	.LBB3_34
	jmp	.LBB3_33
	.p2align	4, 0x90
.LBB3_42:                               #   in Loop: Header=BB3_26 Depth=1
	movq	32(%rsp,%r14,8), %r12
	movl	$.L.str.248, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	8(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	je	.LBB3_44
# BB#43:                                #   in Loop: Header=BB3_26 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.246, %edx
	xorl	%eax, %eax
	movq	%r15, %rcx
	movq	%r12, %r8
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB3_44:                               #   in Loop: Header=BB3_26 Depth=1
	movq	8(%rsp), %rax
	movq	Map+8(%rbp), %rcx
	movq	%rax, (%rcx)
	jmp	.LBB3_46
	.p2align	4, 0x90
.LBB3_45:                               #   in Loop: Header=BB3_26 Depth=1
	movq	Map+8(%rbp), %rdi
	movq	32(%rsp,%r14,8), %rsi
	movl	$256, %edx              # imm = 0x100
	callq	strncpy
.LBB3_46:                               #   in Loop: Header=BB3_26 Depth=1
	movl	$46, %edi
	callq	putchar
	jmp	.LBB3_48
	.p2align	4, 0x90
.LBB3_47:                               #   in Loop: Header=BB3_26 Depth=1
	movl	$.L.str.249, %edi
	movl	$-1, %esi
	callq	error
.LBB3_48:                               #   in Loop: Header=BB3_26 Depth=1
	addq	$3, %r14
	cmpq	%r13, %r14
	jl	.LBB3_26
.LBB3_49:                               # %._crit_edge
	movq	input(%rip), %rdi
	movl	$configinput, %esi
	movl	$5800, %edx             # imm = 0x16A8
	callq	memcpy
	addq	$80024, %rsp            # imm = 0x13898
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	ParseContent, .Lfunc_end3-ParseContent
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_5
	.quad	.LBB3_13
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_5
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_6
	.quad	.LBB3_5
	.quad	.LBB3_6
	.quad	.LBB3_14
	.quad	.LBB3_17
.LJTI3_1:
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_10
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_13
	.quad	.LBB3_11
	.quad	.LBB3_14
	.quad	.LBB3_17

	.text
	.globl	CeilLog2
	.p2align	4, 0x90
	.type	CeilLog2,@function
CeilLog2:                               # @CeilLog2
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	decl	%edi
	je	.LBB4_2
	.p2align	4, 0x90
.LBB4_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	shrl	%edi
	incl	%eax
	testl	%edi, %edi
	jne	.LBB4_1
.LBB4_2:                                # %._crit_edge
	retq
.Lfunc_end4:
	.size	CeilLog2, .Lfunc_end4-CeilLog2
	.cfi_endproc

	.globl	PatchInputNoFrames
	.p2align	4, 0x90
	.type	PatchInputNoFrames,@function
PatchInputNoFrames:                     # @PatchInputNoFrames
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rcx
	movl	8(%rcx), %eax
	decl	%eax
	movl	4736(%rcx), %edx
	incl	%edx
	imull	%edx, %eax
	incl	%eax
	movl	%eax, 8(%rcx)
	movl	4740(%rcx), %esi
	testl	%esi, %esi
	je	.LBB5_2
# BB#1:
	decl	%esi
	imull	%esi, %edx
	incl	%edx
	movl	%edx, 4740(%rcx)
.LBB5_2:
	movl	%eax, FirstFrameIn2ndIGOP(%rip)
	retq
.Lfunc_end5:
	.size	PatchInputNoFrames, .Lfunc_end5-PatchInputNoFrames
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ProfileIDC"
	.size	.L.str, 11

	.type	configinput,@object     # @configinput
	.comm	configinput,5800,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"LevelIDC"
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"FrameRate"
	.size	.L.str.2, 10

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"IDRIntraEnable"
	.size	.L.str.3, 15

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"ResendSPS"
	.size	.L.str.4, 10

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"StartFrame"
	.size	.L.str.5, 11

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"IntraPeriod"
	.size	.L.str.6, 12

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"EnableOpenGOP"
	.size	.L.str.7, 14

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"FramesToBeEncoded"
	.size	.L.str.8, 18

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"QPISlice"
	.size	.L.str.9, 9

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"QPPSlice"
	.size	.L.str.10, 9

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"QPBSlice"
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"FrameSkip"
	.size	.L.str.12, 10

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"DisableSubpelME"
	.size	.L.str.13, 16

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SearchRange"
	.size	.L.str.14, 12

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"NumberReferenceFrames"
	.size	.L.str.15, 22

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"PList0References"
	.size	.L.str.16, 17

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"BList0References"
	.size	.L.str.17, 17

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"BList1References"
	.size	.L.str.18, 17

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Log2MaxFNumMinus4"
	.size	.L.str.19, 18

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Log2MaxPOCLsbMinus4"
	.size	.L.str.20, 20

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"GenerateMultiplePPS"
	.size	.L.str.21, 20

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Generate_SEIVUI"
	.size	.L.str.22, 16

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"SEIMessageText"
	.size	.L.str.23, 15

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"ResendPPS"
	.size	.L.str.24, 10

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"SourceWidth"
	.size	.L.str.25, 12

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"SourceHeight"
	.size	.L.str.26, 13

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"MbLineIntraUpdate"
	.size	.L.str.27, 18

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"SliceMode"
	.size	.L.str.28, 10

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"SliceArgument"
	.size	.L.str.29, 14

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"UseConstrainedIntraPred"
	.size	.L.str.30, 24

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"InputFile"
	.size	.L.str.31, 10

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"InputHeaderLength"
	.size	.L.str.32, 18

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"OutputFile"
	.size	.L.str.33, 11

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"ReconFile"
	.size	.L.str.34, 10

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"TraceFile"
	.size	.L.str.35, 10

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"DisposableP"
	.size	.L.str.36, 12

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"DispPQPOffset"
	.size	.L.str.37, 14

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"NumberBFrames"
	.size	.L.str.38, 14

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"PReplaceBSlice"
	.size	.L.str.39, 15

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"BRefPicQPOffset"
	.size	.L.str.40, 16

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"DirectModeType"
	.size	.L.str.41, 15

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"DirectInferenceFlag"
	.size	.L.str.42, 20

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"SPPicturePeriodicity"
	.size	.L.str.43, 21

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"QPSPSlice"
	.size	.L.str.44, 10

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"QPSP2Slice"
	.size	.L.str.45, 11

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"SI_FRAMES"
	.size	.L.str.46, 10

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"SP_output"
	.size	.L.str.47, 10

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"SP_output_name"
	.size	.L.str.48, 15

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"SP2_FRAMES"
	.size	.L.str.49, 11

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"SP2_input_name1"
	.size	.L.str.50, 16

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"SP2_input_name2"
	.size	.L.str.51, 16

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"SymbolMode"
	.size	.L.str.52, 11

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"OutFileMode"
	.size	.L.str.53, 12

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"PartitionMode"
	.size	.L.str.54, 14

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"InterSearch16x16"
	.size	.L.str.55, 17

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"InterSearch16x8"
	.size	.L.str.56, 16

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"InterSearch8x16"
	.size	.L.str.57, 16

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"InterSearch8x8"
	.size	.L.str.58, 15

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"InterSearch8x4"
	.size	.L.str.59, 15

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"InterSearch4x8"
	.size	.L.str.60, 15

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"InterSearch4x4"
	.size	.L.str.61, 15

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"IntraDisableInterOnly"
	.size	.L.str.62, 22

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"Intra4x4ParDisable"
	.size	.L.str.63, 19

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"Intra4x4DiagDisable"
	.size	.L.str.64, 20

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"Intra4x4DirDisable"
	.size	.L.str.65, 19

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"Intra16x16ParDisable"
	.size	.L.str.66, 21

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"Intra16x16PlaneDisable"
	.size	.L.str.67, 23

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"EnableIPCM"
	.size	.L.str.68, 11

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"ChromaIntraDisable"
	.size	.L.str.69, 19

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"RestrictSearchRange"
	.size	.L.str.70, 20

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"LastFrameNumber"
	.size	.L.str.71, 16

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"ChangeQPI"
	.size	.L.str.72, 10

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"ChangeQPP"
	.size	.L.str.73, 10

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"ChangeQPB"
	.size	.L.str.74, 10

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"ChangeQPBSRefOffset"
	.size	.L.str.75, 20

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"ChangeQPStart"
	.size	.L.str.76, 14

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"RDOptimization"
	.size	.L.str.77, 15

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"CtxAdptLagrangeMult"
	.size	.L.str.78, 20

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"FastCrIntraDecision"
	.size	.L.str.79, 20

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"DisableThresholding"
	.size	.L.str.80, 20

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"DisableBSkipRDO"
	.size	.L.str.81, 16

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"LossRateA"
	.size	.L.str.82, 10

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"LossRateB"
	.size	.L.str.83, 10

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"LossRateC"
	.size	.L.str.84, 10

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"NumberOfDecoders"
	.size	.L.str.85, 17

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"RestrictRefFrames"
	.size	.L.str.86, 18

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"NumberofLeakyBuckets"
	.size	.L.str.87, 21

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"LeakyBucketRateFile"
	.size	.L.str.88, 20

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"LeakyBucketParamFile"
	.size	.L.str.89, 21

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"PicInterlace"
	.size	.L.str.90, 13

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"MbInterlace"
	.size	.L.str.91, 12

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"IntraBottom"
	.size	.L.str.92, 12

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"NumberFramesInEnhancementLayerSubSequence"
	.size	.L.str.93, 42

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"NumberOfFrameInSecondIGOP"
	.size	.L.str.94, 26

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"RandomIntraMBRefresh"
	.size	.L.str.95, 21

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"WeightedPrediction"
	.size	.L.str.96, 19

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"WeightedBiprediction"
	.size	.L.str.97, 21

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"UseWeightedReferenceME"
	.size	.L.str.98, 23

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"RDPictureDecision"
	.size	.L.str.99, 18

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"RDPictureIntra"
	.size	.L.str.100, 15

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"RDPSliceWeightOnly"
	.size	.L.str.101, 19

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"RDPSliceBTest"
	.size	.L.str.102, 14

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"RDBSliceWeightOnly"
	.size	.L.str.103, 19

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"SkipIntraInInterSlices"
	.size	.L.str.104, 23

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"BReferencePictures"
	.size	.L.str.105, 19

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"HierarchicalCoding"
	.size	.L.str.106, 19

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"HierarchyLevelQPEnable"
	.size	.L.str.107, 23

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"ExplicitHierarchyFormat"
	.size	.L.str.108, 24

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"ReferenceReorder"
	.size	.L.str.109, 17

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"PocMemoryManagement"
	.size	.L.str.110, 20

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"BiPredMotionEstimation"
	.size	.L.str.111, 23

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"BiPredMERefinements"
	.size	.L.str.112, 20

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"BiPredMESearchRange"
	.size	.L.str.113, 20

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"BiPredMESubPel"
	.size	.L.str.114, 15

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"LoopFilterParametersFlag"
	.size	.L.str.115, 25

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"LoopFilterDisable"
	.size	.L.str.116, 18

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"LoopFilterAlphaC0Offset"
	.size	.L.str.117, 24

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"LoopFilterBetaOffset"
	.size	.L.str.118, 21

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"SparePictureOption"
	.size	.L.str.119, 19

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"SparePictureDetectionThr"
	.size	.L.str.120, 25

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"SparePicturePercentageThr"
	.size	.L.str.121, 26

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"num_slice_groups_minus1"
	.size	.L.str.122, 24

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"slice_group_map_type"
	.size	.L.str.123, 21

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"slice_group_change_direction_flag"
	.size	.L.str.124, 34

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"slice_group_change_rate_minus1"
	.size	.L.str.125, 31

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"SliceGroupConfigFileName"
	.size	.L.str.126, 25

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"UseRedundantPicture"
	.size	.L.str.127, 20

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"NumRedundantHierarchy"
	.size	.L.str.128, 22

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"PrimaryGOPLength"
	.size	.L.str.129, 17

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"NumRefPrimary"
	.size	.L.str.130, 14

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"PicOrderCntType"
	.size	.L.str.131, 16

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"ContextInitMethod"
	.size	.L.str.132, 18

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"FixedModelNumber"
	.size	.L.str.133, 17

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"Transform8x8Mode"
	.size	.L.str.134, 17

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"ReportFrameStats"
	.size	.L.str.135, 17

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"DisplayEncParams"
	.size	.L.str.136, 17

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"Verbose"
	.size	.L.str.137, 8

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"RateControlEnable"
	.size	.L.str.138, 18

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"Bitrate"
	.size	.L.str.139, 8

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"InitialQP"
	.size	.L.str.140, 10

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"BasicUnit"
	.size	.L.str.141, 10

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"ChannelType"
	.size	.L.str.142, 12

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"RCUpdateMode"
	.size	.L.str.143, 13

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"RCISliceBitRatio"
	.size	.L.str.144, 17

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"RCBSliceBitRatio0"
	.size	.L.str.145, 18

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"RCBSliceBitRatio1"
	.size	.L.str.146, 18

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"RCBSliceBitRatio2"
	.size	.L.str.147, 18

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"RCBSliceBitRatio3"
	.size	.L.str.148, 18

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"RCBSliceBitRatio4"
	.size	.L.str.149, 18

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"RCBoverPRatio"
	.size	.L.str.150, 14

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"RCIoverPRatio"
	.size	.L.str.151, 14

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"QmatrixFile"
	.size	.L.str.152, 12

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"ScalingMatrixPresentFlag"
	.size	.L.str.153, 25

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"ScalingListPresentFlag0"
	.size	.L.str.154, 24

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"ScalingListPresentFlag1"
	.size	.L.str.155, 24

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"ScalingListPresentFlag2"
	.size	.L.str.156, 24

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"ScalingListPresentFlag3"
	.size	.L.str.157, 24

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"ScalingListPresentFlag4"
	.size	.L.str.158, 24

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"ScalingListPresentFlag5"
	.size	.L.str.159, 24

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"ScalingListPresentFlag6"
	.size	.L.str.160, 24

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"ScalingListPresentFlag7"
	.size	.L.str.161, 24

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"SearchMode"
	.size	.L.str.162, 11

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"UMHexDSR"
	.size	.L.str.163, 9

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"UMHexScale"
	.size	.L.str.164, 11

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"EPZSPattern"
	.size	.L.str.165, 12

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"EPZSDualRefinement"
	.size	.L.str.166, 19

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"EPZSFixedPredictors"
	.size	.L.str.167, 20

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"EPZSTemporal"
	.size	.L.str.168, 13

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"EPZSSpatialMem"
	.size	.L.str.169, 15

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"EPZSMinThresScale"
	.size	.L.str.170, 18

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"EPZSMaxThresScale"
	.size	.L.str.171, 18

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"EPZSMedThresScale"
	.size	.L.str.172, 18

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"EPZSSubPelME"
	.size	.L.str.173, 13

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"EPZSSubPelMEBiPred"
	.size	.L.str.174, 19

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"EPZSSubPelGrid"
	.size	.L.str.175, 15

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"EPZSSubPelThresScale"
	.size	.L.str.176, 21

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"ChromaQPOffset"
	.size	.L.str.177, 15

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"BitDepthLuma"
	.size	.L.str.178, 13

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"BitDepthChroma"
	.size	.L.str.179, 15

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"YUVFormat"
	.size	.L.str.180, 10

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"RGBInput"
	.size	.L.str.181, 9

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"CbQPOffset"
	.size	.L.str.182, 11

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"CrQPOffset"
	.size	.L.str.183, 11

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"QPPrimeYZeroTransformBypassFlag"
	.size	.L.str.184, 32

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	"UseExplicitLambdaParams"
	.size	.L.str.185, 24

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"FixedLambdaPslice"
	.size	.L.str.186, 18

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"FixedLambdaBslice"
	.size	.L.str.187, 18

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"FixedLambdaIslice"
	.size	.L.str.188, 18

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"FixedLambdaSPslice"
	.size	.L.str.189, 19

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"FixedLambdaSIslice"
	.size	.L.str.190, 19

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"FixedLambdaRefBslice"
	.size	.L.str.191, 21

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"LambdaWeightPslice"
	.size	.L.str.192, 19

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"LambdaWeightBslice"
	.size	.L.str.193, 19

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	"LambdaWeightIslice"
	.size	.L.str.194, 19

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"LambdaWeightSPslice"
	.size	.L.str.195, 20

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"LambdaWeightSIslice"
	.size	.L.str.196, 20

	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	"LambdaWeightRefBslice"
	.size	.L.str.197, 22

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"QOffsetMatrixFile"
	.size	.L.str.198, 18

	.type	.L.str.199,@object      # @.str.199
.L.str.199:
	.asciz	"OffsetMatrixPresentFlag"
	.size	.L.str.199, 24

	.type	.L.str.200,@object      # @.str.200
.L.str.200:
	.asciz	"EarlySkipEnable"
	.size	.L.str.200, 16

	.type	.L.str.201,@object      # @.str.201
.L.str.201:
	.asciz	"SelectiveIntraEnable"
	.size	.L.str.201, 21

	.type	.L.str.202,@object      # @.str.202
.L.str.202:
	.asciz	"AdaptiveRounding"
	.size	.L.str.202, 17

	.type	.L.str.203,@object      # @.str.203
.L.str.203:
	.asciz	"AdaptRndPeriod"
	.size	.L.str.203, 15

	.type	.L.str.204,@object      # @.str.204
.L.str.204:
	.asciz	"AdaptRndChroma"
	.size	.L.str.204, 15

	.type	.L.str.205,@object      # @.str.205
.L.str.205:
	.asciz	"AdaptRndWFactorIRef"
	.size	.L.str.205, 20

	.type	.L.str.206,@object      # @.str.206
.L.str.206:
	.asciz	"AdaptRndWFactorPRef"
	.size	.L.str.206, 20

	.type	.L.str.207,@object      # @.str.207
.L.str.207:
	.asciz	"AdaptRndWFactorBRef"
	.size	.L.str.207, 20

	.type	.L.str.208,@object      # @.str.208
.L.str.208:
	.asciz	"AdaptRndWFactorINRef"
	.size	.L.str.208, 21

	.type	.L.str.209,@object      # @.str.209
.L.str.209:
	.asciz	"AdaptRndWFactorPNRef"
	.size	.L.str.209, 21

	.type	.L.str.210,@object      # @.str.210
.L.str.210:
	.asciz	"AdaptRndWFactorBNRef"
	.size	.L.str.210, 21

	.type	.L.str.211,@object      # @.str.211
.L.str.211:
	.asciz	"AdaptRndCrWFactorIRef"
	.size	.L.str.211, 22

	.type	.L.str.212,@object      # @.str.212
.L.str.212:
	.asciz	"AdaptRndCrWFactorPRef"
	.size	.L.str.212, 22

	.type	.L.str.213,@object      # @.str.213
.L.str.213:
	.asciz	"AdaptRndCrWFactorBRef"
	.size	.L.str.213, 22

	.type	.L.str.214,@object      # @.str.214
.L.str.214:
	.asciz	"AdaptRndCrWFactorINRef"
	.size	.L.str.214, 23

	.type	.L.str.215,@object      # @.str.215
.L.str.215:
	.asciz	"AdaptRndCrWFactorPNRef"
	.size	.L.str.215, 23

	.type	.L.str.216,@object      # @.str.216
.L.str.216:
	.asciz	"AdaptRndCrWFactorBNRef"
	.size	.L.str.216, 23

	.type	.L.str.217,@object      # @.str.217
.L.str.217:
	.asciz	"VUISupport"
	.size	.L.str.217, 11

	.type	.L.str.218,@object      # @.str.218
.L.str.218:
	.asciz	"ChromaMCBuffer"
	.size	.L.str.218, 15

	.type	.L.str.219,@object      # @.str.219
.L.str.219:
	.asciz	"ChromaMEEnable"
	.size	.L.str.219, 15

	.type	.L.str.220,@object      # @.str.220
.L.str.220:
	.asciz	"MEDistortionFPel"
	.size	.L.str.220, 17

	.type	.L.str.221,@object      # @.str.221
.L.str.221:
	.asciz	"MEDistortionHPel"
	.size	.L.str.221, 17

	.type	.L.str.222,@object      # @.str.222
.L.str.222:
	.asciz	"MEDistortionQPel"
	.size	.L.str.222, 17

	.type	.L.str.223,@object      # @.str.223
.L.str.223:
	.asciz	"MDDistortion"
	.size	.L.str.223, 13

	.type	Map,@object             # @Map
	.data
	.globl	Map
	.p2align	4
Map:
	.quad	.L.str
	.quad	configinput
	.long	0                       # 0x0
	.zero	4
	.quad	4635892866424504320     # double 88
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.1
	.quad	configinput+4
	.long	0                       # 0x0
	.zero	4
	.quad	4626604192193052672     # double 21
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.2
	.quad	configinput+4080
	.long	2                       # 0x2
	.zero	4
	.quad	4629137466983448576     # double 30
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4636737291354636288     # double 100
	.quad	.L.str.3
	.quad	configinput+1568
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.4
	.quad	configinput+2088
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.5
	.quad	configinput+1572
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.6
	.quad	configinput+1560
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.7
	.quad	configinput+1564
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.8
	.quad	configinput+8
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	2                       # 0x2
	.zero	4
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.quad	.L.str.9
	.quad	configinput+12
	.long	0                       # 0x0
	.zero	4
	.quad	4627448617123184640     # double 24
	.long	3                       # 0x3
	.zero	4
	.quad	0                       # double 0
	.quad	4632374429215621120     # double 51
	.quad	.L.str.10
	.quad	configinput+16
	.long	0                       # 0x0
	.zero	4
	.quad	4627448617123184640     # double 24
	.long	3                       # 0x3
	.zero	4
	.quad	0                       # double 0
	.quad	4632374429215621120     # double 51
	.quad	.L.str.11
	.quad	configinput+2104
	.long	0                       # 0x0
	.zero	4
	.quad	4627448617123184640     # double 24
	.long	3                       # 0x3
	.zero	4
	.quad	0                       # double 0
	.quad	4632374429215621120     # double 51
	.quad	.L.str.12
	.quad	configinput+20
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.13
	.quad	configinput+24
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.14
	.quad	configinput+28
	.long	0                       # 0x0
	.zero	4
	.quad	4625196817309499392     # double 16
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.15
	.quad	configinput+32
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	4607182418800017408     # double 1
	.quad	4625196817309499392     # double 16
	.quad	.L.str.16
	.quad	configinput+36
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4625196817309499392     # double 16
	.quad	.L.str.17
	.quad	configinput+40
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4625196817309499392     # double 16
	.quad	.L.str.18
	.quad	configinput+44
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4625196817309499392     # double 16
	.quad	.L.str.19
	.quad	configinput+48
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	-4616189618054758400    # double -1
	.quad	4622945017495814144     # double 12
	.quad	.L.str.20
	.quad	configinput+52
	.long	0                       # 0x0
	.zero	4
	.quad	4611686018427387904     # double 2
	.long	1                       # 0x1
	.zero	4
	.quad	-4616189618054758400    # double -1
	.quad	4622945017495814144     # double 12
	.quad	.L.str.21
	.quad	configinput+1576
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.22
	.quad	configinput+1580
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.23
	.quad	configinput+1584
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.24
	.quad	configinput+2092
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.25
	.quad	configinput+56
	.long	0                       # 0x0
	.zero	4
	.quad	4640396466051874816     # double 176
	.long	2                       # 0x2
	.zero	4
	.quad	4625196817309499392     # double 16
	.quad	0                       # double 0
	.quad	.L.str.26
	.quad	configinput+60
	.long	0                       # 0x0
	.zero	4
	.quad	4639270566145032192     # double 144
	.long	2                       # 0x2
	.zero	4
	.quad	4625196817309499392     # double 16
	.quad	0                       # double 0
	.quad	.L.str.27
	.quad	configinput+68
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.28
	.quad	configinput+264
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.29
	.quad	configinput+268
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	2                       # 0x2
	.zero	4
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	.L.str.30
	.quad	configinput+272
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.31
	.quad	configinput+280
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.32
	.quad	configinput+276
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.33
	.quad	configinput+536
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.34
	.quad	configinput+792
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.35
	.quad	configinput+1048
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.36
	.quad	configinput+5752
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.37
	.quad	configinput+5756
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	-4590997607639154688    # double -51
	.quad	4632374429215621120     # double 51
	.quad	.L.str.38
	.quad	configinput+2096
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.39
	.quad	configinput+2100
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.40
	.quad	configinput+2108
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	-4590997607639154688    # double -51
	.quad	4632374429215621120     # double 51
	.quad	.L.str.41
	.quad	configinput+2112
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.42
	.quad	configinput+2116
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.43
	.quad	configinput+2136
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.44
	.quad	configinput+2140
	.long	0                       # 0x0
	.zero	4
	.quad	4627448617123184640     # double 24
	.long	3                       # 0x3
	.zero	4
	.quad	0                       # double 0
	.quad	4632374429215621120     # double 51
	.quad	.L.str.45
	.quad	configinput+2144
	.long	0                       # 0x0
	.zero	4
	.quad	4627448617123184640     # double 24
	.long	3                       # 0x3
	.zero	4
	.quad	0                       # double 0
	.quad	4632374429215621120     # double 51
	.quad	.L.str.46
	.quad	configinput+2148
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.47
	.quad	configinput+2156
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.48
	.quad	configinput+2160
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.49
	.quad	configinput+2152
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.50
	.quad	configinput+2416
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.51
	.quad	configinput+2672
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.52
	.quad	configinput+4008
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.53
	.quad	configinput+4012
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.54
	.quad	configinput+4016
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.55
	.quad	configinput+4020
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.56
	.quad	configinput+4024
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.57
	.quad	configinput+4028
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.58
	.quad	configinput+4032
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.59
	.quad	configinput+4036
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.60
	.quad	configinput+4040
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.61
	.quad	configinput+4044
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.62
	.quad	configinput+4048
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.63
	.quad	configinput+4052
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.64
	.quad	configinput+4056
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.65
	.quad	configinput+4060
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.66
	.quad	configinput+4064
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.67
	.quad	configinput+4068
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.68
	.quad	configinput+4076
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.69
	.quad	configinput+4072
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.70
	.quad	configinput+4140
	.long	0                       # 0x0
	.zero	4
	.quad	4611686018427387904     # double 2
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.71
	.quad	configinput+4144
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.72
	.quad	configinput+4160
	.long	0                       # 0x0
	.zero	4
	.quad	4627448617123184640     # double 24
	.long	3                       # 0x3
	.zero	4
	.quad	0                       # double 0
	.quad	4632374429215621120     # double 51
	.quad	.L.str.73
	.quad	configinput+4148
	.long	0                       # 0x0
	.zero	4
	.quad	4627448617123184640     # double 24
	.long	3                       # 0x3
	.zero	4
	.quad	0                       # double 0
	.quad	4632374429215621120     # double 51
	.quad	.L.str.74
	.quad	configinput+4152
	.long	0                       # 0x0
	.zero	4
	.quad	4627448617123184640     # double 24
	.long	3                       # 0x3
	.zero	4
	.quad	0                       # double 0
	.quad	4632374429215621120     # double 51
	.quad	.L.str.75
	.quad	configinput+4164
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	-4590997607639154688    # double -51
	.quad	4632374429215621120     # double 51
	.quad	.L.str.76
	.quad	configinput+4156
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.77
	.quad	configinput+4168
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.78
	.quad	configinput+4172
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.79
	.quad	configinput+4176
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.80
	.quad	configinput+4180
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.81
	.quad	configinput+4184
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.82
	.quad	configinput+4716
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.83
	.quad	configinput+4720
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.84
	.quad	configinput+4724
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.85
	.quad	configinput+4728
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.86
	.quad	configinput+4732
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.87
	.quad	configinput+4188
	.long	0                       # 0x0
	.zero	4
	.quad	4611686018427387904     # double 2
	.long	1                       # 0x1
	.zero	4
	.quad	4611686018427387904     # double 2
	.quad	4643176031446892544     # double 255
	.quad	.L.str.88
	.quad	configinput+4192
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.89
	.quad	configinput+4448
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.90
	.quad	configinput+4704
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.91
	.quad	configinput+4708
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.92
	.quad	configinput+4712
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.93
	.quad	configinput+4736
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.94
	.quad	configinput+4740
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.95
	.quad	configinput+4744
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.96
	.quad	configinput+2928
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.97
	.quad	configinput+2932
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.98
	.quad	configinput+2936
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.99
	.quad	configinput+2940
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.100
	.quad	configinput+2944
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.101
	.quad	configinput+2948
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.102
	.quad	configinput+2952
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.103
	.quad	configinput+2956
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.104
	.quad	configinput+2960
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.105
	.quad	configinput+2964
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.106
	.quad	configinput+2968
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.107
	.quad	configinput+2972
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.108
	.quad	configinput+2976
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.109
	.quad	configinput+4000
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.110
	.quad	configinput+4004
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.111
	.quad	configinput+2120
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.112
	.quad	configinput+2124
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4617315517961601024     # double 5
	.quad	.L.str.113
	.quad	configinput+2128
	.long	0                       # 0x0
	.zero	4
	.quad	4620693217682128896     # double 8
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.114
	.quad	configinput+2132
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.115
	.quad	configinput+4748
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.116
	.quad	configinput+4752
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.117
	.quad	configinput+4756
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	-4604930618986332160    # double -6
	.quad	4618441417868443648     # double 6
	.quad	.L.str.118
	.quad	configinput+4760
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	-4604930618986332160    # double -6
	.quad	4618441417868443648     # double 6
	.quad	.L.str.119
	.quad	configinput+4764
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.120
	.quad	configinput+4768
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.121
	.quad	configinput+4772
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	4636737291354636288     # double 100
	.quad	.L.str.122
	.quad	configinput+5032
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4619567317775286272     # double 7
	.quad	.L.str.123
	.quad	configinput+5036
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4618441417868443648     # double 6
	.quad	.L.str.124
	.quad	configinput+5072
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.125
	.quad	configinput+5076
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.126
	.quad	configinput+4776
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.127
	.quad	configinput+5084
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.128
	.quad	configinput+5760
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4616189618054758400     # double 4
	.quad	.L.str.129
	.quad	configinput+5764
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	4607182418800017408     # double 1
	.quad	4625196817309499392     # double 16
	.quad	.L.str.130
	.quad	configinput+5768
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	4607182418800017408     # double 1
	.quad	4625196817309499392     # double 16
	.quad	.L.str.131
	.quad	configinput+5088
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.132
	.quad	configinput+5092
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.133
	.quad	configinput+5096
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.134
	.quad	configinput+5100
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.135
	.quad	configinput+5104
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.136
	.quad	configinput+5108
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.137
	.quad	configinput+5112
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.138
	.quad	configinput+5116
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.139
	.quad	configinput+5120
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.140
	.quad	configinput+5124
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	3                       # 0x3
	.zero	4
	.quad	0                       # double 0
	.quad	4632374429215621120     # double 51
	.quad	.L.str.141
	.quad	configinput+5128
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.142
	.quad	configinput+5132
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.143
	.quad	configinput+5136
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4616189618054758400     # double 4
	.quad	.L.str.144
	.quad	configinput+5160
	.long	2                       # 0x2
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.145
	.quad	configinput+5168
	.long	2                       # 0x2
	.zero	4
	.quad	4602678819172646912     # double 0.5
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.146
	.quad	configinput+5176
	.long	2                       # 0x2
	.zero	4
	.quad	4598175219545276416     # double 0.25
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.147
	.quad	configinput+5184
	.long	2                       # 0x2
	.zero	4
	.quad	4598175219545276416     # double 0.25
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.148
	.quad	configinput+5192
	.long	2                       # 0x2
	.zero	4
	.quad	4598175219545276416     # double 0.25
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.149
	.quad	configinput+5200
	.long	2                       # 0x2
	.zero	4
	.quad	4598175219545276416     # double 0.25
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.150
	.quad	configinput+5152
	.long	2                       # 0x2
	.zero	4
	.quad	4601778099247172813     # double 0.45000000000000001
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4652007308841189376     # double 1000
	.quad	.L.str.151
	.quad	configinput+5144
	.long	2                       # 0x2
	.zero	4
	.quad	4615739258092021350     # double 3.7999999999999998
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4652007308841189376     # double 1000
	.quad	.L.str.152
	.quad	configinput+1304
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.153
	.quad	configinput+5208
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.154
	.quad	configinput+5212
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.155
	.quad	configinput+5216
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.156
	.quad	configinput+5220
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.157
	.quad	configinput+5224
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.158
	.quad	configinput+5228
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.159
	.quad	configinput+5232
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.160
	.quad	configinput+5236
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.161
	.quad	configinput+5240
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.162
	.quad	configinput+5244
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	-4616189618054758400    # double -1
	.quad	4613937818241073152     # double 3
	.quad	.L.str.163
	.quad	configinput+5248
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.164
	.quad	configinput+5252
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.165
	.quad	configinput+4088
	.long	0                       # 0x0
	.zero	4
	.quad	4611686018427387904     # double 2
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4617315517961601024     # double 5
	.quad	.L.str.166
	.quad	configinput+4092
	.long	0                       # 0x0
	.zero	4
	.quad	4613937818241073152     # double 3
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4618441417868443648     # double 6
	.quad	.L.str.167
	.quad	configinput+4096
	.long	0                       # 0x0
	.zero	4
	.quad	4611686018427387904     # double 2
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.168
	.quad	configinput+4100
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.169
	.quad	configinput+4104
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.170
	.quad	configinput+4108
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.171
	.quad	configinput+4112
	.long	0                       # 0x0
	.zero	4
	.quad	4611686018427387904     # double 2
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.172
	.quad	configinput+4116
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.173
	.quad	configinput+4124
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.174
	.quad	configinput+4128
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.175
	.quad	configinput+4120
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.176
	.quad	configinput+4132
	.long	0                       # 0x0
	.zero	4
	.quad	4611686018427387904     # double 2
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.177
	.quad	configinput+4136
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	-4590997607639154688    # double -51
	.quad	4632374429215621120     # double 51
	.quad	.L.str.178
	.quad	configinput+5256
	.long	0                       # 0x0
	.zero	4
	.quad	4620693217682128896     # double 8
	.long	1                       # 0x1
	.zero	4
	.quad	4620693217682128896     # double 8
	.quad	4622945017495814144     # double 12
	.quad	.L.str.179
	.quad	configinput+5260
	.long	0                       # 0x0
	.zero	4
	.quad	4620693217682128896     # double 8
	.long	1                       # 0x1
	.zero	4
	.quad	4620693217682128896     # double 8
	.quad	4622945017495814144     # double 12
	.quad	.L.str.180
	.quad	configinput+64
	.long	0                       # 0x0
	.zero	4
	.quad	4607182418800017408     # double 1
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.181
	.quad	configinput+5272
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.182
	.quad	configinput+5276
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	-4590997607639154688    # double -51
	.quad	4632374429215621120     # double 51
	.quad	.L.str.183
	.quad	configinput+5280
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	-4590997607639154688    # double -51
	.quad	4632374429215621120     # double 51
	.quad	.L.str.184
	.quad	configinput+5284
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.185
	.quad	configinput+5288
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	.L.str.186
	.quad	configinput+5344
	.long	2                       # 0x2
	.zero	4
	.quad	4591870180066957722     # double 0.10000000000000001
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.187
	.quad	configinput+5352
	.long	2                       # 0x2
	.zero	4
	.quad	4591870180066957722     # double 0.10000000000000001
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.188
	.quad	configinput+5360
	.long	2                       # 0x2
	.zero	4
	.quad	4591870180066957722     # double 0.10000000000000001
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.189
	.quad	configinput+5368
	.long	2                       # 0x2
	.zero	4
	.quad	4591870180066957722     # double 0.10000000000000001
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.190
	.quad	configinput+5376
	.long	2                       # 0x2
	.zero	4
	.quad	4591870180066957722     # double 0.10000000000000001
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.191
	.quad	configinput+5384
	.long	2                       # 0x2
	.zero	4
	.quad	4591870180066957722     # double 0.10000000000000001
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.192
	.quad	configinput+5296
	.long	2                       # 0x2
	.zero	4
	.quad	4604300115038500291     # double 0.68000000000000005
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.193
	.quad	configinput+5304
	.long	2                       # 0x2
	.zero	4
	.quad	4611686018427387904     # double 2
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.194
	.quad	configinput+5312
	.long	2                       # 0x2
	.zero	4
	.quad	4604029899060858061     # double 0.65000000000000002
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.195
	.quad	configinput+5320
	.long	2                       # 0x2
	.zero	4
	.quad	4609434218613702656     # double 1.5
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.196
	.quad	configinput+5328
	.long	2                       # 0x2
	.zero	4
	.quad	4604029899060858061     # double 0.65000000000000002
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.197
	.quad	configinput+5336
	.long	2                       # 0x2
	.zero	4
	.quad	4609434218613702656     # double 1.5
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.198
	.quad	configinput+5392
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.199
	.quad	configinput+5648
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.200
	.quad	configinput+5744
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.201
	.quad	configinput+5748
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.202
	.quad	configinput+5652
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.203
	.quad	configinput+5656
	.long	0                       # 0x0
	.zero	4
	.quad	4625196817309499392     # double 16
	.long	2                       # 0x2
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	.L.str.204
	.quad	configinput+5660
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.205
	.quad	configinput+5692
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.206
	.quad	configinput+5684
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.207
	.quad	configinput+5688
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.208
	.quad	configinput+5672
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.209
	.quad	configinput+5664
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.210
	.quad	configinput+5668
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.211
	.quad	configinput+5732
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.212
	.quad	configinput+5724
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.213
	.quad	configinput+5728
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.214
	.quad	configinput+5712
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.215
	.quad	configinput+5704
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.216
	.quad	configinput+5708
	.long	0                       # 0x0
	.zero	4
	.quad	4616189618054758400     # double 4
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4661225614328463360     # double 4096
	.quad	.L.str.217
	.quad	configinput+2084
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.218
	.quad	configinput+5772
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	.L.str.219
	.quad	configinput+5776
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.220
	.quad	configinput+5780
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.221
	.quad	configinput+5784
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.222
	.quad	configinput+5788
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	.L.str.223
	.quad	configinput+5792
	.long	0                       # 0x0
	.zero	4
	.quad	4611686018427387904     # double 2
	.long	1                       # 0x1
	.zero	4
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	0
	.quad	0
	.long	4294967295              # 0xffffffff
	.zero	4
	.quad	0                       # double 0
	.long	0                       # 0x0
	.zero	4
	.quad	0                       # double 0
	.quad	0                       # double 0
	.size	Map, 12600

	.type	.L.str.224,@object      # @.str.224
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.224:
	.asciz	"\n   lencod [-h] [-d defenc.cfg] {[-f curenc1.cfg]...[-f curencN.cfg]} {[-p EncParam1=EncValue1]..[-p EncParamM=EncValueM]}\n\n## Parameters\n\n## Options\n   -h :  prints function usage\n   -d :  use <defenc.cfg> as default file for parameter initializations.\n         If not used then file defaults to encoder.cfg in local directory.\n   -f :  read <curencM.cfg> for reseting selected encoder parameters.\n         Multiple files could be used that set different parameters\n   -p :  Set parameter <EncParamM> to <EncValueM>.\n         See default encoder.cfg file for description of all parameters.\n\n## Supported video file formats\n   RAW:  .yuv -> YUV 4:2:0\n\n## Examples of usage:\n   lencod\n   lencod  -h\n   lencod  -d default.cfg\n   lencod  -f curenc1.cfg\n   lencod  -f curenc1.cfg -p InputFile=\"e:\\data\\container_qcif_30.yuv\" -p SourceWidth=176 -p SourceHeight=144\n   lencod  -f curenc1.cfg -p FramesToBeEncoded=30 -p QPISlice=28 -p QPPSlice=28 -p QPBSlice=30\n"
	.size	.L.str.224, 956

	.type	.L.str.225,@object      # @.str.225
.L.str.225:
	.asciz	"encoder.cfg"
	.size	.L.str.225, 12

	.type	.L.str.227,@object      # @.str.227
.L.str.227:
	.asciz	"-h"
	.size	.L.str.227, 3

	.type	.L.str.228,@object      # @.str.228
.L.str.228:
	.asciz	"-d"
	.size	.L.str.228, 3

	.type	.L.str.229,@object      # @.str.229
.L.str.229:
	.asciz	"Parsing Configfile %s"
	.size	.L.str.229, 22

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.231,@object      # @.str.231
.L.str.231:
	.asciz	"-f"
	.size	.L.str.231, 3

	.type	.L.str.232,@object      # @.str.232
.L.str.232:
	.asciz	"-p"
	.size	.L.str.232, 3

	.type	.L.str.233,@object      # @.str.233
.L.str.233:
	.asciz	"Configure: content"
	.size	.L.str.233, 19

	.type	.L.str.234,@object      # @.str.234
.L.str.234:
	.asciz	"Parsing command line string '%s'"
	.size	.L.str.234, 33

	.type	.L.str.235,@object      # @.str.235
.L.str.235:
	.asciz	"IGNORED"
	.size	.L.str.235, 8

	.type	.L.str.236,@object      # @.str.236
.L.str.236:
	.asciz	"Error in command line, ac %d, around string '%s', missing -f or -p parameters?"
	.size	.L.str.236, 79

	.type	.L.str.237,@object      # @.str.237
.L.str.237:
	.asciz	"r"
	.size	.L.str.237, 2

	.type	.L.str.238,@object      # @.str.238
.L.str.238:
	.asciz	"Cannot open configuration file %s."
	.size	.L.str.238, 35

	.type	.L.str.239,@object      # @.str.239
.L.str.239:
	.asciz	"Cannot fseek in configuration file %s."
	.size	.L.str.239, 39

	.type	.L.str.240,@object      # @.str.240
.L.str.240:
	.asciz	"Unreasonable Filesize %ld reported by ftell for configuration file %s."
	.size	.L.str.240, 71

	.type	.L.str.241,@object      # @.str.241
.L.str.241:
	.asciz	"GetConfigFileContent: buf"
	.size	.L.str.241, 26

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	.L.str.242,@object      # @.str.242
.L.str.242:
	.asciz	" Parsing error in config file: Parameter Name '%s' not recognized."
	.size	.L.str.242, 67

	.type	.L.str.243,@object      # @.str.243
.L.str.243:
	.asciz	"="
	.size	.L.str.243, 2

	.type	.L.str.244,@object      # @.str.244
.L.str.244:
	.asciz	" Parsing error in config file: '=' expected as the second token in each line."
	.size	.L.str.244, 78

	.type	.L.str.245,@object      # @.str.245
.L.str.245:
	.asciz	"%d"
	.size	.L.str.245, 3

	.type	.L.str.246,@object      # @.str.246
.L.str.246:
	.asciz	" Parsing error: Expected numerical value for Parameter of %s, found '%s'."
	.size	.L.str.246, 74

	.type	.L.str.248,@object      # @.str.248
.L.str.248:
	.asciz	"%lf"
	.size	.L.str.248, 4

	.type	.L.str.249,@object      # @.str.249
.L.str.249:
	.asciz	"Unknown value type in the map definition of configfile.h"
	.size	.L.str.249, 57

	.type	.L.str.252,@object      # @.str.252
.L.str.252:
	.asciz	"Parameter %s = %d\n"
	.size	.L.str.252, 19

	.type	.L.str.253,@object      # @.str.253
.L.str.253:
	.asciz	"Parameter %s = %s\n"
	.size	.L.str.253, 19

	.type	.L.str.254,@object      # @.str.254
.L.str.254:
	.asciz	"Parameter %s = %.2f\n"
	.size	.L.str.254, 21

	.type	.L.str.255,@object      # @.str.255
.L.str.255:
	.asciz	" NumberReferenceFrames=%d and Log2MaxFNumMinus4=%d may lead to an invalid value of frame_num."
	.size	.L.str.255, 94

	.type	.L.str.256,@object      # @.str.256
.L.str.256:
	.asciz	"log2_max_pic_order_cnt_lsb_minus4 might not be sufficient for encoding. Increase value."
	.size	.L.str.256, 88

	.type	.L.str.257,@object      # @.str.257
.L.str.257:
	.asciz	"Number of B-frames %d can not exceed the number of frames skipped"
	.size	.L.str.257, 66

	.type	.L.str.258,@object      # @.str.258
.L.str.258:
	.asciz	"Unsupported direct mode=%d, use TEMPORAL=0 or SPATIAL=1"
	.size	.L.str.258, 56

	.type	.L.str.259,@object      # @.str.259
.L.str.259:
	.asciz	"\nDirectInferenceFlag set to 1 due to interlace coding."
	.size	.L.str.259, 55

	.type	.L.str.260,@object      # @.str.260
.L.str.260:
	.asciz	"Incorrect value %d for IntraBottom. Use 0 (disable) or 1 (enable)."
	.size	.L.str.260, 67

	.type	.L.str.261,@object      # @.str.261
.L.str.261:
	.asciz	"Unsupported symbol mode=%d, use UVLC=0 or CABAC=1"
	.size	.L.str.261, 50

	.type	.L.str.262,@object      # @.str.262
.L.str.262:
	.asciz	"Input file %s does not exist"
	.size	.L.str.262, 29

	.type	.L.str.263,@object      # @.str.263
.L.str.263:
	.asciz	"Error open file %s"
	.size	.L.str.263, 19

	.type	.L.str.264,@object      # @.str.264
.L.str.264:
	.asciz	"even number of lines required for interlaced coding"
	.size	.L.str.264, 52

	.type	.L.str.265,@object      # @.str.265
.L.str.265:
	.asciz	"Warning: Automatic cropping activated: Coded frame Size: %dx%d\n"
	.size	.L.str.265, 64

	.type	.L.str.266,@object      # @.str.266
.L.str.266:
	.asciz	"Warning: slice border within macroblock pair. "
	.size	.L.str.266, 47

	.type	.L.str.267,@object      # @.str.267
.L.str.267:
	.asciz	"Using %d MBs per slice.\n"
	.size	.L.str.267, 25

	.type	.L.str.268,@object      # @.str.268
.L.str.268:
	.asciz	"PatchInp: input->run_length_minus1"
	.size	.L.str.268, 35

	.type	.L.str.269,@object      # @.str.269
.L.str.269:
	.asciz	"%*[^\n]"
	.size	.L.str.269, 7

	.type	.L.str.270,@object      # @.str.270
.L.str.270:
	.asciz	"PatchInp: input->top_left"
	.size	.L.str.270, 26

	.type	.L.str.271,@object      # @.str.271
.L.str.271:
	.asciz	"PatchInp: input->bottom_right"
	.size	.L.str.271, 30

	.type	.L.str.272,@object      # @.str.272
.L.str.272:
	.asciz	"PatchInp: input->slice_group_id"
	.size	.L.str.272, 32

	.type	.L.str.273,@object      # @.str.273
.L.str.273:
	.asciz	"Error read slice group information from file %s"
	.size	.L.str.273, 48

	.type	.L.str.274,@object      # @.str.274
.L.str.274:
	.asciz	"ReferenceReorder Not supported with Interlace encoding methods\n"
	.size	.L.str.274, 64

	.type	.L.str.275,@object      # @.str.275
.L.str.275:
	.asciz	"PocMemoryManagement not supported with Interlace encoding methods\n"
	.size	.L.str.275, 67

	.type	.L.str.276,@object      # @.str.276
.L.str.276:
	.asciz	"Unsupported PicInterlace=%d, use frame based coding=0 or field based coding=1 or adaptive=2"
	.size	.L.str.276, 92

	.type	.L.str.277,@object      # @.str.277
.L.str.277:
	.asciz	"Unsupported MbInterlace=%d, use frame based coding=0 or field based coding=1 or adaptive=2 or frame MB pair only=3"
	.size	.L.str.277, 115

	.type	.L.str.278,@object      # @.str.278
.L.str.278:
	.asciz	"MB AFF is not compatible with non-rd-optimized coding."
	.size	.L.str.278, 55

	.type	.L.str.279,@object      # @.str.279
.L.str.279:
	.asciz	"Fast Mode Decision methods does not support FREX Profiles"
	.size	.L.str.279, 58

	.type	.L.str.280,@object      # @.str.280
.L.str.280:
	.asciz	"MEDistortionQPel=2, MEDistortionHPel=0, MEDistortionFPel=0 is not allowed when SearchMode is set to 1 or 2."
	.size	.L.str.280, 108

	.type	.L.str.281,@object      # @.str.281
.L.str.281:
	.asciz	"NumFramesInELSubSeq (%d) is out of range [0,%d)."
	.size	.L.str.281, 49

	.type	.L.str.282,@object      # @.str.282
.L.str.282:
	.asciz	"Enhanced GOP is not supported in bitstream mode and RTP mode yet."
	.size	.L.str.282, 66

	.type	.L.str.283,@object      # @.str.283
.L.str.283:
	.asciz	"AFF is not compatible with spare picture."
	.size	.L.str.283, 42

	.type	.L.str.284,@object      # @.str.284
.L.str.284:
	.asciz	"Only RTP output mode is compatible with spare picture features."
	.size	.L.str.284, 64

	.type	.L.str.285,@object      # @.str.285
.L.str.285:
	.asciz	"Weighted prediction coding is not supported for MB AFF currently."
	.size	.L.str.285, 66

	.type	.L.str.286,@object      # @.str.286
.L.str.286:
	.asciz	"Enhanced GOP is not supported in weighted prediction coding mode yet."
	.size	.L.str.286, 70

	.type	.L.str.287,@object      # @.str.287
.L.str.287:
	.asciz	"Frame size in macroblocks must be a multiple of BasicUnit."
	.size	.L.str.287, 59

	.type	.L.str.288,@object      # @.str.288
.L.str.288:
	.asciz	"Use RC_MODE_1 only for all-intra coding."
	.size	.L.str.288, 41

	.type	.L.str.289,@object      # @.str.289
.L.str.289:
	.asciz	"Stored B pictures combined with IDR pictures only supported in Picture Order Count type 0\n"
	.size	.L.str.289, 91

	.type	.L.str.290,@object      # @.str.290
.L.str.290:
	.asciz	"temporal direct needs at least 2 ref frames\n"
	.size	.L.str.290, 45

	.type	.L.str.291,@object      # @.str.291
.L.str.291:
	.asciz	"\nThe new 8x8 mode is not implemented for sp-frames."
	.size	.L.str.291, 52

	.type	.L.str.292,@object      # @.str.292
.L.str.292:
	.asciz	"\nTransform8x8Mode may be used only with ProfileIDC %d to %d."
	.size	.L.str.292, 61

	.type	.L.str.293,@object      # @.str.293
.L.str.293:
	.asciz	"\nScalingMatrixPresentFlag may be used only with ProfileIDC %d to %d."
	.size	.L.str.293, 69

	.type	.L.str.294,@object      # @.str.294
.L.str.294:
	.asciz	"\nFRExt Profile(YUV Format) Error!\nYUV422 can be used only with ProfileIDC %d or %d\n"
	.size	.L.str.294, 84

	.type	.L.str.295,@object      # @.str.295
.L.str.295:
	.asciz	"\nFRExt Profile(YUV Format) Error!\nYUV444 can be used only with ProfileIDC %d.\n"
	.size	.L.str.295, 79

	.type	.L.str.296,@object      # @.str.296
.L.str.296:
	.asciz	"\nBiPredMESearchRange must be smaller or equal SearchRange."
	.size	.L.str.296, 59

	.type	.L.str.297,@object      # @.str.297
.L.str.297:
	.asciz	"\nChromaMCBuffer must be set to 1 if ChromaMEEnable is set."
	.size	.L.str.297, 59

	.type	.L.str.298,@object      # @.str.298
.L.str.298:
	.asciz	"\nChromaMEEnable cannot be used with YUV400 color format."
	.size	.L.str.298, 57

	.type	.L.str.299,@object      # @.str.299
.L.str.299:
	.asciz	"Open GOP currently not supported for Field coded pictures."
	.size	.L.str.299, 59

	.type	.L.str.300,@object      # @.str.300
.L.str.300:
	.asciz	"Redundant pictures cannot be used with interlaced tools."
	.size	.L.str.300, 57

	.type	.L.str.301,@object      # @.str.301
.L.str.301:
	.asciz	"Redundant pictures cannot be used with RDPictureDecision."
	.size	.L.str.301, 58

	.type	.L.str.302,@object      # @.str.302
.L.str.302:
	.asciz	"Redundant pictures cannot be used with B frames."
	.size	.L.str.302, 49

	.type	.L.str.303,@object      # @.str.303
.L.str.303:
	.asciz	"PrimaryGOPLength must be equal or greater than 2^NumRedundantHierarchy."
	.size	.L.str.303, 72

	.type	.L.str.304,@object      # @.str.304
.L.str.304:
	.asciz	"NumberReferenceFrames must be greater than or equal to PrimaryGOPLength."
	.size	.L.str.304, 73

	.type	.L.str.305,@object      # @.str.305
.L.str.305:
	.asciz	"\nWarning: B slices used but only one reference allocated within reference buffer.\n"
	.size	.L.str.305, 83

	.type	.L.str.306,@object      # @.str.306
.L.str.306:
	.asciz	"         Performance may be considerably compromised! \n"
	.size	.L.str.306, 56

	.type	.L.str.307,@object      # @.str.307
.L.str.307:
	.asciz	"         2 or more references recommended for use with B slices.\n"
	.size	.L.str.307, 66

	.type	.L.str.308,@object      # @.str.308
.L.str.308:
	.asciz	"\nWarning: Hierarchical coding or Referenced B slices used.\n"
	.size	.L.str.308, 60

	.type	.L.str.309,@object      # @.str.309
.L.str.309:
	.asciz	"         Make sure that you have allocated enough references\n"
	.size	.L.str.309, 62

	.type	.L.str.310,@object      # @.str.310
.L.str.310:
	.asciz	"         in reference buffer to achieve best performance.\n"
	.size	.L.str.310, 59

	.type	.L.str.311,@object      # @.str.311
.L.str.311:
	.asciz	"Error in input parameter %s. Check configuration file. Value should be in [%d, %d] range."
	.size	.L.str.311, 90

	.type	.L.str.312,@object      # @.str.312
.L.str.312:
	.asciz	"Error in input parameter %s. Check configuration file. Value should be in [%.2f, %.2f] range."
	.size	.L.str.312, 94

	.type	.L.str.313,@object      # @.str.313
.L.str.313:
	.asciz	"Error in input parameter %s. Check configuration file. Value should not be smaller than %d."
	.size	.L.str.313, 92

	.type	.L.str.314,@object      # @.str.314
.L.str.314:
	.asciz	"Error in input parameter %s. Check configuration file. Value should not be smaller than %2.f."
	.size	.L.str.314, 94

	.type	.L.str.315,@object      # @.str.315
.L.str.315:
	.asciz	"Profile must be baseline(66)/main(77)/extended(88) or FRExt (%d to %d)."
	.size	.L.str.315, 72

	.type	.L.str.316,@object      # @.str.316
.L.str.316:
	.asciz	"Data partitioning and CABAC is not supported in any profile."
	.size	.L.str.316, 61

	.type	.L.str.317,@object      # @.str.317
.L.str.317:
	.asciz	"Redundant pictures are only allowed in Baseline profile."
	.size	.L.str.317, 57

	.type	.L.str.318,@object      # @.str.318
.L.str.318:
	.asciz	"Data partitioning is only allowed in extended profile."
	.size	.L.str.318, 55

	.type	.L.str.319,@object      # @.str.319
.L.str.319:
	.asciz	"\n----------------------------------------------------------------------------------\n"
	.size	.L.str.319, 85

	.type	.L.str.320,@object      # @.str.320
.L.str.320:
	.asciz	"\n Warning: ChromaIntraDisable and FastCrIntraDecision cannot be combined together.\n Using only Chroma Intra DC mode.\n"
	.size	.L.str.320, 118

	.type	.L.str.321,@object      # @.str.321
.L.str.321:
	.asciz	"B slices are not allowed in baseline."
	.size	.L.str.321, 38

	.type	.L.str.322,@object      # @.str.322
.L.str.322:
	.asciz	"SP pictures are not allowed in baseline."
	.size	.L.str.322, 41

	.type	.L.str.323,@object      # @.str.323
.L.str.323:
	.asciz	"Weighted prediction is not allowed in baseline."
	.size	.L.str.323, 48

	.type	.L.str.324,@object      # @.str.324
.L.str.324:
	.asciz	"CABAC is not allowed in baseline."
	.size	.L.str.324, 34

	.type	.L.str.325,@object      # @.str.325
.L.str.325:
	.asciz	"SP pictures are not allowed in main."
	.size	.L.str.325, 37

	.type	.L.str.326,@object      # @.str.326
.L.str.326:
	.asciz	"num_slice_groups_minus1>0 (FMO) is not allowed in main."
	.size	.L.str.326, 56

	.type	.L.str.327,@object      # @.str.327
.L.str.327:
	.asciz	"direct_8x8_inference flag must be equal to 1 in extended."
	.size	.L.str.327, 58

	.type	.L.str.328,@object      # @.str.328
.L.str.328:
	.asciz	"CABAC is not allowed in extended."
	.size	.L.str.328, 34

	.type	.L.str.329,@object      # @.str.329
.L.str.329:
	.asciz	"\nWarning: LevelIDC 3.0 and above require direct_8x8_inference to be set to 1. Please check your settings.\n"
	.size	.L.str.329, 107

	.type	.L.str.330,@object      # @.str.330
.L.str.330:
	.asciz	"\nInterlace modes only supported for LevelIDC in the range of 2.1 and 4.1. Please check your settings.\n"
	.size	.L.str.330, 103

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Setting Default Parameters..."
	.size	.Lstr, 30

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"*               Encoder Parameters                   *"
	.size	.Lstr.2, 55

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"******************************************************"
	.size	.Lstr.4, 55


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
