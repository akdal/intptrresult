	.text
	.file	"matrix.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_1:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.text
	.globl	_Z8mkmatrixii
	.p2align	4, 0x90
	.type	_Z8mkmatrixii,@function
_Z8mkmatrixii:                          # @_Z8mkmatrixii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movl	%edi, %r14d
	movslq	%r14d, %rbx
	leaq	(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, (%rsp)            # 8-byte Spill
	testl	%ebx, %ebx
	jle	.LBB0_14
# BB#1:                                 # %.lr.ph28
	movslq	%ebp, %rax
	leaq	(,%rax,4), %r15
	testl	%eax, %eax
	jle	.LBB0_2
# BB#4:                                 # %.lr.ph28.split.us.preheader
	movl	%ebp, %r13d
	movl	%r14d, %ebx
	movl	%ebp, %eax
	andl	$7, %eax
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	subq	%rax, %r13
	movl	$1, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph28.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_12 Depth 2
	movq	%r15, %rdi
	callq	malloc
	cmpl	$8, %ebp
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx,%r12,8)
	jb	.LBB0_6
# BB#7:                                 # %min.iters.checked
                                        #   in Loop: Header=BB0_5 Depth=1
	testq	%r13, %r13
	je	.LBB0_6
# BB#8:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB0_5 Depth=1
	leal	(%r14,%r13), %ecx
	leaq	16(%rax), %rdx
	movq	%r13, %rsi
	movl	%r14d, %edi
	movdqa	.LCPI0_0(%rip), %xmm2   # xmm2 = [0,1,2,3]
	movdqa	.LCPI0_1(%rip), %xmm3   # xmm3 = [4,5,6,7]
	.p2align	4, 0x90
.LBB0_9:                                # %vector.body
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	%edi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	%xmm2, %xmm1
	paddd	%xmm3, %xmm0
	movdqu	%xmm1, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$32, %rdx
	addl	$8, %edi
	addq	$-8, %rsi
	jne	.LBB0_9
# BB#10:                                # %middle.block
                                        #   in Loop: Header=BB0_5 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	%r13, %rsi
	jne	.LBB0_11
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_5 Depth=1
	xorl	%esi, %esi
	movl	%r14d, %ecx
.LBB0_11:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_5 Depth=1
	leaq	(%rax,%rsi,4), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	.p2align	4, 0x90
.LBB0_12:                               # %scalar.ph
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, (%rax)
	incl	%ecx
	addq	$4, %rax
	decq	%rdx
	jne	.LBB0_12
.LBB0_13:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_5 Depth=1
	addl	%ebp, %r14d
	incq	%r12
	cmpq	%rbx, %r12
	jne	.LBB0_5
	jmp	.LBB0_14
.LBB0_2:                                # %.lr.ph28.split.preheader
	movl	%r14d, %ebp
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph28.split
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, (%rbx)
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB0_3
.LBB0_14:                               # %._crit_edge29
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_Z8mkmatrixii, .Lfunc_end0-_Z8mkmatrixii
	.cfi_endproc

	.globl	_Z10zeromatrixiiPPi
	.p2align	4, 0x90
	.type	_Z10zeromatrixiiPPi,@function
_Z10zeromatrixiiPPi:                    # @_Z10zeromatrixiiPPi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%edi, %edi
	jle	.LBB1_7
# BB#1:
	testl	%esi, %esi
	jle	.LBB1_7
# BB#2:                                 # %.preheader.us.preheader
	decl	%esi
	leaq	4(,%rsi,4), %r12
	movl	%edi, %r15d
	leaq	-1(%r15), %r13
	movq	%r15, %rbp
	xorl	%ebx, %ebx
	andq	$7, %rbp
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB1_3
.LBB1_4:                                # %.preheader.us.prol.loopexit
	cmpq	$7, %r13
	jb	.LBB1_7
# BB#5:                                 # %.preheader.us.preheader.new
	subq	%rbx, %r15
	leaq	56(%r14,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r15
	jne	.LBB1_6
.LBB1_7:                                # %._crit_edge14
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_Z10zeromatrixiiPPi, .Lfunc_end1-_Z10zeromatrixiiPPi
	.cfi_endproc

	.globl	_Z10freematrixiPPi
	.p2align	4, 0x90
	.type	_Z10freematrixiPPi,@function
_Z10freematrixiPPi:                     # @_Z10freematrixiPPi
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	testl	%edi, %edi
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%edi, %rbx
	incq	%rbx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%r14,%rbx,8), %rdi
	callq	free
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB2_2
.LBB2_3:                                # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	_Z10freematrixiPPi, .Lfunc_end2-_Z10freematrixiPPi
	.cfi_endproc

	.globl	_Z5mmultiiPPiS0_S0_
	.p2align	4, 0x90
	.type	_Z5mmultiiPPiS0_S0_,@function
_Z5mmultiiPPiS0_S0_:                    # @_Z5mmultiiPPiS0_S0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -48
.Lcfi37:
	.cfi_offset %r12, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	testl	%edi, %edi
	jle	.LBB3_9
# BB#1:
	testl	%esi, %esi
	jle	.LBB3_9
# BB#2:                                 # %.preheader.us.us.preheader.preheader
	movl	%esi, %r15d
	movl	%edi, %r9d
	leaq	-1(%r15), %r11
	movl	%r15d, %r12d
	andl	$3, %r12d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader.us.us.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
                                        #       Child Loop BB3_5 Depth 3
                                        #       Child Loop BB3_10 Depth 3
	movq	(%rdx,%r10,8), %rdi
	movq	(%r8,%r10,8), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_4:                                # %.preheader.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_5 Depth 3
                                        #       Child Loop BB3_10 Depth 3
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	testq	%r12, %r12
	je	.LBB3_6
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx,%rsi,8), %rax
	movl	(%rax,%rbx,4), %eax
	imull	(%rdi,%rsi,4), %eax
	addl	%eax, %ebp
	incq	%rsi
	cmpq	%rsi, %r12
	jne	.LBB3_5
.LBB3_6:                                # %.prol.loopexit
                                        #   in Loop: Header=BB3_4 Depth=2
	cmpq	$3, %r11
	jb	.LBB3_7
	.p2align	4, 0x90
.LBB3_10:                               #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx,%rsi,8), %rax
	movl	(%rax,%rbx,4), %eax
	imull	(%rdi,%rsi,4), %eax
	addl	%ebp, %eax
	movq	8(%rcx,%rsi,8), %rbp
	movl	(%rbp,%rbx,4), %ebp
	imull	4(%rdi,%rsi,4), %ebp
	addl	%eax, %ebp
	movq	16(%rcx,%rsi,8), %rax
	movl	(%rax,%rbx,4), %eax
	imull	8(%rdi,%rsi,4), %eax
	addl	%ebp, %eax
	movq	24(%rcx,%rsi,8), %rbp
	movl	(%rbp,%rbx,4), %ebp
	imull	12(%rdi,%rsi,4), %ebp
	addl	%eax, %ebp
	addq	$4, %rsi
	cmpq	%rsi, %r15
	jne	.LBB3_10
.LBB3_7:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB3_4 Depth=2
	movl	%ebp, (%r14,%rbx,4)
	incq	%rbx
	cmpq	%rsi, %rbx
	jne	.LBB3_4
# BB#8:                                 # %._crit_edge35.us
                                        #   in Loop: Header=BB3_3 Depth=1
	incq	%r10
	cmpq	%r9, %r10
	jne	.LBB3_3
.LBB3_9:                                # %._crit_edge37
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_Z5mmultiiPPiS0_S0_, .Lfunc_end3-_Z5mmultiiPPiS0_S0_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
.LCPI4_1:
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
.LCPI4_2:
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
.LCPI4_3:
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
.LCPI4_4:
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	23                      # 0x17
.LCPI4_5:
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	27                      # 0x1b
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 64
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movl	$100000, %r12d          # imm = 0x186A0
	cmpl	$2, %edi
	jne	.LBB4_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r12
.LBB4_2:
	movl	$240, %edi
	callq	malloc
	movq	%rax, %r15
	movl	$1, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph28.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$120, %edi
	callq	malloc
	movq	%rax, (%r15,%r14,8)
	movl	%ebx, (%rax)
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_0(%rip), %xmm1
	movdqu	%xmm1, 4(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_1(%rip), %xmm1
	movdqu	%xmm1, 20(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_2(%rip), %xmm1
	leal	13(%rbx), %ecx
	movdqu	%xmm1, 36(%rax)
	movl	%ecx, 52(%rax)
	leal	14(%rbx), %ecx
	movl	%ecx, 56(%rax)
	leal	15(%rbx), %ecx
	movl	%ecx, 60(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_3(%rip), %xmm1
	movdqu	%xmm1, 64(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_4(%rip), %xmm1
	movdqu	%xmm1, 80(%rax)
	paddd	.LCPI4_5(%rip), %xmm0
	movdqu	%xmm0, 96(%rax)
	leal	28(%rbx), %ecx
	movl	%ecx, 112(%rax)
	leal	29(%rbx), %ecx
	movl	%ecx, 116(%rax)
	incq	%r14
	addl	$30, %ebx
	cmpq	$30, %r14
	jne	.LBB4_3
# BB#4:                                 # %_Z8mkmatrixii.exit
	movl	$240, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$1, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph28.split.us.i27
                                        # =>This Inner Loop Header: Depth=1
	movl	$120, %edi
	callq	malloc
	movq	%rax, (%rbx,%r13,8)
	movl	%r14d, (%rax)
	movd	%r14d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_0(%rip), %xmm1
	movdqu	%xmm1, 4(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_1(%rip), %xmm1
	movdqu	%xmm1, 20(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_2(%rip), %xmm1
	leal	13(%r14), %ecx
	movdqu	%xmm1, 36(%rax)
	movl	%ecx, 52(%rax)
	leal	14(%r14), %ecx
	movl	%ecx, 56(%rax)
	leal	15(%r14), %ecx
	movl	%ecx, 60(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_3(%rip), %xmm1
	movdqu	%xmm1, 64(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_4(%rip), %xmm1
	movdqu	%xmm1, 80(%rax)
	paddd	.LCPI4_5(%rip), %xmm0
	movdqu	%xmm0, 96(%rax)
	leal	28(%r14), %ecx
	movl	%ecx, 112(%rax)
	leal	29(%r14), %ecx
	movl	%ecx, 116(%rax)
	incq	%r13
	addl	$30, %r14d
	cmpq	$30, %r13
	jne	.LBB4_5
# BB#6:                                 # %_Z8mkmatrixii.exit35
	movl	$240, %edi
	callq	malloc
	movq	%rax, %r14
	movl	$1, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph28.split.us.i39
                                        # =>This Inner Loop Header: Depth=1
	movl	$120, %edi
	callq	malloc
	movq	%rax, (%r14,%rbp,8)
	movl	%r13d, (%rax)
	movd	%r13d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_0(%rip), %xmm1
	movdqu	%xmm1, 4(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_1(%rip), %xmm1
	movdqu	%xmm1, 20(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_2(%rip), %xmm1
	leal	13(%r13), %ecx
	movdqu	%xmm1, 36(%rax)
	movl	%ecx, 52(%rax)
	leal	14(%r13), %ecx
	movl	%ecx, 56(%rax)
	leal	15(%r13), %ecx
	movl	%ecx, 60(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_3(%rip), %xmm1
	movdqu	%xmm1, 64(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_4(%rip), %xmm1
	movdqu	%xmm1, 80(%rax)
	paddd	.LCPI4_5(%rip), %xmm0
	movdqu	%xmm0, 96(%rax)
	leal	28(%r13), %ecx
	movl	%ecx, 112(%rax)
	leal	29(%r13), %ecx
	movl	%ecx, 116(%rax)
	incq	%rbp
	addl	$30, %r13d
	cmpq	$30, %rbp
	jne	.LBB4_7
# BB#8:                                 # %_Z8mkmatrixii.exit47.preheader
	testl	%r12d, %r12d
	jle	.LBB4_17
# BB#9:                                 # %.preheader.us.us.preheader.i.preheader.preheader
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB4_10:                               # %.preheader.us.us.preheader.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_11 Depth 2
                                        #       Child Loop BB4_12 Depth 3
                                        #         Child Loop BB4_13 Depth 4
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB4_11:                               # %.preheader.us.us.preheader.i
                                        #   Parent Loop BB4_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_12 Depth 3
                                        #         Child Loop BB4_13 Depth 4
	movq	(%r15,%r9,8), %rdx
	movq	(%r14,%r9,8), %r10
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_12:                               # %.preheader.us.us.i
                                        #   Parent Loop BB4_10 Depth=1
                                        #     Parent Loop BB4_11 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_13 Depth 4
	xorl	%eax, %eax
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB4_13:                               #   Parent Loop BB4_10 Depth=1
                                        #     Parent Loop BB4_11 Depth=2
                                        #       Parent Loop BB4_12 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	-16(%rbx,%rbp,8), %rcx
	movl	(%rcx,%rdi,4), %ecx
	imull	-8(%rdx,%rbp,4), %ecx
	addl	%eax, %ecx
	movq	-8(%rbx,%rbp,8), %rax
	movl	(%rax,%rdi,4), %esi
	imull	-4(%rdx,%rbp,4), %esi
	addl	%ecx, %esi
	movq	(%rbx,%rbp,8), %rax
	movl	(%rax,%rdi,4), %eax
	imull	(%rdx,%rbp,4), %eax
	addl	%esi, %eax
	addq	$3, %rbp
	cmpq	$32, %rbp
	jne	.LBB4_13
# BB#14:                                # %._crit_edge.us.us.i
                                        #   in Loop: Header=BB4_12 Depth=3
	movl	%eax, (%r10,%rdi,4)
	incq	%rdi
	cmpq	$30, %rdi
	jne	.LBB4_12
# BB#15:                                # %._crit_edge35.us.i
                                        #   in Loop: Header=BB4_11 Depth=2
	incq	%r9
	cmpq	$30, %r9
	jne	.LBB4_11
# BB#16:                                # %_Z5mmultiiPPiS0_S0_.exit
                                        #   in Loop: Header=BB4_10 Depth=1
	incl	%r8d
	cmpl	%r12d, %r8d
	jne	.LBB4_10
.LBB4_17:                               # %_Z8mkmatrixii.exit47._crit_edge
	movq	(%r14), %rax
	movl	(%rax), %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSolsEi
	movq	%rax, %r12
	movl	$.L.str, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	16(%r14), %rax
	movl	12(%rax), %esi
	movq	%r12, %rdi
	callq	_ZNSolsEi
	movq	%rax, %r12
	movl	$.L.str, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	24(%r14), %rax
	movl	8(%rax), %esi
	movq	%r12, %rdi
	callq	_ZNSolsEi
	movq	%rax, %r12
	movl	$.L.str, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	32(%r14), %rax
	movl	16(%rax), %esi
	movq	%r12, %rdi
	callq	_ZNSolsEi
	movq	%rax, %r12
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.LBB4_22
# BB#18:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%r13)
	je	.LBB4_20
# BB#19:
	movb	67(%r13), %al
	jmp	.LBB4_21
.LBB4_20:
	movq	%r13, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%r13), %rax
	movl	$10, %esi
	movq	%r13, %rdi
	callq	*48(%rax)
.LBB4_21:                               # %_ZNKSt5ctypeIcE5widenEc.exit
	movsbl	%al, %esi
	movq	%r12, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	232(%r15), %rdi
	callq	free
	movq	224(%r15), %rdi
	callq	free
	movq	216(%r15), %rdi
	callq	free
	movq	208(%r15), %rdi
	callq	free
	movq	200(%r15), %rdi
	callq	free
	movq	192(%r15), %rdi
	callq	free
	movq	184(%r15), %rdi
	callq	free
	movq	176(%r15), %rdi
	callq	free
	movq	168(%r15), %rdi
	callq	free
	movq	160(%r15), %rdi
	callq	free
	movq	152(%r15), %rdi
	callq	free
	movq	144(%r15), %rdi
	callq	free
	movq	136(%r15), %rdi
	callq	free
	movq	128(%r15), %rdi
	callq	free
	movq	120(%r15), %rdi
	callq	free
	movq	112(%r15), %rdi
	callq	free
	movq	104(%r15), %rdi
	callq	free
	movq	96(%r15), %rdi
	callq	free
	movq	88(%r15), %rdi
	callq	free
	movq	80(%r15), %rdi
	callq	free
	movq	72(%r15), %rdi
	callq	free
	movq	64(%r15), %rdi
	callq	free
	movq	56(%r15), %rdi
	callq	free
	movq	48(%r15), %rdi
	callq	free
	movq	40(%r15), %rdi
	callq	free
	movq	32(%r15), %rdi
	callq	free
	movq	24(%r15), %rdi
	callq	free
	movq	16(%r15), %rdi
	callq	free
	movq	8(%r15), %rdi
	callq	free
	movq	(%r15), %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	232(%rbx), %rdi
	callq	free
	movq	224(%rbx), %rdi
	callq	free
	movq	216(%rbx), %rdi
	callq	free
	movq	208(%rbx), %rdi
	callq	free
	movq	200(%rbx), %rdi
	callq	free
	movq	192(%rbx), %rdi
	callq	free
	movq	184(%rbx), %rdi
	callq	free
	movq	176(%rbx), %rdi
	callq	free
	movq	168(%rbx), %rdi
	callq	free
	movq	160(%rbx), %rdi
	callq	free
	movq	152(%rbx), %rdi
	callq	free
	movq	144(%rbx), %rdi
	callq	free
	movq	136(%rbx), %rdi
	callq	free
	movq	128(%rbx), %rdi
	callq	free
	movq	120(%rbx), %rdi
	callq	free
	movq	112(%rbx), %rdi
	callq	free
	movq	104(%rbx), %rdi
	callq	free
	movq	96(%rbx), %rdi
	callq	free
	movq	88(%rbx), %rdi
	callq	free
	movq	80(%rbx), %rdi
	callq	free
	movq	72(%rbx), %rdi
	callq	free
	movq	64(%rbx), %rdi
	callq	free
	movq	56(%rbx), %rdi
	callq	free
	movq	48(%rbx), %rdi
	callq	free
	movq	40(%rbx), %rdi
	callq	free
	movq	32(%rbx), %rdi
	callq	free
	movq	24(%rbx), %rdi
	callq	free
	movq	16(%rbx), %rdi
	callq	free
	movq	8(%rbx), %rdi
	callq	free
	movq	(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	232(%r14), %rdi
	callq	free
	movq	224(%r14), %rdi
	callq	free
	movq	216(%r14), %rdi
	callq	free
	movq	208(%r14), %rdi
	callq	free
	movq	200(%r14), %rdi
	callq	free
	movq	192(%r14), %rdi
	callq	free
	movq	184(%r14), %rdi
	callq	free
	movq	176(%r14), %rdi
	callq	free
	movq	168(%r14), %rdi
	callq	free
	movq	160(%r14), %rdi
	callq	free
	movq	152(%r14), %rdi
	callq	free
	movq	144(%r14), %rdi
	callq	free
	movq	136(%r14), %rdi
	callq	free
	movq	128(%r14), %rdi
	callq	free
	movq	120(%r14), %rdi
	callq	free
	movq	112(%r14), %rdi
	callq	free
	movq	104(%r14), %rdi
	callq	free
	movq	96(%r14), %rdi
	callq	free
	movq	88(%r14), %rdi
	callq	free
	movq	80(%r14), %rdi
	callq	free
	movq	72(%r14), %rdi
	callq	free
	movq	64(%r14), %rdi
	callq	free
	movq	56(%r14), %rdi
	callq	free
	movq	48(%r14), %rdi
	callq	free
	movq	40(%r14), %rdi
	callq	free
	movq	32(%r14), %rdi
	callq	free
	movq	24(%r14), %rdi
	callq	free
	movq	16(%r14), %rdi
	callq	free
	movq	8(%r14), %rdi
	callq	free
	movq	(%r14), %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_22:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end4:
	.size	main, .Lfunc_end4-main
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_matrix.ii,@function
_GLOBAL__sub_I_matrix.ii:               # @_GLOBAL__sub_I_matrix.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end5:
	.size	_GLOBAL__sub_I_matrix.ii, .Lfunc_end5-_GLOBAL__sub_I_matrix.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" "
	.size	.L.str, 2

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_matrix.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
