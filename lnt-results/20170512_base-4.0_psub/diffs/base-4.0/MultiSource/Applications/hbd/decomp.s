	.text
	.file	"decomp.bc"
	.globl	_Z12printintlistP7intlist
	.p2align	4, 0x90
	.type	_Z12printintlistP7intlist,@function
_Z12printintlistP7intlist:              # @_Z12printintlistP7intlist
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	stderr(%rip), %rsi
	movl	$91, %edi
	callq	fputc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_2
.LBB0_3:                                # %._crit_edge
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$2, %esi
	movl	$1, %edx
	popq	%rbx
	jmp	fwrite                  # TAILCALL
.LBB0_1:                                # %.lr.ph
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	stderr(%rip), %rdi
	movl	(%rbx), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_1
	jmp	.LBB0_3
.Lfunc_end0:
	.size	_Z12printintlistP7intlist, .Lfunc_end0-_Z12printintlistP7intlist
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	10                      # 0xa
	.long	35                      # 0x23
.LCPI1_1:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	10                      # 0xa
	.long	36                      # 0x24
	.text
	.globl	_Z14decompileblockP9ClassfileP11method_info
	.p2align	4, 0x90
	.type	_Z14decompileblockP9ClassfileP11method_info,@function
_Z14decompileblockP9ClassfileP11method_info: # @_Z14decompileblockP9ClassfileP11method_info
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 208
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r13
	movq	%rbp, miptr(%rip)
	movl	$-1, cond_pcend(%rip)
	movl	24(%r13), %r15d
	movq	%rbp, %rdi
	callq	_ZN11AccessFlags6strlenEv
	movzwl	%ax, %edi
	incq	%rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	8(%r13), %r14
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	_ZN11AccessFlags8toStringEPc
	movq	%rax, %rcx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	16(%rbp), %rax
	movq	%rax, 144(%rsp)
	movq	8(%r13), %rsi
	movq	8(%rbp), %rcx
	leaq	144(%rsp), %rdx
	movq	%r13, %rdi
	movq	%rbp, %r8
	callq	_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB1_156
# BB#1:                                 # %.preheader
	movl	$1, (%rsp)              # 4-byte Folded Spill
	cmpl	$0, 132(%rbp)
	je	.LBB1_4
# BB#2:                                 # %.lr.ph385
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	136(%rbp), %rax
	movzwl	(%rax,%rbx,4), %eax
	incq	%rbx
	movq	8(%r13), %rdi
	movq	40(%r13), %rcx
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rax
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpl	132(%rbp), %ebx
	jne	.LBB1_3
.LBB1_4:                                # %._crit_edge386
	cmpl	$2, %r15d
	je	.LBB1_17
# BB#5:                                 # %._crit_edge386
	movzwl	(%rbp), %eax
	andl	$1280, %eax             # imm = 0x500
	testw	%ax, %ax
	jne	.LBB1_17
# BB#6:
	movq	32(%rbp), %rax
	movq	%rax, inbuff(%rip)
	movl	28(%rbp), %eax
	movl	%eax, bufflength(%rip)
	movl	$0, currpc(%rip)
	movq	$stack, stkptr(%rip)
	movq	$donestack, donestkptr(%rip)
	movl	$0, indentlevel(%rip)
	testl	%eax, %eax
	jle	.LBB1_15
# BB#7:                                 # %.lr.ph382.preheader
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	cmpl	cond_pcend(%rip), %ecx
	jne	.LBB1_11
	.p2align	4, 0x90
.LBB1_9:
	movq	%r13, %rdi
	callq	_Z17finishconditionalP9Classfile
	testl	%eax, %eax
	jne	.LBB1_158
# BB#10:                                # %._crit_edge392
	movl	currpc(%rip), %ecx
	movl	bufflength(%rip), %eax
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_8:                                # %thread-pre-split..lr.ph382_crit_edge
                                        #   in Loop: Header=BB1_11 Depth=1
	movl	currpc(%rip), %ecx
	cmpl	cond_pcend(%rip), %ecx
	je	.LBB1_9
.LBB1_11:                               # =>This Inner Loop Header: Depth=1
	incl	%ecx
	movl	%ecx, currpc(%rip)
	decl	%eax
	movl	%eax, bufflength(%rip)
	movq	inbuff(%rip), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %edx
	movl	%edx, ch(%rip)
	movl	%ebx, lastaction(%rip)
	movsbl	actions(%rdx), %ebx
	testl	%ebx, %ebx
	je	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_11 Depth=1
	movslq	%ebx, %rax
	movq	%r13, %rdi
	callq	*actiontable(,%rax,8)
	testl	%eax, %eax
	je	.LBB1_14
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_13:                               #   in Loop: Header=BB1_11 Depth=1
	movq	8(%r13), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
.LBB1_14:                               # %thread-pre-split
                                        #   in Loop: Header=BB1_11 Depth=1
	movl	bufflength(%rip), %eax
	testl	%eax, %eax
	jg	.LBB1_8
.LBB1_15:                               # %._crit_edge383
	movq	8(%r13), %rcx
	movl	$.L.str.8, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$16, %edi
	callq	_Znwm
	xorps	%xmm0, %xmm0
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movups	%xmm0, (%rax)
	movq	donestkptr(%rip), %rax
	leaq	-8(%rax), %rcx
	movl	$donestack, %edx
	cmpq	%rdx, %rcx
	jbe	.LBB1_42
# BB#16:                                # %.lr.ph378
	movl	$donestack, %ebp
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %r15
	movq	%r15, 8(%rsp)           # 8-byte Spill
	jmp	.LBB1_20
.LBB1_17:
	movq	8(%r13), %rsi
	movl	$59, %edi
	callq	fputc
	jmp	.LBB1_155
.LBB1_18:
	movl	$1, %ecx
	jmp	.LBB1_156
.LBB1_19:                               #   in Loop: Header=BB1_20 Depth=1
	leaq	24(%rbx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jmp	.LBB1_30
	.p2align	4, 0x90
.LBB1_20:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %r12
	movq	8(%rbp), %rbx
	movq	(%r12), %rax
	cmpl	$19, 12(%rax)
	jne	.LBB1_24
.LBB1_21:                               #   in Loop: Header=BB1_20 Depth=1
	movq	%rbx, %r14
.LBB1_22:                               #   in Loop: Header=BB1_20 Depth=1
	movl	16(%r12), %eax
	cmpl	48(%r12), %eax
	jb	.LBB1_41
# BB#23:                                # %_ZN8looplist3addEP4Loop.exit
                                        #   in Loop: Header=BB1_20 Depth=1
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r12), %eax
	movl	48(%r12), %ecx
	movl	16(%r14), %edx
	movq	24(%r12), %rsi
	movl	%eax, (%rbx)
	movl	%ecx, 4(%rbx)
	movl	%edx, 8(%rbx)
	movq	%rsi, 16(%rbx)
	movl	$1, 24(%rbx)
	movl	$16, %edi
	callq	_Znwm
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	leaq	8(%rcx), %rcx
	cmoveq	%r15, %rcx
	movq	%r15, %rdx
	cmoveq	%rsi, %rdx
	movq	%rax, (%rcx)
	movq	%rax, (%rdx)
	jmp	.LBB1_41
	.p2align	4, 0x90
.LBB1_24:                               #   in Loop: Header=BB1_20 Depth=1
	cmpl	$8, 4(%rax)
	jne	.LBB1_41
# BB#25:                                #   in Loop: Header=BB1_20 Depth=1
	movq	16(%rbp), %rdx
	movq	(%rbx), %rax
	cmpl	$8, 4(%rax)
	je	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_20 Depth=1
	cmpl	$24, 12(%rax)
	jne	.LBB1_21
.LBB1_27:                               #   in Loop: Header=BB1_20 Depth=1
	movl	16(%r12), %ecx
	movl	48(%r12), %eax
	cmpl	16(%rbx), %ecx
	movq	%rbx, %rcx
	cmovbq	%r12, %rcx
	movl	16(%rcx), %r14d
	cmpl	48(%rbx), %eax
	jne	.LBB1_31
# BB#28:                                #   in Loop: Header=BB1_20 Depth=1
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
	movl	%r14d, %eax
	movq	24(%r12), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	24(%rbx), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	$1, 8(%r15)
	movl	%eax, 16(%r15)
	movl	%eax, 12(%r15)
.Ltmp3:
	movl	$24, %edi
	callq	_Znwm
.Ltmp4:
# BB#29:                                # %_ZN3ExpC2Ej7Exptype4Type2OpPS_S3_.exit
                                        #   in Loop: Header=BB1_20 Depth=1
	leaq	24(%rbx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [0,4,10,36]
	movups	%xmm0, (%rax)
	movq	%rax, (%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 24(%r15)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%r15)
	movl	$1, (%rsp)              # 4-byte Folded Spill
.LBB1_30:                               #   in Loop: Header=BB1_20 Depth=1
	movq	%r12, %rdi
	callq	_Z7killexpP3Exp
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%r15, (%rax)
	movl	%r14d, 16(%rbx)
	movq	%rbx, (%rbp)
	movq	$0, 8(%rbp)
	leaq	8(%rbp), %rbp
	xorl	%ecx, %ecx
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_37
.LBB1_31:                               #   in Loop: Header=BB1_20 Depth=1
	movl	$9, %ecx
	cmpl	16(%rdx), %eax
	jne	.LBB1_36
# BB#32:                                #   in Loop: Header=BB1_20 Depth=1
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
	movq	24(%r12), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	24(%rbx), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$1, 8(%r15)
	movl	%r14d, 16(%r15)
	movl	%r14d, 12(%r15)
.Ltmp0:
	movl	$24, %edi
	callq	_Znwm
.Ltmp1:
# BB#33:                                #   in Loop: Header=BB1_20 Depth=1
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,4,10,35]
	movups	%xmm0, (%rax)
	movq	%rax, (%r15)
	movq	%r15, %rdi
	addq	$24, %rdi
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%r15)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%r15)
	callq	_Z6notexpPP3Exp
	movl	$1, %ecx
	testl	%eax, %eax
	je	.LBB1_19
# BB#34:                                #   in Loop: Header=BB1_20 Depth=1
	movq	%rbx, %r14
	movq	%r12, %rbx
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB1_37
.LBB1_36:                               #   in Loop: Header=BB1_20 Depth=1
	movq	%rbx, %r14
	movq	%r12, %rbx
.LBB1_37:                               #   in Loop: Header=BB1_20 Depth=1
	movl	%ecx, %eax
	andb	$15, %al
	je	.LBB1_39
# BB#38:                                #   in Loop: Header=BB1_20 Depth=1
	cmpb	$9, %al
	jne	.LBB1_40
.LBB1_39:                               #   in Loop: Header=BB1_20 Depth=1
	movq	%rbx, %r12
	jmp	.LBB1_22
.LBB1_40:                               #   in Loop: Header=BB1_20 Depth=1
	testl	%ecx, %ecx
	jne	.LBB1_158
	.p2align	4, 0x90
.LBB1_41:                               # %.thread
                                        #   in Loop: Header=BB1_20 Depth=1
	addq	$8, %rbp
	movq	donestkptr(%rip), %rax
	leaq	-8(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.LBB1_20
.LBB1_42:                               # %._crit_edge379
	movl	$donestack, %ecx
	cmpq	%rcx, %rax
	je	.LBB1_154
# BB#43:                                # %.lr.ph375
	movl	$donestack, %ebp
	movl	$0, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	$0, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$0, %edi
	movl	$0, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
                                        # implicit-def: %R9
	.p2align	4, 0x90
.LBB1_44:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_49 Depth 2
                                        #     Child Loop BB1_59 Depth 2
                                        #     Child Loop BB1_71 Depth 2
                                        #     Child Loop BB1_128 Depth 2
                                        #     Child Loop BB1_132 Depth 2
                                        #     Child Loop BB1_140 Depth 2
                                        #     Child Loop BB1_89 Depth 2
                                        #     Child Loop BB1_117 Depth 2
                                        #     Child Loop BB1_98 Depth 2
                                        #     Child Loop BB1_110 Depth 2
                                        #     Child Loop BB1_147 Depth 2
	movq	%rbp, %rax
	leaq	8(%rax), %rbp
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.LBB1_152
# BB#45:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rbx, %r15
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.LBB1_65
# BB#46:                                # %_ZN8looplist3topEv.exit322
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	(%r14), %rax
	movl	4(%rax), %ecx
	cmpl	16(%r12), %ecx
	jne	.LBB1_53
# BB#47:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	$1, 24(%rax)
	movq	8(%r13), %rdi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r9, %rdx
	callq	fprintf
	movl	indentlevel(%rip), %ebx
	movl	%ebx, %eax
	incl	%eax
	movl	%eax, indentlevel(%rip)
	je	.LBB1_50
# BB#48:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_44 Depth=1
	notl	%ebx
	.p2align	4, 0x90
.LBB1_49:                               # %.lr.ph
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	incl	%ebx
	jne	.LBB1_49
.LBB1_50:                               # %.loopexit409
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	(%r14), %rbp
	movq	8(%r14), %rbx
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rbx, (%rax)
	movq	%r14, %rdi
	callq	_ZdlPv
	testq	%rbx, %rbx
	jne	.LBB1_52
# BB#51:                                #   in Loop: Header=BB1_44 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	$0, 8(%rax)
.LBB1_52:                               # %_ZN8looplist3popEv.exit326
                                        #   in Loop: Header=BB1_44 Depth=1
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rdx
	movq	%rbp, (%rdx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 8(%rdx)
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	testq	%rcx, %rcx
	movq	120(%rsp), %rax         # 8-byte Reload
	cmoveq	%rdx, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	cmoveq	%rdx, %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	testq	%rbx, %rbx
	movq	%rbx, %r14
	movq	%rdx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %rsi
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rdx, %r15
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	je	.LBB1_65
.LBB1_53:                               # %_ZN8looplist3topEv.exit328
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	(%r12), %rax
	cmpl	$19, 12(%rax)
	jne	.LBB1_65
# BB#54:                                #   in Loop: Header=BB1_44 Depth=1
	movq	(%r14), %rax
	movl	48(%r12), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB1_65
# BB#55:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	$0, 24(%rax)
	movq	16(%rax), %rdi
	xorl	%esi, %esi
	callq	_ZN3Exp8toStringEj
	movq	%rax, %rbx
	movq	8(%r13), %rdi
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	testq	%rbx, %rbx
	je	.LBB1_57
# BB#56:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB1_57:                               #   in Loop: Header=BB1_44 Depth=1
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rbp, %r12
	movl	indentlevel(%rip), %ebx
	movl	%ebx, %eax
	incl	%eax
	movl	%eax, indentlevel(%rip)
	je	.LBB1_60
# BB#58:                                # %.lr.ph344.preheader
                                        #   in Loop: Header=BB1_44 Depth=1
	notl	%ebx
	.p2align	4, 0x90
.LBB1_59:                               # %.lr.ph344
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	incl	%ebx
	jne	.LBB1_59
.LBB1_60:                               # %._crit_edge345
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	(%r14), %rbx
	movq	8(%r14), %rbp
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	testq	%r14, %r14
	je	.LBB1_62
# BB#61:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%r14, %rdi
	callq	_ZdlPv
.LBB1_62:                               #   in Loop: Header=BB1_44 Depth=1
	testq	%rbp, %rbp
	jne	.LBB1_64
# BB#63:                                #   in Loop: Header=BB1_44 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	$0, 8(%rax)
.LBB1_64:                               # %_ZN8looplist3popEv.exit324
                                        #   in Loop: Header=BB1_44 Depth=1
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rdx
	movq	%rbx, (%rdx)
	movq	%r15, 8(%rdx)
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	testq	%rcx, %rcx
	cmoveq	%rdx, %rcx
	movq	%rdx, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rdx, %rsi
	movq	%rdx, %rbx
	movq	%rcx, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	%rdx, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%r12, %rbp
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	cmpq	donestkptr(%rip), %rbp
	jne	.LBB1_44
	jmp	.LBB1_154
	.p2align	4, 0x90
.LBB1_65:                               # %_ZN8looplist4pushEP4Loop.exit.thread
                                        #   in Loop: Header=BB1_44 Depth=1
	testq	%rdi, %rdi
	je	.LBB1_73
# BB#66:                                # %_ZN7intlist3topEv.exit321
                                        #   in Loop: Header=BB1_44 Depth=1
	movl	(%rdi), %eax
	cmpl	16(%r12), %eax
	jne	.LBB1_75
# BB#67:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%r9, %rbx
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	8(%rdi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB1_69
# BB#68:                                #   in Loop: Header=BB1_44 Depth=1
	callq	_ZdlPv
.LBB1_69:                               # %_ZN7intlist3popEv.exit320
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	8(%r13), %rcx
	movl	$.L.str.12, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movl	indentlevel(%rip), %eax
	movl	%eax, %ecx
	decl	%ecx
	movl	%ecx, indentlevel(%rip)
	je	.LBB1_74
# BB#70:                                # %.lr.ph347.preheader
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%rbp, %r15
	movq	%rbx, %rbp
	movl	$1, %ebx
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB1_71:                               # %.lr.ph347
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	incl	%ebx
	jne	.LBB1_71
# BB#72:                                #   in Loop: Header=BB1_44 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rbp, %r9
	movq	%r15, %rbp
	movq	%r14, %rsi
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB1_75
	.p2align	4, 0x90
.LBB1_73:                               #   in Loop: Header=BB1_44 Depth=1
	xorl	%edi, %edi
	jmp	.LBB1_75
.LBB1_74:                               #   in Loop: Header=BB1_44 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%rbx, %r9
	.p2align	4, 0x90
.LBB1_75:                               # %.loopexit338
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	(%r12), %rax
	movl	12(%rax), %ecx
	cmpl	$19, %ecx
	je	.LBB1_77
# BB#76:                                #   in Loop: Header=BB1_44 Depth=1
	cmpl	$8, 4(%rax)
	jne	.LBB1_83
.LBB1_77:                               #   in Loop: Header=BB1_44 Depth=1
	testq	%rsi, %rsi
	je	.LBB1_82
# BB#78:                                # %_ZN8looplist3topEv.exit
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	(%rsi), %rsi
	movl	(%rsi), %edx
	cmpl	16(%r12), %edx
	jne	.LBB1_87
# BB#79:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%r9, 24(%rsp)           # 8-byte Spill
	cmpl	$1, 24(%rsi)
	jne	.LBB1_92
# BB#80:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%rdi, %r14
	movq	%rbp, %r15
	cmpl	$19, %ecx
	jne	.LBB1_119
# BB#81:                                #   in Loop: Header=BB1_44 Depth=1
	movq	8(%r13), %rdi
	movl	$.L.str.13, %esi
	jmp	.LBB1_93
.LBB1_82:                               #   in Loop: Header=BB1_44 Depth=1
	movq	%rsi, %r8
	xorl	%esi, %esi
	jmp	.LBB1_103
.LBB1_83:                               #   in Loop: Header=BB1_44 Depth=1
	cmpl	$16, %ecx
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	jne	.LBB1_134
# BB#84:                                #   in Loop: Header=BB1_44 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB1_121
# BB#85:                                # %_ZN7intlist3topEv.exit312
                                        #   in Loop: Header=BB1_44 Depth=1
	movl	(%rax), %eax
	movq	(%rbp), %rcx
	testq	%rcx, %rcx
	je	.LBB1_122
# BB#86:                                #   in Loop: Header=BB1_44 Depth=1
	movl	16(%rcx), %ecx
	jmp	.LBB1_123
.LBB1_87:                               #   in Loop: Header=BB1_44 Depth=1
	movq	%rbp, %r15
	movq	112(%rsp), %rbp         # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB1_101
# BB#88:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%rdi, %r14
	movl	48(%r12), %ecx
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB1_89:                               # %.lr.ph.i
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	cmpl	%ecx, 8(%rsi)
	je	.LBB1_116
# BB#90:                                #   in Loop: Header=BB1_89 Depth=2
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_89
# BB#91:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%rbp, %rsi
	movq	%rbp, %r8
	jmp	.LBB1_102
.LBB1_92:                               #   in Loop: Header=BB1_44 Depth=1
	movq	%rdi, %r14
	movq	%rbp, %r15
	movq	8(%r13), %rdi
	movl	$.L.str.15, %esi
.LBB1_93:                               #   in Loop: Header=BB1_44 Depth=1
	xorl	%eax, %eax
	callq	fprintf
.LBB1_94:                               #   in Loop: Header=BB1_44 Depth=1
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	8(%rdi), %rbx
	testq	%rdi, %rdi
	je	.LBB1_96
# BB#95:                                #   in Loop: Header=BB1_44 Depth=1
	callq	_ZdlPv
.LBB1_96:                               # %_ZN8looplist3popEv.exit
                                        #   in Loop: Header=BB1_44 Depth=1
	testq	%rbx, %rbx
	movq	120(%rsp), %rax         # 8-byte Reload
	cmoveq	%rbx, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	136(%rsp), %rax         # 8-byte Reload
	cmoveq	%rbx, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	indentlevel(%rip), %eax
	movl	%eax, %ecx
	decl	%ecx
	movl	%ecx, indentlevel(%rip)
	movq	%r15, %rbp
	je	.LBB1_100
# BB#97:                                # %.lr.ph360.preheader
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%rbx, %r15
	movl	$1, %ebx
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB1_98:                               # %.lr.ph360
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	incl	%ebx
	jne	.LBB1_98
# BB#99:                                #   in Loop: Header=BB1_44 Depth=1
	movq	%r15, %rbx
.LBB1_100:                              #   in Loop: Header=BB1_44 Depth=1
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	%rbx, %rsi
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	%r14, %rdi
	cmpq	donestkptr(%rip), %rbp
	jne	.LBB1_44
	jmp	.LBB1_154
.LBB1_101:                              #   in Loop: Header=BB1_44 Depth=1
	movq	%rbp, %r8
	xorl	%ecx, %ecx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
.LBB1_102:                              # %_ZN8looplist12containsPastEj.exit.thread
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%r15, %rbp
.LBB1_103:                              # %_ZN8looplist12containsPastEj.exit.thread
                                        #   in Loop: Header=BB1_44 Depth=1
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	je	.LBB1_112
# BB#104:                               # %_ZN7intlist3topEv.exit317
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	(%rcx), %ecx
	movq	(%rbp), %rdx
	testq	%rdx, %rdx
	je	.LBB1_106
# BB#105:                               #   in Loop: Header=BB1_44 Depth=1
	movl	16(%rdx), %edx
	cmpl	%edx, %ecx
	je	.LBB1_107
	jmp	.LBB1_112
.LBB1_106:                              #   in Loop: Header=BB1_44 Depth=1
	xorl	%edx, %edx
	cmpl	%edx, %ecx
	jne	.LBB1_112
.LBB1_107:                              # %_ZN7intlist4pushEj.exit316
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%r8, %r14
	movq	%rbp, %r15
	movq	%r9, %rbp
	movl	48(%r12), %ebx
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rcx
	movl	%ebx, (%rcx)
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rcx, %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rcx)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	movq	8(%rcx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rcx, %rdi
	testq	%rdi, %rdi
	je	.LBB1_109
# BB#108:                               #   in Loop: Header=BB1_44 Depth=1
	callq	_ZdlPv
.LBB1_109:                              # %_ZN7intlist3popEv.exit315
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	8(%r13), %rdi
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	16(%rax), %edx
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	indentlevel(%rip), %ebx
	testl	%ebx, %ebx
	je	.LBB1_111
	.p2align	4, 0x90
.LBB1_110:                              # %.lr.ph362
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	decl	%ebx
	jne	.LBB1_110
.LBB1_111:                              #   in Loop: Header=BB1_44 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%r14, %rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %r9
	movq	%r15, %rbp
	movq	24(%rsp), %rsi          # 8-byte Reload
	cmpq	donestkptr(%rip), %rbp
	jne	.LBB1_44
	jmp	.LBB1_154
.LBB1_112:                              #   in Loop: Header=BB1_44 Depth=1
	cmpl	$8, 4(%rax)
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	jne	.LBB1_115
# BB#113:                               #   in Loop: Header=BB1_44 Depth=1
	movl	48(%r12), %ebx
	cmpl	16(%r12), %ebx
	jbe	.LBB1_115
# BB#114:                               # %_ZN7intlist4pushEj.exit
                                        #   in Loop: Header=BB1_44 Depth=1
	movl	$16, %edi
	movq	%r8, %rbp
	callq	_Znwm
	movq	%rax, %rcx
	movl	%ebx, (%rcx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 8(%rcx)
	incl	indentlevel(%rip)
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rcx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %r14
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jmp	.LBB1_145
.LBB1_115:                              #   in Loop: Header=BB1_44 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r8, %r14
	movq	%r8, 8(%rsp)            # 8-byte Spill
	jmp	.LBB1_145
.LBB1_116:                              # %_ZN8looplist12containsPastEj.exit
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%r9, %r12
	movq	8(%r13), %rdi
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	16(%rax), %edx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	indentlevel(%rip), %ebx
	testl	%ebx, %ebx
	je	.LBB1_118
	.p2align	4, 0x90
.LBB1_117:                              # %.lr.ph358
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	decl	%ebx
	jne	.LBB1_117
.LBB1_118:                              #   in Loop: Header=BB1_44 Depth=1
	movq	%rbp, %rsi
	movq	%rbp, %rbx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r15, %rbp
	movq	%r12, %r9
	movq	%r14, %rdi
	cmpq	donestkptr(%rip), %rbp
	jne	.LBB1_44
	jmp	.LBB1_154
.LBB1_119:                              #   in Loop: Header=BB1_44 Depth=1
	movq	16(%rsi), %rdi
	xorl	%esi, %esi
	callq	_ZN3Exp8toStringEj
	movq	%rax, %rbp
	movq	8(%r13), %rdi
	movl	16(%r12), %ecx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
	testq	%rbp, %rbp
	je	.LBB1_94
# BB#120:                               #   in Loop: Header=BB1_44 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
	jmp	.LBB1_94
.LBB1_121:                              #   in Loop: Header=BB1_44 Depth=1
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	jmp	.LBB1_134
.LBB1_122:                              #   in Loop: Header=BB1_44 Depth=1
	xorl	%ecx, %ecx
.LBB1_123:                              #   in Loop: Header=BB1_44 Depth=1
	cmpl	%ecx, %eax
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	jne	.LBB1_134
# BB#124:                               #   in Loop: Header=BB1_44 Depth=1
	movq	%r15, %r14
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	movq	8(%rcx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rcx, %rdi
	testq	%rdi, %rdi
	je	.LBB1_126
# BB#125:                               #   in Loop: Header=BB1_44 Depth=1
	callq	_ZdlPv
.LBB1_126:                              # %_ZN7intlist3popEv.exit311
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%rbp, %r15
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	_ZN3Exp8toStringEj
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_130
# BB#127:                               #   in Loop: Header=BB1_44 Depth=1
	movq	8(%r13), %rdi
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	16(%rax), %ecx
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%rbx, %rbp
	movq	%rbx, %rdx
	callq	fprintf
	movl	indentlevel(%rip), %ebx
	testl	%ebx, %ebx
	je	.LBB1_129
	.p2align	4, 0x90
.LBB1_128:                              # %.lr.ph350
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	decl	%ebx
	jne	.LBB1_128
.LBB1_129:                              # %._crit_edge351
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%rbp, %rbx
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB1_130:                              #   in Loop: Header=BB1_44 Depth=1
	movq	%rbx, %r12
	movq	8(%r13), %rcx
	movl	$.L.str.12, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movl	indentlevel(%rip), %eax
	movl	%eax, %ecx
	decl	%ecx
	movl	%ecx, indentlevel(%rip)
	je	.LBB1_153
# BB#131:                               # %.lr.ph354.preheader
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%r12, %rbp
	movl	$1, %ebx
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB1_132:                              # %.lr.ph354
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	incl	%ebx
	jne	.LBB1_132
# BB#133:                               #   in Loop: Header=BB1_44 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rbp, %r9
	movq	%r15, %rbp
	movq	24(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB1_151
.LBB1_134:                              #   in Loop: Header=BB1_44 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	je	.LBB1_142
# BB#135:                               # %_ZN7intlist3topEv.exit
                                        #   in Loop: Header=BB1_44 Depth=1
	movl	(%rax), %eax
	cmpl	16(%r12), %eax
	jne	.LBB1_144
# BB#136:                               #   in Loop: Header=BB1_44 Depth=1
	movq	%r15, %r14
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	movq	8(%rcx), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	testq	%rdi, %rdi
	je	.LBB1_138
# BB#137:                               #   in Loop: Header=BB1_44 Depth=1
	callq	_ZdlPv
.LBB1_138:                              # %_ZN7intlist3popEv.exit
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	8(%r13), %rcx
	movl	$.L.str.12, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movl	indentlevel(%rip), %eax
	movl	%eax, %ecx
	decl	%ecx
	movl	%ecx, indentlevel(%rip)
	je	.LBB1_141
# BB#139:                               # %.lr.ph356.preheader
                                        #   in Loop: Header=BB1_44 Depth=1
	movl	$1, %ebx
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB1_140:                              # %.lr.ph356
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	incl	%ebx
	jne	.LBB1_140
.LBB1_141:                              #   in Loop: Header=BB1_44 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	jmp	.LBB1_143
.LBB1_142:                              #   in Loop: Header=BB1_44 Depth=1
	movq	%r15, %r14
	xorl	%eax, %eax
.LBB1_143:                              # %.loopexit
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%rax, 104(%rsp)         # 8-byte Spill
	jmp	.LBB1_145
.LBB1_144:                              #   in Loop: Header=BB1_44 Depth=1
	movq	%r15, %r14
	.p2align	4, 0x90
.LBB1_145:                              # %.loopexit
                                        #   in Loop: Header=BB1_44 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	_ZN3Exp8toStringEj
	testq	%rax, %rax
	je	.LBB1_149
# BB#146:                               #   in Loop: Header=BB1_44 Depth=1
	movq	%rax, %rbp
	movq	8(%r13), %rbx
	movl	$123, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	movl	$.L.str.20, %esi
	movl	$.L.str.19, %eax
	cmovneq	%rax, %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %r15
	movq	%rbp, %rdx
	callq	fprintf
	movq	8(%r13), %rdi
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	16(%rax), %edx
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	8(%r13), %rsi
	movl	$10, %edi
	callq	fputc
	movl	indentlevel(%rip), %ebx
	testl	%ebx, %ebx
	je	.LBB1_148
	.p2align	4, 0x90
.LBB1_147:                              # %.lr.ph365
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	decl	%ebx
	jne	.LBB1_147
.LBB1_148:                              # %._crit_edge366
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%r15, %rbx
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%rbx, %r9
	jmp	.LBB1_150
.LBB1_149:                              #   in Loop: Header=BB1_44 Depth=1
	xorl	%r9d, %r9d
.LBB1_150:                              # %.critedge.backedge
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB1_151:                              # %.critedge.backedge
                                        #   in Loop: Header=BB1_44 Depth=1
	movq	%r14, %rbx
.LBB1_152:                              # %.critedge.backedge
                                        #   in Loop: Header=BB1_44 Depth=1
	cmpq	donestkptr(%rip), %rbp
	jne	.LBB1_44
	jmp	.LBB1_154
.LBB1_153:                              #   in Loop: Header=BB1_44 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%r15, %rbp
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rbx
	movq	%r12, %r9
	cmpq	donestkptr(%rip), %rbp
	jne	.LBB1_44
.LBB1_154:                              # %.critedge._crit_edge
	movq	8(%r13), %rcx
	movl	$.L.str.23, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
.LBB1_155:                              # %.thread333
	xorl	%ecx, %ecx
.LBB1_156:                              # %.thread333
	movl	%ecx, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_158:
	movl	$1, %ecx
	jmp	.LBB1_156
.LBB1_159:
.Ltmp2:
	jmp	.LBB1_161
.LBB1_160:
.Ltmp5:
.LBB1_161:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z14decompileblockP9ClassfileP11method_info, .Lfunc_end1-_Z14decompileblockP9ClassfileP11method_info
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp0-.Ltmp4           #   Call between .Ltmp4 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end1-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%d"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	", "
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"]\n"
	.size	.L.str.3, 3

	.type	ch,@object              # @ch
	.bss
	.globl	ch
	.p2align	2
ch:
	.long	0                       # 0x0
	.size	ch, 4

	.type	inbuff,@object          # @inbuff
	.globl	inbuff
	.p2align	3
inbuff:
	.quad	0
	.size	inbuff, 8

	.type	bufflength,@object      # @bufflength
	.globl	bufflength
	.p2align	2
bufflength:
	.long	0                       # 0x0
	.size	bufflength, 4

	.type	currpc,@object          # @currpc
	.globl	currpc
	.p2align	2
currpc:
	.long	0                       # 0x0
	.size	currpc, 4

	.type	lastaction,@object      # @lastaction
	.globl	lastaction
	.p2align	2
lastaction:
	.long	0                       # 0x0
	.size	lastaction, 4

	.type	miptr,@object           # @miptr
	.globl	miptr
	.p2align	3
miptr:
	.quad	0
	.size	miptr, 8

	.type	stack,@object           # @stack
	.globl	stack
	.p2align	4
stack:
	.zero	64
	.size	stack, 64

	.type	stkptr,@object          # @stkptr
	.globl	stkptr
	.p2align	3
stkptr:
	.quad	0
	.size	stkptr, 8

	.type	donestack,@object       # @donestack
	.globl	donestack
	.p2align	4
donestack:
	.zero	2048
	.size	donestack, 2048

	.type	donestkptr,@object      # @donestkptr
	.globl	donestkptr
	.p2align	3
donestkptr:
	.quad	0
	.size	donestkptr, 8

	.type	indentlevel,@object     # @indentlevel
	.globl	indentlevel
	.p2align	2
indentlevel:
	.long	0                       # 0x0
	.size	indentlevel, 4

	.type	actions,@object         # @actions
	.data
	.globl	actions
	.p2align	4
actions:
	.asciz	"\000\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\002\002\003\003\003\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\017\000\000\000\017\000\000\000\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\000\000\000\000\022\000\000\000\025\000\r\031\000\000\000\000\000\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\007\007\007\007\006\006\006\006\006\006\006\006\006\006\006\006\020\007\007\007\007\007\007\007\007\007\007\007\007\007\007\000\026\026\026\026\026\f\f\f\f\f\f\027\027\027\027\027\027\027\027\007\000\000\033\030\n\n\n\n\n\n\b\t\b\t\013\013\013\013\007\007\000\023\016\007\021\032\000\000\000\024\f\f\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	actions, 256

	.type	actiontable,@object     # @actiontable
	.globl	actiontable
	.p2align	4
actiontable:
	.quad	0
	.quad	_Z7pushimpP9Classfile
	.quad	_Z7pushimmP9Classfile
	.quad	_Z9pushconstP9Classfile
	.quad	_Z9pushlocalP9Classfile
	.quad	_Z10storelocalP9Classfile
	.quad	_Z9pushbinopP9Classfile
	.quad	_Z8pushunopP9Classfile
	.quad	_Z5dogetP9Classfile
	.quad	_Z5doputP9Classfile
	.quad	_Z8doreturnP9Classfile
	.quad	_Z10invokefuncP9Classfile
	.quad	_Z5doif1P9Classfile
	.quad	_Z5dodupP9Classfile
	.quad	_Z13doarraylengthP9Classfile
	.quad	_Z10doarraygetP9Classfile
	.quad	_Z9iinclocalP9Classfile
	.quad	_Z11docheckcastP9Classfile
	.quad	_Z10doarrayputP9Classfile
	.quad	_Z9anewarrayP9Classfile
	.quad	_Z14multianewarrayP9Classfile
	.quad	_Z5dopopP9Classfile
	.quad	_Z5docmpP9Classfile
	.quad	_Z5doif2P9Classfile
	.quad	_Z10doluswitchP9Classfile
	.quad	_Z8dodup_x1P9Classfile
	.quad	_Z12doinstanceofP9Classfile
	.quad	_Z13dotableswitchP9Classfile
	.size	actiontable, 224

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"\n  %s"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" throws %s"
	.size	.L.str.6, 11

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"//    unknown opcode 0x%02X\n"
	.size	.L.str.7, 29

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" {\n"
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"    do {\n"
	.size	.L.str.9, 10

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"  "
	.size	.L.str.10, 3

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"    while (%s) {\n"
	.size	.L.str.11, 18

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"  }\n"
	.size	.L.str.12, 5

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"  } while(true);\t/*%d*/\n"
	.size	.L.str.13, 25

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"  } while(%s);\t/*%d*/\n"
	.size	.L.str.14, 23

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"  }\t/*%d*/\n"
	.size	.L.str.15, 12

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"    break;\t/*%d*/\n"
	.size	.L.str.16, 19

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"  } else {\t/*%d*/\n"
	.size	.L.str.17, 19

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"    %s;\t/*%d*/\n"
	.size	.L.str.18, 16

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"    %s"
	.size	.L.str.19, 7

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"    %s;"
	.size	.L.str.20, 8

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\t/*%d*/"
	.size	.L.str.21, 8

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"  }"
	.size	.L.str.23, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
