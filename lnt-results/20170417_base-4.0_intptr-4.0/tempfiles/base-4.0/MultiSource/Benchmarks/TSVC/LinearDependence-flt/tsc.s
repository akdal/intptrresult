	.text
	.file	"tsc.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI0_1:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI0_2:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
.LCPI0_3:
	.quad	5                       # 0x5
	.quad	5                       # 0x5
.LCPI0_4:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
.LCPI0_5:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_6:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.text
	.globl	set1d
	.p2align	4, 0x90
	.type	set1d,@function
set1d:                                  # @set1d
	.cfi_startproc
# BB#0:
	cmpl	$-1, %esi
	je	.LBB0_4
# BB#1:
	cmpl	$-2, %esi
	jne	.LBB0_6
# BB#2:                                 # %vector.body.preheader
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [2,3]
	movl	$4, %eax
	movdqa	.LCPI0_1(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI0_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI0_3(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI0_4(%rip), %xmm5   # xmm5 = [8,8]
	.p2align	4, 0x90
.LBB0_3:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movups	%xmm6, -16(%rdi,%rax,4)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movups	%xmm6, (%rdi,%rax,4)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$8, %rax
	cmpq	$32004, %rax            # imm = 0x7D04
	jne	.LBB0_3
	jmp	.LBB0_22
.LBB0_4:                                # %vector.body43.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [2,3]
	movl	$4, %eax
	movdqa	.LCPI0_5(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI0_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI0_6(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI0_4(%rip), %xmm5   # xmm5 = [8,8]
	.p2align	4, 0x90
.LBB0_5:                                # %vector.body43
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movups	%xmm3, -16(%rdi,%rax,4)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movups	%xmm6, (%rdi,%rax,4)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$8, %rax
	cmpq	$32004, %rax            # imm = 0x7D04
	jne	.LBB0_5
	jmp	.LBB0_22
.LBB0_6:                                # %.preheader.preheader
	movslq	%esi, %r9
	movl	$31999, %eax            # imm = 0x7CFF
	xorl	%edx, %edx
	divq	%r9
	incq	%rax
	cmpq	$8, %rax
	jae	.LBB0_8
# BB#7:
	xorl	%edx, %edx
	jmp	.LBB0_21
.LBB0_8:                                # %min.iters.checked57
	movq	%rax, %rdx
	andq	$65528, %rdx            # imm = 0xFFF8
	je	.LBB0_9
# BB#10:                                # %vector.scevcheck
	cmpl	$1, %esi
	jne	.LBB0_11
# BB#12:                                # %vector.ph59
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	-8(%rdx), %r8
	movl	%r8d, %r10d
	shrl	$3, %r10d
	incl	%r10d
	andq	$3, %r10
	je	.LBB0_13
# BB#14:                                # %vector.body54.prol.preheader
	leaq	16(%rdi), %rcx
	movq	%r9, %r11
	shlq	$5, %r11
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_15:                               # %vector.body54.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, -16(%rcx)
	movups	%xmm1, (%rcx)
	addq	$8, %rsi
	addq	%r11, %rcx
	decq	%r10
	jne	.LBB0_15
	jmp	.LBB0_16
.LBB0_9:
	xorl	%edx, %edx
	jmp	.LBB0_21
.LBB0_11:
	xorl	%edx, %edx
	jmp	.LBB0_21
.LBB0_13:
	xorl	%esi, %esi
.LBB0_16:                               # %vector.body54.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB0_19
# BB#17:                                # %vector.ph59.new
	movq	%rsi, %rcx
	imulq	%r9, %rcx
	leaq	(%rdi,%rcx,4), %rcx
	movq	%r9, %r8
	shlq	$7, %r8
	movq	%r9, %r10
	shlq	$5, %r10
	.p2align	4, 0x90
.LBB0_18:                               # %vector.body54
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, (%rcx)
	movups	%xmm1, 16(%rcx)
	leaq	(%rcx,%r10), %r11
	movups	%xmm1, (%rcx,%r10)
	movups	%xmm1, 16(%rcx,%r10)
	movups	%xmm1, (%r10,%r11)
	movups	%xmm1, 16(%r10,%r11)
	leaq	(%r11,%r10), %r11
	movups	%xmm1, (%r10,%r11)
	movups	%xmm1, 16(%r10,%r11)
	addq	$32, %rsi
	addq	%r8, %rcx
	cmpq	%rdx, %rsi
	jne	.LBB0_18
.LBB0_19:                               # %middle.block55
	cmpq	%rdx, %rax
	je	.LBB0_22
# BB#20:
	imulq	%r9, %rdx
	.p2align	4, 0x90
.LBB0_21:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movss	%xmm0, (%rdi,%rdx,4)
	addq	%r9, %rdx
	cmpq	$32000, %rdx            # imm = 0x7D00
	jl	.LBB0_21
.LBB0_22:                               # %.loopexit
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	set1d, .Lfunc_end0-set1d
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI1_1:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI1_2:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
.LCPI1_3:
	.quad	5                       # 0x5
	.quad	5                       # 0x5
.LCPI1_4:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
.LCPI1_5:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI1_6:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.text
	.globl	set1ds
	.p2align	4, 0x90
	.type	set1ds,@function
set1ds:                                 # @set1ds
	.cfi_startproc
# BB#0:
	movl	%edx, %ecx
	cmpl	$-1, %ecx
	je	.LBB1_4
# BB#1:
	cmpl	$-2, %ecx
	jne	.LBB1_6
# BB#2:                                 # %vector.body.preheader
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI1_0(%rip), %xmm1   # xmm1 = [2,3]
	movl	$4, %eax
	movdqa	.LCPI1_1(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI1_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI1_3(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI1_4(%rip), %xmm5   # xmm5 = [8,8]
	.p2align	4, 0x90
.LBB1_3:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movups	%xmm6, -16(%rsi,%rax,4)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movups	%xmm6, (%rsi,%rax,4)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$8, %rax
	cmpq	$32004, %rax            # imm = 0x7D04
	jne	.LBB1_3
	jmp	.LBB1_22
.LBB1_4:                                # %vector.body43.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI1_0(%rip), %xmm1   # xmm1 = [2,3]
	movl	$4, %eax
	movdqa	.LCPI1_5(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI1_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI1_6(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI1_4(%rip), %xmm5   # xmm5 = [8,8]
	.p2align	4, 0x90
.LBB1_5:                                # %vector.body43
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movups	%xmm3, -16(%rsi,%rax,4)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movups	%xmm6, (%rsi,%rax,4)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$8, %rax
	cmpq	$32004, %rax            # imm = 0x7D04
	jne	.LBB1_5
	jmp	.LBB1_22
.LBB1_6:                                # %.preheader.preheader
	movslq	%ecx, %r9
	movl	$31999, %eax            # imm = 0x7CFF
	xorl	%edx, %edx
	divq	%r9
	incq	%rax
	cmpq	$8, %rax
	jae	.LBB1_8
# BB#7:
	xorl	%edx, %edx
	jmp	.LBB1_21
.LBB1_8:                                # %min.iters.checked57
	movq	%rax, %rdx
	andq	$65528, %rdx            # imm = 0xFFF8
	je	.LBB1_9
# BB#10:                                # %vector.scevcheck
	cmpl	$1, %ecx
	jne	.LBB1_11
# BB#12:                                # %vector.ph59
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	-8(%rdx), %r8
	movl	%r8d, %r10d
	shrl	$3, %r10d
	incl	%r10d
	andq	$3, %r10
	je	.LBB1_13
# BB#14:                                # %vector.body54.prol.preheader
	leaq	16(%rsi), %rdi
	movq	%r9, %r11
	shlq	$5, %r11
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_15:                               # %vector.body54.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$8, %rcx
	addq	%r11, %rdi
	decq	%r10
	jne	.LBB1_15
	jmp	.LBB1_16
.LBB1_9:
	xorl	%edx, %edx
	jmp	.LBB1_21
.LBB1_11:
	xorl	%edx, %edx
	jmp	.LBB1_21
.LBB1_13:
	xorl	%ecx, %ecx
.LBB1_16:                               # %vector.body54.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB1_19
# BB#17:                                # %vector.ph59.new
	movq	%rcx, %rdi
	imulq	%r9, %rdi
	leaq	(%rsi,%rdi,4), %rdi
	movq	%r9, %r8
	shlq	$7, %r8
	movq	%r9, %r10
	shlq	$5, %r10
	.p2align	4, 0x90
.LBB1_18:                               # %vector.body54
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, (%rdi)
	movups	%xmm1, 16(%rdi)
	leaq	(%rdi,%r10), %r11
	movups	%xmm1, (%rdi,%r10)
	movups	%xmm1, 16(%rdi,%r10)
	movups	%xmm1, (%r10,%r11)
	movups	%xmm1, 16(%r10,%r11)
	leaq	(%r11,%r10), %r11
	movups	%xmm1, (%r10,%r11)
	movups	%xmm1, 16(%r10,%r11)
	addq	$32, %rcx
	addq	%r8, %rdi
	cmpq	%rdx, %rcx
	jne	.LBB1_18
.LBB1_19:                               # %middle.block55
	cmpq	%rdx, %rax
	je	.LBB1_22
# BB#20:
	imulq	%r9, %rdx
	.p2align	4, 0x90
.LBB1_21:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movss	%xmm0, (%rsi,%rdx,4)
	addq	%r9, %rdx
	cmpq	$32000, %rdx            # imm = 0x7D00
	jl	.LBB1_21
.LBB1_22:                               # %.loopexit
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	set1ds, .Lfunc_end1-set1ds
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1065353216              # float 1
	.text
	.globl	set2d
	.p2align	4, 0x90
	.type	set2d,@function
set2d:                                  # @set2d
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%esi, %r8d
	cmpl	$-1, %r8d
	je	.LBB2_4
# BB#1:
	cmpl	$-2, %r8d
	jne	.LBB2_6
# BB#2:                                 # %.preheader48.preheader
	addq	$1008, %rdi             # imm = 0x3F0
	movl	$1, %eax
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB2_3:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ecx
	imull	%ecx, %ecx
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movups	%xmm1, -1008(%rdi)
	movups	%xmm1, -992(%rdi)
	movups	%xmm1, -976(%rdi)
	movups	%xmm1, -960(%rdi)
	movups	%xmm1, -944(%rdi)
	movups	%xmm1, -928(%rdi)
	movups	%xmm1, -912(%rdi)
	movups	%xmm1, -896(%rdi)
	movups	%xmm1, -880(%rdi)
	movups	%xmm1, -864(%rdi)
	movups	%xmm1, -848(%rdi)
	movups	%xmm1, -832(%rdi)
	movups	%xmm1, -816(%rdi)
	movups	%xmm1, -800(%rdi)
	movups	%xmm1, -784(%rdi)
	movups	%xmm1, -768(%rdi)
	movups	%xmm1, -752(%rdi)
	movups	%xmm1, -736(%rdi)
	movups	%xmm1, -720(%rdi)
	movups	%xmm1, -704(%rdi)
	movups	%xmm1, -688(%rdi)
	movups	%xmm1, -672(%rdi)
	movups	%xmm1, -656(%rdi)
	movups	%xmm1, -640(%rdi)
	movups	%xmm1, -624(%rdi)
	movups	%xmm1, -608(%rdi)
	movups	%xmm1, -592(%rdi)
	movups	%xmm1, -576(%rdi)
	movups	%xmm1, -560(%rdi)
	movups	%xmm1, -544(%rdi)
	movups	%xmm1, -528(%rdi)
	movups	%xmm1, -512(%rdi)
	movups	%xmm1, -496(%rdi)
	movups	%xmm1, -480(%rdi)
	movups	%xmm1, -464(%rdi)
	movups	%xmm1, -448(%rdi)
	movups	%xmm1, -432(%rdi)
	movups	%xmm1, -416(%rdi)
	movups	%xmm1, -400(%rdi)
	movups	%xmm1, -384(%rdi)
	movups	%xmm1, -368(%rdi)
	movups	%xmm1, -352(%rdi)
	movups	%xmm1, -336(%rdi)
	movups	%xmm1, -320(%rdi)
	movups	%xmm1, -304(%rdi)
	movups	%xmm1, -288(%rdi)
	movups	%xmm1, -272(%rdi)
	movups	%xmm1, -256(%rdi)
	movups	%xmm1, -240(%rdi)
	movups	%xmm1, -224(%rdi)
	movups	%xmm1, -208(%rdi)
	movups	%xmm1, -192(%rdi)
	movups	%xmm1, -176(%rdi)
	movups	%xmm1, -160(%rdi)
	movups	%xmm1, -144(%rdi)
	movups	%xmm1, -128(%rdi)
	movups	%xmm1, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	%xmm1, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	%xmm1, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	%xmm1, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$1024, %rdi             # imm = 0x400
	incq	%rax
	cmpq	$257, %rax              # imm = 0x101
	jne	.LBB2_3
	jmp	.LBB2_22
.LBB2_4:                                # %.preheader44.preheader
	addq	$1008, %rdi             # imm = 0x3F0
	movq	$-256, %rax
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB2_5:                                # %vector.body82
                                        # =>This Inner Loop Header: Depth=1
	leal	257(%rax), %ecx
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movups	%xmm1, -1008(%rdi)
	movups	%xmm1, -992(%rdi)
	movups	%xmm1, -976(%rdi)
	movups	%xmm1, -960(%rdi)
	movups	%xmm1, -944(%rdi)
	movups	%xmm1, -928(%rdi)
	movups	%xmm1, -912(%rdi)
	movups	%xmm1, -896(%rdi)
	movups	%xmm1, -880(%rdi)
	movups	%xmm1, -864(%rdi)
	movups	%xmm1, -848(%rdi)
	movups	%xmm1, -832(%rdi)
	movups	%xmm1, -816(%rdi)
	movups	%xmm1, -800(%rdi)
	movups	%xmm1, -784(%rdi)
	movups	%xmm1, -768(%rdi)
	movups	%xmm1, -752(%rdi)
	movups	%xmm1, -736(%rdi)
	movups	%xmm1, -720(%rdi)
	movups	%xmm1, -704(%rdi)
	movups	%xmm1, -688(%rdi)
	movups	%xmm1, -672(%rdi)
	movups	%xmm1, -656(%rdi)
	movups	%xmm1, -640(%rdi)
	movups	%xmm1, -624(%rdi)
	movups	%xmm1, -608(%rdi)
	movups	%xmm1, -592(%rdi)
	movups	%xmm1, -576(%rdi)
	movups	%xmm1, -560(%rdi)
	movups	%xmm1, -544(%rdi)
	movups	%xmm1, -528(%rdi)
	movups	%xmm1, -512(%rdi)
	movups	%xmm1, -496(%rdi)
	movups	%xmm1, -480(%rdi)
	movups	%xmm1, -464(%rdi)
	movups	%xmm1, -448(%rdi)
	movups	%xmm1, -432(%rdi)
	movups	%xmm1, -416(%rdi)
	movups	%xmm1, -400(%rdi)
	movups	%xmm1, -384(%rdi)
	movups	%xmm1, -368(%rdi)
	movups	%xmm1, -352(%rdi)
	movups	%xmm1, -336(%rdi)
	movups	%xmm1, -320(%rdi)
	movups	%xmm1, -304(%rdi)
	movups	%xmm1, -288(%rdi)
	movups	%xmm1, -272(%rdi)
	movups	%xmm1, -256(%rdi)
	movups	%xmm1, -240(%rdi)
	movups	%xmm1, -224(%rdi)
	movups	%xmm1, -208(%rdi)
	movups	%xmm1, -192(%rdi)
	movups	%xmm1, -176(%rdi)
	movups	%xmm1, -160(%rdi)
	movups	%xmm1, -144(%rdi)
	movups	%xmm1, -128(%rdi)
	movups	%xmm1, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	%xmm1, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	%xmm1, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	%xmm1, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$1024, %rdi             # imm = 0x400
	incq	%rax
	jne	.LBB2_5
	jmp	.LBB2_22
.LBB2_6:                                # %.preheader.preheader
	movslq	%r8d, %r11
	movl	$255, %eax
	xorl	%edx, %edx
	divq	%r11
	incq	%rax
	movl	%eax, %esi
	andl	$504, %esi              # imm = 0x1F8
	leaq	-8(%rsi), %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movl	%ecx, %r10d
	shrl	$3, %r10d
	incl	%r10d
	movq	%r11, %rcx
	imulq	%rsi, %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	andl	$3, %r10d
	movq	%r11, %r12
	shlq	$5, %r12
	leaq	(,%r11,4), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	%r11, %rcx
	shlq	$7, %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	leaq	16(%rdi), %r9
	movl	%r8d, -52(%rsp)         # 4-byte Spill
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	%r10, -24(%rsp)         # 8-byte Spill
	jmp	.LBB2_7
.LBB2_14:                               #   in Loop: Header=BB2_7 Depth=1
	xorl	%edx, %edx
.LBB2_17:                               # %vector.body97.prol.loopexit
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpq	$24, -16(%rsp)          # 8-byte Folded Reload
	jb	.LBB2_20
# BB#18:                                # %vector.ph102.new
                                        #   in Loop: Header=BB2_7 Depth=1
	leaq	8(%rdx), %rbp
	movq	-40(%rsp), %rax         # 8-byte Reload
	imulq	%rax, %rbp
	leaq	24(%rdx), %r8
	imulq	%rax, %r8
	movq	%rax, %r14
	imulq	%rdx, %r14
	leaq	16(%rdx), %r10
	imulq	%rax, %r10
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	%r9, %r13
	.p2align	4, 0x90
.LBB2_19:                               # %vector.body97
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm1, -16(%r14,%r13)
	movups	%xmm1, (%r14,%r13)
	movups	%xmm1, -16(%rbp,%r13)
	movups	%xmm1, (%rbp,%r13)
	movups	%xmm1, -16(%r10,%r13)
	movups	%xmm1, (%r10,%r13)
	movups	%xmm1, -16(%r8,%r13)
	movups	%xmm1, (%r8,%r13)
	addq	$32, %rdx
	addq	%rax, %r13
	cmpq	%rsi, %rdx
	jne	.LBB2_19
.LBB2_20:                               # %middle.block98
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	-8(%rsp), %rax          # 8-byte Reload
	cmpq	%rsi, %rax
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	-52(%rsp), %r8d         # 4-byte Reload
	movq	-24(%rsp), %r10         # 8-byte Reload
	jne	.LBB2_9
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_7:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_16 Depth 2
                                        #     Child Loop BB2_19 Depth 2
                                        #     Child Loop BB2_10 Depth 2
	cmpq	$7, %rax
	jbe	.LBB2_8
# BB#11:                                # %min.iters.checked100
                                        #   in Loop: Header=BB2_7 Depth=1
	testq	%rsi, %rsi
	je	.LBB2_8
# BB#12:                                # %vector.scevcheck
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpl	$1, %r8d
	jne	.LBB2_8
# BB#13:                                # %vector.ph102
                                        #   in Loop: Header=BB2_7 Depth=1
	testq	%r10, %r10
	je	.LBB2_14
# BB#15:                                # %vector.body97.prol.preheader
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%r9, %rbp
	xorl	%edx, %edx
	movq	%r10, %rbx
	.p2align	4, 0x90
.LBB2_16:                               # %vector.body97.prol
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm1, -16(%rbp)
	movups	%xmm1, (%rbp)
	addq	$8, %rdx
	addq	%r12, %rbp
	decq	%rbx
	jne	.LBB2_16
	jmp	.LBB2_17
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_7 Depth=1
	xorl	%edx, %edx
.LBB2_9:                                # %scalar.ph99.preheader
                                        #   in Loop: Header=BB2_7 Depth=1
	leaq	(%rdi,%rdx,4), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_10:                               # %scalar.ph99
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	%xmm0, (%rbp,%rbx,4)
	addq	%r11, %rbx
	leaq	(%rdx,%rbx), %rcx
	cmpq	$256, %rcx              # imm = 0x100
	jl	.LBB2_10
.LBB2_21:                               # %.loopexit115
                                        #   in Loop: Header=BB2_7 Depth=1
	incq	%r15
	addq	$1024, %r9              # imm = 0x400
	addq	$1024, %rdi             # imm = 0x400
	cmpq	$256, %r15              # imm = 0x100
	jne	.LBB2_7
.LBB2_22:                               # %.loopexit
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	set2d, .Lfunc_end2-set2d
	.cfi_endproc

	.globl	sum1d
	.p2align	4, 0x90
	.type	sum1d,@function
sum1d:                                  # @sum1d
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movl	$7, %eax
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	addss	-28(%rdi,%rax,4), %xmm0
	addss	-24(%rdi,%rax,4), %xmm0
	addss	-20(%rdi,%rax,4), %xmm0
	addss	-16(%rdi,%rax,4), %xmm0
	addss	-12(%rdi,%rax,4), %xmm0
	addss	-8(%rdi,%rax,4), %xmm0
	addss	-4(%rdi,%rax,4), %xmm0
	addss	(%rdi,%rax,4), %xmm0
	addq	$8, %rax
	cmpq	$32007, %rax            # imm = 0x7D07
	jne	.LBB3_1
# BB#2:
	retq
.Lfunc_end3:
	.size	sum1d, .Lfunc_end3-sum1d
	.cfi_endproc

	.globl	check
	.p2align	4, 0x90
	.type	check,@function
check:                                  # @check
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	xorps	%xmm3, %xmm3
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	xorps	%xmm4, %xmm4
	xorps	%xmm2, %xmm2
	xorps	%xmm1, %xmm1
	xorps	%xmm8, %xmm8
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm8
	addss	global_data+256016(%rax), %xmm1
	addss	global_data+384048(%rax), %xmm2
	addss	global_data+512080(%rax), %xmm4
	addss	global_data+640128(%rax), %xmm3
	addss	global_data+128004(%rax), %xmm8
	addss	global_data+256020(%rax), %xmm1
	addss	global_data+384052(%rax), %xmm2
	addss	global_data+512084(%rax), %xmm4
	addss	global_data+640132(%rax), %xmm3
	addq	$8, %rax
	jne	.LBB4_1
# BB#2:                                 # %.preheader111.preheader
	xorps	%xmm5, %xmm5
	xorl	%eax, %eax
	movl	$global_data+1164644, %ecx
	xorps	%xmm7, %xmm7
	xorps	%xmm6, %xmm6
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader111
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	-524452(%rsi), %xmm6
	addss	-262228(%rsi), %xmm7
	addss	-4(%rsi), %xmm5
	addss	-524448(%rsi), %xmm6
	addss	-262224(%rsi), %xmm7
	addss	(%rsi), %xmm5
	addq	$8, %rsi
	addq	$-2, %rdx
	jne	.LBB4_4
# BB#5:                                 #   in Loop: Header=BB4_3 Depth=1
	incq	%rax
	addq	$1024, %rcx             # imm = 0x400
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB4_3
# BB#6:                                 # %.preheader.preheader
	xorps	%xmm0, %xmm0
	movq	$-262144, %rax          # imm = 0xFFFC0000
	.p2align	4, 0x90
.LBB4_7:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addss	array+262144(%rax), %xmm0
	addss	array+262148(%rax), %xmm0
	addss	array+262152(%rax), %xmm0
	addss	array+262156(%rax), %xmm0
	addss	array+262160(%rax), %xmm0
	addss	array+262164(%rax), %xmm0
	addss	array+262168(%rax), %xmm0
	addss	array+262172(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB4_7
# BB#8:
	leal	12(%rdi), %eax
	cmpl	$135, %eax
	ja	.LBB4_11
# BB#9:
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_10:
	movl	digits(%rip), %esi
	addss	temp(%rip), %xmm1
	jmp	.LBB4_27
.LBB4_11:
	cmpl	$1122, %edi             # imm = 0x462
	je	.LBB4_32
# BB#12:
	cmpl	$112233, %edi           # imm = 0x1B669
	jne	.LBB4_14
# BB#13:
	movl	digits(%rip), %esi
	addss	%xmm7, %xmm6
	addss	%xmm5, %xmm6
	jmp	.LBB4_33
.LBB4_14:
	retq
.LBB4_15:
	movl	digits(%rip), %esi
	movss	temp(%rip), %xmm0       # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	jmp	.LBB4_34
.LBB4_16:
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	jmp	.LBB4_34
.LBB4_17:
	movl	digits(%rip), %esi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm8, %xmm0
	jmp	.LBB4_34
.LBB4_18:
	movl	digits(%rip), %esi
	jmp	.LBB4_27
.LBB4_19:
	movl	digits(%rip), %esi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm0
	jmp	.LBB4_34
.LBB4_20:
	movl	digits(%rip), %esi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm4, %xmm0
	jmp	.LBB4_34
.LBB4_21:
	movl	digits(%rip), %esi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm3, %xmm0
	jmp	.LBB4_34
.LBB4_22:
	movl	digits(%rip), %esi
	jmp	.LBB4_33
.LBB4_23:
	movl	digits(%rip), %esi
	addss	%xmm1, %xmm8
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm8, %xmm0
	jmp	.LBB4_34
.LBB4_24:
	movl	digits(%rip), %esi
	jmp	.LBB4_31
.LBB4_25:
	movl	digits(%rip), %esi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm7, %xmm0
	jmp	.LBB4_34
.LBB4_26:
	movl	digits(%rip), %esi
	addss	%xmm3, %xmm1
.LBB4_27:
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	jmp	.LBB4_34
.LBB4_28:
	movl	digits(%rip), %esi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm5, %xmm0
	jmp	.LBB4_34
.LBB4_29:
	movl	digits(%rip), %esi
	addss	%xmm6, %xmm8
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm8, %xmm0
	jmp	.LBB4_34
.LBB4_30:
	movl	digits(%rip), %esi
	addss	%xmm1, %xmm8
.LBB4_31:
	addss	%xmm2, %xmm8
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm8, %xmm0
	jmp	.LBB4_34
.LBB4_32:
	movl	digits(%rip), %esi
	addss	%xmm7, %xmm6
.LBB4_33:
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm6, %xmm0
.LBB4_34:
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	check, .Lfunc_end4-check
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_10
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_15
	.quad	.LBB4_16
	.quad	.LBB4_17
	.quad	.LBB4_18
	.quad	.LBB4_19
	.quad	.LBB4_20
	.quad	.LBB4_21
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_22
	.quad	.LBB4_23
	.quad	.LBB4_24
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_25
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_26
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_28
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_29
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_30

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI5_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI5_2:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
.LCPI5_3:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
.LCPI5_4:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
.LCPI5_6:
	.long	1073741824              # float 2
	.long	1073741824              # float 2
	.long	1073741824              # float 2
	.long	1073741824              # float 2
.LCPI5_7:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
.LCPI5_8:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI5_9:
	.quad	5                       # 0x5
	.quad	5                       # 0x5
.LCPI5_10:
	.long	3212836864              # float -1
	.long	3212836864              # float -1
	.long	3212836864              # float -1
	.long	3212836864              # float -1
.LCPI5_11:
	.long	897988541               # float 9.99999997E-7
	.long	897988541               # float 9.99999997E-7
	.long	897988541               # float 9.99999997E-7
	.long	897988541               # float 9.99999997E-7
.LCPI5_12:
	.long	1065353224              # float 1.00000095
	.long	1065353224              # float 1.00000095
	.long	1065353224              # float 1.00000095
	.long	1065353224              # float 1.00000095
.LCPI5_13:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
.LCPI5_14:
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
.LCPI5_15:
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_5:
	.long	1065353216              # float 1
	.text
	.globl	init
	.p2align	4, 0x90
	.type	init,@function
init:                                   # @init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str.1, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_1
# BB#3:
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_4
# BB#14:
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_15
# BB#19:
	movl	$.L.str.4, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_20
# BB#24:
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_25
# BB#29:
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_30
# BB#38:
	movl	$.L.str.7, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_39
# BB#41:
	movl	$.L.str.8, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_42
# BB#46:
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_47
# BB#51:
	movl	$.L.str.10, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_52
# BB#56:
	movl	$.L.str.11, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_57
# BB#61:
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_62
# BB#70:
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_71
# BB#79:
	movl	$.L.str.14, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_80
# BB#86:
	movl	$.L.str.15, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_87
# BB#93:
	movl	$.L.str.16, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_94
# BB#102:
	movl	$.L.str.17, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_103
# BB#109:
	movl	$.L.str.18, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_110
# BB#114:
	movl	$.L.str.19, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_115
# BB#121:
	movl	$.L.str.20, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_122
# BB#126:
	movl	$.L.str.21, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_127
# BB#131:
	movl	$.L.str.22, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_132
# BB#140:
	movl	$.L.str.23, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_141
# BB#153:
	movl	$.L.str.24, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_154
# BB#160:
	movl	$.L.str.25, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_161
# BB#165:
	movl	$.L.str.26, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_166
# BB#170:
	movl	$.L.str.27, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_171
# BB#175:
	movl	$.L.str.28, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_176
# BB#180:
	movl	$.L.str.29, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_181
# BB#185:
	movl	$.L.str.30, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_186
# BB#192:
	movl	$.L.str.31, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_193
# BB#201:
	movl	$.L.str.32, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_202
# BB#210:
	movl	$.L.str.33, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_211
# BB#219:
	movl	$.L.str.34, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_220
# BB#224:
	movl	$.L.str.35, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_225
# BB#229:
	movl	$.L.str.36, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_230
# BB#234:
	movl	$.L.str.37, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_235
# BB#241:
	movl	$.L.str.38, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_242
# BB#248:
	movl	$.L.str.39, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_249
# BB#259:
	movl	$.L.str.40, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_260
# BB#268:
	movl	$.L.str.41, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_269
# BB#277:
	movl	$.L.str.42, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_278
# BB#286:
	movl	$.L.str.43, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_287
# BB#293:
	movl	$.L.str.44, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_294
# BB#302:
	movl	$.L.str.45, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_303
# BB#307:
	movl	$.L.str.46, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_308
# BB#316:
	movl	$.L.str.47, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_317
# BB#319:
	movl	$.L.str.48, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_320
# BB#322:
	movl	$.L.str.49, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_323
# BB#329:
	movl	$.L.str.50, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_330
# BB#336:
	movl	$.L.str.51, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_337
# BB#345:
	movl	$.L.str.52, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_346
# BB#354:
	movl	$.L.str.53, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_355
# BB#361:
	movl	$.L.str.54, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_362
# BB#372:
	movl	$.L.str.55, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_373
# BB#383:
	movl	$.L.str.56, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_384
# BB#392:
	movl	$.L.str.57, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_393
# BB#399:
	movl	$.L.str.58, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_400
# BB#408:
	movl	$.L.str.59, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_409
# BB#421:
	movl	$.L.str.60, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_422
# BB#434:
	movl	$.L.str.61, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_435
# BB#447:
	movl	$.L.str.62, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_448
# BB#458:
	movl	$.L.str.63, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_459
# BB#465:
	movl	$.L.str.64, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_466
# BB#472:
	movl	$.L.str.65, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_473
# BB#477:
	movl	$.L.str.66, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_478
# BB#480:
	movl	$.L.str.67, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_481
# BB#483:
	movl	$.L.str.68, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_484
# BB#486:
	movl	$.L.str.69, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_487
# BB#493:
	movl	$.L.str.70, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_494
# BB#495:
	movl	$.L.str.71, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_494
# BB#496:
	movl	$.L.str.72, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_497
# BB#499:
	movl	$.L.str.73, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_500
# BB#502:
	movl	$.L.str.74, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_503
# BB#507:
	movl	$.L.str.75, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_508
# BB#510:
	movl	$.L.str.76, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_511
# BB#513:
	movl	$.L.str.77, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_514
# BB#516:
	movl	$.L.str.78, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_853
# BB#517:
	movl	$.L.str.79, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_518
# BB#521:
	movl	$.L.str.80, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_522
# BB#528:
	movl	$.L.str.81, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_529
# BB#532:
	movl	$.L.str.82, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_533
# BB#535:
	movl	$.L.str.83, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_536
# BB#540:
	movl	$.L.str.84, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_541
# BB#544:
	movl	$.L.str.85, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_545
# BB#547:
	movl	$.L.str.86, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_548
# BB#551:
	movl	$.L.str.87, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_552
# BB#562:
	movl	$.L.str.88, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_563
# BB#566:
	movl	$.L.str.89, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_567
# BB#570:
	movl	$.L.str.90, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_571
# BB#573:
	movl	$.L.str.91, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_574
# BB#578:
	movl	$.L.str.92, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_579
# BB#583:
	movl	$.L.str.93, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_584
# BB#589:
	movl	$.L.str.94, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_590
# BB#594:
	movl	$.L.str.95, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_595
# BB#600:
	movl	$.L.str.96, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_601
# BB#607:
	movl	$.L.str.97, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_608
# BB#614:
	movl	$.L.str.98, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_615
# BB#623:
	movl	$.L.str.99, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_624
# BB#630:
	movl	$.L.str.100, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_631
# BB#638:
	movl	$.L.str.101, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_639
# BB#643:
	movl	$.L.str.102, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_644
# BB#648:
	movl	$.L.str.103, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_649
# BB#651:
	movl	$.L.str.104, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_652
# BB#656:
	movl	$.L.str.105, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_657
# BB#661:
	movl	$.L.str.106, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_662
# BB#666:
	movl	$.L.str.107, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_667
# BB#677:
	movl	$.L.str.108, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_678
# BB#688:
	movl	$.L.str.109, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_689
# BB#695:
	movl	$.L.str.110, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_696
# BB#700:
	movl	$.L.str.111, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_701
# BB#705:
	movl	$.L.str.112, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_706
# BB#708:
	movl	$.L.str.113, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_709
# BB#719:
	movl	$.L.str.114, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_720
# BB#728:
	movl	$.L.str.115, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_729
# BB#735:
	movl	$.L.str.116, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_736
# BB#742:
	movl	$.L.str.117, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_743
# BB#747:
	movl	$.L.str.118, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_748
# BB#752:
	movl	$.L.str.119, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_753
# BB#759:
	movl	$.L.str.120, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_760
# BB#764:
	movl	$.L.str.121, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_765
# BB#769:
	movl	$.L.str.122, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_770
# BB#776:
	movl	$.L.str.123, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_777
# BB#783:
	cmpb	$118, (%rbx)
	jne	.LBB5_789
# BB#784:
	cmpb	$97, 1(%rbx)
	jne	.LBB5_789
# BB#785:
	cmpb	$9, 2(%rbx)
	jne	.LBB5_789
# BB#786:
	cmpb	$0, 3(%rbx)
	je	.LBB5_787
.LBB5_789:                              # %.thread
	movl	$.L.str.125, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_790
# BB#792:
	movl	$.L.str.126, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_793
# BB#795:
	movl	$.L.str.127, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_796
# BB#798:
	movl	$.L.str.128, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_799
# BB#801:
	movl	$.L.str.129, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_802
# BB#806:
	movl	$.L.str.130, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_807
# BB#813:
	movl	$.L.str.131, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_814
# BB#818:
	movl	$.L.str.132, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_819
# BB#825:
	movl	$.L.str.133, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_826
# BB#832:
	movl	$.L.str.134, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_833
# BB#835:
	movl	$.L.str.135, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_836
# BB#840:
	movl	$.L.str.136, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB5_853
# BB#841:                               # %vector.body.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_842:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm0, %xmm1
	divps	%xmm7, %xmm1
	movaps	%xmm1, global_data+128000(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_842
# BB#843:                               # %vector.body7626.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_844:                              # %vector.body7626
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+256016(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_844
# BB#845:                               # %vector.body7637.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_846:                              # %vector.body7637
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_846
# BB#847:                               # %vector.body7648.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_848:                              # %vector.body7648
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_848
# BB#849:                               # %vector.body7659.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_850:                              # %vector.body7659
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_850
# BB#851:                               # %.preheader44.i.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_852:                              # %vector.body7670
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_852
	jmp	.LBB5_853
.LBB5_1:                                # %vector.body11829.preheader
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_13(%rip), %xmm3  # xmm3 = [2,2,2,2]
	movdqa	.LCPI5_14(%rip), %xmm4  # xmm4 = [3,3,3,3]
	movdqa	.LCPI5_15(%rip), %xmm5  # xmm5 = [4,4]
	movdqa	.LCPI5_3(%rip), %xmm6   # xmm6 = [5,5,5,5]
	.p2align	4, 0x90
.LBB5_2:                                # %vector.body11829
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm0
	shufps	$136, %xmm1, %xmm0      # xmm0 = xmm0[0,2],xmm1[0,2]
	movaps	%xmm0, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm7, X+128000(%rax)
	movaps	%xmm0, %xmm7
	paddd	%xmm3, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm7, Y+128000(%rax)
	movaps	%xmm0, %xmm7
	paddd	%xmm4, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm7, Z+128000(%rax)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	movdqa	%xmm2, %xmm7
	shufps	$136, %xmm1, %xmm7      # xmm7 = xmm7[0,2],xmm1[0,2]
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm7, U+128000(%rax)
	paddd	%xmm6, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm0, V+128000(%rax)
	addq	$16, %rax
	jne	.LBB5_2
	jmp	.LBB5_853
.LBB5_4:                                # %vector.body11772.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_5:                                # %vector.body11772
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_5
# BB#6:                                 # %vector.body11785.preheader
	movl	$1, %eax
	movd	%rax, %xmm1
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm2   # xmm2 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
	.p2align	4, 0x90
.LBB5_7:                                # %vector.body11785
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm5, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm1, %xmm4
	paddq	%xmm8, %xmm4
	movdqa	%xmm4, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm4, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm4, %xmm4
	paddq	%xmm7, %xmm4
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm4      # xmm4 = xmm4[0,2],xmm6[0,2]
	cvtdq2ps	%xmm4, %xmm4
	movaps	%xmm0, %xmm6
	divps	%xmm4, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm5, %xmm4
	paddq	%xmm2, %xmm4
	movdqa	%xmm1, %xmm6
	paddq	%xmm2, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm4, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm4, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm4, %xmm4
	paddq	%xmm7, %xmm4
	shufps	$136, %xmm4, %xmm6      # xmm6 = xmm6[0,2],xmm4[0,2]
	cvtdq2ps	%xmm6, %xmm4
	movaps	%xmm0, %xmm6
	divps	%xmm4, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm1
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_7
# BB#8:                                 # %vector.body11796.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB5_9:                                # %vector.body11796
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm5, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm4, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+384048(%rax)
	movdqa	%xmm5, %xmm1
	paddq	%xmm2, %xmm1
	movdqa	%xmm4, %xmm6
	paddq	%xmm2, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_9
# BB#10:                                # %vector.body11807.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB5_11:                               # %vector.body11807
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm5, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm4, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+512080(%rax)
	movdqa	%xmm5, %xmm1
	paddq	%xmm2, %xmm1
	movdqa	%xmm4, %xmm6
	paddq	%xmm2, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_11
# BB#12:                                # %vector.body11818.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB5_13:                               # %vector.body11818
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm5, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm4, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+640128(%rax)
	movdqa	%xmm5, %xmm1
	paddq	%xmm2, %xmm1
	movdqa	%xmm4, %xmm6
	paddq	%xmm2, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_13
	jmp	.LBB5_853
.LBB5_15:                               # %vector.body11748.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_16:                               # %vector.body11748
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_16
# BB#17:                                # %vector.body11761.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
	.p2align	4, 0x90
.LBB5_18:                               # %vector.body11761
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_18
	jmp	.LBB5_853
.LBB5_20:                               # %vector.body11724.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_21:                               # %vector.body11724
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_21
# BB#22:                                # %vector.body11737.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
	.p2align	4, 0x90
.LBB5_23:                               # %vector.body11737
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_23
	jmp	.LBB5_853
.LBB5_25:                               # %.preheader44.i6885.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB5_26:                               # %vector.body11694
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_26
# BB#27:                                # %.preheader48.i6876.preheader
	movl	$global_data+903424, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB5_28:                               # %vector.body11709
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	imull	%edx, %edx
	cvtsi2ssl	%edx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, -1008(%rax)
	movaps	%xmm1, -992(%rax)
	movaps	%xmm1, -976(%rax)
	movaps	%xmm1, -960(%rax)
	movaps	%xmm1, -944(%rax)
	movaps	%xmm1, -928(%rax)
	movaps	%xmm1, -912(%rax)
	movaps	%xmm1, -896(%rax)
	movaps	%xmm1, -880(%rax)
	movaps	%xmm1, -864(%rax)
	movaps	%xmm1, -848(%rax)
	movaps	%xmm1, -832(%rax)
	movaps	%xmm1, -816(%rax)
	movaps	%xmm1, -800(%rax)
	movaps	%xmm1, -784(%rax)
	movaps	%xmm1, -768(%rax)
	movaps	%xmm1, -752(%rax)
	movaps	%xmm1, -736(%rax)
	movaps	%xmm1, -720(%rax)
	movaps	%xmm1, -704(%rax)
	movaps	%xmm1, -688(%rax)
	movaps	%xmm1, -672(%rax)
	movaps	%xmm1, -656(%rax)
	movaps	%xmm1, -640(%rax)
	movaps	%xmm1, -624(%rax)
	movaps	%xmm1, -608(%rax)
	movaps	%xmm1, -592(%rax)
	movaps	%xmm1, -576(%rax)
	movaps	%xmm1, -560(%rax)
	movaps	%xmm1, -544(%rax)
	movaps	%xmm1, -528(%rax)
	movaps	%xmm1, -512(%rax)
	movaps	%xmm1, -496(%rax)
	movaps	%xmm1, -480(%rax)
	movaps	%xmm1, -464(%rax)
	movaps	%xmm1, -448(%rax)
	movaps	%xmm1, -432(%rax)
	movaps	%xmm1, -416(%rax)
	movaps	%xmm1, -400(%rax)
	movaps	%xmm1, -384(%rax)
	movaps	%xmm1, -368(%rax)
	movaps	%xmm1, -352(%rax)
	movaps	%xmm1, -336(%rax)
	movaps	%xmm1, -320(%rax)
	movaps	%xmm1, -304(%rax)
	movaps	%xmm1, -288(%rax)
	movaps	%xmm1, -272(%rax)
	movaps	%xmm1, -256(%rax)
	movaps	%xmm1, -240(%rax)
	movaps	%xmm1, -224(%rax)
	movaps	%xmm1, -208(%rax)
	movaps	%xmm1, -192(%rax)
	movaps	%xmm1, -176(%rax)
	movaps	%xmm1, -160(%rax)
	movaps	%xmm1, -144(%rax)
	movaps	%xmm1, -128(%rax)
	movaps	%xmm1, -112(%rax)
	movaps	%xmm1, -96(%rax)
	movaps	%xmm1, -80(%rax)
	movaps	%xmm1, -64(%rax)
	movaps	%xmm1, -48(%rax)
	movaps	%xmm1, -32(%rax)
	movaps	%xmm1, -16(%rax)
	movaps	%xmm1, (%rax)
	addq	$1024, %rax             # imm = 0x400
	incq	%rcx
	cmpq	$257, %rcx              # imm = 0x101
	jne	.LBB5_28
	jmp	.LBB5_853
.LBB5_30:                               # %vector.body11642.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_31:                               # %vector.body11642
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_31
# BB#32:                                # %.preheader.i6862.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_11(%rip), %xmm0  # xmm0 = [1.000000e-06,1.000000e-06,1.000000e-06,1.000000e-06]
	.p2align	4, 0x90
.LBB5_33:                               # %vector.body11655
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+902336(%rax)
	movaps	%xmm0, global_data+902352(%rax)
	movaps	%xmm0, global_data+902368(%rax)
	movaps	%xmm0, global_data+902384(%rax)
	movaps	%xmm0, global_data+902400(%rax)
	movaps	%xmm0, global_data+902416(%rax)
	movaps	%xmm0, global_data+902432(%rax)
	movaps	%xmm0, global_data+902448(%rax)
	movaps	%xmm0, global_data+902464(%rax)
	movaps	%xmm0, global_data+902480(%rax)
	movaps	%xmm0, global_data+902496(%rax)
	movaps	%xmm0, global_data+902512(%rax)
	movaps	%xmm0, global_data+902528(%rax)
	movaps	%xmm0, global_data+902544(%rax)
	movaps	%xmm0, global_data+902560(%rax)
	movaps	%xmm0, global_data+902576(%rax)
	movaps	%xmm0, global_data+902592(%rax)
	movaps	%xmm0, global_data+902608(%rax)
	movaps	%xmm0, global_data+902624(%rax)
	movaps	%xmm0, global_data+902640(%rax)
	movaps	%xmm0, global_data+902656(%rax)
	movaps	%xmm0, global_data+902672(%rax)
	movaps	%xmm0, global_data+902688(%rax)
	movaps	%xmm0, global_data+902704(%rax)
	movaps	%xmm0, global_data+902720(%rax)
	movaps	%xmm0, global_data+902736(%rax)
	movaps	%xmm0, global_data+902752(%rax)
	movaps	%xmm0, global_data+902768(%rax)
	movaps	%xmm0, global_data+902784(%rax)
	movaps	%xmm0, global_data+902800(%rax)
	movaps	%xmm0, global_data+902816(%rax)
	movaps	%xmm0, global_data+902832(%rax)
	movaps	%xmm0, global_data+902848(%rax)
	movaps	%xmm0, global_data+902864(%rax)
	movaps	%xmm0, global_data+902880(%rax)
	movaps	%xmm0, global_data+902896(%rax)
	movaps	%xmm0, global_data+902912(%rax)
	movaps	%xmm0, global_data+902928(%rax)
	movaps	%xmm0, global_data+902944(%rax)
	movaps	%xmm0, global_data+902960(%rax)
	movaps	%xmm0, global_data+902976(%rax)
	movaps	%xmm0, global_data+902992(%rax)
	movaps	%xmm0, global_data+903008(%rax)
	movaps	%xmm0, global_data+903024(%rax)
	movaps	%xmm0, global_data+903040(%rax)
	movaps	%xmm0, global_data+903056(%rax)
	movaps	%xmm0, global_data+903072(%rax)
	movaps	%xmm0, global_data+903088(%rax)
	movaps	%xmm0, global_data+903104(%rax)
	movaps	%xmm0, global_data+903120(%rax)
	movaps	%xmm0, global_data+903136(%rax)
	movaps	%xmm0, global_data+903152(%rax)
	movaps	%xmm0, global_data+903168(%rax)
	movaps	%xmm0, global_data+903184(%rax)
	movaps	%xmm0, global_data+903200(%rax)
	movaps	%xmm0, global_data+903216(%rax)
	movaps	%xmm0, global_data+903232(%rax)
	movaps	%xmm0, global_data+903248(%rax)
	movaps	%xmm0, global_data+903264(%rax)
	movaps	%xmm0, global_data+903280(%rax)
	movaps	%xmm0, global_data+903296(%rax)
	movaps	%xmm0, global_data+903312(%rax)
	movaps	%xmm0, global_data+903328(%rax)
	movaps	%xmm0, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_33
# BB#34:                                # %.preheader.i6855.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	.p2align	4, 0x90
.LBB5_35:                               # %vector.body11668
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1164560(%rax)
	movaps	%xmm0, global_data+1164576(%rax)
	movaps	%xmm0, global_data+1164592(%rax)
	movaps	%xmm0, global_data+1164608(%rax)
	movaps	%xmm0, global_data+1164624(%rax)
	movaps	%xmm0, global_data+1164640(%rax)
	movaps	%xmm0, global_data+1164656(%rax)
	movaps	%xmm0, global_data+1164672(%rax)
	movaps	%xmm0, global_data+1164688(%rax)
	movaps	%xmm0, global_data+1164704(%rax)
	movaps	%xmm0, global_data+1164720(%rax)
	movaps	%xmm0, global_data+1164736(%rax)
	movaps	%xmm0, global_data+1164752(%rax)
	movaps	%xmm0, global_data+1164768(%rax)
	movaps	%xmm0, global_data+1164784(%rax)
	movaps	%xmm0, global_data+1164800(%rax)
	movaps	%xmm0, global_data+1164816(%rax)
	movaps	%xmm0, global_data+1164832(%rax)
	movaps	%xmm0, global_data+1164848(%rax)
	movaps	%xmm0, global_data+1164864(%rax)
	movaps	%xmm0, global_data+1164880(%rax)
	movaps	%xmm0, global_data+1164896(%rax)
	movaps	%xmm0, global_data+1164912(%rax)
	movaps	%xmm0, global_data+1164928(%rax)
	movaps	%xmm0, global_data+1164944(%rax)
	movaps	%xmm0, global_data+1164960(%rax)
	movaps	%xmm0, global_data+1164976(%rax)
	movaps	%xmm0, global_data+1164992(%rax)
	movaps	%xmm0, global_data+1165008(%rax)
	movaps	%xmm0, global_data+1165024(%rax)
	movaps	%xmm0, global_data+1165040(%rax)
	movaps	%xmm0, global_data+1165056(%rax)
	movaps	%xmm0, global_data+1165072(%rax)
	movaps	%xmm0, global_data+1165088(%rax)
	movaps	%xmm0, global_data+1165104(%rax)
	movaps	%xmm0, global_data+1165120(%rax)
	movaps	%xmm0, global_data+1165136(%rax)
	movaps	%xmm0, global_data+1165152(%rax)
	movaps	%xmm0, global_data+1165168(%rax)
	movaps	%xmm0, global_data+1165184(%rax)
	movaps	%xmm0, global_data+1165200(%rax)
	movaps	%xmm0, global_data+1165216(%rax)
	movaps	%xmm0, global_data+1165232(%rax)
	movaps	%xmm0, global_data+1165248(%rax)
	movaps	%xmm0, global_data+1165264(%rax)
	movaps	%xmm0, global_data+1165280(%rax)
	movaps	%xmm0, global_data+1165296(%rax)
	movaps	%xmm0, global_data+1165312(%rax)
	movaps	%xmm0, global_data+1165328(%rax)
	movaps	%xmm0, global_data+1165344(%rax)
	movaps	%xmm0, global_data+1165360(%rax)
	movaps	%xmm0, global_data+1165376(%rax)
	movaps	%xmm0, global_data+1165392(%rax)
	movaps	%xmm0, global_data+1165408(%rax)
	movaps	%xmm0, global_data+1165424(%rax)
	movaps	%xmm0, global_data+1165440(%rax)
	movaps	%xmm0, global_data+1165456(%rax)
	movaps	%xmm0, global_data+1165472(%rax)
	movaps	%xmm0, global_data+1165488(%rax)
	movaps	%xmm0, global_data+1165504(%rax)
	movaps	%xmm0, global_data+1165520(%rax)
	movaps	%xmm0, global_data+1165536(%rax)
	movaps	%xmm0, global_data+1165552(%rax)
	movaps	%xmm0, global_data+1165568(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_35
# BB#36:                                # %.preheader.i6848.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	.p2align	4, 0x90
.LBB5_37:                               # %vector.body11681
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1426784(%rax)
	movaps	%xmm0, global_data+1426800(%rax)
	movaps	%xmm0, global_data+1426816(%rax)
	movaps	%xmm0, global_data+1426832(%rax)
	movaps	%xmm0, global_data+1426848(%rax)
	movaps	%xmm0, global_data+1426864(%rax)
	movaps	%xmm0, global_data+1426880(%rax)
	movaps	%xmm0, global_data+1426896(%rax)
	movaps	%xmm0, global_data+1426912(%rax)
	movaps	%xmm0, global_data+1426928(%rax)
	movaps	%xmm0, global_data+1426944(%rax)
	movaps	%xmm0, global_data+1426960(%rax)
	movaps	%xmm0, global_data+1426976(%rax)
	movaps	%xmm0, global_data+1426992(%rax)
	movaps	%xmm0, global_data+1427008(%rax)
	movaps	%xmm0, global_data+1427024(%rax)
	movaps	%xmm0, global_data+1427040(%rax)
	movaps	%xmm0, global_data+1427056(%rax)
	movaps	%xmm0, global_data+1427072(%rax)
	movaps	%xmm0, global_data+1427088(%rax)
	movaps	%xmm0, global_data+1427104(%rax)
	movaps	%xmm0, global_data+1427120(%rax)
	movaps	%xmm0, global_data+1427136(%rax)
	movaps	%xmm0, global_data+1427152(%rax)
	movaps	%xmm0, global_data+1427168(%rax)
	movaps	%xmm0, global_data+1427184(%rax)
	movaps	%xmm0, global_data+1427200(%rax)
	movaps	%xmm0, global_data+1427216(%rax)
	movaps	%xmm0, global_data+1427232(%rax)
	movaps	%xmm0, global_data+1427248(%rax)
	movaps	%xmm0, global_data+1427264(%rax)
	movaps	%xmm0, global_data+1427280(%rax)
	movaps	%xmm0, global_data+1427296(%rax)
	movaps	%xmm0, global_data+1427312(%rax)
	movaps	%xmm0, global_data+1427328(%rax)
	movaps	%xmm0, global_data+1427344(%rax)
	movaps	%xmm0, global_data+1427360(%rax)
	movaps	%xmm0, global_data+1427376(%rax)
	movaps	%xmm0, global_data+1427392(%rax)
	movaps	%xmm0, global_data+1427408(%rax)
	movaps	%xmm0, global_data+1427424(%rax)
	movaps	%xmm0, global_data+1427440(%rax)
	movaps	%xmm0, global_data+1427456(%rax)
	movaps	%xmm0, global_data+1427472(%rax)
	movaps	%xmm0, global_data+1427488(%rax)
	movaps	%xmm0, global_data+1427504(%rax)
	movaps	%xmm0, global_data+1427520(%rax)
	movaps	%xmm0, global_data+1427536(%rax)
	movaps	%xmm0, global_data+1427552(%rax)
	movaps	%xmm0, global_data+1427568(%rax)
	movaps	%xmm0, global_data+1427584(%rax)
	movaps	%xmm0, global_data+1427600(%rax)
	movaps	%xmm0, global_data+1427616(%rax)
	movaps	%xmm0, global_data+1427632(%rax)
	movaps	%xmm0, global_data+1427648(%rax)
	movaps	%xmm0, global_data+1427664(%rax)
	movaps	%xmm0, global_data+1427680(%rax)
	movaps	%xmm0, global_data+1427696(%rax)
	movaps	%xmm0, global_data+1427712(%rax)
	movaps	%xmm0, global_data+1427728(%rax)
	movaps	%xmm0, global_data+1427744(%rax)
	movaps	%xmm0, global_data+1427760(%rax)
	movaps	%xmm0, global_data+1427776(%rax)
	movaps	%xmm0, global_data+1427792(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_37
	jmp	.LBB5_853
.LBB5_39:                               # %vector.body11629.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_40:                               # %vector.body11629
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_40
	jmp	.LBB5_853
.LBB5_42:                               # %vector.body11603.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_43:                               # %vector.body11603
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_43
# BB#44:                                # %.preheader.i6833.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_11(%rip), %xmm0  # xmm0 = [1.000000e-06,1.000000e-06,1.000000e-06,1.000000e-06]
	.p2align	4, 0x90
.LBB5_45:                               # %vector.body11616
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1164560(%rax)
	movaps	%xmm0, global_data+1164576(%rax)
	movaps	%xmm0, global_data+1164592(%rax)
	movaps	%xmm0, global_data+1164608(%rax)
	movaps	%xmm0, global_data+1164624(%rax)
	movaps	%xmm0, global_data+1164640(%rax)
	movaps	%xmm0, global_data+1164656(%rax)
	movaps	%xmm0, global_data+1164672(%rax)
	movaps	%xmm0, global_data+1164688(%rax)
	movaps	%xmm0, global_data+1164704(%rax)
	movaps	%xmm0, global_data+1164720(%rax)
	movaps	%xmm0, global_data+1164736(%rax)
	movaps	%xmm0, global_data+1164752(%rax)
	movaps	%xmm0, global_data+1164768(%rax)
	movaps	%xmm0, global_data+1164784(%rax)
	movaps	%xmm0, global_data+1164800(%rax)
	movaps	%xmm0, global_data+1164816(%rax)
	movaps	%xmm0, global_data+1164832(%rax)
	movaps	%xmm0, global_data+1164848(%rax)
	movaps	%xmm0, global_data+1164864(%rax)
	movaps	%xmm0, global_data+1164880(%rax)
	movaps	%xmm0, global_data+1164896(%rax)
	movaps	%xmm0, global_data+1164912(%rax)
	movaps	%xmm0, global_data+1164928(%rax)
	movaps	%xmm0, global_data+1164944(%rax)
	movaps	%xmm0, global_data+1164960(%rax)
	movaps	%xmm0, global_data+1164976(%rax)
	movaps	%xmm0, global_data+1164992(%rax)
	movaps	%xmm0, global_data+1165008(%rax)
	movaps	%xmm0, global_data+1165024(%rax)
	movaps	%xmm0, global_data+1165040(%rax)
	movaps	%xmm0, global_data+1165056(%rax)
	movaps	%xmm0, global_data+1165072(%rax)
	movaps	%xmm0, global_data+1165088(%rax)
	movaps	%xmm0, global_data+1165104(%rax)
	movaps	%xmm0, global_data+1165120(%rax)
	movaps	%xmm0, global_data+1165136(%rax)
	movaps	%xmm0, global_data+1165152(%rax)
	movaps	%xmm0, global_data+1165168(%rax)
	movaps	%xmm0, global_data+1165184(%rax)
	movaps	%xmm0, global_data+1165200(%rax)
	movaps	%xmm0, global_data+1165216(%rax)
	movaps	%xmm0, global_data+1165232(%rax)
	movaps	%xmm0, global_data+1165248(%rax)
	movaps	%xmm0, global_data+1165264(%rax)
	movaps	%xmm0, global_data+1165280(%rax)
	movaps	%xmm0, global_data+1165296(%rax)
	movaps	%xmm0, global_data+1165312(%rax)
	movaps	%xmm0, global_data+1165328(%rax)
	movaps	%xmm0, global_data+1165344(%rax)
	movaps	%xmm0, global_data+1165360(%rax)
	movaps	%xmm0, global_data+1165376(%rax)
	movaps	%xmm0, global_data+1165392(%rax)
	movaps	%xmm0, global_data+1165408(%rax)
	movaps	%xmm0, global_data+1165424(%rax)
	movaps	%xmm0, global_data+1165440(%rax)
	movaps	%xmm0, global_data+1165456(%rax)
	movaps	%xmm0, global_data+1165472(%rax)
	movaps	%xmm0, global_data+1165488(%rax)
	movaps	%xmm0, global_data+1165504(%rax)
	movaps	%xmm0, global_data+1165520(%rax)
	movaps	%xmm0, global_data+1165536(%rax)
	movaps	%xmm0, global_data+1165552(%rax)
	movaps	%xmm0, global_data+1165568(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_45
	jmp	.LBB5_853
.LBB5_47:                               # %.preheader.i6826.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_48:                               # %vector.body11575
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+902336(%rax)
	movaps	%xmm0, global_data+902352(%rax)
	movaps	%xmm0, global_data+902368(%rax)
	movaps	%xmm0, global_data+902384(%rax)
	movaps	%xmm0, global_data+902400(%rax)
	movaps	%xmm0, global_data+902416(%rax)
	movaps	%xmm0, global_data+902432(%rax)
	movaps	%xmm0, global_data+902448(%rax)
	movaps	%xmm0, global_data+902464(%rax)
	movaps	%xmm0, global_data+902480(%rax)
	movaps	%xmm0, global_data+902496(%rax)
	movaps	%xmm0, global_data+902512(%rax)
	movaps	%xmm0, global_data+902528(%rax)
	movaps	%xmm0, global_data+902544(%rax)
	movaps	%xmm0, global_data+902560(%rax)
	movaps	%xmm0, global_data+902576(%rax)
	movaps	%xmm0, global_data+902592(%rax)
	movaps	%xmm0, global_data+902608(%rax)
	movaps	%xmm0, global_data+902624(%rax)
	movaps	%xmm0, global_data+902640(%rax)
	movaps	%xmm0, global_data+902656(%rax)
	movaps	%xmm0, global_data+902672(%rax)
	movaps	%xmm0, global_data+902688(%rax)
	movaps	%xmm0, global_data+902704(%rax)
	movaps	%xmm0, global_data+902720(%rax)
	movaps	%xmm0, global_data+902736(%rax)
	movaps	%xmm0, global_data+902752(%rax)
	movaps	%xmm0, global_data+902768(%rax)
	movaps	%xmm0, global_data+902784(%rax)
	movaps	%xmm0, global_data+902800(%rax)
	movaps	%xmm0, global_data+902816(%rax)
	movaps	%xmm0, global_data+902832(%rax)
	movaps	%xmm0, global_data+902848(%rax)
	movaps	%xmm0, global_data+902864(%rax)
	movaps	%xmm0, global_data+902880(%rax)
	movaps	%xmm0, global_data+902896(%rax)
	movaps	%xmm0, global_data+902912(%rax)
	movaps	%xmm0, global_data+902928(%rax)
	movaps	%xmm0, global_data+902944(%rax)
	movaps	%xmm0, global_data+902960(%rax)
	movaps	%xmm0, global_data+902976(%rax)
	movaps	%xmm0, global_data+902992(%rax)
	movaps	%xmm0, global_data+903008(%rax)
	movaps	%xmm0, global_data+903024(%rax)
	movaps	%xmm0, global_data+903040(%rax)
	movaps	%xmm0, global_data+903056(%rax)
	movaps	%xmm0, global_data+903072(%rax)
	movaps	%xmm0, global_data+903088(%rax)
	movaps	%xmm0, global_data+903104(%rax)
	movaps	%xmm0, global_data+903120(%rax)
	movaps	%xmm0, global_data+903136(%rax)
	movaps	%xmm0, global_data+903152(%rax)
	movaps	%xmm0, global_data+903168(%rax)
	movaps	%xmm0, global_data+903184(%rax)
	movaps	%xmm0, global_data+903200(%rax)
	movaps	%xmm0, global_data+903216(%rax)
	movaps	%xmm0, global_data+903232(%rax)
	movaps	%xmm0, global_data+903248(%rax)
	movaps	%xmm0, global_data+903264(%rax)
	movaps	%xmm0, global_data+903280(%rax)
	movaps	%xmm0, global_data+903296(%rax)
	movaps	%xmm0, global_data+903312(%rax)
	movaps	%xmm0, global_data+903328(%rax)
	movaps	%xmm0, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_48
# BB#49:                                # %.preheader48.i6820.preheader
	movl	$global_data+903424, %eax
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB5_50:                               # %vector.body11588
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	imull	%edx, %edx
	cvtsi2ssl	%edx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, -1008(%rax)
	movaps	%xmm1, -992(%rax)
	movaps	%xmm1, -976(%rax)
	movaps	%xmm1, -960(%rax)
	movaps	%xmm1, -944(%rax)
	movaps	%xmm1, -928(%rax)
	movaps	%xmm1, -912(%rax)
	movaps	%xmm1, -896(%rax)
	movaps	%xmm1, -880(%rax)
	movaps	%xmm1, -864(%rax)
	movaps	%xmm1, -848(%rax)
	movaps	%xmm1, -832(%rax)
	movaps	%xmm1, -816(%rax)
	movaps	%xmm1, -800(%rax)
	movaps	%xmm1, -784(%rax)
	movaps	%xmm1, -768(%rax)
	movaps	%xmm1, -752(%rax)
	movaps	%xmm1, -736(%rax)
	movaps	%xmm1, -720(%rax)
	movaps	%xmm1, -704(%rax)
	movaps	%xmm1, -688(%rax)
	movaps	%xmm1, -672(%rax)
	movaps	%xmm1, -656(%rax)
	movaps	%xmm1, -640(%rax)
	movaps	%xmm1, -624(%rax)
	movaps	%xmm1, -608(%rax)
	movaps	%xmm1, -592(%rax)
	movaps	%xmm1, -576(%rax)
	movaps	%xmm1, -560(%rax)
	movaps	%xmm1, -544(%rax)
	movaps	%xmm1, -528(%rax)
	movaps	%xmm1, -512(%rax)
	movaps	%xmm1, -496(%rax)
	movaps	%xmm1, -480(%rax)
	movaps	%xmm1, -464(%rax)
	movaps	%xmm1, -448(%rax)
	movaps	%xmm1, -432(%rax)
	movaps	%xmm1, -416(%rax)
	movaps	%xmm1, -400(%rax)
	movaps	%xmm1, -384(%rax)
	movaps	%xmm1, -368(%rax)
	movaps	%xmm1, -352(%rax)
	movaps	%xmm1, -336(%rax)
	movaps	%xmm1, -320(%rax)
	movaps	%xmm1, -304(%rax)
	movaps	%xmm1, -288(%rax)
	movaps	%xmm1, -272(%rax)
	movaps	%xmm1, -256(%rax)
	movaps	%xmm1, -240(%rax)
	movaps	%xmm1, -224(%rax)
	movaps	%xmm1, -208(%rax)
	movaps	%xmm1, -192(%rax)
	movaps	%xmm1, -176(%rax)
	movaps	%xmm1, -160(%rax)
	movaps	%xmm1, -144(%rax)
	movaps	%xmm1, -128(%rax)
	movaps	%xmm1, -112(%rax)
	movaps	%xmm1, -96(%rax)
	movaps	%xmm1, -80(%rax)
	movaps	%xmm1, -64(%rax)
	movaps	%xmm1, -48(%rax)
	movaps	%xmm1, -32(%rax)
	movaps	%xmm1, -16(%rax)
	movaps	%xmm1, (%rax)
	addq	$1024, %rax             # imm = 0x400
	incq	%rcx
	cmpq	$257, %rcx              # imm = 0x101
	jne	.LBB5_50
	jmp	.LBB5_853
.LBB5_52:                               # %vector.body11551.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_53:                               # %vector.body11551
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_53
# BB#54:                                # %vector.body11564.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_55:                               # %vector.body11564
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_55
	jmp	.LBB5_853
.LBB5_57:                               # %vector.body11527.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_58:                               # %vector.body11527
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_58
# BB#59:                                # %vector.body11540.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_60:                               # %vector.body11540
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_60
	jmp	.LBB5_853
.LBB5_62:                               # %.preheader.i6796.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_63:                               # %vector.body11479
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_63
# BB#64:                                # %vector.body11492.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_65:                               # %vector.body11492
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+384048(%rax)
	movaps	%xmm1, global_data+384064(%rax)
	movaps	%xmm1, global_data+384080(%rax)
	movaps	%xmm1, global_data+384096(%rax)
	movaps	%xmm1, global_data+384112(%rax)
	movaps	%xmm1, global_data+384128(%rax)
	movaps	%xmm1, global_data+384144(%rax)
	movaps	%xmm1, global_data+384160(%rax)
	movaps	%xmm1, global_data+384176(%rax)
	movaps	%xmm1, global_data+384192(%rax)
	movaps	%xmm1, global_data+384208(%rax)
	movaps	%xmm1, global_data+384224(%rax)
	movaps	%xmm1, global_data+384240(%rax)
	movaps	%xmm1, global_data+384256(%rax)
	movaps	%xmm1, global_data+384272(%rax)
	movaps	%xmm1, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_65
# BB#66:                                # %vector.body11505.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_67:                               # %vector.body11505
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+512080(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_67
# BB#68:                                # %vector.body11516.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_69:                               # %vector.body11516
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_69
	jmp	.LBB5_853
.LBB5_71:                               # %.preheader.i6774.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_72:                               # %vector.body11431
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_72
# BB#73:                                # %vector.body11444.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_74:                               # %vector.body11444
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+384048(%rax)
	movaps	%xmm1, global_data+384064(%rax)
	movaps	%xmm1, global_data+384080(%rax)
	movaps	%xmm1, global_data+384096(%rax)
	movaps	%xmm1, global_data+384112(%rax)
	movaps	%xmm1, global_data+384128(%rax)
	movaps	%xmm1, global_data+384144(%rax)
	movaps	%xmm1, global_data+384160(%rax)
	movaps	%xmm1, global_data+384176(%rax)
	movaps	%xmm1, global_data+384192(%rax)
	movaps	%xmm1, global_data+384208(%rax)
	movaps	%xmm1, global_data+384224(%rax)
	movaps	%xmm1, global_data+384240(%rax)
	movaps	%xmm1, global_data+384256(%rax)
	movaps	%xmm1, global_data+384272(%rax)
	movaps	%xmm1, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_74
# BB#75:                                # %vector.body11457.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_76:                               # %vector.body11457
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+512080(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_76
# BB#77:                                # %vector.body11468.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_78:                               # %vector.body11468
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_78
	jmp	.LBB5_853
.LBB5_80:                               # %.preheader.i6752.preheader
	movl	$array, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_81:                               # %vector.body11392
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+902336(%rax)
	movaps	%xmm0, global_data+902352(%rax)
	movaps	%xmm0, global_data+902368(%rax)
	movaps	%xmm0, global_data+902384(%rax)
	movaps	%xmm0, global_data+902400(%rax)
	movaps	%xmm0, global_data+902416(%rax)
	movaps	%xmm0, global_data+902432(%rax)
	movaps	%xmm0, global_data+902448(%rax)
	movaps	%xmm0, global_data+902464(%rax)
	movaps	%xmm0, global_data+902480(%rax)
	movaps	%xmm0, global_data+902496(%rax)
	movaps	%xmm0, global_data+902512(%rax)
	movaps	%xmm0, global_data+902528(%rax)
	movaps	%xmm0, global_data+902544(%rax)
	movaps	%xmm0, global_data+902560(%rax)
	movaps	%xmm0, global_data+902576(%rax)
	movaps	%xmm0, global_data+902592(%rax)
	movaps	%xmm0, global_data+902608(%rax)
	movaps	%xmm0, global_data+902624(%rax)
	movaps	%xmm0, global_data+902640(%rax)
	movaps	%xmm0, global_data+902656(%rax)
	movaps	%xmm0, global_data+902672(%rax)
	movaps	%xmm0, global_data+902688(%rax)
	movaps	%xmm0, global_data+902704(%rax)
	movaps	%xmm0, global_data+902720(%rax)
	movaps	%xmm0, global_data+902736(%rax)
	movaps	%xmm0, global_data+902752(%rax)
	movaps	%xmm0, global_data+902768(%rax)
	movaps	%xmm0, global_data+902784(%rax)
	movaps	%xmm0, global_data+902800(%rax)
	movaps	%xmm0, global_data+902816(%rax)
	movaps	%xmm0, global_data+902832(%rax)
	movaps	%xmm0, global_data+902848(%rax)
	movaps	%xmm0, global_data+902864(%rax)
	movaps	%xmm0, global_data+902880(%rax)
	movaps	%xmm0, global_data+902896(%rax)
	movaps	%xmm0, global_data+902912(%rax)
	movaps	%xmm0, global_data+902928(%rax)
	movaps	%xmm0, global_data+902944(%rax)
	movaps	%xmm0, global_data+902960(%rax)
	movaps	%xmm0, global_data+902976(%rax)
	movaps	%xmm0, global_data+902992(%rax)
	movaps	%xmm0, global_data+903008(%rax)
	movaps	%xmm0, global_data+903024(%rax)
	movaps	%xmm0, global_data+903040(%rax)
	movaps	%xmm0, global_data+903056(%rax)
	movaps	%xmm0, global_data+903072(%rax)
	movaps	%xmm0, global_data+903088(%rax)
	movaps	%xmm0, global_data+903104(%rax)
	movaps	%xmm0, global_data+903120(%rax)
	movaps	%xmm0, global_data+903136(%rax)
	movaps	%xmm0, global_data+903152(%rax)
	movaps	%xmm0, global_data+903168(%rax)
	movaps	%xmm0, global_data+903184(%rax)
	movaps	%xmm0, global_data+903200(%rax)
	movaps	%xmm0, global_data+903216(%rax)
	movaps	%xmm0, global_data+903232(%rax)
	movaps	%xmm0, global_data+903248(%rax)
	movaps	%xmm0, global_data+903264(%rax)
	movaps	%xmm0, global_data+903280(%rax)
	movaps	%xmm0, global_data+903296(%rax)
	movaps	%xmm0, global_data+903312(%rax)
	movaps	%xmm0, global_data+903328(%rax)
	movaps	%xmm0, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_81
# BB#82:                                # %.preheader.i6737.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_7(%rip), %xmm0   # xmm0 = [5.000000e-01,5.000000e-01,5.000000e-01,5.000000e-01]
.LBB5_83:                               # %vector.body11405
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1164560(%rax)
	movaps	%xmm0, global_data+1164576(%rax)
	movaps	%xmm0, global_data+1164592(%rax)
	movaps	%xmm0, global_data+1164608(%rax)
	movaps	%xmm0, global_data+1164624(%rax)
	movaps	%xmm0, global_data+1164640(%rax)
	movaps	%xmm0, global_data+1164656(%rax)
	movaps	%xmm0, global_data+1164672(%rax)
	movaps	%xmm0, global_data+1164688(%rax)
	movaps	%xmm0, global_data+1164704(%rax)
	movaps	%xmm0, global_data+1164720(%rax)
	movaps	%xmm0, global_data+1164736(%rax)
	movaps	%xmm0, global_data+1164752(%rax)
	movaps	%xmm0, global_data+1164768(%rax)
	movaps	%xmm0, global_data+1164784(%rax)
	movaps	%xmm0, global_data+1164800(%rax)
	movaps	%xmm0, global_data+1164816(%rax)
	movaps	%xmm0, global_data+1164832(%rax)
	movaps	%xmm0, global_data+1164848(%rax)
	movaps	%xmm0, global_data+1164864(%rax)
	movaps	%xmm0, global_data+1164880(%rax)
	movaps	%xmm0, global_data+1164896(%rax)
	movaps	%xmm0, global_data+1164912(%rax)
	movaps	%xmm0, global_data+1164928(%rax)
	movaps	%xmm0, global_data+1164944(%rax)
	movaps	%xmm0, global_data+1164960(%rax)
	movaps	%xmm0, global_data+1164976(%rax)
	movaps	%xmm0, global_data+1164992(%rax)
	movaps	%xmm0, global_data+1165008(%rax)
	movaps	%xmm0, global_data+1165024(%rax)
	movaps	%xmm0, global_data+1165040(%rax)
	movaps	%xmm0, global_data+1165056(%rax)
	movaps	%xmm0, global_data+1165072(%rax)
	movaps	%xmm0, global_data+1165088(%rax)
	movaps	%xmm0, global_data+1165104(%rax)
	movaps	%xmm0, global_data+1165120(%rax)
	movaps	%xmm0, global_data+1165136(%rax)
	movaps	%xmm0, global_data+1165152(%rax)
	movaps	%xmm0, global_data+1165168(%rax)
	movaps	%xmm0, global_data+1165184(%rax)
	movaps	%xmm0, global_data+1165200(%rax)
	movaps	%xmm0, global_data+1165216(%rax)
	movaps	%xmm0, global_data+1165232(%rax)
	movaps	%xmm0, global_data+1165248(%rax)
	movaps	%xmm0, global_data+1165264(%rax)
	movaps	%xmm0, global_data+1165280(%rax)
	movaps	%xmm0, global_data+1165296(%rax)
	movaps	%xmm0, global_data+1165312(%rax)
	movaps	%xmm0, global_data+1165328(%rax)
	movaps	%xmm0, global_data+1165344(%rax)
	movaps	%xmm0, global_data+1165360(%rax)
	movaps	%xmm0, global_data+1165376(%rax)
	movaps	%xmm0, global_data+1165392(%rax)
	movaps	%xmm0, global_data+1165408(%rax)
	movaps	%xmm0, global_data+1165424(%rax)
	movaps	%xmm0, global_data+1165440(%rax)
	movaps	%xmm0, global_data+1165456(%rax)
	movaps	%xmm0, global_data+1165472(%rax)
	movaps	%xmm0, global_data+1165488(%rax)
	movaps	%xmm0, global_data+1165504(%rax)
	movaps	%xmm0, global_data+1165520(%rax)
	movaps	%xmm0, global_data+1165536(%rax)
	movaps	%xmm0, global_data+1165552(%rax)
	movaps	%xmm0, global_data+1165568(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_83
# BB#84:                                # %.preheader.i6730.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_6(%rip), %xmm0   # xmm0 = [2.000000e+00,2.000000e+00,2.000000e+00,2.000000e+00]
.LBB5_85:                               # %vector.body11418
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1426784(%rax)
	movaps	%xmm0, global_data+1426800(%rax)
	movaps	%xmm0, global_data+1426816(%rax)
	movaps	%xmm0, global_data+1426832(%rax)
	movaps	%xmm0, global_data+1426848(%rax)
	movaps	%xmm0, global_data+1426864(%rax)
	movaps	%xmm0, global_data+1426880(%rax)
	movaps	%xmm0, global_data+1426896(%rax)
	movaps	%xmm0, global_data+1426912(%rax)
	movaps	%xmm0, global_data+1426928(%rax)
	movaps	%xmm0, global_data+1426944(%rax)
	movaps	%xmm0, global_data+1426960(%rax)
	movaps	%xmm0, global_data+1426976(%rax)
	movaps	%xmm0, global_data+1426992(%rax)
	movaps	%xmm0, global_data+1427008(%rax)
	movaps	%xmm0, global_data+1427024(%rax)
	movaps	%xmm0, global_data+1427040(%rax)
	movaps	%xmm0, global_data+1427056(%rax)
	movaps	%xmm0, global_data+1427072(%rax)
	movaps	%xmm0, global_data+1427088(%rax)
	movaps	%xmm0, global_data+1427104(%rax)
	movaps	%xmm0, global_data+1427120(%rax)
	movaps	%xmm0, global_data+1427136(%rax)
	movaps	%xmm0, global_data+1427152(%rax)
	movaps	%xmm0, global_data+1427168(%rax)
	movaps	%xmm0, global_data+1427184(%rax)
	movaps	%xmm0, global_data+1427200(%rax)
	movaps	%xmm0, global_data+1427216(%rax)
	movaps	%xmm0, global_data+1427232(%rax)
	movaps	%xmm0, global_data+1427248(%rax)
	movaps	%xmm0, global_data+1427264(%rax)
	movaps	%xmm0, global_data+1427280(%rax)
	movaps	%xmm0, global_data+1427296(%rax)
	movaps	%xmm0, global_data+1427312(%rax)
	movaps	%xmm0, global_data+1427328(%rax)
	movaps	%xmm0, global_data+1427344(%rax)
	movaps	%xmm0, global_data+1427360(%rax)
	movaps	%xmm0, global_data+1427376(%rax)
	movaps	%xmm0, global_data+1427392(%rax)
	movaps	%xmm0, global_data+1427408(%rax)
	movaps	%xmm0, global_data+1427424(%rax)
	movaps	%xmm0, global_data+1427440(%rax)
	movaps	%xmm0, global_data+1427456(%rax)
	movaps	%xmm0, global_data+1427472(%rax)
	movaps	%xmm0, global_data+1427488(%rax)
	movaps	%xmm0, global_data+1427504(%rax)
	movaps	%xmm0, global_data+1427520(%rax)
	movaps	%xmm0, global_data+1427536(%rax)
	movaps	%xmm0, global_data+1427552(%rax)
	movaps	%xmm0, global_data+1427568(%rax)
	movaps	%xmm0, global_data+1427584(%rax)
	movaps	%xmm0, global_data+1427600(%rax)
	movaps	%xmm0, global_data+1427616(%rax)
	movaps	%xmm0, global_data+1427632(%rax)
	movaps	%xmm0, global_data+1427648(%rax)
	movaps	%xmm0, global_data+1427664(%rax)
	movaps	%xmm0, global_data+1427680(%rax)
	movaps	%xmm0, global_data+1427696(%rax)
	movaps	%xmm0, global_data+1427712(%rax)
	movaps	%xmm0, global_data+1427728(%rax)
	movaps	%xmm0, global_data+1427744(%rax)
	movaps	%xmm0, global_data+1427760(%rax)
	movaps	%xmm0, global_data+1427776(%rax)
	movaps	%xmm0, global_data+1427792(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_85
	jmp	.LBB5_853
.LBB5_87:                               # %.preheader.i6723.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_88:                               # %vector.body11353
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm3, global_data+1164560(%rax)
	movaps	%xmm3, global_data+1164576(%rax)
	movaps	%xmm3, global_data+1164592(%rax)
	movaps	%xmm3, global_data+1164608(%rax)
	movaps	%xmm3, global_data+1164624(%rax)
	movaps	%xmm3, global_data+1164640(%rax)
	movaps	%xmm3, global_data+1164656(%rax)
	movaps	%xmm3, global_data+1164672(%rax)
	movaps	%xmm3, global_data+1164688(%rax)
	movaps	%xmm3, global_data+1164704(%rax)
	movaps	%xmm3, global_data+1164720(%rax)
	movaps	%xmm3, global_data+1164736(%rax)
	movaps	%xmm3, global_data+1164752(%rax)
	movaps	%xmm3, global_data+1164768(%rax)
	movaps	%xmm3, global_data+1164784(%rax)
	movaps	%xmm3, global_data+1164800(%rax)
	movaps	%xmm3, global_data+1164816(%rax)
	movaps	%xmm3, global_data+1164832(%rax)
	movaps	%xmm3, global_data+1164848(%rax)
	movaps	%xmm3, global_data+1164864(%rax)
	movaps	%xmm3, global_data+1164880(%rax)
	movaps	%xmm3, global_data+1164896(%rax)
	movaps	%xmm3, global_data+1164912(%rax)
	movaps	%xmm3, global_data+1164928(%rax)
	movaps	%xmm3, global_data+1164944(%rax)
	movaps	%xmm3, global_data+1164960(%rax)
	movaps	%xmm3, global_data+1164976(%rax)
	movaps	%xmm3, global_data+1164992(%rax)
	movaps	%xmm3, global_data+1165008(%rax)
	movaps	%xmm3, global_data+1165024(%rax)
	movaps	%xmm3, global_data+1165040(%rax)
	movaps	%xmm3, global_data+1165056(%rax)
	movaps	%xmm3, global_data+1165072(%rax)
	movaps	%xmm3, global_data+1165088(%rax)
	movaps	%xmm3, global_data+1165104(%rax)
	movaps	%xmm3, global_data+1165120(%rax)
	movaps	%xmm3, global_data+1165136(%rax)
	movaps	%xmm3, global_data+1165152(%rax)
	movaps	%xmm3, global_data+1165168(%rax)
	movaps	%xmm3, global_data+1165184(%rax)
	movaps	%xmm3, global_data+1165200(%rax)
	movaps	%xmm3, global_data+1165216(%rax)
	movaps	%xmm3, global_data+1165232(%rax)
	movaps	%xmm3, global_data+1165248(%rax)
	movaps	%xmm3, global_data+1165264(%rax)
	movaps	%xmm3, global_data+1165280(%rax)
	movaps	%xmm3, global_data+1165296(%rax)
	movaps	%xmm3, global_data+1165312(%rax)
	movaps	%xmm3, global_data+1165328(%rax)
	movaps	%xmm3, global_data+1165344(%rax)
	movaps	%xmm3, global_data+1165360(%rax)
	movaps	%xmm3, global_data+1165376(%rax)
	movaps	%xmm3, global_data+1165392(%rax)
	movaps	%xmm3, global_data+1165408(%rax)
	movaps	%xmm3, global_data+1165424(%rax)
	movaps	%xmm3, global_data+1165440(%rax)
	movaps	%xmm3, global_data+1165456(%rax)
	movaps	%xmm3, global_data+1165472(%rax)
	movaps	%xmm3, global_data+1165488(%rax)
	movaps	%xmm3, global_data+1165504(%rax)
	movaps	%xmm3, global_data+1165520(%rax)
	movaps	%xmm3, global_data+1165536(%rax)
	movaps	%xmm3, global_data+1165552(%rax)
	movaps	%xmm3, global_data+1165568(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_88
# BB#89:                                # %vector.body11366.preheader
	movl	$1, %eax
	movd	%rax, %xmm1
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_90:                               # %vector.body11366
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	shufps	$136, %xmm2, %xmm6      # xmm6 = xmm6[0,2],xmm2[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm3, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, array+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, array+128016(%rax)
	paddq	%xmm5, %xmm1
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_90
# BB#91:                                # %.preheader44.i6712.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_92:                               # %vector.body11377
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1426784(%rax)
	movaps	%xmm1, global_data+1426800(%rax)
	movaps	%xmm1, global_data+1426816(%rax)
	movaps	%xmm1, global_data+1426832(%rax)
	movaps	%xmm1, global_data+1426848(%rax)
	movaps	%xmm1, global_data+1426864(%rax)
	movaps	%xmm1, global_data+1426880(%rax)
	movaps	%xmm1, global_data+1426896(%rax)
	movaps	%xmm1, global_data+1426912(%rax)
	movaps	%xmm1, global_data+1426928(%rax)
	movaps	%xmm1, global_data+1426944(%rax)
	movaps	%xmm1, global_data+1426960(%rax)
	movaps	%xmm1, global_data+1426976(%rax)
	movaps	%xmm1, global_data+1426992(%rax)
	movaps	%xmm1, global_data+1427008(%rax)
	movaps	%xmm1, global_data+1427024(%rax)
	movaps	%xmm1, global_data+1427040(%rax)
	movaps	%xmm1, global_data+1427056(%rax)
	movaps	%xmm1, global_data+1427072(%rax)
	movaps	%xmm1, global_data+1427088(%rax)
	movaps	%xmm1, global_data+1427104(%rax)
	movaps	%xmm1, global_data+1427120(%rax)
	movaps	%xmm1, global_data+1427136(%rax)
	movaps	%xmm1, global_data+1427152(%rax)
	movaps	%xmm1, global_data+1427168(%rax)
	movaps	%xmm1, global_data+1427184(%rax)
	movaps	%xmm1, global_data+1427200(%rax)
	movaps	%xmm1, global_data+1427216(%rax)
	movaps	%xmm1, global_data+1427232(%rax)
	movaps	%xmm1, global_data+1427248(%rax)
	movaps	%xmm1, global_data+1427264(%rax)
	movaps	%xmm1, global_data+1427280(%rax)
	movaps	%xmm1, global_data+1427296(%rax)
	movaps	%xmm1, global_data+1427312(%rax)
	movaps	%xmm1, global_data+1427328(%rax)
	movaps	%xmm1, global_data+1427344(%rax)
	movaps	%xmm1, global_data+1427360(%rax)
	movaps	%xmm1, global_data+1427376(%rax)
	movaps	%xmm1, global_data+1427392(%rax)
	movaps	%xmm1, global_data+1427408(%rax)
	movaps	%xmm1, global_data+1427424(%rax)
	movaps	%xmm1, global_data+1427440(%rax)
	movaps	%xmm1, global_data+1427456(%rax)
	movaps	%xmm1, global_data+1427472(%rax)
	movaps	%xmm1, global_data+1427488(%rax)
	movaps	%xmm1, global_data+1427504(%rax)
	movaps	%xmm1, global_data+1427520(%rax)
	movaps	%xmm1, global_data+1427536(%rax)
	movaps	%xmm1, global_data+1427552(%rax)
	movaps	%xmm1, global_data+1427568(%rax)
	movaps	%xmm1, global_data+1427584(%rax)
	movaps	%xmm1, global_data+1427600(%rax)
	movaps	%xmm1, global_data+1427616(%rax)
	movaps	%xmm1, global_data+1427632(%rax)
	movaps	%xmm1, global_data+1427648(%rax)
	movaps	%xmm1, global_data+1427664(%rax)
	movaps	%xmm1, global_data+1427680(%rax)
	movaps	%xmm1, global_data+1427696(%rax)
	movaps	%xmm1, global_data+1427712(%rax)
	movaps	%xmm1, global_data+1427728(%rax)
	movaps	%xmm1, global_data+1427744(%rax)
	movaps	%xmm1, global_data+1427760(%rax)
	movaps	%xmm1, global_data+1427776(%rax)
	movaps	%xmm1, global_data+1427792(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_92
	jmp	.LBB5_853
.LBB5_94:                               # %.preheader.i6706.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_95:                               # %vector.body11307
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_95
# BB#96:                                # %vector.body11320.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_97:                               # %vector.body11320
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_97
# BB#98:                                # %vector.body11331.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_99:                               # %vector.body11331
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_99
# BB#100:                               # %vector.body11342.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_101:                              # %vector.body11342
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_101
	jmp	.LBB5_853
.LBB5_103:                              # %.preheader.i6683.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_6(%rip), %xmm0   # xmm0 = [2.000000e+00,2.000000e+00,2.000000e+00,2.000000e+00]
.LBB5_104:                              # %vector.body11268
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_104
# BB#105:                               # %vector.body11281.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_106:                              # %vector.body11281
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_106
# BB#107:                               # %vector.body11294.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_108:                              # %vector.body11294
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	movaps	%xmm0, global_data+512288(%rax)
	movaps	%xmm0, global_data+512304(%rax)
	movaps	%xmm0, global_data+512320(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_108
	jmp	.LBB5_853
.LBB5_110:                              # %vector.body11244.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_111:                              # %vector.body11244
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_111
# BB#112:                               # %vector.body11257.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_113:                              # %vector.body11257
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_113
	jmp	.LBB5_853
.LBB5_115:                              # %.preheader.i6654.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_116:                              # %vector.body11209
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_116
# BB#117:                               # %vector.body11222.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_118:                              # %vector.body11222
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_118
# BB#119:                               # %vector.body11233.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_120:                              # %vector.body11233
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_120
	jmp	.LBB5_853
.LBB5_122:                              # %vector.body11181.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_123:                              # %vector.body11181
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, array+128000(%rax)
	movaps	%xmm0, array+128016(%rax)
	movaps	%xmm0, array+128032(%rax)
	movaps	%xmm0, array+128048(%rax)
	movaps	%xmm0, array+128064(%rax)
	movaps	%xmm0, array+128080(%rax)
	movaps	%xmm0, array+128096(%rax)
	movaps	%xmm0, array+128112(%rax)
	movaps	%xmm0, array+128128(%rax)
	movaps	%xmm0, array+128144(%rax)
	movaps	%xmm0, array+128160(%rax)
	movaps	%xmm0, array+128176(%rax)
	movaps	%xmm0, array+128192(%rax)
	movaps	%xmm0, array+128208(%rax)
	movaps	%xmm0, array+128224(%rax)
	movaps	%xmm0, array+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_123
# BB#124:                               # %.preheader48.i6634.preheader
	movl	$global_data+903424, %eax
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_125:                              # %vector.body11194
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	imull	%edx, %edx
	cvtsi2ssl	%edx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, -1008(%rax)
	movaps	%xmm1, -992(%rax)
	movaps	%xmm1, -976(%rax)
	movaps	%xmm1, -960(%rax)
	movaps	%xmm1, -944(%rax)
	movaps	%xmm1, -928(%rax)
	movaps	%xmm1, -912(%rax)
	movaps	%xmm1, -896(%rax)
	movaps	%xmm1, -880(%rax)
	movaps	%xmm1, -864(%rax)
	movaps	%xmm1, -848(%rax)
	movaps	%xmm1, -832(%rax)
	movaps	%xmm1, -816(%rax)
	movaps	%xmm1, -800(%rax)
	movaps	%xmm1, -784(%rax)
	movaps	%xmm1, -768(%rax)
	movaps	%xmm1, -752(%rax)
	movaps	%xmm1, -736(%rax)
	movaps	%xmm1, -720(%rax)
	movaps	%xmm1, -704(%rax)
	movaps	%xmm1, -688(%rax)
	movaps	%xmm1, -672(%rax)
	movaps	%xmm1, -656(%rax)
	movaps	%xmm1, -640(%rax)
	movaps	%xmm1, -624(%rax)
	movaps	%xmm1, -608(%rax)
	movaps	%xmm1, -592(%rax)
	movaps	%xmm1, -576(%rax)
	movaps	%xmm1, -560(%rax)
	movaps	%xmm1, -544(%rax)
	movaps	%xmm1, -528(%rax)
	movaps	%xmm1, -512(%rax)
	movaps	%xmm1, -496(%rax)
	movaps	%xmm1, -480(%rax)
	movaps	%xmm1, -464(%rax)
	movaps	%xmm1, -448(%rax)
	movaps	%xmm1, -432(%rax)
	movaps	%xmm1, -416(%rax)
	movaps	%xmm1, -400(%rax)
	movaps	%xmm1, -384(%rax)
	movaps	%xmm1, -368(%rax)
	movaps	%xmm1, -352(%rax)
	movaps	%xmm1, -336(%rax)
	movaps	%xmm1, -320(%rax)
	movaps	%xmm1, -304(%rax)
	movaps	%xmm1, -288(%rax)
	movaps	%xmm1, -272(%rax)
	movaps	%xmm1, -256(%rax)
	movaps	%xmm1, -240(%rax)
	movaps	%xmm1, -224(%rax)
	movaps	%xmm1, -208(%rax)
	movaps	%xmm1, -192(%rax)
	movaps	%xmm1, -176(%rax)
	movaps	%xmm1, -160(%rax)
	movaps	%xmm1, -144(%rax)
	movaps	%xmm1, -128(%rax)
	movaps	%xmm1, -112(%rax)
	movaps	%xmm1, -96(%rax)
	movaps	%xmm1, -80(%rax)
	movaps	%xmm1, -64(%rax)
	movaps	%xmm1, -48(%rax)
	movaps	%xmm1, -32(%rax)
	movaps	%xmm1, -16(%rax)
	movaps	%xmm1, (%rax)
	addq	$1024, %rax             # imm = 0x400
	incq	%rcx
	cmpq	$257, %rcx              # imm = 0x101
	jne	.LBB5_125
	jmp	.LBB5_853
.LBB5_127:                              # %vector.body11157.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_128:                              # %vector.body11157
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_128
# BB#129:                               # %vector.body11170.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_130:                              # %vector.body11170
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_130
	jmp	.LBB5_853
.LBB5_132:                              # %vector.body11111.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_133:                              # %vector.body11111
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_133
# BB#134:                               # %.preheader.i6615.preheader
	movl	$global_data+128016, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm4   # xmm4 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm1   # xmm1 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm2   # xmm2 = [8,8]
.LBB5_135:                              # %vector.body11124
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm3, %xmm5
	shufps	$136, %xmm4, %xmm5      # xmm5 = xmm5[0,2],xmm4[0,2]
	movaps	%xmm5, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm1, %xmm5
	cvtdq2ps	%xmm5, %xmm5
	movaps	%xmm0, %xmm6
	divps	%xmm5, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm2, %xmm3
	paddq	%xmm2, %xmm4
	addq	$32, %rax
	jne	.LBB5_135
# BB#136:                               # %vector.body11135.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm4   # xmm4 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_137:                              # %vector.body11135
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm3, %xmm5
	shufps	$136, %xmm4, %xmm5      # xmm5 = xmm5[0,2],xmm4[0,2]
	movaps	%xmm5, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm1, %xmm5
	cvtdq2ps	%xmm5, %xmm5
	movaps	%xmm0, %xmm6
	divps	%xmm5, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm2, %xmm3
	paddq	%xmm2, %xmm4
	addq	$32, %rax
	jne	.LBB5_137
# BB#138:                               # %vector.body11146.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm4   # xmm4 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_139:                              # %vector.body11146
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm3, %xmm5
	shufps	$136, %xmm4, %xmm5      # xmm5 = xmm5[0,2],xmm4[0,2]
	movaps	%xmm5, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm1, %xmm5
	cvtdq2ps	%xmm5, %xmm5
	movaps	%xmm0, %xmm6
	divps	%xmm5, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm2, %xmm3
	paddq	%xmm2, %xmm4
	addq	$32, %rax
	jne	.LBB5_139
	jmp	.LBB5_853
.LBB5_141:                              # %vector.body11063.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_142:                              # %vector.body11063
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_142
# BB#143:                               # %.preheader.i6592.preheader
	xorl	%eax, %eax
.LBB5_144:                              # %.preheader.i6592
                                        # =>This Inner Loop Header: Depth=1
	movl	$1065353216, global_data+128016(,%rax,4) # imm = 0x3F800000
	movl	$1065353216, global_data+128024(,%rax,4) # imm = 0x3F800000
	movl	$1065353216, global_data+128032(,%rax,4) # imm = 0x3F800000
	movl	$1065353216, global_data+128040(,%rax,4) # imm = 0x3F800000
	movl	$1065353216, global_data+128048(,%rax,4) # imm = 0x3F800000
	movl	$1065353216, global_data+128056(,%rax,4) # imm = 0x3F800000
	movl	$1065353216, global_data+128064(,%rax,4) # imm = 0x3F800000
	movl	$1065353216, global_data+128072(,%rax,4) # imm = 0x3F800000
	movl	$1065353216, global_data+128080(,%rax,4) # imm = 0x3F800000
	movl	$1065353216, global_data+128088(,%rax,4) # imm = 0x3F800000
	addq	$20, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	jl	.LBB5_144
# BB#145:                               # %.preheader.i6588.preheader
	xorl	%eax, %eax
.LBB5_146:                              # %.preheader.i6588
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1082130432, global_data+128020(,%rax,4) # imm = 0xBF800000
	movl	$-1082130432, global_data+128028(,%rax,4) # imm = 0xBF800000
	movl	$-1082130432, global_data+128036(,%rax,4) # imm = 0xBF800000
	movl	$-1082130432, global_data+128044(,%rax,4) # imm = 0xBF800000
	movl	$-1082130432, global_data+128052(,%rax,4) # imm = 0xBF800000
	movl	$-1082130432, global_data+128060(,%rax,4) # imm = 0xBF800000
	movl	$-1082130432, global_data+128068(,%rax,4) # imm = 0xBF800000
	movl	$-1082130432, global_data+128076(,%rax,4) # imm = 0xBF800000
	movl	$-1082130432, global_data+128084(,%rax,4) # imm = 0xBF800000
	movl	$-1082130432, global_data+128092(,%rax,4) # imm = 0xBF800000
	addq	$20, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	jl	.LBB5_146
# BB#147:                               # %vector.body11076.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_148:                              # %vector.body11076
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+384048(%rax)
	movaps	%xmm1, global_data+384064(%rax)
	movaps	%xmm1, global_data+384080(%rax)
	movaps	%xmm1, global_data+384096(%rax)
	movaps	%xmm1, global_data+384112(%rax)
	movaps	%xmm1, global_data+384128(%rax)
	movaps	%xmm1, global_data+384144(%rax)
	movaps	%xmm1, global_data+384160(%rax)
	movaps	%xmm1, global_data+384176(%rax)
	movaps	%xmm1, global_data+384192(%rax)
	movaps	%xmm1, global_data+384208(%rax)
	movaps	%xmm1, global_data+384224(%rax)
	movaps	%xmm1, global_data+384240(%rax)
	movaps	%xmm1, global_data+384256(%rax)
	movaps	%xmm1, global_data+384272(%rax)
	movaps	%xmm1, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_148
# BB#149:                               # %vector.body11089.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_150:                              # %vector.body11089
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+512080(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_150
# BB#151:                               # %vector.body11100.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_152:                              # %vector.body11100
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_152
	jmp	.LBB5_853
.LBB5_154:                              # %vector.body11028.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_155:                              # %vector.body11028
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_155
# BB#156:                               # %vector.body11041.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_157:                              # %vector.body11041
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_157
# BB#158:                               # %vector.body11052.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_159:                              # %vector.body11052
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_159
	jmp	.LBB5_853
.LBB5_161:                              # %vector.body11004.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_162:                              # %vector.body11004
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_162
# BB#163:                               # %vector.body11017.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_164:                              # %vector.body11017
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_164
	jmp	.LBB5_853
.LBB5_166:                              # %vector.body10980.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_167:                              # %vector.body10980
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_167
# BB#168:                               # %vector.body10993.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_169:                              # %vector.body10993
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_169
	jmp	.LBB5_853
.LBB5_171:                              # %vector.body10956.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_172:                              # %vector.body10956
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_172
# BB#173:                               # %vector.body10969.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_174:                              # %vector.body10969
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_174
	jmp	.LBB5_853
.LBB5_176:                              # %vector.body10932.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_177:                              # %vector.body10932
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_177
# BB#178:                               # %vector.body10945.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_179:                              # %vector.body10945
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_179
	jmp	.LBB5_853
.LBB5_181:                              # %vector.body10908.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_182:                              # %vector.body10908
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_182
# BB#183:                               # %vector.body10921.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_184:                              # %vector.body10921
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_184
	jmp	.LBB5_853
.LBB5_186:                              # %vector.body10873.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_187:                              # %vector.body10873
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_187
# BB#188:                               # %vector.body10886.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_189:                              # %vector.body10886
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_189
# BB#190:                               # %vector.body10897.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_191:                              # %vector.body10897
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_191
	jmp	.LBB5_853
.LBB5_193:                              # %.preheader.i6497.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_194:                              # %vector.body10827
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_194
# BB#195:                               # %vector.body10840.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_196:                              # %vector.body10840
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_196
# BB#197:                               # %vector.body10851.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_198:                              # %vector.body10851
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_198
# BB#199:                               # %vector.body10862.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_200:                              # %vector.body10862
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_200
	jmp	.LBB5_853
.LBB5_202:                              # %vector.body10779.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_203:                              # %vector.body10779
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm0, %xmm1
	divps	%xmm7, %xmm1
	movaps	%xmm1, global_data+128000(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_203
# BB#204:                               # %vector.body10790.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_205:                              # %vector.body10790
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_205
# BB#206:                               # %vector.body10803.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_207:                              # %vector.body10803
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_207
# BB#208:                               # %vector.body10816.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_209:                              # %vector.body10816
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_209
	jmp	.LBB5_853
.LBB5_211:                              # %vector.body10733.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_212:                              # %vector.body10733
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_212
# BB#213:                               # %vector.body10746.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_214:                              # %vector.body10746
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_214
# BB#215:                               # %vector.body10757.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_216:                              # %vector.body10757
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_216
# BB#217:                               # %vector.body10768.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_218:                              # %vector.body10768
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_218
	jmp	.LBB5_853
.LBB5_220:                              # %.preheader.i6437.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_221:                              # %vector.body10707
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_221
# BB#222:                               # %vector.body10720.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_223:                              # %vector.body10720
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_223
	jmp	.LBB5_853
.LBB5_225:                              # %.preheader.i6421.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_226:                              # %vector.body10679
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+902336(%rax)
	movaps	%xmm0, global_data+902352(%rax)
	movaps	%xmm0, global_data+902368(%rax)
	movaps	%xmm0, global_data+902384(%rax)
	movaps	%xmm0, global_data+902400(%rax)
	movaps	%xmm0, global_data+902416(%rax)
	movaps	%xmm0, global_data+902432(%rax)
	movaps	%xmm0, global_data+902448(%rax)
	movaps	%xmm0, global_data+902464(%rax)
	movaps	%xmm0, global_data+902480(%rax)
	movaps	%xmm0, global_data+902496(%rax)
	movaps	%xmm0, global_data+902512(%rax)
	movaps	%xmm0, global_data+902528(%rax)
	movaps	%xmm0, global_data+902544(%rax)
	movaps	%xmm0, global_data+902560(%rax)
	movaps	%xmm0, global_data+902576(%rax)
	movaps	%xmm0, global_data+902592(%rax)
	movaps	%xmm0, global_data+902608(%rax)
	movaps	%xmm0, global_data+902624(%rax)
	movaps	%xmm0, global_data+902640(%rax)
	movaps	%xmm0, global_data+902656(%rax)
	movaps	%xmm0, global_data+902672(%rax)
	movaps	%xmm0, global_data+902688(%rax)
	movaps	%xmm0, global_data+902704(%rax)
	movaps	%xmm0, global_data+902720(%rax)
	movaps	%xmm0, global_data+902736(%rax)
	movaps	%xmm0, global_data+902752(%rax)
	movaps	%xmm0, global_data+902768(%rax)
	movaps	%xmm0, global_data+902784(%rax)
	movaps	%xmm0, global_data+902800(%rax)
	movaps	%xmm0, global_data+902816(%rax)
	movaps	%xmm0, global_data+902832(%rax)
	movaps	%xmm0, global_data+902848(%rax)
	movaps	%xmm0, global_data+902864(%rax)
	movaps	%xmm0, global_data+902880(%rax)
	movaps	%xmm0, global_data+902896(%rax)
	movaps	%xmm0, global_data+902912(%rax)
	movaps	%xmm0, global_data+902928(%rax)
	movaps	%xmm0, global_data+902944(%rax)
	movaps	%xmm0, global_data+902960(%rax)
	movaps	%xmm0, global_data+902976(%rax)
	movaps	%xmm0, global_data+902992(%rax)
	movaps	%xmm0, global_data+903008(%rax)
	movaps	%xmm0, global_data+903024(%rax)
	movaps	%xmm0, global_data+903040(%rax)
	movaps	%xmm0, global_data+903056(%rax)
	movaps	%xmm0, global_data+903072(%rax)
	movaps	%xmm0, global_data+903088(%rax)
	movaps	%xmm0, global_data+903104(%rax)
	movaps	%xmm0, global_data+903120(%rax)
	movaps	%xmm0, global_data+903136(%rax)
	movaps	%xmm0, global_data+903152(%rax)
	movaps	%xmm0, global_data+903168(%rax)
	movaps	%xmm0, global_data+903184(%rax)
	movaps	%xmm0, global_data+903200(%rax)
	movaps	%xmm0, global_data+903216(%rax)
	movaps	%xmm0, global_data+903232(%rax)
	movaps	%xmm0, global_data+903248(%rax)
	movaps	%xmm0, global_data+903264(%rax)
	movaps	%xmm0, global_data+903280(%rax)
	movaps	%xmm0, global_data+903296(%rax)
	movaps	%xmm0, global_data+903312(%rax)
	movaps	%xmm0, global_data+903328(%rax)
	movaps	%xmm0, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_226
# BB#227:                               # %.preheader48.i6415.preheader
	movl	$global_data+903424, %eax
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_228:                              # %vector.body10692
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	imull	%edx, %edx
	cvtsi2ssl	%edx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, -1008(%rax)
	movaps	%xmm1, -992(%rax)
	movaps	%xmm1, -976(%rax)
	movaps	%xmm1, -960(%rax)
	movaps	%xmm1, -944(%rax)
	movaps	%xmm1, -928(%rax)
	movaps	%xmm1, -912(%rax)
	movaps	%xmm1, -896(%rax)
	movaps	%xmm1, -880(%rax)
	movaps	%xmm1, -864(%rax)
	movaps	%xmm1, -848(%rax)
	movaps	%xmm1, -832(%rax)
	movaps	%xmm1, -816(%rax)
	movaps	%xmm1, -800(%rax)
	movaps	%xmm1, -784(%rax)
	movaps	%xmm1, -768(%rax)
	movaps	%xmm1, -752(%rax)
	movaps	%xmm1, -736(%rax)
	movaps	%xmm1, -720(%rax)
	movaps	%xmm1, -704(%rax)
	movaps	%xmm1, -688(%rax)
	movaps	%xmm1, -672(%rax)
	movaps	%xmm1, -656(%rax)
	movaps	%xmm1, -640(%rax)
	movaps	%xmm1, -624(%rax)
	movaps	%xmm1, -608(%rax)
	movaps	%xmm1, -592(%rax)
	movaps	%xmm1, -576(%rax)
	movaps	%xmm1, -560(%rax)
	movaps	%xmm1, -544(%rax)
	movaps	%xmm1, -528(%rax)
	movaps	%xmm1, -512(%rax)
	movaps	%xmm1, -496(%rax)
	movaps	%xmm1, -480(%rax)
	movaps	%xmm1, -464(%rax)
	movaps	%xmm1, -448(%rax)
	movaps	%xmm1, -432(%rax)
	movaps	%xmm1, -416(%rax)
	movaps	%xmm1, -400(%rax)
	movaps	%xmm1, -384(%rax)
	movaps	%xmm1, -368(%rax)
	movaps	%xmm1, -352(%rax)
	movaps	%xmm1, -336(%rax)
	movaps	%xmm1, -320(%rax)
	movaps	%xmm1, -304(%rax)
	movaps	%xmm1, -288(%rax)
	movaps	%xmm1, -272(%rax)
	movaps	%xmm1, -256(%rax)
	movaps	%xmm1, -240(%rax)
	movaps	%xmm1, -224(%rax)
	movaps	%xmm1, -208(%rax)
	movaps	%xmm1, -192(%rax)
	movaps	%xmm1, -176(%rax)
	movaps	%xmm1, -160(%rax)
	movaps	%xmm1, -144(%rax)
	movaps	%xmm1, -128(%rax)
	movaps	%xmm1, -112(%rax)
	movaps	%xmm1, -96(%rax)
	movaps	%xmm1, -80(%rax)
	movaps	%xmm1, -64(%rax)
	movaps	%xmm1, -48(%rax)
	movaps	%xmm1, -32(%rax)
	movaps	%xmm1, -16(%rax)
	movaps	%xmm1, (%rax)
	addq	$1024, %rax             # imm = 0x400
	incq	%rcx
	cmpq	$257, %rcx              # imm = 0x101
	jne	.LBB5_228
	jmp	.LBB5_853
.LBB5_230:                              # %.preheader.i6405.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_231:                              # %vector.body10666
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+902336(%rax)
	movaps	%xmm0, global_data+902352(%rax)
	movaps	%xmm0, global_data+902368(%rax)
	movaps	%xmm0, global_data+902384(%rax)
	movaps	%xmm0, global_data+902400(%rax)
	movaps	%xmm0, global_data+902416(%rax)
	movaps	%xmm0, global_data+902432(%rax)
	movaps	%xmm0, global_data+902448(%rax)
	movaps	%xmm0, global_data+902464(%rax)
	movaps	%xmm0, global_data+902480(%rax)
	movaps	%xmm0, global_data+902496(%rax)
	movaps	%xmm0, global_data+902512(%rax)
	movaps	%xmm0, global_data+902528(%rax)
	movaps	%xmm0, global_data+902544(%rax)
	movaps	%xmm0, global_data+902560(%rax)
	movaps	%xmm0, global_data+902576(%rax)
	movaps	%xmm0, global_data+902592(%rax)
	movaps	%xmm0, global_data+902608(%rax)
	movaps	%xmm0, global_data+902624(%rax)
	movaps	%xmm0, global_data+902640(%rax)
	movaps	%xmm0, global_data+902656(%rax)
	movaps	%xmm0, global_data+902672(%rax)
	movaps	%xmm0, global_data+902688(%rax)
	movaps	%xmm0, global_data+902704(%rax)
	movaps	%xmm0, global_data+902720(%rax)
	movaps	%xmm0, global_data+902736(%rax)
	movaps	%xmm0, global_data+902752(%rax)
	movaps	%xmm0, global_data+902768(%rax)
	movaps	%xmm0, global_data+902784(%rax)
	movaps	%xmm0, global_data+902800(%rax)
	movaps	%xmm0, global_data+902816(%rax)
	movaps	%xmm0, global_data+902832(%rax)
	movaps	%xmm0, global_data+902848(%rax)
	movaps	%xmm0, global_data+902864(%rax)
	movaps	%xmm0, global_data+902880(%rax)
	movaps	%xmm0, global_data+902896(%rax)
	movaps	%xmm0, global_data+902912(%rax)
	movaps	%xmm0, global_data+902928(%rax)
	movaps	%xmm0, global_data+902944(%rax)
	movaps	%xmm0, global_data+902960(%rax)
	movaps	%xmm0, global_data+902976(%rax)
	movaps	%xmm0, global_data+902992(%rax)
	movaps	%xmm0, global_data+903008(%rax)
	movaps	%xmm0, global_data+903024(%rax)
	movaps	%xmm0, global_data+903040(%rax)
	movaps	%xmm0, global_data+903056(%rax)
	movaps	%xmm0, global_data+903072(%rax)
	movaps	%xmm0, global_data+903088(%rax)
	movaps	%xmm0, global_data+903104(%rax)
	movaps	%xmm0, global_data+903120(%rax)
	movaps	%xmm0, global_data+903136(%rax)
	movaps	%xmm0, global_data+903152(%rax)
	movaps	%xmm0, global_data+903168(%rax)
	movaps	%xmm0, global_data+903184(%rax)
	movaps	%xmm0, global_data+903200(%rax)
	movaps	%xmm0, global_data+903216(%rax)
	movaps	%xmm0, global_data+903232(%rax)
	movaps	%xmm0, global_data+903248(%rax)
	movaps	%xmm0, global_data+903264(%rax)
	movaps	%xmm0, global_data+903280(%rax)
	movaps	%xmm0, global_data+903296(%rax)
	movaps	%xmm0, global_data+903312(%rax)
	movaps	%xmm0, global_data+903328(%rax)
	movaps	%xmm0, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_231
# BB#232:                               # %.preheader.i6398.preheader
	movl	$global_data+902416, %edi
.LBB5_233:                              # %set1d.exit6912
	xorl	%esi, %esi
	movl	$262144, %edx           # imm = 0x40000
	callq	memset
	jmp	.LBB5_853
.LBB5_235:                              # %.preheader44.i6392.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_236:                              # %vector.body10621
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_236
# BB#237:                               # %.preheader44.i6383.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
.LBB5_238:                              # %vector.body10636
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1164560(%rax)
	movaps	%xmm1, global_data+1164576(%rax)
	movaps	%xmm1, global_data+1164592(%rax)
	movaps	%xmm1, global_data+1164608(%rax)
	movaps	%xmm1, global_data+1164624(%rax)
	movaps	%xmm1, global_data+1164640(%rax)
	movaps	%xmm1, global_data+1164656(%rax)
	movaps	%xmm1, global_data+1164672(%rax)
	movaps	%xmm1, global_data+1164688(%rax)
	movaps	%xmm1, global_data+1164704(%rax)
	movaps	%xmm1, global_data+1164720(%rax)
	movaps	%xmm1, global_data+1164736(%rax)
	movaps	%xmm1, global_data+1164752(%rax)
	movaps	%xmm1, global_data+1164768(%rax)
	movaps	%xmm1, global_data+1164784(%rax)
	movaps	%xmm1, global_data+1164800(%rax)
	movaps	%xmm1, global_data+1164816(%rax)
	movaps	%xmm1, global_data+1164832(%rax)
	movaps	%xmm1, global_data+1164848(%rax)
	movaps	%xmm1, global_data+1164864(%rax)
	movaps	%xmm1, global_data+1164880(%rax)
	movaps	%xmm1, global_data+1164896(%rax)
	movaps	%xmm1, global_data+1164912(%rax)
	movaps	%xmm1, global_data+1164928(%rax)
	movaps	%xmm1, global_data+1164944(%rax)
	movaps	%xmm1, global_data+1164960(%rax)
	movaps	%xmm1, global_data+1164976(%rax)
	movaps	%xmm1, global_data+1164992(%rax)
	movaps	%xmm1, global_data+1165008(%rax)
	movaps	%xmm1, global_data+1165024(%rax)
	movaps	%xmm1, global_data+1165040(%rax)
	movaps	%xmm1, global_data+1165056(%rax)
	movaps	%xmm1, global_data+1165072(%rax)
	movaps	%xmm1, global_data+1165088(%rax)
	movaps	%xmm1, global_data+1165104(%rax)
	movaps	%xmm1, global_data+1165120(%rax)
	movaps	%xmm1, global_data+1165136(%rax)
	movaps	%xmm1, global_data+1165152(%rax)
	movaps	%xmm1, global_data+1165168(%rax)
	movaps	%xmm1, global_data+1165184(%rax)
	movaps	%xmm1, global_data+1165200(%rax)
	movaps	%xmm1, global_data+1165216(%rax)
	movaps	%xmm1, global_data+1165232(%rax)
	movaps	%xmm1, global_data+1165248(%rax)
	movaps	%xmm1, global_data+1165264(%rax)
	movaps	%xmm1, global_data+1165280(%rax)
	movaps	%xmm1, global_data+1165296(%rax)
	movaps	%xmm1, global_data+1165312(%rax)
	movaps	%xmm1, global_data+1165328(%rax)
	movaps	%xmm1, global_data+1165344(%rax)
	movaps	%xmm1, global_data+1165360(%rax)
	movaps	%xmm1, global_data+1165376(%rax)
	movaps	%xmm1, global_data+1165392(%rax)
	movaps	%xmm1, global_data+1165408(%rax)
	movaps	%xmm1, global_data+1165424(%rax)
	movaps	%xmm1, global_data+1165440(%rax)
	movaps	%xmm1, global_data+1165456(%rax)
	movaps	%xmm1, global_data+1165472(%rax)
	movaps	%xmm1, global_data+1165488(%rax)
	movaps	%xmm1, global_data+1165504(%rax)
	movaps	%xmm1, global_data+1165520(%rax)
	movaps	%xmm1, global_data+1165536(%rax)
	movaps	%xmm1, global_data+1165552(%rax)
	movaps	%xmm1, global_data+1165568(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_238
# BB#239:                               # %.preheader44.i6374.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
.LBB5_240:                              # %vector.body10651
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1426784(%rax)
	movaps	%xmm1, global_data+1426800(%rax)
	movaps	%xmm1, global_data+1426816(%rax)
	movaps	%xmm1, global_data+1426832(%rax)
	movaps	%xmm1, global_data+1426848(%rax)
	movaps	%xmm1, global_data+1426864(%rax)
	movaps	%xmm1, global_data+1426880(%rax)
	movaps	%xmm1, global_data+1426896(%rax)
	movaps	%xmm1, global_data+1426912(%rax)
	movaps	%xmm1, global_data+1426928(%rax)
	movaps	%xmm1, global_data+1426944(%rax)
	movaps	%xmm1, global_data+1426960(%rax)
	movaps	%xmm1, global_data+1426976(%rax)
	movaps	%xmm1, global_data+1426992(%rax)
	movaps	%xmm1, global_data+1427008(%rax)
	movaps	%xmm1, global_data+1427024(%rax)
	movaps	%xmm1, global_data+1427040(%rax)
	movaps	%xmm1, global_data+1427056(%rax)
	movaps	%xmm1, global_data+1427072(%rax)
	movaps	%xmm1, global_data+1427088(%rax)
	movaps	%xmm1, global_data+1427104(%rax)
	movaps	%xmm1, global_data+1427120(%rax)
	movaps	%xmm1, global_data+1427136(%rax)
	movaps	%xmm1, global_data+1427152(%rax)
	movaps	%xmm1, global_data+1427168(%rax)
	movaps	%xmm1, global_data+1427184(%rax)
	movaps	%xmm1, global_data+1427200(%rax)
	movaps	%xmm1, global_data+1427216(%rax)
	movaps	%xmm1, global_data+1427232(%rax)
	movaps	%xmm1, global_data+1427248(%rax)
	movaps	%xmm1, global_data+1427264(%rax)
	movaps	%xmm1, global_data+1427280(%rax)
	movaps	%xmm1, global_data+1427296(%rax)
	movaps	%xmm1, global_data+1427312(%rax)
	movaps	%xmm1, global_data+1427328(%rax)
	movaps	%xmm1, global_data+1427344(%rax)
	movaps	%xmm1, global_data+1427360(%rax)
	movaps	%xmm1, global_data+1427376(%rax)
	movaps	%xmm1, global_data+1427392(%rax)
	movaps	%xmm1, global_data+1427408(%rax)
	movaps	%xmm1, global_data+1427424(%rax)
	movaps	%xmm1, global_data+1427440(%rax)
	movaps	%xmm1, global_data+1427456(%rax)
	movaps	%xmm1, global_data+1427472(%rax)
	movaps	%xmm1, global_data+1427488(%rax)
	movaps	%xmm1, global_data+1427504(%rax)
	movaps	%xmm1, global_data+1427520(%rax)
	movaps	%xmm1, global_data+1427536(%rax)
	movaps	%xmm1, global_data+1427552(%rax)
	movaps	%xmm1, global_data+1427568(%rax)
	movaps	%xmm1, global_data+1427584(%rax)
	movaps	%xmm1, global_data+1427600(%rax)
	movaps	%xmm1, global_data+1427616(%rax)
	movaps	%xmm1, global_data+1427632(%rax)
	movaps	%xmm1, global_data+1427648(%rax)
	movaps	%xmm1, global_data+1427664(%rax)
	movaps	%xmm1, global_data+1427680(%rax)
	movaps	%xmm1, global_data+1427696(%rax)
	movaps	%xmm1, global_data+1427712(%rax)
	movaps	%xmm1, global_data+1427728(%rax)
	movaps	%xmm1, global_data+1427744(%rax)
	movaps	%xmm1, global_data+1427760(%rax)
	movaps	%xmm1, global_data+1427776(%rax)
	movaps	%xmm1, global_data+1427792(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_240
	jmp	.LBB5_853
.LBB5_242:                              # %.preheader.i6364.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_243:                              # %vector.body10578
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+902336(%rax)
	movaps	%xmm0, global_data+902352(%rax)
	movaps	%xmm0, global_data+902368(%rax)
	movaps	%xmm0, global_data+902384(%rax)
	movaps	%xmm0, global_data+902400(%rax)
	movaps	%xmm0, global_data+902416(%rax)
	movaps	%xmm0, global_data+902432(%rax)
	movaps	%xmm0, global_data+902448(%rax)
	movaps	%xmm0, global_data+902464(%rax)
	movaps	%xmm0, global_data+902480(%rax)
	movaps	%xmm0, global_data+902496(%rax)
	movaps	%xmm0, global_data+902512(%rax)
	movaps	%xmm0, global_data+902528(%rax)
	movaps	%xmm0, global_data+902544(%rax)
	movaps	%xmm0, global_data+902560(%rax)
	movaps	%xmm0, global_data+902576(%rax)
	movaps	%xmm0, global_data+902592(%rax)
	movaps	%xmm0, global_data+902608(%rax)
	movaps	%xmm0, global_data+902624(%rax)
	movaps	%xmm0, global_data+902640(%rax)
	movaps	%xmm0, global_data+902656(%rax)
	movaps	%xmm0, global_data+902672(%rax)
	movaps	%xmm0, global_data+902688(%rax)
	movaps	%xmm0, global_data+902704(%rax)
	movaps	%xmm0, global_data+902720(%rax)
	movaps	%xmm0, global_data+902736(%rax)
	movaps	%xmm0, global_data+902752(%rax)
	movaps	%xmm0, global_data+902768(%rax)
	movaps	%xmm0, global_data+902784(%rax)
	movaps	%xmm0, global_data+902800(%rax)
	movaps	%xmm0, global_data+902816(%rax)
	movaps	%xmm0, global_data+902832(%rax)
	movaps	%xmm0, global_data+902848(%rax)
	movaps	%xmm0, global_data+902864(%rax)
	movaps	%xmm0, global_data+902880(%rax)
	movaps	%xmm0, global_data+902896(%rax)
	movaps	%xmm0, global_data+902912(%rax)
	movaps	%xmm0, global_data+902928(%rax)
	movaps	%xmm0, global_data+902944(%rax)
	movaps	%xmm0, global_data+902960(%rax)
	movaps	%xmm0, global_data+902976(%rax)
	movaps	%xmm0, global_data+902992(%rax)
	movaps	%xmm0, global_data+903008(%rax)
	movaps	%xmm0, global_data+903024(%rax)
	movaps	%xmm0, global_data+903040(%rax)
	movaps	%xmm0, global_data+903056(%rax)
	movaps	%xmm0, global_data+903072(%rax)
	movaps	%xmm0, global_data+903088(%rax)
	movaps	%xmm0, global_data+903104(%rax)
	movaps	%xmm0, global_data+903120(%rax)
	movaps	%xmm0, global_data+903136(%rax)
	movaps	%xmm0, global_data+903152(%rax)
	movaps	%xmm0, global_data+903168(%rax)
	movaps	%xmm0, global_data+903184(%rax)
	movaps	%xmm0, global_data+903200(%rax)
	movaps	%xmm0, global_data+903216(%rax)
	movaps	%xmm0, global_data+903232(%rax)
	movaps	%xmm0, global_data+903248(%rax)
	movaps	%xmm0, global_data+903264(%rax)
	movaps	%xmm0, global_data+903280(%rax)
	movaps	%xmm0, global_data+903296(%rax)
	movaps	%xmm0, global_data+903312(%rax)
	movaps	%xmm0, global_data+903328(%rax)
	movaps	%xmm0, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_243
# BB#244:                               # %.preheader44.i6358.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_245:                              # %vector.body10591
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1164560(%rax)
	movaps	%xmm1, global_data+1164576(%rax)
	movaps	%xmm1, global_data+1164592(%rax)
	movaps	%xmm1, global_data+1164608(%rax)
	movaps	%xmm1, global_data+1164624(%rax)
	movaps	%xmm1, global_data+1164640(%rax)
	movaps	%xmm1, global_data+1164656(%rax)
	movaps	%xmm1, global_data+1164672(%rax)
	movaps	%xmm1, global_data+1164688(%rax)
	movaps	%xmm1, global_data+1164704(%rax)
	movaps	%xmm1, global_data+1164720(%rax)
	movaps	%xmm1, global_data+1164736(%rax)
	movaps	%xmm1, global_data+1164752(%rax)
	movaps	%xmm1, global_data+1164768(%rax)
	movaps	%xmm1, global_data+1164784(%rax)
	movaps	%xmm1, global_data+1164800(%rax)
	movaps	%xmm1, global_data+1164816(%rax)
	movaps	%xmm1, global_data+1164832(%rax)
	movaps	%xmm1, global_data+1164848(%rax)
	movaps	%xmm1, global_data+1164864(%rax)
	movaps	%xmm1, global_data+1164880(%rax)
	movaps	%xmm1, global_data+1164896(%rax)
	movaps	%xmm1, global_data+1164912(%rax)
	movaps	%xmm1, global_data+1164928(%rax)
	movaps	%xmm1, global_data+1164944(%rax)
	movaps	%xmm1, global_data+1164960(%rax)
	movaps	%xmm1, global_data+1164976(%rax)
	movaps	%xmm1, global_data+1164992(%rax)
	movaps	%xmm1, global_data+1165008(%rax)
	movaps	%xmm1, global_data+1165024(%rax)
	movaps	%xmm1, global_data+1165040(%rax)
	movaps	%xmm1, global_data+1165056(%rax)
	movaps	%xmm1, global_data+1165072(%rax)
	movaps	%xmm1, global_data+1165088(%rax)
	movaps	%xmm1, global_data+1165104(%rax)
	movaps	%xmm1, global_data+1165120(%rax)
	movaps	%xmm1, global_data+1165136(%rax)
	movaps	%xmm1, global_data+1165152(%rax)
	movaps	%xmm1, global_data+1165168(%rax)
	movaps	%xmm1, global_data+1165184(%rax)
	movaps	%xmm1, global_data+1165200(%rax)
	movaps	%xmm1, global_data+1165216(%rax)
	movaps	%xmm1, global_data+1165232(%rax)
	movaps	%xmm1, global_data+1165248(%rax)
	movaps	%xmm1, global_data+1165264(%rax)
	movaps	%xmm1, global_data+1165280(%rax)
	movaps	%xmm1, global_data+1165296(%rax)
	movaps	%xmm1, global_data+1165312(%rax)
	movaps	%xmm1, global_data+1165328(%rax)
	movaps	%xmm1, global_data+1165344(%rax)
	movaps	%xmm1, global_data+1165360(%rax)
	movaps	%xmm1, global_data+1165376(%rax)
	movaps	%xmm1, global_data+1165392(%rax)
	movaps	%xmm1, global_data+1165408(%rax)
	movaps	%xmm1, global_data+1165424(%rax)
	movaps	%xmm1, global_data+1165440(%rax)
	movaps	%xmm1, global_data+1165456(%rax)
	movaps	%xmm1, global_data+1165472(%rax)
	movaps	%xmm1, global_data+1165488(%rax)
	movaps	%xmm1, global_data+1165504(%rax)
	movaps	%xmm1, global_data+1165520(%rax)
	movaps	%xmm1, global_data+1165536(%rax)
	movaps	%xmm1, global_data+1165552(%rax)
	movaps	%xmm1, global_data+1165568(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_245
# BB#246:                               # %.preheader44.i6349.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
.LBB5_247:                              # %vector.body10606
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1426784(%rax)
	movaps	%xmm1, global_data+1426800(%rax)
	movaps	%xmm1, global_data+1426816(%rax)
	movaps	%xmm1, global_data+1426832(%rax)
	movaps	%xmm1, global_data+1426848(%rax)
	movaps	%xmm1, global_data+1426864(%rax)
	movaps	%xmm1, global_data+1426880(%rax)
	movaps	%xmm1, global_data+1426896(%rax)
	movaps	%xmm1, global_data+1426912(%rax)
	movaps	%xmm1, global_data+1426928(%rax)
	movaps	%xmm1, global_data+1426944(%rax)
	movaps	%xmm1, global_data+1426960(%rax)
	movaps	%xmm1, global_data+1426976(%rax)
	movaps	%xmm1, global_data+1426992(%rax)
	movaps	%xmm1, global_data+1427008(%rax)
	movaps	%xmm1, global_data+1427024(%rax)
	movaps	%xmm1, global_data+1427040(%rax)
	movaps	%xmm1, global_data+1427056(%rax)
	movaps	%xmm1, global_data+1427072(%rax)
	movaps	%xmm1, global_data+1427088(%rax)
	movaps	%xmm1, global_data+1427104(%rax)
	movaps	%xmm1, global_data+1427120(%rax)
	movaps	%xmm1, global_data+1427136(%rax)
	movaps	%xmm1, global_data+1427152(%rax)
	movaps	%xmm1, global_data+1427168(%rax)
	movaps	%xmm1, global_data+1427184(%rax)
	movaps	%xmm1, global_data+1427200(%rax)
	movaps	%xmm1, global_data+1427216(%rax)
	movaps	%xmm1, global_data+1427232(%rax)
	movaps	%xmm1, global_data+1427248(%rax)
	movaps	%xmm1, global_data+1427264(%rax)
	movaps	%xmm1, global_data+1427280(%rax)
	movaps	%xmm1, global_data+1427296(%rax)
	movaps	%xmm1, global_data+1427312(%rax)
	movaps	%xmm1, global_data+1427328(%rax)
	movaps	%xmm1, global_data+1427344(%rax)
	movaps	%xmm1, global_data+1427360(%rax)
	movaps	%xmm1, global_data+1427376(%rax)
	movaps	%xmm1, global_data+1427392(%rax)
	movaps	%xmm1, global_data+1427408(%rax)
	movaps	%xmm1, global_data+1427424(%rax)
	movaps	%xmm1, global_data+1427440(%rax)
	movaps	%xmm1, global_data+1427456(%rax)
	movaps	%xmm1, global_data+1427472(%rax)
	movaps	%xmm1, global_data+1427488(%rax)
	movaps	%xmm1, global_data+1427504(%rax)
	movaps	%xmm1, global_data+1427520(%rax)
	movaps	%xmm1, global_data+1427536(%rax)
	movaps	%xmm1, global_data+1427552(%rax)
	movaps	%xmm1, global_data+1427568(%rax)
	movaps	%xmm1, global_data+1427584(%rax)
	movaps	%xmm1, global_data+1427600(%rax)
	movaps	%xmm1, global_data+1427616(%rax)
	movaps	%xmm1, global_data+1427632(%rax)
	movaps	%xmm1, global_data+1427648(%rax)
	movaps	%xmm1, global_data+1427664(%rax)
	movaps	%xmm1, global_data+1427680(%rax)
	movaps	%xmm1, global_data+1427696(%rax)
	movaps	%xmm1, global_data+1427712(%rax)
	movaps	%xmm1, global_data+1427728(%rax)
	movaps	%xmm1, global_data+1427744(%rax)
	movaps	%xmm1, global_data+1427760(%rax)
	movaps	%xmm1, global_data+1427776(%rax)
	movaps	%xmm1, global_data+1427792(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_247
	jmp	.LBB5_853
.LBB5_249:                              # %vector.body10515.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_250:                              # %vector.body10515
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_250
# BB#251:                               # %vector.body10528.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_252:                              # %vector.body10528
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_252
# BB#253:                               # %vector.body10539.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_254:                              # %vector.body10539
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_254
# BB#255:                               # %.preheader.i6325.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
.LBB5_256:                              # %vector.body10550
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_256
# BB#257:                               # %.preheader48.i.preheader
	movl	$global_data+903424, %eax
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_258:                              # %vector.body10563
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	imull	%edx, %edx
	cvtsi2ssl	%edx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, -1008(%rax)
	movaps	%xmm1, -992(%rax)
	movaps	%xmm1, -976(%rax)
	movaps	%xmm1, -960(%rax)
	movaps	%xmm1, -944(%rax)
	movaps	%xmm1, -928(%rax)
	movaps	%xmm1, -912(%rax)
	movaps	%xmm1, -896(%rax)
	movaps	%xmm1, -880(%rax)
	movaps	%xmm1, -864(%rax)
	movaps	%xmm1, -848(%rax)
	movaps	%xmm1, -832(%rax)
	movaps	%xmm1, -816(%rax)
	movaps	%xmm1, -800(%rax)
	movaps	%xmm1, -784(%rax)
	movaps	%xmm1, -768(%rax)
	movaps	%xmm1, -752(%rax)
	movaps	%xmm1, -736(%rax)
	movaps	%xmm1, -720(%rax)
	movaps	%xmm1, -704(%rax)
	movaps	%xmm1, -688(%rax)
	movaps	%xmm1, -672(%rax)
	movaps	%xmm1, -656(%rax)
	movaps	%xmm1, -640(%rax)
	movaps	%xmm1, -624(%rax)
	movaps	%xmm1, -608(%rax)
	movaps	%xmm1, -592(%rax)
	movaps	%xmm1, -576(%rax)
	movaps	%xmm1, -560(%rax)
	movaps	%xmm1, -544(%rax)
	movaps	%xmm1, -528(%rax)
	movaps	%xmm1, -512(%rax)
	movaps	%xmm1, -496(%rax)
	movaps	%xmm1, -480(%rax)
	movaps	%xmm1, -464(%rax)
	movaps	%xmm1, -448(%rax)
	movaps	%xmm1, -432(%rax)
	movaps	%xmm1, -416(%rax)
	movaps	%xmm1, -400(%rax)
	movaps	%xmm1, -384(%rax)
	movaps	%xmm1, -368(%rax)
	movaps	%xmm1, -352(%rax)
	movaps	%xmm1, -336(%rax)
	movaps	%xmm1, -320(%rax)
	movaps	%xmm1, -304(%rax)
	movaps	%xmm1, -288(%rax)
	movaps	%xmm1, -272(%rax)
	movaps	%xmm1, -256(%rax)
	movaps	%xmm1, -240(%rax)
	movaps	%xmm1, -224(%rax)
	movaps	%xmm1, -208(%rax)
	movaps	%xmm1, -192(%rax)
	movaps	%xmm1, -176(%rax)
	movaps	%xmm1, -160(%rax)
	movaps	%xmm1, -144(%rax)
	movaps	%xmm1, -128(%rax)
	movaps	%xmm1, -112(%rax)
	movaps	%xmm1, -96(%rax)
	movaps	%xmm1, -80(%rax)
	movaps	%xmm1, -64(%rax)
	movaps	%xmm1, -48(%rax)
	movaps	%xmm1, -32(%rax)
	movaps	%xmm1, -16(%rax)
	movaps	%xmm1, (%rax)
	addq	$1024, %rax             # imm = 0x400
	incq	%rcx
	cmpq	$257, %rcx              # imm = 0x101
	jne	.LBB5_258
	jmp	.LBB5_853
.LBB5_260:                              # %vector.body10463.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_261:                              # %vector.body10463
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_261
# BB#262:                               # %vector.body10476.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_263:                              # %vector.body10476
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_263
# BB#264:                               # %vector.body10489.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_265:                              # %vector.body10489
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_265
# BB#266:                               # %vector.body10502.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_267:                              # %vector.body10502
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	movaps	%xmm0, global_data+512288(%rax)
	movaps	%xmm0, global_data+512304(%rax)
	movaps	%xmm0, global_data+512320(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_267
	jmp	.LBB5_853
.LBB5_269:                              # %vector.body10411.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_11(%rip), %xmm0  # xmm0 = [1.000000e-06,1.000000e-06,1.000000e-06,1.000000e-06]
.LBB5_270:                              # %vector.body10411
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_270
# BB#271:                               # %vector.body10424.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_272:                              # %vector.body10424
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_272
# BB#273:                               # %vector.body10437.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_274:                              # %vector.body10437
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_274
# BB#275:                               # %vector.body10450.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_276:                              # %vector.body10450
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	movaps	%xmm0, global_data+512288(%rax)
	movaps	%xmm0, global_data+512304(%rax)
	movaps	%xmm0, global_data+512320(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_276
	jmp	.LBB5_853
.LBB5_278:                              # %.preheader.i6289.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_279:                              # %vector.body10365
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_279
# BB#280:                               # %vector.body10378.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_281:                              # %vector.body10378
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_281
# BB#282:                               # %vector.body10389.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_283:                              # %vector.body10389
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_283
# BB#284:                               # %vector.body10400.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_285:                              # %vector.body10400
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_285
	jmp	.LBB5_853
.LBB5_287:                              # %.preheader.i6266.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_288:                              # %vector.body10326
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_288
# BB#289:                               # %vector.body10339.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_11(%rip), %xmm0  # xmm0 = [1.000000e-06,1.000000e-06,1.000000e-06,1.000000e-06]
.LBB5_290:                              # %vector.body10339
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_290
# BB#291:                               # %vector.body10352.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_292:                              # %vector.body10352
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	movaps	%xmm0, global_data+512288(%rax)
	movaps	%xmm0, global_data+512304(%rax)
	movaps	%xmm0, global_data+512320(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_292
	jmp	.LBB5_853
.LBB5_294:                              # %.preheader.i6250.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_295:                              # %vector.body10280
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_295
# BB#296:                               # %vector.body10293.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_297:                              # %vector.body10293
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_297
# BB#298:                               # %vector.body10304.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_299:                              # %vector.body10304
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_299
# BB#300:                               # %vector.body10315.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_301:                              # %vector.body10315
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_301
	jmp	.LBB5_853
.LBB5_303:                              # %.preheader.i6227.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_304:                              # %vector.body10254
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_304
# BB#305:                               # %vector.body10267.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_306:                              # %vector.body10267
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_306
	jmp	.LBB5_853
.LBB5_308:                              # %vector.body10204.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_309:                              # %vector.body10204
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm3, global_data+128000(%rax)
	movaps	%xmm3, global_data+128016(%rax)
	movaps	%xmm3, global_data+128032(%rax)
	movaps	%xmm3, global_data+128048(%rax)
	movaps	%xmm3, global_data+128064(%rax)
	movaps	%xmm3, global_data+128080(%rax)
	movaps	%xmm3, global_data+128096(%rax)
	movaps	%xmm3, global_data+128112(%rax)
	movaps	%xmm3, global_data+128128(%rax)
	movaps	%xmm3, global_data+128144(%rax)
	movaps	%xmm3, global_data+128160(%rax)
	movaps	%xmm3, global_data+128176(%rax)
	movaps	%xmm3, global_data+128192(%rax)
	movaps	%xmm3, global_data+128208(%rax)
	movaps	%xmm3, global_data+128224(%rax)
	movaps	%xmm3, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_309
# BB#310:                               # %vector.body10217.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_11(%rip), %xmm1  # xmm1 = [1.000000e-06,1.000000e-06,1.000000e-06,1.000000e-06]
.LBB5_311:                              # %vector.body10217
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_311
# BB#312:                               # %vector.body10230.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_313:                              # %vector.body10230
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm3, global_data+384048(%rax)
	movaps	%xmm3, global_data+384064(%rax)
	movaps	%xmm3, global_data+384080(%rax)
	movaps	%xmm3, global_data+384096(%rax)
	movaps	%xmm3, global_data+384112(%rax)
	movaps	%xmm3, global_data+384128(%rax)
	movaps	%xmm3, global_data+384144(%rax)
	movaps	%xmm3, global_data+384160(%rax)
	movaps	%xmm3, global_data+384176(%rax)
	movaps	%xmm3, global_data+384192(%rax)
	movaps	%xmm3, global_data+384208(%rax)
	movaps	%xmm3, global_data+384224(%rax)
	movaps	%xmm3, global_data+384240(%rax)
	movaps	%xmm3, global_data+384256(%rax)
	movaps	%xmm3, global_data+384272(%rax)
	movaps	%xmm3, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_313
# BB#314:                               # %vector.body10243.preheader
	movl	$1, %eax
	movd	%rax, %xmm1
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_315:                              # %vector.body10243
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	shufps	$136, %xmm2, %xmm6      # xmm6 = xmm6[0,2],xmm2[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm3, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+512080(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm5, %xmm1
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_315
	jmp	.LBB5_853
.LBB5_317:                              # %.preheader.i6198.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_318:                              # %vector.body10191
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_318
	jmp	.LBB5_853
.LBB5_320:                              # %.preheader.i6190.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_321:                              # %vector.body10178
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_321
	jmp	.LBB5_853
.LBB5_323:                              # %vector.body10139.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_324:                              # %vector.body10139
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_324
# BB#325:                               # %.preheader.i6174.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_6(%rip), %xmm1   # xmm1 = [2.000000e+00,2.000000e+00,2.000000e+00,2.000000e+00]
.LBB5_326:                              # %vector.body10152
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_326
# BB#327:                               # %.preheader.i6167.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
.LBB5_328:                              # %vector.body10165
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1164560(%rax)
	movaps	%xmm0, global_data+1164576(%rax)
	movaps	%xmm0, global_data+1164592(%rax)
	movaps	%xmm0, global_data+1164608(%rax)
	movaps	%xmm0, global_data+1164624(%rax)
	movaps	%xmm0, global_data+1164640(%rax)
	movaps	%xmm0, global_data+1164656(%rax)
	movaps	%xmm0, global_data+1164672(%rax)
	movaps	%xmm0, global_data+1164688(%rax)
	movaps	%xmm0, global_data+1164704(%rax)
	movaps	%xmm0, global_data+1164720(%rax)
	movaps	%xmm0, global_data+1164736(%rax)
	movaps	%xmm0, global_data+1164752(%rax)
	movaps	%xmm0, global_data+1164768(%rax)
	movaps	%xmm0, global_data+1164784(%rax)
	movaps	%xmm0, global_data+1164800(%rax)
	movaps	%xmm0, global_data+1164816(%rax)
	movaps	%xmm0, global_data+1164832(%rax)
	movaps	%xmm0, global_data+1164848(%rax)
	movaps	%xmm0, global_data+1164864(%rax)
	movaps	%xmm0, global_data+1164880(%rax)
	movaps	%xmm0, global_data+1164896(%rax)
	movaps	%xmm0, global_data+1164912(%rax)
	movaps	%xmm0, global_data+1164928(%rax)
	movaps	%xmm0, global_data+1164944(%rax)
	movaps	%xmm0, global_data+1164960(%rax)
	movaps	%xmm0, global_data+1164976(%rax)
	movaps	%xmm0, global_data+1164992(%rax)
	movaps	%xmm0, global_data+1165008(%rax)
	movaps	%xmm0, global_data+1165024(%rax)
	movaps	%xmm0, global_data+1165040(%rax)
	movaps	%xmm0, global_data+1165056(%rax)
	movaps	%xmm0, global_data+1165072(%rax)
	movaps	%xmm0, global_data+1165088(%rax)
	movaps	%xmm0, global_data+1165104(%rax)
	movaps	%xmm0, global_data+1165120(%rax)
	movaps	%xmm0, global_data+1165136(%rax)
	movaps	%xmm0, global_data+1165152(%rax)
	movaps	%xmm0, global_data+1165168(%rax)
	movaps	%xmm0, global_data+1165184(%rax)
	movaps	%xmm0, global_data+1165200(%rax)
	movaps	%xmm0, global_data+1165216(%rax)
	movaps	%xmm0, global_data+1165232(%rax)
	movaps	%xmm0, global_data+1165248(%rax)
	movaps	%xmm0, global_data+1165264(%rax)
	movaps	%xmm0, global_data+1165280(%rax)
	movaps	%xmm0, global_data+1165296(%rax)
	movaps	%xmm0, global_data+1165312(%rax)
	movaps	%xmm0, global_data+1165328(%rax)
	movaps	%xmm0, global_data+1165344(%rax)
	movaps	%xmm0, global_data+1165360(%rax)
	movaps	%xmm0, global_data+1165376(%rax)
	movaps	%xmm0, global_data+1165392(%rax)
	movaps	%xmm0, global_data+1165408(%rax)
	movaps	%xmm0, global_data+1165424(%rax)
	movaps	%xmm0, global_data+1165440(%rax)
	movaps	%xmm0, global_data+1165456(%rax)
	movaps	%xmm0, global_data+1165472(%rax)
	movaps	%xmm0, global_data+1165488(%rax)
	movaps	%xmm0, global_data+1165504(%rax)
	movaps	%xmm0, global_data+1165520(%rax)
	movaps	%xmm0, global_data+1165536(%rax)
	movaps	%xmm0, global_data+1165552(%rax)
	movaps	%xmm0, global_data+1165568(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_328
	jmp	.LBB5_853
.LBB5_330:                              # %vector.body10100.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_331:                              # %vector.body10100
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_331
# BB#332:                               # %.preheader.i6156.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_6(%rip), %xmm1   # xmm1 = [2.000000e+00,2.000000e+00,2.000000e+00,2.000000e+00]
.LBB5_333:                              # %vector.body10113
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_333
# BB#334:                               # %.preheader.i6149.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
.LBB5_335:                              # %vector.body10126
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1164560(%rax)
	movaps	%xmm0, global_data+1164576(%rax)
	movaps	%xmm0, global_data+1164592(%rax)
	movaps	%xmm0, global_data+1164608(%rax)
	movaps	%xmm0, global_data+1164624(%rax)
	movaps	%xmm0, global_data+1164640(%rax)
	movaps	%xmm0, global_data+1164656(%rax)
	movaps	%xmm0, global_data+1164672(%rax)
	movaps	%xmm0, global_data+1164688(%rax)
	movaps	%xmm0, global_data+1164704(%rax)
	movaps	%xmm0, global_data+1164720(%rax)
	movaps	%xmm0, global_data+1164736(%rax)
	movaps	%xmm0, global_data+1164752(%rax)
	movaps	%xmm0, global_data+1164768(%rax)
	movaps	%xmm0, global_data+1164784(%rax)
	movaps	%xmm0, global_data+1164800(%rax)
	movaps	%xmm0, global_data+1164816(%rax)
	movaps	%xmm0, global_data+1164832(%rax)
	movaps	%xmm0, global_data+1164848(%rax)
	movaps	%xmm0, global_data+1164864(%rax)
	movaps	%xmm0, global_data+1164880(%rax)
	movaps	%xmm0, global_data+1164896(%rax)
	movaps	%xmm0, global_data+1164912(%rax)
	movaps	%xmm0, global_data+1164928(%rax)
	movaps	%xmm0, global_data+1164944(%rax)
	movaps	%xmm0, global_data+1164960(%rax)
	movaps	%xmm0, global_data+1164976(%rax)
	movaps	%xmm0, global_data+1164992(%rax)
	movaps	%xmm0, global_data+1165008(%rax)
	movaps	%xmm0, global_data+1165024(%rax)
	movaps	%xmm0, global_data+1165040(%rax)
	movaps	%xmm0, global_data+1165056(%rax)
	movaps	%xmm0, global_data+1165072(%rax)
	movaps	%xmm0, global_data+1165088(%rax)
	movaps	%xmm0, global_data+1165104(%rax)
	movaps	%xmm0, global_data+1165120(%rax)
	movaps	%xmm0, global_data+1165136(%rax)
	movaps	%xmm0, global_data+1165152(%rax)
	movaps	%xmm0, global_data+1165168(%rax)
	movaps	%xmm0, global_data+1165184(%rax)
	movaps	%xmm0, global_data+1165200(%rax)
	movaps	%xmm0, global_data+1165216(%rax)
	movaps	%xmm0, global_data+1165232(%rax)
	movaps	%xmm0, global_data+1165248(%rax)
	movaps	%xmm0, global_data+1165264(%rax)
	movaps	%xmm0, global_data+1165280(%rax)
	movaps	%xmm0, global_data+1165296(%rax)
	movaps	%xmm0, global_data+1165312(%rax)
	movaps	%xmm0, global_data+1165328(%rax)
	movaps	%xmm0, global_data+1165344(%rax)
	movaps	%xmm0, global_data+1165360(%rax)
	movaps	%xmm0, global_data+1165376(%rax)
	movaps	%xmm0, global_data+1165392(%rax)
	movaps	%xmm0, global_data+1165408(%rax)
	movaps	%xmm0, global_data+1165424(%rax)
	movaps	%xmm0, global_data+1165440(%rax)
	movaps	%xmm0, global_data+1165456(%rax)
	movaps	%xmm0, global_data+1165472(%rax)
	movaps	%xmm0, global_data+1165488(%rax)
	movaps	%xmm0, global_data+1165504(%rax)
	movaps	%xmm0, global_data+1165520(%rax)
	movaps	%xmm0, global_data+1165536(%rax)
	movaps	%xmm0, global_data+1165552(%rax)
	movaps	%xmm0, global_data+1165568(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_335
	jmp	.LBB5_853
.LBB5_337:                              # %vector.body10052.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_338:                              # %vector.body10052
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm2
	shufps	$136, %xmm1, %xmm2      # xmm2 = xmm2[0,2],xmm1[0,2]
	movaps	%xmm2, %xmm3
	paddd	.LCPI5_1(%rip), %xmm3
	cvtdq2ps	%xmm3, %xmm3
	movaps	.LCPI5_2(%rip), %xmm4   # xmm4 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movaps	%xmm4, %xmm5
	divps	%xmm3, %xmm4
	movaps	%xmm4, global_data+128000(%rax)
	paddd	.LCPI5_3(%rip), %xmm2
	cvtdq2ps	%xmm2, %xmm2
	movaps	%xmm5, %xmm3
	divps	%xmm2, %xmm3
	movaps	%xmm3, global_data+128016(%rax)
	movdqa	.LCPI5_4(%rip), %xmm2   # xmm2 = [8,8]
	paddq	%xmm2, %xmm0
	paddq	%xmm2, %xmm1
	addq	$32, %rax
	jne	.LBB5_338
# BB#339:                               # %.preheader.i6141.preheader
	movl	$global_data+128016, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_340:                              # %vector.body10063
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm2
	shufps	$136, %xmm1, %xmm2      # xmm2 = xmm2[0,2],xmm1[0,2]
	movaps	%xmm2, %xmm3
	paddd	.LCPI5_1(%rip), %xmm3
	cvtdq2ps	%xmm3, %xmm3
	movaps	.LCPI5_2(%rip), %xmm4   # xmm4 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movaps	%xmm4, %xmm5
	divps	%xmm3, %xmm4
	movaps	%xmm4, global_data+384048(%rax)
	paddd	.LCPI5_3(%rip), %xmm2
	cvtdq2ps	%xmm2, %xmm2
	movaps	%xmm5, %xmm3
	divps	%xmm2, %xmm3
	movaps	%xmm3, global_data+384064(%rax)
	movdqa	.LCPI5_4(%rip), %xmm2   # xmm2 = [8,8]
	paddq	%xmm2, %xmm0
	paddq	%xmm2, %xmm1
	addq	$32, %rax
	jne	.LBB5_340
# BB#341:                               # %vector.body10074.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_342:                              # %vector.body10074
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm2
	shufps	$136, %xmm1, %xmm2      # xmm2 = xmm2[0,2],xmm1[0,2]
	movaps	%xmm2, %xmm3
	paddd	.LCPI5_1(%rip), %xmm3
	cvtdq2ps	%xmm3, %xmm3
	movaps	.LCPI5_2(%rip), %xmm4   # xmm4 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movaps	%xmm4, %xmm5
	divps	%xmm3, %xmm4
	movaps	%xmm4, global_data+512080(%rax)
	paddd	.LCPI5_3(%rip), %xmm2
	cvtdq2ps	%xmm2, %xmm2
	movaps	%xmm5, %xmm3
	divps	%xmm2, %xmm3
	movaps	%xmm3, global_data+512096(%rax)
	movdqa	.LCPI5_4(%rip), %xmm2   # xmm2 = [8,8]
	paddq	%xmm2, %xmm0
	paddq	%xmm2, %xmm1
	addq	$32, %rax
	jne	.LBB5_342
# BB#343:                               # %.preheader.i6127.preheader
	movl	$global_data+512128, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_344:                              # %vector.body10085
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_344
	jmp	.LBB5_853
.LBB5_346:                              # %vector.body10004.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_347:                              # %vector.body10004
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_347
# BB#348:                               # %vector.body10017.preheader
	movl	$1, %eax
	movd	%rax, %xmm1
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm2   # xmm2 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_349:                              # %vector.body10017
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm5, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm1, %xmm4
	paddq	%xmm8, %xmm4
	movdqa	%xmm4, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm4, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm4, %xmm4
	paddq	%xmm7, %xmm4
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm4      # xmm4 = xmm4[0,2],xmm6[0,2]
	cvtdq2ps	%xmm4, %xmm4
	movaps	%xmm0, %xmm6
	divps	%xmm4, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm5, %xmm4
	paddq	%xmm2, %xmm4
	movdqa	%xmm1, %xmm6
	paddq	%xmm2, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm4, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm4, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm4, %xmm4
	paddq	%xmm7, %xmm4
	shufps	$136, %xmm4, %xmm6      # xmm6 = xmm6[0,2],xmm4[0,2]
	cvtdq2ps	%xmm6, %xmm4
	movaps	%xmm0, %xmm6
	divps	%xmm4, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm1
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_349
# BB#350:                               # %vector.body10028.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_351:                              # %vector.body10028
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm5, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm4, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+384048(%rax)
	movdqa	%xmm5, %xmm1
	paddq	%xmm2, %xmm1
	movdqa	%xmm4, %xmm6
	paddq	%xmm2, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_351
# BB#352:                               # %vector.body10039.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_353:                              # %vector.body10039
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	movaps	%xmm0, global_data+512288(%rax)
	movaps	%xmm0, global_data+512304(%rax)
	movaps	%xmm0, global_data+512320(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_353
	jmp	.LBB5_853
.LBB5_355:                              # %vector.body9969.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_356:                              # %vector.body9969
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_356
# BB#357:                               # %vector.body9982.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_358:                              # %vector.body9982
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_358
# BB#359:                               # %vector.body9993.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_360:                              # %vector.body9993
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_360
	jmp	.LBB5_853
.LBB5_362:                              # %vector.body9908.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_363:                              # %vector.body9908
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_363
# BB#364:                               # %vector.body9921.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_365:                              # %vector.body9921
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_365
# BB#366:                               # %vector.body9934.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_367:                              # %vector.body9934
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_367
# BB#368:                               # %vector.body9945.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_369:                              # %vector.body9945
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_369
# BB#370:                               # %vector.body9956.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_6(%rip), %xmm0   # xmm0 = [2.000000e+00,2.000000e+00,2.000000e+00,2.000000e+00]
.LBB5_371:                              # %vector.body9956
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+640128(%rax)
	movaps	%xmm0, global_data+640144(%rax)
	movaps	%xmm0, global_data+640160(%rax)
	movaps	%xmm0, global_data+640176(%rax)
	movaps	%xmm0, global_data+640192(%rax)
	movaps	%xmm0, global_data+640208(%rax)
	movaps	%xmm0, global_data+640224(%rax)
	movaps	%xmm0, global_data+640240(%rax)
	movaps	%xmm0, global_data+640256(%rax)
	movaps	%xmm0, global_data+640272(%rax)
	movaps	%xmm0, global_data+640288(%rax)
	movaps	%xmm0, global_data+640304(%rax)
	movaps	%xmm0, global_data+640320(%rax)
	movaps	%xmm0, global_data+640336(%rax)
	movaps	%xmm0, global_data+640352(%rax)
	movaps	%xmm0, global_data+640368(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_371
	jmp	.LBB5_853
.LBB5_373:                              # %vector.body9845.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_374:                              # %vector.body9845
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm3, global_data+128000(%rax)
	movaps	%xmm3, global_data+128016(%rax)
	movaps	%xmm3, global_data+128032(%rax)
	movaps	%xmm3, global_data+128048(%rax)
	movaps	%xmm3, global_data+128064(%rax)
	movaps	%xmm3, global_data+128080(%rax)
	movaps	%xmm3, global_data+128096(%rax)
	movaps	%xmm3, global_data+128112(%rax)
	movaps	%xmm3, global_data+128128(%rax)
	movaps	%xmm3, global_data+128144(%rax)
	movaps	%xmm3, global_data+128160(%rax)
	movaps	%xmm3, global_data+128176(%rax)
	movaps	%xmm3, global_data+128192(%rax)
	movaps	%xmm3, global_data+128208(%rax)
	movaps	%xmm3, global_data+128224(%rax)
	movaps	%xmm3, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_374
# BB#375:                               # %vector.body9858.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_376:                              # %vector.body9858
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm3, global_data+256016(%rax)
	movaps	%xmm3, global_data+256032(%rax)
	movaps	%xmm3, global_data+256048(%rax)
	movaps	%xmm3, global_data+256064(%rax)
	movaps	%xmm3, global_data+256080(%rax)
	movaps	%xmm3, global_data+256096(%rax)
	movaps	%xmm3, global_data+256112(%rax)
	movaps	%xmm3, global_data+256128(%rax)
	movaps	%xmm3, global_data+256144(%rax)
	movaps	%xmm3, global_data+256160(%rax)
	movaps	%xmm3, global_data+256176(%rax)
	movaps	%xmm3, global_data+256192(%rax)
	movaps	%xmm3, global_data+256208(%rax)
	movaps	%xmm3, global_data+256224(%rax)
	movaps	%xmm3, global_data+256240(%rax)
	movaps	%xmm3, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_376
# BB#377:                               # %vector.body9871.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_378:                              # %vector.body9871
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm3, global_data+384048(%rax)
	movaps	%xmm3, global_data+384064(%rax)
	movaps	%xmm3, global_data+384080(%rax)
	movaps	%xmm3, global_data+384096(%rax)
	movaps	%xmm3, global_data+384112(%rax)
	movaps	%xmm3, global_data+384128(%rax)
	movaps	%xmm3, global_data+384144(%rax)
	movaps	%xmm3, global_data+384160(%rax)
	movaps	%xmm3, global_data+384176(%rax)
	movaps	%xmm3, global_data+384192(%rax)
	movaps	%xmm3, global_data+384208(%rax)
	movaps	%xmm3, global_data+384224(%rax)
	movaps	%xmm3, global_data+384240(%rax)
	movaps	%xmm3, global_data+384256(%rax)
	movaps	%xmm3, global_data+384272(%rax)
	movaps	%xmm3, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_378
# BB#379:                               # %vector.body9884.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_11(%rip), %xmm1  # xmm1 = [1.000000e-06,1.000000e-06,1.000000e-06,1.000000e-06]
.LBB5_380:                              # %vector.body9884
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+512080(%rax)
	movaps	%xmm1, global_data+512096(%rax)
	movaps	%xmm1, global_data+512112(%rax)
	movaps	%xmm1, global_data+512128(%rax)
	movaps	%xmm1, global_data+512144(%rax)
	movaps	%xmm1, global_data+512160(%rax)
	movaps	%xmm1, global_data+512176(%rax)
	movaps	%xmm1, global_data+512192(%rax)
	movaps	%xmm1, global_data+512208(%rax)
	movaps	%xmm1, global_data+512224(%rax)
	movaps	%xmm1, global_data+512240(%rax)
	movaps	%xmm1, global_data+512256(%rax)
	movaps	%xmm1, global_data+512272(%rax)
	movaps	%xmm1, global_data+512288(%rax)
	movaps	%xmm1, global_data+512304(%rax)
	movaps	%xmm1, global_data+512320(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_380
# BB#381:                               # %vector.body9897.preheader
	movl	$1, %eax
	movd	%rax, %xmm1
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_382:                              # %vector.body9897
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	shufps	$136, %xmm2, %xmm6      # xmm6 = xmm6[0,2],xmm2[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm3, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+640128(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm5, %xmm1
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_382
	jmp	.LBB5_853
.LBB5_384:                              # %.preheader.i6039.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_385:                              # %vector.body9797
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_385
# BB#386:                               # %vector.body9810.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_387:                              # %vector.body9810
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+384048(%rax)
	movaps	%xmm1, global_data+384064(%rax)
	movaps	%xmm1, global_data+384080(%rax)
	movaps	%xmm1, global_data+384096(%rax)
	movaps	%xmm1, global_data+384112(%rax)
	movaps	%xmm1, global_data+384128(%rax)
	movaps	%xmm1, global_data+384144(%rax)
	movaps	%xmm1, global_data+384160(%rax)
	movaps	%xmm1, global_data+384176(%rax)
	movaps	%xmm1, global_data+384192(%rax)
	movaps	%xmm1, global_data+384208(%rax)
	movaps	%xmm1, global_data+384224(%rax)
	movaps	%xmm1, global_data+384240(%rax)
	movaps	%xmm1, global_data+384256(%rax)
	movaps	%xmm1, global_data+384272(%rax)
	movaps	%xmm1, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_387
# BB#388:                               # %vector.body9823.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_389:                              # %vector.body9823
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+512080(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_389
# BB#390:                               # %vector.body9834.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_391:                              # %vector.body9834
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_391
	jmp	.LBB5_853
.LBB5_393:                              # %.preheader.i6013.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_394:                              # %vector.body9758
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+902336(%rax)
	movaps	%xmm0, global_data+902352(%rax)
	movaps	%xmm0, global_data+902368(%rax)
	movaps	%xmm0, global_data+902384(%rax)
	movaps	%xmm0, global_data+902400(%rax)
	movaps	%xmm0, global_data+902416(%rax)
	movaps	%xmm0, global_data+902432(%rax)
	movaps	%xmm0, global_data+902448(%rax)
	movaps	%xmm0, global_data+902464(%rax)
	movaps	%xmm0, global_data+902480(%rax)
	movaps	%xmm0, global_data+902496(%rax)
	movaps	%xmm0, global_data+902512(%rax)
	movaps	%xmm0, global_data+902528(%rax)
	movaps	%xmm0, global_data+902544(%rax)
	movaps	%xmm0, global_data+902560(%rax)
	movaps	%xmm0, global_data+902576(%rax)
	movaps	%xmm0, global_data+902592(%rax)
	movaps	%xmm0, global_data+902608(%rax)
	movaps	%xmm0, global_data+902624(%rax)
	movaps	%xmm0, global_data+902640(%rax)
	movaps	%xmm0, global_data+902656(%rax)
	movaps	%xmm0, global_data+902672(%rax)
	movaps	%xmm0, global_data+902688(%rax)
	movaps	%xmm0, global_data+902704(%rax)
	movaps	%xmm0, global_data+902720(%rax)
	movaps	%xmm0, global_data+902736(%rax)
	movaps	%xmm0, global_data+902752(%rax)
	movaps	%xmm0, global_data+902768(%rax)
	movaps	%xmm0, global_data+902784(%rax)
	movaps	%xmm0, global_data+902800(%rax)
	movaps	%xmm0, global_data+902816(%rax)
	movaps	%xmm0, global_data+902832(%rax)
	movaps	%xmm0, global_data+902848(%rax)
	movaps	%xmm0, global_data+902864(%rax)
	movaps	%xmm0, global_data+902880(%rax)
	movaps	%xmm0, global_data+902896(%rax)
	movaps	%xmm0, global_data+902912(%rax)
	movaps	%xmm0, global_data+902928(%rax)
	movaps	%xmm0, global_data+902944(%rax)
	movaps	%xmm0, global_data+902960(%rax)
	movaps	%xmm0, global_data+902976(%rax)
	movaps	%xmm0, global_data+902992(%rax)
	movaps	%xmm0, global_data+903008(%rax)
	movaps	%xmm0, global_data+903024(%rax)
	movaps	%xmm0, global_data+903040(%rax)
	movaps	%xmm0, global_data+903056(%rax)
	movaps	%xmm0, global_data+903072(%rax)
	movaps	%xmm0, global_data+903088(%rax)
	movaps	%xmm0, global_data+903104(%rax)
	movaps	%xmm0, global_data+903120(%rax)
	movaps	%xmm0, global_data+903136(%rax)
	movaps	%xmm0, global_data+903152(%rax)
	movaps	%xmm0, global_data+903168(%rax)
	movaps	%xmm0, global_data+903184(%rax)
	movaps	%xmm0, global_data+903200(%rax)
	movaps	%xmm0, global_data+903216(%rax)
	movaps	%xmm0, global_data+903232(%rax)
	movaps	%xmm0, global_data+903248(%rax)
	movaps	%xmm0, global_data+903264(%rax)
	movaps	%xmm0, global_data+903280(%rax)
	movaps	%xmm0, global_data+903296(%rax)
	movaps	%xmm0, global_data+903312(%rax)
	movaps	%xmm0, global_data+903328(%rax)
	movaps	%xmm0, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_394
# BB#395:                               # %.preheader.i6006.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_11(%rip), %xmm0  # xmm0 = [1.000000e-06,1.000000e-06,1.000000e-06,1.000000e-06]
.LBB5_396:                              # %vector.body9771
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1164560(%rax)
	movaps	%xmm0, global_data+1164576(%rax)
	movaps	%xmm0, global_data+1164592(%rax)
	movaps	%xmm0, global_data+1164608(%rax)
	movaps	%xmm0, global_data+1164624(%rax)
	movaps	%xmm0, global_data+1164640(%rax)
	movaps	%xmm0, global_data+1164656(%rax)
	movaps	%xmm0, global_data+1164672(%rax)
	movaps	%xmm0, global_data+1164688(%rax)
	movaps	%xmm0, global_data+1164704(%rax)
	movaps	%xmm0, global_data+1164720(%rax)
	movaps	%xmm0, global_data+1164736(%rax)
	movaps	%xmm0, global_data+1164752(%rax)
	movaps	%xmm0, global_data+1164768(%rax)
	movaps	%xmm0, global_data+1164784(%rax)
	movaps	%xmm0, global_data+1164800(%rax)
	movaps	%xmm0, global_data+1164816(%rax)
	movaps	%xmm0, global_data+1164832(%rax)
	movaps	%xmm0, global_data+1164848(%rax)
	movaps	%xmm0, global_data+1164864(%rax)
	movaps	%xmm0, global_data+1164880(%rax)
	movaps	%xmm0, global_data+1164896(%rax)
	movaps	%xmm0, global_data+1164912(%rax)
	movaps	%xmm0, global_data+1164928(%rax)
	movaps	%xmm0, global_data+1164944(%rax)
	movaps	%xmm0, global_data+1164960(%rax)
	movaps	%xmm0, global_data+1164976(%rax)
	movaps	%xmm0, global_data+1164992(%rax)
	movaps	%xmm0, global_data+1165008(%rax)
	movaps	%xmm0, global_data+1165024(%rax)
	movaps	%xmm0, global_data+1165040(%rax)
	movaps	%xmm0, global_data+1165056(%rax)
	movaps	%xmm0, global_data+1165072(%rax)
	movaps	%xmm0, global_data+1165088(%rax)
	movaps	%xmm0, global_data+1165104(%rax)
	movaps	%xmm0, global_data+1165120(%rax)
	movaps	%xmm0, global_data+1165136(%rax)
	movaps	%xmm0, global_data+1165152(%rax)
	movaps	%xmm0, global_data+1165168(%rax)
	movaps	%xmm0, global_data+1165184(%rax)
	movaps	%xmm0, global_data+1165200(%rax)
	movaps	%xmm0, global_data+1165216(%rax)
	movaps	%xmm0, global_data+1165232(%rax)
	movaps	%xmm0, global_data+1165248(%rax)
	movaps	%xmm0, global_data+1165264(%rax)
	movaps	%xmm0, global_data+1165280(%rax)
	movaps	%xmm0, global_data+1165296(%rax)
	movaps	%xmm0, global_data+1165312(%rax)
	movaps	%xmm0, global_data+1165328(%rax)
	movaps	%xmm0, global_data+1165344(%rax)
	movaps	%xmm0, global_data+1165360(%rax)
	movaps	%xmm0, global_data+1165376(%rax)
	movaps	%xmm0, global_data+1165392(%rax)
	movaps	%xmm0, global_data+1165408(%rax)
	movaps	%xmm0, global_data+1165424(%rax)
	movaps	%xmm0, global_data+1165440(%rax)
	movaps	%xmm0, global_data+1165456(%rax)
	movaps	%xmm0, global_data+1165472(%rax)
	movaps	%xmm0, global_data+1165488(%rax)
	movaps	%xmm0, global_data+1165504(%rax)
	movaps	%xmm0, global_data+1165520(%rax)
	movaps	%xmm0, global_data+1165536(%rax)
	movaps	%xmm0, global_data+1165552(%rax)
	movaps	%xmm0, global_data+1165568(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_396
# BB#397:                               # %.preheader.i5999.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
.LBB5_398:                              # %vector.body9784
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1426784(%rax)
	movaps	%xmm0, global_data+1426800(%rax)
	movaps	%xmm0, global_data+1426816(%rax)
	movaps	%xmm0, global_data+1426832(%rax)
	movaps	%xmm0, global_data+1426848(%rax)
	movaps	%xmm0, global_data+1426864(%rax)
	movaps	%xmm0, global_data+1426880(%rax)
	movaps	%xmm0, global_data+1426896(%rax)
	movaps	%xmm0, global_data+1426912(%rax)
	movaps	%xmm0, global_data+1426928(%rax)
	movaps	%xmm0, global_data+1426944(%rax)
	movaps	%xmm0, global_data+1426960(%rax)
	movaps	%xmm0, global_data+1426976(%rax)
	movaps	%xmm0, global_data+1426992(%rax)
	movaps	%xmm0, global_data+1427008(%rax)
	movaps	%xmm0, global_data+1427024(%rax)
	movaps	%xmm0, global_data+1427040(%rax)
	movaps	%xmm0, global_data+1427056(%rax)
	movaps	%xmm0, global_data+1427072(%rax)
	movaps	%xmm0, global_data+1427088(%rax)
	movaps	%xmm0, global_data+1427104(%rax)
	movaps	%xmm0, global_data+1427120(%rax)
	movaps	%xmm0, global_data+1427136(%rax)
	movaps	%xmm0, global_data+1427152(%rax)
	movaps	%xmm0, global_data+1427168(%rax)
	movaps	%xmm0, global_data+1427184(%rax)
	movaps	%xmm0, global_data+1427200(%rax)
	movaps	%xmm0, global_data+1427216(%rax)
	movaps	%xmm0, global_data+1427232(%rax)
	movaps	%xmm0, global_data+1427248(%rax)
	movaps	%xmm0, global_data+1427264(%rax)
	movaps	%xmm0, global_data+1427280(%rax)
	movaps	%xmm0, global_data+1427296(%rax)
	movaps	%xmm0, global_data+1427312(%rax)
	movaps	%xmm0, global_data+1427328(%rax)
	movaps	%xmm0, global_data+1427344(%rax)
	movaps	%xmm0, global_data+1427360(%rax)
	movaps	%xmm0, global_data+1427376(%rax)
	movaps	%xmm0, global_data+1427392(%rax)
	movaps	%xmm0, global_data+1427408(%rax)
	movaps	%xmm0, global_data+1427424(%rax)
	movaps	%xmm0, global_data+1427440(%rax)
	movaps	%xmm0, global_data+1427456(%rax)
	movaps	%xmm0, global_data+1427472(%rax)
	movaps	%xmm0, global_data+1427488(%rax)
	movaps	%xmm0, global_data+1427504(%rax)
	movaps	%xmm0, global_data+1427520(%rax)
	movaps	%xmm0, global_data+1427536(%rax)
	movaps	%xmm0, global_data+1427552(%rax)
	movaps	%xmm0, global_data+1427568(%rax)
	movaps	%xmm0, global_data+1427584(%rax)
	movaps	%xmm0, global_data+1427600(%rax)
	movaps	%xmm0, global_data+1427616(%rax)
	movaps	%xmm0, global_data+1427632(%rax)
	movaps	%xmm0, global_data+1427648(%rax)
	movaps	%xmm0, global_data+1427664(%rax)
	movaps	%xmm0, global_data+1427680(%rax)
	movaps	%xmm0, global_data+1427696(%rax)
	movaps	%xmm0, global_data+1427712(%rax)
	movaps	%xmm0, global_data+1427728(%rax)
	movaps	%xmm0, global_data+1427744(%rax)
	movaps	%xmm0, global_data+1427760(%rax)
	movaps	%xmm0, global_data+1427776(%rax)
	movaps	%xmm0, global_data+1427792(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_398
	jmp	.LBB5_853
.LBB5_400:                              # %vector.body9712.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_401:                              # %vector.body9712
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_401
# BB#402:                               # %vector.body9725.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_403:                              # %vector.body9725
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_403
# BB#404:                               # %vector.body9736.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_405:                              # %vector.body9736
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_405
# BB#406:                               # %vector.body9747.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_407:                              # %vector.body9747
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_407
	jmp	.LBB5_853
.LBB5_409:                              # %vector.body9640.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_410:                              # %vector.body9640
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm2, global_data+128000(%rax)
	movaps	%xmm2, global_data+128016(%rax)
	movaps	%xmm2, global_data+128032(%rax)
	movaps	%xmm2, global_data+128048(%rax)
	movaps	%xmm2, global_data+128064(%rax)
	movaps	%xmm2, global_data+128080(%rax)
	movaps	%xmm2, global_data+128096(%rax)
	movaps	%xmm2, global_data+128112(%rax)
	movaps	%xmm2, global_data+128128(%rax)
	movaps	%xmm2, global_data+128144(%rax)
	movaps	%xmm2, global_data+128160(%rax)
	movaps	%xmm2, global_data+128176(%rax)
	movaps	%xmm2, global_data+128192(%rax)
	movaps	%xmm2, global_data+128208(%rax)
	movaps	%xmm2, global_data+128224(%rax)
	movaps	%xmm2, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_410
# BB#411:                               # %vector.body9653.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_412:                              # %vector.body9653
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm2, global_data+256016(%rax)
	movaps	%xmm2, global_data+256032(%rax)
	movaps	%xmm2, global_data+256048(%rax)
	movaps	%xmm2, global_data+256064(%rax)
	movaps	%xmm2, global_data+256080(%rax)
	movaps	%xmm2, global_data+256096(%rax)
	movaps	%xmm2, global_data+256112(%rax)
	movaps	%xmm2, global_data+256128(%rax)
	movaps	%xmm2, global_data+256144(%rax)
	movaps	%xmm2, global_data+256160(%rax)
	movaps	%xmm2, global_data+256176(%rax)
	movaps	%xmm2, global_data+256192(%rax)
	movaps	%xmm2, global_data+256208(%rax)
	movaps	%xmm2, global_data+256224(%rax)
	movaps	%xmm2, global_data+256240(%rax)
	movaps	%xmm2, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_412
# BB#413:                               # %vector.body9666.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_10(%rip), %xmm1  # xmm1 = [-1.000000e+00,-1.000000e+00,-1.000000e+00,-1.000000e+00]
.LBB5_414:                              # %vector.body9666
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+320016(%rax)
	movaps	%xmm1, global_data+320032(%rax)
	movaps	%xmm1, global_data+320048(%rax)
	movaps	%xmm1, global_data+320064(%rax)
	movaps	%xmm1, global_data+320080(%rax)
	movaps	%xmm1, global_data+320096(%rax)
	movaps	%xmm1, global_data+320112(%rax)
	movaps	%xmm1, global_data+320128(%rax)
	movaps	%xmm1, global_data+320144(%rax)
	movaps	%xmm1, global_data+320160(%rax)
	movaps	%xmm1, global_data+320176(%rax)
	movaps	%xmm1, global_data+320192(%rax)
	movaps	%xmm1, global_data+320208(%rax)
	movaps	%xmm1, global_data+320224(%rax)
	movaps	%xmm1, global_data+320240(%rax)
	movaps	%xmm1, global_data+320256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_414
# BB#415:                               # %vector.body9679.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm1   # xmm1 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_416:                              # %vector.body9679
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm1, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm2, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_416
# BB#417:                               # %vector.body9690.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_418:                              # %vector.body9690
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm2, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm1, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm2, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_418
# BB#419:                               # %vector.body9701.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_420:                              # %vector.body9701
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm2, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm1, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm2, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_420
	jmp	.LBB5_853
.LBB5_422:                              # %vector.body9568.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_10(%rip), %xmm0  # xmm0 = [-1.000000e+00,-1.000000e+00,-1.000000e+00,-1.000000e+00]
.LBB5_423:                              # %vector.body9568
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_423
# BB#424:                               # %vector.body9581.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_425:                              # %vector.body9581
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+192000(%rax)
	movaps	%xmm1, global_data+192016(%rax)
	movaps	%xmm1, global_data+192032(%rax)
	movaps	%xmm1, global_data+192048(%rax)
	movaps	%xmm1, global_data+192064(%rax)
	movaps	%xmm1, global_data+192080(%rax)
	movaps	%xmm1, global_data+192096(%rax)
	movaps	%xmm1, global_data+192112(%rax)
	movaps	%xmm1, global_data+192128(%rax)
	movaps	%xmm1, global_data+192144(%rax)
	movaps	%xmm1, global_data+192160(%rax)
	movaps	%xmm1, global_data+192176(%rax)
	movaps	%xmm1, global_data+192192(%rax)
	movaps	%xmm1, global_data+192208(%rax)
	movaps	%xmm1, global_data+192224(%rax)
	movaps	%xmm1, global_data+192240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_425
# BB#426:                               # %vector.body9594.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_427:                              # %vector.body9594
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_427
# BB#428:                               # %vector.body9607.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_429:                              # %vector.body9607
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_429
# BB#430:                               # %vector.body9618.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_431:                              # %vector.body9618
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_431
# BB#432:                               # %vector.body9629.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_433:                              # %vector.body9629
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_433
	jmp	.LBB5_853
.LBB5_435:                              # %vector.body9496.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_10(%rip), %xmm0  # xmm0 = [-1.000000e+00,-1.000000e+00,-1.000000e+00,-1.000000e+00]
.LBB5_436:                              # %vector.body9496
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_436
# BB#437:                               # %vector.body9509.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_438:                              # %vector.body9509
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+192000(%rax)
	movaps	%xmm1, global_data+192016(%rax)
	movaps	%xmm1, global_data+192032(%rax)
	movaps	%xmm1, global_data+192048(%rax)
	movaps	%xmm1, global_data+192064(%rax)
	movaps	%xmm1, global_data+192080(%rax)
	movaps	%xmm1, global_data+192096(%rax)
	movaps	%xmm1, global_data+192112(%rax)
	movaps	%xmm1, global_data+192128(%rax)
	movaps	%xmm1, global_data+192144(%rax)
	movaps	%xmm1, global_data+192160(%rax)
	movaps	%xmm1, global_data+192176(%rax)
	movaps	%xmm1, global_data+192192(%rax)
	movaps	%xmm1, global_data+192208(%rax)
	movaps	%xmm1, global_data+192224(%rax)
	movaps	%xmm1, global_data+192240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_438
# BB#439:                               # %vector.body9522.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_440:                              # %vector.body9522
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_440
# BB#441:                               # %vector.body9535.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_442:                              # %vector.body9535
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_442
# BB#443:                               # %vector.body9546.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_444:                              # %vector.body9546
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_444
# BB#445:                               # %vector.body9557.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_446:                              # %vector.body9557
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_446
	jmp	.LBB5_853
.LBB5_448:                              # %vector.body9437.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_449:                              # %vector.body9437
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_449
# BB#450:                               # %vector.body9450.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_451:                              # %vector.body9450
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_451
# BB#452:                               # %vector.body9463.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_453:                              # %vector.body9463
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_453
# BB#454:                               # %vector.body9474.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_455:                              # %vector.body9474
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_455
# BB#456:                               # %vector.body9485.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_457:                              # %vector.body9485
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_457
	jmp	.LBB5_853
.LBB5_459:                              # %vector.body9402.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_460:                              # %vector.body9402
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_460
# BB#461:                               # %vector.body9415.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_462:                              # %vector.body9415
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_462
# BB#463:                               # %vector.body9426.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_464:                              # %vector.body9426
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_464
	jmp	.LBB5_853
.LBB5_466:                              # %vector.body9367.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_467:                              # %vector.body9367
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_467
# BB#468:                               # %vector.body9380.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_469:                              # %vector.body9380
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_469
# BB#470:                               # %vector.body9391.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_471:                              # %vector.body9391
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_471
	jmp	.LBB5_853
.LBB5_473:                              # %.preheader.i5845.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_474:                              # %vector.body9341
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_474
# BB#475:                               # %vector.body9354.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_476:                              # %vector.body9354
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_476
	jmp	.LBB5_853
.LBB5_478:                              # %.preheader.i5833.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_479:                              # %vector.body9328
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_479
	jmp	.LBB5_853
.LBB5_481:                              # %.preheader.i5825.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_482:                              # %vector.body9315
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_482
	jmp	.LBB5_853
.LBB5_484:                              # %vector.body9304.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_485:                              # %vector.body9304
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_485
	jmp	.LBB5_853
.LBB5_494:                              # %.preheader.i5783.preheader
	movl	$global_data+640192, %edi
	jmp	.LBB5_233
.LBB5_487:                              # %.preheader.i5808.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_488:                              # %vector.body9261
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+902336(%rax)
	movaps	%xmm0, global_data+902352(%rax)
	movaps	%xmm0, global_data+902368(%rax)
	movaps	%xmm0, global_data+902384(%rax)
	movaps	%xmm0, global_data+902400(%rax)
	movaps	%xmm0, global_data+902416(%rax)
	movaps	%xmm0, global_data+902432(%rax)
	movaps	%xmm0, global_data+902448(%rax)
	movaps	%xmm0, global_data+902464(%rax)
	movaps	%xmm0, global_data+902480(%rax)
	movaps	%xmm0, global_data+902496(%rax)
	movaps	%xmm0, global_data+902512(%rax)
	movaps	%xmm0, global_data+902528(%rax)
	movaps	%xmm0, global_data+902544(%rax)
	movaps	%xmm0, global_data+902560(%rax)
	movaps	%xmm0, global_data+902576(%rax)
	movaps	%xmm0, global_data+902592(%rax)
	movaps	%xmm0, global_data+902608(%rax)
	movaps	%xmm0, global_data+902624(%rax)
	movaps	%xmm0, global_data+902640(%rax)
	movaps	%xmm0, global_data+902656(%rax)
	movaps	%xmm0, global_data+902672(%rax)
	movaps	%xmm0, global_data+902688(%rax)
	movaps	%xmm0, global_data+902704(%rax)
	movaps	%xmm0, global_data+902720(%rax)
	movaps	%xmm0, global_data+902736(%rax)
	movaps	%xmm0, global_data+902752(%rax)
	movaps	%xmm0, global_data+902768(%rax)
	movaps	%xmm0, global_data+902784(%rax)
	movaps	%xmm0, global_data+902800(%rax)
	movaps	%xmm0, global_data+902816(%rax)
	movaps	%xmm0, global_data+902832(%rax)
	movaps	%xmm0, global_data+902848(%rax)
	movaps	%xmm0, global_data+902864(%rax)
	movaps	%xmm0, global_data+902880(%rax)
	movaps	%xmm0, global_data+902896(%rax)
	movaps	%xmm0, global_data+902912(%rax)
	movaps	%xmm0, global_data+902928(%rax)
	movaps	%xmm0, global_data+902944(%rax)
	movaps	%xmm0, global_data+902960(%rax)
	movaps	%xmm0, global_data+902976(%rax)
	movaps	%xmm0, global_data+902992(%rax)
	movaps	%xmm0, global_data+903008(%rax)
	movaps	%xmm0, global_data+903024(%rax)
	movaps	%xmm0, global_data+903040(%rax)
	movaps	%xmm0, global_data+903056(%rax)
	movaps	%xmm0, global_data+903072(%rax)
	movaps	%xmm0, global_data+903088(%rax)
	movaps	%xmm0, global_data+903104(%rax)
	movaps	%xmm0, global_data+903120(%rax)
	movaps	%xmm0, global_data+903136(%rax)
	movaps	%xmm0, global_data+903152(%rax)
	movaps	%xmm0, global_data+903168(%rax)
	movaps	%xmm0, global_data+903184(%rax)
	movaps	%xmm0, global_data+903200(%rax)
	movaps	%xmm0, global_data+903216(%rax)
	movaps	%xmm0, global_data+903232(%rax)
	movaps	%xmm0, global_data+903248(%rax)
	movaps	%xmm0, global_data+903264(%rax)
	movaps	%xmm0, global_data+903280(%rax)
	movaps	%xmm0, global_data+903296(%rax)
	movaps	%xmm0, global_data+903312(%rax)
	movaps	%xmm0, global_data+903328(%rax)
	movaps	%xmm0, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_488
# BB#489:                               # %.preheader44.i5802.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_490:                              # %vector.body9274
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1164560(%rax)
	movaps	%xmm1, global_data+1164576(%rax)
	movaps	%xmm1, global_data+1164592(%rax)
	movaps	%xmm1, global_data+1164608(%rax)
	movaps	%xmm1, global_data+1164624(%rax)
	movaps	%xmm1, global_data+1164640(%rax)
	movaps	%xmm1, global_data+1164656(%rax)
	movaps	%xmm1, global_data+1164672(%rax)
	movaps	%xmm1, global_data+1164688(%rax)
	movaps	%xmm1, global_data+1164704(%rax)
	movaps	%xmm1, global_data+1164720(%rax)
	movaps	%xmm1, global_data+1164736(%rax)
	movaps	%xmm1, global_data+1164752(%rax)
	movaps	%xmm1, global_data+1164768(%rax)
	movaps	%xmm1, global_data+1164784(%rax)
	movaps	%xmm1, global_data+1164800(%rax)
	movaps	%xmm1, global_data+1164816(%rax)
	movaps	%xmm1, global_data+1164832(%rax)
	movaps	%xmm1, global_data+1164848(%rax)
	movaps	%xmm1, global_data+1164864(%rax)
	movaps	%xmm1, global_data+1164880(%rax)
	movaps	%xmm1, global_data+1164896(%rax)
	movaps	%xmm1, global_data+1164912(%rax)
	movaps	%xmm1, global_data+1164928(%rax)
	movaps	%xmm1, global_data+1164944(%rax)
	movaps	%xmm1, global_data+1164960(%rax)
	movaps	%xmm1, global_data+1164976(%rax)
	movaps	%xmm1, global_data+1164992(%rax)
	movaps	%xmm1, global_data+1165008(%rax)
	movaps	%xmm1, global_data+1165024(%rax)
	movaps	%xmm1, global_data+1165040(%rax)
	movaps	%xmm1, global_data+1165056(%rax)
	movaps	%xmm1, global_data+1165072(%rax)
	movaps	%xmm1, global_data+1165088(%rax)
	movaps	%xmm1, global_data+1165104(%rax)
	movaps	%xmm1, global_data+1165120(%rax)
	movaps	%xmm1, global_data+1165136(%rax)
	movaps	%xmm1, global_data+1165152(%rax)
	movaps	%xmm1, global_data+1165168(%rax)
	movaps	%xmm1, global_data+1165184(%rax)
	movaps	%xmm1, global_data+1165200(%rax)
	movaps	%xmm1, global_data+1165216(%rax)
	movaps	%xmm1, global_data+1165232(%rax)
	movaps	%xmm1, global_data+1165248(%rax)
	movaps	%xmm1, global_data+1165264(%rax)
	movaps	%xmm1, global_data+1165280(%rax)
	movaps	%xmm1, global_data+1165296(%rax)
	movaps	%xmm1, global_data+1165312(%rax)
	movaps	%xmm1, global_data+1165328(%rax)
	movaps	%xmm1, global_data+1165344(%rax)
	movaps	%xmm1, global_data+1165360(%rax)
	movaps	%xmm1, global_data+1165376(%rax)
	movaps	%xmm1, global_data+1165392(%rax)
	movaps	%xmm1, global_data+1165408(%rax)
	movaps	%xmm1, global_data+1165424(%rax)
	movaps	%xmm1, global_data+1165440(%rax)
	movaps	%xmm1, global_data+1165456(%rax)
	movaps	%xmm1, global_data+1165472(%rax)
	movaps	%xmm1, global_data+1165488(%rax)
	movaps	%xmm1, global_data+1165504(%rax)
	movaps	%xmm1, global_data+1165520(%rax)
	movaps	%xmm1, global_data+1165536(%rax)
	movaps	%xmm1, global_data+1165552(%rax)
	movaps	%xmm1, global_data+1165568(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_490
# BB#491:                               # %.preheader44.i5793.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
.LBB5_492:                              # %vector.body9289
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1426784(%rax)
	movaps	%xmm1, global_data+1426800(%rax)
	movaps	%xmm1, global_data+1426816(%rax)
	movaps	%xmm1, global_data+1426832(%rax)
	movaps	%xmm1, global_data+1426848(%rax)
	movaps	%xmm1, global_data+1426864(%rax)
	movaps	%xmm1, global_data+1426880(%rax)
	movaps	%xmm1, global_data+1426896(%rax)
	movaps	%xmm1, global_data+1426912(%rax)
	movaps	%xmm1, global_data+1426928(%rax)
	movaps	%xmm1, global_data+1426944(%rax)
	movaps	%xmm1, global_data+1426960(%rax)
	movaps	%xmm1, global_data+1426976(%rax)
	movaps	%xmm1, global_data+1426992(%rax)
	movaps	%xmm1, global_data+1427008(%rax)
	movaps	%xmm1, global_data+1427024(%rax)
	movaps	%xmm1, global_data+1427040(%rax)
	movaps	%xmm1, global_data+1427056(%rax)
	movaps	%xmm1, global_data+1427072(%rax)
	movaps	%xmm1, global_data+1427088(%rax)
	movaps	%xmm1, global_data+1427104(%rax)
	movaps	%xmm1, global_data+1427120(%rax)
	movaps	%xmm1, global_data+1427136(%rax)
	movaps	%xmm1, global_data+1427152(%rax)
	movaps	%xmm1, global_data+1427168(%rax)
	movaps	%xmm1, global_data+1427184(%rax)
	movaps	%xmm1, global_data+1427200(%rax)
	movaps	%xmm1, global_data+1427216(%rax)
	movaps	%xmm1, global_data+1427232(%rax)
	movaps	%xmm1, global_data+1427248(%rax)
	movaps	%xmm1, global_data+1427264(%rax)
	movaps	%xmm1, global_data+1427280(%rax)
	movaps	%xmm1, global_data+1427296(%rax)
	movaps	%xmm1, global_data+1427312(%rax)
	movaps	%xmm1, global_data+1427328(%rax)
	movaps	%xmm1, global_data+1427344(%rax)
	movaps	%xmm1, global_data+1427360(%rax)
	movaps	%xmm1, global_data+1427376(%rax)
	movaps	%xmm1, global_data+1427392(%rax)
	movaps	%xmm1, global_data+1427408(%rax)
	movaps	%xmm1, global_data+1427424(%rax)
	movaps	%xmm1, global_data+1427440(%rax)
	movaps	%xmm1, global_data+1427456(%rax)
	movaps	%xmm1, global_data+1427472(%rax)
	movaps	%xmm1, global_data+1427488(%rax)
	movaps	%xmm1, global_data+1427504(%rax)
	movaps	%xmm1, global_data+1427520(%rax)
	movaps	%xmm1, global_data+1427536(%rax)
	movaps	%xmm1, global_data+1427552(%rax)
	movaps	%xmm1, global_data+1427568(%rax)
	movaps	%xmm1, global_data+1427584(%rax)
	movaps	%xmm1, global_data+1427600(%rax)
	movaps	%xmm1, global_data+1427616(%rax)
	movaps	%xmm1, global_data+1427632(%rax)
	movaps	%xmm1, global_data+1427648(%rax)
	movaps	%xmm1, global_data+1427664(%rax)
	movaps	%xmm1, global_data+1427680(%rax)
	movaps	%xmm1, global_data+1427696(%rax)
	movaps	%xmm1, global_data+1427712(%rax)
	movaps	%xmm1, global_data+1427728(%rax)
	movaps	%xmm1, global_data+1427744(%rax)
	movaps	%xmm1, global_data+1427760(%rax)
	movaps	%xmm1, global_data+1427776(%rax)
	movaps	%xmm1, global_data+1427792(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_492
.LBB5_853:                              # %set1d.exit6912
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB5_497:                              # %vector.body9250.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_498:                              # %vector.body9250
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_498
	jmp	.LBB5_853
.LBB5_500:                              # %vector.body9237.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_12(%rip), %xmm0  # xmm0 = [1.000001e+00,1.000001e+00,1.000001e+00,1.000001e+00]
.LBB5_501:                              # %vector.body9237
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_501
	jmp	.LBB5_853
.LBB5_503:                              # %vector.body9215.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_504:                              # %vector.body9215
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm0, %xmm1
	divps	%xmm7, %xmm1
	movaps	%xmm1, global_data+128000(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_504
# BB#505:                               # %vector.body9226.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_506:                              # %vector.body9226
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+256016(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_506
	jmp	.LBB5_853
.LBB5_508:                              # %vector.body9204.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_509:                              # %vector.body9204
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_509
	jmp	.LBB5_853
.LBB5_511:                              # %vector.body9193.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_512:                              # %vector.body9193
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_512
	jmp	.LBB5_853
.LBB5_514:                              # %vector.body9182.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_515:                              # %vector.body9182
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_515
	jmp	.LBB5_853
.LBB5_518:                              # %vector.body9171.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_519:                              # %vector.body9171
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_519
# BB#520:                               # %set1d.exit5740
	movl	$-1073741824, global_data+127996(%rip) # imm = 0xC0000000
	jmp	.LBB5_853
.LBB5_522:                              # %.preheader.i5734.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$global_data+128016, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_523:                              # %vector.body9138
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm0, %xmm1
	divps	%xmm7, %xmm1
	movaps	%xmm1, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_523
# BB#524:                               # %vector.body9149.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_525:                              # %vector.body9149
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_525
# BB#526:                               # %vector.body9160.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_527:                              # %vector.body9160
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_527
	jmp	.LBB5_853
.LBB5_529:                              # %.preheader44.i5708.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_530:                              # %vector.body9123
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_530
# BB#531:                               # %set2d.exit5712
	movl	$1073741824, global_data+902332(%rip) # imm = 0x40000000
	jmp	.LBB5_853
.LBB5_533:                              # %vector.body9112.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_534:                              # %vector.body9112
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_534
	jmp	.LBB5_853
.LBB5_536:                              # %vector.body9101.preheader
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_537:                              # %vector.body9101
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+128000(%rax)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_537
	jmp	.LBB5_538
.LBB5_541:                              # %vector.body9090.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_542:                              # %vector.body9090
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_542
# BB#543:                               # %set1d.exit5689
	movl	$-1073741824, global_data+127996(%rip) # imm = 0xC0000000
	jmp	.LBB5_853
.LBB5_545:                              # %vector.body9077.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_546:                              # %vector.body9077
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_546
.LBB5_538:                              # %.preheader.i5692.preheader
	movl	$global_data+128016, %edi
.LBB5_539:                              # %set1d.exit6912
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	jmp	.LBB5_853
.LBB5_548:                              # %vector.body9064.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_549:                              # %vector.body9064
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_549
# BB#550:                               # %.preheader.i5671.preheader
	movl	$global_data+128016, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$global_data+256048, %edi
	jmp	.LBB5_539
.LBB5_552:                              # %vector.body9005.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_553:                              # %vector.body9005
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_553
# BB#554:                               # %vector.body9018.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_555:                              # %vector.body9018
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_555
# BB#556:                               # %vector.body9031.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_557:                              # %vector.body9031
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_557
# BB#558:                               # %vector.body9042.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_559:                              # %vector.body9042
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_559
# BB#560:                               # %vector.body9053.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_561:                              # %vector.body9053
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_561
	jmp	.LBB5_853
.LBB5_563:                              # %vector.body8994.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_564:                              # %vector.body8994
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_564
# BB#565:                               # %set1d.exit5641
	movl	$-1082130432, global_data+127996(%rip) # imm = 0xBF800000
	jmp	.LBB5_853
.LBB5_567:                              # %vector.body8983.preheader
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_568:                              # %vector.body8983
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+128000(%rax)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_568
# BB#569:                               # %set1d.exit5636
	movl	$1073741824, global_data+127996(%rip) # imm = 0x40000000
	jmp	.LBB5_853
.LBB5_571:                              # %.preheader.i5630.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_572:                              # %vector.body8972
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+256016(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_572
	jmp	.LBB5_853
.LBB5_574:                              # %vector.body8950.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_575:                              # %vector.body8950
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm0, %xmm1
	divps	%xmm7, %xmm1
	movaps	%xmm1, global_data+128000(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_575
# BB#576:                               # %vector.body8961.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_577:                              # %vector.body8961
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+256016(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_577
	jmp	.LBB5_853
.LBB5_579:                              # %.preheader44.i5608.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_580:                              # %vector.body8922
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_580
# BB#581:                               # %.preheader.i5598.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_582:                              # %vector.body8937
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1164560(%rax)
	movaps	%xmm0, global_data+1164576(%rax)
	movaps	%xmm0, global_data+1164592(%rax)
	movaps	%xmm0, global_data+1164608(%rax)
	movaps	%xmm0, global_data+1164624(%rax)
	movaps	%xmm0, global_data+1164640(%rax)
	movaps	%xmm0, global_data+1164656(%rax)
	movaps	%xmm0, global_data+1164672(%rax)
	movaps	%xmm0, global_data+1164688(%rax)
	movaps	%xmm0, global_data+1164704(%rax)
	movaps	%xmm0, global_data+1164720(%rax)
	movaps	%xmm0, global_data+1164736(%rax)
	movaps	%xmm0, global_data+1164752(%rax)
	movaps	%xmm0, global_data+1164768(%rax)
	movaps	%xmm0, global_data+1164784(%rax)
	movaps	%xmm0, global_data+1164800(%rax)
	movaps	%xmm0, global_data+1164816(%rax)
	movaps	%xmm0, global_data+1164832(%rax)
	movaps	%xmm0, global_data+1164848(%rax)
	movaps	%xmm0, global_data+1164864(%rax)
	movaps	%xmm0, global_data+1164880(%rax)
	movaps	%xmm0, global_data+1164896(%rax)
	movaps	%xmm0, global_data+1164912(%rax)
	movaps	%xmm0, global_data+1164928(%rax)
	movaps	%xmm0, global_data+1164944(%rax)
	movaps	%xmm0, global_data+1164960(%rax)
	movaps	%xmm0, global_data+1164976(%rax)
	movaps	%xmm0, global_data+1164992(%rax)
	movaps	%xmm0, global_data+1165008(%rax)
	movaps	%xmm0, global_data+1165024(%rax)
	movaps	%xmm0, global_data+1165040(%rax)
	movaps	%xmm0, global_data+1165056(%rax)
	movaps	%xmm0, global_data+1165072(%rax)
	movaps	%xmm0, global_data+1165088(%rax)
	movaps	%xmm0, global_data+1165104(%rax)
	movaps	%xmm0, global_data+1165120(%rax)
	movaps	%xmm0, global_data+1165136(%rax)
	movaps	%xmm0, global_data+1165152(%rax)
	movaps	%xmm0, global_data+1165168(%rax)
	movaps	%xmm0, global_data+1165184(%rax)
	movaps	%xmm0, global_data+1165200(%rax)
	movaps	%xmm0, global_data+1165216(%rax)
	movaps	%xmm0, global_data+1165232(%rax)
	movaps	%xmm0, global_data+1165248(%rax)
	movaps	%xmm0, global_data+1165264(%rax)
	movaps	%xmm0, global_data+1165280(%rax)
	movaps	%xmm0, global_data+1165296(%rax)
	movaps	%xmm0, global_data+1165312(%rax)
	movaps	%xmm0, global_data+1165328(%rax)
	movaps	%xmm0, global_data+1165344(%rax)
	movaps	%xmm0, global_data+1165360(%rax)
	movaps	%xmm0, global_data+1165376(%rax)
	movaps	%xmm0, global_data+1165392(%rax)
	movaps	%xmm0, global_data+1165408(%rax)
	movaps	%xmm0, global_data+1165424(%rax)
	movaps	%xmm0, global_data+1165440(%rax)
	movaps	%xmm0, global_data+1165456(%rax)
	movaps	%xmm0, global_data+1165472(%rax)
	movaps	%xmm0, global_data+1165488(%rax)
	movaps	%xmm0, global_data+1165504(%rax)
	movaps	%xmm0, global_data+1165520(%rax)
	movaps	%xmm0, global_data+1165536(%rax)
	movaps	%xmm0, global_data+1165552(%rax)
	movaps	%xmm0, global_data+1165568(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_582
	jmp	.LBB5_853
.LBB5_584:                              # %vector.body8896.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_585:                              # %vector.body8896
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_585
# BB#586:                               # %vector.body8909.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_587:                              # %vector.body8909
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_587
# BB#588:                               # %set1d.exit5592
	movl	$1065353216, global_data+256048(%rip) # imm = 0x3F800000
	jmp	.LBB5_853
.LBB5_590:                              # %vector.body8874.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_591:                              # %vector.body8874
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm0, %xmm1
	divps	%xmm7, %xmm1
	movaps	%xmm1, global_data+128000(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_591
# BB#592:                               # %vector.body8885.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_593:                              # %vector.body8885
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+256016(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_593
	jmp	.LBB5_853
.LBB5_595:                              # %vector.body8848.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_596:                              # %vector.body8848
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_596
# BB#597:                               # %vector.body8861.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_598:                              # %vector.body8861
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_598
# BB#599:                               # %set1d.exit5574
	movl	$1065353216, global_data+256048(%rip) # imm = 0x3F800000
	jmp	.LBB5_853
.LBB5_601:                              # %vector.body8813.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_602:                              # %vector.body8813
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_602
# BB#603:                               # %vector.body8826.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_604:                              # %vector.body8826
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_604
# BB#605:                               # %vector.body8837.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_606:                              # %vector.body8837
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_606
	jmp	.LBB5_853
.LBB5_608:                              # %vector.body8778.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_609:                              # %vector.body8778
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_609
# BB#610:                               # %vector.body8791.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_611:                              # %vector.body8791
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_611
# BB#612:                               # %vector.body8802.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_613:                              # %vector.body8802
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_613
	jmp	.LBB5_853
.LBB5_615:                              # %.preheader.i5541.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_616:                              # %vector.body8730
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_616
# BB#617:                               # %vector.body8743.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_618:                              # %vector.body8743
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+384048(%rax)
	movaps	%xmm1, global_data+384064(%rax)
	movaps	%xmm1, global_data+384080(%rax)
	movaps	%xmm1, global_data+384096(%rax)
	movaps	%xmm1, global_data+384112(%rax)
	movaps	%xmm1, global_data+384128(%rax)
	movaps	%xmm1, global_data+384144(%rax)
	movaps	%xmm1, global_data+384160(%rax)
	movaps	%xmm1, global_data+384176(%rax)
	movaps	%xmm1, global_data+384192(%rax)
	movaps	%xmm1, global_data+384208(%rax)
	movaps	%xmm1, global_data+384224(%rax)
	movaps	%xmm1, global_data+384240(%rax)
	movaps	%xmm1, global_data+384256(%rax)
	movaps	%xmm1, global_data+384272(%rax)
	movaps	%xmm1, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_618
# BB#619:                               # %vector.body8756.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_620:                              # %vector.body8756
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+512080(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_620
# BB#621:                               # %vector.body8767.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_622:                              # %vector.body8767
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_622
	jmp	.LBB5_853
.LBB5_624:                              # %.preheader.i5516.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_625:                              # %vector.body8687
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+902336(%rax)
	movaps	%xmm0, global_data+902352(%rax)
	movaps	%xmm0, global_data+902368(%rax)
	movaps	%xmm0, global_data+902384(%rax)
	movaps	%xmm0, global_data+902400(%rax)
	movaps	%xmm0, global_data+902416(%rax)
	movaps	%xmm0, global_data+902432(%rax)
	movaps	%xmm0, global_data+902448(%rax)
	movaps	%xmm0, global_data+902464(%rax)
	movaps	%xmm0, global_data+902480(%rax)
	movaps	%xmm0, global_data+902496(%rax)
	movaps	%xmm0, global_data+902512(%rax)
	movaps	%xmm0, global_data+902528(%rax)
	movaps	%xmm0, global_data+902544(%rax)
	movaps	%xmm0, global_data+902560(%rax)
	movaps	%xmm0, global_data+902576(%rax)
	movaps	%xmm0, global_data+902592(%rax)
	movaps	%xmm0, global_data+902608(%rax)
	movaps	%xmm0, global_data+902624(%rax)
	movaps	%xmm0, global_data+902640(%rax)
	movaps	%xmm0, global_data+902656(%rax)
	movaps	%xmm0, global_data+902672(%rax)
	movaps	%xmm0, global_data+902688(%rax)
	movaps	%xmm0, global_data+902704(%rax)
	movaps	%xmm0, global_data+902720(%rax)
	movaps	%xmm0, global_data+902736(%rax)
	movaps	%xmm0, global_data+902752(%rax)
	movaps	%xmm0, global_data+902768(%rax)
	movaps	%xmm0, global_data+902784(%rax)
	movaps	%xmm0, global_data+902800(%rax)
	movaps	%xmm0, global_data+902816(%rax)
	movaps	%xmm0, global_data+902832(%rax)
	movaps	%xmm0, global_data+902848(%rax)
	movaps	%xmm0, global_data+902864(%rax)
	movaps	%xmm0, global_data+902880(%rax)
	movaps	%xmm0, global_data+902896(%rax)
	movaps	%xmm0, global_data+902912(%rax)
	movaps	%xmm0, global_data+902928(%rax)
	movaps	%xmm0, global_data+902944(%rax)
	movaps	%xmm0, global_data+902960(%rax)
	movaps	%xmm0, global_data+902976(%rax)
	movaps	%xmm0, global_data+902992(%rax)
	movaps	%xmm0, global_data+903008(%rax)
	movaps	%xmm0, global_data+903024(%rax)
	movaps	%xmm0, global_data+903040(%rax)
	movaps	%xmm0, global_data+903056(%rax)
	movaps	%xmm0, global_data+903072(%rax)
	movaps	%xmm0, global_data+903088(%rax)
	movaps	%xmm0, global_data+903104(%rax)
	movaps	%xmm0, global_data+903120(%rax)
	movaps	%xmm0, global_data+903136(%rax)
	movaps	%xmm0, global_data+903152(%rax)
	movaps	%xmm0, global_data+903168(%rax)
	movaps	%xmm0, global_data+903184(%rax)
	movaps	%xmm0, global_data+903200(%rax)
	movaps	%xmm0, global_data+903216(%rax)
	movaps	%xmm0, global_data+903232(%rax)
	movaps	%xmm0, global_data+903248(%rax)
	movaps	%xmm0, global_data+903264(%rax)
	movaps	%xmm0, global_data+903280(%rax)
	movaps	%xmm0, global_data+903296(%rax)
	movaps	%xmm0, global_data+903312(%rax)
	movaps	%xmm0, global_data+903328(%rax)
	movaps	%xmm0, global_data+903344(%rax)
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_625
# BB#626:                               # %.preheader44.i5511.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_627:                              # %vector.body8700
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1164560(%rax)
	movaps	%xmm1, global_data+1164576(%rax)
	movaps	%xmm1, global_data+1164592(%rax)
	movaps	%xmm1, global_data+1164608(%rax)
	movaps	%xmm1, global_data+1164624(%rax)
	movaps	%xmm1, global_data+1164640(%rax)
	movaps	%xmm1, global_data+1164656(%rax)
	movaps	%xmm1, global_data+1164672(%rax)
	movaps	%xmm1, global_data+1164688(%rax)
	movaps	%xmm1, global_data+1164704(%rax)
	movaps	%xmm1, global_data+1164720(%rax)
	movaps	%xmm1, global_data+1164736(%rax)
	movaps	%xmm1, global_data+1164752(%rax)
	movaps	%xmm1, global_data+1164768(%rax)
	movaps	%xmm1, global_data+1164784(%rax)
	movaps	%xmm1, global_data+1164800(%rax)
	movaps	%xmm1, global_data+1164816(%rax)
	movaps	%xmm1, global_data+1164832(%rax)
	movaps	%xmm1, global_data+1164848(%rax)
	movaps	%xmm1, global_data+1164864(%rax)
	movaps	%xmm1, global_data+1164880(%rax)
	movaps	%xmm1, global_data+1164896(%rax)
	movaps	%xmm1, global_data+1164912(%rax)
	movaps	%xmm1, global_data+1164928(%rax)
	movaps	%xmm1, global_data+1164944(%rax)
	movaps	%xmm1, global_data+1164960(%rax)
	movaps	%xmm1, global_data+1164976(%rax)
	movaps	%xmm1, global_data+1164992(%rax)
	movaps	%xmm1, global_data+1165008(%rax)
	movaps	%xmm1, global_data+1165024(%rax)
	movaps	%xmm1, global_data+1165040(%rax)
	movaps	%xmm1, global_data+1165056(%rax)
	movaps	%xmm1, global_data+1165072(%rax)
	movaps	%xmm1, global_data+1165088(%rax)
	movaps	%xmm1, global_data+1165104(%rax)
	movaps	%xmm1, global_data+1165120(%rax)
	movaps	%xmm1, global_data+1165136(%rax)
	movaps	%xmm1, global_data+1165152(%rax)
	movaps	%xmm1, global_data+1165168(%rax)
	movaps	%xmm1, global_data+1165184(%rax)
	movaps	%xmm1, global_data+1165200(%rax)
	movaps	%xmm1, global_data+1165216(%rax)
	movaps	%xmm1, global_data+1165232(%rax)
	movaps	%xmm1, global_data+1165248(%rax)
	movaps	%xmm1, global_data+1165264(%rax)
	movaps	%xmm1, global_data+1165280(%rax)
	movaps	%xmm1, global_data+1165296(%rax)
	movaps	%xmm1, global_data+1165312(%rax)
	movaps	%xmm1, global_data+1165328(%rax)
	movaps	%xmm1, global_data+1165344(%rax)
	movaps	%xmm1, global_data+1165360(%rax)
	movaps	%xmm1, global_data+1165376(%rax)
	movaps	%xmm1, global_data+1165392(%rax)
	movaps	%xmm1, global_data+1165408(%rax)
	movaps	%xmm1, global_data+1165424(%rax)
	movaps	%xmm1, global_data+1165440(%rax)
	movaps	%xmm1, global_data+1165456(%rax)
	movaps	%xmm1, global_data+1165472(%rax)
	movaps	%xmm1, global_data+1165488(%rax)
	movaps	%xmm1, global_data+1165504(%rax)
	movaps	%xmm1, global_data+1165520(%rax)
	movaps	%xmm1, global_data+1165536(%rax)
	movaps	%xmm1, global_data+1165552(%rax)
	movaps	%xmm1, global_data+1165568(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_627
# BB#628:                               # %.preheader44.i5502.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
.LBB5_629:                              # %vector.body8715
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1426784(%rax)
	movaps	%xmm1, global_data+1426800(%rax)
	movaps	%xmm1, global_data+1426816(%rax)
	movaps	%xmm1, global_data+1426832(%rax)
	movaps	%xmm1, global_data+1426848(%rax)
	movaps	%xmm1, global_data+1426864(%rax)
	movaps	%xmm1, global_data+1426880(%rax)
	movaps	%xmm1, global_data+1426896(%rax)
	movaps	%xmm1, global_data+1426912(%rax)
	movaps	%xmm1, global_data+1426928(%rax)
	movaps	%xmm1, global_data+1426944(%rax)
	movaps	%xmm1, global_data+1426960(%rax)
	movaps	%xmm1, global_data+1426976(%rax)
	movaps	%xmm1, global_data+1426992(%rax)
	movaps	%xmm1, global_data+1427008(%rax)
	movaps	%xmm1, global_data+1427024(%rax)
	movaps	%xmm1, global_data+1427040(%rax)
	movaps	%xmm1, global_data+1427056(%rax)
	movaps	%xmm1, global_data+1427072(%rax)
	movaps	%xmm1, global_data+1427088(%rax)
	movaps	%xmm1, global_data+1427104(%rax)
	movaps	%xmm1, global_data+1427120(%rax)
	movaps	%xmm1, global_data+1427136(%rax)
	movaps	%xmm1, global_data+1427152(%rax)
	movaps	%xmm1, global_data+1427168(%rax)
	movaps	%xmm1, global_data+1427184(%rax)
	movaps	%xmm1, global_data+1427200(%rax)
	movaps	%xmm1, global_data+1427216(%rax)
	movaps	%xmm1, global_data+1427232(%rax)
	movaps	%xmm1, global_data+1427248(%rax)
	movaps	%xmm1, global_data+1427264(%rax)
	movaps	%xmm1, global_data+1427280(%rax)
	movaps	%xmm1, global_data+1427296(%rax)
	movaps	%xmm1, global_data+1427312(%rax)
	movaps	%xmm1, global_data+1427328(%rax)
	movaps	%xmm1, global_data+1427344(%rax)
	movaps	%xmm1, global_data+1427360(%rax)
	movaps	%xmm1, global_data+1427376(%rax)
	movaps	%xmm1, global_data+1427392(%rax)
	movaps	%xmm1, global_data+1427408(%rax)
	movaps	%xmm1, global_data+1427424(%rax)
	movaps	%xmm1, global_data+1427440(%rax)
	movaps	%xmm1, global_data+1427456(%rax)
	movaps	%xmm1, global_data+1427472(%rax)
	movaps	%xmm1, global_data+1427488(%rax)
	movaps	%xmm1, global_data+1427504(%rax)
	movaps	%xmm1, global_data+1427520(%rax)
	movaps	%xmm1, global_data+1427536(%rax)
	movaps	%xmm1, global_data+1427552(%rax)
	movaps	%xmm1, global_data+1427568(%rax)
	movaps	%xmm1, global_data+1427584(%rax)
	movaps	%xmm1, global_data+1427600(%rax)
	movaps	%xmm1, global_data+1427616(%rax)
	movaps	%xmm1, global_data+1427632(%rax)
	movaps	%xmm1, global_data+1427648(%rax)
	movaps	%xmm1, global_data+1427664(%rax)
	movaps	%xmm1, global_data+1427680(%rax)
	movaps	%xmm1, global_data+1427696(%rax)
	movaps	%xmm1, global_data+1427712(%rax)
	movaps	%xmm1, global_data+1427728(%rax)
	movaps	%xmm1, global_data+1427744(%rax)
	movaps	%xmm1, global_data+1427760(%rax)
	movaps	%xmm1, global_data+1427776(%rax)
	movaps	%xmm1, global_data+1427792(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_629
	jmp	.LBB5_853
.LBB5_631:                              # %vector.body8652.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_632:                              # %vector.body8652
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_632
# BB#633:                               # %vector.body8665.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_634:                              # %vector.body8665
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_634
# BB#635:                               # %vector.body8676.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_636:                              # %vector.body8676
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_636
# BB#637:                               # %set1d.exit5488
	movl	$-1082130432, global_data+127996(%rip) # imm = 0xBF800000
	jmp	.LBB5_853
.LBB5_639:                              # %vector.body8628.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_640:                              # %vector.body8628
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128000(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_640
# BB#641:                               # %vector.body8639.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_642:                              # %vector.body8639
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_642
	jmp	.LBB5_853
.LBB5_644:                              # %vector.body8604.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_645:                              # %vector.body8604
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, array+128000(%rax)
	movaps	%xmm0, array+128016(%rax)
	movaps	%xmm0, array+128032(%rax)
	movaps	%xmm0, array+128048(%rax)
	movaps	%xmm0, array+128064(%rax)
	movaps	%xmm0, array+128080(%rax)
	movaps	%xmm0, array+128096(%rax)
	movaps	%xmm0, array+128112(%rax)
	movaps	%xmm0, array+128128(%rax)
	movaps	%xmm0, array+128144(%rax)
	movaps	%xmm0, array+128160(%rax)
	movaps	%xmm0, array+128176(%rax)
	movaps	%xmm0, array+128192(%rax)
	movaps	%xmm0, array+128208(%rax)
	movaps	%xmm0, array+128224(%rax)
	movaps	%xmm0, array+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_645
# BB#646:                               # %vector.body8617.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_647:                              # %vector.body8617
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128000(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_647
	jmp	.LBB5_853
.LBB5_649:                              # %.preheader.i5464.preheader
	movl	$array, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_650:                              # %vector.body8593
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+128000(%rax)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_650
	jmp	.LBB5_853
.LBB5_652:                              # %vector.body8569.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_653:                              # %vector.body8569
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, array+128000(%rax)
	movaps	%xmm0, array+128016(%rax)
	movaps	%xmm0, array+128032(%rax)
	movaps	%xmm0, array+128048(%rax)
	movaps	%xmm0, array+128064(%rax)
	movaps	%xmm0, array+128080(%rax)
	movaps	%xmm0, array+128096(%rax)
	movaps	%xmm0, array+128112(%rax)
	movaps	%xmm0, array+128128(%rax)
	movaps	%xmm0, array+128144(%rax)
	movaps	%xmm0, array+128160(%rax)
	movaps	%xmm0, array+128176(%rax)
	movaps	%xmm0, array+128192(%rax)
	movaps	%xmm0, array+128208(%rax)
	movaps	%xmm0, array+128224(%rax)
	movaps	%xmm0, array+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_653
# BB#654:                               # %vector.body8582.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_655:                              # %vector.body8582
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128000(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_655
	jmp	.LBB5_853
.LBB5_657:                              # %vector.body8545.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_658:                              # %vector.body8545
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_658
# BB#659:                               # %vector.body8558.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_660:                              # %vector.body8558
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_660
	jmp	.LBB5_853
.LBB5_662:                              # %vector.body8521.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_663:                              # %vector.body8521
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_663
# BB#664:                               # %vector.body8534.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_665:                              # %vector.body8534
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_665
	jmp	.LBB5_853
.LBB5_667:                              # %vector.body8460.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_668:                              # %vector.body8460
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_668
# BB#669:                               # %vector.body8473.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm4   # xmm4 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm1   # xmm1 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm2   # xmm2 = [8,8]
.LBB5_670:                              # %vector.body8473
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm3, %xmm5
	shufps	$136, %xmm4, %xmm5      # xmm5 = xmm5[0,2],xmm4[0,2]
	movaps	%xmm5, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	.LCPI5_2(%rip), %xmm7   # xmm7 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movaps	%xmm7, %xmm0
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+256016(%rax)
	paddd	%xmm1, %xmm5
	cvtdq2ps	%xmm5, %xmm5
	movaps	%xmm0, %xmm6
	divps	%xmm5, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm2, %xmm3
	paddq	%xmm2, %xmm4
	addq	$32, %rax
	jne	.LBB5_670
# BB#671:                               # %vector.body8484.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm4   # xmm4 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_672:                              # %vector.body8484
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm3, %xmm5
	shufps	$136, %xmm4, %xmm5      # xmm5 = xmm5[0,2],xmm4[0,2]
	movaps	%xmm5, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	.LCPI5_2(%rip), %xmm7   # xmm7 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movaps	%xmm7, %xmm0
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm1, %xmm5
	cvtdq2ps	%xmm5, %xmm5
	movaps	%xmm0, %xmm6
	divps	%xmm5, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm2, %xmm3
	paddq	%xmm2, %xmm4
	addq	$32, %rax
	jne	.LBB5_672
# BB#673:                               # %vector.body8495.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_10(%rip), %xmm0  # xmm0 = [-1.000000e+00,-1.000000e+00,-1.000000e+00,-1.000000e+00]
.LBB5_674:                              # %vector.body8495
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	movaps	%xmm0, global_data+512288(%rax)
	movaps	%xmm0, global_data+512304(%rax)
	movaps	%xmm0, global_data+512320(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_674
# BB#675:                               # %.preheader.i5410.preheader
	movl	$global_data+426744, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_676:                              # %vector.body8508
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, global_data+597412(%rax)
	movups	%xmm0, global_data+597428(%rax)
	movups	%xmm0, global_data+597444(%rax)
	movups	%xmm0, global_data+597460(%rax)
	movups	%xmm0, global_data+597476(%rax)
	movups	%xmm0, global_data+597492(%rax)
	movups	%xmm0, global_data+597508(%rax)
	movups	%xmm0, global_data+597524(%rax)
	movups	%xmm0, global_data+597540(%rax)
	movups	%xmm0, global_data+597556(%rax)
	movups	%xmm0, global_data+597572(%rax)
	movups	%xmm0, global_data+597588(%rax)
	movups	%xmm0, global_data+597604(%rax)
	movups	%xmm0, global_data+597620(%rax)
	movups	%xmm0, global_data+597636(%rax)
	movups	%xmm0, global_data+597652(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_676
	jmp	.LBB5_853
.LBB5_678:                              # %vector.body8403.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_679:                              # %vector.body8403
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_679
# BB#680:                               # %vector.body8416.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_681:                              # %vector.body8416
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_681
# BB#682:                               # %vector.body8427.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_683:                              # %vector.body8427
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_683
# BB#684:                               # %vector.body8438.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_685:                              # %vector.body8438
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_685
# BB#686:                               # %vector.body8449.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_687:                              # %vector.body8449
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_687
	jmp	.LBB5_853
.LBB5_689:                              # %vector.body8368.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_690:                              # %vector.body8368
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_690
# BB#691:                               # %vector.body8381.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_692:                              # %vector.body8381
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_692
# BB#693:                               # %vector.body8392.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_694:                              # %vector.body8392
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_694
	jmp	.LBB5_853
.LBB5_696:                              # %vector.body8346.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_697:                              # %vector.body8346
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm0, %xmm1
	divps	%xmm7, %xmm1
	movaps	%xmm1, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_697
# BB#698:                               # %vector.body8357.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_699:                              # %vector.body8357
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_699
	jmp	.LBB5_853
.LBB5_701:                              # %.preheader.i5355.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_702:                              # %vector.body8320
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_702
# BB#703:                               # %vector.body8333.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_11(%rip), %xmm0  # xmm0 = [1.000000e-06,1.000000e-06,1.000000e-06,1.000000e-06]
.LBB5_704:                              # %vector.body8333
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_704
	jmp	.LBB5_853
.LBB5_706:                              # %.preheader.i5343.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_707:                              # %vector.body8309
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_707
	jmp	.LBB5_853
.LBB5_709:                              # %vector.body8248.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_710:                              # %vector.body8248
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_710
# BB#711:                               # %vector.body8261.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_712:                              # %vector.body8261
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_712
# BB#713:                               # %vector.body8274.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_714:                              # %vector.body8274
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+384048(%rax)
	movaps	%xmm1, global_data+384064(%rax)
	movaps	%xmm1, global_data+384080(%rax)
	movaps	%xmm1, global_data+384096(%rax)
	movaps	%xmm1, global_data+384112(%rax)
	movaps	%xmm1, global_data+384128(%rax)
	movaps	%xmm1, global_data+384144(%rax)
	movaps	%xmm1, global_data+384160(%rax)
	movaps	%xmm1, global_data+384176(%rax)
	movaps	%xmm1, global_data+384192(%rax)
	movaps	%xmm1, global_data+384208(%rax)
	movaps	%xmm1, global_data+384224(%rax)
	movaps	%xmm1, global_data+384240(%rax)
	movaps	%xmm1, global_data+384256(%rax)
	movaps	%xmm1, global_data+384272(%rax)
	movaps	%xmm1, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_714
# BB#715:                               # %vector.body8287.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_716:                              # %vector.body8287
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+512080(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_716
# BB#717:                               # %vector.body8298.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_718:                              # %vector.body8298
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+640128(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+640144(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_718
	jmp	.LBB5_853
.LBB5_720:                              # %vector.body8202.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_721:                              # %vector.body8202
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_721
# BB#722:                               # %vector.body8215.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_723:                              # %vector.body8215
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_723
# BB#724:                               # %vector.body8226.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_725:                              # %vector.body8226
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_725
# BB#726:                               # %vector.body8237.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_727:                              # %vector.body8237
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_727
	jmp	.LBB5_853
.LBB5_729:                              # %vector.body8167.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_730:                              # %vector.body8167
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_730
# BB#731:                               # %vector.body8180.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_732:                              # %vector.body8180
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_732
# BB#733:                               # %vector.body8191.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_734:                              # %vector.body8191
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_734
	jmp	.LBB5_853
.LBB5_736:                              # %.preheader.i5279.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_737:                              # %vector.body8132
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_737
# BB#738:                               # %vector.body8145.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_739:                              # %vector.body8145
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_739
# BB#740:                               # %vector.body8156.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_741:                              # %vector.body8156
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_741
	jmp	.LBB5_853
.LBB5_743:                              # %vector.body8108.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_744:                              # %vector.body8108
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm3, global_data+128000(%rax)
	movaps	%xmm3, global_data+128016(%rax)
	movaps	%xmm3, global_data+128032(%rax)
	movaps	%xmm3, global_data+128048(%rax)
	movaps	%xmm3, global_data+128064(%rax)
	movaps	%xmm3, global_data+128080(%rax)
	movaps	%xmm3, global_data+128096(%rax)
	movaps	%xmm3, global_data+128112(%rax)
	movaps	%xmm3, global_data+128128(%rax)
	movaps	%xmm3, global_data+128144(%rax)
	movaps	%xmm3, global_data+128160(%rax)
	movaps	%xmm3, global_data+128176(%rax)
	movaps	%xmm3, global_data+128192(%rax)
	movaps	%xmm3, global_data+128208(%rax)
	movaps	%xmm3, global_data+128224(%rax)
	movaps	%xmm3, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_744
# BB#745:                               # %vector.body8121.preheader
	movl	$1, %eax
	movd	%rax, %xmm1
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_746:                              # %vector.body8121
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	shufps	$136, %xmm2, %xmm6      # xmm6 = xmm6[0,2],xmm2[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm3, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm1
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_746
	jmp	.LBB5_853
.LBB5_748:                              # %.preheader.i5252.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_749:                              # %vector.body8084
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_749
# BB#750:                               # %vector.body8097.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_751:                              # %vector.body8097
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+384048(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_751
	jmp	.LBB5_853
.LBB5_753:                              # %.preheader.i5239.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_754:                              # %vector.body8049
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_754
# BB#755:                               # %vector.body8062.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_756:                              # %vector.body8062
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_756
# BB#757:                               # %vector.body8073.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_758:                              # %vector.body8073
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_758
	jmp	.LBB5_853
.LBB5_760:                              # %vector.body8027.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_761:                              # %vector.body8027
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm0, %xmm1
	divps	%xmm7, %xmm1
	movaps	%xmm1, global_data+128000(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_761
# BB#762:                               # %vector.body8038.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_763:                              # %vector.body8038
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+256016(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_763
	jmp	.LBB5_853
.LBB5_765:                              # %vector.body8001.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_766:                              # %vector.body8001
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_766
# BB#767:                               # %.preheader44.i5203.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_768:                              # %vector.body8012
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB5_768
	jmp	.LBB5_853
.LBB5_770:                              # %.preheader.i5197.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_771:                              # %vector.body7966
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	movaps	%xmm1, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_771
# BB#772:                               # %vector.body7979.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_773:                              # %vector.body7979
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+384048(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_773
# BB#774:                               # %vector.body7990.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_775:                              # %vector.body7990
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+512080(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+512096(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_775
	jmp	.LBB5_853
.LBB5_777:                              # %vector.body7931.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_778:                              # %vector.body7931
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_778
# BB#779:                               # %vector.body7944.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_780:                              # %vector.body7944
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_780
# BB#781:                               # %vector.body7955.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_782:                              # %vector.body7955
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_782
	jmp	.LBB5_853
.LBB5_787:                              # %.preheader.i5165.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_788:                              # %vector.body7920
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_788
	jmp	.LBB5_853
.LBB5_790:                              # %.preheader.i5156.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_791:                              # %vector.body7909
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_791
	jmp	.LBB5_853
.LBB5_793:                              # %.preheader.i5147.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_794:                              # %vector.body7898
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_794
	jmp	.LBB5_853
.LBB5_796:                              # %.preheader.i5138.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_797:                              # %vector.body7887
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_797
	jmp	.LBB5_853
.LBB5_799:                              # %.preheader.i5129.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	memset
	movl	$1, %eax
	movd	%rax, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_800:                              # %vector.body7876
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm1, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm2, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm0      # xmm0 = xmm0[0,2],xmm6[0,2]
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm1, %xmm0
	paddq	%xmm4, %xmm0
	movdqa	%xmm2, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm0, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm0, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm0, %xmm0
	paddq	%xmm7, %xmm0
	shufps	$136, %xmm0, %xmm6      # xmm6 = xmm6[0,2],xmm0[0,2]
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm3, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm2
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_800
	jmp	.LBB5_853
.LBB5_802:                              # %vector.body7850.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_803:                              # %vector.body7850
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_803
# BB#804:                               # %vector.body7863.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_805:                              # %vector.body7863
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_805
	jmp	.LBB5_853
.LBB5_807:                              # %vector.body7815.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_808:                              # %vector.body7815
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+128000(%rax)
	movaps	%xmm1, global_data+128016(%rax)
	movaps	%xmm1, global_data+128032(%rax)
	movaps	%xmm1, global_data+128048(%rax)
	movaps	%xmm1, global_data+128064(%rax)
	movaps	%xmm1, global_data+128080(%rax)
	movaps	%xmm1, global_data+128096(%rax)
	movaps	%xmm1, global_data+128112(%rax)
	movaps	%xmm1, global_data+128128(%rax)
	movaps	%xmm1, global_data+128144(%rax)
	movaps	%xmm1, global_data+128160(%rax)
	movaps	%xmm1, global_data+128176(%rax)
	movaps	%xmm1, global_data+128192(%rax)
	movaps	%xmm1, global_data+128208(%rax)
	movaps	%xmm1, global_data+128224(%rax)
	movaps	%xmm1, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_808
# BB#809:                               # %vector.body7828.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_810:                              # %vector.body7828
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm1, %xmm0
	divps	%xmm7, %xmm0
	movaps	%xmm0, global_data+256016(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_810
# BB#811:                               # %vector.body7839.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_812:                              # %vector.body7839
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm0
	shufps	$136, %xmm5, %xmm0      # xmm0 = xmm0[0,2],xmm5[0,2]
	movaps	%xmm0, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm1, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+384048(%rax)
	paddd	%xmm2, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	movaps	%xmm1, %xmm6
	divps	%xmm0, %xmm6
	movaps	%xmm6, global_data+384064(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_812
	jmp	.LBB5_853
.LBB5_814:                              # %vector.body7791.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_815:                              # %vector.body7791
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_815
# BB#816:                               # %vector.body7804.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_817:                              # %vector.body7804
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256016(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_817
	jmp	.LBB5_853
.LBB5_819:                              # %vector.body7754.preheader
	movl	$1, %eax
	movd	%rax, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	movdqa	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_8(%rip), %xmm8   # xmm8 = [1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_9(%rip), %xmm4   # xmm4 = [5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_820:                              # %vector.body7754
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm2, %xmm6
	paddq	%xmm8, %xmm6
	movdqa	%xmm3, %xmm1
	paddq	%xmm8, %xmm1
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	shufps	$136, %xmm6, %xmm1      # xmm1 = xmm1[0,2],xmm6[0,2]
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128000(%rax)
	movdqa	%xmm2, %xmm1
	paddq	%xmm4, %xmm1
	movdqa	%xmm3, %xmm6
	paddq	%xmm4, %xmm6
	movdqa	%xmm6, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm6, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm6, %xmm6
	paddq	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	psrlq	$32, %xmm7
	pmuludq	%xmm1, %xmm7
	paddq	%xmm7, %xmm7
	psllq	$32, %xmm7
	pmuludq	%xmm1, %xmm1
	paddq	%xmm7, %xmm1
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm3
	paddq	%xmm5, %xmm2
	addq	$32, %rax
	jne	.LBB5_820
# BB#821:                               # %vector.body7765.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_822:                              # %vector.body7765
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_822
# BB#823:                               # %vector.body7778.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_10(%rip), %xmm0  # xmm0 = [-1.000000e+00,-1.000000e+00,-1.000000e+00,-1.000000e+00]
.LBB5_824:                              # %vector.body7778
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_824
	jmp	.LBB5_853
.LBB5_826:                              # %vector.body7715.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
.LBB5_827:                              # %vector.body7715
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_827
# BB#828:                               # %vector.body7728.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_6(%rip), %xmm0   # xmm0 = [2.000000e+00,2.000000e+00,2.000000e+00,2.000000e+00]
.LBB5_829:                              # %vector.body7728
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_829
# BB#830:                               # %vector.body7741.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI5_7(%rip), %xmm0   # xmm0 = [5.000000e-01,5.000000e-01,5.000000e-01,5.000000e-01]
.LBB5_831:                              # %vector.body7741
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_831
	jmp	.LBB5_853
.LBB5_833:                              # %vector.body7704.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm4   # xmm4 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm5   # xmm5 = [8,8]
.LBB5_834:                              # %vector.body7704
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm2, %xmm3
	divps	%xmm7, %xmm3
	movaps	%xmm3, global_data+128000(%rax)
	paddd	%xmm4, %xmm6
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm2, %xmm6
	divps	%xmm3, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB5_834
	jmp	.LBB5_853
.LBB5_836:                              # %vector.body7682.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movdqa	.LCPI5_3(%rip), %xmm2   # xmm2 = [5,5,5,5]
	movdqa	.LCPI5_4(%rip), %xmm3   # xmm3 = [8,8]
.LBB5_837:                              # %vector.body7682
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm6
	shufps	$136, %xmm5, %xmm6      # xmm6 = xmm6[0,2],xmm5[0,2]
	movaps	%xmm6, %xmm7
	paddd	%xmm8, %xmm7
	cvtdq2ps	%xmm7, %xmm7
	movaps	%xmm0, %xmm1
	divps	%xmm7, %xmm1
	movaps	%xmm1, global_data+128000(%rax)
	paddd	%xmm2, %xmm6
	cvtdq2ps	%xmm6, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+128016(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_837
# BB#838:                               # %vector.body7693.preheader
	movl	$1, %eax
	movd	%rax, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm5   # xmm5 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
.LBB5_839:                              # %vector.body7693
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm1
	shufps	$136, %xmm5, %xmm1      # xmm1 = xmm1[0,2],xmm5[0,2]
	movaps	%xmm1, %xmm6
	paddd	%xmm8, %xmm6
	cvtdq2ps	%xmm6, %xmm6
	movaps	%xmm0, %xmm7
	divps	%xmm6, %xmm7
	movaps	%xmm7, global_data+256016(%rax)
	paddd	%xmm2, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm0, %xmm6
	divps	%xmm1, %xmm6
	movaps	%xmm6, global_data+256032(%rax)
	paddq	%xmm3, %xmm4
	paddq	%xmm3, %xmm5
	addq	$32, %rax
	jne	.LBB5_839
	jmp	.LBB5_853
.Lfunc_end5:
	.size	init, .Lfunc_end5-init
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.text
	.globl	s000
	.p2align	4, 0x90
	.type	s000,@function
s000:                                   # @s000
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movl	$.L.str.1, %edi
	callq	init
	cmpl	$0, ntimes(%rip)
	jle	.LBB6_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	movaps	.LCPI6_0(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB6_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_3 Depth 2
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB6_3:                                # %vector.body
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	Y+128000(%rax), %xmm0
	addps	%xmm2, %xmm0
	movaps	Y+128016(%rax), %xmm1
	addps	%xmm2, %xmm1
	movaps	%xmm0, X+128000(%rax)
	movaps	%xmm1, X+128016(%rax)
	movaps	Y+128032(%rax), %xmm0
	addps	%xmm2, %xmm0
	movaps	Y+128048(%rax), %xmm1
	addps	%xmm2, %xmm1
	movaps	%xmm0, X+128032(%rax)
	movaps	%xmm1, X+128048(%rax)
	addq	$64, %rax
	jne	.LBB6_3
# BB#4:                                 # %middle.block
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	$X, %edi
	movl	$Y, %esi
	movl	$Z, %edx
	movl	$U, %ecx
	movl	$V, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	movaps	.LCPI6_0(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	addl	%eax, %eax
	cmpl	%eax, %ebx
	jl	.LBB6_2
.LBB6_5:                                # %._crit_edge
	movl	$.L.str.137, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm0
	addss	global_data+128004(%rax), %xmm0
	addss	global_data+128008(%rax), %xmm0
	addss	global_data+128012(%rax), %xmm0
	addss	global_data+128016(%rax), %xmm0
	addss	global_data+128020(%rax), %xmm0
	addss	global_data+128024(%rax), %xmm0
	addss	global_data+128028(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB6_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_8:                                # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB6_9:                                #   Parent Loop BB6_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB6_9
# BB#10:                                #   in Loop: Header=BB6_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB6_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	s000, .Lfunc_end6-s000
	.cfi_endproc

	.globl	s111
	.p2align	4, 0x90
	.type	s111,@function
s111:                                   # @s111
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movl	$.L.str.2, %edi
	callq	init
	cmpl	$0, ntimes(%rip)
	jle	.LBB7_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_3 Depth 2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_3:                                #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	global_data(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+128020(,%rax,4), %xmm0
	movss	%xmm0, global_data+4(,%rax,4)
	movss	global_data+8(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+128028(,%rax,4), %xmm0
	movss	%xmm0, global_data+12(,%rax,4)
	movss	global_data+16(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+128036(,%rax,4), %xmm0
	movss	%xmm0, global_data+20(,%rax,4)
	movss	global_data+24(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+128044(,%rax,4), %xmm0
	movss	%xmm0, global_data+28(,%rax,4)
	leaq	8(%rax), %rcx
	addq	$9, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	movq	%rcx, %rax
	jl	.LBB7_3
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	addl	%eax, %eax
	cmpl	%eax, %ebx
	jl	.LBB7_2
.LBB7_5:                                # %._crit_edge
	movl	$.L.str.138, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB7_6:                                # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm0
	addss	global_data+128004(%rax), %xmm0
	addss	global_data+128008(%rax), %xmm0
	addss	global_data+128012(%rax), %xmm0
	addss	global_data+128016(%rax), %xmm0
	addss	global_data+128020(%rax), %xmm0
	addss	global_data+128024(%rax), %xmm0
	addss	global_data+128028(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB7_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_8:                                # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB7_9:                                #   Parent Loop BB7_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB7_9
# BB#10:                                #   in Loop: Header=BB7_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB7_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	s111, .Lfunc_end7-s111
	.cfi_endproc

	.globl	s1111
	.p2align	4, 0x90
	.type	s1111,@function
s1111:                                  # @s1111
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movl	$.L.str.2, %edi
	callq	init
	cmpl	$0, ntimes(%rip)
	jle	.LBB8_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_3 Depth 2
	movq	$-64000, %rax           # imm = 0xFFFF0600
	.p2align	4, 0x90
.LBB8_3:                                #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	global_data+320048(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	global_data+192016(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	mulss	%xmm1, %xmm2
	movss	global_data+448080(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm2, %xmm4
	addss	%xmm1, %xmm4
	mulss	%xmm3, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, global_data+128000(%rax,%rax)
	addq	$4, %rax
	jne	.LBB8_3
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	addl	%eax, %eax
	cmpl	%eax, %ebx
	jl	.LBB8_2
.LBB8_5:                                # %._crit_edge
	movl	$.L.str.139, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB8_6:                                # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm0
	addss	global_data+128004(%rax), %xmm0
	addss	global_data+128008(%rax), %xmm0
	addss	global_data+128012(%rax), %xmm0
	addss	global_data+128016(%rax), %xmm0
	addss	global_data+128020(%rax), %xmm0
	addss	global_data+128024(%rax), %xmm0
	addss	global_data+128028(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB8_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_8:                                # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB8_9:                                #   Parent Loop BB8_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB8_9
# BB#10:                                #   in Loop: Header=BB8_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB8_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	s1111, .Lfunc_end8-s1111
	.cfi_endproc

	.globl	s112
	.p2align	4, 0x90
	.type	s112,@function
s112:                                   # @s112
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
.Lcfi30:
	.cfi_offset %rbx, -16
	movl	$.L.str.3, %edi
	callq	init
	cmpl	$0, ntimes(%rip)
	jle	.LBB9_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_3 Depth 2
	movl	$64002, %eax            # imm = 0xFA02
	jmp	.LBB9_3
	.p2align	4, 0x90
.LBB9_12:                               #   in Loop: Header=BB9_3 Depth=2
	movss	global_data-128028(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data-12(,%rax,4), %xmm0
	movss	%xmm0, global_data-128024(,%rax,4)
	addq	$-4, %rax
.LBB9_3:                                #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-32004(%rax), %rcx
	movss	global_data-128016(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data(,%rax,4), %xmm0
	movss	%xmm0, global_data-128012(,%rax,4)
	movss	global_data-128020(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data-4(,%rax,4), %xmm0
	movss	%xmm0, global_data-128016(,%rax,4)
	movss	global_data-128024(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data-8(,%rax,4), %xmm0
	movss	%xmm0, global_data-128020(,%rax,4)
	cmpq	$3, %rcx
	jge	.LBB9_12
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	imull	$3, ntimes(%rip), %eax
	cmpl	%eax, %ebx
	jl	.LBB9_2
.LBB9_5:                                # %._crit_edge
	movl	$.L.str.140, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB9_6:                                # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm0
	addss	global_data+128004(%rax), %xmm0
	addss	global_data+128008(%rax), %xmm0
	addss	global_data+128012(%rax), %xmm0
	addss	global_data+128016(%rax), %xmm0
	addss	global_data+128020(%rax), %xmm0
	addss	global_data+128024(%rax), %xmm0
	addss	global_data+128028(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB9_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_8:                                # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB9_9:                                #   Parent Loop BB9_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB9_9
# BB#10:                                #   in Loop: Header=BB9_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB9_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	s112, .Lfunc_end9-s112
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.text
	.globl	s1112
	.p2align	4, 0x90
	.type	s1112,@function
s1112:                                  # @s1112
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
.Lcfi35:
	.cfi_offset %rbx, -16
	movl	$.L.str.3, %edi
	callq	init
	cmpl	$0, ntimes(%rip)
	jle	.LBB10_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	movaps	.LCPI10_0(%rip), %xmm1  # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB10_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_3 Depth 2
	movl	$128000, %eax           # imm = 0x1F400
	.p2align	4, 0x90
.LBB10_3:                               # %vector.body
                                        #   Parent Loop BB10_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	global_data+128000(%rax), %xmm0
	addps	%xmm1, %xmm0
	movups	%xmm0, global_data-16(%rax)
	movups	global_data+127984(%rax), %xmm0
	addps	%xmm1, %xmm0
	movups	%xmm0, global_data-32(%rax)
	movups	global_data+127968(%rax), %xmm0
	addps	%xmm1, %xmm0
	movups	%xmm0, global_data-48(%rax)
	movups	global_data+127952(%rax), %xmm0
	addps	%xmm1, %xmm0
	movups	%xmm0, global_data-64(%rax)
	movups	global_data+127936(%rax), %xmm0
	addps	%xmm1, %xmm0
	movups	%xmm0, global_data-80(%rax)
	addq	$-80, %rax
	jne	.LBB10_3
# BB#4:                                 # %middle.block
                                        #   in Loop: Header=BB10_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	movaps	.LCPI10_0(%rip), %xmm1  # xmm1 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	addq	$16, %rsp
.Lcfi38:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	imull	$3, ntimes(%rip), %eax
	cmpl	%eax, %ebx
	jl	.LBB10_2
.LBB10_5:                               # %._crit_edge
	movl	$.L.str.141, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB10_6:                               # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm0
	addss	global_data+128004(%rax), %xmm0
	addss	global_data+128008(%rax), %xmm0
	addss	global_data+128012(%rax), %xmm0
	addss	global_data+128016(%rax), %xmm0
	addss	global_data+128020(%rax), %xmm0
	addss	global_data+128024(%rax), %xmm0
	addss	global_data+128028(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB10_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB10_8:                               # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB10_9:                               #   Parent Loop BB10_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB10_9
# BB#10:                                #   in Loop: Header=BB10_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB10_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	s1112, .Lfunc_end10-s1112
	.cfi_endproc

	.globl	s113
	.p2align	4, 0x90
	.type	s113,@function
s113:                                   # @s113
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 16
.Lcfi40:
	.cfi_offset %rbx, -16
	movl	$.L.str.4, %edi
	callq	init
	cmpl	$0, ntimes(%rip)
	jle	.LBB11_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_3 Depth 2
	movq	$-127984, %rax          # imm = 0xFFFE0C10
	jmp	.LBB11_3
	.p2align	4, 0x90
.LBB11_12:                              #   in Loop: Header=BB11_3 Depth=2
	movss	global_data(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+256016(%rax), %xmm0
	movss	%xmm0, global_data+128000(%rax)
	addq	$16, %rax
.LBB11_3:                               #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	global_data(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+256004(%rax), %xmm0
	movss	%xmm0, global_data+127988(%rax)
	movss	global_data(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+256008(%rax), %xmm0
	movss	%xmm0, global_data+127992(%rax)
	movss	global_data(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+256012(%rax), %xmm0
	movss	%xmm0, global_data+127996(%rax)
	testq	%rax, %rax
	jne	.LBB11_12
# BB#4:                                 #   in Loop: Header=BB11_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	shll	$2, %eax
	cmpl	%eax, %ebx
	jl	.LBB11_2
.LBB11_5:                               # %._crit_edge
	movl	$.L.str.142, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB11_6:                               # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm0
	addss	global_data+128004(%rax), %xmm0
	addss	global_data+128008(%rax), %xmm0
	addss	global_data+128012(%rax), %xmm0
	addss	global_data+128016(%rax), %xmm0
	addss	global_data+128020(%rax), %xmm0
	addss	global_data+128024(%rax), %xmm0
	addss	global_data+128028(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB11_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_8:                               # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB11_9:                               #   Parent Loop BB11_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB11_9
# BB#10:                                #   in Loop: Header=BB11_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB11_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	s113, .Lfunc_end11-s113
	.cfi_endproc

	.globl	s1113
	.p2align	4, 0x90
	.type	s1113,@function
s1113:                                  # @s1113
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 16
.Lcfi45:
	.cfi_offset %rbx, -16
	movl	$.L.str.4, %edi
	callq	init
	cmpl	$0, ntimes(%rip)
	jle	.LBB12_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_3 Depth 2
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB12_3:                               #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	global_data+64000(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+256016(%rax), %xmm0
	movss	%xmm0, global_data+128000(%rax)
	movss	global_data+64000(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+256020(%rax), %xmm0
	movss	%xmm0, global_data+128004(%rax)
	movss	global_data+64000(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+256024(%rax), %xmm0
	movss	%xmm0, global_data+128008(%rax)
	movss	global_data+64000(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+256028(%rax), %xmm0
	movss	%xmm0, global_data+128012(%rax)
	movss	global_data+64000(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+256032(%rax), %xmm0
	movss	%xmm0, global_data+128016(%rax)
	addq	$20, %rax
	jne	.LBB12_3
# BB#4:                                 #   in Loop: Header=BB12_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi48:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	addl	%eax, %eax
	cmpl	%eax, %ebx
	jl	.LBB12_2
.LBB12_5:                               # %._crit_edge
	movl	$.L.str.143, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB12_6:                               # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm0
	addss	global_data+128004(%rax), %xmm0
	addss	global_data+128008(%rax), %xmm0
	addss	global_data+128012(%rax), %xmm0
	addss	global_data+128016(%rax), %xmm0
	addss	global_data+128020(%rax), %xmm0
	addss	global_data+128024(%rax), %xmm0
	addss	global_data+128028(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB12_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB12_8:                               # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB12_9:                               #   Parent Loop BB12_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB12_9
# BB#10:                                #   in Loop: Header=BB12_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB12_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end12:
	.size	s1113, .Lfunc_end12-s1113
	.cfi_endproc

	.globl	s114
	.p2align	4, 0x90
	.type	s114,@function
s114:                                   # @s114
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movl	$.L.str.5, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB13_6
# BB#1:                                 # %.preheader31.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB13_2:                               # %.preheader31
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_3 Depth 2
                                        #       Child Loop BB13_14 Depth 3
	movl	$global_data+902420, %eax
	movl	$global_data+641216, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_3:                               # %.preheader
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_14 Depth 3
	testq	%rdx, %rdx
	jle	.LBB13_15
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB13_3 Depth=2
	testb	$1, %dl
	jne	.LBB13_11
# BB#5:                                 #   in Loop: Header=BB13_3 Depth=2
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB13_13
	jmp	.LBB13_15
	.p2align	4, 0x90
.LBB13_11:                              # %.lr.ph.prol
                                        #   in Loop: Header=BB13_3 Depth=2
	movss	global_data+640192(,%rdx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %rsi
	shlq	$10, %rsi
	addss	global_data+902416(%rsi), %xmm0
	movss	%xmm0, global_data+640192(%rsi)
	movl	$1, %esi
	cmpq	$1, %rdx
	je	.LBB13_15
.LBB13_13:                              # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB13_3 Depth=2
	movq	%rdx, %rdi
	subq	%rsi, %rdi
	leaq	(%rax,%rsi,4), %rbx
	shlq	$10, %rsi
	addq	%rcx, %rsi
	.p2align	4, 0x90
.LBB13_14:                              # %.lr.ph
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	-1024(%rsi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rbx), %xmm0
	movss	%xmm0, -262228(%rbx)
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rbx), %xmm0
	movss	%xmm0, -262224(%rbx)
	addq	$2048, %rsi             # imm = 0x800
	addq	$8, %rbx
	addq	$-2, %rdi
	jne	.LBB13_14
.LBB13_15:                              # %._crit_edge
                                        #   in Loop: Header=BB13_3 Depth=2
	incq	%rdx
	addq	$4, %rcx
	addq	$1024, %rax             # imm = 0x400
	cmpq	$256, %rdx              # imm = 0x100
	jne	.LBB13_3
# BB#16:                                #   in Loop: Header=BB13_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi56:
	.cfi_adjust_cfa_offset -16
	incl	%r14d
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$200, %ecx, %eax
	cmpl	%eax, %r14d
	jl	.LBB13_2
.LBB13_6:                               # %._crit_edge37
	movl	$.L.str.144, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$global_data+640204, %ecx
	.p2align	4, 0x90
.LBB13_7:                               # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_8 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB13_8:                               #   Parent Loop BB13_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	-12(%rsi), %xmm0
	addss	-8(%rsi), %xmm0
	addss	-4(%rsi), %xmm0
	addss	(%rsi), %xmm0
	addq	$16, %rsi
	addq	$-4, %rdx
	jne	.LBB13_8
# BB#9:                                 #   in Loop: Header=BB13_7 Depth=1
	incq	%rax
	addq	$1024, %rcx             # imm = 0x400
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB13_7
# BB#10:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	s114, .Lfunc_end13-s114
	.cfi_endproc

	.globl	s115
	.p2align	4, 0x90
	.type	s115,@function
s115:                                   # @s115
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 64
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movl	$.L.str.6, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB14_18
# BB#1:                                 # %.preheader.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB14_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_3 Depth 2
                                        #       Child Loop BB14_9 Depth 3
                                        #       Child Loop BB14_15 Depth 3
	xorl	%eax, %eax
	movl	$255, %r11d
	movl	$global_data+640212, %r13d
	movl	$global_data+20, %r14d
	xorl	%r8d, %r8d
	movl	$1, %edi
	.p2align	4, 0x90
.LBB14_3:                               #   Parent Loop BB14_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_9 Depth 3
                                        #       Child Loop BB14_15 Depth 3
	movq	%r8, %r9
	leaq	1(%r9), %r8
	cmpq	$255, %r8
	jg	.LBB14_16
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB14_3 Depth=2
	movl	$255, %r10d
	subq	%r9, %r10
	leaq	global_data(,%r9,4), %rbx
	cmpq	$8, %r10
	movq	%rdi, %rdx
	jb	.LBB14_11
# BB#5:                                 # %min.iters.checked
                                        #   in Loop: Header=BB14_3 Depth=2
	movq	%r10, %r12
	andq	$-8, %r12
	movq	%rdi, %rdx
	je	.LBB14_11
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB14_3 Depth=2
	leaq	global_data+4(,%r9,4), %rcx
	imulq	$1028, %r9, %rdx        # imm = 0x404
	leaq	global_data+640196(%rdx), %rbp
	movq	%r8, %rsi
	shlq	$10, %rsi
	leaq	global_data+640192(%rsi), %rsi
	cmpq	%rsi, %rcx
	sbbb	%sil, %sil
	movl	$global_data+1024, %edx
	cmpq	%rdx, %rbp
	sbbb	%dl, %dl
	andb	%sil, %dl
	cmpq	%rbx, %rcx
	sbbb	%cl, %cl
	cmpq	$256, %r9               # imm = 0x100
	setl	%sil
	testb	$1, %dl
	movq	%rdi, %rdx
	jne	.LBB14_11
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB14_3 Depth=2
	andb	%sil, %cl
	movq	%rdi, %rdx
	jne	.LBB14_11
# BB#8:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB14_3 Depth=2
	movq	%r11, %rcx
	andq	$-8, %rcx
	leaq	(%rdi,%r12), %rdx
	movq	%r13, %rbp
	movq	%r14, %rsi
	.p2align	4, 0x90
.LBB14_9:                               # %vector.body
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movss	(%rbx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	mulps	%xmm2, %xmm0
	mulps	%xmm1, %xmm2
	movups	-16(%rsi), %xmm1
	movups	(%rsi), %xmm3
	subps	%xmm0, %xmm1
	subps	%xmm2, %xmm3
	movups	%xmm1, -16(%rsi)
	movups	%xmm3, (%rsi)
	addq	$32, %rsi
	addq	$32, %rbp
	addq	$-8, %rcx
	jne	.LBB14_9
# BB#10:                                # %middle.block
                                        #   in Loop: Header=BB14_3 Depth=2
	cmpq	%r12, %r10
	je	.LBB14_16
	.p2align	4, 0x90
.LBB14_11:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB14_3 Depth=2
	testb	$1, %dl
	movq	%rdx, %rcx
	je	.LBB14_13
# BB#12:                                # %scalar.ph.prol
                                        #   in Loop: Header=BB14_3 Depth=2
	movq	%r9, %rcx
	shlq	$10, %rcx
	movss	global_data+640192(%rcx,%rdx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	global_data(,%r9,4), %xmm0
	movss	global_data(,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, global_data(,%rdx,4)
	leaq	1(%rdx), %rcx
.LBB14_13:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB14_3 Depth=2
	cmpq	$255, %rdx
	je	.LBB14_16
# BB#14:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB14_3 Depth=2
	leaq	(%rax,%rcx,4), %rdx
	addq	$-256, %rcx
	movl	$global_data, %esi
	.p2align	4, 0x90
.LBB14_15:                              # %scalar.ph
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	640192(%rdx,%rsi), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	(%rbx), %xmm0
	movss	global_data+1024(,%rcx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, global_data+1024(,%rcx,4)
	movss	640196(%rdx,%rsi), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	(%rbx), %xmm0
	movss	global_data+1028(,%rcx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, global_data+1028(,%rcx,4)
	addq	$8, %rsi
	addq	$2, %rcx
	jne	.LBB14_15
.LBB14_16:                              # %.loopexit
                                        #   in Loop: Header=BB14_3 Depth=2
	incq	%rdi
	addq	$4, %r14
	addq	$1028, %r13             # imm = 0x404
	decq	%r11
	addq	$1024, %rax             # imm = 0x400
	cmpq	$256, %r8               # imm = 0x100
	jne	.LBB14_3
# BB#17:                                #   in Loop: Header=BB14_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi72:
	.cfi_adjust_cfa_offset -16
	incl	%r15d
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$1000, %ecx, %eax       # imm = 0x3E8
	cmpl	%eax, %r15d
	jl	.LBB14_2
.LBB14_18:                              # %._crit_edge
	movl	$.L.str.145, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB14_19:                              # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm0
	addss	global_data+128004(%rax), %xmm0
	addss	global_data+128008(%rax), %xmm0
	addss	global_data+128012(%rax), %xmm0
	addss	global_data+128016(%rax), %xmm0
	addss	global_data+128020(%rax), %xmm0
	addss	global_data+128024(%rax), %xmm0
	addss	global_data+128028(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB14_19
# BB#20:                                # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB14_21:                              # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_22 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB14_22:                              #   Parent Loop BB14_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB14_22
# BB#23:                                #   in Loop: Header=BB14_21 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB14_21
# BB#24:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	s115, .Lfunc_end14-s115
	.cfi_endproc

	.globl	s1115
	.p2align	4, 0x90
	.type	s1115,@function
s1115:                                  # @s1115
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 16
.Lcfi74:
	.cfi_offset %rbx, -16
	movl	$.L.str.6, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB15_7
# BB#1:                                 # %.preheader32.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_2:                               # %.preheader32
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_3 Depth 2
                                        #       Child Loop BB15_4 Depth 3
	movl	$global_data+640192, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_3:                               # %.preheader
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_4 Depth 3
	movq	$-262144, %rdx          # imm = 0xFFFC0000
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB15_4:                               #   Parent Loop BB15_2 Depth=1
                                        #     Parent Loop BB15_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	global_data+1426784(%rdx,%rcx,4), %xmm0
	addss	262224(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movss	4(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	global_data+1427808(%rdx,%rcx,4), %xmm0
	addss	262228(%rsi), %xmm0
	movss	%xmm0, 4(%rsi)
	addq	$8, %rsi
	addq	$2048, %rdx             # imm = 0x800
	jne	.LBB15_4
# BB#5:                                 #   in Loop: Header=BB15_3 Depth=2
	incq	%rcx
	addq	$1024, %rax             # imm = 0x400
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB15_3
# BB#6:                                 #   in Loop: Header=BB15_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi77:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$100, %ecx, %eax
	cmpl	%eax, %ebx
	jl	.LBB15_2
.LBB15_7:                               # %._crit_edge
	movl	$.L.str.146, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$global_data+640204, %ecx
	.p2align	4, 0x90
.LBB15_8:                               # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_9 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB15_9:                               #   Parent Loop BB15_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	-12(%rsi), %xmm0
	addss	-8(%rsi), %xmm0
	addss	-4(%rsi), %xmm0
	addss	(%rsi), %xmm0
	addq	$16, %rsi
	addq	$-4, %rdx
	jne	.LBB15_9
# BB#10:                                #   in Loop: Header=BB15_8 Depth=1
	incq	%rax
	addq	$1024, %rcx             # imm = 0x400
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB15_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end15:
	.size	s1115, .Lfunc_end15-s1115
	.cfi_endproc

	.globl	s116
	.p2align	4, 0x90
	.type	s116,@function
s116:                                   # @s116
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 16
.Lcfi79:
	.cfi_offset %rbx, -16
	movl	$.L.str.7, %edi
	callq	init
	cmpl	$0, ntimes(%rip)
	jle	.LBB16_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_3 Depth 2
	movss	global_data(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movl	$5, %eax
	jmp	.LBB16_3
	.p2align	4, 0x90
.LBB16_12:                              #   in Loop: Header=BB16_3 Depth=2
	movups	global_data+4(,%rax,4), %xmm1
	shufps	$0, %xmm1, %xmm0        # xmm0 = xmm0[0,0],xmm1[0,0]
	shufps	$152, %xmm1, %xmm0      # xmm0 = xmm0[0,2],xmm1[1,2]
	mulps	%xmm1, %xmm0
	movups	%xmm0, global_data(,%rax,4)
	movss	global_data+20(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	shufps	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3]
	mulss	%xmm0, %xmm1
	movss	%xmm1, global_data+16(,%rax,4)
	addq	$10, %rax
.LBB16_3:                               #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	global_data-16(,%rax,4), %xmm1
	shufps	$0, %xmm1, %xmm0        # xmm0 = xmm0[0,0],xmm1[0,0]
	shufps	$152, %xmm1, %xmm0      # xmm0 = xmm0[0,2],xmm1[1,2]
	mulps	%xmm1, %xmm0
	movups	%xmm0, global_data-20(,%rax,4)
	movss	global_data(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	shufps	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3]
	mulss	%xmm0, %xmm1
	movss	%xmm1, global_data-4(,%rax,4)
	cmpq	$31994, %rax            # imm = 0x7CFA
	jle	.LBB16_12
# BB#4:                                 #   in Loop: Header=BB16_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi82:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	addl	%eax, %eax
	leal	(%rax,%rax,4), %eax
	cmpl	%eax, %ebx
	jl	.LBB16_2
.LBB16_5:                               # %._crit_edge
	movl	$.L.str.147, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB16_6:                               # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm0
	addss	global_data+128004(%rax), %xmm0
	addss	global_data+128008(%rax), %xmm0
	addss	global_data+128012(%rax), %xmm0
	addss	global_data+128016(%rax), %xmm0
	addss	global_data+128020(%rax), %xmm0
	addss	global_data+128024(%rax), %xmm0
	addss	global_data+128028(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB16_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_8:                               # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB16_9:                               #   Parent Loop BB16_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB16_9
# BB#10:                                #   in Loop: Header=BB16_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB16_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end16:
	.size	s116, .Lfunc_end16-s116
	.cfi_endproc

	.globl	s118
	.p2align	4, 0x90
	.type	s118,@function
s118:                                   # @s118
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -24
.Lcfi87:
	.cfi_offset %r14, -16
	movl	$.L.str.8, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB17_5
# BB#1:                                 # %.preheader30.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB17_2:                               # %.preheader30
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_3 Depth 2
                                        #       Child Loop BB17_15 Depth 3
	xorl	%eax, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB17_3:                               # %.lr.ph
                                        #   Parent Loop BB17_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_15 Depth 3
	movss	global_data(,%rcx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	testb	$1, %al
	jne	.LBB17_4
# BB#12:                                #   in Loop: Header=BB17_3 Depth=2
	movss	global_data+902416(,%rcx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	global_data-4(,%rcx,4), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, global_data(,%rcx,4)
	movl	$1, %ebx
	testq	%rax, %rax
	jne	.LBB17_14
	jmp	.LBB17_16
	.p2align	4, 0x90
.LBB17_4:                               #   in Loop: Header=BB17_3 Depth=2
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.LBB17_16
.LBB17_14:                              # %.lr.ph.new
                                        #   in Loop: Header=BB17_3 Depth=2
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	shlq	$10, %rsi
	leaq	global_data(%rsi), %rsi
	shlq	$2, %rbx
	movl	$global_data, %edi
	subq	%rbx, %rdi
	.p2align	4, 0x90
.LBB17_15:                              #   Parent Loop BB17_2 Depth=1
                                        #     Parent Loop BB17_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	902420(%rsi,%rax,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi,%rax,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, global_data(,%rcx,4)
	movss	903444(%rsi,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	-4(%rdi,%rax,4), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, global_data(,%rcx,4)
	addq	$2048, %rsi             # imm = 0x800
	addq	$-8, %rdi
	addq	$-2, %rdx
	jne	.LBB17_15
.LBB17_16:                              # %._crit_edge
                                        #   in Loop: Header=BB17_3 Depth=2
	incq	%rcx
	incq	%rax
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB17_3
# BB#17:                                #   in Loop: Header=BB17_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi90:
	.cfi_adjust_cfa_offset -16
	incl	%r14d
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$200, %ecx, %eax
	cmpl	%eax, %r14d
	jl	.LBB17_2
.LBB17_5:                               # %._crit_edge38
	movl	$.L.str.148, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB17_6:                               # =>This Inner Loop Header: Depth=1
	addss	global_data+128000(%rax), %xmm0
	addss	global_data+128004(%rax), %xmm0
	addss	global_data+128008(%rax), %xmm0
	addss	global_data+128012(%rax), %xmm0
	addss	global_data+128016(%rax), %xmm0
	addss	global_data+128020(%rax), %xmm0
	addss	global_data+128024(%rax), %xmm0
	addss	global_data+128028(%rax), %xmm0
	addq	$32, %rax
	jne	.LBB17_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB17_8:                               # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB17_9:                               #   Parent Loop BB17_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB17_9
# BB#10:                                #   in Loop: Header=BB17_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB17_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	s118, .Lfunc_end17-s118
	.cfi_endproc

	.globl	s119
	.p2align	4, 0x90
	.type	s119,@function
s119:                                   # @s119
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 16
.Lcfi92:
	.cfi_offset %rbx, -16
	movl	$.L.str.9, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB18_7
# BB#1:                                 # %.preheader30.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_2:                               # %.preheader30
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_3 Depth 2
                                        #       Child Loop BB18_4 Depth 3
	movl	$global_data+640192, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB18_3:                               # %.preheader
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_4 Depth 3
	leaq	-1(%rcx), %rdx
	movl	$248, %esi
	movq	%rax, %rdi
	jmp	.LBB18_4
	.p2align	4, 0x90
.LBB18_12:                              # %vector.body.1
                                        #   in Loop: Header=BB18_4 Depth=3
	movups	263268(%rdi), %xmm0
	addps	16(%rdi), %xmm0
	movups	%xmm0, 1044(%rdi)
	addq	$32, %rdi
	addq	$-8, %rsi
.LBB18_4:                               # %vector.body
                                        #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	263252(%rdi), %xmm0
	addps	(%rdi), %xmm0
	movups	%xmm0, 1028(%rdi)
	testq	%rsi, %rsi
	jne	.LBB18_12
# BB#5:                                 # %scalar.ph
                                        #   in Loop: Header=BB18_3 Depth=2
	shlq	$10, %rdx
	movss	global_data+641200(%rdx), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movq	%rcx, %rsi
	shlq	$10, %rsi
	addss	global_data+903428(%rsi), %xmm0
	movss	%xmm0, global_data+641204(%rsi)
	movss	global_data+641204(%rdx), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+903432(%rsi), %xmm0
	movss	%xmm0, global_data+641208(%rsi)
	movss	global_data+641208(%rdx), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	global_data+903436(%rsi), %xmm0
	movss	%xmm0, global_data+641212(%rsi)
	incq	%rcx
	addq	$1024, %rax             # imm = 0x400
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB18_3
# BB#6:                                 #   in Loop: Header=BB18_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi95:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$200, %ecx, %eax
	cmpl	%eax, %ebx
	jl	.LBB18_2
.LBB18_7:                               # %._crit_edge
	movl	$.L.str.149, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$global_data+640204, %ecx
	.p2align	4, 0x90
.LBB18_8:                               # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_9 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB18_9:                               #   Parent Loop BB18_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	-12(%rsi), %xmm0
	addss	-8(%rsi), %xmm0
	addss	-4(%rsi), %xmm0
	addss	(%rsi), %xmm0
	addq	$16, %rsi
	addq	$-4, %rdx
	jne	.LBB18_9
# BB#10:                                #   in Loop: Header=BB18_8 Depth=1
	incq	%rax
	addq	$1024, %rcx             # imm = 0x400
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB18_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end18:
	.size	s119, .Lfunc_end18-s119
	.cfi_endproc

	.globl	s1119
	.p2align	4, 0x90
	.type	s1119,@function
s1119:                                  # @s1119
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 16
.Lcfi97:
	.cfi_offset %rbx, -16
	movl	$.L.str.9, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB19_7
# BB#1:                                 # %.preheader30.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_2:                               # %.preheader30
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_3 Depth 2
                                        #       Child Loop BB19_4 Depth 3
	movl	$global_data+640192, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB19_3:                               # %.preheader
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_4 Depth 3
	movl	$256, %edx              # imm = 0x100
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB19_4:                               # %vector.body
                                        #   Parent Loop BB19_2 Depth=1
                                        #     Parent Loop BB19_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movaps	(%rsi), %xmm0
	addps	263248(%rsi), %xmm0
	movaps	%xmm0, 1024(%rsi)
	movaps	16(%rsi), %xmm0
	addps	263264(%rsi), %xmm0
	movaps	%xmm0, 1040(%rsi)
	addq	$32, %rsi
	addq	$-8, %rdx
	jne	.LBB19_4
# BB#5:                                 # %middle.block
                                        #   in Loop: Header=BB19_3 Depth=2
	incq	%rcx
	addq	$1024, %rax             # imm = 0x400
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB19_3
# BB#6:                                 #   in Loop: Header=BB19_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+128016, %esi
	movl	$global_data+256048, %edx
	movl	$global_data+384080, %ecx
	movl	$global_data+512128, %r8d
	movl	$global_data+640192, %r9d
	xorps	%xmm0, %xmm0
	pushq	$global_data+1164640
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+902416
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi100:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$200, %ecx, %eax
	cmpl	%eax, %ebx
	jl	.LBB19_2
.LBB19_7:                               # %._crit_edge
	movl	$.L.str.150, %edi
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$global_data+640204, %ecx
	.p2align	4, 0x90
.LBB19_8:                               # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_9 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB19_9:                               #   Parent Loop BB19_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	-12(%rsi), %xmm0
	addss	-8(%rsi), %xmm0
	addss	-4(%rsi), %xmm0
	addss	(%rsi), %xmm0
	addq	$16, %rsi
	addq	$-4, %rdx
	jne	.LBB19_9
# BB#10:                                #   in Loop: Header=BB19_8 Depth=1
	incq	%rax
	addq	$1024, %rcx             # imm = 0x400
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB19_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end19:
	.size	s1119, .Lfunc_end19-s1119
	.cfi_endproc

	.globl	min
	.p2align	4, 0x90
	.type	min,@function
min:                                    # @min
	.cfi_startproc
# BB#0:
	cmpl	%esi, %edi
	cmovlel	%edi, %esi
	movl	%esi, %eax
	retq
.Lfunc_end20:
	.size	min, .Lfunc_end20-min
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI21_0:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
.LCPI21_2:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI21_3:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI21_4:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
.LCPI21_5:
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
.LCPI21_6:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI21_1:
	.long	1065353216              # float 1
	.text
	.globl	set
	.p2align	4, 0x90
	.type	set,@function
set:                                    # @set
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 32
.Lcfi104:
	.cfi_offset %rbx, -32
.Lcfi105:
	.cfi_offset %r14, -24
.Lcfi106:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$xx, %edi
	movl	$16, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	posix_memalign
	movl	$10, %edi
	callq	putchar
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB21_1:                               # =>This Inner Loop Header: Depth=1
	leal	4(%rax), %ecx
	movl	%ecx, (%rbx,%rax,4)
	leal	2(%rax), %ecx
	movl	%ecx, 4(%rbx,%rax,4)
	movl	%eax, 8(%rbx,%rax,4)
	leal	3(%rax), %ecx
	movl	%ecx, 12(%rbx,%rax,4)
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rbx,%rax,4)
	leal	9(%rax), %ecx
	movl	%ecx, 20(%rbx,%rax,4)
	leal	7(%rax), %ecx
	movl	%ecx, 24(%rbx,%rax,4)
	leal	5(%rax), %ecx
	movl	%ecx, 28(%rbx,%rax,4)
	leal	8(%rax), %ecx
	movl	%ecx, 32(%rbx,%rax,4)
	leal	6(%rax), %ecx
	movl	%ecx, 36(%rbx,%rax,4)
	addq	$10, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	jl	.LBB21_1
# BB#2:                                 # %vector.body.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movaps	.LCPI21_0(%rip), %xmm0  # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB21_3:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+128000(%rax)
	movaps	%xmm0, global_data+128016(%rax)
	movaps	%xmm0, global_data+128032(%rax)
	movaps	%xmm0, global_data+128048(%rax)
	movaps	%xmm0, global_data+128064(%rax)
	movaps	%xmm0, global_data+128080(%rax)
	movaps	%xmm0, global_data+128096(%rax)
	movaps	%xmm0, global_data+128112(%rax)
	movaps	%xmm0, global_data+128128(%rax)
	movaps	%xmm0, global_data+128144(%rax)
	movaps	%xmm0, global_data+128160(%rax)
	movaps	%xmm0, global_data+128176(%rax)
	movaps	%xmm0, global_data+128192(%rax)
	movaps	%xmm0, global_data+128208(%rax)
	movaps	%xmm0, global_data+128224(%rax)
	movaps	%xmm0, global_data+128240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB21_3
# BB#4:                                 # %vector.body70.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB21_5:                               # %vector.body70
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	movaps	%xmm0, global_data+256256(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB21_5
# BB#6:                                 # %vector.body83.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB21_7:                               # %vector.body83
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	movaps	%xmm0, global_data+384256(%rax)
	movaps	%xmm0, global_data+384272(%rax)
	movaps	%xmm0, global_data+384288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB21_7
# BB#8:                                 # %vector.body96.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB21_9:                               # %vector.body96
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	movaps	%xmm0, global_data+512288(%rax)
	movaps	%xmm0, global_data+512304(%rax)
	movaps	%xmm0, global_data+512320(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB21_9
# BB#10:                                # %vector.body109.preheader
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	.p2align	4, 0x90
.LBB21_11:                              # %vector.body109
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+640128(%rax)
	movaps	%xmm0, global_data+640144(%rax)
	movaps	%xmm0, global_data+640160(%rax)
	movaps	%xmm0, global_data+640176(%rax)
	movaps	%xmm0, global_data+640192(%rax)
	movaps	%xmm0, global_data+640208(%rax)
	movaps	%xmm0, global_data+640224(%rax)
	movaps	%xmm0, global_data+640240(%rax)
	movaps	%xmm0, global_data+640256(%rax)
	movaps	%xmm0, global_data+640272(%rax)
	movaps	%xmm0, global_data+640288(%rax)
	movaps	%xmm0, global_data+640304(%rax)
	movaps	%xmm0, global_data+640320(%rax)
	movaps	%xmm0, global_data+640336(%rax)
	movaps	%xmm0, global_data+640352(%rax)
	movaps	%xmm0, global_data+640368(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB21_11
# BB#12:                                # %.preheader44.i39.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	movss	.LCPI21_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB21_13:                              # %vector.body122
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+902336(%rax)
	movaps	%xmm1, global_data+902352(%rax)
	movaps	%xmm1, global_data+902368(%rax)
	movaps	%xmm1, global_data+902384(%rax)
	movaps	%xmm1, global_data+902400(%rax)
	movaps	%xmm1, global_data+902416(%rax)
	movaps	%xmm1, global_data+902432(%rax)
	movaps	%xmm1, global_data+902448(%rax)
	movaps	%xmm1, global_data+902464(%rax)
	movaps	%xmm1, global_data+902480(%rax)
	movaps	%xmm1, global_data+902496(%rax)
	movaps	%xmm1, global_data+902512(%rax)
	movaps	%xmm1, global_data+902528(%rax)
	movaps	%xmm1, global_data+902544(%rax)
	movaps	%xmm1, global_data+902560(%rax)
	movaps	%xmm1, global_data+902576(%rax)
	movaps	%xmm1, global_data+902592(%rax)
	movaps	%xmm1, global_data+902608(%rax)
	movaps	%xmm1, global_data+902624(%rax)
	movaps	%xmm1, global_data+902640(%rax)
	movaps	%xmm1, global_data+902656(%rax)
	movaps	%xmm1, global_data+902672(%rax)
	movaps	%xmm1, global_data+902688(%rax)
	movaps	%xmm1, global_data+902704(%rax)
	movaps	%xmm1, global_data+902720(%rax)
	movaps	%xmm1, global_data+902736(%rax)
	movaps	%xmm1, global_data+902752(%rax)
	movaps	%xmm1, global_data+902768(%rax)
	movaps	%xmm1, global_data+902784(%rax)
	movaps	%xmm1, global_data+902800(%rax)
	movaps	%xmm1, global_data+902816(%rax)
	movaps	%xmm1, global_data+902832(%rax)
	movaps	%xmm1, global_data+902848(%rax)
	movaps	%xmm1, global_data+902864(%rax)
	movaps	%xmm1, global_data+902880(%rax)
	movaps	%xmm1, global_data+902896(%rax)
	movaps	%xmm1, global_data+902912(%rax)
	movaps	%xmm1, global_data+902928(%rax)
	movaps	%xmm1, global_data+902944(%rax)
	movaps	%xmm1, global_data+902960(%rax)
	movaps	%xmm1, global_data+902976(%rax)
	movaps	%xmm1, global_data+902992(%rax)
	movaps	%xmm1, global_data+903008(%rax)
	movaps	%xmm1, global_data+903024(%rax)
	movaps	%xmm1, global_data+903040(%rax)
	movaps	%xmm1, global_data+903056(%rax)
	movaps	%xmm1, global_data+903072(%rax)
	movaps	%xmm1, global_data+903088(%rax)
	movaps	%xmm1, global_data+903104(%rax)
	movaps	%xmm1, global_data+903120(%rax)
	movaps	%xmm1, global_data+903136(%rax)
	movaps	%xmm1, global_data+903152(%rax)
	movaps	%xmm1, global_data+903168(%rax)
	movaps	%xmm1, global_data+903184(%rax)
	movaps	%xmm1, global_data+903200(%rax)
	movaps	%xmm1, global_data+903216(%rax)
	movaps	%xmm1, global_data+903232(%rax)
	movaps	%xmm1, global_data+903248(%rax)
	movaps	%xmm1, global_data+903264(%rax)
	movaps	%xmm1, global_data+903280(%rax)
	movaps	%xmm1, global_data+903296(%rax)
	movaps	%xmm1, global_data+903312(%rax)
	movaps	%xmm1, global_data+903328(%rax)
	movaps	%xmm1, global_data+903344(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB21_13
# BB#14:                                # %.preheader44.i30.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB21_15:                              # %vector.body137
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1164560(%rax)
	movaps	%xmm1, global_data+1164576(%rax)
	movaps	%xmm1, global_data+1164592(%rax)
	movaps	%xmm1, global_data+1164608(%rax)
	movaps	%xmm1, global_data+1164624(%rax)
	movaps	%xmm1, global_data+1164640(%rax)
	movaps	%xmm1, global_data+1164656(%rax)
	movaps	%xmm1, global_data+1164672(%rax)
	movaps	%xmm1, global_data+1164688(%rax)
	movaps	%xmm1, global_data+1164704(%rax)
	movaps	%xmm1, global_data+1164720(%rax)
	movaps	%xmm1, global_data+1164736(%rax)
	movaps	%xmm1, global_data+1164752(%rax)
	movaps	%xmm1, global_data+1164768(%rax)
	movaps	%xmm1, global_data+1164784(%rax)
	movaps	%xmm1, global_data+1164800(%rax)
	movaps	%xmm1, global_data+1164816(%rax)
	movaps	%xmm1, global_data+1164832(%rax)
	movaps	%xmm1, global_data+1164848(%rax)
	movaps	%xmm1, global_data+1164864(%rax)
	movaps	%xmm1, global_data+1164880(%rax)
	movaps	%xmm1, global_data+1164896(%rax)
	movaps	%xmm1, global_data+1164912(%rax)
	movaps	%xmm1, global_data+1164928(%rax)
	movaps	%xmm1, global_data+1164944(%rax)
	movaps	%xmm1, global_data+1164960(%rax)
	movaps	%xmm1, global_data+1164976(%rax)
	movaps	%xmm1, global_data+1164992(%rax)
	movaps	%xmm1, global_data+1165008(%rax)
	movaps	%xmm1, global_data+1165024(%rax)
	movaps	%xmm1, global_data+1165040(%rax)
	movaps	%xmm1, global_data+1165056(%rax)
	movaps	%xmm1, global_data+1165072(%rax)
	movaps	%xmm1, global_data+1165088(%rax)
	movaps	%xmm1, global_data+1165104(%rax)
	movaps	%xmm1, global_data+1165120(%rax)
	movaps	%xmm1, global_data+1165136(%rax)
	movaps	%xmm1, global_data+1165152(%rax)
	movaps	%xmm1, global_data+1165168(%rax)
	movaps	%xmm1, global_data+1165184(%rax)
	movaps	%xmm1, global_data+1165200(%rax)
	movaps	%xmm1, global_data+1165216(%rax)
	movaps	%xmm1, global_data+1165232(%rax)
	movaps	%xmm1, global_data+1165248(%rax)
	movaps	%xmm1, global_data+1165264(%rax)
	movaps	%xmm1, global_data+1165280(%rax)
	movaps	%xmm1, global_data+1165296(%rax)
	movaps	%xmm1, global_data+1165312(%rax)
	movaps	%xmm1, global_data+1165328(%rax)
	movaps	%xmm1, global_data+1165344(%rax)
	movaps	%xmm1, global_data+1165360(%rax)
	movaps	%xmm1, global_data+1165376(%rax)
	movaps	%xmm1, global_data+1165392(%rax)
	movaps	%xmm1, global_data+1165408(%rax)
	movaps	%xmm1, global_data+1165424(%rax)
	movaps	%xmm1, global_data+1165440(%rax)
	movaps	%xmm1, global_data+1165456(%rax)
	movaps	%xmm1, global_data+1165472(%rax)
	movaps	%xmm1, global_data+1165488(%rax)
	movaps	%xmm1, global_data+1165504(%rax)
	movaps	%xmm1, global_data+1165520(%rax)
	movaps	%xmm1, global_data+1165536(%rax)
	movaps	%xmm1, global_data+1165552(%rax)
	movaps	%xmm1, global_data+1165568(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB21_15
# BB#16:                                # %.preheader44.i.preheader
	movq	$-262144, %rax          # imm = 0xFFFC0000
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB21_17:                              # %vector.body152
                                        # =>This Inner Loop Header: Depth=1
	cvtsi2ssl	%ecx, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm1, global_data+1426784(%rax)
	movaps	%xmm1, global_data+1426800(%rax)
	movaps	%xmm1, global_data+1426816(%rax)
	movaps	%xmm1, global_data+1426832(%rax)
	movaps	%xmm1, global_data+1426848(%rax)
	movaps	%xmm1, global_data+1426864(%rax)
	movaps	%xmm1, global_data+1426880(%rax)
	movaps	%xmm1, global_data+1426896(%rax)
	movaps	%xmm1, global_data+1426912(%rax)
	movaps	%xmm1, global_data+1426928(%rax)
	movaps	%xmm1, global_data+1426944(%rax)
	movaps	%xmm1, global_data+1426960(%rax)
	movaps	%xmm1, global_data+1426976(%rax)
	movaps	%xmm1, global_data+1426992(%rax)
	movaps	%xmm1, global_data+1427008(%rax)
	movaps	%xmm1, global_data+1427024(%rax)
	movaps	%xmm1, global_data+1427040(%rax)
	movaps	%xmm1, global_data+1427056(%rax)
	movaps	%xmm1, global_data+1427072(%rax)
	movaps	%xmm1, global_data+1427088(%rax)
	movaps	%xmm1, global_data+1427104(%rax)
	movaps	%xmm1, global_data+1427120(%rax)
	movaps	%xmm1, global_data+1427136(%rax)
	movaps	%xmm1, global_data+1427152(%rax)
	movaps	%xmm1, global_data+1427168(%rax)
	movaps	%xmm1, global_data+1427184(%rax)
	movaps	%xmm1, global_data+1427200(%rax)
	movaps	%xmm1, global_data+1427216(%rax)
	movaps	%xmm1, global_data+1427232(%rax)
	movaps	%xmm1, global_data+1427248(%rax)
	movaps	%xmm1, global_data+1427264(%rax)
	movaps	%xmm1, global_data+1427280(%rax)
	movaps	%xmm1, global_data+1427296(%rax)
	movaps	%xmm1, global_data+1427312(%rax)
	movaps	%xmm1, global_data+1427328(%rax)
	movaps	%xmm1, global_data+1427344(%rax)
	movaps	%xmm1, global_data+1427360(%rax)
	movaps	%xmm1, global_data+1427376(%rax)
	movaps	%xmm1, global_data+1427392(%rax)
	movaps	%xmm1, global_data+1427408(%rax)
	movaps	%xmm1, global_data+1427424(%rax)
	movaps	%xmm1, global_data+1427440(%rax)
	movaps	%xmm1, global_data+1427456(%rax)
	movaps	%xmm1, global_data+1427472(%rax)
	movaps	%xmm1, global_data+1427488(%rax)
	movaps	%xmm1, global_data+1427504(%rax)
	movaps	%xmm1, global_data+1427520(%rax)
	movaps	%xmm1, global_data+1427536(%rax)
	movaps	%xmm1, global_data+1427552(%rax)
	movaps	%xmm1, global_data+1427568(%rax)
	movaps	%xmm1, global_data+1427584(%rax)
	movaps	%xmm1, global_data+1427600(%rax)
	movaps	%xmm1, global_data+1427616(%rax)
	movaps	%xmm1, global_data+1427632(%rax)
	movaps	%xmm1, global_data+1427648(%rax)
	movaps	%xmm1, global_data+1427664(%rax)
	movaps	%xmm1, global_data+1427680(%rax)
	movaps	%xmm1, global_data+1427696(%rax)
	movaps	%xmm1, global_data+1427712(%rax)
	movaps	%xmm1, global_data+1427728(%rax)
	movaps	%xmm1, global_data+1427744(%rax)
	movaps	%xmm1, global_data+1427760(%rax)
	movaps	%xmm1, global_data+1427776(%rax)
	movaps	%xmm1, global_data+1427792(%rax)
	incl	%ecx
	addq	$1024, %rax             # imm = 0x400
	jne	.LBB21_17
# BB#18:                                # %vector.body167.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI21_2(%rip), %xmm1  # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI21_3(%rip), %xmm2  # xmm2 = [1,1,1,1]
	movdqa	.LCPI21_4(%rip), %xmm8  # xmm8 = [5,5,5,5]
	movdqa	.LCPI21_5(%rip), %xmm4  # xmm4 = [4294967292,4294967292,4294967292,4294967292]
	movdqa	.LCPI21_6(%rip), %xmm5  # xmm5 = [8,8]
	.p2align	4, 0x90
.LBB21_19:                              # %vector.body167
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm3
	paddd	%xmm2, %xmm3
	paddd	%xmm8, %xmm6
	movdqa	%xmm3, %xmm7
	psrad	$31, %xmm7
	psrld	$30, %xmm7
	paddd	%xmm3, %xmm7
	pand	%xmm4, %xmm7
	psubd	%xmm7, %xmm3
	movdqa	%xmm6, %xmm7
	psrad	$31, %xmm7
	psrld	$30, %xmm7
	paddd	%xmm6, %xmm7
	pand	%xmm4, %xmm7
	psubd	%xmm7, %xmm6
	paddd	%xmm2, %xmm3
	paddd	%xmm2, %xmm6
	movdqa	%xmm3, indx+128000(%rax)
	movdqa	%xmm6, indx+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB21_19
# BB#20:                                # %middle.block168
	movl	$1065353216, (%r15)     # imm = 0x3F800000
	movl	$1073741824, (%r14)     # imm = 0x40000000
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	set, .Lfunc_end21-set
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi109:
	.cfi_def_cfa_offset 48
.Lcfi110:
	.cfi_offset %rbx, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	leaq	8(%rsp), %rdi
	movl	$16, %esi
	movl	$128000, %edx           # imm = 0x1F400
	callq	posix_memalign
	cmpl	$1, %ebp
	jle	.LBB22_1
# BB#2:
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rcx
	movl	%ecx, ntimes(%rip)
	movl	$.L.str.152, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	cmpl	$2, %ebp
	je	.LBB22_4
# BB#3:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, digits(%rip)
	jmp	.LBB22_4
.LBB22_1:                               # %.thread
	movl	ntimes(%rip), %esi
	movl	$.L.str.152, %edi
	xorl	%eax, %eax
	callq	printf
.LBB22_4:
	movq	8(%rsp), %rdi
	leaq	20(%rsp), %rsi
	leaq	16(%rsp), %rdx
	callq	set
	movl	$.Lstr, %edi
	callq	puts
	callq	s000
	callq	s111
	callq	s1111
	callq	s112
	callq	s1112
	callq	s113
	callq	s1113
	callq	s114
	callq	s115
	callq	s1115
	callq	s116
	callq	s118
	callq	s119
	callq	s1119
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end22:
	.size	main, .Lfunc_end22-main
	.cfi_endproc

	.type	global_data,@object     # @global_data
	.comm	global_data,1689024,16
	.type	a,@object               # @a
	.section	.rodata,"a",@progbits
	.globl	a
	.p2align	4
a:
	.quad	global_data
	.size	a, 8

	.type	b,@object               # @b
	.globl	b
	.p2align	4
b:
	.quad	global_data+128016
	.size	b, 8

	.type	c,@object               # @c
	.globl	c
	.p2align	4
c:
	.quad	global_data+256048
	.size	c, 8

	.type	d,@object               # @d
	.globl	d
	.p2align	4
d:
	.quad	global_data+384080
	.size	d, 8

	.type	e,@object               # @e
	.globl	e
	.p2align	4
e:
	.quad	global_data+512128
	.size	e, 8

	.type	aa,@object              # @aa
	.globl	aa
	.p2align	4
aa:
	.quad	global_data+640192
	.size	aa, 8

	.type	bb,@object              # @bb
	.globl	bb
	.p2align	4
bb:
	.quad	global_data+902416
	.size	bb, 8

	.type	cc,@object              # @cc
	.globl	cc
	.p2align	4
cc:
	.quad	global_data+1164640
	.size	cc, 8

	.type	tt,@object              # @tt
	.globl	tt
	.p2align	4
tt:
	.quad	global_data+1426880
	.size	tt, 8

	.type	array,@object           # @array
	.comm	array,262144,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%.*g \n"
	.size	.L.str, 7

	.type	digits,@object          # @digits
	.data
	.p2align	2
digits:
	.long	6                       # 0x6
	.size	digits, 4

	.type	temp,@object            # @temp
	.comm	temp,4,4
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"s000 "
	.size	.L.str.1, 6

	.type	X,@object               # @X
	.comm	X,128000,16
	.type	Y,@object               # @Y
	.comm	Y,128000,16
	.type	Z,@object               # @Z
	.comm	Z,128000,16
	.type	U,@object               # @U
	.comm	U,128000,16
	.type	V,@object               # @V
	.comm	V,128000,16
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"s111 "
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"s112 "
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"s113 "
	.size	.L.str.4, 6

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"s114 "
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"s115 "
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"s116 "
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"s118 "
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"s119 "
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"s121 "
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"s122 "
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"s123 "
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"s124 "
	.size	.L.str.13, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"s125 "
	.size	.L.str.14, 6

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"s126 "
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"s127 "
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"s128 "
	.size	.L.str.17, 6

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"s131 "
	.size	.L.str.18, 6

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"s132 "
	.size	.L.str.19, 6

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"s141 "
	.size	.L.str.20, 6

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"s151 "
	.size	.L.str.21, 6

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"s152 "
	.size	.L.str.22, 6

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"s161 "
	.size	.L.str.23, 6

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"s162 "
	.size	.L.str.24, 6

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"s171 "
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"s172 "
	.size	.L.str.26, 6

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"s173 "
	.size	.L.str.27, 6

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"s174 "
	.size	.L.str.28, 6

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"s175 "
	.size	.L.str.29, 6

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"s176 "
	.size	.L.str.30, 6

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"s211 "
	.size	.L.str.31, 6

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"s212 "
	.size	.L.str.32, 6

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"s221 "
	.size	.L.str.33, 6

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"s222 "
	.size	.L.str.34, 6

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"s231 "
	.size	.L.str.35, 6

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"s232 "
	.size	.L.str.36, 6

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"s233 "
	.size	.L.str.37, 6

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"s234 "
	.size	.L.str.38, 6

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"s235 "
	.size	.L.str.39, 6

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"s241 "
	.size	.L.str.40, 6

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"s242 "
	.size	.L.str.41, 6

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"s243 "
	.size	.L.str.42, 6

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"s244 "
	.size	.L.str.43, 6

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"s251 "
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"s252 "
	.size	.L.str.45, 6

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"s253 "
	.size	.L.str.46, 6

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"s254 "
	.size	.L.str.47, 6

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"s255 "
	.size	.L.str.48, 6

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"s256 "
	.size	.L.str.49, 6

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"s257 "
	.size	.L.str.50, 6

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"s258 "
	.size	.L.str.51, 6

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"s261 "
	.size	.L.str.52, 6

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"s271 "
	.size	.L.str.53, 6

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"s272 "
	.size	.L.str.54, 6

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"s273 "
	.size	.L.str.55, 6

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"s274 "
	.size	.L.str.56, 6

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"s275 "
	.size	.L.str.57, 6

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"s276 "
	.size	.L.str.58, 6

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"s277 "
	.size	.L.str.59, 6

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"s278 "
	.size	.L.str.60, 6

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"s279 "
	.size	.L.str.61, 6

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"s2710"
	.size	.L.str.62, 6

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"s2711"
	.size	.L.str.63, 6

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"s2712"
	.size	.L.str.64, 6

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"s281 "
	.size	.L.str.65, 6

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"s291 "
	.size	.L.str.66, 6

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"s292 "
	.size	.L.str.67, 6

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"s293 "
	.size	.L.str.68, 6

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"s2101"
	.size	.L.str.69, 6

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"s2102"
	.size	.L.str.70, 6

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"s2111"
	.size	.L.str.71, 6

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"s311 "
	.size	.L.str.72, 6

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"s312 "
	.size	.L.str.73, 6

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"s313 "
	.size	.L.str.74, 6

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"s314 "
	.size	.L.str.75, 6

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"s315 "
	.size	.L.str.76, 6

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"s316 "
	.size	.L.str.77, 6

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"s317 "
	.size	.L.str.78, 6

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"s318 "
	.size	.L.str.79, 6

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"s319 "
	.size	.L.str.80, 6

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"s3110"
	.size	.L.str.81, 6

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"s3111"
	.size	.L.str.82, 6

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"s3112"
	.size	.L.str.83, 6

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"s3113"
	.size	.L.str.84, 6

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"s321 "
	.size	.L.str.85, 6

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"s322 "
	.size	.L.str.86, 6

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"s323 "
	.size	.L.str.87, 6

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"s331 "
	.size	.L.str.88, 6

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"s332 "
	.size	.L.str.89, 6

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"s341 "
	.size	.L.str.90, 6

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"s342 "
	.size	.L.str.91, 6

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"s343 "
	.size	.L.str.92, 6

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"s351 "
	.size	.L.str.93, 6

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"s352 "
	.size	.L.str.94, 6

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"s353 "
	.size	.L.str.95, 6

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"s411 "
	.size	.L.str.96, 6

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"s412 "
	.size	.L.str.97, 6

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"s413 "
	.size	.L.str.98, 6

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"s414 "
	.size	.L.str.99, 6

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"s415 "
	.size	.L.str.100, 6

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"s421 "
	.size	.L.str.101, 6

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"s422 "
	.size	.L.str.102, 6

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"s423 "
	.size	.L.str.103, 6

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"s424 "
	.size	.L.str.104, 6

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"s431 "
	.size	.L.str.105, 6

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"s432 "
	.size	.L.str.106, 6

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"s441 "
	.size	.L.str.107, 6

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"s442 "
	.size	.L.str.108, 6

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"s443 "
	.size	.L.str.109, 6

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"s451 "
	.size	.L.str.110, 6

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"s452 "
	.size	.L.str.111, 6

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"s453 "
	.size	.L.str.112, 6

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"s471 "
	.size	.L.str.113, 6

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"s481 "
	.size	.L.str.114, 6

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"s482 "
	.size	.L.str.115, 6

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"s491 "
	.size	.L.str.116, 6

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"s4112"
	.size	.L.str.117, 6

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"s4113"
	.size	.L.str.118, 6

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"s4114"
	.size	.L.str.119, 6

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"s4115"
	.size	.L.str.120, 6

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"s4116"
	.size	.L.str.121, 6

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"s4117"
	.size	.L.str.122, 6

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"s4121"
	.size	.L.str.123, 6

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"vag  "
	.size	.L.str.125, 6

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"vas  "
	.size	.L.str.126, 6

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"vif  "
	.size	.L.str.127, 6

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"vpv  "
	.size	.L.str.128, 6

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"vtv  "
	.size	.L.str.129, 6

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"vpvtv"
	.size	.L.str.130, 6

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"vpvts"
	.size	.L.str.131, 6

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"vpvpv"
	.size	.L.str.132, 6

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"vtvtv"
	.size	.L.str.133, 6

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"vsumr"
	.size	.L.str.134, 6

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"vdotr"
	.size	.L.str.135, 6

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"vbor "
	.size	.L.str.136, 6

	.type	ntimes,@object          # @ntimes
	.data
	.p2align	2
ntimes:
	.long	200000                  # 0x30d40
	.size	ntimes, 4

	.type	.L.str.137,@object      # @.str.137
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.137:
	.asciz	"S000\t %.2f \t\t"
	.size	.L.str.137, 14

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"S111\t %.2f \t\t"
	.size	.L.str.138, 14

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"S1111\t %.2f \t\t "
	.size	.L.str.139, 16

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"S112\t %.2f \t\t"
	.size	.L.str.140, 14

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"S1112\t %.2f \t\t "
	.size	.L.str.141, 16

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"S113\t %.2f \t\t"
	.size	.L.str.142, 14

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"S1113\t %.2f \t\t"
	.size	.L.str.143, 15

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"S114\t %.2f \t\t"
	.size	.L.str.144, 14

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"S115\t %.2f \t\t"
	.size	.L.str.145, 14

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"S1115\t %.2f \t\t"
	.size	.L.str.146, 15

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"S116\t %.2f \t\t"
	.size	.L.str.147, 14

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"S118\t %.2f \t\t"
	.size	.L.str.148, 14

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"S119\t %.2f \t\t "
	.size	.L.str.149, 15

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"S1119\t %.2f \t\t "
	.size	.L.str.150, 16

	.type	xx,@object              # @xx
	.comm	xx,8,8
	.type	indx,@object            # @indx
	.comm	indx,128000,16
	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"Running each loop %d times...\n"
	.size	.L.str.152, 31

	.type	x,@object               # @x
	.comm	x,128000,16
	.type	temp_int,@object        # @temp_int
	.comm	temp_int,4,4
	.type	yy,@object              # @yy
	.comm	yy,8,8
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Loop \t Time(Sec) \t Checksum "
	.size	.Lstr, 29


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
