	.text
	.file	"kimwl.bc"
	.globl	_Z5yylexv
	.p2align	4, 0x90
	.type	_Z5yylexv,@function
_Z5yylexv:                              # @_Z5yylexv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movb	_ZL7yy_init(%rip), %al
	testb	%al, %al
	jne	.LBB0_16
# BB#1:
	movb	$1, _ZL7yy_init(%rip)
	cmpq	$0, _ZL12yy_state_buf(%rip)
	jne	.LBB0_3
# BB#2:
	movl	$16386, %edi            # imm = 0x4002
	callq	malloc
	movq	%rax, _ZL12yy_state_buf(%rip)
.LBB0_3:
	cmpl	$0, _ZL8yy_start(%rip)
	jne	.LBB0_5
# BB#4:
	movl	$1, _ZL8yy_start(%rip)
.LBB0_5:
	movq	yyin(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_7
# BB#6:
	movq	stdin(%rip), %rbx
	movq	%rbx, yyin(%rip)
.LBB0_7:
	cmpq	$0, yyout(%rip)
	jne	.LBB0_9
# BB#8:
	movq	stdout(%rip), %rax
	movq	%rax, yyout(%rip)
.LBB0_9:
	movq	_ZL15yy_buffer_stack(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
# BB#10:
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rdi,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB0_15
# BB#11:                                # %.thread
	movq	_ZL19yy_buffer_stack_max(%rip), %rax
	leaq	-1(%rax), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB0_14
# BB#12:
	shlq	$32, %rax
	movabsq	$34359738368, %rcx      # imm = 0x800000000
	addq	%rax, %rcx
	movq	%rcx, %rbx
	sarq	$32, %rbx
	shrq	$29, %rcx
	movl	%ecx, %esi
	callq	realloc
	movq	%rax, _ZL15yy_buffer_stack(%rip)
	movq	_ZL19yy_buffer_stack_max(%rip), %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rax,%rcx,8)
	movups	%xmm0, 32(%rax,%rcx,8)
	movups	%xmm0, 16(%rax,%rcx,8)
	movups	%xmm0, (%rax,%rcx,8)
	movq	%rbx, _ZL19yy_buffer_stack_max(%rip)
	movq	yyin(%rip), %rbx
	jmp	.LBB0_14
.LBB0_13:                               # %.thread.thread
	movl	$8, %edi
	callq	malloc
	movq	%rax, _ZL15yy_buffer_stack(%rip)
	movq	$0, (%rax)
	movq	$1, _ZL19yy_buffer_stack_max(%rip)
	movq	$0, _ZL19yy_buffer_stack_top(%rip)
.LBB0_14:                               # %_ZL21yyensure_buffer_stackv.exit
	movl	$16384, %esi            # imm = 0x4000
	movq	%rbx, %rdi
	callq	_Z16yy_create_bufferP8_IO_FILEi
	movq	_ZL15yy_buffer_stack(%rip), %rdi
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	%rax, (%rdi,%rcx,8)
.LBB0_15:
	movl	28(%rax), %edx
	movl	%edx, _ZL10yy_n_chars(%rip)
	movq	16(%rax), %rax
	movq	%rax, _ZL10yy_c_buf_p(%rip)
	movq	%rax, yytext_ptr(%rip)
	movq	(%rdi,%rcx,8), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, yyin(%rip)
	movb	(%rax), %al
	movb	%al, _ZL12yy_hold_char(%rip)
.LBB0_16:                               # %_ZN12_GLOBAL__N_15countEb.exit.preheader
	movl	$yytext, %r14d
	movabsq	$-4294967296, %r12      # imm = 0xFFFFFFFF00000000
	jmp	.LBB0_18
.LBB0_17:                               #   in Loop: Header=BB0_18 Depth=1
	movq	yyout(%rip), %rcx
	movl	$yytext, %edi
	movl	$1, %edx
	callq	fwrite
	.p2align	4, 0x90
.LBB0_18:                               # %_ZN12_GLOBAL__N_15countEb.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_21 Depth 2
                                        #       Child Loop BB0_23 Depth 3
                                        #         Child Loop BB0_24 Depth 4
                                        #       Child Loop BB0_44 Depth 3
                                        #         Child Loop BB0_46 Depth 4
                                        #           Child Loop BB0_48 Depth 5
                                        #         Child Loop BB0_66 Depth 4
                                        #         Child Loop BB0_70 Depth 4
                                        #         Child Loop BB0_72 Depth 4
                                        #         Child Loop BB0_81 Depth 4
                                        #         Child Loop BB0_88 Depth 4
                                        #         Child Loop BB0_29 Depth 4
                                        #           Child Loop BB0_34 Depth 5
                                        #         Child Loop BB0_40 Depth 4
                                        #         Child Loop BB0_99 Depth 4
                                        #           Child Loop BB0_104 Depth 5
                                        #       Child Loop BB0_110 Depth 3
                                        #         Child Loop BB0_115 Depth 4
                                        #     Child Loop BB0_312 Depth 2
                                        #     Child Loop BB0_301 Depth 2
                                        #     Child Loop BB0_290 Depth 2
                                        #     Child Loop BB0_279 Depth 2
                                        #     Child Loop BB0_268 Depth 2
                                        #     Child Loop BB0_257 Depth 2
                                        #     Child Loop BB0_246 Depth 2
                                        #     Child Loop BB0_235 Depth 2
                                        #     Child Loop BB0_224 Depth 2
                                        #     Child Loop BB0_213 Depth 2
                                        #     Child Loop BB0_202 Depth 2
                                        #     Child Loop BB0_189 Depth 2
                                        #     Child Loop BB0_196 Depth 2
                                        #     Child Loop BB0_178 Depth 2
                                        #     Child Loop BB0_167 Depth 2
                                        #     Child Loop BB0_156 Depth 2
                                        #     Child Loop BB0_145 Depth 2
                                        #     Child Loop BB0_134 Depth 2
                                        #     Child Loop BB0_123 Depth 2
	movq	_ZL10yy_c_buf_p(%rip), %rbx
	movb	_ZL12yy_hold_char(%rip), %al
	movb	%al, (%rbx)
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %rax
	movl	40(%rax), %esi
	addl	_ZL8yy_start(%rip), %esi
	movq	_ZL12yy_state_buf(%rip), %rax
	leaq	4(%rax), %rdx
	movq	%rdx, _ZL12yy_state_ptr(%rip)
	movl	%esi, (%rax)
	movq	%rbx, %r8
	.p2align	4, 0x90
.LBB0_21:                               #   Parent Loop BB0_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_23 Depth 3
                                        #         Child Loop BB0_24 Depth 4
                                        #       Child Loop BB0_44 Depth 3
                                        #         Child Loop BB0_46 Depth 4
                                        #           Child Loop BB0_48 Depth 5
                                        #         Child Loop BB0_66 Depth 4
                                        #         Child Loop BB0_70 Depth 4
                                        #         Child Loop BB0_72 Depth 4
                                        #         Child Loop BB0_81 Depth 4
                                        #         Child Loop BB0_88 Depth 4
                                        #         Child Loop BB0_29 Depth 4
                                        #           Child Loop BB0_34 Depth 5
                                        #         Child Loop BB0_40 Depth 4
                                        #         Child Loop BB0_99 Depth 4
                                        #           Child Loop BB0_104 Depth 5
                                        #       Child Loop BB0_110 Depth 3
                                        #         Child Loop BB0_115 Depth 4
	movzbl	(%rbx), %eax
	leaq	_ZL5yy_ec(,%rax,4), %rax
	jmp	.LBB0_23
	.p2align	4, 0x90
.LBB0_22:                               #   in Loop: Header=BB0_23 Depth=3
	leaq	_ZL7yy_meta(,%rax,4), %rax
.LBB0_23:                               # %.sink.split
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_24 Depth 4
	movzbl	(%rax), %eax
	.p2align	4, 0x90
.LBB0_24:                               #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	%esi, %rcx
	movswq	_ZL7yy_base(%rcx,%rcx), %rsi
	leaq	(%rax,%rsi), %rsi
	movswl	_ZL6yy_chk(%rsi,%rsi), %edi
	cmpl	%ecx, %edi
	je	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_24 Depth=4
	movswl	_ZL6yy_def(%rcx,%rcx), %esi
	cmpl	$438, %esi              # imm = 0x1B6
	jl	.LBB0_24
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_21 Depth=2
	movl	%esi, %eax
	movswl	_ZL6yy_nxt(%rax,%rax), %esi
	leaq	4(%rdx), %rcx
	incq	%rbx
	cmpl	$437, %esi              # imm = 0x1B5
	movq	%rcx, _ZL12yy_state_ptr(%rip)
	movl	%esi, (%rdx)
	movq	%rcx, %rdx
	jne	.LBB0_21
.LBB0_44:                               # %.thread385
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_46 Depth 4
                                        #           Child Loop BB0_48 Depth 5
                                        #         Child Loop BB0_66 Depth 4
                                        #         Child Loop BB0_70 Depth 4
                                        #         Child Loop BB0_72 Depth 4
                                        #         Child Loop BB0_81 Depth 4
                                        #         Child Loop BB0_88 Depth 4
                                        #         Child Loop BB0_29 Depth 4
                                        #           Child Loop BB0_34 Depth 5
                                        #         Child Loop BB0_40 Depth 4
                                        #         Child Loop BB0_99 Depth 4
                                        #           Child Loop BB0_104 Depth 5
	leaq	-4(%rcx), %rdx
	movq	%rdx, _ZL12yy_state_ptr(%rip)
	movslq	-4(%rcx), %rsi
	jmp	.LBB0_46
	.p2align	4, 0x90
.LBB0_45:                               #   in Loop: Header=BB0_46 Depth=4
	decq	%rbx
	movq	%rdx, _ZL12yy_state_ptr(%rip)
	movslq	(%rdx), %rsi
.LBB0_46:                               #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_48 Depth 5
	movswl	_ZL9yy_accept(%rsi,%rsi), %edi
	movq	%rdx, %rax
	jmp	.LBB0_48
.LBB0_47:                               #   in Loop: Header=BB0_48 Depth=5
	addq	$4, %rdx
	incl	%edi
	.p2align	4, 0x90
.LBB0_48:                               #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        #         Parent Loop BB0_46 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	addq	$-4, %rdx
	testl	%edi, %edi
	je	.LBB0_45
# BB#49:                                #   in Loop: Header=BB0_48 Depth=5
	movslq	%esi, %rcx
	movswl	_ZL9yy_accept+2(%rcx,%rcx), %ecx
	cmpl	%ecx, %edi
	jge	.LBB0_45
# BB#50:                                #   in Loop: Header=BB0_48 Depth=5
	movslq	%edi, %rcx
	movswl	_ZL10yy_acclist(%rcx,%rcx), %ecx
	movl	%ecx, %r9d
	andl	$16384, %ecx            # imm = 0x4000
	movl	_ZL26yy_looking_for_trail_begin(%rip), %ebp
	orl	%ebp, %ecx
	je	.LBB0_52
# BB#51:                                #   in Loop: Header=BB0_48 Depth=5
	cmpl	%ebp, %r9d
	movl	%r9d, %ecx
	jne	.LBB0_47
	jmp	.LBB0_54
.LBB0_52:                               #   in Loop: Header=BB0_48 Depth=5
	movl	%r9d, %ecx
	testb	$32, %ch
	je	.LBB0_55
# BB#53:                                #   in Loop: Header=BB0_48 Depth=5
	andl	$-24577, %ecx           # imm = 0x9FFF
	orl	$16384, %ecx            # imm = 0x4000
	movl	%ecx, _ZL26yy_looking_for_trail_begin(%rip)
	movq	%rbx, _ZL13yy_full_match(%rip)
	movq	%rax, _ZL13yy_full_state(%rip)
	jmp	.LBB0_47
	.p2align	4, 0x90
.LBB0_54:                               #   in Loop: Header=BB0_44 Depth=3
	movl	%edi, _ZL5yy_lp(%rip)
	movl	$0, _ZL26yy_looking_for_trail_begin(%rip)
	andl	$-16385, %ecx           # imm = 0xBFFF
	jmp	.LBB0_56
	.p2align	4, 0x90
.LBB0_55:                               #   in Loop: Header=BB0_44 Depth=3
	movl	%edi, _ZL5yy_lp(%rip)
	movq	%rbx, _ZL13yy_full_match(%rip)
	movq	%rax, _ZL13yy_full_state(%rip)
.LBB0_56:                               #   in Loop: Header=BB0_44 Depth=3
	movl	%ecx, %r9d
	movq	%r8, yytext_ptr(%rip)
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, yyleng(%rip)
	movb	(%rbx), %al
	movb	%al, _ZL12yy_hold_char(%rip)
	movb	$0, (%rbx)
	movslq	yyleng(%rip), %rax
	cmpq	$8192, %rax             # imm = 0x2000
	jge	.LBB0_1234
# BB#57:                                #   in Loop: Header=BB0_44 Depth=3
	movq	yytext_ptr(%rip), %r15
	testl	%eax, %eax
	js	.LBB0_73
# BB#58:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_44 Depth=3
	leal	1(%rax), %ebp
	cmpl	$32, %ebp
	jae	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_44 Depth=3
	xorl	%esi, %esi
	jmp	.LBB0_68
	.p2align	4, 0x90
.LBB0_60:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_44 Depth=3
	movl	%ebp, %edi
	andl	$31, %edi
	movq	%rbp, %rsi
	subq	%rdi, %rsi
	je	.LBB0_64
# BB#61:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_44 Depth=3
	leaq	(%r15,%rbp), %rcx
	cmpq	%r14, %rcx
	jbe	.LBB0_65
# BB#62:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_44 Depth=3
	leaq	yytext(%rbp), %rcx
	cmpq	%rcx, %r15
	jae	.LBB0_65
.LBB0_64:                               #   in Loop: Header=BB0_44 Depth=3
	xorl	%esi, %esi
	jmp	.LBB0_68
.LBB0_65:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_44 Depth=3
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_66:                               # %vector.body
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	(%r15,%rdx), %xmm0
	movups	16(%r15,%rdx), %xmm1
	movaps	%xmm0, yytext(%rdx)
	movaps	%xmm1, yytext+16(%rdx)
	addq	$32, %rdx
	cmpq	%rdx, %rsi
	jne	.LBB0_66
# BB#67:                                # %middle.block
                                        #   in Loop: Header=BB0_44 Depth=3
	testl	%edi, %edi
	je	.LBB0_73
	.p2align	4, 0x90
.LBB0_68:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_44 Depth=3
	movl	%ebp, %edx
	subl	%esi, %edx
	leaq	-1(%rbp), %rdi
	subq	%rsi, %rdi
	andq	$7, %rdx
	je	.LBB0_71
# BB#69:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB0_44 Depth=3
	negq	%rdx
	.p2align	4, 0x90
.LBB0_70:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r15,%rsi), %ecx
	movb	%cl, yytext(%rsi)
	incq	%rsi
	incq	%rdx
	jne	.LBB0_70
.LBB0_71:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB0_44 Depth=3
	cmpq	$7, %rdi
	jb	.LBB0_73
	.p2align	4, 0x90
.LBB0_72:                               # %.lr.ph.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r15,%rsi), %ecx
	movb	%cl, yytext(%rsi)
	movzbl	1(%r15,%rsi), %ecx
	movb	%cl, yytext+1(%rsi)
	movzbl	2(%r15,%rsi), %ecx
	movb	%cl, yytext+2(%rsi)
	movzbl	3(%r15,%rsi), %ecx
	movb	%cl, yytext+3(%rsi)
	movzbl	4(%r15,%rsi), %ecx
	movb	%cl, yytext+4(%rsi)
	movzbl	5(%r15,%rsi), %ecx
	movb	%cl, yytext+5(%rsi)
	movzbl	6(%r15,%rsi), %ecx
	movb	%cl, yytext+6(%rsi)
	movzbl	7(%r15,%rsi), %ecx
	movb	%cl, yytext+7(%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %rbp
	jne	.LBB0_72
.LBB0_73:                               # %_ZL15yy_flex_strncpyPcPKci.exit
                                        #   in Loop: Header=BB0_44 Depth=3
	movq	%rbx, _ZL10yy_c_buf_p(%rip)
	movl	$94, %edx
	movl	%r9d, %ecx
	cmpl	$94, %ecx
	je	.LBB0_88
# BB#74:                                #   in Loop: Header=BB0_44 Depth=3
	movslq	%ecx, %rcx
	cmpl	$0, _ZL21yy_rule_can_match_eol(,%rcx,4)
	je	.LBB0_86
# BB#75:                                #   in Loop: Header=BB0_44 Depth=3
	testl	%eax, %eax
	jle	.LBB0_86
# BB#76:                                # %.lr.ph
                                        #   in Loop: Header=BB0_44 Depth=3
	testb	$1, %al
	jne	.LBB0_78
# BB#77:                                #   in Loop: Header=BB0_44 Depth=3
	xorl	%edx, %edx
	cmpl	$1, %eax
	jne	.LBB0_81
	jmp	.LBB0_86
.LBB0_78:                               #   in Loop: Header=BB0_44 Depth=3
	cmpb	$10, yytext(%rip)
	jne	.LBB0_80
# BB#79:                                #   in Loop: Header=BB0_44 Depth=3
	incl	yylineno(%rip)
.LBB0_80:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_44 Depth=3
	movl	$1, %edx
	cmpl	$1, %eax
	je	.LBB0_86
	.p2align	4, 0x90
.LBB0_81:                               #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$10, yytext(%rdx)
	jne	.LBB0_83
# BB#82:                                #   in Loop: Header=BB0_81 Depth=4
	incl	yylineno(%rip)
.LBB0_83:                               #   in Loop: Header=BB0_81 Depth=4
	cmpb	$10, yytext+1(%rdx)
	jne	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_81 Depth=4
	incl	yylineno(%rip)
.LBB0_85:                               #   in Loop: Header=BB0_81 Depth=4
	addq	$2, %rdx
	cmpq	%rax, %rdx
	jl	.LBB0_81
.LBB0_86:                               #   in Loop: Header=BB0_44 Depth=3
	movl	%r9d, %edx
	jmp	.LBB0_88
.LBB0_87:                               # %.thread391
                                        #   in Loop: Header=BB0_88 Depth=4
	movq	yytext_ptr(%rip), %r15
	movq	%r15, _ZL10yy_c_buf_p(%rip)
	movl	_ZL8yy_start(%rip), %eax
	leal	-1(%rax), %ecx
	shrl	$31, %ecx
	leal	-1(%rax,%rcx), %edx
	sarl	%edx
	addl	$95, %edx
	.p2align	4, 0x90
.LBB0_88:                               # %.preheader584
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	decl	%edx
	cmpl	$103, %edx
	ja	.LBB0_1235
# BB#89:                                # %.preheader584
                                        #   in Loop: Header=BB0_88 Depth=4
	jmpq	*.LJTI0_0(,%rdx,8)
.LBB0_90:                               #   in Loop: Header=BB0_88 Depth=4
	movzbl	_ZL12yy_hold_char(%rip), %eax
	movb	%al, (%rbx)
	movq	_ZL15yy_buffer_stack(%rip), %rsi
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rsi,%rdx,8), %rax
	cmpl	$0, 56(%rax)
	je	.LBB0_92
# BB#91:                                # %._crit_edge
                                        #   in Loop: Header=BB0_88 Depth=4
	movl	_ZL10yy_n_chars(%rip), %ecx
	jmp	.LBB0_93
.LBB0_92:                               #   in Loop: Header=BB0_88 Depth=4
	movl	28(%rax), %ecx
	movl	%ecx, _ZL10yy_n_chars(%rip)
	movq	yyin(%rip), %rdi
	movq	%rdi, (%rax)
	movq	(%rsi,%rdx,8), %rax
	movl	$1, 56(%rax)
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	(%rax,%rdx,8), %rax
.LBB0_93:                               #   in Loop: Header=BB0_88 Depth=4
	movslq	%ecx, %rcx
	addq	8(%rax), %rcx
	cmpq	%rcx, _ZL10yy_c_buf_p(%rip)
	jbe	.LBB0_27
# BB#94:                                #   in Loop: Header=BB0_88 Depth=4
	callq	_ZL18yy_get_next_bufferv
	cmpl	$1, %eax
	je	.LBB0_87
# BB#95:                                #   in Loop: Header=BB0_44 Depth=3
	testl	%eax, %eax
	je	.LBB0_108
# BB#96:                                #   in Loop: Header=BB0_44 Depth=3
	cmpl	$2, %eax
	jne	.LBB0_18
# BB#97:                                #   in Loop: Header=BB0_44 Depth=3
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	_ZL10yy_n_chars(%rip), %rbx
	addq	8(%rax), %rbx
	movq	%rbx, _ZL10yy_c_buf_p(%rip)
	movl	40(%rax), %edx
	addl	_ZL8yy_start(%rip), %edx
	movq	_ZL12yy_state_buf(%rip), %rax
	leaq	4(%rax), %rcx
	movq	%rcx, _ZL12yy_state_ptr(%rip)
	movl	%edx, (%rax)
	movq	yytext_ptr(%rip), %r8
	cmpq	%rbx, %r8
	jae	.LBB0_44
# BB#98:                                # %.lr.ph.i82.preheader
                                        #   in Loop: Header=BB0_44 Depth=3
	movq	%rbx, %r9
	subq	%r8, %r9
	movq	%rcx, %r10
	movq	%r8, %rdi
	jmp	.LBB0_99
.LBB0_43:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_44 Depth=3
	leaq	(%rcx,%r9,4), %rcx
	movq	%rcx, _ZL12yy_state_ptr(%rip)
	jmp	.LBB0_44
.LBB0_27:                               #   in Loop: Header=BB0_44 Depth=3
	subl	%r15d, %ebx
	movq	yytext_ptr(%rip), %r8
	shlq	$32, %rbx
	movq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, %rbx
	sarq	$32, %rbx
	addq	%r8, %rbx
	movq	%rbx, _ZL10yy_c_buf_p(%rip)
	movl	40(%rax), %eax
	addl	_ZL8yy_start(%rip), %eax
	movq	_ZL12yy_state_buf(%rip), %rsi
	leaq	4(%rsi), %rcx
	movq	%rcx, _ZL12yy_state_ptr(%rip)
	movl	%eax, (%rsi)
	testq	%rdx, %rdx
	jle	.LBB0_39
# BB#28:                                # %.lr.ph.i103.preheader
                                        #   in Loop: Header=BB0_44 Depth=3
	movq	%rbx, %r9
	subq	%r8, %r9
	movq	%rcx, %r10
	movq	%r8, %rdi
.LBB0_29:                               # %.lr.ph.i103
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_34 Depth 5
	movzbl	(%rdi), %edx
	testq	%rdx, %rdx
	je	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_29 Depth=4
	leaq	_ZL5yy_ec(,%rdx,4), %rbp
	jmp	.LBB0_32
.LBB0_31:                               #   in Loop: Header=BB0_29 Depth=4
	movl	$1, %ebp
	jmp	.LBB0_33
.LBB0_32:                               # %.outer.sink.split.i106
                                        #   in Loop: Header=BB0_29 Depth=4
	movzbl	(%rbp), %ebp
.LBB0_33:                               # %.outer.i109
                                        #   in Loop: Header=BB0_29 Depth=4
	movzbl	%bpl, %ebp
	.p2align	4, 0x90
.LBB0_34:                               #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        #         Parent Loop BB0_29 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cltq
	movswq	_ZL7yy_base(%rax,%rax), %rdx
	leaq	(%rbp,%rdx), %rdx
	movswl	_ZL6yy_chk(%rdx,%rdx), %esi
	cmpl	%eax, %esi
	je	.LBB0_37
# BB#35:                                #   in Loop: Header=BB0_34 Depth=5
	movswl	_ZL6yy_def(%rax,%rax), %eax
	cmpl	$438, %eax              # imm = 0x1B6
	jl	.LBB0_34
# BB#36:                                #   in Loop: Header=BB0_29 Depth=4
	leaq	_ZL7yy_meta(,%rbp,4), %rbp
	jmp	.LBB0_32
.LBB0_37:                               #   in Loop: Header=BB0_29 Depth=4
	movl	%edx, %eax
	movswl	_ZL6yy_nxt(%rax,%rax), %eax
	movl	%eax, (%r10)
	addq	$4, %r10
	incq	%rdi
	cmpq	%rbx, %rdi
	jne	.LBB0_29
# BB#38:                                # %._crit_edge.i111
                                        #   in Loop: Header=BB0_44 Depth=3
	leaq	(%rcx,%r9,4), %rcx
	movq	%rcx, _ZL12yy_state_ptr(%rip)
.LBB0_39:                               # %_ZL21yy_get_previous_statev.exit113
                                        #   in Loop: Header=BB0_44 Depth=3
	movslq	%eax, %rdx
	movswq	_ZL7yy_base(%rdx,%rdx), %rax
	movswl	_ZL6yy_chk+2(%rax,%rax), %esi
	cmpl	%edx, %esi
	je	.LBB0_41
.LBB0_40:                               # %.lr.ph.i99
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movswq	_ZL6yy_def(%rdx,%rdx), %rdx
	movswq	_ZL7yy_base(%rdx,%rdx), %rax
	cmpw	%dx, _ZL6yy_chk+2(%rax,%rax)
	jne	.LBB0_40
.LBB0_41:                               #   in Loop: Header=BB0_44 Depth=3
	incq	%rax
	movl	%eax, %edx
	movswl	_ZL6yy_nxt(%rdx,%rdx), %esi
	cmpl	$437, %esi              # imm = 0x1B5
	je	.LBB0_44
# BB#42:                                # %_ZL16yy_try_NUL_transi.exit
                                        #   in Loop: Header=BB0_44 Depth=3
	leaq	4(%rcx), %rdx
	movq	%rdx, _ZL12yy_state_ptr(%rip)
	movl	%esi, (%rcx)
	testl	%eax, %eax
	movq	%rdx, %rcx
	je	.LBB0_44
	jmp	.LBB0_19
.LBB0_102:                              # %.outer.sink.split.i
                                        #   in Loop: Header=BB0_99 Depth=4
	movzbl	(%rbp), %ebp
.LBB0_103:                              # %.outer.i
                                        #   in Loop: Header=BB0_99 Depth=4
	movzbl	%bpl, %ebp
.LBB0_104:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        #         Parent Loop BB0_99 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movslq	%edx, %rdx
	movswq	_ZL7yy_base(%rdx,%rdx), %rax
	leaq	(%rbp,%rax), %rax
	movswl	_ZL6yy_chk(%rax,%rax), %esi
	cmpl	%edx, %esi
	je	.LBB0_107
# BB#105:                               #   in Loop: Header=BB0_104 Depth=5
	movswl	_ZL6yy_def(%rdx,%rdx), %edx
	cmpl	$438, %edx              # imm = 0x1B6
	jl	.LBB0_104
# BB#106:                               #   in Loop: Header=BB0_99 Depth=4
	leaq	_ZL7yy_meta(,%rbp,4), %rbp
	jmp	.LBB0_102
.LBB0_99:                               # %.lr.ph.i82
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_44 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_104 Depth 5
	movzbl	(%rdi), %eax
	testq	%rax, %rax
	je	.LBB0_101
# BB#100:                               #   in Loop: Header=BB0_99 Depth=4
	leaq	_ZL5yy_ec(,%rax,4), %rbp
	jmp	.LBB0_102
.LBB0_101:                              #   in Loop: Header=BB0_99 Depth=4
	movl	$1, %ebp
	jmp	.LBB0_103
.LBB0_107:                              #   in Loop: Header=BB0_99 Depth=4
	movl	%eax, %eax
	movswl	_ZL6yy_nxt(%rax,%rax), %edx
	movl	%edx, (%r10)
	addq	$4, %r10
	incq	%rdi
	cmpq	%rbx, %rdi
	jne	.LBB0_99
	jmp	.LBB0_43
.LBB0_108:                              #   in Loop: Header=BB0_21 Depth=2
	subl	%r15d, %ebx
	movq	yytext_ptr(%rip), %r8
	shlq	$32, %rbx
	movq	%rbx, %rax
	addq	%r12, %rax
	movq	%rax, %rbx
	sarq	$32, %rbx
	addq	%r8, %rbx
	movq	%rbx, _ZL10yy_c_buf_p(%rip)
	movq	_ZL15yy_buffer_stack(%rip), %rcx
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movl	40(%rcx), %esi
	addl	_ZL8yy_start(%rip), %esi
	movq	_ZL12yy_state_buf(%rip), %rcx
	leaq	4(%rcx), %rdx
	movq	%rdx, _ZL12yy_state_ptr(%rip)
	movl	%esi, (%rcx)
	testq	%rax, %rax
	jle	.LBB0_21
# BB#109:                               # %.lr.ph.i85.preheader
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	%rbx, %r9
	subq	%r8, %r9
	movq	%rdx, %r10
	movq	%r8, %rdi
	jmp	.LBB0_110
.LBB0_19:                               #   in Loop: Header=BB0_21 Depth=2
	incq	%rbx
	movq	%rbx, _ZL10yy_c_buf_p(%rip)
	jmp	.LBB0_21
.LBB0_20:                               # %._crit_edge.i93
                                        #   in Loop: Header=BB0_21 Depth=2
	leaq	(%rdx,%r9,4), %rdx
	movq	%rdx, _ZL12yy_state_ptr(%rip)
	jmp	.LBB0_21
.LBB0_113:                              # %.outer.sink.split.i88
                                        #   in Loop: Header=BB0_110 Depth=3
	movzbl	(%rbp), %ebp
.LBB0_114:                              # %.outer.i91
                                        #   in Loop: Header=BB0_110 Depth=3
	movzbl	%bpl, %ebp
.LBB0_115:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_110 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	%esi, %rsi
	movswq	_ZL7yy_base(%rsi,%rsi), %rax
	leaq	(%rbp,%rax), %rax
	movswl	_ZL6yy_chk(%rax,%rax), %ecx
	cmpl	%esi, %ecx
	je	.LBB0_118
# BB#116:                               #   in Loop: Header=BB0_115 Depth=4
	movswl	_ZL6yy_def(%rsi,%rsi), %esi
	cmpl	$438, %esi              # imm = 0x1B6
	jl	.LBB0_115
# BB#117:                               #   in Loop: Header=BB0_110 Depth=3
	leaq	_ZL7yy_meta(,%rbp,4), %rbp
	jmp	.LBB0_113
.LBB0_110:                              # %.lr.ph.i85
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_115 Depth 4
	movzbl	(%rdi), %eax
	testq	%rax, %rax
	je	.LBB0_112
# BB#111:                               #   in Loop: Header=BB0_110 Depth=3
	leaq	_ZL5yy_ec(,%rax,4), %rbp
	jmp	.LBB0_113
.LBB0_112:                              #   in Loop: Header=BB0_110 Depth=3
	movl	$1, %ebp
	jmp	.LBB0_114
.LBB0_118:                              #   in Loop: Header=BB0_110 Depth=3
	movl	%eax, %eax
	movswl	_ZL6yy_nxt(%rax,%rax), %esi
	movl	%esi, (%r10)
	addq	$4, %r10
	incq	%rdi
	cmpq	%rbx, %rdi
	jne	.LBB0_110
	jmp	.LBB0_20
.LBB0_119:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_121
# BB#120:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_121:                              # %.preheader587.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_123
	.p2align	4, 0x90
.LBB0_122:                              #   in Loop: Header=BB0_123 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_123:                              # %.preheader587
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_127
# BB#124:                               # %.preheader587
                                        #   in Loop: Header=BB0_123 Depth=2
	testb	%dl, %dl
	je	.LBB0_18
# BB#125:                               #   in Loop: Header=BB0_123 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_128
# BB#126:                               #   in Loop: Header=BB0_123 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_122
.LBB0_127:                              #   in Loop: Header=BB0_123 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_129
.LBB0_128:                              #   in Loop: Header=BB0_123 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_129:                              #   in Loop: Header=BB0_123 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_122
.LBB0_130:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_132
# BB#131:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_132:                              # %.preheader589.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_134
	.p2align	4, 0x90
.LBB0_133:                              #   in Loop: Header=BB0_134 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_134:                              # %.preheader589
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_138
# BB#135:                               # %.preheader589
                                        #   in Loop: Header=BB0_134 Depth=2
	testb	%dl, %dl
	je	.LBB0_321
# BB#136:                               #   in Loop: Header=BB0_134 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_139
# BB#137:                               #   in Loop: Header=BB0_134 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_133
.LBB0_138:                              #   in Loop: Header=BB0_134 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_140
.LBB0_139:                              #   in Loop: Header=BB0_134 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_140:                              #   in Loop: Header=BB0_134 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_133
.LBB0_141:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_143
# BB#142:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_143:                              # %.preheader590.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_145
	.p2align	4, 0x90
.LBB0_144:                              #   in Loop: Header=BB0_145 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_145:                              # %.preheader590
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_149
# BB#146:                               # %.preheader590
                                        #   in Loop: Header=BB0_145 Depth=2
	testb	%dl, %dl
	je	.LBB0_18
# BB#147:                               #   in Loop: Header=BB0_145 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_150
# BB#148:                               #   in Loop: Header=BB0_145 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_144
.LBB0_149:                              #   in Loop: Header=BB0_145 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_151
.LBB0_150:                              #   in Loop: Header=BB0_145 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_151:                              #   in Loop: Header=BB0_145 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_144
.LBB0_152:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_154
# BB#153:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_154:                              # %.preheader592.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_156
	.p2align	4, 0x90
.LBB0_155:                              #   in Loop: Header=BB0_156 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_156:                              # %.preheader592
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_160
# BB#157:                               # %.preheader592
                                        #   in Loop: Header=BB0_156 Depth=2
	testb	%dl, %dl
	je	.LBB0_322
# BB#158:                               #   in Loop: Header=BB0_156 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_161
# BB#159:                               #   in Loop: Header=BB0_156 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_155
.LBB0_160:                              #   in Loop: Header=BB0_156 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_162
.LBB0_161:                              #   in Loop: Header=BB0_156 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_162:                              #   in Loop: Header=BB0_156 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_155
.LBB0_163:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_165
# BB#164:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_165:                              # %.preheader593.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_167
	.p2align	4, 0x90
.LBB0_166:                              #   in Loop: Header=BB0_167 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_167:                              # %.preheader593
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_171
# BB#168:                               # %.preheader593
                                        #   in Loop: Header=BB0_167 Depth=2
	testb	%dl, %dl
	je	.LBB0_325
# BB#169:                               #   in Loop: Header=BB0_167 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_172
# BB#170:                               #   in Loop: Header=BB0_167 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_166
.LBB0_171:                              #   in Loop: Header=BB0_167 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_173
.LBB0_172:                              #   in Loop: Header=BB0_167 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_173:                              #   in Loop: Header=BB0_167 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_166
.LBB0_174:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_176
# BB#175:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_176:                              # %.preheader594.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_178
	.p2align	4, 0x90
.LBB0_177:                              #   in Loop: Header=BB0_178 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_178:                              # %.preheader594
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_182
# BB#179:                               # %.preheader594
                                        #   in Loop: Header=BB0_178 Depth=2
	testb	%dl, %dl
	je	.LBB0_327
# BB#180:                               #   in Loop: Header=BB0_178 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_183
# BB#181:                               #   in Loop: Header=BB0_178 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_177
.LBB0_182:                              #   in Loop: Header=BB0_178 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_184
.LBB0_183:                              #   in Loop: Header=BB0_178 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_184:                              #   in Loop: Header=BB0_178 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_177
.LBB0_185:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_187
# BB#186:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_187:                              # %.preheader595.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_189
	.p2align	4, 0x90
.LBB0_188:                              #   in Loop: Header=BB0_189 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_189:                              # %.preheader595
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_193
# BB#190:                               # %.preheader595
                                        #   in Loop: Header=BB0_189 Depth=2
	testb	%dl, %dl
	je	.LBB0_196
# BB#191:                               #   in Loop: Header=BB0_189 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_194
# BB#192:                               #   in Loop: Header=BB0_189 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_188
.LBB0_193:                              #   in Loop: Header=BB0_189 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_195
.LBB0_194:                              #   in Loop: Header=BB0_189 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_195:                              #   in Loop: Header=BB0_189 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_188
	.p2align	4, 0x90
.LBB0_196:                              # %_ZN12_GLOBAL__N_15countEb.exit371
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	_ZL7yyinputv
	cmpl	$10, %eax
	jne	.LBB0_196
# BB#197:                               # %_ZN12_GLOBAL__N_116eat_line_commentEv.exit
                                        #   in Loop: Header=BB0_18 Depth=1
	incl	pg_lineno(%rip)
	movl	$0, pg_column(%rip)
	incl	pg_charpos(%rip)
	jmp	.LBB0_18
.LBB0_198:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_200
# BB#199:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_200:                              # %.preheader596.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_202
	.p2align	4, 0x90
.LBB0_201:                              #   in Loop: Header=BB0_202 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_202:                              # %.preheader596
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_206
# BB#203:                               # %.preheader596
                                        #   in Loop: Header=BB0_202 Depth=2
	testb	%dl, %dl
	je	.LBB0_18
# BB#204:                               #   in Loop: Header=BB0_202 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_207
# BB#205:                               #   in Loop: Header=BB0_202 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_201
.LBB0_206:                              #   in Loop: Header=BB0_202 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_208
.LBB0_207:                              #   in Loop: Header=BB0_202 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_208:                              #   in Loop: Header=BB0_202 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_201
.LBB0_209:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_211
# BB#210:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_211:                              # %.preheader598.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_213
	.p2align	4, 0x90
.LBB0_212:                              #   in Loop: Header=BB0_213 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_213:                              # %.preheader598
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_217
# BB#214:                               # %.preheader598
                                        #   in Loop: Header=BB0_213 Depth=2
	testb	%dl, %dl
	je	.LBB0_18
# BB#215:                               #   in Loop: Header=BB0_213 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_218
# BB#216:                               #   in Loop: Header=BB0_213 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_212
.LBB0_217:                              #   in Loop: Header=BB0_213 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_219
.LBB0_218:                              #   in Loop: Header=BB0_213 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_219:                              #   in Loop: Header=BB0_213 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_212
.LBB0_220:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_222
# BB#221:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_222:                              # %.preheader600.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_224
	.p2align	4, 0x90
.LBB0_223:                              #   in Loop: Header=BB0_224 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_224:                              # %.preheader600
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_228
# BB#225:                               # %.preheader600
                                        #   in Loop: Header=BB0_224 Depth=2
	testb	%dl, %dl
	je	.LBB0_328
# BB#226:                               #   in Loop: Header=BB0_224 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_229
# BB#227:                               #   in Loop: Header=BB0_224 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_223
.LBB0_228:                              #   in Loop: Header=BB0_224 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_230
.LBB0_229:                              #   in Loop: Header=BB0_224 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_230:                              #   in Loop: Header=BB0_224 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_223
.LBB0_231:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_233
# BB#232:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_233:                              # %.preheader601.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_235
	.p2align	4, 0x90
.LBB0_234:                              #   in Loop: Header=BB0_235 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_235:                              # %.preheader601
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_239
# BB#236:                               # %.preheader601
                                        #   in Loop: Header=BB0_235 Depth=2
	testb	%dl, %dl
	je	.LBB0_329
# BB#237:                               #   in Loop: Header=BB0_235 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_240
# BB#238:                               #   in Loop: Header=BB0_235 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_234
.LBB0_239:                              #   in Loop: Header=BB0_235 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_241
.LBB0_240:                              #   in Loop: Header=BB0_235 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_241:                              #   in Loop: Header=BB0_235 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_234
.LBB0_242:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_244
# BB#243:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_244:                              # %.preheader602.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_246
	.p2align	4, 0x90
.LBB0_245:                              #   in Loop: Header=BB0_246 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_246:                              # %.preheader602
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_250
# BB#247:                               # %.preheader602
                                        #   in Loop: Header=BB0_246 Depth=2
	testb	%dl, %dl
	je	.LBB0_332
# BB#248:                               #   in Loop: Header=BB0_246 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_251
# BB#249:                               #   in Loop: Header=BB0_246 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_245
.LBB0_250:                              #   in Loop: Header=BB0_246 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_252
.LBB0_251:                              #   in Loop: Header=BB0_246 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_252:                              #   in Loop: Header=BB0_246 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_245
.LBB0_253:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_255
# BB#254:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_255:                              # %.preheader603.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_257
	.p2align	4, 0x90
.LBB0_256:                              #   in Loop: Header=BB0_257 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_257:                              # %.preheader603
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_261
# BB#258:                               # %.preheader603
                                        #   in Loop: Header=BB0_257 Depth=2
	testb	%dl, %dl
	je	.LBB0_333
# BB#259:                               #   in Loop: Header=BB0_257 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_262
# BB#260:                               #   in Loop: Header=BB0_257 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_256
.LBB0_261:                              #   in Loop: Header=BB0_257 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_263
.LBB0_262:                              #   in Loop: Header=BB0_257 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_263:                              #   in Loop: Header=BB0_257 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_256
.LBB0_264:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_266
# BB#265:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_266:                              # %.preheader604.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_268
	.p2align	4, 0x90
.LBB0_267:                              #   in Loop: Header=BB0_268 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_268:                              # %.preheader604
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_272
# BB#269:                               # %.preheader604
                                        #   in Loop: Header=BB0_268 Depth=2
	testb	%dl, %dl
	je	.LBB0_334
# BB#270:                               #   in Loop: Header=BB0_268 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_273
# BB#271:                               #   in Loop: Header=BB0_268 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_267
.LBB0_272:                              #   in Loop: Header=BB0_268 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_274
.LBB0_273:                              #   in Loop: Header=BB0_268 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_274:                              #   in Loop: Header=BB0_268 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_267
.LBB0_275:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_277
# BB#276:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_277:                              # %.preheader605.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_279
	.p2align	4, 0x90
.LBB0_278:                              #   in Loop: Header=BB0_279 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_279:                              # %.preheader605
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_283
# BB#280:                               # %.preheader605
                                        #   in Loop: Header=BB0_279 Depth=2
	testb	%dl, %dl
	je	.LBB0_335
# BB#281:                               #   in Loop: Header=BB0_279 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_284
# BB#282:                               #   in Loop: Header=BB0_279 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_278
.LBB0_283:                              #   in Loop: Header=BB0_279 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_285
.LBB0_284:                              #   in Loop: Header=BB0_279 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_285:                              #   in Loop: Header=BB0_279 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_278
.LBB0_286:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_288
# BB#287:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_288:                              # %.preheader606.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_290
	.p2align	4, 0x90
.LBB0_289:                              #   in Loop: Header=BB0_290 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_290:                              # %.preheader606
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_294
# BB#291:                               # %.preheader606
                                        #   in Loop: Header=BB0_290 Depth=2
	testb	%dl, %dl
	je	.LBB0_336
# BB#292:                               #   in Loop: Header=BB0_290 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_295
# BB#293:                               #   in Loop: Header=BB0_290 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_289
.LBB0_294:                              #   in Loop: Header=BB0_290 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_296
.LBB0_295:                              #   in Loop: Header=BB0_290 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_296:                              #   in Loop: Header=BB0_290 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_289
.LBB0_297:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_299
# BB#298:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_299:                              # %.preheader607.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_301
	.p2align	4, 0x90
.LBB0_300:                              #   in Loop: Header=BB0_301 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_301:                              # %.preheader607
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_305
# BB#302:                               # %.preheader607
                                        #   in Loop: Header=BB0_301 Depth=2
	testb	%dl, %dl
	je	.LBB0_337
# BB#303:                               #   in Loop: Header=BB0_301 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_306
# BB#304:                               #   in Loop: Header=BB0_301 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_300
.LBB0_305:                              #   in Loop: Header=BB0_301 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_307
.LBB0_306:                              #   in Loop: Header=BB0_301 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_307:                              #   in Loop: Header=BB0_301 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_300
.LBB0_308:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_310
# BB#309:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_310:                              # %.preheader608.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$yytext, %eax
	jmp	.LBB0_312
	.p2align	4, 0x90
.LBB0_311:                              #   in Loop: Header=BB0_312 Depth=2
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_312:                              # %.preheader608
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_316
# BB#313:                               # %.preheader608
                                        #   in Loop: Header=BB0_312 Depth=2
	testb	%dl, %dl
	je	.LBB0_338
# BB#314:                               #   in Loop: Header=BB0_312 Depth=2
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_317
# BB#315:                               #   in Loop: Header=BB0_312 Depth=2
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_311
.LBB0_316:                              #   in Loop: Header=BB0_312 Depth=2
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_318
.LBB0_317:                              #   in Loop: Header=BB0_312 Depth=2
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_318:                              #   in Loop: Header=BB0_312 Depth=2
	movl	$1, %ecx
	jmp	.LBB0_311
.LBB0_319:                              #   in Loop: Header=BB0_18 Depth=1
	movslq	yyleng(%rip), %rsi
	testq	%rsi, %rsi
	jle	.LBB0_17
# BB#320:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%eax, %eax
	cmpb	$10, yytext-1(%rsi)
	sete	%al
	movq	_ZL15yy_buffer_stack(%rip), %rcx
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movl	%eax, 40(%rcx)
	jmp	.LBB0_17
.LBB0_321:                              # %_ZN12_GLOBAL__N_15countEb.exit224
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$19, _ZL8yy_start(%rip)
	jmp	.LBB0_18
.LBB0_322:                              # %_ZN12_GLOBAL__N_15countEb.exit365
                                        #   in Loop: Header=BB0_18 Depth=1
	movb	yytext(%rip), %r15b
	movq	_ZN12_GLOBAL__N_18cincludeB5cxx11E+8(%rip), %rbx
	leaq	1(%rbx), %rbp
	movq	_ZN12_GLOBAL__N_18cincludeB5cxx11E(%rip), %rax
	movl	$_ZN12_GLOBAL__N_18cincludeB5cxx11E+16, %ecx
	cmpq	%rcx, %rax
	movl	$15, %ecx
	cmovneq	_ZN12_GLOBAL__N_18cincludeB5cxx11E+16(%rip), %rcx
	cmpq	%rcx, %rbp
	jbe	.LBB0_324
# BB#323:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$_ZN12_GLOBAL__N_18cincludeB5cxx11E, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rbx, %rsi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm
	movq	_ZN12_GLOBAL__N_18cincludeB5cxx11E(%rip), %rax
.LBB0_324:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLEc.exit
                                        #   in Loop: Header=BB0_18 Depth=1
	movb	%r15b, (%rax,%rbx)
	movq	%rbp, _ZN12_GLOBAL__N_18cincludeB5cxx11E+8(%rip)
	movq	_ZN12_GLOBAL__N_18cincludeB5cxx11E(%rip), %rax
	movb	$0, 1(%rax,%rbx)
	jmp	.LBB0_18
.LBB0_325:                              # %_ZN12_GLOBAL__N_15countEb.exit368
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	pg_filename(%rip), %rdi
	movl	pg_lineno(%rip), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.2, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	movl	$yytext, %edi
	callq	strlen
	movabsq	$9223372036854775807, %rcx # imm = 0x7FFFFFFFFFFFFFFF
	subq	_ZN12_GLOBAL__N_18cincludeB5cxx11E+8(%rip), %rcx
	cmpq	%rax, %rcx
	jb	.LBB0_1236
# BB#326:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLEPKc.exit
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$_ZN12_GLOBAL__N_18cincludeB5cxx11E, %edi
	movl	$yytext, %esi
	movq	%rax, %rdx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm
	jmp	.LBB0_18
.LBB0_327:                              # %_ZN12_GLOBAL__N_15countEb.exit384
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	pg_filename(%rip), %rdi
	callq	_ZN12_GLOBAL__N_111eat_commentEPN2kc20impl_casestring__StrE
	jmp	.LBB0_18
.LBB0_328:                              # %_ZN12_GLOBAL__N_15countEb.exit155
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$3, _ZL8yy_start(%rip)
	jmp	.LBB0_18
.LBB0_329:                              # %_ZN12_GLOBAL__N_15countEb.exit152
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$g_options+80, %edi
	movl	$.L.str, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
	testl	%eax, %eax
	je	.LBB0_339
# BB#330:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$g_options+80, %edi
	movl	$.L.str.17, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
	testl	%eax, %eax
	je	.LBB0_339
# BB#331:                               #   in Loop: Header=BB0_18 Depth=1
	movq	pg_filename(%rip), %rdi
	movl	pg_lineno(%rip), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movq	g_options+80(%rip), %rsi
	movl	$.L.str.18, %edi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB0_18
.LBB0_332:                              # %_ZN12_GLOBAL__N_15countEb.exit137
                                        #   in Loop: Header=BB0_18 Depth=1
	movb	$1, g_options+66(%rip)
	jmp	.LBB0_18
.LBB0_333:                              # %_ZN12_GLOBAL__N_15countEb.exit134
                                        #   in Loop: Header=BB0_18 Depth=1
	movb	$1, g_options+67(%rip)
	jmp	.LBB0_18
.LBB0_334:                              # %_ZN12_GLOBAL__N_15countEb.exit131
                                        #   in Loop: Header=BB0_18 Depth=1
	movb	$1, g_options+68(%rip)
	jmp	.LBB0_18
.LBB0_335:                              # %_ZN12_GLOBAL__N_15countEb.exit128
                                        #   in Loop: Header=BB0_18 Depth=1
	movb	$1, g_options+69(%rip)
	jmp	.LBB0_18
.LBB0_336:                              # %_ZN12_GLOBAL__N_15countEb.exit125
                                        #   in Loop: Header=BB0_18 Depth=1
	movb	$1, g_options+70(%rip)
	jmp	.LBB0_18
.LBB0_337:                              # %_ZN12_GLOBAL__N_15countEb.exit122
                                        #   in Loop: Header=BB0_18 Depth=1
	movb	$1, g_options+72(%rip)
	jmp	.LBB0_18
.LBB0_338:                              # %_ZN12_GLOBAL__N_15countEb.exit119
                                        #   in Loop: Header=BB0_18 Depth=1
	movw	$257, g_options+72(%rip) # imm = 0x101
	jmp	.LBB0_18
.LBB0_339:                              #   in Loop: Header=BB0_18 Depth=1
	movq	g_options+88(%rip), %rdx
	movl	$g_options+80, %edi
	xorl	%esi, %esi
	movl	$.L.str.17, %ecx
	movl	$9, %r8d
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm
	jmp	.LBB0_18
.LBB0_340:                              # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1389
	xorl	%eax, %eax
	jmp	.LBB0_1227
.LBB0_341:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_343
# BB#342:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_343:                              # %.preheader.preheader
	movl	$yytext, %eax
	jmp	.LBB0_345
	.p2align	4, 0x90
.LBB0_344:                              #   in Loop: Header=BB0_345 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_345:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_349
# BB#346:                               # %.preheader
                                        #   in Loop: Header=BB0_345 Depth=1
	testb	%dl, %dl
	je	.LBB0_1159
# BB#347:                               #   in Loop: Header=BB0_345 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_350
# BB#348:                               #   in Loop: Header=BB0_345 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_344
.LBB0_349:                              #   in Loop: Header=BB0_345 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_351
.LBB0_350:                              #   in Loop: Header=BB0_345 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_351:                              #   in Loop: Header=BB0_345 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_344
.LBB0_352:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_354
# BB#353:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_354:                              # %.preheader483.preheader
	movl	$yytext, %eax
	jmp	.LBB0_356
	.p2align	4, 0x90
.LBB0_355:                              #   in Loop: Header=BB0_356 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_356:                              # %.preheader483
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_360
# BB#357:                               # %.preheader483
                                        #   in Loop: Header=BB0_356 Depth=1
	testb	%dl, %dl
	je	.LBB0_1160
# BB#358:                               #   in Loop: Header=BB0_356 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_361
# BB#359:                               #   in Loop: Header=BB0_356 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_355
.LBB0_360:                              #   in Loop: Header=BB0_356 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_362
.LBB0_361:                              #   in Loop: Header=BB0_356 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_362:                              #   in Loop: Header=BB0_356 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_355
.LBB0_363:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_365
# BB#364:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_365:                              # %.preheader485.preheader
	movl	$yytext, %eax
	jmp	.LBB0_367
	.p2align	4, 0x90
.LBB0_366:                              #   in Loop: Header=BB0_367 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_367:                              # %.preheader485
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_371
# BB#368:                               # %.preheader485
                                        #   in Loop: Header=BB0_367 Depth=1
	testb	%dl, %dl
	je	.LBB0_1161
# BB#369:                               #   in Loop: Header=BB0_367 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_372
# BB#370:                               #   in Loop: Header=BB0_367 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_366
.LBB0_371:                              #   in Loop: Header=BB0_367 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_373
.LBB0_372:                              #   in Loop: Header=BB0_367 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_373:                              #   in Loop: Header=BB0_367 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_366
.LBB0_374:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_376
# BB#375:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_376:                              # %.preheader487.preheader
	movl	$yytext, %eax
	jmp	.LBB0_378
	.p2align	4, 0x90
.LBB0_377:                              #   in Loop: Header=BB0_378 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_378:                              # %.preheader487
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_382
# BB#379:                               # %.preheader487
                                        #   in Loop: Header=BB0_378 Depth=1
	testb	%dl, %dl
	je	.LBB0_1162
# BB#380:                               #   in Loop: Header=BB0_378 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_383
# BB#381:                               #   in Loop: Header=BB0_378 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_377
.LBB0_382:                              #   in Loop: Header=BB0_378 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_384
.LBB0_383:                              #   in Loop: Header=BB0_378 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_384:                              #   in Loop: Header=BB0_378 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_377
.LBB0_385:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_387
# BB#386:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_387:                              # %.preheader489.preheader
	movl	$yytext, %eax
	jmp	.LBB0_389
	.p2align	4, 0x90
.LBB0_388:                              #   in Loop: Header=BB0_389 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_389:                              # %.preheader489
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_393
# BB#390:                               # %.preheader489
                                        #   in Loop: Header=BB0_389 Depth=1
	testb	%dl, %dl
	je	.LBB0_1163
# BB#391:                               #   in Loop: Header=BB0_389 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_394
# BB#392:                               #   in Loop: Header=BB0_389 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_388
.LBB0_393:                              #   in Loop: Header=BB0_389 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_395
.LBB0_394:                              #   in Loop: Header=BB0_389 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_395:                              #   in Loop: Header=BB0_389 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_388
.LBB0_396:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_398
# BB#397:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_398:                              # %.preheader491.preheader
	movl	$yytext, %eax
	jmp	.LBB0_400
	.p2align	4, 0x90
.LBB0_399:                              #   in Loop: Header=BB0_400 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_400:                              # %.preheader491
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_404
# BB#401:                               # %.preheader491
                                        #   in Loop: Header=BB0_400 Depth=1
	testb	%dl, %dl
	je	.LBB0_1164
# BB#402:                               #   in Loop: Header=BB0_400 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_405
# BB#403:                               #   in Loop: Header=BB0_400 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_399
.LBB0_404:                              #   in Loop: Header=BB0_400 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_406
.LBB0_405:                              #   in Loop: Header=BB0_400 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_406:                              #   in Loop: Header=BB0_400 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_399
.LBB0_407:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_409
# BB#408:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_409:                              # %.preheader493.preheader
	movl	$yytext, %eax
	jmp	.LBB0_411
	.p2align	4, 0x90
.LBB0_410:                              #   in Loop: Header=BB0_411 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_411:                              # %.preheader493
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_415
# BB#412:                               # %.preheader493
                                        #   in Loop: Header=BB0_411 Depth=1
	testb	%dl, %dl
	je	.LBB0_1165
# BB#413:                               #   in Loop: Header=BB0_411 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_416
# BB#414:                               #   in Loop: Header=BB0_411 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_410
.LBB0_415:                              #   in Loop: Header=BB0_411 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_417
.LBB0_416:                              #   in Loop: Header=BB0_411 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_417:                              #   in Loop: Header=BB0_411 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_410
.LBB0_418:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_420
# BB#419:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_420:                              # %.preheader494.preheader
	movl	$yytext, %eax
	jmp	.LBB0_422
	.p2align	4, 0x90
.LBB0_421:                              #   in Loop: Header=BB0_422 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_422:                              # %.preheader494
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_426
# BB#423:                               # %.preheader494
                                        #   in Loop: Header=BB0_422 Depth=1
	testb	%dl, %dl
	je	.LBB0_1194
# BB#424:                               #   in Loop: Header=BB0_422 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_427
# BB#425:                               #   in Loop: Header=BB0_422 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_421
.LBB0_426:                              #   in Loop: Header=BB0_422 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_428
.LBB0_427:                              #   in Loop: Header=BB0_422 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_428:                              #   in Loop: Header=BB0_422 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_421
.LBB0_429:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_431
# BB#430:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_431:                              # %.preheader495.preheader
	movl	$yytext, %eax
	jmp	.LBB0_433
	.p2align	4, 0x90
.LBB0_432:                              #   in Loop: Header=BB0_433 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_433:                              # %.preheader495
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_437
# BB#434:                               # %.preheader495
                                        #   in Loop: Header=BB0_433 Depth=1
	testb	%dl, %dl
	je	.LBB0_1166
# BB#435:                               #   in Loop: Header=BB0_433 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_438
# BB#436:                               #   in Loop: Header=BB0_433 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_432
.LBB0_437:                              #   in Loop: Header=BB0_433 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_439
.LBB0_438:                              #   in Loop: Header=BB0_433 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_439:                              #   in Loop: Header=BB0_433 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_432
.LBB0_440:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_442
# BB#441:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_442:                              # %.preheader496.preheader
	movl	$yytext, %eax
	jmp	.LBB0_444
	.p2align	4, 0x90
.LBB0_443:                              #   in Loop: Header=BB0_444 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_444:                              # %.preheader496
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_448
# BB#445:                               # %.preheader496
                                        #   in Loop: Header=BB0_444 Depth=1
	testb	%dl, %dl
	je	.LBB0_1167
# BB#446:                               #   in Loop: Header=BB0_444 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_449
# BB#447:                               #   in Loop: Header=BB0_444 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_443
.LBB0_448:                              #   in Loop: Header=BB0_444 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_450
.LBB0_449:                              #   in Loop: Header=BB0_444 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_450:                              #   in Loop: Header=BB0_444 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_443
.LBB0_451:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_453
# BB#452:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_453:                              # %.preheader497.preheader
	movl	$yytext, %eax
	jmp	.LBB0_455
	.p2align	4, 0x90
.LBB0_454:                              #   in Loop: Header=BB0_455 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_455:                              # %.preheader497
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_459
# BB#456:                               # %.preheader497
                                        #   in Loop: Header=BB0_455 Depth=1
	testb	%dl, %dl
	je	.LBB0_1168
# BB#457:                               #   in Loop: Header=BB0_455 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_460
# BB#458:                               #   in Loop: Header=BB0_455 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_454
.LBB0_459:                              #   in Loop: Header=BB0_455 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_461
.LBB0_460:                              #   in Loop: Header=BB0_455 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_461:                              #   in Loop: Header=BB0_455 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_454
.LBB0_462:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_464
# BB#463:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_464:                              # %.preheader498.preheader
	movl	$yytext, %eax
	jmp	.LBB0_466
	.p2align	4, 0x90
.LBB0_465:                              #   in Loop: Header=BB0_466 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_466:                              # %.preheader498
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_470
# BB#467:                               # %.preheader498
                                        #   in Loop: Header=BB0_466 Depth=1
	testb	%dl, %dl
	je	.LBB0_473
# BB#468:                               #   in Loop: Header=BB0_466 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_471
# BB#469:                               #   in Loop: Header=BB0_466 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_465
.LBB0_470:                              #   in Loop: Header=BB0_466 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_472
.LBB0_471:                              #   in Loop: Header=BB0_466 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_472:                              #   in Loop: Header=BB0_466 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_465
	.p2align	4, 0x90
.LBB0_473:                              # %_ZN12_GLOBAL__N_15countEb.exit381
                                        # =>This Inner Loop Header: Depth=1
	callq	_ZL7yyinputv
	cmpl	$10, %eax
	jne	.LBB0_473
# BB#474:                               # %_ZN12_GLOBAL__N_116eat_line_commentEv.exit372
	incl	pg_lineno(%rip)
	movl	$0, pg_column(%rip)
	incl	pg_charpos(%rip)
	movl	$1, %edi
	jmp	.LBB0_1169
.LBB0_475:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_477
# BB#476:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_477:                              # %.preheader499.preheader
	movl	$yytext, %eax
	jmp	.LBB0_479
	.p2align	4, 0x90
.LBB0_478:                              #   in Loop: Header=BB0_479 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_479:                              # %.preheader499
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_483
# BB#480:                               # %.preheader499
                                        #   in Loop: Header=BB0_479 Depth=1
	testb	%dl, %dl
	je	.LBB0_1170
# BB#481:                               #   in Loop: Header=BB0_479 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_484
# BB#482:                               #   in Loop: Header=BB0_479 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_478
.LBB0_483:                              #   in Loop: Header=BB0_479 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_485
.LBB0_484:                              #   in Loop: Header=BB0_479 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_485:                              #   in Loop: Header=BB0_479 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_478
.LBB0_486:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_488
# BB#487:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_488:                              # %.preheader501.preheader
	movl	$yytext, %eax
	jmp	.LBB0_490
	.p2align	4, 0x90
.LBB0_489:                              #   in Loop: Header=BB0_490 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_490:                              # %.preheader501
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_494
# BB#491:                               # %.preheader501
                                        #   in Loop: Header=BB0_490 Depth=1
	testb	%dl, %dl
	je	.LBB0_1171
# BB#492:                               #   in Loop: Header=BB0_490 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_495
# BB#493:                               #   in Loop: Header=BB0_490 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_489
.LBB0_494:                              #   in Loop: Header=BB0_490 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_496
.LBB0_495:                              #   in Loop: Header=BB0_490 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_496:                              #   in Loop: Header=BB0_490 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_489
.LBB0_497:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_499
# BB#498:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_499:                              # %.preheader503.preheader
	movl	$yytext, %eax
	jmp	.LBB0_501
	.p2align	4, 0x90
.LBB0_500:                              #   in Loop: Header=BB0_501 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_501:                              # %.preheader503
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_505
# BB#502:                               # %.preheader503
                                        #   in Loop: Header=BB0_501 Depth=1
	testb	%dl, %dl
	je	.LBB0_1172
# BB#503:                               #   in Loop: Header=BB0_501 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_506
# BB#504:                               #   in Loop: Header=BB0_501 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_500
.LBB0_505:                              #   in Loop: Header=BB0_501 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_507
.LBB0_506:                              #   in Loop: Header=BB0_501 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_507:                              #   in Loop: Header=BB0_501 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_500
.LBB0_508:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_510
# BB#509:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_510:                              # %.preheader505.preheader
	movl	$yytext, %eax
	jmp	.LBB0_512
	.p2align	4, 0x90
.LBB0_511:                              #   in Loop: Header=BB0_512 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_512:                              # %.preheader505
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_516
# BB#513:                               # %.preheader505
                                        #   in Loop: Header=BB0_512 Depth=1
	testb	%dl, %dl
	je	.LBB0_1173
# BB#514:                               #   in Loop: Header=BB0_512 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_517
# BB#515:                               #   in Loop: Header=BB0_512 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_511
.LBB0_516:                              #   in Loop: Header=BB0_512 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_518
.LBB0_517:                              #   in Loop: Header=BB0_512 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_518:                              #   in Loop: Header=BB0_512 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_511
.LBB0_519:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_521
# BB#520:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_521:                              # %.preheader506.preheader
	movl	$yytext, %eax
	jmp	.LBB0_523
	.p2align	4, 0x90
.LBB0_522:                              #   in Loop: Header=BB0_523 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_523:                              # %.preheader506
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_527
# BB#524:                               # %.preheader506
                                        #   in Loop: Header=BB0_523 Depth=1
	testb	%dl, %dl
	je	.LBB0_1174
# BB#525:                               #   in Loop: Header=BB0_523 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_528
# BB#526:                               #   in Loop: Header=BB0_523 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_522
.LBB0_527:                              #   in Loop: Header=BB0_523 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_529
.LBB0_528:                              #   in Loop: Header=BB0_523 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_529:                              #   in Loop: Header=BB0_523 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_522
.LBB0_530:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_532
# BB#531:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_532:                              # %.preheader508.preheader
	movl	$yytext, %eax
	jmp	.LBB0_534
	.p2align	4, 0x90
.LBB0_533:                              #   in Loop: Header=BB0_534 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_534:                              # %.preheader508
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_538
# BB#535:                               # %.preheader508
                                        #   in Loop: Header=BB0_534 Depth=1
	testb	%dl, %dl
	je	.LBB0_1175
# BB#536:                               #   in Loop: Header=BB0_534 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_539
# BB#537:                               #   in Loop: Header=BB0_534 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_533
.LBB0_538:                              #   in Loop: Header=BB0_534 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_540
.LBB0_539:                              #   in Loop: Header=BB0_534 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_540:                              #   in Loop: Header=BB0_534 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_533
.LBB0_541:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_543
# BB#542:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_543:                              # %.preheader510.preheader
	movl	$yytext, %eax
	jmp	.LBB0_545
	.p2align	4, 0x90
.LBB0_544:                              #   in Loop: Header=BB0_545 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_545:                              # %.preheader510
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_549
# BB#546:                               # %.preheader510
                                        #   in Loop: Header=BB0_545 Depth=1
	testb	%dl, %dl
	je	.LBB0_1176
# BB#547:                               #   in Loop: Header=BB0_545 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_550
# BB#548:                               #   in Loop: Header=BB0_545 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_544
.LBB0_549:                              #   in Loop: Header=BB0_545 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_551
.LBB0_550:                              #   in Loop: Header=BB0_545 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_551:                              #   in Loop: Header=BB0_545 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_544
.LBB0_552:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_554
# BB#553:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_554:                              # %.preheader512.preheader
	movl	$yytext, %eax
	jmp	.LBB0_556
	.p2align	4, 0x90
.LBB0_555:                              #   in Loop: Header=BB0_556 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_556:                              # %.preheader512
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_560
# BB#557:                               # %.preheader512
                                        #   in Loop: Header=BB0_556 Depth=1
	testb	%dl, %dl
	je	.LBB0_1177
# BB#558:                               #   in Loop: Header=BB0_556 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_561
# BB#559:                               #   in Loop: Header=BB0_556 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_555
.LBB0_560:                              #   in Loop: Header=BB0_556 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_562
.LBB0_561:                              #   in Loop: Header=BB0_556 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_562:                              #   in Loop: Header=BB0_556 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_555
.LBB0_563:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_565
# BB#564:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_565:                              # %.preheader514.preheader
	movl	$yytext, %eax
	jmp	.LBB0_567
	.p2align	4, 0x90
.LBB0_566:                              #   in Loop: Header=BB0_567 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_567:                              # %.preheader514
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_571
# BB#568:                               # %.preheader514
                                        #   in Loop: Header=BB0_567 Depth=1
	testb	%dl, %dl
	je	.LBB0_1178
# BB#569:                               #   in Loop: Header=BB0_567 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_572
# BB#570:                               #   in Loop: Header=BB0_567 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_566
.LBB0_571:                              #   in Loop: Header=BB0_567 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_573
.LBB0_572:                              #   in Loop: Header=BB0_567 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_573:                              #   in Loop: Header=BB0_567 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_566
.LBB0_574:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_576
# BB#575:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_576:                              # %.preheader516.preheader
	movl	$yytext, %eax
	jmp	.LBB0_578
	.p2align	4, 0x90
.LBB0_577:                              #   in Loop: Header=BB0_578 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_578:                              # %.preheader516
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_582
# BB#579:                               # %.preheader516
                                        #   in Loop: Header=BB0_578 Depth=1
	testb	%dl, %dl
	je	.LBB0_1179
# BB#580:                               #   in Loop: Header=BB0_578 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_583
# BB#581:                               #   in Loop: Header=BB0_578 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_577
.LBB0_582:                              #   in Loop: Header=BB0_578 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_584
.LBB0_583:                              #   in Loop: Header=BB0_578 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_584:                              #   in Loop: Header=BB0_578 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_577
.LBB0_585:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_587
# BB#586:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_587:                              # %.preheader518.preheader
	movl	$yytext, %eax
	jmp	.LBB0_589
	.p2align	4, 0x90
.LBB0_588:                              #   in Loop: Header=BB0_589 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_589:                              # %.preheader518
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_593
# BB#590:                               # %.preheader518
                                        #   in Loop: Header=BB0_589 Depth=1
	testb	%dl, %dl
	je	.LBB0_1180
# BB#591:                               #   in Loop: Header=BB0_589 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_594
# BB#592:                               #   in Loop: Header=BB0_589 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_588
.LBB0_593:                              #   in Loop: Header=BB0_589 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_595
.LBB0_594:                              #   in Loop: Header=BB0_589 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_595:                              #   in Loop: Header=BB0_589 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_588
.LBB0_596:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_598
# BB#597:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_598:                              # %.preheader520.preheader
	movl	$yytext, %eax
	jmp	.LBB0_600
	.p2align	4, 0x90
.LBB0_599:                              #   in Loop: Header=BB0_600 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_600:                              # %.preheader520
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_604
# BB#601:                               # %.preheader520
                                        #   in Loop: Header=BB0_600 Depth=1
	testb	%dl, %dl
	je	.LBB0_1181
# BB#602:                               #   in Loop: Header=BB0_600 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_605
# BB#603:                               #   in Loop: Header=BB0_600 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_599
.LBB0_604:                              #   in Loop: Header=BB0_600 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_606
.LBB0_605:                              #   in Loop: Header=BB0_600 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_606:                              #   in Loop: Header=BB0_600 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_599
.LBB0_607:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_609
# BB#608:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_609:                              # %.preheader522.preheader
	movl	$yytext, %eax
	jmp	.LBB0_611
	.p2align	4, 0x90
.LBB0_610:                              #   in Loop: Header=BB0_611 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_611:                              # %.preheader522
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_615
# BB#612:                               # %.preheader522
                                        #   in Loop: Header=BB0_611 Depth=1
	testb	%dl, %dl
	je	.LBB0_1182
# BB#613:                               #   in Loop: Header=BB0_611 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_616
# BB#614:                               #   in Loop: Header=BB0_611 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_610
.LBB0_615:                              #   in Loop: Header=BB0_611 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_617
.LBB0_616:                              #   in Loop: Header=BB0_611 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_617:                              #   in Loop: Header=BB0_611 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_610
.LBB0_618:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_620
# BB#619:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_620:                              # %.preheader524.preheader
	movl	$yytext, %eax
	jmp	.LBB0_622
	.p2align	4, 0x90
.LBB0_621:                              #   in Loop: Header=BB0_622 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_622:                              # %.preheader524
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_626
# BB#623:                               # %.preheader524
                                        #   in Loop: Header=BB0_622 Depth=1
	testb	%dl, %dl
	je	.LBB0_1183
# BB#624:                               #   in Loop: Header=BB0_622 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_627
# BB#625:                               #   in Loop: Header=BB0_622 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_621
.LBB0_626:                              #   in Loop: Header=BB0_622 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_628
.LBB0_627:                              #   in Loop: Header=BB0_622 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_628:                              #   in Loop: Header=BB0_622 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_621
.LBB0_629:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_631
# BB#630:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_631:                              # %.preheader526.preheader
	movl	$yytext, %eax
	jmp	.LBB0_633
	.p2align	4, 0x90
.LBB0_632:                              #   in Loop: Header=BB0_633 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_633:                              # %.preheader526
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_637
# BB#634:                               # %.preheader526
                                        #   in Loop: Header=BB0_633 Depth=1
	testb	%dl, %dl
	je	.LBB0_1184
# BB#635:                               #   in Loop: Header=BB0_633 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_638
# BB#636:                               #   in Loop: Header=BB0_633 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_632
.LBB0_637:                              #   in Loop: Header=BB0_633 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_639
.LBB0_638:                              #   in Loop: Header=BB0_633 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_639:                              #   in Loop: Header=BB0_633 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_632
.LBB0_640:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_642
# BB#641:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_642:                              # %.preheader527.preheader
	movl	$yytext, %eax
	jmp	.LBB0_644
	.p2align	4, 0x90
.LBB0_643:                              #   in Loop: Header=BB0_644 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_644:                              # %.preheader527
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_648
# BB#645:                               # %.preheader527
                                        #   in Loop: Header=BB0_644 Depth=1
	testb	%dl, %dl
	je	.LBB0_1185
# BB#646:                               #   in Loop: Header=BB0_644 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_649
# BB#647:                               #   in Loop: Header=BB0_644 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_643
.LBB0_648:                              #   in Loop: Header=BB0_644 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_650
.LBB0_649:                              #   in Loop: Header=BB0_644 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_650:                              #   in Loop: Header=BB0_644 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_643
.LBB0_651:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_653
# BB#652:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_653:                              # %.preheader529.preheader
	movl	$yytext, %eax
	jmp	.LBB0_655
	.p2align	4, 0x90
.LBB0_654:                              #   in Loop: Header=BB0_655 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_655:                              # %.preheader529
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_659
# BB#656:                               # %.preheader529
                                        #   in Loop: Header=BB0_655 Depth=1
	testb	%dl, %dl
	je	.LBB0_1186
# BB#657:                               #   in Loop: Header=BB0_655 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_660
# BB#658:                               #   in Loop: Header=BB0_655 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_654
.LBB0_659:                              #   in Loop: Header=BB0_655 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_661
.LBB0_660:                              #   in Loop: Header=BB0_655 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_661:                              #   in Loop: Header=BB0_655 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_654
.LBB0_662:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_664
# BB#663:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_664:                              # %.preheader531.preheader
	movl	$yytext, %eax
	jmp	.LBB0_666
	.p2align	4, 0x90
.LBB0_665:                              #   in Loop: Header=BB0_666 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_666:                              # %.preheader531
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_670
# BB#667:                               # %.preheader531
                                        #   in Loop: Header=BB0_666 Depth=1
	testb	%dl, %dl
	je	.LBB0_1187
# BB#668:                               #   in Loop: Header=BB0_666 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_671
# BB#669:                               #   in Loop: Header=BB0_666 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_665
.LBB0_670:                              #   in Loop: Header=BB0_666 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_672
.LBB0_671:                              #   in Loop: Header=BB0_666 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_672:                              #   in Loop: Header=BB0_666 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_665
.LBB0_673:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_675
# BB#674:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_675:                              # %.preheader533.preheader
	movl	$yytext, %eax
	jmp	.LBB0_677
	.p2align	4, 0x90
.LBB0_676:                              #   in Loop: Header=BB0_677 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_677:                              # %.preheader533
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_681
# BB#678:                               # %.preheader533
                                        #   in Loop: Header=BB0_677 Depth=1
	testb	%dl, %dl
	je	.LBB0_1188
# BB#679:                               #   in Loop: Header=BB0_677 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_682
# BB#680:                               #   in Loop: Header=BB0_677 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_676
.LBB0_681:                              #   in Loop: Header=BB0_677 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_683
.LBB0_682:                              #   in Loop: Header=BB0_677 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_683:                              #   in Loop: Header=BB0_677 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_676
.LBB0_684:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_686
# BB#685:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_686:                              # %.preheader535.preheader
	movl	$yytext, %eax
	jmp	.LBB0_688
	.p2align	4, 0x90
.LBB0_687:                              #   in Loop: Header=BB0_688 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_688:                              # %.preheader535
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_692
# BB#689:                               # %.preheader535
                                        #   in Loop: Header=BB0_688 Depth=1
	testb	%dl, %dl
	je	.LBB0_1189
# BB#690:                               #   in Loop: Header=BB0_688 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_693
# BB#691:                               #   in Loop: Header=BB0_688 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_687
.LBB0_692:                              #   in Loop: Header=BB0_688 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_694
.LBB0_693:                              #   in Loop: Header=BB0_688 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_694:                              #   in Loop: Header=BB0_688 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_687
.LBB0_695:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_697
# BB#696:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_697:                              # %.preheader537.preheader
	movl	$yytext, %eax
	jmp	.LBB0_699
	.p2align	4, 0x90
.LBB0_698:                              #   in Loop: Header=BB0_699 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_699:                              # %.preheader537
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_703
# BB#700:                               # %.preheader537
                                        #   in Loop: Header=BB0_699 Depth=1
	testb	%dl, %dl
	je	.LBB0_1190
# BB#701:                               #   in Loop: Header=BB0_699 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_704
# BB#702:                               #   in Loop: Header=BB0_699 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_698
.LBB0_703:                              #   in Loop: Header=BB0_699 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_705
.LBB0_704:                              #   in Loop: Header=BB0_699 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_705:                              #   in Loop: Header=BB0_699 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_698
.LBB0_706:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_708
# BB#707:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_708:                              # %.preheader539.preheader
	movl	$yytext, %eax
	jmp	.LBB0_710
	.p2align	4, 0x90
.LBB0_709:                              #   in Loop: Header=BB0_710 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_710:                              # %.preheader539
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_714
# BB#711:                               # %.preheader539
                                        #   in Loop: Header=BB0_710 Depth=1
	testb	%dl, %dl
	je	.LBB0_1191
# BB#712:                               #   in Loop: Header=BB0_710 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_715
# BB#713:                               #   in Loop: Header=BB0_710 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_709
.LBB0_714:                              #   in Loop: Header=BB0_710 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_716
.LBB0_715:                              #   in Loop: Header=BB0_710 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_716:                              #   in Loop: Header=BB0_710 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_709
.LBB0_717:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_719
# BB#718:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_719:                              # %.preheader541.preheader
	movl	$yytext, %eax
	jmp	.LBB0_721
	.p2align	4, 0x90
.LBB0_720:                              #   in Loop: Header=BB0_721 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_721:                              # %.preheader541
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_725
# BB#722:                               # %.preheader541
                                        #   in Loop: Header=BB0_721 Depth=1
	testb	%dl, %dl
	je	.LBB0_1192
# BB#723:                               #   in Loop: Header=BB0_721 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_726
# BB#724:                               #   in Loop: Header=BB0_721 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_720
.LBB0_725:                              #   in Loop: Header=BB0_721 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_727
.LBB0_726:                              #   in Loop: Header=BB0_721 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_727:                              #   in Loop: Header=BB0_721 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_720
.LBB0_728:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_730
# BB#729:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_730:                              # %.preheader542.preheader
	movl	$yytext, %eax
	jmp	.LBB0_732
	.p2align	4, 0x90
.LBB0_731:                              #   in Loop: Header=BB0_732 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_732:                              # %.preheader542
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_736
# BB#733:                               # %.preheader542
                                        #   in Loop: Header=BB0_732 Depth=1
	testb	%dl, %dl
	je	.LBB0_1193
# BB#734:                               #   in Loop: Header=BB0_732 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_737
# BB#735:                               #   in Loop: Header=BB0_732 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_731
.LBB0_736:                              #   in Loop: Header=BB0_732 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_738
.LBB0_737:                              #   in Loop: Header=BB0_732 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_738:                              #   in Loop: Header=BB0_732 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_731
.LBB0_739:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_741
# BB#740:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_741:                              # %.preheader543.preheader
	movl	$yytext, %eax
	jmp	.LBB0_743
	.p2align	4, 0x90
.LBB0_742:                              #   in Loop: Header=BB0_743 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_743:                              # %.preheader543
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_747
# BB#744:                               # %.preheader543
                                        #   in Loop: Header=BB0_743 Depth=1
	testb	%dl, %dl
	je	.LBB0_1194
# BB#745:                               #   in Loop: Header=BB0_743 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_748
# BB#746:                               #   in Loop: Header=BB0_743 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_742
.LBB0_747:                              #   in Loop: Header=BB0_743 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_749
.LBB0_748:                              #   in Loop: Header=BB0_743 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_749:                              #   in Loop: Header=BB0_743 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_742
.LBB0_750:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_752
# BB#751:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_752:                              # %.preheader544.preheader
	movl	$yytext, %eax
	jmp	.LBB0_754
	.p2align	4, 0x90
.LBB0_753:                              #   in Loop: Header=BB0_754 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_754:                              # %.preheader544
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_758
# BB#755:                               # %.preheader544
                                        #   in Loop: Header=BB0_754 Depth=1
	testb	%dl, %dl
	je	.LBB0_993
# BB#756:                               #   in Loop: Header=BB0_754 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_759
# BB#757:                               #   in Loop: Header=BB0_754 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_753
.LBB0_758:                              #   in Loop: Header=BB0_754 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_760
.LBB0_759:                              #   in Loop: Header=BB0_754 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_760:                              #   in Loop: Header=BB0_754 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_753
.LBB0_761:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_763
# BB#762:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_763:                              # %.preheader545.preheader
	movl	$yytext, %eax
	jmp	.LBB0_765
	.p2align	4, 0x90
.LBB0_764:                              #   in Loop: Header=BB0_765 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_765:                              # %.preheader545
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_769
# BB#766:                               # %.preheader545
                                        #   in Loop: Header=BB0_765 Depth=1
	testb	%dl, %dl
	je	.LBB0_1220
# BB#767:                               #   in Loop: Header=BB0_765 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_770
# BB#768:                               #   in Loop: Header=BB0_765 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_764
.LBB0_769:                              #   in Loop: Header=BB0_765 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_771
.LBB0_770:                              #   in Loop: Header=BB0_765 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_771:                              #   in Loop: Header=BB0_765 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_764
.LBB0_772:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_774
# BB#773:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_774:                              # %.preheader546.preheader
	movl	$yytext, %eax
	jmp	.LBB0_776
	.p2align	4, 0x90
.LBB0_775:                              #   in Loop: Header=BB0_776 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_776:                              # %.preheader546
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_780
# BB#777:                               # %.preheader546
                                        #   in Loop: Header=BB0_776 Depth=1
	testb	%dl, %dl
	je	.LBB0_1195
# BB#778:                               #   in Loop: Header=BB0_776 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_781
# BB#779:                               #   in Loop: Header=BB0_776 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_775
.LBB0_780:                              #   in Loop: Header=BB0_776 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_782
.LBB0_781:                              #   in Loop: Header=BB0_776 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_782:                              #   in Loop: Header=BB0_776 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_775
.LBB0_783:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_785
# BB#784:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_785:                              # %.preheader548.preheader
	movl	$yytext, %eax
	jmp	.LBB0_787
	.p2align	4, 0x90
.LBB0_786:                              #   in Loop: Header=BB0_787 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_787:                              # %.preheader548
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_791
# BB#788:                               # %.preheader548
                                        #   in Loop: Header=BB0_787 Depth=1
	testb	%dl, %dl
	je	.LBB0_1196
# BB#789:                               #   in Loop: Header=BB0_787 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_792
# BB#790:                               #   in Loop: Header=BB0_787 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_786
.LBB0_791:                              #   in Loop: Header=BB0_787 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_793
.LBB0_792:                              #   in Loop: Header=BB0_787 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_793:                              #   in Loop: Header=BB0_787 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_786
.LBB0_794:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_796
# BB#795:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_796:                              # %.preheader550.preheader
	movl	$yytext, %eax
	jmp	.LBB0_798
	.p2align	4, 0x90
.LBB0_797:                              #   in Loop: Header=BB0_798 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_798:                              # %.preheader550
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_802
# BB#799:                               # %.preheader550
                                        #   in Loop: Header=BB0_798 Depth=1
	testb	%dl, %dl
	je	.LBB0_993
# BB#800:                               #   in Loop: Header=BB0_798 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_803
# BB#801:                               #   in Loop: Header=BB0_798 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_797
.LBB0_802:                              #   in Loop: Header=BB0_798 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_804
.LBB0_803:                              #   in Loop: Header=BB0_798 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_804:                              #   in Loop: Header=BB0_798 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_797
.LBB0_805:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_807
# BB#806:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_807:                              # %.preheader551.preheader
	movl	$yytext, %eax
	jmp	.LBB0_809
	.p2align	4, 0x90
.LBB0_808:                              #   in Loop: Header=BB0_809 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_809:                              # %.preheader551
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_813
# BB#810:                               # %.preheader551
                                        #   in Loop: Header=BB0_809 Depth=1
	testb	%dl, %dl
	je	.LBB0_1197
# BB#811:                               #   in Loop: Header=BB0_809 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_814
# BB#812:                               #   in Loop: Header=BB0_809 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_808
.LBB0_813:                              #   in Loop: Header=BB0_809 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_815
.LBB0_814:                              #   in Loop: Header=BB0_809 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_815:                              #   in Loop: Header=BB0_809 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_808
.LBB0_816:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_818
# BB#817:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_818:                              # %.preheader552.preheader
	movl	$yytext, %eax
	jmp	.LBB0_820
	.p2align	4, 0x90
.LBB0_819:                              #   in Loop: Header=BB0_820 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_820:                              # %.preheader552
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_824
# BB#821:                               # %.preheader552
                                        #   in Loop: Header=BB0_820 Depth=1
	testb	%dl, %dl
	je	.LBB0_1198
# BB#822:                               #   in Loop: Header=BB0_820 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_825
# BB#823:                               #   in Loop: Header=BB0_820 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_819
.LBB0_824:                              #   in Loop: Header=BB0_820 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_826
.LBB0_825:                              #   in Loop: Header=BB0_820 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_826:                              #   in Loop: Header=BB0_820 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_819
.LBB0_827:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_829
# BB#828:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_829:                              # %.preheader553.preheader
	movl	$yytext, %eax
	jmp	.LBB0_831
	.p2align	4, 0x90
.LBB0_830:                              #   in Loop: Header=BB0_831 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_831:                              # %.preheader553
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_835
# BB#832:                               # %.preheader553
                                        #   in Loop: Header=BB0_831 Depth=1
	testb	%dl, %dl
	je	.LBB0_1199
# BB#833:                               #   in Loop: Header=BB0_831 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_836
# BB#834:                               #   in Loop: Header=BB0_831 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_830
.LBB0_835:                              #   in Loop: Header=BB0_831 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_837
.LBB0_836:                              #   in Loop: Header=BB0_831 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_837:                              #   in Loop: Header=BB0_831 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_830
.LBB0_838:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_840
# BB#839:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_840:                              # %.preheader554.preheader
	movl	$yytext, %eax
	jmp	.LBB0_842
	.p2align	4, 0x90
.LBB0_841:                              #   in Loop: Header=BB0_842 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_842:                              # %.preheader554
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_846
# BB#843:                               # %.preheader554
                                        #   in Loop: Header=BB0_842 Depth=1
	testb	%dl, %dl
	je	.LBB0_1200
# BB#844:                               #   in Loop: Header=BB0_842 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_847
# BB#845:                               #   in Loop: Header=BB0_842 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_841
.LBB0_846:                              #   in Loop: Header=BB0_842 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_848
.LBB0_847:                              #   in Loop: Header=BB0_842 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_848:                              #   in Loop: Header=BB0_842 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_841
.LBB0_849:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_851
# BB#850:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_851:                              # %.preheader555.preheader
	movl	$yytext, %eax
	jmp	.LBB0_853
	.p2align	4, 0x90
.LBB0_852:                              #   in Loop: Header=BB0_853 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_853:                              # %.preheader555
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_857
# BB#854:                               # %.preheader555
                                        #   in Loop: Header=BB0_853 Depth=1
	testb	%dl, %dl
	je	.LBB0_1201
# BB#855:                               #   in Loop: Header=BB0_853 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_858
# BB#856:                               #   in Loop: Header=BB0_853 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_852
.LBB0_857:                              #   in Loop: Header=BB0_853 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_859
.LBB0_858:                              #   in Loop: Header=BB0_853 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_859:                              #   in Loop: Header=BB0_853 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_852
.LBB0_860:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_862
# BB#861:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_862:                              # %.preheader556.preheader
	movl	$yytext, %eax
	jmp	.LBB0_864
	.p2align	4, 0x90
.LBB0_863:                              #   in Loop: Header=BB0_864 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_864:                              # %.preheader556
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_868
# BB#865:                               # %.preheader556
                                        #   in Loop: Header=BB0_864 Depth=1
	testb	%dl, %dl
	je	.LBB0_1210
# BB#866:                               #   in Loop: Header=BB0_864 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_869
# BB#867:                               #   in Loop: Header=BB0_864 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_863
.LBB0_868:                              #   in Loop: Header=BB0_864 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_870
.LBB0_869:                              #   in Loop: Header=BB0_864 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_870:                              #   in Loop: Header=BB0_864 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_863
.LBB0_871:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_873
# BB#872:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_873:                              # %.preheader557.preheader
	movl	$yytext, %eax
	jmp	.LBB0_875
	.p2align	4, 0x90
.LBB0_874:                              #   in Loop: Header=BB0_875 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_875:                              # %.preheader557
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_879
# BB#876:                               # %.preheader557
                                        #   in Loop: Header=BB0_875 Depth=1
	testb	%dl, %dl
	je	.LBB0_1202
# BB#877:                               #   in Loop: Header=BB0_875 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_880
# BB#878:                               #   in Loop: Header=BB0_875 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_874
.LBB0_879:                              #   in Loop: Header=BB0_875 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_881
.LBB0_880:                              #   in Loop: Header=BB0_875 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_881:                              #   in Loop: Header=BB0_875 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_874
.LBB0_882:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_884
# BB#883:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_884:                              # %.preheader558.preheader
	movl	$yytext, %eax
	jmp	.LBB0_886
	.p2align	4, 0x90
.LBB0_885:                              #   in Loop: Header=BB0_886 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_886:                              # %.preheader558
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_890
# BB#887:                               # %.preheader558
                                        #   in Loop: Header=BB0_886 Depth=1
	testb	%dl, %dl
	je	.LBB0_1203
# BB#888:                               #   in Loop: Header=BB0_886 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_891
# BB#889:                               #   in Loop: Header=BB0_886 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_885
.LBB0_890:                              #   in Loop: Header=BB0_886 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_892
.LBB0_891:                              #   in Loop: Header=BB0_886 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_892:                              #   in Loop: Header=BB0_886 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_885
.LBB0_893:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_895
# BB#894:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_895:                              # %.preheader559.preheader
	movl	$yytext, %eax
	jmp	.LBB0_897
	.p2align	4, 0x90
.LBB0_896:                              #   in Loop: Header=BB0_897 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_897:                              # %.preheader559
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_901
# BB#898:                               # %.preheader559
                                        #   in Loop: Header=BB0_897 Depth=1
	testb	%dl, %dl
	je	.LBB0_1204
# BB#899:                               #   in Loop: Header=BB0_897 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_902
# BB#900:                               #   in Loop: Header=BB0_897 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_896
.LBB0_901:                              #   in Loop: Header=BB0_897 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_903
.LBB0_902:                              #   in Loop: Header=BB0_897 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_903:                              #   in Loop: Header=BB0_897 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_896
.LBB0_904:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_906
# BB#905:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_906:                              # %.preheader560.preheader
	movl	$yytext, %eax
	jmp	.LBB0_908
	.p2align	4, 0x90
.LBB0_907:                              #   in Loop: Header=BB0_908 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_908:                              # %.preheader560
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_912
# BB#909:                               # %.preheader560
                                        #   in Loop: Header=BB0_908 Depth=1
	testb	%dl, %dl
	je	.LBB0_1206
# BB#910:                               #   in Loop: Header=BB0_908 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_913
# BB#911:                               #   in Loop: Header=BB0_908 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_907
.LBB0_912:                              #   in Loop: Header=BB0_908 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_914
.LBB0_913:                              #   in Loop: Header=BB0_908 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_914:                              #   in Loop: Header=BB0_908 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_907
.LBB0_915:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_917
# BB#916:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_917:                              # %.preheader561.preheader
	movl	$yytext, %eax
	jmp	.LBB0_919
	.p2align	4, 0x90
.LBB0_918:                              #   in Loop: Header=BB0_919 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_919:                              # %.preheader561
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_923
# BB#920:                               # %.preheader561
                                        #   in Loop: Header=BB0_919 Depth=1
	testb	%dl, %dl
	je	.LBB0_1207
# BB#921:                               #   in Loop: Header=BB0_919 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_924
# BB#922:                               #   in Loop: Header=BB0_919 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_918
.LBB0_923:                              #   in Loop: Header=BB0_919 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_925
.LBB0_924:                              #   in Loop: Header=BB0_919 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_925:                              #   in Loop: Header=BB0_919 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_918
.LBB0_926:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_928
# BB#927:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_928:                              # %.preheader562.preheader
	movl	$yytext, %eax
	jmp	.LBB0_930
	.p2align	4, 0x90
.LBB0_929:                              #   in Loop: Header=BB0_930 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_930:                              # %.preheader562
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_934
# BB#931:                               # %.preheader562
                                        #   in Loop: Header=BB0_930 Depth=1
	testb	%dl, %dl
	je	.LBB0_993
# BB#932:                               #   in Loop: Header=BB0_930 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_935
# BB#933:                               #   in Loop: Header=BB0_930 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_929
.LBB0_934:                              #   in Loop: Header=BB0_930 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_936
.LBB0_935:                              #   in Loop: Header=BB0_930 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_936:                              #   in Loop: Header=BB0_930 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_929
.LBB0_937:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_939
# BB#938:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_939:                              # %.preheader563.preheader
	movl	$yytext, %eax
	jmp	.LBB0_941
	.p2align	4, 0x90
.LBB0_940:                              #   in Loop: Header=BB0_941 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_941:                              # %.preheader563
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_945
# BB#942:                               # %.preheader563
                                        #   in Loop: Header=BB0_941 Depth=1
	testb	%dl, %dl
	je	.LBB0_970
# BB#943:                               #   in Loop: Header=BB0_941 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_946
# BB#944:                               #   in Loop: Header=BB0_941 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_940
.LBB0_945:                              #   in Loop: Header=BB0_941 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_947
.LBB0_946:                              #   in Loop: Header=BB0_941 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_947:                              #   in Loop: Header=BB0_941 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_940
.LBB0_948:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_950
# BB#949:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_950:                              # %.preheader564.preheader
	movl	$yytext, %eax
	jmp	.LBB0_952
	.p2align	4, 0x90
.LBB0_951:                              #   in Loop: Header=BB0_952 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_952:                              # %.preheader564
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_956
# BB#953:                               # %.preheader564
                                        #   in Loop: Header=BB0_952 Depth=1
	testb	%dl, %dl
	je	.LBB0_1211
# BB#954:                               #   in Loop: Header=BB0_952 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_957
# BB#955:                               #   in Loop: Header=BB0_952 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_951
.LBB0_956:                              #   in Loop: Header=BB0_952 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_958
.LBB0_957:                              #   in Loop: Header=BB0_952 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_958:                              #   in Loop: Header=BB0_952 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_951
.LBB0_959:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_961
# BB#960:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_961:                              # %.preheader565.preheader
	movl	$yytext, %eax
	jmp	.LBB0_963
	.p2align	4, 0x90
.LBB0_962:                              #   in Loop: Header=BB0_963 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_963:                              # %.preheader565
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_967
# BB#964:                               # %.preheader565
                                        #   in Loop: Header=BB0_963 Depth=1
	testb	%dl, %dl
	je	.LBB0_970
# BB#965:                               #   in Loop: Header=BB0_963 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_968
# BB#966:                               #   in Loop: Header=BB0_963 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_962
.LBB0_967:                              #   in Loop: Header=BB0_963 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_969
.LBB0_968:                              #   in Loop: Header=BB0_963 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_969:                              #   in Loop: Header=BB0_963 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_962
.LBB0_970:                              # %_ZN12_GLOBAL__N_15countEb.exit218
	movl	$9, _ZL8yy_start(%rip)
	movsbl	yytext(%rip), %eax
	jmp	.LBB0_1227
.LBB0_971:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_973
# BB#972:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_973:                              # %.preheader566.preheader
	movl	$yytext, %eax
	jmp	.LBB0_975
	.p2align	4, 0x90
.LBB0_974:                              #   in Loop: Header=BB0_975 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_975:                              # %.preheader566
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_979
# BB#976:                               # %.preheader566
                                        #   in Loop: Header=BB0_975 Depth=1
	testb	%dl, %dl
	je	.LBB0_1212
# BB#977:                               #   in Loop: Header=BB0_975 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_980
# BB#978:                               #   in Loop: Header=BB0_975 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_974
.LBB0_979:                              #   in Loop: Header=BB0_975 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_981
.LBB0_980:                              #   in Loop: Header=BB0_975 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_981:                              #   in Loop: Header=BB0_975 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_974
.LBB0_982:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_984
# BB#983:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_984:                              # %.preheader567.preheader
	movl	$yytext, %eax
	jmp	.LBB0_986
	.p2align	4, 0x90
.LBB0_985:                              #   in Loop: Header=BB0_986 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_986:                              # %.preheader567
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_990
# BB#987:                               # %.preheader567
                                        #   in Loop: Header=BB0_986 Depth=1
	testb	%dl, %dl
	je	.LBB0_993
# BB#988:                               #   in Loop: Header=BB0_986 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_991
# BB#989:                               #   in Loop: Header=BB0_986 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_985
.LBB0_990:                              #   in Loop: Header=BB0_986 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_992
.LBB0_991:                              #   in Loop: Header=BB0_986 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_992:                              #   in Loop: Header=BB0_986 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_985
.LBB0_993:                              # %_ZN12_GLOBAL__N_15countEb.exit281
	movl	$yytext, %edi
	jmp	.LBB0_1213
.LBB0_994:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_996
# BB#995:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_996:                              # %.preheader568.preheader
	movl	$yytext, %eax
	jmp	.LBB0_998
	.p2align	4, 0x90
.LBB0_997:                              #   in Loop: Header=BB0_998 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_998:                              # %.preheader568
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1002
# BB#999:                               # %.preheader568
                                        #   in Loop: Header=BB0_998 Depth=1
	testb	%dl, %dl
	je	.LBB0_1214
# BB#1000:                              #   in Loop: Header=BB0_998 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1003
# BB#1001:                              #   in Loop: Header=BB0_998 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_997
.LBB0_1002:                             #   in Loop: Header=BB0_998 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1004
.LBB0_1003:                             #   in Loop: Header=BB0_998 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1004:                             #   in Loop: Header=BB0_998 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_997
.LBB0_1005:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1007
# BB#1006:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1007:                             # %.preheader569.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1009
	.p2align	4, 0x90
.LBB0_1008:                             #   in Loop: Header=BB0_1009 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1009:                             # %.preheader569
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1013
# BB#1010:                              # %.preheader569
                                        #   in Loop: Header=BB0_1009 Depth=1
	testb	%dl, %dl
	je	.LBB0_1215
# BB#1011:                              #   in Loop: Header=BB0_1009 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1014
# BB#1012:                              #   in Loop: Header=BB0_1009 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1008
.LBB0_1013:                             #   in Loop: Header=BB0_1009 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1015
.LBB0_1014:                             #   in Loop: Header=BB0_1009 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1015:                             #   in Loop: Header=BB0_1009 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1008
.LBB0_1016:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1018
# BB#1017:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1018:                             # %.preheader570.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1020
	.p2align	4, 0x90
.LBB0_1019:                             #   in Loop: Header=BB0_1020 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1020:                             # %.preheader570
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1024
# BB#1021:                              # %.preheader570
                                        #   in Loop: Header=BB0_1020 Depth=1
	testb	%dl, %dl
	je	.LBB0_1216
# BB#1022:                              #   in Loop: Header=BB0_1020 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1025
# BB#1023:                              #   in Loop: Header=BB0_1020 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1019
.LBB0_1024:                             #   in Loop: Header=BB0_1020 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1026
.LBB0_1025:                             #   in Loop: Header=BB0_1020 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1026:                             #   in Loop: Header=BB0_1020 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1019
.LBB0_1027:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1029
# BB#1028:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1029:                             # %.preheader571.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1031
	.p2align	4, 0x90
.LBB0_1030:                             #   in Loop: Header=BB0_1031 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1031:                             # %.preheader571
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1035
# BB#1032:                              # %.preheader571
                                        #   in Loop: Header=BB0_1031 Depth=1
	testb	%dl, %dl
	je	.LBB0_1217
# BB#1033:                              #   in Loop: Header=BB0_1031 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1036
# BB#1034:                              #   in Loop: Header=BB0_1031 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1030
.LBB0_1035:                             #   in Loop: Header=BB0_1031 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1037
.LBB0_1036:                             #   in Loop: Header=BB0_1031 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1037:                             #   in Loop: Header=BB0_1031 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1030
.LBB0_1038:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1040
# BB#1039:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1040:                             # %.preheader572.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1042
	.p2align	4, 0x90
.LBB0_1041:                             #   in Loop: Header=BB0_1042 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1042:                             # %.preheader572
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1046
# BB#1043:                              # %.preheader572
                                        #   in Loop: Header=BB0_1042 Depth=1
	testb	%dl, %dl
	je	.LBB0_1218
# BB#1044:                              #   in Loop: Header=BB0_1042 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1047
# BB#1045:                              #   in Loop: Header=BB0_1042 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1041
.LBB0_1046:                             #   in Loop: Header=BB0_1042 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1048
.LBB0_1047:                             #   in Loop: Header=BB0_1042 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1048:                             #   in Loop: Header=BB0_1042 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1041
.LBB0_1049:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1051
# BB#1050:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1051:                             # %.preheader573.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1053
	.p2align	4, 0x90
.LBB0_1052:                             #   in Loop: Header=BB0_1053 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1053:                             # %.preheader573
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1057
# BB#1054:                              # %.preheader573
                                        #   in Loop: Header=BB0_1053 Depth=1
	testb	%dl, %dl
	je	.LBB0_1219
# BB#1055:                              #   in Loop: Header=BB0_1053 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1058
# BB#1056:                              #   in Loop: Header=BB0_1053 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1052
.LBB0_1057:                             #   in Loop: Header=BB0_1053 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1059
.LBB0_1058:                             #   in Loop: Header=BB0_1053 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1059:                             #   in Loop: Header=BB0_1053 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1052
.LBB0_1060:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1062
# BB#1061:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1062:                             # %.preheader574.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1064
	.p2align	4, 0x90
.LBB0_1063:                             #   in Loop: Header=BB0_1064 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1064:                             # %.preheader574
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1068
# BB#1065:                              # %.preheader574
                                        #   in Loop: Header=BB0_1064 Depth=1
	testb	%dl, %dl
	je	.LBB0_1221
# BB#1066:                              #   in Loop: Header=BB0_1064 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1069
# BB#1067:                              #   in Loop: Header=BB0_1064 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1063
.LBB0_1068:                             #   in Loop: Header=BB0_1064 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1070
.LBB0_1069:                             #   in Loop: Header=BB0_1064 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1070:                             #   in Loop: Header=BB0_1064 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1063
.LBB0_1071:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1073
# BB#1072:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1073:                             # %.preheader576.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1075
	.p2align	4, 0x90
.LBB0_1074:                             #   in Loop: Header=BB0_1075 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1075:                             # %.preheader576
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1079
# BB#1076:                              # %.preheader576
                                        #   in Loop: Header=BB0_1075 Depth=1
	testb	%dl, %dl
	je	.LBB0_1222
# BB#1077:                              #   in Loop: Header=BB0_1075 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1080
# BB#1078:                              #   in Loop: Header=BB0_1075 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1074
.LBB0_1079:                             #   in Loop: Header=BB0_1075 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1081
.LBB0_1080:                             #   in Loop: Header=BB0_1075 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1081:                             #   in Loop: Header=BB0_1075 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1074
.LBB0_1082:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1084
# BB#1083:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1084:                             # %.preheader577.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1086
	.p2align	4, 0x90
.LBB0_1085:                             #   in Loop: Header=BB0_1086 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1086:                             # %.preheader577
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1090
# BB#1087:                              # %.preheader577
                                        #   in Loop: Header=BB0_1086 Depth=1
	testb	%dl, %dl
	je	.LBB0_1225
# BB#1088:                              #   in Loop: Header=BB0_1086 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1091
# BB#1089:                              #   in Loop: Header=BB0_1086 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1085
.LBB0_1090:                             #   in Loop: Header=BB0_1086 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1092
.LBB0_1091:                             #   in Loop: Header=BB0_1086 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1092:                             #   in Loop: Header=BB0_1086 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1085
.LBB0_1093:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1095
# BB#1094:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1095:                             # %.preheader578.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1097
	.p2align	4, 0x90
.LBB0_1096:                             #   in Loop: Header=BB0_1097 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1097:                             # %.preheader578
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1101
# BB#1098:                              # %.preheader578
                                        #   in Loop: Header=BB0_1097 Depth=1
	testb	%dl, %dl
	je	.LBB0_1225
# BB#1099:                              #   in Loop: Header=BB0_1097 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1102
# BB#1100:                              #   in Loop: Header=BB0_1097 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1096
.LBB0_1101:                             #   in Loop: Header=BB0_1097 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1103
.LBB0_1102:                             #   in Loop: Header=BB0_1097 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1103:                             #   in Loop: Header=BB0_1097 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1096
.LBB0_1104:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1106
# BB#1105:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1106:                             # %.preheader579.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1108
	.p2align	4, 0x90
.LBB0_1107:                             #   in Loop: Header=BB0_1108 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1108:                             # %.preheader579
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1112
# BB#1109:                              # %.preheader579
                                        #   in Loop: Header=BB0_1108 Depth=1
	testb	%dl, %dl
	je	.LBB0_1225
# BB#1110:                              #   in Loop: Header=BB0_1108 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1113
# BB#1111:                              #   in Loop: Header=BB0_1108 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1107
.LBB0_1112:                             #   in Loop: Header=BB0_1108 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1114
.LBB0_1113:                             #   in Loop: Header=BB0_1108 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1114:                             #   in Loop: Header=BB0_1108 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1107
.LBB0_1115:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1117
# BB#1116:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1117:                             # %.preheader580.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1119
	.p2align	4, 0x90
.LBB0_1118:                             #   in Loop: Header=BB0_1119 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1119:                             # %.preheader580
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1123
# BB#1120:                              # %.preheader580
                                        #   in Loop: Header=BB0_1119 Depth=1
	testb	%dl, %dl
	je	.LBB0_1225
# BB#1121:                              #   in Loop: Header=BB0_1119 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1124
# BB#1122:                              #   in Loop: Header=BB0_1119 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1118
.LBB0_1123:                             #   in Loop: Header=BB0_1119 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1125
.LBB0_1124:                             #   in Loop: Header=BB0_1119 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1125:                             #   in Loop: Header=BB0_1119 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1118
.LBB0_1126:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1128
# BB#1127:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1128:                             # %.preheader581.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1130
	.p2align	4, 0x90
.LBB0_1129:                             #   in Loop: Header=BB0_1130 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1130:                             # %.preheader581
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1134
# BB#1131:                              # %.preheader581
                                        #   in Loop: Header=BB0_1130 Depth=1
	testb	%dl, %dl
	je	.LBB0_1225
# BB#1132:                              #   in Loop: Header=BB0_1130 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1135
# BB#1133:                              #   in Loop: Header=BB0_1130 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1129
.LBB0_1134:                             #   in Loop: Header=BB0_1130 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1136
.LBB0_1135:                             #   in Loop: Header=BB0_1130 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1136:                             #   in Loop: Header=BB0_1130 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1129
.LBB0_1137:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1139
# BB#1138:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1139:                             # %.preheader582.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1141
	.p2align	4, 0x90
.LBB0_1140:                             #   in Loop: Header=BB0_1141 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1141:                             # %.preheader582
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1145
# BB#1142:                              # %.preheader582
                                        #   in Loop: Header=BB0_1141 Depth=1
	testb	%dl, %dl
	je	.LBB0_1230
# BB#1143:                              #   in Loop: Header=BB0_1141 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1146
# BB#1144:                              #   in Loop: Header=BB0_1141 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1140
.LBB0_1145:                             #   in Loop: Header=BB0_1141 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1147
.LBB0_1146:                             #   in Loop: Header=BB0_1141 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1147:                             #   in Loop: Header=BB0_1141 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1140
.LBB0_1148:
	movslq	yyleng(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1150
# BB#1149:
	xorl	%ecx, %ecx
	cmpb	$10, yytext-1(%rax)
	sete	%cl
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
.LBB0_1150:                             # %.preheader583.preheader
	movl	$yytext, %eax
	jmp	.LBB0_1152
	.p2align	4, 0x90
.LBB0_1151:                             #   in Loop: Header=BB0_1152 Depth=1
	addl	%ecx, pg_charpos(%rip)
	incq	%rax
.LBB0_1152:                             # %.preheader583
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	je	.LBB0_1156
# BB#1153:                              # %.preheader583
                                        #   in Loop: Header=BB0_1152 Depth=1
	testb	%dl, %dl
	je	.LBB0_1230
# BB#1154:                              #   in Loop: Header=BB0_1152 Depth=1
	movl	pg_column(%rip), %ecx
	cmpb	$9, %dl
	jne	.LBB0_1157
# BB#1155:                              #   in Loop: Header=BB0_1152 Depth=1
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%ecx, %edx
	andl	$-8, %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	negl	%esi
	leal	8(%rcx,%rsi), %ecx
	movl	%ecx, pg_column(%rip)
	movl	pg_charpos(%rip), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	movl	$8, %ecx
	subl	%edx, %ecx
	jmp	.LBB0_1151
.LBB0_1156:                             #   in Loop: Header=BB0_1152 Depth=1
	movl	$0, pg_column(%rip)
	incl	pg_lineno(%rip)
	jmp	.LBB0_1158
.LBB0_1157:                             #   in Loop: Header=BB0_1152 Depth=1
	incl	%ecx
	movl	%ecx, pg_column(%rip)
.LBB0_1158:                             #   in Loop: Header=BB0_1152 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_1151
.LBB0_1159:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit
	movl	$280, %eax              # imm = 0x118
	jmp	.LBB0_1227
.LBB0_1160:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1361
	movl	$281, %eax              # imm = 0x119
	jmp	.LBB0_1227
.LBB0_1161:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1362
	movl	$278, %eax              # imm = 0x116
	jmp	.LBB0_1227
.LBB0_1162:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1363
	movl	$279, %eax              # imm = 0x117
	jmp	.LBB0_1227
.LBB0_1163:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1364
	movl	$282, %eax              # imm = 0x11A
	jmp	.LBB0_1227
.LBB0_1164:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1365
	movl	$283, %eax              # imm = 0x11B
	jmp	.LBB0_1227
.LBB0_1165:                             # %_ZN12_GLOBAL__N_15countEb.exit233
	movl	$7, _ZL8yy_start(%rip)
	movl	$266, %eax              # imm = 0x10A
	jmp	.LBB0_1227
.LBB0_1166:                             # %_ZN12_GLOBAL__N_15countEb.exit350
	movq	_ZN12_GLOBAL__N_18cincludeB5cxx11E+8(%rip), %rdx
	movl	$_ZN12_GLOBAL__N_18cincludeB5cxx11E, %edi
	xorl	%esi, %esi
	movl	$.L.str, %ecx
	xorl	%r8d, %r8d
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm
	movl	$_ZN12_GLOBAL__N_18cincludeB5cxx11E, %edi
	movl	$1024, %esi             # imm = 0x400
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm
	movl	$5, _ZL8yy_start(%rip)
	movl	$267, %eax              # imm = 0x10B
	jmp	.LBB0_1227
.LBB0_1167:                             # %_ZN12_GLOBAL__N_15countEb.exit375
	movl	$3, _ZL8yy_start(%rip)
	movq	_ZN12_GLOBAL__N_18cincludeB5cxx11E(%rip), %rdi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, yylval(%rip)
	movl	$265, %eax              # imm = 0x109
	jmp	.LBB0_1227
.LBB0_1168:                             # %_ZN12_GLOBAL__N_15countEb.exit378
	movq	pg_filename(%rip), %rdi
	callq	_ZN12_GLOBAL__N_111eat_commentEPN2kc20impl_casestring__StrE
	movl	%eax, %edi
.LBB0_1169:                             # %_ZN12_GLOBAL__N_15countEb.exit98
	callq	_ZN2kc9mkintegerEi
	movq	%rax, yylval(%rip)
	movl	$261, %eax              # imm = 0x105
	jmp	.LBB0_1227
.LBB0_1170:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1366
	movl	$269, %eax              # imm = 0x10D
	jmp	.LBB0_1227
.LBB0_1171:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1367
	movl	$270, %eax              # imm = 0x10E
	jmp	.LBB0_1227
.LBB0_1172:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1368
	movl	$275, %eax              # imm = 0x113
	jmp	.LBB0_1227
.LBB0_1173:                             # %_ZN12_GLOBAL__N_15countEb.exit353
	movl	$yytext, %edi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, yylval(%rip)
	movl	$260, %eax              # imm = 0x104
	jmp	.LBB0_1227
.LBB0_1174:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1369
	movl	$268, %eax              # imm = 0x10C
	jmp	.LBB0_1227
.LBB0_1175:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1370
	movl	$271, %eax              # imm = 0x10F
	jmp	.LBB0_1227
.LBB0_1176:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1371
	movl	$284, %eax              # imm = 0x11C
	jmp	.LBB0_1227
.LBB0_1177:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1372
	movl	$285, %eax              # imm = 0x11D
	jmp	.LBB0_1227
.LBB0_1178:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1373
	movl	$286, %eax              # imm = 0x11E
	jmp	.LBB0_1227
.LBB0_1179:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1374
	movl	$287, %eax              # imm = 0x11F
	jmp	.LBB0_1227
.LBB0_1180:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1375
	movl	$288, %eax              # imm = 0x120
	jmp	.LBB0_1227
.LBB0_1181:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1376
	movl	$289, %eax              # imm = 0x121
	jmp	.LBB0_1227
.LBB0_1182:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1377
	movl	$290, %eax              # imm = 0x122
	jmp	.LBB0_1227
.LBB0_1183:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1378
	movl	$291, %eax              # imm = 0x123
	jmp	.LBB0_1227
.LBB0_1184:                             # %_ZN12_GLOBAL__N_15countEb.exit317
	movl	$17, _ZL8yy_start(%rip)
	movl	$294, %eax              # imm = 0x126
	jmp	.LBB0_1227
.LBB0_1185:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1379
	movl	$295, %eax              # imm = 0x127
	jmp	.LBB0_1227
.LBB0_1186:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1380
	movl	$296, %eax              # imm = 0x128
	jmp	.LBB0_1227
.LBB0_1187:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1381
	movl	$297, %eax              # imm = 0x129
	jmp	.LBB0_1227
.LBB0_1188:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1382
	movl	$298, %eax              # imm = 0x12A
	jmp	.LBB0_1227
.LBB0_1189:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1383
	movl	$299, %eax              # imm = 0x12B
	jmp	.LBB0_1227
.LBB0_1190:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1384
	movl	$292, %eax              # imm = 0x124
	jmp	.LBB0_1227
.LBB0_1191:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1385
	movl	$293, %eax              # imm = 0x125
	jmp	.LBB0_1227
.LBB0_1192:                             # %_ZN12_GLOBAL__N_15countEb.exit290
	movl	$3, _ZL8yy_start(%rip)
	movl	$yytext, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, yylval(%rip)
	movl	$259, %eax              # imm = 0x103
	jmp	.LBB0_1227
.LBB0_1193:                             # %_ZN12_GLOBAL__N_15countEb.exit287
	movl	$3, _ZL8yy_start(%rip)
.LBB0_1194:                             # %_ZN12_GLOBAL__N_15countEb.exit245
	movl	$yytext, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, yylval(%rip)
	movl	$258, %eax              # imm = 0x102
	jmp	.LBB0_1227
.LBB0_1195:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1386
	movl	$276, %eax              # imm = 0x114
	jmp	.LBB0_1227
.LBB0_1196:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1387
	movl	$277, %eax              # imm = 0x115
	jmp	.LBB0_1227
.LBB0_1197:                             # %_ZN12_GLOBAL__N_15countEb.exit263
	movsbl	yytext(%rip), %eax
	jmp	.LBB0_1227
.LBB0_1198:                             # %_ZN12_GLOBAL__N_15countEb.exit260
	movl	$.L.str.3, %edi
	jmp	.LBB0_1213
.LBB0_1199:                             # %_ZN12_GLOBAL__N_15countEb.exit257
	movl	$.L.str.4, %edi
	jmp	.LBB0_1213
.LBB0_1200:                             # %_ZN12_GLOBAL__N_15countEb.exit254
	movl	$11, _ZL8yy_start(%rip)
	movsbl	yytext(%rip), %eax
	jmp	.LBB0_1227
.LBB0_1201:                             # %_ZN12_GLOBAL__N_15countEb.exit251
	movl	$13, _ZL8yy_start(%rip)
.LBB0_1202:                             # %_ZN12_GLOBAL__N_15countEb.exit242
	movsbl	yytext(%rip), %eax
	jmp	.LBB0_1227
.LBB0_1203:                             # %_ZN12_GLOBAL__N_15countEb.exit239
	incl	_ZN12_GLOBAL__N_119cinit_paren_nestingE(%rip)
	movl	$.L.str.5, %edi
	jmp	.LBB0_1209
.LBB0_1204:                             # %_ZN12_GLOBAL__N_15countEb.exit236
	movl	_ZN12_GLOBAL__N_119cinit_paren_nestingE(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_1210
# BB#1205:
	decl	%eax
	movl	%eax, _ZN12_GLOBAL__N_119cinit_paren_nestingE(%rip)
	movl	$.L.str.6, %edi
	jmp	.LBB0_1209
.LBB0_1206:                             # %_ZN12_GLOBAL__N_15countEb.exit230
	incl	_ZN12_GLOBAL__N_119cinit_array_nestingE(%rip)
	movl	$.L.str.7, %edi
	jmp	.LBB0_1209
.LBB0_1207:                             # %_ZN12_GLOBAL__N_15countEb.exit227
	movl	_ZN12_GLOBAL__N_119cinit_array_nestingE(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_1210
# BB#1208:
	decl	%eax
	movl	%eax, _ZN12_GLOBAL__N_119cinit_array_nestingE(%rip)
	movl	$.L.str.8, %edi
.LBB0_1209:                             # %_ZN12_GLOBAL__N_15countEb.exit98
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, yylval(%rip)
	movsbl	yytext(%rip), %eax
	jmp	.LBB0_1227
.LBB0_1210:                             # %_ZN12_GLOBAL__N_15countEb.exit248
	movl	$3, _ZL8yy_start(%rip)
	callq	_ZN12_GLOBAL__N_115reset_nestcountEv
	movsbl	yytext(%rip), %eax
	jmp	.LBB0_1227
.LBB0_1211:                             # %_ZN12_GLOBAL__N_15countEb.exit215
	movl	$.L.str.9, %edi
	jmp	.LBB0_1213
.LBB0_1212:                             # %_ZN12_GLOBAL__N_15countEb.exit209
	movl	$.L.str.10, %edi
.LBB0_1213:                             # %_ZN12_GLOBAL__N_15countEb.exit98
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, yylval(%rip)
	movl	$263, %eax              # imm = 0x107
	jmp	.LBB0_1227
.LBB0_1214:                             # %_ZN12_GLOBAL__N_15countEb.exit203
	movl	$.L.str.11, %edi
	jmp	.LBB0_1226
.LBB0_1215:                             # %_ZN12_GLOBAL__N_15countEb.exit200
	movl	$.L.str.4, %edi
	jmp	.LBB0_1226
.LBB0_1216:                             # %_ZN12_GLOBAL__N_15countEb.exit197
	movl	$.L.str.3, %edi
	jmp	.LBB0_1226
.LBB0_1217:                             # %_ZN12_GLOBAL__N_15countEb.exit194
	movl	$.L.str.12, %edi
	jmp	.LBB0_1226
.LBB0_1218:                             # %_ZN12_GLOBAL__N_15countEb.exit191
	movl	$.L.str.13, %edi
	jmp	.LBB0_1226
.LBB0_1219:                             # %_ZN12_GLOBAL__N_15countEb.exit188
	movl	$yytext, %edi
	movl	$.L.str.11, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_1230
.LBB0_1220:                             # %_ZN12_GLOBAL__N_15countEb.exit278
	movl	$yytext+1, %edi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, yylval(%rip)
	movl	$264, %eax              # imm = 0x108
	jmp	.LBB0_1227
.LBB0_1230:
	movsbl	yytext(%rip), %eax
	jmp	.LBB0_1227
.LBB0_1221:                             # %_ZN12_GLOBAL__N_15countEb.exit98.loopexit1388
	movl	$274, %eax              # imm = 0x112
	jmp	.LBB0_1227
.LBB0_1222:                             # %_ZN12_GLOBAL__N_15countEb.exit179
	movl	$yytext, %edi
	movl	$.L.str.14, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_1231
# BB#1223:
	movl	$yytext, %edi
	movl	$.L.str.15, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_1232
# BB#1224:
	movl	$yytext, %edi
	movl	$.L.str.16, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_1233
.LBB0_1225:
	movl	$yytext, %edi
.LBB0_1226:                             # %_ZN12_GLOBAL__N_15countEb.exit98
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, yylval(%rip)
	movl	$262, %eax              # imm = 0x106
.LBB0_1227:                             # %_ZN12_GLOBAL__N_15countEb.exit98
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_1231:
	movl	$273, %eax              # imm = 0x111
	jmp	.LBB0_1227
.LBB0_1232:
	movl	$274, %eax              # imm = 0x112
	jmp	.LBB0_1227
.LBB0_1233:
	movl	$272, %eax              # imm = 0x110
	jmp	.LBB0_1227
.LBB0_1234:
	movl	$.L.str.1, %edi
	callq	_ZL14yy_fatal_errorPKc
.LBB0_1235:
	movl	$.L.str.19, %edi
	callq	_ZL14yy_fatal_errorPKc
.LBB0_1236:
	movl	$.L.str.32, %edi
	callq	_ZSt20__throw_length_errorPKc
.Lfunc_end0:
	.size	_Z5yylexv, .Lfunc_end0-_Z5yylexv
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_119
	.quad	.LBB0_341
	.quad	.LBB0_352
	.quad	.LBB0_363
	.quad	.LBB0_374
	.quad	.LBB0_385
	.quad	.LBB0_396
	.quad	.LBB0_130
	.quad	.LBB0_407
	.quad	.LBB0_418
	.quad	.LBB0_141
	.quad	.LBB0_429
	.quad	.LBB0_152
	.quad	.LBB0_163
	.quad	.LBB0_440
	.quad	.LBB0_451
	.quad	.LBB0_174
	.quad	.LBB0_462
	.quad	.LBB0_185
	.quad	.LBB0_475
	.quad	.LBB0_486
	.quad	.LBB0_497
	.quad	.LBB0_508
	.quad	.LBB0_519
	.quad	.LBB0_530
	.quad	.LBB0_541
	.quad	.LBB0_552
	.quad	.LBB0_563
	.quad	.LBB0_574
	.quad	.LBB0_585
	.quad	.LBB0_596
	.quad	.LBB0_607
	.quad	.LBB0_618
	.quad	.LBB0_629
	.quad	.LBB0_640
	.quad	.LBB0_651
	.quad	.LBB0_662
	.quad	.LBB0_673
	.quad	.LBB0_684
	.quad	.LBB0_695
	.quad	.LBB0_706
	.quad	.LBB0_198
	.quad	.LBB0_717
	.quad	.LBB0_728
	.quad	.LBB0_739
	.quad	.LBB0_750
	.quad	.LBB0_761
	.quad	.LBB0_772
	.quad	.LBB0_783
	.quad	.LBB0_794
	.quad	.LBB0_805
	.quad	.LBB0_816
	.quad	.LBB0_827
	.quad	.LBB0_838
	.quad	.LBB0_849
	.quad	.LBB0_860
	.quad	.LBB0_871
	.quad	.LBB0_882
	.quad	.LBB0_893
	.quad	.LBB0_904
	.quad	.LBB0_915
	.quad	.LBB0_926
	.quad	.LBB0_937
	.quad	.LBB0_948
	.quad	.LBB0_959
	.quad	.LBB0_971
	.quad	.LBB0_982
	.quad	.LBB0_994
	.quad	.LBB0_1005
	.quad	.LBB0_1016
	.quad	.LBB0_1027
	.quad	.LBB0_1038
	.quad	.LBB0_1049
	.quad	.LBB0_1060
	.quad	.LBB0_1071
	.quad	.LBB0_1082
	.quad	.LBB0_1093
	.quad	.LBB0_1104
	.quad	.LBB0_1115
	.quad	.LBB0_1126
	.quad	.LBB0_1137
	.quad	.LBB0_209
	.quad	.LBB0_220
	.quad	.LBB0_231
	.quad	.LBB0_242
	.quad	.LBB0_253
	.quad	.LBB0_264
	.quad	.LBB0_275
	.quad	.LBB0_286
	.quad	.LBB0_297
	.quad	.LBB0_308
	.quad	.LBB0_1148
	.quad	.LBB0_319
	.quad	.LBB0_90
	.quad	.LBB0_340
	.quad	.LBB0_340
	.quad	.LBB0_340
	.quad	.LBB0_340
	.quad	.LBB0_340
	.quad	.LBB0_340
	.quad	.LBB0_340
	.quad	.LBB0_340
	.quad	.LBB0_340
	.quad	.LBB0_340

	.text
	.globl	_Z7yyallocj
	.p2align	4, 0x90
	.type	_Z7yyallocj,@function
_Z7yyallocj:                            # @_Z7yyallocj
	.cfi_startproc
# BB#0:
	movl	%edi, %edi
	jmp	malloc                  # TAILCALL
.Lfunc_end1:
	.size	_Z7yyallocj, .Lfunc_end1-_Z7yyallocj
	.cfi_endproc

	.globl	_Z16yy_create_bufferP8_IO_FILEi
	.p2align	4, 0x90
	.type	_Z16yy_create_bufferP8_IO_FILEi,@function
_Z16yy_create_bufferP8_IO_FILEi:        # @_Z16yy_create_bufferP8_IO_FILEi
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	movl	$64, %edi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB2_14
# BB#1:
	movl	%r15d, 24(%r12)
	addl	$2, %r15d
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 8(%r12)
	testq	%rbx, %rbx
	je	.LBB2_14
# BB#2:
	movl	$1, 32(%r12)
	callq	__errno_location
	movl	(%rax), %ecx
	movl	$0, 28(%r12)
	movb	$0, (%rbx)
	movb	$0, 1(%rbx)
	movq	%rbx, 16(%r12)
	movl	$1, 40(%r12)
	movl	$0, 56(%r12)
	movq	_ZL15yy_buffer_stack(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB2_3
# BB#4:
	movq	_ZL19yy_buffer_stack_top(%rip), %rsi
	movq	(%rdx,%rsi,8), %rsi
	cmpq	%r12, %rsi
	jne	.LBB2_7
	jmp	.LBB2_6
.LBB2_3:
	xorl	%esi, %esi
	cmpq	%r12, %rsi
	je	.LBB2_6
.LBB2_7:                                # %_Z15yy_flush_bufferP15yy_buffer_state.exit.i
	testq	%rdx, %rdx
	movq	%r14, (%r12)
	movl	$1, 52(%r12)
	je	.LBB2_8
# BB#9:                                 # %_Z15yy_flush_bufferP15yy_buffer_state.exit.i._crit_edge
	movq	_ZL19yy_buffer_stack_top(%rip), %rsi
	jmp	.LBB2_10
.LBB2_6:                                # %_Z15yy_flush_bufferP15yy_buffer_state.exit.thread.i
	movq	_ZL19yy_buffer_stack_top(%rip), %rsi
	movq	(%rdx,%rsi,8), %rdi
	movl	28(%rdi), %ebx
	movl	%ebx, _ZL10yy_n_chars(%rip)
	movq	16(%rdi), %rdi
	movq	%rdi, _ZL10yy_c_buf_p(%rip)
	movq	%rdi, yytext_ptr(%rip)
	movq	(%rdx,%rsi,8), %rbx
	movq	(%rbx), %rbx
	movq	%rbx, yyin(%rip)
	movb	(%rdi), %bl
	movb	%bl, _ZL12yy_hold_char(%rip)
	movq	%r14, (%r12)
	movl	$1, 52(%r12)
.LBB2_10:
	movq	(%rdx,%rsi,8), %rdx
	cmpq	%r12, %rdx
	jne	.LBB2_12
	jmp	.LBB2_13
.LBB2_8:
	xorl	%edx, %edx
	cmpq	%r12, %rdx
	je	.LBB2_13
.LBB2_12:
	movq	$1, 44(%r12)
.LBB2_13:                               # %_ZL14yy_init_bufferP15yy_buffer_stateP8_IO_FILE.exit
	movl	$0, 36(%r12)
	movl	%ecx, (%rax)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB2_14:
	movl	$.L.str.20, %edi
	callq	_ZL14yy_fatal_errorPKc
.Lfunc_end2:
	.size	_Z16yy_create_bufferP8_IO_FILEi, .Lfunc_end2-_Z16yy_create_bufferP8_IO_FILEi
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL14yy_fatal_errorPKc,@function
_ZL14yy_fatal_errorPKc:                 # @_ZL14yy_fatal_errorPKc
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	stderr(%rip), %rdi
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movl	$2, %edi
	callq	exit
.Lfunc_end3:
	.size	_ZL14yy_fatal_errorPKc, .Lfunc_end3-_ZL14yy_fatal_errorPKc
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN12_GLOBAL__N_111eat_commentEPN2kc20impl_casestring__StrE,@function
_ZN12_GLOBAL__N_111eat_commentEPN2kc20impl_casestring__StrE: # @_ZN12_GLOBAL__N_111eat_commentEPN2kc20impl_casestring__StrE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 48
.Lcfi25:
	.cfi_offset %rbx, -48
.Lcfi26:
	.cfi_offset %r12, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$32, %ebp
	movl	$1, %r12d
	movl	pg_lineno(%rip), %r15d
	jmp	.LBB4_1
.LBB4_13:                               #   in Loop: Header=BB4_1 Depth=1
	movl	$-1, %eax
	addl	%eax, %r12d
	jg	.LBB4_1
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_6:                                #   in Loop: Header=BB4_1 Depth=1
	movl	pg_column(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	andl	$-8, %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	negl	%edx
	leal	8(%rax,%rdx), %eax
	movl	%eax, pg_column(%rip)
	movl	pg_charpos(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	andl	$-8, %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	negl	%edx
	leal	8(%rax,%rdx), %eax
	movl	%eax, pg_charpos(%rip)
	jmp	.LBB4_1
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_1 Depth=1
	incl	pg_lineno(%rip)
	movl	$0, pg_column(%rip)
	incl	pg_charpos(%rip)
	jmp	.LBB4_1
.LBB4_10:                               #   in Loop: Header=BB4_1 Depth=1
	movl	$1, %eax
	addl	%eax, %r12d
	jg	.LBB4_1
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_1 Depth=1
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.30, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB4_1:                                # %.thread21.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_2 Depth 2
	movl	%ebp, %ebx
.LBB4_2:                                # %.thread21
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	_ZL7yyinputv
	movl	%eax, %ebp
	movsbl	%bpl, %eax
	testb	%al, %al
	je	.LBB4_7
# BB#3:                                 # %.thread21
                                        #   in Loop: Header=BB4_2 Depth=2
	cmpl	$9, %eax
	je	.LBB4_6
# BB#4:                                 # %.thread21
                                        #   in Loop: Header=BB4_2 Depth=2
	cmpl	$10, %eax
	je	.LBB4_5
# BB#8:                                 #   in Loop: Header=BB4_2 Depth=2
	incl	pg_column(%rip)
	incl	pg_charpos(%rip)
	cmpb	$47, %bl
	jne	.LBB4_11
# BB#9:                                 #   in Loop: Header=BB4_2 Depth=2
	cmpl	$42, %eax
	je	.LBB4_10
.LBB4_11:                               #   in Loop: Header=BB4_2 Depth=2
	movsbl	%bl, %ecx
	cmpl	$42, %ecx
	movl	%ebp, %ebx
	jne	.LBB4_2
# BB#12:                                #   in Loop: Header=BB4_2 Depth=2
	cmpl	$47, %eax
	movl	%ebp, %ebx
	jne	.LBB4_2
	jmp	.LBB4_13
.LBB4_15:
	movl	pg_lineno(%rip), %eax
	subl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN12_GLOBAL__N_111eat_commentEPN2kc20impl_casestring__StrE, .Lfunc_end4-_ZN12_GLOBAL__N_111eat_commentEPN2kc20impl_casestring__StrE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN12_GLOBAL__N_115reset_nestcountEv,@function
_ZN12_GLOBAL__N_115reset_nestcountEv:   # @_ZN12_GLOBAL__N_115reset_nestcountEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 16
.Lcfi31:
	.cfi_offset %rbx, -16
	cmpl	$0, _ZN12_GLOBAL__N_119cinit_paren_nestingE(%rip)
	je	.LBB5_2
# BB#1:
	callq	_ZN2kc13PosNoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.28, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB5_2:
	cmpl	$0, _ZN12_GLOBAL__N_119cinit_array_nestingE(%rip)
	je	.LBB5_4
# BB#3:
	callq	_ZN2kc13PosNoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.29, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB5_4:
	movl	$0, _ZN12_GLOBAL__N_119cinit_paren_nestingE(%rip)
	movl	$0, _ZN12_GLOBAL__N_119cinit_array_nestingE(%rip)
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZN12_GLOBAL__N_115reset_nestcountEv, .Lfunc_end5-_ZN12_GLOBAL__N_115reset_nestcountEv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL18yy_get_next_bufferv,@function
_ZL18yy_get_next_bufferv:               # @_ZL18yy_get_next_bufferv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	_ZL15yy_buffer_stack(%rip), %r8
	movq	_ZL19yy_buffer_stack_top(%rip), %r9
	movq	(%r8,%r9,8), %r13
	movq	8(%r13), %rax
	movq	_ZL10yy_c_buf_p(%rip), %rdx
	movslq	_ZL10yy_n_chars(%rip), %rcx
	leaq	1(%rax,%rcx), %rcx
	cmpq	%rcx, %rdx
	ja	.LBB6_44
# BB#1:
	movq	yytext_ptr(%rip), %rcx
	subq	%rcx, %rdx
	cmpl	$0, 52(%r13)
	je	.LBB6_2
# BB#3:
	movslq	%edx, %rsi
	leaq	-1(%rsi), %r12
	cmpl	$2, %esi
	jl	.LBB6_19
# BB#4:                                 # %.lr.ph72.preheader
	leal	-2(%rdx), %esi
	incq	%rsi
	cmpq	$32, %rsi
	jb	.LBB6_5
# BB#6:                                 # %min.iters.checked
	leal	31(%rdx), %r10d
	andl	$31, %r10d
	subq	%r10, %rsi
	je	.LBB6_5
# BB#7:                                 # %vector.memcheck
	movl	$4294967294, %ebp       # imm = 0xFFFFFFFE
	movl	%edx, %edi
	addl	%ebp, %edi
	leaq	1(%rcx,%rdi), %rbp
	cmpq	%rbp, %rax
	jae	.LBB6_9
# BB#8:                                 # %vector.memcheck
	leaq	1(%rax,%rdi), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB6_9
.LBB6_5:
	xorl	%esi, %esi
	movq	%rcx, %rdi
	movq	%rax, %rbx
.LBB6_12:                               # %.lr.ph72.preheader92
	leal	7(%rdx), %ecx
	subl	%esi, %ecx
	leal	-2(%rdx), %ebp
	subl	%esi, %ebp
	andl	$7, %ecx
	je	.LBB6_15
# BB#13:                                # %.lr.ph72.prol.preheader
	negl	%ecx
	.p2align	4, 0x90
.LBB6_14:                               # %.lr.ph72.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %eax
	incq	%rdi
	movb	%al, (%rbx)
	incq	%rbx
	incl	%esi
	incl	%ecx
	jne	.LBB6_14
.LBB6_15:                               # %.lr.ph72.prol.loopexit
	cmpl	$7, %ebp
	jb	.LBB6_18
# BB#16:                                # %.lr.ph72.preheader92.new
	leal	-1(%rdx), %eax
	subl	%esi, %eax
	.p2align	4, 0x90
.LBB6_17:                               # %.lr.ph72
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %ecx
	movb	%cl, (%rbx)
	movzbl	1(%rdi), %ecx
	movb	%cl, 1(%rbx)
	movzbl	2(%rdi), %ecx
	movb	%cl, 2(%rbx)
	movzbl	3(%rdi), %ecx
	movb	%cl, 3(%rbx)
	movzbl	4(%rdi), %ecx
	movb	%cl, 4(%rbx)
	movzbl	5(%rdi), %ecx
	movb	%cl, 5(%rbx)
	movzbl	6(%rdi), %ecx
	movb	%cl, 6(%rbx)
	movzbl	7(%rdi), %ecx
	movb	%cl, 7(%rbx)
	addq	$8, %rdi
	addq	$8, %rbx
	addl	$-8, %eax
	jne	.LBB6_17
.LBB6_18:                               # %._crit_edge.loopexit
	movq	(%r8,%r9,8), %r13
.LBB6_19:                               # %._crit_edge
	cmpl	$2, 56(%r13)
	jne	.LBB6_21
# BB#20:                                # %.thread81
	movl	$0, _ZL10yy_n_chars(%rip)
	movl	$0, 28(%r13)
	testl	%r12d, %r12d
	jne	.LBB6_41
	jmp	.LBB6_34
.LBB6_2:
	xorl	%eax, %eax
	cmpq	$1, %rdx
	setne	%al
	incl	%eax
	jmp	.LBB6_43
.LBB6_21:
	movl	24(%r13), %eax
	subl	%r12d, %eax
	decl	%eax
	je	.LBB6_45
# BB#22:
	cmpl	$8192, %eax             # imm = 0x2000
	movl	$8192, %r14d            # imm = 0x2000
	cmovbl	%eax, %r14d
	cmpl	$0, 36(%r13)
	je	.LBB6_35
# BB#23:                                # %.lr.ph61
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_24:                               # =>This Inner Loop Header: Depth=1
	movq	yyin(%rip), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB6_27
# BB#25:                                #   in Loop: Header=BB6_24 Depth=1
	cmpl	$10, %eax
	je	.LBB6_27
# BB#26:                                #   in Loop: Header=BB6_24 Depth=1
	movq	_ZL15yy_buffer_stack(%rip), %rcx
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	8(%rcx), %rcx
	addq	%r12, %rcx
	movb	%al, (%rbx,%rcx)
	incq	%rbx
	cmpq	%r14, %rbx
	jb	.LBB6_24
.LBB6_27:                               # %.thread
	cmpl	$-1, %eax
	je	.LBB6_30
# BB#28:                                # %.thread
	cmpl	$10, %eax
	jne	.LBB6_31
# BB#29:                                # %.thread56
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%r12, %rcx
	addq	8(%rax), %rcx
	movb	$10, (%rbx,%rcx)
	incq	%rbx
	jmp	.LBB6_31
.LBB6_35:
	callq	__errno_location
	movq	%rax, %r15
	movl	$0, (%r15)
	movslq	%r12d, %rbp
	movq	8(%r13), %rdi
	jmp	.LBB6_36
	.p2align	4, 0x90
.LBB6_40:                               #   in Loop: Header=BB6_36 Depth=1
	movl	$0, (%r15)
	movq	%rbx, %rdi
	callq	clearerr
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	8(%rax), %rdi
.LBB6_36:                               # =>This Inner Loop Header: Depth=1
	addq	%rbp, %rdi
	movq	yyin(%rip), %rcx
	movl	$1, %esi
	movq	%r14, %rdx
	callq	fread
	movq	%rax, %rbx
	movl	%ebx, _ZL10yy_n_chars(%rip)
	testl	%ebx, %ebx
	jne	.LBB6_32
# BB#37:                                # %.lr.ph
                                        #   in Loop: Header=BB6_36 Depth=1
	movq	yyin(%rip), %rbx
	movq	%rbx, %rdi
	callq	ferror
	testl	%eax, %eax
	je	.LBB6_38
# BB#39:                                #   in Loop: Header=BB6_36 Depth=1
	cmpl	$4, (%r15)
	je	.LBB6_40
.LBB6_46:
	movl	$.L.str.26, %edi
	callq	_ZL14yy_fatal_errorPKc
.LBB6_30:
	movq	yyin(%rip), %rdi
	callq	ferror
	testl	%eax, %eax
	jne	.LBB6_46
.LBB6_31:
	movl	%ebx, _ZL10yy_n_chars(%rip)
.LBB6_32:                               # %.loopexit
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %r13
	movl	%ebx, 28(%r13)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	jne	.LBB6_42
# BB#33:
	testl	%r12d, %r12d
	je	.LBB6_34
.LBB6_41:
	movl	$2, 56(%r13)
	movl	$2, %eax
	xorl	%ebx, %ebx
	jmp	.LBB6_42
.LBB6_34:
	movq	yyin(%rip), %rdi
	callq	_Z9yyrestartP8_IO_FILE
	movl	_ZL10yy_n_chars(%rip), %ebx
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %r13
	movl	$1, %eax
.LBB6_42:
	addl	%r12d, %ebx
	movl	%ebx, _ZL10yy_n_chars(%rip)
	movq	8(%r13), %rcx
	movslq	%ebx, %rdx
	movb	$0, (%rcx,%rdx)
	movq	_ZL15yy_buffer_stack(%rip), %rcx
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rcx,%rdx,8), %rsi
	movq	8(%rsi), %rsi
	movslq	_ZL10yy_n_chars(%rip), %rdi
	movb	$0, 1(%rsi,%rdi)
	movq	(%rcx,%rdx,8), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, yytext_ptr(%rip)
.LBB6_43:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_9:                                # %vector.body.preheader
	leaq	(%rax,%rsi), %rbx
	addq	$16, %rax
	leaq	(%rcx,%rsi), %rdi
	addq	$16, %rcx
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB6_10:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-32, %rbp
	jne	.LBB6_10
# BB#11:                                # %middle.block
	testq	%r10, %r10
	jne	.LBB6_12
	jmp	.LBB6_18
.LBB6_38:
	xorl	%ebx, %ebx
	jmp	.LBB6_32
.LBB6_44:
	movl	$.L.str.24, %edi
	callq	_ZL14yy_fatal_errorPKc
.LBB6_45:
	movl	$.L.str.25, %edi
	callq	_ZL14yy_fatal_errorPKc
.Lfunc_end6:
	.size	_ZL18yy_get_next_bufferv, .Lfunc_end6-_ZL18yy_get_next_bufferv
	.cfi_endproc

	.globl	_Z9yyrestartP8_IO_FILE
	.p2align	4, 0x90
	.type	_Z9yyrestartP8_IO_FILE,@function
_Z9yyrestartP8_IO_FILE:                 # @_Z9yyrestartP8_IO_FILE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 48
.Lcfi50:
	.cfi_offset %rbx, -40
.Lcfi51:
	.cfi_offset %r12, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	_ZL15yy_buffer_stack(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB7_12
# BB#1:
	movq	_ZL19yy_buffer_stack_top(%rip), %r15
	movq	(%rdi,%r15,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB7_5
# BB#2:                                 # %.critedge
	movq	_ZL19yy_buffer_stack_max(%rip), %rax
	leaq	-1(%rax), %rcx
	cmpq	%rcx, %r15
	jb	.LBB7_4
# BB#3:
	shlq	$32, %rax
	movabsq	$34359738368, %rcx      # imm = 0x800000000
	addq	%rax, %rcx
	movq	%rcx, %rbx
	sarq	$32, %rbx
	shrq	$29, %rcx
	movl	%ecx, %esi
	callq	realloc
	movq	%rax, _ZL15yy_buffer_stack(%rip)
	movq	_ZL19yy_buffer_stack_max(%rip), %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rax,%rcx,8)
	movups	%xmm0, 32(%rax,%rcx,8)
	movups	%xmm0, 16(%rax,%rcx,8)
	movups	%xmm0, (%rax,%rcx,8)
	movq	%rbx, _ZL19yy_buffer_stack_max(%rip)
	jmp	.LBB7_4
.LBB7_12:                               # %.critedge.thread
	movl	$8, %edi
	callq	malloc
	movq	%rax, _ZL15yy_buffer_stack(%rip)
	movq	$0, (%rax)
	movq	$1, _ZL19yy_buffer_stack_max(%rip)
	movq	$0, _ZL19yy_buffer_stack_top(%rip)
.LBB7_4:
	movq	yyin(%rip), %rdi
	movl	$16384, %esi            # imm = 0x4000
	callq	_Z16yy_create_bufferP8_IO_FILEi
	movq	%rax, %rbx
	movq	_ZL15yy_buffer_stack(%rip), %rdi
	movq	_ZL19yy_buffer_stack_top(%rip), %r15
	movq	%rbx, (%rdi,%r15,8)
	testq	%rdi, %rdi
	je	.LBB7_13
.LBB7_5:                                # %.thread1
	leaq	(%rdi,%r15,8), %r12
	callq	__errno_location
	movl	(%rax), %ecx
	testq	%rbx, %rbx
	je	.LBB7_6
# BB#7:
	movl	$0, 28(%rbx)
	movq	8(%rbx), %rdx
	movb	$0, (%rdx)
	movq	8(%rbx), %rdx
	movb	$0, 1(%rdx)
	movq	8(%rbx), %rdx
	movq	%rdx, 16(%rbx)
	movl	$1, 40(%rbx)
	movl	$0, 56(%rbx)
	cmpq	%rbx, (%r12)
	jne	.LBB7_9
# BB#8:                                 # %_Z15yy_flush_bufferP15yy_buffer_state.exit.thread.i
	movq	(%r12), %rdx
	movl	28(%rdx), %esi
	movl	%esi, _ZL10yy_n_chars(%rip)
	movq	16(%rdx), %rdx
	movq	%rdx, _ZL10yy_c_buf_p(%rip)
	movq	%rdx, yytext_ptr(%rip)
	movq	(%r12), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, yyin(%rip)
	movb	(%rdx), %dl
	movb	%dl, _ZL12yy_hold_char(%rip)
	jmp	.LBB7_9
.LBB7_6:
	xorl	%ebx, %ebx
.LBB7_9:                                # %_Z15yy_flush_bufferP15yy_buffer_state.exit.i
	movq	%r14, (%rbx)
	movl	$1, 52(%rbx)
	cmpq	%rbx, (%r12)
	je	.LBB7_11
# BB#10:
	movq	$1, 44(%rbx)
.LBB7_11:                               # %_ZL14yy_init_bufferP15yy_buffer_stateP8_IO_FILE.exit
	movl	$0, 36(%rbx)
	movl	%ecx, (%rax)
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	(%rax,%r15,8), %rcx
	movl	28(%rcx), %edx
	movl	%edx, _ZL10yy_n_chars(%rip)
	movq	16(%rcx), %rcx
	movq	%rcx, _ZL10yy_c_buf_p(%rip)
	movq	%rcx, yytext_ptr(%rip)
	movq	(%rax,%r15,8), %rax
	movq	(%rax), %rax
	movq	%rax, yyin(%rip)
	movb	(%rcx), %al
	movb	%al, _ZL12yy_hold_char(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB7_13:                               # %.thread5
	ud2
.Lfunc_end7:
	.size	_Z9yyrestartP8_IO_FILE, .Lfunc_end7-_Z9yyrestartP8_IO_FILE
	.cfi_endproc

	.globl	_Z19yy_switch_to_bufferP15yy_buffer_state
	.p2align	4, 0x90
	.type	_Z19yy_switch_to_bufferP15yy_buffer_state,@function
_Z19yy_switch_to_bufferP15yy_buffer_state: # @_Z19yy_switch_to_bufferP15yy_buffer_state
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -24
.Lcfi58:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	_ZL15yy_buffer_stack(%rip), %rax
	testq	%rax, %rax
	je	.LBB8_1
# BB#2:
	movq	_ZL19yy_buffer_stack_max(%rip), %rcx
	leaq	-1(%rcx), %rdx
	cmpq	%rdx, _ZL19yy_buffer_stack_top(%rip)
	jb	.LBB8_6
# BB#3:
	shlq	$32, %rcx
	movabsq	$34359738368, %rdx      # imm = 0x800000000
	addq	%rcx, %rdx
	movq	%rdx, %r14
	sarq	$32, %r14
	shrq	$29, %rdx
	movl	%edx, %esi
	movq	%rax, %rdi
	callq	realloc
	movq	%rax, _ZL15yy_buffer_stack(%rip)
	movq	_ZL19yy_buffer_stack_max(%rip), %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rax,%rcx,8)
	movups	%xmm0, 32(%rax,%rcx,8)
	movups	%xmm0, 16(%rax,%rcx,8)
	movups	%xmm0, (%rax,%rcx,8)
	movq	%r14, _ZL19yy_buffer_stack_max(%rip)
	testq	%rax, %rax
	jne	.LBB8_6
	jmp	.LBB8_5
.LBB8_1:
	movl	$8, %edi
	callq	malloc
	movq	%rax, _ZL15yy_buffer_stack(%rip)
	movq	$0, (%rax)
	movq	$1, _ZL19yy_buffer_stack_max(%rip)
	movq	$0, _ZL19yy_buffer_stack_top(%rip)
	testq	%rax, %rax
	je	.LBB8_5
.LBB8_6:                                # %_ZL21yyensure_buffer_stackv.exit.thread
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %rcx
	cmpq	%rbx, %rcx
	jne	.LBB8_8
	jmp	.LBB8_12
.LBB8_5:
	xorl	%ecx, %ecx
	cmpq	%rbx, %rcx
	je	.LBB8_12
.LBB8_8:
	testq	%rax, %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	je	.LBB8_11
# BB#9:
	cmpq	$0, (%rax,%rcx,8)
	je	.LBB8_11
# BB#10:
	movb	_ZL12yy_hold_char(%rip), %dl
	movq	_ZL10yy_c_buf_p(%rip), %rsi
	movb	%dl, (%rsi)
	movq	(%rax,%rcx,8), %rdx
	movq	%rsi, 16(%rdx)
	movl	_ZL10yy_n_chars(%rip), %edx
	movq	(%rax,%rcx,8), %rsi
	movl	%edx, 28(%rsi)
.LBB8_11:                               # %.critedge
	movq	%rbx, (%rax,%rcx,8)
	movl	28(%rbx), %edx
	movl	%edx, _ZL10yy_n_chars(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, _ZL10yy_c_buf_p(%rip)
	movq	%rdx, yytext_ptr(%rip)
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	movq	%rax, yyin(%rip)
	movb	(%rdx), %al
	movb	%al, _ZL12yy_hold_char(%rip)
.LBB8_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	_Z19yy_switch_to_bufferP15yy_buffer_state, .Lfunc_end8-_Z19yy_switch_to_bufferP15yy_buffer_state
	.cfi_endproc

	.globl	_Z16yy_delete_bufferP15yy_buffer_state
	.p2align	4, 0x90
	.type	_Z16yy_delete_bufferP15yy_buffer_state,@function
_Z16yy_delete_bufferP15yy_buffer_state: # @_Z16yy_delete_bufferP15yy_buffer_state
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 16
.Lcfi60:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB9_9
# BB#1:
	movq	_ZL15yy_buffer_stack(%rip), %rax
	testq	%rax, %rax
	je	.LBB9_2
# BB#3:
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %rcx
	cmpq	%rbx, %rcx
	jne	.LBB9_6
	jmp	.LBB9_5
.LBB9_9:
	popq	%rbx
	retq
.LBB9_2:
	xorl	%ecx, %ecx
	cmpq	%rbx, %rcx
	jne	.LBB9_6
.LBB9_5:
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	$0, (%rax,%rcx,8)
.LBB9_6:
	cmpl	$0, 32(%rbx)
	je	.LBB9_8
# BB#7:
	movq	8(%rbx), %rdi
	callq	free
.LBB9_8:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end9:
	.size	_Z16yy_delete_bufferP15yy_buffer_state, .Lfunc_end9-_Z16yy_delete_bufferP15yy_buffer_state
	.cfi_endproc

	.globl	_Z6yyfreePv
	.p2align	4, 0x90
	.type	_Z6yyfreePv,@function
_Z6yyfreePv:                            # @_Z6yyfreePv
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end10:
	.size	_Z6yyfreePv, .Lfunc_end10-_Z6yyfreePv
	.cfi_endproc

	.globl	_Z15yy_flush_bufferP15yy_buffer_state
	.p2align	4, 0x90
	.type	_Z15yy_flush_bufferP15yy_buffer_state,@function
_Z15yy_flush_bufferP15yy_buffer_state:  # @_Z15yy_flush_bufferP15yy_buffer_state
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB11_6
# BB#1:
	movl	$0, 28(%rdi)
	movq	8(%rdi), %rax
	movb	$0, (%rax)
	movq	8(%rdi), %rax
	movb	$0, 1(%rax)
	movq	8(%rdi), %rax
	movq	%rax, 16(%rdi)
	movl	$1, 40(%rdi)
	movl	$0, 56(%rdi)
	movq	_ZL15yy_buffer_stack(%rip), %rax
	testq	%rax, %rax
	je	.LBB11_2
# BB#3:
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %rcx
	cmpq	%rdi, %rcx
	jne	.LBB11_6
	jmp	.LBB11_5
.LBB11_2:
	xorl	%ecx, %ecx
	cmpq	%rdi, %rcx
	jne	.LBB11_6
.LBB11_5:
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %rdx
	movl	28(%rdx), %esi
	movl	%esi, _ZL10yy_n_chars(%rip)
	movq	16(%rdx), %rdx
	movq	%rdx, _ZL10yy_c_buf_p(%rip)
	movq	%rdx, yytext_ptr(%rip)
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	movq	%rax, yyin(%rip)
	movb	(%rdx), %al
	movb	%al, _ZL12yy_hold_char(%rip)
.LBB11_6:
	retq
.Lfunc_end11:
	.size	_Z15yy_flush_bufferP15yy_buffer_state, .Lfunc_end11-_Z15yy_flush_bufferP15yy_buffer_state
	.cfi_endproc

	.globl	_Z19yypush_buffer_stateP15yy_buffer_state
	.p2align	4, 0x90
	.type	_Z19yypush_buffer_stateP15yy_buffer_state,@function
_Z19yypush_buffer_stateP15yy_buffer_state: # @_Z19yypush_buffer_stateP15yy_buffer_state
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 32
.Lcfi64:
	.cfi_offset %rbx, -24
.Lcfi65:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB12_13
# BB#1:
	movq	_ZL15yy_buffer_stack(%rip), %rax
	testq	%rax, %rax
	je	.LBB12_2
# BB#3:
	movq	_ZL19yy_buffer_stack_max(%rip), %rcx
	leaq	-1(%rcx), %rdx
	cmpq	%rdx, _ZL19yy_buffer_stack_top(%rip)
	jb	.LBB12_7
# BB#4:
	shlq	$32, %rcx
	movabsq	$34359738368, %rdx      # imm = 0x800000000
	addq	%rcx, %rdx
	movq	%rdx, %r14
	sarq	$32, %r14
	shrq	$29, %rdx
	movl	%edx, %esi
	movq	%rax, %rdi
	callq	realloc
	movq	%rax, _ZL15yy_buffer_stack(%rip)
	movq	_ZL19yy_buffer_stack_max(%rip), %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rax,%rcx,8)
	movups	%xmm0, 32(%rax,%rcx,8)
	movups	%xmm0, 16(%rax,%rcx,8)
	movups	%xmm0, (%rax,%rcx,8)
	movq	%r14, _ZL19yy_buffer_stack_max(%rip)
	testq	%rax, %rax
	jne	.LBB12_7
	jmp	.LBB12_6
.LBB12_2:
	movl	$8, %edi
	callq	malloc
	movq	%rax, _ZL15yy_buffer_stack(%rip)
	movq	$0, (%rax)
	movq	$1, _ZL19yy_buffer_stack_max(%rip)
	movq	$0, _ZL19yy_buffer_stack_top(%rip)
	testq	%rax, %rax
	je	.LBB12_6
.LBB12_7:                               # %_ZL21yyensure_buffer_stackv.exit.thread
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	cmpq	$0, (%rax,%rcx,8)
	je	.LBB12_9
# BB#8:
	movb	_ZL12yy_hold_char(%rip), %dl
	movq	_ZL10yy_c_buf_p(%rip), %rsi
	movb	%dl, (%rsi)
	movq	(%rax,%rcx,8), %rdx
	movq	%rsi, 16(%rdx)
	movl	_ZL10yy_n_chars(%rip), %edx
	movq	(%rax,%rcx,8), %rsi
	movl	%edx, 28(%rsi)
.LBB12_9:                               # %.critedge
	testq	%rax, %rax
	je	.LBB12_12
# BB#10:
	cmpq	$0, (%rax,%rcx,8)
	je	.LBB12_12
# BB#11:
	incq	%rcx
	movq	%rcx, _ZL19yy_buffer_stack_top(%rip)
	jmp	.LBB12_12
.LBB12_6:                               # %_ZL21yyensure_buffer_stackv.exit..critedge3_crit_edge
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
.LBB12_12:                              # %.critedge3
	movq	%rbx, (%rax,%rcx,8)
	movl	28(%rbx), %edx
	movl	%edx, _ZL10yy_n_chars(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, _ZL10yy_c_buf_p(%rip)
	movq	%rdx, yytext_ptr(%rip)
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	movq	%rax, yyin(%rip)
	movb	(%rdx), %al
	movb	%al, _ZL12yy_hold_char(%rip)
.LBB12_13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	_Z19yypush_buffer_stateP15yy_buffer_state, .Lfunc_end12-_Z19yypush_buffer_stateP15yy_buffer_state
	.cfi_endproc

	.globl	_Z18yypop_buffer_statev
	.p2align	4, 0x90
	.type	_Z18yypop_buffer_statev,@function
_Z18yypop_buffer_statev:                # @_Z18yypop_buffer_statev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 16
.Lcfi67:
	.cfi_offset %rbx, -16
	movq	_ZL15yy_buffer_stack(%rip), %rax
	testq	%rax, %rax
	je	.LBB13_10
# BB#1:
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB13_10
# BB#2:
	movq	$0, (%rax,%rcx,8)
	cmpl	$0, 32(%rbx)
	je	.LBB13_4
# BB#3:
	movq	8(%rbx), %rdi
	callq	free
.LBB13_4:                               # %_Z16yy_delete_bufferP15yy_buffer_state.exit
	movq	%rbx, %rdi
	callq	free
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	$0, (%rax,%rcx,8)
	testq	%rcx, %rcx
	je	.LBB13_5
# BB#6:
	decq	%rcx
	movq	%rcx, _ZL19yy_buffer_stack_top(%rip)
	testq	%rax, %rax
	jne	.LBB13_8
	jmp	.LBB13_10
.LBB13_5:
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.LBB13_10
.LBB13_8:
	movq	(%rax,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.LBB13_10
# BB#9:
	movl	28(%rdx), %esi
	movl	%esi, _ZL10yy_n_chars(%rip)
	movq	16(%rdx), %rdx
	movq	%rdx, _ZL10yy_c_buf_p(%rip)
	movq	%rdx, yytext_ptr(%rip)
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	movq	%rax, yyin(%rip)
	movb	(%rdx), %al
	movb	%al, _ZL12yy_hold_char(%rip)
.LBB13_10:                              # %.critedge
	popq	%rbx
	retq
.Lfunc_end13:
	.size	_Z18yypop_buffer_statev, .Lfunc_end13-_Z18yypop_buffer_statev
	.cfi_endproc

	.globl	_Z14yy_scan_bufferPcj
	.p2align	4, 0x90
	.type	_Z14yy_scan_bufferPcj,@function
_Z14yy_scan_bufferPcj:                  # @_Z14yy_scan_bufferPcj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -32
.Lcfi72:
	.cfi_offset %r14, -24
.Lcfi73:
	.cfi_offset %r15, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r14
	cmpl	$2, %esi
	jae	.LBB14_2
# BB#1:
	xorl	%ebx, %ebx
	jmp	.LBB14_8
.LBB14_2:
	leal	-2(%rsi), %r15d
	cmpb	$0, (%r14,%r15)
	je	.LBB14_4
# BB#3:
	xorl	%ebx, %ebx
	jmp	.LBB14_8
.LBB14_4:
	decl	%esi
	cmpb	$0, (%r14,%rsi)
	je	.LBB14_6
# BB#5:
	xorl	%ebx, %ebx
	jmp	.LBB14_8
.LBB14_6:
	movl	$64, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB14_9
# BB#7:
	movl	%r15d, 24(%rbx)
	movq	%r14, 8(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 32(%rbx)
	movq	$0, (%rbx)
	movl	%r15d, 28(%rbx)
	movl	$0, 36(%rbx)
	movl	$1, 40(%rbx)
	movl	$0, 52(%rbx)
	movl	$0, 56(%rbx)
	movq	%rbx, %rdi
	callq	_Z19yy_switch_to_bufferP15yy_buffer_state
.LBB14_8:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB14_9:
	movl	$.L.str.21, %edi
	callq	_ZL14yy_fatal_errorPKc
.Lfunc_end14:
	.size	_Z14yy_scan_bufferPcj, .Lfunc_end14-_Z14yy_scan_bufferPcj
	.cfi_endproc

	.globl	_Z14yy_scan_stringPKc
	.p2align	4, 0x90
	.type	_Z14yy_scan_stringPKc,@function
_Z14yy_scan_stringPKc:                  # @_Z14yy_scan_stringPKc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 16
.Lcfi75:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	strlen
	movq	%rbx, %rdi
	movl	%eax, %esi
	popq	%rbx
	jmp	_Z13yy_scan_bytesPKci   # TAILCALL
.Lfunc_end15:
	.size	_Z14yy_scan_stringPKc, .Lfunc_end15-_Z14yy_scan_stringPKc
	.cfi_endproc

	.globl	_Z13yy_scan_bytesPKci
	.p2align	4, 0x90
	.type	_Z13yy_scan_bytesPKci,@function
_Z13yy_scan_bytesPKci:                  # @_Z13yy_scan_bytesPKci
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 48
.Lcfi81:
	.cfi_offset %rbx, -40
.Lcfi82:
	.cfi_offset %r12, -32
.Lcfi83:
	.cfi_offset %r14, -24
.Lcfi84:
	.cfi_offset %r15, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	leal	2(%r14), %edi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB16_9
# BB#1:                                 # %.preheader
	testl	%r14d, %r14d
	jle	.LBB16_3
# BB#2:                                 # %.lr.ph.preheader
	leal	-1(%r14), %edx
	incq	%rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	memcpy
.LBB16_3:                               # %._crit_edge
	leal	1(%r14), %eax
	movslq	%eax, %rcx
	movb	$0, (%r12,%rcx)
	movslq	%r14d, %rcx
	movb	$0, (%r12,%rcx)
	cmpl	$-3, %r14d
	ja	.LBB16_8
# BB#4:
	movl	%r14d, %ecx
	cmpb	$0, (%r12,%rcx)
	jne	.LBB16_8
# BB#5:
	movl	%eax, %eax
	cmpb	$0, (%r12,%rax)
	jne	.LBB16_8
# BB#6:
	movl	$64, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB16_10
# BB#7:                                 # %_Z14yy_scan_bufferPcj.exit
	movl	%r14d, 24(%rbx)
	movq	%r12, 8(%rbx)
	movq	%r12, 16(%rbx)
	movl	$0, 32(%rbx)
	movq	$0, (%rbx)
	movl	%r14d, 28(%rbx)
	movl	$0, 36(%rbx)
	movl	$1, 40(%rbx)
	movl	$0, 52(%rbx)
	movl	$0, 56(%rbx)
	movq	%rbx, %rdi
	callq	_Z19yy_switch_to_bufferP15yy_buffer_state
	movl	$1, 32(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB16_8:                               # %_Z14yy_scan_bufferPcj.exit.thread
	movl	$.L.str.23, %edi
	callq	_ZL14yy_fatal_errorPKc
.LBB16_9:
	movl	$.L.str.22, %edi
	callq	_ZL14yy_fatal_errorPKc
.LBB16_10:
	movl	$.L.str.21, %edi
	callq	_ZL14yy_fatal_errorPKc
.Lfunc_end16:
	.size	_Z13yy_scan_bytesPKci, .Lfunc_end16-_Z13yy_scan_bytesPKci
	.cfi_endproc

	.globl	_Z12yyget_linenov
	.p2align	4, 0x90
	.type	_Z12yyget_linenov,@function
_Z12yyget_linenov:                      # @_Z12yyget_linenov
	.cfi_startproc
# BB#0:
	movl	yylineno(%rip), %eax
	retq
.Lfunc_end17:
	.size	_Z12yyget_linenov, .Lfunc_end17-_Z12yyget_linenov
	.cfi_endproc

	.globl	_Z8yyget_inv
	.p2align	4, 0x90
	.type	_Z8yyget_inv,@function
_Z8yyget_inv:                           # @_Z8yyget_inv
	.cfi_startproc
# BB#0:
	movq	yyin(%rip), %rax
	retq
.Lfunc_end18:
	.size	_Z8yyget_inv, .Lfunc_end18-_Z8yyget_inv
	.cfi_endproc

	.globl	_Z9yyget_outv
	.p2align	4, 0x90
	.type	_Z9yyget_outv,@function
_Z9yyget_outv:                          # @_Z9yyget_outv
	.cfi_startproc
# BB#0:
	movq	yyout(%rip), %rax
	retq
.Lfunc_end19:
	.size	_Z9yyget_outv, .Lfunc_end19-_Z9yyget_outv
	.cfi_endproc

	.globl	_Z10yyget_lengv
	.p2align	4, 0x90
	.type	_Z10yyget_lengv,@function
_Z10yyget_lengv:                        # @_Z10yyget_lengv
	.cfi_startproc
# BB#0:
	movl	yyleng(%rip), %eax
	retq
.Lfunc_end20:
	.size	_Z10yyget_lengv, .Lfunc_end20-_Z10yyget_lengv
	.cfi_endproc

	.globl	_Z10yyget_textv
	.p2align	4, 0x90
	.type	_Z10yyget_textv,@function
_Z10yyget_textv:                        # @_Z10yyget_textv
	.cfi_startproc
# BB#0:
	movl	$yytext, %eax
	retq
.Lfunc_end21:
	.size	_Z10yyget_textv, .Lfunc_end21-_Z10yyget_textv
	.cfi_endproc

	.globl	_Z12yyset_linenoi
	.p2align	4, 0x90
	.type	_Z12yyset_linenoi,@function
_Z12yyset_linenoi:                      # @_Z12yyset_linenoi
	.cfi_startproc
# BB#0:
	movl	%edi, yylineno(%rip)
	retq
.Lfunc_end22:
	.size	_Z12yyset_linenoi, .Lfunc_end22-_Z12yyset_linenoi
	.cfi_endproc

	.globl	_Z8yyset_inP8_IO_FILE
	.p2align	4, 0x90
	.type	_Z8yyset_inP8_IO_FILE,@function
_Z8yyset_inP8_IO_FILE:                  # @_Z8yyset_inP8_IO_FILE
	.cfi_startproc
# BB#0:
	movq	%rdi, yyin(%rip)
	retq
.Lfunc_end23:
	.size	_Z8yyset_inP8_IO_FILE, .Lfunc_end23-_Z8yyset_inP8_IO_FILE
	.cfi_endproc

	.globl	_Z9yyset_outP8_IO_FILE
	.p2align	4, 0x90
	.type	_Z9yyset_outP8_IO_FILE,@function
_Z9yyset_outP8_IO_FILE:                 # @_Z9yyset_outP8_IO_FILE
	.cfi_startproc
# BB#0:
	movq	%rdi, yyout(%rip)
	retq
.Lfunc_end24:
	.size	_Z9yyset_outP8_IO_FILE, .Lfunc_end24-_Z9yyset_outP8_IO_FILE
	.cfi_endproc

	.globl	_Z11yyget_debugv
	.p2align	4, 0x90
	.type	_Z11yyget_debugv,@function
_Z11yyget_debugv:                       # @_Z11yyget_debugv
	.cfi_startproc
# BB#0:
	movl	yy_flex_debug(%rip), %eax
	retq
.Lfunc_end25:
	.size	_Z11yyget_debugv, .Lfunc_end25-_Z11yyget_debugv
	.cfi_endproc

	.globl	_Z11yyset_debugi
	.p2align	4, 0x90
	.type	_Z11yyset_debugi,@function
_Z11yyset_debugi:                       # @_Z11yyset_debugi
	.cfi_startproc
# BB#0:
	movl	%edi, yy_flex_debug(%rip)
	retq
.Lfunc_end26:
	.size	_Z11yyset_debugi, .Lfunc_end26-_Z11yyset_debugi
	.cfi_endproc

	.globl	_Z13yylex_destroyv
	.p2align	4, 0x90
	.type	_Z13yylex_destroyv,@function
_Z13yylex_destroyv:                     # @_Z13yylex_destroyv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 16
.Lcfi86:
	.cfi_offset %rbx, -16
	movq	_ZL15yy_buffer_stack(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB27_2
	jmp	.LBB27_10
	.p2align	4, 0x90
.LBB27_6:                               # %_Z16yy_delete_bufferP15yy_buffer_state.exit.thread11
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	$0, (%rdi,%rax,8)
.LBB27_1:                               #   in Loop: Header=BB27_2 Depth=1
	testq	%rdi, %rdi
	je	.LBB27_10
.LBB27_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	_ZL19yy_buffer_stack_top(%rip), %rax
	cmpq	$0, (%rdi,%rax,8)
	je	.LBB27_11
# BB#3:                                 #   in Loop: Header=BB27_2 Depth=1
	testq	%rdi, %rdi
	je	.LBB27_4
# BB#5:                                 #   in Loop: Header=BB27_2 Depth=1
	movq	(%rdi,%rax,8), %rbx
	testq	%rbx, %rbx
	je	.LBB27_6
# BB#7:                                 #   in Loop: Header=BB27_2 Depth=1
	movq	$0, (%rdi,%rax,8)
	cmpl	$0, 32(%rbx)
	je	.LBB27_9
# BB#8:                                 #   in Loop: Header=BB27_2 Depth=1
	movq	8(%rbx), %rdi
	callq	free
.LBB27_9:                               # %_Z16yy_delete_bufferP15yy_buffer_state.exit
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	%rbx, %rdi
	callq	free
	movq	_ZL15yy_buffer_stack(%rip), %rdi
	movq	_ZL19yy_buffer_stack_top(%rip), %rax
	movq	$0, (%rdi,%rax,8)
	testq	%rdi, %rdi
	jne	.LBB27_1
	jmp	.LBB27_10
.LBB27_4:                               # %_Z16yy_delete_bufferP15yy_buffer_state.exit.thread
	movq	$0, (,%rax,8)
.LBB27_10:                              # %.thread
	movq	_ZL15yy_buffer_stack(%rip), %rdi
.LBB27_11:                              # %.loopexit
	callq	free
	movq	$0, _ZL15yy_buffer_stack(%rip)
	movq	_ZL12yy_state_buf(%rip), %rdi
	callq	free
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end27:
	.size	_Z13yylex_destroyv, .Lfunc_end27-_Z13yylex_destroyv
	.cfi_endproc

	.globl	_Z9yyreallocPvj
	.p2align	4, 0x90
	.type	_Z9yyreallocPvj,@function
_Z9yyreallocPvj:                        # @_Z9yyreallocPvj
	.cfi_startproc
# BB#0:
	movl	%esi, %esi
	jmp	realloc                 # TAILCALL
.Lfunc_end28:
	.size	_Z9yyreallocPvj, .Lfunc_end28-_Z9yyreallocPvj
	.cfi_endproc

	.globl	_Z9do_NORMALv
	.p2align	4, 0x90
	.type	_Z9do_NORMALv,@function
_Z9do_NORMALv:                          # @_Z9do_NORMALv
	.cfi_startproc
# BB#0:
	movl	$3, _ZL8yy_start(%rip)
	jmp	_ZN12_GLOBAL__N_115reset_nestcountEv # TAILCALL
.Lfunc_end29:
	.size	_Z9do_NORMALv, .Lfunc_end29-_Z9do_NORMALv
	.cfi_endproc

	.globl	_Z8do_CEXPRv
	.p2align	4, 0x90
	.type	_Z8do_CEXPRv,@function
_Z8do_CEXPRv:                           # @_Z8do_CEXPRv
	.cfi_startproc
# BB#0:
	movl	$9, _ZL8yy_start(%rip)
	retq
.Lfunc_end30:
	.size	_Z8do_CEXPRv, .Lfunc_end30-_Z8do_CEXPRv
	.cfi_endproc

	.globl	_Z10do_CEXPRDQv
	.p2align	4, 0x90
	.type	_Z10do_CEXPRDQv,@function
_Z10do_CEXPRDQv:                        # @_Z10do_CEXPRDQv
	.cfi_startproc
# BB#0:
	movl	$11, _ZL8yy_start(%rip)
	retq
.Lfunc_end31:
	.size	_Z10do_CEXPRDQv, .Lfunc_end31-_Z10do_CEXPRDQv
	.cfi_endproc

	.globl	_Z10do_CEXPRSQv
	.p2align	4, 0x90
	.type	_Z10do_CEXPRSQv,@function
_Z10do_CEXPRSQv:                        # @_Z10do_CEXPRSQv
	.cfi_startproc
# BB#0:
	movl	$13, _ZL8yy_start(%rip)
	retq
.Lfunc_end32:
	.size	_Z10do_CEXPRSQv, .Lfunc_end32-_Z10do_CEXPRSQv
	.cfi_endproc

	.globl	_Z4do_Cv
	.p2align	4, 0x90
	.type	_Z4do_Cv,@function
_Z4do_Cv:                               # @_Z4do_Cv
	.cfi_startproc
# BB#0:
	movl	$15, _ZL8yy_start(%rip)
	retq
.Lfunc_end33:
	.size	_Z4do_Cv, .Lfunc_end33-_Z4do_Cv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL7yyinputv,@function
_ZL7yyinputv:                           # @_ZL7yyinputv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 32
.Lcfi90:
	.cfi_offset %rbx, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movb	_ZL12yy_hold_char(%rip), %al
	movq	_ZL10yy_c_buf_p(%rip), %rbx
	movb	%al, (%rbx)
	testb	%al, %al
	jne	.LBB34_9
# BB#1:
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	_ZL10yy_n_chars(%rip), %rcx
	addq	8(%rax), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB34_3
# BB#2:
	movb	$0, (%rbx)
	jmp	.LBB34_9
.LBB34_3:
	movq	yytext_ptr(%rip), %rbp
	leaq	1(%rbx), %rax
	movq	%rax, _ZL10yy_c_buf_p(%rip)
	callq	_ZL18yy_get_next_bufferv
	testl	%eax, %eax
	je	.LBB34_8
# BB#4:
	movl	$-1, %ebp
	cmpl	$1, %eax
	je	.LBB34_11
# BB#5:
	cmpl	$2, %eax
	jne	.LBB34_6
# BB#7:
	movq	yyin(%rip), %rdi
	callq	_Z9yyrestartP8_IO_FILE
	jmp	.LBB34_11
.LBB34_8:
	subl	%ebp, %ebx
	movslq	%ebx, %rbx
	addq	yytext_ptr(%rip), %rbx
	movq	%rbx, _ZL10yy_c_buf_p(%rip)
	jmp	.LBB34_9
.LBB34_6:                               # %..critedge_crit_edge
	movq	_ZL10yy_c_buf_p(%rip), %rbx
.LBB34_9:                               # %.critedge
	movzbl	(%rbx), %ebp
	leaq	1(%rbx), %rax
	xorl	%ecx, %ecx
	cmpl	$10, %ebp
	sete	%cl
	movb	$0, (%rbx)
	movq	%rax, _ZL10yy_c_buf_p(%rip)
	movb	1(%rbx), %al
	movb	%al, _ZL12yy_hold_char(%rip)
	movq	_ZL15yy_buffer_stack(%rip), %rax
	movq	_ZL19yy_buffer_stack_top(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%ecx, 40(%rax)
	jne	.LBB34_11
# BB#10:
	incl	yylineno(%rip)
.LBB34_11:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end34:
	.size	_ZL7yyinputv, .Lfunc_end34-_ZL7yyinputv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_kimwl.ii,@function
_GLOBAL__sub_I_kimwl.ii:                # @_GLOBAL__sub_I_kimwl.ii
	.cfi_startproc
# BB#0:
	movq	$_ZN12_GLOBAL__N_18cincludeB5cxx11E+16, _ZN12_GLOBAL__N_18cincludeB5cxx11E(%rip)
	movq	$0, _ZN12_GLOBAL__N_18cincludeB5cxx11E+8(%rip)
	movb	$0, _ZN12_GLOBAL__N_18cincludeB5cxx11E+16(%rip)
	movl	$_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev, %edi
	movl	$_ZN12_GLOBAL__N_18cincludeB5cxx11E, %esi
	movl	$__dso_handle, %edx
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end35:
	.size	_GLOBAL__sub_I_kimwl.ii, .Lfunc_end35-_GLOBAL__sub_I_kimwl.ii
	.cfi_endproc

	.type	yyleng,@object          # @yyleng
	.bss
	.globl	yyleng
	.p2align	2
yyleng:
	.long	0                       # 0x0
	.size	yyleng, 4

	.type	yyin,@object            # @yyin
	.globl	yyin
	.p2align	3
yyin:
	.quad	0
	.size	yyin, 8

	.type	yyout,@object           # @yyout
	.globl	yyout
	.p2align	3
yyout:
	.quad	0
	.size	yyout, 8

	.type	yylineno,@object        # @yylineno
	.data
	.globl	yylineno
	.p2align	2
yylineno:
	.long	1                       # 0x1
	.size	yylineno, 4

	.type	yy_flex_debug,@object   # @yy_flex_debug
	.bss
	.globl	yy_flex_debug
	.p2align	2
yy_flex_debug:
	.long	0                       # 0x0
	.size	yy_flex_debug, 4

	.type	yytext,@object          # @yytext
	.globl	yytext
	.p2align	4
yytext:
	.zero	8192
	.size	yytext, 8192

	.type	yytext_ptr,@object      # @yytext_ptr
	.globl	yytext_ptr
	.p2align	3
yytext_ptr:
	.quad	0
	.size	yytext_ptr, 8

	.type	_ZN12_GLOBAL__N_18cincludeB5cxx11E,@object # @_ZN12_GLOBAL__N_18cincludeB5cxx11E
	.local	_ZN12_GLOBAL__N_18cincludeB5cxx11E
	.comm	_ZN12_GLOBAL__N_18cincludeB5cxx11E,32,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.zero	1
	.size	.L.str, 1

	.type	_ZL7yy_init,@object     # @_ZL7yy_init
	.local	_ZL7yy_init
	.comm	_ZL7yy_init,1,4
	.type	_ZL12yy_state_buf,@object # @_ZL12yy_state_buf
	.local	_ZL12yy_state_buf
	.comm	_ZL12yy_state_buf,8,8
	.type	_ZL8yy_start,@object    # @_ZL8yy_start
	.local	_ZL8yy_start
	.comm	_ZL8yy_start,4,4
	.type	_ZL15yy_buffer_stack,@object # @_ZL15yy_buffer_stack
	.local	_ZL15yy_buffer_stack
	.comm	_ZL15yy_buffer_stack,8,8
	.type	_ZL19yy_buffer_stack_top,@object # @_ZL19yy_buffer_stack_top
	.local	_ZL19yy_buffer_stack_top
	.comm	_ZL19yy_buffer_stack_top,8,8
	.type	_ZL10yy_c_buf_p,@object # @_ZL10yy_c_buf_p
	.local	_ZL10yy_c_buf_p
	.comm	_ZL10yy_c_buf_p,8,8
	.type	_ZL12yy_hold_char,@object # @_ZL12yy_hold_char
	.local	_ZL12yy_hold_char
	.comm	_ZL12yy_hold_char,1,1
	.type	_ZL12yy_state_ptr,@object # @_ZL12yy_state_ptr
	.local	_ZL12yy_state_ptr
	.comm	_ZL12yy_state_ptr,8,8
	.type	_ZL5yy_ec,@object       # @_ZL5yy_ec
	.section	.rodata,"a",@progbits
	.p2align	4
_ZL5yy_ec:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	29                      # 0x1d
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	30                      # 0x1e
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	31                      # 0x1f
	.long	32                      # 0x20
	.long	33                      # 0x21
	.long	34                      # 0x22
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	36                      # 0x24
	.long	37                      # 0x25
	.long	38                      # 0x26
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	39                      # 0x27
	.long	26                      # 0x1a
	.long	40                      # 0x28
	.long	41                      # 0x29
	.long	42                      # 0x2a
	.long	43                      # 0x2b
	.long	44                      # 0x2c
	.long	1                       # 0x1
	.long	45                      # 0x2d
	.long	46                      # 0x2e
	.long	47                      # 0x2f
	.long	48                      # 0x30
	.long	49                      # 0x31
	.long	50                      # 0x32
	.long	51                      # 0x33
	.long	52                      # 0x34
	.long	53                      # 0x35
	.long	26                      # 0x1a
	.long	54                      # 0x36
	.long	55                      # 0x37
	.long	56                      # 0x38
	.long	57                      # 0x39
	.long	58                      # 0x3a
	.long	59                      # 0x3b
	.long	26                      # 0x1a
	.long	60                      # 0x3c
	.long	61                      # 0x3d
	.long	62                      # 0x3e
	.long	63                      # 0x3f
	.long	64                      # 0x40
	.long	65                      # 0x41
	.long	66                      # 0x42
	.long	67                      # 0x43
	.long	26                      # 0x1a
	.long	68                      # 0x44
	.long	69                      # 0x45
	.long	70                      # 0x46
	.long	71                      # 0x47
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	_ZL5yy_ec, 1024

	.type	_ZL6yy_chk,@object      # @_ZL6yy_chk
	.p2align	4
_ZL6yy_chk:
	.short	0                       # 0x0
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	6                       # 0x6
	.short	28                      # 0x1c
	.short	91                      # 0x5b
	.short	91                      # 0x5b
	.short	28                      # 0x1c
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	9                       # 0x9
	.short	19                      # 0x13
	.short	19                      # 0x13
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	431                     # 0x1af
	.short	84                      # 0x54
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	9                       # 0x9
	.short	11                      # 0xb
	.short	71                      # 0x47
	.short	9                       # 0x9
	.short	11                      # 0xb
	.short	401                     # 0x191
	.short	9                       # 0x9
	.short	12                      # 0xc
	.short	84                      # 0x54
	.short	13                      # 0xd
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	30                      # 0x1e
	.short	27                      # 0x1b
	.short	32                      # 0x20
	.short	47                      # 0x2f
	.short	13                      # 0xd
	.short	30                      # 0x1e
	.short	14                      # 0xe
	.short	32                      # 0x20
	.short	47                      # 0x2f
	.short	51                      # 0x33
	.short	401                     # 0x191
	.short	51                      # 0x33
	.short	61                      # 0x3d
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	61                      # 0x3d
	.short	347                     # 0x15b
	.short	10                      # 0xa
	.short	10                      # 0xa
	.short	71                      # 0x47
	.short	426                     # 0x1aa
	.short	10                      # 0xa
	.short	10                      # 0xa
	.short	10                      # 0xa
	.short	425                     # 0x1a9
	.short	11                      # 0xb
	.short	10                      # 0xa
	.short	347                     # 0x15b
	.short	87                      # 0x57
	.short	10                      # 0xa
	.short	19                      # 0x13
	.short	12                      # 0xc
	.short	10                      # 0xa
	.short	13                      # 0xd
	.short	19                      # 0x13
	.short	14                      # 0xe
	.short	76                      # 0x4c
	.short	87                      # 0x57
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	19                      # 0x13
	.short	76                      # 0x4c
	.short	423                     # 0x1a7
	.short	20                      # 0x14
	.short	80                      # 0x50
	.short	80                      # 0x50
	.short	80                      # 0x50
	.short	20                      # 0x14
	.short	27                      # 0x1b
	.short	20                      # 0x14
	.short	27                      # 0x1b
	.short	10                      # 0xa
	.short	10                      # 0xa
	.short	10                      # 0xa
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	17                      # 0x11
	.short	48                      # 0x30
	.short	48                      # 0x30
	.short	48                      # 0x30
	.short	48                      # 0x30
	.short	68                      # 0x44
	.short	73                      # 0x49
	.short	73                      # 0x49
	.short	73                      # 0x49
	.short	93                      # 0x5d
	.short	93                      # 0x5d
	.short	48                      # 0x30
	.short	48                      # 0x30
	.short	75                      # 0x4b
	.short	48                      # 0x30
	.short	99                      # 0x63
	.short	48                      # 0x30
	.short	48                      # 0x30
	.short	162                     # 0xa2
	.short	48                      # 0x30
	.short	74                      # 0x4a
	.short	74                      # 0x4a
	.short	74                      # 0x4a
	.short	89                      # 0x59
	.short	48                      # 0x30
	.short	75                      # 0x4b
	.short	81                      # 0x51
	.short	81                      # 0x51
	.short	81                      # 0x51
	.short	420                     # 0x1a4
	.short	89                      # 0x59
	.short	89                      # 0x59
	.short	144                     # 0x90
	.short	144                     # 0x90
	.short	155                     # 0x9b
	.short	155                     # 0x9b
	.short	155                     # 0x9b
	.short	156                     # 0x9c
	.short	156                     # 0x9c
	.short	156                     # 0x9c
	.short	68                      # 0x44
	.short	167                     # 0xa7
	.short	167                     # 0xa7
	.short	167                     # 0xa7
	.short	370                     # 0x172
	.short	162                     # 0xa2
	.short	208                     # 0xd0
	.short	208                     # 0xd0
	.short	162                     # 0xa2
	.short	73                      # 0x49
	.short	412                     # 0x19c
	.short	211                     # 0xd3
	.short	162                     # 0xa2
	.short	218                     # 0xda
	.short	419                     # 0x1a3
	.short	370                     # 0x172
	.short	418                     # 0x1a2
	.short	421                     # 0x1a5
	.short	218                     # 0xda
	.short	422                     # 0x1a6
	.short	430                     # 0x1ae
	.short	99                      # 0x63
	.short	413                     # 0x19d
	.short	74                      # 0x4a
	.short	417                     # 0x1a1
	.short	218                     # 0xda
	.short	218                     # 0xda
	.short	412                     # 0x19c
	.short	421                     # 0x1a5
	.short	218                     # 0xda
	.short	422                     # 0x1a6
	.short	430                     # 0x1ae
	.short	415                     # 0x19f
	.short	413                     # 0x19d
	.short	75                      # 0x4b
	.short	414                     # 0x19e
	.short	75                      # 0x4b
	.short	155                     # 0x9b
	.short	211                     # 0xd3
	.short	413                     # 0x19d
	.short	156                     # 0x9c
	.short	211                     # 0xd3
	.short	411                     # 0x19b
	.short	410                     # 0x19a
	.short	409                     # 0x199
	.short	211                     # 0xd3
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	408                     # 0x198
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	407                     # 0x197
	.short	404                     # 0x194
	.short	403                     # 0x193
	.short	402                     # 0x192
	.short	400                     # 0x190
	.short	399                     # 0x18f
	.short	398                     # 0x18e
	.short	397                     # 0x18d
	.short	396                     # 0x18c
	.short	395                     # 0x18b
	.short	394                     # 0x18a
	.short	393                     # 0x189
	.short	392                     # 0x188
	.short	391                     # 0x187
	.short	390                     # 0x186
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	389                     # 0x185
	.short	387                     # 0x183
	.short	386                     # 0x182
	.short	382                     # 0x17e
	.short	381                     # 0x17d
	.short	380                     # 0x17c
	.short	379                     # 0x17b
	.short	378                     # 0x17a
	.short	377                     # 0x179
	.short	374                     # 0x176
	.short	373                     # 0x175
	.short	372                     # 0x174
	.short	371                     # 0x173
	.short	369                     # 0x171
	.short	368                     # 0x170
	.short	367                     # 0x16f
	.short	361                     # 0x169
	.short	360                     # 0x168
	.short	358                     # 0x166
	.short	357                     # 0x165
	.short	356                     # 0x164
	.short	355                     # 0x163
	.short	354                     # 0x162
	.short	353                     # 0x161
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	424                     # 0x1a8
	.short	438                     # 0x1b6
	.short	438                     # 0x1b6
	.short	438                     # 0x1b6
	.short	438                     # 0x1b6
	.short	438                     # 0x1b6
	.short	438                     # 0x1b6
	.short	438                     # 0x1b6
	.short	438                     # 0x1b6
	.short	438                     # 0x1b6
	.short	439                     # 0x1b7
	.short	439                     # 0x1b7
	.short	439                     # 0x1b7
	.short	439                     # 0x1b7
	.short	439                     # 0x1b7
	.short	439                     # 0x1b7
	.short	439                     # 0x1b7
	.short	439                     # 0x1b7
	.short	439                     # 0x1b7
	.short	440                     # 0x1b8
	.short	440                     # 0x1b8
	.short	440                     # 0x1b8
	.short	440                     # 0x1b8
	.short	440                     # 0x1b8
	.short	440                     # 0x1b8
	.short	440                     # 0x1b8
	.short	440                     # 0x1b8
	.short	440                     # 0x1b8
	.short	441                     # 0x1b9
	.short	441                     # 0x1b9
	.short	441                     # 0x1b9
	.short	441                     # 0x1b9
	.short	441                     # 0x1b9
	.short	441                     # 0x1b9
	.short	441                     # 0x1b9
	.short	441                     # 0x1b9
	.short	441                     # 0x1b9
	.short	442                     # 0x1ba
	.short	442                     # 0x1ba
	.short	442                     # 0x1ba
	.short	442                     # 0x1ba
	.short	442                     # 0x1ba
	.short	442                     # 0x1ba
	.short	442                     # 0x1ba
	.short	442                     # 0x1ba
	.short	442                     # 0x1ba
	.short	443                     # 0x1bb
	.short	443                     # 0x1bb
	.short	443                     # 0x1bb
	.short	443                     # 0x1bb
	.short	443                     # 0x1bb
	.short	443                     # 0x1bb
	.short	443                     # 0x1bb
	.short	443                     # 0x1bb
	.short	443                     # 0x1bb
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	445                     # 0x1bd
	.short	445                     # 0x1bd
	.short	446                     # 0x1be
	.short	352                     # 0x160
	.short	351                     # 0x15f
	.short	350                     # 0x15e
	.short	349                     # 0x15d
	.short	348                     # 0x15c
	.short	446                     # 0x1be
	.short	446                     # 0x1be
	.short	446                     # 0x1be
	.short	447                     # 0x1bf
	.short	346                     # 0x15a
	.short	345                     # 0x159
	.short	447                     # 0x1bf
	.short	447                     # 0x1bf
	.short	447                     # 0x1bf
	.short	447                     # 0x1bf
	.short	447                     # 0x1bf
	.short	447                     # 0x1bf
	.short	448                     # 0x1c0
	.short	342                     # 0x156
	.short	448                     # 0x1c0
	.short	448                     # 0x1c0
	.short	341                     # 0x155
	.short	448                     # 0x1c0
	.short	448                     # 0x1c0
	.short	448                     # 0x1c0
	.short	448                     # 0x1c0
	.short	449                     # 0x1c1
	.short	339                     # 0x153
	.short	337                     # 0x151
	.short	336                     # 0x150
	.short	335                     # 0x14f
	.short	449                     # 0x1c1
	.short	333                     # 0x14d
	.short	449                     # 0x1c1
	.short	450                     # 0x1c2
	.short	450                     # 0x1c2
	.short	451                     # 0x1c3
	.short	451                     # 0x1c3
	.short	452                     # 0x1c4
	.short	452                     # 0x1c4
	.short	452                     # 0x1c4
	.short	452                     # 0x1c4
	.short	452                     # 0x1c4
	.short	452                     # 0x1c4
	.short	452                     # 0x1c4
	.short	332                     # 0x14c
	.short	331                     # 0x14b
	.short	330                     # 0x14a
	.short	329                     # 0x149
	.short	328                     # 0x148
	.short	327                     # 0x147
	.short	326                     # 0x146
	.short	325                     # 0x145
	.short	324                     # 0x144
	.short	323                     # 0x143
	.short	322                     # 0x142
	.short	321                     # 0x141
	.short	320                     # 0x140
	.short	319                     # 0x13f
	.short	318                     # 0x13e
	.short	317                     # 0x13d
	.short	316                     # 0x13c
	.short	315                     # 0x13b
	.short	314                     # 0x13a
	.short	313                     # 0x139
	.short	312                     # 0x138
	.short	311                     # 0x137
	.short	310                     # 0x136
	.short	309                     # 0x135
	.short	308                     # 0x134
	.short	306                     # 0x132
	.short	305                     # 0x131
	.short	304                     # 0x130
	.short	302                     # 0x12e
	.short	301                     # 0x12d
	.short	300                     # 0x12c
	.short	299                     # 0x12b
	.short	298                     # 0x12a
	.short	297                     # 0x129
	.short	296                     # 0x128
	.short	295                     # 0x127
	.short	294                     # 0x126
	.short	293                     # 0x125
	.short	292                     # 0x124
	.short	290                     # 0x122
	.short	289                     # 0x121
	.short	288                     # 0x120
	.short	287                     # 0x11f
	.short	286                     # 0x11e
	.short	285                     # 0x11d
	.short	284                     # 0x11c
	.short	283                     # 0x11b
	.short	282                     # 0x11a
	.short	281                     # 0x119
	.short	280                     # 0x118
	.short	279                     # 0x117
	.short	274                     # 0x112
	.short	273                     # 0x111
	.short	272                     # 0x110
	.short	271                     # 0x10f
	.short	270                     # 0x10e
	.short	269                     # 0x10d
	.short	268                     # 0x10c
	.short	267                     # 0x10b
	.short	266                     # 0x10a
	.short	265                     # 0x109
	.short	263                     # 0x107
	.short	262                     # 0x106
	.short	261                     # 0x105
	.short	260                     # 0x104
	.short	259                     # 0x103
	.short	258                     # 0x102
	.short	257                     # 0x101
	.short	256                     # 0x100
	.short	255                     # 0xff
	.short	254                     # 0xfe
	.short	253                     # 0xfd
	.short	252                     # 0xfc
	.short	251                     # 0xfb
	.short	250                     # 0xfa
	.short	249                     # 0xf9
	.short	248                     # 0xf8
	.short	247                     # 0xf7
	.short	246                     # 0xf6
	.short	245                     # 0xf5
	.short	244                     # 0xf4
	.short	243                     # 0xf3
	.short	242                     # 0xf2
	.short	241                     # 0xf1
	.short	240                     # 0xf0
	.short	239                     # 0xef
	.short	238                     # 0xee
	.short	237                     # 0xed
	.short	236                     # 0xec
	.short	235                     # 0xeb
	.short	234                     # 0xea
	.short	233                     # 0xe9
	.short	232                     # 0xe8
	.short	231                     # 0xe7
	.short	230                     # 0xe6
	.short	229                     # 0xe5
	.short	227                     # 0xe3
	.short	226                     # 0xe2
	.short	225                     # 0xe1
	.short	223                     # 0xdf
	.short	222                     # 0xde
	.short	221                     # 0xdd
	.short	220                     # 0xdc
	.short	219                     # 0xdb
	.short	217                     # 0xd9
	.short	216                     # 0xd8
	.short	215                     # 0xd7
	.short	214                     # 0xd6
	.short	213                     # 0xd5
	.short	212                     # 0xd4
	.short	210                     # 0xd2
	.short	207                     # 0xcf
	.short	206                     # 0xce
	.short	205                     # 0xcd
	.short	204                     # 0xcc
	.short	203                     # 0xcb
	.short	202                     # 0xca
	.short	201                     # 0xc9
	.short	200                     # 0xc8
	.short	199                     # 0xc7
	.short	198                     # 0xc6
	.short	197                     # 0xc5
	.short	196                     # 0xc4
	.short	195                     # 0xc3
	.short	194                     # 0xc2
	.short	193                     # 0xc1
	.short	192                     # 0xc0
	.short	191                     # 0xbf
	.short	190                     # 0xbe
	.short	189                     # 0xbd
	.short	188                     # 0xbc
	.short	187                     # 0xbb
	.short	186                     # 0xba
	.short	185                     # 0xb9
	.short	184                     # 0xb8
	.short	183                     # 0xb7
	.short	181                     # 0xb5
	.short	180                     # 0xb4
	.short	179                     # 0xb3
	.short	178                     # 0xb2
	.short	177                     # 0xb1
	.short	176                     # 0xb0
	.short	175                     # 0xaf
	.short	174                     # 0xae
	.short	173                     # 0xad
	.short	171                     # 0xab
	.short	170                     # 0xaa
	.short	169                     # 0xa9
	.short	166                     # 0xa6
	.short	159                     # 0x9f
	.short	157                     # 0x9d
	.short	143                     # 0x8f
	.short	142                     # 0x8e
	.short	141                     # 0x8d
	.short	140                     # 0x8c
	.short	139                     # 0x8b
	.short	138                     # 0x8a
	.short	137                     # 0x89
	.short	136                     # 0x88
	.short	135                     # 0x87
	.short	134                     # 0x86
	.short	133                     # 0x85
	.short	132                     # 0x84
	.short	131                     # 0x83
	.short	130                     # 0x82
	.short	129                     # 0x81
	.short	128                     # 0x80
	.short	127                     # 0x7f
	.short	126                     # 0x7e
	.short	125                     # 0x7d
	.short	124                     # 0x7c
	.short	123                     # 0x7b
	.short	122                     # 0x7a
	.short	121                     # 0x79
	.short	120                     # 0x78
	.short	119                     # 0x77
	.short	114                     # 0x72
	.short	112                     # 0x70
	.short	111                     # 0x6f
	.short	110                     # 0x6e
	.short	107                     # 0x6b
	.short	106                     # 0x6a
	.short	105                     # 0x69
	.short	104                     # 0x68
	.short	103                     # 0x67
	.short	101                     # 0x65
	.short	98                      # 0x62
	.short	97                      # 0x61
	.short	96                      # 0x60
	.short	95                      # 0x5f
	.short	92                      # 0x5c
	.short	90                      # 0x5a
	.short	86                      # 0x56
	.short	85                      # 0x55
	.short	83                      # 0x53
	.short	82                      # 0x52
	.short	79                      # 0x4f
	.short	78                      # 0x4e
	.short	64                      # 0x40
	.short	57                      # 0x39
	.short	46                      # 0x2e
	.short	45                      # 0x2d
	.short	44                      # 0x2c
	.short	43                      # 0x2b
	.short	42                      # 0x2a
	.short	41                      # 0x29
	.short	40                      # 0x28
	.short	39                      # 0x27
	.short	38                      # 0x26
	.short	37                      # 0x25
	.short	36                      # 0x24
	.short	34                      # 0x22
	.short	33                      # 0x21
	.short	31                      # 0x1f
	.short	21                      # 0x15
	.short	5                       # 0x5
	.short	4                       # 0x4
	.short	2                       # 0x2
	.short	1                       # 0x1
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.size	_ZL6yy_chk, 1844

	.type	_ZL7yy_base,@object     # @_ZL7yy_base
	.p2align	4
_ZL7yy_base:
	.short	0                       # 0x0
	.short	846                     # 0x34e
	.short	845                     # 0x34d
	.short	0                       # 0x0
	.short	839                     # 0x347
	.short	843                     # 0x34b
	.short	69                      # 0x45
	.short	77                      # 0x4d
	.short	0                       # 0x0
	.short	146                     # 0x92
	.short	186                     # 0xba
	.short	159                     # 0x9f
	.short	165                     # 0xa5
	.short	167                     # 0xa7
	.short	169                     # 0xa9
	.short	228                     # 0xe4
	.short	0                       # 0x0
	.short	299                     # 0x12b
	.short	0                       # 0x0
	.short	148                     # 0x94
	.short	157                     # 0x9d
	.short	845                     # 0x34d
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	155                     # 0x9b
	.short	13                      # 0xd
	.short	850                     # 0x352
	.short	154                     # 0x9a
	.short	827                     # 0x33b
	.short	162                     # 0xa2
	.short	824                     # 0x338
	.short	822                     # 0x336
	.short	0                       # 0x0
	.short	778                     # 0x30a
	.short	782                     # 0x30e
	.short	790                     # 0x316
	.short	772                     # 0x304
	.short	784                     # 0x310
	.short	777                     # 0x309
	.short	775                     # 0x307
	.short	785                     # 0x311
	.short	771                     # 0x303
	.short	765                     # 0x2fd
	.short	774                     # 0x306
	.short	123                     # 0x7b
	.short	326                     # 0x146
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	114                     # 0x72
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	850                     # 0x352
	.short	811                     # 0x32b
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	172                     # 0xac
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	788                     # 0x314
	.short	850                     # 0x352
	.short	0                       # 0x0
	.short	850                     # 0x352
	.short	369                     # 0x171
	.short	0                       # 0x0
	.short	850                     # 0x352
	.short	153                     # 0x99
	.short	0                       # 0x0
	.short	374                     # 0x176
	.short	388                     # 0x184
	.short	376                     # 0x178
	.short	198                     # 0xc6
	.short	0                       # 0x0
	.short	787                     # 0x313
	.short	777                     # 0x309
	.short	217                     # 0xd9
	.short	394                     # 0x18a
	.short	803                     # 0x323
	.short	802                     # 0x322
	.short	146                     # 0x92
	.short	812                     # 0x32c
	.short	800                     # 0x320
	.short	189                     # 0xbd
	.short	850                     # 0x352
	.short	377                     # 0x179
	.short	799                     # 0x31f
	.short	52                      # 0x34
	.short	798                     # 0x31e
	.short	356                     # 0x164
	.short	0                       # 0x0
	.short	778                     # 0x30a
	.short	796                     # 0x31c
	.short	769                     # 0x301
	.short	768                     # 0x300
	.short	362                     # 0x16a
	.short	850                     # 0x352
	.short	814                     # 0x32e
	.short	850                     # 0x352
	.short	757                     # 0x2f5
	.short	758                     # 0x2f6
	.short	764                     # 0x2fc
	.short	745                     # 0x2e9
	.short	792                     # 0x318
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	746                     # 0x2ea
	.short	745                     # 0x2e9
	.short	789                     # 0x315
	.short	850                     # 0x352
	.short	790                     # 0x316
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	0                       # 0x0
	.short	744                     # 0x2e8
	.short	748                     # 0x2ec
	.short	754                     # 0x2f2
	.short	741                     # 0x2e5
	.short	741                     # 0x2e5
	.short	752                     # 0x2f0
	.short	742                     # 0x2e6
	.short	748                     # 0x2ec
	.short	753                     # 0x2f1
	.short	738                     # 0x2e2
	.short	735                     # 0x2df
	.short	735                     # 0x2df
	.short	739                     # 0x2e3
	.short	731                     # 0x2db
	.short	747                     # 0x2eb
	.short	729                     # 0x2d9
	.short	728                     # 0x2d8
	.short	744                     # 0x2e8
	.short	739                     # 0x2e3
	.short	728                     # 0x2d8
	.short	722                     # 0x2d2
	.short	723                     # 0x2d3
	.short	720                     # 0x2d0
	.short	781                     # 0x30d
	.short	780                     # 0x30c
	.short	400                     # 0x190
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	850                     # 0x352
	.short	0                       # 0x0
	.short	850                     # 0x352
	.short	0                       # 0x0
	.short	850                     # 0x352
	.short	0                       # 0x0
	.short	402                     # 0x192
	.short	405                     # 0x195
	.short	731                     # 0x2db
	.short	850                     # 0x352
	.short	761                     # 0x2f9
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	386                     # 0x182
	.short	850                     # 0x352
	.short	0                       # 0x0
	.short	850                     # 0x352
	.short	717                     # 0x2cd
	.short	409                     # 0x199
	.short	850                     # 0x352
	.short	765                     # 0x2fd
	.short	754                     # 0x2f2
	.short	753                     # 0x2f1
	.short	0                       # 0x0
	.short	720                     # 0x2d0
	.short	709                     # 0x2c5
	.short	771                     # 0x303
	.short	756                     # 0x2f4
	.short	726                     # 0x2d6
	.short	725                     # 0x2d5
	.short	708                     # 0x2c4
	.short	715                     # 0x2cb
	.short	714                     # 0x2ca
	.short	850                     # 0x352
	.short	708                     # 0x2c4
	.short	704                     # 0x2c0
	.short	719                     # 0x2cf
	.short	714                     # 0x2ca
	.short	700                     # 0x2bc
	.short	701                     # 0x2bd
	.short	696                     # 0x2b8
	.short	706                     # 0x2c2
	.short	696                     # 0x2b8
	.short	708                     # 0x2c4
	.short	703                     # 0x2bf
	.short	693                     # 0x2b5
	.short	709                     # 0x2c5
	.short	691                     # 0x2b3
	.short	691                     # 0x2b3
	.short	693                     # 0x2b5
	.short	692                     # 0x2b4
	.short	692                     # 0x2b4
	.short	692                     # 0x2b4
	.short	685                     # 0x2ad
	.short	693                     # 0x2b5
	.short	687                     # 0x2af
	.short	691                     # 0x2b3
	.short	741                     # 0x2e5
	.short	740                     # 0x2e4
	.short	414                     # 0x19e
	.short	850                     # 0x352
	.short	679                     # 0x2a7
	.short	419                     # 0x1a3
	.short	709                     # 0x2c5
	.short	706                     # 0x2c2
	.short	700                     # 0x2bc
	.short	688                     # 0x2b0
	.short	687                     # 0x2af
	.short	695                     # 0x2b7
	.short	376                     # 0x178
	.short	674                     # 0x2a2
	.short	679                     # 0x2a7
	.short	670                     # 0x29e
	.short	682                     # 0x2aa
	.short	681                     # 0x2a9
	.short	0                       # 0x0
	.short	667                     # 0x29b
	.short	665                     # 0x299
	.short	667                     # 0x29b
	.short	0                       # 0x0
	.short	681                     # 0x2a9
	.short	672                     # 0x2a0
	.short	663                     # 0x297
	.short	670                     # 0x29e
	.short	674                     # 0x2a2
	.short	670                     # 0x29e
	.short	657                     # 0x291
	.short	657                     # 0x291
	.short	658                     # 0x292
	.short	668                     # 0x29c
	.short	656                     # 0x290
	.short	655                     # 0x28f
	.short	663                     # 0x297
	.short	667                     # 0x29b
	.short	659                     # 0x293
	.short	662                     # 0x296
	.short	650                     # 0x28a
	.short	660                     # 0x294
	.short	659                     # 0x293
	.short	673                     # 0x2a1
	.short	669                     # 0x29d
	.short	671                     # 0x29f
	.short	644                     # 0x284
	.short	641                     # 0x281
	.short	660                     # 0x294
	.short	640                     # 0x280
	.short	655                     # 0x28f
	.short	639                     # 0x27f
	.short	649                     # 0x289
	.short	640                     # 0x280
	.short	634                     # 0x27a
	.short	679                     # 0x2a7
	.short	627                     # 0x273
	.short	628                     # 0x274
	.short	627                     # 0x273
	.short	0                       # 0x0
	.short	636                     # 0x27c
	.short	633                     # 0x279
	.short	627                     # 0x273
	.short	640                     # 0x280
	.short	625                     # 0x271
	.short	639                     # 0x27f
	.short	636                     # 0x27c
	.short	627                     # 0x273
	.short	638                     # 0x27e
	.short	629                     # 0x275
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	618                     # 0x26a
	.short	631                     # 0x277
	.short	621                     # 0x26d
	.short	613                     # 0x265
	.short	632                     # 0x278
	.short	611                     # 0x263
	.short	615                     # 0x267
	.short	637                     # 0x27d
	.short	638                     # 0x27e
	.short	638                     # 0x27e
	.short	621                     # 0x26d
	.short	621                     # 0x26d
	.short	850                     # 0x352
	.short	618                     # 0x26a
	.short	607                     # 0x25f
	.short	614                     # 0x266
	.short	601                     # 0x259
	.short	606                     # 0x25e
	.short	648                     # 0x288
	.short	604                     # 0x25c
	.short	603                     # 0x25b
	.short	597                     # 0x255
	.short	596                     # 0x254
	.short	597                     # 0x255
	.short	0                       # 0x0
	.short	600                     # 0x258
	.short	608                     # 0x260
	.short	607                     # 0x25f
	.short	0                       # 0x0
	.short	605                     # 0x25d
	.short	605                     # 0x25d
	.short	598                     # 0x256
	.short	597                     # 0x255
	.short	606                     # 0x25e
	.short	590                     # 0x24e
	.short	592                     # 0x250
	.short	584                     # 0x248
	.short	596                     # 0x254
	.short	582                     # 0x246
	.short	595                     # 0x253
	.short	605                     # 0x25d
	.short	614                     # 0x266
	.short	607                     # 0x25f
	.short	583                     # 0x247
	.short	600                     # 0x258
	.short	586                     # 0x24a
	.short	586                     # 0x24a
	.short	580                     # 0x244
	.short	576                     # 0x240
	.short	590                     # 0x24e
	.short	575                     # 0x23f
	.short	575                     # 0x23f
	.short	583                     # 0x247
	.short	586                     # 0x24a
	.short	573                     # 0x23d
	.short	0                       # 0x0
	.short	556                     # 0x22c
	.short	567                     # 0x237
	.short	554                     # 0x22a
	.short	0                       # 0x0
	.short	565                     # 0x235
	.short	0                       # 0x0
	.short	558                     # 0x22e
	.short	553                     # 0x229
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	547                     # 0x223
	.short	537                     # 0x219
	.short	189                     # 0xbd
	.short	565                     # 0x235
	.short	560                     # 0x230
	.short	528                     # 0x210
	.short	545                     # 0x221
	.short	528                     # 0x210
	.short	460                     # 0x1cc
	.short	459                     # 0x1cb
	.short	467                     # 0x1d3
	.short	459                     # 0x1cb
	.short	460                     # 0x1cc
	.short	464                     # 0x1d0
	.short	850                     # 0x352
	.short	456                     # 0x1c8
	.short	455                     # 0x1c7
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	465                     # 0x1d1
	.short	466                     # 0x1d2
	.short	452                     # 0x1c4
	.short	412                     # 0x19c
	.short	493                     # 0x1ed
	.short	483                     # 0x1e3
	.short	473                     # 0x1d9
	.short	459                     # 0x1cb
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	462                     # 0x1ce
	.short	458                     # 0x1ca
	.short	443                     # 0x1bb
	.short	443                     # 0x1bb
	.short	450                     # 0x1c2
	.short	445                     # 0x1bd
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	446                     # 0x1be
	.short	451                     # 0x1c3
	.short	850                     # 0x352
	.short	469                     # 0x1d5
	.short	458                     # 0x1ca
	.short	448                     # 0x1c0
	.short	446                     # 0x1be
	.short	433                     # 0x1b1
	.short	441                     # 0x1b9
	.short	440                     # 0x1b8
	.short	431                     # 0x1af
	.short	425                     # 0x1a9
	.short	441                     # 0x1b9
	.short	440                     # 0x1b8
	.short	455                     # 0x1c7
	.short	164                     # 0xa4
	.short	436                     # 0x1b4
	.short	427                     # 0x1ab
	.short	419                     # 0x1a3
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	418                     # 0x1a2
	.short	425                     # 0x1a9
	.short	393                     # 0x189
	.short	406                     # 0x196
	.short	424                     # 0x1a8
	.short	418                     # 0x1a2
	.short	430                     # 0x1ae
	.short	393                     # 0x189
	.short	393                     # 0x189
	.short	850                     # 0x352
	.short	385                     # 0x181
	.short	366                     # 0x16e
	.short	363                     # 0x16b
	.short	347                     # 0x15b
	.short	425                     # 0x1a9
	.short	427                     # 0x1ab
	.short	199                     # 0xc7
	.short	455                     # 0x1c7
	.short	138                     # 0x8a
	.short	135                     # 0x87
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	0                       # 0x0
	.short	428                     # 0x1ac
	.short	136                     # 0x88
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	850                     # 0x352
	.short	526                     # 0x20e
	.short	535                     # 0x217
	.short	544                     # 0x220
	.short	553                     # 0x229
	.short	562                     # 0x232
	.short	571                     # 0x23b
	.short	573                     # 0x23d
	.short	575                     # 0x23f
	.short	584                     # 0x248
	.short	593                     # 0x251
	.short	602                     # 0x25a
	.short	611                     # 0x263
	.short	612                     # 0x264
	.short	614                     # 0x266
	.short	623                     # 0x26f
	.size	_ZL7yy_base, 906

	.type	_ZL6yy_def,@object      # @_ZL6yy_def
	.p2align	4
_ZL6yy_def:
	.short	0                       # 0x0
	.short	438                     # 0x1b6
	.short	438                     # 0x1b6
	.short	437                     # 0x1b5
	.short	3                       # 0x3
	.short	439                     # 0x1b7
	.short	439                     # 0x1b7
	.short	437                     # 0x1b5
	.short	7                       # 0x7
	.short	440                     # 0x1b8
	.short	440                     # 0x1b8
	.short	441                     # 0x1b9
	.short	441                     # 0x1b9
	.short	442                     # 0x1ba
	.short	442                     # 0x1ba
	.short	437                     # 0x1b5
	.short	15                      # 0xf
	.short	437                     # 0x1b5
	.short	17                      # 0x11
	.short	443                     # 0x1bb
	.short	443                     # 0x1bb
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	445                     # 0x1bd
	.short	446                     # 0x1be
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	446                     # 0x1be
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	447                     # 0x1bf
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	448                     # 0x1c0
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	449                     # 0x1c1
	.short	449                     # 0x1c1
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	451                     # 0x1c3
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	451                     # 0x1c3
	.short	451                     # 0x1c3
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	445                     # 0x1bd
	.short	446                     # 0x1be
	.short	446                     # 0x1be
	.short	446                     # 0x1be
	.short	437                     # 0x1b5
	.short	447                     # 0x1bf
	.short	437                     # 0x1b5
	.short	448                     # 0x1c0
	.short	437                     # 0x1b5
	.short	449                     # 0x1c1
	.short	449                     # 0x1c1
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	451                     # 0x1c3
	.short	451                     # 0x1c3
	.short	451                     # 0x1c3
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	451                     # 0x1c3
	.short	451                     # 0x1c3
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	451                     # 0x1c3
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	451                     # 0x1c3
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	451                     # 0x1c3
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	444                     # 0x1bc
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	450                     # 0x1c2
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	452                     # 0x1c4
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	0                       # 0x0
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.size	_ZL6yy_def, 906

	.type	_ZL7yy_meta,@object     # @_ZL7yy_meta
	.p2align	4
_ZL7yy_meta:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	6                       # 0x6
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	7                       # 0x7
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	1                       # 0x1
	.size	_ZL7yy_meta, 288

	.type	_ZL6yy_nxt,@object      # @_ZL6yy_nxt
	.p2align	4
_ZL6yy_nxt:
	.short	0                       # 0x0
	.short	22                      # 0x16
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	24                      # 0x18
	.short	22                      # 0x16
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	22                      # 0x16
	.short	29                      # 0x1d
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	30                      # 0x1e
	.short	31                      # 0x1f
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	35                      # 0x23
	.short	37                      # 0x25
	.short	38                      # 0x26
	.short	39                      # 0x27
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	40                      # 0x28
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	41                      # 0x29
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	45                      # 0x2d
	.short	46                      # 0x2e
	.short	47                      # 0x2f
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	50                      # 0x32
	.short	110                     # 0x6e
	.short	170                     # 0xaa
	.short	168                     # 0xa8
	.short	111                     # 0x6f
	.short	51                      # 0x33
	.short	22                      # 0x16
	.short	52                      # 0x34
	.short	53                      # 0x35
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	32                      # 0x20
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	56                      # 0x38
	.short	101                     # 0x65
	.short	102                     # 0x66
	.short	26                      # 0x1a
	.short	57                      # 0x39
	.short	436                     # 0x1b4
	.short	168                     # 0xa8
	.short	29                      # 0x1d
	.short	58                      # 0x3a
	.short	59                      # 0x3b
	.short	101                     # 0x65
	.short	102                     # 0x66
	.short	60                      # 0x3c
	.short	56                      # 0x38
	.short	153                     # 0x99
	.short	61                      # 0x3d
	.short	67                      # 0x43
	.short	412                     # 0x19c
	.short	62                      # 0x3e
	.short	56                      # 0x38
	.short	168                     # 0xa8
	.short	56                      # 0x38
	.short	67                      # 0x43
	.short	56                      # 0x38
	.short	112                     # 0x70
	.short	107                     # 0x6b
	.short	115                     # 0x73
	.short	130                     # 0x82
	.short	70                      # 0x46
	.short	113                     # 0x71
	.short	70                      # 0x46
	.short	116                     # 0x74
	.short	131                     # 0x83
	.short	143                     # 0x8f
	.short	413                     # 0x19d
	.short	144                     # 0x90
	.short	147                     # 0x93
	.short	63                      # 0x3f
	.short	64                      # 0x40
	.short	65                      # 0x41
	.short	56                      # 0x38
	.short	148                     # 0x94
	.short	370                     # 0x172
	.short	26                      # 0x1a
	.short	57                      # 0x39
	.short	149                     # 0x95
	.short	435                     # 0x1b3
	.short	29                      # 0x1d
	.short	58                      # 0x3a
	.short	59                      # 0x3b
	.short	434                     # 0x1b2
	.short	68                      # 0x44
	.short	60                      # 0x3c
	.short	371                     # 0x173
	.short	168                     # 0xa8
	.short	61                      # 0x3d
	.short	103                     # 0x67
	.short	68                      # 0x44
	.short	62                      # 0x3e
	.short	71                      # 0x47
	.short	104                     # 0x68
	.short	71                      # 0x47
	.short	162                     # 0xa2
	.short	168                     # 0xa8
	.short	105                     # 0x69
	.short	103                     # 0x67
	.short	106                     # 0x6a
	.short	163                     # 0xa3
	.short	432                     # 0x1b0
	.short	104                     # 0x68
	.short	167                     # 0xa7
	.short	167                     # 0xa7
	.short	167                     # 0xa7
	.short	105                     # 0x69
	.short	108                     # 0x6c
	.short	106                     # 0x6a
	.short	109                     # 0x6d
	.short	63                      # 0x3f
	.short	64                      # 0x40
	.short	65                      # 0x41
	.short	72                      # 0x48
	.short	73                      # 0x49
	.short	74                      # 0x4a
	.short	73                      # 0x49
	.short	72                      # 0x48
	.short	26                      # 0x1a
	.short	75                      # 0x4b
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	29                      # 0x1d
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	76                      # 0x4c
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	72                      # 0x48
	.short	78                      # 0x4e
	.short	72                      # 0x48
	.short	72                      # 0x48
	.short	77                      # 0x4d
	.short	79                      # 0x4f
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	77                      # 0x4d
	.short	22                      # 0x16
	.short	72                      # 0x48
	.short	22                      # 0x16
	.short	72                      # 0x48
	.short	22                      # 0x16
	.short	80                      # 0x50
	.short	81                      # 0x51
	.short	80                      # 0x50
	.short	82                      # 0x52
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	83                      # 0x53
	.short	84                      # 0x54
	.short	22                      # 0x16
	.short	85                      # 0x55
	.short	22                      # 0x16
	.short	86                      # 0x56
	.short	87                      # 0x57
	.short	88                      # 0x58
	.short	89                      # 0x59
	.short	22                      # 0x16
	.short	90                      # 0x5a
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	91                      # 0x5b
	.short	92                      # 0x5c
	.short	93                      # 0x5d
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	95                      # 0x5f
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	96                      # 0x60
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	97                      # 0x61
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	98                      # 0x62
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	94                      # 0x5e
	.short	22                      # 0x16
	.short	99                      # 0x63
	.short	22                      # 0x16
	.short	88                      # 0x58
	.short	132                     # 0x84
	.short	133                     # 0x85
	.short	134                     # 0x86
	.short	135                     # 0x87
	.short	151                     # 0x97
	.short	155                     # 0x9b
	.short	156                     # 0x9c
	.short	155                     # 0x9b
	.short	168                     # 0xa8
	.short	171                     # 0xab
	.short	136                     # 0x88
	.short	137                     # 0x89
	.short	158                     # 0x9e
	.short	138                     # 0x8a
	.short	168                     # 0xa8
	.short	139                     # 0x8b
	.short	140                     # 0x8c
	.short	211                     # 0xd3
	.short	141                     # 0x8d
	.short	156                     # 0x9c
	.short	156                     # 0x9c
	.short	156                     # 0x9c
	.short	168                     # 0xa8
	.short	142                     # 0x8e
	.short	159                     # 0x9f
	.short	167                     # 0xa7
	.short	167                     # 0xa7
	.short	167                     # 0xa7
	.short	429                     # 0x1ad
	.short	168                     # 0xa8
	.short	169                     # 0xa9
	.short	208                     # 0xd0
	.short	209                     # 0xd1
	.short	155                     # 0x9b
	.short	156                     # 0x9c
	.short	155                     # 0x9b
	.short	156                     # 0x9c
	.short	156                     # 0x9c
	.short	156                     # 0x9c
	.short	149                     # 0x95
	.short	167                     # 0xa7
	.short	167                     # 0xa7
	.short	167                     # 0xa7
	.short	370                     # 0x172
	.short	212                     # 0xd4
	.short	208                     # 0xd0
	.short	209                     # 0xd1
	.short	213                     # 0xd5
	.short	157                     # 0x9d
	.short	412                     # 0x19c
	.short	211                     # 0xd3
	.short	214                     # 0xd6
	.short	254                     # 0xfe
	.short	428                     # 0x1ac
	.short	371                     # 0x173
	.short	427                     # 0x1ab
	.short	430                     # 0x1ae
	.short	255                     # 0xff
	.short	422                     # 0x1a6
	.short	430                     # 0x1ae
	.short	168                     # 0xa8
	.short	422                     # 0x1a6
	.short	157                     # 0x9d
	.short	426                     # 0x1aa
	.short	256                     # 0x100
	.short	257                     # 0x101
	.short	413                     # 0x19d
	.short	431                     # 0x1af
	.short	258                     # 0x102
	.short	423                     # 0x1a7
	.short	431                     # 0x1af
	.short	425                     # 0x1a9
	.short	423                     # 0x1a7
	.short	160                     # 0xa0
	.short	424                     # 0x1a8
	.short	161                     # 0xa1
	.short	157                     # 0x9d
	.short	212                     # 0xd4
	.short	413                     # 0x19d
	.short	157                     # 0x9d
	.short	213                     # 0xd5
	.short	421                     # 0x1a5
	.short	420                     # 0x1a4
	.short	419                     # 0x1a3
	.short	214                     # 0xd6
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	418                     # 0x1a2
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	417                     # 0x1a1
	.short	416                     # 0x1a0
	.short	415                     # 0x19f
	.short	414                     # 0x19e
	.short	411                     # 0x19b
	.short	410                     # 0x19a
	.short	409                     # 0x199
	.short	408                     # 0x198
	.short	407                     # 0x197
	.short	406                     # 0x196
	.short	405                     # 0x195
	.short	404                     # 0x194
	.short	403                     # 0x193
	.short	402                     # 0x192
	.short	401                     # 0x191
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	400                     # 0x190
	.short	399                     # 0x18f
	.short	398                     # 0x18e
	.short	397                     # 0x18d
	.short	396                     # 0x18c
	.short	395                     # 0x18b
	.short	394                     # 0x18a
	.short	393                     # 0x189
	.short	392                     # 0x188
	.short	391                     # 0x187
	.short	390                     # 0x186
	.short	389                     # 0x185
	.short	388                     # 0x184
	.short	387                     # 0x183
	.short	386                     # 0x182
	.short	385                     # 0x181
	.short	384                     # 0x180
	.short	383                     # 0x17f
	.short	382                     # 0x17e
	.short	381                     # 0x17d
	.short	380                     # 0x17c
	.short	379                     # 0x17b
	.short	378                     # 0x17a
	.short	377                     # 0x179
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	49                      # 0x31
	.short	49                      # 0x31
	.short	49                      # 0x31
	.short	49                      # 0x31
	.short	49                      # 0x31
	.short	49                      # 0x31
	.short	49                      # 0x31
	.short	49                      # 0x31
	.short	49                      # 0x31
	.short	55                      # 0x37
	.short	55                      # 0x37
	.short	55                      # 0x37
	.short	55                      # 0x37
	.short	55                      # 0x37
	.short	55                      # 0x37
	.short	55                      # 0x37
	.short	55                      # 0x37
	.short	55                      # 0x37
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	100                     # 0x64
	.short	100                     # 0x64
	.short	100                     # 0x64
	.short	100                     # 0x64
	.short	100                     # 0x64
	.short	100                     # 0x64
	.short	100                     # 0x64
	.short	100                     # 0x64
	.short	100                     # 0x64
	.short	118                     # 0x76
	.short	118                     # 0x76
	.short	145                     # 0x91
	.short	145                     # 0x91
	.short	146                     # 0x92
	.short	376                     # 0x178
	.short	375                     # 0x177
	.short	374                     # 0x176
	.short	373                     # 0x175
	.short	372                     # 0x174
	.short	146                     # 0x92
	.short	146                     # 0x92
	.short	146                     # 0x92
	.short	150                     # 0x96
	.short	369                     # 0x171
	.short	368                     # 0x170
	.short	150                     # 0x96
	.short	150                     # 0x96
	.short	150                     # 0x96
	.short	150                     # 0x96
	.short	150                     # 0x96
	.short	150                     # 0x96
	.short	152                     # 0x98
	.short	367                     # 0x16f
	.short	152                     # 0x98
	.short	152                     # 0x98
	.short	366                     # 0x16e
	.short	152                     # 0x98
	.short	152                     # 0x98
	.short	152                     # 0x98
	.short	152                     # 0x98
	.short	154                     # 0x9a
	.short	365                     # 0x16d
	.short	364                     # 0x16c
	.short	363                     # 0x16b
	.short	362                     # 0x16a
	.short	154                     # 0x9a
	.short	361                     # 0x169
	.short	154                     # 0x9a
	.short	164                     # 0xa4
	.short	164                     # 0xa4
	.short	172                     # 0xac
	.short	172                     # 0xac
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	433                     # 0x1b1
	.short	360                     # 0x168
	.short	359                     # 0x167
	.short	358                     # 0x166
	.short	357                     # 0x165
	.short	356                     # 0x164
	.short	355                     # 0x163
	.short	354                     # 0x162
	.short	353                     # 0x161
	.short	352                     # 0x160
	.short	351                     # 0x15f
	.short	350                     # 0x15e
	.short	349                     # 0x15d
	.short	348                     # 0x15c
	.short	347                     # 0x15b
	.short	346                     # 0x15a
	.short	333                     # 0x14d
	.short	345                     # 0x159
	.short	332                     # 0x14c
	.short	344                     # 0x158
	.short	343                     # 0x157
	.short	342                     # 0x156
	.short	341                     # 0x155
	.short	340                     # 0x154
	.short	339                     # 0x153
	.short	338                     # 0x152
	.short	337                     # 0x151
	.short	336                     # 0x150
	.short	335                     # 0x14f
	.short	334                     # 0x14e
	.short	333                     # 0x14d
	.short	332                     # 0x14c
	.short	331                     # 0x14b
	.short	330                     # 0x14a
	.short	329                     # 0x149
	.short	328                     # 0x148
	.short	327                     # 0x147
	.short	326                     # 0x146
	.short	325                     # 0x145
	.short	324                     # 0x144
	.short	323                     # 0x143
	.short	322                     # 0x142
	.short	321                     # 0x141
	.short	320                     # 0x140
	.short	319                     # 0x13f
	.short	318                     # 0x13e
	.short	317                     # 0x13d
	.short	316                     # 0x13c
	.short	315                     # 0x13b
	.short	314                     # 0x13a
	.short	313                     # 0x139
	.short	312                     # 0x138
	.short	311                     # 0x137
	.short	310                     # 0x136
	.short	309                     # 0x135
	.short	308                     # 0x134
	.short	307                     # 0x133
	.short	306                     # 0x132
	.short	305                     # 0x131
	.short	304                     # 0x130
	.short	303                     # 0x12f
	.short	302                     # 0x12e
	.short	301                     # 0x12d
	.short	300                     # 0x12c
	.short	299                     # 0x12b
	.short	298                     # 0x12a
	.short	297                     # 0x129
	.short	296                     # 0x128
	.short	295                     # 0x127
	.short	294                     # 0x126
	.short	293                     # 0x125
	.short	292                     # 0x124
	.short	291                     # 0x123
	.short	290                     # 0x122
	.short	289                     # 0x121
	.short	288                     # 0x120
	.short	287                     # 0x11f
	.short	286                     # 0x11e
	.short	285                     # 0x11d
	.short	284                     # 0x11c
	.short	283                     # 0x11b
	.short	282                     # 0x11a
	.short	281                     # 0x119
	.short	280                     # 0x118
	.short	279                     # 0x117
	.short	278                     # 0x116
	.short	277                     # 0x115
	.short	276                     # 0x114
	.short	275                     # 0x113
	.short	274                     # 0x112
	.short	273                     # 0x111
	.short	272                     # 0x110
	.short	271                     # 0x10f
	.short	270                     # 0x10e
	.short	269                     # 0x10d
	.short	268                     # 0x10c
	.short	267                     # 0x10b
	.short	266                     # 0x10a
	.short	265                     # 0x109
	.short	264                     # 0x108
	.short	263                     # 0x107
	.short	262                     # 0x106
	.short	261                     # 0x105
	.short	260                     # 0x104
	.short	259                     # 0x103
	.short	253                     # 0xfd
	.short	252                     # 0xfc
	.short	251                     # 0xfb
	.short	250                     # 0xfa
	.short	249                     # 0xf9
	.short	248                     # 0xf8
	.short	247                     # 0xf7
	.short	207                     # 0xcf
	.short	206                     # 0xce
	.short	246                     # 0xf6
	.short	245                     # 0xf5
	.short	244                     # 0xf4
	.short	243                     # 0xf3
	.short	242                     # 0xf2
	.short	241                     # 0xf1
	.short	240                     # 0xf0
	.short	239                     # 0xef
	.short	238                     # 0xee
	.short	237                     # 0xed
	.short	236                     # 0xec
	.short	235                     # 0xeb
	.short	234                     # 0xea
	.short	233                     # 0xe9
	.short	232                     # 0xe8
	.short	231                     # 0xe7
	.short	230                     # 0xe6
	.short	229                     # 0xe5
	.short	228                     # 0xe4
	.short	227                     # 0xe3
	.short	226                     # 0xe2
	.short	225                     # 0xe1
	.short	224                     # 0xe0
	.short	223                     # 0xdf
	.short	222                     # 0xde
	.short	221                     # 0xdd
	.short	220                     # 0xdc
	.short	219                     # 0xdb
	.short	218                     # 0xda
	.short	175                     # 0xaf
	.short	217                     # 0xd9
	.short	216                     # 0xd8
	.short	168                     # 0xa8
	.short	168                     # 0xa8
	.short	168                     # 0xa8
	.short	215                     # 0xd7
	.short	159                     # 0x9f
	.short	210                     # 0xd2
	.short	207                     # 0xcf
	.short	206                     # 0xce
	.short	205                     # 0xcd
	.short	204                     # 0xcc
	.short	203                     # 0xcb
	.short	202                     # 0xca
	.short	201                     # 0xc9
	.short	200                     # 0xc8
	.short	199                     # 0xc7
	.short	198                     # 0xc6
	.short	197                     # 0xc5
	.short	196                     # 0xc4
	.short	195                     # 0xc3
	.short	194                     # 0xc2
	.short	193                     # 0xc1
	.short	192                     # 0xc0
	.short	191                     # 0xbf
	.short	190                     # 0xbe
	.short	189                     # 0xbd
	.short	188                     # 0xbc
	.short	187                     # 0xbb
	.short	186                     # 0xba
	.short	185                     # 0xb9
	.short	184                     # 0xb8
	.short	183                     # 0xb7
	.short	182                     # 0xb6
	.short	112                     # 0x70
	.short	181                     # 0xb5
	.short	180                     # 0xb4
	.short	107                     # 0x6b
	.short	179                     # 0xb3
	.short	178                     # 0xb2
	.short	177                     # 0xb1
	.short	176                     # 0xb0
	.short	175                     # 0xaf
	.short	174                     # 0xae
	.short	173                     # 0xad
	.short	168                     # 0xa8
	.short	168                     # 0xa8
	.short	168                     # 0xa8
	.short	168                     # 0xa8
	.short	168                     # 0xa8
	.short	168                     # 0xa8
	.short	168                     # 0xa8
	.short	168                     # 0xa8
	.short	166                     # 0xa6
	.short	165                     # 0xa5
	.short	149                     # 0x95
	.short	107                     # 0x6b
	.short	129                     # 0x81
	.short	128                     # 0x80
	.short	127                     # 0x7f
	.short	126                     # 0x7e
	.short	125                     # 0x7d
	.short	124                     # 0x7c
	.short	123                     # 0x7b
	.short	122                     # 0x7a
	.short	121                     # 0x79
	.short	120                     # 0x78
	.short	119                     # 0x77
	.short	117                     # 0x75
	.short	112                     # 0x70
	.short	114                     # 0x72
	.short	437                     # 0x1b5
	.short	50                      # 0x32
	.short	48                      # 0x30
	.short	23                      # 0x17
	.short	23                      # 0x17
	.short	21                      # 0x15
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.short	437                     # 0x1b5
	.size	_ZL6yy_nxt, 1844

	.type	_ZL9yy_accept,@object   # @_ZL9yy_accept
	.p2align	4
_ZL9yy_accept:
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	21                      # 0x15
	.short	24                      # 0x18
	.short	26                      # 0x1a
	.short	28                      # 0x1c
	.short	30                      # 0x1e
	.short	33                      # 0x21
	.short	35                      # 0x23
	.short	38                      # 0x26
	.short	41                      # 0x29
	.short	44                      # 0x2c
	.short	47                      # 0x2f
	.short	50                      # 0x32
	.short	53                      # 0x35
	.short	56                      # 0x38
	.short	59                      # 0x3b
	.short	62                      # 0x3e
	.short	65                      # 0x41
	.short	68                      # 0x44
	.short	71                      # 0x47
	.short	74                      # 0x4a
	.short	76                      # 0x4c
	.short	79                      # 0x4f
	.short	81                      # 0x51
	.short	84                      # 0x54
	.short	87                      # 0x57
	.short	89                      # 0x59
	.short	92                      # 0x5c
	.short	95                      # 0x5f
	.short	97                      # 0x61
	.short	100                     # 0x64
	.short	103                     # 0x67
	.short	106                     # 0x6a
	.short	109                     # 0x6d
	.short	112                     # 0x70
	.short	115                     # 0x73
	.short	118                     # 0x76
	.short	121                     # 0x79
	.short	124                     # 0x7c
	.short	127                     # 0x7f
	.short	130                     # 0x82
	.short	133                     # 0x85
	.short	136                     # 0x88
	.short	139                     # 0x8b
	.short	142                     # 0x8e
	.short	145                     # 0x91
	.short	148                     # 0x94
	.short	150                     # 0x96
	.short	153                     # 0x99
	.short	156                     # 0x9c
	.short	159                     # 0x9f
	.short	162                     # 0xa2
	.short	165                     # 0xa5
	.short	168                     # 0xa8
	.short	170                     # 0xaa
	.short	173                     # 0xad
	.short	176                     # 0xb0
	.short	179                     # 0xb3
	.short	181                     # 0xb5
	.short	184                     # 0xb8
	.short	187                     # 0xbb
	.short	190                     # 0xbe
	.short	193                     # 0xc1
	.short	196                     # 0xc4
	.short	199                     # 0xc7
	.short	202                     # 0xca
	.short	205                     # 0xcd
	.short	208                     # 0xd0
	.short	210                     # 0xd2
	.short	213                     # 0xd5
	.short	216                     # 0xd8
	.short	219                     # 0xdb
	.short	222                     # 0xde
	.short	224                     # 0xe0
	.short	227                     # 0xe3
	.short	229                     # 0xe5
	.short	231                     # 0xe7
	.short	233                     # 0xe9
	.short	235                     # 0xeb
	.short	237                     # 0xed
	.short	238                     # 0xee
	.short	239                     # 0xef
	.short	240                     # 0xf0
	.short	240                     # 0xf0
	.short	240                     # 0xf0
	.short	241                     # 0xf1
	.short	242                     # 0xf2
	.short	242                     # 0xf2
	.short	243                     # 0xf3
	.short	244                     # 0xf4
	.short	245                     # 0xf5
	.short	246                     # 0xf6
	.short	247                     # 0xf7
	.short	248                     # 0xf8
	.short	249                     # 0xf9
	.short	250                     # 0xfa
	.short	251                     # 0xfb
	.short	252                     # 0xfc
	.short	253                     # 0xfd
	.short	254                     # 0xfe
	.short	255                     # 0xff
	.short	256                     # 0x100
	.short	257                     # 0x101
	.short	258                     # 0x102
	.short	259                     # 0x103
	.short	259                     # 0x103
	.short	259                     # 0x103
	.short	259                     # 0x103
	.short	259                     # 0x103
	.short	259                     # 0x103
	.short	259                     # 0x103
	.short	259                     # 0x103
	.short	259                     # 0x103
	.short	259                     # 0x103
	.short	259                     # 0x103
	.short	260                     # 0x104
	.short	261                     # 0x105
	.short	261                     # 0x105
	.short	262                     # 0x106
	.short	263                     # 0x107
	.short	265                     # 0x109
	.short	267                     # 0x10b
	.short	268                     # 0x10c
	.short	269                     # 0x10d
	.short	270                     # 0x10e
	.short	271                     # 0x10f
	.short	272                     # 0x110
	.short	273                     # 0x111
	.short	274                     # 0x112
	.short	274                     # 0x112
	.short	274                     # 0x112
	.short	275                     # 0x113
	.short	276                     # 0x114
	.short	277                     # 0x115
	.short	278                     # 0x116
	.short	279                     # 0x117
	.short	280                     # 0x118
	.short	281                     # 0x119
	.short	282                     # 0x11a
	.short	283                     # 0x11b
	.short	284                     # 0x11c
	.short	285                     # 0x11d
	.short	286                     # 0x11e
	.short	287                     # 0x11f
	.short	288                     # 0x120
	.short	289                     # 0x121
	.short	290                     # 0x122
	.short	291                     # 0x123
	.short	292                     # 0x124
	.short	292                     # 0x124
	.short	292                     # 0x124
	.short	292                     # 0x124
	.short	292                     # 0x124
	.short	292                     # 0x124
	.short	292                     # 0x124
	.short	293                     # 0x125
	.short	294                     # 0x126
	.short	295                     # 0x127
	.short	296                     # 0x128
	.short	297                     # 0x129
	.short	298                     # 0x12a
	.short	299                     # 0x12b
	.short	300                     # 0x12c
	.short	301                     # 0x12d
	.short	302                     # 0x12e
	.short	303                     # 0x12f
	.short	304                     # 0x130
	.short	305                     # 0x131
	.short	306                     # 0x132
	.short	306                     # 0x132
	.short	306                     # 0x132
	.short	306                     # 0x132
	.short	306                     # 0x132
	.short	306                     # 0x132
	.short	306                     # 0x132
	.short	306                     # 0x132
	.short	306                     # 0x132
	.short	306                     # 0x132
	.short	306                     # 0x132
	.short	307                     # 0x133
	.short	308                     # 0x134
	.short	308                     # 0x134
	.short	309                     # 0x135
	.short	309                     # 0x135
	.short	309                     # 0x135
	.short	309                     # 0x135
	.short	309                     # 0x135
	.short	309                     # 0x135
	.short	310                     # 0x136
	.short	311                     # 0x137
	.short	313                     # 0x139
	.short	313                     # 0x139
	.short	313                     # 0x139
	.short	313                     # 0x139
	.short	313                     # 0x139
	.short	313                     # 0x139
	.short	313                     # 0x139
	.short	315                     # 0x13b
	.short	316                     # 0x13c
	.short	317                     # 0x13d
	.short	318                     # 0x13e
	.short	320                     # 0x140
	.short	321                     # 0x141
	.short	322                     # 0x142
	.short	323                     # 0x143
	.short	324                     # 0x144
	.short	325                     # 0x145
	.short	326                     # 0x146
	.short	327                     # 0x147
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	328                     # 0x148
	.short	329                     # 0x149
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	330                     # 0x14a
	.short	332                     # 0x14c
	.short	333                     # 0x14d
	.short	334                     # 0x14e
	.short	335                     # 0x14f
	.short	336                     # 0x150
	.short	337                     # 0x151
	.short	338                     # 0x152
	.short	339                     # 0x153
	.short	340                     # 0x154
	.short	341                     # 0x155
	.short	342                     # 0x156
	.short	343                     # 0x157
	.short	344                     # 0x158
	.short	345                     # 0x159
	.short	346                     # 0x15a
	.short	346                     # 0x15a
	.short	346                     # 0x15a
	.short	346                     # 0x15a
	.short	346                     # 0x15a
	.short	346                     # 0x15a
	.short	346                     # 0x15a
	.short	346                     # 0x15a
	.short	346                     # 0x15a
	.short	346                     # 0x15a
	.short	346                     # 0x15a
	.short	347                     # 0x15b
	.short	348                     # 0x15c
	.short	349                     # 0x15d
	.short	349                     # 0x15d
	.short	349                     # 0x15d
	.short	349                     # 0x15d
	.short	349                     # 0x15d
	.short	349                     # 0x15d
	.short	349                     # 0x15d
	.short	349                     # 0x15d
	.short	349                     # 0x15d
	.short	349                     # 0x15d
	.short	349                     # 0x15d
	.short	350                     # 0x15e
	.short	352                     # 0x160
	.short	353                     # 0x161
	.short	354                     # 0x162
	.short	355                     # 0x163
	.short	357                     # 0x165
	.short	358                     # 0x166
	.short	359                     # 0x167
	.short	360                     # 0x168
	.short	361                     # 0x169
	.short	361                     # 0x169
	.short	361                     # 0x169
	.short	361                     # 0x169
	.short	362                     # 0x16a
	.short	362                     # 0x16a
	.short	363                     # 0x16b
	.short	363                     # 0x16b
	.short	363                     # 0x16b
	.short	363                     # 0x16b
	.short	363                     # 0x16b
	.short	364                     # 0x16c
	.short	366                     # 0x16e
	.short	366                     # 0x16e
	.short	366                     # 0x16e
	.short	366                     # 0x16e
	.short	366                     # 0x16e
	.short	366                     # 0x16e
	.short	366                     # 0x16e
	.short	366                     # 0x16e
	.short	366                     # 0x16e
	.short	366                     # 0x16e
	.short	366                     # 0x16e
	.short	368                     # 0x170
	.short	369                     # 0x171
	.short	370                     # 0x172
	.short	371                     # 0x173
	.short	373                     # 0x175
	.short	374                     # 0x176
	.short	376                     # 0x178
	.short	377                     # 0x179
	.short	377                     # 0x179
	.short	378                     # 0x17a
	.short	379                     # 0x17b
	.short	379                     # 0x17b
	.short	379                     # 0x17b
	.short	379                     # 0x17b
	.short	379                     # 0x17b
	.short	379                     # 0x17b
	.short	380                     # 0x17c
	.short	380                     # 0x17c
	.short	380                     # 0x17c
	.short	380                     # 0x17c
	.short	380                     # 0x17c
	.short	380                     # 0x17c
	.short	380                     # 0x17c
	.short	380                     # 0x17c
	.short	380                     # 0x17c
	.short	381                     # 0x17d
	.short	381                     # 0x17d
	.short	381                     # 0x17d
	.short	383                     # 0x17f
	.short	385                     # 0x181
	.short	387                     # 0x183
	.short	389                     # 0x185
	.short	391                     # 0x187
	.short	391                     # 0x187
	.short	391                     # 0x187
	.short	391                     # 0x187
	.short	391                     # 0x187
	.short	391                     # 0x187
	.short	391                     # 0x187
	.short	391                     # 0x187
	.short	392                     # 0x188
	.short	393                     # 0x189
	.short	394                     # 0x18a
	.short	394                     # 0x18a
	.short	394                     # 0x18a
	.short	394                     # 0x18a
	.short	394                     # 0x18a
	.short	394                     # 0x18a
	.short	394                     # 0x18a
	.short	395                     # 0x18b
	.short	396                     # 0x18c
	.short	397                     # 0x18d
	.short	397                     # 0x18d
	.short	397                     # 0x18d
	.short	398                     # 0x18e
	.short	398                     # 0x18e
	.short	398                     # 0x18e
	.short	399                     # 0x18f
	.short	399                     # 0x18f
	.short	399                     # 0x18f
	.short	399                     # 0x18f
	.short	399                     # 0x18f
	.short	399                     # 0x18f
	.short	399                     # 0x18f
	.short	399                     # 0x18f
	.short	399                     # 0x18f
	.short	399                     # 0x18f
	.short	399                     # 0x18f
	.short	400                     # 0x190
	.short	400                     # 0x190
	.short	400                     # 0x190
	.short	401                     # 0x191
	.short	402                     # 0x192
	.short	402                     # 0x192
	.short	402                     # 0x192
	.short	402                     # 0x192
	.short	402                     # 0x192
	.short	402                     # 0x192
	.short	402                     # 0x192
	.short	402                     # 0x192
	.short	403                     # 0x193
	.short	403                     # 0x193
	.short	404                     # 0x194
	.short	404                     # 0x194
	.short	404                     # 0x194
	.short	404                     # 0x194
	.short	404                     # 0x194
	.short	404                     # 0x194
	.short	404                     # 0x194
	.short	404                     # 0x194
	.short	406                     # 0x196
	.short	406                     # 0x196
	.short	406                     # 0x196
	.short	407                     # 0x197
	.short	408                     # 0x198
	.short	409                     # 0x199
	.short	409                     # 0x199
	.short	409                     # 0x199
	.short	410                     # 0x19a
	.short	411                     # 0x19b
	.short	412                     # 0x19c
	.short	413                     # 0x19d
	.short	414                     # 0x19e
	.short	414                     # 0x19e
	.size	_ZL9yy_accept, 878

	.type	_ZL5yy_lp,@object       # @_ZL5yy_lp
	.local	_ZL5yy_lp
	.comm	_ZL5yy_lp,4,4
	.type	_ZL10yy_acclist,@object # @_ZL10yy_acclist
	.p2align	4
_ZL10yy_acclist:
	.short	0                       # 0x0
	.short	42                      # 0x2a
	.short	42                      # 0x2a
	.short	82                      # 0x52
	.short	82                      # 0x52
	.short	94                      # 0x5e
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	93                      # 0x5d
	.short	1                       # 0x1
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	1                       # 0x1
	.short	93                      # 0x5d
	.short	54                      # 0x36
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	55                      # 0x37
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	23                      # 0x17
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	45                      # 0x2d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	13                      # 0xd
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	13                      # 0xd
	.short	93                      # 0x5d
	.short	13                      # 0xd
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	11                      # 0xb
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	12                      # 0xc
	.short	93                      # 0x5d
	.short	10                      # 0xa
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	46                      # 0x2e
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	51                      # 0x33
	.short	93                      # 0x5d
	.short	50                      # 0x32
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	58                      # 0x3a
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	59                      # 0x3b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	57                      # 0x39
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	46                      # 0x2e
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	56                      # 0x38
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	60                      # 0x3c
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	52                      # 0x34
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	61                      # 0x3d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	62                      # 0x3e
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	63                      # 0x3f
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	52                      # 0x34
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	67                      # 0x43
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	65                      # 0x41
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	52                      # 0x34
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	76                      # 0x4c
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	76                      # 0x4c
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	51                      # 0x33
	.short	93                      # 0x5d
	.short	73                      # 0x49
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	77                      # 0x4d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	75                      # 0x4b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	70                      # 0x46
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	75                      # 0x4b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	42                      # 0x2a
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	42                      # 0x2a
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	44                      # 0x2c
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	44                      # 0x2c
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	44                      # 0x2c
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	43                      # 0x2b
	.short	81                      # 0x51
	.short	93                      # 0x5d
	.short	92                      # 0x5c
	.short	93                      # 0x5d
	.short	82                      # 0x52
	.short	92                      # 0x5c
	.short	93                      # 0x5d
	.short	83                      # 0x53
	.short	93                      # 0x5d
	.short	92                      # 0x5c
	.short	93                      # 0x5d
	.short	92                      # 0x5c
	.short	93                      # 0x5d
	.short	92                      # 0x5c
	.short	93                      # 0x5d
	.short	92                      # 0x5c
	.short	93                      # 0x5d
	.short	47                      # 0x2f
	.short	48                      # 0x30
	.short	49                      # 0x31
	.short	23                      # 0x17
	.short	20                      # 0x14
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	41                      # 0x29
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	9                       # 0x9
	.short	14                      # 0xe
	.short	10                      # 0xa
	.short	46                      # 0x2e
	.short	16                      # 0x10
	.short	46                      # 0x2e
	.short	18                      # 0x12
	.short	46                      # 0x2e
	.short	53                      # 0x35
	.short	62                      # 0x3e
	.short	64                      # 0x40
	.short	67                      # 0x43
	.short	66                      # 0x42
	.short	76                      # 0x4c
	.short	76                      # 0x4c
	.short	68                      # 0x44
	.short	73                      # 0x49
	.short	71                      # 0x47
	.short	72                      # 0x48
	.short	16                      # 0x10
	.short	18                      # 0x12
	.short	75                      # 0x4b
	.short	69                      # 0x45
	.short	75                      # 0x4b
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	43                      # 0x2b
	.short	43                      # 0x2b
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	44                      # 0x2c
	.short	44                      # 0x2c
	.short	82                      # 0x52
	.short	40                      # 0x28
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	9                       # 0x9
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	75                      # 0x4b
	.short	44                      # 0x2c
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	26                      # 0x1a
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	24                      # 0x18
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	75                      # 0x4b
	.short	44                      # 0x2c
	.short	30                      # 0x1e
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	38                      # 0x26
	.short	39                      # 0x27
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	75                      # 0x4b
	.short	44                      # 0x2c
	.short	43                      # 0x2b
	.short	45                      # 0x2d
	.short	28                      # 0x1c
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	25                      # 0x19
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	4                       # 0x4
	.short	2                       # 0x2
	.short	75                      # 0x4b
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	22                      # 0x16
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	29                      # 0x1d
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	33                      # 0x21
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	37                      # 0x25
	.short	8                       # 0x8
	.short	75                      # 0x4b
	.short	84                      # 0x54
	.short	34                      # 0x22
	.short	45                      # 0x2d
	.short	21                      # 0x15
	.short	45                      # 0x2d
	.short	27                      # 0x1b
	.short	45                      # 0x2d
	.short	32                      # 0x20
	.short	45                      # 0x2d
	.short	31                      # 0x1f
	.short	45                      # 0x2d
	.short	75                      # 0x4b
	.short	43                      # 0x2b
	.short	85                      # 0x55
	.short	5                       # 0x5
	.short	3                       # 0x3
	.short	7                       # 0x7
	.short	78                      # 0x4e
	.short	75                      # 0x4b
	.short	75                      # 0x4b
	.short	87                      # 0x57
	.short	86                      # 0x56
	.short	75                      # 0x4b
	.short	88                      # 0x58
	.short	75                      # 0x4b
	.short	16458                   # 0x404a
	.short	91                      # 0x5b
	.short	6                       # 0x6
	.short	16458                   # 0x404a
	.short	80                      # 0x50
	.short	8266                    # 0x204a
	.short	89                      # 0x59
	.short	90                      # 0x5a
	.short	79                      # 0x4f
	.size	_ZL10yy_acclist, 828

	.type	_ZL26yy_looking_for_trail_begin,@object # @_ZL26yy_looking_for_trail_begin
	.local	_ZL26yy_looking_for_trail_begin
	.comm	_ZL26yy_looking_for_trail_begin,4,4
	.type	_ZL13yy_full_match,@object # @_ZL13yy_full_match
	.local	_ZL13yy_full_match
	.comm	_ZL13yy_full_match,8,8
	.type	_ZL13yy_full_state,@object # @_ZL13yy_full_state
	.local	_ZL13yy_full_state
	.comm	_ZL13yy_full_state,8,8
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"token too large, exceeds YYLMAX"
	.size	.L.str.1, 32

	.type	_ZL21yy_rule_can_match_eol,@object # @_ZL21yy_rule_can_match_eol
	.section	.rodata,"a",@progbits
	.p2align	4
_ZL21yy_rule_can_match_eol:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	_ZL21yy_rule_can_match_eol, 376

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"Opening '%{' (nested C++ includes; preceding closing '%}' not found)"
	.size	.L.str.2, 69

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\\"
	.size	.L.str.3, 2

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\\\\"
	.size	.L.str.4, 3

	.type	_ZN12_GLOBAL__N_119cinit_paren_nestingE,@object # @_ZN12_GLOBAL__N_119cinit_paren_nestingE
	.local	_ZN12_GLOBAL__N_119cinit_paren_nestingE
	.comm	_ZN12_GLOBAL__N_119cinit_paren_nestingE,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"("
	.size	.L.str.5, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	")"
	.size	.L.str.6, 2

	.type	_ZN12_GLOBAL__N_119cinit_array_nestingE,@object # @_ZN12_GLOBAL__N_119cinit_array_nestingE
	.local	_ZN12_GLOBAL__N_119cinit_array_nestingE
	.comm	_ZN12_GLOBAL__N_119cinit_array_nestingE,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"["
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"]"
	.size	.L.str.8, 2

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\\\""
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\\'"
	.size	.L.str.10, 3

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"$"
	.size	.L.str.11, 2

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"{"
	.size	.L.str.12, 2

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"}"
	.size	.L.str.13, 2

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"foreach"
	.size	.L.str.14, 8

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"afterforeach"
	.size	.L.str.15, 13

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"with"
	.size	.L.str.16, 5

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"yystype.h"
	.size	.L.str.17, 10

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Name of yystype header is set to "
	.size	.L.str.18, 34

	.type	_ZL10yy_n_chars,@object # @_ZL10yy_n_chars
	.local	_ZL10yy_n_chars
	.comm	_ZL10yy_n_chars,4,4
	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"fatal flex scanner internal error--no action found"
	.size	.L.str.19, 51

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"out of dynamic memory in yy_create_buffer()"
	.size	.L.str.20, 44

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"out of dynamic memory in yy_scan_buffer()"
	.size	.L.str.21, 42

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"out of dynamic memory in yy_scan_bytes()"
	.size	.L.str.22, 41

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"bad buffer in yy_scan_bytes()"
	.size	.L.str.23, 30

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"fatal flex scanner internal error--end of buffer missed"
	.size	.L.str.24, 56

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"input buffer overflow, can't enlarge buffer because scanner uses REJECT"
	.size	.L.str.25, 72

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"input in flex scanner failed"
	.size	.L.str.26, 29

	.type	_ZL19yy_buffer_stack_max,@object # @_ZL19yy_buffer_stack_max
	.local	_ZL19yy_buffer_stack_max
	.comm	_ZL19yy_buffer_stack_max,8,8
	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"%s\n"
	.size	.L.str.27, 4

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"opening parenthesis '(' was not closed"
	.size	.L.str.28, 39

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"opening bracket '[' was not closed"
	.size	.L.str.29, 35

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"start of comment containing unexpected eof"
	.size	.L.str.30, 43

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"basic_string::append"
	.size	.L.str.32, 21

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_kimwl.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
