	.text
	.file	"reservoir.bc"
	.globl	ResvFrameBegin
	.p2align	4, 0x90
	.type	ResvFrameBegin,@function
ResvFrameBegin:                         # @ResvFrameBegin
	.cfi_startproc
# BB#0:
	cmpq	$0, 168(%rdi)
	je	.LBB0_2
# BB#1:                                 # %._crit_edge
	movl	ResvSize(%rip), %eax
	jmp	.LBB0_3
.LBB0_2:
	movl	$0, ResvSize(%rip)
	xorl	%eax, %eax
.LBB0_3:
	cmpl	$1, 192(%rdi)
	movl	$4088, %r8d             # imm = 0xFF8
	movl	$2040, %esi             # imm = 0x7F8
	cmovel	%r8d, %esi
	imull	200(%rdi), %edx
	addl	%eax, %edx
	movl	$7680, %eax             # imm = 0x1E00
	subl	%ecx, %eax
	xorl	%r8d, %r8d
	cmpl	$7680, %ecx             # imm = 0x1E00
	cmovgl	%r8d, %eax
	cmpl	$0, 72(%rdi)
	cmovnel	%r8d, %eax
	cmpl	%esi, %eax
	cmovgl	%esi, %eax
	movl	%eax, ResvMax(%rip)
	movl	%edx, %eax
	retq
.Lfunc_end0:
	.size	ResvFrameBegin, .Lfunc_end0-ResvFrameBegin
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4624746457346762342     # double 15.199999999999999
	.text
	.globl	ResvMaxBits
	.p2align	4, 0x90
	.type	ResvMaxBits,@function
ResvMaxBits:                            # @ResvMaxBits
	.cfi_startproc
# BB#0:
	movl	%edi, (%rsi)
	movl	ResvSize(%rip), %r8d
	movl	ResvMax(%rip), %r9d
	leal	(%r9,%r9,8), %eax
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rax
	shrq	$63, %rax
	sarq	$34, %rcx
	addl	%eax, %ecx
	movl	%r8d, %eax
	subl	%ecx, %eax
	jle	.LBB1_2
# BB#1:
	addl	%eax, %edi
	jmp	.LBB1_3
.LBB1_2:
	cvtsi2sdl	%edi, %xmm0
	divsd	.LCPI1_0(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	subl	%eax, %edi
	xorl	%eax, %eax
.LBB1_3:
	movl	%edi, (%rsi)
	addl	%r9d, %r9d
	leal	(%r9,%r9,2), %ecx
	movslq	%ecx, %rcx
	imulq	$1717986919, %rcx, %rcx # imm = 0x66666667
	movq	%rcx, %rsi
	shrq	$63, %rsi
	sarq	$34, %rcx
	addl	%esi, %ecx
	cmpl	%ecx, %r8d
	cmovlel	%r8d, %ecx
	xorl	%esi, %esi
	subl	%eax, %ecx
	cmovsl	%esi, %ecx
	movl	%ecx, (%rdx)
	retq
.Lfunc_end1:
	.size	ResvMaxBits, .Lfunc_end1-ResvMaxBits
	.cfi_endproc

	.globl	ResvAdjust
	.p2align	4, 0x90
	.type	ResvAdjust,@function
ResvAdjust:                             # @ResvAdjust
	.cfi_startproc
# BB#0:
	movl	%ecx, %eax
	cltd
	idivl	204(%rdi)
	subl	(%rsi), %eax
	addl	%eax, ResvSize(%rip)
	retq
.Lfunc_end2:
	.size	ResvAdjust, .Lfunc_end2-ResvAdjust
	.cfi_endproc

	.globl	ResvFrameEnd
	.p2align	4, 0x90
	.type	ResvFrameEnd,@function
ResvFrameEnd:                           # @ResvFrameEnd
	.cfi_startproc
# BB#0:
	testb	$1, %dl
	movl	ResvSize(%rip), %eax
	je	.LBB3_3
# BB#1:
	cmpl	$2, 204(%rdi)
	jne	.LBB3_3
# BB#2:
	incl	%eax
	movl	%eax, ResvSize(%rip)
.LBB3_3:                                # %._crit_edge
	xorl	%ecx, %ecx
	movl	%eax, %edx
	subl	ResvMax(%rip), %edx
	cmovsl	%ecx, %edx
	subl	%edx, %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	andl	$-8, %ecx
	subl	%ecx, %eax
	addl	%edx, %eax
	movl	%ecx, ResvSize(%rip)
	movl	%eax, 8(%rsi)
	retq
.Lfunc_end3:
	.size	ResvFrameEnd, .Lfunc_end3-ResvFrameEnd
	.cfi_endproc

	.type	ResvSize,@object        # @ResvSize
	.local	ResvSize
	.comm	ResvSize,4,4
	.type	ResvMax,@object         # @ResvMax
	.local	ResvMax
	.comm	ResvMax,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
