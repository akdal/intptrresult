for i in 2 3 4 ; do
python2 ../../lnt/parse.py \
  /home/juneyoung.lee/inttoptr/remote-result/freeze${i}/lntresult.0924.insts-5.0.nocapture-5.0.insts-opt-5.0.nocapture-opt-5.0/mysandbox-copyforrsync/mysandbox cc${i}.csv rt${i}.csv
echo "-----"

for j in 1 5 ;  do
for k in cc rt ; do
  echo "Making cc${i}.sorted.${j}per.csv : "
  python2 ../../lnt/columns-get.py ../20170924.base-5.0.insts-5.0.nocapture-5.0/${k}${i}.csv 0 4 ${k}${i}.csv 1 4 ${k}${i}.insts-5.0.csv
  python2 ../../lnt/columns-get.py ../20170924.base-5.0.insts-5.0.nocapture-5.0/${k}${i}.csv 0 4 ${k}${i}.csv 9 12 ${k}${i}.nocapture-5.0.csv
  python2 ../../lnt/columns-get.py ../20170924.base-5.0.insts-5.0.nocapture-5.0/${k}${i}.csv 0 4 ${k}${i}.csv 5 8 ${k}${i}.insts-opt-5.0.csv
  python2 ../../lnt/columns-get.py ../20170924.base-5.0.insts-5.0.nocapture-5.0/${k}${i}.csv 0 4 ${k}${i}.csv 13 16 ${k}${i}.nocapture-opt-5.0.csv
  echo "sort.py ${k}${i}.insts-5.0.csv"
  python2 ../../lnt/sort.py ${k}${i}.insts-5.0.csv ${k}${i}.insts-5.0.sorted.${j}per.csv ${j}
  echo "sort.py ${k}${i}.nocapture-5.0.csv"
  python2 ../../lnt/sort.py ${k}${i}.nocapture-5.0.csv ${k}${i}.nocapture-5.0.sorted.${j}per.csv ${j}
  echo "sort.py ${k}${i}.insts-opt-5.0.csv"
  python2 ../../lnt/sort.py ${k}${i}.insts-opt-5.0.csv ${k}${i}.insts-opt-5.0.sorted.${j}per.csv ${j}
  echo "sort.py ${k}${i}.nocapture-opt-5.0.csv"
  python2 ../../lnt/sort.py ${k}${i}.nocapture-opt-5.0.csv ${k}${i}.nocapture-opt-5.0.sorted.${j}per.csv ${j}
done
done
echo "-----"
#./filter-and-sort.sh
done

python2 ../../lnt/maketable.py . rt 2,3,4 insts-5.0,insts-opt-5.0,nocapture-5.0,nocapture-opt-5.0 sorted.5per runtime.csv
python2 ../../lnt/maketable.py . cc 2,3,4 insts-5.0,insts-opt-5.0,nocapture-5.0,nocapture-opt-5.0 sorted.5per compilationtime.csv
