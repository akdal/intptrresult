	.text
	.file	"libclamav_unzip.bc"
	.globl	zip_dir_close
	.p2align	4, 0x90
	.type	zip_dir_close,@function
zip_dir_close:                          # @zip_dir_close
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#1:
	callq	free
.LBB0_2:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#3:
	callq	free
.LBB0_4:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#5:
	callq	free
.LBB0_6:
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	zip_dir_close, .Lfunc_end0-zip_dir_close
	.cfi_endproc

	.globl	zip_dir_open
	.p2align	4, 0x90
	.type	zip_dir_open,@function
zip_dir_open:                           # @zip_dir_open
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi8:
	.cfi_def_cfa_offset 496
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movl	%edi, %ebx
	movl	$1, %edi
	movl	$72, %esi
	callq	cli_calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_6
# BB#1:
	testq	%rbp, %rbp
	je	.LBB1_3
# BB#2:
	xorl	%edx, %edx
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB1_37
.LBB1_3:
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%r15, %r13
	movl	%ebx, (%r13)
	leaq	296(%rsp), %rdx
	movl	$1, %edi
	movl	%ebx, %esi
	callq	__fxstat
	movl	(%r13), %ebx
	cmpl	$-1, %eax
	je	.LBB1_8
# BB#4:
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movq	344(%rsp), %rcx
	movq	%rcx, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	cmpq	$21, %rcx
	jg	.LBB1_9
# BB#5:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB1_69
.LBB1_6:
	testq	%r14, %r14
	jne	.LBB1_78
	jmp	.LBB1_79
.LBB1_8:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	jmp	.LBB1_70
.LBB1_9:
	movl	$1024, %edi             # imm = 0x400
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_69
# BB#10:                                # %.lr.ph111.i.i
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	cmpq	$1023, %rbx             # imm = 0x3FF
	jg	.LBB1_39
# BB#11:                                # %.lr.ph111.split.us.i.preheader.i
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph111.split.us.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_30 Depth 2
	cmpq	$1024, %rbx             # imm = 0x400
	jl	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_12 Depth=1
	cmpq	88(%rsp), %rbx          # 8-byte Folded Reload
	movq	$-1002, %rax            # imm = 0xFC16
	movq	$-1024, %rcx            # imm = 0xFC00
	cmoveq	%rcx, %rax
	addq	%rbx, %rax
	movl	$1024, %ebx             # imm = 0x400
	jmp	.LBB1_15
	.p2align	4, 0x90
.LBB1_14:                               #   in Loop: Header=BB1_12 Depth=1
	xorl	%eax, %eax
.LBB1_15:                               #   in Loop: Header=BB1_12 Depth=1
	movq	%rax, %rsi
	xorl	%edx, %edx
	movl	4(%rsp), %ebp           # 4-byte Reload
	movl	%ebp, %edi
	movq	%rsi, %r12
	callq	lseek
	testq	%rax, %rax
	js	.LBB1_63
# BB#16:                                #   in Loop: Header=BB1_12 Depth=1
	movl	%ebp, %edi
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	cli_readn
	cltq
	cmpq	%rbx, %rax
	jl	.LBB1_64
# BB#17:                                #   in Loop: Header=BB1_12 Depth=1
	movq	%r14, 8(%rsp)           # 8-byte Spill
	leaq	-1(%r15,%rbx), %r13
	cmpq	%r15, %r13
	jae	.LBB1_19
# BB#18:                                #   in Loop: Header=BB1_12 Depth=1
	movq	%r12, %rbx
	jmp	.LBB1_36
	.p2align	4, 0x90
.LBB1_19:                               #   in Loop: Header=BB1_12 Depth=1
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rbx, %r14
	addq	%r15, %r14
	movq	%r12, %rbx
	jmp	.LBB1_30
.LBB1_20:                               # %.preheader.us.i.i
                                        #   in Loop: Header=BB1_30 Depth=2
	movzwl	8(%r13), %eax
	movw	%ax, 22(%rsp)           # 2-byte Spill
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	(%rbp,%rcx), %esi
	leaq	46(%rsi), %rax
	cmpq	72(%rsp), %rax          # 8-byte Folded Reload
	jae	.LBB1_24
# BB#21:                                #   in Loop: Header=BB1_30 Depth=2
	xorl	%edx, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	callq	lseek
	testq	%rax, %rax
	js	.LBB1_81
# BB#22:                                #   in Loop: Header=BB1_30 Depth=2
	movl	$46, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	leaq	152(%rsp), %rsi
	callq	cli_readn
	cmpl	$46, %eax
	jl	.LBB1_89
# BB#23:                                #   in Loop: Header=BB1_30 Depth=2
	cmpl	$33639248, 152(%rsp)    # imm = 0x2014B50
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	jne	.LBB1_25
	jmp	.LBB1_83
.LBB1_24:                               #   in Loop: Header=BB1_30 Depth=2
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
.LBB1_25:                               #   in Loop: Header=BB1_30 Depth=2
	leal	(%rcx,%rbp), %esi
	leaq	46(%rsi), %rax
	cmpq	72(%rsp), %rax          # 8-byte Folded Reload
	jae	.LBB1_29
# BB#26:                                #   in Loop: Header=BB1_30 Depth=2
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	callq	lseek
	testq	%rax, %rax
	js	.LBB1_81
# BB#27:                                #   in Loop: Header=BB1_30 Depth=2
	movl	$46, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	leaq	152(%rsp), %rsi
	callq	cli_readn
	cmpl	$46, %eax
	jl	.LBB1_89
# BB#28:                                #   in Loop: Header=BB1_30 Depth=2
	cmpl	$33639248, 152(%rsp)    # imm = 0x2014B50
	movq	80(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jne	.LBB1_35
	jmp	.LBB1_84
.LBB1_29:                               #   in Loop: Header=BB1_30 Depth=2
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_30:                               #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$80, (%r13)
	jne	.LBB1_35
# BB#31:                                #   in Loop: Header=BB1_30 Depth=2
	movq	%r14, %rax
	subq	%r13, %rax
	cmpq	$20, %rax
	jl	.LBB1_35
# BB#32:                                #   in Loop: Header=BB1_30 Depth=2
	cmpl	$101010256, (%r13)      # imm = 0x6054B50
	jne	.LBB1_35
# BB#33:                                # %__fixup_rootseek.exit.us.i.i
                                        #   in Loop: Header=BB1_30 Depth=2
	movl	16(%r13), %eax
	leaq	(%r13,%rbx), %rcx
	subq	%r15, %rcx
	movl	12(%r13), %r12d
	movq	%rcx, %rbp
	subq	%r12, %rbp
	cmpq	%rbp, %rax
	cmovlel	%eax, %ebp
	cmpq	%r12, %rcx
	cmovlel	%eax, %ebp
	cmpl	88(%rsp), %ebp          # 4-byte Folded Reload
	jbe	.LBB1_20
# BB#34:                                #   in Loop: Header=BB1_30 Depth=2
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	.p2align	4, 0x90
.LBB1_35:                               # %.backedge.us.i.i
                                        #   in Loop: Header=BB1_30 Depth=2
	decq	%r13
	cmpq	%r15, %r13
	jae	.LBB1_30
.LBB1_36:                               # %.loopexit84.us.i.i
                                        #   in Loop: Header=BB1_12 Depth=1
	testq	%rbx, %rbx
	movq	8(%rsp), %r14           # 8-byte Reload
	jg	.LBB1_12
	jmp	.LBB1_62
.LBB1_37:
	xorl	%r13d, %r13d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	testq	%r14, %r14
	je	.LBB1_80
# BB#38:
	movl	$-123, (%r14)
	jmp	.LBB1_79
.LBB1_39:                               # %.lr.ph111.split.i.i
	leaq	1024(%r15), %r13
	leaq	1023(%r15), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movq	%rbx, %r14
	.p2align	4, 0x90
.LBB1_40:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_55 Depth 2
	cmpq	$1024, %r14             # imm = 0x400
	jl	.LBB1_42
# BB#41:                                #   in Loop: Header=BB1_40 Depth=1
	cmpq	88(%rsp), %r14          # 8-byte Folded Reload
	movq	$-1002, %rax            # imm = 0xFC16
	movq	$-1024, %rcx            # imm = 0xFC00
	cmoveq	%rcx, %rax
	addq	%rax, %r14
	jmp	.LBB1_43
	.p2align	4, 0x90
.LBB1_42:                               #   in Loop: Header=BB1_40 Depth=1
	xorl	%r14d, %r14d
.LBB1_43:                               #   in Loop: Header=BB1_40 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
	xorl	%edx, %edx
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	lseek
	testq	%rax, %rax
	js	.LBB1_63
# BB#44:                                #   in Loop: Header=BB1_40 Depth=1
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movl	$1024, %r12d            # imm = 0x400
	movl	$1024, %edx             # imm = 0x400
	movl	%ebp, %edi
	movq	%r15, %rsi
	callq	cli_readn
	cmpl	$1024, %eax             # imm = 0x400
	movq	48(%rsp), %rbx          # 8-byte Reload
	jge	.LBB1_55
	jmp	.LBB1_65
.LBB1_45:                               # %.preheader.i.i
                                        #   in Loop: Header=BB1_55 Depth=2
	movzwl	8(%rbx), %eax
	movw	%ax, 22(%rsp)           # 2-byte Spill
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	(%rbp,%rcx), %esi
	leaq	46(%rsi), %rax
	cmpq	72(%rsp), %rax          # 8-byte Folded Reload
	jae	.LBB1_49
# BB#46:                                #   in Loop: Header=BB1_55 Depth=2
	xorl	%edx, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	callq	lseek
	testq	%rax, %rax
	js	.LBB1_81
# BB#47:                                #   in Loop: Header=BB1_55 Depth=2
	movl	$46, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	leaq	152(%rsp), %rsi
	callq	cli_readn
	cmpl	$45, %eax
	jle	.LBB1_82
# BB#48:                                #   in Loop: Header=BB1_55 Depth=2
	cmpl	$33639248, 152(%rsp)    # imm = 0x2014B50
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	jne	.LBB1_50
	jmp	.LBB1_83
.LBB1_49:                               #   in Loop: Header=BB1_55 Depth=2
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
.LBB1_50:                               #   in Loop: Header=BB1_55 Depth=2
	leal	(%rcx,%rbp), %esi
	leaq	46(%rsi), %rax
	cmpq	72(%rsp), %rax          # 8-byte Folded Reload
	jae	.LBB1_54
# BB#51:                                #   in Loop: Header=BB1_55 Depth=2
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	callq	lseek
	testq	%rax, %rax
	js	.LBB1_81
# BB#52:                                #   in Loop: Header=BB1_55 Depth=2
	movl	$46, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	leaq	152(%rsp), %rsi
	callq	cli_readn
	cmpl	$46, %eax
	jl	.LBB1_82
# BB#53:                                #   in Loop: Header=BB1_55 Depth=2
	cmpl	$33639248, 152(%rsp)    # imm = 0x2014B50
	movq	80(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jne	.LBB1_60
	jmp	.LBB1_84
.LBB1_54:                               #   in Loop: Header=BB1_55 Depth=2
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jmp	.LBB1_60
	.p2align	4, 0x90
.LBB1_55:                               #   Parent Loop BB1_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$80, (%rbx)
	jne	.LBB1_60
# BB#56:                                #   in Loop: Header=BB1_55 Depth=2
	movq	%r13, %rax
	subq	%rbx, %rax
	cmpq	$20, %rax
	jl	.LBB1_60
# BB#57:                                #   in Loop: Header=BB1_55 Depth=2
	cmpl	$101010256, (%rbx)      # imm = 0x6054B50
	jne	.LBB1_60
# BB#58:                                # %__fixup_rootseek.exit.i.i
                                        #   in Loop: Header=BB1_55 Depth=2
	movl	16(%rbx), %eax
	leaq	(%rbx,%r14), %rcx
	subq	%r15, %rcx
	movl	12(%rbx), %r12d
	movq	%rcx, %rbp
	subq	%r12, %rbp
	cmpq	%rbp, %rax
	cmovlel	%eax, %ebp
	cmpq	%r12, %rcx
	cmovlel	%eax, %ebp
	cmpl	88(%rsp), %ebp          # 4-byte Folded Reload
	jbe	.LBB1_45
# BB#59:                                #   in Loop: Header=BB1_55 Depth=2
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	.p2align	4, 0x90
.LBB1_60:                               # %.backedge.i.i
                                        #   in Loop: Header=BB1_55 Depth=2
	decq	%rbx
	cmpq	%r15, %rbx
	jae	.LBB1_55
# BB#61:                                # %.loopexit84.i.i
                                        #   in Loop: Header=BB1_40 Depth=1
	testq	%r14, %r14
	movq	8(%rsp), %r12           # 8-byte Reload
	jg	.LBB1_40
.LBB1_62:                               # %._crit_edge.i.i
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_67
.LBB1_63:                               # %.us-lcssa.us.i.i
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	jmp	.LBB1_66
.LBB1_64:
	movq	%rbx, %r12
.LBB1_65:                               # %.us-lcssa112.us.i.i
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
.LBB1_66:                               # %__zip_find_disk_trailer.exit.thread.i
	callq	cli_errmsg
.LBB1_67:                               # %__zip_find_disk_trailer.exit.thread.i
	movq	%r15, %rdi
	callq	free
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB1_68:                               # %__zip_find_disk_trailer.exit.thread.i
	movq	40(%rsp), %r13          # 8-byte Reload
.LBB1_69:                               # %__zip_find_disk_trailer.exit.thread.i
	movq	56(%rsp), %r15          # 8-byte Reload
.LBB1_70:
	addq	$24, %r15
.LBB1_71:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB1_73
# BB#72:
	callq	free
.LBB1_73:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB1_75
# BB#74:
	callq	free
.LBB1_75:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB1_77
# BB#76:
	callq	free
.LBB1_77:                               # %zip_dir_close.exit
	movq	%r13, %rdi
	callq	free
	testq	%r14, %r14
	je	.LBB1_79
.LBB1_78:
	movl	$-114, (%r14)
.LBB1_79:
	xorl	%r13d, %r13d
.LBB1_80:
	movq	%r13, %rax
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_81:                               # %.us-lcssa113.us.i.i
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	4(%rsp), %esi           # 4-byte Reload
	jmp	.LBB1_66
.LBB1_82:
	movl	$1024, %esi             # imm = 0x400
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_90
.LBB1_83:
	movq	8(%rsp), %rax           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 96(%rsp)          # 8-byte Spill
.LBB1_84:                               # %.loopexit.i
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	movq	32(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	96(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	free
	movq	40(%rsp), %r13          # 8-byte Reload
	movl	(%r13), %ebx
	movq	56(%rsp), %r15          # 8-byte Reload
	addq	$24, %r15
	leaq	152(%rsp), %rdx
	movl	$1, %edi
	movl	%ebx, %esi
	callq	__fxstat
	cmpl	$-1, %eax
	movq	24(%rsp), %r14          # 8-byte Reload
	je	.LBB1_88
# BB#85:
	cmpw	$0, 22(%rsp)            # 2-byte Folded Reload
	je	.LBB1_91
# BB#86:
	cmpl	%r12d, 200(%rsp)
	jae	.LBB1_93
# BB#87:                                # %.critedge23
	movl	$.L.str.22, %edi
	jmp	.LBB1_92
.LBB1_88:                               # %.critedge
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	jmp	.LBB1_71
.LBB1_89:
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB1_90:                               # %.us-lcssa114.us.i.i
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_errmsg
	movq	%r15, %rdi
	callq	free
	jmp	.LBB1_68
.LBB1_91:                               # %.critedge26
	movl	$.L.str.21, %edi
.LBB1_92:
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB1_71
.LBB1_93:
	movl	%r12d, %edi
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB1_71
# BB#94:                                # %.lr.ph.i9.i
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	addl	96(%rsp), %ebp          # 4-byte Folded Reload
	movzwl	22(%rsp), %ecx          # 2-byte Folded Reload
	movl	%ecx, 68(%rsp)          # 4-byte Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	movq	%r15, 56(%rsp)          # 8-byte Spill
.LBB1_95:                               # =>This Inner Loop Header: Depth=1
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	leal	(%rbp,%rdx), %esi
	xorl	%edx, %edx
	movl	4(%rsp), %r14d          # 4-byte Reload
	movl	%r14d, %edi
	callq	lseek
	testq	%rax, %rax
	js	.LBB1_112
# BB#96:                                #   in Loop: Header=BB1_95 Depth=1
	movl	$46, %edx
	movl	%r14d, %edi
	leaq	104(%rsp), %rsi
	callq	cli_readn
	cmpl	$45, %eax
	jle	.LBB1_113
# BB#97:                                #   in Loop: Header=BB1_95 Depth=1
	movl	80(%rsp), %r15d         # 4-byte Reload
	addq	$46, %r15
	cmpq	88(%rsp), %r15          # 8-byte Folded Reload
	ja	.LBB1_115
# BB#98:                                #   in Loop: Header=BB1_95 Depth=1
	movzwl	132(%rsp), %r13d
	cmpl	$1025, %r13d            # imm = 0x401
	jae	.LBB1_116
# BB#99:                                #   in Loop: Header=BB1_95 Depth=1
	movzwl	134(%rsp), %r14d
	movzwl	136(%rsp), %esi
	movzwl	112(%rsp), %edi
	movl	120(%rsp), %eax
	movl	%eax, 8(%rbx)
	movl	124(%rsp), %eax
	movl	%eax, 4(%rbx)
	movl	128(%rsp), %ecx
	movl	%ecx, (%rbx)
	movl	146(%rsp), %edx
	addl	96(%rsp), %edx          # 4-byte Folded Reload
	movl	%edx, 12(%rbx)
	movzwl	114(%rsp), %edx
	movw	%dx, 20(%rbx)
	testw	%dx, %dx
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	je	.LBB1_102
# BB#100:                               # %.thread.i.i
                                        #   in Loop: Header=BB1_95 Depth=1
	cmpl	%ecx, %eax
	jne	.LBB1_104
# BB#101:                               #   in Loop: Header=BB1_95 Depth=1
	movw	%di, 72(%rsp)           # 2-byte Spill
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	72(%rsp), %edi          # 2-byte Folded Reload
	movw	$0, 22(%rbx)
	jmp	.LBB1_106
.LBB1_102:                              #   in Loop: Header=BB1_95 Depth=1
	cmpl	%ecx, %eax
	jne	.LBB1_105
.LBB1_104:                              #   in Loop: Header=BB1_95 Depth=1
	xorl	%eax, %eax
	jmp	.LBB1_107
.LBB1_105:                              #   in Loop: Header=BB1_95 Depth=1
	movw	%di, 72(%rsp)           # 2-byte Spill
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	72(%rsp), %edi          # 2-byte Folded Reload
	movw	$8, 22(%rbx)
.LBB1_106:                              #   in Loop: Header=BB1_95 Depth=1
	movl	$1, %eax
.LBB1_107:                              #   in Loop: Header=BB1_95 Depth=1
	movw	$-1, 22(%rbx,%rax,2)
	movw	%di, 26(%rbx)
	leaq	(%r13,%r15), %rax
	cmpq	88(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB1_117
# BB#108:                               #   in Loop: Header=BB1_95 Depth=1
	leaq	28(%rbx), %rsi
	movl	4(%rsp), %edi           # 4-byte Reload
	movl	%r13d, %edx
	callq	cli_readn
	cmpl	%r13d, %eax
	jne	.LBB1_118
# BB#109:                               #   in Loop: Header=BB1_95 Depth=1
	movb	$0, 28(%rbx,%r13)
	movw	%r13w, 18(%rbx)
	addl	%r15d, %r14d
	movq	80(%rsp), %rax          # 8-byte Reload
	addl	%r14d, %eax
	leal	(%r13,%rax), %edx
	cmpl	%r12d, %edx
	ja	.LBB1_119
# BB#110:                               #   in Loop: Header=BB1_95 Depth=1
	leaq	(%rbx,%r13), %rax
	leaq	33(%rbx,%r13), %rcx
	andl	$1, %ecx
	leaq	33(%rcx,%rax), %rax
	movl	%eax, %ecx
	andl	$2, %ecx
	addq	%rax, %rcx
	subq	%rbx, %rcx
	movw	%cx, 16(%rbx)
	movzwl	%cx, %eax
	addq	%rbx, %rax
	leaq	16(%rbx), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	8(%rsp), %ecx           # 4-byte Reload
	incl	%ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	cmpw	%cx, 22(%rsp)           # 2-byte Folded Reload
	movq	%rax, %rbx
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	jne	.LBB1_95
# BB#111:
	xorl	%ebx, %ebx
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_122
.LBB1_112:                              # %.critedge24
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_errmsg
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_71
.LBB1_113:
	cmpw	$0, 8(%rsp)             # 2-byte Folded Reload
	je	.LBB1_127
# BB#114:
	xorl	%ebx, %ebx
	jmp	.LBB1_121
.LBB1_115:
	movl	68(%rsp), %ebx          # 4-byte Reload
	subl	8(%rsp), %ebx           # 4-byte Folded Reload
	movl	$.L.str.25, %edi
	jmp	.LBB1_120
.LBB1_116:
	movl	68(%rsp), %ebx          # 4-byte Reload
	subl	8(%rsp), %ebx           # 4-byte Folded Reload
	movl	$.L.str.26, %edi
	jmp	.LBB1_120
.LBB1_117:
	movl	68(%rsp), %ebx          # 4-byte Reload
	subl	8(%rsp), %ebx           # 4-byte Folded Reload
	movl	$.L.str.31, %edi
	jmp	.LBB1_120
.LBB1_118:
	movl	68(%rsp), %ebx          # 4-byte Reload
	subl	8(%rsp), %ebx           # 4-byte Folded Reload
	movl	$.L.str.32, %edi
	jmp	.LBB1_120
.LBB1_119:
	movl	68(%rsp), %ebx          # 4-byte Reload
	subl	8(%rsp), %ebx           # 4-byte Folded Reload
	movl	$.L.str.33, %edi
.LBB1_120:                              # %.loopexit.i.i
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
.LBB1_121:                              # %.loopexit.i.i
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
.LBB1_122:                              # %.loopexit.i.i
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB1_124
# BB#123:
	movw	$0, (%rax)
	movq	%rdi, (%r15)
	testw	%bx, %bx
	jne	.LBB1_71
	jmp	.LBB1_125
.LBB1_124:
	callq	free
	testw	%bx, %bx
	jne	.LBB1_71
.LBB1_125:
	movq	24(%r13), %rax
	movq	%rax, 32(%r13)
	testq	%r14, %r14
	je	.LBB1_80
# BB#126:
	movl	$0, (%r14)
	jmp	.LBB1_80
.LBB1_127:                              # %.critedge25
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	$.L.str.24, %edi
	movl	$46, %esi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	jmp	.LBB1_71
.Lfunc_end1:
	.size	zip_dir_open, .Lfunc_end1-zip_dir_open
	.cfi_endproc

	.globl	zip_dir_read
	.p2align	4, 0x90
	.type	zip_dir_read,@function
zip_dir_read:                           # @zip_dir_read
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#1:
	testq	%rsi, %rsi
	je	.LBB2_4
# BB#2:
	movq	32(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB2_4
# BB#3:
	movzwl	20(%rcx), %eax
	movw	%ax, (%rsi)
	movl	4(%rcx), %eax
	movl	%eax, 4(%rsi)
	movl	(%rcx), %eax
	movl	%eax, 8(%rsi)
	leaq	28(%rcx), %rax
	movq	%rax, 16(%rsi)
	movzwl	26(%rcx), %eax
	movw	%ax, 12(%rsi)
	movl	12(%rcx), %eax
	movl	%eax, 28(%rsi)
	movl	8(%rcx), %eax
	movl	%eax, 24(%rsi)
	movzwl	16(%rcx), %eax
	addq	%rax, %rcx
	testq	%rax, %rax
	cmoveq	%rax, %rcx
	movq	%rcx, 32(%rdi)
	movl	$1, %eax
.LBB2_4:
	retq
.Lfunc_end2:
	.size	zip_dir_read, .Lfunc_end2-zip_dir_read
	.cfi_endproc

	.globl	zip_file_close
	.p2align	4, 0x90
	.type	zip_file_close,@function
zip_file_close:                         # @zip_file_close
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#2:
	leaq	64(%rbx), %rdi
	callq	inflateEnd
	movq	(%rbx), %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_6
# BB#3:
	cmpq	$0, 16(%r14)
	je	.LBB3_4
# BB#5:
	callq	free
	jmp	.LBB3_6
.LBB3_1:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, %eax
	jmp	.LBB3_10
.LBB3_4:
	movq	%rdi, 16(%r14)
.LBB3_6:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	cmpq	$0, 8(%r14)
	je	.LBB3_7
# BB#8:
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB3_9
.LBB3_7:
	movq	%rbx, 8(%r14)
.LBB3_9:
	xorl	%eax, %eax
.LBB3_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	zip_file_close, .Lfunc_end3-zip_file_close
	.cfi_endproc

	.globl	zip_file_open
	.p2align	4, 0x90
	.type	zip_file_open,@function
zip_file_open:                          # @zip_file_open
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 48
.Lcfi25:
	.cfi_offset %rbx, -48
.Lcfi26:
	.cfi_offset %r12, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB4_2
# BB#1:
	cmpl	$0, (%r15)
	js	.LBB4_2
# BB#3:
	movq	24(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB4_8
# BB#4:                                 # %.preheader
	cmpl	$-1, %r14d
	jne	.LBB4_9
# BB#5:                                 # %.preheader.split.us.preheader
	leaq	28(%rbp), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_11
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	16(%rbp), %eax
	testq	%rax, %rax
	je	.LBB4_50
# BB#7:                                 # %.preheader.split.us
                                        #   in Loop: Header=BB4_6 Depth=1
	leaq	(%rbp,%rax), %r12
	leaq	28(%rbp,%rax), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	movq	%r12, %rbp
	jne	.LBB4_6
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_49:                               #   in Loop: Header=BB4_9 Depth=1
	addq	%rax, %rbp
.LBB4_9:                                # %.preheader.split
                                        # =>This Inner Loop Header: Depth=1
	leaq	28(%rbp), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB4_48
# BB#10:                                #   in Loop: Header=BB4_9 Depth=1
	cmpl	%r14d, 12(%rbp)
	je	.LBB4_11
.LBB4_48:                               #   in Loop: Header=BB4_9 Depth=1
	movzwl	16(%rbp), %eax
	testq	%rax, %rax
	jne	.LBB4_49
.LBB4_50:                               # %.us-lcssa96.us
	movl	$-115, 4(%r15)
.LBB4_51:                               # %zip_file_close.exit
	xorl	%r14d, %r14d
	jmp	.LBB4_52
.LBB4_2:
	xorl	%r14d, %r14d
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB4_52:                               # %zip_file_close.exit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_8:
	xorl	%r14d, %r14d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, 4(%r15)
	jmp	.LBB4_52
.LBB4_11:
	movq	%rbp, %r12
.LBB4_12:                               # %.us-lcssa.us
	movzwl	20(%r12), %esi
	cmpq	$99, %rsi
	ja	.LBB4_15
# BB#13:                                # %.us-lcssa.us
	jmpq	*.LJTI4_0(,%rsi,8)
.LBB4_14:
	xorl	%r14d, %r14d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	$-125, 4(%r15)
	jmp	.LBB4_52
.LBB4_16:
	movq	8(%r15), %r14
	testq	%r14, %r14
	je	.LBB4_24
# BB#17:
	movq	$0, 8(%r15)
	jmp	.LBB4_18
.LBB4_24:
	movl	$1, %edi
	movl	$176, %esi
	callq	cli_calloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB4_25
.LBB4_18:
	movq	%r15, (%r14)
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB4_26
# BB#19:
	movq	%rax, 56(%r14)
	movq	$0, 16(%r15)
	jmp	.LBB4_20
.LBB4_15:
	xorl	%r14d, %r14d
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_errmsg
	movl	$-124, 4(%r15)
	jmp	.LBB4_52
.LBB4_26:
	movl	$32768, %edi            # imm = 0x8000
	callq	cli_malloc
	movq	%rax, 56(%r14)
	testq	%rax, %rax
	je	.LBB4_27
.LBB4_20:
	movl	(%r15), %edi
	movl	12(%r12), %esi
	xorl	%edx, %edx
	callq	lseek
	testq	%rax, %rax
	js	.LBB4_21
# BB#33:
	movq	56(%r14), %rbx
	movl	(%r15), %edi
	movl	$30, %edx
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$29, %eax
	jg	.LBB4_37
# BB#34:
	movslq	%eax, %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-123, 4(%r15)
	leaq	64(%r14), %rdi
	callq	inflateEnd
	movq	(%r14), %rbx
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_31
# BB#35:
	cmpq	$0, 16(%rbx)
	jne	.LBB4_30
# BB#36:
	movq	%rdi, 16(%rbx)
	jmp	.LBB4_31
.LBB4_21:
	movl	(%r15), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-123, 4(%r15)
	leaq	64(%r14), %rdi
	callq	inflateEnd
	movq	(%r14), %rbx
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_31
# BB#22:
	cmpq	$0, 16(%rbx)
	jne	.LBB4_30
# BB#23:
	movq	%rdi, 16(%rbx)
	jmp	.LBB4_31
.LBB4_37:
	movzwl	26(%rbx), %eax
	movzwl	28(%rbx), %esi
	addq	%rax, %rsi
	movl	(%r15), %edi
	movl	$1, %edx
	callq	lseek
	testq	%rax, %rax
	js	.LBB4_38
# BB#41:
	movl	(%r12), %eax
	movq	%rax, 40(%r14)
	movl	4(%r12), %ecx
	movq	%rcx, 48(%r14)
	leaq	22(%r12), %rcx
	movq	%rcx, 16(%r14)
	movzwl	20(%r12), %ecx
	movw	%cx, 8(%r14)
	movq	%rax, 24(%r14)
	testw	%cx, %cx
	je	.LBB4_52
# BB#42:
	leaq	64(%r14), %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%r14)
	movups	%xmm0, 144(%r14)
	movups	%xmm0, 128(%r14)
	movups	%xmm0, 112(%r14)
	movups	%xmm0, 96(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 64(%r14)
	movl	$-15, %esi
	movl	$.L.str.34, %edx
	movl	$112, %ecx
	movq	%rbx, %rdi
	callq	inflateInit2_
	testl	%eax, %eax
	je	.LBB4_43
# BB#44:
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-123, 4(%r15)
	movq	%rbx, %rdi
	callq	inflateEnd
	movq	(%r14), %rbx
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_31
# BB#45:
	cmpq	$0, 16(%rbx)
	jne	.LBB4_30
# BB#46:
	movq	%rdi, 16(%rbx)
	jmp	.LBB4_31
.LBB4_25:
	movl	$-114, 4(%r15)
	jmp	.LBB4_51
.LBB4_27:
	movl	$-114, 4(%r15)
	leaq	64(%r14), %rdi
	callq	inflateEnd
	movq	(%r14), %rbx
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_31
# BB#28:
	cmpq	$0, 16(%rbx)
	jne	.LBB4_30
# BB#29:
	movq	%rdi, 16(%rbx)
	jmp	.LBB4_31
.LBB4_38:
	movl	(%r15), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-123, 4(%r15)
	leaq	64(%r14), %rdi
	callq	inflateEnd
	movq	(%r14), %rbx
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_31
# BB#39:
	cmpq	$0, 16(%rbx)
	je	.LBB4_40
.LBB4_30:
	callq	free
.LBB4_31:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%r14)
	movups	%xmm0, 144(%r14)
	movups	%xmm0, 128(%r14)
	movups	%xmm0, 112(%r14)
	movups	%xmm0, 96(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 48(%r14)
	movups	%xmm0, 32(%r14)
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
	cmpq	$0, 8(%rbx)
	je	.LBB4_32
# BB#47:
	movq	%r14, %rdi
	callq	free
	jmp	.LBB4_51
.LBB4_32:
	movq	%r14, 8(%rbx)
	jmp	.LBB4_51
.LBB4_43:
	movl	4(%r12), %eax
	movq	%rax, 32(%r14)
	jmp	.LBB4_52
.LBB4_40:
	movq	%rdi, 16(%rbx)
	jmp	.LBB4_31
.Lfunc_end4:
	.size	zip_file_open, .Lfunc_end4-zip_file_open
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_16
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_16
	.quad	.LBB4_16
	.quad	.LBB4_14
	.quad	.LBB4_15
	.quad	.LBB4_14
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_14

	.text
	.globl	zip_file_read
	.p2align	4, 0x90
	.type	zip_file_read,@function
zip_file_read:                          # @zip_file_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB5_2
# BB#1:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.LBB5_2
# BB#3:
	movq	24(%rbx), %rax
	cmpq	%r14, %rax
	cmovbeq	%rax, %r14
	testq	%rax, %rax
	je	.LBB5_4
# BB#5:
	movzwl	8(%rbx), %eax
	leal	-8(%rax), %ecx
	movzwl	%cx, %ecx
	cmpl	$2, %ecx
	jae	.LBB5_6
# BB#11:
	leaq	64(%rbx), %r15
	movl	%r14d, 96(%rbx)
	movq	%rsi, 88(%rbx)
	.p2align	4, 0x90
.LBB5_12:                               # =>This Inner Loop Header: Depth=1
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.LBB5_16
# BB#13:                                #   in Loop: Header=BB5_12 Depth=1
	cmpl	$0, 72(%rbx)
	jne	.LBB5_16
# BB#14:                                #   in Loop: Header=BB5_12 Depth=1
	cmpq	$32768, %r12            # imm = 0x8000
	movl	$32768, %eax            # imm = 0x8000
	cmovaeq	%rax, %r12
	movl	(%r13), %edi
	movq	56(%rbx), %rsi
	movl	%r12d, %edx
	callq	cli_readn
	movslq	%eax, %rdx
	testl	%edx, %edx
	jle	.LBB5_28
# BB#15:                                # %.thread
                                        #   in Loop: Header=BB5_12 Depth=1
	subq	%rdx, 32(%rbx)
	movl	%edx, 72(%rbx)
	movq	56(%rbx), %rax
	movq	%rax, 64(%rbx)
.LBB5_16:                               #   in Loop: Header=BB5_12 Depth=1
	movq	104(%rbx), %rbp
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	inflate
	testl	%eax, %eax
	jne	.LBB5_17
# BB#22:                                #   in Loop: Header=BB5_12 Depth=1
	subq	104(%rbx), %rbp
	addq	24(%rbx), %rbp
	movq	%rbp, 24(%rbx)
	je	.LBB5_24
# BB#23:                                #   in Loop: Header=BB5_12 Depth=1
	cmpl	$0, 96(%rbx)
	jne	.LBB5_12
	jmp	.LBB5_24
.LBB5_2:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB5_26:                               # %.thread78
	movq	$-1, %r15
.LBB5_27:                               # %.thread78
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_4:
	xorl	%r15d, %r15d
	jmp	.LBB5_27
.LBB5_6:
	testw	%ax, %ax
	jne	.LBB5_25
# BB#7:
	movl	(%r13), %edi
	movl	%r14d, %edx
	callq	cli_readn
	movslq	%eax, %r15
	testl	%r15d, %r15d
	jle	.LBB5_9
# BB#8:
	subq	%r15, 24(%rbx)
	jmp	.LBB5_27
.LBB5_25:
	movzwl	%ax, %esi
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-104, 4(%r13)
	jmp	.LBB5_26
.LBB5_17:
	cmpl	$1, %eax
	jne	.LBB5_19
# BB#18:                                # %.thread80
	movq	$0, 24(%rbx)
.LBB5_24:                               # %.critedge
	movl	96(%rbx), %eax
	subq	%rax, %r14
	movq	%r14, %r15
	jmp	.LBB5_27
.LBB5_9:
	jns	.LBB5_27
# BB#10:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_errmsg
	movl	$-123, 4(%r13)
	jmp	.LBB5_27
.LBB5_19:
	movl	$-104, %eax
	movzwl	8(%rbx), %ecx
	cmpl	$9, %ecx
	jne	.LBB5_21
# BB#20:
	movl	$.L.str.11, %edi
	movl	$9, %esi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-125, %eax
.LBB5_21:
	movl	%eax, 4(%r13)
	jmp	.LBB5_26
.LBB5_28:
	movl	$-123, 4(%r13)
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_errmsg
	jmp	.LBB5_26
.Lfunc_end5:
	.size	zip_file_read, .Lfunc_end5-zip_file_read
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Unzip: zip_dir_open: Can't lseek descriptor %d\n"
	.size	.L.str, 48

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Unzip: zip_file_close: fp == NULL\n"
	.size	.L.str.1, 35

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Unzip: zip_file_open: dir == NULL || dir->fd <= 0\n"
	.size	.L.str.2, 51

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Unzip: zip_file_open: hdr == NULL\n"
	.size	.L.str.3, 35

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Unzip: zip_file_open: Not supported compression method (%d)\n"
	.size	.L.str.4, 61

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Unzip: zip_file_read: Unknown compression method (%d)\n"
	.size	.L.str.5, 55

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Unzip: zip_file_open: Can't lseek descriptor %d\n"
	.size	.L.str.6, 49

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Unzip: zip_file_open: Can't read zip header (only read %d bytes)\n"
	.size	.L.str.7, 66

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Unzip: zip_file_read: fp == NULL || fp->dir == NULL\n"
	.size	.L.str.8, 53

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Unzip: zip_file_read: Can't read %d bytes\n"
	.size	.L.str.9, 43

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Unzip: zip_file_read: Can't read %d bytes (read %d)\n"
	.size	.L.str.10, 53

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Unzip: zip_file_read: Not supported compression method (%u)\n"
	.size	.L.str.11, 61

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Unzip: __zip_dir_parse: Can't fstat file descriptor %d\n"
	.size	.L.str.12, 56

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Unzip: __zip_find_disk_trailer: File too short\n"
	.size	.L.str.14, 48

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Unzip: __zip_find_disk_trailer: Central directory not found\n"
	.size	.L.str.15, 61

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Unzip: __zip_find_disk_trailer: Can't lseek descriptor %d\n"
	.size	.L.str.16, 59

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Unzip: __zip_find_disk_trailer: Can't read %u bytes\n"
	.size	.L.str.17, 53

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Unzip: __zip_find_disk_trailer: u_rootseek > filesize, continue search\n"
	.size	.L.str.18, 72

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Unzip: __zip_find_disk_trailer: found file header at %u, shift %u\n"
	.size	.L.str.19, 67

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Unzip: __zip_parse_root_directory: Can't fstat file descriptor %d\n"
	.size	.L.str.20, 67

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Unzip: __zip_parse_root_directory: File contains no entries\n"
	.size	.L.str.21, 61

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Unzip: __zip_parse_root_directory: Incorrect root size\n"
	.size	.L.str.22, 56

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Unzip: __zip_parse_root_directory: Can't lseek descriptor %d\n"
	.size	.L.str.23, 62

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Unzip: __zip_parse_root_directory: Can't read %d bytes\n"
	.size	.L.str.24, 56

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Unzip: __zip_parse_root_directory: Entry %d outside of root directory\n"
	.size	.L.str.25, 71

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Unzip: __zip_parse_root_directory: Entry %d name too long\n"
	.size	.L.str.26, 59

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Unzip: __zip_parse_root_directory: File claims to be stored but csize != usize\n"
	.size	.L.str.27, 80

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Unzip: __zip_parse_root_directory: Also checking for method 'deflated'\n"
	.size	.L.str.28, 72

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Unzip: __zip_parse_root_directory: File claims to be deflated but csize == usize\n"
	.size	.L.str.29, 82

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Unzip: __zip_parse_root_directory: Also checking for method 'stored'\n"
	.size	.L.str.30, 70

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Unzip: __zip_parse_root_directory: Name of entry %d outside of root directory\n"
	.size	.L.str.31, 79

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Unzip: __zip_parse_root_directory: Can't read name of entry %d\n"
	.size	.L.str.32, 64

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Unzip: __zip_parse_root_directory: End of entry %d outside of root directory\n"
	.size	.L.str.33, 78

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"1.2.8"
	.size	.L.str.34, 6

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Unzip: __zip_inflate_init: inflateInit2 failed\n"
	.size	.L.str.35, 48


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
