	.text
	.file	"HYPRE_struct_grid.bc"
	.globl	HYPRE_StructGridCreate
	.p2align	4, 0x90
	.type	HYPRE_StructGridCreate,@function
HYPRE_StructGridCreate:                 # @HYPRE_StructGridCreate
	.cfi_startproc
# BB#0:
	jmp	hypre_StructGridCreate  # TAILCALL
.Lfunc_end0:
	.size	HYPRE_StructGridCreate, .Lfunc_end0-HYPRE_StructGridCreate
	.cfi_endproc

	.globl	HYPRE_StructGridDestroy
	.p2align	4, 0x90
	.type	HYPRE_StructGridDestroy,@function
HYPRE_StructGridDestroy:                # @HYPRE_StructGridDestroy
	.cfi_startproc
# BB#0:
	jmp	hypre_StructGridDestroy # TAILCALL
.Lfunc_end1:
	.size	HYPRE_StructGridDestroy, .Lfunc_end1-HYPRE_StructGridDestroy
	.cfi_endproc

	.globl	HYPRE_StructGridSetExtents
	.p2align	4, 0x90
	.type	HYPRE_StructGridSetExtents,@function
HYPRE_StructGridSetExtents:             # @HYPRE_StructGridSetExtents
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 64
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %r15
	movl	$0, 8(%rsp)
	movl	$0, 12(%rsp)
	movl	$0, 16(%rsp)
	movq	$0, 20(%rsp)
	movl	$0, 28(%rsp)
	movl	4(%r15), %eax
	testl	%eax, %eax
	jle	.LBB2_2
# BB#1:                                 # %.lr.ph
	decl	%eax
	leaq	4(,%rax,4), %rbx
	leaq	8(%rsp), %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	20(%rsp), %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
.LBB2_2:                                # %._crit_edge
	leaq	8(%rsp), %rsi
	leaq	20(%rsp), %rdx
	movq	%r15, %rdi
	callq	hypre_StructGridSetExtents
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	HYPRE_StructGridSetExtents, .Lfunc_end2-HYPRE_StructGridSetExtents
	.cfi_endproc

	.globl	HYPRE_StructGridSetPeriodic
	.p2align	4, 0x90
	.type	HYPRE_StructGridSetPeriodic,@function
HYPRE_StructGridSetPeriodic:            # @HYPRE_StructGridSetPeriodic
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$0, 4(%rsp)
	movl	$0, 12(%rsp)
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB3_2
# BB#1:                                 # %.lr.ph
	decl	%eax
	leaq	4(,%rax,4), %rdx
	leaq	4(%rsp), %rdi
	callq	memcpy
.LBB3_2:                                # %._crit_edge
	leaq	4(%rsp), %rsi
	movq	%rbx, %rdi
	callq	hypre_StructGridSetPeriodic
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	HYPRE_StructGridSetPeriodic, .Lfunc_end3-HYPRE_StructGridSetPeriodic
	.cfi_endproc

	.globl	HYPRE_StructGridAssemble
	.p2align	4, 0x90
	.type	HYPRE_StructGridAssemble,@function
HYPRE_StructGridAssemble:               # @HYPRE_StructGridAssemble
	.cfi_startproc
# BB#0:
	jmp	hypre_StructGridAssemble # TAILCALL
.Lfunc_end4:
	.size	HYPRE_StructGridAssemble, .Lfunc_end4-HYPRE_StructGridAssemble
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
