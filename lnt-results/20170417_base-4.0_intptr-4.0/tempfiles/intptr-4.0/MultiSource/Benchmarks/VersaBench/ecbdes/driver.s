	.text
	.file	"driver.bc"
	.globl	driver
	.p2align	4, 0x90
	.type	driver,@function
driver:                                 # @driver
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$9000, %rsp             # imm = 0x2328
.Lcfi6:
	.cfi_def_cfa_offset 9056
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movl	$key_data, %ebp
	leaq	288(%rsp), %rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	des_key_sched
	testl	%eax, %eax
	jne	.LBB0_12
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	incq	%r15
	addq	$8, %rbp
	addq	$256, %rbx              # imm = 0x100
	cmpq	$34, %r15
	jl	.LBB0_1
# BB#3:                                 # %.preheader18
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	testl	%r14d, %r14d
	jle	.LBB0_8
# BB#4:                                 # %.preheader.us.preheader
	movl	12(%rsp), %r14d         # 4-byte Reload
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
	movq	%r14, %r12
	leaq	288(%rsp), %r15
	leaq	16(%rsp), %rbp
	movl	$plain_data, %ebx
	.p2align	4, 0x90
.LBB0_6:                                #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	des_ecb_encrypt
	addq	$8, %rbx
	addq	$8, %rbp
	addq	$256, %r15              # imm = 0x100
	decq	%r12
	jne	.LBB0_6
# BB#7:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB0_5 Depth=1
	incl	%r13d
	cmpl	$1000000, %r13d         # imm = 0xF4240
	jne	.LBB0_5
.LBB0_8:                                # %.us-lcssa.us
	movl	12(%rsp), %eax          # 4-byte Reload
	shll	$3, %eax
	movslq	%eax, %rdx
	leaq	16(%rsp), %rsi
	movl	$cipher_data, %edi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_10
# BB#9:
	movl	$.Lstr.1, %edi
	jmp	.LBB0_11
.LBB0_10:
	movl	$.Lstr, %edi
.LBB0_11:
	callq	puts
	addq	$9000, %rsp             # imm = 0x2328
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_12:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	driver, .Lfunc_end0-driver
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	cmpl	$2, %edi
	jne	.LBB1_3
# BB#1:
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	cmpl	$35, %eax
	jl	.LBB1_2
.LBB1_3:
	movq	(%rbx), %rsi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.LBB1_2:
	movl	%eax, %edi
	callq	driver
	xorl	%edi, %edi
	callq	exit
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	key_data,@object        # @key_data
	.data
	.p2align	4
key_data:
	.zero	8
	.zero	8,255
	.asciz	"0\000\000\000\000\000\000"
	.zero	8,17
	.ascii	"\001#Eg\211\253\315\357"
	.zero	8,17
	.zero	8
	.ascii	"\376\334\272\230vT2\020"
	.ascii	"|\241\020EJ\032nW"
	.ascii	"\0011\331a\235\3017n"
	.ascii	"\007\241\023>J\013&\206"
	.ascii	"8IgL&\0021\236"
	.ascii	"\004\271\025\272C\376\265\266"
	.ascii	"\001\023\271p\3754\362\316"
	.ascii	"\001p\361uF\217\265\346"
	.ascii	"C)\177\2558\343s\376"
	.ascii	"\007\247\023pE\332*\026"
	.ascii	"\004h\221\004\302\375;/"
	.ascii	"7\320k\265\026\313uF"
	.ascii	"\037\b&\r\032\302F^"
	.ascii	"X@#d\032\272av"
	.ascii	"\002X\026\026F)\260\007"
	.ascii	"Iy>\274y\263%\217"
	.ascii	"O\260^\025\025\253s\247"
	.ascii	"I\351]mL\242)\277"
	.ascii	"\001\203\020\334@\233&\326"
	.ascii	"\034X\177\034\023\222O\357"
	.zero	8,1
	.ascii	"\037\037\037\037\016\016\016\016"
	.ascii	"\340\376\340\376\361\376\361\376"
	.zero	8
	.zero	8,255
	.ascii	"\001#Eg\211\253\315\357"
	.ascii	"\376\334\272\230vT2\020"
	.size	key_data, 272

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Key %d error!\n"
	.size	.L.str, 15

	.type	plain_data,@object      # @plain_data
	.data
	.p2align	4
plain_data:
	.zero	8
	.zero	8,255
	.ascii	"\020\000\000\000\000\000\000\001"
	.zero	8,17
	.zero	8,17
	.ascii	"\001#Eg\211\253\315\357"
	.zero	8
	.ascii	"\001#Eg\211\253\315\357"
	.ascii	"\001\241\326\3209wgB"
	.ascii	"\\\325L\250=\357W\332"
	.ascii	"\002H\3248\006\366qr"
	.ascii	"QEKX-\337D\n"
	.ascii	"B\375D0YW\177\242"
	.ascii	"\005\233^\bQ\317\024:"
	.ascii	"\007V\330\340wGa\322"
	.ascii	"v%\024\270)\277Hj"
	.ascii	";\335\021\220I7(\002"
	.ascii	"&\225_h5\257`\232"
	.ascii	"\026M^@O'R2"
	.ascii	"k\005n\030u\237\\\312"
	.ascii	"\000K\326\357\t\027`b"
	.ascii	"H\r9\000n\347b\362"
	.ascii	"Cu@\310i\217<\372"
	.ascii	"\007-C\240w\007R\222"
	.ascii	"\002\376Uw\201\027\361*"
	.ascii	"\035\235\\P\030\367(\302"
	.ascii	"0U2(mo)Z"
	.ascii	"\001#Eg\211\253\315\357"
	.ascii	"\001#Eg\211\253\315\357"
	.ascii	"\001#Eg\211\253\315\357"
	.zero	8,255
	.zero	8
	.zero	8
	.zero	8,255
	.size	plain_data, 272

	.type	cipher_data,@object     # @cipher_data
	.p2align	4
cipher_data:
	.ascii	"\214\246M\351\301\261#\247"
	.ascii	"sY\262\026>N\334X"
	.ascii	"\225\216nbz\005U{"
	.ascii	"\364\003y\253\236\016\3053"
	.ascii	"\027f\215\374r\222S-"
	.ascii	"\212Z\341\370\032\270\362\335"
	.ascii	"\214\246M\351\301\261#\247"
	.ascii	"\3559\331P\372t\274\304"
	.ascii	"i\017[\r\232&\223\233"
	.ascii	"z8\235\0205K\322q"
	.ascii	"\206\216\273Q\312\264Y\232"
	.ascii	"qx\207n\001\361\233*"
	.ascii	"\2577\373B\037\214@\225"
	.ascii	"\206\245`\361\016\306\330["
	.ascii	"\f\323\332\002\000!\334\t"
	.ascii	"\352gk,\267\333+z"
	.ascii	"\337\326J\201\\\257\032\017"
	.ascii	"\\Q<\234H\206\300\210"
	.ascii	"\n*\356\256?\364\253w"
	.ascii	"\357\033\360>]\372WZ"
	.ascii	"\210\277\r\266\327\r\356V"
	.ascii	"\241\371\221UA\002\013V"
	.ascii	"o\277\034\257\317\375\005V"
	.ascii	"/\"\344\233\253|\241\254"
	.ascii	"Zka,\302l\316J"
	.ascii	"_L\003\216\321+.A"
	.ascii	"c\372\300\3204\331\367\223"
	.asciz	"a{:\f\350\360q"
	.ascii	"\333\225\206\005\370\310\306\006"
	.ascii	"\355\277\321\306l)\314\307"
	.ascii	"5UP\262\025\016$Q"
	.ascii	"\312\252\257M\352\361\333\256"
	.ascii	"\325\324O\367 h=\r"
	.ascii	"*+\260\b\337\227\302\362"
	.size	cipher_data, 272

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"Driver for Data Encryption Standard benchmark.\n\nusage: %s <size> (where size <= 34)\n\nsize is the number of processing node for hardware version \n\n"
	.size	.L.str.3, 147

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Encrypted correctly."
	.size	.Lstr, 21

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Encryption error."
	.size	.Lstr.1, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
