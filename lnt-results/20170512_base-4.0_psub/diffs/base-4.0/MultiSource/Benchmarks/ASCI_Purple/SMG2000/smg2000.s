	.text
	.file	"smg2000.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_4:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_2:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI0_3:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$392, %rsp              # imm = 0x188
.Lcfi6:
	.cfi_def_cfa_offset 448
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edi, 20(%rsp)
	movq	%rsi, 288(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 368(%rsp)
	movq	$0, 384(%rsp)
	leaq	20(%rsp), %rdi
	leaq	288(%rsp), %rsi
	callq	hypre_MPI_Init
	xorl	%ebp, %ebp
	leaq	240(%rsp), %rsi
	xorl	%edi, %edi
	callq	hypre_MPI_Comm_size
	leaq	76(%rsp), %rsi
	xorl	%edi, %edi
	callq	hypre_MPI_Comm_rank
	movl	240(%rsp), %r14d
	movl	$-17, 260(%rsp)
	movl	$0, 264(%rsp)
	movl	$32, 268(%rsp)
	cmpl	$2, 20(%rsp)
	jl	.LBB0_31
# BB#1:                                 # %.lr.ph1002.lr.ph.lr.ph.lr.ph.lr.ph.lr.ph.preheader
	movl	$3, %r12d
	xorl	%ebp, %ebp
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	$10, %r15d
	movl	$1, %ecx
	movl	$10, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	$10, %edx
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	movl	$1, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movapd	%xmm0, 320(%rsp)        # 16-byte Spill
	movapd	%xmm0, 336(%rsp)        # 16-byte Spill
	movl	$1, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
.LBB0_2:                                # %.lr.ph1002.lr.ph.lr.ph.lr.ph.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
                                        #       Child Loop BB0_4 Depth 3
                                        #         Child Loop BB0_5 Depth 4
                                        #           Child Loop BB0_6 Depth 5
	movq	%r12, 128(%rsp)         # 8-byte Spill
.LBB0_3:                                # %.lr.ph1002.lr.ph.lr.ph.lr.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_4 Depth 3
                                        #         Child Loop BB0_5 Depth 4
                                        #           Child Loop BB0_6 Depth 5
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
.LBB0_4:                                # %.lr.ph1002.lr.ph.lr.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_5 Depth 4
                                        #           Child Loop BB0_6 Depth 5
	movapd	%xmm0, 192(%rsp)        # 16-byte Spill
.LBB0_5:                                # %.lr.ph1002
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_3 Depth=2
                                        #       Parent Loop BB0_4 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_6 Depth 5
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movslq	%ecx, %rdi
	movq	%rdi, %rax
	shlq	$32, %rax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	leaq	(%rax,%rcx), %r14
	movabsq	$8589934592, %rcx       # imm = 0x200000000
	leaq	(%rax,%rcx), %r13
	movabsq	$12884901888, %rcx      # imm = 0x300000000
	leaq	(%rax,%rcx), %rsi
	movq	%rdi, %r12
	leaq	24(,%rdi,8), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_6:                                #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_3 Depth=2
                                        #       Parent Loop BB0_4 Depth=3
                                        #         Parent Loop BB0_5 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	288(%rsp), %rcx
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(%rcx,%rax), %rbx
	movq	-24(%rbx,%r15,8), %rbp
	cmpb	$45, (%rbp)
	jne	.LBB0_19
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=5
	cmpb	$110, 1(%rbp)
	jne	.LBB0_10
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=5
	cmpb	$0, 2(%rbp)
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_6 Depth=5
	movq	-16(%rbx,%r15,8), %rdi
	movq	%rsi, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	-8(%rbx,%r15,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	(%rbx,%r15,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rbp, %rsi
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	movq	%rax, %rdx
	movslq	20(%rsp), %rax
	movabsq	$17179869184, %rcx      # imm = 0x400000000
	addq	%rcx, %r14
	addq	%rcx, %r13
	addq	%rcx, %rsi
	leaq	4(%r12,%r15), %rcx
	addq	$4, %r15
	cmpq	%rax, %rcx
	jl	.LBB0_6
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_10:                               # %.thread1542
                                        #   in Loop: Header=BB0_5 Depth=4
	cmpb	$80, 1(%rbp)
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	jne	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_5 Depth=4
	cmpb	$0, 2(%rbp)
	jne	.LBB0_13
# BB#12:                                # %.outer847
                                        #   in Loop: Header=BB0_5 Depth=4
	sarq	$29, %r14
	movq	(%rcx,%r14), %rdi
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rcx, %r14
	callq	strtol
	movq	%rax, %r12
	sarq	$29, %r13
	movq	(%r14,%r13), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	56(%rsp), %rbp          # 8-byte Reload
	leal	4(%rbp,%r15), %ebp
	sarq	$29, %rbx
	movq	(%r14,%rbx), %rdi
	movq	%r12, %r14
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rbp, %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	movq	%rax, 48(%rsp)          # 8-byte Spill
	cmpl	20(%rsp), %ecx
	jl	.LBB0_5
	jmp	.LBB0_156
.LBB0_13:                               # %.thread1545
                                        #   in Loop: Header=BB0_5 Depth=4
	cmpb	$98, 1(%rbp)
	movq	128(%rsp), %r12         # 8-byte Reload
	jne	.LBB0_16
# BB#14:                                #   in Loop: Header=BB0_5 Depth=4
	cmpb	$0, 2(%rbp)
	jne	.LBB0_16
# BB#15:                                # %.outer836
                                        #   in Loop: Header=BB0_5 Depth=4
	sarq	$29, %r14
	movq	(%rcx,%r14), %rdi
	movq	%rsi, %r14
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rcx, %rbx
	callq	strtol
	movq	%rax, 24(%rsp)          # 8-byte Spill
	sarq	$29, %r13
	movq	(%rbx,%r13), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rbp          # 8-byte Reload
	leal	4(%rbp,%r15), %ebp
	sarq	$29, %r14
	movq	(%rbx,%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rbp, %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	movq	%rax, 176(%rsp)         # 8-byte Spill
	cmpl	20(%rsp), %ecx
	movq	40(%rsp), %r14          # 8-byte Reload
	jl	.LBB0_5
	jmp	.LBB0_161
.LBB0_16:                               # %.thread1548
                                        #   in Loop: Header=BB0_4 Depth=3
	cmpb	$99, 1(%rbp)
	jne	.LBB0_22
# BB#17:                                #   in Loop: Header=BB0_4 Depth=3
	cmpb	$0, 2(%rbp)
	jne	.LBB0_22
# BB#18:                                # %.outer822
                                        #   in Loop: Header=BB0_4 Depth=3
	sarq	$29, %r14
	movq	(%rcx,%r14), %rdi
	movq	%rsi, %r14
	xorl	%esi, %esi
	movq	%rcx, %rbx
	callq	strtod
	movapd	%xmm0, 336(%rsp)        # 16-byte Spill
	sarq	$29, %r13
	movq	(%rbx,%r13), %rdi
	xorl	%esi, %esi
	callq	strtod
	movapd	%xmm0, 320(%rsp)        # 16-byte Spill
	movq	56(%rsp), %rbp          # 8-byte Reload
	leal	4(%rbp,%r15), %ebp
	sarq	$29, %r14
	movq	(%rbx,%r14), %rdi
	xorl	%esi, %esi
	callq	strtod
	movq	%rbp, %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	cmpl	20(%rsp), %ecx
	movq	40(%rsp), %r14          # 8-byte Reload
	jl	.LBB0_4
	jmp	.LBB0_161
	.p2align	4, 0x90
.LBB0_19:                               #   in Loop: Header=BB0_3 Depth=2
	movq	%rcx, %rbx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	addl	%r15d, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	128(%rsp), %r12         # 8-byte Reload
.LBB0_20:                               # %.thread1555
                                        #   in Loop: Header=BB0_3 Depth=2
	movl	$.L.str.6, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB0_157
# BB#21:                                # %.outer807
                                        #   in Loop: Header=BB0_3 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
	addl	$2, %ebp
	sarq	$29, %r14
	movq	(%rbx,%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rbp, %rcx
	movq	%rax, %rbp
	cmpl	20(%rsp), %ecx
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	jl	.LBB0_3
	jmp	.LBB0_34
.LBB0_22:                               # %.thread1551
                                        #   in Loop: Header=BB0_3 Depth=2
	cmpb	$118, 1(%rbp)
	jne	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_3 Depth=2
	cmpb	$0, 2(%rbp)
	je	.LBB0_28
.LBB0_24:                               # %.thread1554
                                        #   in Loop: Header=BB0_3 Depth=2
	movl	56(%rsp), %eax          # 4-byte Reload
	addq	%r15, %rax
	cmpb	$100, 1(%rbp)
	jne	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_3 Depth=2
	movq	%rcx, %rbx
	cmpb	$0, 2(%rbp)
	jne	.LBB0_27
	jmp	.LBB0_29
.LBB0_26:                               # %.thread1554..thread1555_crit_edge
                                        #   in Loop: Header=BB0_3 Depth=2
	movq	%rcx, %rbx
.LBB0_27:                               # %..thread1555_crit_edge
                                        #   in Loop: Header=BB0_3 Depth=2
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	24(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_20
.LBB0_28:                               # %.outer
                                        #   in Loop: Header=BB0_2 Depth=1
	sarq	$29, %r14
	movq	(%rcx,%r14), %rdi
	xorl	%esi, %esi
	movq	%rdx, %r14
	movl	$10, %edx
	movq	%rcx, %rbx
	callq	strtol
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rbp          # 8-byte Reload
	leal	3(%rbp,%r15), %ebp
	sarq	$29, %r13
	movq	(%rbx,%r13), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rbp, %rcx
	movq	%r14, %rdx
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	movq	%rax, 216(%rsp)         # 8-byte Spill
	cmpl	20(%rsp), %ecx
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_30
.LBB0_29:                               # %.outer791
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	56(%rsp), %rbp          # 8-byte Reload
	leal	2(%rbp,%r15), %ebp
	sarq	$29, %r14
	movq	(%rbx,%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rbp, %rcx
	movq	%rax, %r12
	cmpl	20(%rsp), %ecx
	movq	96(%rsp), %rbp          # 8-byte Reload
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
.LBB0_30:                               # %.outer791
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	40(%rsp), %r14          # 8-byte Reload
	jl	.LBB0_2
	jmp	.LBB0_34
.LBB0_31:
	movl	$10, %edx
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	$3, %r12d
	movl	$1, %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movapd	%xmm0, 320(%rsp)        # 16-byte Spill
	movapd	%xmm0, 336(%rsp)        # 16-byte Spill
	movl	$1, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	movl	$1, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$10, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	$10, %r15d
	jmp	.LBB0_34
.LBB0_32:
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB0_33:                               # %.thread773
	movq	128(%rsp), %r12         # 8-byte Reload
.LBB0_34:                               # %.thread773
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movapd	%xmm0, 192(%rsp)        # 16-byte Spill
	movl	%r14d, %ebx
	imull	8(%rsp), %ebx           # 4-byte Folded Reload
	movl	%ebx, %eax
	imull	48(%rsp), %eax          # 4-byte Folded Reload
	cmpl	240(%rsp), %eax
	jne	.LBB0_162
# BB#35:
	cmpl	$0, 76(%rsp)
	jne	.LBB0_37
# BB#36:
	movl	$.Lstr, %edi
	callq	puts
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movq	112(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	80(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	printf
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	48(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	printf
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	176(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	printf
	movl	$.L.str.26, %edi
	movb	$3, %al
	movapd	336(%rsp), %xmm0        # 16-byte Reload
	movapd	320(%rsp), %xmm1        # 16-byte Reload
	movapd	192(%rsp), %xmm2        # 16-byte Reload
	callq	printf
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	movq	224(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	216(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
.LBB0_37:
	xorl	%edi, %edi
	callq	hypre_MPI_Barrier
	movl	$.L.str.30, %edi
	callq	hypre_InitializeTiming
	movl	%eax, 300(%rsp)         # 4-byte Spill
	movl	%eax, %edi
	callq	hypre_BeginTiming
	cmpl	$3, %r12d
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	je	.LBB0_41
# BB#38:
	cmpl	$2, %r12d
	je	.LBB0_42
# BB#39:
	cmpl	$1, %r12d
	jne	.LBB0_43
# BB#40:
	movl	$2, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, %r15
	movl	$2, %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	$1, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, (%rbx)
	movl	$-1, (%rax)
	movl	$1, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, 8(%rbx)
	movl	$0, (%rax)
	movl	76(%rsp), %eax
	cltd
	idivl	%r14d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%rbx, 248(%rsp)         # 8-byte Spill
	movq	%rbx, 232(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movl	%r13d, %ebp
                                        # implicit-def: %EAX
	movq	%rax, 160(%rsp)         # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jmp	.LBB0_44
.LBB0_41:
	movq	112(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	imull	80(%rsp), %eax          # 4-byte Folded Reload
	imull	%r15d, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%r14, %r15
	movl	%eax, %r14d
	imull	176(%rsp), %r14d        # 4-byte Folded Reload
	imull	%r13d, %r14d
	movl	$4, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	$4, %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbp
	movl	$3, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, (%rbp)
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	movq	%rcx, (%rax)
	movl	$0, 8(%rax)
	movl	$3, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, 8(%rbp)
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	movq	%rcx, (%rax)
	movl	$0, 8(%rax)
	movl	$3, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, 16(%rbp)
	movq	$0, (%rax)
	movl	$-1, 8(%rax)
	movl	$3, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, 24(%rbp)
	movq	$0, (%rax)
	movl	$0, 8(%rax)
	movl	76(%rsp), %ecx
	movl	%ecx, %eax
	cltd
	idivl	%r15d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	subl	%edx, %ecx
	cltd
	idivl	8(%rsp)                 # 4-byte Folded Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	imull	%r15d, %eax
	movq	136(%rsp), %r15         # 8-byte Reload
	subl	%eax, %ecx
	movl	%ecx, %eax
	cltd
	idivl	%ebx
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	%rbp, 248(%rsp)         # 8-byte Spill
	movq	%rbp, 232(%rsp)         # 8-byte Spill
	movl	%r14d, %ebp
	jmp	.LBB0_44
.LBB0_42:
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	imull	112(%rsp), %r15d        # 4-byte Folded Reload
	movq	%r15, 168(%rsp)         # 8-byte Spill
	movl	%r13d, %ebp
	imull	64(%rsp), %ebp          # 4-byte Folded Reload
	movl	$3, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, %r15
	movl	$3, %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	$2, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, (%rbx)
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	movq	%rcx, (%rax)
	movl	$2, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, 8(%rbx)
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	movq	%rcx, (%rax)
	movl	$2, %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, 16(%rbx)
	movq	$0, (%rax)
	movl	76(%rsp), %eax
	cltd
	idivl	%r14d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	cltd
	idivl	8(%rsp)                 # 4-byte Folded Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 248(%rsp)         # 8-byte Spill
	movq	%rbx, 232(%rsp)         # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 160(%rsp)         # 8-byte Spill
	jmp	.LBB0_44
.LBB0_43:
                                        # implicit-def: %R15
                                        # implicit-def: %RAX
	movq	%rax, 248(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 232(%rsp)         # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 168(%rsp)         # 8-byte Spill
                                        # implicit-def: %EBP
                                        # implicit-def: %EAX
	movq	%rax, 160(%rsp)         # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 40(%rsp)          # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 120(%rsp)         # 8-byte Spill
.LBB0_44:
	movl	$8, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$8, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	jle	.LBB0_47
# BB#45:                                # %.lr.ph997.preheader
	movl	16(%rsp), %r14d         # 4-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph997
                                        # =>This Inner Loop Header: Depth=1
	movl	$4, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	%rax, (%rbx)
	movl	$4, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	%rax, (%rbp)
	addq	$8, %rbx
	addq	$8, %rbp
	decq	%r14
	jne	.LBB0_46
.LBB0_47:                               # %.preheader790
	testl	%r12d, %r12d
	jle	.LBB0_56
# BB#48:                                # %.lr.ph993.preheader
	movl	%r12d, %eax
	cmpl	$3, %r12d
	jbe	.LBB0_53
# BB#49:                                # %min.iters.checked
	movl	%r12d, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB0_53
# BB#50:                                # %vector.body.preheader
	leaq	368(%rsp), %rsi
	movapd	.LCPI0_1(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB0_51:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, 16(%rsi)
	movupd	%xmm0, (%rsi)
	addq	$32, %rsi
	addq	$-4, %rdi
	jne	.LBB0_51
# BB#52:                                # %middle.block
	testl	%edx, %edx
	jne	.LBB0_54
	jmp	.LBB0_56
.LBB0_53:
	xorl	%ecx, %ecx
.LBB0_54:                               # %.lr.ph993.preheader1945
	leaq	372(%rsp,%rcx,8), %rdx
	subq	%rcx, %rax
	movabsq	$4294967297, %rcx       # imm = 0x100000001
	.p2align	4, 0x90
.LBB0_55:                               # %.lr.ph993
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, -4(%rdx)
	addq	$8, %rdx
	decq	%rax
	jne	.LBB0_55
.LBB0_56:                               # %._crit_edge994
	cmpl	$1, %r12d
	movq	%r12, 128(%rsp)         # 8-byte Spill
	movq	%r15, 136(%rsp)         # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	je	.LBB0_69
# BB#57:                                # %._crit_edge994
	cmpl	$2, %r12d
	movq	32(%rsp), %r15          # 8-byte Reload
	je	.LBB0_72
# BB#58:                                # %._crit_edge994
	cmpl	$3, %r12d
	movq	176(%rsp), %rdx         # 8-byte Reload
	jne	.LBB0_81
# BB#59:                                # %.preheader788
	testl	%edx, %edx
	jle	.LBB0_81
# BB#60:                                # %.preheader787.lr.ph
	movq	120(%rsp), %rbp         # 8-byte Reload
	movl	%ebp, %r14d
	movq	24(%rsp), %rax          # 8-byte Reload
	imull	%eax, %r14d
	movl	264(%rsp), %ecx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movl	268(%rsp), %ecx
	movq	%rcx, 360(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	imull	%ecx, %esi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	160(%rsp), %rsi         # 8-byte Reload
	imull	%edx, %esi
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	movl	%eax, %esi
	imull	%ecx, %esi
	movl	%esi, 304(%rsp)         # 4-byte Spill
	incl	%r14d
	imull	%r15d, %r14d
	addl	$-18, %r14d
	movl	%r15d, %edi
	imull	%eax, %edi
	imull	%ebp, %edi
	addl	$-17, %edi
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_61:                               # %.preheader787
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_64 Depth 2
                                        #       Child Loop BB0_65 Depth 3
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_68
# BB#62:                                # %.preheader786.lr.ph
                                        #   in Loop: Header=BB0_61 Depth=1
	movq	24(%rsp), %r11          # 8-byte Reload
	testl	%r11d, %r11d
	jle	.LBB0_68
# BB#63:                                # %.preheader786.us.preheader
                                        #   in Loop: Header=BB0_61 Depth=1
	movq	160(%rsp), %rcx         # 8-byte Reload
	leal	(%rbp,%rcx), %edx
	movq	80(%rsp), %rax          # 8-byte Reload
	imull	%eax, %edx
	movq	360(%rsp), %rsi         # 8-byte Reload
	addl	%esi, %edx
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	leal	1(%rbp,%rcx), %ecx
	imull	%eax, %ecx
	leal	-1(%rcx,%rsi), %esi
	xorl	%r12d, %r12d
	movl	%ebx, 308(%rsp)         # 4-byte Spill
	movl	%ebx, %eax
	.p2align	4, 0x90
.LBB0_64:                               # %.preheader786.us
                                        #   Parent Loop BB0_61 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_65 Depth 3
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movslq	%eax, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%rcx,8), %r8
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rcx
	movq	40(%rsp), %rbp          # 8-byte Reload
	leal	(%r12,%rbp), %r9d
	movq	112(%rsp), %rax         # 8-byte Reload
	imull	%eax, %r9d
	movq	272(%rsp), %rbx         # 8-byte Reload
	addl	%ebx, %r9d
	leal	1(%r12,%rbp), %ebp
	imull	%eax, %ebp
	leal	-1(%rbp,%rbx), %r10d
	xorl	%ebp, %ebp
                                        # kill: %R11D<def> %R11D<kill> %R11<kill>
	.p2align	4, 0x90
.LBB0_65:                               #   Parent Loop BB0_61 Depth=1
                                        #     Parent Loop BB0_64 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rdi,%rbp), %r15d
	movq	(%r8), %r13
	movl	%r15d, (%r13)
	leal	(%r14,%rbp), %ebx
	movq	(%rcx), %rax
	movl	%ebx, (%rax)
	movl	%r9d, 4(%r13)
	movl	%r10d, 4(%rax)
	movl	%edx, 8(%r13)
	movl	%esi, 8(%rax)
	addq	$8, %r8
	addq	$8, %rcx
	addl	32(%rsp), %ebp          # 4-byte Folded Reload
	decl	%r11d
	jne	.LBB0_65
# BB#66:                                # %._crit_edge984.us
                                        #   in Loop: Header=BB0_64 Depth=2
	incl	%r12d
	movq	24(%rsp), %r11          # 8-byte Reload
	movl	56(%rsp), %eax          # 4-byte Reload
	addl	%r11d, %eax
	cmpl	64(%rsp), %r12d         # 4-byte Folded Reload
	jne	.LBB0_64
# BB#67:                                # %._crit_edge988.loopexit
                                        #   in Loop: Header=BB0_61 Depth=1
	movl	308(%rsp), %ebx         # 4-byte Reload
	addl	304(%rsp), %ebx         # 4-byte Folded Reload
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	120(%rsp), %rbp         # 8-byte Reload
.LBB0_68:                               # %._crit_edge988
                                        #   in Loop: Header=BB0_61 Depth=1
	incl	%ebp
	cmpl	%edx, %ebp
	jne	.LBB0_61
	jmp	.LBB0_81
.LBB0_69:                               # %.preheader782
	testl	%r13d, %r13d
	movq	32(%rsp), %rdi          # 8-byte Reload
	jle	.LBB0_81
# BB#70:                                # %.lr.ph973
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	120(%rsp), %rsi         # 8-byte Reload
	imull	%ebp, %esi
	movl	%ebp, %eax
	testb	$1, %al
	jne	.LBB0_78
# BB#71:
	movq	%rsi, %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	cmpl	$1, %ebp
	jne	.LBB0_79
	jmp	.LBB0_81
.LBB0_72:                               # %.preheader784
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_81
# BB#73:                                # %.preheader783.lr.ph
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_81
# BB#74:                                # %.preheader783.us.preheader
	movq	120(%rsp), %rdx         # 8-byte Reload
	movl	%edx, %r14d
	movq	24(%rsp), %rax          # 8-byte Reload
	imull	%eax, %r14d
	movl	264(%rsp), %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	imull	64(%rsp), %ecx          # 4-byte Folded Reload
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leal	-1(%rax), %ecx
	incq	%rcx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	incl	%r14d
	imull	%r15d, %r14d
	addl	$-18, %r14d
	movl	%r15d, %r13d
	imull	%eax, %r13d
	imull	%edx, %r13d
	addl	$-17, %r13d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_75:                               # %.preheader783.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_76 Depth 2
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%r10,%rax), %ebx
	movq	112(%rsp), %rcx         # 8-byte Reload
	imull	%ecx, %ebx
	movq	56(%rsp), %rdx          # 8-byte Reload
	addl	%edx, %ebx
	leal	1(%r10,%rax), %eax
	imull	%ecx, %eax
	leal	-1(%rax,%rdx), %edx
	movslq	%r11d, %r11
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%r11,8), %rcx
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r11,8), %rdi
	xorl	%ebp, %ebp
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	.p2align	4, 0x90
.LBB0_76:                               #   Parent Loop BB0_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r13,%rbp), %esi
	movq	(%rcx), %r8
	movl	%esi, (%r8)
	leal	(%r14,%rbp), %esi
	movq	(%rdi), %r9
	movl	%esi, (%r9)
	movl	%ebx, 4(%r8)
	movl	%edx, 4(%r9)
	addq	$8, %rcx
	addq	$8, %rdi
	addl	%r15d, %ebp
	decl	%eax
	jne	.LBB0_76
# BB#77:                                # %._crit_edge977.us
                                        #   in Loop: Header=BB0_75 Depth=1
	addq	272(%rsp), %r11         # 8-byte Folded Reload
	incl	%r10d
	cmpl	64(%rsp), %r10d         # 4-byte Folded Reload
	jne	.LBB0_75
	jmp	.LBB0_81
.LBB0_78:
	movl	%esi, %ecx
	imull	%edi, %ecx
	addl	$-17, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx), %rdx
	movl	%ecx, (%rdx)
	movq	%rsi, %rbx
	leal	1(%rsi), %ecx
	imull	%edi, %ecx
	addl	$-18, %ecx
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rdx
	movl	%ecx, (%rdx)
	movl	$1, %esi
	movl	$1, %edx
	cmpl	$1, %ebp
	je	.LBB0_81
.LBB0_79:                               # %.lr.ph973.new
	subq	%rdx, %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	8(%rcx,%rdx,8), %rcx
	movq	48(%rsp), %rbp          # 8-byte Reload
	leaq	8(%rbp,%rdx,8), %rdx
	leal	(%rsi,%rbx), %r8d
	leal	2(%rsi,%rbx), %r10d
	imull	%edi, %r10d
	addl	$-18, %r10d
	leal	(%rdi,%rdi), %r9d
	leal	1(%rsi,%rbx), %ebx
	imull	%edi, %ebx
	leal	-17(%rbx), %r11d
	imull	%edi, %r8d
	addl	$-17, %r8d
	addl	$-18, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_80:                               # =>This Inner Loop Header: Depth=1
	leal	(%r8,%rbp), %edi
	movq	-8(%rcx), %rsi
	movl	%edi, (%rsi)
	leal	(%rbx,%rbp), %esi
	movq	-8(%rdx), %rdi
	movl	%esi, (%rdi)
	leal	(%r11,%rbp), %esi
	movq	(%rcx), %rdi
	movl	%esi, (%rdi)
	leal	(%r10,%rbp), %esi
	movq	(%rdx), %rdi
	movl	%esi, (%rdi)
	addq	$16, %rcx
	addq	$16, %rdx
	addl	%r9d, %ebp
	addq	$-2, %rax
	jne	.LBB0_80
.LBB0_81:                               # %.loopexit
	leaq	184(%rsp), %rdx
	xorl	%edi, %edi
	movl	%r12d, %esi
	callq	HYPRE_StructGridCreate
	movq	184(%rsp), %rdi
	movl	16(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB0_84
# BB#82:                                # %.lr.ph968.preheader
	movl	%eax, %r14d
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	136(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_83:                               # %.lr.ph968
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rbp), %rdx
	callq	HYPRE_StructGridSetExtents
	movq	184(%rsp), %rdi
	addq	$8, %rbx
	addq	$8, %rbp
	decq	%r14
	jne	.LBB0_83
	jmp	.LBB0_85
.LBB0_84:
	movq	136(%rsp), %r13         # 8-byte Reload
.LBB0_85:                               # %._crit_edge969
	callq	HYPRE_StructGridAssemble
	leal	1(%r12), %r15d
	leaq	280(%rsp), %rdx
	movl	%r12d, %edi
	movl	%r15d, %esi
	callq	HYPRE_StructStencilCreate
	testl	%r12d, %r12d
	movq	232(%rsp), %r14         # 8-byte Reload
	js	.LBB0_88
# BB#86:                                # %.lr.ph964.preheader
	movl	%r15d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_87:                               # %.lr.ph964
                                        # =>This Inner Loop Header: Depth=1
	movq	280(%rsp), %rdi
	movq	(%r14,%rbx,8), %rdx
	movl	%ebx, %esi
	callq	HYPRE_StructStencilSetElement
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB0_87
.LBB0_88:                               # %._crit_edge965
	movq	184(%rsp), %rsi
	movq	280(%rsp), %rdx
	leaq	88(%rsp), %rcx
	xorl	%edi, %edi
	callq	HYPRE_StructMatrixCreate
	movq	88(%rsp), %rdi
	movl	$1, %esi
	callq	HYPRE_StructMatrixSetSymmetric
	movq	88(%rsp), %rdi
	leaq	368(%rsp), %rsi
	callq	HYPRE_StructMatrixSetNumGhost
	movq	88(%rsp), %rdi
	callq	HYPRE_StructMatrixInitialize
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	%r15d, %ecx
	movl	%eax, %r15d
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	imull	%ecx, %r15d
	movl	$8, %esi
	movl	%r15d, %edi
	callq	hypre_CAlloc
	testl	%r15d, %r15d
	movapd	192(%rsp), %xmm5        # 16-byte Reload
	jle	.LBB0_106
# BB#89:                                # %.preheader781.lr.ph
	movapd	.LCPI0_2(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	movapd	336(%rsp), %xmm3        # 16-byte Reload
	movapd	%xmm3, %xmm1
	xorpd	%xmm0, %xmm1
	movapd	320(%rsp), %xmm4        # 16-byte Reload
	movapd	%xmm4, %xmm2
	xorpd	%xmm0, %xmm2
	addsd	%xmm3, %xmm4
	xorpd	%xmm5, %xmm0
	addsd	%xmm4, %xmm5
	movl	64(%rsp), %edx          # 4-byte Reload
	movslq	%edx, %rsi
	movslq	%r15d, %rcx
	addsd	%xmm3, %xmm3
	addsd	%xmm4, %xmm4
	addsd	%xmm5, %xmm5
	movl	%edx, %edx
	movl	%edx, %r8d
	andl	$1, %r8d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_90:                               # %.preheader781
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_100 Depth 2
	testl	%r12d, %r12d
	js	.LBB0_105
# BB#91:                                # %.lr.ph949
                                        #   in Loop: Header=BB0_90 Depth=1
	cmpl	$1, %r12d
	je	.LBB0_94
# BB#92:                                # %.lr.ph949
                                        #   in Loop: Header=BB0_90 Depth=1
	cmpl	$2, %r12d
	jne	.LBB0_95
# BB#93:                                # %.lr.ph949.split.split.us.preheader
                                        #   in Loop: Header=BB0_90 Depth=1
	movl	$0, (%r13)
	movl	$1, 4(%r13)
	movl	$2, 8(%r13)
	movsd	%xmm1, (%rax,%rdi,8)
	movsd	%xmm2, 8(%rax,%rdi,8)
	movsd	%xmm4, 16(%rax,%rdi,8)
	jmp	.LBB0_105
	.p2align	4, 0x90
.LBB0_94:                               # %.lr.ph949.split.us.preheader
                                        #   in Loop: Header=BB0_90 Depth=1
	movl	$0, (%r13)
	movl	$1, 4(%r13)
	movsd	%xmm1, (%rax,%rdi,8)
	movsd	%xmm3, 8(%rax,%rdi,8)
	jmp	.LBB0_105
	.p2align	4, 0x90
.LBB0_95:                               # %.lr.ph949.split.split.preheader
                                        #   in Loop: Header=BB0_90 Depth=1
	testq	%r8, %r8
	jne	.LBB0_97
# BB#96:                                #   in Loop: Header=BB0_90 Depth=1
	xorl	%ebx, %ebx
	testl	%r12d, %r12d
	jne	.LBB0_100
	jmp	.LBB0_105
.LBB0_97:                               # %.lr.ph949.split.split.prol
                                        #   in Loop: Header=BB0_90 Depth=1
	movl	$0, (%r13)
	cmpl	$3, %r12d
	jne	.LBB0_99
# BB#98:                                # %.sink.split.prol
                                        #   in Loop: Header=BB0_90 Depth=1
	movsd	%xmm1, (%rax,%rdi,8)
	movsd	%xmm2, 8(%rax,%rdi,8)
	movsd	%xmm0, 16(%rax,%rdi,8)
	movsd	%xmm5, 24(%rax,%rdi,8)
.LBB0_99:                               # %.lr.ph949.split.split.prol.loopexit
                                        #   in Loop: Header=BB0_90 Depth=1
	movl	$1, %ebx
	testl	%r12d, %r12d
	je	.LBB0_105
	.p2align	4, 0x90
.LBB0_100:                              # %.lr.ph949.split.split
                                        #   Parent Loop BB0_90 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, (%r13,%rbx,4)
	cmpl	$3, %r12d
	jne	.LBB0_102
# BB#101:                               # %.sink.split
                                        #   in Loop: Header=BB0_100 Depth=2
	movsd	%xmm1, (%rax,%rdi,8)
	movsd	%xmm2, 8(%rax,%rdi,8)
	movsd	%xmm0, 16(%rax,%rdi,8)
	movsd	%xmm5, 24(%rax,%rdi,8)
.LBB0_102:                              # %.lr.ph949.split.split.12122
                                        #   in Loop: Header=BB0_100 Depth=2
	leaq	1(%rbx), %rbp
	cmpl	$3, %r12d
	movl	%ebp, 4(%r13,%rbx,4)
	jne	.LBB0_104
# BB#103:                               # %.sink.split.1
                                        #   in Loop: Header=BB0_100 Depth=2
	movsd	%xmm1, (%rax,%rdi,8)
	movsd	%xmm2, 8(%rax,%rdi,8)
	movsd	%xmm0, 16(%rax,%rdi,8)
	movsd	%xmm5, 24(%rax,%rdi,8)
.LBB0_104:                              #   in Loop: Header=BB0_100 Depth=2
	incq	%rbp
	cmpq	%rdx, %rbp
	movq	%rbp, %rbx
	jne	.LBB0_100
	.p2align	4, 0x90
.LBB0_105:                              # %._crit_edge950
                                        #   in Loop: Header=BB0_90 Depth=1
	addq	%rsi, %rdi
	cmpq	%rcx, %rdi
	jl	.LBB0_90
.LBB0_106:                              # %.preheader780
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movl	64(%rsp), %r15d         # 4-byte Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jle	.LBB0_109
# BB#107:                               # %.lr.ph947.preheader
	movl	16(%rsp), %r14d         # 4-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_108:                              # %.lr.ph947
                                        # =>This Inner Loop Header: Depth=1
	movq	88(%rsp), %rdi
	movq	(%rbx), %rsi
	movq	(%rbp), %rdx
	movl	%r15d, %ecx
	movq	%r13, %r8
	movq	32(%rsp), %r9           # 8-byte Reload
	callq	HYPRE_StructMatrixSetBoxValues
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	$8, %rbx
	addq	$8, %rbp
	decq	%r14
	jne	.LBB0_108
.LBB0_109:                              # %.preheader779
	movq	168(%rsp), %rcx         # 8-byte Reload
	testl	%ecx, %ecx
	jle	.LBB0_111
# BB#110:                               # %.lr.ph945.preheader
	leal	-1(%rcx), %ecx
	leaq	8(,%rcx,8), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	memset
.LBB0_111:                              # %.preheader778
	testl	%r12d, %r12d
	jle	.LBB0_120
# BB#112:                               # %.preheader778
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_120
# BB#113:                               # %.preheader777.us.preheader
	movl	16(%rsp), %eax          # 4-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	128(%rsp), %eax         # 4-byte Reload
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	$-17, %r13d
	xorl	%r12d, %r12d
	jmp	.LBB0_116
	.p2align	4, 0x90
.LBB0_114:                              # %._crit_edge941.us
                                        #   in Loop: Header=BB0_116 Depth=1
	leaq	1(%r12), %rax
	cmpq	112(%rsp), %rax         # 8-byte Folded Reload
	je	.LBB0_120
# BB#115:                               # %._crit_edge941.us..preheader777.us_crit_edge
                                        #   in Loop: Header=BB0_116 Depth=1
	movl	264(%rsp,%r12,4), %r13d
	movq	%rax, %r12
.LBB0_116:                              # %.preheader777.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_117 Depth 2
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_117:                              #   Parent Loop BB0_116 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rsi
	cmpl	%r13d, (%rsi,%r12,4)
	jne	.LBB0_119
# BB#118:                               #   in Loop: Header=BB0_117 Depth=2
	movq	(%rbp), %rdx
	movl	(%rdx,%r12,4), %ebx
	movl	%r13d, (%rdx,%r12,4)
	movq	136(%rsp), %r8          # 8-byte Reload
	movl	%r12d, (%r8)
	movq	88(%rsp), %rdi
	movl	$1, %ecx
	movq	32(%rsp), %r9           # 8-byte Reload
	callq	HYPRE_StructMatrixSetBoxValues
	movq	(%rbp), %rax
	movl	%ebx, (%rax,%r12,4)
.LBB0_119:                              #   in Loop: Header=BB0_117 Depth=2
	addq	$8, %r14
	addq	$8, %rbp
	decq	%r15
	jne	.LBB0_117
	jmp	.LBB0_114
.LBB0_120:                              # %._crit_edge943
	movq	88(%rsp), %rdi
	callq	HYPRE_StructMatrixAssemble
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movl	$8, %esi
	movq	168(%rsp), %r13         # 8-byte Reload
	movl	%r13d, %edi
	callq	hypre_CAlloc
	movq	%rax, %r14
	movq	184(%rsp), %rsi
	leaq	152(%rsp), %rdx
	xorl	%edi, %edi
	callq	HYPRE_StructVectorCreate
	movq	152(%rsp), %rdi
	callq	HYPRE_StructVectorInitialize
	testl	%r13d, %r13d
	movl	16(%rsp), %ebp          # 4-byte Reload
	jle	.LBB0_129
# BB#121:                               # %.lr.ph938.preheader
	movl	%r13d, %eax
	cmpl	$3, %r13d
	jbe	.LBB0_126
# BB#122:                               # %min.iters.checked1929
	movl	%r13d, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB0_126
# BB#123:                               # %vector.body1925.preheader
	leaq	16(%r14), %rsi
	movapd	.LCPI0_3(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB0_124:                              # %vector.body1925
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm0, (%rsi)
	addq	$32, %rsi
	addq	$-4, %rdi
	jne	.LBB0_124
# BB#125:                               # %middle.block1926
	testl	%edx, %edx
	jne	.LBB0_127
	jmp	.LBB0_129
.LBB0_126:
	xorl	%ecx, %ecx
.LBB0_127:                              # %.lr.ph938.preheader1942
	leaq	(%r14,%rcx,8), %rdx
	subq	%rcx, %rax
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB0_128:                              # %.lr.ph938
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rdx)
	addq	$8, %rdx
	decq	%rax
	jne	.LBB0_128
.LBB0_129:                              # %.preheader776
	movq	152(%rsp), %rdi
	testl	%ebp, %ebp
	jle	.LBB0_132
# BB#130:                               # %.lr.ph933.preheader
	movl	%ebp, %r15d
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_131:                              # %.lr.ph933
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	movq	(%rbx), %rdx
	movq	%r14, %rcx
	callq	HYPRE_StructVectorSetBoxValues
	movq	152(%rsp), %rdi
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r15
	jne	.LBB0_131
.LBB0_132:                              # %._crit_edge934
	callq	HYPRE_StructVectorAssemble
	movq	184(%rsp), %rsi
	leaq	144(%rsp), %rdx
	xorl	%edi, %edi
	callq	HYPRE_StructVectorCreate
	movq	144(%rsp), %rdi
	callq	HYPRE_StructVectorInitialize
	testl	%r13d, %r13d
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	%r13, %rax
	movq	136(%rsp), %r13         # 8-byte Reload
	jle	.LBB0_134
# BB#133:                               # %.lr.ph931.preheader
	decl	%eax
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
.LBB0_134:                              # %.preheader
	movq	144(%rsp), %rdi
	movl	16(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB0_137
# BB#135:                               # %.lr.ph927.preheader
	movl	%eax, %r15d
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_136:                              # %.lr.ph927
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	movq	(%rbx), %rdx
	movq	%r14, %rcx
	callq	HYPRE_StructVectorSetBoxValues
	movq	144(%rsp), %rdi
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r15
	jne	.LBB0_136
.LBB0_137:                              # %._crit_edge928
	callq	HYPRE_StructVectorAssemble
	movq	%r14, %rdi
	callq	hypre_Free
	movl	300(%rsp), %ebx         # 4-byte Reload
	movl	%ebx, %edi
	callq	hypre_EndTiming
	movl	$.L.str.30, %edi
	xorl	%esi, %esi
	callq	hypre_PrintTiming
	movl	%ebx, %edi
	callq	hypre_FinalizeTiming
	callq	hypre_ClearTiming
	movq	96(%rsp), %rbp          # 8-byte Reload
	testl	%ebp, %ebp
	je	.LBB0_141
# BB#138:
	movl	64(%rsp), %r15d         # 4-byte Reload
	jle	.LBB0_147
# BB#139:
	movl	$.L.str.35, %edi
	callq	hypre_InitializeTiming
	movl	%eax, %ebx
	movl	%ebx, %edi
	callq	hypre_BeginTiming
	movq	%rsp, %rsi
	xorl	%edi, %edi
	callq	HYPRE_StructPCGCreate
	movq	(%rsp), %rdi
	movl	$50, %esi
	callq	HYPRE_PCGSetMaxIter
	movq	(%rsp), %rdi
	movsd	.LCPI0_4(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	HYPRE_PCGSetTol
	movq	(%rsp), %rdi
	movl	$1, %esi
	callq	HYPRE_PCGSetTwoNorm
	movq	(%rsp), %rdi
	xorl	%esi, %esi
	callq	HYPRE_PCGSetRelChange
	movq	(%rsp), %rdi
	movl	$1, %esi
	callq	HYPRE_PCGSetLogging
	cmpl	$1, %ebp
	jne	.LBB0_142
# BB#140:
	leaq	104(%rsp), %rsi
	xorl	%edi, %edi
	callq	HYPRE_StructSMGCreate
	movq	104(%rsp), %rdi
	xorl	%esi, %esi
	callq	HYPRE_StructSMGSetMemoryUse
	movq	104(%rsp), %rdi
	movl	$1, %esi
	callq	HYPRE_StructSMGSetMaxIter
	movq	104(%rsp), %rdi
	xorpd	%xmm0, %xmm0
	callq	HYPRE_StructSMGSetTol
	movq	104(%rsp), %rdi
	callq	HYPRE_StructSMGSetZeroGuess
	movq	104(%rsp), %rdi
	movq	224(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	HYPRE_StructSMGSetNumPreRelax
	movq	104(%rsp), %rdi
	movq	216(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	HYPRE_StructSMGSetNumPostRelax
	movq	104(%rsp), %rdi
	xorl	%esi, %esi
	callq	HYPRE_StructSMGSetLogging
	movq	(%rsp), %rdi
	movq	104(%rsp), %rcx
	movl	$HYPRE_StructSMGSolve, %esi
	movl	$HYPRE_StructSMGSetup, %edx
	jmp	.LBB0_144
.LBB0_141:                              # %.thread774
	movl	$.L.str.31, %edi
	callq	hypre_InitializeTiming
	movl	%eax, %ebx
	movl	%ebx, %edi
	callq	hypre_BeginTiming
	movq	%rsp, %rsi
	xorl	%edi, %edi
	callq	HYPRE_StructSMGCreate
	movq	(%rsp), %rdi
	xorl	%esi, %esi
	callq	HYPRE_StructSMGSetMemoryUse
	movq	(%rsp), %rdi
	movl	$50, %esi
	callq	HYPRE_StructSMGSetMaxIter
	movq	(%rsp), %rdi
	movsd	.LCPI0_4(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	HYPRE_StructSMGSetTol
	movq	(%rsp), %rdi
	xorl	%esi, %esi
	callq	HYPRE_StructSMGSetRelChange
	movq	(%rsp), %rdi
	movq	224(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	HYPRE_StructSMGSetNumPreRelax
	movq	(%rsp), %rdi
	movq	216(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	HYPRE_StructSMGSetNumPostRelax
	movq	(%rsp), %rdi
	movl	$1, %esi
	callq	HYPRE_StructSMGSetLogging
	movq	(%rsp), %rdi
	movq	88(%rsp), %rsi
	movq	152(%rsp), %rdx
	movq	144(%rsp), %rcx
	callq	HYPRE_StructSMGSetup
	movl	%ebx, %edi
	callq	hypre_EndTiming
	movl	$.L.str.32, %edi
	xorl	%esi, %esi
	callq	hypre_PrintTiming
	movl	%ebx, %edi
	callq	hypre_FinalizeTiming
	callq	hypre_ClearTiming
	movl	$.L.str.33, %edi
	callq	hypre_InitializeTiming
	movl	%eax, %ebx
	movl	%ebx, %edi
	callq	hypre_BeginTiming
	movq	(%rsp), %rdi
	movq	88(%rsp), %rsi
	movq	152(%rsp), %rdx
	movq	144(%rsp), %rcx
	callq	HYPRE_StructSMGSolve
	movl	%ebx, %edi
	callq	hypre_EndTiming
	movl	$.L.str.34, %edi
	xorl	%esi, %esi
	callq	hypre_PrintTiming
	movl	%ebx, %edi
	callq	hypre_FinalizeTiming
	callq	hypre_ClearTiming
	movq	(%rsp), %rdi
	leaq	244(%rsp), %rsi
	callq	HYPRE_StructSMGGetNumIterations
	movq	(%rsp), %rdi
	leaq	312(%rsp), %rsi
	callq	HYPRE_StructSMGGetFinalRelativeResidualNorm
	movq	(%rsp), %rdi
	callq	HYPRE_StructSMGDestroy
	movl	64(%rsp), %r15d         # 4-byte Reload
	cmpl	$0, 76(%rsp)
	jne	.LBB0_149
	jmp	.LBB0_148
.LBB0_142:
	cmpl	$2, %ebp
	jne	.LBB0_145
# BB#143:
	movq	$0, 104(%rsp)
	movq	(%rsp), %rdi
	movl	$HYPRE_StructDiagScale, %esi
	movl	$HYPRE_StructDiagScaleSetup, %edx
	xorl	%ecx, %ecx
.LBB0_144:
	callq	HYPRE_PCGSetPrecond
.LBB0_145:
	movq	(%rsp), %rdi
	movq	88(%rsp), %rsi
	movq	152(%rsp), %rdx
	movq	144(%rsp), %rcx
	callq	HYPRE_PCGSetup
	movl	%ebx, %edi
	callq	hypre_EndTiming
	movl	$.L.str.32, %edi
	xorl	%esi, %esi
	callq	hypre_PrintTiming
	movl	%ebx, %edi
	callq	hypre_FinalizeTiming
	callq	hypre_ClearTiming
	movl	$.L.str.36, %edi
	callq	hypre_InitializeTiming
	movl	%eax, %ebx
	movl	%ebx, %edi
	callq	hypre_BeginTiming
	movq	(%rsp), %rdi
	movq	88(%rsp), %rsi
	movq	152(%rsp), %rdx
	movq	144(%rsp), %rcx
	callq	HYPRE_PCGSolve
	movl	%ebx, %edi
	callq	hypre_EndTiming
	movl	$.L.str.34, %edi
	xorl	%esi, %esi
	callq	hypre_PrintTiming
	movl	%ebx, %edi
	callq	hypre_FinalizeTiming
	callq	hypre_ClearTiming
	movq	(%rsp), %rdi
	leaq	244(%rsp), %rsi
	callq	HYPRE_PCGGetNumIterations
	movq	(%rsp), %rdi
	leaq	312(%rsp), %rsi
	callq	HYPRE_PCGGetFinalRelativeResidualNorm
	movq	(%rsp), %rdi
	callq	HYPRE_StructPCGDestroy
	cmpl	$1, %ebp
	jne	.LBB0_147
# BB#146:
	movq	104(%rsp), %rdi
	callq	HYPRE_StructSMGDestroy
.LBB0_147:
	cmpl	$0, 76(%rsp)
	jne	.LBB0_149
.LBB0_148:
	movl	$10, %edi
	callq	putchar
	movl	244(%rsp), %esi
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	312(%rsp), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.38, %edi
	movb	$1, %al
	callq	printf
	movl	$10, %edi
	callq	putchar
.LBB0_149:
	movq	184(%rsp), %rdi
	callq	HYPRE_StructGridDestroy
	movq	280(%rsp), %rdi
	callq	HYPRE_StructStencilDestroy
	movq	88(%rsp), %rdi
	callq	HYPRE_StructMatrixDestroy
	movq	152(%rsp), %rdi
	callq	HYPRE_StructVectorDestroy
	movq	144(%rsp), %rdi
	callq	HYPRE_StructVectorDestroy
	movl	16(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB0_152
# BB#150:                               # %.lr.ph924.preheader
	movl	%eax, %r14d
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_151:                              # %.lr.ph924
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	callq	hypre_Free
	movq	$0, (%rbx)
	movq	(%rbp), %rdi
	callq	hypre_Free
	movq	$0, (%rbp)
	addq	$8, %rbx
	addq	$8, %rbp
	decq	%r14
	jne	.LBB0_151
.LBB0_152:                              # %._crit_edge925
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	hypre_Free
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movq	%r13, %rdi
	callq	hypre_Free
	testl	%r12d, %r12d
	movq	232(%rsp), %rbp         # 8-byte Reload
	js	.LBB0_155
# BB#153:                               # %.lr.ph.preheader
	movl	%r15d, %ebx
	.p2align	4, 0x90
.LBB0_154:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	callq	hypre_Free
	movq	$0, (%rbp)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB0_154
.LBB0_155:                              # %._crit_edge
	movq	248(%rsp), %rdi         # 8-byte Reload
	callq	hypre_Free
	callq	hypre_MPI_Finalize
	xorl	%eax, %eax
	addq	$392, %rsp              # imm = 0x188
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_156:
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_33
.LBB0_157:
	movl	$.L.str.7, %esi
	movq	%rbp, %rdi
	callq	strcmp
	movl	%eax, %ebp
	movl	76(%rsp), %eax
	orl	%ebp, %eax
	movq	40(%rsp), %r14          # 8-byte Reload
	jne	.LBB0_159
# BB#158:
	movl	$10, %edi
	callq	putchar
	movq	288(%rsp), %rax
	movq	(%rax), %rsi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	movl	$.Lstr.6, %edi
	callq	puts
	movl	$.Lstr.7, %edi
	callq	puts
	movl	$.Lstr.8, %edi
	callq	puts
	movl	$.Lstr.9, %edi
	callq	puts
	movl	$.Lstr.10, %edi
	callq	puts
	movl	$.Lstr.11, %edi
	callq	puts
	movl	$.Lstr.12, %edi
	callq	puts
	movl	$10, %edi
	callq	putchar
.LBB0_159:
	testl	%ebp, %ebp
	movq	96(%rsp), %rbp          # 8-byte Reload
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	jne	.LBB0_34
# BB#160:
	movl	$1, %edi
	callq	exit
.LBB0_161:
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_34
.LBB0_162:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"-solver"
	.size	.L.str.6, 8

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"-help"
	.size	.L.str.7, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Usage: %s [<options>]\n"
	.size	.L.str.9, 23

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"  (nx, ny, nz)    = (%d, %d, %d)\n"
	.size	.L.str.23, 34

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"  (Px, Py, Pz)    = (%d, %d, %d)\n"
	.size	.L.str.24, 34

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"  (bx, by, bz)    = (%d, %d, %d)\n"
	.size	.L.str.25, 34

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"  (cx, cy, cz)    = (%f, %f, %f)\n"
	.size	.L.str.26, 34

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"  (n_pre, n_post) = (%d, %d)\n"
	.size	.L.str.27, 30

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"  dim             = %d\n"
	.size	.L.str.28, 24

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"  solver ID       = %d\n"
	.size	.L.str.29, 24

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Struct Interface"
	.size	.L.str.30, 17

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"SMG Setup"
	.size	.L.str.31, 10

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Setup phase times"
	.size	.L.str.32, 18

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"SMG Solve"
	.size	.L.str.33, 10

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Solve phase times"
	.size	.L.str.34, 18

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"PCG Setup"
	.size	.L.str.35, 10

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"PCG Solve"
	.size	.L.str.36, 10

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Iterations = %d\n"
	.size	.L.str.37, 17

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Final Relative Residual Norm = %e\n"
	.size	.L.str.38, 35

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Running with these driver parameters:"
	.size	.Lstr, 38

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Error: Invalid number of processors or processor topology "
	.size	.Lstr.1, 59

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"  -n <nx> <ny> <nz>    : problem size per block"
	.size	.Lstr.2, 48

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"  -P <Px> <Py> <Pz>    : processor topology"
	.size	.Lstr.3, 44

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"  -b <bx> <by> <bz>    : blocking per processor"
	.size	.Lstr.4, 48

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"  -c <cx> <cy> <cz>    : diffusion coefficients"
	.size	.Lstr.5, 48

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"  -v <n_pre> <n_post>  : number of pre and post relaxations"
	.size	.Lstr.6, 60

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"  -d <dim>             : problem dimension (2 or 3)"
	.size	.Lstr.7, 52

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	"  -solver <ID>         : solver ID (default = 0)"
	.size	.Lstr.8, 49

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	"                         0 - SMG"
	.size	.Lstr.9, 33

	.type	.Lstr.10,@object        # @str.10
	.p2align	4
.Lstr.10:
	.asciz	"                         1 - CG with SMG precond"
	.size	.Lstr.10, 49

	.type	.Lstr.11,@object        # @str.11
	.p2align	4
.Lstr.11:
	.asciz	"                         2 - CG with diagonal scaling"
	.size	.Lstr.11, 54

	.type	.Lstr.12,@object        # @str.12
	.p2align	4
.Lstr.12:
	.asciz	"                         3 - CG"
	.size	.Lstr.12, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
