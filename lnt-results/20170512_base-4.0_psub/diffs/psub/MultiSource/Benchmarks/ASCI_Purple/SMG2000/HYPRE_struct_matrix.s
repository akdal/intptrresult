	.text
	.file	"HYPRE_struct_matrix.bc"
	.globl	HYPRE_StructMatrixCreate
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixCreate,@function
HYPRE_StructMatrixCreate:               # @HYPRE_StructMatrixCreate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	callq	hypre_StructMatrixCreate
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	HYPRE_StructMatrixCreate, .Lfunc_end0-HYPRE_StructMatrixCreate
	.cfi_endproc

	.globl	HYPRE_StructMatrixDestroy
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixDestroy,@function
HYPRE_StructMatrixDestroy:              # @HYPRE_StructMatrixDestroy
	.cfi_startproc
# BB#0:
	jmp	hypre_StructMatrixDestroy # TAILCALL
.Lfunc_end1:
	.size	HYPRE_StructMatrixDestroy, .Lfunc_end1-HYPRE_StructMatrixDestroy
	.cfi_endproc

	.globl	HYPRE_StructMatrixInitialize
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixInitialize,@function
HYPRE_StructMatrixInitialize:           # @HYPRE_StructMatrixInitialize
	.cfi_startproc
# BB#0:
	jmp	hypre_StructMatrixInitialize # TAILCALL
.Lfunc_end2:
	.size	HYPRE_StructMatrixInitialize, .Lfunc_end2-HYPRE_StructMatrixInitialize
	.cfi_endproc

	.globl	HYPRE_StructMatrixSetValues
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixSetValues,@function
HYPRE_StructMatrixSetValues:            # @HYPRE_StructMatrixSetValues
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 32
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	8(%rdi), %r9
	cmpl	$0, 4(%r9)
	jle	.LBB3_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%r10,4), %eax
	movl	%eax, 12(%rsp,%r10,4)
	incq	%r10
	movslq	4(%r9), %rax
	cmpq	%rax, %r10
	jl	.LBB3_2
.LBB3_3:                                # %._crit_edge
	leaq	12(%rsp), %rsi
	xorl	%r9d, %r9d
	callq	hypre_StructMatrixSetValues
	addq	$24, %rsp
	retq
.Lfunc_end3:
	.size	HYPRE_StructMatrixSetValues, .Lfunc_end3-HYPRE_StructMatrixSetValues
	.cfi_endproc

	.globl	HYPRE_StructMatrixSetBoxValues
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixSetBoxValues,@function
HYPRE_StructMatrixSetBoxValues:         # @HYPRE_StructMatrixSetBoxValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 80
.Lcfi9:
	.cfi_offset %rbx, -48
.Lcfi10:
	.cfi_offset %r12, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movl	%ecx, %r12d
	movq	%rdi, %rbx
	movl	$0, 8(%rsp)
	movl	$0, 12(%rsp)
	movl	$0, 16(%rsp)
	movq	$0, 20(%rsp)
	movl	$0, 28(%rsp)
	movq	8(%rbx), %rax
	cmpl	$0, 4(%rax)
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx,4), %edi
	movl	%edi, 8(%rsp,%rcx,4)
	movl	(%rdx,%rcx,4), %edi
	movl	%edi, 20(%rsp,%rcx,4)
	incq	%rcx
	movslq	4(%rax), %rdi
	cmpq	%rdi, %rcx
	jl	.LBB4_2
.LBB4_3:                                # %._crit_edge
	callq	hypre_BoxCreate
	movq	%rax, %rbp
	leaq	8(%rsp), %rsi
	leaq	20(%rsp), %rdx
	movq	%rbp, %rdi
	callq	hypre_BoxSetExtents
	xorl	%r9d, %r9d
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movl	%r12d, %edx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	hypre_StructMatrixSetBoxValues
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	hypre_BoxDestroy
	movl	%ebx, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	HYPRE_StructMatrixSetBoxValues, .Lfunc_end4-HYPRE_StructMatrixSetBoxValues
	.cfi_endproc

	.globl	HYPRE_StructMatrixAddToValues
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixAddToValues,@function
HYPRE_StructMatrixAddToValues:          # @HYPRE_StructMatrixAddToValues
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 32
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	8(%rdi), %r9
	cmpl	$0, 4(%r9)
	jle	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%r10,4), %eax
	movl	%eax, 12(%rsp,%r10,4)
	incq	%r10
	movslq	4(%r9), %rax
	cmpq	%rax, %r10
	jl	.LBB5_2
.LBB5_3:                                # %._crit_edge
	leaq	12(%rsp), %rsi
	movl	$1, %r9d
	callq	hypre_StructMatrixSetValues
	addq	$24, %rsp
	retq
.Lfunc_end5:
	.size	HYPRE_StructMatrixAddToValues, .Lfunc_end5-HYPRE_StructMatrixAddToValues
	.cfi_endproc

	.globl	HYPRE_StructMatrixAddToBoxValues
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixAddToBoxValues,@function
HYPRE_StructMatrixAddToBoxValues:       # @HYPRE_StructMatrixAddToBoxValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 80
.Lcfi21:
	.cfi_offset %rbx, -48
.Lcfi22:
	.cfi_offset %r12, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movl	%ecx, %r12d
	movq	%rdi, %rbx
	movl	$0, 8(%rsp)
	movl	$0, 12(%rsp)
	movl	$0, 16(%rsp)
	movq	$0, 20(%rsp)
	movl	$0, 28(%rsp)
	movq	8(%rbx), %rax
	cmpl	$0, 4(%rax)
	jle	.LBB6_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx,4), %edi
	movl	%edi, 8(%rsp,%rcx,4)
	movl	(%rdx,%rcx,4), %edi
	movl	%edi, 20(%rsp,%rcx,4)
	incq	%rcx
	movslq	4(%rax), %rdi
	cmpq	%rdi, %rcx
	jl	.LBB6_2
.LBB6_3:                                # %._crit_edge
	callq	hypre_BoxCreate
	movq	%rax, %rbp
	leaq	8(%rsp), %rsi
	leaq	20(%rsp), %rdx
	movq	%rbp, %rdi
	callq	hypre_BoxSetExtents
	movl	$1, %r9d
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movl	%r12d, %edx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	hypre_StructMatrixSetBoxValues
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	hypre_BoxDestroy
	movl	%ebx, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	HYPRE_StructMatrixAddToBoxValues, .Lfunc_end6-HYPRE_StructMatrixAddToBoxValues
	.cfi_endproc

	.globl	HYPRE_StructMatrixAssemble
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixAssemble,@function
HYPRE_StructMatrixAssemble:             # @HYPRE_StructMatrixAssemble
	.cfi_startproc
# BB#0:
	jmp	hypre_StructMatrixAssemble # TAILCALL
.Lfunc_end7:
	.size	HYPRE_StructMatrixAssemble, .Lfunc_end7-HYPRE_StructMatrixAssemble
	.cfi_endproc

	.globl	HYPRE_StructMatrixSetNumGhost
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixSetNumGhost,@function
HYPRE_StructMatrixSetNumGhost:          # @HYPRE_StructMatrixSetNumGhost
	.cfi_startproc
# BB#0:
	jmp	hypre_StructMatrixSetNumGhost # TAILCALL
.Lfunc_end8:
	.size	HYPRE_StructMatrixSetNumGhost, .Lfunc_end8-HYPRE_StructMatrixSetNumGhost
	.cfi_endproc

	.globl	HYPRE_StructMatrixGetGrid
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixGetGrid,@function
HYPRE_StructMatrixGetGrid:              # @HYPRE_StructMatrixGetGrid
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	HYPRE_StructMatrixGetGrid, .Lfunc_end9-HYPRE_StructMatrixGetGrid
	.cfi_endproc

	.globl	HYPRE_StructMatrixSetSymmetric
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixSetSymmetric,@function
HYPRE_StructMatrixSetSymmetric:         # @HYPRE_StructMatrixSetSymmetric
	.cfi_startproc
# BB#0:
	movl	%esi, 72(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	HYPRE_StructMatrixSetSymmetric, .Lfunc_end10-HYPRE_StructMatrixSetSymmetric
	.cfi_endproc

	.globl	HYPRE_StructMatrixPrint
	.p2align	4, 0x90
	.type	HYPRE_StructMatrixPrint,@function
HYPRE_StructMatrixPrint:                # @HYPRE_StructMatrixPrint
	.cfi_startproc
# BB#0:
	jmp	hypre_StructMatrixPrint # TAILCALL
.Lfunc_end11:
	.size	HYPRE_StructMatrixPrint, .Lfunc_end11-HYPRE_StructMatrixPrint
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
